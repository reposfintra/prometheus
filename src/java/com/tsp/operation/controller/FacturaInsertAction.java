/*
 * FacturaInsertAction.java
 *
 * Created on 6 de enero de 2005, 09:00 AM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  AMARTINEZ
 */

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.pdf.*;
import com.tsp.util.*;
import java.text.*;
import com.tsp.operation.model.threads.*;

public class FacturaInsertAction extends Action{

    /** Creates a new instance of ConsultarConductor */
    public FacturaInsertAction() {

    }

    public void run() throws ServletException, InformationException {

        HttpSession session = request.getSession();
        String distrito = (String) session.getAttribute("Distrito");
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String dstrct   = usuario.getDstrct();
        String base = usuario.getBase();

        com.tsp.finanzas.contab.model.Model modelcontab = (com.tsp.finanzas.contab.model.Model) session.getAttribute("modelcontab");

        Date fecha = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String actual = format.format(fecha);


        String busqueda = (request.getParameter("busqueda")!=null)?request.getParameter("busqueda"):"";
        String aceptar = (request.getParameter("aceptar")!=null)?request.getParameter("aceptar"):"";
        String regresar = (request.getParameter("regresar")!=null)?request.getParameter("regresar"):"";
        String cargar = (request.getParameter("cargar")!=null)?request.getParameter("cargar"):"";
        String recargar = (request.getParameter("recargar")!=null)?request.getParameter("recargar"):"";
        String eliminar = (request.getParameter("eliminar")!=null)?request.getParameter("eliminar"):"";
        String modificar = (request.getParameter("modificar")!=null)?request.getParameter("modificar"):"";
        String filtrar = (request.getParameter("filtrar")!=null)?request.getParameter("filtrar"):"";
        String codigocuenta = (request.getParameter("codigocuenta")!=null)?request.getParameter("codigocuenta"):"";
        String Verificarcuenta = (request.getParameter("Verificarcuenta")!=null)?request.getParameter("Verificarcuenta"):"";
        String imprimir = (request.getParameter("imprimir")!=null)?request.getParameter("imprimir"):"";
        String cmc= (request.getParameter("ForHC")!=null)?request.getParameter("ForHC"):"";
        String cargarcodigo = (request.getParameter("cargarcodigo")!=null)?request.getParameter("cargarcodigo"):"";

        String cargarDocumento= (request.getParameter("cargarDocumento")!=null)?request.getParameter("cargarDocumento"):"";

        String opcion = (request.getParameter("opcion")!=null)?request.getParameter("opcion"):"";
        /*************************************
         *
         ************************************/
        String cliente  = (request.getParameter("codcli")!=null)?request.getParameter("codcli"):"";
        String Agregar= (request.getParameter("Agregar")!=null)?request.getParameter("Agregar"):"";

        String next="/jsp/cxcobrar/facturacion/Iniciofacturacion.jsp";

        String impchk = (request.getParameter("impchk")!=null)?request.getParameter("impchk"):"";

        try{
            if(opcion.equals("BUSCAR_FACTURA_CAMBIO")){
                String factura = request.getParameter("factura");
                model.facturaService.buscarFacturaCambio(usuario.getDstrct(), factura);
                if(model.facturaService.getFactu()==null){
                    next="/jsp/cxcobrar/facturacion/Cambio_impresion.jsp?men=La factura no existe";
                    model.facturaService.setFactu(new factura());
                }else{
                    next="/jsp/cxcobrar/facturacion/Cambio_impresion.jsp";
                    model.agenciaService.loadAgencias();
                }
            }else if(opcion.equals("UPDATE_IMPRESION_CAMBIO")){
                String factura           = model.facturaService.getFactu().getFactura();
                String agencia_impresion = request.getParameter("Agencia");
                String formato           = request.getParameter("formato");

                model.facturaService.updateImpresion(usuario.getDstrct(), factura, agencia_impresion,formato);
                model.facturaService.buscarFacturaCambio(usuario.getDstrct(), factura);
                model.agenciaService.loadAgencias();
                next="/jsp/cxcobrar/facturacion/Cambio_impresion.jsp?men=Fatura guardada con exito";

            }else if(opcion.equals("ELIMINAR")){
                next="/jsp/cxcobrar/facturacion/FacturacionAceptada.jsp";
                String id = request.getParameter("id");
                if(model.facturaService.getRemesasFacturar().size()>1){
                    factura_detalle Remesas_fac = (factura_detalle) model.facturaService.getRemesasFacturar().get(Integer.parseInt(id));
                    if(!Remesas_fac.getNumero_remesa().equals("")){

                        //recorremos todas las remesas para marcar la remesa eliminada del listado a facturar
                        for(int i=0; i<model.facturaService.getRemesas().size();i++){
                            factura_detalle remesas = (factura_detalle) model.facturaService.getRemesas().get(i);
                            if(Remesas_fac.getNumero_remesa().equals(remesas.getNumero_remesa())){
                                remesas.setIncluida(false);
                                break;
                            }

                        }//fin del for que recorre todas la remesas
                    }//fin if(!Remesas_fac.getNumero_remesa().equals(""))

                    //Removemos del listado el item
                    model.facturaService.getRemesasFacturar().remove(Integer.parseInt(id));
                    //este metodo calcula el total de los item a facturar
                    model.facturaService.calcularTotal();

                }
            } else if(opcion.equals("FACTURAR_TODO")){

                /************************************************
                 * /**Se mete para anexar las remesas escojidas****/
                /***********************************************/
                next="/jsp/cxcobrar/facturacion/FacturacionAceptada.jsp?close=ok&reload=ok";



                Vector a = new Vector();
                Vector remesasModificadas = model.facturaService.getRemesasFacturar();
                String s = "";
                String[] c={"",""};
                double total =0;


                for(int i=0; i<model.facturaService.getRemesas().size();i++){
                    factura_detalle factu_deta = (factura_detalle) ((factura_detalle)model.facturaService.getRemesas().get(i)).clonee();
                    factu_deta.setTiposubledger("");
                    factu_deta.setAuxliliar("");
                    factu_deta.setAux("N");
                    if(!s.equals(factu_deta.getCodigo_cuenta_contable())){
                        c=model.facturaService.buscarplancuentas(factu_deta.getCodigo_cuenta_contable());

                        if(c[1].equals("S")){
                            try{
                                modelcontab.subledgerService.buscarCuentasTipoSubledger(factu_deta.getCodigo_cuenta_contable());
                                factu_deta.setTiposub(modelcontab.subledgerService.getCuentastsubledger());
                            }catch(Exception o){
                                o.printStackTrace();
                            }
                        }else{
                            factu_deta.setTiposub(null);
                            factu_deta.setTiposubledger("");
                            factu_deta.setAuxliliar("");
                        }

                    }
                    factu_deta.setAux(c[1]);


                    int modif=0;
                    for(int j=0;j<remesasModificadas.size();j++){
                        factura_detalle factu_det =(factura_detalle)remesasModificadas.get(j);
                        if(factu_det.getNumero_remesa().equals(factu_deta.getNumero_remesa())){
                            modif++;
                            factu_deta= factu_det;

                            remesasModificadas.remove(j);
                        }
                    }

                    ((factura_detalle) model.facturaService.getRemesas().get(i)).setIncluida(true);
                    Vector cr= model.facturaService.costoReembolsable(factu_deta.getNumero_remesa());


                    if(modif>0){
                        a.add(factu_deta);
                        total += factu_deta.getValor_item();
                    }
                    else{
                        if(model.facturaService.getFactu().getMoneda().equals(factu_deta.getMoneda())){
                            //System.out.println("iguales");
                            model.facturaService.getFactu().setValor_tasa(new Double("1"));
                            factu_deta.setValor_tasa(new Double("1"));
                            factu_deta.setValor_unitariome(factu_deta.getValor_unitariome());
                            factu_deta.setValor_itemme( factu_deta.getValor_itemme());

                        }
                        else{

                            //System.out.println("diferentes");
                            try{
                                Tasa tasas = model.tasaService.buscarValorTasa( model.facturaService.getMonedaLocal(),factu_deta.getMoneda(),model.facturaService.getFactu().getMoneda(), actual);


                                //System.out.println("val:"+factu_deta.getValor_unitario());
                                //System.out.println("tasa:"+model.facturaService.getFactu().getValor_tasa().doubleValue());
                                double val2 = model.facturaService.getFactu().getValor_tasa().doubleValue();

                                if(tasas!=null){
                                    double valoruni=0;
                                    if(tasas.getValor_tasa1()==tasas.getValor_tasa2()){
                                        valoruni = factu_deta.getValor_unitario()*tasas.getValor_tasa1();
                                        //System.out.println("if:"+valoruni );
                                        //ESTA FUE LA QUE COMENT
                                        model.facturaService.getFactu().setValor_tasa(new Double(tasas.getValor_tasa1()));
                                    }
                                    else{
                                        valoruni = factu_deta.getValor_unitario()*val2*tasas.getValor_tasa2();

                                        //ESTA FUE LA QUE COMENT
                                        model.facturaService.getFactu().setValor_tasa(new Double(tasas.getValor_tasa2()));
                                        //System.out.println("else:"+valoruni );
                                    }
                                    //System.out.println("tasas!= "+valoruni);
                                    factu_deta.setValor_unitario(valoruni);
                                    factu_deta.setValor_item(this.getValor(valoruni*factu_deta.getCantidad().doubleValue(),model.facturaService.getFactu().getMoneda()));
                                    factu_deta.setValor_itemme(factu_deta.getValor_item());
                                    factu_deta.setValor_unitariome(factu_deta.getValor_unitario());
                                    factu_deta.setMoneda(model.facturaService.getFactu().getMoneda());
                                    factu_deta.setValor_tasa(new Double(""+tasas.getValor_tasa2()));

                                }

                            }catch(Exception e){
                                e.printStackTrace();
                            }

                        }



                        a.add(factu_deta);
                        total += factu_deta.getValor_item();
                        //Para calcular el valor en moneda si es diferente para los costos reembolsables
                        for(int f=0;f<cr.size();f++){
                            factura_detalle factu_d = (factura_detalle)cr.get(f);


                            if(!s.equals(factu_d.getCodigo_cuenta_contable())){
                                c=model.facturaService.buscarplancuentas(factu_d.getCodigo_cuenta_contable());

                                if(c[1].equals("S")){
                                    try{
                                        modelcontab.subledgerService.buscarCuentasTipoSubledger(factu_d.getCodigo_cuenta_contable());
                                        factu_d.setTiposub(modelcontab.subledgerService.getCuentastsubledger());
                                    }catch(Exception o){
                                        o.printStackTrace();
                                    }
                                }else{
                                    factu_d.setTiposub(null);
                                    factu_d.setTiposubledger("");
                                    factu_d.setAuxliliar("");
                                }

                            }
                            factu_d.setAux(c[1]);


                            /////////////////
                            if(model.facturaService.getFactu().getMoneda().equals(factu_d.getMoneda())){
                                //System.out.println("iguales");
                                factu_d.setValor_tasa(new Double("1"));
                                factu_d.setValor_unitariome(factu_d.getValor_unitariome());
                                factu_d.setValor_itemme( factu_d.getValor_itemme());

                            }
                            else{

                                //System.out.println("diferentes");
                                try{
                                    Tasa tasas = model.tasaService.buscarValorTasa( model.facturaService.getMonedaLocal(),factu_d.getMoneda(),model.facturaService.getFactu().getMoneda(), actual);


                                    //System.out.println("val:"+factu_deta.getValor_unitario());
                                    //System.out.println("tasa:"+model.facturaService.getFactu().getValor_tasa().doubleValue());
                                    double val2 = model.facturaService.getFactu().getValor_tasa().doubleValue();

                                    if(tasas!=null){
                                        double valoruni=0;
                                        if(tasas.getValor_tasa1()==tasas.getValor_tasa2()){
                                            valoruni = factu_d.getValor_unitario()*tasas.getValor_tasa1();
                                            //System.out.println("if:"+valoruni );
                                        }
                                        else{
                                            valoruni = factu_d.getValor_unitario()*val2*tasas.getValor_tasa2();
                                            //System.out.println("else:"+valoruni );
                                        }
                                        //System.out.println("tasas!= "+valoruni);
                                        factu_d.setValor_unitario(valoruni);
                                        factu_d.setValor_item(this.getValor(valoruni*factu_d.getCantidad().doubleValue(),model.facturaService.getFactu().getMoneda()));
                                        factu_d.setValor_itemme(factu_d.getValor_item());
                                        factu_d.setValor_unitariome(factu_d.getValor_unitario());
                                        factu_d.setMoneda(model.facturaService.getFactu().getMoneda());
                                        factu_d.setValor_tasa(new Double(""+tasas.getValor_tasa2()));

                                    }

                                }catch(Exception e){
                                    e.printStackTrace();
                                }

                            }
                            /////////////////

                            a.add(factu_d);
                            total += factu_d.getValor_item();
                        }

                    }

                }



                // por ultimo se agregan los items modificados o manuales

                for(int j=0;j<remesasModificadas.size();j++){
                    a.add((factura_detalle)remesasModificadas.get(j));
                    total += ((factura_detalle)remesasModificadas.get(j)).getValor_item();
                }
                model.facturaService.setRemesasFacturar(a);
                model.facturaService.getFactu().setValor_factura(total);


            }




            if(Agregar.equals("BUSCAR_ITEM")){
                next="/jsp/cxcobrar/facturacion/FacturacionAceptada.jsp";
                String item = request.getParameter("item");
                String item_buscar = request.getParameter("item_buscar");
                String maxfila = request.getParameter("numerofilas");
                int x = ( !com.tsp.util.Util.coalesce(item,"").equals(""))? Integer.parseInt(item):1;
                Vector a = model.facturaService.getRemesasFacturar();

                /****************************************/
                //BLOQUE DE LA CABECERA
                /**************************************/

                String fpago = (request.getParameter("textfield4")!=null)?request.getParameter("textfield4"):"";
                String zona = (request.getParameter("zona")!=null)?request.getParameter("zona"):"";
                String afacturacion= (request.getParameter("textfield5")!=null)?request.getParameter("textfield5"):"";
                String plazo = (request.getParameter("textfield8")!=null)?request.getParameter("textfield8"):"";
                String vtotal = (request.getParameter("textfield9")!=null)?request.getParameter("textfield9"):"0";
                String ffactura = (request.getParameter("c_fecha")!=null)?request.getParameter("c_fecha"):"";
                String fvencimiento = (request.getParameter("c_fecha2")!=null)?request.getParameter("c_fecha2"):"";
                String descripcion =(request.getParameter("textarea")!=null)?request.getParameter("textarea"):"";
                String observacion =(request.getParameter("textarea2")!=null)?request.getParameter("textarea2"):"";
                String moneda =(request.getParameter("mone")!=null)?request.getParameter("mone"):"";
                String tasa = (request.getParameter("textfield")!=null)?request.getParameter("textfield"):"";
                cmc  =(request.getParameter("ForHC")!=null)?request.getParameter("ForHC"):"";
                System.out.println("cmc*889*"+cmc);

                /**************************************
                 * setea datos enviados del formulario
                 ***************************************/

                if(tasa.equals("")){
                    tasa+="0";
                }

                if(vtotal.equals("")){
                    vtotal+="0";
                }
                model.facturaService.getFactu().setForma_pago(fpago);
                model.facturaService.getFactu().setAgencia_facturacion(afacturacion);
                model.facturaService.getFactu().setPlazo(plazo);
                model.facturaService.getFactu().setValor_factura(Double.parseDouble(vtotal.replaceAll(",","")));
                model.facturaService.getFactu().setFecha_factura(ffactura);
                model.facturaService.getFactu().setPlazo(fvencimiento);
                model.facturaService.getFactu().setDescripcion(descripcion);
                model.facturaService.getFactu().setObservacion(observacion);
                model.facturaService.getFactu().setMoneda(moneda);
                model.facturaService.getFactu().setValor_tasa(new Double(tasa.replaceAll(",","")));
                model.facturaService.getFactu().setZona(zona);



                /*************************************************/


                if ( a != null && a.size()>0){
                    int itemFin = Integer.parseInt(maxfila);

                    for( x=x ;x<=itemFin;x++){
                        factura_detalle factu_deta = (factura_detalle)a.get(x-1);
                        String textfield11= (request.getParameter("textfield11"+(x-1))!=null)?request.getParameter("textfield11"+(x-1)):"";
                        String textfield12= (request.getParameter("textfield12"+(x-1))!=null)?request.getParameter("textfield12"+(x-1)):"";
                        String textfield13= (request.getParameter("textfield13"+(x-1))!=null)?request.getParameter("textfield13"+(x-1)):"";
                        String textfield14= (request.getParameter("textfield14"+(x-1))!=null)?request.getParameter("textfield14"+(x-1)):"";
                        String textfield15= (request.getParameter("textfield15"+(x-1))!=null)?request.getParameter("textfield15"+(x-1)):"";
                        String textfield16= (request.getParameter("textfield16"+(x-1))!=null)?request.getParameter("textfield16"+(x-1)):"";
                        String textfield17= (request.getParameter("textfield17"+(x-1))!=null)?request.getParameter("textfield17"+(x-1)):"";
                        String textfield18= (request.getParameter("textfield18"+(x-1))!=null)?request.getParameter("textfield18"+(x-1)):"";
                        String textfield19= (request.getParameter("textfield19"+(x-1))!=null)?request.getParameter("textfield19"+(x-1)):"";

                        String hiddenField= (request.getParameter("hiddenField"+(x-1))!=null)?request.getParameter("hiddenField"+(x-1)):"0";
                        String hiddenField2= (request.getParameter("hiddenField2"+(x-1))!=null)?request.getParameter("hiddenField2"+(x-1)):"0";
                        String hiddenField3= (request.getParameter("hiddenField3"+(x-1))!=null)?request.getParameter("hiddenField3"+(x-1)):"0";

                        //Nuevos Campos para auxiliar
                        String tipo=(request.getParameter("tipo"+(x-1))!=null)?request.getParameter("tipo"+(x-1)):"";
                        String auxiliar=(request.getParameter("auxiliar"+(x-1))!=null)?request.getParameter("auxiliar"+(x-1)):"";
                        String aux=(request.getParameter("aux"+(x-1))!=null)?request.getParameter("aux"+(x-1)):"";

                        if(!(textfield11.equals("")&&textfield12.equals("")&&textfield13.equals("")&&textfield14.equals("")&&textfield15.equals("")&&textfield16.equals("")&&textfield17.equals("")&&textfield18.equals(""))){

                            factu_deta.setNumero_remesa(textfield11);
                            factu_deta.setFecrem(textfield12);
                            factu_deta.setDescripcion(textfield13);
                            factu_deta.setTiposubledger(tipo);
                            factu_deta.setAuxliliar(auxiliar);
                            factu_deta.setAux(aux);



                            if(textfield14.equals(""))
                                textfield14+="0";
                            if(textfield15.equals(""))
                                textfield15+="0";
                            if(textfield16.equals(""))
                                textfield16+="0";
                            if(hiddenField.equals(""))
                                hiddenField+="0";
                            if(hiddenField2.equals(""))
                                hiddenField2+="0";
                            if(hiddenField3.equals(""))
                                hiddenField3+="0";

                            factu_deta.setCantidad(new Double(textfield14.replaceAll(",","")));
                            factu_deta.setUnidad(textfield18);
                            factu_deta.setValor_unitario(Double.parseDouble((textfield15.replaceAll(",",""))));
                            factu_deta.setValor_unitariome(Double.parseDouble(hiddenField2));
                            factu_deta.setCodigo_cuenta_contable(textfield17);

                            String[] c={"",""};
                            c=model.facturaService.buscarplancuentas(factu_deta.getCodigo_cuenta_contable());

                            if(c[1].equals("S")){
                                try{
                                    modelcontab.subledgerService.buscarCuentasTipoSubledger(factu_deta.getCodigo_cuenta_contable());
                                    factu_deta.setTiposub(modelcontab.subledgerService.getCuentastsubledger());
                                }catch(Exception o){
                                    o.printStackTrace();
                                }
                            }else{
                                factu_deta.setTiposub(null);
                                factu_deta.setTiposubledger("");
                                factu_deta.setAuxliliar("");
                            }
                            factu_deta.setAux(c[1]);



                            factu_deta.setMoneda( model.facturaService.getFactu().getMoneda());

                            factu_deta.setValor_tasa(new Double(hiddenField));
                            factu_deta.setValor_item(Double.parseDouble((textfield16.replaceAll(",",""))));
                            factu_deta.setValor_itemme(Double.parseDouble(hiddenField3));
                        }


                    }

                    model.facturaService.setRemesasFacturar(a);
                }
                //esta opcion es para agregar un item vacio
                if(opcion.equals("BUSCAR_ULTIMO")){
                    int it = 1;

                    for(int i=0; i<=a.size();i+=20){
                        if((a.size()-19)<i){
                            it = i+1;
                            break;
                        }
                    }

                    next+="?item_buscar="+it;
                    factura_detalle factu_deta = new factura_detalle();
                    factu_deta.setNumero_remesa("");
                    factu_deta.setFecrem("");
                    factu_deta.setDescripcion("");
                    factu_deta.setTiposubledger("");
                    factu_deta.setAuxliliar("");
                    factu_deta.setAux("");
                    factu_deta.setCantidad(new Double(0));
                    factu_deta.setUnidad("");
                    factu_deta.setValor_unitario(0);
                    factu_deta.setValor_unitariome(0);
                    factu_deta.setCodigo_cuenta_contable("");
                    factu_deta.setMoneda( model.facturaService.getFactu().getMoneda());
                    factu_deta.setValor_tasa(new Double(1));
                    factu_deta.setValor_item(0);
                    factu_deta.setValor_itemme(0);


                    model.facturaService.getRemesasFacturar().add(factu_deta);

                }else
                    next+="?item_buscar="+item_buscar;

                model.facturaService.calcularTotal();

            }else if(imprimir.equals("ok")){

                //System.out.println("request.getContextPath()----------->"+request.getContextPath());
                String ruta = application.getRealPath( "/" );

                FacturaPDF facturapdf = new FacturaPDF("", dstrct,  impchk, usuario.getLogin() );
                try{

                    facturapdf.generarPDF(ruta,model,usuario.getLogin());
                }catch(Exception e){
                    e.printStackTrace();
                }
                next = "/pdf/FacturaPDF.pdf";


            } else
                if(imprimir.equals("uno")){

                    String ruta = application.getRealPath( "/" );
                    String f = (request.getParameter("factura")!=null)?request.getParameter("factura"):"";

                    FacturaPDF facturapdf = new FacturaPDF(f, dstrct,  impchk, usuario.getLogin() );
                    try{

                        facturapdf.generarPDF(ruta,model,usuario.getLogin());
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                    next = "/pdf/FacturaPDF.pdf";

                } else /*Modificacion 05-02-07*/
                    if(imprimir.equals("chk")){

                        String ruta = application.getRealPath( "/" );
                        String f = (request.getParameter("factura")!=null)?request.getParameter("factura"):"";

                        Vector vf = model.facturaService.getVFacturas();

                        for ( int j  = 0; vf!=null && j<vf.size(); j++){
                            ((factura)vf.elementAt(j)).setEstadoImpresion(false);
                        }


                        String datos[] = Util.coalesce( request.getParameter("LOV"), "" ).split(",");
                        for ( int i  = 0; datos!=null && i<datos.length; i++){
                            ((factura)vf.elementAt( Integer.parseInt(datos[i]) )).setEstadoImpresion(true);
                        }

                        model.facturaService.setVFacturas(vf);

                        FacturaPDF facturapdf = new FacturaPDF(f, dstrct, impchk, usuario.getLogin() );
                        try{

                            facturapdf.generarPDF(ruta,model,usuario.getLogin());
                        }catch(Exception e){
                            e.printStackTrace();
                        }
                        next = "/pdf/FacturaPDF.pdf";
                    }
            if(Verificarcuenta.equals("ok")){

                String forma= (request.getParameter("forma")!=null)?request.getParameter("forma"):"";
                String cuenta= (request.getParameter("cuenta")!=null)?request.getParameter("cuenta"):"";

                String campo = (request.getParameter("campo")!=null)?request.getParameter("campo"):"";
                String client = (request.getParameter("cliente")!=null)?request.getParameter("cliente"):"";
                String nit = (request.getParameter("nit")!=null)?request.getParameter("nit"):"";
                next="/jsp/cxcobrar/facturacion/CodigoCuenta.jsp?campo="+campo+"&cliente="+client+"&nit="+nit;

                if(forma.equals("1")){
                    String c[] ={"",""};
                    c=model.facturaService.buscarplancuentas(cuenta);
                    String s = model.facturaService.buscarsubledger(cuenta,nit);

                    if(!c[0].equals("")){

                        if(!s.equals("")){

                            next="/jsp/cxcobrar/facturacion/CodigoCuenta.jsp?campo="+campo+"&cliente="+client+"&nit="+nit+"&close=ok"+"&cuenta="+cuenta+"_IT_"+nit+"&descripcion="+c[0]+"-"+s;
                        }
                        else
                            next="/jsp/cxcobrar/facturacion/CodigoCuenta.jsp?campo="+campo+"&cliente="+client+"&nit="+nit+"&Mensaje=No se encontro cuenta!!!";
                    }else
                        next="/jsp/cxcobrar/facturacion/CodigoCuenta.jsp?campo="+campo+"&cliente="+client+"&nit="+nit+"&Mensaje=No se encontro cuenta!!!";

                }
                else if(forma.equals("2")){

                    for(int h=0;h<cuenta.length();h++){
                        if("-".equals(""+cuenta.charAt(h))){
                            cuenta= cuenta.substring(0,h);

                        }
                    }
                    String c[] ={"",""};
                    c =model.facturaService.buscarplancuentas(cuenta);
                    if(!c[0].equals("")){
                        next="/jsp/cxcobrar/facturacion/CodigoCuenta.jsp?campo="+campo+"&cliente="+client+"&nit="+nit+"&close=ok"+"&cuenta="+cuenta;
                    }else
                        next="/jsp/cxcobrar/facturacion/CodigoCuenta.jsp?campo="+campo+"&cliente="+client+"&nit="+nit+"&Mensaje=No se encontro cuenta!!!";

                }
            }

            if(codigocuenta.equals("ok")){
                String campo = (request.getParameter("campo")!=null)?request.getParameter("campo"):"";
                String client = (request.getParameter("cliente")!=null)?request.getParameter("cliente"):"";
                String nit = (request.getParameter("nit")!=null)?request.getParameter("nit"):"";
                next="/jsp/cxcobrar/facturacion/CodigoCuenta.jsp?campo="+campo+"&cliente="+client+"&nit="+nit;
                model.facturaService.buscartelementos();
                model.facturaService.buscartunidades();

            }
            if(cargarDocumento.equals("ok")){
                next="/jsp/cxcobrar/facturacion/PuenteFiltro.jsp";
                String tipo = (request.getParameter("tipo")!=null)?request.getParameter("tipo"):"";

                model.facturaService.setVDocumentos(model.facturaService.buscarDocumentos(tipo));

            }
            if(busqueda.equals("ok")){

                if(cliente.equals(""))
                    next="/jsp/cxcobrar/facturacion/Iniciofacturacion.jsp?Mensaje=Favor digitar codigo Cliente!";
                else{
                    //next="/jsp/cxcobrar/facturacion/FacturacionAceptada.jsp?sinitems=ok";
                    next="/jsp/cxcobrar/facturacion/Filtros.jsp";
                    /****************************
                     * seteo todo a 0
                     ****************************/
                    String cod ="";
                    /*20100525
                    for(int i=0; i<6-cliente.length();i++ ){
                        cod +="0";
                    }*/
                    cliente = cod + cliente;


                    String cumplidas = (request.getParameter("cumplidas")==null?"a.reg_status = 'C'":"a.reg_status != 'A'");
                    model.facturaService.setIncluirCumplidas(cumplidas);
                    Vector a= new Vector();
                    model.facturaService.setRemesasFacturar(a);
                    model.facturaService.setRemesasModificadas(a);
                    model.facturaService.setRemesas(a);
                    model.facturaService.setRemesasEliminadas(a);
                    factura b = new factura();
                    model.facturaService.setFactu(b);

                    /**************************************
                     *buscarMoneda:Busca la moneda del distrito
                     *buscarCliente:Carga datos del cliente en el objeto factura del dao
                     *buscarRemesas:Cargar en el vector remesas todas las remesas cumplidas del cliente
                     ***************************************/


                    model.facturaService.buscarMoneda(distrito);
                    if(model.facturaService.buscarCliente(cliente,usuario.getId_agencia())){

                        model.facturaService.buscarRemesas(cliente,model.facturaService.getIncluirCumplidas(),usuario.getDstrct());
                        //model.facturaService.getFactu().setAgencia_facturacion(usuario.getId_agencia());

                        Vector tipodoc = new Vector(model.tablaGenService.obtenerInfoTablaGen("TDOC","DESPACHO"));

                        model.facturaService.setVtipodoc(tipodoc);
                        try{
                            if(model.facturaService.getFactu().getMoneda().equals("")){
                                model.facturaService.getFactu().setMoneda(model.facturaService.getMonedaLocal());
                            }
                            Tasa tasas = model.tasaService.buscarValorTasa( model.facturaService.getMonedaLocal(),model.facturaService.getFactu().getMoneda(),model.facturaService.getMonedaLocal(), actual);

                            model.facturaService.getFactu().setValor_tasa(new Double(""+tasas.getValor_tasa()));

                        }catch(Exception e){
                            model.facturaService.getFactu().setValor_tasa(new Double("0"));
                            e.printStackTrace();
                        }
                    }
                    else
                        next="/jsp/cxcobrar/facturacion/Iniciofacturacion.jsp?Mensaje=Cliente no encontrado o pertenece a otra agencia";
                }
            }

            if(filtrar.equals("ok")){
                next="/jsp/cxcobrar/facturacion/FacturacionAceptada.jsp?sinitems=ok";
                model.facturaService.getFactu().setAgencia_facturacion(usuario.getId_agencia());
                String check1 =(request.getParameter("checkbox")!=null)?request.getParameter("checkbox"):"no";
                String check2 =(request.getParameter("checkbox2")!=null)?request.getParameter("checkbox2"):"no";
                String check3 =(request.getParameter("checkbox3")!=null)?request.getParameter("checkbox3"):"no";
                String check4 =(request.getParameter("checkbox4")!=null)?request.getParameter("checkbox4"):"no";
                String check5 =(request.getParameter("checkbox5")!=null)?request.getParameter("checkbox5"):"no";
                String chk_stdadd =(request.getParameter("chk_stdadd")!=null)?request.getParameter("chk_stdadd"):"no";

                Vector estandares = new Vector();
                Vector num_stds   = new Vector();
                Vector por_rango  = new Vector();

                String stdadd = "";
                boolean std = false;
                if( chk_stdadd.equals("") ){
                    stdadd = request.getParameter("stdadd")!=null?request.getParameter("stdadd"):"" ;
                    String[] split = stdadd.split(",");
                    num_stds = Util.toVector( split, true );
                    //Si solo se selecciona filtro por estandares especificos, cargar solo estas remesas
                    if( (check1+check2+check3+check4+check5).equals("nonononono") ){
                        Vector v = model.facturaService.getRemesas();
                        if( v.size()>0 && stdadd.length()>0 && estandares.size()==0 ){
                            for( int i=0; i<v.size(); i++ ){
                                factura_detalle fdet = (factura_detalle)v.get(i);
                                if( num_stds.contains(fdet.getStd_job_no()) ){
                                    estandares.add(fdet);
                                }
                            }
                            model.facturaService.setRemesas(estandares);
                        }
                    }
                }

                if(check5.equals("")){
                    String rango1 =(request.getParameter("c_fecha")!=null)?request.getParameter("c_fecha"):"";
                    String rango2 =(request.getParameter("c_fecha2")!=null)?request.getParameter("c_fecha2"):"";
                    Vector nuevo = model.facturaService.getRemesas();

                    for(int i=0;i<nuevo.size();i++){
                        factura_detalle factu_de =(factura_detalle)nuevo.get(i);

                        try{
                            java.text.SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");

                            Date r1 = fmt.parse(rango1);
                            Date r2 = fmt.parse(rango2);
                            Date compa = fmt.parse(factu_de.getFecrem());

                            if(compa.before(r1)||compa.after(r2)){
                                //if(compa.compareTo(r1)==0 || compa.compareTo(r1)==1 || compa.compareTo(r2)== -1 || compa.compareTo(r2)== 0){
                                //if( !num_stds.contains(factu_de.getStd_job_no()) ){
                                nuevo.remove(i);
                                i--;
                                /*}else{
                                    estandares.add(nuevo.get(i));
                                }*/
                            }
                        }catch (Exception r){
                            r.printStackTrace();
                        }

                    }
                    model.facturaService.setRemesas(nuevo);
                }

                if(check2.equals("")){
                    String rango1 =(request.getParameter("rango1")!=null)?request.getParameter("rango1"):"";
                    String rango2 =(request.getParameter("rango2")!=null)?request.getParameter("rango2"):"";
                    Vector nuevo = model.facturaService.getRemesas();

                    if(Long.parseLong(rango1)>Long.parseLong(rango2)){
                        String temp=rango1;
                        rango1=rango2;
                        rango2=temp;

                    }

                    for(int i=0;i<nuevo.size();i++){
                        factura_detalle factu_de =(factura_detalle)nuevo.get(i);
                        if((Long.parseLong(factu_de.getStd_job_no())<Long.parseLong(rango1))||(Long.parseLong(factu_de.getStd_job_no())>Long.parseLong(rango2))){
                            if( !num_stds.contains(factu_de.getStd_job_no()) ){
                                nuevo.remove(i);
                                i--;
                            }else{
                                estandares.add(nuevo.get(i));
                            }
                        }else{
                            por_rango.add( factu_de.getStd_job_no() );
                        }

                    }
                    model.facturaService.setRemesas(nuevo);
                }
                if( chk_stdadd.equals("") ){

                    stdadd = request.getParameter("stdadd")!=null?request.getParameter("stdadd"):"" ;
                    String[] split = stdadd.split(",");
                    num_stds = Util.toVector( split, true );
                    Vector nuevo = model.facturaService.getRemesas();

                    for(int i=0;i<nuevo.size();i++){
                        factura_detalle factu_de =(factura_detalle)nuevo.get(i);
                        if( !num_stds.contains(factu_de.getStd_job_no()) && !por_rango.contains(factu_de.getStd_job_no()) ){
                            nuevo.remove(i);
                            i--;
                        }
                    }
                    model.facturaService.setRemesas(nuevo);
                }
                if(check1.equals("")){
                    String ruta =(request.getParameter("ruta")!=null)?request.getParameter("ruta"):"";
                    Vector nuevo = model.facturaService.getRemesas();
                    for(int i=0;i<nuevo.size();i++){
                        factura_detalle factu_de =(factura_detalle)nuevo.get(i);
                        if(factu_de.getNomorigendestino().equals(ruta)){
                        }
                        else{
                            //if( !num_stds.contains(factu_de.getStd_job_no()) ){
                            nuevo.remove(i);
                            i--;
                            /*}else{
                                estandares.add(nuevo.get(i));
                            }*/
                        }
                    }
                    model.facturaService.setRemesas(nuevo);
                }
                if(check3.equals("")){
                    String producto =(request.getParameter("producto")!=null)?request.getParameter("producto"):"";
                    Vector nuevo = model.facturaService.getRemesas();
                    for(int i=0;i<nuevo.size();i++){
                        factura_detalle factu_de =(factura_detalle)nuevo.get(i);
                        if(factu_de.getTipo_doc()==null){
                            factu_de.setTipo_doc("");
                        }
                        if(factu_de.getTipo_doc().equals(producto)){
                        }
                        else{
                            //if( !num_stds.contains(factu_de.getStd_job_no()) ){
                            nuevo.remove(i);
                            i--;
                            /*}else{
                                estandares.add(nuevo.get(i));
                            }*/
                        }
                    }
                    model.facturaService.setRemesas(nuevo);
                }
                if(check4.equals("")){

                    String documento =(request.getParameter("documento")!=null)?request.getParameter("documento"):"";
                    String docu1 =(request.getParameter("docu1")!=null)?request.getParameter("docu1"):"";


                    Vector v = model.facturaService.getRemesas();
                    //Se carga vector con las remesas de los estandares adicionales deseados

                    /*if( v.size()>0 && stdadd.length()>0 && estandares.size()==0 ){
                        for( int i=0; i<v.size(); i++ ){
                            factura_detalle fdet = (factura_detalle)v.get(i);
                            if( !num_stds.contains(fdet.getStd_job_no()) ){
                                estandares.add(fdet);
                            }
                        }
                    }*/

                    Vector listDoc = model.facturaService.getVDocumentos();
                    for(int i=0;i<listDoc.size();i++){
                        factura_detalle fdeta = (factura_detalle)listDoc.get(i);
                        if(fdeta.getDocumento().equals(docu1)){
                        }
                        else{
                            listDoc.remove(i);
                            i--;
                        }

                    }
                    Vector nuevo = model.facturaService.getRemesas();
                    Vector nuevo1 = new Vector();
                    for(int i=0;i<listDoc.size();i++){
                        factura_detalle fdeta = (factura_detalle)listDoc.get(i);
                        for(int j=0;j<nuevo.size();j++){
                            factura_detalle fdeta1 = (factura_detalle)nuevo.get(j);
                            if(fdeta.getNumero_remesa().equals(fdeta1.getNumero_remesa())){
                                nuevo1.add(fdeta1);
                            }
                        }
                    }
                    model.facturaService.setRemesas(nuevo1);
                    v = model.facturaService.getRemesas();

                    //Se agregan las remesas pertenecientes a los estandares dados
                    //al vector general de remesas (si no estaban contenidas)
                    for( int i=0;i<estandares.size();i++ ){
                        factura_detalle fdet = (factura_detalle)estandares.get(i);
                        String numrem = fdet.getNumero_remesa();

                        boolean esta = false;

                        for( int j=0; j<v.size(); j++ ){
                            factura_detalle f = (factura_detalle)v.get(i);

                            if( f.getNumero_remesa().equals(numrem) ){
                                esta = true;
                                j = v.size();
                            }
                        }
                        if( esta == false ){
                            v.add( estandares.get(i));
                        }
                    }

                    model.facturaService.setRemesas(v);

                }

                //Esta condicion significa que si el cliente es de carbon
               /* if(model.facturaService.getFactu().getZona().equals("FC")){
                     next = "/controller?estado=Factura&accion=Carbon&opcion=MOSTRAR_FACTURA";
                }*/

                //Esto es para setear el primer item de la factura con vacio
                Vector a = new Vector();
                factura_detalle fac = new factura_detalle();
                fac.setNumero_remesa("");
                fac.setFecrem("");
                fac.setDescripcion("");
                fac.setTiposubledger("");
                fac.setAuxliliar("");
                fac.setAux("");
                fac.setCantidad(new Double(0));
                fac.setUnidad("");
                fac.setValor_unitario(0);
                fac.setValor_unitariome(0);
                fac.setCodigo_cuenta_contable("");
                fac.setMoneda( model.facturaService.getFactu().getMoneda());
                fac.setValor_tasa(new Double(1));
                fac.setValor_item(0);
                fac.setValor_itemme(0);

                a.add(fac);
                model.facturaService.setRemesasFacturar(a);

            }
            /**************************************
             * Condicional para cargar remesas digitadas
             ***************************************/
            else if(cargar.equals("ok")){

                /**************************************
                 * Obtiene los datos enviados desde el formulario
                 ***************************************/

                String fpago = (request.getParameter("textfield4")!=null)?request.getParameter("textfield4"):"";
                String zona = (request.getParameter("zona")!=null)?request.getParameter("zona"):"";
                String afacturacion= (request.getParameter("textfield5")!=null)?request.getParameter("textfield5"):"";
                String plazo = (request.getParameter("textfield8")!=null)?request.getParameter("textfield8"):"";
                String vtotal = (request.getParameter("textfield9")!=null)?request.getParameter("textfield9"):"0";
                String ffactura = (request.getParameter("c_fecha")!=null)?request.getParameter("c_fecha"):"";
                String fvencimiento = (request.getParameter("c_fecha2")!=null)?request.getParameter("c_fecha2"):"";
                String descripcion =(request.getParameter("textarea")!=null)?request.getParameter("textarea"):"";
                String observacion =(request.getParameter("textarea2")!=null)?request.getParameter("textarea2"):"";
                String moneda =(request.getParameter("mone")!=null)?request.getParameter("mone"):"";
                String x = (request.getParameter("x")!=null)?request.getParameter("x"):"";
                String tasa = (request.getParameter("textfield")!=null)?request.getParameter("textfield"):"";
                String gasto = (request.getParameter("gasto")!=null)?request.getParameter("gasto"):"";
                cmc  =(request.getParameter("ForHC")!=null)?request.getParameter("ForHC"):"";
                System.out.println("cmc***"+cmc);

                /**************************************
                 * setea datos enviados del formulario
                 ***************************************/

                if(tasa.equals("")){
                    tasa+="0";
                }

                if(vtotal.equals("")){
                    vtotal+="0";
                }
                model.facturaService.getFactu().setForma_pago(fpago);
                model.facturaService.getFactu().setAgencia_facturacion(afacturacion);
                model.facturaService.getFactu().setPlazo(plazo);
                model.facturaService.getFactu().setValor_factura(Double.parseDouble(vtotal.replaceAll(",","")));
                model.facturaService.getFactu().setFecha_factura(ffactura);
                model.facturaService.getFactu().setPlazo(fvencimiento);
                model.facturaService.getFactu().setDescripcion(descripcion);
                model.facturaService.getFactu().setObservacion(observacion);
                model.facturaService.getFactu().setMoneda(moneda);
                model.facturaService.getFactu().setValor_tasa(new Double(tasa.replaceAll(",","")));
                model.facturaService.getFactu().setZona(zona);
                model.facturaService.getFactu().setCmc(cmc);

                String maxfila = request.getParameter("numerofilas");
                String item = request.getParameter("item");

                int t = ( !com.tsp.util.Util.coalesce(item,"").equals(""))? Integer.parseInt(item):1;
                Vector a = model.facturaService.getRemesasFacturar();

                if ( a != null && a.size()>0){
                    int itemFin = Integer.parseInt(maxfila);

                    for(int i=t ;i<=itemFin;i++){
                        factura_detalle factu_deta = (factura_detalle)a.get(i-1);
                        String textfield11= (request.getParameter("textfield11"+(i-1))!=null)?request.getParameter("textfield11"+(i-1)):"";
                        String textfield12= (request.getParameter("textfield12"+(i-1))!=null)?request.getParameter("textfield12"+(i-1)):"";
                        String textfield13= (request.getParameter("textfield13"+(i-1))!=null)?request.getParameter("textfield13"+(i-1)):"";
                        String textfield14= (request.getParameter("textfield14"+(i-1))!=null)?request.getParameter("textfield14"+(i-1)):"";
                        String textfield15= (request.getParameter("textfield15"+(i-1))!=null)?request.getParameter("textfield15"+(i-1)):"";
                        String textfield16= (request.getParameter("textfield16"+(i-1))!=null)?request.getParameter("textfield16"+(i-1)):"";
                        String textfield17= (request.getParameter("textfield17"+(i-1))!=null)?request.getParameter("textfield17"+(i-1)):"";
                        String textfield18= (request.getParameter("textfield18"+(i-1))!=null)?request.getParameter("textfield18"+(i-1)):"";
                        String textfield19= (request.getParameter("textfield19"+(i-1))!=null)?request.getParameter("textfield19"+(i-1)):"";

                        String hiddenField= (request.getParameter("hiddenField"+(i-1))!=null)?request.getParameter("hiddenField"+(i-1)):"0";
                        String hiddenField2= (request.getParameter("hiddenField2"+(i-1))!=null)?request.getParameter("hiddenField2"+(i-1)):"0";
                        String hiddenField3= (request.getParameter("hiddenField3"+(i-1))!=null)?request.getParameter("hiddenField3"+(i-1)):"0";

                        //Nuevos Campos para auxiliar
                        String tipo=(request.getParameter("tipo"+(i-1))!=null)?request.getParameter("tipo"+(i-1)):"";
                        String auxiliar=(request.getParameter("auxiliar"+(i-1))!=null)?request.getParameter("auxiliar"+(i-1)):"";
                        String aux=(request.getParameter("aux"+(i-1))!=null)?request.getParameter("aux"+(i-1)):"";

                        if(textfield11.equals(x)&& textfield12.equals("")){
                            textfield11 = "";
                        }



                        factu_deta.setNumero_remesa(textfield11);
                        factu_deta.setFecrem(textfield12);
                        factu_deta.setDescripcion(textfield13);
                        factu_deta.setTiposubledger(tipo);
                        factu_deta.setAuxliliar(auxiliar);
                        factu_deta.setAux(aux);



                        if(textfield14.equals(""))
                            textfield14+="0";
                        if(textfield15.equals(""))
                            textfield15+="0";
                        if(textfield16.equals(""))
                            textfield16+="0";
                        if(hiddenField.equals(""))
                            hiddenField+="0";
                        if(hiddenField2.equals(""))
                            hiddenField2+="0";
                        if(hiddenField3.equals(""))
                            hiddenField3+="0";

                        factu_deta.setCantidad(new Double(textfield14.replaceAll(",","")));
                        factu_deta.setUnidad(textfield18);
                        factu_deta.setValor_unitario(Double.parseDouble((textfield15.replaceAll(",",""))));
                        factu_deta.setValor_unitariome(Double.parseDouble(hiddenField2));
                        factu_deta.setCodigo_cuenta_contable(textfield17);

                        String[] c={"",""};
                        c=model.facturaService.buscarplancuentas(factu_deta.getCodigo_cuenta_contable());

                        if(c[1].equals("S")){
                            try{
                                modelcontab.subledgerService.buscarCuentasTipoSubledger(factu_deta.getCodigo_cuenta_contable());
                                factu_deta.setTiposub(modelcontab.subledgerService.getCuentastsubledger());
                            }catch(Exception o){
                                o.printStackTrace();
                            }
                        }else{
                            factu_deta.setTiposub(null);
                            factu_deta.setTiposubledger("");
                            factu_deta.setAuxliliar("");
                        }
                        factu_deta.setAux(c[1]);



                        factu_deta.setMoneda( model.facturaService.getFactu().getMoneda());

                        factu_deta.setValor_tasa(new Double(hiddenField));
                        factu_deta.setValor_item(Double.parseDouble((textfield16.replaceAll(",",""))));
                        factu_deta.setValor_itemme(Double.parseDouble(hiddenField3));
                        //}


                    }

                    model.facturaService.setRemesasFacturar(a);
                }




                /**************************************
                 * Busca si la remesa ya esta en la lista de remesas
                 * escojidas por el cliente para facturar
                 ***************************************/

                a = model.facturaService.getRemesasFacturar();
                Vector b = model.facturaService.getRemesas();



                /**************************************
                 * condicional para no recorrer el vector
                 * si la remesa esta escojida
                 ***************************************/
                next="/jsp/cxcobrar/facturacion/FacturacionAceptada.jsp";
                if(gasto.equals("no")){
                    String mon = "";
                    int cont=0;
                    for(int i =0; i< a.size(); i++){
                        factura_detalle factu_de =(factura_detalle)a.get(i);
                        if(factu_de.getNumero_remesa().equals(x)){
                            cont++;
                        }
                    }
                    if(a.size()>0){
                        for(int i =0; i< model.facturaService.getRemesas().size(); i++){
                            factura_detalle fa =(factura_detalle)model.facturaService.getRemesas().get(i);
                            factura_detalle facA =(factura_detalle)a.get(0);
                            if(fa.getNumero_remesa().equals(facA.getNumero_remesa())){
                                mon = fa.getMoneda();
                            }
                        }
                    }


                    if(cont==0){
                        next="/jsp/cxcobrar/facturacion/FacturacionAceptada.jsp?gas= No se encuentra la remesa "+x;
                        for(int i =0; i< model.facturaService.getRemesas().size(); i++){
                            factura_detalle factu_deta =(factura_detalle)((factura_detalle)model.facturaService.getRemesas().get(i)).clonee();

                            if(factu_deta.getNumero_remesa().equals(x)){
                                ((factura_detalle) model.facturaService.getRemesas().get(i)).setIncluida(true);
                                next="/jsp/cxcobrar/facturacion/FacturacionAceptada.jsp?";
                                String msj ="";
                                if(!mon.equals("") && !mon.equals(factu_deta.getMoneda())){
                                    msj ="La moneda de la remesa "+factu_deta.getNumero_remesa() +"  es diferente a las anteriores";
                                    next+="Mensaje="+msj;
                                }

                                // modif para cargar remesas con el tipo
                                String c[] ={"",""};
                                c=model.facturaService.buscarplancuentas(factu_deta.getCodigo_cuenta_contable());
                                if(!c[0].equals("")){
                                    if(c[1].equals("S")){
                                        try{
                                            modelcontab.subledgerService.buscarCuentasTipoSubledger(factu_deta.getCodigo_cuenta_contable());
                                            factu_deta.setTiposub(modelcontab.subledgerService.getCuentastsubledger());
                                            factu_deta.setAux("S");

                                        }catch(Exception e){

                                        }
                                    }else{
                                        factu_deta.setTiposub(null);
                                        factu_deta.setAux("N");
                                        factu_deta.setAuxliliar("");
                                    }
                                }
                                else{
                                    factu_deta.setAux("N");
                                    factu_deta.setAuxliliar("");
                                }

                                /**************************************
                                 * condicional para ver si la remesa que se digito tiene la misma moneda
                                 * local, de lo contrario se hace la conversion
                                 ***************************************/
                                if(moneda.equals(factu_deta.getMoneda())){

                                    factu_deta.setValor_tasa(new Double("1"));
                                    factu_deta.setValor_unitariome(factu_deta.getValor_unitariome());
                                    factu_deta.setValor_itemme( factu_deta.getValor_itemme());
                                }
                                else{
                                    try{

                                        Tasa tasas = model.tasaService.buscarValorTasa( model.facturaService.getMonedaLocal(),factu_deta.getMoneda(),model.facturaService.getFactu().getMoneda(), actual);


                                        double val2 = model.facturaService.getFactu().getValor_tasa().doubleValue();


                                        if(tasas!=null){

                                            double valoruni=0;
                                            if(tasas.getValor_tasa1()==tasas.getValor_tasa2()){
                                                valoruni = factu_deta.getValor_unitario()*tasas.getValor_tasa1();

                                            }
                                            else{
                                                valoruni = factu_deta.getValor_unitario()*val2*tasas.getValor_tasa2();

                                            }

                                            factu_deta.setValor_unitario(valoruni);
                                            factu_deta.setValor_item(this.getValor(valoruni*factu_deta.getCantidad().doubleValue(),model.facturaService.getFactu().getMoneda()));
                                            factu_deta.setValor_itemme(factu_deta.getValor_item());
                                            factu_deta.setValor_unitariome(factu_deta.getValor_unitario());
                                            factu_deta.setMoneda(model.facturaService.getFactu().getMoneda());
                                            factu_deta.setValor_tasa(new Double(""+tasas.getValor_tasa2()));

                                        }


                                    }catch(Exception e){
                                        e.printStackTrace();
                                    }



                                }
                                if(msj.equals(""))
                                    a.add(factu_deta);


                                Vector cr= model.facturaService.costoReembolsable(factu_deta.getNumero_remesa());

                                for(int f=0;f<cr.size();f++){
                                    factura_detalle factu_d = (factura_detalle)cr.get(f);

                                    String cc[] ={"",""};
                                    cc=model.facturaService.buscarplancuentas(factu_d.getCodigo_cuenta_contable());
                                    if(!cc[0].equals("")){
                                        if(cc[1].equals("S")){
                                            try{
                                                modelcontab.subledgerService.buscarCuentasTipoSubledger(factu_d.getCodigo_cuenta_contable());
                                                factu_d.setTiposub(modelcontab.subledgerService.getCuentastsubledger());
                                                factu_d.setAux("S");

                                            }catch(Exception e){

                                            }
                                        }else{
                                            factu_d.setTiposub(null);
                                            factu_d.setAux("N");
                                            factu_d.setAuxliliar("");
                                        }
                                    }
                                    else{
                                        factu_deta.setAux("N");
                                        factu_deta.setAuxliliar("");
                                    }

                                    /////////////////
                                    if(model.facturaService.getFactu().getMoneda().equals(factu_d.getMoneda())){
                                        //System.out.println("iguales");
                                        factu_d.setValor_tasa(new Double("1"));
                                        factu_d.setValor_unitariome(factu_d.getValor_unitariome());
                                        factu_d.setValor_itemme( factu_d.getValor_itemme());

                                    }
                                    else{

                                        //System.out.println("diferentes");
                                        try{
                                            Tasa tasas = model.tasaService.buscarValorTasa( model.facturaService.getMonedaLocal(),factu_d.getMoneda(),model.facturaService.getFactu().getMoneda(), actual);


                                            //System.out.println("val:"+factu_deta.getValor_unitario());
                                            //System.out.println("tasa:"+model.facturaService.getFactu().getValor_tasa().doubleValue());
                                            double val2 = model.facturaService.getFactu().getValor_tasa().doubleValue();

                                            if(tasas!=null){
                                                double valoruni=0;
                                                if(tasas.getValor_tasa1()==tasas.getValor_tasa2()){
                                                    valoruni = factu_d.getValor_unitario()*tasas.getValor_tasa1();
                                                    //System.out.println("if:"+valoruni );
                                                }
                                                else{
                                                    valoruni = factu_d.getValor_unitario()*val2*tasas.getValor_tasa2();
                                                    //System.out.println("else:"+valoruni );
                                                }
                                                //System.out.println("tasas!= "+valoruni);
                                                factu_d.setValor_unitario(valoruni);
                                                factu_d.setValor_item(this.getValor(valoruni*factu_d.getCantidad().doubleValue(),model.facturaService.getFactu().getMoneda()));
                                                factu_d.setValor_itemme(factu_d.getValor_item());
                                                factu_d.setValor_unitariome(factu_d.getValor_unitario());
                                                factu_d.setMoneda(model.facturaService.getFactu().getMoneda());
                                                factu_d.setValor_tasa(new Double(""+tasas.getValor_tasa2()));

                                            }

                                        }catch(Exception e){
                                            e.printStackTrace();
                                        }

                                    }
                                    /////////////////
                                    if(msj.equals(""))
                                        a.add(factu_d);
                                }
                                /*******************/
                            }
                        }
                        model.facturaService.setRemesasFacturar(a);
                    }
                    else{
                        next="/jsp/cxcobrar/facturacion/FacturacionAceptada.jsp?gas=La remesa "+x+" ya fue ingresada!";
                    }

                }
                model.facturaService.calcularTotal();
                if(gasto.equals("ok")){



                    if(model.facturaService.existeRemesa(model.facturaService.getFactu().getCodcli(),x)){
                        next="/jsp/cxcobrar/facturacion/FacturacionAceptada.jsp?";
                        factura_detalle factu_deta = new factura_detalle();
                        factu_deta.setNumero_remesa("");
                        factu_deta.setFecrem("");
                        factu_deta.setDescripcion("GASTO DE LA REMESA "+x);
                        factu_deta.setCantidad(new Double("0"));
                        factu_deta.setUnidad("");
                        factu_deta.setValor_unitario(Double.parseDouble("0"));
                        factu_deta.setValor_unitariome(Double.parseDouble("0"));
                        factu_deta.setCodigo_cuenta_contable("");
                        factu_deta.setAux("N");
                        factu_deta.setAuxliliar("");
                        factu_deta.setTiposubledger("");
                        factu_deta.setMoneda(moneda);

                        factu_deta.setValor_tasa(new Double("0"));
                        factu_deta.setValor_item(Double.parseDouble("0"));
                        factu_deta.setValor_itemme(Double.parseDouble("0"));
                        a.add(factu_deta);
                        model.facturaService.setRemesasFacturar(a);
                    }
                    else{
                        next="/jsp/cxcobrar/facturacion/FacturacionAceptada.jsp?gas=No se encuentra la remesa "+x;
                    }
                }


            }

            else if(cargarcodigo.equals("ok")){
                String c[] ={"",""};
                String x = (request.getParameter("x")!=null)?request.getParameter("x"):"";
                String campo = (request.getParameter("campo")!=null)?request.getParameter("campo"):"";//Este es el id

                String causa = (request.getParameter("causa")!=null)?request.getParameter("causa"):"";

                /**************************************
                 * Obtiene los datos enviados desde el formulario
                 ***************************************/

                String fpago = (request.getParameter("textfield4")!=null)?request.getParameter("textfield4"):"";
                String zona = (request.getParameter("zona")!=null)?request.getParameter("zona"):"";
                String afacturacion= (request.getParameter("textfield5")!=null)?request.getParameter("textfield5"):"";
                String plazo = (request.getParameter("textfield8")!=null)?request.getParameter("textfield8"):"0";
                String vtotal = (request.getParameter("textfield9")!=null)?request.getParameter("textfield9"):"0";
                String ffactura = (request.getParameter("c_fecha")!=null)?request.getParameter("c_fecha"):"";
                String fvencimiento = (request.getParameter("c_fecha2")!=null)?request.getParameter("c_fecha2"):"";

                String descripcion =(request.getParameter("textarea")!=null)?request.getParameter("textarea"):"";

                String observacion =(request.getParameter("textarea2")!=null)?request.getParameter("textarea2"):"";
                String moneda =(request.getParameter("mone")!=null)?request.getParameter("mone"):"";

                String guardar =(request.getParameter("guardar")!=null)?request.getParameter("guardar"):"";
                String actualizar =(request.getParameter("actualizar")!=null)?request.getParameter("actualizar"):"";
                String tasa = (request.getParameter("textfield")!=null)?request.getParameter("textfield"):"";
                cmc  =(request.getParameter("ForHC")!=null)?request.getParameter("ForHC"):"";
                session.setAttribute("cmc", cmc);

                String cs="";

                c= model.facturaService.buscarplancuentas(x);
                if(!c[0].equals("")){
                    cs="ok";
                    next="/jsp/cxcobrar/facturacion/FacturacionAceptada.jsp?cargarCodigo=ok&x="+x+"&campo="+campo;
                    if(c[1].equals("S")){
                        try{
                            modelcontab.subledgerService.buscarCuentasTipoSubledger(x);

                        }catch(Exception o){
                            o.printStackTrace();
                        }
                    }else cs="no";
                }else{
                    cs="no";
                    next="/jsp/cxcobrar/facturacion/FacturacionAceptada.jsp?cargarCodigo=no&x="+x+"&campo="+campo;
                }


                if(causa.equals("pormoneda")){
                    try{
                        //System.out.println("Moneda1:"+model.facturaService.getFactu().getMoneda()+" MONEDA2:"+moneda);
                        Tasa tasas = model.tasaService.buscarValorTasa( model.facturaService.getMonedaLocal(),moneda,model.facturaService.getMonedaLocal(), actual);

                        tasa=""+tasas.getValor_tasa();
                        //System.out.println("por moneda!!!:"+tasa);
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }

                model.facturaService.getFactu().setAgencia_facturacion(usuario.getId_agencia());

                /**************************************
                 * setea datos enviados del formulario
                 ***************************************/

                if(tasa.equals("")){
                    tasa+="0";
                }

                model.facturaService.getFactu().setForma_pago(fpago);
                model.facturaService.getFactu().setAgencia_facturacion(afacturacion);
                model.facturaService.getFactu().setPlazo(plazo);
                model.facturaService.getFactu().setValor_factura(Double.parseDouble(vtotal.replaceAll(",","")));
                model.facturaService.getFactu().setFecha_factura(ffactura);
                model.facturaService.getFactu().setPlazo(fvencimiento);
                model.facturaService.getFactu().setDescripcion(descripcion);
                model.facturaService.getFactu().setObservacion(observacion);
                model.facturaService.getFactu().setZona(zona);


                model.facturaService.getFactu().setMoneda(moneda);
                //System.out.println(" moneda factu---------->"+ model.facturaService.getFactu().getMoneda());

                String maxfila= (request.getParameter("numerofilas")!=null)?request.getParameter("numerofilas"):"";
                String facturacion= (request.getParameter("facturacion")!=null)?request.getParameter("facturacion"):"";
                String facturacionmodif= (request.getParameter("facturacionmodif")!=null)?request.getParameter("facturacionmodif"):"";

                //System.out.println("maxfila:"+maxfila);


                /**************************************
                 * for para recorre todos los items del formulario
                 * y obtenerlos
                 ***************************************/
                String item = request.getParameter("item");

                int t = ( !com.tsp.util.Util.coalesce(item,"").equals(""))? Integer.parseInt(item):1;
                Vector a = model.facturaService.getRemesasFacturar();

                if ( a != null && a.size()>0){
                    int itemFin = Integer.parseInt(maxfila);

                    for(int i=t ;i<=itemFin;i++){

                        factura_detalle factu_deta = (factura_detalle)a.get(i-1);




                        String textfield11= (request.getParameter("textfield11"+(i-1))!=null)?request.getParameter("textfield11"+(i-1)):"";
                        String textfield12= (request.getParameter("textfield12"+(i-1))!=null)?request.getParameter("textfield12"+(i-1)):"";
                        String textfield13= (request.getParameter("textfield13"+(i-1))!=null)?request.getParameter("textfield13"+(i-1)):"";
                        String textfield14= (request.getParameter("textfield14"+(i-1))!=null)?request.getParameter("textfield14"+(i-1)):"0";
                        String textfield15= (request.getParameter("textfield15"+(i-1))!=null)?request.getParameter("textfield15"+(i-1)):"0";
                        String textfield16= (request.getParameter("textfield16"+(i-1))!=null)?request.getParameter("textfield16"+(i-1)):"0";
                        String textfield17= (request.getParameter("textfield17"+(i-1))!=null)?request.getParameter("textfield17"+(i-1)):"";
                        String textfield18= (request.getParameter("textfield18"+(i-1))!=null)?request.getParameter("textfield18"+(i-1)):"";
                        String textfield19= (request.getParameter("textfield19"+(i-1))!=null)?request.getParameter("textfield19"+(i-1)):"";

                        String hiddenField= (request.getParameter("hiddenField"+(i-1))!=null)?request.getParameter("hiddenField"+(i-1)):"0";
                        String hiddenField2= (request.getParameter("hiddenField2"+(i-1))!=null)?request.getParameter("hiddenField2"+(i-1)):"0";
                        String hiddenField3= (request.getParameter("hiddenField3"+(i-1))!=null)?request.getParameter("hiddenField3"+(i-1)):"0";

                        //Nuevos Campos para auxiliar
                        String tipo=(request.getParameter("tipo"+(i-1))!=null)?request.getParameter("tipo"+(i-1)):"";
                        String auxiliar=(request.getParameter("auxiliar"+(i-1))!=null)?request.getParameter("auxiliar"+(i-1)):"";
                        String aux=(request.getParameter("aux"+(i-1))!=null)?request.getParameter("aux"+(i-1)):"";

                        textfield14= (textfield14.equals(""))?"0":textfield14.replaceAll(",","");;
                        textfield15= (textfield15.equals(""))?"0":textfield15.replaceAll(",","");;
                        textfield16= (textfield16.equals(""))?"0":textfield16.replaceAll(",","");;

                       /*
                        if(x.equals(textfield17)){
                            if(cs.equals("no")){
                                textfield17="";
                            }

                        }*/
                        /**************************************
                         * condicional para saber si los campos q se mandaron del formulario
                         * estan vacios, de caso contrario es por que hay un item !.
                         ***************************************/
                        if(!(textfield11.equals("")&&textfield12.equals("")&&textfield13.equals("")&& (Double.parseDouble(textfield14) == 0) &&   (Double.parseDouble(textfield15) == 0) &&  (Double.parseDouble(textfield16) == 0) && textfield17.equals("")&&textfield18.equals(""))){


                            factu_deta.setNumero_remesa(textfield11);
                            factu_deta.setFecrem(textfield12);
                            factu_deta.setDescripcion(textfield13);
                            factu_deta.setTiposubledger(tipo);
                            factu_deta.setAuxliliar(auxiliar);
                            factu_deta.setAux(aux);

                            if(textfield14.equals(""))
                                textfield14+="0";
                            if(textfield15.equals(""))
                                textfield15+="0";
                            if(textfield16.equals(""))
                                textfield16+="0";
                            if(hiddenField.equals(""))
                                hiddenField+="0";
                            if(hiddenField2.equals(""))
                                hiddenField2+="0";
                            if(hiddenField3.equals(""))
                                hiddenField3+="0";

                            factu_deta.setCantidad(new Double(textfield14.replaceAll(",","")));
                            factu_deta.setUnidad(textfield18);
                            factu_deta.setValor_unitario(Double.parseDouble((textfield15.replaceAll(",",""))));
                            factu_deta.setValor_unitariome(Double.parseDouble(hiddenField2));
                            factu_deta.setCodigo_cuenta_contable(textfield17);

                            if(c[1]==null)
                                factu_deta.setAux("N");

                            if(x.equals(textfield17)){
                                if(cs.equals("no")){
                                    textfield17="";
                                    factu_deta.setTiposub(null);
                                    factu_deta.setTiposubledger("");
                                    factu_deta.setAuxliliar("");
                                }else{
                                    factu_deta.setTiposub(modelcontab.subledgerService.getCuentastsubledger());
                                    factu_deta.setAux(c[1]);
                                }
                            }


                            factu_deta.setMoneda(moneda);

                            factu_deta.setValor_tasa(new Double(hiddenField));
                            factu_deta.setValor_item(Double.parseDouble((textfield16.replaceAll(",",""))));
                            factu_deta.setValor_itemme(Double.parseDouble(hiddenField3));

                            if(moneda.equals(textfield19)){
                                //System.out.println("la moneda es igual la remesa!!!");

                                double val = new Double(textfield15.replaceAll(",","")).doubleValue();
                                if(model.facturaService.getFactu().getValor_tasa().doubleValue()==0.0){
                                    model.facturaService.getFactu().setValor_tasa(new Double("1"));
                                }
                                val/=model.facturaService.getFactu().getValor_tasa().doubleValue();
                                double val2 = new Double(tasa.replaceAll(",","")).doubleValue();
                                val*=val2;
                                factu_deta.setValor_unitario(val);
                                factu_deta.setValor_item(this.getValor(val*factu_deta.getCantidad().doubleValue(),moneda));
                                //System.out.println("factu_deta.getValor_unitario():"+factu_deta.getValor_unitario());
                                //System.out.println("factu_deta.getValor_item():"+factu_deta.getValor_item());
                            }

                            else{

                                //System.out.println("la moneda es dif!!!");
                                //System.out.println("moneda remesa:"+textfield19);

                                try{
                                    Tasa tasas = model.tasaService.buscarValorTasa( model.facturaService.getMonedaLocal(),textfield19,moneda, actual);

                                    double val = new Double(textfield15.replaceAll(",","")).doubleValue();
                                    //System.out.println("val:"+val);
                                    //System.out.println("tasa:"+model.facturaService.getFactu().getValor_tasa().doubleValue());
                                    double val2 = new Double(tasa.replaceAll(",","")).doubleValue();

                                    if(causa.equals("pormoneda")){
                                        if(tasas==null){
                                            //System.out.println("es nulo!!!");
                                            tasa="0";
                                        }
                                        else{

                                            val2=tasas.getValor_tasa1();
                                        }//System.out.println("por monesa val2:"+val2);

                                    }
                                    else{
                                        //System.out.println("val:"+val);
                                        if(model.facturaService.getFactu().getValor_tasa().doubleValue()==0.0){
                                            model.facturaService.getFactu().setValor_tasa(new Double("1"));
                                        }
                                        val/=model.facturaService.getFactu().getValor_tasa().doubleValue();


                                        //System.out.println("val2:"+val2);
                                        val*=val2;
                                        //System.out.println("val:"+val);
                                    }

                                    factu_deta.setValor_unitario(val);
                                    //System.out.println("factu_deta.getValor_unitario:"+factu_deta.getValor_unitario());

                                    if(tasas!=null){

                                        double valoruni=0;
                                        if(tasas.getValor_tasa1()==tasas.getValor_tasa2()){

                                            valoruni = factu_deta.getValor_unitario()*tasas.getValor_tasa1();
                                            //System.out.println("if:"+valoruni );

                                        }
                                        else{
                                            valoruni = factu_deta.getValor_unitario()*val2*tasas.getValor_tasa2();
                                            //System.out.println( );
                                            //System.out.println(factu_deta.getValor_unitario()+" * "+val2+" * "+tasas.getValor_tasa2()+" = "+valoruni);

                                        }
                                        //System.out.println("tasas!= "+valoruni);
                                        factu_deta.setValor_unitario(valoruni);
                                        factu_deta.setValor_item(this.getValor(valoruni*factu_deta.getCantidad().doubleValue(),model.facturaService.getFactu().getMoneda()));
                                        factu_deta.setValor_itemme(factu_deta.getValor_item());
                                        factu_deta.setValor_unitariome(factu_deta.getValor_unitario());

                                        factu_deta.setMoneda(model.facturaService.getFactu().getMoneda());
                                        factu_deta.setValor_tasa(new Double(""+tasas.getValor_tasa2()));


                                    }


                                }catch(Exception e){
                                    e.printStackTrace();
                                }
                            }

                            a.setElementAt(factu_deta,i-1);


                        }
                    }
                }

                model.facturaService.setRemesasFacturar(a);
                model.facturaService.calcularTotal();

            }


            if(recargar.equals("ok")){
                //System.out.println("recargar");
                next="/jsp/cxcobrar/facturacion/FacturacionAceptada.jsp";
            }
            /**************************************
             * condicional para cargar remesas que estan
             * actualmente escojidas y digitadas
             ***************************************/
            else if(Agregar.equals("ok")){//Agregar
                String causa = (request.getParameter("causa")!=null)?request.getParameter("causa"):"";

                System.out.println("agregar");
                /**************************************
                 * Obtiene los datos enviados desde el formulario
                 ***************************************/

                String fpago = (request.getParameter("textfield4")!=null)?request.getParameter("textfield4"):"";
                String zona = (request.getParameter("zona")!=null)?request.getParameter("zona"):"";
                String afacturacion= (request.getParameter("textfield5")!=null)?request.getParameter("textfield5"):"";
                String plazo = (request.getParameter("textfield8")!=null)?request.getParameter("textfield8"):"0";
                String vtotal = (request.getParameter("textfield9")!=null)?request.getParameter("textfield9"):"0";
                String ffactura = (request.getParameter("c_fecha")!=null)?request.getParameter("c_fecha"):"";
                String fvencimiento = (request.getParameter("c_fecha2")!=null)?request.getParameter("c_fecha2"):"";

                String descripcion =(request.getParameter("textarea")!=null)?request.getParameter("textarea"):"";

                String observacion =(request.getParameter("textarea2")!=null)?request.getParameter("textarea2"):"";
                String moneda =(request.getParameter("mone")!=null)?request.getParameter("mone"):"";

                String guardar =(request.getParameter("guardar")!=null)?request.getParameter("guardar"):"";
                String actualizar =(request.getParameter("actualizar")!=null)?request.getParameter("actualizar"):"";
                String tasa = (request.getParameter("textfield")!=null)?request.getParameter("textfield").replaceAll(",",""):"1";
                cmc  =(request.getParameter("ForHC")!=null)?request.getParameter("ForHC"):"";


                double valtasa = Double.parseDouble(tasa);
                String tasa2=tasa;
                String mensaje="";
                if(causa.equals("pormoneda")){
                    try{
                        //System.out.println("Moneda1:"+model.facturaService.getFactu().getMoneda()+" MONEDA2:"+moneda);
                        Tasa tasas = model.tasaService.buscarValorTasa( model.facturaService.getMonedaLocal(),model.facturaService.getFactu().getMoneda(),moneda, actual);
                        System.out.println("moneda local:"+model.facturaService.getMonedaLocal());
                        System.out.println("moneda factura:"+model.facturaService.getFactu().getMoneda());
                        System.out.println("moneda a cambiar:"+moneda);

                        System.out.println("tasas:"+tasas);
                        if(tasas!=null){
                            System.out.println("tasas:"+tasas.getValor_tasa());
                            if(tasas.getValor_tasa()==0){
                                System.out.println("si entro!");
                                tasas=null;
                                mensaje="No se pudo realizar conversión a "+moneda+", No se encontró valor de la tasa!";
                            }else
                                tasa=""+tasas.getValor_tasa();
                        }else
                            mensaje="No se pudo realizar conversión a "+moneda+", No se encontró valor de la tasa!";
                        //System.out.println("por moneda!!!:"+tasa);
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }

                model.facturaService.getFactu().setAgencia_facturacion(usuario.getId_agencia());

                /**************************************
                 * setea datos enviados del formulario
                 ***************************************/

                if(tasa.equals("")){
                    tasa+="0";
                }

                model.facturaService.getFactu().setForma_pago(fpago);
                model.facturaService.getFactu().setAgencia_facturacion(afacturacion);
                model.facturaService.getFactu().setPlazo(plazo);
                model.facturaService.getFactu().setValor_factura(Double.parseDouble(vtotal.replaceAll(",","")));
                model.facturaService.getFactu().setFecha_factura(ffactura);
                model.facturaService.getFactu().setPlazo(fvencimiento);
                model.facturaService.getFactu().setDescripcion(descripcion);
                model.facturaService.getFactu().setObservacion(observacion);
                model.facturaService.getFactu().setZona(zona);
                double total_fac = model.facturaService.getFactu().getValor_factura();
                if(mensaje.equals(""))
                    model.facturaService.getFactu().setMoneda(moneda);
                System.out.println(" moneda factu---------->"+ model.facturaService.getFactu().getMoneda());

                String maxfila= (request.getParameter("numerofilas")!=null)?request.getParameter("numerofilas"):"";
                String facturacion= (request.getParameter("facturacion")!=null)?request.getParameter("facturacion"):"";
                String facturacionmodif= (request.getParameter("facturacionmodif")!=null)?request.getParameter("facturacionmodif"):"";

                //System.out.println("maxfila:"+maxfila);
                // Vector a = new Vector();

                /**************************************
                 * for para recorre todos los items del formulario
                 * y obtenerlos
                 ***************************************/
                String item = request.getParameter("item");

                int x = ( !com.tsp.util.Util.coalesce(item,"").equals(""))? Integer.parseInt(item):1;
                Vector a = model.facturaService.getRemesasFacturar();

                if ( a != null && a.size()>0){
                    int itemFin = Integer.parseInt(maxfila);

                    for(int i=x ;i<=itemFin;i++){

                        factura_detalle factu_deta = (factura_detalle)a.get(i-1);


                        String textfield11= (request.getParameter("textfield11"+(i-1))!=null)?request.getParameter("textfield11"+(i-1)):"";
                        String textfield12= (request.getParameter("textfield12"+(i-1))!=null)?request.getParameter("textfield12"+(i-1)):"";
                        String textfield13= (request.getParameter("textfield13"+(i-1))!=null)?request.getParameter("textfield13"+(i-1)):"";
                        String textfield14= (request.getParameter("textfield14"+(i-1))!=null)?request.getParameter("textfield14"+(i-1)):"0";
                        String textfield15= (request.getParameter("textfield15"+(i-1))!=null)?request.getParameter("textfield15"+(i-1)):"0";
                        String textfield16= (request.getParameter("textfield16"+(i-1))!=null)?request.getParameter("textfield16"+(i-1)):"0";
                        String textfield17= (request.getParameter("textfield17"+(i-1))!=null)?request.getParameter("textfield17"+(i-1)):"";
                        String textfield18= (request.getParameter("textfield18"+(i-1))!=null)?request.getParameter("textfield18"+(i-1)):"";
                        String textfield19= (request.getParameter("textfield19"+(i-1))!=null)?request.getParameter("textfield19"+(i-1)):"";

                        String hiddenField= (request.getParameter("hiddenField"+(i-1))!=null)?request.getParameter("hiddenField"+(i-1)).replaceAll(",",""):"0";
                        String hiddenField2= (request.getParameter("hiddenField2"+(i-1))!=null)?request.getParameter("hiddenField2"+(i-1)):"0";
                        String hiddenField3= (request.getParameter("hiddenField3"+(i-1))!=null)?request.getParameter("hiddenField3"+(i-1)):"0";

                        //Nuevos Campos para auxiliar
                        String tipo=(request.getParameter("tipo"+(i-1))!=null)?request.getParameter("tipo"+(i-1)):"";
                        String auxiliar=(request.getParameter("auxiliar"+(i-1))!=null)?request.getParameter("auxiliar"+(i-1)):"";
                        String aux=(request.getParameter("aux"+(i-1))!=null)?request.getParameter("aux"+(i-1)):"";

                        System.out.println("MONEDAD ##1--->"+  textfield19);
                        /**************************************
                         * condicional para saber si los campos q se mandaron del formulario
                         * estan vacios, de caso contrario es por que hay un item !.
                         ***************************************/
                        hiddenField = hiddenField.equals("null")?"0":hiddenField;
                        textfield14= (textfield14.equals(""))?"0":textfield14.replaceAll(",","");
                        textfield15= (textfield15.equals(""))?"0":textfield15.replaceAll(",","");
                        textfield16= (textfield16.equals(""))?"0":textfield16.replaceAll(",","");



                        if(!(textfield11.equals("")&&textfield12.equals("")&&textfield13.equals("")&& (Double.parseDouble(textfield14) == 0) &&   (Double.parseDouble(textfield15) == 0) &&  (Double.parseDouble(textfield16) == 0) && textfield17.equals("")&&textfield18.equals(""))){

                            /**************************************
                             * A continuacion se setea el item en el objeto con los
                             * campos obtenidos del formulario y luego se
                             * almacena en el vector
                             ***************************************/

                            //System.out.println("hay!!! item!!!");
                            System.out.println("textfield11  "+textfield11);
                            System.out.println("tipo  "+tipo);
                            System.out.println("auxiliar  "+auxiliar);
                            System.out.println("aux   "+aux);
                            factu_deta.setNumero_remesa(textfield11);
                            factu_deta.setFecrem(textfield12);
                            factu_deta.setDescripcion(textfield13);
                            factu_deta.setTiposubledger(tipo);
                            factu_deta.setAuxliliar(auxiliar);
                            factu_deta.setAux(aux);



                            if(textfield14.equals(""))
                                textfield14+="0";
                            if(textfield15.equals(""))
                                textfield15+="0";
                            if(textfield16.equals(""))
                                textfield16+="0";
                            if(hiddenField.equals(""))
                                hiddenField+="0";
                            if(hiddenField2.equals(""))
                                hiddenField2+="0";
                            if(hiddenField3.equals(""))
                                hiddenField3+="0";

                            factu_deta.setCantidad(new Double(textfield14.replaceAll(",","")));
                            factu_deta.setUnidad(textfield18);
                            factu_deta.setValor_unitario(Double.parseDouble((textfield15.replaceAll(",",""))));
                            factu_deta.setValor_unitariome(Double.parseDouble(hiddenField2));
                            factu_deta.setCodigo_cuenta_contable(textfield17);

                            String[] c={"",""};
                            c=model.facturaService.buscarplancuentas(factu_deta.getCodigo_cuenta_contable());

                            if(c[1].equals("S")){
                                try{
                                    modelcontab.subledgerService.buscarCuentasTipoSubledger(factu_deta.getCodigo_cuenta_contable());
                                    factu_deta.setTiposub(modelcontab.subledgerService.getCuentastsubledger());
                                }catch(Exception o){
                                    o.printStackTrace();
                                }
                            }else{
                                factu_deta.setTiposub(null);
                                factu_deta.setTiposubledger("");
                                factu_deta.setAuxliliar("");
                            }
                            factu_deta.setAux(c[1]);



                            if(mensaje.equals(""))
                                factu_deta.setMoneda(moneda);
                            else
                                factu_deta.setMoneda( model.facturaService.getFactu().getMoneda());

                            System.out.println("hiddenFieldhiddenFieldhiddenField--->" +hiddenField);

                            factu_deta.setValor_tasa(new Double(""+hiddenField));
                            total_fac -= factu_deta.getValor_item();
                            factu_deta.setValor_item(Double.parseDouble((textfield16.replaceAll(",",""))));
                            total_fac += factu_deta.getValor_item();
                            factu_deta.setValor_itemme(Double.parseDouble(hiddenField3));


                            /////////////////////

                            factura_detalle fac = new factura_detalle();
                            if(causa.equals("portasa")){
                                Vector remesas = model.facturaService.getRemesas();
                                if(remesas != null && remesas.size() > 0){
                                    for(int h=0; h< remesas.size();h++){
                                        factura_detalle fd= (factura_detalle) ((factura_detalle)remesas.get(h)).clonee();
                                        if(fd.getNumero_remesa().equals(textfield11)){
                                            fd.setAuxliliar(factu_deta.getAuxliliar());
                                            fd.setAux(factu_deta.getAux());
                                            fd.setTiposub(factu_deta.getTiposub());
                                            fd.setTiposubledger(factu_deta.getTiposubledger());
                                            factu_deta = fd;
                                            break;
                                        }

                                    }
                                }


                            }


                            ///////////////////



                            System.out.println("REMESAAAAAAAA--->"+  textfield11);
                            System.out.println("MONEDAD ##2--->"+  factu_deta.getMoneda());


                            if(model.facturaService.getFactu().getMoneda().equals(factu_deta.getMoneda())){
                                System.out.println("la moneda es igual la remesa!!!");

                            /*double val = new Double(textfield15.replaceAll(",","")).doubleValue();
                            if(model.facturaService.getFactu().getValor_tasa().doubleValue()==0.0){
                                model.facturaService.getFactu().setValor_tasa(new Double("1"));
                            }
                            val/=model.facturaService.getFactu().getValor_tasa().doubleValue();
                            double val2 = new Double(tasa.replaceAll(",","")).doubleValue();
                            val*=val2;
                            factu_deta.setValor_unitario(val);
                            factu_deta.setValor_item(val*factu_deta.getCantidad().doubleValue());*/
                                //System.out.println("factu_deta.getValor_unitario():"+factu_deta.getValor_unitario());
                                //System.out.println("factu_deta.getValor_item():"+factu_deta.getValor_item());

                            }

                            else{

                                System.out.println("la moneda es dif!!!");
                                System.out.println("moneda remesa:"+textfield19);

                                try{
                                    if(causa.equals("portasa")){


                                        double valuni = factu_deta.getValor_unitario();
                                        double raro = 0;

                                        raro/=model.facturaService.getFactu().getValor_tasa().doubleValue();
                                        System.out.println("Raro "+raro);

                                        factu_deta.setValor_unitario(valuni*valtasa);
                                        factu_deta.setValor_unitariome(valuni);

                                        //System.out.println("QWQWQWQWQWQWQWW********* valuni: "+ valuni  + "*  factu_deta.getCantidad().doubleValue():   "+factu_deta.getCantidad().doubleValue()+"    *    "+valtasa);
                                        factu_deta.setValor_item(this.getValor(valuni*factu_deta.getCantidad().doubleValue()*valtasa ,model.facturaService.getFactu().getMoneda() ));
                                        factu_deta.setValor_itemme(factu_deta.getValor_item());




                                        factu_deta.setMoneda(model.facturaService.getFactu().getMoneda());
                                        factu_deta.setValor_tasa(new Double(valtasa));

                                        System.out.println("Tasa:"+valtasa);
                                        System.out.println("Valor Uni "+valuni*valtasa);
                                        System.out.println("Sub Total "+this.getValor(valuni*factu_deta.getCantidad().doubleValue()*valtasa ,model.facturaService.getFactu().getMoneda() ));


                                    }else{


                                        Tasa tasas = model.tasaService.buscarValorTasa( model.facturaService.getMonedaLocal(),textfield19,model.facturaService.getFactu().getMoneda(), actual);

                                        double val = new Double(textfield15.replaceAll(",","")).doubleValue();
                                        //System.out.println("val:"+val);
                                        //System.out.println("tasa:"+model.facturaService.getFactu().getValor_tasa().doubleValue());
                                        double val2 = new Double(tasa.replaceAll(",","")).doubleValue();

                                        if(causa.equals("pormoneda")){
                                            if(tasas==null){
                                                //System.out.println("es nulo!!!");
                                                tasa="0";
                                            }
                                            else{

                                                val2=tasas.getValor_tasa1();
                                            }//System.out.println("por monesa val2:"+val2);

                                        }
                                        else{
                                            //System.out.println("val:"+val);
                                            if(model.facturaService.getFactu().getValor_tasa().doubleValue()==0.0){
                                                model.facturaService.getFactu().setValor_tasa(new Double("1"));
                                            }
                                            val/=model.facturaService.getFactu().getValor_tasa().doubleValue();


                                            //System.out.println("val2:"+val2);
                                            val*=val2;
                                            //System.out.println("val:"+val);
                                        }

                                        factu_deta.setValor_unitario(val);
                                        //System.out.println("factu_deta.getValor_unitario:"+factu_deta.getValor_unitario());

                                        if(tasas!=null){

                                            double valoruni=0;
                                            if(tasas.getValor_tasa1()==tasas.getValor_tasa2()){

                                                valoruni = factu_deta.getValor_unitario()*tasas.getValor_tasa1();
                                                //System.out.println("if:"+valoruni );

                                            }
                                            else{
                                                valoruni = factu_deta.getValor_unitario()*val2*tasas.getValor_tasa2();
                                                //System.out.println( );
                                                //System.out.println(factu_deta.getValor_unitario()+" * "+val2+" * "+tasas.getValor_tasa2()+" = "+valoruni);

                                            }
                                            //System.out.println("tasas!= "+valoruni);
                                            factu_deta.setValor_unitario(valoruni);
                                            factu_deta.setValor_item(this.getValor(valoruni*factu_deta.getCantidad().doubleValue(),model.facturaService.getFactu().getMoneda()));
                                            factu_deta.setValor_itemme(factu_deta.getValor_item());
                                            factu_deta.setValor_unitariome(factu_deta.getValor_unitario());

                                            factu_deta.setMoneda(model.facturaService.getFactu().getMoneda());
                                            factu_deta.setValor_tasa(new Double(""+tasas.getValor_tasa2()));
                                            model.facturaService.getFactu().setValor_tasa(new Double(""+tasas.getValor_tasa2()));


                                        }

                                    }
                                }catch(Exception e){
                                    e.printStackTrace();
                                }
                            }
                            a.setElementAt(factu_deta,i-1);
                            // a.add(factu_deta);


                        }
                    }
                }

                model.facturaService.getFactu().setValor_factura(total_fac);
                /**************************************
                 * el vector de items seteados (a), el a su
                 * vez copiado en los vectores de
                 * remesasfacturadas y modificadas
                 ***************************************/
                // model.facturaService.setRemesasModificadas(a);
                model.facturaService.setRemesasFacturar(a);
                next="/jsp/cxcobrar/facturacion/FacturacionAceptada.jsp?agregarItems=ok&Mensaje="+mensaje;

                /******************************************************
                 *Este bloque es para recorrer todas las remesas a facturar para aplicar el cambiop de moneda
                 *****************************************************/

                total_fac =0;
                a = model.facturaService.getRemesasFacturar();

                for(int i=0 ;i< a.size();i++){

                    factura_detalle factu_deta = (factura_detalle)a.get(i);

                    //System.out.println("getNumero_remesa:  "+factu_deta.getNumero_remesa()+"    getFecrem:"+factu_deta.getFecrem()+"     getDescripcion"+factu_deta.getDescripcion()+"        getCantidad"+factu_deta.getCantidad() +"          getValor_unitario:"+factu_deta.getValor_unitario()+"      getValor_item"+factu_deta.getValor_item() +"      getCodigo_cuenta_contable: "+factu_deta.getCodigo_cuenta_contable()+"       getUnidad: "+ factu_deta.getUnidad());
                    if(!factu_deta.getNumero_remesa().equals("") || !factu_deta.getFecrem().equals("")|| !factu_deta.getDescripcion().equals("")|| (factu_deta.getCantidad().doubleValue() != (new Double("0.0").doubleValue())) ||   (factu_deta.getValor_unitario() != 0.0) ||  (factu_deta.getValor_item() != 0.0) || !factu_deta.getCodigo_cuenta_contable().equals("")|| !factu_deta.getUnidad().equals("")){
                        // System.out.println("ENTRO1");

                        /////////////////////


                        if(causa.equals("portasa")){
                            Vector remesas = model.facturaService.getRemesas();
                            if(remesas != null && remesas.size() > 0){
                                for(int h=0; h< remesas.size();h++){
                                    factura_detalle fd= (factura_detalle) ((factura_detalle)remesas.get(h)).clonee();
                                    if(fd.getNumero_remesa().equals(factu_deta.getNumero_remesa())){
                                        fd.setAuxliliar(factu_deta.getAuxliliar());
                                        fd.setAux(factu_deta.getAux());
                                        fd.setTiposub(factu_deta.getTiposub());
                                        fd.setTiposubledger(factu_deta.getTiposubledger());
                                        factu_deta = fd;
                                        break;
                                    }

                                }
                            }


                        }


                        ///////////////////


                        if(!model.facturaService.getFactu().getMoneda().equals(factu_deta.getMoneda())){

                            try{
                                if(causa.equals("portasa")){

                                    double valuni = factu_deta.getValor_unitario();
                                    double raro = 0;

                                    raro/=model.facturaService.getFactu().getValor_tasa().doubleValue();
                                    System.out.println("Raro "+raro);

                                    factu_deta.setValor_unitario(valuni*valtasa);
                                    factu_deta.setValor_unitariome(valuni);

                                    //System.out.println("QWQWQWQWQWQWQWW********* valuni: "+ valuni  + "*  factu_deta.getCantidad().doubleValue():   "+factu_deta.getCantidad().doubleValue()+"    *    "+valtasa);
                                    factu_deta.setValor_item(this.getValor(valuni*factu_deta.getCantidad().doubleValue()*valtasa , model.facturaService.getFactu().getMoneda()));
                                    factu_deta.setValor_itemme(factu_deta.getValor_item());




                                    factu_deta.setMoneda(model.facturaService.getFactu().getMoneda());
                                    factu_deta.setValor_tasa(new Double(valtasa));

                                    System.out.println("Tasa:"+valtasa);
                                    System.out.println("Valor Uni "+valuni*valtasa);
                                    System.out.println("Sub Total "+this.getValor(valuni*factu_deta.getCantidad().doubleValue()*valtasa,model.facturaService.getFactu().getMoneda()));


                                }else{


                                    Tasa tasas = model.tasaService.buscarValorTasa( model.facturaService.getMonedaLocal(),factu_deta.getMoneda(),model.facturaService.getFactu().getMoneda(), actual);

                                    double val = factu_deta.getValor_unitario();
                                    //System.out.println("val:"+val);
                                    //System.out.println("tasa:"+model.facturaService.getFactu().getValor_tasa().doubleValue());
                                    double val2 = new Double(tasa.replaceAll(",","")).doubleValue();

                                    if(causa.equals("pormoneda")){
                                        if(tasas==null){
                                            //System.out.println("es nulo!!!");
                                            tasa="0";
                                        }
                                        else{

                                            val2=tasas.getValor_tasa1();
                                        }//System.out.println("por monesa val2:"+val2);

                                    }
                                    else{
                                        //System.out.println("val:"+val);
                                        if(model.facturaService.getFactu().getValor_tasa().doubleValue()==0.0){
                                            model.facturaService.getFactu().setValor_tasa(new Double("1"));
                                        }
                                        val/=model.facturaService.getFactu().getValor_tasa().doubleValue();


                                        //System.out.println("val2:"+val2);
                                        val*=val2;
                                        //System.out.println("val:"+val);
                                    }

                                    factu_deta.setValor_unitario(val);
                                    //System.out.println("factu_deta.getValor_unitario:"+factu_deta.getValor_unitario());

                                    if(tasas!=null){

                                        double valoruni=0;
                                        if(tasas.getValor_tasa1()==tasas.getValor_tasa2()){

                                            valoruni = factu_deta.getValor_unitario()*tasas.getValor_tasa1();
                                            //System.out.println("if:"+valoruni );

                                        }
                                        else{
                                            valoruni = factu_deta.getValor_unitario()*val2*tasas.getValor_tasa2();
                                            //System.out.println( );
                                            //System.out.println(factu_deta.getValor_unitario()+" * "+val2+" * "+tasas.getValor_tasa2()+" = "+valoruni);

                                        }
                                        //System.out.println("tasas!= "+valoruni);
                                        factu_deta.setValor_unitario(valoruni);
                                        factu_deta.setValor_item(this.getValor(valoruni*factu_deta.getCantidad().doubleValue(),model.facturaService.getFactu().getMoneda()));
                                        factu_deta.setValor_itemme(factu_deta.getValor_item());
                                        factu_deta.setValor_unitariome(factu_deta.getValor_unitario());

                                        factu_deta.setMoneda(model.facturaService.getFactu().getMoneda());
                                        factu_deta.setValor_tasa(new Double(""+tasas.getValor_tasa2()));
                                        model.facturaService.getFactu().setValor_tasa(new Double(""+tasas.getValor_tasa2()));


                                    }

                                }

                            }catch(Exception e){
                                e.printStackTrace();
                            }
                        }
                        a.setElementAt(factu_deta,i);
                        total_fac += factu_deta.getValor_item();


                    }
                }

                model.facturaService.setRemesasFacturar(a);
                model.facturaService.getFactu().setValor_factura(total_fac);





                /**************************************
                 * condicional para refrescar la pantalla y poder
                 *cargar modificaciones etc.
                 ***************************************/

                if(actualizar.equals("ok")){
                    //System.out.println("actualizar");
                    next="/jsp/cxcobrar/facturacion/FacturacionAceptada.jsp?Mensaje="+mensaje;

                }else if(opcion.equals("EXPORTAR")){
                    boolean doc = request.getParameter("documentos")!= null?true:false;
                    HFacturaClienteExcel hilo = new HFacturaClienteExcel();
                    hilo.start(model, usuario);
                    next="/jsp/cxcobrar/facturacion/FacturacionAceptada.jsp?Mensaje=Archivo generado con exito";
                }

                /**************************************
                 * condicionalcuando se undio el boton de aceptar
                 * y porder ingresar la factura
                 ***************************************/

                if(facturacion.equals("ok")){
                    //System.out.println("facturacion");

                    boolean error = false;

                    //BLOQUE PARA VALIDAR TODOS LOS ITEM DE LA FACTURA
                    Vector remesas_facturar = model.facturaService.getRemesasFacturar();
                    for(int i=0 ;i< remesas_facturar.size();i++){
                        mensaje ="";
                        factura_detalle factu_deta = (factura_detalle)remesas_facturar.get(i);

                        // System.out.println("ULTIMAAAAAA-->getNumero_remesa: "+factu_deta.getNumero_remesa()+"     getFecrem:"+ factu_deta.getFecrem()+"   getDescripcion:"+ factu_deta.getDescripcion()+"    getCantidad:"+factu_deta.getCantidad()+"     getValor_unitario"+factu_deta.getValor_unitario()+"     getValor_item:"+factu_deta.getValor_item()+"     getCodigo_cuenta_contable:"+factu_deta.getCodigo_cuenta_contable()+"    getUnidad:"+ factu_deta.getUnidad());
                        System.out.println(!factu_deta.getNumero_remesa().equals("") || !factu_deta.getFecrem().equals("")|| !factu_deta.getDescripcion().equals("")|| (factu_deta.getCantidad().doubleValue() != (new Double("0.0").doubleValue())) ||   (factu_deta.getValor_unitario() != 0.0) ||  (factu_deta.getValor_item() != 0.0) || !factu_deta.getCodigo_cuenta_contable().equals("")|| !factu_deta.getUnidad().equals(""));
                        if(!factu_deta.getNumero_remesa().equals("") || !factu_deta.getFecrem().equals("")|| !factu_deta.getDescripcion().equals("")|| (factu_deta.getCantidad().doubleValue() != (new Double("0.0").doubleValue())) ||   (factu_deta.getValor_unitario() != 0.0) ||  (factu_deta.getValor_item() != 0.0) || !factu_deta.getCodigo_cuenta_contable().equals("")|| !factu_deta.getUnidad().equals("")){
                            //   System.out.println("ENTRO2");
                            if(factu_deta.getCantidad().doubleValue() == (new Double("0.0").doubleValue())){
                                mensaje+="La cantidad del item "+ (i+1) +"  no puede estar en cero<BR>";
                            }
                            if(factu_deta.getValor_unitario()== 0){
                                mensaje+="El valor unitario del item "+ (i+1) +"  no puede estar en cero<BR>";
                            }
                            if(factu_deta.getValor_item()== 0){
                                mensaje+="El valor del item "+ (i+1) +"  no puede estar en cero<BR>";
                            }
                            if(factu_deta.getCodigo_cuenta_contable().equals("")){
                                mensaje+="El codigo de cuenta del item "+ (i+1) +"  no puede estar vacio<BR>";
                            }else{
                                String[] c={"",""};
                                c = model.facturaService.buscarplancuentas(factu_deta.getCodigo_cuenta_contable());
                                if(c[1].equals("S") && factu_deta.getAuxliliar().equals("")){
                                    mensaje +="La cuenta contable del item "+(i+1)+" requiere de Auxiliar<BR>";
                                }
                            }

                            if(!mensaje.equals("")){
                                error = true;
                                break;
                            }
                        }else{
                            remesas_facturar.remove(i);
                        }
                    }
                    // FIN BLOQUE


                    if(!error){
                        /*************************************************
                         * Antes de insertar la factura se cerciora que la
                         * factura este en la moneda del distrito,
                         * sino se hace la respectiva conversion.
                         *************************************************/


                        /*************************************************
                         * la consulta arroja un mesaje f, el cual
                         * es concatenado con el next para mostrar la alerta
                         * en la pagina jsp.
                         *************************************************/

                        double tasa_remesa = request.getParameter("textfield")!=null?Double.parseDouble(request.getParameter("textfield").replaceAll(",","")):1;
                        model.facturaService.getFactu().setVlr_tasa_remesas(tasa_remesa);


                        Vector vfd= new Vector();
                        for(int i=0;i<model.facturaService.getRemesasFacturar().size();i++){

                            factura_detalle fd = (factura_detalle)model.facturaService.getRemesasFacturar().get(i);
                            System.out.println("REMESA :"+fd.getNumero_remesa());
                            System.out.println("VALOR TASA:"+fd.getValor_tasa());
                            System.out.println("MONEDA:"+fd.getMoneda());

                            Vector remesas = model.facturaService.getRemesas();
                            if(remesas != null && remesas.size() > 0){
                                for(int h=0; h< remesas.size();h++){
                                    factura_detalle remesasList= (factura_detalle) ((factura_detalle)remesas.get(h)).clonee();
                                    if(remesasList.getNumero_remesa().equals(fd.getNumero_remesa()) && !remesasList.getMoneda().equals(fd.getMoneda()) ){

                                        String desc = fd.getDescripcion();
                                        String total = (!fd.getMoneda().equals("DOL"))?UtilFinanzas.customFormat(fd.getValor_item()):UtilFinanzas.customFormat2(fd.getValor_item());
                                        String flete = (!remesasList.getMoneda().equals("DOL"))?UtilFinanzas.customFormat(fd.getValor_unitario()):UtilFinanzas.customFormat2(fd.getValor_unitario());
                                        if(fd.getCantidad().doubleValue() > 1){
                                            flete = (!remesasList.getMoneda().equals("DOL"))?UtilFinanzas.customFormat(this.getValor(fd.getValor_item()/ tasa_remesa / fd.getCantidad().doubleValue(),remesasList.getMoneda())):UtilFinanzas.customFormat2(this.getValor(fd.getValor_item()/ tasa_remesa / fd.getCantidad().doubleValue(),remesasList.getMoneda()));
                                        }else{
                                            flete = (!remesasList.getMoneda().equals("DOL"))?UtilFinanzas.customFormat(this.getValor(fd.getValor_item()/ tasa_remesa,remesasList.getMoneda())):UtilFinanzas.customFormat2(this.getValor(fd.getValor_item()/ tasa_remesa ,remesasList.getMoneda()));
                                        }


                                        desc += "  Flete: "+flete+" "+  remesasList.getMoneda()+"  Tasa:"+request.getParameter("textfield");//AMATURANA
                                        fd.setDescripcion(desc);
                                    }else if(remesasList.getNumero_remesa().equals(fd.getNumero_remesa()) && remesasList.getMoneda().equals(fd.getMoneda()) ){
                                        String desc = fd.getDescripcion();
                                        String flete = (!remesasList.getMoneda().equals("DOL"))?UtilFinanzas.customFormat(fd.getValor_unitario()):UtilFinanzas.customFormat2(fd.getValor_unitario());
                                        String total = (!fd.getMoneda().equals("DOL"))?UtilFinanzas.customFormat(fd.getValor_item()):UtilFinanzas.customFormat2(fd.getValor_item());
                                        desc += "  Flete: "+flete+" "+  remesasList.getMoneda();//AMATURANA
                                        fd.setDescripcion(desc);
                                    }

                                }
                            }


                            //System.out.println("moneda fact:"+fd.getMoneda());
                           /* if(model.facturaService.getFactu().getMoneda().equals(model.facturaService.getMonedaLocal())){
                                fd.setValor_itemme(this.getValor(fd.getValor_itemme(),model.facturaService.getFactu().getMoneda()));
                                fd.setValor_tasa(new Double(1));
                                //System.out.println("la moneda es igual la remesa!!!");
                            }
                            else{*/
                                //System.out.println("la moneda es dif!!!");

                                try{

                                    Tasa tasas = model.tasaService.buscarValorTasa( model.facturaService.getMonedaLocal(),model.facturaService.getFactu().getMoneda(),model.facturaService.getMonedaLocal(), actual);
                                    //System.out.println("val:"+fd.getValor_unitario());
                                    //System.out.println("tasa:"+model.facturaService.getFactu().getValor_tasa().doubleValue());
                                    double val2 = model.facturaService.getFactu().getValor_tasa().doubleValue();

                                    if(tasas!=null){

                                        double valoruni=0;
                                        if(tasas.getValor_tasa1()==tasas.getValor_tasa2()){
                                            valoruni = fd.getValor_unitario()*tasas.getValor_tasa1();
                                            //System.out.println("if:"+valoruni );
                                        }
                                        else{
                                            valoruni = fd.getValor_unitario()*val2*tasas.getValor_tasa2();
                                            //System.out.println("else:"+valoruni );
                                        }
                                        //System.out.println("tasas!= "+valoruni);
                                        fd.setValor_unitariome(fd.getValor_unitario());
                                        fd.setValor_unitario(this.getValor(valoruni,model.facturaService.getMonedaLocal()));
                                        fd.setValor_itemme(this.getValor(fd.getValor_item(),model.facturaService.getFactu().getMoneda()));
                                        fd.setValor_item(this.getValor(valoruni*fd.getCantidad().doubleValue(),model.facturaService.getMonedaLocal()));
                                        fd.setMoneda(model.facturaService.getMonedaLocal());
                                        fd.setValor_tasa(new Double(""+tasas.getValor_tasa2()));
                                    }

                                }catch(Exception e){
                                    e.printStackTrace();
                                }
                          //  }



                            vfd.add(fd);
                        }
                        model.facturaService.setRemesasFacturar(vfd);
                        model.facturaService.calcularTotalme();

                        try{

                            if(model.facturaService.getMonedaLocal().equals(model.facturaService.getFactu().getMoneda())){
                                //System.out.println("son iguales las monedas:");
                                //System.out.println("getValor_factura:"+model.facturaService.getFactu().getValor_factura());
                                //model.facturaService.getFactu().setValor_facturame(model.facturaService.getFactu().getValor_factura());
                                //System.out.println("getValor_facturame:"+model.facturaService.getFactu().getValor_facturame());
                                model.facturaService.getFactu().setValor_tasa(new Double("1"));
                                //System.out.println("getValor_tasa:"+ model.facturaService.getFactu().getValor_tasa());

                            }
                            else{
                                //System.out.println("son diferentes las monedas:");
                                //model.facturaService.getFactu().setValor_facturame(model.facturaService.getFactu().getValor_factura());
                                //System.out.println("getValor_facturame:"+model.facturaService.getFactu().getValor_facturame());

                                try{
                                    Tasa tasas = model.tasaService.buscarValorTasa( model.facturaService.getMonedaLocal(),model.facturaService.getFactu().getMoneda(),model.facturaService.getMonedaLocal(), actual);
//                                    double valorF = this.getValor(model.facturaService.getFactu().getValor_facturame()*tasas.getValor_tasa(),model.facturaService.getMonedaLocal());
//                                    model.facturaService.getFactu().setValor_factura(valorF);
                                    model.facturaService.getFactu().setValor_tasa(new Double(""+tasas.getValor_tasa()));
                                }catch(Exception e){
                                    e.printStackTrace();
                                    throw  new  Exception(e.getMessage());
                                }
                            }
                            //comentari hecho 2006-12-29
                            System.out.println("CODIGO CLIENTEEEEEE--->" + model.facturaService.getFactu().getCodcli());
                            model.facturaService.getFactu().setAgencia_cobro( model.facturaService.buscarAgenciaCobro(model.facturaService.getFactu().getCodcli()));
                            model.facturaService.getFactu().setCmc(cmc);
                            String sql = model.facturaService.ingresarFactura(model.facturaService.getFactu(), distrito,usuario.getLogin(), base);
                            System.out.println("SQL---->" +sql);
                            if(model.facturaService.getNumfac().equals(""))
                                next="/jsp/cxcobrar/facturacion/resultadoFacturacion.jsp?Mensaje=La factura no pudo ser creada !!! favor intente de nuevo";
                            else{
                                sql+=model.facturaService.ingresarFacturaDetalle(model.facturaService.getFactu(), distrito,usuario.getLogin(), base,model.facturaService.getNumfac());
                                
                                String[] vactorSql=sql.split(";");
                                ArrayList<String> listQuerys = new ArrayList<>();
                                for (String sql1 : vactorSql) {
                                    listQuerys.add(sql1);
                                }
                              
                                System.out.println("SQL-------->"+ sql);
                                TransaccionService tService = new TransaccionService(usuario.getBd());
                                tService.crearStatement();                                
                                tService=getAddBatch(tService, listQuerys);  // Cabecera, Detalle, Impuesto detalle
                                tService.execute();
                                //eliminar el archivo de texto

                                next="/jsp/cxcobrar/facturacion/resultadoFacturacion.jsp?Mensaje=La factura creada exitosamente!!! con el numero:"+model.facturaService.getNumfac();
                                model.facturaService.borrarArchivo( "factura"+usuario.getLogin()+".txt", usuario.getLogin() );
                            }
                        }catch(Exception ex){
                            ex.printStackTrace();
                            next="/jsp/cxcobrar/facturacion/resultadoFacturacion.jsp?Mensaje=La factura no pudo ser creada ERROR :"+ex.getMessage();

                        }

                        //}
                    }else{
                        next="/jsp/cxcobrar/facturacion/FacturacionAceptada.jsp?Mensaje="+mensaje;
                    }
                }

                /**************************************
                 * condicionalcuando para guardar los items en un
                 * archivo .txt, el cual se encuentra en c:slt/migraciones/
                 ***************************************/

                if(guardar.equals("ok")){
                    //System.out.println("guardar");
                    model.facturaService.setRemesasFacturar(a);
                    try{
                        next="/jsp/cxcobrar/facturacion/FacturacionAceptada.jsp";
                        model.facturaService.escribirArchivo(model.facturaService.getFactu(), model.facturaService.getRemesasFacturar(), "factura"+usuario.getLogin()+".txt", usuario.getLogin());

                    }catch(Exception ex){
                        request.setAttribute("mensaje","Error no se pudo almacenar el archivo");
                    }
                }

                if(causa.equals("pormoneda")){
                    try{
                        if(mensaje.equals("")){
                            Tasa tasas = model.tasaService.buscarValorTasa( model.facturaService.getMonedaLocal(),moneda,model.facturaService.getMonedaLocal(), actual);

                            model.facturaService.getFactu().setValor_tasa(new Double(""+tasas.getValor_tasa()));
                        }
                        else
                            model.facturaService.getFactu().setValor_tasa(new Double(tasa2));
                    }catch(Exception r){

                    }
                }else
                    model.facturaService.getFactu().setValor_tasa(new Double(tasa.replaceAll(",","")));

            }//FIN DEL SI AGREGAR

            else if(aceptar.equals("ok")){

                /************************************************
                 * /**Se mete para anexar las remesas escojidas****/
                /***********************************************/
                next="/jsp/cxcobrar/facturacion/FacturacionAceptada.jsp?close=ok&reload=ok";



                Vector a = new Vector();
                Vector remesasModificadas = model.facturaService.getRemesasFacturar();
                String s = "";
                String[] c={"",""};
                double total =0;


                for(int i=0; i<model.facturaService.getRemesas().size();i++){
                    factura_detalle factu_deta = (factura_detalle) ((factura_detalle)model.facturaService.getRemesas().get(i)).clonee();
                    factu_deta.setTiposubledger("");
                    factu_deta.setAuxliliar("");
                    factu_deta.setAux("N");
                    if(!s.equals(factu_deta.getCodigo_cuenta_contable())){
                        c=model.facturaService.buscarplancuentas(factu_deta.getCodigo_cuenta_contable());

                        if(c[1].equals("S")){
                            try{
                                modelcontab.subledgerService.buscarCuentasTipoSubledger(factu_deta.getCodigo_cuenta_contable());
                                factu_deta.setTiposub(modelcontab.subledgerService.getCuentastsubledger());
                            }catch(Exception o){
                                o.printStackTrace();
                            }
                        }else{
                            factu_deta.setTiposub(null);
                            factu_deta.setTiposubledger("");
                            factu_deta.setAuxliliar("");
                        }

                    }
                    factu_deta.setAux(c[1]);


                    int modif=0;
                    for(int j=0;j<remesasModificadas.size();j++){
                        factura_detalle factu_det =(factura_detalle)remesasModificadas.get(j);
                        if(factu_det.getNumero_remesa().equals(factu_deta.getNumero_remesa())){
                            modif++;
                            factu_deta= factu_det;

                            remesasModificadas.remove(j);
                        }
                    }

                    //TENGO QUE COLOCAR LA CONDICION PARA SELECCIONAR LAS QUE YA ESTABAN EN LA FACTURA

                    if(request.getParameter("checkbox"+i)!=null){
                        ((factura_detalle) model.facturaService.getRemesas().get(i)).setIncluida(true);
                        Vector cr= model.facturaService.costoReembolsable(factu_deta.getNumero_remesa());
                        System.out.println("costo reem:"+cr.size());

                        if(modif>0){
                            a.add(factu_deta);
                            total += factu_deta.getValor_item();
                        }
                        else{
                            if(model.facturaService.getFactu().getMoneda().equals(factu_deta.getMoneda())){
                                //System.out.println("iguales");
                                model.facturaService.getFactu().setValor_tasa(new Double("1"));
                                factu_deta.setValor_tasa(new Double("1"));
                                factu_deta.setValor_unitariome(factu_deta.getValor_unitariome());
                                factu_deta.setValor_itemme( factu_deta.getValor_itemme());

                            }
                            else{

                                //System.out.println("diferentes");
                                try{
                                    Tasa tasas = model.tasaService.buscarValorTasa( model.facturaService.getMonedaLocal(),factu_deta.getMoneda(),model.facturaService.getFactu().getMoneda(), actual);


                                    //System.out.println("val:"+factu_deta.getValor_unitario());
                                    //System.out.println("tasa:"+model.facturaService.getFactu().getValor_tasa().doubleValue());
                                    double val2 = model.facturaService.getFactu().getValor_tasa().doubleValue();

                                    if(tasas!=null){
                                        double valoruni=0;
                                        if(tasas.getValor_tasa1()==tasas.getValor_tasa2()){
                                            valoruni = factu_deta.getValor_unitario()*tasas.getValor_tasa1();
                                            //System.out.println("if:"+valoruni );
                                            //ESTA FUE LA QUE COMENT
                                            model.facturaService.getFactu().setValor_tasa(new Double(tasas.getValor_tasa1()));
                                        }
                                        else{
                                            valoruni = factu_deta.getValor_unitario()*val2*tasas.getValor_tasa2();

                                            //ESTA FUE LA QUE COMENT
                                            model.facturaService.getFactu().setValor_tasa(new Double(tasas.getValor_tasa2()));
                                            //System.out.println("else:"+valoruni );
                                        }
                                        //System.out.println("tasas!= "+valoruni);
                                        factu_deta.setValor_unitario(valoruni);
                                        factu_deta.setValor_item(this.getValor(valoruni*factu_deta.getCantidad().doubleValue(),model.facturaService.getFactu().getMoneda()));
                                        factu_deta.setValor_itemme(factu_deta.getValor_item());
                                        factu_deta.setValor_unitariome(factu_deta.getValor_unitario());
                                        factu_deta.setMoneda(model.facturaService.getFactu().getMoneda());
                                        factu_deta.setValor_tasa(new Double(""+tasas.getValor_tasa2()));

                                    }

                                }catch(Exception e){
                                    e.printStackTrace();
                                }

                            }



                            a.add(factu_deta);
                            total += factu_deta.getValor_item();
                            //Para calcular el valor en moneda si es diferente para los costos reembolsables
                            for(int f=0;f<cr.size();f++){
                                factura_detalle factu_d = (factura_detalle)cr.get(f);


                                if(!s.equals(factu_d.getCodigo_cuenta_contable())){
                                    c=model.facturaService.buscarplancuentas(factu_d.getCodigo_cuenta_contable());

                                    if(c[1].equals("S")){
                                        try{
                                            modelcontab.subledgerService.buscarCuentasTipoSubledger(factu_d.getCodigo_cuenta_contable());
                                            factu_d.setTiposub(modelcontab.subledgerService.getCuentastsubledger());
                                        }catch(Exception o){
                                            o.printStackTrace();
                                        }
                                    }else{
                                        factu_d.setTiposub(null);
                                        factu_d.setTiposubledger("");
                                        factu_d.setAuxliliar("");
                                    }

                                }
                                factu_d.setAux(c[1]);


                                /////////////////
                                if(model.facturaService.getFactu().getMoneda().equals(factu_d.getMoneda())){
                                    //System.out.println("iguales");
                                    factu_d.setValor_tasa(new Double("1"));
                                    factu_d.setValor_unitariome(factu_d.getValor_unitariome());
                                    factu_d.setValor_itemme( factu_d.getValor_itemme());

                                }
                                else{

                                    //System.out.println("diferentes");
                                    try{
                                        Tasa tasas = model.tasaService.buscarValorTasa( model.facturaService.getMonedaLocal(),factu_d.getMoneda(),model.facturaService.getFactu().getMoneda(), actual);


                                        //System.out.println("val:"+factu_deta.getValor_unitario());
                                        //System.out.println("tasa:"+model.facturaService.getFactu().getValor_tasa().doubleValue());
                                        double val2 = model.facturaService.getFactu().getValor_tasa().doubleValue();

                                        if(tasas!=null){
                                            double valoruni=0;
                                            if(tasas.getValor_tasa1()==tasas.getValor_tasa2()){
                                                valoruni = factu_d.getValor_unitario()*tasas.getValor_tasa1();
                                                //System.out.println("if:"+valoruni );
                                            }
                                            else{
                                                valoruni = factu_d.getValor_unitario()*val2*tasas.getValor_tasa2();
                                                //System.out.println("else:"+valoruni );
                                            }
                                            //System.out.println("tasas!= "+valoruni);
                                            factu_d.setValor_unitario(valoruni);
                                            factu_d.setValor_item(this.getValor(valoruni*factu_d.getCantidad().doubleValue(),model.facturaService.getFactu().getMoneda()));
                                            factu_d.setValor_itemme(factu_d.getValor_item());
                                            factu_d.setValor_unitariome(factu_d.getValor_unitario());
                                            factu_d.setMoneda(model.facturaService.getFactu().getMoneda());
                                            factu_d.setValor_tasa(new Double(""+tasas.getValor_tasa2()));

                                        }

                                    }catch(Exception e){
                                        e.printStackTrace();
                                    }

                                }
                                /////////////////

                                a.add(factu_d);
                                total += factu_d.getValor_item();
                            }

                        }
                    }else{
                        ((factura_detalle) model.facturaService.getRemesas().get(i)).setIncluida(false);
                    }
                }



                // por ultimo se agregan los items modificados o manuales

                for(int j=0;j<remesasModificadas.size();j++){
                    a.add((factura_detalle)remesasModificadas.get(j));
                    total += ((factura_detalle)remesasModificadas.get(j)).getValor_item();
                }
                model.facturaService.setRemesasFacturar(a);
                model.facturaService.getFactu().setValor_factura(total);


            }

            /************************************
             * Condicional para regresar a inicioFacturacion,
             * de esta manera se setean los vectores en cero.
             ***********************************/

            if(regresar.equals("ok")){
                //System.out.println("regresar");
                Vector a= new Vector();
                model.facturaService.setRemesasFacturar(a);
                model.facturaService.setRemesasModificadas(a);
                model.facturaService.setRemesas(a);
                factura b = new factura();
                model.facturaService.setFactu(b);
                //System.out.println("regresar!!!");
            }

        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }


        //System.out.println("next:"+next);
        this.dispatchRequest(next);
    }


    public double getValor(double valor, String moneda){
        int decimales = (moneda.equals("DOL")?2:0);
        return com.tsp.util.Util.roundByDecimal(valor, decimales);
    }
}
//Tito 23 Febrero