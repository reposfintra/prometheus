/*
 * BuscarNovedadnomAction.java
 *
 * Created on 3 de marzo de 2005, 02:07 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;

public class BuscarNovedadNomAction extends Action {
    
    /** Creates a new instance of BuscarNovedadnomAction */
    public BuscarNovedadNomAction() {
    }
    
    public void run() throws ServletException {
        String next="/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina");
        String nombre = (request.getParameter("nombre").toUpperCase())+"%";        
        try{ 
            model.novedadService.buscarNovedadNombre(nombre);
            Vector VecNovedads = model.novedadService.obtNovedades();            
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
         
         // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
    }
   
}
