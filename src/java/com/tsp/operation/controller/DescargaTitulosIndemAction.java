/********************************************************************
 *  Nombre Clase.................   DescargaTitulosIndemAction.java
 *  Descripci�n..................   Descarga de titulos vencidos para indemnizacion
 *  Autor........................   Ing. Iris Vargas
 *  Fecha........................   11/05/2012
 *  Versi�n......................   1.0
 *  Copyright....................   Geotech S.A
 *******************************************************************/



package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.services.ProcesoIndemnizacionServices;
import com.tsp.operation.model.threads.HDescargaContingIndem;


public class DescargaTitulosIndemAction extends Action{

  
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        try{
            HttpSession session = request.getSession();
            com.tsp.util.Util.logController(((com.tsp.operation.model.beans.Usuario)session.getAttribute("Usuario")).getLogin(),this.getClass().getName());
            Usuario     usuario   = (Usuario) session.getAttribute("Usuario");       
            ProcesoIndemnizacionServices indemserv = new ProcesoIndemnizacionServices(usuario.getBd());
            String  base    = "/jsp/fenalco/negocios/";
            String  next    = "DescargaTitulosIndem.jsp?msg=";
            String  msj     = "";
            String fecha =  request.getParameter("fecha");
            if( ! indemserv.isProcess() ){
                indemserv.setProcess( true );
                HDescargaContingIndem  hilo =  new HDescargaContingIndem ();
                hilo.start(usuario, fecha);
                msj   = "Proceso de Descarga de titulos vencidos para indemnizacion ha iniciado. Fecha de corte " + fecha  ;
            }  
            else
              msj   = "Actualmente el proceso se est� realizando, por favor intente mas tarde....";
            dispatchRequest( base + next + msj );
            
        } catch (Exception e){
             throw new ServletException(e.getMessage());
        }
    }
    
}
