/*
 * ClienteSearchAction.java
 *
 * Created on 21 de abril de 2005, 09:34 AM
 */

package com.tsp.operation.controller;


import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
/**
 *
 * @author  henry
 */
public class ClienteSearchAction extends Action{
    
    
    public ClienteSearchAction() {
    }
    
    public void run() throws ServletException {
        String next   =  "/consultas/consultasCliente.jsp";
        String var    =  request.getParameter("var");        
        try{                       
           model.clienteService.searchGeneralClientes(var.toUpperCase());
           Vector vec = model.clienteService.getClientesVec();
           if (vec.size()==0) {
               next+="?msg=";
           } else { 
               next =  "/consultas/listadoClientes.jsp";
           }
               
        }catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}
