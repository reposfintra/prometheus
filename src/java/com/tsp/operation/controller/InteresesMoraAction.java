/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.controller;

import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.beans.Usuario;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import com.tsp.operation.model.services.InteresesMoraService;
import com.tsp.operation.model.threads.HExportarExcel;
import java.util.ArrayList;
/**
 *
 * @author maltamiranda
 */
public class InteresesMoraAction extends Action{

    public InteresesMoraAction() {
    }

    @Override
    public void run() throws ServletException, InformationException {
        InteresesMoraService ims=new InteresesMoraService();
        HttpSession session  = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        String parametro=request.getParameter("parametro");
        String idclie=request.getParameter("idclie");
        String ms=request.getParameter("ms");
        String fechai=request.getParameter("fechai");
        String fechaf=request.getParameter("fechaf");
        String factura=request.getParameter("factura");
        String nota=request.getParameter("nota");
        String next="/jsp/consorcio/ResultadoInteresMora.jsp";
        String[] intereses = request.getParameterValues("fact");
        try{
            if(parametro.equals("BUSCAR_INTERESES"))
            {   session.removeAttribute("Intereses");
                session.setAttribute("Intereses",ims.buscar_intereses(idclie,ms,fechai,fechaf,factura,nota));
                next=next+"?pagina=0";
            }
            else{
                if(parametro.equals("PAGINACION"))
                {   next=next+"?pagina="+request.getParameter("pagina");
                }
                else{
                    if(parametro.equals("ANULAR_INTERESES"))
                    {   ims.anular_intereses(intereses, usuario.getLogin());
                        session.removeAttribute("Intereses");
                        session.setAttribute("Intereses",ims.buscar_intereses(idclie,ms,fechai,fechaf,factura,nota));
                        next=next+"?pagina="+request.getParameter("pagina");
                    }
                    else
                    {   if (parametro.equals("IGNORAR_INTERESES"))
                        {   ims.ignorar_intereses(intereses, usuario.getLogin());
                            session.removeAttribute("Intereses");
                            session.setAttribute("Intereses",ims.buscar_intereses(idclie,ms,fechai,fechaf,factura,nota));
                            next=next+"?pagina="+request.getParameter("pagina");
                        }
                        else{
                            if (parametro.equals("FACTURAR_INTERESES"))
                            {   ims.facturar_intereses(intereses, usuario.getLogin());
                                session.removeAttribute("Intereses");
                                session.setAttribute("Intereses",ims.buscar_intereses(idclie,ms,fechai,fechaf,factura,nota));
                                next=next+"?pagina="+request.getParameter("pagina");
                            }
                             else{
                                if (parametro.equals("EXPORTAR_INTERESES"))
                                {   HExportarExcel hilo = new HExportarExcel();
                                    hilo.start(model,(ArrayList)session.getAttribute("Intereses"), usuario,"REPORTE INTERESES MORA ECA");
                                }
                             }
                        }
                    }
                }
            }
            if (!parametro.equals("EXPORTAR_INTERESES"))
            {   this.dispatchRequest(next);
            }
            else{
                response.setContentType("text/plain");
                response.setHeader("Cache-Control", "no-cache");
                response.getWriter().println("El proceso ha terminado, por favor verifique el estado en el log de procesos ...");
            }

        }
        catch(Exception e){
            e.printStackTrace();
            e.getMessage();
        }
    }

}
