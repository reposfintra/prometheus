/********************************************************************
 *      Nombre Clase.................   RelacionGrupoSubgrupoPlacaInsertAction.java
 *      Descripci�n..................   Inserta Relaciones entre Grupo, subgrupo y placa
 *      Autor........................   Ing. Leonardo Parody
 *      Fecha........................   16.01.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  EQUIPO12
 */
public class RelacionGrupoSubgrupoPlacaInsertAction extends Action{
        
        /** Creates a new instance of RelacionGrupoSubgrupoPlacaInsertAction */
        public RelacionGrupoSubgrupoPlacaInsertAction() {
        }
        
        public void run() throws ServletException, InformationException {
                RelacionGrupoSubgrupoPlaca relacion = new RelacionGrupoSubgrupoPlaca();
                RelacionGrupoSubgrupoPlaca relacionaux = new RelacionGrupoSubgrupoPlaca();
                String  next="";
                //Info del usuario
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario) session.getAttribute("Usuario");
                String placa = request.getParameter("placa");
                String grupo = request.getParameter("grupo");
                String subgrupo = request.getParameter("subgrupo");
                String user = usuario.getLogin();
                String base = usuario.getBase();
                String dstrct = usuario.getDstrct();
                
                relacion.setPlaca(placa);
                relacion.setGroup(grupo);
                relacion.setSubgroup(subgrupo);
                relacion.setBase(base);
                relacion.setCreation_user(user);
                relacion.setDstrct(dstrct);
                
                try {
                        List relaciones = model.relaciongruposubgrupoplaca.ConsultaInsertRelacion(placa,grupo,subgrupo);
                        for (int i = 0; i < relaciones.size(); i++){
                                relacionaux = (RelacionGrupoSubgrupoPlaca)relaciones.get(i);
                        }
                        String reg_status = relacionaux.getReg_status();
                        if (model.relaciongruposubgrupoplaca.ExisteRelacion(placa, subgrupo, grupo)==true){     
                                if(reg_status!=null){
                                        if (!reg_status.equals("A")) {
                                                next = "/jsp/trafico/group_subgroup_placa/GroupSubgroupPlaca.jsp?msg=La relacion ya existe";
                                        }else if (reg_status.equals("A")){
                                                relacion.setUser_update(user);
                                                model.relaciongruposubgrupoplaca.UpdateRelacion(relacion, placa, subgrupo, grupo);
                                                next = "/jsp/trafico/group_subgroup_placa/GroupSubgroupPlaca.jsp?msg=Relacion Agregada Satisfactoriamente";
                                        }
                                }
                        }else{
                                model.relaciongruposubgrupoplaca.InsertRelacion(relacion);
                                next = "/jsp/trafico/group_subgroup_placa/GroupSubgroupPlaca.jsp?msg=Relacion Agregada Satisfactoriamente";
                        }
                }catch(SQLException e){
                        throw new ServletException(e.getMessage());
                }
                this.dispatchRequest(next);
        }
        
        
}
