/*************************************************************************
 * Nombre:        PlanillaFacturaConsultaAction.java                     *
 * Descripci�n:   Clase  Que consulta las facturas de una planilla       *
 * Autor:         Ing. Henry A. Osorio Gonzalez                          *
 * Fecha:         8 de mayo de 2007, 04:04 PM                            *
 * Versi�n        1.0                                                    *
 * Coyright:      Transportes Sanchez Polo S.A.                          *
 *************************************************************************/
package com.tsp.operation.controller;

import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  hosorio
 */
public class PlanillaFacturaConsultaAction extends Action {
    //2009-09-02
    /** Creates a new instance of PlanillaFacturaConsultaAction */
    public PlanillaFacturaConsultaAction() {
    }
    
    
     public void run() throws ServletException, InformationException {
        
        String oc = request.getParameter("planilla").toUpperCase();
        String distrito = request.getParameter("distrito");
        String next = "";
        try{
           // model.cxpItemDocService.getFacturasPlanilla(distrito, oc);
            next="/consultas/consultaFacturaPlanilla.jsp?planilla="+oc+"&distrito="+distrito;
        }
        catch(Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}
