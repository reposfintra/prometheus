/***********************************************
 * Nombre clase: DiscrepanciaAnularAction.java
 * Descripci�n: Accion para actualizar una sanci�n a la bd.
 * Autor: Jose de la rosa
 * Fecha: 27 de septiembre de 2005, 11:44 AM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 **********************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;

/**
 *
 * @author  Jose
 */
public class SancionUpdateAction extends Action{
    
    /** Creates a new instance of SancionUpdateAction */
    public SancionUpdateAction () {
    }
    
    public void run () throws ServletException, com.tsp.exceptions.InformationException {
        String next="/jsp/cumplidos/sancion/SancionModificar.jsp?reload=ok";
        HttpSession session = request.getSession ();
        int cod_sancion = Integer.parseInt (request.getParameter ("c_cod_sancion"));
        String tipo_sancion = request.getParameter ("c_tipo_sancion");
        double valor = Double.parseDouble (request.getParameter ("c_valor"));
        String observacion = request.getParameter ("c_observacion");
        String numpla = (request.getParameter ("c_numpla").toUpperCase ());
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        try{
            Sancion s = new Sancion ();
            s.setCod_sancion (cod_sancion);
            s.setNumpla (numpla);
            s.setTipo_sancion (tipo_sancion);
            s.setObservacion (observacion);
            s.setValor (valor);
            s.setUsuario_modificacion (usuario.getLogin ());
            model.sancionService.updateSancion (s);
            request.setAttribute ("msg","Sanci�n Modificada");
        }catch (SQLException e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
}
