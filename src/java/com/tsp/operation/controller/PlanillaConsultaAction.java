 /*
 * PlanillaConsultaAction.java
 *
 * Created on 4 de octubre de 2005, 09:14 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
/**
 *
 * @author  Jose
 */
public class PlanillaConsultaAction extends Action{
    
    /** Creates a new instance of PlanillaConsultaAction */
    public PlanillaConsultaAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        String next="";
        HttpSession session = request.getSession();
        String usuario = request.getParameter("c_usuario");
        String placa = (request.getParameter("c_placa")!=null)?request.getParameter("c_placa").toUpperCase():"";
        String fecha_fin = (request.getParameter("c_fecha_fin")!=null)?request.getParameter("c_fecha_fin"):"";
        String fecha_inicio = (request.getParameter("c_fecha_inicio")!=null)?request.getParameter("c_fecha_inicio"):"";
        String agencia_origen = (request.getParameter("c_agencia_origen").toUpperCase());
        String agencia_destino = (request.getParameter("c_agencia_destino").toUpperCase());
        String agencia_despacho = (request.getParameter("c_agencia_despacho").toUpperCase());
        String cedula = request.getParameter("c_cedula");
        String listar = (String) request.getParameter("listar");
        if((fecha_inicio.equals(""))&&(fecha_fin.equals(""))){
            fecha_fin = Util.getFechaActual_String(4);
            fecha_inicio = Util.fechaFinal(fecha_fin,-30);
        }
        try{                
            next="/datosplanilla/PlanillaListar.jsp";                   
            model.planillaService.consultaPlanillaUsuario(usuario, placa, fecha_inicio, fecha_fin, agencia_origen, agencia_destino, agencia_despacho, cedula);
            next = Util.LLamarVentana(next, "Listar Planillas");
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
