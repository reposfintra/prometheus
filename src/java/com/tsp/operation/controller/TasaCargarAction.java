/*
 * TasaCargarActon.java
 *
 * Created on 29 de junio de 2005, 02:58 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  DIOGENES
 */
public class TasaCargarAction extends Action {
    
    /** Creates a new instance of TasaCargarAction */
    public TasaCargarAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException {
        String next = "/jsp/trafico/tasa/Tasa.jsp?mensaje=cargado";
        this.dispatchRequest(next);
    }
    
}
