/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.tsp.operation.model.DAOS.ReasignarNegociosDAO;
//import com.tsp.operation.model.beans.AsigPerfilRiesgoAnalista;
import com.tsp.operation.model.beans.ReasignacionNegAna;
import com.tsp.operation.model.beans.Usuario;
import java.util.ArrayList;

import javax.servlet.http.HttpSession;

/**
 *
 * @author Roberto Parra
 */
public class NegociosReasignarAction extends Action{
     private final int LISTAR_ANALISTAS = 1; 
     private final int LISTAR_ANALISTAS_A_REASIGNAR = 2; 
     private final int LISTAR_NEGOCIOS_ANALISTA_AGRUPADOS = 3;  
     private final int RETORNAR_NEGOCIOS_A_REASIGNAR = 4; 
     private final int REASIGNAR_NEGOCIOS = 5; 
     private final int RETORNAR_NEGOCIOS_ANALISTA = 6;  
     
     
    
    @Override
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
           //System.out.println("Usuario login");
        String next= "";
        try {
            HttpSession session  = request.getSession();
            Usuario usuario = (Usuario)session.getAttribute("Usuario");
            String usuarioSesion= usuario.getLogin();
            String dstrct = usuario.getDstrct();
            int opcion = Integer.parseInt((request.getParameter("opcion")!=null?request.getParameter("opcion"):"0"));
            String urlpagina = "/"+request.getParameter("carpeta") +"/"+request.getParameter("pagina");
            
            String analista = request.getParameter("analista");
            
            String analistaAsignado = request.getParameter("analistaAsignado");
            String analistaAReasignar = request.getParameter("analistaAReasignar");
            String negocios_selecionados = request.getParameter("negocios_selecionados");
            String perfiles_selecionados = request.getParameter("perfiles_selecionados");
  
            
            switch (opcion) {
                case LISTAR_ANALISTAS :
                     ListarAnalistas();
                    break;
                    
               case LISTAR_ANALISTAS_A_REASIGNAR :
                     ListarAnalistasReasignar(analista);
                    break;  
                    
               case LISTAR_NEGOCIOS_ANALISTA_AGRUPADOS :
                     RetornarNegociosAnalistaAgr(analista);
                    break;  
                    
               case RETORNAR_NEGOCIOS_A_REASIGNAR :
                   RetornarNegociosAReasignar(analistaAsignado,analistaAReasignar);
                    break;  
                    
               case REASIGNAR_NEGOCIOS :
                   ReasignarNegocios( analistaAsignado,
                                      analistaAReasignar,
                                      negocios_selecionados,
                                      perfiles_selecionados,
                                      usuarioSesion,
                                      dstrct);
                    break;                    
               case RETORNAR_NEGOCIOS_ANALISTA :
                   RetornarNegociosAnalista(analistaAsignado);
                    break;

                default:
                    next = urlpagina;
                    break;
            }
                   
        } catch (Exception e) {
            e.printStackTrace();
        }
  
      // Redireccionar a la p�gina indicada.
        if (!next.equals("")) {
            this.dispatchRequest(next);
        }

    }

            private void ListarAnalistas() throws Exception {
        try {
            ReasignarNegociosDAO ReasignarNegocios = new ReasignarNegociosDAO();

           ArrayList<ReasignacionNegAna> listaanalista= ReasignarNegocios.ListarAnalistas();
            Gson gson = new Gson();
            String json = gson.toJson(listaanalista);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
   private void ListarAnalistasReasignar(String analista) throws Exception {
        try {
            ReasignarNegociosDAO ReasignarNegocios = new ReasignarNegociosDAO();

           ArrayList<ReasignacionNegAna> listaanalistaReasignar= ReasignarNegocios.ListarAnalistasReasignar(analista);
            Gson gson = new Gson();
            String json = gson.toJson(listaanalistaReasignar);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }     
   
   private void RetornarNegociosAnalistaAgr(String analista) throws Exception {
        try {
            ReasignarNegociosDAO ReasignarNegocios = new ReasignarNegociosDAO();

           ArrayList<ReasignacionNegAna> listaanalistaReasignar= ReasignarNegocios.RetornarNegociosAnalistaAgr(analista);
            Gson gson = new Gson();
            String json = gson.toJson(listaanalistaReasignar);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    } 
   
    private void RetornarNegociosAnalista(String analistaAsignado) throws Exception {
        try {
            ReasignarNegociosDAO ReasignarNegocios = new ReasignarNegociosDAO();

           ArrayList<ReasignacionNegAna> listaanalistaReasignar= ReasignarNegocios.RetornarNegociosAnalista(analistaAsignado);
            Gson gson = new Gson();
            String json = gson.toJson(listaanalistaReasignar);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }  
   
   private void RetornarNegociosAReasignar(String analistaAsignado,String analistaAReasignar) throws Exception {
        try {
            ReasignarNegociosDAO ReasignarNegocios = new ReasignarNegociosDAO();

           ArrayList<ReasignacionNegAna> listaNegociosReasignar= ReasignarNegocios.RetornarNegociosAReasignar(analistaAsignado,analistaAReasignar);
            Gson gson = new Gson();
            String json = gson.toJson(listaNegociosReasignar);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    } 
 
     private void ReasignarNegocios(String analistaAsignado,
                                     String analistaAReasignar,
                                     String negocios_selecionados,
                                     String perfiles_selecionados,
                                     String usuarioSesion,
                                     String dstrct) throws Exception {
            String respuesta="";
        try {
            ReasignarNegociosDAO ReasignarNegocios = new ReasignarNegociosDAO();
            
                // asigPerfilRiesgo.EliminarAsigPerfiles(nit);//elimina los perfiles que tiene relacionado un analista
                 if(!negocios_selecionados.equals("")){
                   String[] perfiles = perfiles_selecionados.split(",");
                   String[] negocios = negocios_selecionados.split(",");
                   int i=0;
                   int cant_asig=0;
                   int CantRepetidos=0;
                   
                   
                   /*Se realiza un conteo de los registros del vector y los 
                     que esten repetidos se cuentan dos veces
                     por lo que si hay datos repetidos la cantidad de regtros 
                     contados sera mayor a la cantidad de registros del vector*/
                    for (String idnegocio : negocios){
                       for (String idnegociocom : negocios){
                          if(idnegocio.equals(idnegociocom)){
                            CantRepetidos+=1;
                          }
                       }
                      }
                    
                   if(CantRepetidos>negocios.length){
                         respuesta="a marcado negocios repetidos";
                   }else{
                     for (String negocio : negocios){
                         String perfil = perfiles[i];
                         ArrayList<ReasignacionNegAna> listaCantidadesNegocios= ReasignarNegocios.RetornarCantidadNegocios(analistaAReasignar,perfil);
                         int cant_cred_asig = 0;
                         int cant_max_cred =  0;
                         if(!listaCantidadesNegocios.isEmpty()){
                                ReasignacionNegAna cantidades = listaCantidadesNegocios.get(0);
                                cant_cred_asig= Integer.parseInt(cantidades.getCant_cred_asig());
                                cant_max_cred=  Integer.parseInt(cantidades.getCant_max_cred());
                             }
                         i+=1;
                         if( (cant_cred_asig < cant_max_cred) || listaCantidadesNegocios.isEmpty() ){
                             cant_asig=cant_asig+1;
                           respuesta =  ReasignarNegocios.ActualizarNegocio(analistaAReasignar,
                                                                   negocio);
                           

                         if("ok".equals(respuesta)){
                            String ValNegocio= ReasignarNegocios.buscarNegocio(negocio);//Variable para validar si el negocio existe en la tabla <negocios_asig_temp>
                             if(analistaAsignado.equals("-1") || ValNegocio.equals("") ){
                                //Esta asigna el negocio.
                              respuesta =  ReasignarNegocios.RegistrarTemporal(
                                                                   analistaAReasignar,
                                                                   perfil,
                                                                   negocio,
                                                                   dstrct,
                                                                   usuarioSesion);
                             }else{
                              respuesta =  ReasignarNegocios.ActualizarTemporal(
                                                                   analistaAReasignar,
                                                                   perfil,
                                                                   negocio,
                                                                   dstrct,
                                                                   usuarioSesion);
                             }

                            if("ok".equals(respuesta)){
                              respuesta =  ReasignarNegocios.ActualizarHistorico(negocio);
                             }
                           }
                         }else{
                           respuesta="Se asignaron "+cant_asig+" negocio(s), no fue posible asignar la totalidad de negocios debido a que fue superado la cantidad m�xima de negocios para alg�n perfil.";
                         }
                   }                
                   }

                 }else{
                 respuesta="No a seleccionado ning�n negocio";
                 }
           
            
        } catch (Exception ex) {
              respuesta="Error al asignar las unidades";
            ex.printStackTrace();
        }
        Gson gson = new Gson();
            String json = gson.toJson(respuesta);
            //this.printlnResponse(json, "application/json;");
            this.printlnResponseAjax(json, "application/string;");
    }   
    
    
    
    
    
    
    
    
    
    
          
    public void printlnResponseAjax(String respuesta, String contentType) throws Exception {

        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);

    }
}
