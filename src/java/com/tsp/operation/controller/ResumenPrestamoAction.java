/***************************************
* Nombre Clase ............. ResumenPrestamoAction.java
* Descripci�n  .. . . . . .  Maneja los eventos del resumen general de prestamo
* Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
* Fecha . . . . . . . . . .  04/03/2006
* versi�n . . . . . . . . .  1.0
* Copyright ...Transportes Sanchez Polo S.A.
*******************************************/



package com.tsp.operation.controller;




import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.beans.*;



public class ResumenPrestamoAction extends Action{
    
   
    public void run() throws ServletException, InformationException {
        
        try{
            
             HttpSession session = request.getSession();
             Usuario     usuario = (Usuario)session.getAttribute("Usuario");
          
            
             String next   = "/jsp/cxpagar/prestamos/consultas/ListadoResumenPrestamos.jsp?msj=";
             String msj    = "";
             String evento = request.getParameter("evento");
             
             if(evento!=null){
                 
                   if(evento.equals("BUSCAR")){
                       String tercero       = request.getParameter("tercero");
                       String clasificacion = request.getParameter("clasificacion");
                       model.ResumenPrtSvc.searchPrestamos( usuario.getDstrct(), tercero, clasificacion );
                       List lista = model.ResumenPrtSvc.getListaPrestamos();
                       if(lista.size()==0)
                           msj="No se encontraron prestamos...";
                   }
             }
             
             
             next+=msj;
        
             RequestDispatcher rd = application.getRequestDispatcher(next);
             if(rd == null)
                    throw new Exception("No se pudo encontrar "+ next);
             rd.forward(request, response);
            
        }catch(Exception e){
            throw new ServletException( e.getMessage() );
        }
        
    }
    
}
