/*
 * CargarVariosrutaAction.java
 *
 * Created on 29 de junio de 2005, 03:43 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  Jcuesta
 */
public class CargarVariosrutaAction extends Action{
    
    /** Creates a new instance of CargarVariosrutaAction */
    public CargarVariosrutaAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/" + request.getParameter("carpeta")+ "/" + request.getParameter("pagina")+"?sw=ok";
                
        String pagina = request.getParameter("pagina");
        HttpSession session = request.getSession();        
        try{
            //CARGAR ESTADOS ORIGEN
            if(request.getParameter("anular")==null){
                String pais = request.getParameter("c_paiso");
                model.ciudadservice.listarCiudadesxpais(pais);
                request.setAttribute("ciudadO", model.ciudadservice.obtenerCiudades());
                
                //ESTADO DESTINO
                String paisd = request.getParameter("c_paisd");
                model.ciudadservice.listarCiudadesxpais(paisd);
                request.setAttribute("ciudadD", model.ciudadservice.obtenerCiudades());
            }
            
            String cia = request.getParameter("c_cia");
            //System.out.println("Distrito: " + cia);
            String via =request.getParameter("c_via");
            //System.out.println("Via: "+via);
            if(!via.equals("")){
                String origen = via.substring(via.length()-2,via.length());
                model.tramoService.listTramos(origen,cia,"Seleccione");
               //System.out.println("Origen 1: "+origen);    
            }else{
                via =request.getParameter("c_origen");                
                String origen = via;
                model.tramoService.listTramos(origen,cia,"Seleccione");
               //System.out.println("Origen2: "+origen);
            }
            
            
            
        }catch (SQLException e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
