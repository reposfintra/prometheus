/*
 * Nombre        MenuAnularOpcionAction.java
 * Autor         Ing. Sandra M. Escalante G.
 * Fecha         4 de mayo de 2005, 01:43 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Administrador
 */
public class MenuAnularOpcionAction extends Action {
    
    /** Creates a new instance of MenuAnularOpcionAction */
    public MenuAnularOpcionAction() {
    }
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");        
        
        String next = request.getParameter("carpeta") + "/" + request.getParameter("pagina");
        
        int ido = Integer.parseInt(request.getParameter("opcion"));
        int idpadre = Integer.parseInt(request.getParameter("id_padre"));        
        
        try{ 
            model.menuService.obtenerOpcion(ido);
            Menu m = model.menuService.getMenuOpcion();
            m.setUser_update(usuario.getLogin());
            model.menuService.anularOpcionMenu(m);
            
            ///anulo la opcion de los perfiles que la tienen asiganda
            model.perfilService.listarPOxOpcion(ido);
            Vector vpo = model.perfilService.getVPerfil();
            for (int i = 0; i < vpo.size(); i++){
                PerfilOpcion po = (PerfilOpcion) vpo.elementAt(i);
                model.perfilService.anularPerfilOpcion(usuario.getLogin(), po.getId_perfil(), ido);
            }                                   
            
            if (request.getParameter("pagina").equals("AnularOpcionMenu2.jsp")){
                next = next + "?idop="+ idpadre + "&msg=Anulaci�n Exitosa!";
                if (idpadre == 0){
                    next = request.getParameter("carpeta") + "/AnularOpcionMenu.jsp";
                }
            }
            
            if (idpadre != 0){
                Menu m1 = model.menuService.buscarOpcionMenuxIdopcion(idpadre);
                model.menuService.obtenerPadreMenuxIDopcion(idpadre);                   
                model.menuService.obtenerHijos(idpadre);  
            }else{
                model.menuService.obtenerPadresRaizMenu ();
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}
