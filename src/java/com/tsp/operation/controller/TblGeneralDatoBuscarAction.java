/******************************************************************
* Nombre ......................TblGeneralDatoBuscarAction.java
* Descripci�n..................Clase Action para Vehiculos no retornados
* Autor........................Armando Oviedo
* Fecha........................21/12/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
/**
 *
 * @author  Armando Oviedo
 */
public class TblGeneralDatoBuscarAction extends Action{
    
    /** Creates a new instance of TblGeneralDatoBuscarAction */
    public TblGeneralDatoBuscarAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "/jsp/general/TblGeneralDato/TblGeneralDatoListar.jsp";
        try{
            String mensaje = request.getParameter("mensaje");
            if(mensaje!=null){
                if(mensaje.equalsIgnoreCase("listartodos")){                    
                    model.tblgendatosvc.buscarTodosTablaGeneralDato();                    
                }
                else if(mensaje.equalsIgnoreCase("listar")){
                    String codtabla = request.getParameter("codtabla");                    
                    TblGeneralDato tmp = new TblGeneralDato();                    
                    model.tblgendatosvc.buscarItemsCodigoTabla(codtabla);                                        
                }
            }
        }catch(SQLException ex){
            ex.printStackTrace();
        }
        this.dispatchRequest(next);
    }
    
}
