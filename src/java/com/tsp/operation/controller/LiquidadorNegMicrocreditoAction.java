package com.tsp.operation.controller;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.html.simpleparser.HTMLWorker;
import com.lowagie.text.html.simpleparser.StyleSheet;
import com.lowagie.text.pdf.Barcode;
import com.lowagie.text.pdf.Barcode128;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.GestionConveniosDAO;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.GestionCondicionesService;
import com.tsp.operation.model.services.GestionSolicitudAvalService;
import javax.servlet.http.HttpSession;
import com.tsp.util.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.StringReader;
import java.text.SimpleDateFormat;
import com.tsp.operation.model.services.GestionConveniosService;

/**
 *
 * @author Iris Vargas
 */
public class LiquidadorNegMicrocreditoAction extends Action {

    Usuario usuario = null;
    private String next;

    @Override
    public void run() throws ServletException, InformationException {

        try {
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            String opcion = request.getParameter("opcion");
            boolean redirect = false;
            String strRespuesta = "";
            next = "/jsp/fenalco/liquidadores/LiquidacionMicrocredito.jsp";
            try {
                if (opcion.equals("buscarForm")) {
                    strRespuesta = obtenerForm();
                } else if (opcion.equals("validartasa")) {
                    strRespuesta = validarTasa();
                } else if (opcion.equals("buscarTitVlrConv")) {
                    strRespuesta = obtenerTitulosValorConvenio();
                } else if (opcion.equals("calcularLiquidacion")) {
                    calcularLiquidacion();
                    redirect = true;
                } else if (opcion.equals("generarRecibos")) {
                    strRespuesta = generarRecibos();
                    redirect = false;
                } else if (opcion.equals("datosConvenio")) {
                    strRespuesta = datosConvenio();
                    redirect = false;
                }
                if (redirect == false) {
                    this.escribirResponse(strRespuesta);
                } else {
                    this.dispatchRequest(next);
                }
            } catch (Exception e) {
                throw new ServletException(e.getMessage(), e);
            }
        } catch (Exception ex) {
            Logger.getLogger(LiquidadorNegMicrocreditoAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Escribe el resultado de la consulta en el response de Ajax
     * @param dato la cadena a escribir
     * @throws Exception cuando hay un error
     */
    protected void escribirResponse(String dato) throws Exception {
        try {
            response.setContentType("text/plain; charset=utf-8");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println(dato);
        } catch (Exception e) {
            throw new Exception("Error al escribir el response: " + e.toString());
        }
    }

    private String obtenerForm() throws Exception {
        String trans = request.getParameter("trans") != null ? request.getParameter("trans") : "N";
        ArrayList<BeanGeneral> lista = null;
        BeanGeneral bean = null;
        boolean pasa = true;
        String cadenatabla = "<div style='overlay: auto;'><table width='100%' style='border-collapse: collapse;'>"
                + "<thead>"
                + "<tr class='subtitulo1'>"
                + "<th>Cedula</th>"
                + "<th>Nombre</th>"
                + "<th>No Formulario</th>"
                + "<th>Valor Solicitado</th>"
                + "</tr>"
                + "</thead></tbody>";
        try {
            lista = model.gestionConveniosSvc.obtenerForm("Microcredito",trans.equals("S")?false:true );
        } catch (Exception e) {
            pasa = false;
            System.out.println("Error al buscar datos: " + e.toString());
        }
        if (pasa == true && lista.size() > 0) {
            for (int i = 0; i < lista.size(); i++) {
                bean = lista.get(i);
                cadenatabla += "<tr style='cursor:pointer;' class='filaazul' onclick='datosform(\"" + bean.getValor_03() + "\",\"" + bean.getValor_01() + "\",\"" + bean.getValor_04() + "\",\"" + bean.getValor_05() + "\",\"" + bean.getValor_07() + "\",\"" + bean.getValor_08() + "\",\"" + bean.getValor_09() + "\",\"" + bean.getValor_10() + "\",\""+bean.getValor_11()+ "\",\""+bean.getValor_12()+"\",\""+bean.getValor_13()+"\",\""+bean.getValor_14()+"\");cerrarDiv();'>"
                        + "<td>" + bean.getValor_01() + "</td>"
                        + "<td>" + bean.getValor_02() + "</td>"
                        + "<td>" + bean.getValor_03() + "</td>"
                        + "<td>" + bean.getValor_04() + "</td>"
                        + "</tr>";
                bean = null;
            }
        } else {
            cadenatabla += "<tr class='filaazul' align='center' style='cursor:pointer;' onclick='cerrarDiv();'>"
                    + "<td colspan='4'>No se encontraron registros</td></tr>";
        }
        cadenatabla += "</tbody></table></div>";

        return cadenatabla;
    }
    
     private String datosConvenio() throws Exception {
        String idConvenio = request.getParameter("idConvenio");
         GestionConveniosService convenioserv = new GestionConveniosService(usuario.getBd());
        Convenio convenio = convenioserv.buscar_convenio(usuario.getBd(),idConvenio + "");
        String str = ""+convenio.getTasa_interes()+";_;"+convenio.getMonto_minimo()+";_;"+convenio.getMonto_maximo()+";_;"+convenio.getPlazo_maximo();

        return str;
    }
    
     private String validarTasa() throws Exception {
        String solc = request.getParameter("solc") != null ? request.getParameter("solc") : "0";
        String tasaconv = request.getParameter("solc") != null ? request.getParameter("solc") : "0";
        GestionSolicitudAvalService gserv = new GestionSolicitudAvalService(usuario.getBd());
        SolicitudAval s = gserv.buscarSolicitud(Integer.parseInt(solc));
        String cadena = "S";
        if (s != null && s.getCodNegocio() != null) {
            Negocios neg = model.Negociossvc.buscarNegocio(s.getCodNegocio());
            if (Double.parseDouble(neg.getTasa()) != Double.parseDouble(tasaconv)) {
                if (neg.getNuevaTasa() != null) {
                    if (neg.getNuevaTasa().equals("N")) {
                        cadena = "N";
                    }
                } else {
                    cadena = "E";
                }
            }
        }
        return cadena;
    }

    private String obtenerTitulosValorConvenio() throws Exception {
        String idConvenio = request.getParameter("idConvenio");
        GestionCondicionesService gserv = new GestionCondicionesService(usuario.getBd());
        ArrayList<String> lista = gserv.titulosValorConvenio(idConvenio);
        String strHtml = "<option value='' selected>...</option>";
        if (lista != null && lista.size() > 0) {
            String row[] = null;
            for (int i = 0; i < lista.size(); i++) {
                row = (lista.get(i)).split(";_;");
                strHtml += "<option value=" + row[0] + "_" + row[2] + ">" + row[1] + "</option>";
            }
        }

        return strHtml;
    }

    private void calcularLiquidacion() throws Exception {
        String trans = request.getParameter("trans") != null ? request.getParameter("trans") : "N";        
        Negocios neg = new Negocios();
        neg.setId_convenio(Integer.parseInt(request.getParameter("convid")));
        GestionConveniosService convenioserv = new GestionConveniosService(usuario.getBd());
        Convenio convenio = convenioserv.buscar_convenio(usuario.getBd(), neg.getId_convenio() + "");
        neg.setVr_negocio(Double.parseDouble(request.getParameter("txtValor")));
        String numSolc = request.getParameter("numsolc");
        String tneg = request.getParameter("cmbTituloValor").split("_")[0];
        neg.setNodocs(Integer.parseInt(request.getParameter("txtNumCuotas")));
        String nit = request.getParameter("nit");
        String renovacion=request.getParameter("renovacion");
        String fianza=request.getParameter("fianza") != null?request.getParameter("fianza"):"N";
        if(!convenio.isCat()){
            double tasa=convenioserv.getTasaProveedor( neg.getId_convenio()+"",nit );
            neg.setTasa((tasa==-1?convenio.getTasa_interes():tasa) + "");
        }else{
            neg.setTasa(convenio.getTasa_interes() + "");
        }
        
        
        if (model.Negociossvc.validarcompracartera(numSolc)){
         
            neg.setTasa(convenio.getTasa_Compra_cartera()+"");
            
        }
        
        neg.setPorcentaje_cat(convenio.getPorcentaje_cat());
        neg.setValor_capacitacion(convenio.getValor_capacitacion());
        neg.setValor_seguro(convenio.getValor_seguro() * neg.getNodocs());
        neg.setValor_central(convenio.getValor_central());
        neg.setFecha_neg(request.getParameter("fechainicio"));
        String fechaItem = request.getParameter("primeracuota");
        neg.setFpago(Util.fechasDiferenciaEnDias(Util.convertiraDate(neg.getFecha_neg()), Util.convertiraDate(fechaItem)) + "");
        neg.setFecha_liquidacion(neg.getFecha_neg());
        neg.setTipo_cuota(request.getParameter("tipo_cuota"));
        GestionSolicitudAvalService solcService = new GestionSolicitudAvalService(usuario.getBd());
        SolicitudAval solicitud = solcService.buscarSolicitud(Integer.parseInt(numSolc));  
//        get_valor_fianza(    _idsolicitud integer,    _idconvenio integer,    _plazo integer,    _valornegocio numeric)
        if(fianza.equals("S")){
            double vlr_fianza = model.Negociossvc.obtenerValorFianza(String.valueOf(neg.getId_convenio()), neg.getVr_negocio(), neg.getNodocs(),numSolc,solicitud.getProducto_fondo(),solicitud.getCobertura_fondo(),solicitud.getNit_fondo());
            neg.setValor_fianza(vlr_fianza);
        }else{
            neg.setValor_fianza(0);
        }
       
        Tipo_impuesto impuesto = model.TimpuestoSvc.buscarImpuestoxCodigo(usuario.getDstrct(), convenio.getImpuesto());
        if (impuesto == null) {
            throw new Exception("Error: el convenio " + convenio.getId_convenio() + " no tiene impuesto vigente");
        }
             
        AvalFianzaBeans avalFianza=solcService.getConfiguracionFianza(Integer.parseInt(convenio.getId_convenio()),neg.getNodocs(),solicitud.getProducto_fondo(),solicitud.getCobertura_fondo(),solicitud.getNit_fondo());
        neg.setPorc_dto_aval(avalFianza.getPorc_dto_fianza());
        neg.setPorc_fin_aval(avalFianza.getPorc_fin_fianza());

//        model.Negociossvc.calcularLiquidacionMicrocredito(neg, usuario, impuesto,renovacion,numSolc);
        
        model.Negociossvc.liquidarNegocioMicrocredito(neg, solicitud, convenio);
        
         

        double total_valor_poliza=0; //model.Negociossvc.total_valor_poliza(numSolc);
        
         next += "?fechanegocio=" + neg.getFecha_neg() + "&numsolc=" + numSolc + "&numcuotas=" + neg.getNodocs() + "&valornegocio=" + neg.getVr_negocio() + "&tasainteres=" + neg.getTasa()
                + "&capacitacion=" + convenio.getValor_capacitacion() + "&cat=" + convenio.getPorcentaje_cat() + "&central=" + convenio.getValor_central() + "&tneg=" + tneg + "&convid=" + neg.getId_convenio()
                + "&nit=" + nit + "&fpago=" + neg.getFpago() + "&fechaliqui=" + neg.getFecha_liquidacion()+"&trans="+trans+"&tipo_cuota="+neg.getTipo_cuota()
                +"&act_liq="+request.getParameter("act_liq") +"&valorfianza="+neg.getValor_fianza()+"&valor_total_poliza="+total_valor_poliza
                +"&porc_dto_aval="+neg.getPorc_dto_aval() +"&porc_fin_aval="+neg.getPorc_fin_aval()+"&fechaItem="+fechaItem+"&tasa_sic_ea="+convenio.getTasa_sic_EA()+"&tasa_max_fintra="+convenio.getTasa_Max_Fintra();

    }

    private String generarRecibos() throws Exception {
        boolean pasa = true;
        String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
        try {
            pasa = this.exportarPdf(negocio, usuario.getLogin());
        } catch (Exception e) {
            System.out.println("error en generacion pdf: " + e.toString());
            e.printStackTrace();
        }
        if (pasa == true) {
            return "Archivo generado exitosamente!\nRevise su directorio.";
        } else {
            return "Error en la generacion del archivo";
        }
    }

    /**
     * Crea un objeto de tipo Document
     * @return Objeto creado
     */
    private Document createDoc() {
        Document doc = new Document(PageSize.LETTER, 52, 50, 20, 0);

        return doc;
    }

    /**
     * Genera la ruta en que se guardara el archivo
     * @param user usuario que genera el archivo
     * @param cons consecutivo de la cotizacion
     * @param extension La extension del archivo
     * @return String con la ruta en la que queda el archivo
     * @throws Exception cuando ocurre algun error
     */
    private String directorioArchivo(String user, String cons, String extension) throws Exception {
        String ruta = "";
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + user;
            SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
            File archivo = new File(ruta);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }
            ruta = ruta + "/" + cons + "_" + fmt.format(new Date()) + "." + extension;
        } catch (Exception e) {
            throw new Exception("Error al generar el directorio: " + e.toString());
        }
        return ruta;
    }

    protected boolean exportarPdf(String negocio, String userlogin) {
        boolean generado = true;
        String directorio = "";
        System.out.println("inicia elaboracion pdf");
        ResourceBundle rb = null;
        try {
            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            directorio = this.directorioArchivo(userlogin, "cupones_" + negocio, "pdf");
            System.out.println("elaborando directorio");
            Document documento = null;
            Font fuente = new Font(Font.HELVETICA, 10);
            Font fuenteB = new Font(Font.HELVETICA, 12, Font.BOLD);
            Font fuenteN = new Font(Font.HELVETICA, 10, Font.BOLD);
            Font fuenteG = new Font(Font.HELVETICA, 9, Font.BOLD, new java.awt.Color(255, 255, 255));
            documento = this.createDoc();
            PdfWriter writer = PdfWriter.getInstance(documento, new FileOutputStream(directorio));
            documento.open();
            documento.newPage();
            float[] widths2 = {0.34f, 0.18f, 0.26f, 0.22f};
            PdfPTable thead = new PdfPTable(widths2);
            PdfPTable thead2 = new PdfPTable(widths2);
            PdfContentByte cb = writer.getDirectContent();

            String espacio = "             ";
            Negocios neg = model.Negociossvc.buscarNegocio(negocio);
            ArrayList<DocumentosNegAceptado> liqui = model.Negociossvc.buscarDetallesNegocio(negocio);
            Cliente cliente = model.Negociossvc.datosCliente(negocio);
            //encabezado
            //logo de fintra aqui
            //creamos una tabla para el logo
            PdfPTable tabla_temp = new PdfPTable(1);
            tabla_temp.setWidthPercentage(20);

            String url_logo ="logo_fintra_new.jpg";
            com.lowagie.text.Image img = com.lowagie.text.Image.getInstance(rb.getString("ruta") + "/images/" + url_logo);
            img.scaleToFit(150, 150);
            img.setBorder(1);

            PdfPCell celda_logo = new PdfPCell();
            celda_logo = new PdfPCell();
            celda_logo.setBorderWidthTop(0);
            celda_logo.setBorderWidthLeft(0);
            celda_logo.setBorderWidthRight(0);
            celda_logo.setBorderWidthBottom(0);
            celda_logo.setPhrase(new Phrase(" ", fuente));
            celda_logo.addElement(img);
            celda_logo.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
            //agremos la celda con la imagen a la tabla del logo
            tabla_temp.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
            tabla_temp.addCell(celda_logo);

            documento.add(tabla_temp);
            
            documento.add(this.getEncabezadoCarta(cliente, negocio));
            //cuerpo del documento
            StyleSheet styles = new StyleSheet();
            styles.loadTagStyle("body", "face", "times new roman");
            styles.loadTagStyle("body", "size", "11px");
            //inicia la generacion

            String texto = model.tablaGenService.obtenerTablaGen("CARTMICRO", "01").getDato() + "$" + Util.customFormat(neg.getVr_negocio())
                    + ".</p>" + model.tablaGenService.obtenerTablaGen("CARTMICRO", "02").getDato() + "(" + neg.getNodocs() + ") "
                    + model.tablaGenService.obtenerTablaGen("CARTMICRO", "03").getDato() + "<b>"
                    + " <br/><br/> __________________________________ <br/>"
                    + model.tablaGenService.obtenerTablaGen("SERALCLIEN", "01").getDato();

            //Creas un p�rrafo:
            Paragraph parrafo = new Paragraph();
            //Estableces el tipo de alineaci�n:
            parrafo.setAlignment(Element.ALIGN_JUSTIFIED);

            //As� se alinear� todo lo que tengas en el p�rrfo. Si el texto, por ejemplo se toma de un HTML, que puede estar formateado y con varios p�rrafos, todos los p�rrafos que tenga el HTML se justifican:
            ArrayList htmlObjs = HTMLWorker.parseToList(new StringReader(texto), styles);
            for (int k = 0; k < htmlObjs.size(); ++k) {
                Element elementHtml = (Element) htmlObjs.get(k);
                //Vamos a�adiendo el elemento HTML al p�rrafo anterior:
                parrafo.add(elementHtml);
            }                //A�adimos el p�rrafo al PDF:
            documento.add(parrafo);
            
            //firma del pdf.
            //String firmaDirmicro ="firma_temilda.png";
            //com.lowagie.text.Image img2 = com.lowagie.text.Image.getInstance(rb.getString("ruta") + "/images/" + firmaDirmicro);
            img.scaleToFit(10, 10);
            //img2.scaleToFit(70, 70);
            //img2.setWidthPercentage(10);
            img.setBorder(1);
            img.setAlignment(PdfPCell.ALIGN_LEFT);
            //documento.add(img2);
            
            //String texto2=" "+ model.tablaGenService.obtenerTablaGen("DIRMICRO", "01").getDato()+"\n  Jefe de Operaciones";
            //Phrase parrafo2 = new Phrase(texto2);
            //documento.add(parrafo2);

            documento.newPage();
            PdfPCell celda = new PdfPCell();
            PdfPCell celda2 = new PdfPCell();
            
            PdfPTable tableinfocli = new PdfPTable(2);
            tableinfocli.setWidths(new int[]{2, 5});
            tableinfocli.setWidthPercentage(80);
            PdfPCell cell1;
            cell1 = new PdfPCell(new Phrase("Cliente", fuenteG));
            cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell1.setBackgroundColor(new java.awt.Color(0, 128, 255));
            tableinfocli.addCell(cell1);
            cell1 = new PdfPCell(new Phrase(neg.getNom_cli(), fuente));
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell1.setBackgroundColor(new java.awt.Color(255, 255, 255));
            tableinfocli.addCell(cell1);
            cell1 = new PdfPCell(new Phrase("Identificacion", fuenteG));
            cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell1.setBackgroundColor(new java.awt.Color(0, 128, 255));
            tableinfocli.addCell(cell1);
            cell1 = new PdfPCell(new Phrase(Util.PuntoDeMil(cliente.getCodcli()), fuente));
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell1.setBackgroundColor(new java.awt.Color(255, 255, 255));
            tableinfocli.addCell(cell1);
            cell1 = new PdfPCell(new Phrase("No Obligacion", fuenteG));
            cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell1.setBackgroundColor(new java.awt.Color(0, 128, 255));
            tableinfocli.addCell(cell1);
            cell1 = new PdfPCell(new Phrase(neg.getCod_negocio(), fuente));
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell1.setBackgroundColor(new java.awt.Color(255, 255, 255));
            tableinfocli.addCell(cell1);
            cell1 = new PdfPCell(new Phrase("Cuotas", fuenteG));
            cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell1.setBackgroundColor(new java.awt.Color(0, 128, 255));
            tableinfocli.addCell(cell1);
            cell1 = new PdfPCell(new Phrase(String.valueOf(neg.getNodocs()), fuente));
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell1.setBackgroundColor(new java.awt.Color(255, 255, 255));
            tableinfocli.addCell(cell1);
            cell1 = new PdfPCell(new Phrase("Fecha Negocio", fuenteG));
            cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
            cell1.setBackgroundColor(new java.awt.Color(0, 128, 255));
            tableinfocli.addCell(cell1);
            cell1 = new PdfPCell(new Phrase(neg.getFecha_neg(), fuente));
            cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            cell1.setBackgroundColor(new java.awt.Color(255, 255, 255));
            tableinfocli.addCell(cell1);
            
            documento.add(tableinfocli);
            documento.add(new Paragraph(new Phrase(" ", fuente)));
                                    
            PdfPTable tableinfoneg = new PdfPTable(3);
            tableinfoneg.setWidths(new int[]{4, 2, 4});
            tableinfoneg.setWidthPercentage(80);
            PdfPCell cell2;
            cell2 = new PdfPCell(new Phrase("Fecha Vencimiento", fuenteG));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(new java.awt.Color(0, 128, 255));
            tableinfoneg.addCell(cell2);
            cell2 = new PdfPCell(new Phrase("Numero Cuotas", fuenteG));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(new java.awt.Color(0, 128, 255));
            tableinfoneg.addCell(cell2);
            cell2 = new PdfPCell(new Phrase("Valor Cuotas", fuenteG));
            cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
            cell2.setBackgroundColor(new java.awt.Color(0, 128, 255));
            tableinfoneg.addCell(cell2);
            for (int i = 0; i < liqui.size(); i++) {
                
                cell2 = new PdfPCell(new Phrase(liqui.get(i).getFecha().substring(0, 10), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell2.setBackgroundColor(new java.awt.Color(255, 255, 255));
                tableinfoneg.addCell(cell2);
                cell2 = new PdfPCell(new Phrase(liqui.get(i).getItem(), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell2.setBackgroundColor(new java.awt.Color(255, 255, 255));
                tableinfoneg.addCell(cell2);
                cell2 = new PdfPCell(new Phrase(String.valueOf(Util.PuntoDeMil(String.valueOf(Math.round(liqui.get(i).getValor())))), fuente));
                cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
                cell2.setBackgroundColor(new java.awt.Color(255, 255, 255));
                tableinfoneg.addCell(cell2);
                
//                thead = new PdfPTable(widths2);
//                
//                celda = new PdfPCell();
//                celda.setBorderWidth(0);
//                celda.setPhrase(new Phrase(" ", fuente));
//                if ((i + 1) % 2 == 0) {
//                    celda.setFixedHeight(90);
//                }else{
//                    celda.setFixedHeight(28);
//                }
//                celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
//                celda.setColspan(4);
//                thead.addCell(celda);
                
//                celda = new PdfPCell();
//                celda.setBorderWidth(0);
//                celda.setPhrase(new Phrase(espacio + neg.getNom_cli(), fuente));
//                celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
//                celda.setColspan(3);
//                thead.addCell(celda);
//                celda = new PdfPCell();
//                celda.setBorderWidth(0);
//                celda.setPhrase(new Phrase(Util.PuntoDeMil(neg.getCod_cli()), fuente));
//                celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
//                thead.addCell(celda);
//                celda = new PdfPCell();
//                celda.setBorderWidth(0);
//                celda.setPhrase(new Phrase(" \n\n", fuente));
//                celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
//                celda.setColspan(4);
//                thead.addCell(celda);
//                celda = new PdfPCell();
//                celda.setBorderWidth(0);
//                celda.setPhrase(new Phrase("Pago de la Cuota " + (i + 1), fuente));
//                celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
//                thead.addCell(celda);
//                celda = new PdfPCell();
//                celda.setBorderWidth(0);
//                celda.setPhrase(new Phrase(Util.customFormat((int)liqui.get(i).getValor()), fuente));
//                celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
//                celda.setColspan(3);
//                thead.addCell(celda);
               
//                celda = new PdfPCell();
//                celda.setBorderWidth(0);
//                celda.setPhrase(new Phrase("REFERENCIA No. " + consneg+convenio, fuenteB));
//                celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
//                thead.addCell(celda);
//                celda = new PdfPCell();
//                celda.setBorderWidth(0);
//                celda.setPhrase(new Phrase(" ", fuente));
//                celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
//                celda.setColspan(3);
//                thead.addCell(celda);
//                celda = new PdfPCell();
//                celda.setBorderWidth(0);
//                celda.setPhrase(new Phrase(" \n", fuente));
//                celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
//                celda.setColspan(4);
//                thead.addCell(celda);
//                celda = new PdfPCell();
//                celda.setBorderWidth(0);
//                celda.setPhrase(new Phrase("\n" + model.tablaGenService.obtenerTablaGen("OBSCOMERC", "01").getDato(), fuente));
//                celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
//                celda.setColspan(2);
//                thead.addCell(celda);
//                celda = new PdfPCell();
//                celda.setBorderWidth(0);
//                celda.setPhrase(new Phrase(" ", fuente));
//                celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
//                thead.addCell(celda);
//                celda = new PdfPCell();
//                celda.setBorderWidth(0);
//                celda.setPhrase(new Phrase("\n" + Util.customFormat((int)liqui.get(i).getValor()) + " \n\n" + liqui.get(i).getFecha().substring(0, 10), fuente));
//                celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
//                thead.addCell(celda);
//                celda = new PdfPCell();
//                celda.setBorderWidth(0);
//                celda.setPhrase(new Phrase(" \n\n\n\n\n\n\n", fuente));
//                celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
//                celda.setColspan(4);
//                thead.addCell(celda);
//                celda = new PdfPCell();
//                celda.setBorderWidth(0);
//                celda.setPhrase(new Phrase(espacio + neg.getNom_cli(), fuente));
//                celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
//                celda.setColspan(3);
//                thead.addCell(celda);
//                celda = new PdfPCell();
//                celda.setBorderWidth(0);
//                celda.setPhrase(new Phrase(Util.PuntoDeMil(neg.getCod_cli()), fuente));
//                celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
//                thead.addCell(celda);
//                celda = new PdfPCell();
//                celda.setBorderWidth(0);
//                celda.setPhrase(new Phrase("\n", fuente));
//                celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
//                celda.setColspan(4);
//                thead.addCell(celda);
                

//                celda = new PdfPCell();
//                celda.setBorderWidth(0);
//                celda.setPhrase(new Phrase("\nREFERENCIA No. " + consneg+convenio, fuenteB));
//                celda.setColspan(3);
//                celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
//                thead.addCell(celda);

//                celda = new PdfPCell();
//                celda.setBorderWidth(0);
//                celda.setPaddingTop(5);
//                celda.setPhrase(new Phrase(Util.customFormat(liqui.get(i).getValor()) + " \n\n" + liqui.get(i).getFecha().substring(0, 10), fuente));
//                celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
//                thead.addCell(celda);
                
                /*celda = new PdfPCell();
                celda.setBorderWidth(1);
                celda.setPhrase(new Phrase("", fuente));
                celda.setColspan(4);
                celda.setFixedHeight(12);
                thead.addCell(celda);*/
//                thead.setWidthPercentage(100);
//                documento.add(thead);
//                if ((i + 1) % 2 == 0) {
//                    documento.newPage();
//                }
            }
            
            celda = new PdfPCell();
            celda.setBorderWidth(0);
            celda.setPhrase(new Phrase(" \n\n", fuente));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
            celda.setColspan(4);
            thead.addCell(celda);
            
            celda2 = new PdfPCell();
            celda2.setBorderWidth(0);
            celda2.setPhrase(new Phrase(" \n\n", fuente));
            celda2.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            celda2.setColspan(4);
            thead2.addCell(celda2);
            
            
            String consneg = neg.getCod_negocio().substring(2);
            String ceros = "00000000";
            //consneg = consneg.length() % 2 == 0 ? consneg : "000" + consneg;
            consneg = consneg.length() == 8 ? consneg : ceros.substring(0, 8-consneg.length())+consneg;
            //String convenio = (neg.getId_convenio() + "").length() % 2 == 0 ? neg.getId_convenio() + "" : "0" + neg.getId_convenio();
            celda2 = new PdfPCell();
            celda2.setBorderWidth(0);
            String valor = ((int) Math.round(liqui.get(1).getValor()) + "").replace(".", "");
            int ultimafecha = (neg.getNodocs()-1);
            //valor = valor.length() % 2 == 0 ? valor : "00" + valor;
            valor = valor.length() == 8 ? valor : ceros.substring(0, 8-valor.length())+valor;
            
            Barcode128 uccEan128 = new Barcode128();
            uccEan128.setCodeType(Barcode.CODE128_UCC);
            uccEan128.setCode("(415)" + model.tablaGenService.obtenerTablaGen("FITRAGS1", "01").getDato()
                    + "(8020)" + consneg  + "(3900)" + valor
                    + "(96)" + liqui.get(ultimafecha).getFecha().substring(0, 10).replaceAll("-", ""));
            uccEan128.setBarHeight(48f);
            uccEan128.setSize(8f);
            celda2.addElement(uccEan128.createImageWithBarcode(cb, java.awt.Color.BLACK,
                    java.awt.Color.BLACK));

            celda2.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            celda2.setColspan(4);
            thead2.addCell(celda2);
            celda2 = new PdfPCell();
            celda2.setBorderWidth(0);
            celda2.setPhrase(new Phrase(" ", fuente));
            celda2.setColspan(4);
            celda2.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            thead2.addCell(celda2);
            
            documento.add(thead);
            documento.add(tableinfoneg);
            documento.add(thead2);
            documento.close();

        } catch (Exception e) {
            generado = false;
            System.out.println("error al generar pdf: " + e.toString());
            e.printStackTrace();
        }
        System.out.println("fin elaboracion pdf");
        return generado;
    }

    protected PdfPTable getEncabezadoCarta(Cliente cliente, String negocio) {
        PdfPTable tcontainer = new PdfPTable(1);
        try {
            Font fuente = new Font(Font.TIMES_ROMAN, 11);
            Font fuenteA = new Font(Font.TIMES_ROMAN, 11, Font.BOLD);
            //encabezado del documento

            PdfPCell celda;
            PdfPCell celda_vacia = new PdfPCell();
            String fecha = Util.ObtenerFechaCompleta(Calendar.getInstance());


            tcontainer.setWidthPercentage(100);

            celda_vacia = new PdfPCell();
            celda_vacia.setBorderWidth(0);
            celda_vacia.setPhrase(new Phrase("", fuente));
            tcontainer.addCell(celda_vacia);
            celda_vacia = new PdfPCell();
            celda_vacia.setBorderWidth(0);
            celda_vacia.setPhrase(new Phrase(" ", fuente));
            tcontainer.addCell(celda_vacia);
            celda = new PdfPCell();
            celda.setBorderWidth(0);
            celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
            celda.setPhrase(new Phrase("Barranquilla, " + fecha, fuente));
            tcontainer.addCell(celda);
            /*celda_vacia = new PdfPCell();
            celda_vacia.setBorderWidth(0);
            celda_vacia.setPhrase(new Phrase(" ", fuente));
            tcontainer.addCell(celda_vacia);
            celda_vacia = new PdfPCell();
            celda_vacia.setBorderWidth(0);
            celda_vacia.setPhrase(new Phrase(" ", fuente));
            tcontainer.addCell(celda_vacia);
            celda = new PdfPCell();
            celda.setBorderWidth(0);
            celda.setPhrase(new Phrase(negocio, fuente));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
            tcontainer.addCell(celda);*/
            celda_vacia = new PdfPCell();
            celda_vacia.setBorderWidth(0);
            celda_vacia.setPhrase(new Phrase(" ", fuente));
            tcontainer.addCell(celda_vacia);
            celda_vacia = new PdfPCell();
            celda_vacia.setBorderWidth(0);
            celda_vacia.setPhrase(new Phrase(" ", fuente));
            tcontainer.addCell(celda_vacia);
            celda = new PdfPCell();
            celda.setBorderWidth(0);
            celda.setPhrase(new Phrase("Se\u00F1or(a)", fuenteA));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
            tcontainer.addCell(celda);
            celda_vacia = new PdfPCell();
            celda_vacia.setBorderWidth(0);
            celda_vacia.setPhrase(new Phrase(" ", fuente));
            tcontainer.addCell(celda_vacia);
            celda = new PdfPCell();
            celda.setBorderWidth(0);
            celda.setPhrase(new Phrase("      "+cliente.getNomcli().toUpperCase(), fuenteA));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
            tcontainer.addCell(celda);
            celda = new PdfPCell();
            celda.setBorderWidth(0);
            celda.setPhrase(new Phrase("      "+negocio, fuenteA));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
            tcontainer.addCell(celda);
            /*celda = new PdfPCell();
            celda.setBorderWidth(0);
            //celda.setPhrase(new Phrase(cliente.getTipo()+" "+Util.PuntoDeMil(cliente.getCodcli()), fuenteA));
            //celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
            //tcontainer.addCell(celda);*/
            celda = new PdfPCell();
            celda.setBorderWidth(0);
            celda.setPhrase(new Phrase("      "+cliente.getDireccion().toUpperCase()+", "+cliente.getBarrio(), fuenteA));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
            tcontainer.addCell(celda);
            /*celda = new PdfPCell();
            celda.setBorderWidth(0);
            celda.setPhrase(new Phrase("      "+cliente.getBarrio().toUpperCase(), fuenteA));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
            tcontainer.addCell(celda);*/
            celda = new PdfPCell();
            celda.setBorderWidth(0);
            celda.setPhrase(new Phrase("      Cel. "+cliente.getCelular(), fuenteA));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
            tcontainer.addCell(celda);
            celda = new PdfPCell();
            celda.setBorderWidth(0);
            celda.setPhrase(new Phrase("      "+cliente.getCiudad().toUpperCase(), fuenteA));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
            tcontainer.addCell(celda);

        } catch (Exception ex) {
            Logger.getLogger("LiquidadorNegMicrocredito").log(Level.SEVERE, null, ex);
        }
        return tcontainer;
    }
}
