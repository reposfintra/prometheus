/***************************************************************************
 * Nombre clase : ............... RegistroTarjetaManagerAction.java        *
 * Descripcion :................. Clase que maneja los eventos             *
 *                                relacionados con el programa de          *
 *                                RegistroTarjeta                          *
 * Autor :....................... Ing. Juan Manuel Escandon Perez          *
 * Fecha :........................ 6 de diciembre de 2005, 08:56 AM        *
 * Version :...................... 1.0                                     *
 * Copyright :.................... Fintravalores S.A.                 *
 ***************************************************************************/
package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import java.io.File;
import com.tsp.pdf.*;
/**
 *
 * @author  JuanM
 */
public class TrasladoNacionalManagerAction extends Action{
    
    
    public TrasladoNacionalManagerAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        try{
            
            /*
             *Declaracion de variables
             */
            String documento              = (request.getParameter("documento")!=null)?request.getParameter("documento"):"";
            
            
            HttpSession Session           = request.getSession();
            Usuario user                  = new Usuario();
            user                          = (Usuario)Session.getAttribute("Usuario");
            String usuario                = user.getLogin();
            String dstrct                 = user.getDstrct();
            String base                   = user.getBase();
            
            String next         = "";
            String Mensaje      = "";
            
            
            model.RemesaSvc.buscaRemesa(documento);
            
            if( model.RemesaSvc.getRemesa() != null  ){
                
                DatoFormato dato = new DatoFormato();
                Vector vec       = new Vector();
                
                model.FormatoSvc.formato(dstrct, "002", "DRUMMOND01", documento);
                
                vec = model.FormatoSvc.getVector();
                String DesRemision = "";
                String convRemision = "";
                String Compensac = "";
                String conCompensac = "";
                if( vec.size() >= 1 ){
                    String rep [] = (String[])vec.get(0);
                    
                    String datos   []    =  rep[1].split(",");
                    String titulos []    =  rep[2].split(",");
                    DesRemision = rep[3];
                    Compensac = rep [4]; 
                   
                    System.out.println("descripcion de remision1  ---->> "+DesRemision);
                    convRemision = model.FormatoSvc.consultarDescripcionRemesion(DesRemision);
                    conCompensac = model.FormatoSvc.consultarCompensac(Compensac);
                    System.out.println("compensac   "+Compensac);
                    dato.setDesRemision(convRemision);
                    
                    dato.setCompensac(conCompensac);
                    System.out.println("bean compensac   "+dato.getCompensac());
                     String centro = "";
                    String centroConvertido = "";
                    String condicionCentro = "";
                    for( int i = 0 ; i < datos.length ; i++ ){
                        System.out.println("titulo"+ titulos[i].trim());
                        System.out.println("dato"+ datos[i].trim());
                        if( titulos[i].trim() != null || datos[i].trim() != null){
                            if( titulos[i].trim().equals("CCOSTO")){
                                dato.setCentrocosto(datos[i]);
                                System.out.println("centroCosto1   "+dato.getCentrocosto());   
                            }    
                            if( titulos[i].trim().equals("DO"))
                                dato.setElementodo(datos[i]);
                              if( titulos[i].trim().equals("PO"))
                                dato.setPo(datos[i]);
                            if( titulos[i].trim().equals("CONTENIDO"))
                                dato.setContenido(datos[i]);
                            if( titulos[i].trim().equals("OBSERVACION"))
                                dato.setObservaciones(datos[i]);
                        }
                    }
                    System.out.println("centroCosto2   "+dato.getCentrocosto());   
                    System.out.println("compensac  "+dato.getCompensac());
                    centro = dato.getCentrocosto().trim();
                    if(!centro.equals("")){
                        condicionCentro = centro.substring(0,2);
                        if(condicionCentro.equals("FC")){
                            centroConvertido = model.FormatoSvc.consultarCentro(centro);
                            dato.setCentrocosto(centroConvertido);
                        }
                    }    
                }
                
                
                DatoFormato datoaux = model.FormatoSvc.formatoComp(dato, documento);
                
                String ruta = application.getRealPath("/");
                File xslt = new File(ruta + "Templates/RemisionMercancia.xsl");
                File pdf = new File(ruta + "pdf/remision.pdf");
                
                RemisionMercanciaPDF remision = new RemisionMercanciaPDF();
                
                remision.RemisionPlantilla(xslt, pdf);
                remision.crearCabecera();
                remision.crearRemision( datoaux );
                remision.generarPDF();
                
                next = "/pdf/remision.pdf";
               
            }
            else{
                Mensaje = "La OT no existe";
                next    = "/impresiones_pdf/TrasladoNacional.jsp?Mensaje="+Mensaje;
            }
                
                RequestDispatcher rd = application.getRequestDispatcher(next);
            
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en TransladoNacionalManagerAction .....\n"+e.getMessage());
        }
        
    }
    
}
