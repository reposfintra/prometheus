/*
     - Author(s)       :      Ing. Luis Eduardo Frieri
     - Date            :      10/01/2007  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
*/
package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.*;
import com.tsp.exceptions.*;

public class MantenimientoFestivosAction extends Action{
    
    public MantenimientoFestivosAction() {
    }
    
    public void run() throws ServletException, InformationException{
        String next = "";
        HttpSession session = request.getSession();
        Usuario usuario = ( Usuario ) session.getAttribute ( "Usuario" );
        
        String opcion = request.getParameter("opcion")!=null?request.getParameter("opcion"):"";
        String fecha = request.getParameter("fecha")!=null?request.getParameter("fecha"):"";
        String descripcion = request.getParameter("descripcion")!=null?request.getParameter("descripcion"):"";
        String pais = request.getParameter("pais")!=null?request.getParameter("pais"):"";
        String fechai = request.getParameter("fechai")!=null?request.getParameter("fechai"):"";
        String fechaf = request.getParameter("fechaf")!=null?request.getParameter("fechaf"):"";
        
        int sw = 0;
        
        try{
             if (opcion.equals("insertar")){
                 String reg_status = "";
                 String dstrct = usuario.getDstrct();
                 String base = usuario.getBase();
                 if(!model.mantenimientoFestivosService.existeFestivo(fecha, pais)){
                      model.mantenimientoFestivosService.insertarFestivo(reg_status, dstrct, fecha, descripcion, usuario.getLogin (), usuario.getLogin (), base, pais);
                 }
                 else{
                     sw = 1;
                 }             
                 if(sw==1){
                    next = "/jsp/masivo/festivos/InsertarFestivo.jsp?mensaje=wrong";
                 }else{
                    next = "/jsp/masivo/festivos/InsertarFestivo.jsp?mensaje=ok";
                 }    
            }
            if (opcion.equals("listar")){
                if((fechai.equals("")) && (fechaf.equals("")) && (pais.equals(""))){
                    next="/jsp/masivo/festivos/ListarFestivo.jsp";
                    model.mantenimientoFestivosService.listarFestivo();
                }
                else{
                    next="/jsp/masivo/festivos/ListarFestivo.jsp";
                    model.mantenimientoFestivosService.BuscarFestivo(fechai, fechaf, pais);
                }
             }
            if (opcion.equals("detalle")){
                next="/jsp/masivo/festivos/ModificarFestivo.jsp";
            } 
            if (opcion.equals("modificar")){
                //String fech = request.getParameter("fecha_vieja")!=null?request.getParameter("fecha_vieja"):"";
                model.mantenimientoFestivosService.modificarFestivo(pais, descripcion, usuario.getLogin(), fecha);
                next="/jsp/masivo/festivos/ModificarFestivo.jsp?mensaje=modificado";
            }
            if (opcion.equals("eliminar")){
                model.mantenimientoFestivosService.eliminarFestivo(fecha);
                next="/jsp/masivo/festivos/ModificarFestivo.jsp?mensaje=eliminado";
            }
            if (opcion.equals("excel")){
                 HiloMantenimientoFestivos  hilo = new HiloMantenimientoFestivos();
                 hilo.start ( model.mantenimientoFestivosService.getVector(), usuario.getLogin () );
                 next="/jsp/masivo/festivos/BuscarFestivo.jsp?mensaje=exportado";

            }
            
        }catch(SQLException e){      
            e.printStackTrace();
        }
        this.dispatchRequest(next);
    }
    
}
