
/*************************************************************************************
 * Nombre clase : ............... MigracionProveedorEscoltaAction.java               *
 * Descripcion :................. Clase que maneja los eventos relacionados con la   *
 *                                generacion del archivo de migracion de proveedores *
 *                                de escolta                                         *
 * Autor :....................... Ing. Henry A.Osorio Gonz�lez                       *
 * Fecha :....................... 30 de Noviembre de 2005, 08:09 AM                  *
 * Version :..................... 1.0                                                *
 * Copyright :................... Fintravalores S.A.                            *
 ************************************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.HMigracionProveedorEscolta;

public class MigracionProveedorEscoltaAction extends Action{
    
    public void run() throws ServletException {
        String next = "/jsp/trafico/caravana/MigracionProveedorEscolta.jsp?msg=";
        String feci = request.getParameter("fecini");        
        String fecf = request.getParameter("fecfin");        
        HttpSession session = request.getSession();    
        String msg = "Archivo no Generado";
        Usuario usuario = (Usuario) session.getAttribute("Usuario");                          
        try{ 
            Vector car = model.migracionProveedorEsc.searchServiciosEscoltasACaravana(feci,fecf);            
            Vector veh = model.migracionProveedorEsc.searchServiciosEscoltasAVehiculos(feci,fecf);      
            HMigracionProveedorEscolta hilo = new HMigracionProveedorEscolta();
            hilo.start(car, veh, usuario.getLogin());
            msg = "Archivo Generado";            
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }        
        next+=msg;
        this.dispatchRequest(next);
    }
    
}
