/*
 * ModificarEstadoAction.java
 *
 * Created on 3 de marzo de 2005, 08:12 PM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  DIBASMO
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class ModificarEstadoAction extends Action {
    
    /** Creates a new instance of ModificarEstadoAction */
    public ModificarEstadoAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException{
        String next = "/jsp/trafico/estado/estado.jsp?mensaje=Modificado&reload=ok";
        String codigo = (request.getParameter("c_codigo").toUpperCase());
        String nombre = (request.getParameter("c_nombre").toUpperCase());
        String pais = (request.getParameter("c_pais"));
        String zona = (request.getParameter("c_zona").toUpperCase());
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
      //  String lenguaje = (String) session.getAttribute("idioma");
   
   
        try{

            
            Estado estado = new Estado(); 
            estado.setdepartament_code(codigo);
            estado.setdepartament_name(nombre);
            estado.setpais_code(pais);
            estado.setzona(zona);
            estado.setUser_update(usuario.getLogin());
            estado.setCreation_user(usuario.getLogin());
            model.estadoservice.modificarestado(estado);
            next=next+"&sw=1";
            model.paisservice.buscarpais(pais);
        }
        catch (SQLException e){
               throw new ServletException(e.getMessage());
        }
         
         // Redireccionar a la p�gina indicada.
       this.dispatchRequest(next);
    
    }
    
}
