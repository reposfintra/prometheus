/*
 * AnticipoGasolinaAction.java
 *
 * Created on 29 de enero de 2008, 10:06 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.HArchivosTransferenciasGasolina;
import com.tsp.exceptions.*;
import java.util.regex.*;
import org.apache.log4j.Logger;
import com.tsp.finanzas.contab.model.*;
import com.tsp.finanzas.contab.*;

import com.tsp.util.Util;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.FileItem;

import com.lowagie.text.Document;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

/**
 *
 * @author iamorales
 */

public class AnticipoGasolinaAction extends Action {

    ArrayList preanticipos;

    Logger logger = Logger.getLogger(this.getClass());
    //String rutica="";//C:/Tomcat5/webapps/slt/feb6
    String carpeta="images/fotos";
    ResourceBundle rb ;
    String directorioArchivos="";
    String soloNombreArchivo="";
    String conductor ="";
    String loginx="";

    String msj      = "";



    /** Creates a new instance of ChequeCxPAnularAction */
    public AnticipoGasolinaAction() {
    }

    public void run() throws ServletException, InformationException {
        try{

            ///Miguel Altamiranda///
            String planilla = request.getParameter("planilla");
            if (planilla==null){planilla="";}
            ///Fin Miguel Altamiranda///

            rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");//se consiguen los datos de db.properties
            directorioArchivos = rb.getString("ruta")+"/"+carpeta;//se establece la ruta de la imagen
            model.AnticiposGasolinaService.setDirectorioArchivos(directorioArchivos );
            HttpSession session = request.getSession();
            com.tsp.util.Util.logController(((com.tsp.operation.model.beans.Usuario)session.getAttribute("Usuario")).getLogin(),this.getClass().getName());
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String dstrct = session.getAttribute("Distrito").toString();

            loginx=usuario.getLogin();

            conductor = request.getParameter("conductor");
            if (conductor==null){conductor="";}
            String placa = request.getParameter("placa");
            if (placa==null){placa="";}
            String clavegas = request.getParameter("clavegas");
            RequestDispatcher rd ;
            String idAntGas;
            idAntGas=request.getParameter("idAnticipoGas");
            if (idAntGas==null){idAntGas="";}

            String valor="";
            //valor=request.getParameter("valor");
            //if (valor==null){valor="";}
            String valor_neto="";
            //valor_neto=request.getParameter("valor_neto");
            //if (valor_neto==null){valor_neto="";}

            ArrayList lista=null;
            Imagen img ;
            //String condImg=null;
            //String firma=null;

            String next = "/jsp/gasolina/mostrarAnticiposGasolina.jsp?conductor=" + conductor.toString()+"&placa="+placa.toString()+"&idAnticipoGas="+idAntGas;

            /*if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("listarAnticipos"))        {
                try{
                    if (model.AnticiposGasolinaService.existeParLoginDescuento(loginx)){
                        if ( model.AnticiposGasolinaService.isAnticipoGasolina(conductor,placa,loginx) == false ) {

                            next = "/jsp/gasolina/anticipo_gasolina.jsp?anticipoValido=no&conductor=" + conductor.toString()+"&placa="+placa.toString()+"&idAnticipoGas="+idAntGas;

                        }else{
                            model.AnticiposGasolinaService.setEstado_foto(false);
                            //condImg=getImagenConductor( conductor, loginx);
                            //if (condImg!=null){next=next+"&cond="+condImg;}
                            //firma =  getImagenFirma( conductor, loginx)            ;
                            //if (firma!=null){next=next+"&firma="+firma;}

                        }
                    }else{
                        next = "/jsp/gasolina/anticipo_gasolina.jsp?anticipoValido=descuentopendiente&conductor=" + conductor.toString()+"&placa="+placa.toString()+"&idAnticipoGas="+idAntGas;
                    }


                } catch ( Exception e){
                    System.out.println("error en listarAnticipos..."+e.toString()+"__"+e.getMessage());
                    e.printStackTrace();
                    throw new ServletException("Error en AnticipoGasolinaAction en opcion listarAnticipos .....\n"+e.getMessage());
                }
            }*/

            ////Modificado Miguel Altamiranda////
            if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("listarAnticipos"))        {
                try{
                    if (model.AnticiposGasolinaService.existeParLoginDescuento(loginx)){
                        if ( model.AnticiposGasolinaService.isAnticipoGasolina2(conductor,placa,loginx,planilla) == false ) {
                            if ( model.AnticiposGasolinaService.isAnticipoGasolina3(conductor,placa,loginx,planilla) == false ) {
                                next = "/jsp/gasolina/anticipo_gasolina.jsp?anticipoValido=no&conductor=" + conductor.toString()+"&placa="+placa.toString()+"&idAnticipoGas="+idAntGas;
                            }
                            else
                            {   next = "/jsp/gasolina/mostrarAnticiposGasolina2.jsp?conductor=" + conductor.toString()+"&placa="+placa.toString()+"&idAnticipoGas="+idAntGas;
                            }
                        }else{
                            model.AnticiposGasolinaService.setEstado_foto(false);
                            //condImg=getImagenConductor( conductor, loginx);
                            //if (condImg!=null){next=next+"&cond="+condImg;}
                            //firma =  getImagenFirma( conductor, loginx)            ;
                            //if (firma!=null){next=next+"&firma="+firma;}

                        }
                    }else{
                        next = "/jsp/gasolina/anticipo_gasolina.jsp?anticipoValido=descuentopendiente&conductor=" + conductor.toString()+"&placa="+placa.toString()+"&idAnticipoGas="+idAntGas;
                    }


                } catch ( Exception e){
                    System.out.println("error en listarAnticipos..."+e.toString()+"__"+e.getMessage());
                    e.printStackTrace();
                    throw new ServletException("Error en AnticipoGasolinaAction en opcion listarAnticipos .....\n"+e.getMessage());
                }
            }

            if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("escogerAnticipoGas"))        {
                //System.out.println("escogerAnticipoGas");
                //System.out.println("antes de antGas");
                //System.out.println("antGas"+idAntGas);
                String tot_preant1=request.getParameter("tot_preant");
                //System.out.println("tot_preant1"+tot_preant1);
                if (tot_preant1==null || tot_preant1.equals("")){tot_preant1="0";}
                double tot_preant=Double.parseDouble(tot_preant1);
                next = "/jsp/gasolina/mostrarElAnticipoGasolina.jsp?idAnticipoGas="+idAntGas+"&conductor=" + conductor+"&placa="+placa+"&loginx="+loginx+"&tot_preant="+tot_preant;

            }

            /*if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("aceptarAnticipoGas"))        {
                //System.out.println("aceptarAnticipoGas");
                String valEfectiv=request.getParameter("valefectivo");
                String valGasolin=request.getParameter("valgasolina");
                String vlrnetoreal=request.getParameter("valor_neto");
                //System.out.println("antes de antGas");
                //System.out.println("antGas"+idAntGas);
                String MostrarPdf="";
                String aceptacion=model.AnticiposGasolinaService.aceptarAnticipoGasolina(idAntGas,loginx,valEfectiv,valGasolin,vlrnetoreal);
                if (aceptacion.equals("Anticipo de Estaci�n de Gasolina Aceptado.")){
                    String idAnticipoGasolinaAceptado=model.AnticiposGasolinaService.getIdAnticipoGasolinaAceptado();

                    valEfectiv=model.AnticiposGasolinaService.getValEfectivoAceptado();
                    valGasolin=model.AnticiposGasolinaService.getValGasolinaAceptado();
                    model.AnticiposGasolinaService.prepararPdfAnticipoGasolina(idAnticipoGasolinaAceptado,valEfectiv,valGasolin,loginx);
                    MostrarPdf="mostrar";
                    //valor=model.AnticiposGasolinaService.getVal();
                    //valor_neto=model.AnticiposGasolinaService.getValNeto();
                }
                //System.out.println("aceptacion"+aceptacion);

                model.AnticiposGasolinaService.isAnticipoGasolina(conductor,placa,loginx) ;
                next = "/jsp/gasolina/mostrarAnticiposGasolina.jsp?conductor=" + conductor+"&placa="+placa+"&idAnticipoGas="+idAntGas+"&respuesta="+aceptacion+"&MostrarPdf="+MostrarPdf;//+"&valor_neto="+valor_neto;//&valor="+valor+"

            }*/

            if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("aceptarAnticipoGas"))        {
                //System.out.println("aceptarAnticipoGas");
                String valEfectiv=request.getParameter("valefectivo");
                String valGasolin=request.getParameter("valgasolina");
                String vlrnetoreal=request.getParameter("valor_neto");
                //System.out.println("antes de antGas");
                //System.out.println("antGas"+idAntGas);
                String MostrarPdf="";
                String aceptacion=model.AnticiposGasolinaService.aceptarAnticipoGasolina(idAntGas,loginx,valEfectiv,valGasolin,vlrnetoreal);
                if (aceptacion.equals("Anticipo de Estaci�n de Gasolina Aceptado.")){
                    String idAnticipoGasolinaAceptado=model.AnticiposGasolinaService.getIdAnticipoGasolinaAceptado();

                    valEfectiv=model.AnticiposGasolinaService.getValEfectivoAceptado();
                    valGasolin=model.AnticiposGasolinaService.getValGasolinaAceptado();
                    model.AnticiposGasolinaService.prepararPdfAnticipoGasolina(idAnticipoGasolinaAceptado,valEfectiv,valGasolin,loginx);
                    MostrarPdf="mostrar";
                    //valor=model.AnticiposGasolinaService.getVal();
                    //valor_neto=model.AnticiposGasolinaService.getValNeto();
                }
                //System.out.println("aceptacion"+aceptacion);

                model.AnticiposGasolinaService.isAnticipoGasolina(conductor,placa,loginx) ;
                next = "/jsp/gasolina/mostrarAnticiposGasolina.jsp?conductor=" + conductor+"&placa="+placa+"&planilla="+planilla+"&idAnticipoGas="+idAntGas+"&respuesta="+aceptacion+"&MostrarPdf="+MostrarPdf;//+"&valor_neto="+valor_neto;//&valor="+valor+"

            }
            ////Fin Modificado Miguel Altamiranda////

            if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("verPdf"))        {
                try{
                    System.out.println("verpdf");
                    next="/pdf/PdfGasolina.pdf";
                }catch(Exception e){
                    System.out.println("errorcillo endoso"+e.toString()+"___"+e.getMessage());
                }
                //System.out.println("fin2 endoso");
            }

            /*if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("facturarAnticipoGas"))        {
                //System.out.println("facturarAnticipoGas");
                String answer1="";
                String[] anticipos  =  request.getParameterValues("idAnticipo");
                List     seleccion  =  model.AnticiposGasolinaService.getSeleccionFacturable( anticipos );

                if( seleccion.size()>0){
                    answer1=model.AnticiposGasolinaService.facturarAnticipos(seleccion,loginx);
                }

                next = "/jsp/gasolina/facturar_anticipos_estacion.jsp?respuesta="+ answer1;
            }*/


             if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("facturarAnticipoGas"))        {

                next = "/jsp/gasolina/facturar_anticipos_estacion.jsp?respuesta=";

                String sql="";
                String[] anticipos  =  request.getParameterValues("idAnticipo");


                String[] idAnticipos = new String[anticipos.length];
                for(int i=0;i<idAnticipos.length;i++){
                    idAnticipos[i] = anticipos[i].split(";_")[0];
                }

                List     seleccion  =  model.AnticiposGasolinaService.getSeleccionFacturable( idAnticipos );

                if( seleccion.size()>0){
                    String documento = request.getParameter("documento");

                    //generar un numero de documento automatico
                    String doc = "";//2010-09-28
                    try {
                        doc = model.AnticiposGasolinaService.codigoCXPAuto("FACTESTA");//2010-09-28
                    }
                    catch (Exception e) {
                        System.out.println("error: "+e.toString());
                        e.printStackTrace();
                    }
                    if(doc!=null && doc.equals("")==false) documento = doc;//2010-09-28

                    sql=model.AnticiposGasolinaService.facturarAnticipos(seleccion,loginx,documento);
                    int estado = 0;
                    //jemartinez
                    //Contiene la informacion de cada anticipo separada por el comodin ;_
                    //item;_descripcion;_estacion;_valor

                    double sumaAnticipos = 0;

                    for(int i=0;i<anticipos.length;i++){
                        sumaAnticipos = sumaAnticipos + Double.parseDouble(anticipos[i].split(";_")[3]);
                    }

                    try{
                        sql = sql + model.AnticiposGasolinaService.setCxpDocService(anticipos[0],documento, loginx, sumaAnticipos)+";";
                        estado++;
                    }catch(Exception e){
                        System.out.println("Action cxp: "+e.toString());

                    }

                    try{
                        sql = sql + model.AnticiposGasolinaService.setCxpItemsService(anticipos, documento,loginx)+";";
                        estado++;
                    }catch(Exception e){
                        System.out.println("Action cxp item: "+e.toString());
                    }

                    if(sql!=null && sql.equals("")==false && estado==2){
                        TransaccionService scv =  new TransaccionService(usuario.getBd());
                        try{
                            scv.crearStatement();
                            String[] listSql = sql.split(";");
                            for(int i=0; i < listSql.length; i++){
                                scv.getSt().addBatch(listSql[i]);
                            }
                            scv.execute();
                            System.out.println("Exito en la operacion. ");
                            next=next+"Operacion Completada con Exito";
                            //Si el usuario que factura es una estacion, generar un .pdf
                            if(model.AnticiposGasolinaService.esEstacionUsuario(loginx)){//2010-09-28
                                //generar el pdf. se usa la variable sumaAnticipos para saber cuanto es en total
                                next = next + this.genPdf(sumaAnticipos, loginx, documento);
                            }
                        }catch(Exception e){
                            next=next+"Error durante la operacion. Verifique el No. de la factura";
                            System.out.println("Error catch 1 en la operacion. "+e.toString());
                        }
                    }else{
                         next=next+"Error: durante la operacion. Verifique el No. de la factura";
                         System.out.println("Error en sino. ");
                    }

                }


            }

            if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("transferirAnticipoGas"))        {
                //System.out.println("transferirAnticipoGas");
                String answer1="";
                String[] anticipos  =  request.getParameterValues("idAnticipo");
                List     seleccion  =  model.AnticiposGasolinaService.getSeleccionTransferible( anticipos );
                String comisionador=request.getParameter("comisionador");

                if( seleccion.size()>0){

                     //inicio de archivo
                     HArchivosTransferenciasGasolina  hilo =  new  HArchivosTransferenciasGasolina();

                     String[] listt=request.getParameterValues("idAnticipo");
                     String[] vcodn= new String[listt.length];//vector con los codigos de los negocios
                     //String vno="";//vector con los valores de la comision
                     String[] bn2=new String[3];//iniciada
                     String bco=((String)request.getParameter("bco"));
                     String[] bn;
                     bn=bco.split(",");
                     for (int j=0;j<listt.length;j++)               {
                        bn2=listt[j].split(",");
                        vcodn[j]=bn2[0];


                     }
                     String codb="";
                     int sw=0;
                     if (bn[0].equals("BANCOLOMBIA")){ codb="07";}
                     else                   {
                        //msj  = "No existen formatos para este banco";
                        sw=1;
                     }
                     if (sw==0)
                     {
                        //msj  = "El archivo para transferir al banco "+  bn[0] +" est� siendo generado....";
                        hilo.start(model, usuario,vcodn,"8020220161", "FINTRA", codb,bn[0],  bn[2], bn[1],comisionador);
                     }
                     hilo.join();
                     if (sw==0){
                        answer1=model.AnticiposGasolinaService.transferirAnticipos(seleccion,loginx,comisionador);
                     }
                }

                next = "/jsp/gasolina/transferir_anticipos_estacion.jsp?respuesta="+ answer1;
            }

            if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("CONSULTAR"))        {
                //System.out.println("consultarAnticipoGas");

                String ckAgencia      =  request.getParameter("ckAgencia");
                String Agencia        =  request.getParameter("Agencia");     //
                String ckPropietario  =  request.getParameter("ckPropietario");
                String Propietario    =  request.getParameter("Propietario");
                String ckPlanilla     =  request.getParameter("ckPlanilla");
                String Planilla       =  request.getParameter("Planilla");
                String ckPlaca        =  request.getParameter("ckPlaca");
                String Placa          =  request.getParameter("Placa");
                String ckConductor    =  request.getParameter("ckConductor");
                String Conductor      =  request.getParameter("Conductor");
                String ckReanticipo   =  request.getParameter("ckReanticipo");
                String reanticipo     =  request.getParameter("reanticipo");
                String ckFechas       =  request.getParameter("ckFechas");
                String fechaIni       =  request.getParameter("fechaInicio");
                String fechaFin       =  request.getParameter("fechaFinal");

                String ckLiquidacion     =  request.getParameter("ckLiquidacion");
                String Liquidacion       =  request.getParameter("Liquidacion");
                String ckTransferencia   =  request.getParameter("ckTransferencia");
                String Transferencia     =  request.getParameter("Transferencia");
                String ckFactura         =  request.getParameter("ckFactura");
                String Factura           =  request.getParameter("Factura");

                model.AnticiposGasolinaService.searchAnticipos("TSP", "802022016", ckAgencia, Agencia, ckPropietario,
                    Propietario, ckPlanilla, Planilla, ckPlaca, Placa, ckConductor, Conductor,  ckReanticipo, reanticipo,
                    ckFechas, fechaIni, fechaFin,  ckLiquidacion, Liquidacion, ckTransferencia, Transferencia, ckFactura, Factura );
                //List listax = model.AnticiposGasolinaService.getLista();
                //String msj="";
                next        = "/jsp/gasolina/consultar_anticipos_estacion.jsp";
                /*if(listax.size()>0){
                   next        = "/jsp/finanzas/fintra/consultar_anticipos_estacion.jsp";
                }else{
                   msj = "No se encontraron registros para dichos parametros de busqueda...";
                }*/



            }

            if (request.getParameter("opcion")!=null && (request.getParameter("opcion").equals("SAVEESTACION")  || request.getParameter("opcion").equals("UPDATEESTACION") )    )      {
                next     = "/jsp/gasolina/creacion_estacion.jsp?msj=";
                msj      = "";
                if(request.getParameter("opcion").equals("SAVEESTACION")){
                   model.AnticiposGasolinaService.loadRequest(request);
                   msj = model.AnticiposGasolinaService.insert();

               }

               if(request.getParameter("opcion").equals("UPDATEESTACION")){
                   model.AnticiposGasolinaService.loadRequest(request);
                   msj = model.AnticiposGasolinaService.update();

               }

	       next= next + msj;

            }

            if(request.getParameter("opcion").equals("LISTAR_ESTACIONES")){
               next     = "/jsp/gasolina/lista_estaciones.jsp?msj=";
               String  nit = request.getParameter("nit");
               //System.out.println("LISTAR_ESTACIONES");
               msj=model.AnticiposGasolinaService.listarEstaciones();
            }


            if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("subirFoto"))        {
              String conductor_foto="",placa_foto="";
              String respuesta="";
              try{
                //System.out.println("subirFoto");

                String aleatorio=customFormat(Util.roundByDecimal(Math.random()*100,0));

                //if (conductor_foto==null){conductor_foto="";}
                String nombre="";
                String valorxx="";

                if (ServletFileUpload.isMultipartContent(request)){

                    ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
                    List fileItemsList = servletFileUpload.parseRequest(request);

                    //// Itero para obtener todos los FileItem
		    Iterator it = fileItemsList.iterator();
                    String nombre_archivo;
                    while (it.hasNext()){
                        FileItem fileItem = (FileItem)it.next();

			//// El FileItem es un campo simple, del tipo nombre-valor
			if (fileItem.isFormField()){
                            nombre = fileItem.getFieldName();
                            valorxx = fileItem.getString();
                            if (nombre.equals("conductor")){
                                conductor_foto=valorxx;//request.getParameter("conductor_foto");
                            }
                            if (nombre.equals("placa")){
                                placa_foto=valorxx;//request.getParameter("conductor_foto");
                            }
                            //System.out.println( "<p>Par�metro:" + nombre + "   Valor:" + valorxx + "</p>");
                            //System.out.println("conductor_foto"+conductor_foto+"placa_foto"+placa_foto);
			}else{//// El FileItem contiene un archivo para upload

                            //// Atributo "name" del elemento input type="file"
                            String nombreCampo = fileItem.getFieldName();
                            //// Tama�o de archivo en bytes
                            long tamanioArchivo = fileItem.getSize();

                            //// Nombre del archivo en el cliente. Algunos navegadores (por ej. IE 6)
                            //// incluyen el path completo, lo que puede implicar separar path
                            //// de nombre.
                            String nombreArchivo = fileItem.getName();

                            //// Content type (tipo MIME) del archivo.
                            //// Esta informaci�n la proporciona el navegador del cliente.
                            //// Algunos ejemplos: .jpg = image/jpeg, .txt = text/plain
                            String contentType = fileItem.getContentType();

                            //// Obtengo caracteristicas de campo y archivo
                            //System.out.println( "<p>--> Name:" + nombreCampo + "</p>");
                            //System.out.println( "<p>--> Tama�o archivo:" + tamanioArchivo + "</p>");
                            //System.out.println( "<p>--> Nombre archivo del cliente:" + nombreArchivo + "</p>");
                            //System.out.println( "<p>--> contentType:" + contentType + "</p>");

                            //// Obtengo extensi�n del archivo de cliente
                            String extension = nombreArchivo.substring(nombreArchivo.indexOf("."));
                            //System.out.println( "<p>--> Extensi�n del archivo:" + extension + "</p>");

                            //// Guardo archivo del cliente en servidor, con un nombre 'fijo' y la
                            //// extensi�n que me manda el cliente
                            //directorioArchivos=rutica;
                            Date ahora=new Date();
                            String ahorax= Util.dateFormat(ahora);
                            soloNombreArchivo="cc"+ conductor_foto +"fec"+ahorax+"usu"+loginx+"x"+aleatorio+""+ extension;
                            model.AnticiposGasolinaService.setNombreNewFotoBd(soloNombreArchivo);
                            nombre_archivo=directorioArchivos + "/cc"+ conductor_foto +"fec"+ahorax+"usu"+loginx+"x"+aleatorio+""+ extension;
                            File archivo = new File(nombre_archivo);

                            fileItem.write(archivo);
                            if ( archivo.exists() ){
                                    //System.out.println( "<p>--> GUARDADO " + archivo.getAbsolutePath() + "</p>");
                                    respuesta="&respuesta=Foto Guardada";
                                    model.AnticiposGasolinaService.setEstado_foto(true);
                                    model.AnticiposGasolinaService.setNombre_archivo(nombre_archivo);
                                    guardarArchivoEnBd(conductor_foto);
                            }else{
                                    //System.out.println( "<p>--> FALLO AL GUARDAR. NO EXISTE " + archivo.getAbsolutePath() + "</p>");
                                    respuesta="&respuesta=Foto Rechazada";
                                    model.AnticiposGasolinaService.setEstado_foto(false);
                            }

                        }	//// FIN: es un archivo para upload

                    }	//// FIN: iteraci�n de FileItems
                }
              }catch(Exception e){
                System.out.println("e"+e.toString()+"__"+e.getMessage());
                respuesta="&respuesta=Problema al cargar...";
              }
              //System.out.println("conductor_foto"+conductor_foto+"placa_foto"+placa_foto);
              next = "/jsp/gasolina/mostrarAnticiposGasolina.jsp?conductor=" + conductor_foto+"&placa="+placa_foto+""+respuesta;
                //condImg=getImagenConductor( conductor_foto, loginx);
                //if (condImg!=null){next=next+"&cond="+condImg;}
                //firma =  getImagenFirma( conductor_foto, loginx)            ;
                //if (firma!=null){next=next+"&firma="+firma;}

            }

            if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("INIT"))        {
                 //model.CuentaBancoSvc.reset();
                 //model.CuentaBancoSvc.loadTipoCuentas();
                 //model.CuentaBancoSvc.loadBancos();
                 //System.out.println("init");
                 String nit       = reset( request.getParameter("nit")       );
                 String tel        = reset( request.getParameter("tel")  );
                 String dir     = reset( request.getParameter("dir")     );
                 String id  = reset( request.getParameter("id")  );
                 String tipo_id   = reset( request.getParameter("tipo_id")   );
                 String nombre    = reset( request.getParameter("nombre")    );
                 String e_mail     = reset( request.getParameter("e_mail")     );
                 String descuento   = reset( request.getParameter("descuento")   );
                 String c_paiso    = reset( request.getParameter("c_paiso")    );
                 String c_origen    = reset( request.getParameter("c_origen")    );

                 next        = "/jsp/gasolina/creacion_estacion.jsp?nit="+ nit +"&tel="+ tel +"&dir="+ dir  +"&id="+id +"&tipo_id="+ tipo_id +"&nombre="+ nombre +"&e_mail="+ e_mail +"&descuento="+ descuento +"&c_paiso="+ c_paiso +"&c_origen="+c_origen+"&msj=";
             }

             if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("CONSULTARPERIODO"))        {
                //System.out.println("consultarAnticipoGas");

                String ckAgencia      =  null;
                String Agencia        =  loginx;     //
                String ckPropietario  =  null;
                String Propietario    =  null;
                String ckPlanilla     =  null;
                String Planilla       =  null;
                String ckPlaca        =  null;
                String Placa          =  "marca de estacion como agencia";
                String ckConductor    =  null;
                String Conductor      =  null;
                String ckReanticipo   =  null;
                String reanticipo     =  null;
                String ckFechas       =  "1";
                String fechaIni       =  request.getParameter("fechaInicio");
                String fechaFin       =  request.getParameter("fechaFinal");

                String ckLiquidacion     =  null;
                String Liquidacion       =  null;
                String ckTransferencia   =  null;
                String Transferencia     =  null;
                String ckFactura         =  null;
                String Factura           =  null;


                String menu = request.getParameter("menu");
                //Esta linea valida que se haya escogido la opcion consultar anticipos propios y no consultar anticipos de
                //estaciones. Cuando se consultan anticipos de estaciones es posible que no se hayan aprobados dichos anticipos
                //y esto generaria una inconsistencia al momento de imprimir el pdf
                if(menu!=null && menu.equals("consultar_propio")){
                    model.AnticiposGasolinaService.searchAnticipos("TSP", "802022016", ckAgencia, Agencia, ckPropietario,
                    Propietario, ckPlanilla, Planilla, ckPlaca, Placa, ckConductor, Conductor,  ckReanticipo, reanticipo,
                    ckFechas, fechaIni, fechaFin,  ckLiquidacion, Liquidacion, ckTransferencia, Transferencia, ckFactura, Factura );
                    //List listax = model.AnticiposGasolinaService.getLista();
                    //String msj="";
                    next        = "/jsp/gasolina/consultar_anticipos_estacion.jsp?menu="+menu;
                    //System.out.println("Entro por el si");
                }else{
                    next        = "/jsp/gasolina/consultar_anticipos_estacion.jsp";
                    //System.out.println("Entro por el sino");
                }

            }

            if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("agregarPreanticipo"))        {
                //System.out.println("agregarPreanticipo");

                String cedulin       =  request.getParameter("conductor");
                String valorcin       =  request.getParameter("valorcillo");

                String[] preanticipox=new String[2];
                preanticipox[0]=cedulin;
                preanticipox[1]=valorcin;

                preanticipos=model.AnticiposGasolinaService.getPreanticipos();
                preanticipos.add(preanticipox);
                model.AnticiposGasolinaService.setPreanticipos(preanticipos);
                next        = "/jsp/gasolina/preanticipo.jsp?primeravez=no";
                /*if(listax.size()>0){
                   next        = "/jsp/finanzas/fintra/consultar_anticipos_estacion.jsp";
                }else{
                   msj = "No se encontraron registros para dichos parametros de busqueda...";
                }*/


            }


            if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("aceptarPreanticipos"))        {
                preanticipos=model.AnticiposGasolinaService.getPreanticipos();
                String msg="";
                if (model.AnticiposGasolinaService.aceptarPreanticipos(preanticipos,loginx)){
                    //System.out.println("ok");
                    msg ="ok";
                }else{
                    //System.out.println("oops");
                    msg="oops";
                }
                model.AnticiposGasolinaService.reiniciarArregloPreanticipos();
                //
                /*
                if (msg.equals("ok")){
                    model.preanticipoService.generarPdf(preanticipos,loginx);
                    next="/pdf/rotu"+model.preanticipoService.getN()+".pdf";
                }else{
                    next        = "/jsp/gasolina/preanticipo.jsp?mensaje="+msg;
                }*/
                next        = "/jsp/gasolina/preanticipo.jsp?mensaje="+msg;

            }

              if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("login_descuento"))        {
                  //System.out.println("login_descuento");
                  String login_usuario=request.getParameter("login");
                  next=    "/jsp/gasolina/usuario_descuento.jsp";
                  String respuesta_login_descuento="";
                  String descuento_login=request.getParameter("descuento");
                  if (login_usuario==null){login_usuario="";}
                  if (model.AnticiposGasolinaService.isLogin(login_usuario)){
                      //System.out.println("existe el login");
                      respuesta_login_descuento=model.AnticiposGasolinaService.asignarLoginDescuento(login_usuario,descuento_login,loginx);
                      next=next+"?loginValido="+respuesta_login_descuento;
                  }else{
                        next=next+"?loginValido=no";
                  }
              }


            if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("CONSULTARPREANTICIPOS"))        {
              //System.out.println("consultarAnticipoGas");

              String fechaIni       =  request.getParameter("fechaInicio");
              String fechaFin       =  request.getParameter("fechaFinal");

              model.AnticiposGasolinaService.searchPreAnticipos(fechaIni, fechaFin,loginx );

              next        = "/jsp/gasolina/consultar_preanticipos.jsp";

            }

            //jemartinez
            //Opcion para exportar el documento a pdf
            if(request.getParameter("opcion")!=null && request.getParameter("opcion").equals("exportarPdf")){

                String menu = request.getParameter("menu");
                String vlrnetoreal=request.getParameter("valor_neto");
                idAntGas = request.getParameter("idAntGas");
                //System.out.println("El id es : "+idAntGas);
                //La siguiente linea obtiene el valor de combustible y el valor en efectivo de idAntgas
                String valores = model.AnticiposGasolinaService.obtenerValoresAnticiposService(idAntGas);
                valores.split(";_");

                String valEfectiv = valores.split(";_")[1];
                String valGasolin = valores.split(";_")[0];
                String cedula_conductor = valores.split(";_")[2];

                System.out.println("Desde el action cedula: "+cedula_conductor);
                //System.out.println("string "+valores);
                System.out.println("Desde el action efectivo: "+valEfectiv+" gasolina: "+valGasolin);
                //String aceptacion=model.AnticiposGasolinaService.aceptarAnticipoGasolina(idAntGas,loginx,valEfectiv,valGasolin,vlrnetoreal);

                String cond= model.AnticiposGasolinaService.getImagenConductor( cedula_conductor, loginx, "023", "031","1" );
                System.out.println("imagenes: "+cond);

                model.AnticiposGasolinaService.setNombre_archivo(cond);
                model.AnticiposGasolinaService.prepararPdfAnticipoGasolina(idAntGas,valEfectiv,valGasolin,loginx);
                System.out.println("despues de preparar");
                String MostrarPdf="mostrar";

                model.AnticiposGasolinaService.isAnticipoGasolina(conductor,placa,loginx) ;
                next = "/jsp/gasolina/consultar_anticipos_estacion.jsp?MostrarPdf="+MostrarPdf+"&menu="+menu;

            }

            if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("CONSULTARPREANTICIPOSANULABLES"))        {
              //System.out.println("consultarAnticipoGas");

              String fechaIni       =  request.getParameter("fechaInicio");
              String fechaFin       =  request.getParameter("fechaFinal");

              model.AnticiposGasolinaService.searchPreAnticiposAnulables(fechaIni, fechaFin,loginx );

              next        = "/jsp/gasolina/consultar_preanticipos_anulables.jsp";

            }

            if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("ANULARPREANTICIPOS"))        {
              //System.out.println("consultarAnticipoGas");
              String[] anticiposAanular =request.getParameterValues("idPreanticipo");
              String respuesta_anulacion= model.AnticiposGasolinaService.AnularPreAnticipos(anticiposAanular );

              next        = "/jsp/gasolina/consultar_preanticipos_anulables.jsp?respuesta="+respuesta_anulacion;

            }

            if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("IMPRIMIRPREANTICIPOS"))        {
              //System.out.println("consultarAnticipoGas");
              String[] anticiposAimprimir =request.getParameterValues("idPreanticipo");
              //String respuesta_impresion= model.AnticiposGasolinaService.AnularPreAnticipos(anticiposAimprimir );
              //System.out.println("anticiposAimprimir"+anticiposAimprimir[0]);
              ArrayList preanticiposimprimibles=model.preanticipoService.getPreanticiposImprimibles(anticiposAimprimir);
              //if (respuesta_impresion.equals("ok")){
                  model.preanticipoService.generarPdf(preanticiposimprimibles,loginx);
                  next="/pdf/rotu"+model.preanticipoService.getN()+".pdf";
              //}else{
                  //next        = "/jsp/gasolina/consultar_preanticipos.jsp?respuesta="+respuesta_impresion;
              //}


            }

            try{
                rd = application.getRequestDispatcher(next);
                if(rd == null)
                    throw new ServletException("No se pudo encontrar "+ next);
                rd.forward(request, response);
            } catch ( Exception e){
                System.out.println("error en run de action..."+e.toString()+"__"+e.getMessage());
                e.printStackTrace();
                throw new ServletException("Error en AnticipoGasolinaAction en  run.....\n"+e.getMessage());
           }

        }catch(Exception ee){
            System.out.println("eee"+ee.toString()+ee.getMessage());
        }

    }

    public static String customFormat(double value) {
        DecimalFormat df = (DecimalFormat) NumberFormat.getNumberInstance(new Locale("en", "US"));
        df.applyPattern("#,##0");
        df.setMaximumFractionDigits(0);
        df.setMinimumFractionDigits(0);
        String respuesta=df.format(value);
        //respuesta=respuesta.substring(0,respuesta.length()-3);
        return respuesta;
    }

    public void guardarArchivoEnBd(String conduc){
        try{
            //variables:  soloNombreArchivo,model.AnticiposGasolinaService.getNombre_archivo(),directorioArchivos,
            //conductor,loginx
            //System.out.println("guardarArchivoEnBd");
            List archivos   = new LinkedList();
            //System.out.println("soloNombreArchivo"+soloNombreArchivo);
            archivos.add(""+soloNombreArchivo);
            //System.out.println("model.AnticiposGasolinaService.getNombre_archivo()"+model.AnticiposGasolinaService.getNombre_archivo());
            model.ImagenSvc.setImagenes(archivos);

            Dictionary fields   = new Hashtable();
            fields.put("actividad",     "023" );
            fields.put("tipoDocumento", "031"   );
            fields.put("documento",     conduc);
            fields.put("agencia",       "OP"   );
            fields.put("usuario",       loginx);

            String ruta               = directorioArchivos;//
            //System.out.println("ruta"+ruta);
            File  file                = new File( ruta );

            String comentario="";

            if(file.exists()){
               //System.out.println("existe");
               File []arc =  file.listFiles();
               for (int i=0;i< arc.length;i++){
                   //System.out.println("in for"+arc[i].getAbsolutePath()+"__"+arc[i].getName()+"__"+arc[i].getPath());
                     if( ! arc[i].isDirectory()){
                           //System.out.println("no dir");
                           String name  = arc[i].getName();
                           //System.out.println("name"+name);
                           if(  model.ImagenSvc.isLoad( model.ImagenSvc.getImagenes(), name)   && model.ImagenSvc.ext(name) ){
                                  //System.out.println("isload_"+file+"_"+name);
                                  FileInputStream       in  = new FileInputStream(file  +"/"+  name);
                                  //System.out.println("in");
                                  ByteArrayOutputStream out = new ByteArrayOutputStream();
                                  ByteArrayInputStream  bfin;
                                  //System.out.println("antes de read");
                                  int input                 = in.read();
                                  //System.out.println("despues de read");
                                  byte[] savedFile          = null;
                                  while(input!=-1){
                                      //System.out.println("while");
                                      out.write(input);
                                      input  = in.read();
                                      //System.out.println("fin de while");
                                  }
                                  //System.out.println("antes de close");
                                  out.close();
                                  //System.out.println("despues de close");
                                  savedFile  = out.toByteArray();
                                  //System.out.println("saved");
                                  bfin       = new ByteArrayInputStream( savedFile );
                                  //System.out.println("bfin ya");
                                  fields.put("imagen", name );
                                  //System.out.println("antes de comentario");
                              // Insertar imagen
                                 comentario += this.insertImagen(bfin, savedFile.length ,  fields);
                                 //System.out.println("insertada");
                           }
                     }
                }
            }

            model.ImagenSvc.reset();

        }catch(Exception ee){
            System.out.println("eee"+ee.toString()+"_"+ee.getMessage());
        }
    }

    public String insertImagen(ByteArrayInputStream bfin, int longitud,  Dictionary fields) throws ServletException{
        String estado   = "";
        String fileName = (String)fields.get("imagen");
        //System.out.println("longitud"+longitud+"bfin"+bfin.toString());

        try{

        model.ImagenSvc.setEstado(false);
        model.ImagenSvc.insertImagen(bfin,longitud,fields);
        estado = "Imagen "+ fileName +" ha sido guardada <br>";
        model.ImagenSvc.setEstado(true);

        }catch(Exception e){
         estado =  "No se pudo guardar la imagen "+  fileName  +"<br>" + e.getMessage() ;
        }
        return estado;
  }

  public  String reset(String val){
      if(val==null)
         val="";
      return val;
  }

    /*public String getRutica(){
        return rutica;
    }*/

    /*
    private String getImagenConductor(String conductor,String loginx){
        String respuesta=null;
       try{

            model.ImagenSvc.searchImagen("1","1","1",null,null,null,"003", "032",
                conductor,null,null,null,null,loginx);//88156794
            List lista = model.ImagenSvc.getImagenes();
            Imagen img;
            if (lista!=null && lista.size()>0){
                img = (Imagen) lista.get(0);
                respuesta = loginx+"/"+img.getNameImagen();
            }
            img=null;
            lista=null;
        }catch(Exception e ){
            System.out.println("e"+e.toString());
        }
        return respuesta;
    }*/
    /*
    private String getImagenFirma(String conductor,String loginx){
        String respuesta=null;
        try{

            model.ImagenSvc.searchImagen("1","1","1",null,null,null,"003", "039",
                conductor,null,null,null,null,loginx);
            List lista = model.ImagenSvc.getImagenes();
            Imagen img;
            if (lista!=null){
                img = (Imagen) lista.get(0);
                respuesta = loginx+"/"+img.getNameImagen();
            }
            img=null;
            lista=null;

        }catch(Exception e ){
            System.out.println("e"+e.toString());
        }
        return respuesta;
    }*/

  /**
     * Crea un objeto de tipo Document
     * @return Objeto creado
     */
    private Document createDoc() {
        Document doc = new Document(PageSize.A4,25,25,35,30);
        return doc;
    }

    /**
     * Genera la ruta en que se guardara el archivo
     * @param user usuario que genera el archivo
     * @param cons Numero de la cxp generada
     * @param extension La extension del archivo
     * @return String con la ruta en la que queda el archivo
     * @throws Exception cuando ocurre algun error
     */
    private String directorioArchivo(String user,String cons,String extension) throws Exception{
        String ruta = "";
        try {
            ResourceBundle rsb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rsb.getString("ruta") + "/exportar/migracion/" + user;
            SimpleDateFormat fmt = new SimpleDateFormat("yyyMMdd_hhmmss");
            File archivo = new File( ruta );
            if (!archivo.exists()) archivo.mkdirs();
            ruta = ruta + "/" + "cuenta_cobro_" + cons +"_" + fmt.format( new Date() ) +"."+extension;
        }
        catch (Exception e) {
            throw new Exception("Error al generar el directorio: "+e.toString());
        }
        return ruta;
    }

    /**
     * Devuelve el nombre del mes del a�o que se le pasa
     * @param mes Numero del mes que se quiere transformar
     * @return Cadena con el nombre del numero de mes
     */
    private String mesToString(int mes){
        String texto = "";
        switch (mes) {
            case 1:
                texto = "Enero";
            break;
            case 2:
                texto = "Febrero";
            break;
            case 3:
                texto = "Marzo";
            break;
            case 4:
                texto = "Abril";
            break;
            case 5:
                texto = "Mayo";
            break;
            case 6:
                texto = "Junio";
            break;
            case 7:
                texto = "Julio";
            break;
            case 8:
                texto = "Agosto";
            break;
            case 9:
                texto = "Septiembre";
            break;
            case 10:
                texto = "Octubre";
            break;
            case 11:
                texto = "Noviembre";
            break;
            case 12:
                texto = "Diciembre";
            break;
            default:
                texto = "Enero";
            break;
        }
        return texto;
    }

    /**
     * Genera una cuenta de cobro de la estacion en formato .pdf. Guarda el archivo en el directorio de archivos del usuario
     * @param total Total de la cuenta
     * @param login_usuario Usuario que genera
     * @param consec Codigo de la cxp
     * @return Mensaje de generacion correcta o incorrecta del archivo
     * @throws Exception Cuando hay error
     */
    public String genPdf(double total,String login_usuario,String consec) throws Exception{
        String ruta = "";
        String msg = ".Pdf generado, revise su directorio de archivos.";
        try {
            ruta = this.directorioArchivo(login_usuario, consec, "pdf");
            BeanGeneral bean = null;
            bean = model.AnticiposGasolinaService.datosEstacion(login_usuario);
            Font fuente = new Font(Font.HELVETICA,10,Font.NORMAL,new java.awt.Color(0,0,0));
            Font fuenteG = new Font(Font.HELVETICA,10,Font.BOLD,new java.awt.Color(0,0,0));
            PdfPCell celda = null;
            Document documento = null;
            documento = this.createDoc();
            PdfWriter.getInstance(documento, new FileOutputStream( ruta ));
            documento.open();
            documento.newPage();
            documento.add(new Paragraph(new Phrase(bean.getValor_08(),fuenteG)));
            documento.add(new Paragraph(new Phrase(bean.getValor_07(),fuenteG)));
            documento.add(new Paragraph(new Phrase(" ",fuente)));
            documento.add(new Paragraph(new Phrase(bean.getValor_03()+" , "+bean.getValor_06()+". Tel: "+bean.getValor_02(),fuente)));
            documento.add(new Paragraph(new Phrase("_____________________________________________"
                    + "_____________________________________________________",fuente)));
            documento.add(new Paragraph(new Phrase(" ",fuente)));
            PdfPTable thead = new PdfPTable(1);
            celda = new PdfPCell();
            celda.setBorder(0);
            celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            celda.setPhrase(new Paragraph(new Phrase(" ",fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase("Cuenta de cobro No.: "+consec,fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ",fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ",fuente)));
            thead.addCell(celda);
            documento.add(thead);
            //row2
            thead = new PdfPTable(1);
            Calendar calendario = Calendar.getInstance();
            String mes = "";
            mes = this.mesToString(calendario.get(Calendar.MONTH) + 1);
            celda.setPhrase(new Paragraph(new Phrase("Ciudad y fecha : "+bean.getValor_06()+", "+mes+" "+calendario.get(Calendar.DAY_OF_MONTH)+" de "+calendario.get(Calendar.YEAR),fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ",fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ",fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ",fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ",fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ",fuente)));
            thead.addCell(celda);
            documento.add(thead);
            //
            thead = new PdfPTable(1);
            celda.setPhrase(new Paragraph(new Phrase("FINTRA S.A.",fuenteG)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase("",fuente)));
            thead.addCell(celda);
            documento.add(thead);
            //
            thead = new PdfPTable(1);
            celda.setPhrase(new Paragraph(new Phrase("Nit: 802022016-1",fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ",fuente)));
            thead.addCell(celda);
            documento.add(thead);
            //
            thead = new PdfPTable(1);
            celda.setPhrase(new Paragraph(new Phrase("Debe a",fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ",fuente)));
            thead.addCell(celda);
            documento.add(thead);
            //
            thead = new PdfPTable(1);
            celda.setPhrase(new Paragraph(new Phrase(bean.getValor_08(),fuenteG)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase("",fuente)));
            thead.addCell(celda);
            documento.add(thead);
            //
            thead = new PdfPTable(1);
            celda.setPhrase(new Paragraph(new Phrase("Nit: "+bean.getValor_01(),fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ",fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ",fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ",fuente)));
            thead.addCell(celda);
            celda.setPhrase(new Paragraph(new Phrase(" ",fuente)));
            thead.addCell(celda);
            documento.add(thead);
            //
            RMCantidadEnLetras c = new RMCantidadEnLetras();
            String[] a;
            a = c.getTexto(total);
            String res = "";
            for(int i=0;i<a.length;i++){
                res = res + ((String)a[i]).replace("-"," ");
            }
            res=res.trim();
            documento.add(new Paragraph(new Phrase("La suma de "+res+" pesos ($ "+Util.customFormat(total)+") por concepto"
                    + " de reembolso por entrega de anticipos de viaje a conductores TRANSPORTES SANCHEZ POLO.",fuente)));
            //
            documento.add(new Paragraph(new Phrase(" ",fuente)));
            documento.add(new Paragraph(new Phrase(" ",fuente)));
            documento.add(new Paragraph(new Phrase(" ",fuente)));
            documento.add(new Paragraph(new Phrase(" ",fuente)));
            documento.add(new Paragraph(new Phrase("Atentamente,",fuente)));
            documento.add(new Paragraph(new Phrase(" ",fuente)));
            documento.add(new Paragraph(new Phrase(" ",fuente)));
            documento.add(new Paragraph(new Phrase(" ",fuente)));
            documento.add(new Paragraph(new Phrase("__________________________________",fuente)));
            documento.add(new Paragraph(new Phrase(bean.getValor_05(),fuente)));
            documento.add(new Paragraph(new Phrase("Representante legal",fuente)));
            documento.add(new Paragraph(new Phrase("Nit: "+bean.getValor_01(),fuente)));
            documento.close();
        }
        catch (Exception e) {
            msg = ".No se genero el pdf.";
            throw new Exception("error al generar el pdf: "+e.toString());
        }
        return msg;
    }

}
