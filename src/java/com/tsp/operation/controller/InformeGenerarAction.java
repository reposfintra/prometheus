/*
 * InformeGenerarAction.java
 *
 * Created on 1 de abril de 2005, 03:20 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class InformeGenerarAction extends Action {
    
    /** Creates a new instance of InformeGenerarAction */
    public InformeGenerarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/informes/InformeDespacho.jsp";
        String fechai = request.getParameter("fechai");
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        try{
            
            model.planillaService.llenarClientes(fechai,"", usuario.getBase());
            request.setAttribute("lista", model.planillaService.getClientes());
            Informe inf = new Informe ();
            inf.setFechai(fechai);
            inf.setFechaf("");
            model.planillaService.setInforme(inf);
            
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
    
}
