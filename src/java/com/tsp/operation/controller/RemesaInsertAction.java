/*
 * RemesaInsertAction.java
 *
 * Created on 15 de diciembre de 2004, 02:00 PM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  KREALES
 */
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;

public class RemesaInsertAction extends Action{
    static Logger logger = Logger.getLogger(RemesaInsertAction.class);
    private String nombreR;
    private String nombreD;
    /** Creates a new instance of RemesaInsertAction */
    public RemesaInsertAction() {
    }
    
    public void buscarNombres(String remitente, String destinatario)throws ServletException {
        try{
            String remitentes[]= remitente.split(",");
            for(int i=0; i<remitentes.length; i++){
                nombreR=model.remidestService.getNombre(remitentes[i])+",";
            }
            
            String destinatarios[]= destinatario.split(",");
            for(int i=0; i<destinatarios.length; i++){
                nombreD=model.remidestService.getNombre(destinatarios[i])+",";
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
    }
    
    public void run() throws ServletException, InformationException {
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        float pesoreal= Float.parseFloat(request.getParameter("cfacturar"));
        String remitentes[]= request.getParameter("remitentes").split(",");
        String destinatarios[]= request.getParameter("destinatarios").split(",");
        String destinatario=request.getParameter("destinatarios");
        String remitente=request.getParameter("remitentes");
        String origen="";
        String destino="";
        String unit_of_work="";
        String moneda="";
        String numre="";
        String numpla=request.getParameter("planilla").toUpperCase();
        String cf_code="";
        String descripcion="";
        String docinterno=request.getParameter("docinterno");
        float costoRem=0;
        float costoUnit=0;
        String facturable = "S";
        String cdocking = request.getParameter("cdock")!=null?"Y":"N";
        if(request.getParameter("facturable")!=null){
            facturable = "N";
        }
        String next="/colpapel/agregarRemesa.jsp";
        String cadena = request.getParameter("cadena")!=null?"S":"N";
        try{
            model.tService.crearStatement();
            float cantreal = 0;
            if(request.getParameter("cantreal")!=null)
                try{
                    cantreal = Float.parseFloat(request.getParameter("cantreal"));
                }catch (Exception ex){
                    cantreal=0;
                }
            
         /*   //HACEMOS UNA RUTINA PARA OBTENER TODOS LOS DOCUMENTOS INTERNOS DEL DESTINATARIO Y AGREGARRLOS A UNA SOLA VBLE.
            if(request.getParameter("docudest")!=null){
                if(!request.getParameter("docudest").equals("a")){
                    String docdest[] = request.getParameter("docudest").split(",");
                    for(int k =0; k<docdest.length;k++){
                        String cod[]= docdest[k].split("=");
                        docinterno = docinterno + cod[0] +",";
                    }
                }
            }*/
            
            String tipoCarga="";
            
            Remesa rem = new Remesa();
            String wo_type="NA";
            String aduana="N";
            String account_code_i="";
            String account_code_c="";
            String pagador="";
            model.stdjobdetselService.buscaStandard(request.getParameter("standard"));
            if(model.stdjobdetselService.getStandardDetSel()!=null){
                Stdjobdetsel stdjobdetsel = model.stdjobdetselService.getStandardDetSel();
                destino=stdjobdetsel.getDestination_code();
                descripcion= stdjobdetsel.getSj_desc();
                origen= stdjobdetsel.getOrigin_code();
                unit_of_work= stdjobdetsel.getUnit_of_work();
                moneda=stdjobdetsel.getCurrency();
                costoRem=stdjobdetsel.getVlr_freight()*pesoreal;
                costoUnit = stdjobdetsel.getVlr_freight();
                wo_type = stdjobdetsel.getWoType();
                tipoCarga = stdjobdetsel.getTipoCarga();
                if(wo_type.equals("RM")||wo_type.equals("RC")||wo_type.equals("RE")){
                    aduana = "S";
                }
                account_code_i=stdjobdetsel.getAccount_code_i();
                account_code_c=stdjobdetsel.getAccount_code_c();
                pagador = stdjobdetsel.getPagador()!=null?stdjobdetsel.getPagador():"";
            }
            model.seriesService.obtenerSerie( "002", usuario);
            if(model.seriesService.getSerie()!=null){
                model.tService.crearStatement();
                Series serie = model.seriesService.getSerie();
                model.tService.getSt().addBatch(model.seriesService.asignarSerie(serie,usuario));
                model.tService.execute();
                numre=serie.getPrefix()+serie.getLast_number();
                
            }
            request.setAttribute("numrem",numre);
            
            //AGREGO DOCUMENTOS RELACIONADOS CON LA REMESA
            String insertDocto=model.RemDocSvc.INSERTDOC(docinterno, usuario.getDstrct(), numre, usuario.getLogin());
            if( !insertDocto.equals("") ){
                String inserts[]=insertDocto.split(";");
                for(int i = 0; i<inserts.length;i++){
                    model.tService.getSt().addBatch(inserts[i]);
                }
            }
            
            Vector doc = model.RemDocSvc.getDocumentos();
            if( doc != null ){
                String insertDocRel=model.RemDocSvc.INSERTDOCREL2(doc, usuario.getDstrct(), numre, destinatario, usuario.getLogin());
                String  inserts2[]=insertDocRel.split(";");
                for(int j = 0; j<inserts2.length;j++){
                    model.tService.getSt().addBatch(inserts2[j]);
                }
            }
            
            // SE BUSCA EL CMC DEL CLIENTE
            model.clienteService.searchCliente("000"+request.getParameter("standard").substring(0,3));
            Cliente cli =null;
            String cmcCli="";
            if(model.clienteService.getCliente()!=null){
                cli = model.clienteService.getCliente();
                cmcCli = cli.getCmc();
            }
            rem.setAduana(aduana);
            rem.setAgcRem(usuario.getId_agencia());
            rem.setCia(usuario.getDstrct());
            rem.setCliente(request.getParameter("standard").substring(0,3));
            rem.setDesRem(destino);
            rem.setRemitente(remitente);
            rem.setNumRem(numre);
            rem.setOriRem(origen);
            rem.setStdJobNo(request.getParameter("standard"));
            rem.setTipoViaje(wo_type);
            rem.setUnidcam(moneda);
            rem.setUnitOfWork(unit_of_work);
            rem.setUsuario(usuario.getLogin());
            rem.setVlrRem(costoRem);
            rem.setPesoReal(pesoreal);
            rem.setDocInterno(docinterno);
            rem.setRemitente(remitente);
            rem.setDestinatario(destinatario);
            rem.setObservacion(request.getParameter("observacion"));
            rem.setDescripcion(descripcion);
            rem.setUnidcam("");
            rem.setRemision("");
            rem.setFaccial(request.getParameter("fcial"));
            rem.setCantreal(cantreal);
            rem.setUnidad(request.getParameter("unidad"));
            rem.setFacturable(facturable);
            rem.setCurrency(moneda);
            rem.setVlr_pesos((float)com.tsp.util.Util.redondear(costoRem,0));
            rem.setCrossdocking(cdocking);
            rem.setTipoCarga(tipoCarga);
            rem.setCadena(cadena);
            rem.setPagador(pagador.equals("")?"000"+request.getParameter("standard").substring(0,3):pagador);
            rem.setCmc(cmcCli);
            rem.setQty_value(costoUnit);
            if(!moneda.equals("PES")){
                Date hoy = new Date();
                java.text.SimpleDateFormat s = new java.text.SimpleDateFormat("yyyy-MM-dd");
                String fecha_cambio = s.format(hoy);
                double vlrrem2= model.tasaService.buscarValor("PES",moneda, fecha_cambio, costoRem);
                rem.setVlr_pesos((float)com.tsp.util.Util.redondear(vlrrem2,0));
            }
            //Agrego la Remesa.
            model.tService.getSt().addBatch(model.remesaService.insertRemesa(rem,usuario.getBase()));
            for(int i=0; i<remitentes.length; i++){
                RemesaDest rd= new RemesaDest();
                rd.setCodigo(remitentes[i]);
                rd.setDstrct(usuario.getDstrct());
                rd.setNumrem(numre);
                rd.setTipo("RE");
                rd.setUsuario(usuario.getLogin());
                model.tService.getSt().addBatch(model.rdService.insertRemesa(rd,usuario.getBase()));
            }
            for(int i=0; i<destinatarios.length; i++){
                RemesaDest rd= new RemesaDest();
                rd.setCodigo(destinatarios[i]);
                rd.setDstrct(usuario.getDstrct());
                rd.setNumrem(numre);
                rd.setTipo("DE");
                rd.setUsuario(usuario.getLogin());
                model.tService.getSt().addBatch(model.rdService.insertRemesa(rd,usuario.getBase()));
            }
            //ESTO LO HACEMOS SI HAY Q AGREGAR LA REMESA A UNA PLANILLA.
            if(!numpla.equals("")){
                List plaerror = new LinkedList();
                List plaagre = new LinkedList();
                if(cadena.equals("S")){
                    model.tService.getSt().addBatch(model.rmtService.marcarCadena(numpla));
                }
                if(session.getAttribute("plaerror")!=null){
                    plaerror= (List)session.getAttribute("plaerror");
                    Iterator it=plaerror.iterator();
                    while (it.hasNext()){
                        Planilla plani = (Planilla) it.next();
                        if(!model.remplaService.estaRempla(plani.getNumpla(),numre)){
                            
                            RemPla rempla= new RemPla();
                            rempla.setPlanilla(plani.getNumpla());
                            rempla.setRemesa(numre);
                            rempla.setDstrct(usuario.getDstrct());
                            rempla.setPorcent(Integer.parseInt(request.getParameter("porcent")));
                            rempla.setAccount_code_c(account_code_c);
                            rempla.setAccount_code_i(account_code_i);
                            
                            //Agrego la Remesa-Planilla
                            model.tService.getSt().addBatch(model.remplaService.insertRemesa(rempla,usuario.getBase()));
                            plaagre.add(plani);
                        }
                        
                    }
                    String nombreR="";
                    for(int i=0; i<remitentes.length; i++){
                        nombreR=model.remidestService.getNombre(remitentes[i])+",";
                    }
                    String nombreD="";
                    for(int i=0; i<destinatarios.length; i++){
                        nombreD=model.remidestService.getNombre(destinatarios[i])+",";
                    }
                    
                    rem.setRemitente(nombreR);
                    rem.setDestinatario(nombreD);
                    request.setAttribute("remesa",rem);
                    request.setAttribute("planillas",plaagre);
                    next="/colpapel/mostrarRemesa.jsp";
                }
                else{
                    RemPla rempla= new RemPla();
                    rempla.setPlanilla(numpla);
                    rempla.setRemesa(numre);
                    rempla.setDstrct(usuario.getDstrct());
                    rempla.setPorcent(Integer.parseInt(request.getParameter("porcent")));
                    rempla.setAccount_code_c(account_code_c);
                    rempla.setAccount_code_i(account_code_i);
                    
                    //Agrego la Remesa-Planilla
                    model.tService.getSt().addBatch(model.remplaService.insertRemesa(rempla,usuario.getBase()));
                }
                
                
                List lremesas = new LinkedList();
                List remesas = model.planillaService.buscarRemesas(numpla);
                Iterator it=remesas.iterator();
                while (it.hasNext()){
                    rem = (Remesa) it.next();
                    this.buscarNombres(rem.getRemitente(), rem.getDestinatario());
                    rem.setRemitente(nombreR);
                    rem.setDestinatario(nombreD);
                    lremesas.add(rem);
                }
                model.remesaService.setRemesasList(lremesas);
                
                //AQUI ACTUALIZO LOS PORCENTAJES DE LAS REMESAS EN PLAREM.
                /*double valortotalR = costoRem;
                List list = model.planillaService.buscarRemesas(numpla);
               // list.add(rem);
                Iterator itRem = list.iterator();
                while(itRem.hasNext()){
                    rem = (Remesa) itRem.next();
                    ////System.out.println("Remesa "+rem.getNumrem());
                    valortotalR  =valortotalR+ rem.getVlr_pesos();
                }
                ////System.out.println("Valor total de remesas "+valortotalR);
                 
                //AHORA LE ASIGNO A CADA UNA EL PROCENTAJE
                itRem = list.iterator();
                while(itRem.hasNext()){
                    rem = (Remesa) itRem.next();
                    model.remesaService.buscaRemesa(rem.getNumrem());
                    Remesa r = model.remesaService.getRemesa();
                 
                    ////System.out.println("Valor de la remesa  "+rem.getNumrem()+" : "+r.getVlr_pesos());
                 
                    double porcentaje = (r.getVlr_pesos()/valortotalR) *100;
                 
                    ////System.out.println("Porcentaje de la remesa "+rem.getNumrem()+" : "+porcentaje);
                 
                    model.remplaService.buscaRemPla(numpla,r.getNumrem());
                    if(model.remplaService.getRemPla()!=null){
                        RemPla rp = model.remplaService.getRemPla();
                        ////System.out.println("Actualizo la remesa numero "+rp.getRemesa());
                        rp.setPorcent((float)porcentaje);
                        model.remplaService.updateRemPla(rp);
                 
                    }
                }*/
                
                //AQUI ACTUALIZO LOS PORCENTAJES DE LAS REMESAS EN PLAREM.
                java.util.Enumeration enum1;
                String parametro;
                enum1 = request.getParameterNames(); // Leemos todos los atributos del request
                int cant = 0;
                while (enum1.hasMoreElements()) {
                    parametro = (String) enum1.nextElement();
                    if(parametro.indexOf("por")==0){
                        cant = Integer.parseInt(request.getParameter(parametro))+cant;
                        String reme = parametro.substring(3);
                        model.remplaService.buscaRemPla(numpla,reme);
                        if(model.remplaService.getRemPla()!=null){
                            RemPla rp = model.remplaService.getRemPla();
                            rp.setPorcent(Float.parseFloat(request.getParameter(parametro)));
                            model.tService.getSt().addBatch(model.remplaService.updateRemPla(rp));
                            
                        }
                    }
                }
                
                //ESTO SE HACE CUANDO SE INTENTO ANULAR UNA REMESA RELACIONADA A UNA PLANILLA
                // Y HAY Q REEMPLAZAR LA REMESA POR UNA NUEVA,.
                //EN ESTE CASO YA CREAMOS LA NUEVA REMESA, AHOR HAY Q ANULARLA.
                if(request.getParameter("anular")!=null && !request.getParameter("anular").equals("") ){
                    
                    String remanu = request.getParameter("anular");
                    int cantPla  = 1;
                    List planillas= model.remesaService.buscarPlanillas(remanu);
                    it=planillas.iterator();
                    while (it.hasNext()){
                        Planilla plan = (Planilla) it.next();
                        String numpla2 = plan.getNumpla();
                        if(!numpla2.equals(numpla)){
                            cantPla++;
                        }
                        
                        
                    }
                    
                    if(cantPla ==1){
                        next= "/colpapel/cerrarARemesa.jsp";
                        
                        
                        logger.info("ANULAR LA REMESA "+remanu);
                        model.remesaService.buscaRemesa(remanu);
                        if(model.remesaService.getRemesa()!=null){
                            logger.info("Se encontro la remesa");
                            Remesa remAn =model.remesaService.getRemesa();
                            remAn.setDistrito(usuario.getDstrct());
                            remAn.setUsuario(usuario.getLogin());
                            remAn.setBase(usuario.getBase());
                            model.tService.getSt().addBatch(model.remesaService.anularRemesa(remAn));
                        }
                        model.remplaService.buscaRemPla(numpla,remanu);
                        if(model.remplaService.getRemPla()!=null){
                            
                            RemPla rempla= model.remplaService.getRemPla();
                            model.tService.getSt().addBatch(model.remplaService.anularPlanilla(rempla));
                            
                        }
                    }
                }
                
                /*if(request.getParameter("ruta")!=null && !request.getParameter("ruta").equals("0")){
                    //AHORA CREO UN NUEVO MOVIMIENTO PARA AJUSTAR EL VIEJO FLETE AL NUEVO FLETE...
                    //BUSCO LA PLANILLA
                 
                 
                    model.planillaService.bucarColpapel(numpla);
                    ////System.out.println("Numpla "+numpla);
                    Planilla pla=model.planillaService.getPlanilla();
                 
                    if(pla!=null){
                 
                        //Busco el nuevo valor de la planilla
                        ////System.out.println("Encontre la planilla");
                        String unit_transp ="";
                        float valorPlaN = 0;
                        float valorPlaV = 0;
                        float unit_cost = 0;
                        float viejoFlete=0;
                        float cantDesp = Float.parseFloat(request.getParameter("toneladas"));
                        boolean  justificar =false;
                        model.stdjobcostoService.buscarStandardJobCostoFull(request.getParameter("standard"),request.getParameter("valorpla"));
                        if(model.stdjobcostoService.getStandardCosto()!=null){
                            ////System.out.println("Ok encontre el std job");
                            Stdjobcosto stdjobcosto = model.stdjobcostoService.getStandardCosto();
                            unit_cost=stdjobcosto.getUnit_cost();
                            unit_transp = stdjobcosto.getUnit_transp();
                            viejoFlete = stdjobcosto.getUnit_cost();
                            if(!request.getParameter("otro").equals("NO")&&!request.getParameter("otro").equals("")){
                                unit_cost = Float.parseFloat(request.getParameter("otro"));
                                if(unit_cost>stdjobcosto.getUnit_cost() && request.getParameter("autorizado").equals("no")){
                                    justificar=true;
                                }
                            }
                            valorPlaN =stdjobcosto.getUnit_cost()*cantDesp;
                        }
                        //MODIFICO LA PLANILLA
                        pla.setPesoreal(cantDesp);
                        pla.setUnit_transp(unit_transp);
                        pla.setUnit_cost(unit_cost);
                        pla.setGroup_code("");
                        model.tService.getSt().addBatch(model.planillaService.updatePlanilla(pla));
                 
                 
                        float totalAjustes = model.movplaService.totalAjuste(numpla);
                        valorPlaV = pla.getVlrpla()+totalAjustes;
                        ////System.out.println("Valor viejo "+valorPlaV);
                        ////System.out.println("Valor nuevo "+valorPlaN);
                        float Nflete = valorPlaN - valorPlaV;
                        ////System.out.println("Diferencia de la planilla "+Nflete);
                        Movpla movpla= new Movpla();
                 
                        if(Nflete!=0){
                            movpla.setAgency_id(usuario.getId_agencia());
                            movpla.setCurrency(pla.getMoneda());
                            movpla.setDstrct(usuario.getDstrct());
                            movpla.setPla_owner(pla.getNitpro());
                            movpla.setPlanilla(numpla);
                            movpla.setSupplier(pla.getPlaveh());
                            movpla.setVlr(Nflete);
                            movpla.setDocument_type("001");
                            movpla.setConcept_code("09");
                            movpla.setAp_ind("V");
                            movpla.setProveedor("8901031611");
                            movpla.setCreation_user(usuario.getLogin());
                            movpla.setVlr_for(0);
                            movpla.setSucursal("");
                            movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+11));
                            model.tService.getSt().addBatch(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                        }
                 
                        //AGREGAMOS LOS EXTRAFLETES
                        Vector efletes= model.sjextrafleteService.getFletes();
                        if(efletes!=null){
                            for(int i=0; i<efletes.size();i++){
                                SJExtraflete sjE = (SJExtraflete)efletes.elementAt(i);
                                if(sjE.isSelec()){
                                    movpla= new Movpla();
                                    movpla.setAgency_id(usuario.getId_agencia());
                                    movpla.setCurrency(pla.getMoneda());
                                    movpla.setDstrct(usuario.getDstrct());
                                    movpla.setPla_owner(pla.getNitpro());
                                    movpla.setPlanilla(numpla);
                                    movpla.setSupplier(pla.getPlaveh());
                                    movpla.setVlr(sjE.getVlrtotal());
                                    movpla.setDocument_type("001");
                                    movpla.setConcept_code(sjE.getConcepto());
                                    movpla.setAp_ind("N");
                                    movpla.setProveedor("");
                                    movpla.setSucursal("");
                                    movpla.setCreation_user(usuario.getLogin());
                                    movpla.setCantidad(sjE.getCantidad());
                                    movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+12+i));
                                    model.tService.getSt().addBatch(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                 
                                }
                            }
                        }
                        //AGREGO EL NUEVO ANTICIPO, SI ES NECESARIO.
                        if(request.getParameter("anticipo")!=null){
                            //SE ANULA EL ANTICIPO ANTERIOR
                            model.movplaService.buscarAnticipos(numpla);
                            Vector anticipos = model.movplaService.getLista();
                            for(int l=0; l<anticipos.size();l++){
                                Movpla mp =(Movpla) anticipos.elementAt(l);
                                if(mp.getVlr()>0){
                                    if("".equals(mp.getUser_anul())){
                                        mp.setCreation_user(usuario.getLogin());
                                        model.movplaService.setMovPla(mp);
                                        model.movplaService.anularChequeNoImpreso();
                                    }
                                }
                            }
                            //SE AGREGA EL NUEVO ANTICIPO
                            String banco_cuenta =request.getParameter("banco");
                            String banvec[] = banco_cuenta.split("/");
                            String banco=banvec[0];
                            String cuenta=banvec[1];
                            float anticipo = Float.parseFloat(request.getParameter("anticipo"));
                 
                            movpla= new Movpla();
                            movpla.setAgency_id(usuario.getId_agencia());
                            movpla.setCurrency(request.getParameter("moneda"));
                            movpla.setDstrct(usuario.getDstrct());
                            movpla.setPla_owner(pla.getNitpro());
                            movpla.setPlanilla(numpla);
                            movpla.setSupplier(pla.getPlaveh());
                            movpla.setVlr(anticipo);
                            movpla.setDocument_type("001");
                            movpla.setConcept_code("01");
                            movpla.setAp_ind("S");
                            movpla.setProveedor("890103161");
                            movpla.setCreation_user(usuario.getLogin());
                            movpla.setVlr_for(0);
                            movpla.setSucursal("");
                            movpla.setBanco(banco);
                            movpla.setCuenta(cuenta);
                            movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+11));
                            model.tService.getSt().addBatch(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                 
                        }
                    }
                 
                 
                 
                }*/
            }
            
            model.tService.execute();
            
            Planilla plani = new Planilla();
            plani.setNumpla(numpla);
            plani.setClientes(model.planillaService.buscarClientes(plani.getNumpla()));
            model.planillaService.setPlanilla(plani);
            model.planillaService.actualizarTrafico();
            
            model.planillaService.bucarColpapel(numpla);
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
