/*
 * ValidarDocumentosDespachoAction.java
 *
 * Created on 3 de noviembre de 2006, 03:04 PM
 */

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.util.*;

/**
 *
 * @author  EQUIPO13
 */
public class ValidarDocumentosDespachoAction extends Action{
    
    /** Creates a new instance of ValidarDocumentosDespachoAction */
    public ValidarDocumentosDespachoAction () { }
    
    public void run () throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        
        String numrem  = request.getParameter ("numrem").toUpperCase ();
        String tipo    =   (request.getParameter("tipo")!=null)?request.getParameter("tipo"):"";
        String tipodoc = request.getParameter ("c_tdoc");
        String Opcion  = (request.getParameter("Opcion")!=null)?request.getParameter("Opcion"):"";
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String distrito = (String) session.getAttribute ("Distrito");
        String next="/jsp/masivo/Documentos_Despacho/ListaRemesas.jsp?numrem="+numrem+"&Opcion="+Opcion+"&tipo="+tipo;
        
        //System.out.println("- numrem: "+numrem);
        //System.out.println("- tipo: "+tipo);
        //System.out.println("- tipodoc: "+tipodoc);
        //System.out.println("- Opcion: "+Opcion);
        
        try {

            if ( tipodoc.equals ("001") ) {
                
                model.planillaService.consultaPlanillaRemesa ( numrem );
                
            } else {
                
                next = "/controller?estado=remesa_docto&accion=Manager&Opcion="+Opcion+"&tipo="+tipo+"&c_tdoc="+tipodoc+"&numrem="+numrem;
            
            }

        } catch ( SQLException e ) {
            
            e.printStackTrace();
            throw new ServletException ( e.getMessage () );
            
        }
        
        this.dispatchRequest ( next );
        
    }
    
}