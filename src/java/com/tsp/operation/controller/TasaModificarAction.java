/*
 * TasaModificarAction.java
 * 
 * Created on 4 de julio de 2005, 12:33 PM
 */
package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  DIOGENES
 */
public class TasaModificarAction extends Action {
    
    /** Creates a new instance of TasaCargarAction */
    public TasaModificarAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException {
        String mensaje = "";
        String next = "/jsp/trafico/tasa/Tasa.jsp?mensaje=MsgModificado&reload=ok&sw=1";
        String moneda1 = (request.getParameter("c_combom1").toUpperCase());
        String moneda2 = (request.getParameter("c_combom2").toUpperCase());
        String fec = (request.getParameter("c_fecha").toUpperCase());
        float conver = Float.parseFloat(request.getParameter("c_conver"));
        float compra = Float.parseFloat(request.getParameter("c_compra"));
        float venta = Float.parseFloat(request.getParameter("c_venta"));
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");        
        int sw = 0;
        try{                        
            Tasa tasa = new Tasa();
            tasa.setMoneda1(moneda1);
            tasa.setMoneda2(moneda2);
            tasa.setFecha(fec );
            tasa.setVlr_conver(conver);
            tasa.setCompra(compra);
            tasa.setVenta(venta);
            tasa.setCia(usuario.getCia());
            tasa.setUser_update(usuario.getLogin());
            tasa.setBase(usuario.getBase());
            model.tasaService.modificarTasa(tasa);
        }
        catch (SQLException e){
               sw =1;
        }
        if(sw == 1){
            request.setAttribute("sw","0");                    
            next = "/jsp/trafico/tasa/Tasa.jsp?mensaje=MsgErrorT&reload=ok&sw=1";                     
       }
         
         // Redireccionar a la p�gina indicada.
       this.dispatchRequest(next);
    
    }
    
}
