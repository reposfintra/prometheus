 /*
 * TraficoObtenerPlacaAction.java
 *
 * Created on 11 de noviembre de 2005, 04:56 PM
 */

package com.tsp.operation.controller;

import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.exceptions.*;
import com.tsp.util.Util.*;

/**
 *
 * @author  sescalante
 */
public class TraficoObtenerPlacaAction extends Action{
    
    /** Creates a new instance of TraficoObtenerPlacaAction */
    public TraficoObtenerPlacaAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String placa = "";
        try{
            placa = model.planillaService.getPlacaPlanilla(request.getParameter("planilla"));
        }catch( Exception e){throw new ServletException (e.getMessage());}
        this.dispatchRequest("/jsp/trafico/controltrafico/red1.jsp?cz="+request.getParameter("planilla")+"&op=55&placa="+placa);
    }
}
