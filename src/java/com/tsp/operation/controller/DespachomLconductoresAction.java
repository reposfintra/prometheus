/*
 * DespachoManualLconductoresAction.java
 *
 * Created on 21 de noviembre de 2005, 12:31 AM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;;
/**
 *
 * @author  David A
 */
public class DespachomLconductoresAction extends Action {
    
    /** Creates a new instance of DespachoManualLconductoresAction */
    public DespachomLconductoresAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next="";
        try {
            String conductor = ""+request.getParameter("c_conductor");
            model.conductorService.obtConductoresPorNombre(conductor);
            next="/jsp/trafico/despacho_manual/conductor.jsp?accion=1";
            
        }catch (Exception e) {
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
    
}
