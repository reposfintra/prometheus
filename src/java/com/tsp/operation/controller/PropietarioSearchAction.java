/*
 * PropietarioSearchAction.java
 *
 * Created on 15 de junio de 2005, 12:02 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  Sandrameg
 */
public class PropietarioSearchAction extends Action {
    
    /** Creates a new instance of PropietarioSearchAction */
    public PropietarioSearchAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/propietarios/propietarioSearch.jsp";        
        String cedula = request.getParameter("ced").toUpperCase();
        
        try{
            model.propService.buscarxCedula(cedula);
            
            if(model.propService.getPropietario() != null){
                request.setAttribute("prop", model.propService.getPropietario());
                next = next + "?msg=exitoB";            
            }
            else {                
                next = next + "?msg=errorB&c=" + cedula;
                ////System.out.println("ERROR " + next );
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}
