/*
 * ProveedorAnticipoInsertAction.java
 *
 * Created on 22 de abril de 2005, 08:41 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  kreales
 */
public class ProveedorAnticipoInsertAction extends Action{
    
    /** Creates a new instance of ProveedorAnticipoInsertAction */
    public ProveedorAnticipoInsertAction() {
    }
    
    public boolean estaProv(String nit){
        
        boolean sw=false;
        java.util.Enumeration enum1;
        String parametro;
        enum1 = request.getParameterNames(); // Leemos todos los atributos del request
        
        while (enum1.hasMoreElements()) {
            parametro = (String) enum1.nextElement();
            if(parametro.equalsIgnoreCase(nit)){
                sw=true;
            }
        }
        
        return sw;
    }
    
    public void run() throws ServletException, InformationException {
        
        String next ="/anticipos/proveedorAnticipoInsert.jsp";
        String codant = request.getParameter("ant");
        HttpSession session = request.getSession();
        Usuario usuario     = (Usuario) session.getAttribute("Usuario");
        
        try{
            //PRIMERO INSERTO LOS ANTICIPOS - PROVEEDORES
            java.util.Enumeration enum1;
            String parametro;
            enum1 = request.getParameterNames(); // Leemos todos los atributos del request
            
            while (enum1.hasMoreElements()) {
                parametro = (String) enum1.nextElement();
                String vec[]= parametro.split("/");
                String nit= vec[0];
                String sucursal="";
                if(vec.length>1){
                    sucursal = vec[1];
                }
                model.proveedoresService.searchProveedor(nit, sucursal);
                if(model.proveedoresService.getProveedor()!=null){
                    if(!model.proveedoresService.estaAnticipoProvee(nit,sucursal,codant)){
                        Proveedores prov = model.proveedoresService.getProveedor();
                        prov.setAnticipo(codant);
                        prov.setCreation_user(usuario.getLogin());
                        model.anticiposService.setProveedores(prov);
                        model.anticiposService.insertProveedorAnticipos(usuario.getBase());
                    }
                }
            }
            
            model.proveedoresService.listaAnticipoProvee(codant);
            Vector vec = model.proveedoresService.getProveedores();
            for(int i=0; i<vec.size(); i++){
                Proveedores prov = (Proveedores) vec.elementAt(i);
                String nit = prov.getNit()+"/"+prov.getSucursal();
                if(!estaProv(nit)){
                    model.proveedoresService.deleteAnticipoProvee(codant, prov.getNit(), prov.getSucursal());
                }
                
            }
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        
        
        this.dispatchRequest(next);
    }
    
}
