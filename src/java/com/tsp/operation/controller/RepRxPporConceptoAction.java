/*
 * RepRxPporConceptoAction.java
 *
 * Created on 26 de marzo de 2007, 19:24
 */

package com.tsp.operation.controller;


import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.exceptions.*;
import com.tsp.util.Util;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.*;

/**
 *
 * @author  Operario
 */
public class RepRxPporConceptoAction extends Action{
    
    Logger logger = Logger.getLogger(this.getClass());
    
    /** Creates a new instance of RepRxPporConceptoAction */
    public RepRxPporConceptoAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next  = "/jsp/cxpagar/reportes/repRxPConcepto.jsp?msg=Se ha iniciado la generaci�n del reporte en Ms Excel. Por favor consulte el Log de Procesos para verificar su estado.";
        
        //Usuario en sesi�n
        HttpSession session = request.getSession();
        Usuario user = (Usuario) session.getAttribute("Usuario");
        String dstrct = (String) session.getAttribute("Distrito");  
        
        String nit = request.getParameter("nit")!=null ? request.getParameter("nit") : "";
        String concepto = request.getParameter("concept");
        String fechai =  request.getParameter("FechaI");
        String fechaf =  request.getParameter("FechaF");
        String pendiente = request.getParameter("pendiente");
        
        try {
            model.factrecurrService.cargarConceptos(user.getDstrct());
            request.setAttribute("conceptos", model.factrecurrService.getTreemap());
            
            model.factrecurrService.consultarRxPConcepto(dstrct, nit, fechai, fechaf, concepto, pendiente);
            Vector info = model.factrecurrService.getVecCxp_doc();
            ListadoRxPporConceptoTh hilo = new ListadoRxPporConceptoTh();
            hilo.start(model, info, user, fechai, fechaf);
            
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
}
