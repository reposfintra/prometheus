/**
* Autor : Ing. Roberto Rocha P..
* Date  : 10 de Julio de 2007
* Copyrigth Notice : Fintravalores S.A. S.A
* Version 1.0
-->
<%--
-@(#)
--Descripcion : Action que maneja los avales de Fenalco
**/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.threads.*;
import com.tsp.util.Util;
import com.tsp.operation.model.beans.Usuario;
import javax.servlet.http.*;


public class ConsultarAvalesAction  extends Action{
    
   //protected HttpServletRequest request;
    public ConsultarAvalesAction () {
    }
    
    public void run() throws ServletException, InformationException
    {
       String op = (String) request.getParameter("op");
       String next="";
       Usuario usuario = null;
       HttpSession session  = request.getSession();
       usuario              = (Usuario) session.getAttribute("Usuario"); 
       next="/jsp/fenalco/avales/listado_avales.jsp?opc=1";
       if (op.equals("3"))
       {
           String np = (String) request.getParameter("proveedor");
           String nc = "";
           try{
               model.Avales.listado(nc,np);
            }catch (Exception e){
                throw new ServletException(e.getMessage());
            }
           next="/jsp/fenalco/avales/listado_avales2.jsp?opc=2";
       }
       else
       {
           String nid = (String) request.getParameter("c_nit");
           String user=usuario.getCedula();
           try{
               model.Negociossvc.actaux();
            }catch (Exception e){
                throw new ServletException(e.getMessage());
            }
       }
       this.dispatchRequest(next);
   
    }
}
    
