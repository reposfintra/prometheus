/********************************************************************
 *      Nombre Clase.................   DocumentoAplicacionActualizarAction.java    
 *      Descripci�n..................   Obtiene los documentos de una aplicaci�n.
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class DocumentoAplicacionActualizarAction extends Action{
        
        /** Creates a new instance of DocumentoInsertAction */
        public DocumentoAplicacionActualizarAction() {
        }
        
        public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
                //Pr�xima vista
                String pag  = "/jsp/masivo/tblapl/DocAppInsert.jsp?mensaje=MsgUpdate";
                String next = "";                
                
                String app = request.getParameter("app_selec");
                
                //Usuario en sesi�n
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario) session.getAttribute("Usuario");
                String distrito = (String) session.getAttribute("Distrito");

                try{                           
                        model.apl_docSvc.setAppselected(app);                        
                        model.apl_docSvc.GenerarJSCampos(distrito, app);
                        next = com.tsp.util.Util.LLamarVentana(pag, "Asignaci�n de Documentos a Actividad");

                }catch (SQLException e){
                       throw new ServletException(e.getMessage());
                }

                this.dispatchRequest(next);
        }
        
}
