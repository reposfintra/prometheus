/*************************************************************************
 * Nombre ......................InfoactBusactAction.java                 *
 * Descripci�n..................Clase Action para buscar actividad       *
 * Autor........................LREALES                                  *
 * Fecha........................3 de agosto de 2006, 08:02 AM            *
 * Versi�n......................1.0                                      *
 * Coyright.....................Transportes Sanchez Polo S.A.            *
 *************************************************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class InfoactBusactAction extends Action {
    
    /** Creates a new instance of InfoactBusactAction */
    public InfoactBusactAction() {}
    
    public void run() throws ServletException, InformationException {
        
        String next = "/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina");
        String act = request.getParameter("codact");
        String cliente = request.getParameter("cliente");
        String numpla = request.getParameter("numpla");
        String numrem = request.getParameter("numrem");
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String estado = (request.getParameter("est")!= null) ? request.getParameter("est") :"";
        
        try{
            if(!estado.equals("")){
                Planilla pla = new Planilla();
                model.actplanService.buscarInfoplanilla(numpla);
                pla = model.actplanService.getPlan();
                model.clientactSvc.setPla(pla);
                pla.setNumrem(numrem);
                pla.setClientes(request.getParameter("nomcliente"));
                pla.setTipoviaje(request.getParameter("tipo"));
                pla.setNomcliente(cliente);
                model.clientactSvc.setPla(pla);
            }
            
            model.actividadSvc.buscarActividad(act, usuario.getCia());
            model.jerarquia_tiempoService.buscarCausaAct(act);
            model.jerarquia_tiempoService.buscarResponsablaAct(act);
            boolean a = model.infoactService.buscarInfoActividad(act, cliente, usuario.getCia(),numpla,numrem);
            next+="?act="+a;
            if(!a){
                next+="&ult_fec="+model.infoactService.fecha_ult_act_Registrada(cliente, usuario.getCia(), numpla, numrem);
            }
            
        } catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest( next );
        
    }
    
}