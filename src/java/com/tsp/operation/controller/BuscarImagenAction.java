/*
 * BuscarImagenAction.java
 *
 * Created on 19 de octubre de 2005, 03:57 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import com.tsp.exceptions.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;


/**
 *
 * @author  dbastidas
 */
public class BuscarImagenAction extends Action {
    
    /** Creates a new instance of BuscarImagenAction */
    public BuscarImagenAction() {
    }
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        String codact = request.getParameter("actividad");
        String tipodoc = request.getParameter("tipoDocumento");
        String doc = request.getParameter("documento");
        String next="/imagen/VerImagen.jsp";
        //System.out.println("");
        //System.out.println("Actividad "+codact );
        //System.out.println("Tipo "+ tipodoc );
        //System.out.println("Documento "+doc );
        try {
            model.ImagenSvc.searchImagen("1","1","1",null,null,null,codact, tipodoc,
                                               doc,null,null,null,null,usuario.getLogin());
            List lista = model.ImagenSvc.getImagenes();                   
            Imagen img = null;
            String foto ="";
            if (lista==null){
                foto = "";
            } 
            else {
                img = (Imagen) lista.get(0);
                foto = img.getCreation_user()+"/"+img.getNameImagen();
            }
            next = next+"?foto="+foto;
            //System.out.println("Next "+next);
        }catch (Exception e){
               e.printStackTrace();
               throw new ServletException(e.getMessage());
         } 
         this.dispatchRequest(next);
    }
    
}
