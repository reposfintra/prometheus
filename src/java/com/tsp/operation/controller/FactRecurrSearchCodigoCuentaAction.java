


/*
 *  Nombre clase    :  FacturaSearchCodigoCuentaAction.java
 *  Descripcion     :
 *  Autor           : Ing. Juan Manuel Escand�n P�rez
 *  Fecha           : 10 de marzo de 2006, 03:12 PM
 *  Version         : 1.0
 *  Copyright       : Fintravalores S.A.
 */

package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;

public class FactRecurrSearchCodigoCuentaAction extends Action {
    
    /** Crea una nueva instancia de  FacturaSearchCodigoCuentaAction */
    public FactRecurrSearchCodigoCuentaAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        try{
            //ivan 21 julio 2006
            HttpSession session = request.getSession();
            com.tsp.finanzas.contab.model.Model modelcontab = (com.tsp.finanzas.contab.model.Model) session.getAttribute("modelcontab");
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            
            String Modificar =  (request.getParameter("Modificar")!=null)?request.getParameter("Modificar"):"";
            String maxfila                  = request.getParameter("maxfila");
            String numpla                   = (request.getParameter("numpla")!=null)?request.getParameter("numpla"):"";
            String indice                   = (request.getParameter("indice")!=null)?request.getParameter("indice"):"";
            String hidden                   = (request.getParameter("hidden")!=null)?request.getParameter("hidden"):"";
            String cuenta                   = (request.getParameter("cuenta")!=null)?request.getParameter("cuenta"):"";
            String next                     = "";
            String validar  = "";
            String[] codcuenta = {cuenta.substring(0,1),cuenta.substring(1,3),cuenta.substring(3,6),cuenta.substring(6,9), cuenta.substring(9,13)};
            
            
            model.clienteService.searchAccount_Code_C(numpla);
            Cliente cliente = model.clienteService.getCliente();
            validar =""+ request.getParameter("validar");
            
            if(cliente == null){
                String mensaje_error = "La planilla no existe";
                next = "/jsp/cxpagar/facturasrec/facturaP.jsp?ms="+mensaje_error+"&ag="+validar+"&maxfila="+maxfila+"&Modificar="+Modificar;
            }
            else{
                
                
                
                if( !cliente.getUnidad().equals("") ){
                    
                    String  codigo  = cliente.getUnidad();
                    
                    String unidadC  = codigo.substring(3,6);
                    String codcli   = codigo.substring(6,9);
                    System.out.println("CODIGO CLIENTE " + codcli);
                    System.out.println("UNIDAD " + unidadC);
                    if( cuenta.length() > 0 ) {
                        codcuenta[0]  = cuenta.substring(0,1);
                        //codcuenta[1]  = hidden.split(",")[0];
                        codcuenta[2]  = unidadC;
                        codcuenta[3]  = codcli;
                        codcuenta[4]  = cuenta.substring(9,13);
                    }
                    else{
                        codcuenta[2] = unidadC ;
                        codcuenta[3] = codcli;
                    }
                    
                    
                    //+"&agContable="+hidden.split(",")[0] +"&unidadC="+unidadC
                    next = "/jsp/cxpagar/facturasrec/facturaP.jsp?cliente="+codcli+"&op=cargar&indice="+indice+"&ag="+validar+"&maxfila="+maxfila+"&Modificar="+Modificar;
                }
                else{
                    String msg = "No hay informaci�n relacionada con esa planilla ";
                    next = "/jsp/cxpagar/facturasrec/facturaP.jsp?ms="+msg+"&ag="+validar+"&maxfila="+maxfila+"&Modificar="+Modificar;
                }
                
                
            }
            String tipo_documento   =   ""+ request.getParameter("tipo_documento");
            String documento        =   ""+ request.getParameter("documento");
            String proveedor        =   ""+ request.getParameter("proveedor");
            String tipo_documento_rel       =   ""+ request.getParameter("tipo_documento_rel");
            String documento_relacionado    =   ""+ request.getParameter("documento_relacionado");
            String fecha_documento  =   ""+request.getParameter("fecha_documento");
            String banco            =   ""+ request.getParameter("c_banco");
            double vlr_neto         =   model.cxpDocService.getNumero(""+ request.getParameter("vlr_neto"));
            double total            =   model.cxpDocService.getNumero(""+ request.getParameter("total"));
            String moneda_banco     = (request.getParameter("moneda_banco")!=null)?request.getParameter("moneda_banco"):"";
            
            
            //Ivan Dario 28 Octubre 2006
            String agenciaBanco     = (request.getParameter("agenciaBanco")!=null)?request.getParameter("agenciaBanco"):"";
            /////////////////////////////////
            String beneficiario = (request.getParameter("beneficiario")!=null)?request.getParameter("beneficiario"):"";
            String hc = (request.getParameter("hc")!=null)?request.getParameter("hc"):"";
            
            int plazo   =   0;
            int num_items =  0;
            String CabIva           =   (request.getParameter("CabIva")!=null?request.getParameter("CabIva").toUpperCase():"");
            String CabRiva          =   (request.getParameter("CabRiva")!=null?request.getParameter("CabRiva").toUpperCase():"");
            String CabRica          =   (request.getParameter("CabRica")!=null?request.getParameter("CabRica").toUpperCase():"");
            String CabRfte          =   (request.getParameter("CabRfte")!=null?request.getParameter("CabRfte").toUpperCase():"");
            //Ivan Debito
            double saldo_me_anterior = Double.parseDouble((request.getParameter("saldo_me_anterior")!=null)?request.getParameter("saldo_me_anterior"):"0");
            double saldo_anterior    = Double.parseDouble((request.getParameter("saldo_anterior")!=null)?request.getParameter("saldo_anterior"):"0");
            ///////////////////////////////////////////
            
            String fecha_aprobacion  =   (request.getParameter("fecha_aprobacion")!=null?request.getParameter("fecha_aprobacion"):"");
            String usu_ap            =   (request.getParameter("usu_ap")!=null?request.getParameter("usu_ap"):"");
            
            
            try{
                num_items  = Integer.parseInt(""+ request.getParameter("num_items"));
                plazo      = Integer.parseInt(""+ request.getParameter("plazo"));
            }
            catch(java.lang.NumberFormatException e){
                vlr_neto    =   0;
                plazo       =   0;
                num_items   =   1;
            }
            
            Vector vItems = new Vector();
            System.out.println("Numero de Items " + num_items);
            int MaxFi = Integer.parseInt(maxfila);
            for (int i=1;i <= MaxFi; i++){
                CXPItemDoc item = new CXPItemDoc();
                String filaTabla = request.getParameter("valor1"+i);
                if ( filaTabla!=null  ){
                    
                    String descripcion = ""+request.getParameter("desc"+i);
                    String concepto=""+ request.getParameter("descripcion_i"+i);
                    String codigo_cuenta=""+ request.getParameter("codigo_cuenta"+i);
                    String codigo_abc=""+ request.getParameter("codigo_abc"+i).toUpperCase();
                    String planilla=""+ request.getParameter("planilla"+i);
                    String iva       = ""+ request.getParameter("iva"+i);
                    //Ivan 26 julio 2006
                    String ree = ""+ request.getParameter("REE"+i).toUpperCase();
                    String Ref3 = ""+ request.getParameter("Ref3"+i);
                    String Ref4 = ""+ request.getParameter("Ref4"+i);
                    String agenciaItem = ""+ request.getParameter("agencia"+i);
                    
                    String cod1 = request.getParameter("cod1"+i);
                    String cod2 = request.getParameter("cod2"+i);
                    String cod3 = request.getParameter("cod3"+i);
                    String cod4 = request.getParameter("cod4"+i);
                    String cod5 = request.getParameter("cod5"+i);
                    String [] codigos = {cod1,cod2,cod3,cod4,cod5};
                    
                    // Modificacion 21 julio 2006
                    LinkedList tbltipo = null;
                    String auxiliar      = request.getParameter("auxiliar"+i);
                    String tipoSubledger = request.getParameter("tipo"+i);
                    if(modelcontab.planDeCuentasService.existCuenta(usuario.getDstrct(),codigo_cuenta)){
                        modelcontab.subledgerService.busquedaCuentasTipoSubledger(usuario.getDstrct(),codigo_cuenta);
                        tbltipo = modelcontab.subledgerService.getCuentastsubledger();
                    }
                    //////////////////////////////////////////////////////////////////////
                    double valor =0;
                    double vlr_total = 0;
                    
                    String tipcliarea=""+ request.getParameter("cod_oc"+i);
                    String codcliarea ="";
                    if(i != Integer.parseInt(indice)){
                        codcliarea= ""+ request.getParameter("oc"+i);
                    }
                    
                    String descliarea= ""+ request.getParameter("doc"+i);
                    
                    valor     = model.cxpDocService.getNumero(request.getParameter("valor1"+i));
                    vlr_total = model.cxpDocService.getNumero(request.getParameter("valorNeto"+i));
                    
                    if(i == Integer.parseInt(indice)){
                        item.setCodigos(codcuenta);
                        auxiliar = "";
                        tbltipo =null;
                    }else{
                        item.setCodigos(codigos);
                    }
                    
                    item.setRee(ree);
                    item.setRef3(Ref3);
                    item.setRef4(Ref4);
                    item.setAgencia(agenciaItem);
                    
                    item.setConcepto(concepto);
                    item.setDescripcion(descripcion);
                    item.setCodigo_cuenta(codigo_cuenta);
                    item.setCodigo_abc(codigo_abc);
                    item.setPlanilla(planilla);
                    item.setVlr_me(valor);
                    item.setVlr_total(vlr_total);
                    item.setTipcliarea(tipcliarea);
                    item.setCodcliarea(codcliarea);
                    item.setDescliarea(descliarea);
                    item.setIva(iva);
                    
                    // ivan 21 julio 2006
                    item.setAuxiliar(auxiliar);
                    item.setTipo(tbltipo);
                    item.setTipoSubledger(tipoSubledger);
                    /////////////////////////////////
                    
                    
                    Vector vTipoImp= model.TimpuestoSvc.vTiposImpuestos();
                    Vector vImpuestosPorItem= new Vector();
                    for(int x=0;x<vTipoImp.size();x++){
                        CXPImpItem impuestoItem = new CXPImpItem();
                        String cod_impuesto = ""+ request.getParameter("impuesto"+x+""+i).toUpperCase();
                        impuestoItem.setCod_impuesto(cod_impuesto);
                        System.out.println("impuesto"+cod_impuesto);
                        vImpuestosPorItem.add(impuestoItem);
                    }
                    
                    item.setVItems( vImpuestosPorItem);
                    item.setVCopia(vImpuestosPorItem);
                    vItems.add(item);
                }else{
                    vItems.add(item);
                }
            }
            
            model.factrecurrService.setVecRxpItemsDoc(vItems);
            
            String sucursal     =   ""+ request.getParameter("c_sucursal");
            String moneda       =   ""+ request.getParameter("moneda");
            String descripcion  =   ""+ request.getParameter("descripcion");
            String observacion  =   ""+ request.getParameter("observacion");
            String usuario_aprobacion   =   ""+request.getParameter("usuario_aprobacion");
            
            Proveedor o_proveedor = model.proveedorService.obtenerProveedorPorNit(proveedor);
            int b = 0;
            if(o_proveedor != null) {
                b=1;
            }
            
            if (b!=1) {
                proveedor   = " ";
            }
            
            CXP_Doc factura = new CXP_Doc();
            
            //Ivan 21 sep
            factura.setValor_saldo_anterior(saldo_anterior);
            factura.setValor_saldo_me_anterior(saldo_me_anterior);
            ////////////////////////
            
            factura.setTipo_documento(tipo_documento);
            factura.setDocumento(documento);
            factura.setProveedor(proveedor);
            factura.setTipo_documento_rel(tipo_documento_rel);
            factura.setDocumento_relacionado(documento_relacionado);
            factura.setFecha_documento(fecha_documento);
            factura.setBanco(banco);
            factura.setVlr_neto(total);
            factura.setVlr_total(vlr_neto);
            factura.setMoneda(moneda);
            factura.setSucursal(sucursal);
            factura.setDescripcion(descripcion);
            factura.setObservacion(observacion);
            factura.setUsuario_aprobacion(usuario_aprobacion);
            factura.setPlazo(plazo);
            factura.setIva(CabIva);
            factura.setRiva(CabRiva);
            factura.setRica(CabRica);
            factura.setRfte(CabRfte);
            factura.setMoneda_banco(moneda_banco);
            
             //Ivan DArio 28 Octubre 2006
            factura.setAgenciaBanco(agenciaBanco);
            ////////////////////////////////////
            
            factura.setFecha_aprobacion(fecha_aprobacion);
            factura.setAprobador(usu_ap);
            factura.setBeneficiario(beneficiario);
            factura.setHandle_code(hc);
            model.factrecurrService.setFactura(factura);
            
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en FacturaSearchCodigoCuentaAction .....\n"+e.getMessage());
        }
    }
}

