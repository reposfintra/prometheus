/*
 * Nombre        ReporteTiemposPuestosDeControlAction.java
 * Descripci�n   Ejecuta las acciones necesarias para elaborar el reporte de tiempos 
 *               entre puestos de control.
 * Autor         Alejandro Payares
 * Fecha         13 de febrero de 2006, 05:51 PM
 * Version       1.0
 * Coyright      Transportes Sanchez Polo SA.
 */

package com.tsp.operation.controller;

import com.tsp.operation.model.threads.HExportarExcel_TiemposPC;
import com.tsp.operation.model.threads.HExportarExcel_DetallePromedioEntrePCs;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.exceptions.InformationException;


/**
 * Ejecuta las acciones necesarias para elaborar el reporte de tiempos entre puestos de control.
 * @author Alejandro Payares
 */
public class ReporteTiemposPuestosDeControlAction extends Action{
    
    /**
     * Crea una nueva instancia de ReporteTiemposPuestosDeControlAction
     * @autor  Alejandro Payares
     */
    public ReporteTiemposPuestosDeControlAction() {
    }
    
    /**
     * Ejecuta la accion propia de la clase
     * @throws ServletException si algun problema ocurre al enviar la respuesta al cliente
     * @throws InformationException para enviar mensajes informativos
     */    
    public void run() throws javax.servlet.ServletException {
        String cmd = request.getParameter("cmd");
        String next = "/jsp/sot/body/PantallaFiltroReporteTiemposPC.jsp";
        try {
            if ( "show".equals(cmd) ) {
                String fechaIni = request.getParameter("fechaini");
                String fechaFin = request.getParameter("fechafin");
                String formato  = request.getParameter("formatoSalida");
                if ( formato.equals("web") ) {
                    model.repTiemposPCService.buscarDatos(fechaIni, fechaFin);
                    next = "/jsp/sot/reports/ReporteTiemposPC.jsp?inicio="+fechaIni+"&fin="+fechaFin;
                }
                else if ( formato.equals("excel") ) {
                    HExportarExcel_TiemposPC hiloReporte = new HExportarExcel_TiemposPC(model,(Usuario)request.getSession().getAttribute("Usuario"),fechaIni, fechaFin);
                    hiloReporte.generar();
                    next += "?mensaje=El archivo excel est� siendo generado, cuando termine puede consultarlo en el manejador de archivos";
                }
            }
            else if ( "loadPCs".equals(cmd) ) {
                model.ciudadService.searchTreMapCiudades();
                next = "/jsp/sot/body/PantallaFiltroPromedioEntrePCs.jsp";
                model.repTiemposPCService.borrarDatos();
            }
            else if ( "avg".equals(cmd) ) {
                String pcOrigen = request.getParameter("origen");
                String pcDestino = request.getParameter("destino");
                model.repTiemposPCService.calcularTiempoPromedioEntrePuestosDeControl(pcOrigen, pcDestino);
                model.ciudadService.searchTreMapCiudades();
                next = "/jsp/sot/body/PantallaFiltroPromedioEntrePCs.jsp?origen="+pcOrigen+"&destino="+pcDestino+"&nombreOrigen="+request.getParameter("nombreOrigen")+"&nombreDestino="+request.getParameter("nombreDestino");
            }
            else if ( "detalles".equals(cmd) ) {
                String nombreOrigen = request.getParameter("nombreOrigen");
                String nombreDestino = request.getParameter("nombreDestino");
                String formato = request.getParameter("formatoSalida");
                if ( "web".equals(formato) ) {
                    next = "/jsp/sot/reports/ReporteDetallePromedioEntrePCs.jsp?nombreOrigen="+nombreOrigen+"&nombreDestino="+nombreDestino;
                }
                else if ( "excel".equals(formato) ) {
                    HExportarExcel_DetallePromedioEntrePCs hiloReporte = new HExportarExcel_DetallePromedioEntrePCs(model,(Usuario)request.getSession().getAttribute("Usuario"),nombreOrigen,nombreDestino);
                    hiloReporte.generar();
                    next = "/jsp/sot/body/PantallaFiltroPromedioEntrePCs.jsp?mensaje=El archivo excel est� siendo generado, cuando termine puede consultarlo en el manejador de archivos&nombreOrigen=" + nombreOrigen + "&nombreDestino=" + nombreDestino;
                }
            }
        }
        catch( InformationException ex ){
            next = "/jsp/sot/body/PantallaFiltroPromedioEntrePCs.jsp?origen="+request.getParameter("origen")+"&destino="+request.getParameter("destino")+"&mensaje="+ex.getMessage();
        }
        catch( Exception ex ){
            ex.printStackTrace();
            throw new javax.servlet.ServletException(ex.getMessage());
        }
        this.dispatchRequest(next);
        
    }
    
}
