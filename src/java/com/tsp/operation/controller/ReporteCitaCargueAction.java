/**************************************************************************
 * Nombre clase: ReporteDrummondAction.java                                *
 * Descripci�n: Clase Recoge los parametros de la jsp y se los envia a el  *
 * hilo HReporteDrummond.java                                              *
 * Autor: Ing. Ivan DArio Gomez Vanegas                                    *
 * Fecha: Created on 1 de octubre de 2005, 08:20 AM                        *
 * Versi�n: Java 1.0                                                       *
 * Copyright: Fintravalores S.A. S.A.                                 *
 ***************************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.threads.*;
import com.tsp.operation.model.DAOS.*;

/**
 *
 * @author Igomez
 */
public class ReporteCitaCargueAction extends Action{
    
    public ReporteCitaCargueAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        String next="";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String FecIni  = request.getParameter("fechai");
        String FecFin  = request.getParameter("fechaf");
        
        
        try{
            HReporteCitaCargue hilo =  new HReporteCitaCargue();
            hilo.start(FecIni, FecFin, usuario.getLogin());     
            next = "/jsp/masivo/reportes/ReporteCitaCargue.jsp?msg=Su reporte ha iniciado...";
                     
        }catch (Exception ex){
            throw new ServletException("Error en ReporteExportarExcel .....\n"+ex.getMessage());
        }

        this.dispatchRequest(next);  
    }
    
}
