package com.tsp.operation.controller;

import java.util.Vector;
import java.lang.*;
import java.sql.*;
//import com.tsp.operation.model.GenerarReporteDiario;
import javax.servlet.ServletException;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class GenerarReporteDiarioAction extends Action {
    
    public GenerarReporteDiarioAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        Vector agencias = new Vector();
        String agencia;
        String fechaInicio = request.getParameter("fechai");
        String fechaFin = request.getParameter("fechaf");
        String distrito = (request.getParameter("distrito") != null)?request.getParameter("distrito").toUpperCase():"";
        String despachador = (request.getParameter("despachador") != null)?request.getParameter("despachador").toUpperCase():"";
        String plaveh = (request.getParameter("placa") != null)?request.getParameter("placa").toUpperCase():"";
        String cedConductor = (request.getParameter("conductor")!= null)?request.getParameter("conductor"):"";
        String cliente = (request.getParameter("cliente")!= null)?request.getParameter("cliente"):"";
        
        String ags    = (request.getParameter("agencias") != null )?request.getParameter("agencias").toUpperCase():"";
        
        HttpSession Session           = request.getSession();
        Usuario user                  = new Usuario();
        user                          = (Usuario)Session.getAttribute("Usuario");
        
        if( ags.length() > 0 ){
            
            if ( !ags.equals("") ){
                
                if ( !ags.equals("A") ) {
                    
                    ags           = ags.substring(0, ags.length() - 1 );
                    ags = ags.replaceAll(",","','");
                    ags = "'"+ags+"'";
                    
                }
            }
        }
        
        
        
        //int tipo =Integer.parseInt((request.getParameter("tipo")!=null)?request.getParameter("tipo"):"1");
        String opc =    (   request.getParameter("opc")!=null  )?request.getParameter("opc"):"";
        String tipo =   (   request.getParameter("tipo")!=null  )?request.getParameter("tipo"):"";
        
        System.out.println("TIPO " + opc);
        Agencia ag;
        
        try{
            //System.out.println("AGENCIAS " + request.getParameter("agencias") );
            //BUSQUEDA POR AGENCIA
            if( tipo.equals("0") ){
                if( !request.getParameter("agencias").equals("") ){
                    if ( !request.getParameter("agencias").equals("a") ) {
                        String[] datos = request.getParameter("agencias").split(",");
                        for ( int i = 0; i < datos.length; i++ ) {
                            ag = model.agenciaService.obtenerAgencia(datos[i]);
                            agencias.add(ag);
                        }
                    }
                }
            }
            
                        
            //BUSQUEDA POR DESPACHADOR
            else{
                Vector agenciasC = new Vector();
                try{
                    agenciasC = model.agenciaService.obtenerAgenciasdespachador(despachador);
                } catch ( Exception ex ) {
                    ex.printStackTrace();
                }
                for ( int i = 0; i < agenciasC.size(); i++ ) {                
                    ag =model.agenciaService.obtenerAgencia((String)agenciasC.elementAt(i));
                    agencias.add(ag);                                        
                }
                
            }
            
            if( opc.equals("3") ){
                agencias = model.agenciaService.obtenerAgencias(distrito);
            }
            
            
            model.agenciaService.setAgencias(agencias);
            
            if ( ags.equals("A") )
                ags = "";
            model.planillaService.generarReporteDiario( distrito, fechaInicio, fechaFin, plaveh, cedConductor, despachador, ags, cliente, user.getLogin()  );
            
            Session.setAttribute("fechaInicio", fechaInicio );
            Session.setAttribute("fechaFin", fechaFin );
            Session.setAttribute("Informes", model.planillaService.getInformes());
            Session.setAttribute("Agencias", model.agenciaService.getAgencias());
            Session.setAttribute("id", user.getLogin());
            
        }catch(SQLException  e){
            throw new ServletException("Error generando reporte diario "+e.getMessage());
        }
        
        String next = "/agencia/ReporteDD.jsp";
        this.dispatchRequest(next);
    }
    
}
