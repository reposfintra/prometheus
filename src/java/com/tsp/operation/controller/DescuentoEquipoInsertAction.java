/********************************************************************
* Nombre ......................DescuentoEquipoInsertAction.java     *
* Descripci�n..................Clase Action de descuento de equipos *
* Autor........................Armando Oviedo                       *
* Fecha Creaci�n...............06/12/2005                           *
* Modificado por...............LREALES                              *
* Fecha Modificaci�n...........22/05/2006                           *
* Versi�n......................1.0                                  *
* Coyright.....................Transportes Sanchez Polo S.A.        *
********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

public class DescuentoEquipoInsertAction extends Action{
    
    /** Creates a new instance of DescuentoEquipoInsertAction */
    public DescuentoEquipoInsertAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        
        String next="/jsp/equipos/descuentos_equipo/IngresarDescuentoEquipos.jsp" ;
        
        try{
            
            HttpSession session = request.getSession();
            
            String codigo = ( request.getParameter("codigo") != null )?request.getParameter("codigo").toUpperCase():"";
            String descripcion = ( request.getParameter("descripcion") != null )?request.getParameter("descripcion"):"";
            String conc_contable = ( request.getParameter("conc_contable") != null )?request.getParameter("conc_contable"):"";
            String conc_especial = ( request.getParameter("conc_especial") != null )?request.getParameter("conc_especial"):"";
            
            if( codigo != null ){
                
                Usuario usuario = ( Usuario ) session.getAttribute("Usuario");                        
                String base = usuario.getBase().toUpperCase();                
                String distrito = ( String )( session.getAttribute("Distrito") );
                distrito = distrito.toUpperCase();
                String user = usuario.getLogin().toUpperCase();
                
                DescuentoEquipo tmp = new DescuentoEquipo();
                
                tmp.setRegStatus( "" );                
                tmp.setCodigo( codigo );
                tmp.setConcContable( conc_contable );
                tmp.setConcEspecial( conc_especial );                
                tmp.setDescripcion( descripcion );
                tmp.setDstrct( distrito );
                tmp.setCreationUser( user );
                tmp.setCreationDate( "now()" );
                tmp.setUserUpdate( user );
                tmp.setLastUpdate( "now()" );
                tmp.setBase( base );
                
                model.descuentoequiposvc.setDE( tmp );
                
                boolean existe = model.descuentoequiposvc.existeDE();
                
                if( !existe ){
                    
                    model.descuentoequiposvc.addDE();
                    next += "?mensaje=Descuento ingresado correctamente";
                    
                } else{
                    
                    next += "?mensaje=Ya existe un descuento con este c�digo";
                    
                }
                
            }
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new ServletException( e.getMessage () );
            
        }
        
        this.dispatchRequest( next );
        
    }
    
}