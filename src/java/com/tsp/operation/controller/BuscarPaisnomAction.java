/*
 * BuscarPaisnomAction.java
 *
 * Created on 3 de marzo de 2005, 02:07 PM 
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  DIBASMO
 */
public class BuscarPaisnomAction extends Action {
    
    /** Creates a new instance of BuscarPaisnomAction */
    public BuscarPaisnomAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException{
        String next="/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina");
        String nombre = (request.getParameter("nombre").toUpperCase());
        HttpSession session = request.getSession();
        session.setAttribute("nom", nombre);
         // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);

    }
    
}
