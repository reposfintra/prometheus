/*
 * TodosCargarCiudadesAction.java
 *
 * Created on 13 de marzo de 2005, 11:29 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  Administrador
 */
public class TodosCargarCiudadesAction extends Action {
    
    /** Creates a new instance of CompaniaCargarCiudadesAction */
    public TodosCargarCiudadesAction() {
    }
    
    public void run() throws ServletException, InformationException{
        String next = "/" + request.getParameter("carpeta") + "/" + request.getParameter("pagina");
        String estado = request.getParameter("e");
        HttpSession session = request.getSession();        
        
        try{ 
            model.paisservice.buscarpais(request.getParameter("pais"));      
            Pais spais = model.paisservice.obtenerpais(); 
            
            model.estadoservice.buscarestado(spais.getCountry_code(), request.getParameter("est"));      
            Estado sest = model.estadoservice.obtenerestado(); 
            
            next = next + "?cp=" + spais.getCountry_code() + "&np=" +spais.getCountry_name() + "&ce=" + sest.getdepartament_code() + "&ne=" + sest.getdepartament_name();
            
            request.setAttribute("ciudades", "cargados");
            request.setAttribute("msg", "");
            request.setAttribute("estados","");
        }catch (SQLException e){
               throw new ServletException(e.getMessage());
         }
        ////System.out.println("next= " + next);
        this.dispatchRequest(next);
    }
}
