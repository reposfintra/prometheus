/********************************************************************
 *      Nombre Clase.................   FacturaBuscarproveedoresAction.java
 *      Descripci�n..................   Action que se encarga de guradar un los registros de cxp_doc,cxp_imp_doc,cxp_imp_item,cxp_items_doc
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.util.Util;
/**
 *
 * @author  dlamadrid
 */
//logger
import org.apache.log4j.Logger;
/**
 *
 * @author  dlamadrid
 */
public class FactRecurrGuardarAction extends Action {
    
    Logger log = Logger.getLogger(this.getClass());
    
    /** Creates a new instance of FacturaGuardarAction */
    public FactRecurrGuardarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        try {
            String Modificar = (request.getParameter("Modificar")!=null)?request.getParameter("Modificar"):"";
            
            String maxfila = request.getParameter("maxfila");
            java.util.Date utilDate = new java.util.Date(); //fecha actual
            long lnMilisegundos     = utilDate.getTime();
            java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(lnMilisegundos);//Fecha Actual en sql
            int sw  =   0;
            String fechaActual  =   ""+sqlTimestamp;
            HttpSession session = request.getSession();
            
            //Ivan 21 julio 2006
            com.tsp.finanzas.contab.model.Model modelcontab = (com.tsp.finanzas.contab.model.Model) session.getAttribute("modelcontab");
            ////////////////////////////
            
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String dstrct   = usuario.getDstrct();
            String base = usuario.getBase();
            String moneda_usuario = (String)session.getAttribute("Moneda");
            
            String tipo_documento   =   ""+ request.getParameter("tipo_documento");
            String documento        =   ""+ request.getParameter("documento");
            String proveedor        =   ""+ request.getParameter("proveedor");
            String tipo_documento_rel = ""+ request.getParameter("tipo_documento_rel");
            if (tipo_documento_rel.equals("null")) {
                tipo_documento_rel =    "";
            }
            String documento_relacionado =  ""+ request.getParameter("documento_relacionado");
            if (documento_relacionado.equals("null")) {
                documento_relacionado = "";
            }
            String fecha_documento  =   ""+request.getParameter("fecha_documento");
            String banco            =   ""+ request.getParameter("c_banco");
            double vlr_neto          = 0;
            int plazo               = 0;
            double vlr_total         = 0;
            String sucursal         =   ""+ request.getParameter("c_sucursal");
            String moneda           =   ""+ request.getParameter("moneda");
            String descripcion      =   ""+ request.getParameter("descripcion");
            String observacion      =   ""+ request.getParameter("observacion");
            String usuario_aprobacion   =   ""+request.getParameter("usuario_aprobacion");
            String agencia  =  ( request.getParameter("agencia") != null )?request.getParameter("agencia"):"";
            String ni       =  ( request.getParameter("num_items") != null )?request.getParameter("num_items"):"0";
            String validar  =   ""+ request.getParameter("validar");
            String CabIva           =   (request.getParameter("CabIva")!=null?request.getParameter("CabIva"):"");
            String CabRiva          =   (request.getParameter("CabRiva")!=null?request.getParameter("CabRiva"):"");
            String CabRica          =   (request.getParameter("CabRica")!=null?request.getParameter("CabRica"):"");
            String CabRfte          =   (request.getParameter("CabRfte")!=null?request.getParameter("CabRfte"):"");
            int num_items   =  0;
            String moneda_banco     = (request.getParameter("moneda_banco")!=null)?request.getParameter("moneda_banco"):"";
            
            //Ivan Dario 28 Octubre 2006
            String agenciaBanco     = (request.getParameter("agenciaBanco")!=null)?request.getParameter("agenciaBanco"):"";
            /////////////////////////////////
            String beneficiario = (request.getParameter("beneficiario")!=null)?request.getParameter("beneficiario"):"";
            String hc = (request.getParameter("hc")!=null)?request.getParameter("hc"):"";
            
            String referencia = request.getParameter("referencia")!=null? request.getParameter("referencia") : "";//Osvaldo
            String tipo_factura = request.getParameter("tipo_factura")!=null? request.getParameter("tipo_factura") : "";//Osvaldo
            
            /**************************************************************************************************************/
            int num_cuotas = (request.getParameter("ncuotas")!=null && request.getParameter("ncuotas").length()!=0? Integer.parseInt(request.getParameter("ncuotas")) : 0);
            String fecha_inicio_transfer = (request.getParameter("fecha_inicio")!=null?request.getParameter("fecha_inicio"):"");
            String frecuencia = (request.getParameter("CabRfte")!=null?request.getParameter("frecuencia"):"");
            String fecha_inicio_transferO = (request.getParameter("fecha_inicio2")!=null?request.getParameter("fecha_inicio2"):"");
            
            log.info("NCUOTAS: " + num_cuotas);
            log.info("FECHA INICIO TRANSFER: " + fecha_inicio_transfer);
            log.info("FRECUENCIA: " + frecuencia);
            /**************************************************************************************************************/
            
            try {
                num_items   = Integer.parseInt(ni);
                vlr_neto    = model.factrecurrService.getNumero(""+ request.getParameter("vlr_neto") );
                vlr_total   = model.factrecurrService.getNumero(""+ request.getParameter("total"));
                plazo       = Integer.parseInt(""+ request.getParameter("plazo"));
            }
            catch(java.lang.NumberFormatException e) {
                vlr_total   = 0;
                vlr_neto    = 0;
                plazo       = 0;
                num_items   = 1;
            }
            
            
            Proveedor o_proveedor   = model.proveedorService.obtenerProveedorPorNit(proveedor);
            String id_mims          = o_proveedor.getC_idMims();
            //String handle_code      = o_proveedor.getC_hc();
            int b                   = 0;
            String next             = "";
            String mensaje_error    = "";
            
            
            CXP_Doc factura         = new CXP_Doc();
            factura.setDstrct(dstrct);
            factura.setProveedor(proveedor);
            factura.setTipo_documento(tipo_documento);
            factura.setDocumento(documento);
            factura.setDescripcion(descripcion);
            factura.setAgencia(agencia);
            //factura.setHandle_code(handle_code);
            factura.setId_mims(id_mims);
            factura.setFecha_documento(fecha_documento);
            factura.setTipo_documento_rel(tipo_documento_rel);
            factura.setDocumento_relacionado(documento_relacionado);
            factura.setFecha_aprobacion("0099-01-01 00:00:00");
            factura.setUsuario_aprobacion("");
            factura.setAprobador(usuario_aprobacion);
            String fecha_vencimiento = "";
            Util u = new Util();
            fecha_vencimiento = u.fechaFinalYYYYMMDD(fecha_documento,plazo);
            factura.setFecha_vencimiento(fecha_vencimiento);
            factura.setUltima_fecha_pago("0099-01-01 00:00:00");
            factura.setBanco(banco);
            factura.setSucursal(sucursal);
            factura.setMoneda(moneda);
            factura.setMoneda_banco(moneda_banco);
            factura.setReferencia(referencia);//Osvaldo
            factura.setTipo(tipo_factura);//Osvaldo
            
              //Ivan DArio 28 Octubre 2006
            factura.setAgenciaBanco(agenciaBanco);
            ////////////////////////////////////
            factura.setBeneficiario(beneficiario);
            factura.setHandle_code(hc);
            
            double  valor_local = 0;
            double  valor_tasa = 0;
            log.info("Moneda usuario "+ moneda_usuario + "Moneda 1 "+moneda+" Moneda 2 "+moneda_usuario);
            Tasa tasa = model.tasaService.buscarValorTasa(moneda_usuario, moneda, moneda_usuario, fecha_documento);
            if (tasa != null)
                valor_tasa = tasa.getValor_tasa();
            
            /*if( !moneda.equals(moneda_usuario)){
                valor_local =   model.factrecurrService.buscarValor(moneda_usuario , moneda, fecha_documento+"  00:00:00",vlr_total);
                valor_local =   Util.redondear1(valor_local, 2);
            }
            else{
                valor_local = vlr_total;
            }*/
            
            //            factura.setVlr_total(vlr_neto);
            //            //factura.setVlr_neto(valor_local);
            //            factura.setVlr_total_abonos(0);
            //            factura.setVlr_saldo(valor_local);
            //            factura.setVlr_neto_me(vlr_total);
            //            factura.setVlr_total_abonos_me(0);
            //            factura.setVlr_saldo_me(vlr_total);
            //            /*model.tasaService.buscarTasa(moneda_usuario ,moneda, fecha_documento,dstrct);
            //            Tasa o_tasa = model.tasaService.obtenerTasa();
            //            float tasa = 1;
            //            if (o_tasa != null) {
            //                tasa = o_tasa.getVlr_conver();
            //            }*/
            
            factura.setTasa(valor_tasa);
            factura.setUsuario_contabilizo("");
            factura.setFecha_contabilizacion("0099-01-01 00:00:00");
            factura.setUsuario_anulo("");
            factura.setFecha_anulacion("0099-01-01 00:00:00");
            factura.setFecha_contabilizacion_anulacion("0099-01-01 00:00:00");
            factura.setObservacion(observacion);
            factura.setNum_obs_autorizador(0);
            factura.setNum_obs_pagador(0);
            factura.setNum_obs_registra(0);
            factura.setCreation_user(usuario.getLogin());
            factura.setUser_update(usuario.getLogin());
            factura.setBase(base);
            factura.setPlazo(plazo);
            factura.setIva(CabIva);
            factura.setRiva(CabRiva);
            factura.setRica(CabRica);
            factura.setRfte(CabRfte);
            
            /**************************************************************/
            factura.setNum_cuotas(num_cuotas);
            factura.setFecha_inicio_transfer(fecha_inicio_transfer);
            factura.setFrecuencia(frecuencia);
            factura.setFecha_inicio_transferO(fecha_inicio_transferO);
            /**************************************************************/
            model.factrecurrService.setFactura(factura);
            
            Vector vItems = new Vector();
            boolean Error  = false;
            
            
            int MaxFi = Integer.parseInt(maxfila);
            int coditem = 1;
            double totalFac = 0;/**/
            double totalFacLocal = 0;/**/
            for (int i=1;i <= MaxFi ; i++) {
                String filaTabla = request.getParameter("valor1"+i);
                CXPItemDoc item = new CXPItemDoc();
                if ( filaTabla!=null  ){
                    boolean ErrorCuenta = false; 
                    String cod_item         = String.valueOf(coditem);
                    coditem++;
                    String descripcion_i    = (request.getParameter("desc"+i)!=null)?request.getParameter("desc"+i):"";
                    String concepto         = (request.getParameter("descripcion_i"+i)!=null)?request.getParameter("descripcion_i"+i):"";
                    String codigo_cuenta    = ""+ request.getParameter("codigo_cuenta"+i);
                    String codigo_abc       = ""+ request.getParameter("codigo_abc"+i).toUpperCase();
                    String planilla         = ""+ request.getParameter("planilla"+i);
                    //Ivan 26 julio 2006
                    String ree  = ""+ request.getParameter("REE"+i).toUpperCase();
                    String Ref3 = ""+ request.getParameter("Ref3"+i);
                    String Ref4 = ""+ request.getParameter("Ref4"+i);
                    String agenciaItem = ""+ request.getParameter("agencia"+i);
                    ////////////////////////////////////////////////////////////
                    String tipcliarea       = ( request.getParameter("cod_oc"+i) != null )?request.getParameter("cod_oc"+i):"C";
                    String codcliarea       = ( request.getParameter("oc"+i)!= null )?request.getParameter("oc"+i):"";
                    String iva              = ""+ request.getParameter("iva"+i);
                    String cod1 = request.getParameter("cod1"+i);
                    String cod2 = request.getParameter("cod2"+i);
                    String cod3 = request.getParameter("cod3"+i);
                    String cod4 = request.getParameter("cod4"+i);
                    String cod5 = request.getParameter("cod5"+i);
                    String [] codigos = {cod1,cod2,cod3,cod4,cod5};
                    
                    // Modificacion 21 julio 2006
                    boolean Existe      = false;
                    boolean ReqAuxiliar = false;
                    Hashtable datoCuenta;
                    LinkedList tbltipo = null;
                    String auxiliar      = request.getParameter("auxiliar"+i);
                    String tipoSubledger = request.getParameter("tipo"+i);
                   if(modelcontab.planDeCuentasService.existCuenta(usuario.getDstrct(),codigo_cuenta)){
                        if(model.cxpDocService.CuentaModuloCXP(usuario.getDstrct(),codigo_cuenta)){
                            modelcontab.subledgerService.buscarCuentasTipoSubledger(codigo_cuenta);
                            tbltipo = modelcontab.subledgerService.getCuentastsubledger();
                            Existe = true;
                            datoCuenta = model.cxpDocService.buscarCuenta(usuario.getDstrct(),codigo_cuenta);
                            if(datoCuenta !=null){
                                String subledger = (String)datoCuenta.get("subledger");
                                if(subledger.equals("S") && auxiliar.equals("") ){
                                    ReqAuxiliar = true;
                                    Error = true;
                                    mensaje_error+="El auxiliar del item "+ cod_item +" marcado con rojo es obligatorio ";
                                    
                                }else if(subledger.equals("N") && !auxiliar.equals("") ){
                                    ReqAuxiliar = true;
                                    Error = true;
                                    mensaje_error+="El auxiliar del item "+ cod_item +" marcado con rojo no es requerido para la cuenta ";
                                }
                            }else{
                                Error = true;
                                mensaje_error +="El numero de cuenta del item "+ cod_item +" marcado con rojo debe ser de detalle";
                            }
                        }else{
                            Error = true;
                            ErrorCuenta =true;
                            mensaje_error +="El numero de cuenta del item "+ cod_item +" marcado con rojo no pertenece al modulo CXP";
                        }
                        
                    }else{
                        Error = true;
                        ErrorCuenta =true;
                        mensaje_error +="El numero de cuenta del item "+ cod_item +" marcado con rojo no existe en la base de datos";
                    }
                    //////////////////////////////////////////////////////////////////////
                    
                    if (codcliarea.equals(""))
                        tipcliarea = "";
                    
                    if( !planilla.equals("") ){
                        if( model.planillaService.existPlanilla(planilla)==false && sw == 0) {
                            sw  =   1;
                            log.info("ERROR: No Existe La planilla "+ planilla + " en el Item " + i);
                            mensaje_error = "No Existe La planilla "+ planilla + " en el Item " + i;
                        }
                    }
                    double valor = 0;
                    double valor_total = 0;
                    try {
                        valor       = model.factrecurrService.getNumero(request.getParameter("valor1"+i) );
                        valor_total = model.factrecurrService.getNumero(request.getParameter("valorNeto"+i) );
                    }
                    catch(java.lang.NumberFormatException e) {
                        valor = 0;
                        valor_total = 0;
                    }
                    
                    item.setErrorCuenta(ErrorCuenta);
                   
                    //Ivan 26 julio 2006
                    item.setRef3(Ref3);
                    item.setRef4(Ref4);
                    item.setRee(ree);
                    item.setAgencia(agenciaItem);
                    ///////////////////////////////
                    
                    item.setExiteCuenta(Existe);
                    item.setCodigos(codigos);
                    item.setDstrct(dstrct);
                    item.setProveedor(proveedor);
                    item.setTipo_documento(tipo_documento);
                    item.setDocumento(documento);
                    item.setItem(com.tsp.util.Utility.rellenar( String.valueOf(cod_item) ,3));
                    item.setDescripcion(descripcion_i);
                    item.setConcepto(concepto);
                    //double valor_l = valor*valor_tasa;
                    /*
                    if( !moneda.equals(moneda_usuario) ){
                        valor_l = model.factrecurrService.buscarValor( moneda_usuario ,moneda,fecha_documento,valor);
                    }
                    else{
                        valor_l = valor;
                    }*/
                    // valor_l = Util.redondear(valor_l*valor_tasa, 2);
                    item.setVlr(Util.roundByDecimal( valor*valor_tasa,0));//valor moneda local
                    item.setVlr_me(valor);//valor me del item
                    item.setCodigo_cuenta(codigo_cuenta);
                    item.setCodigo_abc(codigo_abc);
                    item.setPlanilla(planilla);
                    item.setCodcliarea(codcliarea);
                    item.setTipcliarea(tipcliarea);
                    item.setCreation_user(usuario.getLogin());
                    item.setUser_update(usuario.getLogin());
                    item.setBase(base);
                    item.setVlr_total(valor_total);
                    item.setIva(iva);
                    // ivan 21 julio 2006
                    item.setAuxiliar(auxiliar);
                    item.setTipo(tbltipo);
                    item.setTipoSubledger(tipoSubledger);
                    item.setReqAuxilar(ReqAuxiliar);
                    /////////////////////////////////
                    
                    
                    totalFac += valor;
                    totalFacLocal += item.getVlr();
                    
                    Vector vTipoImp             = model.TimpuestoSvc.vTiposImpuestos();
                    Vector vImpuestosPorItem    = new Vector();
                    Vector vImpuestosCopia      = new Vector();
                    
                    double vlrIvaAplicado = 0;
                    double vlrIvaAplicado_me = 0;
                    for(int x=0;x<vTipoImp.size();x++) {
                        CXPImpItem impuestoItem =   new CXPImpItem();
                        String cod_impuesto     =   ""+ request.getParameter("impuesto"+x+""+i);
                        if( !cod_impuesto.equals("") ){
                            String imp = (String) vTipoImp.elementAt(x);
                            if(model.TimpuestoSvc.existeImpuestoPorCodigo(cod_impuesto ,imp, fechaActual,agenciaBanco)==false && sw == 0 && !cod_impuesto.equals("")) {
                                sw  =   1;
                                log.info("ERROR: No Existe el Codigo de Impuesto " + cod_impuesto + " en el Item " + i);
                                mensaje_error   =   "No Existe el Codigo de Impuesto "+ cod_impuesto +" en el Item "+i;
                            }
                            String tipo_impuesto    =   ""+request.getParameter("tipo_impuesto"+x+""+i);
                            Tipo_impuesto o_impuesto;
                            o_impuesto = model.TimpuestoSvc.buscarImpuestoPorCodigos(tipo_impuesto,cod_impuesto,dstrct,agenciaBanco);
                            double por_imp = 0 ;
                            int    Ind_signo = 1;/**/
                            log.info("O_IMPUESTO: " + (o_impuesto!=null ? o_impuesto.getCodigo_impuesto() : "NR"));
                            if (o_impuesto !=null ) {
                                por_imp =   o_impuesto.getPorcentaje1();
                                Ind_signo = o_impuesto.getInd_signo();
                                log.info("--PORCENTAJE: " + por_imp);
                                log.info("--IND SIGNO: " + Ind_signo);
                            }
                            
                            impuestoItem.setDstrct(dstrct);
                            impuestoItem.setProveedor(proveedor);
                            impuestoItem.setTipo_documento(tipo_documento);
                            impuestoItem.setDocumento(documento);
                            impuestoItem.setItem(com.tsp.util.Utility.rellenar( String.valueOf(cod_item) ,3));
                            impuestoItem.setCod_impuesto(cod_impuesto);
                            impuestoItem.setTipo_impuesto(tipo_impuesto);
                            impuestoItem.setPorcent_impuesto(por_imp);
                            /*********************************************/
                            double valor_impuesto =0;
                            double valor_impuesto_me =0;
                            
                            //Tito favor agregar esto!!
                            if(tipo_impuesto.equals("IVA")){
                                valor_impuesto   = Util.roundByDecimal(( item.getVlr() * por_imp)/100,0);
                                //valor del impuesto d ela moneda estranjera
                                valor_impuesto_me =(moneda.equals("DOL"))?Util.roundByDecimal((valor * por_imp)/100 ,2):Util.roundByDecimal(( valor * por_imp)/100,0);
                                
                                vlrIvaAplicado    =  valor_impuesto;
                                vlrIvaAplicado_me =  valor_impuesto_me;
                            }else  if(tipo_impuesto.equals("RIVA")){
                                
                                valor_impuesto   = Util.roundByDecimal(( vlrIvaAplicado * por_imp)/100,0);
                                //valor del impuesto d ela moneda estranjera
                                valor_impuesto_me =(moneda.equals("DOL"))?Util.roundByDecimal((vlrIvaAplicado_me * por_imp)/100 ,2):Util.roundByDecimal(( vlrIvaAplicado_me * por_imp)/100,0);
                                
                            }else{
                                valor_impuesto   = Util.roundByDecimal(( item.getVlr() * por_imp)/100,0);
                                //valor del impuesto d ela moneda estranjera
                                valor_impuesto_me =(moneda.equals("DOL"))?Util.roundByDecimal((valor * por_imp)/100 ,2):Util.roundByDecimal(( valor * por_imp)/100,0);
                            }
                            
                            valor_impuesto    = valor_impuesto * Ind_signo;
                            valor_impuesto_me = valor_impuesto_me * Ind_signo;
                            totalFac += valor_impuesto_me;
                            totalFacLocal += valor_impuesto;
                            
                            log.info("IND SIGNO: " + Ind_signo);
                            log.info("VALOR IMPUESTO: "+ valor_impuesto);
                            log.info("VALOR IMPUESTO_ME: "+ valor_impuesto_me);
                            /***********************************************/
                            
                            /*double valor_impuesto   = ( (valor*valor_tasa) * por_imp)/100;
                             
                            valor_impuesto          =  Util.redondear(valor_impuesto, 2);
                             
                            //valor del impuesto d ela moneda estranjera
                            double valor_impuesto_me = (valor * por_imp)/100;
                            valor_impuesto_me = Util.redondear(valor_impuesto_me, 2);
                             
                            /*if( !moneda.equals(moneda_usuario) ){
                                valor_impuesto_me = model.factrecurrService.buscarValor(moneda_usuario,moneda,fecha_documento,vi);
                            }
                            else{
                                valor_impuesto_me = vi;
                            }*/
                            //valor_impuesto_me   =   Util.redondear(valor_impuesto_me*valor_tasa, 2);
                             impuestoItem.setVlr_total_impuesto(valor_impuesto);
                            impuestoItem.setVlr_total_impuesto_me(valor_impuesto_me);
                            impuestoItem.setCreation_user(usuario.getLogin());
                            impuestoItem.setUser_update(usuario.getLogin());
                            impuestoItem.setBase(base);
                            vImpuestosPorItem.add(impuestoItem);
                            vImpuestosCopia.add(impuestoItem);
                        }else{
                            CXPImpItem impItem = new CXPImpItem();
                            impItem.setCod_impuesto("");
                            impItem.setTipo_impuesto("");
                            vImpuestosCopia.add(impItem);
                            
                        }
                        
                        
                    }
                    
                    item.setVItems( vImpuestosPorItem );
                    item.setVCopia(vImpuestosCopia);
                    vItems.add(item);
                }else{
                    vItems.add(item);
                }
            }
            log.info("VALOR FAC-->"+ totalFac);
            /*****************************************/
            valor_local = totalFacLocal;
            factura.setVlr_neto(valor_local);
            
            
            factura.setVlr_total(totalFac);
            factura.setVlr_total_abonos(0);
            factura.setVlr_saldo(valor_local);
            factura.setVlr_neto_me(totalFac);
            factura.setVlr_total_abonos_me(0);
            factura.setVlr_saldo_me(totalFac);
            /*******************************************/
            model.factrecurrService.setFactura(factura);
            model.factrecurrService.setVecRxpItemsDoc(vItems);
            
            if(!Error){
                if(tasa!=null){
                    if (model.factrecurrService.existeDoc(dstrct, proveedor,tipo_documento,documento)==false || Modificar.equals("si")) {
                        if ((model.factrecurrService.existeDoc(dstrct, proveedor,tipo_documento_rel,documento_relacionado)==true)|| documento_relacionado.equals("")) {
                            if(sw==0) {
                                if(o_proveedor != null) {
                                    b=1;
                                    
                                    id_mims =   ""+o_proveedor.getC_idMims();
                                    Vector vImpDoc = new Vector();
                                    for (int i=0;i < vItems.size();i++) {
                                        CXPItemDoc item = new CXPItemDoc();
                                        item=(CXPItemDoc)vItems.elementAt(i);
                                        if(item.getVItems()!=null){
                                            Vector vImpuestosPorItem = item.getVItems();
                                            for(int x=0; x <vImpuestosPorItem.size();x++) {
                                                CXPImpItem impuestoI = (CXPImpItem)vImpuestosPorItem.elementAt(x);
                                                String cod_impuesto_doc =   "";
                                                double por_impuesto_doc =   0;
                                                double valor_impuesto    =   0;
                                                double valor_impuesto_me =   0;
                                                cod_impuesto_doc    = impuestoI.getCod_impuesto();
                                                por_impuesto_doc    = impuestoI.getPorcent_impuesto();
                                                valor_impuesto      = impuestoI.getVlr_total_impuesto();
                                                valor_impuesto_me   = impuestoI.getVlr_total_impuesto_me();
                                                CXPImpDoc impDoc    = new CXPImpDoc();
                                                impDoc.setDstrct(dstrct);
                                                impDoc.setProveedor(proveedor);
                                                impDoc.setTipo_documento(tipo_documento);
                                                impDoc.setDocumento(documento);
                                                impDoc.setCod_impuesto(cod_impuesto_doc);
                                                impDoc.setPorcent_impuesto(por_impuesto_doc);
                                                impDoc.setVlr_total_impuesto(valor_impuesto);
                                                impDoc.setVlr_total_impuesto_me(valor_impuesto_me);
                                                impDoc.setCreation_user(usuario.getLogin());
                                                impDoc.setUser_update(usuario.getLogin());
                                                impDoc.setBase(base);
                                                vImpDoc.add(impDoc);
                                            }
                                        }
                                    }
                                    
                                    vImpDoc = model.factrecurrImpService.generarVImpuestosDoc(vImpDoc);
                                    model.factrecurrImpService.setVImpuestosDoc(vImpDoc);
                                    
                                    
                                    
                                    if(Modificar.equals("si")){
                                        model.factrecurrService.UpdateFactura(factura, vItems, vImpDoc, agenciaBanco);
                                        
                                    }else{
                                        model.factrecurrService.insertarCXPDoc(factura, vItems, vImpDoc, agenciaBanco);
                                        
                                    }
                                    
                                    mensaje_error="Documento Guardado ";
                                    next = "/jsp/cxpagar/facturasrec/facturaP.jsp?ms="+mensaje_error+"&op=cargarB&num_items="+num_items+"&ag="+validar+"&id_agencia="+agencia+"&maxfila="+maxfila+"&Modificar="+Modificar;
                                }
                                
                                
                                if (b!=1) {
                                    proveedor   =   "";
                                    mensaje_error="El Proveedor no existe ";
                                    next = "/jsp/cxpagar/facturasrec/facturaP.jsp?ms="+mensaje_error+"&op=cargarB&num_items="+num_items+"&ag="+validar+"&id_agencia="+agencia+"&maxfila="+maxfila+"&Modificar="+Modificar;
                                   
                                }
                                
                            }
                            else {
                                next = "/jsp/cxpagar/facturasrec/facturaP.jsp?ms="+mensaje_error+"&op=cargarB&num_items="+num_items+"&ag="+validar+"&id_agencia="+agencia+"&maxfila="+maxfila+"&Modificar="+Modificar;
                                
                            }
                        }
                        else {
                            proveedor="";
                            mensaje_error="El Documento relacionado no existe en el Sistema ";
                            next = "/jsp/cxpagar/facturasrec/facturaP.jsp?ms="+mensaje_error+"&op=cargarB&num_items="+num_items+"&ag="+validar+"&id_agencia="+agencia+"&maxfila="+maxfila+"&Modificar="+Modificar;
                            
                        }
                    }
                    else {
                        proveedor="";
                        mensaje_error="El Documento ya existe ";
                        next = "/jsp/cxpagar/facturasrec/facturaP.jsp?ms="+mensaje_error+"&op=cargarB&num_items="+num_items+"&ag="+validar+"&id_agencia="+agencia+"&maxfila="+maxfila+"&Modificar="+Modificar;
                        
                    }
                }else{
                    mensaje_error="No existe el valor de la tasa";
                    next = "/jsp/cxpagar/facturasrec/facturaP.jsp?ms="+mensaje_error+"&op=cargarB&num_items="+num_items+"&ag="+validar+"&id_agencia="+agencia+"&maxfila="+maxfila+"&Modificar="+Modificar;
                    
                }
            }else{
                next = "/jsp/cxpagar/facturasrec/facturaP.jsp?ms="+mensaje_error+"&op=cargarB&num_items="+num_items+"&ag="+validar+"&id_agencia="+agencia+"&maxfila="+maxfila+"&Modificar="+Modificar;
               
                
            }
            factura.setVlr_total(vlr_total);
            factura.setVlr_neto_me(vlr_total);
            factura.setVlr_saldo_me(vlr_total);
            factura.setVlr_neto(vlr_total);
            factura.setUsuario_aprobacion(usuario_aprobacion);
            model.factrecurrService.setFactura(factura);
            log.info("NEXT --->"+ next);
            this.dispatchRequest(next);
            
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new ServletException("Accion:"+ e.getMessage());
        }
    }
    
}
