/********************************************************************
 *      Nombre Clase.................   AplicaciobUpdateAction.java    
 *      Descripci�n..................   Actualiza un registro en la tabla tblapl    
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class AplicacionUpdateAction extends Action{
        
        /** Creates a new instance of DocumentoInsertAction */
        public AplicacionUpdateAction() {
        }
        
        public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
                //Pr�xima vista
                String pag  = "/jsp/masivo/tblapl/AppUpdate.jsp?mensaje=";
                String next = "";
                
                //Usuario en sesi�n
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario) session.getAttribute("Usuario");

                try{   
                        Aplicacion app = new Aplicacion();
                        app.setBase(usuario.getBase());//request.getParameter("
                        app.setC_codigo(request.getParameter("c_codigo"));
                        app.setC_descripcion(request.getParameter("c_descripcion"));                        
                        app.setDistrito((String) session.getAttribute("Distrito"));
                        app.setUsuario_modificacion(usuario.getLogin());
                        app.setEstado("");
                        
                        model.appSvc.actualizarAplicacion(app     );
                        pag+= "MsgModificado&msg=Se ha modificado la aplicaci�n exitosamente.";                   
                        model.appSvc.setAplicacion(app);
                        
                        next = com.tsp.util.Util.LLamarVentana(pag, "Actualizar Documento");

                }catch (SQLException e){
                       throw new ServletException(e.getMessage());
                }

                this.dispatchRequest(next);
        }
        
}
