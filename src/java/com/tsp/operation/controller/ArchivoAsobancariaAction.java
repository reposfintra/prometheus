/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.ArchivoAsobancariaDAO;
import com.tsp.operation.model.DAOS.impl.ArchivoAsobancariaImpl;
import com.tsp.operation.model.MenuOpcionesModulos;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.RecaudoAsobancaria;
import com.tsp.operation.model.beans.RecaudoAsobancariaDetalle;
import com.tsp.operation.model.beans.RecaudoAsobancariaLote;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.UtilFinanzas;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpSession;
import javax.xml.rpc.holders.DoubleWrapperHolder;

/**
 *
 * @author egonzalez
 */
@MultipartConfig
public class ArchivoAsobancariaAction extends Action{
    
    private final int CARGAR_OPCIONES_RECAUDO = 0;
    private final int CARGAR_ARCHIVO = 1;
    private final int LISTAR_CABECERA_RECAUDO = 2;
    private final int LISTAR_DETALLE_RECAUDO = 3;
    private final int BUSCAR_ENTIDADES_RECAUDADORAS = 4;
    private final int LISTAR_CABECERA_RECAUDO_ID = 5;
    private final int LISTAR_INFO_PAGO_EXTRACTO = 6;
    private final int LISTAR_DETALLE_INGRESO = 7;
    private final int OBTENER_CAUSAL_DEVOLUCION = 8;
    private final int LISTAR_ENTIDADES_RECAUDADORAS = 9;
    private final int GUARDAR_ENTIDAD_RECAUDO = 10;
    private final int ACTUALIZAR_ENTIDAD_RECAUDO = 11;
    private final int BUSCAR_PAISES = 12;
    private final int BUSCAR_DEPARTAMENTOS = 13;
    private final int BUSCAR_CIUDADES = 14;
    private final int BUSCAR_ENTIDADES_RECAUDO = 15;
    private final int ACTUALIZAR_COMISION_ENTIDADES= 16;
    private ArchivoAsobancariaDAO dao;
    private String txtResp = "";
    private RecaudoAsobancaria recaudoCabecera;
    private RecaudoAsobancariaDetalle recaudoDetalle;
    private RecaudoAsobancariaLote recaudoLote;
    ArrayList<RecaudoAsobancariaLote> listaLote = new ArrayList();
    ArrayList<RecaudoAsobancariaDetalle> listaDetalle = new ArrayList();
//  ArrayList<RecaudoAsobancariaDiccionario> cargarDiccionario = new ArrayList();
     Usuario usuario = null;
    
    @Override
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            dao = new ArchivoAsobancariaImpl(usuario.getBd()); 
            recaudoCabecera = null;
            recaudoDetalle = null;
            recaudoLote = null;
          
            if (!listaLote.isEmpty()) listaLote.clear();
            if (!listaDetalle.isEmpty()) listaDetalle.clear();
            int opcion = (request.getParameter("opcion") != null)
                    ? Integer.parseInt(request.getParameter("opcion"))
                    : -1;
            switch (opcion) {
                case CARGAR_OPCIONES_RECAUDO:
                    cargarMenuRecaudo();
                    break;
                case CARGAR_ARCHIVO:
                    cargarArchivo();
                    break;
                case LISTAR_CABECERA_RECAUDO:
                    buscarCabeceraRecaudo();
                    break;
                case LISTAR_DETALLE_RECAUDO:
                    buscarDetalleRecaudo();
                    break;
                case BUSCAR_ENTIDADES_RECAUDADORAS:
                    cargarEntidadesRecaudo();
                    break;
                case LISTAR_CABECERA_RECAUDO_ID:
                    int idrecaudo = request.getParameter("idRecaudo") != null ? Integer.parseInt(request.getParameter("idRecaudo")) : 0;
                    buscarCabeceraRecaudo(idrecaudo);
                    break;
                case LISTAR_INFO_PAGO_EXTRACTO:
                    buscarInfoPagoExtracto();
                    break;
                case LISTAR_DETALLE_INGRESO:
                    buscarDetalleIngreso();
                    break;
                case OBTENER_CAUSAL_DEVOLUCION:
                    obtenerCausalDevolucion();
                    break;
                case LISTAR_ENTIDADES_RECAUDADORAS:
                    cargarEntidadesRecaudo(usuario.getDstrct());
                    break;
                case GUARDAR_ENTIDAD_RECAUDO:  
                    guardarEntidadRecaudo();
                    break;
                case ACTUALIZAR_ENTIDAD_RECAUDO: 
                    actualizarEntidadRecaudo();
                    break;              
                case BUSCAR_PAISES:
                    cargarPaises();
                    break;
                case BUSCAR_DEPARTAMENTOS:
                    cargarDepartamentos();
                    break;
                case BUSCAR_CIUDADES:
                    cargarCiudades();
                    break;
                case BUSCAR_ENTIDADES_RECAUDO:
                    cargarComisionEntidadesRecaudo();
                    break;
                case ACTUALIZAR_COMISION_ENTIDADES:
                    actualizarComisionEntidadRecaudo();
                    break;
                default:
                    break;
            }
        } catch(Exception e ) {
            txtResp = "{error:"+e.getMessage()+"}";
        } /* finally {
            this.responser(txtResp);
         }*/
    }    
    
    /** envia respuesta al cliente **/
    private void responser(String texto) {
        try {
            response.setContentType("application/json; charset=utf-8");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println(texto);
        } catch (Exception e) {
        }
    }
    
    public void cargarArchivo() { 
        
        String usuario = "";
        String error = "";
        String respuestaJson = "{}";
        try {
                                
            Usuario us = (Usuario) request.getSession().getAttribute("Usuario");
            String ruta = UtilFinanzas.obtenerRuta("ruta", "/exportar/migracion/"
                    + ((us != null) ? us.getLogin() : "anonimo"));
            usuario = (us.getLogin() != null) ? us.getLogin() : "";
           this.model.LogProcesosSvc.InsertProceso ("Cargue archivo recaudo Asobancaria", this.hashCode(), "Proceso de cargue archivo recaudo Asobancaria", usuario);
            
            //Obtenemos campos de formulario (Tanto de texto como archivo) (enctype=multipart/form-data)
            MultipartRequest mreq = new MultipartRequest(request, ruta, 4096 * 1024, new DefaultFileRenamePolicy());
            String filetype = mreq.getContentType("archivo");
            //Buscamos el nombre del archivo cargado
            String filename = mreq.getFilesystemName("archivo");
            File f = new File(ruta + "/" + filename);
            //Validamos la extensi�n del archivo que va a cargarse
            if (filetype.equals("text/plain") || filetype.equals("application/octet-stream")) {
//              Upload upload = new Upload(ruta + "/", request);
//              upload.load();
                //Buscamos el nombre original del archivo
                String originalname = mreq.getOriginalFileName("archivo");
                if (originalname.equals(filename)) {
                    String respuesta = validarArchivo(ruta + "/" + filename);
                    //Validamos la informaci�n suministrada en el archivo
                    if (respuesta.equals("")) {         
                        String empresa = (us.getDstrct()!= null) ? us.getDstrct() : "";
                        int id_Recaudo = dao.insertarRecaudos(recaudoCabecera, listaDetalle, (us.getLogin() != null) ? us.getLogin() : "", empresa);
                        if (id_Recaudo > 0) {
                            String tipoAsobancaria = (recaudoCabecera.getNum_lotes() == 0) ? "ASO98" : "ASO2001-09";
//                          respuestaJson = "{\"respuesta\":\"OK\",\"idRecaudo\":\"" + id_Recaudo + "\",\"tipo\":\"" + tipoAsobancaria + "\"}";

//Modificacion para cargar asobancaria desde aplicacion de pagos masivo
                            error ="02"; //dao.procesarRecaudosDetalle(id_Recaudo, usuario);
                            respuestaJson = (error.equals("02")) ? "{\"respuesta\":\"OK\",\"idRecaudo\":\""+id_Recaudo+"\",\"tipo\":\"" + tipoAsobancaria + "\"}" : "{\"error\":\"Ocurri� un error al procesar el archivo\",\"idRecaudo\":\""+id_Recaudo+"\",\"tipo\":\"" + tipoAsobancaria + "\"}";
                        } else {
                            f.delete();  // borramos el archivo cargado
                            error = "18";
                            respuestaJson = "{\"error\":\"Ocurri� un error en la subida del archivo. Para m�s informaci�n, consulte el log de proceso.\",\"idRecaudo\":\"0\"}";
                        }
                    } else {
                        f.delete();  // borramos el archivo cargado
                        error = respuesta;
                        respuestaJson = "{\"error\":\"Ocurri� un error al validar el archivo. Para m�s informaci�n, consulte el log de proceso.\",\"idRecaudo\":\"0\"}";
                    }
                } else {
                    f.delete();  // borramos el archivo cargado
                    error = "20";
                    respuestaJson = "{\"error\":\"Existe un archivo cargado con ese nombre. Verifique.\",\"idRecaudo\":\"0\"}";
                }
//                
            } else {
                f.delete();  // borramos el archivo cargado
                error = "03";
                respuestaJson = "{\"error\":\"La extensi�n del archivo es inv�lida. Verifique.\",\"idRecaudo\":\"0\"}";
            }     

            this.printlnResponse(respuestaJson, "application/json;");        
                              
        } catch (Exception ex) {     
            error = "99 cargarArchivo(Error: "+ex.getMessage()+")";
            Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
            try {
                this.model.LogProcesosSvc.finallyProceso2("Cargue archivo recaudo Asobancaria", this.hashCode(), usuario, dao.obtenerEstadoRecaudo(error));
            } catch (SQLException ex) {
//                Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
    
     /**
     * Escribe la respuesta de una opcion ejecutada desde AJAX
     *
     * @param respuesta
     * @param contentType
     * @throws Exception
     */
    private void printlnResponse(String respuesta, String contentType) throws Exception {

        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);

    }
       
    
    public String validarArchivo(String rutaArchivo) {
        String result = "";
        try {
            FileReader fr = null;
            BufferedReader br = null;

            // Apertura del fichero y creacion de BufferedReader para poder
            // hacer una lectura comoda (disponer del metodo readLine()).
            fr = new FileReader(rutaArchivo);
            br = new BufferedReader(fr);     
            
            //Cargamos la informaci�n correspondiente a la tabla Diccionario  
            //cargarDiccionario = dao.cargarDiccionario();
    
            // Lectura del fichero
            String linea;
            String tipoArchivo = "";
            int size = 0;
            int cont = 0;
            int contLotes = 0; //Variable que lleva el conteo de lotes
            while ((linea = br.readLine()) != null) {
                if (linea.equals("")) {
                    continue;
                }
                String regtype = linea.substring(0, 2); //Obtenemos el tipo de registro
                if (cont == 0) {
                    size = linea.trim().length();
                    switch (size) {
                        case 64:
                        tipoArchivo="asobancaria98";
                        break;
                        case 55:
                        tipoArchivo="asobancaria2001";
                        break;
                        case 61:
                        tipoArchivo="asobancaria2011";
                        break;
                    }
//                    tipoArchivo = (size == 64) ? "asobancaria98" : "asobancaria2001";
                }             
                switch (size) {
                    case 64:
                        switch (regtype) {
                            case "01":
                            case "09":
                                result = setCabeceraRecaudo(regtype, linea, contLotes);
                                if (!result.equals("")) {
                                    return result;
                                }
                                break;
                            case "02":
                                result = setDetalleRecaudo(linea);
                                if (!result.equals("")) {
                                    return result;
                                }
                                break;
                            default:
                                return "04";
                        }   break;
                    case 55:
                        switch (regtype) {
                            case "01":
                            case "09":
                                result = setCabeceraRecaudo(regtype, linea, contLotes);
                                if (!result.equals("")) {
                                    return result;
                                }
                                break;
                            case "05":
                            case "08":
                                if (regtype.equals("05")) {
                                    contLotes++;
                                }
                                result = setLoteRecaudo(regtype, linea);
                                if (!result.equals("")) {
                                    return result;
                                }
                                break;
                            case "06":
                                result = setDetalleRecaudo(linea);
                                if (!result.equals("")) {
                                    return result;
                                }
                                break;
                            default:
                                return "04";
                        }   break;
                    case 61:
                        switch (regtype) {
                            case "01":
                            case "09":
                                result = setCabeceraRecaudo(regtype, linea, contLotes);
                                if (!result.equals("")) {
                                    return result;
                                }
                                break;
                            case "05":
                            case "08":
                                if (regtype.equals("05")) {
                                    contLotes++;
                                }
                                result = setLoteRecaudo(regtype, linea);
                                if (!result.equals("")) {
                                    return result;
                                }
                                break;
                            case "06":
                                result = setDetalleRecaudo(linea);
                                if (!result.equals("")) {
                                    return result;
                                }
                                break;
                            default:
                                return "04";
                        }   break;
                    default:
                        break;
                }
       
                cont++;
            }

            //Cerramos el stream
            fr.close();      
           
            if (tipoArchivo.equals("asobancaria98")) {
                 if (ingresadoArchivoRecaudo(recaudoCabecera.getTotal_registros(), recaudoCabecera.getValor_total(), recaudoCabecera.getRecaudadora_cod(), recaudoCabecera.getFecha_archivo())) return "21";
                 //Validamos totales y cantidad de registros del detalle y los comparamos con el registro de control de la cabecera
                result = validarDetalleConCabecera();
            } else if (tipoArchivo.equals("asobancaria2001")){
                if (ingresadoArchivoRecaudo(recaudoCabecera.getTotal_registros(), recaudoCabecera.getValor_total()/100, recaudoCabecera.getRecaudadora_cod(), recaudoCabecera.getFecha_archivo())) return "21";
                //Validamos totales y cantidad de registros del detalle y los comparamos con el registro de control del lote
                result = validarDetalleConLote();
                if (result.equals("")) result = validarLoteConCabecera();   //Validamos totales y cantidad de registros de los lotes y los comparamos con el registro de control del archivo
            } else if (tipoArchivo.equals("asobancaria2011")){
                
                if (ingresadoArchivoRecaudo(recaudoCabecera.getTotal_registros(), recaudoCabecera.getValor_total()/100, recaudoCabecera.getRecaudadora_cod(), recaudoCabecera.getFecha_archivo())) return "21";
                //Validamos totales y cantidad de registros del detalle y los comparamos con el registro de control del lote
                result = validarDetalleConLote();
                if (result.equals("")) result = validarLoteConCabecera();   //Validamos totales y cantidad de registros de los lotes y los comparamos con el registro de control del archivo
            }
                      
        } catch (Exception ex) {
            result = "99 validarArchivo(Error: " + ex.getMessage() + ")";
            Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }
 
    private String setCabeceraRecaudo(String type, String linea, int cantLotes) {

        String resultado = "";
        try {
            if (type.equals("01")) {
                if (linea.trim().length() == 55) {
                    recaudoCabecera = new RecaudoAsobancaria();
                    recaudoCabecera.setFacturadora_nit(linea.substring(2, 12).replaceFirst("^0+", ""));
                    recaudoCabecera.setFecha_recaudo(linea.substring(12, 20));
                    int codEntidad = Integer.parseInt(linea.substring(20, 23));
                    //Validamos que el codigo entidad recaudadora corresponda a uno de los existente tabla entidad_recaudo
                    if (!existeCodEntidadRecaudo(codEntidad)) return "10";          
                    recaudoCabecera.setRecaudadora_cod(codEntidad);
                    recaudoCabecera.setCuenta_cli(linea.substring(23, 40).replaceFirst("^0+", ""));
                    recaudoCabecera.setFecha_archivo(linea.substring(40, 52));
                    recaudoCabecera.setModificador(linea.substring(52, 53));
                    recaudoCabecera.setTipo_cuenta(linea.substring(53, 55));

                } else if (linea.trim().length() == 64) {
                    
                    recaudoCabecera = new RecaudoAsobancaria();
                    recaudoCabecera.setFacturadora_nit(linea.substring(2, 15).replaceFirst("^0+", ""));
                    recaudoCabecera.setFecha_recaudo(linea.substring(15, 23));
                    int codEntidad = Integer.parseInt(linea.substring(23, 26));
                    //Validamos que el codigo entidad recaudadora corresponda a uno de los existente tabla entidad_recaudo
                    if (!existeCodEntidadRecaudo(codEntidad)) return "10";
                    recaudoCabecera.setRecaudadora_cod(codEntidad);
                    recaudoCabecera.setCuenta_cli(linea.substring(26, 41).replaceFirst("^0+", ""));
                    recaudoCabecera.setFecha_archivo(linea.substring(15, 23)+"0000");
                    recaudoCabecera.setModificador("");
                    recaudoCabecera.setTipo_cuenta("");
                    
                } else if (linea.trim().length() == 61) {
                    
                    recaudoCabecera = new RecaudoAsobancaria();
                    recaudoCabecera.setFacturadora_nit(linea.substring(2, 18).replaceFirst("^0+", ""));
                    recaudoCabecera.setFecha_recaudo(linea.substring(18, 26));
                    int codEntidad = Integer.parseInt(linea.substring(26, 29));
                     //Validamos que el codigo entidad recaudadora corresponda a uno de los existente tabla entidad_recaudo
                    if (!existeCodEntidadRecaudo(codEntidad)) return "10";
                    recaudoCabecera.setRecaudadora_cod(codEntidad);
                    recaudoCabecera.setTipo_cuenta(linea.substring(29, 31));
                    recaudoCabecera.setCuenta_cli(linea.substring(31, 48).replaceFirst("^0+", ""));
                    recaudoCabecera.setFecha_archivo(linea.substring(48, 56));
                    recaudoCabecera.setModificador(linea.substring(60,61));
                
                } else {
                    resultado = "05";
                }
            } else {
                if (linea.trim().length() == 29) {
                    recaudoCabecera.setNum_lotes(cantLotes);
                    recaudoCabecera.setTotal_registros(Integer.parseInt(linea.substring(2, 11)));
                    recaudoCabecera.setValor_total(Double.parseDouble(linea.substring(11, 29)));
                } else if (linea.trim().length() == 64) {
                    recaudoCabecera.setNum_lotes(cantLotes);
                    recaudoCabecera.setTotal_registros(Integer.parseInt(linea.substring(2, 11)));
                    recaudoCabecera.setValor_total(Double.parseDouble(linea.substring(11, 29)));
                } else if (linea.trim().length() == 47) {
                    recaudoCabecera.setNum_lotes(cantLotes);
                    recaudoCabecera.setTotal_registros(Integer.parseInt(linea.substring(2, 11)));
                    recaudoCabecera.setValor_total(Double.parseDouble(linea.substring(11, 29)));
                    recaudoCabecera.setTotal_recaudo_cheque(Double.parseDouble(linea.substring(29, 47)));
                } else {
                    resultado = "06";
                }
            }
        } catch (Exception ex) {
            resultado = "99 setCabeceraRecaudo(Error: " + ex.getMessage() + ")";
            Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return resultado;

    }

    private String setLoteRecaudo(String type, String linea) {

        String resultado = "";
        try {
            if (type.equals("05")) {
                switch (linea.trim().length()) {
                    case 19:
                        recaudoLote = new RecaudoAsobancariaLote();
                        recaudoLote.setCod_servicio_rec(linea.substring(2, 15));
                        recaudoLote.setNumero_lote(Integer.parseInt(linea.substring(15, 19)));
                        break;
                    case 29:
                        recaudoLote = new RecaudoAsobancariaLote();
                        recaudoLote.setCod_servicio_rec(linea.substring(2, 15));
                        recaudoLote.setCod_convenio(Integer.parseInt(linea.substring(15, 25)));
                        recaudoLote.setNumero_lote(Integer.parseInt(linea.substring(25, 29)));
                        break;
                    default:
                        resultado = "07";
                        break;
                }
            } else {
                if (linea.trim().length() == 33) {
                    recaudoLote.setTotal_registros(Integer.parseInt(linea.substring(2, 11)));
                    recaudoLote.setTotal_recaudado(Double.parseDouble(linea.substring(11, 29)));
                    listaLote.add(recaudoLote);
                } else if (linea.trim().length() == 51) {
                    recaudoLote.setTotal_registros(Integer.parseInt(linea.substring(2, 11)));
                    recaudoLote.setTotal_recaudado(Double.parseDouble(linea.substring(11, 29)));
                    recaudoLote.setTotal_recaudado_cheque(Double.parseDouble(linea.substring(29, 47)));
                    recaudoLote.setNumero_lote(Integer.parseInt(linea.substring(47, 51)));
                    listaLote.add(recaudoLote);
                } else {
                    resultado = "08";
                }
            }
        } catch (Exception ex) {
            resultado = "99 setLoteRecaudo(Error: " + ex.getMessage() + ")";
            Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return resultado;

    }

    private String setDetalleRecaudo(String linea) {
        
        String resultado = "";        
        try {
            if (linea.trim().length() == 97 || linea.trim().length()==94) {
                recaudoDetalle = new RecaudoAsobancariaDetalle();
                recaudoDetalle.setCod_servicio_rec(recaudoLote.getCod_servicio_rec());
                recaudoDetalle.setNumero_lote(recaudoLote.getNumero_lote());
                recaudoDetalle.setReferencia_factura(linea.substring(2, 50).replaceFirst("^0+", ""));
                recaudoDetalle.setValor_recaudado(Double.parseDouble(linea.substring(50, 64)));
                recaudoDetalle.setProcedencia_pago(linea.substring(64, 66));
                recaudoDetalle.setMedio_pago(linea.substring(66, 68));
                recaudoDetalle.setNum_operacion(linea.substring(68, 74).replaceFirst("^0+", ""));
                recaudoDetalle.setNum_autorizacion(linea.substring(74, 80).replaceFirst("^0+", ""));
                int codEntidad = Integer.parseInt(linea.substring(80, 83));
//              if (codEntidad != recaudoCabecera.getRecaudadora_cod()) return "11";                
                recaudoDetalle.setCod_entidad_debitada(codEntidad);
                recaudoDetalle.setCod_sucursal(linea.substring(83, 87));
                recaudoDetalle.setSecuencia(Integer.parseInt(linea.substring(87, 94)));
                recaudoDetalle.setCausal_devolucion(linea.substring(94, 97));
                recaudoDetalle.setFecha_real_recaudo("01000101");
                recaudoDetalle.setNumero_cheque("");
                recaudoDetalle.setCod_compensacion_cheque("0");
                listaDetalle.add(recaudoDetalle);
                
            }else if (linea.trim().length() == 184) {
                recaudoDetalle = new RecaudoAsobancariaDetalle();
                recaudoDetalle.setCod_servicio_rec(recaudoLote.getCod_servicio_rec());
                recaudoDetalle.setNumero_lote(recaudoLote.getNumero_lote());
                recaudoDetalle.setReferencia_factura(linea.substring(2, 66).replaceFirst("^0+", ""));
                recaudoDetalle.setSegunda_referencia_usuario(linea.substring(66, 98).replaceFirst("^0+", ""));
                recaudoDetalle.setTipo_recaudo(Integer.parseInt(linea.substring(98, 100)));
                recaudoDetalle.setFecha_real_recaudo(linea.substring(100, 108));
                recaudoDetalle.setValor_recaudado(Double.parseDouble(linea.substring(114, 128)));
                recaudoDetalle.setValor_recaudo_canje(Double.parseDouble(linea.substring(128, 142)));
                recaudoDetalle.setProcedencia_pago(linea.substring(142, 144));
                recaudoDetalle.setMedio_pago(linea.substring(144, 146));
                recaudoDetalle.setNum_operacion(linea.substring(146, 152).replaceFirst("^0+", ""));
                recaudoDetalle.setNumero_cheque(linea.substring(152, 162).replaceFirst("^0+", ""));
                recaudoDetalle.setCod_compensacion_cheque(linea.substring(162, 164));
                recaudoDetalle.setNum_autorizacion(linea.substring(164, 170).replaceFirst("^0+", ""));
                int codEntidad = Integer.parseInt(linea.substring(170, 173));
//              if (codEntidad != recaudoCabecera.getRecaudadora_cod()) return "11";                
                recaudoDetalle.setCod_entidad_debitada(codEntidad);
                recaudoDetalle.setCod_sucursal(linea.substring(173, 177));
                recaudoDetalle.setSecuencia(Integer.parseInt(linea.substring(177, 184)));
                recaudoDetalle.setCausal_devolucion(linea.substring(184, 187));
                listaDetalle.add(recaudoDetalle);
                
            } else if (linea.trim().length() == 64) {
                recaudoDetalle = new RecaudoAsobancariaDetalle();
                recaudoDetalle.setCod_servicio_rec("");
                recaudoDetalle.setNumero_lote(0);
                recaudoDetalle.setReferencia_factura(linea.substring(2, 27).replaceFirst("^0+", ""));
                recaudoDetalle.setValor_recaudado(Double.parseDouble(linea.substring(27, 40)));
                recaudoDetalle.setProcedencia_pago(linea.substring(40, 42));
                recaudoDetalle.setMedio_pago("");
                recaudoDetalle.setNum_operacion("");
                recaudoDetalle.setNum_autorizacion("");         
                recaudoDetalle.setCod_entidad_debitada(recaudoCabecera.getRecaudadora_cod());
                recaudoDetalle.setCod_sucursal("");
                recaudoDetalle.setSecuencia(Integer.parseInt(linea.substring(42, 49)));
                recaudoDetalle.setCausal_devolucion("");
                listaDetalle.add(recaudoDetalle);
            } else {
                resultado = "09";
            }
        } catch (Exception ex) {
            resultado = "99 setDetalleRecaudo(Error: " + ex.getMessage() + ")";
            Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return resultado;
    }
    
    
    private String validarDetalleConCabecera() {

        String resultado = "";
        int totalRegistrosDetalle;
        double valorTotalDetalle;
        try {

                totalRegistrosDetalle = 0;
                valorTotalDetalle = 0;
                Iterator<RecaudoAsobancariaDetalle> itrDetalles = listaDetalle.iterator();
                while (itrDetalles.hasNext()) {
                    RecaudoAsobancariaDetalle detalle = itrDetalles.next();
                    totalRegistrosDetalle++;
                    valorTotalDetalle += detalle.getValor_recaudado();                  
                }
                if (recaudoCabecera.getTotal_registros() != totalRegistrosDetalle) {
                    resultado = "16";
                    return resultado;
                } else if (recaudoCabecera.getValor_total() != valorTotalDetalle) {
                    resultado = "17";
                    return resultado;
                }
            
        } catch (Exception ex) {
            resultado = "99 validarDetalleConCabecera(Error: " + ex.getMessage() + ")";
            Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return resultado;

    }
    
    private String validarDetalleConLote() {

        String resultado = "";
        int totalRegistrosDetalle;
        double valorTotalDetalle;
        try {
            Iterator<RecaudoAsobancariaLote> itrLotes = listaLote.iterator();
            while (itrLotes.hasNext()) {
                RecaudoAsobancariaLote lote = itrLotes.next();
                totalRegistrosDetalle = 0;
                valorTotalDetalle = 0;
                Iterator<RecaudoAsobancariaDetalle> itrDetalles = listaDetalle.iterator();
                while (itrDetalles.hasNext()) {
                    RecaudoAsobancariaDetalle detalle = itrDetalles.next();
                    if (lote.getNumero_lote() == detalle.getNumero_lote()) {
                        totalRegistrosDetalle++;
                        valorTotalDetalle += detalle.getValor_recaudado();
                    }
                }
                if (lote.getTotal_registros() != totalRegistrosDetalle) {
                    resultado = "12";
                    return resultado;
                } else if (lote.getTotal_recaudado() != valorTotalDetalle) {
                    resultado = "13";
                    return resultado;
                }
            }
        } catch (Exception ex) {
            resultado = "99 validarDetalleConLote(Error: " + ex.getMessage() + ")";
            Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return resultado;

    }
    
    private String validarLoteConCabecera() {
        
        String resultado = "";
        int totalRegistrosLote = 0;
        double valorTotalLote = 0;
        try {

            Iterator<RecaudoAsobancariaLote> itrLotes = listaLote.iterator();
            while (itrLotes.hasNext()) {
                RecaudoAsobancariaLote lote = itrLotes.next();
                totalRegistrosLote += lote.getTotal_registros();
                valorTotalLote += lote.getTotal_recaudado();
            }
            if (recaudoCabecera.getTotal_registros() != totalRegistrosLote) {
                resultado = "14";
            } else if (recaudoCabecera.getValor_total() != valorTotalLote) {
                resultado = "15";
            }

        } catch (Exception ex) {
            resultado = "99 validarLoteConCabecera(Error: " + ex.getMessage() + ")";
            Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
        }

        return resultado;

    }
    
    private void buscarCabeceraRecaudo(int idRecaudo) {
        try {          
            ArrayList<RecaudoAsobancaria> listCabeceraRecaudo = dao.buscarCabeceraRecaudo(idRecaudo);
            Gson gson = new Gson();
            String json;
            json = "{\"page\":1,\"rows\":" + gson.toJson(listCabeceraRecaudo) + "}";
            this.printlnResponse(json, "application/json;");

        } catch (SQLException ex) {
            Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        
    private void buscarCabeceraRecaudo() {
        try {
            String fechaini = request.getParameter("fechaini") != null ? request.getParameter("fechaini") : "";
            String fechafin = request.getParameter("fechafin") != null ? request.getParameter("fechafin") : "";
            String entidad_recaudo = request.getParameter("referencia") != null ? request.getParameter("entidad_recaudadora") : "";
            String referencia = request.getParameter("referencia") != null ? request.getParameter("referencia") : "";
            ArrayList<RecaudoAsobancaria> listCabeceraRecaudo = dao.buscarCabeceraRecaudo(fechaini, fechafin, entidad_recaudo, referencia, usuario.getDstrct());
            Gson gson = new Gson();
            String json;
            json = "{\"page\":1,\"rows\":" + gson.toJson(listCabeceraRecaudo) + "}";
            this.printlnResponse(json, "application/json;");

        } catch (SQLException ex) {
            Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void buscarDetalleRecaudo() {
        try {
            int id_recaudo = request.getParameter("id") != null ? Integer.parseInt(request.getParameter("id")): 0;
            String tipo_asobancaria = request.getParameter("tipoasobanc") != null ? request.getParameter("tipoasobanc"): "";
            ArrayList<RecaudoAsobancariaDetalle> listDetalleRecaudo = dao.buscarDetalleRecaudo(id_recaudo, tipo_asobancaria);
            Gson gson = new Gson();
            String json;
            json = "{\"page\":1,\"rows\":" + gson.toJson(listDetalleRecaudo) + "}";
            this.printlnResponse(json, "application/json;");

        } catch (SQLException ex) {
            Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void cargarMenuRecaudo() {
        try {
            Usuario us = (Usuario) request.getSession().getAttribute("Usuario");            
            String usuario = (us.getLogin() != null) ? us.getLogin() : "";       
           
            ArrayList<MenuOpcionesModulos> listOpcionesRecaudo = dao.cargarMenuRecaudo(usuario,us.getDstrct());
            Gson gson = new Gson();
            String json;
            json =  gson.toJson(listOpcionesRecaudo);
            this.printlnResponse(json, "application/json;");

        } catch (SQLException ex) {
            Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void cargarEntidadesRecaudo() {
        try {          
            String json = dao.cargarCboEntidadesRecaudo(usuario.getDstrct());
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private boolean ingresadoArchivoRecaudo(int total_registros, double valor_total, int cod_entidad, String fecha_archivo) {
        boolean resp = false;
        try {      
            resp = dao.ingresadoArchivoRecaudo(total_registros, valor_total, cod_entidad, fecha_archivo,usuario.getDstrct());
        } catch (SQLException ex) {
            Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
           return resp;
        }

    }
     
    private boolean existeCodEntidadRecaudo(int cod_entidad) {
        boolean resp = false;
        try {  
           resp = dao.existeEntidadRecaudo(usuario.getDstrct(),cod_entidad);          
        } catch (SQLException ex) {
            Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
           return resp;
        }

    }
    
            
    private void buscarInfoPagoExtracto() {
        try {
            int idDetalleRecaudo = request.getParameter("idDetRecaudo") != null ? Integer.parseInt(request.getParameter("idDetRecaudo")) : 0;       
            ArrayList<BeanGeneral> listInfoPagoExtracto = dao.buscarInfoPagoExtracto(idDetalleRecaudo);
            Gson gson = new Gson();
            String json;
            json = "{\"page\":1,\"rows\":" + gson.toJson(listInfoPagoExtracto) + "}";
            this.printlnResponse(json, "application/json;");

        } catch (SQLException ex) {
            Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void buscarDetalleIngreso() {
        try {
            String numIngreso = request.getParameter("num_ingreso") != null ? request.getParameter("num_ingreso") : "";       
            ArrayList<BeanGeneral> listIngresoDetalle = dao.buscarDetalleIngreso(numIngreso);
            Gson gson = new Gson();
            String json;
            json = "{\"page\":1,\"rows\":" + gson.toJson(listIngresoDetalle) + "}";
            this.printlnResponse(json, "application/json;");

        } catch (SQLException ex) {
            Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    private void obtenerCausalDevolucion() {
        try{ 
            String idCausal = request.getParameter("idCausal");            
            String resp = "{}";
            String Causal = "";  
            Causal = dao.obtenerCausalDevolucion(idCausal);                  
            resp = "{\"Causal\":\" "+Causal+"\"}"; 
            this.printlnResponse(resp, "application/json;");           
             
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    private void cargarEntidadesRecaudo(String idEmpresa) {
        try {
            ArrayList<BeanGeneral> listEntidadRecaudo = dao.cargarEntidadesRecaudo(idEmpresa);
            Gson gson = new Gson();
            String json;
            json = "{\"page\":1,\"rows\":" + gson.toJson(listEntidadRecaudo) + "}";
            this.printlnResponse(json, "application/json;");

        } catch (SQLException ex) {
            Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void cargarPaises() {
        try {          
            String json = dao.cargarPais();
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void cargarDepartamentos() {
        try {       
             String codPais = request.getParameter("cod_pais") != null ? request.getParameter("cod_pais") : "";    
            String json = dao.cargarDepartamento(codPais);
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void cargarCiudades() {
        try {
            String codDpto = request.getParameter("cod_dpto") != null ? request.getParameter("cod_dpto") : "";   
            String json = dao.cargarCiudad(codDpto);
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
       
    private void guardarEntidadRecaudo() {
        try{
            int codigo = request.getParameter("codigo") != null ? Integer.parseInt(request.getParameter("codigo")) : 0;     
            String descripcion = request.getParameter("descripcion") != null ? request.getParameter("descripcion") : ""; 
            String nit = request.getParameter("nit") != null ? request.getParameter("nit") : ""; 
            String direccion = request.getParameter("direccion") != null ? request.getParameter("direccion") : "";     
            String telefono = request.getParameter("telefono") != null ? request.getParameter("telefono") : ""; 
            String email = request.getParameter("email") != null ? request.getParameter("email") : ""; 
            String ciudad = request.getParameter("ciudad") != null ? request.getParameter("ciudad") : "";     
            String cuenta = request.getParameter("cuenta") != null ? request.getParameter("cuenta") : ""; 
            String is_bank = request.getParameter("is_bank") != null ? request.getParameter("is_bank") : ""; 
            String pago_automatico = request.getParameter("pago_automatico") != null ? request.getParameter("pago_automatico") : ""; 
            String reg_status = request.getParameter("reg_status") != null ? request.getParameter("reg_status") : "";
            String empresa = usuario.getDstrct();
            String resp = "{}";
            if(!dao.existeEntidadRecaudo(empresa,codigo)){         
               resp =  dao.guardarEntidadRecaudadora(empresa,codigo,descripcion,nit,direccion,telefono,email,ciudad,cuenta,is_bank,pago_automatico,usuario.getLogin(),reg_status);                        
            }else{
                resp = "{\"error\":\" No se cre� la entidad, puede que el codigo ya este asignado\"}";
            }
            this.printlnResponse(resp, "application/json;");  
        }catch(Exception e){
            e.printStackTrace();
    }
    }
    
    private void actualizarEntidadRecaudo() {
        try{
            int codigo = request.getParameter("codigo") != null ? Integer.parseInt(request.getParameter("codigo")) : 0;     
            String descripcion = request.getParameter("descripcion") != null ? request.getParameter("descripcion") : ""; 
            String nit = request.getParameter("nit") != null ? request.getParameter("nit") : ""; 
            String direccion = request.getParameter("direccion") != null ? request.getParameter("direccion") : "";     
            String telefono = request.getParameter("telefono") != null ? request.getParameter("telefono") : ""; 
            String email = request.getParameter("email") != null ? request.getParameter("email") : ""; 
            String ciudad = request.getParameter("ciudad") != null ? request.getParameter("ciudad") : "";     
            String cuenta = request.getParameter("cuenta") != null ? request.getParameter("cuenta") : ""; 
            String is_bank = request.getParameter("is_bank") != null ? request.getParameter("is_bank") : ""; 
            String pago_automatico = request.getParameter("pago_automatico") != null ? request.getParameter("pago_automatico") : "";
            String reg_status = request.getParameter("reg_status") != null ? request.getParameter("reg_status") : "";
            String empresa = usuario.getDstrct();
            String resp = "{}";     
            resp =  dao.actualizarEntidadRecaudadora(empresa,codigo,descripcion,nit,direccion,telefono,email,ciudad,cuenta,is_bank,pago_automatico,usuario.getLogin(),reg_status);  
            this.printlnResponse(resp, "application/json;"); 
        }catch(Exception e){
            e.printStackTrace();
    }
    }
    
    private void cambiaEstadoEntidadRecaudadora(String estado) {
        try {
            int idEntidad = Integer.parseInt(request.getParameter("idEntidad"));
            String resp = dao.cambiarEstadoEntidadRecaudadora(idEntidad, estado);         
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
       
//    private boolean validaExisteSucursal(String cod_sucursal) {
//
//        boolean estado =false;        
//        try {
//
//            Iterator<String> itrSucursales = listaSucursales.iterator();
//            while (itrSucursales.hasNext()) {
//              if (cod_sucursal.equals(itrSucursales.next())){
//                 estado = true;
//                 return estado;
//              }              
//            }           
//
//        } catch (Exception ex) {  
//              estado=false;
//              Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
//        }finally{
//           return estado;
//        }
//
//    }

    private void cargarComisionEntidadesRecaudo() {
        try {          
            String json = dao.cargarComisionEntidadesRecaudo();
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(ArchivoAsobancariaAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void actualizarComisionEntidadRecaudo() {
       try {
            int cod_entidad  = Integer.parseInt(request.getParameter("codigo_entidad")!=null?request.getParameter("codigo_entidad"):"0");
            String  codigo_canal  =request.getParameter("codigo_canal")!=null?request.getParameter("codigo_canal"):"";
            double valor_comision =Double.parseDouble(request.getParameter("valor_comision")!=null?request.getParameter("valor_comision"):"0");
            double porcentaje_iva =Double.parseDouble(request.getParameter("porcentaje_iva")!=null?request.getParameter("porcentaje_iva"):"0");
            double valor_total_comision =Double.parseDouble(request.getParameter("valor_total_comision")!=null?request.getParameter("valor_total_comision"):"0");
            
            String resp = dao.actualizarComisionEntidadRecaudo(cod_entidad, codigo_canal, valor_comision,porcentaje_iva,valor_total_comision,usuario);         
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    
   
}