/********************************************************************
 *      Nombre Clase.................   ProveedorActualizarAction.java
 *      Descripci�n..................   Actualiza un registro en el archivo proveedor
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   30.09.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;

import java.util.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;


/**
 *
 * @author  Andres
 */
public class ProveedorActualizarAction extends Action{

    /** Creates a new instance of ProveedorActualizarAction */
    public ProveedorActualizarAction() {
    }

    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        //Pr�xima vista
        String next  = "/jsp/cxpagar/proveedor/ProveedorUpdate.jsp?mensaje=MsgModificado&msg=Proveedor modificado existosamente.";

        if( request.getParameter("norefresh")!=null ){//updated 02.02.2006
            next  = "/jsp/cxpagar/proveedor/ProveedorUpdate.jsp?mensaje=&norefresh=OK&msg=Proveedor modificado existosamente.";
        }

        String nit = request.getParameter("nit");
        String evento = request.getParameter("evento") != null ? request.getParameter("evento") : "";

        //Usuario en sesi�n
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");

        try{

            Proveedor prov = new Proveedor();
            prov.setC_agency_id(request.getParameter("c_agency_id"));
            prov.setC_autoretenedor_iva( ( request.getParameter("c_autoretenedor_iva") ==null )? "N" : request.getParameter("c_autoretenedor_iva")) ;
            prov.setC_agente_retenedor(( request.getParameter("c_agente_retenedor") ==null ) ? "N" : request.getParameter("c_agente_retenedor"));
            prov.setC_autoretenedor_ica( ( request.getParameter("c_autoretenedor_ica") ==null ) ? "N" : request.getParameter("c_autoretenedor_ica"));
            prov.setC_autoretenedor_rfte( ( request.getParameter("c_autoretenedor_rfte") ==null  ) ? "N" : request.getParameter("c_autoretenedor_rfte"));
            prov.setC_banco_transfer(request.getParameter("c_banco_transfer")==null?"":request.getParameter("c_banco_transfer"));
            prov.setC_branch_code(request.getParameter("c_branch_code"));
            prov.setC_bank_account(request.getParameter("c_bank_account"));
            prov.setC_clasificacion(request.getParameter("c_clasificacion"));
            prov.setC_codciudad_cuenta(request.getParameter("c_codciudad_cuenta")==null?"":request.getParameter("c_codciudad_cuenta"));
            prov.setC_gran_contribuyente((request.getParameter("c_gran_contribuyente") ==null  ) ? "N" : request.getParameter("c_gran_contribuyente"));
            prov.setC_idMims(request.getParameter("c_idMims"));
            prov.setC_nit(request.getParameter("c_nit"));
            prov.setC_payment_name(request.getParameter("c_payment_name").toUpperCase());
            prov.setC_numero_cuenta(request.getParameter("c_numero_cuenta")!=null?request.getParameter("c_numero_cuenta"):"");
            prov.setC_sucursal_transfer(request.getParameter("c_sucursal_transfer")!=null?request.getParameter("c_sucursal_transfer"):"");
            prov.setC_tipo_cuenta(request.getParameter("c_tipo_cuenta")==null?"":request.getParameter("c_tipo_cuenta"));
            prov.setC_tipo_doc(request.getParameter("c_tipo_doc"));
            prov.setHandle_code(request.getParameter("handle_code"));
            prov.setPlazo(Integer.parseInt(request.getParameter("plazo")));
            prov.setBase(usuario.getBase());
            prov.setUsuario_creacion(usuario.getLogin());
            prov.setUsuario_modificacion(usuario.getLogin());
            //nuevo 18-03-2006
            prov.setCedula_cuenta( (request.getParameter("cedula_cuenta")==null)?"":request.getParameter("cedula_cuenta").toUpperCase() );
            prov.setNombre_cuenta( (request.getParameter("nombre_cuenta")==null)?"":request.getParameter("nombre_cuenta").toUpperCase() );
            prov.setTipo_pago(request.getParameter("tipo_pago"));
            //jose 2006-09-28
            prov.setNit_beneficiario( (request.getParameter("nit_ben")==null)?"":request.getParameter("nit_ben") );
            prov.setNom_beneficiario( (request.getParameter("nom_ben")==null)?"":request.getParameter("nom_ben") );


            //01-02-2007
            prov.setConcept_code( request.getParameter("concept_code") );
            prov.setCmc(request.getParameter("handle_code"));
            prov.setAprobado(request.getParameter("aprobado")==null?"N":request.getParameter("aprobado"));
            prov.setAfil(request.getParameter("afil")!=null ? request.getParameter("afil") : "N");
            prov.setSede(request.getParameter("sede")!=null ? request.getParameter("sede") : "N");
            prov.setRegimen(request.getParameter("regimen")!=null ? request.getParameter("regimen") : "");//20100827
            prov.setCodfen(request.getParameter("codigo")!=null&&request.getParameter("codigo1")!=null&&request.getParameter("codigo2")!=null ? request.getParameter("codigo")+request.getParameter("codigo1")+request.getParameter("codigo2") : "");
            prov.setTipoProveedor(request.getParameter("tipo_proveedor"));
            prov.setDistrito(usuario.getDstrct());
            session.setAttribute("proveedor", prov);
            //<!-- 20101111 -->
            int maxfilas = request.getParameter("maxfilas")==null? 0: Integer.parseInt(request.getParameter("maxfilas"));//20100817
            int fils = request.getParameter("fils")==null? 0 : Integer.parseInt(request.getParameter("fils"));//20100817

            ArrayList lista1 = new ArrayList();
            Vector lista2 = null;
            BeanGeneral bean1 = null;
            BeanGeneral bean2 = null;
            System.out.println("fils: "+fils);
            System.out.println("maxfils: "+maxfilas);
            for (int i = 0; i < fils; i++) {
                System.out.println("ciclo: "+i);
                if(request.getParameter("conv"+(i+1))!=null && !(request.getParameter("conv"+(i+1)).equals(""))){
                    System.out.println("entra: "+i);
                    bean1 = new BeanGeneral();
                    bean1.setValor_01(request.getParameter("conv"+(i+1))!=null ? request.getParameter("conv"+(i+1)) : "0");
                    bean1.setValor_02(request.getParameter("sector"+(i+1))!=null ? request.getParameter("sector"+(i+1)) : "0");
                    bean1.setValor_03(request.getParameter("subsector"+(i+1))!=null ? request.getParameter("subsector"+(i+1)) : "0");
                    bean1.setValor_04(request.getParameter("cobertura"+(i+1))!=null ? (request.getParameter("cobertura"+(i+1))).replaceAll(",", "") : "0");
                    bean1.setValor_05(request.getParameter("cobert_flotante"+(i+1))!=null ? request.getParameter("cobert_flotante"+(i+1)).replaceAll(",", "") : "0");
                    bean1.setValor_06(request.getParameter("tasa"+(i+1))!=null ? request.getParameter("tasa"+(i+1)).replaceAll(",", "") : "0");
                    bean1.setValor_07(request.getParameter("custodia"+(i+1))!=null ? request.getParameter("custodia"+(i+1)).replaceAll(",", "") : "0");
                    bean1.setValor_08(request.getParameter("comision"+(i+1))!=null ? request.getParameter("comision"+(i+1)) : "false");
                    bean1.setValor_09(request.getParameter("cuenta"+(i+1))!=null ? request.getParameter("cuenta"+(i+1)) : "0");
                    bean1.setValor_10(request.getParameter("porc_afil"+(i+1))!=null ? request.getParameter("porc_afil"+(i+1)) : "0");
                    bean1.setValor_11(usuario.getLogin());
                    bean1.setValor_12(request.getParameter("idprovconv"+(i+1))!=null ? request.getParameter("idprovconv"+(i+1)) : "0");
                    lista2 = new Vector();
                    for (int j = 0; j < maxfilas; j++) {
                        System.out.println("ciclo: "+i+"_"+j);
                        bean2 = new BeanGeneral();
                        if(request.getParameter("riavales"+(i+1)+"_"+(j+1))!=null && !(request.getParameter("riavales"+(i+1)+"_"+(j+1)).equals(""))){
                            System.out.println("entra: "+i+"_"+j);
                            bean2.setValor_01(request.getParameter("riavales"+(i+1)+"_"+(j+1))!=null && !(request.getParameter("riavales"+(i+1)+"_"+(j+1)).equals(""))
                                    ? request.getParameter("riavales"+(i+1)+"_"+(j+1)) : "0");
                            bean2.setValor_02(request.getParameter("rfavales"+(i+1)+"_"+(j+1))!=null && !(request.getParameter("rfavales"+(i+1)+"_"+(j+1)).equals(""))
                                    ? request.getParameter("rfavales"+(i+1)+"_"+(j+1)) : "0");
                            bean2.setValor_03(request.getParameter("pavales"+(i+1)+"_"+(j+1))!=null && !(request.getParameter("pavales"+(i+1)+"_"+(j+1)).equals(""))
                                    ? request.getParameter("pavales"+(i+1)+"_"+(j+1)) : "0");
                            lista2.add(bean2);
                        }
                        System.out.println("ciclo: "+i+"_"+j);
                    }
                    bean1.setVec(lista2);
                    lista1.add(bean1);
                    System.out.println("ciclof: "+i);
                }
            }
            //<!-- 20101111 -->
            //modificacion 19/04/2007 Enrique
            String agency_mod = (String) session.getAttribute("agency");
            String branch_code_mod = (String) session.getAttribute("branch_code");
            String bank_account_mod = (String) session.getAttribute("bank_account");

            /*System.out.println("agency_mod: "+agency_mod);
            System.out.println("c_branch_code2: "+branch_code_mod);
            System.out.println("c_bank_account2: "+bank_account_mod);*/
            if(!agency_mod.equals(request.getParameter("c_agency_id"))|| !branch_code_mod.equals(request.getParameter("c_branch_code"))|| !bank_account_mod.equals(request.getParameter("c_bank_account"))){
                model.proveedorService.actualizarProveedor(prov,agency_mod,branch_code_mod,bank_account_mod);
            }
            else{
                model.proveedorService.actualizarProveedor(prov);
            }
            //<!-- 20101111 -->
          /*  try {
                model.proveedorService.deleteConvs(prov.getC_nit());
                model.proveedorService.insertConvs(prov.getC_nit(),lista1);//<!-- 20101201 -->
               // model.proveedorService.updateConvs(prov.getC_nit(),lista1);
            }
            catch (Exception e) {
                System.out.println("no se pudo actualizar la informacion de convenios del afiliado, error: "+e.toString());
                e.printStackTrace();
            }*/
            //<!-- 20101111 -->
             session.setAttribute("agency", prov.getC_agency_id());
             session.setAttribute("branch_code", prov.getC_branch_code());
             session.setAttribute("bank_account", prov.getC_bank_account());

            //modificacion 2006-02-17 Diogenes
            model.ciudadService.buscarCiudad(prov.getC_agency_id());
            Ciudad ciu = model.ciudadService.obtenerCiudad();
            next=next+"&sw="+ciu.getPais();
            if(evento.equals("aprobar")){
                prov.setAprobado(request.getParameter("aprobado")!=null?request.getParameter("aprobado"):"N");
                model.proveedorService.aprobarProveedor(prov);
            }

            //carga los combo box clasificacion y handle code
            model.tablaGenService.buscarClasificacion();
            model.tablaGenService.buscarHandle_code();
            model.tablaGenService.buscarBanco();
            model.tblgensvc.buscarListaSinDato("TCUENTA", "PROVEEDOR");
            //*******

        }catch (SQLException e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }

        this.dispatchRequest(next);
    }

}
