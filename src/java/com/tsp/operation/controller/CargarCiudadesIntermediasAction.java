/*
 * InsertRepMovTraficoAction.java
 *
 * Created on 14 de septiembre de 2005, 08:31 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import org.apache.log4j.Logger;
/**
 *
 * @author Karen Reales
 */
public class CargarCiudadesIntermediasAction extends Action{
    
    static Logger logger = Logger.getLogger(AplicarCaravanaAction.class);
    
    /** Creates a new instance of InsertRepMovTraficoAction */
    public CargarCiudadesIntermediasAction() {
    }
    public void run() throws javax.servlet.ServletException, InformationException{
        String next="/colpapel/ciudades.jsp" ;
        String ruta = request.getParameter("ruta");
        String origen = ruta.length()>=2 ? ruta.substring(0,2):"";
        String destino = ruta.length()>=4 ? ruta.substring(2,4):"";
        String cmd = request.getParameter("cmd")!=null?request.getParameter("cmd"):"";
        
        try{
            
            if(cmd.equals("cargar")){
                model.ciudadService.ciudadesIntermedias(origen,destino);
                model.ciudadService.setVias(new TreeMap());
            }
            else if(cmd.equals("via")){
                String ciudades = request.getParameter("eintermed");
                String via = "";
                String desc = "";
                model.ciudadService.viasxCiudad(origen, destino, ciudades);
                
            }
            
            
            
        }catch(SQLException ex){
            ex.printStackTrace();
            throw new ServletException(ex.getMessage());
        }
        this.dispatchRequest(next);
    }
}
