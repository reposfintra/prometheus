/***********************************************
 * Nombre clase: Cumplido_docInsertAction.java
 * Descripci�n: Accion para buscar un cumplido
 * Autor: Rodrigo Salazar
 * Fecha: 1 de octubre de 2005, 11:52 AM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 **********************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  R.SALAZAR
 */
public class Cumplido_docInsertarAction extends Action{
    
    /** Creates a new instance of Cuplido_docI nsertarAction */
    public Cumplido_docInsertarAction () {
    }
    public void run () throws ServletException, InformationException{
        String rem = request.getParameter ("c_rem");
        String pla = request.getParameter ("c_pla");
        String next="/jsp/cumplidos/cumplido_doc/ListaDocs.jsp?pla="+pla+"&rem="+rem;
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        Cumplido_doc cumplido_doc = null;
        List vec = new Vector ();
        remesa_docto remdoc;
        int sw=0;
        String msg="Debe seleccionar por lo menos un Documento para Cumplir";
        try{
            
            model.RemDocSvc.Listar (rem);
            vec = model.RemDocSvc.getList ();
            if (vec!=null){
                cumplido_doc = new Cumplido_doc ();
                for(int i=0; i < vec.size (); i++){
                    cumplido_doc = new Cumplido_doc ();
                    if(request.getParameter (""+i)!= null){
                        remdoc = (remesa_docto)vec.get (i);
                        cumplido_doc.setNumrem (rem);
                        cumplido_doc.setNumpla (pla);
                        cumplido_doc.setTipo_doc (remdoc.getTipo_doc ());
                        cumplido_doc.setDocumento (remdoc.getDocumento ());
                        cumplido_doc.setTipo_doc_rel (remdoc.getTipo_doc_rel ());
                        cumplido_doc.setDocumento_rel (remdoc.getDocumento_rel ());
                        cumplido_doc.setCrea_user (usuario.getLogin ());
                        cumplido_doc.setMod_user (usuario.getLogin ());
                        cumplido_doc.setBase (usuario.getBase ());
                        cumplido_doc.setDistrict ((String)session.getAttribute ("Distrito"));
                        cumplido_doc.setAgencia (usuario.getId_agencia ());
                        try{
                            model.cumplido_docService.InsertarCumplido_doc (cumplido_doc);
                        }catch(SQLException e){
                            sw=1;
                        }
                        if(sw==1){
                            if( model.cumplido_docService.searchCumplido_doc ( (String)session.getAttribute ("Distrito"), rem, pla, remdoc.getTipo_doc (), remdoc.getDocumento (), remdoc.getTipo_doc_rel (), remdoc.getDocumento_rel () ) )
                                msg = "El documento ya estaba agregado";
                            else
                                msg = "Se agregaron los documentos";
                        }
                        else
                            msg = "Se agregaron los documentos";
                    }
                }
            }
            request.setAttribute ("mensaje",msg);
            model.RemDocSvc.Listar (rem);
            vec = model.RemDocSvc.getList ();
            request.setAttribute ("documentos",vec);
        }catch (Exception e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
}
