/*
* Nombre        TraficoSearchAction.java
* Descripci�n   Acci�n ejecutada por el controlador para elaborar el reporte de tr�fico
* Autor         Alejandro Payares
* Fecha         24 de marzo de 2004, 04:42 PM
* Versi�n       1.0
* Coyright      Transportes Sanchez Polo S.A.
*/


package com.tsp.operation.controller;

import java.io.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;

import org.apache.log4j.Logger;

/**
 * Acci�n ejecutada por el controlador para elaborar el reporte de tr�fico
 * @author  Nestor Parejo
 */
public class TraficoSearchAction extends Action
{
  static Logger logger = Logger.getLogger(TraficoSearchAction.class);
  
   /**
   * Ejecuta la accion.
   * @throws ServletException Si ocurre un error durante el procesamiento
   *         de la accion.
   * @throws IOException Si no se puede abrir la vista solicitada por esta
   *         accion, la cual corresponde a una p�gina jsp.
   */
  public void run() throws ServletException
  {
    String next = null;
    // Perform search
    String numpla  = request.getParameter("numpla");
    String estador = request.getParameter("estador");    
    String origen  = request.getParameter("origen");    
    String destino = request.getParameter("destino");        
    String placa   = request.getParameter("placa");            
    if( numpla == null )
      next = "/jsp/sot/reports/FiltroTrafico.jsp";
    else{
      
      try
      {          
        HttpSession session = request.getSession();
        Usuario loggedUser = (Usuario)session.getAttribute("Usuario");
 
        model.traficoService.traficoSearch( numpla );
        model.traficoService.traficoObSearch( numpla );
        
        logger.info(  loggedUser.getNombre() + " ejecuto Consulta de Trafico: PLanilla " + numpla 
                        );  
        
      }catch (SQLException e){
          e.printStackTrace();
       // throw new ServletException(e.getMessage());
      }
      String param = "?numpla="+numpla+"&estador="+estador+"&origen="+origen+"&destino="+destino+"&placa="+placa;
      next = "/jsp/sot/reports/ReporteTrafico.jsp" + param;
    }
    // Redireccionar a la p�gina indicada.
    this.dispatchRequest(next);
  }  
}
