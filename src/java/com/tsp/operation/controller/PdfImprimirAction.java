
/*
 * PdfImprimirAction.java
 *
 * Created on 15 de enero de 2008, 05:56 PM
 */

package com.tsp.operation.controller;

import com.tsp.opav.model.services.ClientesVerService;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.GestionSolicitudAvalService;
import com.tsp.util.Utility;
import java.util.ArrayList;
import java.util.ResourceBundle;

import javax.servlet.*;
import javax.servlet.http.*;

/**
 *
-+
 
 +
 
 +* @author  Administrador
 */
public class PdfImprimirAction  extends Action {
    String next="";
    String opcion="imprimir_letras";
    BeanGeneral bg;
    boolean  redirect= true;
    /** Creates a new instance of PdfImprimirAction */
    public PdfImprimirAction() {
        //System.out.println("constructor de action");
    }
    
    public void run() throws ServletException {
        //System.out.println("inicio de action");
        
        HttpSession session   = request.getSession();
        Usuario     usuario   = (Usuario) session.getAttribute("Usuario");
        
        String codnegocio=(String)request.getParameter("codigo_negocio");
       //parametro para ve //parametro para verificar el tipo de documento a generar
         String tipoDocumento=(String)request.getParameter("pagare");
                      
        opcion=request.getParameter("opcion");
        
        String num_letras=request.getParameter("num_letras");
        String cc_representante=request.getParameter("cc_representante");
        String representada=request.getParameter("representada");
        String cc_deudor=request.getParameter("cc_deudor");
        String cc_codeudor=request.getParameter("cc_codeudor");
        String swcodeudor="concodeudor";
        if (cc_codeudor==null){
            swcodeudor="sincodeudor";
            cc_codeudor="";
        }
        
        bg=model.PdfImprimirSvc.getBeanGeneral();
        if (opcion.equals("imprimir_letras")){
            try{
                model.PdfImprimirSvc.setVariables(bg);
                //System.out.println("antes de reporte");
                model.PdfImprimirSvc.reporteLetras(model);
                //System.out.println("fin0");
                next="/pdf/rotu"+model.PdfImprimirSvc.getN()+".pdf";
                this.dispatchRequest(next);
                //System.out.println("fin1");
            }catch(Exception e){
                System.out.println("errorcillo"+e.toString()+"___"+e.getMessage());
            }
            //System.out.println("fin2");
        }                
        
        if (opcion.equals("imprimirpdfendosocodeudor") || opcion.equals("imprimirpdfendoso")){
            try{
                model.PdfImprimirSvc.setVariablesEndoso(num_letras,cc_representante,representada,cc_deudor,cc_codeudor,swcodeudor);
                //System.out.println("antes de reporte endoso");
                model.PdfImprimirSvc.reporteEndosos(model);
                //System.out.println("fin0 endoso");
                next="/pdf/rotu"+model.PdfImprimirSvc.getN()+".pdf";
                this.dispatchRequest(next);
                //System.out.println("fin1 endoso");
            }catch(Exception e){
                System.out.println("errorcillo endoso"+e.toString()+"___"+e.getMessage());
            }
        } 
               
        if (opcion.equals("imprimirpdfpagare")){
            try{ 
                
                String aprob=request.getParameter("aprob");
                String codneg2=request.getParameter("negocio");
                     String  resp="";
                String mensajePagare = "";
                

                String afavor=request.getParameter("afavor"); 
                
                
                //------- FV- 210 Impresion de pagares Persona Natural o Persona Juridica Factura Fenalco - Ing. Jose Avila - 24/07/2012  ------//
                if(codnegocio == null){
                    codnegocio = codneg2;
                }
                Negocios negocio = model.Negociossvc.buscarNegocio(codnegocio);
                Convenio convenio = model.gestionConveniosSvc.buscar_convenio(usuario.getBd(), negocio.getId_convenio() + "");
                GestionSolicitudAvalService gsas = new GestionSolicitudAvalService(usuario.getBd());
                SolicitudAval aval = gsas.buscarSolicitud(codnegocio);

             
                    SolicitudPersona sp = gsas.buscarPersona(Integer.parseInt(aval.getNumeroSolicitud()), "S");                
                    //aqui obtenemos el proveedor para el convenio educativo.
                    //Proveedor pro = model.proveedorService.obtenerProveedorPorNit(negocio.getNit_tercero());
                    //poner el numero de convenio de fenalco bolivar correspondiente a productivo y a piloto
                    String nit_tercero = (negocio.getId_convenio() == 17 || negocio.getId_convenio() == 31 || negocio.getId_convenio() == 35) ? "8020220161" : negocio.getNit_tercero();

                    Proveedor pro = model.proveedorService.obtenerProveedorPorNit(nit_tercero);
                    NitDAO nitDAO = new NitDAO(usuario.getBd());
                    NitSot objnit = nitDAO.searchNit(negocio.getNit_tercero());


                    if (aval != null && convenio.getTipo().equals("Consumo") && convenio.isAval_anombre() && convenio.isRedescuento()&&!convenio.getPrefijo_negocio().equals("FINTRA")) {

                        if (negocio.getTipoProceso().equals("DSR")) {
                            model.tablaGenService.buscarDatos("CONCEP_FAC", "DSR");
                        } else {
                            model.tablaGenService.buscarDatos("CONCEP_FAC", negocio.getTneg());
                        }
                      
                        TablaGen tbconceptos = model.tablaGenService.getTblgen();
                        BeanGeneral bg = model.Negociossvc.letras(codnegocio);      
                        
                        //bloque para validar el tipo de documento a generar.
                        boolean sw = false;
                        String url_pagare = "";
                        String url_factura = "";
                        resp = Utility.getIcono(request.getContextPath(), 5);
                       
                                                                    
                        //System.out.println(usuario.getLogin()); 
                        //Aqui asignamos a la variable validaUser si el usuario en session puede generar factura y pagare. 
                        ClientesVerService clvsrv =new ClientesVerService(usuario.getBd());
                        boolean validaUser =clvsrv.ispermitted(usuario.getLogin(),usuario.getCedula());
                        //inicio generacion de documentos....
                        //si es igual 'S' se genera la factura y el pagare
                        if (tipoDocumento.equals("S")) {

                           //model.pdfImprimirPagareService.generarPdfNuevoPagare(negocio, sp, pro, aval, usuario);
                           boolean pagareIsNew = model.pdfImprimirPagareService.generarPdfPagareVacio(negocio, aval, true, usuario);
                           mensajePagare = (pagareIsNew) ? "Documentos Generado Con Exito. Se ha generado un nuevo # de pagar�. Favor, imprimir y recoger firma al cliente.<br/><br/>":"N�mero de pagar� ya ha sido asignado.<br/><br/>";
                           //aqui validamos que solo los usuarios permitido vean la factura.
                            if (!model.Negociossvc.isNegocioAval(negocio.getCod_negocio()) && validaUser == true) {
                                if (!convenio.isMediador_aval()) {
                                    if (negocio.getEstado().equals("V") || negocio.getEstado().equals("A") || negocio.getEstado().equals("T")) {
                                        model.pdfImprimirFacturaAval.generarPdfFacturaAval(negocio, sp, objnit, aval, tbconceptos, usuario, convenio);
                                        sw = true;
                                    }
                                } else {
                                    model.pdfImprimirFacturaAval.generarPdfFacturaAval(negocio, sp, objnit, aval, tbconceptos, usuario, convenio);
                                    sw = true;
                                }
                            }
                            
                            //generar url para pagares y factura
                            url_pagare = (request.getContextPath() + "/pdf/pagarePersonaNatural.pdf");
                            url_factura = (request.getContextPath() + "/pdf/FacturaAval.pdf");

                            resp += "<span style='font-size:28px;'>" + mensajePagare + "</style>" + Utility.getIcono(request.getContextPath(), 8)  + " <a href='" + url_pagare + "' target=_blank >Ver Pagare</a><br/>";
                            if (sw) {
                                resp += Utility.getIcono(request.getContextPath(), 8) + " <a href='" + url_factura + "' target=_blank >Ver Factura </a>";
                            }

                        } else {

                            if (!model.Negociossvc.isNegocioAval(negocio.getCod_negocio()) && validaUser == true) {
                                if (!convenio.isMediador_aval()) {
                                    if (negocio.getEstado().equals("V") || negocio.getEstado().equals("A") || negocio.getEstado().equals("T")) {
                                        model.pdfImprimirFacturaAval.generarPdfFacturaAval(negocio, sp, objnit, aval, tbconceptos, usuario, convenio);
                                        sw = true;
                                    }
                                } else {
                                    model.pdfImprimirFacturaAval.generarPdfFacturaAval(negocio, sp, objnit, aval, tbconceptos, usuario, convenio);
                                    sw = true;
                                }
                            }
                            
                            //generar url de la factura
                            
                            url_factura = (request.getContextPath() + "/pdf/FacturaAval.pdf");
                            if (sw) {
                                resp = resp + Utility.getIcono(request.getContextPath(), 8) + " <a href='" + url_factura + "' target=_blank >Ver Factura </a>";
                            }else{
                              resp = resp+"Usted no tiene permisos para generar facturas";
                            }


                        }
                        //fin generacion de documentos.....
        
                    redirect= false;
                    this.printlnResponse(resp, "text/plain");
        
                        // next = "/jsp/fenalco/PagarePersonaNatuoJuri.jsp?pagarePerNatural=" + pagarePerNatural;

                    }else if(convenio.getTipo().equals("Microcredito") || convenio.getTipo().equals("Libranza") ||( convenio.getTipo().equals("Consumo") && convenio.getPrefijo_negocio().equals("FINTRA"))){
                        resp = Utility.getIcono(request.getContextPath(), 5);
                        if (tipoDocumento.equals("S")) {
                              boolean pagareIsNew = model.pdfImprimirPagareService.generarPdfPagareVacio(negocio, aval, false, usuario);
                              mensajePagare = (pagareIsNew) ? "Pagar� Generado Con Exito. Se ha generado un nuevo # de pagar�. Favor, imprimir y recoger firma al cliente.<br/><br/>":"N�mero de pagar� ya ha sido asignado.<br/><br/>";
                              resp += "<span style='font-size:28px;'>" + mensajePagare + "</style>";
                              resp +=  "<b>Pagare No: </b><span style='font-size:28px;color:red;font-weight:bold'>"+negocio.getNum_pagare()+"</style><br/>";
                        }
                        redirect= false;
                        this.printlnResponse(resp, "text/plain");
                    }                
                else 
                {
                  //  next = "/jsp/fenalco/PagarePersonaNatuoJuri.jsp?pagarePerNatural=0";
                
                String nombrecoodeudor=((String)request.getParameter("NomCood")).split("_-_-")[2].toUpperCase();
                String idcoodeudor=((String)request.getParameter("NomCood")).split("_-_-")[0];
                String codigoafiliado=request.getParameter("Celular");
                String numaprobacion=request.getParameter("aprob");
                String idendoso=request.getParameter("ccaut");
                String domiciliopago=request.getParameter("c_dir");
                String ciudadotorgamiento=request.getParameter("c_otor");
                String domi2=request.getParameter("domi2");
                String domi_cood=((String)request.getParameter("NomCood")).split("_-_-")[1];
                String tipoid=((String)request.getParameter("NomCood")).split("_-_-")[3];

                BeanGeneral bg = model.Negociossvc.letras(codneg2);
                if(afavor.equals("FINTRA"))
                {   bg.setValor_10("FINTRA SA");
                    bg.setValor_11("8020220161");
                    bg.setValor_13("BARRANQUILLA");
                }
                else
                {   if(afavor.equals("PROVINT"))
                    {   bg.setValor_10("PROVINTEGRAL LTDA");
                        bg.setValor_11("9000742958");
                        bg.setValor_13("BARRANQUILLA");
                    }
                }
                //System.out.println("codneggg"+codneg2+"nombrecoodeudor"+nombrecoodeudor+"idcoodeudor"+idcoodeudor+
                   //"codigoafiliado"+codigoafiliado+"numaprobacion"+numaprobacion+"idendoso"+idendoso+"domiciliopago"+domiciliopago+"ciudadotorgamiento"+ciudadotorgamiento);
                //String codneg="NG02422";
                ArrayList datos=new ArrayList();

                //model.PdfImprimirSvc.setVariablesPagare(model.Negociossvc.obtainDocsAceptados(codneg));
                //System.out.println("antes de reporte endoso");
                model.PdfImprimirSvc.generarPdfPagare(model.Negociossvc.obtainDocsAceptados(codneg2),bg,ciudadotorgamiento,domiciliopago,nombrecoodeudor,idcoodeudor,domi2,domi_cood,aprob,idendoso,afavor,tipoid);
                if(idcoodeudor.equals("____________"))
                {   idcoodeudor="";
                }
                model.Negociossvc.setCodeudor(codneg2,idcoodeudor,idendoso,domiciliopago,ciudadotorgamiento,domi2,afavor);
                //System.out.println("fin0 endoso");
                next="/pdf/pagare"+model.PdfImprimirSvc.getPagareN()+".pdf";
                
                //System.out.println("fin1 endoso");
            }
            }
               catch(Exception e){
                System.out.println("errorcillo imprimirpdfpagare"+e.toString()+"___"+e.getMessage());
            }
            //System.out.println("fin2 endoso");
            
            if (redirect == true) {
                    this.dispatchRequest(next);
                }
        }


             
    }
    
    
    
    
            public void printlnResponse(String respuesta, String contentType) throws Exception {
        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);

    }
    
    
}
