/*******************************************************************
 * Nombre clase: ClienteBuscarAction.java
 * Descripci�n: Accion para buscar una placa de la bd.
 * Autor: Ing. fily fernandez
 * Fecha: 16 de febrero de 2006, 05:41 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import com.tsp.exceptions.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import java.util.Vector;
import java.lang.*;
import java.sql.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import com.tsp.operation.model.services.*;
/**
 *
 * @author  jdelarosa
 */
public class ReporteIcaAction extends Action{
    
    /** Creates a new instance of PlacaBuscarAction */
    public ReporteIcaAction () {
    }
    
    public void run () throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String FechaI = request.getParameter ("fechai")!=null?request.getParameter ("fechai"):"0099-01-01";
        String FechaF = request.getParameter ("fechaf")!=null?request.getParameter ("fechaf"):"0099-01-01";
       String next = "/jsp/general/reporte/reporteIca.jsp?fechaf="+FechaI+"&fechaf="+FechaF;
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario)session.getAttribute ("Usuario");
        String usu = usuario.getLogin();
        String opcion = request.getParameter ("opcion")!=null?request.getParameter ("opcion"):"";
        try {
             model.reporteGeneralService.ReoporteIca(FechaI, FechaF);
            Vector vector =  model.reporteGeneralService.getVector();
           if ( vector.size() <= 0 ) {    
               next = next + "&msg=Su busqueda no arrojo resultados!";
                
           }else{
                 HReporteICA cli = new HReporteICA();
                 cli.start(FechaI, FechaF, usu);
          
                 next = next + "&msg=Se genero el excel !";
               
           }
           
             
               
            
        } catch ( Exception e ) {
            
            throw new ServletException ( "Error en Reporte de ReporteIcaAction : " + e.getMessage() );
            
        }        
        
        this.dispatchRequest ( next );
        
    }
    
}
