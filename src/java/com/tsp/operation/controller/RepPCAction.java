/********************************************************************
 *      Nombre Clase.................   RepFacturasClientAction.java
 *      Descripci�n..................   
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   27 de junio de 2006, 02:52 PM
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes S�nchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.ReportePCTh;

//dom4j
import org.dom4j.Element;
import org.dom4j.DocumentHelper;
import org.dom4j.Document;

//Logger
import org.apache.log4j.*;

/**
 *
 * @author  Ing. Andr�s Maturana De La Cruz
 */
public class RepPCAction extends Action{
    
    Logger log = Logger.getLogger (RepPCAction.class);
    
    /** Crea una nueva instancia de  RepVentasDiarioPDFAction */
    public RepPCAction() {
    }
    
    public void run() throws ServletException, InformationException {
        try{
            String fecha_ini = request.getParameter("FechaI");
            String fecha_fin = request.getParameter("FechaF");
            
            log.info("Fecha Inicial: " + fecha_ini);
            log.info("Fecha Final: " + fecha_fin);
            
            String next = "/jsp/trafico/reportes/RepPC.jsp?msg=Se ha iniciado la generaci�n del reporte exitosamente.";
            HttpSession session = request.getSession();
            Usuario user = (Usuario) session.getAttribute("Usuario");
            
            ReportePCTh hilo = new ReportePCTh();
            hilo.start(model, user, fecha_ini, fecha_fin);
           
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en RepFacturasClientAction .....\n" + e.getMessage());
        }
    }
    
}
