/********************************************************************
 *      Nombre Clase.................   PosBancariaRefreshAction.java
 *      Descripci�n..................   Actualiza o anula un registro del archivo posicion_bancaria
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   19.01.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
/**
 *
 * @author  Andres
 */
public class PosBancariaUpdateAction extends Action{
       
    /** Creates a new instance of PosBancariaRefreshAction */
    public PosBancariaUpdateAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/jsp/cxpagar/posbancaria/PosBancariaUpdate.jsp?mensaje=MsgModificado";
        
        String banco = request.getParameter("banco");
        String sucursal = request.getParameter("sucursal");
        String saldoi = request.getParameter("campo1");
        String saldoa = request.getParameter("saldo_ant");
        String anticipos = request.getParameter("campo3");
        String proveedores = request.getParameter("campo4");
        String cupo = request.getParameter("campo5");
        String agency_id = request.getParameter("agency_id");
        String fecha = request.getParameter("fecha");        
        String nsaldo = request.getParameter("nuevo_saldo");
        
        HttpSession session = request.getSession();
        Usuario user = (Usuario) session.getAttribute("Usuario");
        String cia = (String) session.getAttribute("Distrito");
        
        
        try{
            PosBancaria posb = new PosBancaria();
            posb.setCreation_user(user.getLogin());
            posb.setDstrct((String) session.getAttribute("Distrito"));
            posb.setBase(user.getBase());
            posb.setAgency_id(agency_id);
            posb.setFecha(fecha);
            posb.setBranch_code(banco);
            posb.setBank_account_no(sucursal);
            posb.setAnticipos(Double.parseDouble(anticipos.replaceAll(",","")));
            posb.setProveedores(Double.parseDouble(proveedores.replaceAll(",","")));
            posb.setCupo(Double.parseDouble(cupo.replaceAll(",","")));
            posb.setSaldo_anterior(Double.parseDouble(saldoa.replaceAll(",","")));
            posb.setSaldo_inicial(Double.parseDouble(saldoi.replaceAll(",","")));
            posb.setNuevo_saldo(Double.parseDouble(nsaldo.replaceAll(",","")));
            posb.setReg_status("");
            
            if( request.getParameter("anular")!=null ){
                posb.setReg_status("A");
                next = "/jsp/trafico/mensaje/MsgAnulado.jsp";
            } else {
                next += "&msg=Posici�n Bancaria modificada exitosamente.";
            }
            
//            String reg_status = model.posbancariaSvc.existe(cia, posb.getAgency_id(), posb.getBranch_code(), 
//                    posb.getBank_account_no(), posb.getFecha());
//            
//            model.posbancariaSvc.setPosbanc(posb);
//            
//            if( reg_status==null && (posb.getCupo()!=0 || posb.getSaldo_inicial()!=0) ){                            
//                model.posbancariaSvc.ingresar();               
//            } else {                
//                model.posbancariaSvc.actualizar();
//            }
//            
            String nomagencia = model.agenciaService.obtenerAgencia(agency_id).getNombre();
            request.setAttribute("agencia", nomagencia);
            request.setAttribute("agency_id", agency_id);
            request.setAttribute("banco", banco);
            request.setAttribute("sucursal", sucursal);
            request.setAttribute("fecha", fecha);
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
}
