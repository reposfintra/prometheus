/*
 * PlacaSearchAction.java
 *
 * Created on 24 de octubre de 2005, 02:48 PM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  Henry
 */
import java.io.*;
import com.tsp.exceptions.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;

public class PlacaSearchOtroAction extends Action{
    
    
    /** Creates a new instance of PlacaInsertAction */
        public PlacaSearchOtroAction() {
    }
    
    public void run() throws ServletException, InformationException {
       String next = "/jsp/hvida/buscarHVida.jsp?m=";
       
       HttpSession session = request.getSession();
       Usuario usuario = (Usuario)session.getAttribute("Usuario");       
       String conductor = (request.getParameter("conductor")!=null)?request.getParameter("conductor"):"";
       String propietario = (request.getParameter("propietario")!=null)?request.getParameter("propietario"):"";
       String placa = (request.getParameter("placa")!=null)?request.getParameter("placa"):"";
       String opcion = (request.getParameter("checkbox")!=null)?request.getParameter("checkbox"):"";              
       String cedula = "", busq=""; 
       ////System.out.println("Opcion:"+opcion);       
       if(opcion.equals("1")) {
               cedula = conductor;
               busq   = "Conductor"; 
       } else if(opcion.equals("2")) {
               cedula = propietario;
               busq   = "Propietario";
       } 
       next+="&busq="+busq+"&cedu="+cedula;
       try {
           if (!opcion.equals("")) {
               if(opcion.equals("3")) {
                       model.placaService.buscaPlaca2(placa);
                       if (model.placaService.getPlaca() != null) {
                               next = "/controller?estado=Placa&accion=Search&cmd=show&pag=hoja&idplaca="+placa;
                       }
               } else {
                       model.placaService.buscarConductorPropietario(busq,cedula);
               }
           } else {
               next+="&msg=Debe seleccionar una opcion de busqueda";
           }
       }catch (Exception e){e.printStackTrace();
               throw new ServletException(e.getMessage());       
       }      
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
    } 
}
