/*
 * ReporteCumplimientoAction.java
 *
 * Created on 14 de junio de 2006, 05:05 PM
 */

/***********************************************************************************
 * Nombre clase : ................ ReporteCumplimientoAction.java                  *
 * Descripcion :.................. Action para llamar al hilo                      * 
 *                                 ReporteCumplimientoColocacionXLS                *
 * Autor :........................ Osvaldo P�rez Ferrer                            *
 * Fecha :........................ 14 de Junio de 2006                             *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.threads.*;
/**
 *
 * @author  Osvaldo
 */
public class ReporteCumplimientoAction extends Action{
    
    /** Creates a new instance of ReporteCumplimientoAction */
    public ReporteCumplimientoAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        
        HttpSession session =  request.getSession();
        Usuario u = (Usuario)session.getAttribute("Usuario");
        String next = "/jsp/masivo/reportes/reporteCumplimiento.jsp";                
        String inicio = (String) request.getParameter("inicio");
        String fin    = (String) request.getParameter("fin");
        Vector reporte = null;
        try{                                                                       
                reporte = model.imprimirOrdenService.reporteCumplimiento(inicio, fin);                
                ReporteCumplimientoColocacionXLS rpt = new ReporteCumplimientoColocacionXLS();
                rpt.start(reporte, u.getLogin(), inicio, fin);
                request.setAttribute("mensaje", "Su proceso ha iniciado con exito!");
        }catch(SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    
    }
    
}
