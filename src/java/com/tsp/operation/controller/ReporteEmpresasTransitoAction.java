/*******************************************************************
 * Nombre clase: ClienteBuscarAction.java
 * Descripci�n: Accion para buscar una placa de la bd.
 * Autor: Ing. fily fernandez
 * Fecha: 16 de febrero de 2006, 05:41 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import com.tsp.exceptions.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import java.util.Vector;
import java.lang.*;
import java.sql.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import com.tsp.operation.model.services.*;
/**
 *
 * @author  jdelarosa
 */
public class ReporteEmpresasTransitoAction extends Action{
    
    /** Creates a new instance of PlacaBuscarAction */
    public ReporteEmpresasTransitoAction () {
    }
    
    public void run () throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String FechaI = request.getParameter ("FechaI")!=null?request.getParameter ("FechaI"):"";
        String FechaF  = request.getParameter ("FechaF")!=null?request.getParameter ("FechaF"):"";
        
        String Manifiesto  = request.getParameter ("Manifiesto")!=null?request.getParameter ("Manifiesto"):"";
        String Empresas  = request.getParameter ("Empresas")!=null?request.getParameter ("Empresas"):"";
        String Mercancias  = request.getParameter ("Mercancias")!=null?request.getParameter ("Mercancias"):"";
        String Personas  = request.getParameter ("Personas")!=null?request.getParameter ("Personas"):"";
        String Vehiculos  = request.getParameter ("Vehiculos")!=null?request.getParameter ("Vehiculos"):"";
      
        String next = "/jsp/general/reportes/Manifiesto/Empresas.jsp?";
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario)session.getAttribute ("Usuario");
        String usu = usuario.getLogin();
        int cont1=0;
        int cont2=0;
        int cont3=0;
        int cont4=0;
        int cont5=0;
        String Mensaje1 = "";
        String Mensaje2 = "";
        String Mensaje3 = "";
        String Mensaje4 = "";
        String Mensaje5 = "";
            
        
        
        try {
             if(Manifiesto.equals("1")){
                  model.transitoService.BusquedaMinifiesto(FechaI, FechaF);
                  Vector Mani =  model.transitoService.getVector(); 
              
                  if(Mani.size() <= 0){
                      Mensaje1 = "No encontro resultados de Manifiestos ";
                  }else{
                     ReporteMigracionManifiesto cli = new ReporteMigracionManifiesto();
                     cli.start(usu, FechaI,FechaF); 
                     Mani =  model.transitoService.getVector();
                      Mensaje1 = "Se ha migrado Manifiesto con exito ";
                  }   
             }
             if(Empresas.equals("1")){
                  model.transitoService.BusquedaEmpresas(FechaI, FechaF);
                  Vector Emp =  model.transitoService.getVector();

                  if(Emp.size() <= 0){
                       Mensaje2 = "No encontro resultados de Empresas ";
                  }else{
                       ReporteMigracionEmpresas cli = new ReporteMigracionEmpresas();
                       cli.start(usu, FechaI,FechaF); 
                       Emp =  model.transitoService.getVector();
                       Mensaje2 = "Se ha migrado Empresas con exito "; 
                  }
             }   
             if(Mercancias.equals("1")){
                 model.transitoService.BusquedaMercancias(FechaI, FechaF);
              Vector mer =  model.transitoService.getVector();
              
              if(mer.size() <= 0){
                  Mensaje3 = "No encontro resultados de Mercancias "; 
              }else{
                  ReporteMigracionMercancia cli = new ReporteMigracionMercancia();
                  cli.start(usu, FechaI,FechaF); 
                  mer =  model.transitoService.getVector();
                  Mensaje3 = "Se ha migrado Mercancias con exito "; 
                  }
                 
             }
             if(Personas.equals("1")){ 
                 model.transitoService.BusquedaConductores(FechaI, FechaF);
                 Vector per =  model.transitoService.getVector();

                  if(per.size() <= 0){
                      Mensaje4 = "No encontro resultados de Personas "; 
                      System.out.println("entro personas no");
                  }else{
                       ReporteMigracionConductores cli = new ReporteMigracionConductores();
                       cli.start(usu, FechaI,FechaF); 
                       per =  model.transitoService.getVector();
                       Mensaje4 = "Se ha migrado Personas con exito "; 
                       System.out.println("entro personas");
                  }
             } 
             if(Vehiculos.equals("1")){ 
                  model.transitoService.BusquedaVehiculos(FechaI, FechaF);
                  Vector Veh =  model.transitoService.getVector();

                  if(Veh.size() <= 0){
                      Mensaje5 = "No encontro resultados de Vehiculos "; 
                  }else{
                       ReporteMigracionVehiculos cli = new ReporteMigracionVehiculos();
                       cli.start(usu, FechaI,FechaF); 
                       Veh =  model.transitoService.getVector();
                       Mensaje5 = "Se ha migrado Vehiculos con exito "; 
                  }   
             }
             
                
             next = next + "&msg1="+Mensaje1+ "&msg2="+Mensaje2+ "&msg3="+Mensaje3+ "&msg4="+Mensaje4+ "&msg5="+Mensaje5;  
            
        } catch ( Exception e ) {
            
            throw new ServletException ( "Error en Reporte de ReporteManifiestoAction : " + e.getMessage() );
            
        }        
        
        this.dispatchRequest ( next );
        
    }
    
}
