/*
 * DespachomInsertarAction.java
 *
 * Created on 21 de noviembre de 2005, 06:41 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  dlamadrid
 */
public class DespachomInsertarAction extends Action
{
        
        /** Creates a new instance of DespachomInsertarAction */
        public DespachomInsertarAction ()
        {
        }
        public void run () throws ServletException, InformationException
        {
                String next="";
                try
                {
                        
                        HttpSession session = request.getSession ();
                        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
                        String userlogin=""+usuario.getLogin ();
                        
                        String distrito=""+ usuario.getDstrct ();
                        String placa=""+request.getParameter ("c_placa");
                        String conductor=""+request.getParameter ("c_conductor");
                        //System.out.println("conductor::::::::::::::::::::::::::::::::::::::::::"+conductor);
                        String nombreConductor=""+request.getParameter ("n_co");
                        String ruta = ""+request.getParameter ("c_cod_ruta");
                        String cliente =""+request.getParameter ("c_cliente");
                        String nombreCliente=""+request.getParameter ("n_cl");
                        String carga =""+request.getParameter ("c_carga");
                        String fecha_despacho=""+request.getParameter ("c_fecha");
                        //System.out.println("FECHA    "+fecha_despacho);
                        model.clienteService.vClientes (cliente);
                        
                        Vector vCon=new Vector();
                        String ms="";
                        boolean existe=false;
                        boolean sw=false;
                        boolean existeCliente=false;
                        boolean existeConductor=false;
                        DespachoManual despacho = new DespachoManual ();
                        despacho.setDstrct (distrito);
                        despacho.setPlaca (placa);
                        despacho.setConductor (conductor);
                        despacho.setNombreCliente (nombreCliente);
                        despacho.setNombreConductor (nombreConductor);
                        despacho.setRuta (ruta);
                        despacho.setCliente (cliente);
                        despacho.setCarga (carga);
                        despacho.setFecha_despacho (fecha_despacho);
                        despacho.setCreation_user (userlogin);
                        model.despachoManualService.setDespacho (despacho);
                        
                        sw= model.placaService.existePlaca (placa);
                        existeCliente= model.clienteService.existeCliente (cliente);
                        existeConductor= model.conductorService.existeConductor (conductor);
                        
                        existe=model.despachoManualService.existeDespacho (distrito, placa, fecha_despacho);
                        if(existeConductor==true){
                            if(existeCliente==true){
                                if (sw==true)
                                {
                                        if(existe ==false)
                                        {
                                              model.conductorService.obtConductoresPorNombre (conductor);
                                              vCon = model.conductorService.getConductores ();
                                              for(int i=0;i<vCon.size ();i++){
                                                  Vector fila = (Vector)vCon.elementAt (i);
                                                  nombreConductor=""+fila.elementAt (1);
                                              }
                                              despacho.setNombreConductor (nombreConductor);
                                              model.despachoManualService.setDespacho (despacho);
                                              
                                              model.despachoManualService.insertarDespacho ();
                                              ms="Registro Insertado";
                                        }
                                        else
                                        {
                                                ms="Ya existe ese despacho";
                                        }
                                }
                                else
                                {
                                        ms="No existe la placa";
                                }
                            }
                            else{
                              ms="No existe un Cliente con ese Codigo";

                            }
                        }
                        else{
                            ms="No existe un Conductor con esa Cedula";
                        }
                            
                        next="/jsp/trafico/despacho_manual/insertar.jsp?accion=1&ms="+ms;
                        
                }
                catch (Exception e)
                {
                        throw new ServletException (e.getMessage ());
                        // e.printStackTrace();
                }
                this.dispatchRequest (next);
        }
}
