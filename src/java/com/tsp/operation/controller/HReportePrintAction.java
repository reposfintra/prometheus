
package com.tsp.operation.controller;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;

public class HReportePrintAction extends Action {
  static Logger logger = Logger.getLogger(HReportePrintAction.class);
  
  public void run() throws ServletException, InformationException {   
      String next = null;
 
      HttpSession session = request.getSession();
      Usuario usuario = (Usuario)session.getAttribute("Usuario");
      String planilla = request.getParameter("planilla");
      String distrito = request.getParameter("cia");
      try{
         model.hReporteService.imprimirHojaReporte(distrito, planilla, usuario.getLogin());
         logger.info( usuario.getNombre() + " imprimi� Hoja de Reporte con: Planilla = " + planilla);
         next = "/hojareporte/PantallaReporteHR.jsp";
      }
      catch (SQLException e){
        throw new ServletException(e.getMessage());
      }
   
      // Redireccionar a la p�gina indicada.
      this.dispatchRequest(next);
  }
}
