/***************************************
 * Nombre Clase ............. ChequeXFacturaAction.java
 * Descripci�n  .. . . . . .  Genera el Cheque para una factura
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  23/12/2005
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  Transportes Sanchez Polo S.A.
 *******************************************/


package com.tsp.operation.controller;




import java.io.*;
import java.util.*;
import com.tsp.exceptions.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.Util;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.threads.*;




public class ChequeXFacturaAction  extends Action {
    
    private String PREFIJO_LASER = "LASER ";
    
    public void run() throws ServletException, InformationException {
        
        try{
            
            HttpSession session   = request.getSession();
            Usuario     usuario   = (Usuario) session.getAttribute("Usuario");
            
            
            String next           = "/jsp/cxpagar/impresion/ChequesXFactura.jsp?msj=";
            String evento         = request.getParameter("evento");
            String msj            = "";
            
            if ( evento!=null ){
                
                model.ChequeXFacturaSvc.setUsuario( usuario );
                
                // LIMPIA VARIABLES Y OBJETOS:
                if( evento.equals("CANCEL") ){
                    model.ChequeXFacturaSvc.resetDatos();                    
                }
                
                //BUSCAR EL NOMBRE DEL PROVEEDOR
                else if( evento.equals("REFRESHNOMBRE") ){
                    String nit = request.getParameter("provee");
                    
                    model.ChequeXFacturaSvc.searchProveedores( nit );
                    model.ChequeXFacturaSvc.setProveedor( nit );
                    List lista = model.ChequeXFacturaSvc.getProveedores();
                    model.ChequeXFacturaSvc.getListFacturasPro().clear(); 
                    
                    
                    if( lista != null && lista.size() > 0 ){
                        Hashtable ht = (Hashtable) lista.get(0);
                        request.setAttribute("nit", nit );
                        request.setAttribute("nombre_proveedor", ht.get("nombre") );
                        request.setAttribute("rpProveedor", ht.get("retpagoProveedor"));
                    }
                    
                }
                
                
                // BUSCA LAS FACTURAS DEL PROVEEDOR:
                else if( evento.equals("BUSCAR") ){
                    
                    String nit = request.getParameter("provee");
                    String nom = request.getParameter("nombre");
                    
                    model.ChequeXFacturaSvc.resetDatos();
                    model.ChequeXFacturaSvc.loadRequest(request);
                    model.ChequeXFacturaSvc.setProveedor( nit );
                    model.ChequeXFacturaSvc.searchFacturasProveedor();
                    List lista = model.ChequeXFacturaSvc.getListFacturasPro();                    
                    if(lista.size()==0)
                        msj = "El proveedor no tiene facturas disponibles...";                                        
                    request.setAttribute("nombre_proveedor", nom );
                }
                
                
                
                
                // CONSTRUYE EL CHEQUE A IMPRIMIR:
                else if( evento.equals("CHEQUE") ){
                    
                    model.ChequeXFacturaSvc.loadRequest(request);
                    //msj = model.ChequeXFacturaSvc.formarCheque();
                    msj = model.ChequeXFacturaSvc.formarPrecheque();
                    model.ChequeXFacturaSvc.loadNextCheque();
                    //Agencias para impresion
                    model.agenciaService.listarOrdenadas();
                    if( model.ChequeXFacturaSvc.getCheque() !=null)
                        next = "/jsp/cxpagar/impresion/viewChequeXFactura.jsp?msj=";
                }
                
                
                
                
                // IMPRIMIMOS EL CHEQUE
                else if ( evento.equals("IMPRIMIR")){
                    Cheque cheque    = model.ChequeXFacturaSvc.getCheque();
                    double vlrPagar  = Double.parseDouble( request.getParameter("vlr"));
                    String laser     = request.getParameter("laser")!=null? request.getParameter("laser") : "" ;//Osvaldo
                    // Redondemos valor a pagar de acuerdo a la moneda:
                    String  monedaBanco  = cheque.getMoneda();
                    vlrPagar  =  (  monedaBanco.equals("PES") ||  monedaBanco.equals("BOL")  )?(double)  Math.round( vlrPagar ) :  Util.roundByDecimal(vlrPagar, 2);//Modificado jpena 08-04-2010

                    
                    cheque.setVlrPagar( vlrPagar );
                    
                    
                    if( laser.equals("S") ){//Osvaldo - Se busca esquema de impresion en laser para el banco dado
                        List  esquema   = model.ImprimirChequeSvc.getEsquema( usuario.getDstrct(), PREFIJO_LASER + cheque.getBanco() );
                        if( esquema != null && esquema.size() > 0 ){
                            next = "/jsp/cxpagar/impresion/viewChequeXFactura2.jsp?laser="+laser+"&msj=";
                        }else{
                            msj = "No hay esquema de impresi�n L�ser para el banco "+ cheque.getBanco();
                            next = "/jsp/cxpagar/impresion/viewChequeXFactura2.jsp?laser=error&msj=";
                        }
                    }else{
                        next = "/jsp/cxpagar/impresion/ChequeCMS.jsp?msj=&laser="+laser;
                    }
                    
                }
                
                
                
                // ACTUALIZA DATOS CHEQUES
                else if ( evento.equals("ACTUALIZAR")){
                    Cheque cheque    = model.ChequeXFacturaSvc.getCheque();
                    double vlrPagar  = cheque.getVlrPagar();
                    
                    ArrayList<String> listSql = model.ChequeXFacturaSvc.updateChequePreCheque(vlrPagar,0);
                    model.ChequeXFacturaSvc.removePrecheque( cheque.getId_precheque() );
                    //String sql       = model.ChequeXFacturaSvc.guardarPreCheque(vlrPagar);
                    model.tService.crearStatement();
                    Iterator i = listSql.iterator();
                    while (i.hasNext()) {
                        String sql = (String) i.next();
                        System.out.println(sql);
                        model.tService.getSt().addBatch(sql);
                    }
                    ////System.out.println("CHEQUE \n"+sql);
                    model.tService.execute();
                    
                    msj = "El cheque n�mero "+ cheque.getNumero() + " del banco " +  cheque.getBanco() +" "+ cheque.getSucursal() +" por la suma de "+  Util.customFormat( cheque.getVlrPagar() )  +" a nombre de  "+   cheque.getNombre()  +", ha sido  impreso...";
                    
                    model.ChequeXFacturaSvc.loadNextCheque();
                    if( model.ChequeXFacturaSvc.getCheque() !=null){
                        next = "/jsp/cxpagar/impresion/viewChequeXFactura2.jsp?msj=";
                    }
                    else{
                        model.ChequeXFacturaSvc.resetDatos();
                        //model.ChequeXFacturaSvc.setPrecheques(null);                        
                        next = "/jsp/cxpagar/impresion/viewPrechequesXFactura.jsp?msj=";
                    }
                    
                }
                
                
                //SE GUARDA EL PRECHEQUE
                else if( evento.equals("PRECHEQUE") ){
                    
                    Cheque cheque    = model.ChequeXFacturaSvc.getCheque();
                    double vlrPagar  = Double.parseDouble( request.getParameter("vlr"));
                    
                    // Redondemos valor a pagar de acuerdo a la moneda:
                    String  monedaBanco  = cheque.getMoneda();
                  //vlrPagar  =  (  monedaBanco.equals("PES") ||  monedaBanco.equals("BOL")  )?(int   )  Math.round( vlrPagar ) :  Util.roundByDecimal(vlrPagar, 2);
                    vlrPagar  =  (  monedaBanco.equals("PES") ||  monedaBanco.equals("BOL")  )?(double)  Math.round( vlrPagar ) :  Util.roundByDecimal(vlrPagar, 2);//Modificado jpena 08-04-2010
                    
                    cheque.setVlrPagar( vlrPagar );
                    model.ChequeXFacturaSvc.setAgencia_impresion( request.getParameter("agencia") );
                    ArrayList<String> listSql  = model.ChequeXFacturaSvc.guardarPreCheque(vlrPagar);
                    
                    model.tService.crearStatement();
                    Iterator i = listSql.iterator();
                    while (i.hasNext()) {
                        String sql = (String) i.next();
                        System.out.println(sql);
                        model.tService.getSt().addBatch(sql);
                    }
                    model.tService.execute();
                    
                    msj = "El cheque del banco " +  cheque.getBanco() +" "+ cheque.getSucursal() +" por la suma de "+  Util.customFormat( cheque.getVlrPagar() ) + "  " + cheque.getMoneda() +" a nombre de  "+   cheque.getNombre()  +", ha sido generado...";
                    
                    model.ChequeXFacturaSvc.loadNextCheque();
                    if( model.ChequeXFacturaSvc.getCheque() !=null)
                        next = "/jsp/cxpagar/impresion/viewChequeXFactura.jsp?msj=";                    
                    else
                        model.ChequeXFacturaSvc.resetDatos();
                }
                
                else if( evento.equals("LIST_PRECHEQUES") ){
                    
                    model.ChequeXFacturaSvc.resetDatos();
                    model.ChequeXFacturaSvc.resetCheque();
                    model.ChequeXFacturaSvc.loadPrecheques();
                    next = "/jsp/cxpagar/impresion/viewPrechequesXFactura.jsp?msj=";
                    
                }
                
                else if(evento.equals("PRINT_PRECHEQUES")){
                    
                    String ids = request.getParameter("ids");                    
                    next = "/jsp/cxpagar/impresion/viewPrechequesXFactura.jsp?msj=";
                    
                    String[] split = ids.split(",");
                    
                    Vector v = model.ChequeXFacturaSvc.getPrecheques();                   
                    
                    Vector seleccionados = Util.toVector( split, true );
                    
                    msj = model.ChequeXFacturaSvc.formarPrechequesToCheque(seleccionados);
                                        
                    if( model.ChequeXFacturaSvc.getCheque() !=null){
                        next = "/jsp/cxpagar/impresion/viewChequeXFactura2.jsp?print=OK&msj=";
                    }
                    model.ChequeXFacturaSvc.loadNextCheque();
                    
                }else if( evento.equals("ANULATE_PRECHEQUES") ){
                    String ids = request.getParameter("ids");
                    String[] split = ids.split(",");
                    String in = "";
                    
                    for( int i=0; i<split.length; i++ ){
                        in += "'"+split[i]+"',";
                    }
                    in = in.substring( 0, in.length()-1);
                    
                    model.ChequeXFacturaSvc.anularPrecheque( in );
                    
                    model.ChequeXFacturaSvc.resetDatos();
                    model.ChequeXFacturaSvc.resetCheque();
                    model.ChequeXFacturaSvc.loadPrecheques();
                    next = "/jsp/cxpagar/impresion/viewPrechequesXFactura.jsp?msj=";
                    
                }
                
                
            }
            
            
            next+= msj;
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new Exception("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
            
        } catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        
    }
    
}
