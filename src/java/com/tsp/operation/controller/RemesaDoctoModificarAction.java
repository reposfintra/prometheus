/*
 * RemesaDoctoModificar.java
 *
 * Created on 2 de noviembre de 2005, 03:31 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  JuanM
 */
public class RemesaDoctoModificarAction extends Action{
    static Logger logger = Logger.getLogger(RemesaDoctoModificarAction.class);
    /** Creates a new instance of RemesaDoctoModificar */
    public RemesaDoctoModificarAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        try{
            
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            
            
            String nombre = request.getParameter("nombre");
            String numrem = request.getParameter("numrem");
            
            String next="/docremesas/ModificarDocRel.jsp?nombre="+nombre;
            
            Vector original = new Vector();
            
            Vector vec = new Vector();
            
            
            int cant = 0;
            String destinatario = request.getParameter("destinatario");
            
            
            java.util.Enumeration enum1;
            String parametro;
            enum1 = request.getParameterNames(); // Leemos todos los atributos del request
            while (enum1.hasMoreElements()) {
                parametro = (String) enum1.nextElement();
                if(parametro.indexOf("_")<0){
                    if(parametro.substring(0,2).equals("dp")){
                        if(!request.getParameter(parametro).equals("")){
                            int numero =Integer.parseInt( parametro.substring(2));
                            //SE ENCONTRARON LOS PADRES, SE CREAUN OBJETO DE TIPO REMESA DOC QUE OBTIENE LOS DATOS DEL PADRE
                            logger.info("Nombre del padre = "+parametro);
                            logger.info("Numero "+numero);
                            logger.info("Documento padre = "+request.getParameter(parametro));
                            
                            remesa_docto rem = new remesa_docto();
                            rem.setDestinatario(destinatario);
                            rem.setTipo_doc(request.getParameter("tp"+numero));
                            rem.setDocumento(request.getParameter(parametro));
                            
                            Vector hijos = new Vector();
                            // RECORRO NUEVAMENTE LOS PARAMETROS PARA OBTENER LOS HIJOS DE ESTE OBJETO
                            java.util.Enumeration enum2;
                            String parametro2;
                            enum2 = request.getParameterNames(); // Leemos todos los atributos del request
                            while (enum2.hasMoreElements()) {
                                parametro2 = (String) enum2.nextElement();
                                logger.info("NOMBRE PARAM = "+parametro2);
                                if(parametro2.indexOf("tp"+numero+"_TipodocRel")>=0){
                                    logger.info("ES UN HIJO!! "+parametro2);
                                    String vecS [] = parametro2.split("_");
                                    String numeroH = vecS[1].substring(10);
                                    String hijo2 = "tp"+numero+"_DocumentoRel"+numeroH;
                                    logger.info("Este es un hijo valor: "+request.getParameter(hijo2));
                                    remesa_docto remH = new remesa_docto();
                                    remH.setTipo_doc(request.getParameter(parametro2));
                                    remH.setDocumento(request.getParameter(hijo2));
                                    hijos.add(remH);
                                    
                                }
                            }
                            rem.setHijos(hijos);
                            vec.add(rem);
                            cant++;
                        }
                    }
                    
                }
                
            }
            
            
            
            logger.info("El tamano del vector es "+vec.size());
            if(vec.size()<5){
                for(int i=vec.size(); i<5; i++){
                    remesa_docto rem = new remesa_docto();
                    rem.setDestinatario(request.getParameter("destinatario"));
                    rem.setTipo_doc("");
                    rem.setDocumento("");
                    Vector hijos = new Vector();
                    remesa_docto remH = new remesa_docto();
                    remH.setTipo_doc("");
                    remH.setDocumento("");
                    rem.setHijos(hijos);
                    vec.add(rem);
                }
                
                // this.dispatchRequest(next);
            }
            model.RemDocSvc.setDocumentos(vec);
            
            
            for( int i = 0; i < vec.size(); i++ ){
                
                remesa_docto r = (remesa_docto)vec.elementAt(i);
                
                for( int j = 0; j < r.getHijos().size(); j++ ){
                    remesa_docto r2 = (remesa_docto)r.getHijos().elementAt(j);
                    logger.info("Documento " + r.getDocumento() + "Tipo documento " + r.getTipo_doc() + " HIJO " + r2.getDocumento() + " " + r2.getTipo_doc() );
                }
            }
            
            if(model.remesaService.getRemesa()!=null){
                logger.info("La remesa existe y es de tipo "+model.remesaService.getRemesa().getTipo_documento());
                numrem = model.remesaService.getRemesa().getNumrem();
                if(model.remesaService.getRemesa().getTipo_documento().equals("002")){
                    model.RemDocSvc.DELETEREL(numrem, request.getParameter("destinatario"));
                    model.RemDocSvc.INSERTDOCREL(vec, usuario.getDstrct(), numrem, request.getParameter("destinatario"),usuario.getLogin());
                    model.RemDocSvc.LISTVC(numrem, destinatario);
                }
                else{
                    model.RemDocSvc.DeleteDocOcargue(numrem, request.getParameter("destinatario"));
                    
                    Vector consultas = new Vector();
                    String insertDocRel=model.RemDocSvc.insertOCargue_docto(vec, usuario.getDstrct(), numrem, destinatario, usuario.getLogin());
                    String  inserts2[]=insertDocRel.split(";");
                    for(int j = 0; j<inserts2.length;j++){
                        consultas.add(inserts2[j]);
                    }
                    model.despachoService.insertar(consultas);
                    model.RemDocSvc.listarDocOCargue(numrem,destinatario);
                }
                
            }
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en ReporteEgresoAction .....\n"+e.getMessage());
        }
    }
    
    
    
}

