/********************************************************************
 *      Nombre Clase.................   TraficoElimiconAction.java
 *      Descripci�n..................   Action que se encarga de eliminar un filtro creado por el usuario
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  David A
 */
public class TraficoElimiconAction extends Action{
    
    /** Creates a new instance of TraficoElimiconAction */
    public TraficoElimiconAction() {
    }
     
    public void run() throws ServletException, InformationException {
         try{
            String user        =""+ request.getParameter ("usuario");
            String idconsulta  =""+ request.getParameter ("consultas");
            String zonasUsuario = request.getParameter("zonasUsuario");//23.06.2006
            ////System.out.println("idconsulta"+idconsulta);
 
            model.traficoService.eliminararFiltro(idconsulta);
          
            String next = "/jsp/trafico/controltrafico/filtro.jsp?"+"zonasUsuario="+zonasUsuario;
            ////System.out.println("next en Consultas"+next);  
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null){
                throw new Exception("No se pudo encontrar "+ next);
            }    
            rd.forward(request, response); 
        }
        catch(Exception e){
          throw new ServletException("Accion:"+ e.getMessage());
        } 
        
        
    }
    
}  