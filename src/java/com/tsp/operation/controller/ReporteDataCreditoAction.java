/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.tsp.exceptions.InformationException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.threads.*;
import java.util.ArrayList;

/**
/**
 *
 * @author maltamiranda
 */
public class ReporteDataCreditoAction extends Action {

    @Override
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String filtro = request.getParameter("filtro");
            String nomselect = request.getParameter("nomselect");
            String parametro = request.getParameter("parametro");
            String opcion = request.getParameter("opcion");
            ReporteDataCreditoService rdc = new ReporteDataCreditoService();
            String idclie = (request.getParameter("idclie").equals("")) ? request.getParameter("codcli") : request.getParameter("idclie");
            String rangoi = request.getParameter("rangoi");
            String rangof = request.getParameter("rangof");
            String negocio = request.getParameter("negocio");
            String proveedor = request.getParameter("idafl");
            String fechaReporte = request.getParameter("fechaReporte");
            String next = "/jsp/datacredito/resultadoDataCredito.jsp";
            String[] obligaciones = request.getParameterValues("obligaciones");
            int columna = Integer.parseInt((request.getParameter("columna") != null && !request.getParameter("columna").equals("")) ? request.getParameter("columna") : "10");
            int columnaII = Integer.parseInt((request.getParameter("columnaII") != null && !request.getParameter("columnaII").equals("")) ? request.getParameter("columnaII") : "8");
            if (parametro.equals("clselect")) {
                response.setContentType("text/plain; charset=UTF-8");
                response.setHeader("Cache-Control", "no-cache");
                response.getWriter().println(rdc.getClientes(filtro, nomselect, ((request.getParameter("idclie") != null) ? request.getParameter("idclie") : "")));
            } else {
                if(parametro.equals("aflselect")){
                    response.setContentType("text/plain; charset=UTF-8");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().println(rdc.getAfiliados(filtro, nomselect, ((request.getParameter("idafl") != null) ? request.getParameter("idafl") : "")));
                } else {
                    if (parametro.equals("BUSCAR_NEGOCIOS")) {
                        session.removeAttribute("datacredito");
                        session.setAttribute("datacredito", rdc.buscar_negocios(idclie, negocio, proveedor, rangoi, rangof, columna, fechaReporte));
                        next = next + "?pagina=0" + "&opcion=" + opcion;
                    } else {
                        if (parametro.equals("PAGINACION")) {
                            next = next + "?pagina=" + request.getParameter("pagina") + "&opcion=" + opcion;
                        } else {
                            if (parametro.equals("AGREGAR_ACUMULADO")) {
                                session.removeAttribute("datacredito");
                                session.setAttribute("datacredito", rdc.agregar_negocios_acumulado(idclie, negocio, proveedor, rangoi, rangof, columna, obligaciones, usuario.getLogin(), fechaReporte));
                                next = next + "?pagina=" + request.getParameter("pagina") + "&opcion=" + opcion;
                            } else {
                                if (parametro.equals("ORDENAMIENTO")) {
                                    if (!opcion.equals("subtabla")) {
                                        session.removeAttribute("datacredito");
                                        session.setAttribute("datacredito", rdc.buscar_negocios(idclie, negocio, proveedor, rangoi, rangof, columna, fechaReporte));
                                    } else {
                                        session.removeAttribute("acumulados");
                                        session.setAttribute("acumulados", rdc.buscar_acumulados(columnaII));
                                    }
                                    next = next + "?pagina=0&opcion=" + opcion;
                                } else {
                                    if (parametro.equals("VER_ACUMULADOS")) {
                                        session.removeAttribute("acumulados");
                                        session.setAttribute("acumulados", rdc.buscar_acumulados(columnaII));
                                        next = next + "?pagina=0&opcion=" + opcion;
                                    } else {
                                        if (parametro.equals("ELIMINAR_ACUMULADOS")) {
                                            session.removeAttribute("acumulados");
                                            session.setAttribute("acumulados", rdc.eliminar_negocios_acumulado(idclie, negocio, rangoi, rangof, columnaII, obligaciones));
                                            next = next + "?pagina=" + request.getParameter("pagina") + "&opcion=" + opcion;
                                        } else {
                                            if (parametro.equals("LIMPIAR_ACUMULADOS")) {
                                                session.removeAttribute("acumulados");
                                                rdc.limpiar_negocios_acumulado();
                                            } else {
                                                if (parametro.equals("GENERAR_REPORTE")) {
                                                    HGenerarArchivoDataCredito hilo = new HGenerarArchivoDataCredito();
                                                    hilo.start(model, usuario,"REPORTE DATACREDITO");
                                                    response.setContentType("text/plain; charset=UTF-8");
                                                    response.setHeader("Cache-Control", "no-cache");
                                                    response.getWriter().println("El proceso ha sido iniciado, porfavor verifique el estado en el log de procesos");
                                                }
                                                else{
                                                    if(parametro.equals("EXPORTAR_EXCEL")){
                                                        HExportarExcel hilo = new HExportarExcel();
                                                        hilo.start(model,(ArrayList)session.getAttribute("acumulados") , usuario,"REPORTE DATACREDITO");
                                                        response.setContentType("text/plain");
                                                        response.setHeader("Cache-Control", "no-cache");
                                                        response.getWriter().println("El proceso ha sido iniciado, porfavor verifique el estado en el log de procesos");
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if (!parametro.equals("clselect") && !parametro.equals("aflselect") && !parametro.equals("LIMPIAR_ACUMULADOS") && !parametro.equals("GENERAR_REPORTE") && !parametro.equals("EXPORTAR_EXCEL")) {
                this.dispatchRequest(next);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }
}
