/*************************************************************************
 * Nombre:        ReferenciaUpdateAction.java         
 * Descripci�n:   Clase Action para modificar actividad    
 * Autor:         Ing. Henry Osorio 
 * Fecha:         26 de septiembre de 2005, 04:00 PM                           
 * Versi�n        1.0                                       
 * Coyright:      Transportes Sanchez Polo S.A.            
 *************************************************************************/


package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*; 
import com.tsp.util.Util;

public class ReferenciaUpdateAction extends Action{
    
    /** Creates a new instance of ReferenciaInsertAction */
    public ReferenciaUpdateAction() {
    }
    public void run() throws ServletException {
        Referencia referencia = new Referencia();                
        String doc = request.getParameter("documento");
        String tipo = request.getParameter("tipo");
        String msg = "Datos modificados exitosamente";
        String next = "/jsp/hvida/referencias/referenciaUpdate.jsp?documento="+doc+"&tipo="+tipo+"&msg=";
        try{ 
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");  
            referencia.setClase(doc);
            referencia.setBase(usuario.getBase());
            referencia.setUser_update(usuario.getLogin());
            referencia.setLast_update("now()");   
            referencia.setContacto("");
            referencia.setCargo_contacto("");
            referencia.setReferencia("");
            referencia.setRelacion("");                        
            referencia.setCiudaddir("");                        
            referencia.setCiudadtel("");  
            referencia.setDireccion(""); 
            referencia.setOtra_relacion(""); 
            
            Vector vec = model.referenciaService.getVectorReferencias();
            int num[] = model.referenciaService.getOcurrenciasTipoReferencias(vec);
            boolean sw = false;
            //actualizacion de Empresa afiliada
            for (int i=1; i<=num[0]; i++){
                //primary key
                referencia.setTiporef(request.getParameter("tiporeferenciaEA"+i));
                referencia.setFecref(request.getParameter("fecreferenciaEA"+i));
                referencia.setDstrct_code(request.getParameter("dstrct_codeEA"+i));                     
                //
                referencia.setNomempresa(request.getParameter("nombreEA"+i));
                referencia.setTelefono(request.getParameter("telefonoEA"+i));
                referencia.setCiudadtel(request.getParameter("telciuEA"+i));
                referencia.setContacto(request.getParameter("contactoEA"+i));
                referencia.setCargo_contacto(request.getParameter("cargoEA"+i));
                referencia.setReferencia(request.getParameter("referenciaEA"+i));
                model.referenciaService.updateReferencia(referencia);
                sw = true;
                
            }
             //actualizacion de Referencia conductor
            for (int i=1; i<=num[1]; i++){                
                //primary key
                referencia.setTiporef(request.getParameter("tiporeferenciaCO"+i));                
                referencia.setFecref(request.getParameter("fecreferenciaCO"+i));
                referencia.setDstrct_code(request.getParameter("dstrct_codeCO"+i));         
                //
                referencia.setNomempresa(request.getParameter("nombreCO"+i));
                referencia.setTelefono(request.getParameter("telefonoCO"+i));
                referencia.setCiudadtel(request.getParameter("telciuCO"+i));
                referencia.setDireccion(request.getParameter("direccionCO"+i));
                referencia.setCiudaddir(request.getParameter("ciudaddirCO"+i));
                referencia.setReferencia(request.getParameter("referenciaCO"+i));
                referencia.setRelacion(request.getParameter("relacionCO"+i));
                referencia.setOtra_relacion(request.getParameter("otra_relCO"+i).trim());
                model.referenciaService.updateReferencia(referencia);
                sw = true;
            } 
            //actualizacion Referencia Propietario            
            for (int i=1; i<=num[2]; i++){                
                //primary key
                referencia.setTiporef(request.getParameter("tiporeferenciaPR"+i));                
                referencia.setFecref(request.getParameter("fecreferenciaPR"+i));
                referencia.setDstrct_code(request.getParameter("dstrct_codePR"+i));         
                //
                referencia.setNomempresa(request.getParameter("nombrePR"+i));
                referencia.setTelefono(request.getParameter("telefonoPR"+i));
                referencia.setCiudadtel(request.getParameter("telciuPR"+i));
                referencia.setDireccion(request.getParameter("direccionPR"+i));
                referencia.setCiudaddir(request.getParameter("ciudaddirPR"+i));
                referencia.setReferencia(request.getParameter("referenciaPR"+i));
                referencia.setRelacion(request.getParameter("relacionPR"+i));                
                referencia.setOtra_relacion(request.getParameter("otra_relPR"+i).trim());
                model.referenciaService.updateReferencia(referencia);
                sw = true;
            }
             //actualizacion de Empresa EC
            for (int i=1; i<=num[3]; i++){                
                //primary key
                referencia.setTiporef(request.getParameter("tiporeferenciaEC"+i));                
                referencia.setFecref(request.getParameter("fecreferenciaEC"+i));
                referencia.setDstrct_code(request.getParameter("dstrct_codeEC"+i));         
                referencia.setNomempresa(request.getParameter("nombreEC"+i));
                referencia.setTelefono(request.getParameter("telefonoEC"+i));
                referencia.setCiudadtel(request.getParameter("telciuEC"+i));
                referencia.setDireccion(request.getParameter("direccionEC"+i));
                referencia.setCiudaddir(request.getParameter("ciudaddirEC"+i));
                referencia.setReferencia(request.getParameter("referenciaEC"+i));
                referencia.setCargo_contacto(request.getParameter("cargoEC"+i));
                referencia.setContacto(request.getParameter("contactoEC"+i));
                referencia.setRelacion("");
                model.referenciaService.updateReferencia(referencia);
                sw = true;
            }        
            if (sw==true)
                next+=msg;
            model.referenciaService.buscarReferencias(doc);
        }catch (Exception e){            
            throw new ServletException(e.getMessage());                       
        }        
        this.dispatchRequest(next); 
    }   
}
