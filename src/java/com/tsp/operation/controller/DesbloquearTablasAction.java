/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.DesbloquearTablasDAO;
import com.tsp.operation.model.DAOS.impl.DesbloquearTablasImpl;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.Usuario;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

/**
 *
 * @author mariana
 */
public class DesbloquearTablasAction extends Action {

    Usuario usuario = null;
    private final int CARGAR_TABLAS = 1;
    private final int UPDATE_MASIVO = 2;
    private DesbloquearTablasDAO dao;

    @Override
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            dao = new DesbloquearTablasImpl(usuario.getBd());
            int opcion = (request.getParameter("opcion") != null ? Integer.parseInt(request.getParameter("opcion")) : -1);
            switch (opcion) {
                case CARGAR_TABLAS:
                    cargarTablas();
                    break;
                case UPDATE_MASIVO:
                    updateMasivo();
                    break;
            }
        } catch (Exception e) {
        }
    }

    public void cargarTablas() {
        try {
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.cargarTablas()) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
            Logger.getLogger(DesbloquearTablasAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void updateMasivo() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            TransaccionService tService = new TransaccionService(this.usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.updateMasivo());
            tService.execute();

            //this.printlnResponse(, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"ERROR\"}";
        }
    }

    /**
     * Escribe la respuesta de una opcion ejecutada desde AJAX
     *
     * @param respuesta
     * @param contentType
     * @throws Exception
     */
    private void printlnResponse(String respuesta, String contentType) throws Exception {

        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);
    }
}
