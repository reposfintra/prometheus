/***************************************************************************
 * Nombre clase :                 ReporteCumplidoAction.java               *
 * Descripcion :                  Clase que maneja los eventos             *
 *                                relacionados con el programa de          *
 *                                generacion del Reporte de                *
 *                                cumplidos                                *
 * Autor :                        Ing. Leonardo Parody                     *
 * Fecha :                        13 de enero de 2006, 04:24 PM            *
 * Version :                      1.0                                      *
 * Copyright :                    Fintravalores S.A.                  *
 ***************************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import com.tsp.exceptions.*;


public class ReporteCumplidoAction extends Action {
    
    public ReporteCumplidoAction() {
        
    }
    
    public void run() throws javax.servlet.ServletException, InformationException{
        try {
            String next="/jsp/cumplidos/reporte_cumplidos/ReporteCumplido.jsp?Mensaje=Su peticion a sido procesada";
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario)session.getAttribute("Usuario");
            String user = usuario.getLogin();

            /*Vector de String con las columnas que seran visibles en el repote de cumplidos*/
            String []LOV                  = request.getParameterValues("LOV");

            Calendar fechaHoy = Calendar.getInstance();
            java.util.Date fech = fechaHoy.getTime();
            SimpleDateFormat fec = new SimpleDateFormat("yyyy-MM-dd");            
            String Fecha = fec.format(fech);

            ReportePlaRemPorFechaXLS hilo = new ReportePlaRemPorFechaXLS();

            hilo.start(request.getParameter("FechaI"),request.getParameter("FechaF"),request.getParameter("c_agencia").toUpperCase(), user, Fecha, LOV, request.getParameter("usuario"));
              
            // Redireccionar a la p�gina indicada.
            this.dispatchRequest(next);
        } catch(Exception e){
            e.printStackTrace();                        
        }
    }
    
}
