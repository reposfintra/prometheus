/*
 * RecursosDisponiblesAction.java
 *
 * Created on 14 de julio de 2006, 05:23 PM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  Osvaldo
 */

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import java.sql.Timestamp;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;


public class RecursosDisponiblesAction extends Action{
    
    /** Creates a new instance of RecursosDisponiblesAction */
    public RecursosDisponiblesAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next="";
        String base = "";
        String codigo = "";
        String opc = request.getParameter("opcion");
        HttpSession session = request.getSession();
        Usuario u = (Usuario) session.getAttribute("Usuario");
        
        try{
            if(opc.equals("filter")){
                
                Vector rec = model.rdSvc.obtenerRecursos();
                model.tablaGenService.buscarRegistrosOrdenadosByDesc("WG");
                LinkedList wg = model.tablaGenService.obtenerTablas();
                
                request.setAttribute("wg",  wg);
                request.setAttribute("recursos" , rec );
                next="/pags_predo/recursos_disp/recursosDispFiltros.jsp";
            }
            else if( opc.equals("search") ){
                
                Vector v;
                String placa = request.getParameter("placa").toUpperCase();
                String recurso = request.getParameter( "recurso" );
                String origen = request.getParameter( "origen" ).toUpperCase();
                String destino = request.getParameter( "destino" ).toUpperCase();
                String wg = request.getParameter( "work_group" );
                                                
                String where = this.buildWhere( placa, recurso, origen, destino, wg );
                
                model.rdSvc.obtenerRecursosByWhere( where, u.getDstrct() );
                v = model.rdSvc.getRecursosdisp();
                
                if( v.size() > 0 ){                    
                    request.setAttribute("recursos", v);
                }else{
                    request.setAttribute( "mensaje", "Su b�squeda no produjo resultados" );
                }
                
                next="/pags_predo/recursos_disp/recursosDispListar.jsp";
                
            }
            else if( opc.equals("update") ){
                
                String placa = request.getParameter("placa");
                String fecha = request.getParameter("fecha");
                
                request.setAttribute("placa", placa);
                request.setAttribute("fecha", fecha);
                
                next="/pags_predo/recursos_disp/recursosDispMod.jsp";
            }
            else if( opc.equals("asign") ){
                
                String placa = request.getParameter("placa");
                String fechadisp = request.getParameter("fechadisp");
                String ag = request.getParameter("ciudad").toUpperCase();
                String fecharec = request.getParameter("fecharec");
                String tiempo = request.getParameter("tiempo");
                
                fechadisp = fechadisp+":00";
                Util ut = new Util();
                
                if( !model.VExternosSvc.EXISTE(placa, ut.ConvertiraTimestamp( fechadisp )) ){
                    model.VExternosSvc.INSERT( placa, fechadisp, Integer.parseInt(tiempo), ag, u.getBase(), u.getLogin(), u.getDstrct() );
                    
                    model.rdSvc.updateRecursoAsignar( placa, fecharec, ag, fechadisp, u.getLogin() );
                    
                    request.setAttribute("mensaje", "Recurso retornado exit�samente" );
                    request.setAttribute("modified", "" );
                }
                else{
                    request.setAttribute("mensaje", "Ya se encuentra asignado este recurso para la fecha: "+fechadisp );
                }
                request.setAttribute("fecharec", fecharec);
                request.setAttribute("placa", placa);
                next="/pags_predo/recursos_disp/recursosDispMod.jsp";
            }
            
            
        }catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
    
    public String buildWhere(String placa, String recurso, String origen, String destino, String wg){
        
        String where = "";
        int and = 0;
        String sql_origen = "AND placa NOT IN "+

                            " (SELECT placa "+

                            " FROM recursos_disp "+

                            " WHERE destino = '"+origen+"')";                    
                                      
        
        if( (placa+recurso+origen+destino+wg).length() > 0){
            
            if( placa.length() > 0 ){
                placa = "## placa = '"+placa+"'";
                and ++;
            }
            if( recurso.length() > 0 ){
                recurso = "## recursos_disp.recurso = '"+recurso+"'";
                and++;
            }
            if( origen.length() > 0 ){
                origen = "## recursos_disp.origen = '"+origen+"' "+sql_origen;
                and++;
            }
            if( destino.length() > 0 ){
                destino = "## recursos_disp.destino = '"+destino+"'";
                and++;
            }
            if ( wg.length() > 0 ){
                wg =  "## placa IN (SELECT placa "+
                               
                      "FROM wgroup_placa WHERE work_group='"+wg+"')";;
                and++;
            }
            
            if( and>1 ){
                where = placa+recurso+origen+destino+wg;
                where = where.replaceAll("##",  " AND " );
                where = where.substring( 4, where.length() );
            }
            else{
                where = placa+recurso+origen+destino+wg;
                where = where.substring( 2, where.length() );
            }
        }
        
        return where;
    }
    
}
