
package com.tsp.operation.controller;

import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.threads.HExportarExcel;
import com.tsp.operation.model.threads.HGenerarArchivoCorficolombiana;
import javax.servlet.http.HttpSession;


public class ReporteCorficolombianaAction  extends Action{
   //protected HttpServletRequest request;
    public ReporteCorficolombianaAction () {

    
    }

    public void run() throws ServletException, InformationException
    {   
        String id_reporte, tipo, periodo,parametro,reporte;
        HttpSession session  = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        id_reporte=(request.getParameter("reporte")!=null)?request.getParameter("reporte").split(";;;")[0]:"";
        reporte=(request.getParameter("reporte")!=null)?request.getParameter("reporte").split(";;;")[1]:"";
        periodo=request.getParameter("periodo");
        tipo=request.getParameter("tipo");
        parametro=request.getParameter("parametro");
        String next="/jsp/reportes/tabla_reporte.jsp";
        try{
            /*response.setContentType("text/plain");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println(model.corficolombianaSvc.getConsulta(id_reporte, tipo, periodo));*/

            if(parametro.equals("EXPORTAR")){
                HExportarExcel hilo = new HExportarExcel();
                hilo.start(model,model.corficolombianaSvc.getResultado(), usuario,"REPORTE CORFICOLOMBIANA");
                response.setContentType("text/plain");
                response.setHeader("Cache-Control", "no-cache");
                response.getWriter().println("El proceso ha sido iniciado, porfavor verifique el estado en el log de procesos");
            }
            else{
                if(parametro.equals("GENERAR_REPORTE")){
                    HGenerarArchivoCorficolombiana hilo = new HGenerarArchivoCorficolombiana();
                    model.corficolombianaSvc.getConsulta(id_reporte, tipo, periodo,parametro);
                    hilo.start(model,model.corficolombianaSvc.getResultado(), usuario,reporte);
                    response.setContentType("text/plain");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().println("El proceso ha sido iniciado, porfavor verifique el estado en el log de procesos");
                }
                else{
                    model.corficolombianaSvc.getConsulta(id_reporte, tipo, periodo,parametro);
                    this.dispatchRequest(next+"?parametro="+parametro);
                }
            }
        }
        catch(Exception e){
            e.printStackTrace();
            e.toString();
        }
    }

}
