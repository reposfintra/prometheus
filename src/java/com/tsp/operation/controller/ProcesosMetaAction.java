/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.ProcesosMetaDAO;
import com.tsp.operation.model.DAOS.UnidadesNegocioDAO;
import com.tsp.operation.model.DAOS.impl.ProcesosMetaImpl;
import com.tsp.operation.model.DAOS.impl.UnidadesNegocioImpl;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.ProcesoMeta;
import com.tsp.operation.model.beans.UnidadesNegocio;
import com.tsp.operation.model.beans.Usuario;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

/**
 *
 * @author desarrollo
 */
public class ProcesosMetaAction extends Action{
    
    private final int CARGAR_PROCESOS_META = 1;
    private final int GUARDAR_PROCESOS_META = 2;
    private final int ACTUALIZAR_PROCESO_META = 3;
    private final int CARGAR_PROCESO_INTERNO = 4;
    private final int GUARDAR_PROCESO_INTERNO = 5;
    private final int CARGAR_UNIDADES_NEGOCIO = 6;
    private final int ASOCIAR_UNIDADES_NEGOCIO = 7;
    private final int CARGAR_UND_NEGOCIOS_PROINTERNO = 8;
    private final int ACTUALIZAR_PROINTERNO = 9;
    private final int DESASOCIAR_UND_PROINTERNO = 10;
    private final int LISTAR_USUARIOS = 11;
    private final int LISTAR_PROINTERNO_USUARIO = 12;
    private final int LISTAR_PROINTERNO = 13;
    private final int ASOCIAR_PROINTERNO_USUARIO = 14;
    private final int ANULAR_PROCESO_META = 15;
    private final int ANULAR_PROCESO_INTERNO = 16;
    private final int CARGAR_CBO_PROCESOS_META = 17;
    private final int LISTAR_USUARIOS_PROINTERNO = 18;
    private final int LISTAR_USUARIOS_REL_PROINTERNO = 19;
    private final int ASOCIAR_USUARIO_PROINTERNO = 20;
    private final int DESASOCIAR_USUARIO_PROINTERNO = 21;
    private final int EXISTE_USUARIO_REL_PROCESO = 22;
    private final int CARGAR_METAS_ANULADOS = 23;
    private final int ACTIVAR_PROCESO_META = 24;
    private final int CARGAR_PROCESOS_ANULADOS = 25;
    private final int ACTIVAR_PROCESO_INTERNO = 26;
    private final int ACTUALIZAR_MODERADOR_PROINTERNO = 27;
    private final int BUSCAR_USUARIOS_REL_PROINTERNO = 28;
    private final int CARGAR_REL_PROCESOS_USUARIO = 29;
    private final int CARGAR_REL_TIPO_REQ_USUARIO = 30;
    private final int ASIGNAR_PROCESOS_REQ_USUARIO = 31;
    private final int ASIGNAR_TIPOS_REQ_USUARIO = 32;
    private ProcesosMetaDAO dao;
    Usuario usuario = null;

    @Override
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            dao = new ProcesosMetaImpl(usuario.getBd());
            int opcion = (request.getParameter("opcion") != null)
                    ? Integer.parseInt(request.getParameter("opcion"))
                    : -1;
            switch (opcion) {
                case CARGAR_PROCESOS_META:
                    this.cargarProcesosMeta("A");
                    break;
                case GUARDAR_PROCESOS_META:
                    this.guardarProcesosMeta();
                    break;    
                case ACTUALIZAR_PROCESO_META:
                    this.actualizarProcesosMeta();
                    break; 
                case CARGAR_PROCESO_INTERNO:
                    this.cargarProcesoInterno("A");
                    break;  
                case GUARDAR_PROCESO_INTERNO:
                    this.guardarProcesoInterno();
                    break;  
                case CARGAR_UNIDADES_NEGOCIO:
                    this.cargarUnidadesNegocio();
                    break; 
                case ASOCIAR_UNIDADES_NEGOCIO:
                    this.asociarUnidadesNegocio();
                    break;  
                case CARGAR_UND_NEGOCIOS_PROINTERNO:
                    this.cargarUndNegociosProinterno();
                    break;        
                case ACTUALIZAR_PROINTERNO:
                    this.actualizarProinterno();
                    break;            
                case DESASOCIAR_UND_PROINTERNO:
                    this.desasociarUndProinterno();
                    break; 
                case LISTAR_USUARIOS:
                    this.listarUsuario();
                    break;  
                case LISTAR_PROINTERNO_USUARIO:
                    this.listarProinternoUsuario();
                    break;   
                case LISTAR_PROINTERNO:
                    this.listarProinterno();
                    break;         
                case ASOCIAR_PROINTERNO_USUARIO:
                    this.asociarProinternoUsuario();
                    break;   
                case ANULAR_PROCESO_META:
                    this.anularProcesosMeta();
                    break; 
                case ANULAR_PROCESO_INTERNO:
                    this.anularProcesoInterno();
                    break;  
                case CARGAR_CBO_PROCESOS_META:
                    this.cargarComboProcesosMeta();
                    break;
                case LISTAR_USUARIOS_PROINTERNO:
                    this.listarUsuariosProinterno();
                    break;
                case LISTAR_USUARIOS_REL_PROINTERNO:
                    this.listarUsuariosRelProInterno();
                    break;  
                case ASOCIAR_USUARIO_PROINTERNO:
                    this.asociarUsuariosProinterno();
                    break;  
                case DESASOCIAR_USUARIO_PROINTERNO:
                    this.desasociarUsuariosProinterno();
                    break; 
                case EXISTE_USUARIO_REL_PROCESO:
                    this.existenUsuariosRelProceso();
                    break; 
                case CARGAR_METAS_ANULADOS:
                  this.cargarProcesosMeta("");
                  break;
                case ACTIVAR_PROCESO_META: 
                  activarProcesosMeta();
                  break;
                case CARGAR_PROCESOS_ANULADOS:
                    this.cargarProcesoInterno("");
                    break;
                case ACTIVAR_PROCESO_INTERNO:
                    activarProcesoInterno();
                    break;
                case ACTUALIZAR_MODERADOR_PROINTERNO:
                    actualizarModerador();
                    break;
                case BUSCAR_USUARIOS_REL_PROINTERNO:
                    this.cargarUsuariosRelProInterno();
                    break;
                case CARGAR_REL_PROCESOS_USUARIO:
                    this.cargarRelProcesosUsuario();
                    break;
                case CARGAR_REL_TIPO_REQ_USUARIO:
                    this.cargarRelTipoRequisicionUsuario();
                    break;
                case ASIGNAR_PROCESOS_REQ_USUARIO:
                    this.asignarProcesosReqUsuario();
                    break;
                case ASIGNAR_TIPOS_REQ_USUARIO:
                    this.asignarTiposReqUsuario();
                    break;
            }
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void cargarProcesosMeta(String status) {
        try {           
            ArrayList<ProcesoMeta> lista = dao.cargarProcesosMeta(usuario.getDstrct(),status);
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /** 
     * envia respuesta al cliente
     **/
    public void printlnResponse(String respuesta, String contentType) throws Exception {
        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);

    }

    private void guardarProcesosMeta() {
        try{
            String nombre = request.getParameter("nombre");
            String descripcion = request.getParameter("descripcion");
            String empresa = usuario.getDstrct();
            String resp = "{}";
            if(!dao.existeMetaProceso(empresa,nombre)){         
                resp =   dao.guardarMetaProceso(empresa,nombre,descripcion,usuario.getLogin());                        
            }else{
                resp = "{\"error\":\" No se cre� el meta proceso, puede que el nombre ya exista\"}";
            }
            this.printlnResponse(resp, "application/json;");  
        }catch(Exception e){
            e.printStackTrace();
    }
    }

    private void actualizarProcesosMeta() {
        try{
            String nombre = request.getParameter("nombre");
            String descripcion = request.getParameter("descripcion");
            String empresa = usuario.getDstrct();
            String idProceso = request.getParameter("idProceso");
            String resp = "{}";
            if(!dao.existeMetaProceso(empresa,nombre,Integer.parseInt(idProceso))){
                resp = dao.actualizarMetaProceso(empresa,nombre,descripcion,idProceso,usuario.getLogin());              
            }else{
                resp = "{\"error\":\" No se actualiz� el meta proceso, puede que este ya exista\"}";                             
            }
            this.printlnResponse(resp, "application/json;"); 
        }catch(Exception e){
            e.printStackTrace();
    }
    }

    private void cargarProcesoInterno(String status) {
        try {
            ArrayList<ProcesoMeta> lista;
            if(request.getParameter("procesoMeta")!= null){
                String idProMeta=request.getParameter("procesoMeta");
                lista = dao.cargarProcesoInterno(Integer.parseInt(idProMeta),status);
            }else{
                lista = dao.cargarProcesoInterno();
            }
          
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void guardarProcesoInterno() {
        try{
            String nombre = request.getParameter("nombre");
            String descripcion = request.getParameter("descripcion");
            String procesoMeta = request.getParameter("procesoMeta");
            String resp = "{}";
            if(!dao.existeProcesoInterno(procesoMeta,nombre)){              
               resp =  dao.guardarProcesoInterno(procesoMeta,nombre,descripcion,usuario.getLogin(),usuario.getDstrct());            
               this.printlnResponse(resp, "application/json;");  
            }else{
                resp = "{\"error\":\" No se cre� el proceso interno, puede que el nombre ya exista.\"}";             
                this.printlnResponse(resp, "application/json;");  
            }
        }catch(Exception e){
            e.printStackTrace();
    }
    }

    private void cargarUnidadesNegocio() {
        
        try {
            String idProceso = request.getParameter("idProceso");
            ArrayList<UnidadesNegocio> lista = dao.cargarUnidadesNegocio(Integer.parseInt(idProceso));
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void asociarUnidadesNegocio() {
        String nomProceso = request.getParameter("nomProceso");
        String idProMeta = request.getParameter("procesoMeta");
        String idProcesointerno = request.getParameter("idProinterno");
        String listado[] = request.getParameter("listado").split(",");
        int idProInterno = 0;
        try{
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            if (idProcesointerno == null) {
                idProInterno = dao.obtenerIdProcesoInterno(nomProceso, idProMeta);
            }else {
                idProInterno = Integer.parseInt(idProcesointerno);
            }
            for(int i = 0; i < listado.length; i++){
                tService.getSt().addBatch(dao.insertarRelUnidadProInterno(idProInterno,listado[i],usuario.getDstrct()));
            }
            tService.execute();
            tService.closeAll();
            String resp = "{\"respuesta\":\"OK\"}";
            this.printlnResponse(resp , "application/json;");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void cargarUndNegociosProinterno() {
        String idProinterno = request.getParameter("idProinterno");
        try {
            ArrayList<UnidadesNegocio> lista = dao.cargarUndNegocioProinterno(idProinterno);
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void actualizarProinterno() {
        try{            
            int idProinterno = Integer.parseInt(request.getParameter("idProinterno"));
            String nombre = request.getParameter("nomProinterno");
            String descripcion = request.getParameter("descProinterno");
            String idProMeta =   request.getParameter("idProMeta");
            String resp = "{}";
            if(!dao.existeProcesoInterno(idProMeta,nombre,idProinterno)){ 
               resp = dao.actualizarProcesoInterno(idProinterno,nombre,descripcion,Integer.parseInt(idProMeta),usuario.getLogin(),usuario.getDstrct());                  
            }else{
                resp = "{\"error\":\" No se actualiz� el proceso interno, puede que ya exista.\"}";   
            }     
            this.printlnResponse(resp, "application/json;");           
             
        }catch(Exception e){
            e.printStackTrace();
    }
    }

    private void desasociarUndProinterno() {
        try {
            String idProInterno = request.getParameter("idProinterno");          
            String listado[] = request.getParameter("listado").split(",");
            try{
                TransaccionService tService = new TransaccionService(usuario.getBd());
                tService.crearStatement();                  
                for(int i = 0; i < listado.length; i++){
                    tService.getSt().addBatch(dao.eliminarUndProinterno(idProInterno,listado[i]));
                }
                tService.execute();
                tService.closeAll();
                String resp = "{\"respuesta\":\"OK\"}";
                this.printlnResponse(resp , "application/json;");
            }catch(Exception e){
                e.printStackTrace();
            }           
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void listarUsuario() {
        try {
            ArrayList<Usuario> lista = dao.listarUsuario();
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void listarProinternoUsuario() {
        try {
            String codUsuario = request.getParameter("codUsuario");
            ArrayList<ProcesoMeta> lista = dao.listarProinternoUsuario(Integer.parseInt(codUsuario));
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void listarProinterno() {
        try {
            ArrayList<ProcesoMeta> lista = dao.listarProinterno();
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void asociarProinternoUsuario() {
        String listado[] = request.getParameter("listado").split(",");
        String codUsuario = request.getParameter("codUsuario");
        String login = request.getParameter("login");
        try{
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            
            for(int i = 0; i < listado.length; i++){
                tService.getSt().addBatch(dao.insertarRelProInternoUser(listado[i],Integer.parseInt(codUsuario),login,usuario.getDstrct()));
            }
            tService.execute();
            tService.closeAll();
            this.printlnResponse("OK", "application/text;");
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    private void anularProcesosMeta() {
        try {
            int idProceso = Integer.parseInt(request.getParameter("idProceso"));
            String resp = dao.anularMetaProceso(idProceso);           
            this.printlnResponse(resp, "application/json;");   
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
     
    private void anularProcesoInterno() {
        try {
            int idProinterno = Integer.parseInt(request.getParameter("idProinterno"));
            String resp = dao.anularProcesoInterno(idProinterno);         
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void cargarComboProcesosMeta() {
        try {
            String json = dao.cargarComboProcesoMeta(usuario.getDstrct());
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void listarUsuariosProinterno() {
        try {
            String idProceso = request.getParameter("idProceso");
            ArrayList<Usuario> lista = dao.listarUsuariosProinterno(Integer.parseInt(idProceso));
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    private void listarUsuariosRelProInterno() {
        try {
            String idProceso = request.getParameter("idProceso");
            ArrayList<Usuario> lista = dao.listarUsuariosRelProInterno(Integer.parseInt(idProceso));
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    private void asociarUsuariosProinterno() {
        String listado = request.getParameter("listado");      
        String idProInterno = request.getParameter("idProInterno");    

        try{
            JsonParser jsonParser = new JsonParser();
            JsonObject jo = (JsonObject) jsonParser.parse(listado);
            JsonArray jsonArr = jo.getAsJsonArray("usuarios");
            
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            for (int i = 0; i < jsonArr.size(); i++){
               String codUsuario=jsonArr.get(i).getAsJsonObject().get("cod_usuario").getAsJsonPrimitive().getAsString();
               String login=jsonArr.get(i).getAsJsonObject().get("id_usuario").getAsJsonPrimitive().getAsString();
               tService.getSt().addBatch(dao.insertarRelProInternoUser(idProInterno,Integer.parseInt(codUsuario),login,usuario.getDstrct()));              
            }
            tService.execute();
            tService.closeAll();
            String resp = "{\"respuesta\":\"OK\"}";
            this.printlnResponse(resp, "application/json;");
        }catch(Exception e){
            e.printStackTrace();
        }
    }      
    
    private void desasociarUsuariosProinterno() {
        try {
            String idProInterno = request.getParameter("idProinterno");          
            String listado[] = request.getParameter("listado").split(",");
            try{
                TransaccionService tService = new TransaccionService(usuario.getBd());
                tService.crearStatement();                  
                for(int i = 0; i < listado.length; i++){
                    tService.getSt().addBatch(dao.eliminarUsuarioProinterno(Integer.parseInt(idProInterno),listado[i]));
                }
                tService.execute();
                tService.closeAll();
                String resp = "{\"respuesta\":\"OK\"}";
                this.printlnResponse(resp , "application/json;");
            }catch(Exception e){
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void existenUsuariosRelProceso() {
        try{
            String id = request.getParameter("idProceso");
            String tipoProceso = request.getParameter("tipo");
            String resp = "{}";
            if(dao.existenUsuariosRelProceso(Integer.parseInt(id),tipoProceso)){              
               resp = "{\"respuesta\":\"SI\"}";            
               this.printlnResponse(resp, "application/json;");  
            }else{
                resp = "{\"respuesta\":\"NO\"}";                
                this.printlnResponse(resp, "application/json;");  
            }
        }catch(Exception e){
            e.printStackTrace();
    }
    }
    
    private void activarProcesosMeta() {
        try {
            int idProceso = Integer.parseInt(request.getParameter("idProceso"));
            String resp = dao.activarMetaProceso(idProceso);           
            this.printlnResponse(resp, "application/json;");   
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void activarProcesoInterno() {
        try {
            int idProinterno = Integer.parseInt(request.getParameter("idProinterno"));
            String resp = dao.activarProcesoInterno(idProinterno);         
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
     private void actualizarModerador() {
        try {
            int idProinterno = Integer.parseInt(request.getParameter("idProinterno"));
            int idusuario = Integer.parseInt(request.getParameter("idusuario"));
            String moderador = request.getParameter("moderador");
            String resp = dao.actualizarModerador(idProinterno,idusuario,moderador);         
            this.printlnResponse(resp, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
     
    private void cargarUsuariosRelProInterno() {
        try {         
            ArrayList<Usuario> lista = dao.listarUsuariosRelProInterno();
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    private void cargarRelProcesosUsuario() {
        try {
            String usuario_proceso = (request.getParameter("usuario_proc") != null) ? request.getParameter("usuario_proc") : "";
            ArrayList<ProcesoMeta> lista = dao.cargarProcesosRelUsuario(usuario_proceso);
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");  
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void cargarRelTipoRequisicionUsuario() {
        try {
            String usuario_proceso = (request.getParameter("usuario_proc") != null) ? request.getParameter("usuario_proc") : "";
            ArrayList<ProcesoMeta> lista = dao.cargarTipoRequisicionRelUsuario(usuario_proceso);
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");  
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void asignarProcesosReqUsuario() {
        String idUsuario = (request.getParameter("id_usuario") != null) ? request.getParameter("id_usuario") : "";
        String login = (request.getParameter("login") != null) ? request.getParameter("login") : "";      
        String listado[] = request.getParameter("listado").split(",");       
        try{
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();    
            tService.getSt().addBatch(dao.eliminarRelProcesoReqUser(Integer.parseInt(idUsuario)));
            for(int i = 0; i < listado.length; i++){
                tService.getSt().addBatch(dao.insertarRelProcesoReqUser(listado[i],Integer.parseInt(idUsuario), login, usuario));
            }
            tService.execute();
            tService.closeAll();
            String resp = "{\"respuesta\":\"OK\"}";
            this.printlnResponse(resp , "application/json;");
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    private void asignarTiposReqUsuario() {
        String idUsuario = (request.getParameter("id_usuario") != null) ? request.getParameter("id_usuario") : "";
        String login = (request.getParameter("login") != null) ? request.getParameter("login") : "";
        String listado[] = request.getParameter("listado").split(",");        
        try{
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();           
            tService.getSt().addBatch(dao.eliminarRelTipoReqUser(Integer.parseInt(idUsuario)));
            for(int i = 0; i < listado.length; i++){
                tService.getSt().addBatch(dao.insertarRelTipoReqUser(listado[i],Integer.parseInt(idUsuario), login, usuario));
            }
            tService.execute();
            tService.closeAll();
            String resp = "{\"respuesta\":\"OK\"}";
            this.printlnResponse(resp , "application/json;");
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    
}
