/*
 * GestionAvalesAction.java
 * Created on 1 de mayo de 2009, 15:18
 */
package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.HArchivosTransferenciasGasolina;
import com.tsp.exceptions.*;
import java.util.regex.*;
import org.apache.log4j.Logger;
import com.tsp.finanzas.contab.model.*;
import com.tsp.finanzas.contab.*;
import com.tsp.util.Util;
import org.apache.commons.fileupload.servlet.ServletFileUpload; 
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.FileItem;
 // @author  navi
public class GestionAvalesAction extends Action {      
    ArrayList preanticipos;  
    public GestionAvalesAction() {        
    }
    public void run() throws ServletException, InformationException {
        try{            
            HttpSession session = request.getSession();
            com.tsp.util.Util.logController(((com.tsp.operation.model.beans.Usuario)session.getAttribute("Usuario")).getLogin(),this.getClass().getName());
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String dstrct = session.getAttribute("Distrito").toString();            
            String loginx=usuario.getLogin();                        
            RequestDispatcher rd ;                                               
            String fechaIni       =  request.getParameter("fechaInicio");
            String fechaFin       =  request.getParameter("fechaFinal");
            String numAval=  request.getParameter("Aval");
            String ckMostrar=request.getParameter("ckMostrar");
            //.out.println("ckMostrar"+ckMostrar);
            
            String next = "/jsp/fenalco/avales/mostrarAvales.jsp?fechaInicio="+fechaIni+"&fechaFinal="+fechaFin+"&ckMostrar="+ckMostrar;       
            //.out.println("request.getParameter(opcion)"+request.getParameter("opcion"));
            if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("CONSULTARAVALES")){              
              
                if(numAval.length() == 7){
                    numAval= "0"+numAval;
                }
              //.out.println("fechaIni"+fechaIni+"numAval"+numAval);
              model.avalesService.searchAvales(fechaIni, fechaFin, numAval);              
            } 
            if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("APROBARAVALES")){              
              String[] avales=request.getParameterValues("idAval");
              String respues=model.avalesService.aprobarAvales(avales,loginx);
              model.avalesService.searchAvales(fechaIni, fechaFin ,numAval);                            
            }            
            try{
                rd = application.getRequestDispatcher(next);
                if(rd == null)
                    throw new ServletException("No se pudo encontrar "+ next);
                rd.forward(request, response);   
            } catch ( Exception e){
                System.out.println("error en run de action..."+e.toString()+"__"+e.getMessage());
                e.printStackTrace();
                throw new ServletException("Error en AnticipoGasolinaAction en  run.....\n"+e.getMessage());
           }          
        }catch(Exception ee){
            System.out.println("eee"+ee.toString()+ee.getMessage());
        }         
    }
}