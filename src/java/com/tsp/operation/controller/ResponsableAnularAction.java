/*
 * ResponsableAnularAction.java
 *
 * Created on 28 de octubre de 2005, 04:54 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  Jose
 */
public class ResponsableAnularAction extends Action{
    
    /** Creates a new instance of ResponsableAnularAction */
    public ResponsableAnularAction() {
    }
    public void run() throws ServletException, InformationException {
        String next="/jsp/trafico/mensaje/MsgAnulado.jsp";
        HttpSession session = request.getSession();        
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String codigo = request.getParameter("c_codigo");
        String descripcion = request.getParameter("c_descripcion");
        try{
            Responsable resp = new Responsable();
            resp.setCodigo(codigo);
            resp.setUsuario_modificacion(usuario.getLogin().toUpperCase());
            model.responsableService.anularResponsable(resp);
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);

    }
}
