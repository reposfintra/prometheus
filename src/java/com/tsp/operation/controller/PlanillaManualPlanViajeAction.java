/*****************************************************************************
 * Nombre clase :      PlanillaManualPlanViajeAction.java                    *
 * Descripcion :       Action del PlanillaManualPlanViajeAction.java         *
 * Autor :             LREALES                                               *
 * Fecha :             11 de agosto de 2006, 08:15 AM                        *
 * Version :           1.0                                                   *
 * Copyright :         Fintravalores S.A.                               *
 *****************************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.services.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.*;

public class PlanillaManualPlanViajeAction extends Action {
        
    /** Creates a new instance of PlanillaManualPlanViajeAction */
    public PlanillaManualPlanViajeAction () { }
    
    public void run () throws ServletException, InformationException {
        
        HttpSession session = request.getSession();
        
        String next = "/jsp/trafico/despacho_manual/insertar.jsp"; // ?sw=1
             
        try{
 
            String numpla = ( request.getParameter( "numpla" ) != null )?request.getParameter( "numpla" ).toUpperCase() : "";   
            
            model.planillaManualPlanViajeService.buscarInformacion ( numpla );
            
            Vector vec = model.planillaManualPlanViajeService.getVectorPlanilla();
            
            int tam = vec.size();
            
            if ( tam > 0 ) {
                
                Hashtable ht = ( Hashtable ) vec.get( 0 );
                
                session.setAttribute ( "placa", ( String ) ht.get( "placa" ) );
                session.setAttribute ( "fecha_despacho", ( String ) ht.get( "fecha_despacho" ) );
                session.setAttribute ( "fecha_salida", ( String ) ht.get( "fecha_salida" ) );
                session.setAttribute ( "cedcon", ( String ) ht.get( "cedcon" ) );
                session.setAttribute ( "nomcon", ( String ) ht.get( "nomcon" ) ); 
                session.setAttribute ( "codruta", ( String ) ht.get( "codruta" ) );
                session.setAttribute ( "ruta", ( String ) ht.get( "ruta" ) );
                session.setAttribute ( "codcli", ( String ) ht.get( "codcli" ) );
                session.setAttribute ( "nomcli", ( String ) ht.get( "nomcli" ) );
                session.setAttribute ( "carga", ( String ) ht.get( "carga" ) );    
                //String observacion = ( String ) ht.get( "otro" ).equals("")?"":( String ) ht.get( "otro" )
                session.setAttribute ( "observacion", ( String ) ht.get( "otro" ) + "  " + ( String ) ht.get( "comentario1" ) + "  " + ( String ) ht.get( "comentario2" ) + "  " + ( String ) ht.get( "comentario3" ) );
                session.setAttribute ( "numpla", numpla );
                
                model.despachoManualService.BuscarPuestosControl( ( String ) ht.get( "codruta" ) );
                
            } else {
                
                session.setAttribute ( "placa", "" );
                session.setAttribute ( "fecha_despacho", "" );
                session.setAttribute ( "fecha_salida", "" );
                session.setAttribute ( "cedcon", "" );
                session.setAttribute ( "nomcon", "" );  
                session.setAttribute ( "codruta", "" );
                session.setAttribute ( "ruta", "" );
                session.setAttribute ( "codcli", "" );
                session.setAttribute ( "nomcli", "" );
                session.setAttribute ( "carga", "" );
                session.setAttribute ( "observacion", "" );
                session.setAttribute ( "numpla", numpla );
                
                next = next + "?ms=No se encontro informacion de la planilla manual de plan de viaje!";
            
            }
                           
        } catch ( SQLException e ){
                    
            e.printStackTrace();
            throw new ServletException( e.getMessage () );
            
        }
        
        this.dispatchRequest( next );
        
    }
    
}