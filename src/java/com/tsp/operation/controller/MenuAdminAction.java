/*
 * Created on 11 de agosto de 2005, 02:25 PM
 */

package com.tsp.operation.controller;

/**@author  trafisur*/



import java.io.*;
import java.util.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.exceptions.*;


public class MenuAdminAction extends Action{
    
   
   public void run() throws ServletException, InformationException {
        try{
           String comentario="";
           String []UsuariosA  = request.getParameterValues ("UsuariosA");
           String []GruposA    = request.getParameterValues ("GruposNA");
           String Opcion       = request.getParameter ("Opcion");
           String user         = request.getParameter ("usuario");
           
           if (Opcion!=null){
              if (Opcion.equals ("Grabar")){
                    model.menuService.insert(UsuariosA, GruposA, user);
                    comentario="Actualización realizada...";
              }
           }
           
           
           String next = "/jsp/trafico/perfil/Mantenimiento.jsp?comentario="+comentario;
           RequestDispatcher rd = application.getRequestDispatcher(next);
           if(rd == null)
              throw new Exception("No se pudo encontrar "+ next);
           rd.forward(request, response); 
        }
        catch(Exception e){
          throw new ServletException("Accion:"+ e.getMessage());
        } 
        
        
    }
    
}
