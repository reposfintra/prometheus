/*
 * ConsultarProveedorAnticipoAction.java
 *
 * Created on 12 de enero de 2005, 11:00 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class ConsultarProveedorAnticipoAction extends Action{
    
    /** Creates a new instance of ConsultarProveedorAnticipoAction */
    public ConsultarProveedorAnticipoAction() {
    }
    public void run() throws ServletException,InformationException{
        String nit = request.getParameter("nit");
        String ciudad = request.getParameter("ciudad").toUpperCase();
        String nombre = request.getParameter("nombre").toUpperCase();
        String next="/consultas/consultasRedirect.jsp?anticipo=ok";
        try{
            if(request.getParameter("solo")==null){
                model.proveedoranticipoService.consultaProveedor(ciudad, nombre, nit);
                
            }
            else{
                model.proveedoranticipoService.buscaProveedor(nit,"");
                next="/consultas/datosProveedorAnticipo.jsp";
                
                
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}
