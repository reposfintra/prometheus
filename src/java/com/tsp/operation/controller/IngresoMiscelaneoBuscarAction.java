
/***************
 * Nombre:        IngresoMiscelaneoBuscarAction.java
 * Descripci�n:   Clase Action para controlar los eventos del
 *                y las suscursales del banco
 * Autor:         Ing. Diogenes Antonio Bastidas Morales
 * Fecha:         Created on 27 de junio de 2006, 03:10 PM
 * Versi�n        1.0
 * Coyright:      Transportes Sanchez Polo S.A.
 ****************/



package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import com.tsp.finanzas.contab.model.*;

import com.tsp.pdf.*;


public class IngresoMiscelaneoBuscarAction extends Action{
    
    /** Creates a new instance of IngresoMiscelaneoBuscarAction */
    public IngresoMiscelaneoBuscarAction() {
    }
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        HttpSession session = request.getSession();
        com.tsp.finanzas.contab.model.Model modelcontab = (com.tsp.finanzas.contab.model.Model) session.getAttribute("modelcontab");
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String next = "/jsp/cxcobrar/ingresoMiscelaneo/";
        String num = request.getParameter("numero");
        String codcli = request.getParameter("identificacion");
        String fecini = request.getParameter("fechaInicio");
        String fecfin = request.getParameter("fechaFinal");
        String evento = ( request.getParameter("evento") != null )?request.getParameter("evento"):"";
        String dstrct = request.getParameter("dstrct") != null ?request.getParameter("dstrct"):usuario.getDstrct();
        String pagina = request.getParameter("pagina") != null ?request.getParameter("pagina"):"";
        String tipo = request.getParameter("tipodoc");
        
        /*Jescandon 28-03-07*/
        //Impresion de Ingresos por Lotes
        String imp          = request.getParameter("imp") != null ?request.getParameter("imp"):"";
        String datos[]      = Util.coalesce( request.getParameter("LOV"), "" ).split(";");
        
        
        Ingreso ingre = null;
        Vector vec = null;
        
        String mon_local = "";
        boolean sw = false;
        try{
            if(evento.equals("1")){
                model.ingresoService.buscarIngresoMiscelaneo(dstrct,tipo, num);
                Ingreso ing = model.ingresoService.getIngreso();
                if(ing != null){
                    String age = (ing.getAgencia_ingreso().equals("OP"))?"":ing.getAgencia_ingreso();
                    model.servicioBanco.loadBancos(age, usuario.getDstrct());
                    model.servicioBanco.loadSucursalesAgenciaCuenta(age, ing.getBranch_code().replaceAll("\n",""), usuario.getDstrct());
                    model.monedaService.cargarMonedas();
                    model.tablaGenService.buscarRegistros("CONINGRESO");
                    next+="ingresoMiscelaneoMod.jsp?sw=ok";
                    if(!model.ingresoService.tieneItemsIngreso( ing.getDstrct(), tipo, ing.getNum_ingreso() )  ){
                        next+="&pagina="+pagina+"&modificar=true";
                    }
                    else{
                        next+="&pagina="+pagina+"&modificar="+null;
                    }
                }
                else{
                    sw = true;
                }
            }
            else if(evento.equals("2") || evento.equals("3") || evento.equals("4")){
                model.ingresoService.busquedaIngresoMiscelaneo(dstrct,tipo, request.getParameter("identificacion"), fecini, fecfin);
                next+="verIngresos.jsp";
            }
            else if(evento.equals("Cabecera")){
                int nummax = 1;
                String modificar = "";
                model.ingresoService.buscarIngresoMiscelaneo(dstrct ,tipo, num);
                Ingreso ing = model.ingresoService.getIngreso();
                ing.setPagina(pagina);
                //busco si tiene items
                model.ingreso_detalleService.itemsMiscelaneo( ing.getNum_ingreso(), tipo, ing.getDstrct());
                Vector vecitems = model.ingreso_detalleService.getItemsMiscelaneo();
                session.setAttribute("fin",null);
                if(vecitems.size()>0){
                    modificar = "ok";
                    nummax = model.ingreso_detalleService.nroMayorItems(dstrct,tipo, num);
                    for(int i = 0; i < vecitems.size(); i++){
                        Ingreso_detalle ingdetalle = (Ingreso_detalle) vecitems.get(i);
                        if(modelcontab.planDeCuentasService.existCuenta(ingdetalle.getDistrito(),ingdetalle.getCuenta())){
                            modelcontab.subledgerService.buscarCuentasTipoSubledger(ingdetalle.getCuenta());
                            ingdetalle.setTipos(modelcontab.subledgerService.getCuentastsubledger());
                        }else{
                            ingdetalle.setTipos(null);
                        }
                    }
                    model.ingreso_detalleService.setItemsMiscelaneo(vecitems);
                }
                else{
                    try{
                        ingre = model.ingreso_detalleService.leerArchivoTemporal(usuario.getLogin(),""+ing.getNum_ingreso());
                        vec = model.ingreso_detalleService.leerArchivoItemsTemporal(usuario.getLogin(),""+ing.getNum_ingreso());
                        nummax = (vec.size()>0)?vec.size():1;
                    }catch(Exception e){
                        vec = new Vector();
                        model.ingreso_detalleService.setItemsMiscelaneo(vec);
                    }
                    if( ingre != null && vec != null){
                        model.ingreso_detalleService.setItemsMiscelaneo(vec);
                    }
                }
                next="/jsp/cxcobrar/ItemsIngreso/ItemsIngresoMiscelaneo.jsp?numero="+nummax+"&sw="+modificar;
            }
            else if(evento.equals("Borrar")){
                String numing = request.getParameter("numingreso");
                boolean flag = model.ingreso_detalleService.BorrarArchivo("Ing_miscelaneo"+numing+".txt", usuario.getLogin());
                if(flag){
                    model.ingreso_detalleService.setItemsMiscelaneo(new Vector());
                }
                next="/jsp/cxcobrar/ItemsIngreso/ItemsIngresoMiscelaneo.jsp";
            }
            
            if(sw){
                next="/jsp/cxcobrar/ingresoMiscelaneo/BuscarIngreso.jsp?msg=no";
            }
            
            //Jescandon 28-03-07
            if( imp.equals("lote") ){
                
                IngresoMPDF remision = new IngresoMPDF();
                
                remision.RemisionPlantilla();
                
                remision.crearRaiz();
                
                remision.crearRemision( model, datos , "ALL", usuario.getLogin());
                
                remision.generarPDF();
                
                next = "/pdf/IngresoMPDF.pdf";
                
            }//fin de Opcion
            
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }
}
