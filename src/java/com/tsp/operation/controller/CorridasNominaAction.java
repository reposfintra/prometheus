/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.CorridasNominaDAO;
import com.tsp.operation.model.DAOS.impl.CorridasNominaImpl;
import com.tsp.operation.model.beans.Usuario;
import javax.servlet.ServletException;

/**
 *
 * @author egonzalez
 */
public class CorridasNominaAction extends Action{

    private final int GETFACTURAS = 0;
    private final int CREARCORRIDAS = 1;
    private final int BUSCARBANCOS = 2;
    private final int GENERARARCHIVO = 3;
    private final int GENERARPREVIO = 4;
    
    private CorridasNominaDAO dao;
    private String txtRespuesta;
    
    @Override
    public void run() throws ServletException, InformationException {
        try {
            this.txtRespuesta = "";
            dao = new CorridasNominaImpl(((Usuario) request.getSession().getAttribute("Usuario")).getBd());
            int opcion = (request.getParameter("opcion") != null)
                    ? Integer.parseInt(request.getParameter("opcion"))
                    : -1;
            switch (opcion) {
                case GETFACTURAS:
                    getFacturas();
                    break;
                case CREARCORRIDAS:
                    crearCorridas();
                    break;
                case BUSCARBANCOS:
                    buscarBanco();
                    break;
                case GENERARARCHIVO:
                    generarArchivo();
                    break;
                case GENERARPREVIO:
                    generarPrevio();
                    break;
            }
        } catch(Exception e) {
            this.txtRespuesta = "{\"mensaje\":\""+e.getMessage()+"\", causa:\"\"}";
        } finally {
            try {
                response.setContentType("application/json; charset=utf-8");
                response.setHeader("Cache-Control", "no-cache");
                response.getWriter().println(txtRespuesta);
            } catch (Exception e) {  }
        }
    }
    
    private void getFacturas() {
        String empleados = request.getParameter("filGerencia");
        String perido=request.getParameter("periodo");
        txtRespuesta = dao.getFacturas(empleados,perido);
    }
    
    private void crearCorridas() {
        JsonObject obj = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
        obj.addProperty("usuario",((Usuario) request.getSession().getAttribute("Usuario")).getLogin());
        obj.addProperty("distrito",((Usuario) request.getSession().getAttribute("Usuario")).getDstrct());
        txtRespuesta = dao.crearCorridas(obj);
    }
    
    private void buscarBanco() {
        JsonObject obj = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
        String banco = obj.get("banco").getAsString();
        String usuario = ((Usuario) request.getSession().getAttribute("Usuario")).getLogin();
        txtRespuesta = dao.buscarBanco(usuario, banco);
    }
    
    private void generarArchivo() {
        JsonObject obj = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
        obj.addProperty("usuario",((Usuario) request.getSession().getAttribute("Usuario")).getLogin());
        txtRespuesta = dao.generarArchivo(obj,((Usuario) request.getSession().getAttribute("Usuario")));
    }
    
    private void generarPrevio() {
        JsonObject obj = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
        obj.addProperty("usuario",((Usuario) request.getSession().getAttribute("Usuario")).getLogin());
        obj.addProperty("distrito",((Usuario) request.getSession().getAttribute("Usuario")).getDstrct());
        txtRespuesta = dao.generarPrevio(obj); 
    }
}
