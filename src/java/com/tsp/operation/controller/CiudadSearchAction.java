/*
 * CiudadSearchAction.java
 *
 * Created on 21 de abril de 2005, 09:34 AM
 */

package com.tsp.operation.controller;


import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
/**
 *
 * @author  henry
 */
public class CiudadSearchAction extends Action{
   CiudadDAO  ciudad;

    public CiudadSearchAction() {
    }

    public void run() throws ServletException {
        Usuario usuario = (Usuario) request.getSession().getAttribute("Usuario");
        ciudad= new CiudadDAO(usuario.getBd());
        String next   =  "/consultas/consultaCiudad.jsp";
        String var    =  request.getParameter("var");
       String  opcion = request.getParameter("opcion") != null ? request.getParameter("opcion") : "";
       boolean redirect =true;
        String cadenaWrite = "";
        try{





           if (opcion.equals("cargarciu"))
           {
                String codpt = request.getParameter("dept") != null ? request.getParameter("dept") : "";
                String idciu = request.getParameter("ciu") != null ? request.getParameter("ciu") : "";
                ArrayList lista = null;
                redirect = false;
                try {
                    lista = ciudad.listadoCiudades(codpt);
                } catch (Exception e) {
                    System.out.println("error(action): " + e.toString());
                    e.printStackTrace();
                }
                cadenaWrite = "<select id='" + idciu + "' name='" + idciu + "' style='width:20em;'>";
                cadenaWrite += "<option value=''>...</option>";
                String[] dato1 = null;
                for (int i = 0; i < lista.size(); i++) {
                    dato1 = ((String) lista.get(i)).split(";_;");
                    cadenaWrite += " <option value='" + dato1[0] + "'>" + dato1[1] + "</option>";
                }

                cadenaWrite += "</select>";
            }
           else
           {
           model.ciudadService.buscarCiudadesGeneral(var.toUpperCase());
           Vector vec = model.ciudadService.obtCiudades();
           if (vec.size()==0) {
               next+="?msg=";
           }
           else
           {
               next =  "/consultas/listadoCiudades.jsp";
           }

           }



        }catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        finally {
            try {
                if (redirect == false) {
                    this.escribirResponse(cadenaWrite);
                } else {
                    this.dispatchRequest(next);
                }
            } catch (Exception e) {
                System.out.println("Error al redireccionar o escribir respuesta: " + e.toString());
                e.printStackTrace();
            }
        }

    }


        /**
     * Escribe el resultado de la consulta en el response de Ajax
     * @param dato la cadena a escribir
     * @throws Exception cuando hay un error
     */
    protected void escribirResponse(String dato) throws Exception {
        try {
            response.setContentType("text/plain; charset=utf-8");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println(dato);
        } catch (Exception e) {
            throw new Exception("Error al escribir el response: " + e.toString());
        }
    }
}

