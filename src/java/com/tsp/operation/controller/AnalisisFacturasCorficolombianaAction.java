/**********************************************************************
 *      Nombre Clase.................   AnalisisFacturasCorficolombianaAction
 *      Autor........................   Tmolina
 *      Fecha........................   07.2008
 *      Versi�n......................   1.1
 *********************************************************************/
//Adaptaci�n para el an�lisis de facturas de AnalisisVencCXCAction.java

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.operation.model.threads.RepAnalisisVencTh;
import com.tsp.operation.model.threads.*;

/**
 *
 * @author  Tmolina
 */
public class AnalisisFacturasCorficolombianaAction extends Action{

    Vector general;
    HttpSession session;
    
    public AnalisisFacturasCorficolombianaAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        
        session = request.getSession();
   
        String codcli = request.getParameter("clientes")==null ? "" : request.getParameter("clientes");
        String prov = request.getParameter("proveedor")==null ? "" : request.getParameter("proveedor");
        String cccli = request.getParameter("cccli")==null ? "" : request.getParameter("cccli");
        String agc = request.getParameter("agencia")==null ? "" : request.getParameter("agencia");
        String agcd = request.getParameter("agencia_d")==null ? "" : request.getParameter("agencia_d");
        String agcobro = request.getParameter("agencia_c")==null ? "" : request.getParameter("agencia_c");
        String fecha = request.getParameter("fecha");
        String degen = request.getParameter("general");
        String vto = request.getParameter("vence");
        String cofac = request.getParameter("tfac");
        String hc = request.getParameter("hc");
        String lim1 = request.getParameter("lim1");
        String lim2 = request.getParameter("lim2");
        
        String exp1 = request.getParameter("exp1");
        String exp2 = request.getParameter("exp2");
        String nit_fiducia = request.getParameter("nit_fiducia")==null ? "" : request.getParameter("nit_fiducia");
        
        //Info del usuario
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        //Pr�xima vista
        String next = "";
        
        try{
           model.facturaService.facturasCorficolombiana(codcli, agc, fecha, (String) session.getAttribute("Distrito"), agcd, agcobro,prov,cccli,vto,cofac,hc,lim1,lim2, nit_fiducia);
           Vector info = model.facturaService.getVDocumentos();
            
            this.organizarInfo(info, fecha);
            
       /*     if( request.getParameter("exp")!= null ){
                ListadoCarteraTh hilo = new ListadoCarteraTh();
                hilo.start(model, this.general, usuario, fecha);
                next = "/jsp/cxcobrar/reportes/analisisCarteraMsg.jsp";
            } else {*/
                next = "/jsp/cxcobrar/reportes/analisisFacturaDet.jsp";
                request.setAttribute("det", this.general);
       //     }

            request.setAttribute("fecha", fecha);
            request.setAttribute("afcfact", agc);
            request.setAttribute("codcli", codcli);
            request.setAttribute("afcdu", agcd);
            request.setAttribute("afcob", agcobro);
            request.setAttribute("prov", prov);
            request.setAttribute("cccli", cccli);
            request.setAttribute("vencem", vto);
            request.setAttribute("cfact", cofac);
            request.setAttribute("hc", hc);
            request.setAttribute("lim1",lim1 );
            request.setAttribute("lim2",lim2 );
            request.setAttribute("nit_fiducia", nit_fiducia );
                        
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
    public void organizarInfo(Vector info, String fecha) throws Exception{
        Calendar FechaHoy = Calendar.getInstance();
        Date d = FechaHoy.getTime();
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
        
        this.general = new Vector();
        
        for( int i=0; i< info.size(); i++){
            RepGral obj = null;
            obj = new RepGral();
            obj = (RepGral) info.elementAt(i);

            int dias = obj.getNdias();
            
            obj.setVencida_6(0);
            obj.setVencida_5(0);
            obj.setVencida_4(0);
            obj.setVencida_3(0);
            obj.setVencida_2(0);
            obj.setVencida_1(0);
            obj.setNo_vencida(0);
            
            if( dias>90 ){
                obj.setVencimiento(">90");
            } else if ( dias > 60 ){
                obj.setVencimiento("90");
            } else if ( dias > 30 ){
                obj.setVencimiento("60");
            } else if ( dias > 14 ){
                obj.setVencimiento("30");
            } else if ( dias > 7 ){
                obj.setVencimiento("14");
            } else if ( dias > 0 ){
                obj.setVencimiento("7");
            } else if ( dias > -8 ){
                obj.setVencimiento("-7");
            } else if ( dias > -15 ){
                obj.setVencimiento("-14");
            } else if ( dias > -22 ){
                obj.setVencimiento("-21");
            } else {
                obj.setVencimiento("-28");
            }
            this.general.add(obj);
        }
    }      
}

