 /***************************************
    * Nombre Clase ............. PlanViajeManagerAction.java
    * Descripci�n  .. . . . . .  Realiza actualizaciones en la tabla de Plan de Viajes, con los datos provinientes del formulario.
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  10/10/2005
    * versi�n . . . . . . . . .  1.0
    * Copyright ...Transportes Sanchez Polo S.A.
    *******************************************/


package com.tsp.operation.controller;


import java.io.*;
import java.util.*;
import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;

public class PlanViajeManagerAction extends Action {
    
    
    
    public void run() throws ServletException, InformationException {
        
          try{
            String next            = "/jsp/trafico/planviaje/frmPlanViaje.jsp";
            String opcion          = request.getParameter("Opcion");
            String comentario      = "";
            int swObjeto           = 0;
            PlanDeViaje planViaje  = new PlanDeViaje();
            planViaje.LoadFormRequest(request);
            planViaje.loadUbicaciones(model.PlanViajeSvc.getPlan());
            
            comentario = "El plan de Viaje para la Planilla "+ planViaje.getPlanilla() + " ha sido "+ opcion;            
            
             if( opcion!=null){
                opcion= opcion.toUpperCase();
                if( opcion.equals("CREADO") ){
                    if( ! planViaje.getEstado().trim().equals("C") ){
                       model.PlanViajeSvc.save(planViaje);
                       model.PlanViajeSvc.formatBtn("MODIFY");
                    }
                    else{
                        comentario=" No se puede crear el plan de Viaje, la planilla ya esta cumplida";
                        model.PlanViajeSvc.formatBtn("ninguno"); 
                        swObjeto = 1;
                     }
                }
                if( opcion.equals("MODIFICADO") ){
                    model.PlanViajeSvc.modify(planViaje); 
                }
                if( opcion.equals("ELIMINADO") ){  
                    model.PlanViajeSvc.delete(planViaje);
                    next="/jsp/trafico/planviaje/planViaje.jsp";   
                }
                
                if(swObjeto==0)
                   planViaje = model.PlanViajeSvc.search_planViaje( planViaje.getPlanilla(), planViaje.getDistrito());
            }
             
            
            model.PlanViajeSvc.setPlan(planViaje);
            request.setAttribute("planViajeObject", planViaje );
            next+="?msj="+ comentario;
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new Exception("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        } catch (Exception e){
             throw new ServletException(e.getMessage());
        }
    }
    
}
