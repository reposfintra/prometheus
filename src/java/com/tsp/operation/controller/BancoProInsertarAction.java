/***********************************************
 * Nombre clase: PlacaInsertAction.java
 * Descripci�n: Accion para ingresar una placa a la bd.
 * Autor: AMENDEZ
 * Fecha: 4 de noviembre de 2004, 02:48 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 **********************************************/

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import java.lang.*;
import com.tsp.operation.model.threads.*;
public class BancoProInsertarAction extends Action{
    
    //static Logger logger = Logger.getLogger(PlacaInsertAction.class);
    
    /** Creates a new instance of PlacaInsertAction */
    public BancoProInsertarAction() {
    }
    
    public void run() throws ServletException, InformationException{
        
        
        String next = "";
        next = "/jsp/sot/body/propietario/Ingresar_Propietario.jsp";
        Banco_Propietario BancoP = new Banco_Propietario();
        Date fecha = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String fechacrea = format.format(fecha);
        String msg = "";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
       
        //bean de banco propietario    
        BancoP.setDistrito(usuario.getDstrct());
        BancoP.setPropietario((request.getParameter("Propietario")!=null)?request.getParameter("Propietario"):"");
        BancoP.setHc((request.getParameter("HC")!=null)?request.getParameter("HC"):"");
        BancoP.setBanco((request.getParameter("Banco")!=null)?request.getParameter("Banco"):"");
        BancoP.setSucursal((request.getParameter("Sucursal")!=null)?request.getParameter("Sucursal"):"");
        BancoP.setCreation_user(usuario.getLogin());
        BancoP.setBase(usuario.getBase());


        try{
            /*//VALIDACION DE EXISTENCIA DEL CLIENTE*/

            if(!model.banco_PropietarioService.existeBancoP(BancoP.getPropietario())) {
                if (model.identidadService.existePropietario(BancoP.getPropietario())){
                    model.banco_PropietarioService.agregarBancoPropietario(BancoP);
                    next += "?msg=" + "El Banco Propietario fue agregado exitosamente";

                }else{
                    next += "?msg=" + "EL NIT DEL PROPIETARIO NO EXISTE";
                }
               
             
            }else{
               next += "?msg=" + "El Banco Propietario ya existe";

            }

        }
           catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
            
        
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
        
    }
}
/********************************************************
 Entregado a Fily 12 Febrero 2007
*********************************************************/
