/*******************************************************************
 * Nombre clase: ClienteBuscarAction.java
 * Descripci�n: Accion para buscar una placa de la bd.
 * Autor: Ing. fily fernandez
 * Fecha: 16 de febrero de 2006, 05:41 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import com.tsp.exceptions.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import java.util.Vector;
import java.lang.*;
import java.sql.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import com.tsp.operation.model.services.*;
/**
 *
 * @author  jdelarosa
 */
public class BancoProBuscarAction extends Action{
    
    /** Creates a new instance of PlacaBuscarAction */
    public BancoProBuscarAction () {
    }
    
    public void run () throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String Propietario = request.getParameter ("Propietario")!=null?request.getParameter ("Propietario").toUpperCase():"";
        String HC = request.getParameter ("HC")!=null?request.getParameter ("HC"):"";
        String Banco = request.getParameter ("Banco")!=null?request.getParameter ("Banco"):"";
       
        
        
        String next = "/jsp/sot/body/propietario/ReporteBancoPropietario.jsp?Propietario="+Propietario+"&HC="+HC+"&Banco="+Banco;
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario)session.getAttribute ("Usuario");
        String usu = usuario.getLogin();
        String opcion = request.getParameter ("opcion")!=null?request.getParameter ("opcion"):"";
       
        
        try {
              Vector datos = model.banco_PropietarioService.getVector();
              
           if ( opcion.equals("1") ){
                model.banco_PropietarioService.BusquedaBancoPro(Propietario, HC, Banco);
                datos = model.banco_PropietarioService.getVector();
                request.setAttribute("Propietario",Propietario);
                request.setAttribute("HC",HC);
                request.setAttribute("Banco",Banco);
                
                if ( datos.size () <= 0 ) {    
                     next = next + "&msg=Su busqueda no arrojo resultados!";
                
                }  
           } 
             
            if ( opcion.equals("2") ) {
                HReporteBancoPropietario cli = new HReporteBancoPropietario();
                cli.start( Propietario, HC , Banco, usuario);
            }
             
            
        } catch ( Exception e ) {
            
            throw new ServletException ( "Error en Reporte de ClienteBuscarAction : " + e.getMessage() );
            
        }        
        
        this.dispatchRequest ( next );
        
    }
    
}
