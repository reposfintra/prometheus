/*
 * Codigo_discrepanciaUpdateAction.java
 *
 * Created on 28 de junio de 2005, 09:18 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Jose
 */
public class Codigo_discrepanciaUpdateAction extends Action{
    
    /** Creates a new instance of Codigo_discrepanciaUpdateAction */
    public Codigo_discrepanciaUpdateAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/jsp/trafico/codigo_discrepancia/Codigo_discrepanciaModificar.jsp?&reload=ok";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String codigo = request.getParameter("c_codigo");
        String descripcion = request.getParameter("c_descripcion");
        String base = request.getParameter("c_base");
        try{
            Codigo_discrepancia codigo_discrepancia = new Codigo_discrepancia();
            codigo_discrepancia.setCodigo(codigo);
            codigo_discrepancia.setDescripcion(descripcion);
            codigo_discrepancia.setUser_update(usuario.getLogin().toUpperCase());
            codigo_discrepancia.setCia(usuario.getCia().toUpperCase());
            model.codigo_discrepanciaService.updateCodigo_discrepancia(codigo, descripcion, usuario.getLogin().toUpperCase());
            model.codigo_discrepanciaService.serchCodigo_discrepancia(codigo);
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);    
    }
    
}
