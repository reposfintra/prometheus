/*
 * Nombre        UbicacionVehicularPropietarioAction.java
 * Descripci�n
 * Autor         Ivan Dario Gomez Vanegas
 * Fecha         28 de enero de 2006, 11:08
 * Version       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.controller;

import java.io.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;
import javax.servlet.jsp.*;
import javax.servlet.http.*;
import com.tsp.operation.model.*;
import com.tsp.util.Utility;
import java.util.*;
import com.tsp.operation.model.threads.*;


/**
 * Acci�n ejecutada por el controlador para elaborar el reporte de ubicaci�n
 * y llegada de equipos en ruta.
 * @author  Ivan Gomez
 */
public class UbicacionVehicularPropietarioAction extends Action {
    
    static Logger logger = Logger.getLogger(UbicacionVehicularPropietarioAction.class);
    
    
    
    public void run() throws ServletException, InformationException {
        String next = null;
        try {
            HttpSession session = request.getSession();
            Usuario loggedUser  = (Usuario)session.getAttribute("Usuario");
            String User = loggedUser.getLogin();
            String Perfil = (String)session.getAttribute("Perfil");
            // Perform search
            
            String Fecini = request.getParameter("fechaini");
            String cmd = request.getParameter("cmd");
            if ( "fin".equals(cmd) ) {
                String numpla = request.getParameter("planilla");
                //model.UbicaVehicuPorpSvc.buscarInformacionFinanciera(numpla);
                //Hashtable datos = model.UbicaVehicuPorpSvc.getInfoPlanilla();
                //next = "/jsp/sot/reports/RepFinanzasUbicacionProp.jsp?planilla="+numpla+"&conductor="+datos.get("conductor")+"&fechaPlanilla="+datos.get("fecpla");
            }
            else {
                if( Fecini == null ) {
                    try {
                        if(Perfil.equals("PROPIETARIO")){
                            //model.UbicaVehicuPorpSvc.BuscarPlacasADMIN();
                            model.UbicaVehicuPorpSvc.BuscarNitsPropietario(User);
                            String nitsPropietario = model.UbicaVehicuPorpSvc.getNitPropietario();
                            String nit ="'";
                            nit += nitsPropietario.replaceAll(",", "','");
                            nit +="'";
                            ////System.out.println("nit="+nit);
                            model.UbicaVehicuPorpSvc.BuscarPlacas(nit);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    next = "/jsp/sot/body/UbicacionVehicularPropietario.jsp";
                }else {
                    
                    String Placa         = request.getParameter("PlacaAdmin");
                    Placa                = (Placa!=null)?Placa.toUpperCase().trim():"";
                    String []Placas      = request.getParameterValues("PlacasBuscar");
                    if ( Placas == null ) {
                        Placas = Placa.split(",");
                    }
                    String Fecfin        = request.getParameter("fechafin");
                    Fecfin               = (Fecfin == null ? "" : Fecfin);
                    String ListTipoviaje = request.getParameter("tipoViaje");
                    String EstadoViajes  = request.getParameter("viajes");
                    next                = "/jsp/sot/reports/ReporteUbicacionVehicularPropietario.jsp";
                    
                    String view = request.getParameter("displayInExcel");
                    if(view!=null && !view.equals("File")){
                        //if(Perfil.equals("PROPIETARIO")){
                        model.UbicaVehicuPorpSvc.buscarDatosReporte(Fecini,Fecfin,Placas,ListTipoviaje,EstadoViajes, User );
                        //}else{
                        //    model.UbicaVehicuPorpSvc.buscarDatosReportePlaca(Fecini,Fecfin,Placa,ListTipoviaje,EstadoViajes, User );
                        //}
                    }else{
                        next = "/jsp/sot/body/UbicacionVehicularPropietario.jsp?comentario=Su proceso de Exportaci�n de Ubicaci�n y LLegada de Equipos se ha iniciado... Puede consultar en Manejo de Archivo";
                        
                        
                        //--- tomamos los parametros
                        Hashtable headerValues = new Hashtable();
                        headerValues.clear();
                        
                        Fecini        = (request.getParameter("fechaini")      == null)?"":request.getParameter("fechaini");
                        EstadoViajes  = (request.getParameter("viajes")  == null)?"":request.getParameter("viajes");
                        EstadoViajes  = (EstadoViajes.equals("RUTA") ? "En Ruta" :
                            (EstadoViajes.equals("PORCONF") ? "Por Confirmar Salida" :
                                (EstadoViajes.equals("CONENTREGA") ? "Con Entrega" : "TODOS")));
                                ListTipoviaje = (request.getParameter("tipoViaje") == null )?"":request.getParameter("tipoViaje");
                                Fecfin       = (request.getParameter("fechafin")       == null )?"":request.getParameter("fechafin");
                                if(Fecini != null && !Fecini.equals("")){
                                    headerValues.put( "fechaInicial", Fecini );
                                    headerValues.put( "fechaFinal",   Fecfin);
                                }
                                headerValues.put( "estadoViajes",   EstadoViajes );
                                headerValues.put( "listaTipoViaje", ListTipoviaje );
                                String ruta = this.application.getRealPath("/WEB-INF/");
                                headerValues.put( "rutaPaginaLastUpdate", ruta );
                                
                                HUbicacionVehicularPropietario  hilo  =  new HUbicacionVehicularPropietario();
                                hilo.start(model, loggedUser , headerValues,Placas);
                                
                    }
                    
                    
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        ////System.out.println("redirecionando hacia: "+next);
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
    }
}
