/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.controller;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import javax.servlet.http.*;
/**
 *
 * @author rhonalf
 */
public class CotizacionModificarAction extends Action {

    public CotizacionModificarAction(){

    }

    public void run(){
        CotizacionService cserv = new CotizacionService();
        String parametro ="";
        String ms="";
        String nota="";
        String fecha="";
        double cant=0;
        int act=0;
        int k=0;
        //double subt=0.0;
        ArrayList listado = new ArrayList();
        ArrayList actualizar = new ArrayList();
        String idcot="";
        Cotizacion cot = null;
        HttpSession session   = request.getSession();//090922
        Usuario     usuario   = (Usuario) session.getAttribute("Usuario"); //090922
        String loginx=usuario.getLogin();
        String next  = "/jsp/delectricaribe/cotizacion/cambiar_cotizacion.jsp";
        request.setAttribute("msg", "");
        try{
            ms = request.getParameter("multiservicios")!=null? request.getParameter("multiservicios"):"";
            fecha = request.getParameter("fecha")!=null? request.getParameter("fecha"):"";
            k = request.getParameter("filas")!=null? Integer.parseInt(request.getParameter("filas")):0;
            act = request.getParameter("contact")!=null? Integer.parseInt(request.getParameter("contact")):0;
            System.out.println("Filas listas para verificar: "+k);
            System.out.println("Filas existentes "+act);
            //k = k - act;
            //MaterialesService prs = new MaterialesService();
            //ArrayList ars = null;
            //Material mat = null;
            if(k>0){
                //int contadorx=1;//091028
                int i=1;//091028
                while (/*contadorx*/i<=k){//091212
                    //se debe adicionar a listado y aumentar i cuando realmente hayan datos (volviendo el for un while y teniendo otro contador para recorrer realmente las filas)//091028
                    if (request.getParameter("codp"+i)!=null && !(request.getParameter("codp"+i).equals("")) && !(request.getParameter("codp"+i).equals("0")) ){//091028
                        parametro = request.getParameter("codp"+i)!=null? request.getParameter("codp"+i):"";
                        //ars = prs.buscarPor(1, parametro);
                        //mat = (Material)ars.get(0);
                        cant = request.getParameter("cantidad"+i)!=null? Double.parseDouble(request.getParameter("cantidad"+i)):0;//100226
                        nota = request.getParameter("nota"+i)!=null? request.getParameter("nota"+i):"";
                        cot = new Cotizacion();
                        cot.setAccion(ms);
                        cot.setCantidad(cant);
                        cot.setCodigo("");
                        cot.setFecha(fecha);
                        cot.setMaterial(parametro);
                        cot.setObservacion(nota);
                        listado.add(cot);
                        System.out.println("reconocida fila para insertar");
                        
                        //contadorx=contadorx+1;//091028
                        System.out.println("__request.getParameter(codp_"+i+")="+request.getParameter("codp"+i)+" en el if ");//091028

                    }else{//091028
                        /*if(i<=act){*/ //091212
                            if(request.getParameter("anular"+i)!=null && !(request.getParameter("anular"+i).equals(""))){
                                idcot = request.getParameter("anular"+i);//esto es lo de actualizar...
                                actualizar.add(idcot);//esto es lo de actualizar...
                                //contadorx=contadorx+1;
                                System.out.println("reconocida fila para anular ... id: "+idcot);
                            }
                            
                       /* }*/ //091212
                        
                        System.out.println("request.getParameter(codp_"+i+")="+request.getParameter("codp"+i)+" en el else ");//091028
                    }
                    //if(i>k) break;
                    i=i+1;//091028
                }
                if(actualizar.size()>0) cserv.anularRegistro(actualizar,loginx);//esto es lo de actualizar...
                else System.out.println("no hay datos para anular");
                if(listado.size()>0) cserv.insertDetsPlusOrden(listado, fecha,loginx);
                else System.out.println("no hay datos para insertar");
                next=next+"?idaccion="+ms;
            }
            else {
                System.out.println("Error: no se enviaron filas para insertar...");
                request.setAttribute("msg", "Error: no se enviaron filas para insertar...");
            }
            this.dispatchRequest(next);
        }
        catch(Exception ec){
            System.out.println("Error en el run del action de modificar cotizacion: "+ec.toString());
            request.setAttribute("msg", "Ha ocurrido un error!");
        }
    }

}
