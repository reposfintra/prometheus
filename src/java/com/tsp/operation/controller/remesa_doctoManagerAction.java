/*
 * remesa_doctoManagerAction.java
 *
 * Created on 12 de septiembre de 2005, 11:46 AM
 */

package com.tsp.operation.controller;
import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
/**
 *
 * @author  Administrador
 */
public class remesa_doctoManagerAction extends Action{
    
    /** Creates a new instance of remesa_doctoManagerAction */
    public remesa_doctoManagerAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        try{
            
            String Opcion = (request.getParameter("Opcion")!=null)?request.getParameter("Opcion"):"";
            HttpSession Session           = request.getSession();
            Usuario user                  = new Usuario();
            user                          = (Usuario)Session.getAttribute("Usuario");
            String usuario                = user.getLogin();
            String distrito               = user.getDstrct();
            int cant_doc                  = Integer.parseInt(request.getParameter("items") != null ?request.getParameter("items"):"0");
            Vector vecdoc = new Vector();
            
            //System.out.println("Opcion :"+Opcion);
            
            //capturo del formulario los documentos
            for(int i=0; i<cant_doc;i++){
                if(!request.getParameter("documento"+i).trim().equals("") ){
                    remesa_docto doc = new remesa_docto();
                    String dato_info[] = request.getParameter("tipodoc"+i).split("-");
                    doc.setImportacion(dato_info[0]);
                    doc.setExportacion(dato_info[1]);
                    doc.setTipo_doc(dato_info[2]);
                    doc.setDocumento(request.getParameter("documento"+i));
                    doc.setDescripcion_cga(request.getParameter("descripcion"+i).toUpperCase() );
                    doc.setFecha_sia( !request.getParameter("fecha_sia"+i).equals("") ? request.getParameter("fecha_sia"+i):"0099-01-01 00:00");
                    doc.setFecha_eta( !request.getParameter("fecha_eta"+i).equals("") ? request.getParameter("fecha_eta"+i):"0099-01-01 00:00");
                    //Nota se utilizan para almecenar el tipo documento y documento anterior
                    doc.setTipo_doc_rel(request.getParameter("tdoc"+i)!=null ? request.getParameter("tdoc"+i):"");
                    doc.setDocumento_rel(request.getParameter("doc"+i)!=null ? request.getParameter("doc"+i):"");
                    vecdoc.add(doc);
                }
                
            }
            
            //Datos remesa_docto
            String numrem       =   (request.getParameter("numrem")!=null)?request.getParameter("numrem"):"";
            
            /*
            String numrem1       =   (request.getParameter("numrem1")!=null)?request.getParameter("numrem1"):"";
            numrem = numrem.equals("")?numrem1:numrem;
             */
            
            String tipodocrel   =   (request.getParameter("tipodocrel")!=null)?request.getParameter("tipodocrel"):"";
            String documentorel =   (request.getParameter("documentorel")!=null)?request.getParameter("documentorel"):"";
            String destinatario =   (request.getParameter("destinatario")!=null)?request.getParameter("destinatario"):"";
            
            String tipo         =   (request.getParameter("tipo")!=null)?request.getParameter("tipo"):"";
            
            //Datos modificacion
            // String ndocumento       =   (request.getParameter("ndocumento")!=null)?request.getParameter("ndocumento"):"";
            String ndocumentorel    =   (request.getParameter("ndocumentorel")!=null)?request.getParameter("ndocumentorel"):"";
            
            
            String []LOV                  = request.getParameterValues("LOV");
            
            String next = "";
            
            String Mensaje = "";
            
            String pag = "";
            
            //Documentos
            String documentos      =    (request.getParameter("documentos")!=null)?request.getParameter("documentos"):"";
            if( documentos.length() > 0 )
                documentos = documentos.substring(1,documentos.length());
            
            String d = request.getParameter("documentosRem");
            
            
            
            if (Opcion.equals("Guardar")){
                model.RemDocSvc.INSERTDOCMAS(documentos, distrito, numrem, usuario);
                next = "/docremesas/AgregarDocE.jsp?reload=ok";
            }
            
            
            if (Opcion.equals("GuardarRel")){
                //    model.RemDocSvc.INSERTDOCREL(documentos, distrito, numrem, destinatario, usuario);
                next = "/docremesas/AgregarDocE.jsp?reload=ok";
            }
            
            if (Opcion.equals("GuardarRel2")){
                //     model.RemDocSvc.INSERTDOCREL2(documentos, distrito, numrem, destinatario, usuario);
                next = "/docremesas/AgregarDocRE.jsp?reload=ok";
            }
            
            if (Opcion.equals("Listado")){
                model.RemDocSvc.LIST(numrem);
            }
            
            if (Opcion.equals("Modificar")){
                model.RemDocSvc.UPDATEDOC(d, usuario, numrem);
                next = "/docremesas/ModificarDoc.jsp?reload=ok";
            }
            
            if( Opcion.equals("ModificarRel")){
                model.RemDocSvc.UPDATEDOCREL(d, usuario, numrem,destinatario);
                next = "/docremesas/AgregarDocE.jsp?reload=ok";
            }
            
            
            if( !tipo.equals("") ){
                model.RemesaSvc.buscaRemesa(numrem);
                
                if( model.RemesaSvc.getRemesa() != null ){
                    if( tipo.equals("0") ){
                        //Documentos Internos
                        next = "/jsp/masivo/Documentos_Despacho/AgregarDocE.jsp?numrem="+numrem;
                        
                    }
                    else{
                        //Documentos Relacionados
                        model.remidestService.listaDestinatariosPla(numrem);
                        pag  = "TipoDocumento.jsp";
                        next = "/jsp/masivo/Documentos_Despacho/destinatarios.jsp?numrem="+numrem+"&pag="+pag;
                    }
                }
                else{
                    Mensaje = "La remesa no existe";
                    next = "/jsp/masivo/Documentos_Despacho/TipoDocumento.jsp?Mensaje="+Mensaje;
                }
                
            }
            
            // Diogenes ---------------------------------------------------
            if ( Opcion.equals("GuardarDocumento") ) {
                model.RemDocSvc.INSERTARDOC( vecdoc, distrito, numrem, usuario );
                
                Mensaje = "Registro insertado exitosamente";
                next = "/jsp/masivo/Documentos_Despacho/AgregarDocE.jsp?Mensaje="+Mensaje+"&numrem="+numrem;
                
            }
            //
            
            if (Opcion.equals("BuscarDocumento")){
                
                model.RemesaSvc.buscaRemesa(numrem);
                
                if( model.RemesaSvc.getRemesa() != null ){
                    if( !tipo.equals("") ){
                        if( tipo.equals("0") ){
                            model.RemDocSvc.LISTTLBDOC();
                            //Documentos Internos
                            model.RemDocSvc.DocumentosRemesa(numrem);
                            next = "/jsp/masivo/Documentos_Despacho/ModificarDoc.jsp?numrem="+numrem;
                            
                        }
                        else{
                            //Documentos Relacionados
                            model.remidestService.listaDestinatariosPla(numrem);
                            pag  = "BuscarDocumento.jsp";
                            next = "/jsp/masivo/Documentos_Despacho/destinatarios.jsp?numrem="+numrem+"&estado=anular&pag="+pag;
                            
                        }
                        
                    }
                    
                }
                else{
                    Mensaje = "La remesa no existe";
                    next = "/jsp/masivo/Documentos_Despacho/BuscarDocumento.jsp?Mensaje="+Mensaje;
                }
            }
            //***********************Diogenes*********************************
            if (Opcion.equals("ModificarDocumento")){
                String SQL=model.RemDocSvc.borrarDocs(distrito,numrem)+"; ";
                SQL+=model.RemDocSvc.MODIFICARDOC(vecdoc, distrito, numrem, usuario);
                TransaccionService tService = new TransaccionService(user.getBd());
                tService.crearStatement();
                tService.getSt().addBatch(SQL);
                tService.execute();
                
                
                model.RemesaSvc.buscaRemesa(numrem);
                model.RemDocSvc.DocumentosRemesa(numrem);
                Mensaje = "Registro modificado exitosamente";
                next = "/jsp/masivo/Documentos_Despacho/ModificarDoc.jsp?Mensaje="+Mensaje;
            }
            //********************************************************
            
            if (Opcion.equals("AnularDocumento")){
                Mensaje = "Registro anulados exitosamente";
                for(int j=0; j<cant_doc;j++){
                    if(request.getParameter("pos"+j)!= null){
                        remesa_docto info = (remesa_docto) vecdoc.get(j);
                        model.RemDocSvc.deleteDocs(numrem,  info.getTipo_doc(), info.getDocumento() );
                        if(info.getImportacion().equals("S") || info.getExportacion().equals("S")){
                            model.RemDocSvc.borrarImpoExpo( user.getDstrct(), info.getTipo_doc(), info.getDocumento() );
                        }
                    }
                }
                model.RemesaSvc.buscaRemesa(numrem);
                model.RemDocSvc.DocumentosRemesa(numrem);
                /*for(int i=0;i<LOV.length;i++){
                    String valor1 = LOV[i].split(":")[0];
                    String valor2 = LOV[i].split(":")[1];
                 
                    model.RemDocSvc.deleteDocs(numrem, valor1, valor2);
                 
                }*/
                
                next = "/jsp/masivo/Documentos_Despacho/ModificarDoc.jsp?Mensaje="+Mensaje;
            }
            
            
            if( Opcion.equals("agregar")){
                
                next = "/jsp/masivo/Documentos_Despacho/ModificarDocRel.jsp?numrem="+numrem;
                destinatario    = request.getParameter("destinatario");
                String nombre   =  request.getParameter("nombre");
                
                model.RemDocSvc.ListDocumentosDestinatarios(numrem, destinatario);
                
            }
            
            
            if( Opcion.equals("anular")){
                
                next = "/jsp/masivo/Documentos_Despacho/AnularDocRel.jsp?numrem="+numrem;
                destinatario    = request.getParameter("destinatario");
                String nombre   =  request.getParameter("nombre");
                
                model.RemDocSvc.ListDocumentosDestinatarios(numrem, destinatario);
                
            }
            
            model.RemDocSvc.LISTTLBDOC();
            model.RemDocSvc.LISTVEC(numrem);
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en remesa_doctoManagerAction .....\n"+e.getMessage());
        }
        
    }
    
}
