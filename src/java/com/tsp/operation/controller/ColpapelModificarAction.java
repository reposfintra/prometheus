/*
 * ColpapelModificarAction.java
 *
 * Created on 13 de diciembre de 2004, 04:07 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class ColpapelModificarAction extends Action{
        //2009-09-02
static Logger logger = Logger.getLogger(ColpapelModificarAction.class);
    /** Creates a new instance of ColpapelModificarAction */
    public ColpapelModificarAction() {
    }
    public void run() throws ServletException, InformationException{
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String next="/colpapel/InicioModificar.jsp";
        String monedaCia="PES";
        
        //VARIABLES A MODIFICAR
        
        
        String remitentes[]= request.getParameter("remitentes").split(",");
        String destinatarios[]= request.getParameter("destinatarios").split(",");
        String destinatario=request.getParameter("destinatarios");
        if(destinatario.length()>40){
            destinatario = destinatario.substring(0,40);
        }
        String remitente=request.getParameter("remitentes");
        
        String inventario = "a".equals(request.getParameter("inventarios"))?"":request.getParameter("inventarios");
        
        //String faccial=request.getParameter("fcial");
        String placa = request.getParameter("placa").toUpperCase();
        String trailer =request.getParameter("trailer").toUpperCase();
        String cedula= request.getParameter("conductor");
        
        String contenedores="";
        if(request.getParameter("c1")!=null){
            contenedores = contenedores+ request.getParameter("c1");
        }
        if(request.getParameter("c2")!=null){
            if(!contenedores.equals("")){
                contenedores = contenedores+",";
            }
            contenedores = contenedores+ request.getParameter("c2");
        }
        if(request.getParameter("tipo_cont")!=""){
            if(request.getParameter("tipo_cont").equals("NA")){
                contenedores ="";
            }
        }
        
        float cfacturar =0;
        
        
        float cantreal = 0;
        if(request.getParameter("cantreal")!=null)
            cantreal = Float.parseFloat(request.getParameter("cantreal"));
        
        String unidad = request.getParameter("unidad");
        String cp1=" ";
        String cp2=" ";
        if(request.getParameter("c1precinto")!=null)
            cp1 = request.getParameter("c1precinto");
        
        if(request.getParameter("c2precinto")!=null)
            cp2 = request.getParameter("c2precinto");
        
        String precintos =cp1+","+cp2+",";
        String prent = request.getParameter("precintos")!=null?request.getParameter("precintos"):"";
        precintos =precintos +prent;
        int k =2;
        while(k<=5){
            if(request.getParameter("precintos"+k)!=null){
                if(!request.getParameter("precintos"+k).equals("")){
                    precintos = precintos+","+request.getParameter("precintos"+k);
                    
                }
            }
            k++;
        }
        
        String observacion = request.getParameter("observacion");
        String numpla=request.getParameter("planilla");
        String numre=request.getParameter("remesa");
        String cdocking = request.getParameter("cdock")!=null?"Y":"N";
        logger.info("Se va a modificar la planilla"+request.getParameter("planilla"));
        float pesoMax=0;
        float valorU=0;
        float pmax=0;
        String origen="";
        String destino="";
        String unit_of_work="";
        String moneda=request.getParameter("moneda");
        String nit="";
        
        String nombre="";
        String cf_code="";
        String descripcion="";
        float costoRem=0;
        float vlrUnit=0;
        String nfacturable = request.getParameter("facturable")!=null?"S":"N";
        String cadena = request.getParameter("cadena")!=null?"S":"N";
        try{
            
            model.remesaService.buscaRemesa(numre);
            Remesa rem =model.remesaService.getRemesa();
            Movrem mov = model.facturaService.buscarMovrem(usuario.getDstrct(),numre, "TOTAL");
            if (mov!=null){
                rem.setVlrrem    ( (float)mov.getVlrrem()     );
                rem.setVlr_pesos   ( (float)mov.getVlrrem2()    );
                rem.setVlrrem2   ( (float)mov.getVlrrem2()    );
                rem.setQty_value( (float)mov.getQty_value()  );
                rem.setPesoReal  ( (float)mov.getPesoreal()   );
                rem.setTasa      ( mov.getTasa()       );
            }
            model.remesaService.setRemesa(rem);
            
            cfacturar  = rem.getPesoReal();
            
            if(request.getParameter("cfacturar")!=null)
                cfacturar = Float.parseFloat(request.getParameter("cfacturar"));
            
            if( cfacturar != rem.getPesoReal()){
                if(request.getParameter("cfacturar")!=null){
                    cfacturar = Float.parseFloat(request.getParameter("cfacturar"));
                }
            }
            
            String proveedor="";
            model.placaService.buscaPlaca(request.getParameter("placa"));
            if(model.placaService.getPlaca()!=null){
                Placa placa1 = model.placaService.getPlaca();
                nit=placa1.getPropietario();
                proveedor = placa1.getProveedor();
            }
            
            model.stdjobdetselService.buscaStandard(request.getParameter("standard"));
            if(model.stdjobdetselService.getStandardDetSel()!=null){
                Stdjobdetsel stdjobdetsel = model.stdjobdetselService.getStandardDetSel();
                destino=stdjobdetsel.getDestination_code();
                descripcion= stdjobdetsel.getSj_desc();
                origen= stdjobdetsel.getOrigin_code();
                unit_of_work= stdjobdetsel.getUnit_of_work();
                moneda=stdjobdetsel.getCurrency();
                costoRem=rem.getVlrrem();
                vlrUnit= stdjobdetsel.getVlr_freight();
                if( cfacturar != rem.getPesoReal()){
                    costoRem=stdjobdetsel.getVlr_freight()*cfacturar;
                }
                
            }
            int sw=0;
            java.util.Enumeration enum1;
            String parametro;
            enum1 = request.getParameterNames();
            while (enum1.hasMoreElements()) {
                parametro = (String) enum1.nextElement();
                if(parametro.indexOf("precintos")>=0 && !request.getParameter(parametro).equals("")){
                    String nombrep  = parametro;
                    String valor = request.getParameter(parametro);
                    java.util.Enumeration enum2;
                    enum2 = request.getParameterNames();
                    while (enum1.hasMoreElements()) {
                        parametro = (String) enum1.nextElement();
                        if(parametro.indexOf("precintos")>=0 && !request.getParameter(parametro).equals("")
                        && !nombrep.equals(parametro) && request.getParameter(parametro).equals(valor)){
                            String numero = parametro.substring(9);
                            String numero1 = nombre.substring(9);
                            request.setAttribute("p"+numero,"filaroja");
                            request.setAttribute("p"+numero1,"filaroja");
                            sw=1;
                            next="/colpapel/modificarDespachoError.jsp";
                        }
                    }
                    
                    
                }
            }
            if(request.getParameter("c2precinto")!=null && request.getParameter("c1precinto")!=null){
                if(!request.getParameter("c2precinto").equals("") || !request.getParameter("c1precinto").equals("")){
                    if(request.getParameter("c2precinto").equals(request.getParameter("c1precinto"))){
                        sw=1;
                        request.setAttribute("cp2","filaroja");
                        request.setAttribute("cp1","filaroja");
                        next="/colpapel/modificarDespachoError.jsp";
                    }
                }
            }
            if(sw==0){
                //Modifico la planilla
                model.tService.crearStatement();
                
                model.planillaService.bucarColpapel(numpla);
                System.out.println("Numpla "+numpla);
                Planilla pla=model.planillaService.getPlanilla();
                if(pla!=null){
                    System.out.println("Encontro la planilla");
                    pla.setNitpro(nit);
                    pla.setPlatlr(trailer);
                    pla.setPlaveh(placa);
                    pla.setCedcon(cedula);
                    pla.setNomCond(nombre);
                    pla.setPrecinto(precintos);
                    pla.setContenedores(contenedores);
                    pla.setTipocont(request.getParameter("tipo_cont"));
                    pla.setTipotrailer(request.getParameter("tipo_tra"));
                    pla.setDespachador(usuario.getLogin());
                    pla.setGroup_code("");
                    pla.setProveedor(proveedor);
                    model.tService.getSt().addBatch(model.discrepanciaService.desmarcarProductos(pla.getNumpla()));
                    
                    if(inventario!=null && !"".equals(inventario)){
                        String productos[]=inventario.split(",");
                        for(int i =0; i<productos.length;i++){
                            
                            String valores[] = productos[i].split("/");
                            String codigo_prod = valores[0];
                            String discrepancia = valores[1];
                            String ubicacion = valores[2];
                            
                            Producto prod = new Producto();
                            prod.setNumpla(numpla);
                            prod.setNumrem(numre);
                            prod.setUsuario_modificacion(usuario.getLogin());
                            prod.setDistrito(usuario.getDstrct());
                            prod.setDiscrepancia(discrepancia);
                            prod.setCodigo(codigo_prod);
                            prod.setUbicacion(ubicacion);
                            
                            model.discrepanciaService.setProducto(prod);
                            model.tService.getSt().addBatch(model.discrepanciaService.utilizarProducto());
                        }
                        
                    }
                    model.tService.getSt().addBatch(model.planillaService.updatePlanilla(pla));
                }
                
                String remiOld[]={""};
                String destiOld[]={""};
                if(rem!=null){
                    remiOld=rem.getRemitente().split(",");
                    destiOld=rem.getDestinatario().split(",");
                    
                    for(int i=0; i<remiOld.length; i++){
                        RemesaDest rd= new RemesaDest();
                        rd.setCodigo(remiOld[i]);
                        rd.setDstrct(usuario.getDstrct());
                        rd.setNumrem(numre);
                        rd.setTipo("RE");
                        rd.setUsuario(usuario.getLogin());
                        logger.info("Remitente a borrar: "+remiOld[i]);
                        model.rdService.updateRemesa(rd);
                    }
                    for(int i=0; i<destiOld.length; i++){
                        RemesaDest rd= new RemesaDest();
                        rd.setCodigo(destiOld[i]);
                        rd.setDstrct(usuario.getDstrct());
                        rd.setNumrem(numre);
                        rd.setTipo("DE");
                        rd.setUsuario(usuario.getLogin());
                        logger.info("Destinatarios a borrar: "+destiOld[i]);
                        model.rdService.updateRemesa(rd);
                    }
                    
                    logger.info("Remitente a agregar: "+remitente);
                    RemesaDest rd= new RemesaDest();
                    rd.setCodigo(remitente);
                    rd.setDstrct(usuario.getDstrct());
                    rd.setNumrem(numre);
                    rd.setTipo("RE");
                    rd.setUsuario(usuario.getLogin());
                    model.tService.getSt().addBatch(model.rdService.insertRemesa(rd,usuario.getBase()));
                    
                    
                    for(int i=0; i<destinatarios.length; i++){
                        rd= new RemesaDest();
                        rd.setCodigo(destinatarios[i]);
                        rd.setDstrct(usuario.getDstrct());
                        rd.setNumrem(numre);
                        rd.setTipo("DE");
                        rd.setUsuario(usuario.getLogin());
                        if(!model.rdService.estaDest(numre,destinatarios[i])){
                            logger.info("Destinatarios a agregar: "+destinatarios[i]);
                            model.tService.getSt().addBatch(model.rdService.insertRemesa(rd,usuario.getBase()));
                        }
                    }
                    double vlrrem2=  costoRem;
                    double vtasa=1;
                    if(!rem.getCurrency().equals("PES")){
                        Date fecha_mes = rem.getFecRem();
                        java.text.SimpleDateFormat s =  new java.text.SimpleDateFormat("yyyy-MM-dd");
                        String fecha = s.format(fecha_mes);
                        
                        try{
                            model.tasaService.buscarValorTasa(monedaCia,rem.getCurrency(),monedaCia,fecha);
                        }catch(Exception et){
                            throw new ServletException(et.getMessage());
                        }
                        
                        Tasa tasa = model.tasaService.obtenerTasa();
                        if(tasa!=null){
                            
                            vtasa = tasa.getValor_tasa();
                            vlrrem2= com.tsp.util.Util.redondear(vtasa*costoRem,0);
                            
                        }else{
                            logger.info("NO EXISTE TASA ");
                        }
                        
                        
                    }
                    rem.setRemitente(remitente);
                    rem.setDestinatario(destinatario);
                    rem.setDocInterno("");
                    rem.setFaccial("");
                    //rem.setPesoReal(cfacturar);
                    rem.setUnidad(request.getParameter("unidad"));
                    //rem.setVlrRem(costoRem);
                    //rem.setVlrrem2((float)com.tsp.util.Util.redondear(vlrrem2,0));
                    //rem.setVlr_pesos((float)com.tsp.util.Util.redondear(vlrrem2,0));
                    rem.setUnidcam(moneda);
                    rem.setObservacion(request.getParameter("observacion"));
                    rem.setUsuario(usuario.getLogin());
                    rem.setNfacturable(nfacturable);
                    rem.setCantreal(Float.parseFloat(request.getParameter("cantreal")));
                    rem.setCrossdocking(cdocking);
                    rem.setCadena(cadena);
                    rem.setPagador(request.getParameter("cliente")!=null?request.getParameter("cliente"):rem.getCodcli());
                    
                    
                    
                    //AJUSTE DE LA REMEMSA
                    double cantRemesa = rem.getPesoReal();
                    if(cfacturar-cantRemesa!=0 ){
                        double ajuste = cfacturar-cantRemesa;
                        double ajusteVlr = costoRem- rem.getVlrRem();
                        double ajusteVlr2 = vlrrem2 - rem.getVlr_pesos();
                        Remesa2 remesa_remplazo = new Remesa2();
                        remesa_remplazo.setCia(rem.getCia());
                        remesa_remplazo.setConcepto("53");
                        remesa_remplazo.setNumrem(rem.getNumrem());
                        remesa_remplazo.setVlrrem        ( rem.getVlrRem()   );
                        remesa_remplazo.setVlrrem2       ( rem.getVlr_pesos()  );
                        remesa_remplazo.setPesoreal      ( cantRemesa );
                        remesa_remplazo.setVlrrem_aj     ( ajusteVlr   );
                        remesa_remplazo.setVlrrem2_aj    ( ajusteVlr2  );
                        remesa_remplazo.setPesoreal_aj   ( ajuste  );
                        remesa_remplazo.setQty_value_aj  (0 );
                        remesa_remplazo.setQty_value  (vlrUnit );
                        remesa_remplazo.setTasa(vtasa);
                        remesa_remplazo.setTasa_aj       ( 0  );
                        remesa_remplazo.setAgcrem(rem.getAgcRem());
                        remesa_remplazo.setCreation_date("now()");
                        remesa_remplazo.setUsuario(usuario.getLogin());
                        remesa_remplazo.setBase(usuario.getBase());
                        model.tService.getSt().addBatch(model.CambioDestinoSvc.actualizarValor(remesa_remplazo));
                    }
                    model.tService.getSt().addBatch(model.remesaService.updateRemesa(rem));
                }
                
                /*
                 * Se modifican cada uno de los anticipos generados.
                 */
                logger.info("PARAMETROS");
                enum1 = request.getParameterNames(); // Leemos todos los atributos del request
                while (enum1.hasMoreElements()) {
                    parametro = (String) enum1.nextElement();
                    logger.info(parametro);
                }
                logger.info("FIN DE PARAMETROS");
                enum1 = request.getParameterNames(); // Leemos todos los atributos del request
                int cant = 0;
                
                while (enum1.hasMoreElements()) {
                    parametro = (String) enum1.nextElement();
                    if(parametro.indexOf("creacion")==0){
                        String numero = parametro.substring(8,parametro.length());
                        model.movplaService.buscaMovpla(numpla, "01",request.getParameter(parametro));
                        Movpla movpla= model.movplaService.getMovPla();
                        if(movpla!=null){
                            
                            logger.info("Numero "+numero);
                            String banco_cuenta =request.getParameter("banco"+numero)!=null?request.getParameter("banco"+numero):movpla.getBanco();
                            String beneficiario=movpla.getBeneficiario();
                            //BUSCAMOS EL NIT DEL BENEFICIARIO
                            if(request.getParameter("beneficiario"+numero)!=null){
                                if(request.getParameter("beneficiario"+numero).equals("C")){
                                    beneficiario = pla.getCedcon();
                                }
                                else{
                                    beneficiario = model.movplaService.buscarBeneficiarioCheque(request.getParameter("beneficiario"+numero),placa);
                                }
                            }
                            
                            if(beneficiario.equals("")){
                                //SE OBTIENE LA AGENCIA ASOCIADA AL ORIGEN DEL STANDARD
                                model.ciudadService.buscarCiudad(pla.getOripla());
                                Ciudad c = model.ciudadService.obtenerCiudad();
                                beneficiario =pla.getCedcon();
                                if(c!=null){
                                    String agenciaC = c.getAgAsoc();
                                    
                                    //SE BUSCA EL PAIS DE LA AGENCIA ASOCIADA
                                    model.ciudadService.buscarCiudad(agenciaC);
                                    c = model.ciudadService.obtenerCiudad();
                                    if(c!=null){
                                        String pais = c.getPais()!=null?c.getPais():"";
                                        if(pais.equals("VE")){
                                            beneficiario=nit;
                                        }
                                    }
                                }
                                
                            }
                            logger.info("Banco "+banco_cuenta);
                            String banvec[] = banco_cuenta.split("/");
                            String banco="";
                            String cuenta="";
                            if(banvec.length>1){
                                banco=banvec[0];
                                cuenta=banvec[1];
                            }
                            movpla.setBanco(banco);
                            movpla.setCuenta(cuenta);
                            movpla.setVlr(request.getParameter("anticipo"+numero)!=null?Float.parseFloat(request.getParameter("anticipo"+numero)):movpla.getVlr());
                            movpla.setBeneficiario(beneficiario);
                            model.tService.getSt().addBatch(model.movplaService.updateMovPla(movpla));
                        }
                        
                    }
                }
                
                //MODIFICACIONES DE ANTICIPOS A PROVEEDOR
                Vector anticipoProv=model.anticiposService.getAnticiposProv();
                if(anticipoProv!=null){
                    for(int i=0; i<anticipoProv.size(); i++){
                        Anticipos ant = (Anticipos) anticipoProv.elementAt(i);
                        model.movplaService.buscaMovpla(numpla, ant.getAnticipo_code(),ant.getCreation_date());
                        Movpla movpla= model.movplaService.getMovPla();
                        if(movpla!=null){
                            String banco_cuenta =movpla.getBanco();
                            logger.info("Banco "+banco_cuenta);
                            String banvec[] = banco_cuenta.split("/");
                            String banco="";
                            String cuenta="";
                            if(banvec.length>1){
                                banco=banvec[0];
                                cuenta=banvec[1];
                            }
                            String provee[] = ant.getProveedor().split("/");
                            movpla.setProveedor(provee.length>0?provee[0]:"");
                            movpla.setSucursal(provee.length>1?provee[1]:"");
                            movpla.setBanco(banco);
                            movpla.setCuenta(cuenta);
                            movpla.setVlr(ant.getValor());
                            model.tService.getSt().addBatch(model.movplaService.updateMovPla(movpla));
                        }
                        
                    }
                    
                }
                
                
                
            /*
            List listTabla = model.tbltiempoService.getTblTiemposSalida(usuario.getBase());
            Iterator itTbla=listTabla.iterator();
            while(itTbla.hasNext()){
             
                Tbltiempo tbl = (Tbltiempo) itTbla.next();
                String tblcode = tbl.getTimeCode();
                int secuence = tbl.getSecuence();
             
                if(request.getParameter(tbl.getTimeCode()).length()>15){
                    String fecha = request.getParameter(tbl.getTimeCode()).substring(0,10);
                    int hora=Integer.parseInt(request.getParameter(tbl.getTimeCode()).substring(11,13));
                    String minuto=request.getParameter(tbl.getTimeCode()).substring(14,16);
             
                    fecha= fecha + " "+hora+":"+minuto+":00";
             
                    Planilla_Tiempo pla_tiempo= new Planilla_Tiempo();
             
                    pla_tiempo.setCreation_user(usuario.getLogin());
                    pla_tiempo.setDate_time_traffic(fecha);
                    pla_tiempo.setPla(numpla);
                    pla_tiempo.setTime_code(tblcode);
             
                    model.tService.getSt().addBatch(model.pla_tiempoService.updateTiempo(pla_tiempo));
                }
            }
             */
                //AGREGAMOS LOS EXTRAFLETES
                Vector efletes= model.sjextrafleteService.getFletes();
                for(int i=0; i<efletes.size();i++){
                    SJExtraflete sjE = (SJExtraflete)efletes.elementAt(i);
                    model.movplaService.buscarValorConcepto(numpla,sjE.getCod_extraflete());
                    logger.info("EXTRAFLETE ENCONTRADO "+sjE.getCod_extraflete());
                    sjE.setNumpla(numpla);
                    sjE.setNumrem(numre);
                    sjE.setFecdsp("now()");
                    sjE.setDistrito(usuario.getDstrct());
                    sjE.setProveedor("");
                    model.sjextrafleteService.setSj(sjE);
                    
                    Movpla movpla= model.movplaService.getMovPla();
                    if(model.movplaService.getMovPla()!=null){
                        if(movpla.getVlr()>0){
                            movpla.setAgency_id(usuario.getId_agencia());
                            movpla.setCurrency(sjE.getMoneda_costo());
                            movpla.setDstrct(usuario.getDstrct());
                            movpla.setPla_owner(nit);
                            movpla.setPlanilla(numpla);
                            movpla.setSupplier(placa);
                            movpla.setDocument_type("001");
                            movpla.setConcept_code(sjE.getCod_extraflete());
                            movpla.setAp_ind("S");
                            movpla.setProveedor("");
                            movpla.setSucursal("");
                            movpla.setVlr_for(movpla.getVlr());
                            movpla.setCreation_user(usuario.getLogin());
                            movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+102+i));
                            
                            logger.info("SE ENCONTRO UN MOVPLA CON ESTE EXTRAFLETE CON VALOR "+movpla.getVlr());
                            logger.info("EL VALOR DEL EXTRAFLETE ES DE  "+sjE.getVlrtotal());
                            
                            
                            if(sjE.isSelec()){
                                if(movpla.getVlr()!=sjE.getVlrtotal()){
                                    movpla.setVlr((movpla.getVlr()-sjE.getVlrtotal())*-1);
                                    movpla.setVlr_for(movpla.getVlr());
                                    movpla.setCantidad(sjE.getCantidad());
//                                    model.tService.getSt().addBatch(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                                    model.tService.getSt().addBatch(model.sjextrafleteService.ActualizarAplicados());
                                }
                            }
                            else{
                                movpla.setVlr(sjE.getVlrtotal()*-1);
                                movpla.setVlr_for(movpla.getVlr());
                                movpla.setCantidad(sjE.getCantidad());
//                                model.tService.getSt().addBatch(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                                model.tService.getSt().addBatch(model.sjextrafleteService.anularCosto());
                            }
                        }
                        else if(sjE.isSelec()){
                            
                            movpla= new Movpla();
                            movpla.setAgency_id(usuario.getId_agencia());
                            movpla.setCurrency(sjE.getMoneda_costo());
                            movpla.setDstrct(usuario.getDstrct());
                            movpla.setPla_owner(nit);
                            movpla.setPlanilla(numpla);
                            movpla.setSupplier(placa);
                            movpla.setVlr(sjE.getVlrtotal());
                            movpla.setVlr_for(movpla.getVlr());
                            movpla.setDocument_type("001");
                            movpla.setConcept_code(sjE.getCod_extraflete());
                            movpla.setAp_ind("N");
                            movpla.setProveedor("");
                            movpla.setSucursal("");
                            movpla.setCreation_user(usuario.getLogin());
                            movpla.setCantidad(sjE.getCantidad());
                            movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+102+i));
///                            model.tService.getSt().addBatch(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                            model.tService.getSt().addBatch(model.sjextrafleteService.insertarCostoReembolsable());
                        }
                        
                    }
                    else{
                        if(sjE.isSelec()){
                            
                            movpla= new Movpla();
                            movpla.setAgency_id(usuario.getId_agencia());
                            movpla.setCurrency(sjE.getMoneda_costo());
                            movpla.setDstrct(usuario.getDstrct());
                            movpla.setPla_owner(nit);
                            movpla.setPlanilla(numpla);
                            movpla.setSupplier(placa);
                            movpla.setVlr(sjE.getVlrtotal());
                            movpla.setVlr_for(sjE.getVlrtotal());
                            movpla.setDocument_type("001");
                            movpla.setConcept_code(sjE.getCod_extraflete());
                            movpla.setAp_ind("N");
                            movpla.setProveedor("");
                            movpla.setSucursal("");
                            movpla.setCreation_user(usuario.getLogin());
                            movpla.setCantidad(sjE.getCantidad());
                            movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+102+i));
//                            model.tService.getSt().addBatch(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                            model.tService.getSt().addBatch(model.sjextrafleteService.insertarCostoReembolsable());
                        }
                    }
                    
                }
                //COSTOS REEMBOLSABLES
                Vector costos= model.sjextrafleteService.getC_reembolsables();
                logger.info("Cantidad de costos para aplicar "+costos.size());
                for(int i=0; i<costos.size();i++){
                    SJExtraflete sjE = (SJExtraflete)costos.elementAt(i);
                    sjE.setNumpla(numpla);
                    sjE.setNumrem(numre);
                    sjE.setFecdsp("now()");
                    sjE.setTipo("R");
                    sjE.setDistrito(usuario.getDstrct());
                    sjE.setAprobador("");
                    model.sjextrafleteService.setSj(sjE);
                    
                    if(sjE.isSelec()){
                        if( model.sjextrafleteService.estaCostoAplicado()){
                            model.tService.getSt().addBatch(model.sjextrafleteService.ActualizarAplicados());
                        }
                        else{
                            model.tService.getSt().addBatch(model.sjextrafleteService.insertarCostoReembolsable());
                        }
                        
                    }
                    else{
                        model.tService.getSt().addBatch(model.sjextrafleteService.anularCosto());
                    }
                    
                }
                
                
                Vector prencintos = new Vector();
                Precinto p = new Precinto();
                p.setNumpla(numpla);
                p.setUser_update(usuario.getLogin());
                p.setTipo_Documento("Precinto");
                p.setAgencia(usuario.getId_agencia());
                model.tService.getSt().addBatch(model.precintosSvc.desmarcar(p));
                logger.info(model.precintosSvc.desmarcar(p));
                if(!cp1.equals("")){
                    p.setPrecinto(cp1);
                    model.tService.getSt().addBatch(model.precintosSvc.reutilizar(p));
                    prencintos.add(p);
                }
                if(!cp2.equals("")){
                    p.setPrecinto(cp2);
                    model.tService.getSt().addBatch(model.precintosSvc.reutilizar(p));
                }
                prent = request.getParameter("precintos")!=null?request.getParameter("precintos"):"";
                if(!prent.equals("")){
                    p.setPrecinto(prent);
                    model.tService.getSt().addBatch(model.precintosSvc.reutilizar(p));
                }
                precintos =precintos +prent;
                k =2;
                while(k<=5){
                    if(request.getParameter("precintos"+k)!=null){
                        if(!request.getParameter("precintos"+k).equals("")){
                            precintos = precintos+","+request.getParameter("precintos"+k);
                            p.setPrecinto(request.getParameter("precintos"+k));
                            logger.info(model.precintosSvc.reutilizar(p));
                            model.tService.getSt().addBatch(model.precintosSvc.reutilizar(p));
                        }
                    }
                    k++;
                }
                //MODIFICAR STICKERS
                p = new Precinto();
                p.setNumpla(numpla);
                p.setUser_update(usuario.getLogin());
                p.setTipo_Documento("Sticker");
                p.setAgencia(usuario.getId_agencia());
                model.tService.getSt().addBatch(model.precintosSvc.desmarcar(p));
                p.setPrecinto(request.getParameter("sticker"));
                model.tService.getSt().addBatch(model.precintosSvc.reutilizar(p));
                if(cadena.equals("S")){
                    model.tService.getSt().addBatch(model.rmtService.marcarCadena(numpla));
                }
                else{
                    model.tService.getSt().addBatch(model.rmtService.desmarcarCadena(numpla));
                }
                model.tService.execute();
                
                //METODO QUE MODIFICA LOS DATOS EN INGRESO_TRAFICO Y TRAFICO
                model.planillaService.updateTrafico(numpla);
            }
        }catch (SQLException e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
}
