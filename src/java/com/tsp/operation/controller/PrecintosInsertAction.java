/********************************************************************
 *  Nombre Clase.................   PrecintosInsertAction.java
 *  Descripci�n..................   Action de la tabla precintos
 *  Autor........................   Ing. Leonardo Parody Ponce
 *  Fecha........................   22.12.2005
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
/**
 *
 * @author  EQUIPO12
 */
public class PrecintosInsertAction extends Action{
    
    /** Creates a new instance of PrecintosInsertAction */
    public PrecintosInsertAction() {
    }
    public void run() throws ServletException, InformationException {
        //////System.out.println("ESTOY EN EL ACTION");
        List precintos = new LinkedList();
        
        String next = "/jsp/trafico/precinto/PrecintosIngresar.jsp";
        long inicio = Long.parseLong(request.getParameter("inicio"));
        long fin = Long.parseLong(request.getParameter("fin"));
        String agencia = request.getParameter("agencia");
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        String base = usuario.getBase();
        String dstrct = usuario.getDstrct();
        //String nombre = usuario.getLogin();
        String login = usuario.getLogin();
        String tipo_documento = request.getParameter("tipo_documento");
        //////System.out.println("inicio = "+inicio+"  fin = "+fin);
        try{
            String list="";
            List series = model.precintosSvc.ExisteSerie(inicio, fin, tipo_documento);
            int cont = 0;
            String ini = "";
            String fi = "";
            for (int i=0; i<series.size(); i++){
                list += ", "+(String)series.get(i);
                if(cont == 0){
                   ini = (String)series.get(i);
                   cont++;
                }else{
                    fi = (String)series.get(i);
                }
            }
            //////System.out.println("cont = "+ series.size());
            if (series.size() == 0) {
                for (long i=inicio; i<=fin; i++){
                    Precinto precinto= new Precinto();
                    //////System.out.println("precintos = "+i+"   agencia = "+agencia);
                    precinto.setPrecinto(""+i);
                    precinto.setAgencia(agencia);
                    precinto.setBase(base);
                    precinto.setDstrct(dstrct);
                    precinto.setCreation_user(login);
                    precinto.setUser_update(login);
                    precinto.setTipo_Documento(tipo_documento);
                    //////System.out.println("precintos = "+precinto.getPrecinto()+"   agencia = "+agencia);
                    model.precintosSvc.IngresarPrecintos(precinto);
                }
                next +="?msg=Serie Ingresada Satisfactoriamente";
            }else{
                next +="?msg=Los "+tipo_documento+"s : en el rango "+ini+" - "+fi +" de serie  ya existen";
            }
            //////System.out.println("agregue a la tabla");
        }catch (SQLException e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
    }
    
}
