/***********************************************
 * Nombre clase: Cumplido_docCargarAction.java
 * Descripci�n: Accion para buscar un cumplido
 * Autor: Rodrigo Salazar
 * Fecha: 1 de octubre de 2005, 11:52 AM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 **********************************************/

package com.tsp.operation.controller;

/**
 *
 * @author  R.SALAZAR
 */

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
public class Cumplido_docCargarAction extends Action{
    
    /** Creates a new instance of Cumplir_docCargarAction */
    public Cumplido_docCargarAction() {
    }
    public void run() throws ServletException{
        //System.out.println("ACTION");
        String next="/jsp/cumplidos/cumplido_doc/ListaDocs.jsp";
        HttpSession session = request.getSession();
        Cumplido cump;
        String rem = request.getParameter("rem").toUpperCase();
        //System.out.println("REM"+rem);
        List vec;
        try{
            model.RemDocSvc.Listar(rem);
            vec = model.RemDocSvc.getList();
            request.setAttribute("documentos",vec);
        }
        catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        // Redireccionar a la p�gina indicada.
         this.dispatchRequest(next);

    }
}
