/*
 * MenuEnviarAction.java
 *
 * Created on 29 de noviembre de 2004, 10:46 AM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

/**
 * * @author  KREALES */
public class MenuEnviarAction extends Action{
    /*** Creates a new instance of MenuEnviarAction */
    public MenuEnviarAction() { }
    public void run() throws javax.servlet.ServletException, InformationException {
        String next="/index.jsp";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        model.planillaService.setPlanilla(null);
        model.usuarioService.setTusuarios(new TreeMap());
        model.causas_anulacionService.setTcausasa(new TreeMap());
        model.trecuperacionaService.setTreeTRA(new TreeMap());
        model.planillaService.setPlanilla(null);
        model.remesaService.setRemesa(null);
        model.movplaService.setMovPla(null);
        model.movplaService.setLista(null);
        model.RemDocSvc.setDocumentos(null);//Juan 12.11.05
        
        
        
        if(request.getParameter("numero")!=null){
            if(request.getParameter("numero").equals("1")){
                if(request.getParameter("base").equalsIgnoreCase("spo"))
                    next="/despacho2/InicioDespacho.jsp";
                else
                    next="/despacho/InicioMasivo.jsp";
                
            }
            else if(request.getParameter("numero").equals("2")){
                next="/ModificarDespacho/InicioModificar.jsp";
            }
            else if(request.getParameter("numero").equals("3")){
                next="/anularDespacho/buscarPlanilla.jsp";
            }
            else if(request.getParameter("numero").equals("4")){
                next="/acpm/acpmInsert.jsp";
            }
            else if(request.getParameter("numero").equals("5")){
                next="/acpm/acpmUpdate.jsp";
            }
            else if(request.getParameter("numero").equals("6")){
                next="/acpm/acpmDelete.jsp";
            }
            else if(request.getParameter("numero").equals("7")){
                next="/anticipo/anticipoInsert.jsp";
            }
            else if(request.getParameter("numero").equals("8")){
                next="/anticipo/anticipoUpdate.jsp";
            }
            else if(request.getParameter("numero").equals("9")){
                next="/anticipo/anticipoDelete.jsp";
            }
            else if(request.getParameter("numero").equals("10")){
                next="/tiquetes/tiketInsert.jsp";
            }
            else if(request.getParameter("numero").equals("11")){
                next="/tiquetes/tiketUpdate.jsp";
            }
            else if(request.getParameter("numero").equals("12")){
                next="/tiquetes/tiketDelete.jsp";
            }
            else if(request.getParameter("numero").equals("13")){
                next="/sjdelay/sjdelayInsert.jsp";
            }
            else if(request.getParameter("numero").equals("14")){
                next="/sjdelay/sjdelayDelete.jsp";
            }
            else if(request.getParameter("numero").equals("15")){
                next="/peajes/peajeInsert.jsp";
            }
            else if(request.getParameter("numero").equals("16")){
                next="/peajes/peajeUpdate.jsp";
            }
            else if(request.getParameter("numero").equals("17")){
                next="/peajes/peajeDelete.jsp";
            }
            else if(request.getParameter("numero").equals("18")){
                next="/placas/placaInsert.jsp";
            }
            else if(request.getParameter("numero").equals("19")){
                next="/placas/placaUpdate.jsp";
            }
            else if(request.getParameter("numero").equals("20")){
                next="/colpapel/preliminar.jsp";
                TreeMap t = new TreeMap();
                model.stdjobdetselService.setCiudadesDest(t);
                model.stdjobdetselService.setCiudadesOri(t);
                model.stdjobdetselService.setStdjobTree(t);
                 try{
                    model.imprimirOrdenService.listar(usuario);
                }catch (SQLException e){
                    throw new ServletException(e.getMessage());
                }
            }
            else if(request.getParameter("numero").equals("21")){
                next="/colpapel/InicioModificar.jsp";
            }
            else if(request.getParameter("numero").equals("22")){
                try{
                    model.trecuperacionaService.llenarTree();
                    model.causas_anulacionService.llenarTree();
                    
                }catch(SQLException e){
                    throw new ServletException(e.getMessage());
                }
                next="/colpapel/anularDespacho.jsp";
            }
            else if(request.getParameter("numero").equals("23")){
                next="/registrotiempo/InicioDespacho.jsp";
            }
            else if(request.getParameter("numero").equals("24")){
                next="/colpapel/InicioDespacho.jsp";
            }
            else if(request.getParameter("numero").equals("25")){
                next="/colpapel/InicioModificar.jsp";
            }
            else if(request.getParameter("numero").equals("26")){
                next="/registrotiempo/registrotiempo.jsp";
            }
            else if(request.getParameter("numero").equals("27")){
                next="/colpapel/agregarPlanilla.jsp";
                try{
                    model.stdjobcostoService.searchRutas("");
                }catch (SQLException e){
                    throw new ServletException(e.getMessage());
                }
            }
            else if(request.getParameter("numero").equals("28")){
                next="/colpapel/anularPlanilla.jsp";
            }
            else if(request.getParameter("numero").equals("29")){
                next="/colpapel/agregarRemesa.jsp";
                TreeMap t = new TreeMap();
                model.stdjobdetselService.setCiudadesDest(t);
                model.stdjobdetselService.setCiudadesOri(t);
                model.stdjobdetselService.setStdjobTree(t);
            }
            else if(request.getParameter("numero").equals("30")){
                next="/colpapel/anularRemesa.jsp";
            }
            else if(request.getParameter("numero").equals("31")){
                next="/colpapel/relacionar.jsp";
            }
            else if(request.getParameter("numero").equals("32")){
                next="/cumplidos/incioCumplido.jsp";
            }
            else if(request.getParameter("numero").equals("33")){
                next="/reanticipo/incioReAnticipo.jsp";
            }
            else if(request.getParameter("numero").equals("34")){
                next="/consultas/consultasPlanilla.jsp";
                if(request.getParameter("normal")!=null){
                    next="/consultas/consultasPlanillaNormal.jsp";
                }
            }
             else if(request.getParameter("numero").equals("35")){
                try {
                    Vector a = null;
                    model.stdjobdetselService.setStdjob( a );
                } catch (SQLException e) {
                    e.printStackTrace();
                }
                next="/consultas/consultasStdjob.jsp?marco=no";
            }
            else if(request.getParameter("numero").equals("36")){
                next="/consultas/consultasRemesa.jsp";
            }
            else if(request.getParameter("numero").equals("37")){
                next="/consultas/consultasConductor.jsp";
                if(request.getParameter("despacho")!=null)
                    next = next +"?despacho=ok";
            }
            else if(request.getParameter("numero").equals("38")){
                next="/consultas/consultasEgreso.jsp";
            }
            else if(request.getParameter("numero").equals("39")){
                next="/consultas/consultasAcpm.jsp";
            }
            else if(request.getParameter("numero").equals("40")){
                next="/consultas/consultasTiquetes.jsp";
            }
            else if(request.getParameter("numero").equals("41")){
                next="/consultas/consultasAnticipo.jsp?marco=no";
            }
            else if(request.getParameter("numero").equals("42")){
                next="/configuracion/consultasStdjob.jsp";
            }
            else if(request.getParameter("numero").equals("43")){
                next="/compa�ia/companiaInsert.jsp";
            }
            else if(request.getParameter("numero").equals("44")){
                next="/compa�ia/companiaUpdate.jsp";
            }
            else if(request.getParameter("numero").equals("45")){
                next="/compa�ia/companiaDelete.jsp";
            }
            else if(request.getParameter("numero").equals("46")){
                next="/diferenciatiempos/tiempoInsert.jsp";
            }
            else if(request.getParameter("numero").equals("47")){
                next="/diferenciatiempos/tiempoUpdate.jsp";
            }
            else if(request.getParameter("numero").equals("48")){
                next="/diferenciatiempos/tiempoAnular.jsp";
            }
            else if(request.getParameter("numero").equals("49")){
                next="/tramos/tramoInsert.jsp";
            }
            else if(request.getParameter("numero").equals("50")){
                next="/tarifas_diferenciales/sjgrupInsert.jsp";
            }
            else if(request.getParameter("numero").equals("51")){
                next="/tarifas_diferenciales/sjgrupUpdate.jsp";
            }
            else if(request.getParameter("numero").equals("53")){
                next="/tarifas_diferenciales/sjgrupDelete.jsp";
            }
            else if(request.getParameter("numero").equals("52")){
                
                File f = new File("/exportar/masivo/Sincronizacion/servidor/");
                Vector lista = new Vector();
                File[] vec=f.listFiles();
                for(int i=0; i<vec.length; i++){
                    if(vec[i].isFile()){
                        lista.add(vec[i].getName());
                    }
                }
                request.setAttribute("lista", lista);
                
                f = new File("/exportar/masivo/Sincronizacion/servidor/recibido/");
                lista = new Vector();
                vec=f.listFiles();
                for(int i=0; i<vec.length; i++){
                    if(vec[i].isFile())
                        lista.add(vec[i].getName());
                }
                request.setAttribute("journal", lista);
                next="/Sincronizacion/Consultas.jsp";
            }
            else if(request.getParameter("numero").equals("54")){
                next="/informes/FechasInfo.jsp";
            }
            else if(request.getParameter("numero").equals("540")){
                next="/informes/FechasInfo2.jsp";
            }
            else if(request.getParameter("numero").equals("55")){
                next="/informes/FechasInfo.jsp?egreso=0k";
            }
            else if(request.getParameter("numero").equals("550")){
                next="/informes/FechasInfo2.jsp?egreso=0k";
            }
            else if(request.getParameter("numero").equals("56")){
                next="/informes/FechasArchivo.jsp";
            }
            else if(request.getParameter("numero").equals("57")){
                next="/anticipos/anticipoInsert.jsp";
            }
            else if(request.getParameter("numero").equals("58")){
                try{
                    model.anticiposService.vecAnticipos(usuario.getBase());
                    
                    next="/anticipos/anticipoUpdate.jsp";
                }catch (SQLException e){
                    throw new ServletException(e.getMessage());
                }
                
            }
            else if(request.getParameter("numero").equals("59")){
                try{
                    model.anticiposService.vecAnticipos(usuario.getBase());
                    
                    next="/anticipos/anticipoDelete.jsp";
                }catch (SQLException e){
                    throw new ServletException(e.getMessage());
                }
            }
            else if(request.getParameter("numero").equals("60")){
                next="/proveedores/proveedorInsert.jsp";
            }
            else if(request.getParameter("numero").equals("61")){
                try{
                    model.proveedoresService.vecProveedor(usuario.getBase());
                    
                    next="/proveedores/proveedorUpdate.jsp";
                }catch (SQLException e){
                    throw new ServletException(e.getMessage());
                }
            }
            else if(request.getParameter("numero").equals("62")){
                try{
                    model.proveedoresService.vecProveedor(usuario.getBase());
                    
                    next="/proveedores/proveedorDelete.jsp";
                }catch (SQLException e){
                    throw new ServletException(e.getMessage());
                }
            }
            else if(request.getParameter("numero").equals("63")){
                try{
                    model.anticiposService.vecAnticipos(usuario.getBase());
                    next="/anticipos/proveedorAnticipoInsert.jsp";
                }catch (SQLException e){
                    throw new ServletException(e.getMessage());
                }
            }
            else if(request.getParameter("numero").equals("64")){
                next="/migracion/plasColpapel.jsp";
            }
            else if(request.getParameter("numero").equals("65")){
                next="/informes/FechasInfoExcel.jsp";
            }
            
            else if(request.getParameter("numero").equals("66")){
                try{
                    model.planillaService.llenarCorte(usuario.getBase());
                    String fecpla = model.planillaService.buscarFecCorte(usuario.getBase());
                    
                    int year=Integer.parseInt(fecpla.substring(0,4));
                    int month=Integer.parseInt(fecpla.substring(5,7))-1;
                    int date= Integer.parseInt(fecpla.substring(8,10));
                    
                    Calendar fecha_ant = Calendar.getInstance();
                    fecha_ant.set(year,month,date+1);
                    
                    Date d = fecha_ant.getTime();
                    SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");
                    String feccort = s.format(d);
                    
                    next="/Corte/corte.jsp?fec="+feccort;
                    
                }catch (SQLException e){
                    throw new ServletException(e.getMessage());
                }
            }
            else if(request.getParameter("numero").equals("001")){
                TreeMap t = new TreeMap();
                model.stdjobdetselService.setCiudadesDest(t);
                model.stdjobdetselService.setCiudadesOri(t);
                model.stdjobdetselService.setStdjobTree(t);
                next="/colpapel/agregarRemesaLista.jsp";
            }
            else if(request.getParameter("numero").equals("67")){
                next="/colpapel/agregarOTpadre.jsp";
            }
            //////////////////////sandrameg
            else if(request.getParameter("numero").equals("68")){
                next="/conciliacion_placas/conciliacion.jsp?sw=0";
            }
            else if(request.getParameter("numero").equals("69")){
                next="/propietarios/propietarioInsert.jsp?msg=";
            }
            else if(request.getParameter("numero").equals("70")){
                next="/propietarios/propietarioSearch.jsp?msg=vacio";
            }
            //kreales
            else if(request.getParameter("numero").equals("71")){
                next="/cheques/buscarCheque.jsp";
            }
            //jcuesta
            else if(request.getParameter("numero").equals("72")){
                next="/informes/FechasReporteViajes.jsp";
            }
            else if(request.getParameter("numero").equals("73")){
                next="/anticipo_placas/anticipopInsert.jsp?msg=vacio";
            }
            else if(request.getParameter("numero").equals("74")){
                next="/anticipo_placas/anticipopSearch.jsp?msg=vacio";
            }
            else if(request.getParameter("numero").equals("75")){
                next="/cheques/buscarChequeNoExist.jsp";
            }
            else if(request.getParameter("numero").equals("76")){
                next="/informes/FechasReporteViajes.jsp?peso=ok";
            }
            //DIOGENES
            else if(request.getParameter("numero").equals("77")){
                try{
                    model.trecuperacionaService.llenarTree();
                    model.causas_anulacionService.llenarTree();
                    next="/cheques/buscarChequeImpre.jsp";
                }catch (SQLException e){
                    throw new ServletException(e.getMessage());
                }
            }
            else if(request.getParameter("numero").equals("78")){
                next="/cheques/buscarChequeNoImpre.jsp";
            }
            //alejo
            else if(request.getParameter("numero").equals("79")){
                next="/cheques/mensajeMigracion.jsp";
                HExport_Migracion h = new HExport_Migracion(usuario.getBd());
                h.start();
            }
            else if(request.getParameter("numero").equals("80")){
                next="/bancoUsuario/BancoUsuario.jsp";
                session.setAttribute("bancos",null);
            }
            else if(request.getParameter("numero").equals("81")){
                next="/cliente/setbuscarCli.jsp";
            }
            else if(request.getParameter("numero").equals("82")){
                next="/agencia/escojerAgenciasParaReporte.jsp";
            }
            else if(request.getParameter("numero").equals("83")){
                next="/migracion/migracionRemesasAnuladas.jsp";
            }
            else if(request.getParameter("numero").equals("84")){
                next="/extraflete/extraflete.jsp?opcion=CL000000ST000000RT00EF00&ruta=&men=";
            }
            else if(request.getParameter("numero").equals("85")){
                next="/extraflete/setbuscarExtraflete.jsp";
            }
            else if(request.getParameter("numero").equals("86")){
                next="/jsp/masivo/extraflete/codextrafleteInsert.jsp";
            }
            else if(request.getParameter("numero").equals("87")){
                next="/jsp/masivo/extraflete/buscarCodextrafletes.jsp";
            }
            else if(request.getParameter("numero").equals("88")){
                next="/jsp/trafico/menu/InsertarOpcionMenu.jsp?msg=";
            }
            else if(request.getParameter("numero").equals("89")){
                next="/jsp/trafico/menu/ModificarOpcionMenu.jsp";
            }
            else if(request.getParameter("numero").equals("90")){
                next="/jsp/trafico/menu/AnularOpcionMenu.jsp";
            }
            else if(request.getParameter("numero").equals("91")){
                next="/jsp/trafico/perfil/Perfil.jsp?tipo=Ingresar&msg=";
            }
            else if(request.getParameter("numero").equals("92")){
                next="/jsp/trafico/perfil/BuscarPerfil.jsp";
            }
            else if(request.getParameter("numero").equals("93")){
                next="/VehExternos/VExternos.jsp";
            }
            else if(request.getParameter("numero").equals("94")){
                next="/pags_predo/InfoxAgencia.jsp";
            }
        }else{
            //planeacion
            if(request.getParameter("op").equals("1")){
                next="/pags_predo/reqCliente.jsp?msg=vacio&tipo=nuevo_pto";
            }
            else if(request.getParameter("op").equals("2")){
                next="/pags_predo/reqCliente.jsp?msg=vacio&tipo=act_pto";
            }
            else if(request.getParameter("op").equals("3")){
                next="/pags_predo/recTSP.jsp?msg=vacio&tipo=nuevo";
            }
            else if(request.getParameter("op").equals("4")){
                next="/pags_predo/recTSP.jsp?msg=vacio&tipo=act";
            }
            else if(request.getParameter("op").equals("5")){
                next="/pags_predo/cruce.jsp?msg=vacio";
            }
            else if(request.getParameter("op").equals("6")){
                next="/pags_predo/procesos.jsp?msg=vacio";
            }
            else if(request.getParameter("op").equals("7")){
                next="/pags_predo/reqClienteF.jsp?msg=vacio&tipo=nuevo_fron";
            }
            else if(request.getParameter("op").equals("8")){
                next="/pags_predo/reqClienteF.jsp?msg=vacio&tipo=act_fron";
            }
            else if(request.getParameter("op").equals("9")){
                next="/pags_predo/FechasInfo.jsp";
            }
            else if(request.getParameter("op").equals("10")){
                next="/reportePlaneacion/mostrarEditorDeReporte.jsp";
            }
            else if(request.getParameter("op").equals("11")){
                next="/controller?estado=Log&accion=Proceso&Estado=ACTIVO";
            }
            else if(request.getParameter("op").equals("12")){
                next="/VAlquiler/VAlquiler.jsp";
                TreeMap t = new TreeMap();
                model.stdjobdetselService.setCiudadesDest(t);
                model.stdjobdetselService.setCiudadesOri(t);
                model.stdjobdetselService.setStdjobTree(t);
            }
            else if(request.getParameter("op").equals("13")){
                next="/jsp/trafico/AsignacionUsuariosClientes/AsignacionUsuariosClientes.jsp";
                try{model.AsigUsuCliSvc.Init();}catch (Exception e){throw new ServletException(e.getMessage());}
            }
        }
        this.dispatchRequest(next);
    }
    
}
