/*Created on 16 de septiembre de 2005, 09:05 AM
 */

package com.tsp.operation.controller;

/**@author  fvillacob*/




import java.io.*;
import java.util.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.exceptions.*;


public class MenuProyectoAction extends Action{
    
   
    
    public void run() throws ServletException, InformationException {
        try{
           
           String comentario="";
           String []UsuariosA  = request.getParameterValues ("UsuariosA");
           String []GruposA    = request.getParameterValues ("GruposNA");
           String Opcion       = request.getParameter ("Opcion");
           String user         = request.getParameter ("usuario");
           
           if (Opcion!=null){
              if (Opcion.equals ("Grabar")){
                    model.ProyectoSvc.Insert(UsuariosA, GruposA, user);
                    comentario="Actualización realizada...";
              }
           }
           
           
           String next = "/proyectos/Mantenimiento.jsp?comentario="+comentario;
           RequestDispatcher rd = application.getRequestDispatcher(next);
           if(rd == null)
              throw new Exception("No se pudo encontrar "+ next);
           rd.forward(request, response); 
           
        }catch(Exception e){
            throw new ServletException("Accion:"+ e.getMessage());
        } 
    }
    
}
