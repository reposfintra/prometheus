/***********************************************
 * Nombre clase: Discrepancia_generalCierreAction.java
 * Descripci�n: Accion para cerrar los productos de las discrepancia y si los productos estan
 *              cerrados se cierra la discrepancia
 * Autor: Jose de la rosa
 * Fecha: 24 de noviembre de 2005, 11:17 AM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 **********************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;

public class Discrepancia_generalCierreAction extends Action{
    
    /** Creates a new instance of Discrepancia_generalCierreAction */
    public Discrepancia_generalCierreAction () {
    }
    
    public void run () throws ServletException, com.tsp.exceptions.InformationException {
        String next="";
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String distrito = (String) session.getAttribute ("Distrito");
        Date hoy = new Date ();
        SimpleDateFormat s = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
        String fecha_hoy = s.format (hoy);
        
        int num_discre = Integer.parseInt (request.getParameter ("c_num_discre"));
        String numpla = request.getParameter ("c_numpla").toUpperCase ();
        
        String nota_debito = request.getParameter ("c_nota_debito");
        String usuario_debito = (request.getParameter ("c_nota_debito")!=null)?usuario.getLogin ():"";
        String fecha_debito = (request.getParameter ("c_nota_debito")!=null)?fecha_hoy:"0099-01-01 00:00:00";
        
        String nota_credito = request.getParameter ("c_nota_credito");
        String usuario_credito = (!request.getParameter ("c_nota_credito").equals (""))?usuario.getLogin ():"";
        String fecha_credito = (!request.getParameter ("c_nota_credito").equals (""))?fecha_hoy:"0099-01-01 00:00:00";
        
        String referencia = (!request.getParameter ("c_referencia").equals (""))?request.getParameter ("c_referencia").toUpperCase ():"";
        String cant = (!request.getParameter ("c_cantidad").equals (""))?request.getParameter ("c_cantidad"):"0";
        double cantidad = Double.parseDouble (cant);
        String observacion = request.getParameter ("c_observacion"); 
        try{
            Discrepancia dis = new Discrepancia ();
            dis.setNro_discrepancia (num_discre);
            dis.setNro_planilla (numpla);
            dis.setNro_remesa ("");
            dis.setTipo_doc ("");
            dis.setDocumento ("");
            dis.setTipo_doc_rel ("");
            dis.setDocumento_rel (""); 
            
            dis.setObservacion_cierre (observacion);
            
            dis.setUsuario_cierre (usuario.getLogin ());
            dis.setFecha_cierre (fecha_hoy);
                        
            dis.setCantidad_autorizada (cantidad);
            dis.setReferencia (referencia);
            
            dis.setNota_debito (nota_debito);
            dis.setUsuario_nota_debito (usuario_debito);
            dis.setFecha_nota_debito (fecha_debito);
            
            dis.setNota_credito (nota_credito);
            dis.setUsuario_nota_credito (usuario_credito); 
            dis.setFecha_nota_credito (fecha_credito);
            request.setAttribute ("msg", "Discrepancia Cerrada");
            model.discrepanciaService.insertarCierreDiscrepanciaDocumento (dis);
            
            next = "/jsp/cumplidos/discrepancia/DiscrepanciaCierreGeneral.jsp?reload=ok&c_numpla=" + numpla+"&marco=no";
            model.discrepanciaService.searchDiscrepanciaGeneral(numpla, num_discre, distrito);
        }catch (SQLException e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
    
}
