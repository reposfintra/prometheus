/*************************************************************************
 * Nombre ......................InfoactBuscaractAction.java              *
 * Descripci�n..................Clase Action para buscar actividad       *
 * Autor........................Ing. Diogenes Antonio Bastidas Morales   *
 * Fecha........................1 de septiembre de 2005, 06:05 PM        *
 * Versi�n......................1.0                                      *
 * Coyright.....................Transportes Sanchez Polo S.A.            *
 *************************************************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
/**
 *
 * @author  Diogenes
 */
public class InfoactBuscaractclinAction extends Action {
    
    /** Creates a new instance of InfoactBuscaractAction */
    public InfoactBuscaractclinAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina");
        String cod = request.getParameter("cliente");
        String tipo = request.getParameter("tipo");
        String numpla = request.getParameter("numpla");
        String numrem = request.getParameter("remesa");
        String nom = request.getParameter("nomcliente");
        String reporte = request.getParameter("est")!= null? request.getParameter("est"): "";
        Planilla pla = new Planilla();
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        boolean est = false;
        try{
            //busca la info planilla si es del reporte
            if(reporte.equals("reporte") ){
                model.actplanService.buscarInfoplanilla(numpla);
                pla = model.actplanService.getPlan();
                model.clientactSvc.setPla(model.actplanService.getPlan());
            }
            //obtengo la planilla
            pla = model.clientactSvc.getPla();
            if(pla!=null){
                pla.setNumrem(numrem);
                pla.setClientes(cod);
                pla.setTipoviaje(tipo);
                pla.setNomcliente(nom);
                model.clientactSvc.setPla(pla);
                //busco la primera actividad del cliente
                est = model.clientactSvc.buscarActCliente(usuario.getCia(), cod, tipo,numrem);
                if (est){
                    ClienteActividad ca =  model.clientactSvc.obtClienteActividad();
                    //busca la actividad
                    model.actividadSvc.buscarActividad(ca.getCodActividad(), ca.getDstrct());
                    model.jerarquia_tiempoService.buscarCausaAct(ca.getCodActividad());
                    model.jerarquia_tiempoService.buscarResponsablaAct(ca.getCodActividad());
                    
                    boolean a = model.infoactService.buscarInfoActividad(ca.getCodActividad(), cod, ca.getDstrct(),numpla, numrem);
                    next=next+"?act="+a;
                    if(!a){
                        next+="&ult_fec="+model.infoactService.fecha_ult_act_Registrada(cod, ca.getDstrct(), numpla, numrem);
                    }
                    
                    model.clientactSvc.actiClientTipo(usuario.getCia(),cod,tipo);
                    
                }
                else{
                    est = model.actplanService.BuscarCLientexPlanilla(numpla);
                    next="/jsp/trafico/actividad/Infoactividad/ClientesxPlan.jsp?men=Noact";
                }
            }
            else{
                next="/jsp/trafico/actividad/Infoactividad/BuscarClientexPlan.jsp?men=false";
            }
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        ////System.out.println(next);
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
    }
    
    
}
