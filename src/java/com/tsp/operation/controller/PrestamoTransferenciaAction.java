/**
* Autor : TMolina
* Date  : 04 - Sept - 2008
* Copyrigth Notice : Fintravalores S.A. 
* Version 1.0
-->
<%--
-@(#)
--Descripcion : Action encargada del proceso de transferencia de prestamos
**/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.threads.*;
import com.tsp.util.Util;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.TransaccionService;
import javax.servlet.http.*;
import java.sql.SQLException;
/**
 *
 * @author  TMolina
 */
public class PrestamoTransferenciaAction extends Action{
    
    /** Creates a new instance of PrestamoTransferenciaAction */
    public PrestamoTransferenciaAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String      next    = "/jsp/cxcobrar/prestamos/inicio_transferencias.jsp";
        HttpSession session = request.getSession();
        Usuario     usuario = (Usuario)session.getAttribute("Usuario");
        String      evento      = ((String)request.getParameter("evento"));
        com.tsp.operation.model.beans.SendMail email = new com.tsp.operation.model.beans.SendMail();
        System.out.println(evento);
        if(evento.equals("1")){ //Buscar Prestamos.
           String   banco = (String) request.getParameter("val");
           String[] bn;
           bn = banco.split(",");
           System.out.println(bn[0]);
           session.setAttribute("bcosel", bn[0]);  //Banco Seleccionado
           try{
               System.out.println("Entro al try");
              model.PrestamoSvc.getPrestamosAprobados(); //Lista de los prestamos Aprobados
           }
           catch(Exception e){
              throw new ServletException(e.getMessage());
           }          
           next = "/jsp/cxcobrar/prestamos/listaPrestamos.jsp?";
           System.out.println(next);
       }//Fin Evento 1
        
       if(evento.equals("3")){
           String msj = "";
           try{
               BeanGeneral mailinfo= new BeanGeneral();
               
               TransaccionService tService = new TransaccionService(usuario.getBd());
               tService.crearStatement();
               
               String[] listPrestamos = request.getParameterValues("listado");    //listado de codigos de prestamos
               String[] comisiones    = request.getParameterValues("selcom");     //listado de comisiones
               String[] cuatroXMils   = request.getParameterValues("cuatroxmil"); //listado de valores 4 x Mil
               
               String[] vCodPrestamos = new String[listPrestamos.length];           //vector con los codigos de los prestamos
               String iComisiones = "";                                             //Posicion en la lista de comisiones
               //String vCuatroXMil = "";                                             //vector con los valores del 4 x Mil
               String[] temp = new String[3];//iniciada
               
               String banco = (String)request.getParameter("bco");
               String[] bn;
               bn           = banco.split(",");   //Banco,Sucursal,Cuenta
               
               //--Actualizando las comisiones y cuatro x Mil de los prestamos Transferidos--//
               for(int j=0; j<listPrestamos.length; j++){
                   temp = listPrestamos[j].split(",");
                   vCodPrestamos[j] = temp[0];
                   iComisiones      = temp[1];
                   try{
                        int index = Integer.parseInt(iComisiones);
                        System.out.println(index);
                        System.out.println(temp[0]);
                        System.out.println(temp[1]);
                        model.PrestamoSvc.updateValoresPrestamo(vCodPrestamos[j],comisiones[index],bn[0],bn[1],cuatroXMils[index]);
                   }catch(Exception e){
                       throw new ServletException(e.getMessage());
                   }
                   iComisiones = "";
               }
               temp = null;//ya la use
               
               HArchivosTransferencias  hilo =  new  HArchivosTransferencias();
               String codB = "";    //Banco
               int sw     = 0;
               if (bn[0].equals("BANCOLOMBIA")){
                   codB="07";
               }else{
                   if (bn[0].equals("BANCO OCCIDENTE")){
                       codB="23";
                   }else{
                       msj  = "No existen formatos para este banco.";
                       sw   = 1;
                   }
               }
               
               if (sw==0){
                   msj  = "El archivo para transferir al banco "+  bn[0] +" est� siendo generado....";
                   hilo.start(model, usuario,vCodPrestamos,"8020220161", "FINTRA,PRESTAMO", codB, bn[0],  bn[2], bn[1],bn); 
               }
               hilo.join();
               model.PrestamoSvc.inicializarNits();
               for (int j=0;j<vCodPrestamos.length;j++){
                   try{
                       model.PrestamoSvc.upPrestamo(vCodPrestamos[j]);
                   }catch(Exception e){
                       throw new Exception(e.getMessage());
                   }
               }
          /*     try{
                  //model.PrestamoSvc.save(vCodPrestamos,usuario.getLogin());   
               }catch(Exception ep){
                    throw new Exception(ep.getMessage());
               }*/
               for (int j=0;j<vCodPrestamos.length;j++){
                   tService.getSt().addBatch(  model.PrestamoSvc.save(vCodPrestamos[j], usuario.getLogin()));
               }
               tService.execute();
               bn    = null;
               banco = null; 
           }catch (Exception e){
               msj = "No se pudo realizar el archivo de transferencias intente mas tarde.";
               throw new ServletException(e.getMessage());
           }
           next = "/jsp/cxcobrar/prestamos/resultado_tranferencias.jsp?msg="+msj;
       }
        
      /* if(evento.equals("4")){
           String con=((String)request.getParameter("com"));
           System.out.println(con);
           String val=((String)request.getParameter("val"));
           System.out.println(val);
           int i=Integer.parseInt(val)-Integer.parseInt(con);
           System.out.println(i);
           next = "/jsp/fenalco/negocios/bcotext.jsp?ms="+i;  
       } */
       this.dispatchRequest(next);
        
    }//End Run  
}
