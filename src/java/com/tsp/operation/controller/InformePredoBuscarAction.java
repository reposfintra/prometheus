/*
 * InformePredoBuscarAction.java
 *
 * Created on 1 de agosto de 2005, 12:11 AM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  DIOGENES
 */
public class InformePredoBuscarAction extends Action {
    
    /** Creates a new instance of InformePredoBuscarAction */
    public InformePredoBuscarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String agencia =request.getParameter("age");
        String fechai =request.getParameter("fec1");
        String fechaf =request.getParameter("fec2");
        String next = "/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina");
        try{
            if(request.getParameter("historial")==null){
                ////System.out.println("Fecha1 "+ fechai+" Fecha2 "+ fechaf);
                model.ipredoSvc.ListarxAgencia(agencia, fechai, fechaf);
            }else{
                model.ipredoSvc.buscarHistorial(request.getParameter("placa"));
                
            }
            
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
}
