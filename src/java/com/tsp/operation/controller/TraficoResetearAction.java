/********************************************************************
 *      Nombre Clase.................   TraficoResetearAction.java
 *      Descripci�n..................   Action para reiniciar el programa de control trafico,borra las 
 *                                      acciones realizadas por el usuario
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.11.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import javax.servlet.*;
import javax.servlet.http.*;
/**
 *
 * @author  dlamadrid
 */
public class TraficoResetearAction extends Action{
    
    /** Creates a new instance of TraficoResetarAction */
    public TraficoResetearAction() {
    }
    
   public void run() throws ServletException, InformationException {
        
        String next="";
        
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String user=""+usuario.getLogin ();
        ////System.out.println ("Usuario:"+user);
        try{
            
            model.traficoService.obtenerZonasPorUsuario (user);
            model.traficoService.gennerarVZonasTurno(user);
            //model.traficoService.obtenerZonasPorUsuario (user);
           // model.traficoService.generarVZonas (user);
            ////System.out.println ("Genrerar zonas ");
            Vector zonas = model.traficoService.obdtVectorZonas ();
            String validacion=" zona='-1'";
            ////System.out.println ("genera zonas ");
            if ((zonas != null)||zonas.size ()>0)
            {
                validacion = model.traficoService.validacion (zonas);
            }
            ////System.out.println ("termina zonas "+validacion );
            
            
            ////System.out.println ("entra a zonas");
            java.util.Date utilDate = new java.util.Date (); //fecha actual
            long lnMilisegundos = utilDate.getTime ();
            java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp (lnMilisegundos);
            model.traficoService.actualizarZonas (validacion, sqlTimestamp);
            ////System.out.println ("sALIO DE ACTUALIZAR ZONAS................................");
            
            String var2="";
            String var1="";
            String var3="";
            String configuracion="";
            String clas="";
            String linea="";
            String filtro="";
            Vector colum= new Vector ();
            String consulta="";
            ConfTraficoU vista = new ConfTraficoU ();
              
                var2=model.traficoService.obtVar1 ();
                var2=model.traficoService.codificar (var2);
                var3= model.traficoService.obtVar3 ();
                filtro=" where "+validacion;
                configuracion=model.traficoService.obtConf()+", color_letra,CASE WHEN reg_status='D' THEN get_ultimaobservaciontrafico(planilla,'detencion') ELSE ult_observacion END AS obs,neintermedias";
                linea=model.traficoService.obtlinea ();
                
                model.traficoService.listarColTrafico (var3);
                Vector col = model.traficoService.obtColTrafico ();
                model.traficoService.listarColtrafico2 (col);
                
                
                consulta= model.traficoService.obtConf ()+  ",reg_status,ult_observacion,color_letra,CASE WHEN reg_status='D' THEN get_ultimaobservaciontrafico(planilla,'detencion') ELSE ult_observacion END AS obs,neintermedias  from ingreso_trafico where "+validacion;
                model.traficoService.listarCamposTrafico (linea);
                colum = model.traficoService.obtenerCampos (linea);
                ////System.out.println("CONSULTA "+consulta);
                model.traficoService.generarTabla (consulta, colum);
                ////System.out.println ("genera consulta");
                
                vista.setConfiguracion (configuracion);
                vista.setFiltro (filtro);
                vista.setLinea (linea);
                vista.setClas (clas);
                vista.setVar1 (var1);
                vista.setVar2 (var2);
                vista.setVar3 (var3);
                vista.setCreation_user (user);
                model.traficoService.actualizarConfiguracion(vista);
                
              filtro=model.traficoService.codificar(filtro);
            ////System.out.println ("RUTA"+next);
             next = "/jsp/trafico/controltrafico/vertrafico2.jsp?var1="+var1+"&var2="+var2+"&var3="+var3+"&filtro="+filtro+"&linea="+linea+"&conf="+configuracion+"&clas="+clas+"";
              
            ////System.out.println("sale del next ");
            
           RequestDispatcher rd = application.getRequestDispatcher(next);
           if(rd == null)
              throw new Exception("No se pudo encontrar "+ next);
           rd.forward(request, response); 
        }
        catch(Exception e){
          throw new ServletException("Accion:"+ e.getMessage());
        } 
    }
    
}
