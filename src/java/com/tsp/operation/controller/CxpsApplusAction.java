
/* * CxpsApplusAction.java * Created on 23 de julio de 2009, 15:39 */
package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.threads.*;
import com.tsp.util.Util;
import com.tsp.operation.model.beans.Usuario;
import javax.servlet.http.*;
/** * * @author  Fintra */

public class CxpsApplusAction  extends Action {    
   
    Usuario usuario = null;
    
    public void run() throws ServletException, InformationException {        
        try{
            String next="";
            HttpSession session  = request.getSession();
            usuario              = (Usuario) session.getAttribute("Usuario"); 
            String loginx=usuario.getLogin();
            
            String cxc_app="",cxc_boni="",cxc_pr="",cxc_pf="",cxc_ar="";
                                   
            if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("mostrarCxps")){                
                cxc_app=request.getParameter("cxc_app"); 
                next="/jsp/applus/mostrarCxpsApplus.jsp";
                next=next+"?cxc_app="+cxc_app;     
                model.cxpsApplusService.buscarCxpDeCxcApp(cxc_app);
            }
                                    
            if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("bajarcxps")){                
                cxc_app=request.getParameter("cxc_app"); 
                String[] idordenes=  request.getParameterValues("ckestado");
                
                String abonoPrefactura=request.getParameter("abonoPrefactura"); 
                String descripcionPrefactura=request.getParameter("descripcionPrefactura"); 
                String fecfaccli=  request.getParameter("fecfaccli");
                ArrayList cxps=model.cxpsApplusService.getCxpDeCxcApp();       
                
                Vector bajada=model.cxpsApplusService.bajarCxps(idordenes,cxps,abonoPrefactura,descripcionPrefactura,loginx,fecfaccli);
                model.applusService.ejecutarSQL(bajada);               
                model.cxpsApplusService.buscarCxpDeCxcApp(cxc_app);
                
                next="/jsp/applus/mostrarCxpsApplus.jsp";
                next=next+"?cxc_app="+cxc_app;     
            }
            
            if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("mostrarCxpsBoni")){                
                cxc_boni=request.getParameter("cxc_boni"); 
                next="/jsp/applus/mostrarCxpsBoni.jsp";
                next=next+"?cxc_boni="+cxc_boni;     
                model.cxpsApplusService.buscarCxpDeCxcBoni(cxc_boni);
            }
                        
            if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("bajarcxpsboni")){                
                cxc_boni=request.getParameter("cxc_boni"); 
                String[] idacciones=  request.getParameterValues("ckestado");
                
                String abonoPrefactura=request.getParameter("abonoPrefactura"); 
                String descripcionPrefactura=request.getParameter("descripcionPrefactura"); 
                String fecfaccli=  request.getParameter("fecfaccli");
                
                ArrayList cxps=model.cxpsApplusService.getCxpDeCxcBoni();       
                
                Vector bajada=model.cxpsApplusService.bajarCxpsBoni(idacciones,cxps,abonoPrefactura,descripcionPrefactura,loginx,fecfaccli);
                model.applusService.ejecutarSQL(bajada);               
                
                model.cxpsApplusService.buscarCxpDeCxcBoni(cxc_boni);
                next="/jsp/applus/mostrarCxpsBoni.jsp";
                next=next+"?cxc_boni="+cxc_boni;     
            }
            
            if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("mostrarCxpsPr")){                
                cxc_pr=request.getParameter("cxc_pr"); 
                next="/jsp/applus/mostrarCxpsPr.jsp";
                next=next+"?cxc_pr="+cxc_pr;     
                model.cxpsApplusService.buscarCxpDeCxcPr(cxc_pr);
                //.out.println("cxc_pr"+cxc_pr);
            }
            
            if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("bajarcxpspr")){                
                cxc_pr=request.getParameter("cxc_pr"); 
                String[] idordenes=  request.getParameterValues("ckestado");
                String fecfaccli=  request.getParameter("fecfaccli");
                String abonoPrefactura=request.getParameter("abonoPrefactura"); 
                String descripcionPrefactura=request.getParameter("descripcionPrefactura"); 
                
                ArrayList cxps=model.cxpsApplusService.getCxpDeCxcPr();       
                
                Vector bajada=model.cxpsApplusService.bajarCxpsPr(idordenes,cxps,abonoPrefactura,descripcionPrefactura,loginx,fecfaccli);
                model.applusService.ejecutarSQL(bajada);               
                model.cxpsApplusService.buscarCxpDeCxcPr(cxc_pr);
                
                next="/jsp/applus/mostrarCxpsPr.jsp";
                next=next+"?cxc_pr="+cxc_pr;     
            }
            
            if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("mostrarCxpsPf")){                
                cxc_pf=request.getParameter("cxc_pf"); 
                next="/jsp/applus/mostrarCxpsPf.jsp";
                next=next+"?cxc_pf="+cxc_pf;     
                model.cxpsApplusService.buscarCxpDeCxcPf(cxc_pf);
            }
            if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("bajarcxpspf")){                
                cxc_pf=request.getParameter("cxc_pf"); 
                String[] idacciones=  request.getParameterValues("ckestado");
                
                String abonoPrefactura=request.getParameter("abonoPrefactura"); 
                String descripcionPrefactura=request.getParameter("descripcionPrefactura"); 
                String fecfaccli=  request.getParameter("fecfaccli");
                
                ArrayList cxps=model.cxpsApplusService.getCxpDeCxcPf();       
                
                Vector bajada=model.cxpsApplusService.bajarCxpsPf(idacciones,cxps,abonoPrefactura,descripcionPrefactura,loginx,fecfaccli);
                model.applusService.ejecutarSQL(bajada);               
                
                model.cxpsApplusService.buscarCxpDeCxcPf(cxc_pf);
                next="/jsp/applus/mostrarCxpsPf.jsp";
                next=next+"?cxc_pf="+cxc_pf;     
            }
            
            if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("mostrarCxpsAr")){                
                cxc_ar=request.getParameter("cxc_ar"); 
                next="/jsp/applus/mostrarCxpsAr.jsp";
                next=next+"?cxc_ar="+cxc_ar;     
                model.cxpsApplusService.buscarCxpDeCxcAr(cxc_ar);
            }
            if (request.getParameter("opcion")!=null && request.getParameter("opcion").equals("bajarcxpsar")){                
                cxc_ar=request.getParameter("cxc_ar"); 
                String[] idacciones=  request.getParameterValues("ckestado");
                
                String abonoPrefactura=request.getParameter("abonoPrefactura"); 
                String descripcionPrefactura=request.getParameter("descripcionPrefactura"); 
                String fecfaccli=  request.getParameter("fecfaccli");
                
                ArrayList cxps=model.cxpsApplusService.getCxpDeCxcAr();       
                
                Vector bajada=model.cxpsApplusService.bajarCxpsAr(idacciones,cxps,abonoPrefactura,descripcionPrefactura,loginx,fecfaccli);
                model.applusService.ejecutarSQL(bajada);               
                
                model.cxpsApplusService.buscarCxpDeCxcAr(cxc_ar);
                next="/jsp/applus/mostrarCxpsAr.jsp";
                next=next+"?cxc_ar="+cxc_ar;     
            }

            
            this.dispatchRequest(next);
        }catch(Exception ee){
            System.out.println("error en cxpsapplusaction:"+ee.toString()+"__"+ee.getMessage());
        }
    }
        
    public CxpsApplusAction() {
        
    }
    
}
