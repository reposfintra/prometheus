/********************************************************************
 *      Nombre Clase.................   FacturaProveedoresnitRAction.java
 *      Descripci�n..................   Action que se encarga de generar un vector con los resgitros de proveedores por numero de nit
 *      Autor........................   David Lamadrid
 *      Fecha........................   29.01.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.services.*;
import org.apache.log4j.Logger;
import com.tsp.util.Util;
/**
 *
 * @author  dlamadrid
 */
public class FacturaProveedoresnitRAction extends Action{
    
    /** Creates a new instance of FacturaProveedoresnitRAction */
    public FacturaProveedoresnitRAction () {
    }
    
     public void run () throws ServletException, InformationException
        {
                try
                {
                        String proveedor =""+ request.getParameter ("proveedor");
                        model.proveedorService.obtenerProveedoresPorNit (proveedor);
                        String next = "/jsp/cxpagar/facturasxpagar/buscarnit2.jsp?accion=1";
                        //System.out.println("next en Lista de Proveedores"+next);
                        this.dispatchRequest (next);
                        
                }
                catch(Exception e)
                {
                        throw new ServletException ("Accion:"+ e.getMessage ());
                }
        }
    
}
