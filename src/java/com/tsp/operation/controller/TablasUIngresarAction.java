/******************************************************************
* Nombre ......................TablasUBTablasAction.java
* Descripci�n..................Clase Action para ingresar al programa de Asignacion de tablas a Uusarios
* Autor........................David lamadrid
* Fecha........................21/12/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/


package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  dlamadrid
 */
public class TablasUIngresarAction extends Action{
    
    /** Creates a new instance of TablasUIngresarAction */
    public TablasUIngresarAction () {
    }
    
     public void run () throws ServletException, InformationException
    {
          String next="";
        try {
            model.tablasUsuarioService.listarTodo (); 
            next="/jsp/general/consultas/tablas.jsp";
        }
        catch (Exception e) {
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
