/*
 * IngresoMiscelaneoDetalleRegistrarAction.java
 *
 * Created on 26 de julio de 2006, 10:42 AM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import com.tsp.finanzas.contab.model.*;

import com.tsp.pdf.*;

public class IngresoMiscelaneoDetalleRegistrarAction extends Action {
    
    /** Creates a new instance of IngresoMiscelaneoDetalleRegistrarAction */
    public IngresoMiscelaneoDetalleRegistrarAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String next="/jsp/cxcobrar/ItemsIngreso/ItemsIngresoMiscelaneo.jsp";
        String auxiliar = "";
        String mon_local = (String) session.getAttribute("Moneda");
        String fecha_actual = Util.getFechaActual_String(4);
        String mon_ingreso = request.getParameter("mon_ingreso");
        String num_ingreso = request.getParameter("numingreso");
        int canItems = Integer.parseInt(request.getParameter("items"));
        double valor_tasa = 0;
        int item = 1;
        Vector vecitems = new Vector();
        ////System.out.println("Cantidad de items "+request.getParameter("items"));
        
        String opcion = ( request.getParameter("opcion") != null )?request.getParameter("opcion"):"";
        try{
            
            if( opcion.equals("") ){
                
                Tasa t = model.tasaService.buscarValorTasa(mon_local, mon_ingreso, mon_local, fecha_actual );
                if(t!=null){
                    valor_tasa = t.getValor_tasa();
                    vecitems = new Vector();
                    for(int i=1; i<= canItems; i++ ){
                        if ( request.getParameter("valor"+i) != null ){
                            Ingreso_detalle ingdetalle = new Ingreso_detalle();
                            ingdetalle.setDistrito(usuario.getDstrct());
                            ingdetalle.setNumero_ingreso(num_ingreso);
                            ingdetalle.setItem(item);
                            ingdetalle.setTipo_documento(request.getParameter("tipodoc"));
                            ingdetalle.setCuenta(request.getParameter( "cuenta"+i ).toUpperCase() );
                            auxiliar = ( request.getParameter( "auxiliar"+i ).equals("") && request.getParameter( "tipo"+i ).equals("") )?"" :request.getParameter( "tipo"+i )+"-"+request.getParameter( "auxiliar"+i ) ;
                            ingdetalle.setAuxiliar( auxiliar );
                            ingdetalle.setDescripcion(request.getParameter( "c_descripcion"+i ) );
                            ingdetalle.setTipo_doc( (request.getParameter( "doc"+i ).equals("") )?"":request.getParameter( "tipodoc"+i ) );
                            ingdetalle.setDocumento( request.getParameter( "doc"+i ).toUpperCase() );
                            ingdetalle.setValor_ingreso_me( convertirdouble(request.getParameter("valor"+i) ) );
                            ingdetalle.setValor_ingreso( convertirdouble( request.getParameter("valor"+i)  ) * valor_tasa );
                            ingdetalle.setCodigo_retefuente( "" );
                            ingdetalle.setValor_retefuente_me( 0.0 );
                            ingdetalle.setValor_retefuente( 0.0 );
                            ingdetalle.setCodigo_reteica( "" );
                            ingdetalle.setValor_reteica_me( 0.0 );
                            ingdetalle.setValor_reteica( 0.0 );
                            ingdetalle.setCreation_user(usuario.getLogin());
                            ingdetalle.setBase(usuario.getBase());
                            vecitems.add(ingdetalle);
                            item++;
                        }
                    }
                    model.ingreso_detalleService.insertarIngresoMiscelaneoDetalle(vecitems);
                    session.setAttribute("fin","ok");
                    model.ingreso_detalleService.BorrarArchivo("Ing_miscelaneo"+num_ingreso+".txt", usuario.getLogin());
                    //busco si tiene items
                    model.ingreso_detalleService.itemsMiscelaneo(num_ingreso, request.getParameter("tipodoc"), usuario.getDstrct());
                    Vector vecitem = model.ingreso_detalleService.getItemsMiscelaneo();
                    if(vecitem != null){
                        com.tsp.finanzas.contab.model.Model modelcontab = (com.tsp.finanzas.contab.model.Model) session.getAttribute("modelcontab");
                        for(int i = 0; i < vecitem.size(); i++){
                            Ingreso_detalle ingdetalle = (Ingreso_detalle) vecitem.get(i);
                            if(modelcontab.planDeCuentasService.existCuenta(ingdetalle.getDistrito(),ingdetalle.getCuenta())){
                                modelcontab.subledgerService.busquedaCuentasTipoSubledger(ingdetalle.getDistrito(),ingdetalle.getCuenta());
                                ingdetalle.setTipos(modelcontab.subledgerService.getCuentastsubledger());
                            }else{
                                ingdetalle.setTipos(null);
                            }
                        }
                    }
                    next+="?mensaje=Se agregarón items satisfactoriamente&cerrar=ok";
                }else{
                    next+="?mensaje=No existe tasa para la conversión";
                }
                
            }
           if( opcion.equals("imp_all") ){
                
                IngresoMPDF remision = new IngresoMPDF();
                
                remision.RemisionPlantilla();
                
                remision.crearCabecera();
                
                String tipo     = ( request.getParameter("tipodoc") != null )?request.getParameter("tipodoc"):"";
                
                String numing   = ( request.getParameter("numingreso") != null )?request.getParameter("numingreso"):"";
                
                remision.crearRemision( model, usuario.getDstrct() , tipo , numing , "ALL", usuario.getLogin() );
                
                remision.generarPDF();
                
                next = "/pdf/IngresoMPDF.pdf";
                
            }
            
            this.dispatchRequest(next);
            
        }catch(Exception e){
            e.printStackTrace();
        }
        
        
        
    }
    
    public double convertirdouble(String valor){
        String temp = (valor.equals(""))? "0": valor.replace(",","");
        return Double.parseDouble( temp );
    }
}
