/*******************************************************************
 * Nombre clase: Flota_directaInsertAction.java
 * Descripción: Accion para ingresar una flota directa a la bd.
 * Autor: Ing. Jose de la rosa
 * Fecha: 2 de diciembre de 2005, 02:49 PM
 * Versión: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class Flota_directaInsertAction extends Action{
    
    /** Creates a new instance of Flota_directaInsertAction */
    public Flota_directaInsertAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next = "/jsp/masivo/flota_directa/flota_directaInsertar.jsp";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");   
        String dstrct = (String) session.getAttribute("Distrito");
        String nit = request.getParameter("propietario");
        String placa = request.getParameter("c_placa");
        int sw=0;
        try{
            Flota_directa flota = new Flota_directa();
            flota.setNit (nit);
            flota.setPlaca (placa);
            flota.setUsuario_modificacion(usuario.getLogin().toUpperCase());
            flota.setUsuario_creacion(usuario.getLogin().toUpperCase());
            flota.setDistrito (dstrct);
            flota.setBase(usuario.getBase().toUpperCase());
            try{
                if(model.identidadService.existeIdentidad(nit)){
                    if(model.placaService.existePlaca (placa)){
                        model.flota_directaService.insertFlota_directa(flota);
                        request.setAttribute("mensaje","La información ha sido ingresada exitosamente!");
                    }
                    else
                        request.setAttribute("mensaje","Error la placa no existe!");
                }
                else
                    request.setAttribute("mensaje","Error el nit no existe!");
            }catch(SQLException e){
                sw=1;
            }
            if(sw==1){
                if(!model.flota_directaService.existFlota_directa(nit, placa)){
                    if(model.identidadService.existeIdentidad(nit)){
                        if(model.placaService.existePlaca (placa)){
                            model.flota_directaService.updateFlota_directa (nit, placa, usuario.getLogin().toUpperCase(), nit, placa);
                            request.setAttribute("mensaje","La información ha sido ingresada exitosamente!");
                        }
                        else
                            request.setAttribute("mensaje","Error la placa no existe!");
                    }
                    else
                        request.setAttribute("mensaje","Error el nit no existe!");
                }
                else{
                    request.setAttribute("mensaje","Error la información ya existe!");
                }
            }
        }catch(SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);                
    }
    
}
