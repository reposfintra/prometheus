/*
 * BuscarZonaAction.java
 *
 * Created on 13 de junio de 2005, 10:00 AM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  Henry
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

public class BuscarZonaAction extends Action {
    
    /** Creates a new instance of BuscarZonaAction */
    public BuscarZonaAction() {
    }
    
    public void run() throws ServletException {
        String next=request.getParameter("carpeta")+"/"+request.getParameter("pagina");        
        String codigo = request.getParameter("codigo");
        String mensaje = request.getParameter("mensaje");
        try{                       
            model.zonaService.buscarZona(codigo); 
            next+="?mensaje="+mensaje;
            //System.out.println("zona codigo: "+codigo);
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }         
         // Redireccionar a la p�gina indicada.
        //next = Util.LLamarVentana(next, "Detalle De Zonas");
        this.dispatchRequest(next);

    }
    
}
