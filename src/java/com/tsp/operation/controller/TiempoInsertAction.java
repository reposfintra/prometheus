/*
 * TiempoInsertAction.java
 *
 * Created on 26 de enero de 2005, 04:32 PM
 */

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  KREALES
 */
public class TiempoInsertAction extends Action{
    
    /** Creates a new instance of TiempoInsertAction */
    public TiempoInsertAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String next                 =   "/diferenciatiempos/tiempoInsert.jsp";
        String dstrct               =   request.getParameter("dstrc").toUpperCase();
        String stdjob               =   request.getParameter("sj").toUpperCase();
        String cf                   =   request.getParameter("cf").toUpperCase();
        String fec1                 =   request.getParameter("fec1").toUpperCase();
        String fec2                 =   request.getParameter("fec2").toUpperCase();
        String sec                  =   request.getParameter("sec").toUpperCase();
        String reporte              =   request.getParameter("reporte").toUpperCase();
        HttpSession session         =   request.getSession();
        Usuario usuario             =   (Usuario) session.getAttribute("Usuario");
        String user_update          =   usuario.getLogin();
        
        try{
            request.setAttribute("error2","");
            request.setAttribute("error","");
            
            model.tiempoService.buscar(dstrct, stdjob, cf, fec1,fec2);
            if(model.tiempoService.getTiempo()!=null){
                ////System.out.println("El tiempo ya existe!");
                request.setAttribute("error","#cc0000");
                next="/diferenciatiempos/tiempoInsertError.jsp";
            }
            
            else{
                
                Tiempo tiempo = new Tiempo();
                tiempo.setcf_code(cf);
                tiempo.setcreation_user(user_update);
                tiempo.setdstrct(dstrct);
                tiempo.setreporte(reporte);
                tiempo.setsecuence(sec);
                tiempo.setsj(stdjob);
                tiempo.settime_code_1(fec1);
                tiempo.settime_code_2(fec2);
                
                
                if(model.tiempoService.estaStandard(stdjob,cf)){
                    if(model.tiempoService.estaFechas(stdjob,fec1)&&model.tiempoService.estaFechas(stdjob,fec2)){
                        model.tiempoService.setTiempo(tiempo);
                        model.tiempoService.insert();
                    }
                    else{
                        request.setAttribute("error2","#cc0000");
                        next="/diferenciatiempos/tiempoInsertError.jsp";
                    }
                }
                else{
                    ////System.out.println("El standard no esta en stdjobcosto!");
                    request.setAttribute("error","#cc0000");
                    next="/diferenciatiempos/tiempoInsertError.jsp";
                }
                
            }
            
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
