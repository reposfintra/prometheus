/********************************************************************
 *      Nombre Clase.................   FacturaLArchivoAction.JAVA
 *      Descripci�n..................   Action que se encarga de inicializar el programa de documentos por pagar
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.util.Util;
/**
 *
 * @author  dlamadrid
 */
public class FacturaLArchivoAction extends Action{
    
    /** Creates a new instance of FacturaLArchivoAction */
    public FacturaLArchivoAction() {
    }
    
    
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            //ivan 21 julio 2006
            com.tsp.finanzas.contab.model.Model modelcontab = (com.tsp.finanzas.contab.model.Model) session.getAttribute("modelcontab");
            //////////////////////
            String age = (usuario.getId_agencia().equals("OP")?"":usuario.getId_agencia());
            model.servicioBanco.loadBancos(age, usuario.getCia());
            
            String Modificar = (request.getParameter("Modificar")!=null)?request.getParameter("Modificar"):"";
            model.cxpDocService.obtenerAgencias();
            model.cxpDocService.BuscarHC();
            
            String userlogin=""+usuario.getLogin();
            String next = "";
            String agencia_usuario = "";
            CXP_Doc factura;
            Vector vItems;
            String maxfila = "1";
            //System.out.println("MODIFICARRRRR ---->"+Modificar);
            model.agenciaService.loadAgencias();
            model.tblgensvc.buscarAutXCP(); 
            
            try{
                factura = model.cxpDocService.leerArchivo("factura"+userlogin+".txt", userlogin);
                model.cxpDocService.setFactura(factura);
                maxfila = (factura.getMaxfila()!=null)?factura.getMaxfila():"1";
                agencia_usuario = factura.getAgencia();
                session.setAttribute("id_agencia", agencia_usuario );
                //System.out.println("MAXFILA ACTION -> "+maxfila);
                vItems = model.cxpDocService.leerArchivoItems("factura"+userlogin+".txt", userlogin);
                // Modificacion 21 julio 2006
                for(int i=0; i< vItems.size();i++){
                    CXPItemDoc item = (CXPItemDoc)vItems.get(i);
                    if(item != null){
                        
                        LinkedList tbltipo = null;
                        if(modelcontab.planDeCuentasService.existCuenta(usuario.getDstrct(),item.getCodigo_cuenta())){
                            modelcontab.subledgerService.busquedaCuentasTipoSubledger(usuario.getDstrct(),item.getCodigo_cuenta());
                            tbltipo = modelcontab.subledgerService.getCuentastsubledger();
                        }
                        
                        item.setTipo(tbltipo);
                    }
                }
                //////////////////////////////////////////////////////////////////////
                
                
                model.cxpItemDocService.setVecCxpItemsDoc(vItems);
            }catch(Exception e){
                model.cxpDocService.setFactura(null);
                model.cxpItemDocService.setVecCxpItemsDoc(null);
            }
            
            next = "/jsp/cxpagar/facturasxpagar/facturaP.jsp?ag=false&maxfila="+maxfila+"&Modificar="+Modificar;
            
            agencia_usuario = usuario.getId_agencia();
            session.setAttribute("id_agencia",agencia_usuario );
            String agContable ="";
            String unidadC ="";
            

            Vector VecAgencias = model.cxpDocService.getAgencias();
            for(int i=0; i< VecAgencias.size();i++){
		Agencia ag = (Agencia) VecAgencias.get(i);
                if(agencia_usuario.equals(ag.getId_agencia())){
                    agContable =  ag.getAgenciaContable();
                    unidadC    =  ag.getUnidadNegocio();
                }
            }
            
            session.setAttribute("agContable",agContable);
            session.setAttribute("unidadC",unidadC);
            
            
            this.dispatchRequest(next);
            
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new ServletException("Accion:"+ e.getMessage());
        }
    }
}
