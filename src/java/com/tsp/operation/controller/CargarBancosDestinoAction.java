/******************************************************************************
 *      Nombre Clase.................   ReporteFacturasProveedorAction.java
 *      Descripci�n..................   Anula un registro en la tabla tblapl
 *      Autor........................   Ing. fily steven fernandez
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *****************************************************************************/

package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import com.tsp.operation.model.threads.*;

import org.apache.log4j.*;
/**
 *
 * @author  Administrador
 */
public class CargarBancosDestinoAction extends Action {
    
    Logger logger = Logger.getLogger(this.getClass());
    
    /** Creates a new instance of ReporteEgresoAction */
    public CargarBancosDestinoAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        try{
           
            
            HttpSession session = request.getSession();
            Usuario user    = (Usuario) session.getAttribute("Usuario");
            String agencia  = (user.getId_agencia().equals("OP")?"%":user.getId_agencia());
            String bancoOri = Util.coalesce( request.getParameter("bancoOri"), "");
            String bancoDes = Util.coalesce( request.getParameter("bancoDes"), "");
            String tipo     = Util.coalesce( request.getParameter("tipo")    , "");
            String opc     = Util.coalesce( request.getParameter("opc")    , "");
            String tip     = Util.coalesce( request.getParameter("tip")    , "");
            String valor     = Util.coalesce( request.getParameter("valor")    , "");
            if (tipo.equals("ORI")){
                model.servicioBanco.loadSucursalesAgencia(agencia, bancoOri, user.getDstrct());
            }
            else if (tipo.equals("DES")){
                model.servicioBanco.loadSucursalesAgenciaDes(agencia, bancoDes, user.getDstrct());
            }
            
            String next = "";
            if(opc.equals("mod")){
                next = "/jsp/finanzas/contab/comprobante/traslados/traslado_modificar.jsp";
            }
            if(opc.equals("ing")){
                next = "/jsp/finanzas/contab/comprobante/traslados/traslado_adicionar.jsp";
                
            }
            if(opc.equals("PROPIETARIO")){
                String bancoPro     = Util.coalesce( request.getParameter("Banco")    , "");
                model.servicioBanco.loadSucursalesAgencia(agencia, bancoPro, user.getDstrct());
                next = "/jsp/sot/body/propietario/Ingresar_Propietario.jsp";
                if(tip.equals("2")){
                  next = "/jsp/sot/body/propietario/Modificar_B_Propietario.jsp";  
                }
                
            }
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en CargarBancosDestinoAction .....\n"+e.getMessage());
        }
    }
    
}
