/******************************************************************
* Nombre ......................TablasUSalirAction.java
* Descripci�n..................Clase Action para salir del programa de asignacion de tablas de usuarios
* Autor........................David lamadrid
* Fecha........................21/12/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  dlamadrid
 */
public class TablasUSalirAction extends Action
{
    
    /** Creates a new instance of TablasUSalirAction */
    public TablasUSalirAction ()
    {
    }
    
     public void run() throws ServletException, InformationException {
        
        String next="";
        try {
            model.tablasUsuarioService.listarTodo (); 
            next="/jsp/general/consultas/tablas.jsp?reload=1";
        }
        catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
