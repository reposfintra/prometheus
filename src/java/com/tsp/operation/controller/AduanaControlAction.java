/***********************************************************************************
 *      Nombre Clase.................   AduanaControlAction.java
 *      Descripci�n..................   Busca un vector con las remesas necesarias
 *                                      para el control de aduanas
 *      Autor........................   Karen Reales
 *      Fecha........................   04.04.2006
 *      Modificado por...............   LREALES
 *      Fecha Modificacion...........   22.01.2007
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 ***********************************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;

public class AduanaControlAction extends Action {
    //2009-09-02
    /** Creates a new instance of AduanaControlAction */
    public AduanaControlAction() { }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        
        //Pr�xima vista
        String next  = "/jsp/aduanas/ReporteAduana.jsp";
        
        //Usuario en sesi�n
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        String opcion = request.getParameter("opcion") != null ? request.getParameter("opcion") : "";
        try{
            
            
            if ( opcion.equals("") ) {
                
                if( request.getParameter("cargarPais") != null ) {
                    
                    model.ciudadService.listarFronteras( request.getParameter("pais") );
                    
                } else {
                    
                    String cliente = request.getParameter("cliente") != null ? request.getParameter("cliente") : "";
                    String estado_tra = request.getParameter("tiporep") != null ? request.getParameter("tiporep") : "";
                    String faccia = request.getParameter("faccial") != null ? request.getParameter("faccial") : "";
                    String tipo = request.getParameter("tipo") != null ? request.getParameter("tipo") : "";
                    String frontera  =request.getParameter("frontera") != null ? request.getParameter("frontera") : "";
                    
                    //model.remesaService.bucarRemesasAduana( frontera, cliente, estado_tra, faccia, tipo,usuario.getLogin() );
                    
                }
                
            }
            
            if ( opcion.equals("InfoTrailer") ) {
                
                String factvieja = request.getParameter("factvieja") != null ? request.getParameter("factvieja") : "";
                
                String numpla = request.getParameter("numpla") != null ? request.getParameter("numpla") : "";
                String numrem = request.getParameter("numrem") != null ? request.getParameter("numrem") : "";
                String platlr = request.getParameter("platlr") != null ? request.getParameter("platlr") : "";
                String tipo_reexp = request.getParameter("tipo_reexp") != null ? request.getParameter("tipo_reexp") : "";
                
                model.remesaService.bucarTiposReexpedicion();
                
                next  = "/jsp/aduanas/ModificarTrailer.jsp?trailer=" + platlr + "&numpla=" + numpla + "&numrem=" + numrem + "&tipo_reexp=" + tipo_reexp + "&factvieja=" + factvieja;
                
            }
            
            //ModificarTrailer
            if ( opcion.equals("ModificarTrailer") ) {
                next  = "/jsp/aduanas/ModificarTrailer.jsp";
                Vector consultas= new Vector();
                String factvieja = request.getParameter("factvieja") != null ? request.getParameter("factvieja") : "";
                
                String distrito = usuario.getDstrct().toString();
                String usu = usuario.getLogin().toString();
                String base = usuario.getBase().toString();
                String numpla = request.getParameter("numpla") != null ? request.getParameter("numpla") : "";
                String numrem = request.getParameter("numrem") != null ? request.getParameter("numrem") : "";
                String platlr = request.getParameter("trailer") != null ? request.getParameter("trailer").toUpperCase() : "";
                platlr = platlr.trim().equals("") ? "NA" : platlr;
                String tipo_reexp = request.getParameter("tipo_reexp") != null ? request.getParameter("tipo_reexp") : "";
                
                String placa = request.getParameter("placa") != null ? request.getParameter("placa").toUpperCase() : "";
                String grupo = request.getParameter("grupo") != null ? request.getParameter("grupo").toUpperCase() : "";
                
                boolean sigue=true;
                //SI ESTA VACIO SE CAMBIA.
                if(platlr.equals("NA")){
                    //consultas.add(model.remesaService.guardarPlacaTrailer( platlr, numpla ));
                    //consultas.add(model.placaService.actualizarFitDef(request.getParameter("trailer").toUpperCase(),"FIT"));
                }
                else{
                    
                    
                    if(!model.remesaService.existeTrailer(platlr)){
                        next  = "/jsp/aduanas/ModificarTrailer.jsp?msg=La placa del trailer no existe!";
                        sigue=false;
                    }
                    else{
                        String trailerPlaca="";
                        String recursoPlaca="";
                        boolean tractomula=false;
                        model.placaService.buscaPlaca(placa.toUpperCase());
                        if(model.placaService.getPlaca()!=null){
                            Placa plac = model.placaService.getPlaca();
                            trailerPlaca = plac.getPlaca_trailer()!=null?plac.getPlaca_trailer():"";
                            recursoPlaca = plac.getRecurso()!=null?plac.getRecurso():"";
                            tractomula = plac.isTractomula();
                        }
                        if(tractomula){
                            model.placaService.buscaPlaca(platlr);
                            String recursoTrailer="";
                            if(model.placaService.getPlaca()!=null){
                                Placa vehiculo = model.placaService.getPlaca();
                                recursoTrailer=vehiculo.getRecurso();
                                if(vehiculo.isTraile()){
                                    if(vehiculo.getPropietario().equals("890103161")){
                                        model.tablaGenService.obtenerRegistro("GRUPOCABEZ",recursoPlaca, "");
                                        if(model.tablaGenService.getTblgen()!=null){
                                            model.tablaGenService.buscarDatosTablasGenerales("GRUPOCABEZ",recursoPlaca,recursoTrailer);
                                            if(model.tablaGenService.getVectorTablasGenerales()!=null){
                                                if(model.tablaGenService.getVectorTablasGenerales().size()<=0){
                                                    sigue=false;
                                                    next  = "/jsp/aduanas/ModificarTrailer.jsp?msg=ERROR: El recurso "+recursoPlaca+" no esta relacionado con el recurso del trailer "+recursoTrailer+".!";
                                                }
                                                
                                            }else{
                                                sigue=false;
                                                next  = "/jsp/aduanas/ModificarTrailer.jsp?msg= ERROR:El recurso "+recursoPlaca+" no esta relacionado con el recurso del trailer "+recursoTrailer+".!";
                                            }
                                        }
                                        if(sigue){
                                            sigue=true;
                                            if(!grupo.equals("")){
                                                model.tablaGenService.obtenerRegistro("GRUPOTRAIL",grupo, "");
                                                if(model.tablaGenService.getTblgen()!=null){
                                                    model.tablaGenService.buscarDatosTablasGenerales("GRUPOTRAIL",grupo,platlr);
                                                    if(model.tablaGenService.getVectorTablasGenerales()!=null){
                                                        if(model.tablaGenService.getVectorTablasGenerales().size()<=0){
                                                            sigue=false;
                                                            next  = "/jsp/aduanas/ModificarTrailer.jsp?msg=ERROR:El trailer no pertenece al grupo "+grupo+".!";
                                                        }
                                                        
                                                    }else{
                                                        next  = "/jsp/aduanas/ModificarTrailer.jsp?msg=ERROR:El trailer no pertenece al grupo "+grupo+".!";
                                                        sigue=false;
                                                    }
                                                }
                                            }
                                            if(sigue){
                                                /*if(!model.planillaService.esReexpedicionTrailer(platlr,numrem)){
                                                    if(vehiculo.getFit().equals("FIT") || vehiculo.getFit().equals("MANTENIMIENTO")){
                                                        //SE TIENEN 3 CONDICIONES.
                                                        //1. SI EL TRAILER QUE TRAE POR DEFAULT LA PLACA ES DISTINTO AL TRAILER QUE SE VA A USAR
                                                        if(!trailerPlaca.equals(platlr)){
                                                            //2. SE DEBE BUSCAR LA ULTIMA PLANILLA DEL CABEZOTE, SI ESA PLANILLA ESTA EN INGRESO TRAFICO
                                                            // SE VERIFICA SI LA PLACA DEL TRAILER Y LA PLACA DEL CABEZOTE SON IGUALES A LAS QUE SE VAN A DESPACHAR.
                                                            Planilla planTra = model.rmtService.buscarUltimaPlanilla(placa);
                                                            if(planTra!=null){
                                                                if(!planTra.getPlatlr().equals(platlr)){
                                                                    
                                                                    //3. SE DEBE BUSCAR LA ULTIMA PLANILLA DE ESE TRAILER, SI ESA PLANILLA  ESTA EN INGRESO TRAFICO
                                                                    // NO SE PUEDE DEJAR DESPACHAR.
                                                                    planTra = model.rmtService.buscarUltimaPlanillaTrailer(platlr);
                                                                    if(planTra!=null){
                                                                        sigue=false;
                                                                        next  = "/jsp/aduanas/ModificarTrailer.jsp?msg=ERROR: La placa del trailer esta enganchada.!";
                                                                    }
                                                                }
                                                            }
                                                            
                                                        }
                                                    }
                                                }*/
                                            }
                                        }
                                    }
                                    else{
                                        //SE ACTUALIZA LA PLACA DEL TRAILER CON FIT.
                                        //consultas.add(model.remesaService.guardarPlacaTrailer( platlr, numpla ));
                                        //consultas.add(model.placaService.actualizarFitDef(request.getParameter("trailer").toUpperCase(),"FIT"));
                                    }
                                    
                                    
                                }else{
                                    sigue=false;
                                    next  = "/jsp/aduanas/ModificarTrailer.jsp?msg=La placa seleccionada no es trailer!";
                                }
                            }
                        }
                        else{
                            sigue=false;
                            next  = "/jsp/aduanas/ModificarTrailer.jsp?msg=La placa del cabezote no es tractomula, no se puede engachar un trailer!";
                        }
                        
                        
                        /*
                         
                        if ( datos.size() > 0 ) {
                         
                            for ( int i = 0; i < datos.size(); i++ ) {
                         
                                Aduana a = ( Aduana ) datos.elementAt( i );
                                model.remesaService.guardarPlacaTrailer( platlr, a.getNumpla() );
                         
                            }
                         
                        }*/
                        
                        
                    }
                    
                }
                
                if(sigue){
                    /*consultas.add(model.remesaService.guardarPlacaTrailer( platlr, numpla ));
                    consultas.add(model.placaService.actualizarFitDef(request.getParameter("trailer").toUpperCase(),"FIT"));*/
                    
                    model.remesaService.bucarRemesasParaModificar( factvieja );
                    Vector datosA = model.remesaService.getRemesas();
                    
                    if ( datosA.size() > 0 ) {
                        
                        for ( int i = 0; i < datosA.size(); i++ ) {
                            
                            Aduana a = ( Aduana ) datosA.elementAt( i );
                            if ( model.remesaService.existeTipoReexpedicion( distrito, a.getNumrem() ) ) {
                                model.remesaService.actualizarTipoReexpedicion( tipo_reexp, usu, distrito, a.getNumrem() );
                            } else {
                                model.remesaService.insertarTipoReexpedicion( distrito, a.getNumrem(), tipo_reexp, usu, base );
                            }
                            
                        }
                        
                    }
                    
                    if ( model.remesaService.existeTipoReexpedicion( distrito, numrem ) ) {
                        model.remesaService.actualizarTipoReexpedicion( tipo_reexp, usu, distrito, numrem );
                    } else {
                        model.remesaService.insertarTipoReexpedicion( distrito, numrem, tipo_reexp, usu, base );
                    }
                    
                    next  = "/jsp/aduanas/ModificarTrailer.jsp?msg=Informacion modificada exitosamente!";
                    
                }
                System.out.println("CONSULTAS ....");
                for(int i =0; i<consultas.size(); i++){
                    System.out.println(consultas.elementAt(i));
                }
                model.despachoService.insertar(consultas);
            }
            
            
            //ModificarAgenteExportacion
            if ( opcion.equals("ModificarAgenteExportacion") ) {
                
                String factvieja = request.getParameter("factvieja") != null ? request.getParameter("factvieja") : "";
                
                String distrito = usuario.getDstrct().toString();
                String usu = usuario.getLogin().toString();
                String base = usuario.getBase().toString();
                String numrem = request.getParameter("numrem") != null ? request.getParameter("numrem") : "";
                String ageexp = request.getParameter("ageexp") != null ? request.getParameter("ageexp").toUpperCase() : "";
                String sj = request.getParameter("sj") != null ? request.getParameter("sj").toUpperCase() : "";
                if ( !ageexp.trim().equals("") ) {
                    
//                    model.remesaService.updateAgenteExp(sj,usuario.getCia(),ageexp);
                    next  = "/jsp/aduanas/ModificarAgenteAduaneroExportacion.jsp?msg=Informacion modificada exitosamente!";
                    
                } else {
                    
                    next  = "/jsp/aduanas/ModificarAgenteAduaneroExportacion.jsp?msg=Defina el Agente Aduanero de Exportacion para poder continuar..";
                    
                }
                
            }
            
            //ModificarAgenteImportacion
            if ( opcion.equals("ModificarAgenteImportacion") ) {
                
                String factvieja = request.getParameter("factvieja") != null ? request.getParameter("factvieja") : "";
                
                String distrito = usuario.getDstrct().toString();
                String usu = usuario.getLogin().toString();
                String base = usuario.getBase().toString();
                String numrem = request.getParameter("numrem") != null ? request.getParameter("numrem") : "";
                String ageimp = request.getParameter("ageimp") != null ? request.getParameter("ageimp").toUpperCase() : "";
                String sj = request.getParameter("sj") != null ? request.getParameter("sj").toUpperCase() : "";
                if ( !ageimp.trim().equals("") ) {
                    
                    
//                    model.remesaService.updateAgenteImp(sj,usuario.getCia(),ageimp);
                    next  = "/jsp/aduanas/ModificarAgenteAduaneroImportacion.jsp?msg=Informacion modificada exitosamente!";
                    
                } else {
                    
                    next  = "/jsp/aduanas/ModificarAgenteAduaneroImportacion.jsp?msg=Defina el Agente Aduanero de Importacion para poder continuar..";
                    
                }
                
            }
            
            //ModificarAlmacenadora
            if ( opcion.equals("ModificarAlmacenadora") ) {
                
                String factvieja = request.getParameter("factvieja") != null ? request.getParameter("factvieja") : "";
                
                String distrito = usuario.getDstrct().toString();
                String usu = usuario.getLogin().toString();
                String base = usuario.getBase().toString();
                String numrem = request.getParameter("numrem") != null ? request.getParameter("numrem") : "";
                String almacenadora = request.getParameter("almacenadora") != null ? request.getParameter("almacenadora").toUpperCase() : "";
                
                if ( !almacenadora.trim().equals("") ) {
                    
                    model.remesaService.bucarRemesasParaModificar( factvieja );
                    Vector datos = model.remesaService.getRemesas();
                    
                    if ( datos.size() > 0 ) {
                        
                        for ( int i = 0; i < datos.size(); i++ ) {
                            
                            Aduana a = ( Aduana ) datos.elementAt( i );
                            if ( model.remesaService.existeAlmacenadora( distrito, a.getNumrem() ) ) {
                                model.remesaService.actualizarAlmacenadora( almacenadora, usu, distrito, a.getNumrem() );
                            } else {
                                model.remesaService.insertarAlmacenadora( distrito, a.getNumrem(), almacenadora, usu, base );
                            }
                            
                        }
                        
                    }
                    
                    if ( model.remesaService.existeAlmacenadora( distrito, numrem ) ) {
                        model.remesaService.actualizarAlmacenadora( almacenadora, usu, distrito, numrem );
                    } else {
                        model.remesaService.insertarAlmacenadora( distrito, numrem, almacenadora, usu, base );
                    }
                    
                    next  = "/jsp/aduanas/ModificarAlmacenadora.jsp?msg=Informacion modificada exitosamente!";
                    
                } else {
                    
                    next  = "/jsp/aduanas/ModificarAlmacenadora.jsp?msg=Defina la Almacenadora para poder continuar..";
                    
                }
                
            }
            
            if ( opcion.equals("InfoDestinatarios") ) {
                
                String factvieja = request.getParameter("factvieja") != null ? request.getParameter("factvieja") : "";
                
                String codcli = request.getParameter("codcli") != null ? request.getParameter("codcli") : "";
                String numrem = request.getParameter("numrem") != null ? request.getParameter("numrem") : "";
                
                model.remesaService.bucarDestinatariosDeUnCliente( codcli );
                Vector dest_clie = model.remesaService.getVector();
                
                if ( dest_clie.size() <= 0 ) {
                    
                    next  = "/jsp/aduanas/ModificarDestinatario.jsp?msg=No existen destinatarios para el cliente de esa remesa!!";
                    
                } else {
                    
                    next  = "/jsp/aduanas/ModificarDestinatario.jsp?numrem=" + numrem + "&factvieja=" + factvieja;
                    
                }
                
                model.remesaService.bucarDestinatariosDeUnaRemesa( numrem );
                //Vector dest_reme = model.remesaService.getOtro_vector();
            }
            
            //ModificarDestinatarios
            if ( opcion.equals("ModificarDestinatarios") ) {
                
                String factvieja = request.getParameter("factvieja") != null ? request.getParameter("factvieja") : "";
                
                String distrito = usuario.getDstrct().toString();
                String usu = usuario.getLogin().toString();
                String base = usuario.getBase().toString();
                String numrem = request.getParameter("numrem") != null ? request.getParameter("numrem") : "";
                //capturar los destinatarios en el vector.
                String lista[] = request.getParameterValues("destinatario");
                
                if ( lista != null ) {
                    
                    Vector info = model.remesaService.getVector();
                    
                    model.remesaService.eliminarDestinatariosDeUnaRemesa( numrem );
                    
                    for ( int k = 0; k < lista.length; k++ ) {
                        
                        int posicion = Integer.parseInt( lista[k] );
                        
                        Aduana ad = ( Aduana ) info.get( posicion );
                        
                        model.remesaService.insertarNuevosDestinatarios( distrito, ad.getCodigo(), numrem, usu, base );
                        
                    }
                    
                    model.remesaService.bucarRemesasParaModificar( factvieja );
                    Vector datos = model.remesaService.getRemesas();
                    
                    if ( datos.size() > 0 ) {
                        
                        for ( int i = 0; i < datos.size(); i++ ) {
                            
                            Aduana a = ( Aduana ) datos.elementAt( i );
                            Vector info2 = model.remesaService.getVector();
                            
                            model.remesaService.eliminarDestinatariosDeUnaRemesa( a.getNumrem() );
                            
                            for ( int j = 0; j < lista.length; j++ ) {
                                
                                int posicion = Integer.parseInt( lista[j] );
                                
                                Aduana ad = ( Aduana ) info2.get( posicion );
                                
                                model.remesaService.insertarNuevosDestinatarios( distrito, ad.getCodigo(), a.getNumrem(), usu, base );
                                
                            }
                            
                        }
                        
                    }
                    
                    next  = "/jsp/aduanas/ModificarDestinatario.jsp?msg=Informacion modificada exitosamente!";
                    
                } else {
                    
                    next  = "/jsp/aduanas/ModificarDestinatario.jsp?msg=Seleccione por lo menos 1 destinatario!!";
                    
                }
                
            }
            
            //ModificarFacturaComercial
            if ( opcion.equals("ModificarFacturaComercial") ) {
                
                String distrito = usuario.getDstrct().toString();
                String usu = usuario.getLogin().toString();
                String numrem = request.getParameter("numrem") != null ? request.getParameter("numrem") : "";
                String coddest = request.getParameter("coddest") != null ? request.getParameter("coddest") : "";
                String factvieja = request.getParameter("factvieja") != null ? request.getParameter("factvieja") : "";
                String factcial = request.getParameter("factcial") != null ? request.getParameter("factcial").toUpperCase() : "";
                
                if ( !factcial.trim().equals("") ) {
                    
                    if ( model.remesaService.existeFacturaComercial( distrito, numrem, factvieja ) ) {
                        
                        model.remesaService.actualizarFacturaComercial( factcial, usu, distrito, numrem, factvieja );
                        
                        model.remesaService.bucarRemesasParaModificar( factvieja );
                        Vector datos = model.remesaService.getRemesas();
                        
                        if ( datos.size() > 0 ) {
                            
                            for ( int i = 0; i < datos.size(); i++ ) {
                                
                                Aduana a = ( Aduana ) datos.elementAt( i );
                                model.remesaService.actualizarFacturaComercial( factcial, usu, distrito, a.getNumrem(), factvieja );
                                
                            }
                            
                        }
                        
                    } else {
                        
                        model.remesaService.insertarFacturaComercial( distrito, numrem, factcial, coddest, usu );
                        
                    }
                    
                    next  = "/jsp/aduanas/ModificarFacturaComercial.jsp?msg=Informacion modificada exitosamente!";
                    
                } else {
                    
                    next  = "/jsp/aduanas/ModificarFacturaComercial.jsp?msg=Defina la Factura Comercial para poder continuar..";
                    
                }
                
            }
            if ( opcion.equals("borraViaje") ) {
                model.tblgensvc.buscarLista("JUSTADUANA", "ADUANA");
                next  = "/jsp/aduanas/ConfirmacionAnulacion.jsp";
                
                
            }
            if ( opcion.equals("ConfirmborraViaje") ) {
                Vector consulta =new Vector();
                Auditoria a = new Auditoria();
//                a.setDistrito(usuario.getDstrct());
                a.setTabla("Remesa");
                a.setLlave(request.getParameter("numrem"));
                a.setCampo("Aduana");
                a.setCont_original("S");
                a.setCont_nuevo("N");
                a.setFormato("varchar(1)");
   //             a.setUsusario(usuario.getLogin());
                a.setBase(usuario.getBase());
//                a.setCampoLlave(model.auditoriaService.nombreLlaves("Remesa"));
    //            a.setJustificacion(request.getParameter("justificacion").replaceAll("/",""));
     //           a.setObservacion(request.getParameter("observacion"));
      //          model.auditoriaService.setAuditoria(a);
//                consulta.add(model.auditoriaService.insert());
                consulta.add(model.rmtService.actualizacionAduana(request.getParameter("numrem")));
                model.despachoService.insertar(consulta);
                next  = "/jsp/aduanas/ConfirmacionAnulacion.jsp?mensajeConfirm=Remesa "+request.getParameter("numrem")+" excluida de control de aduanas con exito!.";
            }
            if ( opcion.equals("CargarAgenteImportacion") ) {
                model.tablaGenService.buscarRegistros("AGADUANA");
                //model.tablaGenService
                next = "/jsp/aduanas/ModificarAgenteAduaneroImportacion.jsp";
            }
            if ( opcion.equals("CargarAgenteExportacion") ) {
                model.tablaGenService.buscarRegistros("AGADUANA");
                
                //model.tablaGenService
                next = "/jsp/aduanas/ModificarAgenteAduaneroExportacion.jsp";
            }
            if ( opcion.equals("ModificarPlacaProy") ) {
                
                String distrito = usuario.getDstrct().toString();
                String usu = usuario.getLogin().toString();
                String base = usuario.getBase().toString();
                String numrem = request.getParameter("numrem") != null ? request.getParameter("numrem") : "";
                String placa = request.getParameter("placaproy") != null ? request.getParameter("placaproy").toUpperCase() : "";
                model.placaService.buscarPlaca(placa);
                if(model.placaService.getPlaca()!=null){
                    next  = "/jsp/aduanas/ModificarPlacaProy.jsp?msg=Informacion modificada exitosamente!";
//                    if ( model.remesaService.existePlacaProy( distrito,numrem) ) {
                        //model.remesaService.actualizarPlacaProy( placa, usu, distrito, numrem );
               //     } else {
//                        model.remesaService.insertarPlacaProy(distrito, numrem, placa,usu,base);
                 //   }
                }
                else{
                    next  = "/jsp/aduanas/ModificarPlacaProy.jsp?msg=La placa no existe!";
                }
                
            }
        } catch ( Exception e ) {
            
            e.printStackTrace();
            
        }
        
       // this.dispatchRequest( next );
        
    }
    
}
