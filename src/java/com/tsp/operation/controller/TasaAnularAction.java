/*
 * TasaAnularAction.java
 *
 * Created on 4 de julio de 2005, 12:49 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  DIOGENES
 */
public class TasaAnularAction extends Action{
    
    /** Creates a new instance of TasaAnularAction */
    public TasaAnularAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/jsp/trafico/mensaje/MsgAnulado.jsp";
        String moneda1 = (request.getParameter("c_combom1").toUpperCase());
        String moneda2 = (request.getParameter("c_combom2").toUpperCase());
        String fec = (request.getParameter("c_fecha").toUpperCase());
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        try{                        
            Tasa tasa = new Tasa();
            tasa.setMoneda1(moneda1);
            tasa.setMoneda2(moneda2);
            tasa.setFecha(fec );
            tasa.setUser_update(usuario.getLogin());
            tasa.setCia(usuario.getCia());    
            tasa.setBase(usuario.getBase());
            model.tasaService.anularTasa(tasa);         
            
        }
        catch (SQLException e){
               throw new ServletException(e.getMessage());
        }
         
         // Redireccionar a la p�gina indicada.
       this.dispatchRequest(next);
    }
    
}
