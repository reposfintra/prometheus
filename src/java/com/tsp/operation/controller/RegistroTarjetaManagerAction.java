/***************************************************************************
 * Nombre clase : ............... RegistroTarjetaManagerAction.java        *
 * Descripcion :................. Clase que maneja los eventos             *
 *                                relacionados con el programa de          *
 *                                RegistroTarjeta                          *
 * Autor :....................... Ing. Juan Manuel Escandon Perez          *
 * Fecha :........................ 6 de diciembre de 2005, 08:56 AM        *
 * Version :...................... 1.0                                     *
 * Copyright :.................... Fintravalores S.A.                 *
 ***************************************************************************/
package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
/**
 *
 * @author  JuanM
 */
public class RegistroTarjetaManagerAction extends Action{
    
    
    public RegistroTarjetaManagerAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        try{
            
            /*
             *Declaracion de variables
             */
            String Opcion                 = (request.getParameter("Opcion")!=null)?request.getParameter("Opcion"):"";
            String num_tarjeta            = (request.getParameter("numtarjeta")!=null)?request.getParameter("numtarjeta"):"";
            String placa                  = (request.getParameter("placa")!=null)?request.getParameter("placa").toUpperCase():"";
            String cedula                 = (request.getParameter("cedula")!=null)?request.getParameter("cedula"):"";
            String fechacreacion          = (request.getParameter("fechacreacion")!=null)?request.getParameter("fechacreacion"):"";
            
            String nnum_tarjeta            = (request.getParameter("nnumtarjeta")!=null)?request.getParameter("nnumtarjeta"):"";
            String nplaca                  = (request.getParameter("nplaca")!=null)?request.getParameter("nplaca").toUpperCase():"";
            String ncedula                 = (request.getParameter("ncedula")!=null)?request.getParameter("ncedula"):"";
            String nfechacreacion          = (request.getParameter("nfechacreacion")!=null)?request.getParameter("nfechacreacion"):"";
            
            
            HttpSession Session           = request.getSession();
            Usuario user                  = new Usuario();
            user                          = (Usuario)Session.getAttribute("Usuario");
            String usuario                = user.getLogin();
            String dstrct                 = user.getDstrct();
            String base                   = user.getBase();
            
            String []LOV                  = request.getParameterValues("LOV");
            
            String fecha_creacion         = com.tsp.util.Util.getFechaActual_String(6);
            String next = "";
            String Mensaje = "";
            
            /*
             *Manejo de eventos
             */
            if (Opcion.equals("Guardar")){
                if(model.placaService.placaExist(placa)){
                    if(model.registroTarjetaSvc.BuscarTarjeta(num_tarjeta)){
                        if( model.conductorService.existeConductorCGA(cedula) ){
                            if(!model.registroTarjetaSvc.Buscar(num_tarjeta,cedula,fecha_creacion)){
                                model.registroTarjetaSvc.Insert(dstrct, num_tarjeta, placa, cedula, usuario, usuario );
                                Mensaje = "El Registro ha sido agregado";
                            }
                            else
                                Mensaje = "El Registro ya existe";
                        }else{
                            Mensaje = "El conductor no existe";
                        }
                    }
                    else{
                        Mensaje = "El numero de Tarjeta no existe";
                    }
                    
                }
                else
                    Mensaje = "El Vehiculo no existe ";
                next ="/jsp/masivo/registro_tarjeta/RegistroTarjetaInsert.jsp?Mensaje="+Mensaje;
            }
            
            if (Opcion.equals("Anular")){
                fechacreacion+="%";
                model.registroTarjetaSvc.UpdateEstado("A", num_tarjeta, cedula, fechacreacion, usuario);
                Mensaje = "El Registro ha sido anulado";
                next ="/jsp/masivo/registro_tarjeta/RegistroTarjetaModificar.jsp?Mensaje="+Mensaje+"&reload=ok";
            }
            
            
            if (Opcion.equals("Eliminar")){
                fechacreacion+="%";
                model.registroTarjetaSvc.Delete(num_tarjeta, cedula, fechacreacion);
                Mensaje = "El Registro ha sido eliminado";
                next ="/jsp/masivo/registro_tarjeta/RegistroTarjetaModificar.jsp?Mensaje="+Mensaje+"&reload=ok";
            }
            
            
            if (Opcion.equals("Modificar")){
                fechacreacion+="%";
                nfechacreacion+="%";
                if(model.placaService.placaExist(nplaca)){
                    if(model.registroTarjetaSvc.BuscarTarjeta(nnum_tarjeta)){
                        if( model.conductorService.existeConductorCGA(ncedula) ){
                            if(!model.registroTarjetaSvc.buscarAll(nnum_tarjeta,ncedula,nfechacreacion, nplaca )){
                                model.registroTarjetaSvc.Update(nnum_tarjeta, nplaca, ncedula, usuario, usuario, num_tarjeta, cedula, fechacreacion, nfechacreacion );
                                model.registroTarjetaSvc.Search(nnum_tarjeta, ncedula, nfechacreacion);
                                Mensaje = "El Registro ha sido modificado";
                            }
                            else
                                Mensaje = "El Registro ya existe";
                        }else{
                            Mensaje = "El conductor no existe";
                        }
                    }
                    else{
                        Mensaje = "El numero de Tarjeta no existe";
                    }
                    
                }
                else
                    Mensaje = "El Vehiculo no existe ";
                next ="/jsp/masivo/registro_tarjeta/RegistroTarjetaModificar.jsp?Mensaje="+Mensaje+"&reload=ok";
            }
            
            
            
            
            
            if (Opcion.equals("Listado")){
                model.registroTarjetaSvc.List();
                next ="/jsp/masivo/registro_tarjeta/RegistroTarjetaListado.jsp";
                
            }
            
            if (Opcion.equals("ListadoReg")){
                model.registroTarjetaSvc.ListREG(num_tarjeta+"%", cedula+"%", placa+"%", fechacreacion+"%");
                next ="/jsp/masivo/registro_tarjeta/RegistroTarjetaListado.jsp";
                
            }
            
            if (Opcion.equals("Buscar")){
                fechacreacion+="%";
                model.registroTarjetaSvc.Search(num_tarjeta, cedula, fechacreacion);
                next ="/jsp/masivo/registro_tarjeta/RegistroTarjetaModificar.jsp";
            }
            
            
            
            
            if ("Anular|Eliminar".indexOf(Opcion) != -1) {
                model.registroTarjetaSvc.ReiniciarDato();
                model.registroTarjetaSvc.List();
            }
            
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en RegistroTarjetaManagerAction .....\n"+e.getMessage());
        }
        
    }
    
}
