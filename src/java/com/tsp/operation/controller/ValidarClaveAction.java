/*
 * AcpmDeleteAction.java
 *
 * Created on 2 de diciembre de 2004, 11:30 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class ValidarClaveAction extends Action{ 
    
    /** Creates a new instance of AcpmDeleteAction */
    public ValidarClaveAction() {
    }
    
    public void run() throws ServletException, InformationException {
        //System.out.println("validar clave");
        String next ="/colpapel/fletes.jsp?ccorrect=NO&mensaje=Clave invalida para este cambio";
        String standar =request.getParameter("standard"); 
        String placa =request.getParameter("placa"); 
        String vflete =request.getParameter("vflete"); 
        float nflete =Float.parseFloat(request.getParameter("nuevo")); 
        String nclave= request.getParameter("nclave"); 
        String numsol= request.getParameter("numsol"); 
        String clave= "NO SE PUDO GENERAR";
        HttpSession Session           = request.getSession(); 
        Usuario user                  = (Usuario)Session.getAttribute("Usuario");
        
        String descripcion="";
        String nomag ="";
        try{
            Agencia a =model.agenciaService.obtenerAgencia(user.getId_agencia()); 
            if(a!=null)
                nomag = a.getNombre();
            model.stdjobdetselService.buscaStandard(standar); 
            if(model.stdjobdetselService.getStandardDetSel()!=null){
                Stdjobdetsel stdjobdetsel = model.stdjobdetselService.getStandardDetSel();
                descripcion= stdjobdetsel.getSj_desc();
                
                
            }
            
            
            String texto1=nflete+placa+standar+"-"+descripcion;
            String texto2=numsol+nomag+user.getNombre();
            ////System.out.println("Texto 1: "+texto1);
            ////System.out.println("Texto 2: "+texto2);
            
            
            try{
                clave= com.tsp.util.Util.getClave(texto1,texto2);
            }catch(UnsupportedEncodingException e){
                throw new ServletException(e.getMessage());
            }
            ////System.out.println("Clave Digitada: "+nclave);
            ////System.out.println("Clave Generada: "+clave);
            if(!model.afleteService.estaCumplida(numsol)){
                if(nclave.equals(clave)){
                    next = "/colpapel/fletes.jsp?ccorrect=SI&mensaje=Clave correcta, Ahora puede aplicar el cambio.";
                }
            }
            else
                next = "/colpapel/fletes.jsp?ccorrect=SI&mensaje=NO ES POSIBLE AUTORIZAR EL CAMBIO: La solicitud ya fue utilizada.";
        }catch(SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
    
    
}
