/*
 * ConsultarEgresosManagerAction.java
 *
 * Created on 18 de julio de 2006, 10:09 AM
 */

package com.tsp.operation.controller;

import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  David
 */
public class ConsultarEgresosManagerAction extends Action {
    
    /** Creates a new instance of ConsultarEgresosManagerAction */
    public ConsultarEgresosManagerAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "/jsp/cxpagar/consultaEgresos/consultarEgresos.jsp";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");        
        try{
            String opc = (request.getParameter( "opc" )!= null)?request.getParameter( "opc" ):"";
            if( opc.equalsIgnoreCase("buscar") ){
                String branch_code     = (request.getParameter( "branch_code" )!=null)?request.getParameter( "branch_code" ):"";
                String bank_account_no = (request.getParameter( "bank_account_no" )!=null)?request.getParameter( "bank_account_no" ):"";
                String document_no     = (request.getParameter( "document_no" )!=null)?request.getParameter( "document_no" ):"";
                String tipo_documento  = (request.getParameter( "tipo_documento" )!=null)?request.getParameter( "tipo_documento" ).split("/")[0]:"";
                String fechaInicial    = (request.getParameter( "fechaInicial" )!=null)?request.getParameter( "fechaInicial" ):"";
                String fechaFinal      = (request.getParameter( "fechaFinal" )!=null)?request.getParameter( "fechaFinal" ):"";
                String nit      = (request.getParameter( "proveedor" )!=null)?request.getParameter( "proveedor" ):"";
                String cxp      = (request.getParameter( "cxp" )!=null)?request.getParameter( "cxp" ):"";
                String conceptos       = com.tsp.util.Util.coalesce( request.getParameter( "conceptos" ), "" ).split("/")[0];
                String usuario_creacion= com.tsp.util.Util.coalesce( request.getParameter( "usuario_creacion" ), "" );
                
                Egreso eg = new Egreso();
                eg.setDstrct( usuario.getDstrct() );
                eg.setBranch_code( branch_code );
                eg.setBank_account_no( bank_account_no );
                eg.setDocument_no( document_no );
                eg.setConcept_code( tipo_documento );
                eg.setNit(nit);
                eg.setDocumento(cxp);
                
                session.setAttribute( "branch_code", branch_code );
                session.setAttribute( "bank_account_no", bank_account_no );
                session.setAttribute( "document_no", document_no );
                session.setAttribute( "tipo_documento", request.getParameter( "tipo_documento" ) );
                session.setAttribute( "fechaInicial", fechaInicial );
                session.setAttribute( "fechaFinal", fechaFinal );
                session.setAttribute( "usuario_creacion", usuario_creacion );
                session.setAttribute( "conceptos", conceptos );
                session.setAttribute( "proveedor", nit );
                session.setAttribute( "cxp", cxp );
                session.setAttribute( "resultadoE", "ok" );
                
                //////System.out.println("Valor Tipo Documento: "+request.getParameter( "tipo_documento" ));
                
                model.egresoService.setEgreso( eg );
                if( tipo_documento.equals( "" ) ){
                    model.egresoService.buscarEgresosCab( fechaInicial, fechaFinal, usuario_creacion, conceptos );                    
                }else{
                    model.egresoService.buscarEgresos();
                }
                
                Vector egresos = model.egresoService.getEgresos();
                session.setAttribute( "egresos", egresos );
                if( egresos.size() == 0 ){
                    request.setAttribute( "mensaje", "No se encontraron resultados" );
                    session.setAttribute( "resultadoE", null );
                }                
                          
            }else if( opc.equalsIgnoreCase("ver") ){
                String dstrct = (request.getParameter( "dstrct" )!=null)?request.getParameter( "dstrct" ):"";
                String branch_code = (request.getParameter( "branch_code" )!=null)?request.getParameter( "branch_code" ):"";
                String bank_account_no = (request.getParameter( "bank_account_no" )!=null)?request.getParameter( "bank_account_no" ):"";
                String document_no = (request.getParameter( "document_no" )!=null)?request.getParameter( "document_no" ):"";
                
                Egreso eg = new Egreso();
                eg.setDstrct( dstrct );
                eg.setBranch_code( branch_code );
                eg.setBank_account_no( bank_account_no );
                eg.setDocument_no( document_no );                
                
                model.egresoService.setEgreso( eg );
                model.egresoService.obtenerDetalleEgreso();
                model.egresoService.obtenerCabeceraEgreso();
                next = "/jsp/cxpagar/consultaEgresos/verEgreso.jsp";
            }else if( opc.equalsIgnoreCase("sucursales") ){
                String branch_code = (request.getParameter( "branch_code" )!=null)?request.getParameter( "branch_code" ):"";                
                String document_no = (request.getParameter( "document_no" )!=null)?request.getParameter( "document_no" ):"";
                String tipo_documento = request.getParameter( "tipo_documento" );
                String fechaInicial = (request.getParameter( "fechaInicial" )!=null)?request.getParameter( "fechaInicial" ):"";
                String fechaFinal = (request.getParameter( "fechaFinal" )!=null)?request.getParameter( "fechaFinal" ):"";
                
                session.setAttribute( "branch_code", branch_code );
                session.setAttribute( "bank_account_no", null );
                session.setAttribute( "document_no", document_no );
                session.setAttribute( "tipo_documento", tipo_documento );
                session.setAttribute( "fechaInicial", fechaInicial );
                session.setAttribute( "fechaFinal", fechaFinal );
                
                model.servicioBanco.loadSusursales( branch_code );
                TreeMap opc_sucursales = model.servicioBanco.getSucursal();
                opc_sucursales.put( " Seleccione Sucursal", "" );                
                session.setAttribute( "opc_sucursales", opc_sucursales );
                
                next = "/jsp/cxpagar/consultaEgresos/consultarEgresos.jsp";
            }else if( opc.equalsIgnoreCase("cancelar") ){
                session.setAttribute( "branch_code", null );
                session.setAttribute( "bank_account_no", null );
                session.setAttribute( "document_no", null );
                session.setAttribute( "tipo_documento", null );
                session.setAttribute( "fechaInicial", null );
                session.setAttribute( "fechaFinal", null );
                session.setAttribute( "opc_sucursales", null );
                session.setAttribute( "resultadoE", null );
                next = "/jsp/cxpagar/consultaEgresos/consultarEgresos.jsp";
            }
        }catch(Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);    
    }
    
}
