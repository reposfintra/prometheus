/*
 * SjdelayDeleteAction.java
 *
 * Created on 4 de diciembre de 2004, 09:03 AM
 */

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  KREALES
 */
public class SjdelayDeleteAction extends Action {
    
    /** Creates a new instance of SjdelayDeleteAction */
    public SjdelayDeleteAction() {
    
    
    }
    
    public void run() throws ServletException, InformationException {
        
        String next="/sjdelay/sjdelayDelete.jsp";
        try{
            String sj= request.getParameter("sj");
            String cf=  request.getParameter("cf");
            model.sdelayService.buscar(sj, cf);
            if(model.sdelayService.get()!=null){
                Sjdelay sd= model.sdelayService.get();
                model.sdelayService.anular(sd);
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }    
    
}
