/*
 * RecTSPProcesoAction.java
 *
 * Created on 25 de junio de 2005, 10:41 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Sandrameg
 */
public class InformePredoGenerarAction extends Action{
    
    /** Creates a new instance of RecTSPProcesoAction */
    public InformePredoGenerarAction() {
    }
    
    public void run() throws ServletException,InformationException {
        
        String fechai =request.getParameter("fechai");
        String fechaf =request.getParameter("fechaf");
        String mensaje = "Su reporte esta generandose, para mas informacion consulte el estado del proceso" +
        " y encuentre el resultado en el Directorio de Archivos.";
        
        String next = "/pags_predo/FechasInfo.jsp?mensaje=" + mensaje;
        HttpSession session   = request.getSession();
        Usuario usuario       = (Usuario) session.getAttribute("Usuario");
        
            //model.ipredoSvc.llenarInforme(fechai, fechaf);
            //Vector datos = model.ipredoSvc.getDatos();
            //if(datos!=null){
                HReportePredo hrp = new HReportePredo();
                hrp.start(fechai, fechaf,usuario.getLogin());
            //}
            
        this.dispatchRequest(next);
    }
}
