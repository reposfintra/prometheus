/********************************************************************
 *      Nombre Clase.................   JspAnularAction.java    
 *      Descripci�n..................   Anula una p�gina jsp en el archivo de jsp.
 *      Autor........................   Ing. Rodrigo Salazar
 *      Fecha........................   18.07.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  Rodrigo
 */
public class JspAnularAction extends Action{
    
    /** Creates a new instance of JspAnularAction */
    public JspAnularAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/jsp/trafico/mensaje/MsgAnulado.jsp";
        HttpSession session = request.getSession();        
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String codigo = request.getParameter("c_codigo");
        try{
            model.jspService.anularJsp(usuario.getLogin().toUpperCase(),codigo);
            request.setAttribute("mensaje","MsgAnulado");            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);

    }
    
}
