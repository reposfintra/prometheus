/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;


import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.ListaPoliticasReporteService;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.threads.HReporteCentralRiesgo;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
/**
 *
 * @author aariza
 */
public class ReporteCentralRiesgoAction extends Action {
    
    public ReporteCentralRiesgoAction () {

    
    }

    @Override
    public void run() throws ServletException, InformationException {
     String reporte;
        HttpSession session  = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        reporte=(request.getParameter("reporte") != null) ? request.getParameter("reporte") : "";
        //pilitica datacredito.
        String politica=(request.getParameter("politica") != null) ? request.getParameter("politica") : "";
       
      ListaPoliticasReporteService repor = new ListaPoliticasReporteService(usuario.getBd());
        try{
             if (reporte.equals("DATACREDITO")) {
                    HReporteCentralRiesgo hilo = new HReporteCentralRiesgo();

                /*obtenemos los rangos de vencimientos dependiendo de la 
                 la politica selecionada */
                String[] rangoVencimiento;
                String result = repor.getVencimientosPoliticas(politica, reporte);
                if (!result.equals("")) { //inicio de if interno

                    rangoVencimiento = result.split(",");
                    String convenio = rangoVencimiento[3].equals("Microcredito") ? "10,11,12,13" : rangoVencimiento[2];
                    //validamos si el reporte a generar es el primer reporte y si es la primera vez que se genera.
                    if (!rangoVencimiento[4].equals("")) {
 
                        String s = repor.getPrimerReporte(rangoVencimiento[4]);
                        if (!s.equals("S")) {
                            repor.getConsulta(reporte, rangoVencimiento[0], rangoVencimiento[1], convenio);
                            hilo.start(model, repor.getResultado(), usuario, reporte);

                            boolean condicion = true;
                            do {

                                if (!hilo.isAlive()) {
                                    //actualizo el estado del primer reporte a generado.
                                    repor.actulizarReporte(rangoVencimiento[4]);
                                    this.escribirResponse("OK");
                                    condicion = false;
            }

                            } while (condicion);
                        } else {

                            this.escribirResponse("GENERADO");
        }
                    }else {
                        //genero el reporte cuando no es el primer reporte
                        repor.getConsulta(reporte, rangoVencimiento[0], rangoVencimiento[1], convenio);
                        hilo.start(model, repor.getResultado(), usuario, reporte);

                        boolean condicion = true;
                        do {

                            if (!hilo.isAlive()) {
                                this.escribirResponse("OK");
                                condicion = false;
                            }

                        } while (condicion);
                    
                    
                    }//fin del if del primer reporte

                }else {
                    //respuesta a jquery
                    this.escribirResponse("N");
                
                
                }//fin if interno

            }else if(reporte.equals("CIFIN")){
            
                    this.escribirResponse("Esta entidad todavia esta en veremos...");
            
            }
        }
        catch(Exception e){
            e.printStackTrace();
            e.toString();
            try {
                this.escribirResponse("ERROR");
            } catch (Exception ex) {
                Logger.getLogger(ReporteCentralRiesgoAction.class.getName()).log(Level.SEVERE, null, ex);
        } 
    
        } 
    
    
    
    
    }
    
     protected void escribirResponse(String dato) throws Exception {
        try {
            response.setContentType("text/plain; charset=utf-8");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().print(dato);
        } catch (Exception e) {
            throw new Exception("Error al escribir el response: " + e.toString());
}
    
         }
    
}
