/*
 * Nombre        UbicacionIngresarAction.java
 * Autor         Ing. Jesus Cuesta
 * Modificado    Ing Sandra Escalante
 * Fecha         21 de junio de 2005, 02:42 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class UbicacionIngresarAction extends Action {
        
    public UbicacionIngresarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/jsp/trafico/ubicacion/ingresarUbicacion.jsp";
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");        
        String distrito = (String) session.getAttribute("Distrito");
        
        String codigo = request.getParameter("c_codigo").toUpperCase();
        String desc = request.getParameter("c_descripcion").toUpperCase();
        String cia=request.getParameter("c_cia");////System.out.println("cia " + cia);
        String tipo=request.getParameter("c_tipou");
        String contacto = request.getParameter("c_contacto"); ////System.out.println("contacto" + contacto);       
        String pais=request.getParameter("c_pais");    
        String est = request.getParameter("c_estado");
        String ciudad=request.getParameter("c_ciudad");
        String dir = request.getParameter("c_direccion").toUpperCase();
        String tel1 = request.getParameter("c_tel1").toUpperCase();
        String tel2 = request.getParameter("c_tel2").toUpperCase();
        String tel3 = request.getParameter("c_tel3").toUpperCase();
        String fax =request.getParameter("c_fax").toUpperCase();
        
        //sescalante 01.02.06
        String tramo = (!request.getParameter("c_tramo").equals(""))?request.getParameter("c_tramo"):"";
        String relacion = (!request.getParameter("c_relacion").equalsIgnoreCase(""))?request.getParameter("c_relacion"):"";
        double tiempo = (!request.getParameter("c_tiempo").equals(""))?Double.parseDouble(request.getParameter("c_tiempo")):0;
        double distancia = (!request.getParameter("c_distancia").equals(""))?Double.parseDouble(request.getParameter("c_distancia")):0;
        
        String modalidad = request.getParameter("modalidad");
        
        int sw = 0;
        String o = "";
        String d = "";
        
        //verifico si el tramo es correcto 
        if (tramo.length() == 4){
            o = tramo.substring(0,2);
            d = tramo.substring(tramo.length()-2,tramo.length());
        }
 
        try{
            //verifico si el tramo y la compania exiten
            if (!model.identidadService.existeIdentidad(cia)){
                sw=1;
                request.setAttribute("mensaje","El Nit de la Compania ingresado no existe!");
                next = next + "?sw=e";
            }
            if (modalidad.equalsIgnoreCase("T")){
                if (!model.tramoService.existeTramo(distrito, o, d) ){
                    sw=1;
                    request.setAttribute("mensaje","El Tramo ingresado no existe!");
                    next = next + "?sw=e";
                }
            }
        }catch(SQLException ex){
            
        }
        
        //seteo el objeto
        if(sw==0){
            Ubicacion u = new Ubicacion();
            u.setCod_ubicacion(codigo);
            u.setDescripcion(desc);
            u.setCia(cia);////System.out.println("pasa");
            u.setTipo(tipo);
            u.setContacto(contacto);
            u.setPais(pais);        
            u.setEstado(est);
            u.setCiudad(ciudad);
            u.setDireccion(dir);
            u.setTorigen(o);
            u.setTdestino(d);
            u.setRelacion(relacion);
            u.setTiempo(tiempo);
            u.setDistancia(distancia);
            u.setTel1(tel1);
            u.setTel2(tel2);
            u.setTel3(tel3);
            u.setFax(fax);
            u.setDstrct(distrito);
            u.setBase(usuario.getBase());
            u.setCreation_user(usuario.getLogin());
            u.setUser_update(usuario.getLogin());
            u.setModalidad(modalidad);
            model.ubService.setUb(u);
            
            try{//ingreso ubicacion                
                model.ubService.agregarUbicacion();
                request.setAttribute("mensaje","La ubicacion fue ingresada con exito!");            
            }catch (SQLException e){
                try{//si existe anulada la actualiza de lo contrario sale error
                    if(model.ubService.existeUbicacionAnulada(codigo,distrito)){
                        model.ubService.modificarUbicacion();
                        request.setAttribute("mensaje","La ubicacion fue ingresada con exito!");            
                    }
                    else{
                        request.setAttribute("mensaje","La ubicacion ya existe!");
                         next = next + "?sw=e";
                    }
                }catch (SQLException a){
                    throw new ServletException(a.getMessage());
                }
            }
        }
        this.dispatchRequest(next);
    }
}
