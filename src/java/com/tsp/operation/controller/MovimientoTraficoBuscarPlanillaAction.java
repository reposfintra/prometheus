/******************************************************************
 * Nombre ...................... MovimientoTraficoBuscarPlanillaAction.java
 * Descripci�n.................. Busca el movimiento en tr�fico correspondientes a una planilla.
 * Autor........................ Ing. Andr�s Maturana De La Cruz
 * Fecha........................ 24 de septiembre de 2005
 * Versi�n...................... 1.0
 * Coyright..................... Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

import org.apache.log4j.*;

/**
 *
 * @author  Tito Andr�s
 */
public class MovimientoTraficoBuscarPlanillaAction extends Action{
    
    Logger logger = Logger.getLogger(this.getClass());
    
    /** Creates a new instance of SancionBuscarPlanillaAction */
        public MovimientoTraficoBuscarPlanillaAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        //Pr�xima vista
        String next  = "/jsp/trafico/Movimiento_trafico/MovimientoTraficoConsulta.jsp?marco=no";
        
        String numpla = request.getParameter("numpla").toUpperCase();
        
        try{
            /////////////////////////////// AMATURANA 16.08.2006
            
            model.traficoService.obtenerIngreso_Trafico(numpla);
            Ingreso_Trafico ingreso0 = model.traficoService.getIngreso();            
            
            //////////////////////////////////////////////////////
            
            
            model.rmtService.BuscarReportesPlanilla(numpla);
            model.traficoService.obtenerTrafico(numpla.toUpperCase());
            Ingreso_Trafico ingreso = model.traficoService.getIngreso();
            
            //////////////////////////////////////////////////////////  AMATURANA 16.08.2006
            
            //logger.info("NOMCOND TRAFICO: " + ingreso.getNomcond());
            //logger.info("NOMCOND ITRAFICO: " + ingreso0.getNomcond());
            
            if ( ingreso!=null ){
                if( ingreso.getNomcond()!=null && ingreso.getNomcond().compareTo("NR")==0
                && ingreso0!=null && ingreso0.getNomcond().trim().length()!=0){
                    
                    ingreso.setNomcond(ingreso0.getNomcond());
                    ingreso.setCedcon(ingreso0.getCedcon());
                }
                
                if( ingreso.getNomprop()!=null && ingreso.getNomprop().compareTo("NR")==0
                && ingreso0!=null && ingreso0.getNomprop().trim().length()!=0){
                    
                    ingreso.setNomprop(ingreso0.getNomprop());
                    ingreso.setCedprop(ingreso0.getCedprop());
                }
            }
            
            
            //boolean esdm = model.rmtService.esDespachoManual(numpla);//despacho manual
            boolean esPLA = model.demorasSvc.existePlanilla(numpla);
            boolean esDM = model.demorasSvc.existeDespachoManual(numpla);
            
            logger.info(" EXISTE PLANILLA ? " + esPLA);
            logger.info(" ES DESPACHO MANUAL? " + esDM);
            
            /////////////////////////////////////////////////////////////////////////////////
            
            if( esPLA || esDM ){
                //////System.out.println(".................... MOVIMIENTO TRAFICO: " + model.rmtService.getReportesPlanilla().size());
                if( ingreso!=null ){
                    if( model.rmtService.getReportesPlanilla().size()>0 ){
                        String escoltas = model.traficoService.obtenerEscoltasVehiculo(numpla) + ", " + model.traficoService.obtenerEscoltasCaravana(numpla);
                        escoltas = escoltas.trim();
                        if( escoltas.startsWith(",") ) escoltas = escoltas.substring(1);
                        if( escoltas.endsWith(",") ) escoltas = escoltas.substring(0, escoltas.length() - 1);
                        String caravana = model.traficoService.obtenerNumeroCaravana(numpla);
                        ingreso.setEscolta(escoltas);
                        ingreso.setCaravana(caravana);
                        model.traficoService.setIngreso(ingreso);
                    } else if ( ingreso!=null ) {
                        String escoltas = model.traficoService.obtenerEscoltasVehiculo(numpla) + ", " + model.traficoService.obtenerEscoltasCaravana(numpla);
                        escoltas = escoltas.trim();
                        if( escoltas.startsWith(",") ) escoltas = escoltas.substring(1);
                        if( escoltas.endsWith(",") ) escoltas = escoltas.substring(0, escoltas.length() - 1);
                        String caravana = model.traficoService.obtenerNumeroCaravana(numpla);
                        ingreso.setEscolta(escoltas);
                        ingreso.setCaravana(caravana);
                        model.traficoService.setIngreso(ingreso);
                    } else {
                        next = "/jsp/trafico/Movimiento_trafico/MovimientoTraficoBuscar.jsp?msg=" +
                        "No existe ning�n registro con el n�mero de planilla digitado.&marco=no" +
                        "&numpla=" + numpla;
                    }
                } else {
                    next = "/jsp/trafico/Movimiento_trafico/MovimientoTraficoBuscar.jsp?msg=" +
                    "La planilla no registra en tr�fico.&marco=no" +
                    "&numpla=" + numpla;
                }
            } else {                
                next = "/jsp/trafico/Movimiento_trafico/MovimientoTraficoBuscar.jsp?msg=" +
                "La planilla no existe en el archivo de planillas ni en despacho manual.&marco=no" +
                "&numpla=" + numpla;
            }
        }catch (SQLException e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
}
