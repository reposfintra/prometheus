/*********************************************************************************
 * Nombre clase :                 BuscarClienteAction.java                       *
 * Descripcion :                  Clase que maneja los eventos relacionados      *
 *                                con el programa que busca para el              *
 *                                reporte de las demoras en la BD.               *
 * Autor :                        LREALES                                        *
 * Fecha :                        26 de septiembre de 2006, 02:00 PM             *
 * Version :                      1.0                                            *
 * Copyright :                    Fintravalores S.A.                        *
 ********************************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class BuscarClienteAction extends Action {
    
    /** Creates a new instance of BuscarClienteAction */
    public BuscarClienteAction () { }
    
    public void run () throws ServletException, InformationException {
        
        String next = "/jsp/general/exportar/demoras/BuscarCliente.jsp";
                
        try {
            
            String nom_cli = ( request.getParameter("nom_cli") != null )?request.getParameter( "nom_cli" ):"";
            
            model.reporteDemorasService.clientes ( nom_cli );
                   
            next = next + "?accion=1";
                        
        } catch ( Exception e ){
            
            e.printStackTrace ();
            throw new ServletException ( e.getMessage () );
            
        }
        
        this.dispatchRequest ( next );
        
    }
    
}