/******************************************************************************
 *      Nombre Clase.................   ReporteFacturasProveedorAction.java
 *      Descripci�n..................   Anula un registro en la tabla tblapl
 *      Autor........................   Ing. Andr�s Maturana
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *****************************************************************************/

package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import com.tsp.operation.model.threads.*;

import org.apache.log4j.*;
/**
 *
 * @author  Administrador
 */
public class ReporteFacturasRecurrentesCargarAction extends Action {
    
    Logger logger = Logger.getLogger(this.getClass());
    
    /** Creates a new instance of ReporteEgresoAction */
    public ReporteFacturasRecurrentesCargarAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        try{
            String factura = request.getParameter("factura");
            String nit = request.getParameter("nit");
            String planilla = request.getParameter("planilla");
            String agencia = request.getParameter("agencia");
            String banco = request.getParameter("banco");
            String sucursal = request.getParameter("sucursal");
            String fechai = request.getParameter("FechaI");
            String fechaf = request.getParameter("FechaF");
            
            logger.info("FACTURA: " + factura);
            logger.info("PROVEEDOR: " + nit);
            logger.info("PLANILLA: " + planilla);
            logger.info("AGENCIA: " + agencia);
            logger.info("BANCO: " + banco);
            logger.info("SUCURSAL: " + sucursal);
            logger.info("FECHAI: " + fechai);
            logger.info("FECHAF: " + fechaf);
                        
            request.setAttribute("fechai", fechai );
            request.setAttribute("fechaf", fechaf );
            request.setAttribute("factura", factura );
            request.setAttribute("nit", nit );
            request.setAttribute("planilla", planilla );
            request.setAttribute("agencia", agencia );
            request.setAttribute("banco", banco );
            request.setAttribute("sucursal", sucursal );
            
            String next = "/jsp/cxpagar/reportes/ReporteFacturasRecurrentes.jsp";
            HttpSession session = request.getSession();
            
            String target =  request.getParameter("target")!=null ? request.getParameter("target") : "";
            
            logger.info("TARGET: " + target);
            //String agc = request.getParameter("agc")!=null ? request.getParameter("agc") : "";
            
            //logger.info("AGC: " + agc);            
            
            List ListaAgencias = model.ciudadService.ListarAgencias();
            TreeMap tm = new TreeMap();
            if(ListaAgencias.size()>0) {
                Iterator It3 = ListaAgencias.iterator();
                while(It3.hasNext()) {
                    Ciudad  datos2 =  (Ciudad) It3.next();
                    tm.put("["+datos2.getCodCiu()+"] "+datos2.getNomCiu(), datos2.getCodCiu());
                    //out.print("<option value='"+datos2.getCodCiu()+"'>["+datos2.getCodCiu()+"] "+datos2.getNomCiu()+"</option> \n");
                }
            }
            request.setAttribute("agencias", ListaAgencias);
            request.setAttribute("agencias_tm", tm);
            
            if( target.length()!=0 && target.compareTo("bancos")==0 ){
                model.servicioBanco.loadBancos(agencia, (String) session.getAttribute("Distrito"));
                model.servicioBanco.setSucursal(new TreeMap());
            } else if( target.length()!=0 && target.compareTo("sucursales")==0 ){
                model.servicioBanco.loadSucursalesAgencia(agencia, banco, (String) session.getAttribute("Distrito"));
            }
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en ReporteFacturasRecurrentesCargarAction .....\n"+e.getMessage());
        }
    }
    
}
