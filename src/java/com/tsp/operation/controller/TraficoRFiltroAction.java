/********************************************************************
 *      Nombre Clase.................   TraficoRFiltroAction.java
 *      Descripci�n..................   Action que se encarga de reestablecer los filtros en el programa de control trafico
 *      Autor........................   David Lamadrid
 *      Fecha........................   1 de diciembre de 2005, 10:41 AM
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/ 

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.Util;
/**
 *
 * @author  dlamadrid
 */
public class TraficoRFiltroAction extends Action 
{
    
    /** Creates a new instance of TraficoRFiltroAction */
    public TraficoRFiltroAction ()
    {
    }
    
    public void run () throws ServletException, InformationException
    {
        try{
           String user        = request.getParameter ("usuario");
            String clas        =""+ request.getParameter ("clas");
            String filtro        = "null";
            String configuracion    = ""+request.getParameter ("conf");
            String var1        = ""+request.getParameter ("var1");
            String var2     = ""+request.getParameter ("var2");
            String linea= ""+request.getParameter ("linea");
            
            Vector v = new Vector();
            v = model.traficoService.vAtributos();
            String var3=""+request.getParameter ("var3");
            /*
            for (int  i =0;i<v.size();i++){
                Vector fila = (Vector)v.elementAt(i);
                String valor = ""+request.getParameter (""+fila.elementAt(1));
                if( valor.equals("null")||valor.equals(""))
                    valor="10";
                int va= Integer.parseInt(valor);
                va=va;
                ////System.out.println("valor"+va);
                String nombre =""+ fila.elementAt(0);
                ////System.out.println("nombre"+nombre);
                var3=var3+"-"+nombre+"-"+va;
            }*/
            ////System.out.println("var3: "+ var3);
            
            model.traficoService.listarColTrafico(var3);
            Vector col = model.traficoService.obtColTrafico();
            model.traficoService.listarColtrafico2(col);
            col= model.traficoService.obtColTrafico();
            
            ////System.out.println("Filtro: "+ filtro);
            
             filtro=model.traficoService.decodificar(filtro);
            
            
            //model.traficoService.generarVZonas(user);
            /*  Modificado por: Andr�s Maturana
             *  Fecha: 02.05.2006 */
            model.traficoService.gennerarVZonasTurno(user);
            
            ////System.out.println("Genrerar zonas ");
            Vector zonas = model.traficoService.obdtVectorZonas();
            String validacion=" zona='-1'";
            
             if ((zonas == null)||zonas.size()<=0){}else{
               validacion = model.traficoService.validacion(zonas);
            }
            ////System.out.println("\nValidacion final: "+ validacion);
            if (clas.equals("null")||clas.equals(""))
                clas="";
            if (configuracion.equals("null")||configuracion.equals(""))
                configuracion = model.traficoService.obtConf();
            
            if (filtro.equals("null")||filtro.equals("")){
                filtro = "";
                filtro= "where "+validacion+filtro;
            }
            else
                filtro=filtro;
            
            
            
            configuracion=configuracion+",color_letra,CASE WHEN reg_status='D' THEN get_ultimaobservaciontrafico(planilla,'detencion') ELSE ult_observacion END AS obs,neintermedias ";
           // ////System.out.println("cofiguracion: "+ configuracion);
            String consulta      = model.traficoService.getConsulta(configuracion,filtro,clas);
            ////System.out.println("consulta: "+ consulta); 
             
            
           // ////System.out.println("linea: "+ linea); 
          
            Vector colum= new Vector();
              
           colum = model.traficoService.obtenerCampos(linea);
            
           // ////System.out.println("linea="+linea+" y obtiene colmunas"+colum.size());
            
           // model.traficoService.listarCamposTrafico(linea);
            
            model.traficoService.generarTabla(consulta, colum);
            ////System.out.println("genera tabla ");     
            filtro=model.traficoService.codificar(filtro);
            ////System.out.println("entra en next ");
            String next = "/jsp/trafico/controltrafico/vertrafico2.jsp?var1="+var1+"&var2="+var2+"&var3="+var3+"&filtro="+filtro+"&linea="+linea+"&conf="+configuracion+"&clas="+clas+"&reload=";
              
            ////System.out.println("sale del next ");
            
           RequestDispatcher rd = application.getRequestDispatcher(next);
           if(rd == null)
              throw new Exception("No se pudo encontrar "+ next);
           rd.forward(request, response); 
        }
        catch(Exception e){
          throw new ServletException("Accion:"+ e.getMessage());
        } 
    }
    
}
