/*************************************************************************
 * Nombre ......................InfoactIngresarAction.java               *
 * Descripci�n..................Clase Action para modificar actividad    *
 * Autor........................Ing. Diogenes Antonio Bastidas Morales   *
 * Fecha........................2 de septiembre de 2005, 07:39 PM        * 
 * Versi�n......................1.0                                      * 
 * Coyright.....................Transportes Sanchez Polo S.A.            *
 *************************************************************************/
 
package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

public class InfoactIngresarAction extends Action {
    
    /** Creates a new instance of InfoactIngresarAction */
    public InfoactIngresarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina");
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        Date fecha = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String now = format.format(fecha);
        int sw=0;
        double dur,tiem;
        InfoActividad infact = new InfoActividad();
        infact.setCodCliente( request.getParameter("cliente") );
        infact.setCodActividad(request.getParameter("acti"));
        infact.setNumpla(request.getParameter("numpla"));
        infact.setNumrem(request.getParameter("numrem"));
        infact.setFecini(Util.formatoFechaTimestamp(request.getParameter("fecini")) ); 
        infact.setFecfin(Util.formatoFechaTimestamp(request.getParameter("fecfin")) );
        if( request.getParameter("duracion")!=null && !request.getParameter("duracion").equals("") )
           dur=  Double.parseDouble(request.getParameter("duracion"));
        else
            dur=0d;         
        
        infact.setDuracion(dur); 
        infact.setDocumento((request.getParameter("documento")!=null)?request.getParameter("documento"):""); 
        
        if (request.getParameter("tiempo")!=null && !request.getParameter("tiempo").equals(""))
            tiem=Double.parseDouble(request.getParameter("tiempo"));
        else
            tiem=0d;
        infact.setTiempoDemora( tiem ); 
        infact.setCausaDemora((request.getParameter("cdemora")!=null)?request.getParameter("cdemora"):"");
        infact.setResdemora((request.getParameter("resdem")!=null)?request.getParameter("resdem"):"");
        
        //Nuevos Campos
        infact.setCantrealizada((request.getParameter("cantrealizada")!=null)?request.getParameter("cantrealizada"):"");
        infact.setCantplaneada((request.getParameter("cantplaneada")!=null)?request.getParameter("cantplaneada"):"");
        
        //Nuevos Campos 19-12-2005
        infact.setReferencia1((request.getParameter("referencia1")!=null)?request.getParameter("referencia1"):"");
        infact.setReferencia2((request.getParameter("referencia2")!=null)?request.getParameter("referencia2"):"");
        infact.setRefnumerica1(Integer.parseInt( (request.getParameter("refnumerica1")!=null)?request.getParameter("refnumerica1"):"0") );
        infact.setRefnumerica2(Integer.parseInt((request.getParameter("refnumerica2")!=null)?request.getParameter("refnumerica2"):"0") );
        
        infact.setFeccierre((!request.getParameter("feccierre").equals("")&&request.getParameter("feccierre")!=null)?request.getParameter("feccierre"):"0099-01-01 00:00:00"); 
        infact.setObservacion((request.getParameter("c_obse")!=null)?request.getParameter("c_obse"):""); 
        infact.setCreation_user(usuario.getLogin());
        infact.setUser_update(usuario.getLogin());
        infact.setBase(usuario.getBase());
        infact.setDstrct(usuario.getCia());
        infact.setLast_update(now);
        infact.setCreation_date(now);
       
        try{
            
            //busco la  actividad para cargar la paguina
            model.actividadSvc.buscarActividad(request.getParameter("acti"), usuario.getCia());            


            try{
                model.infoactService.insertarInfoActividad(infact);
                next = next+"?men=agregado";
            }catch (SQLException e){
                sw=1;
            }
            if(sw==1){
               if (model.infoactService.existeActividadInfoAnulada(infact.getCodActividad(),infact.getCodCliente(),infact.getDstrct(), infact.getNumrem())){
                   ////System.out.println("Modifico");
                    infact.setEstado("");
                    model.infoactService.modificarInfoActividad(infact);    
                    next = next+"?men=agregado";
                }            
                else
                    next = next+"?men=Error";
            }
            
            if (tiem > 0d){
               model.rmtService.sumarDemoraAct(tiem,request.getParameter("numpla"));
               //Ingreso en reporte Trafico
               RepMovTrafico rmt = new RepMovTrafico();
               rmt.setDstrct(usuario.getCia());
               rmt.setReg_Status("");
               rmt.setNumpla(request.getParameter("numpla"));
               rmt.setObservacion(request.getParameter("desact"));
               rmt.setTipo_procedencia("ADU");
               rmt.setUbicacion_procedencia(usuario.getId_agencia());
               rmt.setTipo_reporte("En Via");
               rmt.setFechareporte(now);
               rmt.setLast_update(now);
               rmt.setUpdate_user(usuario.getLogin());
               rmt.setCreation_date(now);
               rmt.setCreation_user(usuario.getLogin());
               rmt.setBase(usuario.getBase());
               rmt.setZona(usuario.getId_agencia());
               //***
               rmt.setCausa(""); 
               rmt.setClasificacion(""); 
               model.rmtService.BuscarReporteMovTraf(request.getParameter("numpla"));
               RepMovTrafico rmt1 = model.rmtService.getReporteMovTraf();
               if (rmt1 != null ){
                    rmt.setFec_rep_pla(rmt1.getFecha_prox_reporte());
               }
               else{
                   rmt.setFec_rep_pla("0099-01-01 00:00:00");
               }
                           
               model.tService.crearStatement();            
               model.tService.getSt().addBatch(model.rmtService.addRMT(rmt));
               model.tService.execute();
            }
            ////System.out.println(next);

        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    
    }
    
}
