/**
 * Nombre        SJCostosAction.java
 * Descripci�n
 * Autor         Mario Fontalvo Solano
 * Fecha         3 de mayo de 2006, 03:20 PM
 * Version       1.0
 * Coyright      Transportes S�nchez Polo S.A.
 **/

package com.tsp.operation.controller;


public class SJCostosAction extends Action {
    
    /** Crea una nueva instancia de  SJCostosAction */
    public SJCostosAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        try{
            String next     = "/jsp/masivo/utilidad/costos.jsp";
            String opcion   = defaultString("Opcion"  , "");
            String distrito = defaultString("Distrito", "");
            String estandar = defaultString("Estandar", "");
            String origen   = defaultString("Origen"  , "");
            if (opcion.equalsIgnoreCase("Consultar")){
                model.SJCostoSvc.obtenerCostos(distrito, estandar);
                if (model.SJCostoSvc.getListado().isEmpty()){
                    request.setAttribute("msg", "No se econtraron datos para el estandar definido");
                }
                else{
                    model.SJCostoSvc.inicializarIndices();
                    model.SJCostoSvc.setEstandar(
                       model.stdjobServices.BuscarStdjob(distrito, estandar)
                    );
                    if (origen.equals(""))
                        model.SJCostoSvc.generarRutas();    
                    else
                        model.SJCostoSvc.generarRutas(origen, model.SJCostoSvc.getEstandar().getdestination_code());    
                }
            }
            else if ( opcion.equalsIgnoreCase("NuevoOrigen") ){
                model.SJCostoSvc.generarRutas(origen, model.SJCostoSvc.getEstandar().getdestination_code());  
            }
            else if ( opcion.equalsIgnoreCase("Resetear" )){
                model.SJCostoSvc.setEstandar(null);
                model.SJCostoSvc.setIndicesIniciales(null);
                model.SJCostoSvc.setListado(null);
                model.SJCostoSvc.setListarutas(null);
            }
            this.dispatchRequest(next);
        }
        catch (Exception ex){
            ex.printStackTrace();
            throw new javax.servlet.ServletException(" Error en SJCostosAction.run() ....\n" + ex.getMessage());
        }
    }
    
    /**
     * Funcion para obtener un parametro del objeto request
     * y en caso de no existri este devuelve el segundo parametro
     * @autor mfontalvo
     * @param name, nombre del parametro
     * @param opcion, valor opcional a devolver en caso de que nom exista
     * @return Parametro del request
     */
    private String defaultString(String name, String opcion){
        return (request.getParameter(name)==null?opcion:request.getParameter(name));
    }    
    
}
