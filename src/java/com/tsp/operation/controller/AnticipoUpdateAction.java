/*
 * AnticipoUpdateAction.java
 *
 * Created on 3 de diciembre de 2004, 08:38 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

/**
 *
 * @author  KREALES
 */
public class AnticipoUpdateAction extends Action{
    
    static Logger logger = Logger.getLogger(SalidaValidarAction.class);
    
    /** Creates a new instance of AnticipoUpdateAction */
    public AnticipoUpdateAction() {
    }
    
    public void run() throws ServletException, InformationException{
        String next = "/anticipo/anticipoUpdate.jsp?mensaje=ok";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String nit = request.getParameter("nit");
        String sucursal = request.getParameter("codigo");
        
        try{
            model.proveedoranticipoService.buscaProveedor(nit,sucursal);
            if(model.proveedoranticipoService.getProveedor()!=null){
                Proveedor_Anticipo pa= model.proveedoranticipoService.getProveedor();
                pa.setCity_code(request.getParameter("ciudad"));
                pa.setDstrct(request.getParameter("distrito"));
                pa.setCreation_user(usuario.getLogin());
                model.proveedoranticipoService.updateProveedor(pa);
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
    
}
