/*
 * PealeDeleteAction.java
 *
 * Created on 6 de diciembre de 2004, 09:00 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class PeajesDeleteAction extends Action{
    
    /** Creates a new instance of PealeDeleteAction */
    public PeajesDeleteAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String Mensaje = "El registro ha sido anulado";
        String next = "/peajes/peajeDelete.jsp?Mensaje="+Mensaje;
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String codigo = request.getParameter ("tiket");
        
        try{
            model.peajeService.buscar (codigo);
            if(model.peajeService.get ()!=null){
                Peajes p= model.peajeService.get ();
                model.peajeService.anular (p);
            }
        }catch (SQLException e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
    
}
