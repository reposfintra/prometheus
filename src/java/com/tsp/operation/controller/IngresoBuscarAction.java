/*************************************************************************
 * Nombre:        IngresoBuscarAction.java
 * Descripci�n:   Clase Action para buscar ingreso
 * Autor:         Ing. Diogenes Antonio Bastidas Morales
 * Fecha:         16 de mayo de 2006, 11:02 AM
 * Versi�n        1.0
 * Coyright:      Transportes Sanchez Polo S.A.
 *************************************************************************/



package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.pdf.*;
import com.tsp.util.*;

public class IngresoBuscarAction extends Action{
    
    /** Creates a new instance of IngresoBuscarAction */
    public IngresoBuscarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        com.tsp.finanzas.contab.model.Model modelcontab = (com.tsp.finanzas.contab.model.Model) session.getAttribute("modelcontab");
        String next = "/jsp/cxcobrar/ingreso/";
        String num    = ( request.getParameter("numero") != null )?request.getParameter("numero").toUpperCase():"";
        String codcli = request.getParameter("cliente");
        String fecini = request.getParameter("fechaInicio");
        String fecfin = request.getParameter("fechaFinal");
        String evento = request.getParameter("evento")!=null?request.getParameter("evento"):"";
        String factura = request.getParameter("factura");
        String tipo = request.getParameter("tipodoc");
        String estado = request.getParameter("reg_status");
        String mon_local = "";
        boolean sw = false;
        
        /*Jescandon 28-03-07*/
        //Impresion de Ingresos por Lotes
        String imp          = ( request.getParameter("imp") != null )?request.getParameter("imp"):"";
        String datos[]      = Util.coalesce( request.getParameter("LOV"), "" ).split(";");
        
        try{
            if ( imp.equals("")  ){
                
                if(evento.equals("1")){
                    model.ingresoService.buscarIngreso(usuario.getDstrct(),tipo, num);
                    Ingreso ing = model.ingresoService.getIngreso();
                    if(ing != null){
                        String age = (ing.getAgencia_ingreso().equals("OP"))?"":ing.getAgencia_ingreso();
                        model.servicioBanco.loadBancos(age, usuario.getDstrct());
                        model.servicioBanco.loadSucursalesAgenciaCuenta(age, ing.getBranch_code().replaceAll("\n",""), usuario.getDstrct());
                        model.monedaService.cargarMonedas();
                        modelcontab.subledgerService.buscarCuentasTipoSubledger(ing.getCuenta());
                        model.tablaGenService.buscarRegistros("CONINGRESO");
                        next+="ingresoMod.jsp?sw=ok";
                        if(!model.ingresoService.tieneItemsIngreso( ing.getDstrct(), ing.getTipo_documento(), ing.getNum_ingreso() )  )
                            next+="&modificar=true";
                        else
                            next+="&modificar="+null;
                    }
                    else{
                        sw = true;
                    }
                    
                }
                else if( evento.equals("2") ){
                    model.ingresoService.buscarIngresos_NroFactura(usuario.getDstrct(), factura.toUpperCase());
                    if(model.ingresoService.getListadoingreso().size() > 0){
                        next+="verIngresos.jsp";
                    }
                    else{
                        sw = true;
                    }
                }
                else if(evento.equals("")){
                    model.ingresoService.buscarIngresosCliente(usuario.getDstrct(), tipo, codcli.toUpperCase(), fecini, fecfin,estado);
                    //System.out.println(model.ingresoService.getListadoingreso().size());
                    if(model.ingresoService.getListadoingreso().size() > 0){
                        next+="verIngresos.jsp";
                    }
                    else{
                        sw = true;
                    }
                }
                
                if(sw){
                    next += "BuscarIngreso.jsp?msg=ok";
                }
                
            }
            
            String Mensaje = "";
            if( imp.equals("lote") ){
                String tipo_aux     = "";
                for ( int i  = 0; datos!=null && i < datos.length; i++ ){
                    String s_obj        = datos[i];
                    
                    if( s_obj.split(",").length > 2  ){
                        tipo_aux             = s_obj.split(",")[1];//Tipo
                    }//Validacion de Longitud de Datos
                    
                    
                }//Validacion De Tipo de Ingresos
                
                if( tipo_aux.equals("ING")){
                    IngresoPDF ing = new IngresoPDF();
                    ing.RemisionPlantilla();
                    ing.crearRaiz();
                    ing.crearRemision(model, datos, "items", usuario.getLogin() );
                    ing.generarPDF();
                    next = "/pdf/IngresoPDF.pdf";
                }//Impresion de Ingresos Clientes
                else{
                    NotaCreditoPDF nota = new NotaCreditoPDF();
                    nota.RemisionPlantilla();
                    nota.crearRaiz();
                    nota.crearRemision(model, datos, "items", usuario.getLogin() );
                    nota.generarPDF();
                    next = "/pdf/NotaCreditoPDF.pdf";
                }//Impresion Notas Creditos
                
            }//fin de Opcion de Nota Credito
            
            
            
            
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
}
