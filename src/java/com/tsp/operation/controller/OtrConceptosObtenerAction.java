/********************************************************************
 *      Nombre Clase.................   AplicacionObtenertAction.java    
 *      Descripci�n..................   Obtiene un registro de la tabla otros_conceptos
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   27.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class OtrConceptosObtenerAction extends Action{
        
        /** Creates a new instance of DocumentoInsertAction */
        public OtrConceptosObtenerAction() {
        }
        
        public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
                //Pr�xima vista
                String pag  = "/jsp/masivo/tblotros_conceptos/otrconceptUpdate.jsp?mensaje=";
                String next = "";
                
                String tabla = request.getParameter("tabla");
                String desc = request.getParameter("desc");
                
                //Usuario en sesi�n
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario) session.getAttribute("Usuario");

                try{   
                        Concepto concepto = model.otros_conceptosSvc.obtenerConcepto(tabla, desc);
                        session.setAttribute("conceptoUpdate", concepto);
                        next = com.tsp.util.Util.LLamarVentana(pag, "Actualizar Definici�n Otros Conceptos");

                }catch (SQLException e){
                       throw new ServletException(e.getMessage());
                }

                this.dispatchRequest(next);
        }
        
}
