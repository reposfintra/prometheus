/*
 * PlanillaInsert.java
 *
 * Created on 22 de noviembre de 2004, 10:29 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import com.tsp.exceptions.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  KREALES
 */
public class PlanillaInsertAction extends Action{
    
    /** Creates a new instance of PlanillaInsert */
    public PlanillaInsertAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String mensajes="";
        String destino="";
        String origen="";
        String nit="";
        String numpla="";
        String numre="";
        String next;
        String unit_of_work="";
        String moneda="";
        String cf_code="";
        float costoRem=0;
        float costoPla=0;
        float pesoreal=0;
        HttpSession session = request.getSession();
        String fecha="";
        String nombre="";
        String descripcion="";
        String unit_transp="";
        float full_weight_m=Float.parseFloat(request.getParameter("pesolm"));
        float empty_weight_m=Float.parseFloat(request.getParameter("pesovm"));
        String remision = request.getParameter("remision");
        String traffic_code =request.getParameter("trafico");
        
        
        Planilla pla= new Planilla();
        Remesa rem = new Remesa();
        Plaaux plaaux= new Plaaux();
        
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        RemPla rempla= new RemPla();
        
        
        try{
            model.tService.crearStatement();
            model.stdjobdetselService.buscaStandardDetSel(request.getParameter("standard"));
            if(model.stdjobdetselService.getStandardDetSel()!=null){
                Stdjobdetsel stdjobdetsel = model.stdjobdetselService.getStandardDetSel();
                destino=stdjobdetsel.getDestination_code();
                origen= stdjobdetsel.getOrigin_code();
                unit_of_work= stdjobdetsel.getUnit_of_work();
                moneda=stdjobdetsel.getCurrency();
                costoRem=stdjobdetsel.getVlr_freight();
                descripcion = stdjobdetsel.getSj_desc();
            }
            
            model.stdjobcostoService.buscaStdJobCosto(request.getParameter("standard"));
            if(model.stdjobcostoService.getStandardCosto()!=null){
                Stdjobcosto stdjobcosto = model.stdjobcostoService.getStandardCosto();
                cf_code=stdjobcosto.getCf_code();
                unit_transp=stdjobcosto.getUnit_transp();
                if(cf_code==null){
                    cf_code="";
                }
            }
            String proveedor="";
            model.placaService.buscaPlaca(request.getParameter("placa"));
            if(model.placaService.getPlaca()!=null){
                Placa placa = model.placaService.getPlaca();
                nit=placa.getPropietario();
                proveedor = placa.getProveedor();
                
            }
            //Se asigna una serie a la planilla.
            model.seriesService.obtenerSerie( "001", usuario);
            if(model.seriesService.getSerie()!=null){
                Series serie = model.seriesService.getSerie();
                String sql = model.seriesService.asignarSerie(serie,usuario);
                ////System.out.println(sql);
                if(sql !=null)
                    model.tService.getSt().addBatch(sql);
                numpla=serie.getPrefix()+serie.getLast_number();
                ////System.out.println("Encontro la serie de planilla");
            }
            
            model.seriesService.obtenerSerie( "002", usuario);
            
            if(model.seriesService.getSerie()!=null){
                Series serie = model.seriesService.getSerie();
                String sql = model.seriesService.asignarSerie(serie,usuario);
                ////System.out.println(""+sql);
                if(sql != null)
                    model.tService.getSt().addBatch(sql);
                numre=serie.getPrefix()+serie.getLast_number();
                ////System.out.println("Encontro la serie de remesa");
            }
            
            if(model.conductorService.conductorExist(request.getParameter("conductor"))){
                model.conductorService.buscaConductor(request.getParameter("conductor"));
                if(model.conductorService.getConductor()!=null){
                    Conductor conductor = model.conductorService.getConductor();
                    nombre=conductor.getNombre();
                }
                
            }
            
            pla.setAgcpla(usuario.getId_agencia());
            pla.setCia(usuario.getDstrct());
            pla.setDespachador(usuario.getLogin());
            pla.setDespla(destino);
            pla.setNitpro(nit);
            pla.setNumpla(numpla);
            pla.setOripla(origen);
            pla.setPlatlr(request.getParameter("trailer").toUpperCase());
            pla.setPlaveh(request.getParameter("placa").toUpperCase());
            pla.setCedcon(request.getParameter("conductor"));
            pla.setTipoviaje("NOR");
            pla.setMoneda(moneda);
            pla.setOrden_carga(request.getParameter("orden"));
            pla.setNomCond(nombre);
            pla.setPesoreal(0);
            pla.setVlrpla(0);
            pla.setRuta_pla(origen+""+destino);
            pla.setPrecinto("");
            pla.setUnit_transp(unit_transp);
            pla.setMoneda(moneda);
            pla.setTraffic_code(traffic_code);
            pla.setUnit_cost(0);
            pla.setProveedor(proveedor);
            model.planillaService.setPlanilla(pla);
            model.tService.getSt().addBatch(model.planillaService.ActualizarTrafficTag(usuario.getBase()));
            //Agrego la planilla
            
            //model.planillaService.insertPlanilla(pla);
            
            
            model.tService.getSt().addBatch(model.planillaService.insertPlanilla(pla,usuario.getBase()));
            
            rem.setAgcRem(usuario.getId_agencia());
            rem.setCia(usuario.getDstrct());
            rem.setCliente(request.getParameter("standard").substring(0,3));
            rem.setDesRem(destino);
            rem.setNumRem(numre);
            rem.setOriRem(origen);
            rem.setStdJobNo(request.getParameter("standard"));
            rem.setTipoViaje("NA");
            rem.setUnidcam(moneda);
            rem.setUnitOfWork(unit_of_work);
            rem.setUsuario(usuario.getLogin());
            rem.setVlrRem(costoRem);
            rem.setPesoReal(pesoreal);
            rem.setDocInterno("");
            rem.setRemitente("");
            rem.setDestinatario("");
            rem.setObservacion("");
            rem.setDescripcion(descripcion);
            rem.setRemision("CC"+remision);
            //Agrego la Remesa.
            model.tService.getSt().addBatch(model.remesaService.insertRemesa(rem,usuario.getBase()));
            
            
            rempla.setPlanilla(numpla);
            rempla.setRemesa(numre);
            rempla.setDstrct(usuario.getDstrct());
            //Agrego la Remesa-Planilla
            
            model.tService.getSt().addBatch(model.remplaService.insertRemesa(rempla,usuario.getBase()));
            
            plaaux.setDstrct(usuario.getDstrct());
            plaaux.setFull_weight(0);
            plaaux.setPlanilla(numpla);
            plaaux.setAcpm_code("");
            plaaux.setFull_weight_m(full_weight_m);
            plaaux.setEmpty_weight_m(empty_weight_m);
            plaaux.setCreation_user(usuario.getLogin());
            //Agrego la planilla auxiliar.
            
            model.tService.getSt().addBatch(model.plaauxService.insertPlaaux(plaaux,usuario.getBase()));
            
            List listTabla = model.tbltiempoService.getTblTiempos(request.getParameter("standard"));
            Iterator itTbla=listTabla.iterator();
            while(itTbla.hasNext()){
                
                Tbltiempo tbl = (Tbltiempo) itTbla.next();
                String tblcode = tbl.getTimeCode();
                int secuence = tbl.getSecuence();
                int hora =0;
                String minuto="00";
                fecha = request.getParameter(tbl.getTimeCode()).substring(0,10);
                hora=Integer.parseInt(request.getParameter("h"+tbl.getTimeCode()).substring(0,2));
                ////System.out.println(""+hora);
                minuto=request.getParameter("h"+tbl.getTimeCode()).substring(3,5);
                ////System.out.println(""+minuto);
                
                
                
                fecha= fecha + " "+hora+":"+minuto+":00";
                
                Planilla_Tiempo pla_tiempo= new Planilla_Tiempo();
                
                pla_tiempo.setCf_code(cf_code);
                pla_tiempo.setCreation_user(usuario.getLogin());
                pla_tiempo.setDate_time_traffic(fecha);
                pla_tiempo.setDstrct(usuario.getDstrct());
                pla_tiempo.setPla(numpla);
                pla_tiempo.setSecuence(secuence);
                pla_tiempo.setSj(request.getParameter("standard"));
                pla_tiempo.setTime_code(tblcode);
                model.tService.getSt().addBatch(model.pla_tiempoService.insertTiempo(pla_tiempo,usuario.getBase()));
            }
            
            
            
            next="/despacho/InicioMasivo.jsp?mensaje="+mensajes+"&numpla="+numpla+"&fecha="+fecha;
            
            model.tService.execute();
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
        
    }
    
}
