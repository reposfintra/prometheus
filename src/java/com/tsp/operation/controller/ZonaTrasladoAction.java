/***********************************************
 * Nombre clase: ZonaTrasladoAction.java
 * Descripci�n: Accion modificar una planilla de zona en trafico.
 * Autor: Ing. Sandra Escalante
 * Fecha: 22 de septiembre de 2005, 04:05 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 **********************************************/

package com.tsp.operation.controller;

//import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.exceptions.*;
import com.tsp.util.Util.*;

/**
 *
 * @author  sescalante
 */
public class ZonaTrasladoAction extends Action {
    /** Creates a new instance of PlanillaBuscarxNumero */
    public ZonaTrasladoAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next= "/jsp/trafico/zona/cambioZona.jsp";
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String distrito = (String) session.getAttribute ("Distrito");
        String planilla = request.getParameter ("planilla");
        try{
           if (request.getParameter ("tipo").equals ("buscar")){//busqueda de la planilla
                model.planillaService.buscarPlanillaxNumero (planilla);
                Planilla pla = model.planillaService.getPlanilla ();
                if (pla == null){
                    next = "/jsp/trafico/zona/planillaxnro.jsp?mensaje=Planilla no existe!!";
                }
            }else if (request.getParameter ("tipo").equals ("modificar")) {//traslado de planilla de zona
                model.zonaService.buscarZona (request.getParameter ("nzona"));
                model.zonaService.cambioZonaPlanilla (planilla, request.getParameter ("nzona"),
                model.zonaService.obtenerZona ().getNomZona (), distrito, usuario.getLogin (),
                usuario.getBase (),request.getParameter ("azona"));
                model.planillaService.buscarPlanillaxNumero (planilla);
                next = next + "?mensaje=Proceso exitoso!!&reload=ok&marco=no";
            }            
        }catch (SQLException e ){
            throw new ServletException (e.getMessage ());
        }
        
        this.dispatchRequest (next);
    }
}
