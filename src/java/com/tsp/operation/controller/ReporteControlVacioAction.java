/**
 * Nombre       ReporteControlVacioAction.java
 * Autor        Ing Jose de la Rosa
 * Fecha        15 de agosto de 2006, 08:24 AM
 * versi�n      1.0
 * Copyright    Transportes Sanchez Polo S.A.
 **/

package com.tsp.operation.controller;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;
import com.tsp.util.*;
import com.tsp.exceptions.*;

public class ReporteControlVacioAction extends Action{
    
    /** Creates a new instance of ReporteControlVacioAction */
    public ReporteControlVacioAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next = "/jsp/general/exportar/ReporteControlVacios.jsp";
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String distrito = (String) session.getAttribute ("Distrito");
        String fchi = (String) request.getParameter ("fechaInicio");
        String fchf = (String) request.getParameter ("fechaFinal");
        int dias = Util.diasTranscurridos (fchi,fchf);
        try{
            if(dias <=30 ){
                ReporteControlVaciosThreads hilo = new ReporteControlVaciosThreads(model);
                hilo.start( fchi, fchf, usuario.getLogin (), distrito);
                next += "?msg=La generacion del Reporte ha iniciado...<br>Para realizar el seguimiento del proceso haga clic en el vinculo&ruta=PROCESOS/Seguimiento de Procesos";
            }
            else
                next += "?msg=Debe escoger un rango de fecha menor o iqual a 30 dias";
        }catch (Exception ex){
            throw new ServletException ("Error en ReporteControlVaciosAction......\n"+ex.getMessage ());
        }
        this.dispatchRequest (next);
    } 
}
