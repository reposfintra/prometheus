/********************************************************************
 *      Nombre Clase.................   DocumentoActividadActualizarAction.java    
 *      Descripci�n..................   Obtiene las actividades de un documento.
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   19.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class DocumentoActividadActualizarAction extends Action{
        
        /** Creates a new instance of DocumentoInsertAction */
        public DocumentoActividadActualizarAction() {
        }
        
        public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
                //Pr�xima vista
                String pag  = "/jsp/masivo/tbldoc_act/DocumentoActividadInsert.jsp?mensaje=MsgUpdate";
                String next = "";                
                
                String doctype = request.getParameter("doc_selec");
                
                //Usuario en sesi�n
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario) session.getAttribute("Usuario");
                String distrito = (String) session.getAttribute("Distrito");

                try{                           
                        model.actividad_documentoSvc.setDocselected(doctype);
                        model.actividad_documentoSvc.GenerarJSCampos(distrito, doctype);
                        next = com.tsp.util.Util.LLamarVentana(pag, "Asignaci�n de Actividades a Documento");

                }catch (SQLException e){
                       throw new ServletException(e.getMessage());
                }

                this.dispatchRequest(next);
        }
        
}
