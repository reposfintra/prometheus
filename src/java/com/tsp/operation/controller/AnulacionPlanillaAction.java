/***************************************************************************
 * Nombre clase : ............... AnulacionPlanillaAction.java             *
 * Descripcion :................. Clase que maneja los eventos             *
 *                                relacionados con el programa de          *
 *                                Reporte de viajes                        *
 * Autor :....................... Ing. Juan Manuel Escandon Perez          *
 * Fecha :........................ 23 de noviembre de 2005, 03:08 PM       *
 * Version :...................... 1.0                                     *
 * Copyright :.................... Fintravalores S.A.                 *
 ***************************************************************************/
package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import com.tsp.operation.model.threads.*;
public class AnulacionPlanillaAction extends Action{
    
    /** Creates a new instance of AnulacionPlanillaAction */
    public AnulacionPlanillaAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        try{
            String next = "";
            
            String opcion                 = request.getParameter("Opcion");
            String agencia                = (request.getParameter("agencia")!=null) ? request.getParameter("agencia") : "";
            String fechai                 = (request.getParameter("finicial")!=null) ? request.getParameter("finicial") : "";
            String fechaf                 = (request.getParameter("ffinal")!=null) ? request.getParameter("ffinal") : "";
            String usuario                = (request.getParameter("usuario")!=null) ? request.getParameter("usuario") : "";
            String numrem                 = (request.getParameter("numrem")!=null) ? request.getParameter("numrem") : "";
            
            HttpSession Session           = request.getSession();
            Usuario user                  = new Usuario();
            user                          = (Usuario)Session.getAttribute("Usuario");
            
            
            String Mensaje = "";
            
            usuario                     =  (usuario.equals("")) ? "%" : usuario;
            
            if(opcion.equals("Generar")){
                model.AnulacionPlanillaSvc.listAnulacionPlanillas(fechai, fechaf, agencia, usuario);
                next = "/jsp/masivo/reportes/PlanillasAnuladas.jsp?fechai="+fechai+"&fechaf="+fechaf;
            }
            
            if(opcion.equals("Excel")){
                ReporteAnulacionPlanilla hilo = new ReporteAnulacionPlanilla();
                hilo.start(model.AnulacionPlanillaSvc.getListPA(),fechai,fechaf,user.getLogin());
                Mensaje = "El proceso se ha iniciado exitosamente";
                next = "/jsp/masivo/reportes/PlanillasAnuladas.jsp?Mensaje="+Mensaje;
                
            }
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en RemesaSinDocAction .....\n"+e.getMessage());
        }
    }
    
}
