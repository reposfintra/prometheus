/********************************************************************
 *      Nombre Clase.................   TurnosEliminarAction.java
 *      Descripci�n..................   Action para Eliminar un registro de turnos en trafico
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  dlamadrid
 */
public class TurnosEliminarAction extends Action{
    
    /** Creates a new instance of TurnosEliminarAction */
    public TurnosEliminarAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next="";
        try {
            HttpSession session = request.getSession ();
            Usuario usuario = (Usuario) session.getAttribute ("Usuario");
            String userlogin=""+usuario.getLogin ();
            String usuario_turno=""+request.getParameter ("u");
            String fecha_turno=""+request.getParameter ("f");
            String h_E=""+request.getParameter ("hE");
            String h_S=""+request.getParameter ("hS");
            model.turnosService.eliminar (usuario_turno, fecha_turno, h_E, h_S);
            //model.turnosService.turnosPorCodigo (usuario_turno,fecha_turno,"now()");
            model.turnosService.turnosPorCodigo (usuario_turno,request.getParameter("fechai"), request.getParameter("fechaf"));
            /*  Modificado por: Andr�s Maturana
             *  Fecha: 02.05.2006 */
            model.traficoService.eliminarConfiguraciones(usuario_turno);
            next="/jsp/trafico/turnos/reporte1.jsp?marco=no";
        }
        catch (Exception e) {
            throw new ServletException (e.getMessage ());
            
        }
        this.dispatchRequest (next);
    }
    
}
