/*
 * MenuSeriesAction.java
 *
 * Created on 1 de diciembre de 2004, 03:25 PM
 */

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
/**
 *
 * @author  mcelin
 */
public class RemesaBuscarAction extends Action {
    
    /** Creates a new instance of MenuSeriesAction */
    public RemesaBuscarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String AgenciaUsuario =usuario.getId_agencia();
            String Numrem = request.getParameter("Numrem");
            String next = "/impresion/remesa.jsp";
            
            if(Numrem == null)
                model.RemesaSvc.buscaRemesa_no_impresa(Numrem,usuario.getLogin(),2);
            else{
                model.RemesaSvc.buscaRemesa_no_impresa(Numrem.toUpperCase(),usuario.getLogin(),1);
                if(model.RemesaSvc.getRegistros().size()==0)
                    next = "/impresion/buscarremesa.jsp?Mensaje='La remesa con codigo ["+Numrem+"] no existe en la base de datos...'";
            }
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
        }
        catch(Exception e) {
            throw new ServletException(e.getMessage());
        }
    }
    
    
}
