/*
 * AnticipoInsertAction.java
 *
 * Created on 2 de diciembre de 2004, 04:47 PM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  KREALES
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class AnticipoInsertAction extends Action {
    
    /** Creates a new instance of AnticipoInsertAction */
    public AnticipoInsertAction() {
    }
    
    public void run() throws ServletException, InformationException{
        String nit= request.getParameter("nit");
        request.setAttribute("nit", "ECE0D8");
        String next = "/anticipo/anticipoInsert.jsp";
        String sucursal = request.getParameter("sucursal");
        int sw=0;
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        float porcentaje = Float.parseFloat(request.getParameter("porcentaje"));
        try{
            if(model.proveedoracpmService.existProveedor(nit)){
                    Proveedor_Anticipo pa = new Proveedor_Anticipo();
                    pa.setNit(nit);
                    pa.setDstrct(usuario.getDstrct());
                    pa.setCreation_user(usuario.getLogin());
                    pa.setCity_code(request.getParameter("ciudad"));
                    pa.setCod_Migracion(request.getParameter("codigo_m").toUpperCase());
                    pa.setPorcentaje(porcentaje);
                    pa.setCodigo(sucursal);
                    try{
                        model.proveedoranticipoService.insertProveedor(pa,usuario.getBase());
                        next=next+"?mensaje=Anticipo Asignado..";
                    }catch (SQLException e){
                                //System.out.println("Error " + e.getMessage()+ " Codigo "+ e.getErrorCode() );
                                sw=1;
                        }
                        if (sw==1){
                                if(model.proveedoranticipoService.existProveedorAnulado(nit,sucursal)){
                                        model.proveedoranticipoService.activarProveedor(pa);
                                        next=next+"?mensaje=Anticipo Asignado..";
                                }
                                else{
                                        next=next+"?mensaje=Ya existe el Proveedor con Anticipo";
                                }
                        }
            }
            else{
                next=next+"?mensaje=Proveedor No Existe...";
            }
            
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
}
