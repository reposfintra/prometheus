/***********************************************
 * Nombre clase: SancionObtenerAction.java
 * Descripci�n: Accion para redireccionar la sancion
 *              para aprobarla.
 * Autor: Jose de la rosa
 * Fecha: 26 de septiembre de 2005, 10:44 AM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 **********************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

/**
 *
 * @author  usuario
 */
public class SancionObtenerAction extends Action{
    
    /** Creates a new instance of SancionObtenerAction */
    public SancionObtenerAction () {
    }
    
    public void run () throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        //Pr�xima vista
        String pag  = "/jsp/cumplidos/sancion/autorizarSancion.jsp";
        String next = "";
        
        String numpla = request.getParameter ("numpla");
        String fecha = request.getParameter ("fecha");
        
        HttpSession session = request.getSession ();
        
        try{
            pag += "?numpla=" + numpla;
            Discrepancia disc = model.discrepanciaService.getDiscrepancia ();
            session.setAttribute ("Discrepancia",  disc);
            next = com.tsp.util.Util.LLamarVentana (pag, "Autorizar Sanci�n Planilla");
            
        }catch (SQLException e){
            throw new ServletException (e.getMessage ());
        }
        
        this.dispatchRequest (next);
    }
    
}
