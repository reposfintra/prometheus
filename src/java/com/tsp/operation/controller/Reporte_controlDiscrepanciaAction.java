/*
 * Reporte_controlDiscrepanciaAction.java
 *
 * Created on 14 de diciembre de 2006, 04:12 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

/**
 *
 * @author  EQUIPO13
 */
public class Reporte_controlDiscrepanciaAction extends Action{
    
    /** Creates a new instance of Reporte_controlDiscrepanciaAction */
    public Reporte_controlDiscrepanciaAction () {
    }
    
    public void run () throws ServletException, InformationException {
        
        String next = "/jsp/general/exportar/ReporteControlDiscrepancia.jsp";
        HttpSession session = request.getSession();
        Usuario usuario       = (Usuario) session.getAttribute("Usuario");
        String distrito       = (String) session.getAttribute ("Distrito");
        String fecha_inicial  = (String) request.getParameter("fechaInicio")!=null?request.getParameter("fechaInicio"):"";
        String fecha_final    = (String) request.getParameter("fechaFinal")!=null?request.getParameter("fechaFinal"):"";
        String tipo           = (String) request.getParameter("tipo")!=null?request.getParameter("tipo"):"";
        String numpla         = (String) request.getParameter("numpla")!=null?request.getParameter("numpla"):"";
        try{
            if (model.cxpDocService.getEnproceso()){
                request.setAttribute ("mensaje","Hay un proceso en ejecucion. Una vez terminado podra realizar la generacion de un nuevo reporte...<br>Para realizar el seguimiento del actual proceso haga clic en el vinculo");
                next += "?ruta=PROCESOS/Seguimiento de Procesos";
            }
            else {
                model.cxpDocService.setEnproceso();
                ReporteControlDiscrepanciaThreads hilo = new ReporteControlDiscrepanciaThreads(model);
                hilo.start( usuario.getLogin(),distrito, fecha_inicial, fecha_final, tipo, numpla );
                request.setAttribute ("mensaje","La generacion del Reporte ha iniciado...<br>Para realizar el seguimiento del proceso haga clic en el vinculo");
                next += "?ruta=PROCESOS/Seguimiento de Procesos";
            }
        }catch (Exception ex){
            throw new ServletException("Error en Reporte_controlDiscrepanciaAction......\n"+ex.getMessage());
        }
        this.dispatchRequest(next); 
        
    }
    
}
