/**
 *
 */
package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;



/**
 * Searches the database for puchase orders matching the
 * purchase search argument
 */
public class PlanViajeSearchAction extends Action {
    static Logger logger = Logger.getLogger(PlanViajeSearchAction.class);
    
    /**
     * Executes the action
     */
    public void run() throws ServletException, InformationException {
        
        String next = "";
        HttpSession session = request.getSession();
        

        // Perform search
        String [] args = new String[2];
        args[0] = request.getParameter("cia");
        args[1] = request.getParameter("planilla");

        if (args[1] != null)
            args[1] = args[1].trim().toUpperCase();
        else
            args[1] = "";

        try {
            Usuario usuario = (Usuario)session.getAttribute("Usuario");
            PlanViaje planVj = (PlanViaje)request.getAttribute("planVj");
            String distrito = (String) session.getAttribute("Distrito");
            
            //************ AMATURANA 21.12.2006 *****************
            
            String dpto = usuario.getDpto();
            
            logger.info("Login: " + usuario.getLogin() + " - Dpto: " + dpto );
            boolean permiso = false;
            
            if( dpto.equals("traf") || model.rmtService.permitirPlanViaje(args[1]) ) {
                logger.info("El usuario: " + usuario.getLogin() + " puede crear/modificar plan de viaje.");
                //***************************************************
                
                if (request.getParameter("tipo")!=null){
                    args[0] = distrito;
                }
                if (model.planViajeService.planViajeExist(args[0], args[1])){
                    //logger.info(" Si existe este plan de viaje " + args[0]);
                    if (model.caravanaService.planViajeExist(args[0], args[1])){
                        //logger.info(" Si existe este en Carabana " + args[0] );
                        model.caravanaService.searchCaravana(args[0], args[1]);
                        model.caravanaService.getPlanViajeCaravana(model.caravanaService.getCodCaravana(), args[1], args[0]);
                        //logger.info(" Se extrajo ls informacion de Caravana " + args[0] );
                    }
                    else
                        model.caravanaService.setCbxCaravana(null);
                    
                    model.planViajeService.regUserQry(args[0], args[1], usuario.getLogin());
                    //CUANDO UN USUARIO DE TRAFICO CONSULTA UN PLAN DE VIAJE ACTUALIZA EL CAMPO LEIDO A 'SI'
                    if ("traf".equals(usuario.getDpto()))
                        model.planViajeService.leer(args[0], args[1]);
                    
                    //EXTRAE LOS USUARIOS QUE HAN CONSULTADO EL PLAN DE VIAJE
                    model.planViajeService.getQryUsers(args[0], args[1]);
                    
                    model.planViajeService.getPlanVjInfoOld(args[0], args[1]);
                    planVj = model.planViajeService.getPlanViaje();
                    model.viaService.getViasRelPg(planVj.getRuta());
                    //VERIICA QUE LA VIA EXISTA EN CASO CONTRARIO DISPARA UN EXCEPCION
                    if (model.viaService.getCbxVias().size() == 0){
                        logger.info("[ER0001] LA VIA '"+planVj.getRuta()+"' ASOCIADA A LA PLANILLA '"+args[1]+"' NO HA SIDO CREADA EN POSGRES");
                        throw new InformationException("[ER0001] LA VIA '"+planVj.getRuta()+"' ASOCIADA A LA PLANILLA '"+args[1]+"' NO HA SIDO CREADA POSGRES");
                    }
                    next = "/planviaje/PantallaModificarPlanViaje.jsp";
                }
                else{
                    if (usuario.getAccesoplanviaje().equals("1")){
                        if (model.planViajeService.planillaExist(args[0], args[1])){
                            model.planViajeService.getPlanVjInfoNew(args[0], args[1]);
                            planVj = model.planViajeService.getPlanViaje();
                            model.viaService.getViasRelPg(planVj.getRuta());
                            //VERIICA QUE LA VIA EXISTA EN CASO CONTRARIO DISPARA UN EXCEPCION
                            if (model.viaService.getCbxVias().size() == 0){
                                logger.info("[ER0000] LA VIA '"+planVj.getRuta()+"' ASOCIADA A LA PLANILLA '"+args[1]+"' NO HA SIDO CREADA");
                                throw new InformationException("[ER0000] LA VIA '"+planVj.getRuta()+"' ASOCIADA A LA PLANILLA '"+args[1]+"' NO HA SIDO CREADA");
                            }
                            next = "/planviaje/PantallaCrearPlanViaje.jsp";
                        }
                        else{
                            request.setAttribute("mensaje", "� LA PLANILLA DIGITADA NO HA SIDO CREADA !");
                            next = "/planviaje/PantallaFiltroPlanViaje.jsp";
                        }
                    }
                    else{
                        request.setAttribute("mensaje", "� EL PLAN DE VIAJE DIGITADO NO HA SIDO CREADO Y NO TIENE LOS PERMISOS PARA CREARLO !");
                        next = "/planviaje/PantallaFiltroPlanViaje.jsp";
                    }
                }
            } else {
                logger.info("El usuario: " + usuario.getLogin() + " NO puede crear/modificar plan de viaje.");
                request.setAttribute("mensaje", "� EL PLAN DE VIAJE NO PUEDE SER CREADO O MODIFICADO. TIENE SEGUIMIENTO EN TRAFICO" +
                        ".<br> SOLO LOS USUARIOS DE TRAFICO PUEDEN HACERLO !");
                        next = "/planviaje/PantallaFiltroPlanViaje.jsp";
            }
            request.setAttribute("planVj", planVj);                  
            logger.info(usuario.getNombre() + " Busc� planViaje con: Planilla " + args[1]);
        }
        catch (SQLException e){
            throw new ServletException(e.getMessage());
        }

        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);    
    }
}