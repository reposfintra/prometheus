/*
 * MenuSeriesAction.java
 *
 * Created on 1 de diciembre de 2004, 03:25 PM
 */

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;
/**
 *
 * @author  mcelin
 */
public class RemesaActualizarAction extends Action {
    
    /** Creates a new instance of MenuSeriesAction */
    public RemesaActualizarAction() {
    }
    
    public void run() throws ServletException, InformationException { 
        try {  
            String Registros = request.getParameter("Registro");            
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            
            
            String[] Datos = Registros.split(",");
            for(int i=0;i<Datos.length;i++) { 
                String[] Datos2 = Datos[i].split("~");
                //if(Util.Imprimir_Remesa(model.RemesaSvc.getRegistros(), Datos2[0], Datos2[1], Usuario))
                    model.RemesaSvc.Actualizar_Remesa_Impresa(Datos2[0]);
            }
            
            model.RemesaSvc.buscaRemesa_no_impresa("",usuario.getLogin(),2);
            
            final String next = "/impresion/remesa.jsp";
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
        }
        catch(Exception e) {
            throw new ServletException("Error en la Accion [RemesaActualizarAction]...\n"+e.getMessage());
        }
     }    
}
