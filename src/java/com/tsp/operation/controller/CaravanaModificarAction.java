/*
 * CaravanaModificarAction.java
 *
 * Created on 04 de Agosto de 2005, 11:00 AM
 */
package com.tsp.operation.controller;
/**
 *
 * @author  Henry
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.Util;
import org.apache.log4j.Logger;

public class CaravanaModificarAction extends Action{    
    
    static Logger logger = Logger.getLogger (CaravanaModificarAction.class);
    
    public void run() throws ServletException {
        int tam = 0;
        String razon = "", razonEsc="";
        boolean swC = false, swE =false;
        String num = (request.getParameter("numC")!=null)?request.getParameter("numC"):"";
        String next = "/jsp/trafico/caravana/modificarCaravana.jsp?caravana="+num+"&first=2&agregar=visible&escolta=visible";
        String opcion = (request.getParameter("opcion")!=null)?request.getParameter("opcion"):"";               
        String origen_car = request.getParameter("origen_caravana")!=null ? request.getParameter("origen_caravana"):"";
        String destino_car = request.getParameter("destino_caravana")!=null ? request.getParameter("destino_caravana"):""; 
        
        try{ 
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
            //creaci�n de la caravana            
            if (opcion.equals("mod")){
                next = "/jsp/trafico/caravana/modificarCaravana.jsp?caravana="+num+"&first=2&agregar=visible&escolta=visible&error=";        
                Vector actual = model.caravanaSVC.getVectorCaravanaActual();
                if (actual.size()==1)
                    next = "/jsp/trafico/caravana/modificarCaravana.jsp?caravana="+num+"&first=2&agregar=visible&escolta=visible&error=V1";        
                    //next = "/jsp/trafico/caravana/caravana.jsp?agregar=visible&escolta=visible&msg=V1";
                else {
                    Vector escActual = model.escoltaVehiculoSVC.getVectorEscoltaCaravanaActual();
                    logger.info("ESCOLTAS CARAVANA: " + escActual.size());
                    Vector datos = model.caravanaSVC.getVectorExcluidos();                
                    Vector esc = model.escoltaVehiculoSVC.getVectorExcluidos();
                    
                    for (int i=0; i<datos.size(); i++){
                        razon = (request.getParameter("razon"+i)!=null)?request.getParameter("razon"+i):"";                    
                        if (razon.equals("")) {
                            next = "/jsp/trafico/caravana/modificarCaravana.jsp?caravana="+num+"&first=2&agregar=visible&escolta=visible&error=ERRORR";        
                            swC = true;
                        } else {
                            model.caravanaSVC.setRazonVectorExcluido(i,razon);
                        }                    
                    }
                    /*
                     * Vector de escoltas excluidos de la caravana
                     */    
                    
                    for (int i=0; i<esc.size(); i++){
                        razonEsc = (request.getParameter("razonesc"+i)!=null)?request.getParameter("razonesc"+i):"";                     
                        if (razonEsc.equals("")) {
                            next = "/jsp/trafico/caravana/modificarCaravana.jsp?caravana="+num+"&first=2&agregar=visible&escolta=visible&error=ERRORE";        
                            swE = true;
                        } else {
                            model.escoltaVehiculoSVC.setRazonVectorExcluido(i,razonEsc);
                        } 
                    }            
                    /*
                     * Se excluyeron vehiculos de la caravana y vehiculos escoltas
                     */
                    if (swC==false){
                        datos = model.caravanaSVC.getVectorExcluidos();
                        for (int i=0; i<datos.size(); i++){
                            VistaCaravana vi = (VistaCaravana) datos.elementAt(i);
                            Caravana c = new Caravana();
                            c.setNumcaravana(Integer.parseInt(num));
                            c.setUser_update(usuario.getLogin());
                            c.setNumpla(vi.getNumpla());
                            c.setRazon_exclusion(vi.getRazon_exclusion());
                            model.caravanaSVC.modificarCaravana(c);
                            // Actualizando el campo caravana a vacio en la tabla ing. trafico
                            model.caravanaSVC.actualizarCaravanaIngresoTrafico(vi.getNumpla(),"");
                            model.escoltaVehiculoSVC.actualizarEscoltaIngresoTrafico(vi.getNumpla(),"");
                            next = "/jsp/trafico/caravana/modificarCaravana.jsp?caravana="+num+"&first=2&agregar=visible&escolta=visible&error=NO";        
                        }
                        tam = datos.size();
                        if (tam>1){
                            for (int i=0; i<datos.size(); i++){
                                VistaCaravana vi = (VistaCaravana) datos.elementAt(i);
                                model.caravanaSVC.limpiarVectorActualPlanillas();
                                model.caravanaSVC.agregarVectorCaravanaActual(vi);
                                next+="&exclu=MA1";                               
                            }                        
                            model.caravanaSVC.limpiarVectorPlanillasExcluidas();
                        }
                    }
                    if (swE==false){
                        esc = model.escoltaVehiculoSVC.getVectorExcluidos();
                        for (int i=0; i<esc.size(); i++){
                            Escolta es = (Escolta) esc.elementAt(i);                                                
                            es.setUser_update(usuario.getLogin());  
                            /*es.setOrigen(origen);
                            es.setDestino(destino);*/
                            //Modificacion de Escoltas.
                            model.escoltaVehiculoSVC.modificarEscoltaVehiculoCaravana(es);
                            model.escoltaVehiculoSVC.liberarEscoltasCaravana(es.getPlaca(),Integer.parseInt(num));  //modificado 31 agosto
                            /*obteniendo las planillas asignadas a un Escolta*/
                            Vector plEscoltas = model.escoltaVehiculoSVC.getPlanillasEscolta(es.getPlaca());
                            for (int q=0; q<plEscoltas.size(); q++){
                                String planilla = (String) plEscoltas.elementAt(q);
                                model.escoltaVehiculoSVC.actualizarEscoltaIngresoTrafico(planilla,"");
                            }
                            next = "/jsp/trafico/caravana/modificarCaravana.jsp?caravana="+num+"&first=2&agregar=visible&escolta=visible&error=NOE";        
                        }   
                        if(tam>1)
                            next+="&exclu=MA1";                               
                        model.escoltaVehiculoSVC.limpiarVectorExcluidos();
                    }
                    //verificando si se agregaron nuevos vehiculos a la caravana                
                    for (int i=0; i<actual.size(); i++) {
                        VistaCaravana va = (VistaCaravana) actual.elementAt(i);
                        if (va.isSeleccionado()) {
                            Caravana c = new Caravana();
                            c.setNumcaravana(Integer.parseInt(num));
                            c.setDstrct(usuario.getDstrct());
                            c.setAgencia(usuario.getId_agencia());
                            c.setBase(usuario.getBase());
                            c.setCreation_user(usuario.getLogin());
                            c.setFecinicio(Util.getFechaActual_String(4));  
                            c.setNumpla(va.getNumpla());
                            c.setNumrem(va.getNumrem());
                            model.caravanaSVC.insertarCaravana(c);
                            model.caravanaSVC.actualizarCaravanaIngresoTrafico(va.getNumpla(),"S");
                            next = "/jsp/trafico/caravana/modificarCaravana.jsp?caravana="+num+"&first=2&agregar=visible&escolta=visible&error=NOE";        
                        }
                    }
                    //Verificando si se agregaron nuevos escoltas a la caravana
                    Vector vecPlani = new Vector();
                    String origen  = "";
                    String destino = "";
                    for (int i=0; i<actual.size(); i++) {
                        VistaCaravana vi = (VistaCaravana)actual.elementAt(i);
                        vecPlani.addElement(vi.getNumpla());                        
                    }
                    EscoltaVehiculo e = new EscoltaVehiculo();            
                    e.setBase(usuario.getBase());
                    e.setCreation_user(usuario.getLogin());
                    e.setOrigen(origen);
                    e.setDestino(destino);
                    e.setFecasignacion(Util.getFechaActual_String(4));    
                    logger.info("Escoltas Asignados a la caravana: " + escActual.size());
                    for (int i=0; i<escActual.size(); i++) {
                        Escolta es = (Escolta) escActual.elementAt(i);                                            
                        /* Actualizacion de Origen y detino */                        
                        origen  = request.getParameter("origen"+i)!=null ? request.getParameter("origen"+i):"";
                        destino = request.getParameter("destino"+i)!=null ? request.getParameter("destino"+i):"";
                        es.setOrigen(origen);
                        es.setDestino(destino);
                        logger.info("Escoltas Seleccion?: " + es.isSeleccionado());
                        model.escoltaVehiculoSVC.updateOriDestEscoltaCaravana(es);
                        if (es.isSeleccionado()){
                            e.setPlaca(es.getPlaca());
                            e.setOrigen(origen);
                            e.setDestino(destino);
                            logger.info("*ESCOLTA CARAVANA: " + e.getNumcaravana());
                            e.setNumcaravana(Integer.parseInt(num));
                            logger.info("**ESCOLTA CARAVANA: " + e.getNumcaravana());
                            model.escoltaVehiculoSVC.insertarEscoltaCaravana(e);//AMATURANA
                            for (int j=0; j<vecPlani.size(); j++){
                                String plan = ""+vecPlani.elementAt(j);
                                e.setNumpla(plan);                             
                                //model.escoltaVehiculoSVC.insertarEscoltaCaravana(e);
                                model.escoltaVehiculoSVC.actualizarEscoltaIngresoTrafico(plan,"S");
                            }
                        } else {
                            e.setPlaca(es.getPlaca());
                            e.setOrigen(origen);
                            e.setDestino(destino);
                            e.setNumcaravana(Integer.parseInt(num));
                            for (int j=0; j<actual.size(); j++){
                                VistaCaravana va = (VistaCaravana) actual.elementAt(j);
                                if (va.isSeleccionado()) {                                
                                    String plan = va.getNumpla();                                   
                                    e.setNumpla(plan);
                                    //model.escoltaVehiculoSVC.insertarEscoltaCaravana(e);
                                    model.escoltaVehiculoSVC.actualizarEscoltaIngresoTrafico(plan,"S");
                                }
                            }
                        }
                        next = "/jsp/trafico/caravana/modificarCaravana.jsp?caravana="+num+"&first=mod&agregar=visible&escolta=visible&error=NOE";        
                    }
                }
                int numEscoltas = model.escoltaVehiculoSVC.searchEscoltasCaravana(Integer.parseInt(num));
                if (numEscoltas==0) {
                    Vector vecPlanillas = model.caravanaSVC.getVectorPlanillasCaravana(Integer.parseInt(num));
                    for (int i = 0; i<vecPlanillas.size(); i++) {
                        String plani = (String) vecPlanillas.elementAt(i);
                        model.escoltaVehiculoSVC.actualizarEscoltaIngresoTrafico(plani, "");
                        //model.escoltaVehiculoSVC.liberarEscoltasCaravana(Integer.parseInt(num));
                    }
                }
                
            } else if (opcion.equals("fin")){
                /*Obteniendo escoltas de la caravana*/
                model.escoltaVehiculoSVC.searchEscoltasCaravana(Integer.parseInt(num));
                Vector placas = model.escoltaVehiculoSVC.getPlacasEscolta();
                /*Actualizando ingreso trafico*/
                Vector vecPlanillas = model.caravanaSVC.getVectorPlanillasCaravana(Integer.parseInt(num));
                for (int i = 0; i<vecPlanillas.size(); i++) {
                    String plani = (String) vecPlanillas.elementAt(i);
                    model.escoltaVehiculoSVC.actualizarEscoltaIngresoTrafico(plani, "");                   
                    model.caravanaSVC.actualizarCaravanaIngresoTrafico(plani,"");
                    model.escoltaVehiculoSVC.actualizarEscoltaIngresoTrafico(plani,"");
                }
                /*Liberando escoltas de la caravana*/
                for(int j=0; j<placas.size(); j++){
                    String placa = (String) placas.elementAt(j);
                    model.escoltaVehiculoSVC.liberarEscoltasCaravana(placa,Integer.parseInt(num));
                }
                /*Finalizando la caravana*/
                model.caravanaSVC.finalizarCaravana(Integer.parseInt(num));
                
                next = "/jsp/trafico/caravana/modificarCaravana.jsp?caravana="+num+"&first=2&agregar=visible&escolta=visible&error=NONE";
            }         
            //Actualizando vector de escoltas
            model.escoltaVehiculoSVC.getVectorEscoltasXCaravana(num);
            this.dispatchRequest(next);
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());                       
        }                
    }
    
}
