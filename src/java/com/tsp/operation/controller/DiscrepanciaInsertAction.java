/***********************************************
 * Nombre clase: DiscrepanciaInsertAction.java
 * Descripci�n: Accion para ingresar una discrepancia a la bd.
 * Autor: Jose de la rosa
 * Fecha: 23 de septiembre de 2005, 02:52 PM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 **********************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class DiscrepanciaInsertAction extends Action {
    
    /** Creates a new instance of DiscrepanciaInsertAction */
    public DiscrepanciaInsertAction () {
    }
    
    public void run () throws ServletException, InformationException {
        HttpSession session = request.getSession ();
        String numpla = request.getParameter ("c_numpla").toUpperCase ();
        String nom_doc = request.getParameter ("nom_doc");
        String numrem = request.getParameter ("c_numrem").toUpperCase ();
        String tipo_doc = request.getParameter ("c_tipo_doc").toUpperCase ();
        String documento = request.getParameter ("c_documento").toUpperCase ();
        String tipo_doc_rel = (String) request.getParameter ("c_tipo_doc_rel");
        String documento_rel = (String) request.getParameter ("c_documento_rel");
        String fecha_devolucion = request.getParameter ("c_fecha_devolucion");
        String nro_rechazo = (request.getParameter ("c_nro_rechazo")!=null)?request.getParameter ("c_nro_rechazo"):"";
        String retencion = request.getParameter ("c_retencion");
        String reportado = (request.getParameter ("c_reportado")!=null)?request.getParameter ("c_reportado"):"";
        String contacto = (request.getParameter ("c_contacto")!=null)?request.getParameter ("c_contacto"):"";
        String ubicacion = (request.getParameter ("c_ubicacion")!=null)?request.getParameter ("c_ubicacion"):"";
        String observacion = request.getParameter ("c_observacion");
        String clientes = request.getParameter ("client");
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String distrito = (String) session.getAttribute ("Distrito");
        String next = "";
        int sw=0;
        try {
            Discrepancia dis = new Discrepancia ();
            dis.setNro_planilla (numpla);
            dis.setNro_remesa (numrem);
            dis.setTipo_doc_rel (tipo_doc_rel);
            dis.setDocumento_rel (documento_rel);
            dis.setTipo_doc (tipo_doc);
            dis.setDocumento (documento);
            dis.setFecha_devolucion (fecha_devolucion);
            dis.setNumero_rechazo (nro_rechazo);
            dis.setReportado (reportado);
            dis.setObservacion (observacion);
            dis.setContacto (contacto);
            dis.setUbicacion (ubicacion);
            dis.setRetencion (retencion);
            dis.setUsuario_modificacion (usuario.getLogin ());
            dis.setUsuario_creacion (usuario.getLogin ());
            dis.setBase (usuario.getBase ());
            dis.setDistrito (distrito);
            dis.setNro_discrepancia ( model.discrepanciaService.numeroDiscrepancia() );
            dis.setFecha_cierre ("0099-01-01 00:00:00");
            model.discrepanciaService.setDiscrepancia (dis);
            try {
                request.setAttribute ("msg", "Discrepancia Agregada");
                model.discrepanciaService.setCabDiscrepancia ( dis );
                model.responsableService.listResponsable();
                next="/jsp/cumplidos/discrepancia/DiscrepanciaInsertar.jsp?c_numrem="+numrem+"&sw=true&exis=true&campos=true&c_numpla"+numpla+"&c_tipo_doc="+tipo_doc+"&c_documento="+documento+"&paso=false&c_tipo_doc_rel="+tipo_doc_rel+"&c_documento_rel="+documento_rel+"&nom_doc="+nom_doc+"&client="+clientes;
            }
            catch (SQLException e) {
                sw=1;
            }
            if ( sw == 1 ) {
                if ( !model.discrepanciaService.existeDiscrepancia (numpla, numrem, tipo_doc, documento, tipo_doc_rel, documento_rel,distrito ) ) {
                    model.discrepanciaService.setCabDiscrepancia ( dis );
                    request.setAttribute ("msg", "Discrepancia Agregada");
                    next="/jsp/cumplidos/discrepancia/DiscrepanciaInsertar.jsp?c_numrem="+numrem+"&sw=true&exis=true&campos=true&c_numpla"+numpla+"&c_tipo_doc="+tipo_doc+"&c_documento="+documento+"&paso=false&nom_doc="+nom_doc+"&c_tipo_doc_rel="+tipo_doc_rel+"&c_documento_rel="+documento_rel+"&client="+clientes;
                }
                else {
                    next = "/jsp/cumplidos/discrepancia/DiscrepanciaInsertar.jsp?c_numpla=" + numpla +"&c_numrem="+ numrem +"&c_tipo_doc="+ tipo_doc +"&c_documento="+ documento +"&sw=true&exis=false&campos=true&mensaje='Error En El Inggreso'&nom_doc="+nom_doc+"&c_tipo_doc_rel="+tipo_doc_rel+"&c_documento_rel="+documento_rel+"&client="+clientes;
                    request.setAttribute ("msg", "Error En El Ingreso");
                }
            }
            model.tablaGenService.buscarGrupo (clientes);
        }catch (SQLException e) {
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
        
    }
}
