/*************************************************************************
 * Nombre ......................InfoactAnularAction.java                 *
 * Descripci�n..................Clase Action para anular info actividad  *
 * Autor........................Ing. Diogenes Antonio Bastidas Morales   *
 * Fecha........................3 de septiembre de 2005, 04:10 PM        * 
 * Versi�n......................1.0                                      * 
 * Coyright.....................Transportes Sanchez Polo S.A.            *
 *************************************************************************/ 
 
package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Diogenes
 */
public class InfoactAnularAction extends Action{
    
    /** Creates a new instance of InfoactAnularAction */
    public InfoactAnularAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina");
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
        
        
        Date fecha = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String now = format.format(fecha);

        InfoActividad infact = new InfoActividad();
        infact.setCodCliente( request.getParameter("cliente") );
        infact.setCodActividad(request.getParameter("acti"));
        infact.setNumrem(request.getParameter("numrem"));
        infact.setDstrct(usuario.getCia());
        infact.setUser_update(usuario.getLogin());        
        infact.setLast_update(now);
        
        try{
            
            
            model.actividadSvc.buscarActividad(request.getParameter("acti"), usuario.getCia());            
            model.infoactService.anularInfoActividad(infact);  
            boolean a = model.infoactService.buscarInfoActividad(request.getParameter("acti"), request.getParameter("cliente") , usuario.getCia(), request.getParameter("numpla"), request.getParameter("numrem"));
            next=next+"?act="+a+"&men=Anulado";



        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
