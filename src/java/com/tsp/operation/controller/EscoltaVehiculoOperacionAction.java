/*******************************************************************
 * Nombre clase: EscoltaVehiculoOperacionAction.java
 * Descripci�n: Accion para buscar un escolta vehiculo.
 * Autor: Ing. Jose de la rosa
 * Fecha: 30 de enero de 2007, 11:33 AM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

public class EscoltaVehiculoOperacionAction extends Action{
    
    private PrintWriter pw;
    private LogWriter   logTrans;
    private String path;
    
    /** Creates a new instance of EscoltaVehiculoOperacionAction */
    public EscoltaVehiculoOperacionAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "/jsp/trafico/escolta_vehiculo/DatosEscoltaVehiculo.jsp";
        HttpSession session     = request.getSession();
        Usuario usuario		= (Usuario) session.getAttribute("Usuario");
        String dstrct		= (String) session.getAttribute("Distrito");
        String opcion           = (String) request.getParameter("opcion");
        try{
            if(opcion.equals("1")){
                Vector vec = model.escoltaVehiculoSVC.getVectorEscoltaCaravanaActual();
                String []textbox = request.getParameterValues("checkbox");
                Vector consulta         = new Vector();
                boolean sw              = true;
                String fac_faltantes    = "";
                if(textbox!=null){
                    
                    for (int j=0; j < textbox.length; j++){
                        int i = Integer.parseInt( textbox[j] );
                        String d_o              = request.getParameter("d_o"+i)!=null?request.getParameter("d_o"+i):"";
                        String factura          = request.getParameter("factura"+i)!=null?request.getParameter("factura"+i):"";
                        double tarifa           = Double.parseDouble   ( request.getParameter("tarifa"+i)!=null?!request.getParameter("tarifa"+i).equals("")?request.getParameter("tarifa"+i).replaceAll (",",""):"0":"0" );
                        
                        EscoltaVehiculo es = (EscoltaVehiculo)vec.get(i);
                        es.setD_o(d_o);
                        es.setFactura(factura);
                        es.setTarifa(tarifa);
                        es.setUsuario_asignacion_factura(usuario.getLogin());
                        if( !model.cxpDocService.ExisteCXP_Doc( dstrct, model.tablaGenService.buscarCampoTablagenDato( (String)session.getAttribute("tablaProv"), (String) session.getAttribute("proveedor"), "1").toString().trim(), "010", factura ) && sw){
                            consulta.add( model.escoltaVehiculoSVC.updateEscoltaVehiculo( es ) );
                        }
                        else{
                            fac_faltantes += factura+", ";
                            sw = false;
                        }
                    }
                    if(sw){
                        model.despachoService.insertar(consulta);
                        next += "?msg=Los datos se ingresaron exitosamente";
                        model.escoltaVehiculoSVC.listadoEscoltaSinGeneracion( model.tablaGenService.buscarCampoTablagenDato( (String)session.getAttribute("tablaProv"), (String) session.getAttribute("proveedor"), "1").toString().trim(), dstrct );
                    }
                    else{
                        next += "?msg=Las facturas "+fac_faltantes+" ya existen en la BD";
                    }
                }
            }
            else if(opcion.equals("2")){
                
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                String ruta = rb.getString("ruta");
                path = ruta + "/exportar/migracion/" + usuario.getLogin();
                
                File archivo = new File( path );
                if (!archivo.exists()) archivo.mkdirs();
                
                initLog();
                
                next = "/jsp/trafico/escolta_vehiculo/GeneracionEscoltaVehiculo.jsp";
                String []textbox = request.getParameterValues("checkbox");
                boolean existe_tasa = false;
                double valor_tasa = 0, valor = 0;
                Vector consulta = new Vector();
                boolean terminacion = true;
                
                for (int j=0; j < textbox.length; j++){
                    existe_tasa = false;
                    
                    Vector vec = model.escoltaVehiculoSVC.getVectorEscoltaCaravanaActual();
                    
                    String nitpro = model.tablaGenService.buscarCampoTablagenDato( (String)session.getAttribute("tablaProv"), (String)session.getAttribute("proveedor"), "1").toString().trim();
                    
                    String auxiliar = model.tablaGenService.buscarCampoTablagenDato( (String)session.getAttribute("tablaProv"), (String)session.getAttribute("proveedor"), "1").toString().trim();
                    
                    String iva = model.tablaGenService.buscarCampoTablagenDato( (String)session.getAttribute("tablaProv"), (String)session.getAttribute("proveedor"), "2").toString().trim();
                    String riva = model.tablaGenService.buscarCampoTablagenDato( (String)session.getAttribute("tablaProv"), (String)session.getAttribute("proveedor"), "3").toString().trim();
                    String rica = model.tablaGenService.buscarCampoTablagenDato( (String)session.getAttribute("tablaProv"), (String)session.getAttribute("proveedor"), "4").toString().trim();
                    String rfte = model.tablaGenService.buscarCampoTablagenDato( (String)session.getAttribute("tablaProv"), (String)session.getAttribute("proveedor"), "5").toString().trim();
                    String fecha = Util.getFechaActual_String(4);
                    if( !model.cxpDocService.ExisteCXP_Doc( dstrct, nitpro, "010", textbox[j]) ){
                        Proveedor prov = model.proveedorService.obtenerProveedor( nitpro, dstrct);
                        if(prov!=null){
                            
                            Banco ban = model.servicioBanco.obtenerBanco(dstrct, prov.getC_branch_code(), prov.getC_bank_account());
                            
                            if( ban!=null ){
                                
                                String moneda_local = model.ingreso_detalleService.monedaLocal(dstrct);
                                
                                CXP_Doc doc = new CXP_Doc();
                                doc.setDstrct( dstrct );
                                doc.setProveedor( nitpro );
                                doc.setTipo_documento( "010" );
                                doc.setDocumento( textbox[j] );
                                doc.setDescripcion( "Acompa�amiento Iverloset" );
                                doc.setHandle_code( prov.getHandle_code() );
                                doc.setId_mims( prov.getC_idMims() );
                                doc.setFecha_documento(fecha);
                                doc.setTipo_documento_rel("");
                                doc.setDocumento_relacionado("");
                                doc.setFecha_aprobacion(fecha);
                                doc.setAprobador( "ADMIN" );
                                doc.setUsuario_aprobacion( "ADMIN" );
                                doc.setFecha_vencimiento(Util.fechaFinalYYYYMMDD( Util.getFechaActual_String(4), prov.getPlazo() ) );
                                doc.setUltima_fecha_pago( "0099-01-01" );
                                doc.setBanco( prov.getC_branch_code() );
                                doc.setSucursal( prov.getC_bank_account() );
                                doc.setMoneda( ban.getMoneda() );
                                valor_tasa = 0;
                                try{
                                    //moneda local, moneda origen, moneda destino
                                    Tasa t = model.tasaService.buscarValorTasa( moneda_local, moneda_local, ban.getMoneda(), Util.getFechaActual_String(4) );
                                    if(t!=null){
                                        valor_tasa = Double.parseDouble(  String.valueOf(t.getValor_tasa()).replaceAll(",","") );
                                    }
                                    else{
                                        existe_tasa = true;
                                    }
                                }catch(Exception ex){
                                    existe_tasa = true;
                                }
                                
                                //los abonos estan abajo
                                doc.setTasa( valor_tasa );
                                doc.setUsuario_contabilizo("");
                                doc.setFecha_contabilizacion( "0099-01-01 00:00:00" );
                                doc.setUsuario_anulo("");
                                doc.setFecha_anulacion( "0099-01-01 00:00:00" );
                                doc.setFecha_contabilizacion_anulacion("0099-01-01 00:00:00");
                                doc.setObservacion("");
                                doc.setNum_obs_autorizador(0);
                                doc.setNum_obs_pagador(0);
                                doc.setNum_obs_registra(0);
                                doc.setCreation_user( "" );
                                doc.setUser_update( usuario.getLogin() );
                                doc.setBase("");
                                doc.setMoneda_banco( ban.getMoneda() );
                                
                                int k = 0, s = 0;
                                String itm = "";
                                Vector vecItem = new Vector();
                                boolean impue = false;
                                double total_iva = 0, total_riva = 0, total_rica = 0, total_rfte = 0;
                                double total_iva_me = 0, total_riva_me = 0, total_rica_me = 0, total_rfte_me = 0;
                                double val_item = 0, val_item_me = 0;
                                
                                //inicio de los items
                                for (int i = 0; i < vec.size(); i++){
                                    EscoltaVehiculo es = (EscoltaVehiculo) vec.get(i);
                                    if(es.getFactura().equals( textbox[j] ) ){
                                        
                                        CXPItemDoc item = new CXPItemDoc();
                                        item.setDstrct( dstrct );
                                        item.setProveedor( nitpro );
                                        item.setTipo_documento( "010" );
                                        item.setDocumento( textbox[j] );
                                        k++;
                                        if( k < 10 )
                                            itm = "00"+k;
                                        else if ( k > 9 && k < 100 )
                                            itm = "0"+k;
                                        else
                                            itm = ""+k;
                                        item.setItem( itm );
                                        item.setDescripcion( es.getD_o()+" "+es.getPlaca()+" "+es.getOrigen()+"-"+es.getDestino()+" "+es.getNumpla() );
                                        val_item += ( ban.getMoneda().equals("DOL") )?Util.roundByDecimal( es.getTarifa() ,2):(int)Math.round( es.getTarifa() );
                                        item.setVlr( es.getTarifa() );
                                        val_item_me += ( ban.getMoneda().equals("DOL") )?Util.roundByDecimal( es.getTarifa() * valor_tasa ,2):(int)Math.round( es.getTarifa() * valor_tasa );
                                        item.setVlr_me( es.getTarifa() * valor_tasa );
                                        
                                        String cuen = model.plremSvc.account_code_c( es.getNumpla() );
                                        if( cuen.equals("") ){
                                            existe_tasa = true;
                                            logTrans.log("No se puede agregar la factura "+textbox[j]+" por que no existe la cuenta contable en la planilla "+es.getNumpla(), logTrans.INFO);
                                        }
                                        item.setCodigo_cuenta( cuen );
                                        item.setCodigo_abc( "DE25" );
                                        item.setPlanilla( es.getNumpla() );
                                        item.setUser_update( "" );
                                        item.setCreation_user( usuario.getLogin() );
                                        item.setBase( "" );
                                        item.setCodcliarea( "" );
                                        item.setTipcliarea( "" );
                                        item.setConcepto( "" );
                                        item.setAuxiliar( nitpro );
                                        item.setTipoSubledger("");
                                        
                                        //items de impuestos
                                        Vector vImpDoc = new Vector();
                                        CXPImpItem impItem = new CXPImpItem();
                                        impItem.setDstrct( dstrct );
                                        impItem.setProveedor( nitpro );
                                        impItem.setTipo_documento( "010" );
                                        impItem.setDocumento( textbox[j] );
                                        impItem.setCreation_user( "" );
                                        impItem.setUser_update( usuario.getLogin() );
                                        impItem.setBase( "" );
                                        if( !iva.equals("") ){
                                            Tipo_impuesto ti = model.TimpuestoSvc.buscarImpuestoPorCodigos( "IVA", iva, dstrct, ban.getCodigo_Agencia() );
                                            if( ti != null ){
                                                impue = true;
                                                s++;
                                                if( s < 10 )
                                                    itm = "00"+s;
                                                else if ( s > 9 && s < 100 )
                                                    itm = "0"+s;
                                                else
                                                    itm = ""+s;
                                                impItem.setItem( itm );
                                                impItem.setCod_impuesto( iva );
                                                impItem.setPorcent_impuesto( ti.getPorcentaje1() );
                                                
                                                total_iva += ( ban.getMoneda().equals("DOL") )?Util.roundByDecimal( ( es.getTarifa() * ti.getPorcentaje1() * ti.getInd_signo() )/100 ,2):(int)Math.round( ( es.getTarifa() * ti.getPorcentaje1() * ti.getInd_signo() )/100 );
                                                impItem.setVlr_total_impuesto( ( ban.getMoneda().equals("DOL") )?Util.roundByDecimal( ( es.getTarifa() * ti.getPorcentaje1() * ti.getInd_signo() )/100 ,2):(int)Math.round( ( es.getTarifa() * ti.getPorcentaje1() * ti.getInd_signo() )/100 ) );
                                                
                                                total_iva_me += ( ban.getMoneda().equals("DOL") )?Util.roundByDecimal( ( ( es.getTarifa() * ti.getPorcentaje1() * ti.getInd_signo() ) * valor_tasa ) / 100 ,2):(int)Math.round( ( ( es.getTarifa() * ti.getPorcentaje1() * ti.getInd_signo() ) * valor_tasa ) / 100 );
                                                impItem.setVlr_total_impuesto_me( ( ( es.getTarifa() * ti.getPorcentaje1() * ti.getInd_signo() ) * valor_tasa ) / 100  );
                                                vImpDoc.add(impItem);
                                            }
                                            else{
                                                logTrans.log("No se puede agregar la factura "+textbox[j]+"por que el impuesto de IVA con codigo "+iva+" no existe", logTrans.INFO);
                                                existe_tasa = true;
                                            }
                                        }
                                        else if( !riva.equals("") ){
                                            Tipo_impuesto ti = model.TimpuestoSvc.buscarImpuestoPorCodigos( "RIVA", riva, dstrct, ban.getCodigo_Agencia() );
                                            if( ti != null ){
                                                impue = true;
                                                s++;
                                                if( s < 10 )
                                                    itm = "00"+s;
                                                else if ( s > 9 && s < 100 )
                                                    itm = "0"+s;
                                                else
                                                    itm = ""+s;
                                                impItem.setItem( itm );
                                                impItem.setCod_impuesto( riva );
                                                impItem.setPorcent_impuesto( ti.getPorcentaje1() );
                                                
                                                total_riva += ( ban.getMoneda().equals("DOL") )?Util.roundByDecimal( ( es.getTarifa() * ti.getPorcentaje1() * ti.getInd_signo() )/100 ,2):(int)Math.round( ( es.getTarifa() * ti.getPorcentaje1() * ti.getInd_signo() )/100 );
                                                impItem.setVlr_total_impuesto( ( ban.getMoneda().equals("DOL") )?Util.roundByDecimal( ( es.getTarifa() * ti.getPorcentaje1() * ti.getInd_signo() )/100 ,2):(int)Math.round( ( es.getTarifa() * ti.getPorcentaje1() * ti.getInd_signo() )/100 ) );
                                                
                                                total_riva_me += ( ban.getMoneda().equals("DOL") )?Util.roundByDecimal( ( ( es.getTarifa() * ti.getPorcentaje1() * ti.getInd_signo() ) * valor_tasa ) / 100 ,2):(int)Math.round( ( ( es.getTarifa() * ti.getPorcentaje1() * ti.getInd_signo() ) * valor_tasa ) / 100 );
                                                impItem.setVlr_total_impuesto_me( ( ( es.getTarifa() * ti.getPorcentaje1() * ti.getInd_signo() ) * valor_tasa ) / 100  );
                                                vImpDoc.add(impItem);
                                            }
                                            else{
                                                logTrans.log("No se puede agregar la factura "+textbox[j]+"por que el impuesto de RIVA con codigo "+riva+" no existe", logTrans.INFO);
                                                existe_tasa = true;
                                            }
                                        }
                                        else if( !rica.equals("") ){
                                            Tipo_impuesto ti = model.TimpuestoSvc.buscarImpuestoPorCodigos( "RICA", rica, dstrct, ban.getCodigo_Agencia() );
                                            if( ti != null ){
                                                impue = true;
                                                s++;
                                                if( s < 10 )
                                                    itm = "00"+s;
                                                else if ( s > 9 && s < 100 )
                                                    itm = "0"+s;
                                                else
                                                    itm = ""+s;
                                                impItem.setItem( itm );
                                                impItem.setCod_impuesto( rica );
                                                impItem.setPorcent_impuesto( ti.getPorcentaje1() );
                                                
                                                total_rica += ( ban.getMoneda().equals("DOL") )?Util.roundByDecimal( ( es.getTarifa() * ti.getPorcentaje1() * ti.getInd_signo() )/100 ,2):(int)Math.round( ( es.getTarifa() * ti.getPorcentaje1() * ti.getInd_signo() )/100 );
                                                impItem.setVlr_total_impuesto( ( ban.getMoneda().equals("DOL") )?Util.roundByDecimal( ( es.getTarifa() * ti.getPorcentaje1() * ti.getInd_signo() )/100 ,2):(int)Math.round( ( es.getTarifa() * ti.getPorcentaje1() * ti.getInd_signo() )/100 ) );
                                                
                                                total_rica_me += ( ban.getMoneda().equals("DOL") )?Util.roundByDecimal( ( ( es.getTarifa() * ti.getPorcentaje1() * ti.getInd_signo() ) * valor_tasa ) / 100 ,2):(int)Math.round( ( ( es.getTarifa() * ti.getPorcentaje1() * ti.getInd_signo() ) * valor_tasa ) / 100 );
                                                impItem.setVlr_total_impuesto_me( ( ( es.getTarifa() * ti.getPorcentaje1() * ti.getInd_signo() ) * valor_tasa ) / 100  );
                                                vImpDoc.add(impItem);
                                            }
                                            else{
                                                logTrans.log("No se puede agregar la factura "+textbox[j]+"por que el impuesto de RICA con codigo "+rica+" no existe", logTrans.INFO);
                                                existe_tasa = true;
                                            }
                                        }
                                        else if( !rfte.equals("") ){
                                            Tipo_impuesto ti = model.TimpuestoSvc.buscarImpuestoPorCodigos( "RFTE", rfte, dstrct, ban.getCodigo_Agencia() );
                                            if( ti != null ){
                                                s++;
                                                if( s < 10 )
                                                    itm = "00"+s;
                                                else if ( s > 9 && s < 100 )
                                                    itm = "0"+s;
                                                else
                                                    itm = ""+s;
                                                impItem.setItem( itm );
                                                impue = true;
                                                impItem.setCod_impuesto( rfte );
                                                impItem.setPorcent_impuesto( ti.getPorcentaje1() );

                                                total_rfte += ( ban.getMoneda().equals("DOL") )?Util.roundByDecimal( ( es.getTarifa() * ti.getPorcentaje1() * ti.getInd_signo() )/100 ,2):(int)Math.round( ( es.getTarifa() * ti.getPorcentaje1() * ti.getInd_signo() )/100 );
                                                impItem.setVlr_total_impuesto( ( ban.getMoneda().equals("DOL") )?Util.roundByDecimal( ( es.getTarifa() * ti.getPorcentaje1() * ti.getInd_signo() )/100 ,2):(int)Math.round( ( es.getTarifa() * ti.getPorcentaje1() * ti.getInd_signo() )/100 ) );
                                                
                                                total_rfte_me += ( ban.getMoneda().equals("DOL") )?Util.roundByDecimal( ( ( es.getTarifa() * ti.getPorcentaje1() * ti.getInd_signo() ) * valor_tasa ) / 100 ,2):(int)Math.round( ( ( es.getTarifa() * ti.getPorcentaje1() * ti.getInd_signo() ) * valor_tasa ) / 100 );
                                                impItem.setVlr_total_impuesto_me( ( ( es.getTarifa() * ti.getPorcentaje1() * ti.getInd_signo() ) * valor_tasa ) / 100  );
                                                vImpDoc.add(impItem);
                                            }
                                            else{
                                                logTrans.log("No se puede agregar la factura "+textbox[j]+"por que el impuesto de RFTE con codigo "+rfte+" no existe", logTrans.INFO);
                                                existe_tasa = true;
                                            }
                                        }
                                        item.setVItems( vImpDoc );//vector de items de impuestos
                                        vecItem.add(item);//vector de items
                                        if( !existe_tasa )
                                            consulta.add( model.escoltaVehiculoSVC.updateGeneracionEscoltaVehiculo(usuario.getLogin(), es.getRegstatus(), es.getNumpla(), es.getPlaca()));
                                    }
                                }
                                //si existen items de impuesto se agrega la cabecera
                                Vector vImpuestosDoc = new Vector();
                                if( impue ){
                                    CXPImpDoc impDoc = new CXPImpDoc();
                                    impDoc.setDstrct( dstrct );
                                    impDoc.setProveedor( nitpro );
                                    impDoc.setTipo_documento( "010" );
                                    impDoc.setDocumento( textbox[j] );
                                    impDoc.setCreation_user( "" );
                                    impDoc.setUser_update( usuario.getLogin() );
                                    impDoc.setBase( "" );
                                    if( !iva.equals("") ){
                                        Tipo_impuesto ti = model.TimpuestoSvc.buscarImpuestoPorCodigos( "IVA", iva, dstrct, ban.getCodigo_Agencia() );
                                        if( ti != null ){
                                            impDoc.setCod_impuesto( iva );
                                            impDoc.setPorcent_impuesto( ti.getPorcentaje1() );
                                            impDoc.setVlr_total_impuesto( total_iva );
                                            impDoc.setVlr_total_impuesto_me( total_iva_me );
                                        }
                                        else
                                            logTrans.log("No se puede agregar la factura "+textbox[j]+"por que el impuesto de IVA con codigo "+iva+" no existe", logTrans.INFO);
                                    }
                                    else if( !riva.equals("") ){
                                        Tipo_impuesto ti = model.TimpuestoSvc.buscarImpuestoPorCodigos( "RIVA", riva, dstrct, ban.getCodigo_Agencia() );
                                        if( ti != null ){
                                            impDoc.setCod_impuesto( riva );
                                            impDoc.setPorcent_impuesto( ti.getPorcentaje1() );
                                            impDoc.setVlr_total_impuesto( total_riva );
                                            impDoc.setVlr_total_impuesto_me( total_riva_me );
                                        }
                                        else
                                            logTrans.log("No se puede agregar la factura "+textbox[j]+"por que el impuesto de RIVA con codigo "+riva+" no existe", logTrans.INFO);
                                    }
                                    else if( !rica.equals("") ){
                                        Tipo_impuesto ti = model.TimpuestoSvc.buscarImpuestoPorCodigos( "RICA", rica, dstrct, ban.getCodigo_Agencia() );
                                        if( ti != null ){
                                            impDoc.setCod_impuesto( rica );
                                            impDoc.setPorcent_impuesto( ti.getPorcentaje1() );
                                            impDoc.setVlr_total_impuesto( total_rica );
                                            impDoc.setVlr_total_impuesto_me( total_rica_me );
                                        }
                                        else
                                            logTrans.log("No se puede agregar la factura "+textbox[j]+"por que el impuesto de RICA con codigo "+rica+" no existe", logTrans.INFO);
                                    }
                                    else if( !rfte.equals("") ){
                                        Tipo_impuesto ti = model.TimpuestoSvc.buscarImpuestoPorCodigos( "RFTE", rfte, dstrct, ban.getCodigo_Agencia() );
                                        if( ti != null ){
                                            impDoc.setCod_impuesto( rfte );
                                            impDoc.setPorcent_impuesto( ti.getPorcentaje1() );
                                            impDoc.setVlr_total_impuesto( total_rfte );
                                            impDoc.setVlr_total_impuesto_me( total_rfte_me );
                                        }
                                        else
                                            logTrans.log("No se puede agregar la factura "+textbox[j]+"por que el impuesto de RFTE con codigo "+rfte+" no existe", logTrans.INFO);
                                    }
                                    
                                    vImpuestosDoc.add(impDoc);//vector de la cabecera de los impuestos
                                }
                                //valores de cabecera
                                doc.setVlr_neto( val_item + total_iva + total_riva + total_rica + total_rfte );
                                doc.setVlr_total_abonos( 0 );
                                doc.setVlr_saldo( val_item + total_iva + total_riva + total_rica + total_rfte );
                                doc.setVlr_neto_me( val_item_me + total_iva_me + total_riva_me + total_rica_me + total_rfte_me );
                                doc.setVlr_total_abonos_me( 0 );
                                doc.setVlr_saldo_me( val_item_me + total_iva_me + total_riva_me + total_rica_me + total_rfte_me );
                                if( !existe_tasa ){
                                    model.cxpDocService.insertarCXPDoc( doc, vecItem, vImpuestosDoc, ban.getCodigo_Agencia() );
                                    model.despachoService.insertar(consulta); 
                                    logTrans.log("La factura "+textbox[j]+" fu� generada correctamente.", logTrans.INFO);
                                    model.escoltaVehiculoSVC.listadoEscoltaConFactura( model.tablaGenService.buscarCampoTablagenDato( (String)session.getAttribute("tablaProv"), (String) session.getAttribute("proveedor"), "1").toString().trim(), dstrct );
                                    if( terminacion )
                                        terminacion = true;
                                }
                                else{
                                    terminacion = false;
                                }
                            }
                            else{
                                logTrans.log("No se encuantra el banco asignado al proveedor "+nitpro+".", logTrans.INFO);
                                terminacion = false;
                            }
                        }
                        else{
                            logTrans.log("El proveedor "+nitpro+" no existe.", logTrans.INFO);
                            terminacion = false;
                        }
                    }else{
                        logTrans.log("La factura "+textbox[j]+" ya existe.", logTrans.INFO);
                        terminacion = false;
                    }
                }
                if(terminacion)
                    next += "?msg=Se generaron todos los datos correctamente en el proceso, consulte en el log para ver los detalles del proceso.";
                else
                    next += "?msg=Consulte en el log para ver los detalles del problema que ocurrio en el proceso.";
                closeLog(); // cerrando archivo de log
                request.setAttribute("aprobacion","ok" );
            }
            
        }catch(Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    public void initLog()throws Exception{
        
        java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat("yyyyMMdd_hhmmss");
        pw     = new PrintWriter(new BufferedWriter(new FileWriter(path + "/logGeneracionFacturas_"+ fmt.format(new Date()) +".txt")));
        logTrans = new LogWriter("Generacion_Facturas", LogWriter.INFO , pw );
        logTrans.log( "Proceso Inicializado", logTrans.INFO );
    }
    
    public void closeLog() throws Exception{
        logTrans.log( "Proceso Finalizado", logTrans.INFO );
        pw.close();
    }
    
}
