/************************************************************************
 * Nombre Discrepancia_productoSerchAction.java
 * Descripci�n: Accion para buscar un producto de discrepancia.
 * Autor: Jose de la rosa
 * Fecha: 14 de octubre de 2005, 10:05 AM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 **************************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  Jose
 */
public class Discrepancia_productoSerchAction extends Action {
    
    /** Creates a new instance of Discrepancia_productoSerchAction */
    public Discrepancia_productoSerchAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next="";
        HttpSession session = request.getSession ();
        int num_discre = Integer.parseInt (request.getParameter ("c_num_discre"));
        String nom_doc = request.getParameter ("nom_doc");
        String client = request.getParameter ("client");
        String numpla = request.getParameter ("c_numpla");
        String numrem = request.getParameter ("c_numrem");
        String tipo_doc = request.getParameter ("c_tipo_doc");
        String documento = request.getParameter ("c_documento");
        String tipo_doc_rel = (String) request.getParameter ("c_tipo_doc_rel");
        String documento_rel = (String) request.getParameter ("c_documento_rel");
        String cod_producto = request.getParameter ("c_cod_producto").toUpperCase ();
        String cod_discrepancia = request.getParameter ("c_cod_discrepancia");
        String fecha = request.getParameter ("c_fecha_creacion");
        int x = Integer.parseInt (request.getParameter ("x")!=null?request.getParameter ("x"):"0");
        String distrito = (String) session.getAttribute ("Distrito");
        try {
            model.discrepanciaService.searchDiscrepanciaProducto (num_discre, numpla, numrem, tipo_doc, documento, tipo_doc_rel, documento_rel, cod_producto, cod_discrepancia, fecha, distrito);
            if ( model.discrepanciaService.getDiscrepancia () == null ){
                Discrepancia dis = (Discrepancia) model.discrepanciaService.getItemsDiscrepancia ().get (x);
                model.discrepanciaService.setDiscrepancia (dis);
            }
            next="/jsp/cumplidos/discrepancia/DiscrepanciaModificar.jsp?msg=&nom_doc="+nom_doc+"&client"+client;
        }catch (SQLException e) {
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
    
}
