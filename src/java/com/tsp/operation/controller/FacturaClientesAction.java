/********************************************************************
 *      Nombre Clase.................   FacturaClientesAction.java
 *      Descripci�n..................   Action que se encarga de cargar los Clientes en la pagina siguiente
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.util.Util;
/**
 *
 * @author  dlamadrid
 */
public class FacturaClientesAction extends Action{
    
    /** Creates a new instance of FacturasClientesAction */
    public FacturaClientesAction() {
    }
    
    public void run() throws ServletException, InformationException {
        try {
            String opcion = (request.getParameter("OP")!=null)?request.getParameter("OP"):"";
            String next   ="";
            String cliente = (request.getParameter("cliente")!=null)?request.getParameter("cliente"):"";
            String x=(request.getParameter("x")==null?"":request.getParameter("x"));
            String cre = (request.getParameter("cre")==null?"":request.getParameter("cre"));
            
            String msg = "";
            //System.out.println("OPCION --->"+ opcion);
            if(opcion.equals("BuscarUnidad")){
                //System.out.println("CLIENTE-->"+cliente);
                model.clienteService.BuscarCliente("000"+cliente);
                next ="/jsp/cxpagar/facturasxpagar/BuscarUnidadCliente.jsp?opcion=UNIDAD&campo="+x;
                
                
            }else if(opcion.equals("validarPlanilla")){
                String planilla = request.getParameter("OC");
                model.clienteService.searchNit(planilla);
                Cliente cli = model.clienteService.getCliente();
                
                if(cli == null){
                    msg ="La planilla no existe";
                }
                next = "/jsp/cxpagar/facturasxpagar/auxiliar.jsp?opcion=Planilla&campo="+x+"&mensaje="+msg;
                
                
            }else if(opcion.equals("validarCliente")){
                
                model.clienteService.BuscarCliente("000"+cliente);
                Cliente cli = model.clienteService.getClienteUnidad();
                //System.out.println("NIT--->"+ cli.getNit());
                if(cli == null){
                    msg ="El cliente no existe";
                }
                next = "/jsp/cxpagar/facturasxpagar/auxiliar.jsp?opcion=Cliente&campo="+x+"&mensaje="+msg;
                
            }else if(opcion.equals("validarABC")){
                String abc = request.getParameter("ABC");
                if(!model.cxpDocService.ExisteABC(abc)){
                    msg ="El codigo ABC no existe";
                }
                
                next = "/jsp/cxpagar/facturasxpagar/auxiliar.jsp?opcion=ABC&campo="+x+"&mensaje="+msg;
            }else{
                
                model.clienteService.consultarCliente("%"+cliente+"%");
                
                int num_items=0;
                
                try {
                    num_items  = Integer.parseInt(""+ request.getParameter("num_items"));
                    
                }
                catch(java.lang.NumberFormatException e) {
                    
                    num_items=1;
                }
                
                next = "/jsp/cxpagar/facturasxpagar/codigocliente.jsp?accion=1&op=cargarB&num_items="+num_items+"&x="+x+"&cre="+cre;
                //System.out.println("next en Filtro"+next);
            }
            
            //System.out.println("NEXT --->"+ next);
            this.dispatchRequest(next);
            
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new ServletException("Accion:"+ e.getMessage());
        }
        
    }
}
