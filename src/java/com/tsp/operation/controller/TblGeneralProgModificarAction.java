/******************************************************************
* Nombre ......................TblGeneralProgModificarAction.java
* Descripci�n..................Clase Action para tabla general
* Autor........................Ricardo Rosero
* Fecha........................23/12/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
/**
 *
 * @author  Ricardo Rosero
 */
public class TblGeneralProgModificarAction extends Action{
    
    /** Creates a new instance of TblGeneralProgModificarAction */
    public TblGeneralProgModificarAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "";
        String Mensaje = "";
        TblGeneralProg tmp = new TblGeneralProg();
                    
        try{
            String mensaje = request.getParameter("mensaje");
            HttpSession session = request.getSession();            
            String table_code = request.getParameter("table_code");
            String table_type = request.getParameter("table_type");
            String program = request.getParameter("program");
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            Calendar fechaXI = Calendar.getInstance();
            fechaXI.add(fechaXI.DATE, -0);
            java.util.Date fechXI = fechaXI.getTime();
            SimpleDateFormat s1 = new SimpleDateFormat("yyyy-MM-dd");
            String fechaX1 = s1.format(fechXI);
            
            if(mensaje!=null){
                if(mensaje.equalsIgnoreCase("modificar")){
                                                                
                    tmp.settable_code(table_code);
                    tmp.settable_type(table_type);
                    tmp.setprogram(program);
                    tmp.setUm(usuario.getLogin());
                    tmp.setFm(fechaX1);
                    model.tbl_GeneralProgService.setTblGeneralProg(tmp);
                    model.tbl_GeneralProgService.updateTblGeneralProg();
                    next = "/jsp/general/tablagen_prog/item.jsp?Mensaje=Datos modificados satisfactoriamente&reload=ok";
                }
                else if(mensaje.equalsIgnoreCase("anular")){                    
                    tmp.settable_type(table_type);
                    tmp.settable_code(table_code);
                    tmp.setprogram(program);
                    tmp.setUm (usuario.getLogin ());
                    model.tbl_GeneralProgService.setTblGeneralProg(tmp);
                    model.tbl_GeneralProgService.updateTblGeneralProgAnulado();
                    Vector reportes = model.tbl_GeneralProgService.getTodosTblGeneralProg();
                    for (int i = 0 ; i < reportes.size (); i++){
                        TblGeneralProg t1 = (TblGeneralProg) reportes.elementAt(i);
                        if( t1.gettable_code ().equals (table_code) && t1.gettable_type ().equals (table_type) && t1.getprogram ().equals (program) )
                            reportes.remove (i);
                    }
                    next="/jsp/trafico/mensaje/MsgAnulado.jsp";
                }
            }
        }catch(Exception ex){
            ex.printStackTrace();
        }
        this.dispatchRequest(next);
    }
    
}
