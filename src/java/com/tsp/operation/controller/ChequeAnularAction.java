/*
 * ChequeAnular.java
 *
 * Created on 22 de junio de 2005, 03:33 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  DIOGENES
 */
public class ChequeAnularAction extends Action {
    
    /** Creates a new instance of ChequeAnular */
    public ChequeAnularAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/cheques/mostrarChequeImpre.jsp";
        String cheque = request.getParameter("cheque");
        
        /*JuanM 25-11-05*/
        String opcion                 = request.getParameter("Opcion");
        String banc                   = (request.getParameter("banco")!=null)?request.getParameter("banco"):"";
        String banck                  = (request.getParameter("banck")!=null)?request.getParameter("banck"):"";
        String sucursalbanco          = (request.getParameter("sucursalbanco")!=null)?request.getParameter("sucursalbanco"):"";
        
        /*fin*/
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        try{
            model.causas_anulacionService.llenarTree();
            model.trecuperacionaService.llenarTree();
            if(opcion.equals("BuscarS")){
                next = "/cheques/buscarChequeImpre.jsp?banco="+banc;
            }
            /*fin*/
            else{
                
                if ( ! model.movplaService.ChequeAnulado(cheque,banck,sucursalbanco) ){
                    model.movplaService.BuscarChequeImpreso(cheque,banck,sucursalbanco);
                    if(model.movplaService.getMovPla()==null){
                        next = "/cheques/buscarChequeImpre.jsp?estado=no";
                    }
                }
                else
                    next = "/cheques/buscarChequeImpre.jsp?estado=anu";
                
                
                
                if( request.getParameter("anular")!=null){
                    String causa = request.getParameter("causas");
                    String trecup = request.getParameter("trecuperacion");
                    String obs = request.getParameter("observacion");
                    
                    model.movplaService.BuscarChequeImpreso(cheque,banck,sucursalbanco);
                    Movpla mp = model.movplaService.getMovPla();
                    mp.setCreation_user(usuario.getLogin());
                    mp.setCausa(causa);
                    mp.setTipo_rec(trecup);
                    mp.setObservacion(obs);
                    model.movplaService.setMovPla(mp);
                    model.tService.crearStatement();
                    model.tService.getSt().addBatch(model.movplaService.anularChequeImpreso());
                    model.tService.execute();
                    next = "/cheques/buscarChequeImpre.jsp?estado=ok";
                }
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
