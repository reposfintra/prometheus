/*
 * ProductoSerchAction.java
 *
 * Created on 17 de octubre de 2005, 11:58 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;

/**
 *
 * @author  Jose
 */
public class ProductoSerchAction extends Action{
    
    /** Creates a new instance of ProductoSerchAction */
    public ProductoSerchAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        String next="";
        HttpSession session = request.getSession();
        String distrito = (String) session.getAttribute ("Distrito");
        String codigo = "";
        String cliente = "";
        String descripcion = "";
        String unidad = "";
        String listar = (String) request.getParameter("listar");
        try{                
            if (listar.equals("True")){
                next = request.getParameter("carpeta")+"/"+request.getParameter("pagina");
                String sw = (String) request.getParameter("sw");
                if(sw.equals("True")){
                    unidad = request.getParameter("c_unidad").toUpperCase();
                    cliente = request.getParameter("codcli").toUpperCase();
                    codigo = (String) request.getParameter("c_codigo").toUpperCase();
                    descripcion = (String) request.getParameter("c_descripcion");
                }
                else{
                    unidad = "";
                    cliente = "";
                    codigo = "";
                    descripcion = "";
                }
                session.setAttribute("codigo", codigo);
                session.setAttribute("descripcion", descripcion);
                session.setAttribute("unidad", unidad);
                session.setAttribute("cliente", cliente);
            }
            else{
                codigo = (String) request.getParameter("c_codigo").toUpperCase();
                cliente = request.getParameter("codcli").toUpperCase();
                model.productoService.serchProducto(codigo, distrito, cliente);
                model.clienteService.datosCliente (cliente);
                next="/jsp/cumplidos/producto/ProductoModificar.jsp";
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);    
    }
    
}
