/*
 * AnularNovedadAction.java
 *
 * Created on 13 de junio de 2005, 10:01 AM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  Henry
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;

public class AnularNovedadAction extends Action{
    
    /** Creates a new instance of AnularNovedadAction */
    public AnularNovedadAction() {
    }
    
    public void run() throws ServletException {
        String next = "/jsp/trafico/novedad/VerNovedades.jsp?reload=";
        String codigo = (request.getParameter("codigo"));
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        try{
            if(!model.novedadService.existeNovedad(codigo) ){
                next = "jsp/trafico/novedad/novedad.jsp?mensaje=Error_Anular";    
            }
            else{
                Novedad novedad = new Novedad();
                novedad.setCodNovedad(codigo);      
                novedad.setUser_update(usuario.getLogin());
                model.novedadService.eliminarNovedad(novedad);               
                next = next + "Anulado";
            }
        }
        catch (SQLException e){
               throw new ServletException(e.getMessage());
        }
         
         // Redireccionar a la p�gina indicada.
       this.dispatchRequest(next);
    }
    
}
