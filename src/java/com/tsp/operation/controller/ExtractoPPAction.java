/***************************************
    * Nombre Clase ............. ExtractoPPAction.java
    * Descripci�n  .. . . . . .  Permite Liq. PP
    * Autor  . . . . . . . . . . JULIO BARROS RUEDA
    * Fecha . . . . . . . . . .  24/11/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...Transportes Sanchez Polo S.A.
    *******************************************/

package com.tsp.operation.controller;




import java.io.*;
import java.util.*;
import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.threads.*;
import com.tsp.util.Util;

public class ExtractoPPAction extends Action{
    
    
    public void run() throws ServletException, InformationException {
        try{
                String next      = "/jsp/cxpagar/liquidacion/verExtracto.jsp";            
            
                String propietario   = request.getParameter("parametro");
                String fechai        = request.getParameter("fechai");
                String fechaf        = request.getParameter("fechaf");
                String opcion        = request.getParameter("opcion");
                int value            = -1;
                int value2           = -1;
                
                if (opcion.equals("VistaPrevia")){
                    value            =  Integer.parseInt(request.getParameter("tipo"));
                    value2           =  Integer.parseInt(request.getParameter("tipo2"));
                }
            
                String fecha         = request.getParameter("fecha");
                String nitt          = request.getParameter("nit");
                String secu          = request.getParameter("secuencia");
                
            
                
            if(propietario==null)
                propietario="";
            String ocs  = propietario;
            if (opcion.equals("VistaPrevia")){                
                Vector datos;
                if( value2 == 0){
                    model.ExtractoPPSvc.buscarExtractosPP(propietario,value,fechai,fechaf);
                    datos = model.ExtractoPPSvc.getExtractoPP();
                }else{
                    model.ExtractoPPSvc.buscarExtractosPPOC(propietario,fechai,fechaf);
                    model.ExtractoPPSvc.buscarExtractoPPOC_Fechas(propietario,fechai,fechaf);
                    propietario = model.ExtractoPPSvc.getNitPropXOC();
                    if (propietario.trim().equals(""))
                        propietario="9999999999999999999999999";
                    model.ExtractoPPSvc.buscarExtractosPP(propietario,value);
                    datos = model.ExtractoPPSvc.getExtractoPP();
                }
                if (datos!=null && !datos.isEmpty()){
                    if( value2 == 0){
                        next = "/jsp/cxpagar/liquidacion/vistaPreviaExtractoPP.jsp?op=0";
                    }else{
                        next = "/jsp/cxpagar/liquidacion/vistaPreviaExtractoPP.jsp?op=1&OC="+ocs;
                    }
                }else{
                    next = "/jsp/cxpagar/liquidacion/ConsultaExtractos.jsp?msg=no se encontraron datos para las restricciones definidas...";
                }                  
            }else if (opcion.equals("ExtractoLiquidacion")){
                model.ExtractoPPSvc.buscarExtractoDetallePP(nitt,fecha,1);
                model.ExtractoPPSvc.buscarExtractoDetallePP(nitt,fecha,2);
                model.ExtractoPPSvc.buscarExtractoDetallePP(nitt,fecha,3);
                Vector datos = model.ExtractoPPSvc.getExtractoDetallePP1();
                if (datos==null && datos.isEmpty()){
                    next = "/jsp/cxpagar/liquidacion/ConsultaExtractos.jsp?msg=no se encontraron datos para las restricciones definidas...";
                }                                
            }else if (opcion.equals("ExtractoSecuencia")){
                model.ExtractoPPSvc.buscarExtractoDetallePP(secu);
                Vector datos = model.ExtractoPPSvc.getExtractoDetallePPS();
                if (datos==null && datos.isEmpty()){
                    next = "/jsp/cxpagar/liquidacion/ConsultaExtractos.jsp?msg=no se encontraron datos para las restricciones definidas...";
                }
                //System.out.println("a ver las secuencias");
                next = "/jsp/cxpagar/liquidacion/VistaPreviaSecuencia.jsp?op=1";
                
            }
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new Exception("No se pudo encontrar "+ next);
            rd.forward(request, response); 
        } catch (Exception e){
             e.printStackTrace();
             throw new ServletException(e.getMessage());
        }
    }
}