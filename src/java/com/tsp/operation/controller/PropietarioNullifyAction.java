/*
 * PropietarioNullifyAction.java
 *
 * Created on 15 de junio de 2005, 01:35 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

/**
 *
 * @author  Sandrameg
 */
public class PropietarioNullifyAction extends Action {
    
    /** Creates a new instance of PropietarioNullifyAction */
    public PropietarioNullifyAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/propietarios/propietarioNullify.jsp";
        
        String dstrct = request.getParameter("dstrct").toUpperCase();
        String cedula = request.getParameter("ced").toUpperCase();
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");        
        
        try{    
            
            model.propService.buscarxCedulaxDistrito(cedula,dstrct);
            
            if(model.propService.getPropietario() != null){
                Propietario p = new Propietario();
                p.setCedula(cedula);
                p.setdstrct(dstrct);
                p.setUser_update(usuario.getLogin());
                
                model.propService.nullify(p);
                
                String res = p.getCedula() + " en el Distrito " + p.getdstrct();
            
                request.setAttribute("res", res);
                next = next + "?msg=exitoA";            
            }            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}
