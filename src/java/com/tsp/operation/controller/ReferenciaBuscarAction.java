/*************************************************************************
 * Nombre:        ReferenciaVerificarAction.java
 * Descripci�n:   Clase Action para Buscar referencia
 * Autor:         Ing. Diogenes Antonio Bastidas Morales
 * Fecha:         24 de febrero de 2006, 02:24 PM                           
 * Versi�n        1.0                                   
 * Coyright:      Transportes Sanchez Polo S.A.         
 *************************************************************************/
 
package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class ReferenciaBuscarAction extends Action {
    
    /** Creates a new instance of ReferenciaVerificarAction */
    public ReferenciaBuscarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina")+"?tipo="+request.getParameter("tipo");
        String doc = request.getParameter("documento").toUpperCase();
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
        String dstrct = (String) session.getAttribute ("Distrito");
        try{
            model.referenciaService.buscarReferencias(doc,dstrct,request.getParameter("tipo"));
            Vector vecRef = model.referenciaService.getVectorReferencias();
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
         
        ////System.out.println(next);
        //Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
    }    
    
}
