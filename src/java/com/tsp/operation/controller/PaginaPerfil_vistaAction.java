/*
 * PaginaPerfil_vistaAction.java
 *
 * Created on 13 de julio de 2005, 23:32
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.Util;

/**
 *
 * @author  Rodrigo
 */
public class PaginaPerfil_vistaAction extends Action{
    
    /** Creates a new instance of ActividadSerchAction */
    public PaginaPerfil_vistaAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String pag = "/jsp/trafico/permisos/perfil_vista/relacionarCampos.jsp";
        String next;
        HttpSession session = request.getSession();
        String perfil = (String) request.getParameter("c_perfil");
        String pagina = (String) request.getParameter("c_pagina");
        String descripcion = (String) request.getParameter("c_descripcion");
        
        //tamatu 25.10.2005
        String consultar = (String) request.getParameter("consulta");
        if( consultar != null ) session.setAttribute("consulta_rcampos",  consultar);
        ////System.out.println(perfil);
                /*try{
                    request.setAttribute("perfil",perfil);
                    request.setAttribute("pagina",pagina);
                }
                catch (Exception e){
                    throw new ServletException(e.getMessage());
                }*/
        pag+="?c_perfil=" + perfil + "&c_pagina=" + pagina;
        next = pag;
        
        this.dispatchRequest(next);
    }
}
