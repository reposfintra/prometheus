/*******************************************************************
 * Nombre:        CambiarEstadoAction.java
 * Descripci�n:   Clase Action para cambiar los registros de placa o nit
 * Autor:         Ing. Diogenes Antonio Bastidas Morales
 * Fecha:         15 de marzo de 2006, 02:53 PM
 * Versi�n        1.0
 * Coyright:      Transportes Sanchez Polo S.A.
 *************************************************************************/


package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;


public class CambiarEstadoBuscarAction extends Action {
    
    /** Creates a new instance of CambiarEstadoAction */
    public CambiarEstadoBuscarAction() {
    }
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = (String) session.getAttribute("Distrito");
        String next = "/jsp/hvida/cambiar_estado/";
        String tipo = request.getParameter("tipo");
        try{
            if (tipo.equals("Conductor")){
                model.conductorService.buscarConductorCGA(request.getParameter("identificacion"));
                if ( model.conductorService.getCond() != null){
                    next += "Conductor.jsp";
                }
                else{
                    next+="buscarConductor.jsp?men=no";
                }
            }else if (tipo.equals("Placa")){
                model.placaService.buscarPlacaCGA(request.getParameter("placa").toUpperCase());
                if ( model.placaService.getV() != null ){
                    next += "Placa.jsp";
                }
                else{
                    next+="buscarPlaca.jsp?men=no";
                }
            } else if (tipo.equals("Nit")){
                model.conductorService.buscarNit(request.getParameter("identificacion"));
                if ( model.conductorService.getCond() != null){
                    next += "Nit.jsp";
                }
                else{
                    next+="buscarNit.jsp?men=no";
                }
            }
        }
        catch(Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}
