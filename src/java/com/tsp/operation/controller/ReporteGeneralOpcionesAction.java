/*
 * ReporteGeneralOpcionesAction.java
 *
 * Created on 26 de abril de 2007, 08:21 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;
import com.tsp.util.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  EQUIPO26
 */
public class ReporteGeneralOpcionesAction extends Action{
    
    /** Creates a new instance of ReporteGeneralOpcionesAction */
    public ReporteGeneralOpcionesAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = (String) session.getAttribute("Distrito");
        String opc = request.getParameter("opcion") !=null? !request.getParameter("opcion").equals("")? (String) request.getParameter("opcion") : "0" :"0";
        try{
            if( opc.equals("1") ){
                next = "/jsp/general/exportar/ReporteRemesasNacionales.jsp";
                if (model.cxpDocService.getEnproceso()){
                    next += "?msg=Hay un proceso en ejecucion. Una vez terminado podra realizar la generacion de un nuevo reporte...<br>Para realizar el seguimiento del actual proceso haga clic en el vinculo&ruta=PROCESOS/Seguimiento de Procesos";
                }
                else {
                    String fchi = (String) request.getParameter("fechaInicio");
                    String fchf = (String) request.getParameter("fechaFinal");
                    ReporteRemesasNacionalesThreads hilo = new ReporteRemesasNacionalesThreads();
                    hilo.start( fchi, fchf, usuario.getLogin());
                    next += "?msg=La generacion del Reporte ha iniciado...<br>Para realizar el seguimiento del proceso haga clic en el vinculo&ruta=PROCESOS/Seguimiento de Procesos";
                }
            }
        }catch (Exception ex){
            throw new ServletException("Error en ReporteGeneralOpcionesAction......\n"+ex.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
