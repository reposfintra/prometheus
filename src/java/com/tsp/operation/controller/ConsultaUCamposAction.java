/******************************************************************
 * Nombre ......................ConsultaUCamposActionConsultaUCamposAction.java
 * Descripci�n..................Clase Action para cargar los campos de las tablas
 * Autor........................David lamadrid
 * Fecha........................21/12/2005
 * Versi�n......................1.0
 * Coyright.....................Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  dlamadrid
 */
public class ConsultaUCamposAction extends Action {
    
    /** Creates a new instance of ConsultaUCamposAction */
    public ConsultaUCamposAction () {
    }
    
    public void run () throws ServletException, InformationException {
        
        String next="";
        try {
            String tabla=""+request.getParameter ("c_tabla");
            String nombre_tabla=""+request.getParameter ("c_tabla");
            String from =""+request.getParameter ("from");
            
            
            
            String[] campos = request.getParameterValues ("cselect");
            String[] cfrom = request.getParameterValues ("cfrom");
            
            if(cfrom != null){
                model.consultaUsuarioService.validarFrom (cfrom,from);
            }
            
            //seteo un Vector vFrom con los Campos de la Tabla
            model.consultaUsuarioService.obtenerCampos (tabla);
            model.consultaUsuarioService.adicionarTabla (tabla);
            from = model.consultaUsuarioService.getFrom ();
            
            //System.out.println("CAMPOSSSSS "+campos);
            if(campos!=null){
                Vector vSelect=new Vector ();
                for( int i=0 ;i<campos.length;i++){
                    vSelect.add (""+campos[i]);
                }
                model.consultaUsuarioService.setVSelect (vSelect);
            }
            else{
                model.consultaUsuarioService.setVSelect (null);
            }
            
            next="/jsp/general/consultas/insertarconsultas.jsp?nombre_tabla="+nombre_tabla+"&from="+from;
            RequestDispatcher rd = application.getRequestDispatcher (next);
            if(rd == null){
                throw new Exception ("No se pudo encontrar "+ next);
            }
            rd.forward (request, response);
            
        }
        catch (Exception e) {
            throw new ServletException (e.getMessage ());
        }
        //  this.dispatchRequest (next);
    }
    
}
