/**********************************************************************
 *      Nombre Clase.................   AnalisisFacturasEndosoAction
 *      Autor........................   Ivargas
 *      Fecha........................   07-07-2011
 *      Versi�n......................   1.1
 *********************************************************************/
package com.tsp.operation.controller;

import java.util.*;
import java.text.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;

public class AnalisisFacturasEndosoAction extends Action {

    Vector general;
    HttpSession session;

    public AnalisisFacturasEndosoAction() {
    }

    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {

        session = request.getSession();

        String codcli = request.getParameter("clientes") == null ? "" : request.getParameter("clientes");
        if(codcli.equals("")){
            codcli = request.getParameter("codcli") == null ? "" : request.getParameter("codcli");
        }
        String prov = request.getParameter("proveedor") == null ? "" : request.getParameter("proveedor");
        String cccli = request.getParameter("cccli") == null ? "" : request.getParameter("cccli");
        String fecha = request.getParameter("fecha");
        String cofac = request.getParameter("tfac");
        String lim1 = request.getParameter("lim1");
        String lim2 = request.getParameter("lim2");
        String factura = request.getParameter("factura");
        String tercero = request.getParameter("terceros");

        //Pr�xima vista
        String next = "";

        try {
            model.facturaService.facturasEndoso(codcli, fecha, (String) session.getAttribute("Distrito"), prov, cccli, cofac, lim1, lim2,factura,tercero);
            Vector info = model.facturaService.getVDocumentos();

            this.organizarInfo(info, fecha);

            next = "/jsp/fenalco/contabilizacion/AnalisisFacturaDetEndoso.jsp?tercero="+tercero;
            request.setAttribute("det", this.general);

            request.setAttribute("fecha", fecha);
            request.setAttribute("codcli", codcli);
            request.setAttribute("prov", prov);
            request.setAttribute("cccli", cccli);
            request.setAttribute("cfact", cofac);
            request.setAttribute("lim1", lim1);
            request.setAttribute("lim2", lim2);

        } catch (Exception e) {
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }

    public void organizarInfo(Vector info, String fecha) throws Exception {
        Calendar FechaHoy = Calendar.getInstance();
        Date d = FechaHoy.getTime();
        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd");

        this.general = new Vector();

        for (int i = 0; i < info.size(); i++) {
            RepGral obj = null;
            obj = new RepGral();
            obj = (RepGral) info.elementAt(i);

            int dias = obj.getNdias();

            obj.setVencida_6(0);
            obj.setVencida_5(0);
            obj.setVencida_4(0);
            obj.setVencida_3(0);
            obj.setVencida_2(0);
            obj.setVencida_1(0);
            obj.setNo_vencida(0);

            if (dias > 90) {
                obj.setVencimiento(">90");
            } else if (dias > 60) {
                obj.setVencimiento("90");
            } else if (dias > 30) {
                obj.setVencimiento("60");
            } else if (dias > 14) {
                obj.setVencimiento("30");
            } else if (dias > 7) {
                obj.setVencimiento("14");
            } else if (dias > 0) {
                obj.setVencimiento("7");
            } else if (dias > -8) {
                obj.setVencimiento("-7");
            } else if (dias > -15) {
                obj.setVencimiento("-14");
            } else if (dias > -22) {
                obj.setVencimiento("-21");
            } else {
                obj.setVencimiento("-28");
            }
            this.general.add(obj);
        }
    }
}
