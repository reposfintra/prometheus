/**
 * Nombre       ReportePlanillasAnuladasMIMSAction.java
 * Autor        Ing Jose de la Rosa
 * Fecha        9 de agosto de 2006, 10:05 AM
 * versi�n      1.0
 * Copyright    Transportes Sanchez Polo S.A.
 **/

package com.tsp.operation.controller;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class ReportePlanillasAnuladasMIMSAction extends Action{
    
    /** Creates a new instance of ReportePlanillasAnuladasMIMSAction */
    public ReportePlanillasAnuladasMIMSAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next = "/jsp/general/exportar/ReportePlanillasAnuladasMIMS.jsp";
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String distrito = (String) session.getAttribute ("Distrito");
        try{
            PlanillaAnuladasMIMSIngresoTraficoThreads hilo = new PlanillaAnuladasMIMSIngresoTraficoThreads (model);
            hilo.start ( usuario.getLogin (), distrito );
            next += "?msg=La generacion del Reporte ha iniciado...<br>Para realizar el seguimiento del proceso haga clic en el vinculo&ruta=PROCESOS/Seguimiento de Procesos";
        }catch (Exception ex){
            throw new ServletException ("Error en ReportePlanillaAnuladasAction......\n"+ex.getMessage ());
        }
        this.dispatchRequest (next);
    }
    
}
