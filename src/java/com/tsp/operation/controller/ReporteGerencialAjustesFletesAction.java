/*
     - Author(s)       :      Ing. Luis Eduardo Frieri
     - Date            :      10/01/2007  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
*/
package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.*;
import com.tsp.exceptions.*;

public class ReporteGerencialAjustesFletesAction extends Action{
    
    public ReporteGerencialAjustesFletesAction() {
    }
    
    public void run() throws ServletException, InformationException{
        String next = "";
        HttpSession session = request.getSession();
        Usuario usuario = ( Usuario ) session.getAttribute ( "Usuario" );
        
        String fechai = request.getParameter("fechai")!=null?request.getParameter("fechai"):"";
        String fechaf = request.getParameter("fechaf")!=null?request.getParameter("fechaf"):"";
        String moneda_usuario = (String)session.getAttribute("Moneda");
        String distrito = usuario.getDstrct();
        
        int sw = 0;
        
                HiloReporteGerencialFletes  hilo = new HiloReporteGerencialFletes();
                hilo.start ( fechai, fechaf, usuario, moneda_usuario, distrito );
                next="/jsp/masivo/reportes/ReporteAjusteFletes.jsp?Msg=El proceso de Creacion del reporte comenzo con Exito.";

        this.dispatchRequest(next);
    }
    
}
