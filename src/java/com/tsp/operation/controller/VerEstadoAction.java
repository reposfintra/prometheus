/*
 * VerEstadoAction.java
 *
 * Created on 1 de marzo de 2005, 03:39 PM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  DIBASMO
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;

public class VerEstadoAction extends Action {
    
    /** Creates a new instance of VerEstadoAction */
    public VerEstadoAction(){        
    }
    
    public void run() throws ServletException {
        String next = "/jsp/trafico/estado/VerEstado.jsp";
        String pais = (request.getParameter("pais"));
        HttpSession session = request.getSession();
        //String lenguaje = (String) session.getAttribute("idioma");
        try{ 
          // model.idiomaService.cargarIdioma(lenguaje,"VerEstado.jsp");
          // Properties pro = model.idiomaService.getIdioma();
           model.paisservice.buscarpais(pais);
         
        }catch (SQLException e){
               throw new ServletException(e.getMessage());
         }
        this.dispatchRequest(next);
    }
    
}
