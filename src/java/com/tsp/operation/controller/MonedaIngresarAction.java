/*
 * MonedaIngresarAction.java
 *
 * Created on 24 de octubre de 2005, 03:23 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  dbastidas
 */
public class MonedaIngresarAction extends Action {
        
        /** Creates a new instance of MonedaIngresarAction */
        public MonedaIngresarAction() {
        }
        
        public void run() throws ServletException, InformationException {
                String codigo = (request.getParameter("c_codigo").toUpperCase());
                String des = (request.getParameter("c_des").toUpperCase());
                String ini = (request.getParameter("c_ini").toUpperCase());
                String next = "/jsp/cxpagar/moneda/Moneda.jsp?mensaje=Moneda Agregada con exito...";
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario) session.getAttribute("Usuario");  
                int sw=0;
                try{ 
                        Moneda moneda = new Moneda();
                        moneda.setCodMoneda(codigo);
                        moneda.setNomMoneda(des);
                        moneda.setInicial(ini);
                        moneda.setUser_update(usuario.getLogin());
                        moneda.setCreation_user(usuario.getLogin());        
                        try{
                                if(!model.monedaSvc.existeInicial(ini)){
                                        model.monedaSvc.insertarMoneda(moneda);                             
                                }
                                else{
                                     next = "/jsp/cxpagar/moneda/Moneda.jsp?mensaje=Existe La Inicial...&sw=0";   
                                }
                        }catch (SQLException e){
                                ////System.out.println("Error " + e.getMessage()+ " Codigo "+ e.getErrorCode() );
                                sw=1;
                        }
                        if ( sw == 1 ){
                                if ( model.monedaSvc.existeMoneda(codigo) ){
                                        moneda.setEstado("");
                                        model.monedaSvc.modificarMoneda(moneda);                
                                }
                                else {
                                        next = "/jsp/cxpagar/moneda/Moneda.jsp?mensaje=Error al Agregar Moneda...&sw=0";
                                        
                                }
                        }
            
            
                }catch (SQLException e){
                        throw new ServletException(e.getMessage());
                }
                this.dispatchRequest(next);

        }
        
}
