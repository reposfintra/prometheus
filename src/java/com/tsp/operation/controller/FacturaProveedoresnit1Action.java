/********************************************************************
 *      Nombre Clase.................   FacturaImpuestoAction.java
 *      Descripci�n..................   Action que se encarga de generar un vector con los resgitros de proveedores por numero de nit
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.util.Util;
/**
 *
 * @author  dlamadrid
 */
public class FacturaProveedoresnit1Action extends Action
{
        
        /** Creates a new instance of FacturaProveedoresnitAction */
        public FacturaProveedoresnit1Action ()
        {
        }
        
        public void run () throws ServletException, InformationException
        {
                try
                {
                        String proveedor =""+ request.getParameter ("proveedor");
                        //System.out.println("proveedor "+proveedor);
                        int num_items=0;
                        try
                        {
                                num_items  = Integer.parseInt (""+ request.getParameter ("num_items"));
                        }
                        catch(java.lang.NumberFormatException e)
                        {
                                num_items=1;
                        }
                        Proveedor o_proveedor = model.proveedorService.obtenerProveedorPorNit (proveedor);
                        String sucursal = o_proveedor.getC_bank_account ();
                        String banco =o_proveedor.getC_branch_code ();
                        //System.out.println("sucursal"+sucursal);
                        //System.out.println("banco "+ banco);
                        CXP_Doc factura = model.cxpDocService.getFactura ();
                        if(factura != null)
                        {
                                factura.setProveedor (proveedor);
                                factura.setBanco (banco);
                                factura.setSucursal (sucursal);
                                model.cxpDocService.setFactura (factura);
                        }
                        else
                        {
                                //System.out.println("la factura es nula");
                        }
                        
                        String next = "/jsp/cxpagar/facturasxpagar/facturaP.jsp?accion=1&num_items="+num_items+"&reload=1";
                        //System.out.println("next en Lista de Proveedores"+next);
                        this.dispatchRequest (next);
                        
                }
                catch(Exception e)
                {
                        throw new ServletException ("Accion:"+ e.getMessage ());
                }
        }
        
}
