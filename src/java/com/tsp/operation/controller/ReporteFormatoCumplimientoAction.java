/*
 * ReporteFormatoCumplimientoAction.java
 *
 * Created on 1 de noviembre de 2006, 02:38 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

/**
 *
 * @author  EQUIPO13
 */
public class ReporteFormatoCumplimientoAction  extends Action{
    
    /** Creates a new instance of ReporteFormatoCumplimientoAction */
    public ReporteFormatoCumplimientoAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next = "/jsp/general/exportar/ReporteFormatoCumplimiento.jsp";
        HttpSession session = request.getSession();
        Usuario usuario =  (Usuario) session.getAttribute("Usuario");
        String distrito =  (String)  request.getParameter("distrito")!=null?request.getParameter("distrito"):"";
        String fecha_inicio =  (String) request.getParameter("fechaInicio")!=null?request.getParameter("fechaInicio"):"";
        String fecha_final  =  (String) request.getParameter("fechaFinal")!=null?request.getParameter("fechaFinal"):"";
        
        //KREALES
        String agencia =request.getParameter("agencias")!=null? request.getParameter("agencias"):"";
        String cliente =request.getParameter("cliente")!=null? request.getParameter("cliente"):"";
        try{
            if (model.cxpDocService.getEnproceso()){
                request.setAttribute ("mensaje","Hay un proceso en ejecucion. Una vez terminado podra realizar la generacion de un nuevo reporte...<br>Para realizar el seguimiento del actual proceso haga clic en el vinculo");
                next += "?ruta=PROCESOS/Seguimiento de Procesos";
            }
            else {
                System.out.println("Agencia "+agencia);
                System.out.println("cliente "+cliente);
                model.cxpDocService.setEnproceso();
                ReporteFormatoCumplimientoThreads hilo = new ReporteFormatoCumplimientoThreads(model);
                hilo.start( usuario.getLogin(),distrito, fecha_inicio, fecha_final, agencia, cliente );
                request.setAttribute ("mensaje","La generacion del Reporte ha iniciado...<br>Para realizar el seguimiento del proceso haga clic en el vinculo");
                next += "?ruta=PROCESOS/Seguimiento de Procesos";
            }
        }catch (Exception ex){
            throw new ServletException("Error en Reporte_FormatoCumplimientoXLSAction......\n"+ex.getMessage());
        }
        this.dispatchRequest(next); 
    }
    
}
