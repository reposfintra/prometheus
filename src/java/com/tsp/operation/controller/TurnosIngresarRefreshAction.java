/********************************************************************
 *      Nombre Clase.................   TurnosIngresarRefreshAction.java
 *      Descripci�n..................   Action para refrescar la vista de ingreso de turnos
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   29.03.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  dlamadrid
 */
public class TurnosIngresarRefreshAction extends Action{
    
    /** Creates a new instance of TurnosIngresarAction */
    public TurnosIngresarRefreshAction() {
    }
    
    public void run () throws ServletException, InformationException {
        String next="";
        try {
            HttpSession session = request.getSession ();
            Usuario usuario = (Usuario) session.getAttribute ("Usuario");
            
            model.usuarioService.getUsuariosPorDpto ("traf");
            next="/jsp/trafico/turnos/insertar.jsp?marco=no";
            
            String usuario_turno = request.getParameter ("usuario");
            model.zonaService.listarZonasxUsuario (usuario_turno);
            String fecha_turno = request.getParameter ("fecha");
            String fecha_turno2 = request.getParameter ("fecha2");
            String h_entrada = request.getParameter ("h_entrada");
            String h_salida = request.getParameter ("h_salida");
            String m_entrada = request.getParameter ("m_entrada");
            String m_salida = request.getParameter ("m_salida");
            String h_e = request.getParameter ("h_e");
            String h_s = request.getParameter ("h_s");
            String dia =request.getParameter ("dia");
            String semanal = request.getParameter ("semanal");            
            
            next += "&turno_sel=" + usuario_turno + 
                    "&fecha_turno=" + fecha_turno + 
                    "&fecha_turno2=" + fecha_turno2 + 
                    "&h_ent=" + h_entrada +
                    "&h_sal=" + h_salida +
                    "&m_ent=" + m_entrada +
                    "&m_sal=" +  m_salida +
                    "&h_e=" + h_e +
                    "&h_s=" + h_s +                    
                    "&d=" + dia;
            next+= (semanal != null )? "&chk=si" :"&chk=no";                    
        }
        catch (Exception e) {
            throw new ServletException (e.getMessage ());
            
        }
        this.dispatchRequest (next);
    }
    
}
