/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.JsonObject;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.AplicacionPagosMasivosDAO;
import com.tsp.operation.model.DAOS.impl.AplicacionPagosMasivosImpl;
import com.tsp.operation.model.beans.Usuario;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import com.tsp.operation.model.beans.POIWrite;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author dvalencia
 */
public class AplicacionPagosMasivosAction extends Action {

    Usuario usuario = null;

    private AplicacionPagosMasivosDAO dao;

    POIWrite xls;
    private int fila = 0;
    HSSFCellStyle header, titulo1, titulo2, titulo3, titulo4, titulo5, letra, numero, dinero, dinero2, numeroCentrado, porcentaje, letraCentrada, numeroNegrita, ftofecha;
    private SimpleDateFormat fmt;
    String filename;
    String rutaInformes;
    String nombre;
    List listado;
    String path;
    String reponseJson = "{}";
    String typeResponse = "application/json;";

    private final int GENERAR_ARCHIVO_PAGOS_MASIVOS = 1;
    private final int CARGAR_PAGOS_MASIVOS = 2;
    private final int APLICAR_PAGOS_MASIVOS = 3;
    private final int CARGAR_COMBO_LOTES = 4;
    private final int ELIMINAR_LOTES = 5;
    private final int CARGAR_COMBO_LOGICA_APLI = 6;
    private final int CIERRE_PLAN_AL_DIA = 7;
    private final int MOSTRAR_RESUMEN_ARCHIVO_ASOBANCARIA = 8;
    private final int CARGAR_ARCHIVO_GENERAR_LOTE_PAGO = 9;
    private final int MOSTRAR_REFE_INCONSISTENCIAS = 10;
    private final int GENERAR_LOTE_PAGOS = 11;
    private final int UPDATE_REFERENCIA_NEGOCIOS = 12;
    private final int MOSTRAR_LOG_REFERENCIA_PAGOS = 13;
    private final int RECLASIFICAR_LOTES = 14 ;
   

    @Override
    public void run() throws ServletException, InformationException {
        try {

            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            dao = new AplicacionPagosMasivosImpl(usuario.getBd());
            int opcion = (request.getParameter("opcion") != null ? Integer.parseInt(request.getParameter("opcion")) : -1);
            switch (opcion) {
                
                case GENERAR_ARCHIVO_PAGOS_MASIVOS:
                    generarArchivoPagosMasivos();
                    break;
                case CARGAR_PAGOS_MASIVOS:
                    CargarPagosMasivos();
                    break;
                case APLICAR_PAGOS_MASIVOS:
                    AplicarPagosMasivos();
                    break;
                case CARGAR_COMBO_LOTES:
                    CargarComboLotes();
                    break;
                case ELIMINAR_LOTES:
                    EliminarLotePagoMasivo();
                    break;
                case CARGAR_COMBO_LOGICA_APLI:
                    CargarComboLogicaApli();
                    break;
                case CIERRE_PLAN_AL_DIA:
                    CierrePlanAlDia();
                    break;
                case RECLASIFICAR_LOTES:
                    ReclasificarLotes();
                    break;
                case MOSTRAR_RESUMEN_ARCHIVO_ASOBANCARIA:
                    resumenArchivoAsobancaria();
                    break;
                case CARGAR_ARCHIVO_GENERAR_LOTE_PAGO:
                    cargarArchivoGenerarPago();
                    break;
                case MOSTRAR_REFE_INCONSISTENCIAS:
                    mostrarRefeInconsistencia();
                    break;
                case GENERAR_LOTE_PAGOS:
                    generarLotePagos();
                    break;
                case UPDATE_REFERENCIA_NEGOCIOS:
                    updateRefrenciaNegocio();
                    break;
                case MOSTRAR_LOG_REFERENCIA_PAGOS:
                    mostrarLogReferenciaPagos();               
            }
        }catch (Exception ex) {
            reponseJson = "{\"error\":\"Algo salio mal al procesar su solicitud\",\"exception\":\"" + ex.getMessage() + "\"}";
            ex.printStackTrace();
        } finally {
            try {
                this.printlnResponseAjax(reponseJson, typeResponse);
            } catch (Exception ex1) {
                Logger.getLogger(NegociosFintraAction.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }

    }

    private void generarArchivoPagosMasivos() throws Exception {        
        
        JsonObject json = new JsonObject();
//        
        try {
            if (ServletFileUpload.isMultipartContent(request)) {

                ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
                List fileItemsList = servletFileUpload.parseRequest(request);

                Iterator it = fileItemsList.iterator();

                if (it.hasNext()) {

                    FileItem fileItem = (FileItem) it.next();

                    if (!fileItem.isFormField()) {
                        boolean periodos = Boolean.parseBoolean(request.getParameter("periodos"));
                        if (dao.leerArchivoPagoMasivo(fileItem.getInputStream(), usuario.getLogin(), usuario.getDstrct())) {
                            json.addProperty("respuesta", "Se gener� el archivo de Pagos �xitosamente.");
                        } else {
                            json.addProperty("erro", "No se pudo generar el archivo. Comun�quese con soporte.");
                        }
                    }
                } else {
                    json.addProperty("error", "Debe cargar un archivo.");
                }
            } else {
                json.addProperty("error", "Petici�n con formato incorrecto.");
            }
        } catch (IOException e) {
            System.err.println(e.getCause());
            System.err.println("Error generando el archivo de Pagos: " + e.getMessage());
            json.addProperty("error", "Error leyendo/escribiendo el archivo.");
        } catch (SQLException e) {
            System.err.println(e.getCause());
            System.err.println("Error generando el archivo de Pagos en: " + e.getLocalizedMessage() + " -- " + e.getMessage());
            json.addProperty("error", "Error en las consultas a la base de datos.");
        }       
        this.printlnResponseAjax(json.toString(), "application/json");
    }

    private void CargarPagosMasivos() {
    
        String lote = request.getParameter("lote") != null ? request.getParameter("lote") : "";
        String logica_aplicacion = request.getParameter("logica_aplicacion") != null ? request.getParameter("logica_aplicacion") : "";
        try {
            this.reponseJson =dao.CargarPagosMasivos(usuario.getLogin(),lote, logica_aplicacion);
        } catch (Exception e) {
             e.printStackTrace();
        }
    }

    private void AplicarPagosMasivos(){        
        String lote = request.getParameter("lote") != null ? request.getParameter("lote") : "";
        String logica_aplicacion = request.getParameter("logica_aplicacion") != null ? request.getParameter("logica_aplicacion") : "";
        try {
            String resp =dao.AplicarPagosMasivos(usuario.getLogin(),lote, logica_aplicacion);
            this.reponseJson = "{\"respuesta\":\"" + resp + "\"}"; 
        } catch (Exception e) {
             e.printStackTrace();
        }
     }

    private void CargarComboLotes() {
         try {
            this.reponseJson =dao.CargarComboLotes(usuario.getLogin());
        } catch (Exception e) {
             e.printStackTrace();
        }
      
    }

    private void EliminarLotePagoMasivo() {
       String lote = request.getParameter("lote") != null ? request.getParameter("lote") : "";
        try {
            String resp =dao.EliminarLotePagoMasivo(usuario.getLogin(),lote);
            this.reponseJson = "{\"respuesta\":\"" + resp + "\"}"; 
        } catch (Exception e) {
             e.printStackTrace();
        }
    }
    
    private void CargarComboLogicaApli() {
         try {
            String lote = request.getParameter("lote") != null ? request.getParameter("lote") : "";
            this.reponseJson =dao.CargarComboLogicaApli(usuario.getLogin(),lote);
        } catch (Exception e) {
             e.printStackTrace();
        }
      
    }
    
    private void CierrePlanAlDia() {
        try {
            String resp =dao.CierrePlanAlDia(usuario.getLogin());
            this.reponseJson = "{\"respuesta\":\"" + resp + "\"}"; 
        } catch (Exception e) {
             e.printStackTrace();
        }
    }
    
     private void ReclasificarLotes() {
        String lote = request.getParameter("lote") != null ? request.getParameter("lote") : "";
        try {
            String resp =dao.ReclasificarLotes(usuario.getLogin(),lote);
            this.reponseJson = "{\"respuesta\":\"" + resp + "\"}"; 
        } catch (Exception e) {
             e.printStackTrace();
        }
    }

    private void resumenArchivoAsobancaria() {
        String idArchivo = request.getParameter("idArchivo") != null ? request.getParameter("idArchivo") : "";
        try {           
            this.reponseJson = dao.resumenArchivoAsobancaria(usuario.getLogin(),idArchivo);
        } catch (Exception e) {
             e.printStackTrace();
        }
    }

    private void cargarArchivoGenerarPago() {
        try {           
            this.reponseJson = dao.cargarArchivoGenerarPago(usuario.getLogin());
        } catch (Exception e) {
             e.printStackTrace();
        }
    }

    private void mostrarRefeInconsistencia() {
        String idArchivo = request.getParameter("IdArchivo") != null ? request.getParameter("IdArchivo") : "";
       this.reponseJson = dao.mostrarRefeInconsistencia(idArchivo);
    }

    private void generarLotePagos() {
        String idArchivo = request.getParameter("idArchivo") != null ? request.getParameter("idArchivo") : "";
       this.reponseJson = dao.generarLotePagos(usuario.getLogin(),idArchivo);
      }

    private void updateRefrenciaNegocio() {
        String idArchivo = request.getParameter("idArchivo") != null ? request.getParameter("idArchivo") : "";
        String idNegocio = request.getParameter("idNegocio") != null ? request.getParameter("idNegocio") : "";
        String idOrden = request.getParameter("idOrden") != null ? request.getParameter("idOrden") : "";
        String idRows = request.getParameter("idRows") != null ? request.getParameter("idRows") : "";
       this.reponseJson = dao.updateRefrenciaNegocio(idArchivo,idNegocio,idOrden,idRows,usuario.getLogin());
    }
    
    private void mostrarLogReferenciaPagos() {
        String idArchivo = request.getParameter("IdArchivo") != null ? request.getParameter("IdArchivo") : "";
       this.reponseJson = dao.mostrarLogReferenciaPagos(idArchivo);
    }

}

