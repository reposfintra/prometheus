/*
 * Nombre        UbicacionIngresarAction.java
 * Autor         Ing. Jesus Cuesta
 * Modificado    Ing. Sandra Escalante
 * Fecha         21 de junio de 2005, 02:42 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.Util;

public class CargarVariosAction extends Action {
    
    /** Creates a new instance of CompaniaCargarEstadosAction */
    public CargarVariosAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/" + request.getParameter("carpeta")+ "/" + request.getParameter("pagina")+"?sw=ok";
        
        //ESTO SE HACE EN TODAS LAS ACCIONES PARA CARGAR EL IDIOMA..
        String pagina = request.getParameter("pagina");
        HttpSession session = request.getSession();
        String distrito = (String) session.getAttribute("Distrito");
        try{
            int sw = Integer.parseInt(request.getParameter("sw"));
            if (sw == 1 ){//buscar origenes tramo
                model.tramoService.loadOrigenes();
                next = "/jsp/trafico/ubicacion/seleccionarTramo.jsp";
            }
            else if (sw == 2){//buscar destinos tramo
                String origen = request.getParameter("origen");
                model.tramoService.loadDestinos(origen);
                next = "/jsp/trafico/ubicacion/seleccionarTramo.jsp?c_origenselec="+origen;
            }
            if(sw==3){//cargar contactos x compania
                String cia = request.getParameter("c_cia");
                model.contactoService.buscarContactoCia(cia);
            }
            else if(sw==4){//carga estados
                String pais = request.getParameter("c_pais");
                model.estadoservice.listarEstado(pais);
            }
            else if(sw==5){//carga ciudades
                String pais = request.getParameter("c_pais");
                String estado = request.getParameter("c_estado");
                model.ciudadservice.listarCiudades(pais, estado);
            } else if(sw==6){//abrir pag de busqueda de compa�ias
                next = "/jsp/trafico/contacto/seleccionarCia.jsp?mostrar=NO";
            } else if(sw==7){//abrir pag de busqueda de estados en modificar ciudad
                next = "/jsp/trafico/ciudad/seleccionarEdo.jsp?mostrar=NO&pais="+request.getParameter("pais");
            } else if(sw==8){//buscar estados seleccionarEdo y buscarEstado
                String pais = request.getParameter("pais");////System.out.println("PAIS " + request.getParameter("pais"));
                String frase = request.getParameter("frase").toUpperCase();
                session.setAttribute("frase", frase);
                model.estadoservice.buscarEstadosxNombre(pais, frase);
                if (request.getParameter("ubicacion")!= null )
                    next = "/jsp/trafico/ubicacion/buscarEstado.jsp?mostrar=OK";
                else
                    next = "/jsp/trafico/ciudad/seleccionarEdo.jsp?mostrar=OK";
                
                if ( model.estadoservice.obtEstados().size() == 0 ){
                    next = "/jsp/trafico/mensaje/ErrorBusq.jsp?ruta=/jsp/trafico/ciudad/seleccionarEdo.jsp?mostrar=NO";
                }
            } else if(sw==9){//carga listados del grupo jdelarosa
                String grupo = request.getParameter("grupo");
                model.tablaGenService.obtenerInfoTablaGen("EMAIL", grupo);
            }
            else if(sw==10){
                DatosPlanillaRMT dp = model.rmtService.getDatosPlanillaRMT();
                model.rmtService.buscarPlanillasReinicio(dp.getNumpla(), dp.getPto_control_proxreporte());
                next+="?fechar="+request.getParameter("fechar");
            }else if(sw==11){
                String ingreso = request.getParameter("ingreso");
                String documento = request.getParameter("documento");
                String cliente = request.getParameter("cliente");
                String tipo_documento = "";
                String vec [] = request.getParameter("tipo_documento").split("/");
                if(vec.length>1 && vec != null)
                    tipo_documento = vec[0];
                String tipo_ingreso = request.getParameter("tipo_ingreso");
                String estado = request.getParameter("estados");
                String fecha_ini = request.getParameter("fechaInicio");
                String fecha_fin = request.getParameter("fechaFinal");
                String periodo = request.getParameter("periodo");
                model.ingresoService.consultaIngresos(distrito, cliente, periodo, tipo_ingreso, fecha_ini, fecha_fin, tipo_documento, documento, estado, ingreso);
            }            
            else if(sw==12){
                String numero_ingreso = (String) request.getParameter("numero_ingreso");
                String tipo_docuento  = (String) request.getParameter("tipo_doc");
                String moneda_local   = (String) session.getAttribute("Moneda");
                model.ingreso_detalleService.datosCabecera( distrito, numero_ingreso, tipo_docuento, "C");
                Ingreso_detalle cabecera = model.ingreso_detalleService.getCabecera();
                Vector vec = model.ingreso_detalleService.datosListadoItems( numero_ingreso, tipo_docuento, distrito );
                cabecera.setValor_tasa (0);
                for(int i = 0; i < vec.size (); i++){
                    Ingreso_detalle in = (Ingreso_detalle) vec.get (i);
                    in.setValor_saldo_factura ( in.getValor_saldo_factura_me() );
                    if( !in.getMoneda_factura ().equals (cabecera.getMoneda ()) ){
                        if( in.getValor_tasa () !=0 ){
                            cabecera.setValor_tasa (  Double.parseDouble ( com.tsp.util.UtilFinanzas.customFormat ("#,###.##########",  (in.getValor_tasa ()) ,10 ).replaceAll (",","") ) );
                            in.setValor_saldo_factura ( in.getValor_saldo_factura_me() * cabecera.getValor_tasa() );
                        }
                    }
                    in.setValor_total_factura((in.getValor_saldo_fing() - in.getValor_abono()) < 0 ? 0 : (in.getValor_saldo_fing() - in.getValor_abono()));
                }
                model.ingreso_detalleService.setVectorIngreso_detalle( vec );
            }   else if(sw==13){
                String fchi     = (String) request.getParameter ("fechaInicio")!=null?request.getParameter ("fechaInicio"):"";
                String fchf     = (String) request.getParameter ("fechaFinal")!=null?request.getParameter ("fechaFinal"):"";
                String trailer  = (String) request.getParameter ("trailer")!=null?request.getParameter ("trailer"):"";
                String cabezote = (String) request.getParameter ("cabezote")!=null?request.getParameter ("cabezote"):"";
                model.reporteGeneralService.consultaTrailers (fchi, fchf, trailer, cabezote);
                if ( cabezote.equals ("") && !trailer.equals ("") )
                    next = "/jsp/general/consultas/ListadoConsultaTrailer.jsp";
            }else if(sw==14){
                String numero_ingreso = (String) request.getParameter("numero_ingreso");
                String tipo_docuento  = (String) request.getParameter("tipo_doc");
                model.ingreso_detalleService.datosCabecera( distrito, numero_ingreso, tipo_docuento, "C");
                Vector vec = model.ingreso_detalleService.datosListadoItems( numero_ingreso, tipo_docuento, distrito );
                if( vec != null && vec.size() >0 )
                    next = "/controller?estado=Ingreso_detalle&accion=Update&opcion=1&numero_ingreso="+numero_ingreso+"&tipo_doc="+tipo_docuento+"&pagina="+request.getParameter("pagina");
                else
                    next = "/controller?estado=Ingreso_detalle&accion=Search&opcion=1&numero_ingreso="+numero_ingreso+"&tipo_doc="+tipo_docuento+"&pagina="+request.getParameter("pagina");
            } else if (sw==15) {
                if (model.cxpDocService.getEnproceso()){
                    next = "/migracion/procesoSaldosContables.jsp?msg=Ya se ejecuto el proceso";
                }
                else {
                    model.cxpDocService.setEnproceso();
                    com.tsp.util.Contabilidad.ActualizarSaldosContables as = new com.tsp.util.Contabilidad.ActualizarSaldosContables();
                    next = "/migracion/procesoSaldosContables.jsp?msg=El proceso ha sido ejecutado exitosamente.";
                }            
            } else if(sw == 16){
                next = "/jsp/trafico/ciudad/SeleccionarPais.jsp?sw=-1";
            }
            else if(sw==17){
                model.paisservice.buscarpaisnombre(request.getParameter("nombre").toUpperCase()+"%");
                next = "/jsp/trafico/ciudad/SeleccionarPais.jsp?sw="+model.paisservice.obtenerpaises().size();
            }
            else if(sw==18) {
                
                model.ciudadService.searchTreMapCiudades();
                model.paisservice.listarpaises();
                model.zonaService.listarZonasFronterisas();
                model.zonaService.listarZonasNoFronterisas();
                //System.out.println("Codigo "+request.getParameter("c_codigo").toUpperCase());
                model.TimpuestoSvc.buscarRicaAgencia( distrito, request.getParameter("c_codigo").toUpperCase(), Util.getFechaActual_String(1)+"-12-31" );
                TreeMap agencia = model.agenciaService.listar();
                Vector codigoRICA = model.TimpuestoSvc.getImpuestos();
                //System.out.println(codigoRICA.size());
                next = "/jsp/trafico/ciudad/auxiliar.jsp";
            } else if(sw==19){
                if( request.getParameter("abrir")!=null && !request.getParameter("abrir").equals("") ){
                    model.nitService.setPropietarios(null);
                    request.setAttribute("mensaje",null);
                }
                else{
                    model.nitService.buscarUsuarioPorNit_Nombre(request.getParameter("nit"));
                    Vector vec = model.nitService.getPropietarios();
                    if( vec == null || vec.size() == 0)
                        request.setAttribute("mensaje", "ok");
                }
                next = "/jsp/cxcobrar/ingresoMiscelaneo/ListaUsuarios.jsp";
            } else if( sw == 20 ){
                String planilla = request.getParameter("c_planilla")!=null?request.getParameter("c_planilla"):"";
                model.planillaService.getInfoPlanilla( planilla );
                if( model.planillaService.getPlanilla() == null )
                    request.setAttribute("mensaje", "No existen datos para la planilla "+planilla);
                next = "/placas/PlacaFit.jsp";
            }
            else if ( sw == 21 ){
               // model.placaService.listarPlacaXGurpo("T");
                next = "/placas/ListadoPlacasGrupo.jsp";
            }
            else if ( sw == 22 ){
                String trailer = request.getParameter("c_trailer")!=null?request.getParameter("c_trailer"):"";
                Planilla pla = model.planillaService.getPlanilla();
                String sql = "", msn = "";
               /* if ( pla != null ){
                    if( model.placaService.existePlacaXGurpo( trailer, "T" )  ){
                        sql += model.placaService.actualizarFitPlaca( trailer, "FIT");
                        sql += model.placaService.actualizarPlacaTrailer( pla.getNumpla(), trailer );
                        if( !pla.getPlatlr().equals("NA") )
                            sql += model.placaService.actualizarFitPlaca( pla.getPlatlr(), "DEFIT");
                        else
                            msn = ", Pero no se pudo actualizar el campo fit del trailer "+pla.getPlatlr() +", por que la placa no existe";
                    }
                    else
                        request.setAttribute("mensaje", "El trailer nuevo no existe o no pertenece a el grupo de trailer");
                }else
                    request.setAttribute("mensaje", "Se modifico correctamente los datos");*/
                
                if( !sql.equals("") ){
                    model.tService.crearStatement();
                    model.tService.getSt().addBatch(sql);
                    model.tService.execute();
                    request.setAttribute("mensaje", msn.equals("")?"Se modifico correctamente los datos":"Se modificar�n los datos"+msn);
                    model.planillaService.getInfoPlanilla( pla.getNumpla() );
                }
                next = "/placas/PlacaFit.jsp";
            }
            
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
    
}
