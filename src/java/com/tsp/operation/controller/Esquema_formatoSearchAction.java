/*******************************************************************
 * Nombre clase: Esquema_formatoSearchAction.java
 * Descripci�n: Accion para actualizar una forma esquema
 * Autor: Ing. Jose de la rosa
 * Fecha: 19 de octubre de 2006, 10:15 AM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

/**
 *
 * @author  EQUIPO13
 */
public class Esquema_formatoSearchAction extends Action{
    
    /** Creates a new instance of Esquema_formatoSearchAction */
    public Esquema_formatoSearchAction () {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        HttpSession session = request.getSession ();
        String dstrct = (String) session.getAttribute ("Distrito");
        String next = "/jsp/masivo/formato/Esquema_formatoListar.jsp";
        try{
            String mensaje = request.getParameter("opcion");
            if(mensaje!=null){

                if(mensaje.equalsIgnoreCase("listar")){
                    String codigo = request.getParameter("c_codigo")!=null?request.getParameter("c_codigo"):"";
                    String nombre = request.getParameter("c_nombre")!=null?request.getParameter("c_nombre"):"";
                    String titulo = request.getParameter ("c_titulo")!=null?request.getParameter ("c_titulo"):"";
                    model.esquema_formatoService.listEsquema_formato (dstrct, codigo, nombre, titulo);
                }
                
                else {
                    String codigo = request.getParameter("c_codigo")!=null?request.getParameter("c_codigo"):"";
                    String nombre = request.getParameter("c_nombre")!=null?request.getParameter("c_nombre"):"";
                    model.esquema_formatoService.searchEsquema_formato (codigo, dstrct, nombre);
                    model.tablaGenService.buscarRegistros("TFORTIP");
                    request.setAttribute ( "set_tipo", model.tablaGenService.obtenerTablas());
                    model.tablaGenService.buscarRegistros("TBLFORMA");
                    next = "/jsp/masivo/formato/Esquema_formatoModificar.jsp";
                }
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        this.dispatchRequest(next);
    }
    
}
