/* * EnvioCorreoAction.java * Created on 16 de junio de 2009, 16:52 */
package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.threads.HArchivosTransferenciaBancos;
import com.tsp.operation.model.threads.HExportarExel_Reporte_de_cumplidos;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.threads.HSendMail2;
/** * @author  Fintra */
public class EnvioCorreoAction extends Action{
    public EnvioCorreoAction() {    }
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
         try{
                HttpSession session     = request.getSession();
                Usuario     usuario     = (Usuario)session.getAttribute("Usuario");
                String      user        = usuario.getLogin(); 
                 
                String evento        = request.getParameter("opcion");
                
                String fromx1       = request.getParameter("from");
                String tox1       = request.getParameter("to");
                String asuntox1       = request.getParameter("asunto");
                String msgx1  = request.getParameter("mensajex");
                String copytox1= request.getParameter("copyto");
                String hiddencopytox1= request.getParameter("hiddencopyto");
                
                if(evento!=null){                   
                    if( evento.equals("enviarmsg")){ 
                        //.out.println("antes");
                        HSendMail2 hSendMail2=new HSendMail2();
                        //.out.println("antes de start");
                        hSendMail2.start( fromx1, tox1, copytox1, hiddencopytox1,  asuntox1, msgx1, "NAVI");
                        //.out.println("ssss");    
                    }               
                }
                String next  = "/jsp/mail/prueba_correo.jsp?msj=Proceso Iniciado"; 
                RequestDispatcher rd = application.getRequestDispatcher(next);
                if(rd == null)
                   throw new Exception("No se pudo encontrar "+ next);
                rd.forward(request, response);   
            
        } catch (Exception e){
             throw new ServletException(e.getMessage());
        }           
    }
    
    
    
}
