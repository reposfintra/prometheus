/*************************************************************************
 * Nombre                       ClientactAnularAction.java               *
 * Descripci�n                  Clase Action para anular                 *
 *                              actividad                                *
 * Autor.                       Ing. Diogenes Antonio Bastidas Morales   *
 * Fecha                        30 de agosto de 2005, 08:10 AM           * 
 * Versi�n                      1.0                                      * 
 * Coyright                     Transportes Sanchez Polo S.A.            *
 *************************************************************************/ 


package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Diogenes
 */
public class ClientactAnularAction extends Action{
    
    /** Creates a new instance of ClientactAnularAction */
    public ClientactAnularAction() {
    }
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
        String distrito = (String) session.getAttribute("Distrito");
        String next = "/jsp/trafico/mensaje/MsgAnulado.jsp";
        Date fecha = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String now = format.format(fecha);
        int sw=0,a=0;
        
        ClienteActividad clientact = new ClienteActividad();
        clientact.setCodActividad(request.getParameter("codact"));        
        clientact.setCodCliente(request.getParameter("codcli"));
        clientact.setTipoViaje(request.getParameter("tipo"));        
        clientact.setUser_update(usuario.getLogin());
        clientact.setDstrct(usuario.getCia());
        clientact.setLast_update(now);
        try{
            model.clientactSvc.anularClientAct(clientact);           
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
