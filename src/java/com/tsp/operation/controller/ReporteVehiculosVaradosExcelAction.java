/********************************************************************
 *      Nombre Clase....   ReporteVehiculosVaradosExcelAction.java
 *      Autor...........   Ing. Luis Eduardo Frieri
 *      Fecha...........   31-01-2007
 *      Versi�n.........   1.0
 *      Copyright.......   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;

public class ReporteVehiculosVaradosExcelAction extends Action{
    
    
    public ReporteVehiculosVaradosExcelAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String next = "/jsp/trafico/reportes/ReporteVehiculosDemoradosRespuesta.jsp&mensaje=ok";
        
        try{
                
                HiloReporteVarados  hilo = new HiloReporteVarados();
                hilo.start ( model.rmtService.getReportesPlanilla() , usuario.getLogin ());
                next="/jsp/trafico/reportes/ReporteVehiculosDemorados.jsp?msg=Lista Exportada";
                
        }catch (SQLException e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
}
