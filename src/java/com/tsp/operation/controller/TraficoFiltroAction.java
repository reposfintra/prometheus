/********************************************************************
 *      Nombre Clase.................   TraficoFiltroAction.java
 *      Descripci�n..................   Action que se encarga de eliminar un filtro creado por el usuario
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import javax.servlet.*;
import org.apache.log4j.Logger;
import javax.servlet.http.*;
/**
 *
 * @author  David A
 */
public class TraficoFiltroAction extends Action{
    
    /** Creates a new instance of TraficoFiltro */
    public TraficoFiltroAction() {
    }
    static Logger logger = Logger.getLogger(TraficoFiltroAction.class);
    
    public void run() throws ServletException, InformationException {
        logger.info("entro en run ");
         try{
             
            String user       =""+ request.getParameter ("usuario");
            String linea      =""+ request.getParameter ("linea");
            String conf       = ""+request.getParameter ("conf");
            String clas       = ""+request.getParameter ("clas");
            String var1       = ""+request.getParameter ("var1");
            String var2       = ""+request.getParameter ("var2");
            String var3       = ""+request.getParameter ("var3");
            String []filtros  = request.getParameterValues ("Cfiltro");
            String zonasUsuario = request.getParameter("zonasUsuario");//23.06.2006
            
            String cadena="";
            for(int i=0; i<filtros.length; i++){
                cadena = cadena+filtros[i];
            }
            logger.info("Cadena de Salida"+cadena);
            
            Vector vectorFiltro = new Vector();
            vectorFiltro=model.traficoService.obtenerVectorFiltros(filtros);
            cadena="";
            for(int i=0;i<vectorFiltro.size();i++){
                cadena=cadena+vectorFiltro.elementAt(i);
            }
            logger.info("Vector de filtros: "+cadena);
            
            cadena="";
            Vector vectorFiltroO = new Vector();
            vectorFiltroO=model.traficoService.ordenarVectorFiltro(vectorFiltro);
            cadena="";
            for(int i=0;i<vectorFiltroO.size();i++){
                cadena=cadena+vectorFiltroO.elementAt(i);
            }
            logger.info("Vector de filtros Ordenado: "+cadena);
            
            String filtro= model.traficoService.filtro(vectorFiltroO);
            String filtroPuro = filtro;
            logger.info("Filtro final: "+ filtro);
            
            //model.traficoService.generarVZonas(user);
            /*  Modificado por: Andr�s Maturana
             *  Fecha: 02.05.2006 */
            model.traficoService.gennerarVZonasTurno(user);
            
            logger.info("Genrerar zonas ");
            Vector zonas = model.traficoService.obdtVectorZonas();
            String validacion=" zona='-1'";
            if ((zonas == null)||zonas.size()<=0){logger.info("no se pudo generar zonas");}else{
               validacion = model.traficoService.validacion(zonas);
            }
            logger.info("\nValidacion final: "+ validacion);
            
            if(filtro.length()>1)
                filtro="where "+validacion +" and "+filtro;
            if(filtro.equals(""))
                filtro="where "+validacion+filtro;
             if(var3.equals("null") )
                var3=model.traficoService.obtVar3();
            if(linea.equals("null")||linea.equals(""))
                linea=model.traficoService.obtlinea();
            if(conf.equals("null")||conf.equals(""))
                conf = model.traficoService.obtConf();      
            if(clas.equals("null")||clas.equals(""))
                clas="";
            logger.info("conf antes de consulta final"+conf);
            Vector colum= new Vector();
            
            model.traficoService.listarColTrafico(var3);
            Vector col = model.traficoService.obtColTrafico();
            model.traficoService.listarColtrafico2(col);

            colum = model.traficoService.obtenerCampos(linea);
            System.out.println("FILTRO--->" + filtro);
            String consulta = model.traficoService.getConsulta(conf, filtro, clas);
            
            System.out.println("CONSULTA-------------------->"+ consulta);
            model.traficoService.generarTabla(consulta, colum);
            
            ConfTraficoU vista = new ConfTraficoU ();
            vista.setConfiguracion (conf.replaceAll("-_-","'"));
            vista.setFiltro (filtro);
            vista.setLinea (linea);
            vista.setClas (clas);
            vista.setVar1 (var1);
            vista.setVar2 (var2);
            vista.setVar3 (var3);
            vista.setCreation_user (user);
            model.traficoService.actualizarConfiguracion(vista); 
            
            
            filtro=model.traficoService.codificar(filtro);
            String next = "/jsp/trafico/controltrafico/vertrafico2.jsp?var3="+var3+"&var1="+var1+"&var2="+var2+"&filtro="+filtro+"&clas="+clas+"&reload=&filtroP="+filtroPuro+"&zonasUsuario="+zonasUsuario;
            logger.info("next en Filtro"+next);  
            RequestDispatcher rd = application.getRequestDispatcher(next);
            
            
              
            if(rd == null)
                throw new Exception("No se pudo encontrar "+ next);
            rd.forward(request, response); 
        }
        catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Accion:"+ e.getMessage());
        } 
        
        
    }
    
}
