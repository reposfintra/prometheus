/*
 * Nombre        LoggingOutAction.java
 * Descripci�n   Clase que realiza las acciones pertinentes al fin de sessi�n
 *               de SLT.
 * Autor         Alejandro Payares
 * Fecha         24 de enero de 2006, 09:34 AM
 * Version       1.0
 * Coyright      Transportes Sanchez Polo SA.
 */

package com.tsp.operation.controller;

import org.apache.log4j.Logger;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.Util;
import com.tsp.util.connectionpool.PoolManager;

/**
 * Clase que realiza las acciones pertinentes al fin de sessi�n de SLT.
 * @author Alejandro Payares
 */
public class LoggingOutAction extends Action{
    
    private Logger logger = Logger.getLogger(LoggingOutAction.class);
    
    /**
     * Crea una nueva instancia de LoggingOutAction
     * @autor  Alejandro Payares
     */
    public LoggingOutAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String nombreUsuario = null;
        HttpSession session = request.getSession();
        String login_into=(String)session.getAttribute("login_into");
        ////System.out.println("session obtenida del request");
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        String dispatcher=""; 
        if (usuario != null )
            nombreUsuario = usuario.getNombre();
        else
            nombreUsuario = "Usuario de FINTRAVALORES";
        ////System.out.println("nombre de usuario: "+nombreUsuario);
        logger.info( nombreUsuario + " ha salido del sistema.");
        ////System.out.println("redireccionando a la pagina index.jsp");
        PoolManager.getInstance().release();
        // Redireccionar a la p�gina indicada.
        session.invalidate(); 
        Util.cerrarSession(usuario.getLogin());
        if( login_into != null ) {
        switch (login_into) {
            case "Fenalco":
                dispatcher="/index_fen.jsp";
                break;
            case "Fenalco_bol":
                dispatcher="/index_fen_bol.jsp";
                break;
            default:
                dispatcher="/index.jsp";
                break;

        }
        } else {
            dispatcher="/index.jsp";
        }
        this.dispatchRequest(dispatcher);
    }
    
}
