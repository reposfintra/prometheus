/*
 * CambioPropietarioOPAction.java
 *
 * Created on 12 de marzo de 2007, 07:43 PM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  Osvaldo P�rez Ferrer
 */

import java.io.*;
import java.util.*;
import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.threads.*;
import javax.servlet.http.*;


public class CambioPropietarioOPAction extends Action{
    
    public void run() throws ServletException, InformationException {
        
        try{
            
            String next          = "/jsp/cxpagar/generarOP/cambiarPropietarioOP.jsp?";
            HttpSession session  = request.getSession();
            Usuario usuario      = (Usuario) session.getAttribute("Usuario");
            String evento        = request.getParameter("evento");
            
            if( evento != null ){
                
                if( evento.equals("SEARCH") ){
                    
                    String planilla = request.getParameter("planilla");
                    model.OpSvc.buscarOPPlanilla(planilla);
                    model.ChequeXFacturaSvc.setProveedor("");
                    
                    next += "msj=";
                    
                }else if( evento.equals("NOMBRE") ){
                    
                    String nit = request.getParameter("provee");
                    
                    model.ChequeXFacturaSvc.setUsuario(usuario);
                    model.ChequeXFacturaSvc.searchProveedores( nit );
                    model.ChequeXFacturaSvc.setProveedor( nit );
                    List lista = model.ChequeXFacturaSvc.getProveedores();
                    
                    if( lista != null && lista.size() > 0 ){
                        Hashtable ht = (Hashtable) lista.get(0);
                        request.setAttribute("nombre_proveedor", ht.get("nombre") );
                    }
                    request.setAttribute("nit", nit );
                    
                }else if( evento.equals("MODIFICAR") ){
                    
                    String nit = request.getParameter("provee");
                    
                    String mens = model.OpSvc.cambiarPropietarioOP( nit, usuario.getLogin() );
                    String ok   = mens.startsWith("No")? "":"ok";
                    
                    next += "msj="+mens+"&ok="+ok;
                }
                
            }
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new Exception("No se pudo encontrar "+ next);
            rd.forward(request, response);
        } catch (Exception e){
            throw new ServletException(e.getMessage());
        }
    }
    
}
