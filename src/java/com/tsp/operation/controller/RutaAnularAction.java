/*
 * RutaAnularAction.java
 *
 * Created on 29 de junio de 2005, 05:05 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Jcuesta
 */
public class RutaAnularAction extends Action{
    
    /** Creates a new instance of RutaAnularAction */
    public RutaAnularAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/jsp/trafico/ruta/mensajeRuta.jsp";
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        
        try{
             String cia = "";
             String origen = "";
             String destino = "";
             String secuencia = "";
             String via = "";
            
            if(request.getParameter("modif")==null){
                cia = request.getParameter("c_cia");
                origen = request.getParameter("c_origen");
                destino = request.getParameter("c_destino");
                secuencia = request.getParameter("c_secuencia");
                via = request.getParameter("c_via");

                Ruta r = new Ruta();
                r.setCia(cia);
                r.setOrigen(origen);
                r.setDestino(destino);
                r.setSecuencia(secuencia);
                r.setUsuario(usuario.getLogin());
                r.setVia(via);

                model.rutaService.setRuta(r);
                model.rutaService.anularRuta();
            
            }
            else{
                cia = request.getParameter("c_cia");
                origen = request.getParameter("c_origen");
                destino = request.getParameter("c_destino");
                secuencia = request.getParameter("c_secuencia");
                via = request.getParameter("c_via");

                String anexoDesc = request.getParameter("descAdd");
                String ciudad_o = request.getParameter("ciudad_orig");
                String ciudad_d = request.getParameter("ciudad_dest");

                String descripcion = request.getParameter("descripcion");

                Ruta r = new Ruta();
                r.setCia(cia);
                r.setOrigen(origen);
                r.setDestino(destino);
                r.setSecuencia(secuencia);
                r.setUsuario(usuario.getLogin());
                r.setVia(via);

                //si la descripcion adicional es diferente de vacio se coloca si no se deja la que tenia 
                if (!anexoDesc.equals("")){
                    descripcion = ciudad_o + "-" + ciudad_d + "(" + anexoDesc + ")";
                }
            
                r.setDescripcion(descripcion);
                
                model.rutaService.setRuta(r);
                next = "/jsp/trafico/ruta/anularRuta.jsp?lista=ok&reload=ok";
                model.rutaService.modificarRuta();
                model.tramoService.buscarTramo("","","");
                model.rutaService.buscarRuta(cia, origen, destino,secuencia);
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
