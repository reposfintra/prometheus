 /*
  * TraficoEliminaritemAction.java
  *
  * Created on 8 de octubre de 2005, 11:28 AM
  */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;import java.io.*;

//Logger
import org.apache.log4j.*;
/**
 *
 * @author  dlamadrid
 */
public class TraficoFiltroeliminaritemAction extends Action{
    
    Logger logger = Logger.getLogger (TraficoFiltroeliminaritemAction.class);
    
    /** Creates a new instance of TraficoEliminaritemAction */
    public TraficoFiltroeliminaritemAction () {
    }
    
    public void run () throws ServletException, InformationException {
        ////System.out.println ("entro en run ");
        try{
            String cFiltro = ""+request.getParameter ("Cfiltro");
            ////System.out.println ("cFiltro "+cFiltro);
            String campo       =""+ request.getParameter ("campo");
            String validacion        =""+ request.getParameter ("validacion");
            String salidaAlt = request.getParameter ("salida2");
            String zonasUsuario = request.getParameter("zonasUsuario");
            
            HttpSession session= request.getSession ();
            String salida = (String)session.getAttribute ("salida");
            int tamPalabra=cFiltro.length ();
            
            if( salida.indexOf (cFiltro)==-1 ){
                salida = salidaAlt;
            }
            
            
            ////System.out.println ("salida: "+ salida);
            
            int in1=salida.indexOf (cFiltro);
            
            logger.info("cFiltro: " + cFiltro);
            logger.info("salida: " + salida);
            logger.info("in1: " + in1 );
            
            
            int in2 =in1+tamPalabra+11+tamPalabra+9;
            
            String option= "<option value='"+cFiltro+"' SELECTED>"+cFiltro+"</option>";
            ////System.out.println ("option "+option);
            //salida = salida.replaceAll(option, "");
            in1= in1-15;
            String salida1 =  salida.substring (0,in1);
            String salida2 =  salida.substring (in2,salida.length ());
            ////System.out.println ("salida1 "+salida1);
            ////System.out.println ("salida2 " + salida2);
            salida=salida1+salida2;
            session.setAttribute ("salida", salida);
            ////System.out.println ("salida seteada "+salida);
            //String linea="";
            String linea = request.getParameter("linea");
            String next = "/jsp/trafico/controltrafico/filtro.jsp?linea="+linea+"&campo="+campo+"&validacion="+validacion+"&reload="+"&zonasUsuario="+zonasUsuario;
            ////System.out.println ("next en Listas para Filtro"+next);
            RequestDispatcher rd = application.getRequestDispatcher (next);
            if(rd == null)
                throw new Exception ("No se pudo encontrar "+ next);
            rd.forward (request, response);
        }
        catch(Exception e){
            e.printStackTrace();
            throw new ServletException ("Accion:"+ e.getMessage ());
        }
    }
    
}
