/***************************************
 * Nombre       GenerarOPAction.java 
 * Autor        FERNEL VILLACOB DIAZ
 * Fecha        18/10/2005
 * versi�n      1.0
 * Copyright    Transportes Sanchez Polo S.A.
 *******************************************/


package com.tsp.operation.controller;


import java.io.*;
import java.util.*;
import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.threads.*;
import javax.servlet.http.*;


public class GenerarOPAction extends Action{
    
   
    public void run() throws ServletException, InformationException {
        
        try{
            
            String next          = "/jsp/cxpagar/generarOP/GenerarOP.jsp?";
            HttpSession session  = request.getSession(); 
            Usuario usuario      = (Usuario) session.getAttribute("Usuario");
            String  distrito     = usuario.getDstrct();
             
//            HttpSession session = request.getSession(); 
//            Usuario usuario = (Usuario) session.getAttribute("Usuario");
//            String agencia_usuario = usuario.getId_agencia();
//            model.tablaGenService.buscarReferencias("TAGECONT", agencia_usuario);
//            
//            LinkedList l = model.tablaGenService.getReferencias();
//            TablaGen tg = (TablaGen)l.getFirst();
//            String agenciaContable = tg.getTable_code();
//
//            model.tablaGenService.buscarReferencias("TUNIDADES", tg.getTable_code());
//            LinkedList l1 = model.tablaGenService.getReferencias();
//            TablaGen tg1 = (TablaGen)l1.getFirst();
//            String unidadNegocio = tg1.getTable_code();
//
//            session.setAttribute("agContable", agenciaContable );
//            session.setAttribute("unidadC", unidadNegocio );

            
            String fechaInicial  = request.getParameter("fechaInicio");
            String fechaFinal    = request.getParameter("fechaFinal");
            String HC            = request.getParameter("HC");
            String user          = request.getParameter("usuario");
            
            //verificamos si hay un proceso activo
            if (!model.LiqOCSvc.getEnproceso()){
                model.LiqOCSvc.setEnproceso();
                HGenerarOPs  hilo  = new HGenerarOPs();
                hilo.start(model, fechaInicial, fechaFinal, usuario,HC, distrito);  
                next += "msj=El proceso de Generaci�n de Facturas de OC se est� llevando a cabo, puede consultar su estado en Seguimiento de Procesos"; 
            }else{
                next += "msj=Hay un proceso de Generaci�n de Facturas de OC realizandose. Debe esperar que este termine para iniciar otro.<br>Si desea consultar el estado de este proceso ingrese a PROCESOS/Seguimiento de Procesos";
            }
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new Exception("No se pudo encontrar "+ next);
            rd.forward(request, response);            
        } catch (Exception e){
             throw new ServletException(e.getMessage());
        }
    }
    
}
