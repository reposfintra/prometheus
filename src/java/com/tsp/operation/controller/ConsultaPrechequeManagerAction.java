/*
 * ConsultaPrechequeManagerAction.java
 *
 * Created on 18 de julio de 2006, 10:09 AM
 */

package com.tsp.operation.controller;

import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import org.apache.log4j.*;

public class ConsultaPrechequeManagerAction extends Action {
    
    Logger logger = Logger.getLogger(this.getClass());
    
    /** Creates a new instance of ConsultarEgresosManagerAction */
    public ConsultaPrechequeManagerAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "/jsp/cxpagar/consultaEgresos/consultarEgresos.jsp";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");  
        String opc = request.getParameter("opc");
        try{
            
            if( opc.equals("ver") ){
                String id = request.getParameter("id");
                Egreso eg = new Egreso();
                eg.setDocument_no( id );                
                model.prechequeSvc.setEgreso( eg );
                model.prechequeSvc.obtenerDetallePrecheque();
                model.prechequeSvc.obtenerCabeceraPrecheque();
                next = "/jsp/cxpagar/consultaPrecheques/verPrecheque.jsp";
            } else if ( opc.equals("cargar") ){
                
                String factura = request.getParameter("factura");
                String id = request.getParameter("id");
                String nit = request.getParameter("nit");
                String nitbe = request.getParameter("nitbe");
                String agencia = request.getParameter("agencia");
                String banco = request.getParameter("banco");
                String sucursal = request.getParameter("sucursal");
                String fechai = request.getParameter("FechaI");
                String fechaf = request.getParameter("FechaF");
                String estadoCon = request.getParameter("estadoCon");
                String usercre = request.getParameter("usercre")!=null ? request.getParameter("usercre").toUpperCase() : request.getParameter("usercre");
                
                logger.info("SERIAL: " + id);
                logger.info("FACTURA: " + factura);
                logger.info("PROVEEDOR: " + nit);
                logger.info("BENEFICIARIO: " + nitbe);
                logger.info("AGENCIA: " + agencia);
                logger.info("BANCO: " + banco);
                logger.info("SUCURSAL: " + sucursal);
                logger.info("FECHAI: " + fechai);
                logger.info("FECHAF: " + fechaf);
                logger.info("USUARIO CREO: " + usercre);
                
                request.setAttribute("id", id);
                request.setAttribute("fechai", fechai );
                request.setAttribute("fechaf", fechaf );
                request.setAttribute("factura", factura );
                request.setAttribute("nit", nit );
                request.setAttribute("nitbe", nitbe);
                request.setAttribute("agencia", agencia );
                request.setAttribute("banco", banco );
                request.setAttribute("sucursal", sucursal );
                request.setAttribute("usercre", usercre);
                request.setAttribute("estadoCon", estadoCon);
                
                String target =  request.getParameter("target")!=null ? request.getParameter("target") : "";
                List ListaAgencias = model.ciudadService.ListarAgencias();
                TreeMap tm = new TreeMap();
                if(ListaAgencias.size()>0) {
                    Iterator It3 = ListaAgencias.iterator();
                    while(It3.hasNext()) {
                        Ciudad  datos2 =  (Ciudad) It3.next();
                        tm.put("["+datos2.getCodCiu()+"] "+datos2.getNomCiu(), datos2.getCodCiu());
                    }
                }
                request.setAttribute("agencias", ListaAgencias);
                request.setAttribute("agencias_tm", tm);
                model.servicioBanco.setBanco(model.servicioBanco.obtenerNombresBancos());
                
                if( target.length()!=0 && target.compareTo("sucursales")==0 ){
                    model.servicioBanco.loadSucursalesAgencia(agencia, banco, (String) session.getAttribute("Distrito"));
                }
                
                next = "/jsp/cxpagar/consultaPrecheques/consultaPrecheque.jsp";
            } else if ( opc.equals("search") ){
                
                String factura = request.getParameter("factura");
                String id = request.getParameter("id");
                String nit = request.getParameter("nit");
                String nitbe = request.getParameter("nitbe");
                String agencia = request.getParameter("agencia");
                String banco = request.getParameter("banco");
                String sucursal = request.getParameter("sucursal");
                String fechai = request.getParameter("FechaI");
                String fechaf = request.getParameter("FechaF");
                String estadoCon = request.getParameter("estadoCon");
                String usercre = request.getParameter("usercre")!=null ? request.getParameter("usercre").toUpperCase() : request.getParameter("usercre");
                
                logger.info("SERIAL: " + id);
                logger.info("FACTURA: " + factura);
                logger.info("PROVEEDOR: " + nit);
                logger.info("BENEFICIARIO: " + nitbe);
                logger.info("AGENCIA: " + agencia);
                logger.info("BANCO: " + banco);
                logger.info("SUCURSAL: " + sucursal);
                logger.info("FECHAI: " + fechai);
                logger.info("FECHAF: " + fechaf);
                logger.info("USUARIO CREO: " + usercre);
                
                String dstrct = (String) session.getAttribute("Distrito");
                System.out.println(" estado "+estadoCon);
                model.prechequeSvc.consultaPrecheque(id, factura, nit, nitbe, agencia, banco, sucursal, usercre, fechai, fechaf, dstrct, estadoCon);                
                
                next = "/jsp/cxpagar/consultaPrecheques/consultaPrechequeRpta.jsp";
                if( model.prechequeSvc.getEgresos().size()==0 ){
                    next += "?msg=No se encontaron resultados.";
                }
            }
            
        }catch(Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);    
    }
    
}
