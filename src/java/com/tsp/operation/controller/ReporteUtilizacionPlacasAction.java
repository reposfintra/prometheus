/*
 * ReporteProduccionEquiposAction.java
 *
 * Created on 20 de abril de 2007, 06:16 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;
import com.tsp.util.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  EQUIPO26
 */
public class ReporteUtilizacionPlacasAction extends Action{
    
    /** Creates a new instance of ReporteProduccionEquiposAction */
    public ReporteUtilizacionPlacasAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/jsp/general/exportar/ReporteUtilizacionPlacas.jsp";
        HttpSession session = request.getSession ();
        Usuario usuario     = (Usuario) session.getAttribute ("Usuario");
        String distrito     = (String)  session.getAttribute ("Distrito");
        String fchi         = (String)  request.getParameter ("fechaInicio");
        String fchf         = (String)  request.getParameter ("fechaFinal");
        String agencia      = (String)  request.getParameter ("agencia");
        try{
            ReporteUtilizacionPlacasThreads hilo = new ReporteUtilizacionPlacasThreads();
            hilo.start( fchi, fchf, agencia, usuario.getLogin ());
            next += "?msg=La generacion del Reporte ha iniciado...<br>Para realizar el seguimiento del proceso haga clic en el vinculo&ruta=PROCESOS/Seguimiento de Procesos";
        }catch (Exception ex){
            throw new ServletException ("Error en ReporteUtilizacionPlacasAction......\n"+ex.getMessage ());
        }
        this.dispatchRequest (next); 
    }
    
}
