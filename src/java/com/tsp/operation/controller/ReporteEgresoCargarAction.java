/*
 * ReporteEgresoAction.java
 *
 * Created on 13 de septiembre de 2005, 10:43 AM
 */

package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import com.tsp.operation.model.threads.*;

import org.apache.log4j.*;
/**
 *
 * @author  Administrador
 */
public class ReporteEgresoCargarAction extends Action {
    
    Logger logger = Logger.getLogger(this.getClass());
    
    /** Creates a new instance of ReporteEgresoAction */
    public ReporteEgresoCargarAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        try{
            String opcion   = request.getParameter("Opcion");
            String agencia  = (request.getParameter("agencia")!=null)?request.getParameter("agencia"):"";
            String fechai   = (request.getParameter("fechai")!=null)?request.getParameter("fechai"):"";
            String fechaf   = (request.getParameter("fechaf")!=null)?request.getParameter("fechaf"):"";
            String usuario  = (request.getParameter("usuario")!=null)?request.getParameter("usuario"):""; //Modificacion  04/01/2006
            
            String banco = request.getParameter("banco")!=null ? request.getParameter("banco") : "";
            
            logger.info("OPCION: " + opcion);
            logger.info("AGENCIA: " + agencia);
            logger.info("BANCO: " + banco);
            logger.info("FECHA INICIO: " + fechai);
            logger.info("FECHA FINAL: " + fechaf);
            logger.info("USUARIO: " + usuario);
            
            String next = "/RelacionEgreso/ParametrosReporte.jsp";
            HttpSession session = request.getSession();
            
            String target =  request.getParameter("target")!=null ? request.getParameter("target") : "";
            
            logger.info("TARGET: " + target);
            //String agc = request.getParameter("agc")!=null ? request.getParameter("agc") : "";
            
            //logger.info("AGC: " + agc);            
            
            List ListaAgencias = model.ciudadService.ListarAgencias();
            TreeMap tm = new TreeMap();
            if(ListaAgencias.size()>0) {
                Iterator It3 = ListaAgencias.iterator();
                while(It3.hasNext()) {
                    Ciudad  datos2 =  (Ciudad) It3.next();
                    tm.put("["+datos2.getCodCiu()+"] "+datos2.getNomCiu(), datos2.getCodCiu());
                    //out.print("<option value='"+datos2.getCodCiu()+"'>["+datos2.getCodCiu()+"] "+datos2.getNomCiu()+"</option> \n");
                }
            }
            request.setAttribute("agencias", ListaAgencias);
            request.setAttribute("agencias_tm", tm);
            
            if( target.length()!=0 && target.compareTo("bancos")==0 ){
                model.servicioBanco.loadBancos(agencia, (String) session.getAttribute("Distrito"));
                model.servicioBanco.setSucursal(new TreeMap());
                if( agencia.equals("OP") ){
                    model.servicioBanco.obtenerNombresBancos();
                }
                if( banco.length()!=0 ){
                    model.servicioBanco.loadSucursalesAgencia(agencia, banco, (String) session.getAttribute("Distrito"));
                }
            } else if( target.length()!=0 && target.compareTo("sucursales")==0 ){
                model.servicioBanco.loadSucursalesAgencia(agencia, banco, (String) session.getAttribute("Distrito"));
                if( agencia.equals("OP") ){
                    model.servicioBanco.obtenerSucursalesBanco(banco);
                }
            }
                        
            request.setAttribute("fechai", fechai );
            request.setAttribute("fechaf", fechaf );
            request.setAttribute("usuario", usuario );
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en ReporteEgresoAction .....\n"+e.getMessage());
        }
    }
    
}
