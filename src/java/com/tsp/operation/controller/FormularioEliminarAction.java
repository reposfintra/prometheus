/*
 * FormularioEliminarAction.java
 *
 * Created on 1 de diciembre de 2006, 03:33 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  dbastidas
 */
public class FormularioEliminarAction extends Action{
    
    /** Creates a new instance of FormularioEliminarAction */
    public FormularioEliminarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        String next="";
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String formato  = request.getParameter("formato");
        String campo_val = "";
        String tabla = "";
        int i=0;
        
        Vector campos = model.formato_tablaService.getVecCampos();
        for( i=0; i < campos.size() ; i++){
            Formato_tabla reg = (Formato_tabla) campos.get(i);
            tabla = reg.getTabla();
            if( reg.getTipo_campo().equals("DO") || reg.getTipo_campo().equals("INT") ){
                reg.setValor_campo( request.getParameter(reg.getCampo_tabla())!=null?request.getParameter(reg.getCampo_tabla()).toUpperCase():"0" );
            }else{
                reg.setValor_campo( request.getParameter(reg.getCampo_tabla())!=null?request.getParameter(reg.getCampo_tabla()).toUpperCase():"" );
            }
        }
        //agrego los campos de para factura migracion
        if(tabla.equals("factura_migracion")){
            System.out.println("Agrega los campos");
            Formato_tabla reg = new Formato_tabla();
            reg.setTabla(tabla);
            reg.setCampo_tabla("formato");
            reg.setTipo_campo("TXT");
            reg.setValor_campo(formato);
            reg.setPrimaria("S");
            reg.setValidar("N");
            campos.add(reg);
            //district
            reg = new Formato_tabla();
            reg.setTabla(tabla);
            reg.setCampo_tabla("dstrct");
            reg.setTipo_campo("TXT");
            reg.setValor_campo(usuario.getDstrct());
            reg.setPrimaria("S");
            reg.setValidar("N");
            campos.add(reg);
        }
        next="/jsp/general/form_tabla/Mensaje.jsp?formato="+formato+"&tabla="+tabla;
        try{
            model.formularioService.eliminar(campos);
            next+="&mensaje=Registro eliminado exitosamente&sw=OK";
        }catch (Exception e){
            next+="&mensaje=El formato no esta bien creado, por favor verifiquelo";
            this.dispatchRequest(next);
            e.printStackTrace();
        }
        this.dispatchRequest(next);
    }
    
}
