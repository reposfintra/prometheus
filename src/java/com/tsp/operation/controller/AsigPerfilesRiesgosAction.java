/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import static com.sun.corba.se.spi.presentation.rmi.StubAdapter.request;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.controller.Action;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.AsigPerfilRiesgoAnalista;
import com.tsp.operation.model.beans.Rol;
import com.tsp.operation.model.beans.Usuario;
import java.util.ArrayList;
//import java.util.Vector;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Roberto Parra
 */
public class AsigPerfilesRiesgosAction  extends Action  {
    
    private final int LISTAR_ANALISTAS = 1;   
    private final int LISTAR_ANALISTAS_FABRICA = 2;
    private final int LISTAR_PERFILES_RIESGOS = 3;
    private final int ELIMINAR_ASIGNACION_ANALISTA = 4;
    private final int ASIGNAR_PERFIL_ANALISTA = 5;
    private final int RETORNAR_DATOS_DEL_FORMULARIO = 6;
    private final int MODIFICAR_ASIGNACION = 7;
    private final int LISTAR_ROLES = 8;
    private final int RETORNAR_UNIDADES_DEL_PERFIL = 9;
    private final int CAMBIAR_ESTADO_ANALISTA = 10;
    
    @Override
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
           //System.out.println("Usuario login");
        String next= "";
        try {
            HttpSession session  = request.getSession();
            Usuario usuario = (Usuario)session.getAttribute("Usuario");
            String asuarioSesion= usuario.getLogin();
            String dstrct = usuario.getDstrct();
            String creation_user = asuarioSesion;
            String user_update = asuarioSesion;
            int opcion = Integer.parseInt((request.getParameter("opcion")!=null?request.getParameter("opcion"):"0"));
            String urlpagina = "/"+request.getParameter("carpeta") +"/"+request.getParameter("pagina");
            
            
            String nit = request.getParameter("nit");
            //String nombre = request.getParameter("nombre");
            String perfil = request.getParameter("perfil");
            String id_perfil = request.getParameter("id_perfil");
            String estado = request.getParameter("estado_asig");
            String idRol = request.getParameter("idRol");
            String perfiles_selecionados = request.getParameter("perfiles_selecionados");
            String monto_decision = request.getParameter("monto_decision");
            String monto_minimo_decision = request.getParameter("monto_minimo_decision");
            
            String form = request.getParameter("form");// retorna el nombre del formulario desde el que estoy accedindo
             
            switch (opcion) {
                case LISTAR_ANALISTAS :
                     ListarAnalistas();
                    break;
                case LISTAR_ANALISTAS_FABRICA:
                     ListarAnalistasFabrica(form);
                     break;
                case LISTAR_PERFILES_RIESGOS:
                     ListarPerfilesRiesgos();
                     break;
                case LISTAR_ROLES:
                     ListarRoles();
                     break;
                case RETORNAR_DATOS_DEL_FORMULARIO:
                      RetornarDatosFormEdicion(nit);
                  //  KHGFD
                     break;
                case ELIMINAR_ASIGNACION_ANALISTA:
                     EliminarAsig(nit,id_perfil);
                     break;
                case ASIGNAR_PERFIL_ANALISTA:
                    AsignarPerfilAnalistas(nit,idRol,perfiles_selecionados,dstrct,creation_user,estado,monto_decision,monto_minimo_decision);
                    break;
                case MODIFICAR_ASIGNACION:
                      ModificarAsignacion(nit,idRol,perfiles_selecionados,dstrct,user_update,estado,monto_decision,monto_minimo_decision);
                    break;
                case RETORNAR_UNIDADES_DEL_PERFIL:
                      retornarPerfilesDelAnalista(nit);
                    break;
                case CAMBIAR_ESTADO_ANALISTA:
                      CambiarEstado(nit,id_perfil,estado);
                    break;
                default:
                    next = urlpagina;
                    break;
            }
                   
        } catch (Exception e) {
            e.printStackTrace();
        }
  
      // Redireccionar a la p�gina indicada.
        if (!next.equals("")) {
            this.dispatchRequest(next);
        }

    }
    
    
        
        private void ListarAnalistas() throws Exception {
        try {
            AsigPerfilesRiesgosAnalistasDAO asigPerfilRiesgo = new AsigPerfilesRiesgosAnalistasDAO();
            
            
           // Vector vAnalista = asigPerfilRiesgo.getVAnalistas();
           ArrayList<AsigPerfilRiesgoAnalista> listaanalista= asigPerfilRiesgo.ListarAnalistas();
            Gson gson = new Gson();
            String json = gson.toJson(listaanalista);
            //this.printlnResponse(json, "application/json;");
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
           private void ListarAnalistasFabrica(String form) throws Exception {
        try {
            AsigPerfilesRiesgosAnalistasDAO asigPerfilRiesgo = new AsigPerfilesRiesgosAnalistasDAO();
           //Vector vAnalista = asigPerfilRiesgo.getVAnalistas();
           ArrayList<AsigPerfilRiesgoAnalista> listaanalFabr =asigPerfilRiesgo.ListarAnalistasFabrica(form);
            Gson gson = new Gson();
            String json = gson.toJson(listaanalFabr);
            //this.printlnResponse(json,"application/json;");
            this.printlnResponseAjax(json,"application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
           
           
            private void ListarPerfilesRiesgos() throws Exception {
        try {
            AsigPerfilesRiesgosAnalistasDAO asigPerfilRiesgo = new AsigPerfilesRiesgosAnalistasDAO();
            
            ArrayList<AsigPerfilRiesgoAnalista> listaPerf=asigPerfilRiesgo.ListarPerfiles();
            // Vector vAnalista = asigPerfilRiesgo.getVAnalistas();
            
            Gson gson = new Gson();
            String json = gson.toJson(listaPerf);
            //this.printlnResponse(json, "application/json;");
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
            
      private void ListarRoles() throws Exception {
        try {
            AsigPerfilesRiesgosAnalistasDAO asigPerfilRiesgo = new AsigPerfilesRiesgosAnalistasDAO();
            
             ArrayList<Rol> listaRol =asigPerfilRiesgo.ListarRoles();
           // Vector vAnalista = asigPerfilRiesgo.getVAnalistas();

            Gson gson = new Gson();
            String json = gson.toJson(listaRol);
            //this.printlnResponse(json, "application/json;");
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
      
      
    private void retornarPerfilesDelAnalista(String idanalista) throws Exception {
        try {
            AsigPerfilesRiesgosAnalistasDAO asigPerfilRiesgo = new AsigPerfilesRiesgosAnalistasDAO();
            
            ArrayList<AsigPerfilRiesgoAnalista> listaPerf=asigPerfilRiesgo.retornarPerfilesDelAnalista(idanalista);
           // Vector vAnalista = asigPerfilRiesgo.getVAnalistas();
            Gson gson = new Gson();
            String json = gson.toJson(listaPerf);
            //this.printlnResponse(json, "application/json;");
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
      
            
        private void RetornarDatosFormEdicion(String nit) throws Exception {
        try {
            AsigPerfilesRiesgosAnalistasDAO asigPerfilRiesgo = new AsigPerfilesRiesgosAnalistasDAO();
            
            
           // Vector vAnalista = asigPerfilRiesgo.getVAnalistas();
           ArrayList<AsigPerfilRiesgoAnalista> listaDatosForm= asigPerfilRiesgo.RetornarDatosFormEdicion(nit);
            Gson gson = new Gson();
            String json = gson.toJson(listaDatosForm);
            //this.printlnResponse(json, "application/json;");
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
        
   private void EliminarAsig(String nit, String id_perfil) throws Exception {
        String respuesta ="";
        try {
         AsigPerfilesRiesgosAnalistasDAO asigPerfilRiesgo = new AsigPerfilesRiesgosAnalistasDAO();
            
          asigPerfilRiesgo.EliminarAsigPerfiles(nit);//elimina los perfiles que tiene relacionado un analista
           respuesta =  asigPerfilRiesgo.EliminarAsigRol(nit);


        } catch (Exception ex) {
            respuesta="Error al eliminar la asignacion";
            ex.printStackTrace();
        }
        
        Gson gson = new Gson();
        String json = gson.toJson(respuesta);
        this.printlnResponseAjax(json, "application/string;");
    }
     
   
   private void AsignarPerfilAnalistas(String nit,
                                       String idRol,
                                       String perfiles_selecionados,
                                       String dstrct,
                                       String creation_user,
                                       String estado,
                                       String monto_decision,
                                       String monto_minimo_decision) throws Exception {
          String respuesta ="";
        try {
            AsigPerfilesRiesgosAnalistasDAO asigPerfilRiesgo = new AsigPerfilesRiesgosAnalistasDAO();
         
             
            if(!perfiles_selecionados.equals("")){
                //Signar rol
               respuesta =  asigPerfilRiesgo.AsignarRol(nit,
                                      idRol,
                                      dstrct,
                                      creation_user);
             
                if(respuesta.equalsIgnoreCase("ok")){
                    //Asignar perfiles
                  respuesta =  AsignarPerfiles(nit,
                                      perfiles_selecionados,
                                      dstrct,
                                      creation_user,
                                      estado,
                                      monto_decision,
                                      monto_minimo_decision);
             
               }
            }else{
               respuesta="Debe de seleccionar al menos un perfil";
            }

                           
          //  Vector vAnalista = asigPerfilRiesgo.getVAnalistas();
            

        } catch (Exception ex) {
            respuesta="Error al asignar el perfil";
            ex.printStackTrace();
        }
        
            Gson gson = new Gson();
            String json = gson.toJson(respuesta);
            this.printlnResponseAjax(json, "application/string;");
    }
   
     private String AsignarPerfiles(String nit,
                                     String perfiles_selecionados,
                                     String dstrct,
                                     String creation_user,
                                     String estado,
                                     String monto_decision,
                                     String monto_minimo_decision) throws Exception {
            String respuesta="";
        try {
            AsigPerfilesRiesgosAnalistasDAO asigPerfilRiesgo = new AsigPerfilesRiesgosAnalistasDAO();
            
                 asigPerfilRiesgo.EliminarAsigPerfiles(nit);//elimina los perfiles que tiene relacionado un analista
                 if(!perfiles_selecionados.equals("")){
                   String[] perfiles = perfiles_selecionados.split(",");
                   for (String idperfil : perfiles){
                       if (estado==null){estado="A";}
                     respuesta =  asigPerfilRiesgo.AsignarPerfiles(nit,
                                                                   idperfil,
                                                                   dstrct,
                                                                   creation_user,
                                                                   estado,
                                                                   monto_decision,
                                                                   monto_minimo_decision);
                     if(!"ok".equals(respuesta)){
                     respuesta="Error al asignar los perfiles ";
                    }
                   }
                 }else{
                 respuesta="ok";
                 }

           

        } catch (Exception ex) {
              respuesta="Error al asignar las unidades";
            ex.printStackTrace();
        }
        return respuesta;
    }
   
   
     private void ModificarAsignacion( String nit,
                                       String idRol,
                                       String perfiles_selecionados,
                                       String dstrct,
                                       String usuarioActual,
                                       String estado,
                                       String monto_decision,
                                       String monto_minimo_decision) throws Exception {
         
             String respuesta ="";
        try {
            AsigPerfilesRiesgosAnalistasDAO asigPerfilRiesgo = new AsigPerfilesRiesgosAnalistasDAO();
            
             
            if(!perfiles_selecionados.equals("")){
                
                 respuesta = asigPerfilRiesgo.ModificarAsignacionRol(idRol,
                                      dstrct,
                                      usuarioActual,
                                      nit);
             
                if(respuesta.equalsIgnoreCase("ok")){
                     estado= asigPerfilRiesgo.obtenerEstadoActualPerfil(nit);
                  respuesta =  AsignarPerfiles(nit,
                                      perfiles_selecionados,
                                      dstrct,
                                      usuarioActual,
                                      estado,
                                      monto_decision,
                                      monto_minimo_decision);
             
               }
            }else{
               respuesta="Debe de seleccionar al menos un perfil";
            }
            

        } catch (Exception ex) {
            respuesta="Error al modificar la asignacion";
            ex.printStackTrace();
        }
         Gson gson = new Gson();
         String json = gson.toJson(respuesta);
         this.printlnResponseAjax(json, "application/string;");
    } 
     
     
          private void CambiarEstado(String nit,String idPerfil,String estado) throws Exception {
        String  respuesta ="";
         try {
             AsigPerfilesRiesgosAnalistasDAO asigPerfilRiesgo = new AsigPerfilesRiesgosAnalistasDAO();

            String nuevoEstado="";
            if(estado.equals("I")){
                nuevoEstado="A";
            }else{
                nuevoEstado="I";
            };  
            
            respuesta = asigPerfilRiesgo.CambiarEstado(nit,idPerfil,nuevoEstado);
           if(respuesta.equals("ok")){
             
              respuesta="Se a eliminado correctamente el perfil";
           }
           
            

        } catch (Exception ex) {
            respuesta="Error al eliminar el perfil";
            ex.printStackTrace();
        }
        Gson gson = new Gson();
        String json = gson.toJson(respuesta);
       this.printlnResponseAjax(json, "application/string;");
    }
   
}
