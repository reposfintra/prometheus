/*
 * Codigo_discrepanciaSerchAction.java
 *
 * Created on 28 de junio de 2005, 01:54 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
/**
 *
 * @author  Jose
 */
public class Codigo_discrepanciaSerchAction extends Action{
    
    /** Creates a new instance of Codigo_discrepanciaSerchAction */
    public Codigo_discrepanciaSerchAction() {
    }
    
    public void run() throws ServletException, InformationException{
        String next="";
        String base = "";
        String codigo = "";
        String descripcion = "";
        HttpSession session = request.getSession();
        String listar = (String) request.getParameter("listar");
        try{
            if (listar.equals("True")){
                next="/jsp/trafico/codigo_discrepancia/Codigo_discrepanciaListar.jsp";
                next = Util.LLamarVentana(next, "Listar Codigo Discrepancia");
                String sw = (String) request.getParameter("sw");
                if(sw.equals("True")){
                    base = request.getParameter("c_base").toUpperCase();
                    codigo = (String) request.getParameter("c_codigo").toUpperCase();
                    descripcion = (String) request.getParameter("c_descripcion");
                }
                else{
                    base = "";
                    codigo = "";
                    descripcion = "";
                }
                model.codigo_discrepanciaService.searchDetalleCodigo_discrepancias(codigo, descripcion, base);
            }
            else{
                codigo = (String) request.getParameter("c_codigo").toUpperCase();
                model.codigo_discrepanciaService.serchCodigo_discrepancia(codigo);
                model.codigo_discrepanciaService.getCodigo_discrepancia();
                next="/jsp/trafico/codigo_discrepancia/Codigo_discrepanciaModificar.jsp";
                next = Util.LLamarVentana(next, "Listar Codigo Discrepancia");
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
