/*
 * ModificarZonaAction.java
 *
 * Created on 13 de junio de 2005, 11:10 AM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  Henry
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;

public class ModificarZonaAction extends Action {
    
    /** Creates a new instance of ModificarZonaAction */
    public ModificarZonaAction() {
    }
    
    public void run() throws ServletException {
        String next = "/jsp/trafico/zona/zona.jsp?mensaje=Modificado&reload=ok";
        String codigo = (request.getParameter("codigo").toUpperCase());
        String nombre = (request.getParameter("nombre").toUpperCase());
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        try{
            
            Zona zona = new Zona();
            zona.setCodZona(codigo);
            zona.setNomZona(nombre);
            zona.setUser_update(usuario.getLogin());
            model.zonaService.modificarZona(zona); 
        }
        catch (SQLException e){
               throw new ServletException(e.getMessage());
        }
         
         // Redireccionar a la p�gina indicada.
       this.dispatchRequest(next);
    }
}
 