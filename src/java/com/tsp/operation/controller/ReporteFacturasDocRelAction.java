/******************************************************************************
 *      Nombre Clase.................   ReporteFacturasDocRelAction.java
 *      Descripci�n..................   Anula un registro en la tabla tblapl
 *      Autor........................   Ing. Andr�s Maturana
 *      Fecha........................   19.20.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *****************************************************************************/
package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.util.Util;


public class ReporteFacturasDocRelAction extends Action{
    
    Logger logger = Logger.getLogger(this.getClass());
    
    /** Creates a new instance of FacturaLArchivoAction */
    public ReporteFacturasDocRelAction() {
    }
    
    
    public void run() throws ServletException, InformationException {
        try {
            
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String userlogin=""+usuario.getLogin();
            String next = "/jsp/cxpagar/reportes/ReporteFacturasDocsRel.jsp";
            String dstrct = (String) session.getAttribute("Distrito");
                        
            
            String documento = request.getParameter("documento").replaceAll("-_-","#");
            String prov      = request.getParameter("prov");
            String tipo_doc  = request.getParameter("tipo_doc")!=null ?request.getParameter("tipo_doc"):"010";
            String opc = request.getParameter("opc");
            
            logger.info("FACTURA: " + documento);
            logger.info("TIPO DOC: " + tipo_doc);
            logger.info("PROVEEDOR: " + prov);
            logger.info("OPC: " + opc);
            
            if( opc==null ){
                
                List lst = model.cxpDocService.buscarDocsRelacionados(usuario.getDstrct(), prov, documento);
                
                logger.info("Docs encontrados: " + lst.size());
                
                request.setAttribute("Items", lst);
                
                if( lst.size()==0 )
                    next+= "?msg=No se encontraron documentos relacionados a la factura.";
                
            } else {
                if( opc.equals("CHEQ") ){
                    /* Egresos Relacionados */
                    
                    model.cxpDocService.getEgresosFactura(dstrct, prov, tipo_doc, documento);
                    Vector items = model.cxpDocService.getInfo();//.getVecCxp_doc();
                    
                    logger.info("?cheques relacionados: " + items.size());
                    
                    next = "/jsp/cxpagar/reportes/ReporteFacturasCheqsRel.jsp";
                    request.setAttribute("Items", items);
                    
                    if( items.size()==0 )
                        next+= "?msg=No se encontraron documentos relacionados a la factura.";
                } else {
                    /* Precheques relacionados */
                    
                    model.cxpDocService.getEgresosPendientesFactura(dstrct, prov, tipo_doc, documento);
                    Vector items = model.cxpDocService.getInfo();//.getVecCxp_doc();
                    
                    logger.info("?cheques pendientes: " + items.size());
                    
                    next = "/jsp/cxpagar/reportes/ReporteFacturasCheqsPendRel.jsp";
                    request.setAttribute("Items", items);
                    
                    if( items.size()==0 )
                        next+= "?msg=No se encontraron documentos relacionados a la factura.";
                }
            }
                
            this.dispatchRequest(next);
            
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new ServletException("Accion:"+ e.getMessage());
        }
    }
}


