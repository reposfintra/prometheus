/***********************************************
 * Nombre clase: PlacaInsertAction.java
 * Descripci�n: Accion para ingresar una placa a la bd.
 * Autor: AMENDEZ
 * Fecha: 4 de noviembre de 2004, 02:48 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A.
 **********************************************/

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import java.lang.*;
import com.tsp.operation.model.threads.*;
public class ClientefenInsertarAction extends Action{

    public ClientefenInsertarAction() {
    }
    
    public void run() throws ServletException, InformationException{
        String next="/jsp/fenalco/clientes/insert_clientes.jsp";
        HttpSession session     = request.getSession();
        Usuario     usuario     = (Usuario)session.getAttribute("Usuario");
        String res="";
        String res2="";
        String res3="";
        if(request.getParameter("chk3")!=null){
            Codeudor cod=new Codeudor();
            cod.setCarrera(request.getParameter("carrera"));
            cod.setCel(request.getParameter("Celular"));
            cod.setCiudad(request.getParameter("c_dir"));
            cod.setCod(request.getParameter("codigo"));
            cod.setCreation_user(usuario.getLogin());
            cod.setDireccion(request.getParameter("Direccion"));
            cod.setExpedicion_id(request.getParameter("expcc"));
            cod.setId(request.getParameter("CedCliente"));
            cod.setNombre(request.getParameter("NomCliente"));
            cod.setSemestre((!request.getParameter("semestre").equals(""))?request.getParameter("semestre"):"0");
            cod.setTelefono(request.getParameter("Telefono"));
            cod.setTipo_id(request.getParameter("tipoid"));
            cod.setUniversidad(request.getParameter("universidad"));
            cod.setUser_update(request.getParameter(""));
            try{
                model.clienteService.add_codeudor(cod);
                next=next+"?msg=Codeudor Almacenado Correctamente!!";
            }
            catch(Exception eee){
                next=next+"?msg=Error al crear el codeudor!!";
            }

        }
        else{
            System.out.println("llego aqui!!");
            if(request.getParameter("chk4")!=null){
                next="";
                String answ="";
                try{
                    res=model.clienteService.Validar("cliente", "nit", request.getParameter("nit"));
                    res2=model.clienteService.Validar_2(usuario.getCedula(),request.getParameter("nit"));
                    if(!res.equals("") && res2.equals(""))
                    {   answ="El cliente ya existe. Desea agregarlo a su lista de clientes?";
                    }
                    try{
                        response.setContentType("text/plain; charset=utf-8");
                        response.setHeader("Cache-Control", "no-cache");
                        response.getWriter().println(answ);
                    }
                    catch (Exception ex) {
                        System.out.println("error en clienteInsertarAction.java :"+ex.getMessage());
                        ex.printStackTrace();
                    }

                }catch (Exception e){
                    e.printStackTrace();
                }

            }
            else
            {   BeanGeneral Cliente=new BeanGeneral();
                Cliente.setValor_01((request.getParameter("NomCliente")!=null)?request.getParameter("NomCliente"):"");//nomblecliente
                Cliente.setValor_02((request.getParameter("tipoid")!=null)?request.getParameter("tipoid"):"");//tippo doc
                Cliente.setValor_03((request.getParameter("CedCliente")!=null)?request.getParameter("CedCliente"):"");
                Cliente.setValor_04((request.getParameter("Direccion")!=null)?request.getParameter("Direccion"):"");
                Cliente.setValor_05((request.getParameter("Telefono")!=null)?request.getParameter("Telefono"):"");
                Cliente.setValor_06((request.getParameter("c_dir")!=null)?request.getParameter("c_dir"):"");
                Cliente.setValor_07((request.getParameter("Celular")!=null)?request.getParameter("Celular"):"");

                Cliente.setValor_08((request.getParameter("expcc")!=null)?request.getParameter("expcc"):""); //expedicion de cc
                Cliente.setValor_09((request.getParameter("replegal")!=null)?request.getParameter("replegal"):"");//representante legal
                Cliente.setValor_10((request.getParameter("ccreplegal")!=null)?request.getParameter("ccreplegal"):"");//cc rep legal

                Cliente.setValor_11(usuario.getLogin());
                Cliente.setValor_12(usuario.getCedula());//nit del afiliado
                Cliente.setValor_13(usuario.getBase());
                try{
                    res=model.clienteService.Validar("cliente", "nit", Cliente.getValor_03());
                    res2=model.clienteService.Validar_2(Cliente.getValor_12(),Cliente.getValor_03());
                    res3=model.clienteService.Validar_3(Cliente.getValor_12(),Cliente.getValor_03());
                 }catch (Exception e){
                    e.printStackTrace();
                 }
                BeanGeneral bgen=new BeanGeneral();
                bgen.setValor_01(usuario.getCedula());
                bgen.setValor_02(Cliente.getValor_03());
                bgen.setValor_03(usuario.getLogin());
                bgen.setValor_04(usuario.getBase());
                TransaccionService tService ;
                if( res.equals("")&& res2.equals("") )
                {
                   try
                        {
                           tService = new TransaccionService(usuario.getBd());
                           tService.crearStatement();
                           tService.getSt().addBatch(model.clienteService.agregarClienteFen(Cliente));
                           tService.getSt().addBatch(model.Negociossvc.relsave(bgen));
                           tService.execute();
                           tService.closeAll();



                           next=next+"?msg=Cliente Almacenado Correctamente!!";
                         }
                         catch (Exception e)
                         {
                            next=next+"?msg=No se puede adicionar el cliente ";
                            e.printStackTrace();
                         }
                }
                if(res.equals("A")&&res2.equals("") )
                {
                    //System.out.println("dos");
                    try
                        {
                           if(res3.equals("A"))
                           {
                               model.clienteService.relsave_2(bgen);
                           }
                           else
                           {   tService = new TransaccionService(usuario.getBd());
                               tService.crearStatement();
                               tService.getSt().addBatch(model.Negociossvc.relsave(bgen));
                               tService.execute();
                               tService.closeAll();
                           }

                           next=next+"?msg=Operacion Completada con exito ";
                         }
                         catch (Exception e)
                         {
                            next=next+"?msg=No se puede agregar el cliente ";
                            e.printStackTrace();
                         }
                }
                if(res.equals("A")&&res2.equals("A"))
                {
                   //System.out.println("tres");
                    next=next+"?msg=El cliente ya se encuentra registrado verifique sus datos";
                }
                bgen=null;
                Cliente=null;
            }
        }
        if(!next.equals(""))
        {   this.dispatchRequest(next);
        }
    }
}

