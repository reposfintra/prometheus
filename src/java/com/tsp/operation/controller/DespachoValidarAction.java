/*
 * DespachoValidarAction.java
 *
 * Created on 22 de marzo de 2005, 04:10 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

/**
 *
 * @author  KREALES
 */
public class DespachoValidarAction extends Action  {
    
    String grupo="ninguno";
    float valor=0;
    float costo=0;
    float maxant=0;
    String dest="";
    String origen="";
    float acpmmax=0;
    float peajeamax=0;
    float peajebmax=0;
    float peajecmax=0;
    String tipo="";
    float e_max=0;
    float vacpm=0;
    float vpeaje=0;
    String nombreProp="No se encontro";
    
    /** Creates a new instance of DespachoValidarAction */
    public DespachoValidarAction() {
    }
    
    public String buscarStandard(String pla)throws ServletException{
        String sj="";
        try{
            model.remplaService.buscaRemPla(pla);
            if(model.remplaService.getRemPla()!=null){
                RemPla rempla = model.remplaService.getRemPla();
                String no_rem= rempla.getRemesa();
                model.remesaService.buscaRemesa(no_rem);
                if(model.remesaService.getRemesa()!=null){
                    Remesa rem=model.remesaService.getRemesa();
                    sj=rem.getStdJobNo();
                }
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        return sj;
    }
    
    public void buscarValor(String sj, String placa, float pesoreal)throws ServletException{
        
        try{
            model.stdjobcostoService.buscaStdJobCosto(sj);
            if(model.stdjobcostoService.getStandardCosto()!=null){
                Stdjobcosto std = model.stdjobcostoService.getStandardCosto();
                maxant=std.getporcentaje_maximo_anticipo();
                costo = std.getUnit_cost();
                valor= pesoreal * costo;
            }
            
            model.plkgruService.buscaPlkgru(placa,sj);
            if(model.plkgruService.getPlkgru()!=null){
                Plkgru plk = model.plkgruService.getPlkgru();
                grupo=plk.getGroupcode();
                model.stdjobplkcostoService.buscaStd(grupo);
                if(model.stdjobplkcostoService.getStd()!=null){
                    Stdjobplkcosto std = model.stdjobplkcostoService.getStd();
                    costo = std.getUnit_cost();
                    valor= pesoreal * costo;
                }
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
    }
    
    public void buscarTramo(String sj)throws ServletException{
        try{
            model.stdjobdetselService.buscaStandardDetSel(sj);
            if(model.stdjobdetselService.getStandardDetSel()!=null){
                Stdjobdetsel sds= model.stdjobdetselService.getStandardDetSel();
                origen=sds.getOrigin_code();
                dest=sds.getDestination_code();
                model.tramoService.buscarTramo(origen, dest);
                if(model.tramoService.getTramo()!=null){
                    Tramo tramo = model.tramoService.getTramo();
                    acpmmax=tramo.getAcpm();
                    peajeamax= tramo.getCantidad_maximo_ticket1();
                    peajebmax= tramo.getCantidad_maximo_ticket2();
                    peajecmax= tramo.getCantidad_maximo_ticket3();
                }
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        
    }
    public void buscarAcpm(String nit)throws ServletException{
        try{
            String [] nitcod = null;
            String sucursal="";
            nitcod = nit.split("/");
            if(nitcod.length>1){
                sucursal=nitcod[1];
            }
            //System.out.println("Codigo del proveedor "+nitcod[0]+", "+sucursal);
            model.proveedoracpmService.buscaProveedor(nitcod[0], sucursal);
            if(model.proveedoracpmService.getProveedor()!=null){
                Proveedor_Acpm acpm= model.proveedoracpmService.getProveedor();
                tipo=acpm.getTipo();
                e_max=acpm.getMax_e();
                vacpm = acpm.getValor();
                //System.out.println("Valor del acpm "+vacpm);
                //System.out.println("Maximo efectivo "+e_max);
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        
    }
    
    
    
    public void run() throws ServletException,InformationException {
        
        
        String next = null;
        String placa = request.getParameter("placa").toUpperCase();
        String standar = request.getParameter("standard");
        String cedula = request.getParameter("conductor");
        String trailer = request.getParameter("trailer");
        String orden = request.getParameter("orden");
        String remision = "CS"+request.getParameter("remision");
        String nombre="";
        String nstandar="";
        float pesovm=Float.parseFloat(request.getParameter("pesovm"));
        float pesolm=Float.parseFloat(request.getParameter("pesolm"));
        float peso = Float.parseFloat(request.getParameter("pesoll"));
        float pesoVacio = Float.parseFloat(request.getParameter("pesov"));
        float anticipo = Float.parseFloat(request.getParameter("anticipo"));
        float gacpm=Float.parseFloat(request.getParameter("gacpm"));
        String proveedorAcpm= request.getParameter("proveedorAcpm");
        int peajea = Integer.parseInt(request.getParameter("peajea"));
        int peajeb = Integer.parseInt(request.getParameter("peajeb"));
        int peajec = Integer.parseInt(request.getParameter("peajec"));
        String proveedorT = request.getParameter("tiquetes");
        String error1="#99cc99", error2="#99cc99", error3="#99cc99", error4="#99cc99",error5="#99cc99",errorTrafico="#99cc99";
        String fecpla="";
        String cmd = request.getParameter("cmd");
        int sw=0;
        float full_w=0;
        float pesoreal=0;
        float pesoMax=0;
        
        request.setAttribute("pesoLleno","#99cc99");
        request.setAttribute("pesoVacio","#99cc99");
        request.setAttribute("anticipo","#99cc99");
        request.setAttribute("acpm","#99cc99");
        request.setAttribute("peajea","#99cc99");
        request.setAttribute("peajeb","#99cc99");
        request.setAttribute("peajec","#99cc99");
        request.setAttribute("proveedora","#99cc99");
        request.setAttribute("planilla","#99cc99");
        request.setAttribute("placa","#99cc99");
        //System.out.println("La cantidad de peaje a = "+peajea);
        //System.out.println("La cantidad de peaje b = "+peajeb);
        //System.out.println("La cantidad de peaje c = "+peajec);
        
        //EMPEZAMOS VALIDANDO LAS FECHAS....
        Calendar fecha=null;
        
        try{
            
           
            model.tbltiempoService.searchTblTiemposAll(standar);
            Vector tiempos = model.tbltiempoService.getTiempos();
            
            //INICIALIZAMOS LOS VALORES DE COLORES DE LA TABLA
            for(int i =0; i<tiempos.size();i++){
                Tbltiempo tbl = (Tbltiempo) tiempos.elementAt(i);
                request.setAttribute("error"+tbl.getTimeCode(),"#99cc99");
                
            }
            //Recupero las fechas
            
            for(int i =0; i<tiempos.size();i++){
                
                fecha = Calendar.getInstance();
                
                Tbltiempo tbl = (Tbltiempo) tiempos.elementAt(i);
                
                //System.out.println("Voy a verificar la fecha con codigo : "+tbl.getTimeCode());
                
                //VERIFICAMOS Q LA FECHA SEA LA DE INICIO DE VIAJE
                if(request.getParameter("IVIA")!=null)
                    fecpla= request.getParameter("IVIA");
                
                if(tbl.getTimeCode().equals("IVIA") && request.getParameter("IVIA")==null){
                    
                    //System.out.println("COMO IVIA ES NULO VOY A BUSCAR EL ULTIMO VIAJE  ");
                    
                    model.planillaService.buscaFecha(placa);
                    if(model.planillaService.getPlanilla()!=null){
                        
                        fecpla =""+ model.planillaService.getPlanilla().getFecdsp();
                        
                        int year=Integer.parseInt(fecpla.substring(0,4));
                        int month=Integer.parseInt(fecpla.substring(5,7))-1;
                        int date= Integer.parseInt(fecpla.substring(8,10));
                        int hora=Integer.parseInt(fecpla.substring(11,13));
                        int minuto=Integer.parseInt(fecpla.substring(14,16));
                        
                        Calendar fecha_ant = Calendar.getInstance();
                        fecha_ant.set(year,month,date,hora,minuto,0);
                        
                        Date fecha_pla_date = fecha_ant.getTime();
                        SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                        
                        fecpla = s.format(fecha_pla_date);
                        
                        //fecha.setTime(model.planillaService.getPlanilla().getFecdspDate());
                        fecha.set(year,month,date,hora,minuto,0);
                        fecha.set(fecha.MILLISECOND,0);
                        
                        
                        
                    }
                    else{
                        //  request.setAttribute("errorIVIA","#cc0000");
                        // sw=1;
                        Tbltiempo tbl2 = (Tbltiempo) tiempos.elementAt(i+1);
                        if(request.getParameter(tbl2.getTimeCode())!=null){
                            
                            int year   =  Integer.parseInt(request.getParameter(tbl2.getTimeCode()).substring(0,4));
                            int month  =  Integer.parseInt(request.getParameter(tbl2.getTimeCode()).substring(5,7))-1;
                            int date   =  Integer.parseInt(request.getParameter(tbl2.getTimeCode()).substring(8,10));
                            int hora   =  Integer.parseInt(request.getParameter(tbl2.getTimeCode()).substring(11,13));
                            int minuto =  Integer.parseInt(request.getParameter(tbl2.getTimeCode()).substring(14,16));
                            
                            Calendar fecha_ant = Calendar.getInstance();
                            fecha_ant.set(year,month,date-1,hora,minuto,0);
                            
                            Date fecha_pla_date = fecha_ant.getTime();
                            SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd HH:mm");
                            
                            
                            fecha.set(year,month,date-1,hora,minuto);
                            
                            fecha.set(fecha.MILLISECOND,0);
                            
                            fecpla = s.format(fecha_pla_date);
                            
                        }
                    }
                }
                
                else{
                    //System.out.println("VOY A BUSCAR EL VALOR DE LA FECHA CON CODIGO...."+tbl.getTimeCode());
                    if(request.getParameter(tbl.getTimeCode())!=null){
                        int year=Integer.parseInt(request.getParameter(tbl.getTimeCode()).substring(0,4));
                        int month=Integer.parseInt(request.getParameter(tbl.getTimeCode()).substring(5,7))-1;
                        int date= Integer.parseInt(request.getParameter(tbl.getTimeCode()).substring(8,10));
                        int hora=Integer.parseInt(request.getParameter(tbl.getTimeCode()).substring(11,13));
                        int minuto=Integer.parseInt(request.getParameter(tbl.getTimeCode()).substring(14,16));
                        fecha.set(year,month,date,hora,minuto,0);
                        fecha.set(fecha.MILLISECOND,0);
                    }
                }
                Calendar hoy=Calendar.getInstance();
                
                if(fecha.after(hoy)){
                    
                    //System.out.println("LA FECHA ESTA DESPUES DE HOY....");
                    request.setAttribute("error"+tbl.getTimeCode(),"#cc0000");
                    sw=1;
                    //System.out.println("La primera fecha es : "+fecha);
                    //System.out.println("La segunda fecha es : "+hoy);
                }
                
                else{
                    
                    int j= i+1;
                    //System.out.println("EL VALOR DE J ES  ..."+j);
                    
                    if(j < tiempos.size()){
                        
                        Tbltiempo tbl2 = (Tbltiempo) tiempos.elementAt(j);
                        
                        //System.out.println("VOY A BUSCAR  EL VALOR DE LA FECHA "+tbl2.getTimeCode()+" CON LA Q VOY A COMPARAR ...");
                        
                        
                        Calendar fecha2 = Calendar.getInstance();
                        
                        if(request.getParameter(tbl2.getTimeCode())!=null){
                            
                            int year2=Integer.parseInt(request.getParameter(tbl2.getTimeCode()).substring(0,4));
                            int month2=Integer.parseInt(request.getParameter(tbl2.getTimeCode()).substring(5,7))-1;
                            int date2= Integer.parseInt(request.getParameter(tbl2.getTimeCode()).substring(8,10));
                            int hora2=Integer.parseInt(request.getParameter(tbl2.getTimeCode()).substring(11,13));
                            int minuto2=Integer.parseInt(request.getParameter(tbl2.getTimeCode()).substring(14,16));
                            
                            fecha2.set(year2,month2,date2,hora2,minuto2,0);
                            fecha2.set(fecha2.MILLISECOND,0);
                            
                            if(fecha.after(fecha2)){
                                //System.out.println("LA PRIMERA FECHA ES MAYOR Q LA SEGUNDA ...");
                                sw=1;
                                request.setAttribute("error"+tbl2.getTimeCode(),"#cc0000");
                                
                            }
                            else if(fecha.equals(fecha2)){
                                sw=1;
                                //System.out.println("LAS FECHAS SON IGUALES....");
                                request.setAttribute("error"+tbl2.getTimeCode(),"#cc0000");
                                
                            }
                            
                        }
                        
                        //System.out.println("La primera fecha es : "+fecha);
                        //System.out.println("La segunda fecha es : "+fecha2);
                        
                    }
                    
                }
                
                
            }
            if(!model.placaService.placaExist(placa)){
                sw=1;
                error1="#cc0000";
            }
            else{
                model.placaService.buscaPlaca(placa);
                if(model.placaService.getPlaca()!=null){
                    Placa p = model.placaService.getPlaca();
                    String cedProp = p.getPropietario();
                    nombreProp = model.placaService.nombreProp(cedProp);
                }
            }
            if(model.conductorService.conductorExist(cedula)){
                //System.out.println("Existe el conductor");
                model.conductorService.buscaConductor(cedula);
                if(model.conductorService.getConductor()!=null){
                    //System.out.println("El conductor no es nulo");
                    Conductor conductor = model.conductorService.getConductor();
                    nombre=conductor.getNombre();
                    cedula=conductor.getCedula();
                    //System.out.println("El nombre es " +nombre);
                }
                
            }
            else{
                sw=1;
                error2="#cc0000";
            }
            
            
            model.stdjobdetselService.buscaStandardDetSel(standar);
            if(model.stdjobdetselService.getStandardDetSel()!=null){
                Stdjobdetsel standard = model.stdjobdetselService.getStandardDetSel();
                nstandar=standard.getSj_desc();
                
            }
            if(model.remesaService.estaRemision(remision)){
                error4="#cc0000";
                sw=1;
            }
            if (pesovm>pesolm){
                error5="#cc0000";
                sw=1;
            }
            
            full_w=peso;
            this.buscarTramo(standar);
            
            //System.out.println("El estandard es: "+standar);
            model.stdjobcostoService.buscaStdJobCosto(standar);
            if(model.stdjobcostoService.getStandardCosto()!=null){
                Stdjobcosto stdjobcosto = model.stdjobcostoService.getStandardCosto();
                pesoMax=stdjobcosto.getPeso_lleno_maximo();
            }
            
            if(full_w>pesoMax){
                sw=1;
                request.setAttribute("pesoLleno","#cc0000");
            }
            
            if(pesoVacio>=full_w){
                request.setAttribute("pesoVacio","#cc0000");
                sw=1;
            }
            else{
                pesoreal=full_w-pesoVacio;
                this.buscarValor(standar,placa,pesoreal);
                
            }
            
            //VALIDO EL ANTICIPO
            //System.out.println("Voy a buscar el proveedor de acpm");
            this.buscarAcpm(proveedorAcpm);
            float totalacpm =0;
            //System.out.println("Los galones de Maximos de Acpm "+acpmmax);
            //System.out.println("Los galones de Acpm son "+gacpm);
            if(tipo.equals("E")){
                totalacpm = vacpm * gacpm;
                if(totalacpm>e_max){
                    request.setAttribute("acpm","#cc0000");
                    sw=1;
                }
            }
            else if(tipo.equals("G")){
                totalacpm = vacpm * gacpm;
                
                if(gacpm>acpmmax){
                    request.setAttribute("acpm","#cc0000");
                    sw=1;
                }
            }
            if(peajea>peajeamax){
                request.setAttribute("peajea","#cc0000");
                sw=1;
            }
            if(peajeb>peajebmax){
                request.setAttribute("peajeb","#cc0000");
                sw=1;
            }
            if(peajec>peajecmax){
                request.setAttribute("peajec","#cc0000");
                sw=1;
            }
            
            int nopeajes = peajec+peajeb+peajea;
            float totalpeajes = vpeaje * nopeajes;
            
            //this.buscarVPeajes(proveedorT);
            float antmax = valor*(maxant/100);
            
            //System.out.println("El valor de la planilla es : "+valor);
            //System.out.println("El anticipo maximo es : "+maxant);
            //System.out.println("El total de peajes es : "+totalpeajes);
            //System.out.println("El total de acpm es : "+totalacpm);
            //System.out.println("El anticipo  es : "+anticipo);
            float totalant = totalpeajes+totalacpm+anticipo;
            
            if(antmax<totalant){
                request.setAttribute("anticipo","#cc0000");
                sw=1;
            }
            
            String traffic="";
            if(request.getParameter("trafico")==null){
                traffic  = model.planillaService.buscarTrafficCode(placa);
                if(traffic.equals("")){
                    sw=1;
                    errorTrafico="#cc0000";
                }
            }
            else{
                //System.out.println("Se recibio un codigo de trafico");
                traffic  = request.getParameter("trafico");
                if(!model.planillaService.estaTagTrafficPlaca(request.getParameter("trafico"),placa)){
                    //System.out.println("No esta la placa  y el codigo");
                    if(model.planillaService.estaTagTraffic(request.getParameter("trafico"))){
                        //System.out.println("Esta el codigo del trafico");
                        sw=1;
                        errorTrafico="#cc0000";
                    }
                }
                
            }
            if(sw==0)
                next = "/despacho2/DespachoValido.jsp?nombre="+nombre+"&IVIA="+fecpla+"&nombreProp="+nombreProp;
            else
                next = "/despacho2/DespachoError.jsp?nombre="+nombre+"&error1="+error1+"&error2="+error2+"&error3="+error3+"&error4="+error4+"&error5="+error5+"&IVIA="+fecpla+"&nombreProp="+nombreProp+"&trafico="+traffic+"&errorTrafico="+errorTrafico;
            
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        
        
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
    }
    
}
