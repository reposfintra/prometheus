/***************************************************************************
 * Nombre clase :                 ReporteVehPerdidosAction.java            *
 * Descripcion :                  Clase que maneja los eventos             *
 *                                relacionados con el programa de          *
 *                                Reporte de vehiculos perdidos            *
 * Autor :                         Ing. Juan Manuel Escandon Perez         *
 * Fecha :                         10 de enero de 2006, 11:14 AM           *
 * Version :                       1.0                                     *
 * Copyright :                     Fintravalores S.A.                 *
 ***************************************************************************/

package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import com.tsp.operation.model.threads.*;

public class ReporteVehPerdidosAction extends Action{
    
    /** Creates a new instance of ReporteVehPerdidosAction */
    public ReporteVehPerdidosAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        try{
            String next =  "";
            String Mensaje = "El proceso se ha iniciado";
            
            HttpSession Session           = request.getSession();
            Usuario user                  = new Usuario();
            user                          = (Usuario)Session.getAttribute("Usuario");
            
            ReporteVehPerdidosXLS hilo = new ReporteVehPerdidosXLS();
            model.reporteVehPerdidosSvc.listVehPerdidos();
            hilo.start(model.reporteVehPerdidosSvc.getList(), user.getLogin());
            
            next = "/jsp/masivo/reportes/ReporteVehPerdidos/ReporteVehPerdidos.jsp?Mensaje="+Mensaje;
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en ReporteVehPerdidosAction .....\n"+e.getMessage());
        }
    }
    
}
