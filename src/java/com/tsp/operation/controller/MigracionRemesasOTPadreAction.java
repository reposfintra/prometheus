/*
 * MigracionRemesasAnuladasAction.java
 *
 * Created on 16 de julio de 2005, 02:43 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
//import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

/**
 *
 * @author  Hager
 */
public class MigracionRemesasOTPadreAction extends Action{
    
    /** Creates a new instance of MigracionRemesasAnuladasAction */
    public MigracionRemesasOTPadreAction() {
    }
    
    public void run() throws ServletException, InformationException {        
        String next="/migracion/migracionRemesasOTPadre.jsp";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        try {
            Vector rem = model.migracionOTPadreService.obtenerRemesasOTPadre();        
            HMigracionRemesasOTPadre hilo = new HMigracionRemesasOTPadre();
            hilo.start(rem, usuario.getLogin());
            next = "/migracion/mostrarOT.jsp";
        }catch (SQLException ex) {
            ex.printStackTrace();
        }    
                        
        this.dispatchRequest(next);
    }
    
}
