/*
 * ModificarValidarAction.java
 *
 * Created on 29 de noviembre de 2004, 04:35 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class ModificarValidarAction extends Action {
    
    static Logger logger = Logger.getLogger(CamposValidarAction.class);
    String grupo="ninguno";
    float valor=0;
    float costo=0;
    float maxant=0;
    String dest="";
    String origen="";
    float acpmmax=0;
    float peajeamax=0;
    float peajebmax=0;
    float peajecmax=0;
    
    /** Creates a new instance of ModificarValidarAction */
    public ModificarValidarAction() {
    }
    
    public float buscarFull_W(String planilla)throws ServletException{
        float full_w=0;
        try{
            model.planillaService.bucaPlaaux(planilla);
            if(model.planillaService.getPlaaux()!=null){
                Plaaux plaaux= model.planillaService.getPlaaux();
                full_w=plaaux.getFull_weight();
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        return full_w;
        
    }
    public void buscarValor(String sj, String placa, float pesoreal)throws ServletException{
        
        try{
            model.stdjobcostoService.buscaStdJobCosto(sj);
            if(model.stdjobcostoService.getStandardCosto()!=null){
                Stdjobcosto std = model.stdjobcostoService.getStandardCosto();
                maxant=std.getporcentaje_maximo_anticipo();
                costo = std.getUnit_cost();
                valor= pesoreal * costo;
            }
            
            model.plkgruService.buscaPlkgru(placa,sj);
            if(model.plkgruService.getPlkgru()!=null){
                Plkgru plk = model.plkgruService.getPlkgru();
                grupo=plk.getGroupcode();
                model.stdjobplkcostoService.buscaStd(grupo);
                if(model.stdjobplkcostoService.getStd()!=null){
                    Stdjobplkcosto std = model.stdjobplkcostoService.getStd();
                    costo = std.getUnit_cost();
                    valor= pesoreal * costo;
                }
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
    }
    public void buscarTramo(String sj)throws ServletException{
        try{
            model.stdjobdetselService.buscaStandardDetSel(sj);
            if(model.stdjobdetselService.getStandardDetSel()!=null){
                Stdjobdetsel sds= model.stdjobdetselService.getStandardDetSel();
                origen=sds.getOrigin_code();
                dest=sds.getDestination_code();
                model.tramoService.buscarTramo(origen, dest);
                if(model.tramoService.getTramo()!=null){
                    Tramo tramo = model.tramoService.getTramo();
                    acpmmax=tramo.getAcpm();
                    peajeamax= tramo.getCantidad_maximo_ticket1();
                    peajebmax= tramo.getCantidad_maximo_ticket2();
                    peajecmax= tramo.getCantidad_maximo_ticket3();
                }
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        
    }
    
    public void run() throws javax.servlet.ServletException, InformationException {
        
        String next = null;
        String placa = request.getParameter("placa").toUpperCase();
        String standar = request.getParameter("standar");
        String cedula = request.getParameter("conductor");
        String trailer = request.getParameter("trailer");
        String orden = request.getParameter("orden");
        float peso = Float.parseFloat(request.getParameter("peso"));
        String planilla = request.getParameter("planilla").toUpperCase();
        float pesoVacio = Float.parseFloat(request.getParameter("pesov"));
        float anticipo = Float.parseFloat(request.getParameter("anticipo"));
        String proveedorA= request.getParameter("proveedorA");
        float gacpm=Float.parseFloat(request.getParameter("acpm"));
        String proveedorAcpm= request.getParameter("proveedorAcpm");
        float peajea = Float.parseFloat(request.getParameter("peajea"));
        float peajeb = Float.parseFloat(request.getParameter("peajeb"));
        float peajec = Float.parseFloat(request.getParameter("peajec"));
        String proveedorT = request.getParameter("tiquetes");
        String nombre="";
        float pesoMax=0;
        float pesoreal=0;
        float full_w=0;
        int sw=0;
        String nstandar="";
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
       
        
        request.setAttribute("error1","#99cc99");
        request.setAttribute("error2","#99cc99");
        request.setAttribute("error3","#99cc99");
        request.setAttribute("pesoVacio","#99cc99");
        request.setAttribute("anticipo","#99cc99");
        request.setAttribute("acpm","#99cc99");
        request.setAttribute("peajea","#99cc99");
        request.setAttribute("peajeb","#99cc99");
        request.setAttribute("peajec","#99cc99");
        
        try{
            full_w=this.buscarFull_W(planilla);
            this.buscarTramo(standar);
            
            
            if(!model.placaService.placaExist(placa)){
                sw=1;
                request.setAttribute("error1","#cc0000");
            }
            if(model.conductorService.conductorExist(cedula)){
                model.conductorService.buscaConductor(cedula);
                if(model.conductorService.getConductor()!=null){
                    Conductor conductor = model.conductorService.getConductor();
                    nombre=conductor.getNombre();
                    cedula=conductor.getCedula();
                }
                
            }
            else{
                sw=1;
                request.setAttribute("error2","#cc0000");
            }
            //Validar el peso Maximo
            model.stdjobcostoService.buscaStdJobCosto(standar);
            if(model.stdjobcostoService.getStandardCosto()!=null){
                Stdjobcosto stdjobcosto = model.stdjobcostoService.getStandardCosto();
                pesoMax=stdjobcosto.getPeso_lleno_maximo();
            }
            if(peso>pesoMax){
                sw=1;
                request.setAttribute("error3","#cc0000");
            }
            
            model.stdjobdetselService.buscaStandardDetSel(standar);
            if(model.stdjobdetselService.getStandardDetSel()!=null){
                Stdjobdetsel standard = model.stdjobdetselService.getStandardDetSel();
                nstandar=standard.getSj_desc();
                
            }
            if(pesoVacio>=peso){
                request.setAttribute("pesoVacio","#cc0000");
                sw=1;
            }
            else{
                pesoreal=peso-pesoVacio;
                this.buscarValor(standar,placa,pesoreal);
                
            }
            float antmax = valor*(maxant/100);
            if(antmax<anticipo){
                request.setAttribute("anticipo","#cc0000");
                sw=1;
            }
            
            if(gacpm>acpmmax){
                request.setAttribute("acpm","#cc0000");
                sw=1;
            }
            if(peajea>peajeamax){
                request.setAttribute("peajea","#cc0000");
                sw=1;
            }
            if(peajeb>peajebmax){
                request.setAttribute("peajeb","#cc0000");
                sw=1;
            }
            if(peajec>peajecmax){
                request.setAttribute("peajec","#cc0000");
                sw=1;
            }
            if(sw==0)
                next = "/ModificarDespacho/DatosModificarValidado.jsp?nombre="+nombre+"&estandar="+nstandar;
            else
                next = "/ModificarDespacho/DatosModificarErrores.jsp?nombre="+nombre+"&estandar="+nstandar;
            
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
