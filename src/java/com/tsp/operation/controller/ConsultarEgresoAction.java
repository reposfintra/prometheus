/*
 * ConsultarEgresoAction.java
 *
 * Created on 11 de enero de 2005, 05:38 PM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  KREALES
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
public class ConsultarEgresoAction extends Action{
    
    /** Creates a new instance of ConsultarEgresoAction */
    public ConsultarEgresoAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String nit = request.getParameter("nit");
        String distrito = request.getParameter("distrito");
        String banco = request.getParameter("banco");
        String cuenta = request.getParameter("cuenta");
        String doc_no = request.getParameter("doc_no");
        String nombre = request.getParameter("nombre");
        String fechaini = request.getParameter("fechaini");
        String fechafin = request.getParameter("fechafin");
        String next="/consultas/consultasRedirect.jsp?egreso=ok";
        try{
            if(request.getParameter("solo")==null){
                model.egresoService.consultarEgreso(distrito, cuenta, banco, doc_no, nit, nombre, fechaini, fechafin);
                if(model.egresoService.getEgresos().size()==1){
                    model.egresoService.buscarEgreso(doc_no);
                    model.egresoService.buscarEgresoDet(doc_no);
                    next="/consultas/datosEgreso.jsp";
                }
            }
            else{
                model.egresoService.buscarEgreso(doc_no);
                model.egresoService.buscarEgresoDet(doc_no);
                next="/consultas/datosEgreso.jsp";
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
