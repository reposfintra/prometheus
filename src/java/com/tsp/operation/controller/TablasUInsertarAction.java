/******************************************************************
* Nombre ......................TablasUEliminarAction.java
* Descripci�n..................Clase Action para cargar una tabla a un ususario
* Autor........................David lamadrid
* Fecha........................21/12/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  dlamadrid
 */
public class TablasUInsertarAction extends Action 
{
    
    /** Creates a new instance of TablasUsuarioInsertarAction */
    public TablasUInsertarAction ()
    {
    }
    
       public void run () throws ServletException, InformationException
        {
                String next="";
                String ms="";
                try
                {
                        
                        HttpSession session = request.getSession ();
                        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
                        String userlogin=""+usuario.getLogin ();
                        String distrito=""+ usuario.getDstrct ();
                        
                        String nombre =""+request.getParameter ("c_nombre");
                        String login  =""+request.getParameter ("c_usuario");
                        String descripcion=""+request.getParameter ("c_descripcion");
                        
                        
                        boolean existe=false;
                        TablasUsuario tablas = new TablasUsuario();
                        tablas.setDstrct (distrito);
                        tablas.setNombre (nombre);
                        tablas.setUsuario (login);
                        tablas.setDescripcion (descripcion);
                        tablas.setCreation_user (userlogin);
                        model.tablasUsuarioService.setTabla (tablas);
                        
                        //model.tablasUsuarioService.getTabla ()
                        existe = model.usuarioService.existeUsuario (login);
                        if (existe == true){
                            existe = model.tablasUsuarioService.existeTabla(nombre);
                            if (existe == true ){
                                existe = model.tablasUsuarioService.existePorU (nombre,login);
                                if(existe==false){
                                    model.tablasUsuarioService.insertar ();
                                    ms="Registro Insertado";
                                }
                                else{
                                    ms="El Usuario ya tiene asignada esa tabla";
                                }
                            }    
                            else{
                                ms="No existe una tabla en el Sistema con ese nombre";
                            }    
                        }    
                        else{
                            ms="No existe un Usuario con ese login";
                        }            
                        next="/jsp/general/consultas/insertar.jsp?accion=1&ms="+ms;
                }
                catch (Exception e)
                {
                        throw new ServletException (e.getMessage ());
                        // e.printStackTrace();
                }
                this.dispatchRequest (next);
        }
    
}
