/***************************************************************************
 * Nombre clase : ............... CaletasAction.java                       *
 * Descripcion :................. Clase que maneja los eventos             *
 *                                relacionados con el programa de          *
 *                                ajuste de planilla por concepto de calets*
 * Autor :.......................  Ing. Enrique De Lavalle                 *
 * Fecha :........................ 22 de Mayo de 2007, 3:03 PM              *
 * Version :...................... 1.0                                     *
 * Copyright :.................... Fintravalores S.A.                 *
 ***************************************************************************/
package com.tsp.operation.controller;
 
import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import org.apache.log4j.*;
/**
 *
 * @author  edelavalle
 */
public class AjusteCaletasAction extends Action{
     // 2009-09-02
    
    public AjusteCaletasAction() {
    }
    
        
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
         /*
         *Declaracion de variables
         */
        String conceptoCaleta       = (request.getParameter("conceptoCaleta")!=null)?request.getParameter("conceptoCaleta"):"";
        String codigoCaleta         = (request.getParameter("codigoCaleta")!=null)?request.getParameter("codigoCaleta"):"";
        String valorCaleta          = (request.getParameter("valorCaleta")!=null)?request.getParameter("valorCaleta"):"";
        String monedaCaleta         = (request.getParameter("c_moneda")!=null)?request.getParameter("c_moneda"):"";
        HttpSession Session    = request.getSession();
        Usuario user           = new Usuario();
        user                   = (Usuario)Session.getAttribute("Usuario"); 
        String nomUser         = user.getLogin();
        String dstrct          = user.getDstrct();            
        String next            = "";          
        String opcion          =  (request.getParameter("opcion")!=null)?request.getParameter("opcion"):"";
        
        
        try{   
            
           /* if (opcion.equals("insertar")){
                
                if (model.ajusteCaletasService.existeCaleta(codigoCaleta)){
                    next = "/jsp/masivo/caleta/InsertarVlrCaleta.jsp?msg=El c�digo ingresado ya Existe!!!";                                        
                }else{                    
                    Caleta c = new Caleta();
                    c.setConcepto(conceptoCaleta);
                    c.setCodigo(codigoCaleta);
                    c.setValor(valorCaleta);
                    c.setMoneda(monedaCaleta);
                    c.setUsuarioCreacion(nomUser);
                    model.ajusteCaletasService.insertarCaleta(c);                               
                    next = "/jsp/masivo/caleta/InsertarVlrCaleta.jsp?msg=El Valor: "+valorCaleta+" para la caleta de: "+codigoCaleta+" ha sido ingresa con exito!!";                    
                }                              
            }else if(opcion.equals("aplicar"))  {
                Model model = new Model();                      
                model.ajusteCaletasService.aplicarAjusteCaletaCF(nomUser);                       
                model.ajusteCaletasService.aplicarAjusteCaletaDest(nomUser);
                next = "/jsp/despacho/ajustePlanillaCaleta/ajustePlanillaCaleta.jsp?msg=Se ha generado el proceso exitosamente, para verificar los resultados puede abrir los archivos generados";                    
            }*/
                
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en AjusteCaletasAction .....\n"+e.getMessage());
        }
        
        this.dispatchRequest ( next );
    }
    
}
