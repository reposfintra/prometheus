/*************************************************************************
 * Nombre:        ConsultarDespachoAction.java            *
 * Descripci�n:   Clase Action para realizar busquedas    *
 * Autor:         Ing. Diogenes Antonio Bastidas Morales   *
 * Fecha:         27 de marzo de 2006, 04:04 PM                              *
 * Versi�n        1.0                                      *
 * Coyright:      Transportes Sanchez Polo S.A.            *
 *************************************************************************/


package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.Util;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.exceptions.*;


public class ConsultarDespachoAction extends Action {
    
    /** Creates a new instance of ConsultarDespachoAction */
    public ConsultarDespachoAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String opcion = request.getParameter("opcion");
        String oc = request.getParameter("planilla").toUpperCase();
        String distrito = request.getParameter("distrito");
        String agencia = request.getParameter("agencia");
        String stdjob = request.getParameter("stdjob");
        String next = "";
        try{
            if (opcion.equals("Movimiento")){
                model.RemisionSvc.buscarMovimiento(oc,distrito,agencia);
                next="/controller?estado=Menu&accion=Cargar&carpeta=/consultas&pagina=MovimientosAnticipos.jsp&titulo=Movimientos de Anticipos";
            }
            else if (opcion.equals("Extraflete")){
                model.sjextrafleteService.buscarExtrafletes(distrito, oc, stdjob);
                next="/datosplanilla/extrafletes.jsp";
            }
            else if(opcion.equalsIgnoreCase("TodosMov")){
                model.movplaService.listaAllMovPla_Factura(oc);
                Vector vec = model.movplaService.getLista();
                Vector nuevo = new Vector();
                for(int i=0; i< vec.size(); i++){
                    Movpla  mov = (Movpla) vec.elementAt(i);
                    boolean descuento = mov.getSigno()<0;
                    boolean valordescuento = mov.getVlr_for() <0;
                    String descripcion = "";
                    mov.setColor("#003399");
                    if(descuento == valordescuento){
                        if(descuento && valordescuento){
                            descripcion = "DESCUENTO "+mov.getConcept_code();
                            mov.setColor("#FF0000");
                        }
                        else{
                            descripcion =mov.getConcept_code();
                        }
                    }
                    else{
                        descripcion = "ANULACION "+mov.getConcept_code();
                        if(valordescuento){
                            mov.setColor("#FF0000");
                        }
                    }
                    mov.setConcept_code(descripcion);
                    nuevo.add(mov);
                }
                model.movplaService.setLista(nuevo);
                next="/consultas/MovimientosPlanilla.jsp?planilla="+oc+"&distrito="+distrito;
            }
            
        }
        catch(Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}

/***************************************************
 * Entregado a karen 12 Feb 2007
 ***************************************************/

