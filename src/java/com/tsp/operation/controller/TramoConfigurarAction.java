/*
 * TramoConfigurarAction.java
 *
 * Created on 1 de febrero de 2005, 03:43 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class TramoConfigurarAction extends Action{
    
    /** Creates a new instance of TramoConfigurarAction */
    public TramoConfigurarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/tramos/tramoInsert.jsp";
        String origen = request.getParameter("ciudado");
        String destino = request.getParameter("ciudadd");
        
        try{
            if(request.getParameter("buscar")!=null){
                request.setAttribute("tramo",null);
                model.tramoService.buscarTramo(origen, destino);
                if(model.tramoService.getTramo()!=null){
                    request.setAttribute("tramo",model.tramoService.getTramo());
                }
            }
            else{
                next="/tramos/tramoInsert.jsp?mensaje=Listo tramo actualizado con exito.";                 
                float acpm = Float.parseFloat(request.getParameter("acpm"));
                float tiketmax1 = Float.parseFloat(request.getParameter("ctmax1"));
                float tiketmax2 = Float.parseFloat(request.getParameter("ctmax2"));
                float tiketmax3 = Float.parseFloat(request.getParameter("ctmax3"));
                float tiketmax4 = Float.parseFloat(request.getParameter("ctmax4"));
                float tiketmax5 = Float.parseFloat(request.getParameter("ctmax5"));
                float tiketmax6 = Float.parseFloat(request.getParameter("ctmax6"));
                
                String tiketid1  = request.getParameter("tid1");
                String tiketid2  = request.getParameter("tid2");
                String tiketid3  = request.getParameter("tid3");
                String tiketid4  = request.getParameter("tid4");
                String tiketid5  = request.getParameter("tid5");
                String tiketid6  = request.getParameter("tid6");
                
                Tramo tramo = new Tramo();
                tramo.setOrigin(origen);
                tramo.setDestination(destino);
                tramo.setTicket_id1(tiketid1);
                tramo.setTicket_id2(tiketid2);
                tramo.setTicket_id3(tiketid3);
                tramo.setTicket_id4(tiketid4);
                tramo.setTicket_id5(tiketid5);
                tramo.setTicket_id6(tiketid6);
                tramo.setCantidad_maximo_ticket1(tiketmax1);
                tramo.setCantidad_maximo_ticket2(tiketmax2);
                tramo.setCantidad_maximo_ticket3(tiketmax3);
                tramo.setCantidad_maximo_ticket4(tiketmax4);
                tramo.setCantidad_maximo_ticket5(tiketmax5);
                tramo.setCantidad_maximo_ticket6(tiketmax6);
                tramo.setAcpm(acpm);
                
                model.tramoService.setTramo(tramo);
                model.tramoService.update();
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
    
}
