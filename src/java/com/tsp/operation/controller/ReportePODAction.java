/**************************************************************************
 * Nombre clase: ReportePODAction.java                                *
 * Autor: Ing. Ivan DArio Gomez Vanegas                                    *
 * Fecha: Created on 2 de septiembre de 2006, 10:53 AM                       *
 * Versi�n: Java 1.0                                                       *
 * Copyright: Fintravalores S.A. S.A.                                 *
 ***************************************************************************/




package com.tsp.operation.controller;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.threads.*;
import com.tsp.operation.model.DAOS.*;

public class ReportePODAction extends Action{ 
    
    /** Creates a new instance of ReportePODAction */
    public ReportePODAction() {
    }
     public void run() throws ServletException, com.tsp.exceptions.InformationException {
        String next="";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String FecIni  = request.getParameter("fechaInicio");
        String FecFin  = request.getParameter("fechaFinal");
        String Cliente = request.getParameter("cliente");
        
        try{
            
            model.ReportePODSvc.searchReporte(FecIni, FecFin, "000"+Cliente);
            next = "/jsp/trafico/reportes/ReportePOD.jsp";
                     
        }catch (Exception ex){
            throw new ServletException("Error en ReportePODAction .....\n"+ex.getMessage());
        }

        this.dispatchRequest(next);  
    }
}
