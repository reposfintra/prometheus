/***************************************************************************
 * Nombre clase : ............... FacturaProveedorAnticipoAction.java         *
 * Descripcion :................. Clase que maneja los eventos             *
 *                                relacionados con el programa de          *
 *                                la generacion de facturas a proveedores  *
 * Autor :.......................  Ing. Enrique De Lavalle                 *
 * Fecha :........................ 23 de noviembre de 2007, 11:36 PM       *
 * Version :...................... 1.0                                     *
 * Copyright :.................... Fintravalores S.A.                 *
 ***************************************************************************/
package com.tsp.operation.controller;
 
import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.util.*;
import java.text.*;
import com.tsp.operation.model.threads.*;
import com.tsp.operation.model.DAOS.*;
import org.apache.log4j.*;
import java.io.*; 
/**
 *
 * @author  edelavalle
 */
public class FacturaProveedorAnticipoAction extends Action{
    
    private PrintWriter pw;
    private LogWriter   logTrans;
    private String path;
     
    public FacturaProveedorAnticipoAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException{
        try{
             /*
             *Declaracion de variables
             */
            String Opcion                 = (request.getParameter("Opcion")!=null)?request.getParameter("Opcion"):"";           
            Usuario user                  = new Usuario();            
            HttpSession Session           = request.getSession();
            user                          = (Usuario)Session.getAttribute("Usuario");  
            String dstrct                 = user.getDstrct();  
            String cia                    = user.getCia();
            String usuario                = user.getLogin();
            String base                   = user.getBase();
            String proveedor              = (request.getParameter("proveedor")!=null)?request.getParameter("proveedor"):"";   
            String next                   = "";
            String Mensaje                = "";
            String fechainicial           = (request.getParameter("fechaini")!=null)?request.getParameter("fechaini"):"";
            String fechafinal             = (request.getParameter("fechafin")!=null)?request.getParameter("fechafin"):"";
               
            /*
             *Manejo de eventos
             */
            //Listado de items para la factura
            if (Opcion.equals("Listado")){
                model.facturaProveedorAnticipoService.List_Items(dstrct,proveedor,fechainicial, fechafinal);
                next = "/jsp/cxpagar/facturas/pagoProveedorAnticipo/facturaProveedorAnticipoItems.jsp?prove="+proveedor+"&fechainicial="+fechainicial+"&fechafinal="+fechafinal;//Mostrar
            }
               
            if (Opcion.equals("Generar")){
                
                ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                String ruta = rb.getString("ruta");
                path = ruta + "/exportar/migracion/" + user.getLogin();                
                File archivo = new File( path );
                if (!archivo.exists()) archivo.mkdirs();
                
                initLog();         
                
                model.facturaProveedorAnticipoService.List_Items(dstrct,proveedor,fechainicial, fechafinal);
                List Listado  = model.facturaProveedorAnticipoService.getItems();
                List ListItem = Listado;
                boolean ejecutar = false, existeAprob = true, sw_cab = false;
                double total_me=0, total=0, tasa, abono=0, val_item=0, val_item_me=0, valorPlaActual = 0, valorConvPla =0, valRound=0;
                int newLastN, plazo =0, numItem = 0;
                String documento = "", descripcion = "", agencia = "", hc = "", idmims = "", bancoP = "", sucursalP = "", monedaB = "", monedaP ="", creationD = "", creationDate="", itm="", ABC ="", conceptCode ="", fechaVenc="";
                              
                if(Listado!=null && Listado.size()>0){    
                    
                    FacturaProveedorAnticipo cab = (FacturaProveedorAnticipo) Listado.get(0);
                    descripcion  = cab.getPlanilla()+"-"+fechainicial+"-"+fechafinal+"-Formato Combustible" ;
                    agencia = cab.getAgenciaProveedor();
                    hc = cab.getHandleCodeProveedor();
                    idmims = cab.getIdMimsProveedor();
                    bancoP = cab.getBancoProveedor();
                    sucursalP = cab.getSucursalProveedor();
                    monedaB = cab.getMonedaBanco(); 
                    monedaP = cab.getMoneda();
                    plazo = cab.getPlazo();
                    creationD = cab.getFecha();
                }
                
                // se obtiene la tasa del distrito para convertir la moneda de la sumatoria del valor de las planillas
                String moneda_local = model.ingreso_detalleService.monedaLocal (cia);                
                        
                for(int i=0; i<Listado.size();i++) {
                    FacturaProveedorAnticipo pF = (FacturaProveedorAnticipo) Listado.get(i);
                    
                    //sumatoria para vlr_neto y vlr_saldo  de la factura
                    valorConvPla = Double.parseDouble(pF.getValor())*(obtenerValMonedaP(pF.getMoneda(),moneda_local,cia)); 
                    valorPlaActual = (moneda_local.equals("DOL") )?Util.roundByDecimal( valorConvPla ,2):(int)Math.round( valorConvPla );
                    total = total +  valorPlaActual;
                }
                //para establecer el valor de el vlr_neto de la factura 
                tasa = obtenerValMonedaP(moneda_local,monedaB,cia);
                total_me = (monedaB.equals("DOL") )?Util.roundByDecimal((total * tasa),2):(int)Math.round((total * tasa));
                               
                //para obtener el aprobador de la factura
                 String aprobador =model.tablaGenService.obtenerProveedor();
                 
                 if (aprobador.equals("")){
                     existeAprob = false;
                 }
                
                // buscar un numero en series
                model.seriesService.obtenerSerie ("FACTER" , user);
                Series s = model.seriesService.getSerie ();

                //se crea el numero de la factura para la cabecera 
                newLastN = s.getLast_number()+1;
                documento = genNumFact(s.getPrefix(),s.getLast_number());
                
                //para obtener el dia de venc;
                try {
                    SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
                    java.util.Date fecha = fmt.parse(com.tsp.util.Util.getFechaActual_String(4));
                    fecha.setDate(fecha.getDate()+plazo);
                    fechaVenc = String.valueOf(fmt.format(fecha));                                         
                 }catch(Exception e){ e.printStackTrace();}          
                
                 //para obtener los items de la factura    
                 Vector vec = new Vector();
                 for(int j=0; j<Listado.size();j++){
                     
                     FacturaProveedorAnticipo it = (FacturaProveedorAnticipo) Listado.get(j);
                     
                     CXPItemDoc item = new CXPItemDoc();
                     item.setDstrct( dstrct );
                     item.setProveedor( proveedor );
                     item.setTipo_documento( "010" );
                     item.setDocumento( documento );
                     
                     numItem++;
                     if( numItem < 10 )
                         itm = "00"+numItem;
                     else if ( numItem > 9 && numItem < 100 )
                         itm = "0"+numItem;
                     else
                         itm = ""+numItem;
                     
                     item.setItem( itm );
                     item.setDescripcion( it.getConcepto()+" "+it.getPlanilla()+" "+it.getFecha() );                   
                     
                     //para establecer el vlr en los items de la factura
                     valorConvPla = Double.parseDouble(it.getValor())*(obtenerValMonedaP(it.getMoneda(),moneda_local,cia)); 
                     valRound = (moneda_local.equals("DOL") )?Util.roundByDecimal( valorConvPla ,2):(int)Math.round( valorConvPla );                    
                     item.setVlr( valRound);                   
                     
                     //para establecer el vlr_me en los items de la factura
                     valorPlaActual = (monedaB.equals("DOL") )?Util.roundByDecimal((valRound * tasa),2):(int)Math.round((valRound * tasa));
                     item.setVlr_me(valorPlaActual);                     
                     
                     String cuen = it.getCodCuenta();
                     
                     if( cuen.equals("") ){
                       ejecutar = true;
                       logTrans.log("No se pudo generar la factura " + documento + " por que no existe la cuenta contable en la planilla "+it.getPlanilla() , logTrans.INFO);
                       break;
                     }
                                        
                     item.setCodigo_cuenta( cuen );
                     
                     OpDAO op = new OpDAO(user.getBd());
                     ABC = op.getABC(agencia);
                     if (ABC.equals("")){
                         ejecutar = true;
                         logTrans.log("No se pudo generar la factura " + documento + " por que no existe codigo ABC en la planilla "+it.getPlanilla() , logTrans.INFO);
                         break;
                     }
                     item.setCodigo_abc( ABC );
                     
                     item.setPlanilla( it.getPlanilla() );
                     item.setUser_update( usuario );
                     item.setCreation_date(it.getFecha());
                     item.setCreation_user( usuario );
                     item.setBase( base );
                     item.setCodcliarea( "" );
                     item.setTipcliarea( "" );
                     item.setConcepto( it.getConcepto() );
                     item.setAuxiliar( "" );
                     conceptCode = it.getConcept_code();
                     
                     vec.add(item);
                     
                 }             
                 
                 if(!ejecutar){
                     if (existeAprob){
                        model.facturaProveedorAnticipoService.generarFact(dstrct,proveedor,documento,descripcion,agencia,hc,idmims,bancoP,sucursalP,monedaB,total,abono,total,total_me,abono,total_me,tasa,usuario,usuario,base,monedaB,fechaVenc,newLastN,vec,conceptCode,aprobador);         
                        logTrans.log("La factura "+documento+" fu� generada correctamente.", logTrans.INFO);
                        Mensaje = "Factura  " + documento+ "\nha sido Generada!";
                        next = "/jsp/cxpagar/facturas/pagoProveedorAnticipo/facturaProveedorAnticipoBuscar.jsp?Mensaje="+Mensaje;
                     }      
                 }else{
                     logTrans.log("La factura "+documento+" no fu� generada correctamente.", logTrans.INFO);
                     Mensaje = "Factura no pudo ser generada verifique los archivos del usuario para mas detalles!";
                     next = "/jsp/cxpagar/facturas/pagoProveedorAnticipo/facturaProveedorAnticipoBuscar.jsp?Mensaje="+Mensaje;                                   
                 }      
              
                 closeLog();    
            }
            
            //regresar a la busqueda     
            if (Opcion.equals("Regresar")){
                next = "/jsp/cxpagar/facturas/pagoProveedorAnticipo/facturaProveedorAnticipoBuscar.jsp";
            }
                    
           
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }catch(Exception e){
            e.printStackTrace();
            //throw new ServletException("Error en FacturaProveedorAnticipoAction .....\n"+e.getMessage());
            try{
               logTrans.log("Error en FacturaProveedorAnticipoAction", logTrans.INFO);                
            } catch( Exception p ){ p.printStackTrace(); }           
        }
        
    }

     public void initLog()throws Exception{        
        java.text.SimpleDateFormat fmt = new java.text.SimpleDateFormat("yyyyMMdd_hhmmss");
        pw     = new PrintWriter(new BufferedWriter(new FileWriter(path + "/logGeneracionFacturas_Proveedor"+ fmt.format(new Date()) +".txt")));
        logTrans = new LogWriter("Generacion_Facturas_Proveedores", LogWriter.INFO , pw );
        logTrans.log( "Proceso Inicializado", logTrans.INFO );
    }
    
    public void closeLog() throws Exception{
        logTrans.log( "Proceso Finalizado", logTrans.INFO );
        pw.close();
    }

    /**
     * Metodo obtenerValMonedaP, convierte los valores de una moneda a otra
     * @autor : Ing. Enrique De Lavalle
     * @param : String monedaTengo
     * @param : String monedaConv
     * @param : String CIA
     * @see   : FacturaProveedorAnticipoAction
     * @version : 1.0
     */
    public double obtenerValMonedaP(String monedaTengo, String monedaConv, String distrito){
        
        double vlr_tasa = 0;
        try{
            String moneda_local = model.ingreso_detalleService.monedaLocal (distrito);
            Tasa tasa = model.tasaService.buscarValorTasa(moneda_local, monedaTengo, monedaConv, Util.getFechaActual_String(6));                
             if (tasa != null)
                vlr_tasa = tasa.getValor_tasa();
                        
        }catch(Exception e){
            e.printStackTrace();                
        }

           
       return vlr_tasa;
    }
    
    /**
     * Metodo genNumFact, obtiene el numero de documento de una factura complemento a 6 cifras
     * @autor : Ing. Enrique De Lavalle
     * @param : String prefijo
     * @param : int lastNum
     * @see   : FacturaProveedorAnticipoAction
     * @version : 1.0
     */
    public String genNumFact(String prefijo, int lastNum){
        
        String sufijo =  String.valueOf(lastNum);
        int complemento = 6 - sufijo.length();
        
        for(int i=0; i<complemento; i++){
            sufijo = 0 + sufijo;
        }
        
        return (prefijo+sufijo);  
        
    }
    
  
    
    
    
}
