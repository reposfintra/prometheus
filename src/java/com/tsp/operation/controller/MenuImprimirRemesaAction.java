/*
 * MenuSeriesAction.java
 *
 * Created on 1 de diciembre de 2004, 03:25 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  mcelin
 */
public class MenuImprimirRemesaAction extends Action {
    
    /** Creates a new instance of MenuSeriesAction */
    public MenuImprimirRemesaAction() {
    }
    
    public void run() throws ServletException, InformationException { 
        try {  
                        
            final String next = "/impresion/buscarremesa.jsp";
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
        }
        catch(Exception e) {
            throw new ServletException(e.getMessage());
        }
     }
}   