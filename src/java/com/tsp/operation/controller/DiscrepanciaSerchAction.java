/**********************************************************
 * Nombre clase: DiscrepanciaSerchAction.java
 * Descripci�n: Accion para listar o modificar las discrepancias.
 * Autor: Jose de la rosa
 * Fecha: 26 de septiembre de 2005, 05:38 PM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 ************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  Jose
 */
public class DiscrepanciaSerchAction extends Action {
    
    /** Creates a new instance of DiscrepanciaSerchAction */
    public DiscrepanciaSerchAction () {
    }
    
    public void run () throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next="";
        HttpSession session = request.getSession ();
        String dstrct = (String) session.getAttribute ("Distrito");
        String sw = (String) request.getParameter ("sw");
        try {
            if (sw.equals ("false")) {
                String numrem = (String) request.getParameter ("c_numrem");
                String numpla = (String) request.getParameter ("c_numpla");
                String client = (String) request.getParameter ("client");
                model.remesaService.consultaRemesaDiscrepancia (numrem,numpla);
                next="/jsp/cumplidos/discrepancia/DiscrepanciaInsertar.jsp?c_numrem="+numrem+"&sw=true&campos=false&c_numpla"+numpla+"&exis=false&campos=false&paso=false&client="+client;
            }
            else {
                
                String numrem = (String) request.getParameter ("c_numrem");
                String numpla = (String) request.getParameter ("c_numpla");
                String tipo_doc = (String) request.getParameter ("c_tipo_doc");
                String documento = (String) request.getParameter ("c_documento");
                String tipo_doc_rel = (String) request.getParameter ("c_tipo_doc_rel");
                String documento_rel = (String) request.getParameter ("c_documento_rel");
                String client = (String) request.getParameter ("client");
                if(request.getParameter ("modi")==null){
                    model.discrepanciaService.searchDiscrepancia (numpla, numrem, tipo_doc, documento, tipo_doc_rel, documento_rel, dstrct );
                    Discrepancia d = model.discrepanciaService.getDiscrepancia ();
                    model.discrepanciaService.resetItemsObjetoDiscrepancia ();
                    if( d != null ){
                        model.discrepanciaService.setCabDiscrepancia ( d );
                        model.discrepanciaService.listDiscrepanciaProducto (d.getNro_discrepancia (),numpla,numrem, tipo_doc,documento,tipo_doc_rel,documento_rel, dstrct );
                        model.discrepanciaService.setItemsDiscrepancia ( model.discrepanciaService.getDiscrepancias () );
                        next="/jsp/cumplidos/discrepancia/DiscrepanciaInsertar.jsp?c_numrem="+numrem+"&sw=true&exis=true&campos=true&c_numpla="+numpla+"&c_tipo_doc="+tipo_doc+"&c_documento="+documento+"&paso=false&c_tipo_doc_rel="+tipo_doc_rel+"&c_documento_rel="+documento_rel+"&client"+client;
                    }
                    else{
                        model.discrepanciaService.setCabDiscrepancia ( d );
                        next="/jsp/cumplidos/discrepancia/DiscrepanciaInsertar.jsp?c_numrem="+numrem+"&sw=true&exis=false&campos=true&c_numpla="+numpla+"&c_tipo_doc="+tipo_doc+"&c_documento="+documento+"&paso=false&c_tipo_doc_rel="+tipo_doc_rel+"&c_documento_rel="+documento_rel+"&client"+client;
                    }
                    model.responsableService.listResponsable ();
                    model.tablaGenService.buscarGrupo (client);
                    model.ubService.listarUbicacionesClientes (numrem);
                }
                else
                    next="/jsp/cumplidos/discrepancia/DiscrepanciaInsertar.jsp?c_numrem="+numrem+"&sw=true&exis=true&campos=true&c_numpla="+numpla+"&c_tipo_doc="+tipo_doc+"&c_documento="+documento+"&paso=false&c_tipo_doc_rel="+tipo_doc_rel+"&c_documento_rel="+documento_rel+"&client"+client;
            }
        }catch (SQLException e) {
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
        
    }
    
}
