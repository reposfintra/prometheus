/********************************************************************
 *      Nombre Clase.................   TraficoAnchoAction.java
 *      Descripci�n..................   Action para controlar el ancho de las columnas
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.11.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  David A
 */
public class TraficoAnchoAction extends Action{
    
    /** Creates a new instance of TraficoAncho */
    public TraficoAnchoAction() {
    }
    
    public void run() throws ServletException, InformationException {
        try{
            
            String user        = request.getParameter("usuario");
            String clas        =""+ request.getParameter("clas");
            String filtro        = ""+request.getParameter("filtro");
            String configuracion    = ""+request.getParameter("conf");
            String var1        = ""+request.getParameter("var1");
            String var2     = ""+request.getParameter("var2");
            String linea= ""+request.getParameter("linea");
            Vector v = new Vector();
            v = model.traficoService.vAtributos();
            String var3="";
            for (int  i =0;i<v.size();i++){
                Vector fila = (Vector)v.elementAt(i);
                String valor =(String) fila.elementAt(2);
                if(request.getParameter("reestablecer")==null){
                    valor=""+request.getParameter(""+fila.elementAt(1));
                    if( valor.equals("null")||valor.equals("")){
                        valor="10";
                    }
                }
                int va= Integer.parseInt(valor);
                va=va;
                ////System.out.println("valor"+va);
                String nombre =""+ fila.elementAt(0);
                ////System.out.println("nombre"+nombre);
                var3=var3+"-"+nombre+"-"+va;
            }
            ////System.out.println("var3: "+ var3);
            
            model.traficoService.listarColTrafico(var3);
            Vector col = model.traficoService.obtColTrafico();
            model.traficoService.listarColtrafico2(col);
            col= model.traficoService.obtColTrafico();
            
            ////System.out.println("Filtro: "+ filtro);
            
            filtro=model.traficoService.decodificar(filtro);
            
            
            model.traficoService.gennerarVZonasTurno(user);
            ////System.out.println("Genrerar zonas ");
            Vector zonas = model.traficoService.obdtVectorZonas();
            String validacion=" zona='-1'";
            
            if ((zonas == null)||zonas.size()<=0){}else{
                validacion = model.traficoService.validacion(zonas);
            }
            ////System.out.println("\nValidacion final: "+ validacion);
            if (clas.equals("null")||clas.equals("")){
                clas="";
            }
            if (configuracion.equals("null")||configuracion.equals("")){
                configuracion = model.traficoService.obtConf();
            }
            if (filtro.equals("null")||filtro.equals("")){
                filtro = "";
                filtro= "where "+validacion+filtro;
            }
            else{
                filtro=filtro;
            }
            configuracion = configuracion+" ,color_letra,CASE WHEN reg_status='D' THEN get_ultimaobservaciontrafico(planilla,'detencion') ELSE ult_observacion END AS obs,neintermedias ";
            String consulta      = model.traficoService.getConsulta(configuracion,filtro,clas); 
            ////System.out.println("consulta: "+ consulta);
            
            Vector colum= new Vector();
            
            colum = model.traficoService.obtenerCampos(linea);
            model.traficoService.generarTabla(consulta, colum);
            ////System.out.println("genera tabla ");
            
            
            ConfTraficoU vista = new ConfTraficoU();
            vista.setConfiguracion(configuracion.replaceAll("-_-","'"));
            vista.setFiltro(filtro);
            vista.setLinea(linea);
            vista.setClas(clas);
            vista.setVar1(var1);
            vista.setVar2(var2);
            vista.setVar3(var3);
            vista.setCreation_user(user);
            model.traficoService.actualizarConfiguracion(vista);
            
            filtro=model.traficoService.codificar(filtro);
            ////System.out.println("entra en next ");
            String next = "/jsp/trafico/controltrafico/vertrafico2.jsp?var1="+var1+"&var2="+var2+"&var3="+var3+"&filtro="+filtro+"&linea="+linea+"&conf="+configuracion.replaceAll("'","-_-")+"&clas="+clas+"&reload=";
            
            ////System.out.println("sale del next ");
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null){
                throw new Exception("No se pudo encontrar "+ next);
            }
            rd.forward(request, response);
        }
        catch(Exception e){
            throw new ServletException("Accion:"+ e.getMessage());
        }
    }
}
