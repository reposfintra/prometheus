/***********************************************
 * Nombre clase: PlacaInsertAction.java
 * Descripci�n: Accion para ingresar una placa a la bd.
 * Autor: AMENDEZ
 * Fecha: 4 de noviembre de 2004, 02:48 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 **********************************************/

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;

import org.apache.log4j.*;

public class PlacaInsertAction extends Action{
    
    Logger logger = Logger.getLogger(this.getClass());
    
    //static Logger logger = Logger.getLogger(PlacaInsertAction.class);
    
    /** Creates a new instance of PlacaInsertAction */
    public PlacaInsertAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "";
        String pag = (request.getParameter("pag")!=null)? request.getParameter("pag") : "";
        String grupoEquipo = request.getParameter("grupo_equipo")!=null?request.getParameter("grupo_equipo"):"";
        if(grupoEquipo.equals("T")){
            next = "/placas/trailerInsert.jsp?msg=";
        } else if(grupoEquipo.equals("C")){
            next = "/placas/placasInsert.jsp?msg=";
        } else if(grupoEquipo.equals("O")){
            next = "/placas/placaInsert.jsp?msg=";
        }
        String cmd = request.getParameter("cmd");
        Placa placa = new Placa();
        Date fecha = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String fechacrea = format.format(fecha);
        String msg = "";
        ////System.out.println ("entro placa");
        if(cmd == null)
            next = "/placas/placaInsert.jsp?msg=";
        
        else{
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario)session.getAttribute("Usuario");
            //            ////System.out.println ("paso placa");
            
            //**************************    AMATURANA
            String capac_gal = request.getParameter("capac_gal")!=null ? request.getParameter("capac_gal") : "0";
            String capac_mts = request.getParameter("capac_mts")!=null ? request.getParameter("capac_mts") : "0";
            
            capac_gal = capac_gal.equals("")? "0" : capac_gal;
            capac_mts = capac_mts.equals("")? "0" : capac_mts;
            
            logger.info("capac_gal: " + capac_gal);
            logger.info("capac_mts: " + capac_mts);
            
            placa.setCapac_gal(capac_gal);
            placa.setCapac_mts(capac_mts);
            //*************************
            
            placa.setReg_status("A");
            placa.setPlaca(request.getParameter("placa").toUpperCase());
            placa.setCondicion((request.getParameter("condicion")!=null)?request.getParameter("condicion"):"");
            placa.setTarjetaoper((request.getParameter("tarjetaoper")!=null)?request.getParameter("tarjetaoper"):"");
            placa.setVenctarjetaoper((request.getParameter("venctarjetaoper")!=null && !request.getParameter("venctarjetaoper").equals(""))?request.getParameter("venctarjetaoper"):"0099-01-01");
            placa.setMarca((request.getParameter("marca")!=null)?request.getParameter("marca"):"");
            placa.setClase((request.getParameter("clase")!=null)?request.getParameter("clase"):"");
            placa.setCapacidad((request.getParameter("capacidad")!=null)?request.getParameter("capacidad"):"");
            placa.setCarroceria((request.getParameter("carroceria")!=null)?request.getParameter("carroceria"):"");
            placa.setModelo((request.getParameter("modelo")!=null)?request.getParameter("modelo"):"");
            placa.setColor((request.getParameter("color")!=null)?request.getParameter("color"):"");
            placa.setNomotor((request.getParameter("nomotor")!=null)?request.getParameter("nomotor"):"");
            placa.setNochasis((request.getParameter("noChasis")!=null)?request.getParameter("noChasis"):"");
            placa.setNoejes((request.getParameter("noejes")!=null)?request.getParameter("noejes"):"");
            placa.setAgencia((request.getParameter("agencia")!=null)?request.getParameter("agencia"):"");
            placa.setDimcarroceria((request.getParameter("dimcarroceria")!=null)?request.getParameter("dimcarroceria"):"");
            placa.setVenseguroobliga((request.getParameter("venseguroobliga")!=null && !request.getParameter("venseguroobliga").equals(""))?request.getParameter("venseguroobliga"):"0099-01-01");
            placa.setHomologado((request.getParameter("homologado")!=null)?request.getParameter("homologado"):"");
            placa.setAtitulo((request.getParameter("atitulo")!=null)?request.getParameter("atitulo"):"");
            placa.setEmpresaafil((request.getParameter("empafiliada")!=null)?request.getParameter("empafiliada"):"");
            placa.setPropietario((request.getParameter("propietario")!=null)?request.getParameter("propietario"):"");
            placa.setConductor((request.getParameter("conductor")!=null)?request.getParameter("conductor"):"");
            placa.setTenedor((request.getParameter("tenedor")!=null)?request.getParameter("tenedor"):"");
            placa.setTipo((request.getParameter("tipo")!=null)?request.getParameter("tipo"):"");
            placa.setTara((request.getParameter("tara")!=null)?request.getParameter("tara"):"");
            placa.setLargo((request.getParameter("largo")!=null)?request.getParameter("largo"):"");
            placa.setAlto((request.getParameter("alto")!=null)?request.getParameter("alto"):"");
            placa.setLlantas((request.getParameter("llantas")!=null)?request.getParameter("llantas"):"");
            placa.setAncho((request.getParameter("ancho")!=null)?request.getParameter("ancho"):"");
            placa.setPiso((request.getParameter("piso")!=null)?request.getParameter("piso"):"");
            placa.setCargue((request.getParameter("cargue")!=null)?request.getParameter("cargue"):"");
            placa.setVolumen((request.getParameter("volumen")!=null)?request.getParameter("volumen"):"");
            placa.setRecurso((request.getParameter("recurso")!=null)?request.getParameter("recurso"):"");
            placa.setGrupoid((request.getParameter("grupoid")!=null)?request.getParameter("grupoid"):"");
            placa.setNombre((request.getParameter("nombre")!=null)?request.getParameter("nombre"):"");
            placa.setGrupo((request.getParameter("grupo")!=null)?request.getParameter("grupo"):"");
            placa.setEstadoequipo((request.getParameter("estadoequipo")!=null)?request.getParameter("estadoequipo"):"");
            placa.setLocalizacion((request.getParameter("localizacion")!=null)?request.getParameter("localizacion"):"");
            placa.setFechacrea(fechacrea);
            placa.setFechaultact(fechacrea);
            //25-09-2005
            placa.setNumero_rin((request.getParameter("norin")!=null)?request.getParameter("norin"):"");
            placa.setPlaca_trailer((request.getParameter("placatrailer")!=null)?request.getParameter("placatrailer"):"");
            placa.setCapacidad_trailer((request.getParameter("captrailer")!=null)?request.getParameter("captrailer"):"");
            placa.setTarhabil((request.getParameter("tarhabil")!=null)?request.getParameter("tarhabil"):"");
            placa.setFecvenhabil((request.getParameter("venhabilitacion")!=null && !request.getParameter("venhabilitacion").equals(""))?request.getParameter("venhabilitacion"):"0099-01-01");
            placa.setTarprop((request.getParameter("tarpropiedad")!=null)?request.getParameter("tarpropiedad"):"");
            placa.setFecvenprop((request.getParameter("venpropiedad")!=null && !request.getParameter("venpropiedad").equals(""))?request.getParameter("venpropiedad"):"0099-01-01");
            placa.setPolizasoat((request.getParameter("soat")!=null)?request.getParameter("soat"):"");
            placa.setVenseguroobliga((request.getParameter("venseguroobliga")!=null && !request.getParameter("venseguroobliga").equals(""))?request.getParameter("venseguroobliga"):"0099-01-01");
            placa.setCertgases((request.getParameter("certgases")!=null)?request.getParameter("certgases"):"");
            placa.setFecvengases((request.getParameter("fecvengases")!=null && !request.getParameter("fecvengases").equals(""))?request.getParameter("fecvengases"):"0099-01-01");
            placa.setTarempresa((request.getParameter("tarEmpresa")!=null)?request.getParameter("tarEmpresa"):"");
            placa.setFecvenempresa((request.getParameter("venempresa")!=null && !request.getParameter("venempresa").equals(""))?request.getParameter("venempresa"):"0099-01-01");
            placa.setReg_nal_carga((request.getParameter("registro")!=null)?request.getParameter("registro"):"");
            placa.setFecvenreg((request.getParameter("venregistro")!=null && !request.getParameter("venregistro").equals(""))?request.getParameter("venregistro"):"0099-01-01");
            placa.setPoliza_andina((request.getParameter("polizaandina")!=null)?request.getParameter("polizaandina"):"");
            placa.setFecvenandina((request.getParameter("venpolizaandina")!=null && !request.getParameter("venpolizaandina").equals(""))?request.getParameter("venpolizaandina"):"0099-01-01");
            placa.setTenedor((request.getParameter("tenedor")!=null)?request.getParameter("tenedor"):"");
            placa.setGrado_riesgo((request.getParameter("griesgo")!=null && !request.getParameter("griesgo").equals(""))?request.getParameter("griesgo"):"0");
            //jose 2006-02-15
            placa.setGrupo_equipo( ((request.getParameter("grupo_equipo"))!=null)?(request.getParameter("grupo_equipo")).toUpperCase():"O");
            placa.setCiudad_tarjeta((request.getParameter("c_ciudad")!=null)?request.getParameter("c_ciudad"):"");
            placa.setResp_civil((request.getParameter("rcivil")!=null)?request.getParameter("rcivil"):"");
            String venrcivil = request.getParameter("venrcivil");
            placa.setFecvresp_civil(  venrcivil == null || "".equals(venrcivil)?"0099-01-01": venrcivil );
            placa.setDescuento_equipo((request.getParameter("c_descuento")!=null)?request.getParameter("c_descuento"):"N");
            placa.setClasificacion_equipo((request.getParameter("c_clasificacion")!=null)?request.getParameter("c_clasificacion"):"");
            //hosorio 2006-02-27
            placa.setEstadoEscolta(request.getParameter("estado_escolta")!=null?request.getParameter("estado_escolta"):"");
            
            //Osvaldo
            placa.setOperador_gps( request.getParameter("operador_gps")!=null?request.getParameter("operador_gps"):"");
            placa.setPasswd_seguimiento( request.getParameter("clave_seg")!=null?request.getParameter("clave_seg"):"");
            
            // modificado Enrique De Lavalle 2007-04-30
            String causa = "";
            String ob = "";
            String veto = "";
            String fuente = "";
            if (request.getParameter("uservetonit")!=null){
                if (request.getParameter("uservetonit").equals("N")){
                    placa.setVeto( request.getParameter("veto")!=null?request.getParameter("veto"):"S");
                    causa = request.getParameter("causa")!=null ? request.getParameter("causa") : "IRN";
                    ob = request.getParameter("observacion")!=null ? request.getParameter("observacion") : "NINGUNA";
                    veto = request.getParameter("veto")!=null?request.getParameter("veto"):"S";
                    fuente = request.getParameter("fuente")!=null?request.getParameter("fuente"):"";
                }else{
                    placa.setVeto( request.getParameter("veto")!=null?request.getParameter("veto"):"S");
                    String vet = placa.getVeto();
                    if (vet.equals("S")){
                        causa = request.getParameter("causa")!=null ? request.getParameter("causa") : "IRN";
                        ob = request.getParameter("observacion")!=null ? request.getParameter("observacion") : "NINGUNA";
                        veto = request.getParameter("veto")!=null?request.getParameter("veto"):"S";
                        fuente = request.getParameter("fuente")!=null?request.getParameter("fuente"):"";
                    }else{
                        veto = request.getParameter("veto")!=null?request.getParameter("veto"):"S";
                    }
                }
            }
            
            
            if (usuario != null){
                placa.setUsuario(usuario.getLogin());
                placa.setUsuariocrea(usuario.getLogin());
            }
            if (pag.equals("ESC")){
                next = "/jsp/trafico/caravana/escoltaInsert.jsp?msg=";
            }
            try{
                if (!model.placaService.placaExist(placa.getPlaca())){
                    /*
                    if (!model.identidadService.existePropietario(placa.getPropietario())){
                        msg="EL NIT DEL PROPIETARIO NO EXISTE";
                    } else if (!model.conductorService.existeConductorCGA( placa.getConductor())){
                        msg="EL NIT DEL CONDUCTOR NO EXISTE";
                    }
                     */
                    if(!pag.equals("ESC")) {
                        /*if (!model.placaService.nitExist(placa.getTenedor()) && (!request.getParameter("grupo_equipo").equals("T")) ){
                            msg="EL NIT DEL TENEDOR NO EXISTE";
                        } */
                        
                        String validacion = this.validar(placa);
                        
                        if( !validacion.equals("") ){
                            msg = validacion;
                        }
                        else {
                            if( placa.getOperador_gps().length() <= 10 ){
                                model.placaService.insertaPlaca(placa);
                                model.vetoSvc.insertarVetoNuevo(usuario.getDstrct(), usuario.getLogin(), placa.getPlaca(), usuario.getBase(), causa, ob, veto, fuente, "P");
                            }else{
                                msg="No se puede guardar la placa, el c�digo del operador GPS tiene longitud mayor de 10 caracteres, deber� modificarlo";
                            }
                            try{
                                model.veridocService.AgregarDocumentos("Placa");//Actualiza los campos de documentos
                            }catch(Exception e){}
                            //*******************autor Leonardo Parody***************************************************************************************
                            model.placaService.archivoTxt("/rmi/Sincronizacion" , placa.getPlaca(), "placa", usuario, "placa");
                            //*******************************************************************************************************************************
                            msg = "Placa agregada exitosamente";
                            enviarCorreo(placa);
                        }
                    } else {
                        /*Insert de vehiculos Tipo Escolta (ESC)*/
                        if (!model.conductorService.existeConductorCGA( placa.getConductor()))
                            msg = "EL NIT DEL CONDUCTOR NO EXISTE";
                        else if (model.placaService.placaExist(placa.getPlaca()))
                            msg="LA PLACA QUE DESEA CREAR YA EXISTE";
                        else {
                            model.placaService.insertaPlaca(placa);
                            msg = "Placa de Escoltas agregada exitosamente";
                        }
                    }
                }
                else{
                    String est = model.placaService.buscarEstadoPlaca(placa.getPlaca());
                    if(est.equals("A")){
                        msg="LA PLACA QUE DESEA CREAR YA EXISTE";
                    }else{
                        model.placaService.actualizaPlaca(placa);
                        msg = "Placa agregada exitosamente";
                    }
                    
                }
            }catch (Exception e){
                e.printStackTrace();
                throw new ServletException(e.getMessage());
            }
        }
        // Redireccionar a la p�gina indicada.
        next+=msg;
        this.dispatchRequest(next);
    }
    
    public String validar( Placa placa ) throws Exception{
        String msg = "";
        
        if (!model.identidadService.existePropietario(placa.getPropietario())){
            msg = "EL NIT DEL PROPIETARIO NO EXISTE";
        } else if (!model.conductorService.existeConductorCGA( placa.getConductor()) && placa.getGrupo_equipo().matches("C|O") ){
            msg = "EL NIT DEL CONDUCTOR NO EXISTE";
        } else if(!placa.getTenedor().trim().equals("")){
            if (!model.placaService.nitExist(placa.getTenedor())){
                msg = "EL NIT DEL TENEDOR NO EXISTE";
            }
        }
        //modificacion 2007-04-25 Enrique De Lavalle
        if (!placa.getRecurso().equals("")){ //modificacion Enrique De Lavalle 2007-04-24
            System.out.println("rec: " + placa.getRecurso()+" recurso placa");
            if (model.placaService.verificarRecurso(placa.getRecurso())){
                if(!model.placaService.obtenerTipoPlacaRecurso(placa.getRecurso()).equals(placa.getGrupo_equipo())){
                    msg = "EL RECURSO NO CORRESPONDE AL TIPO DE PLACA";
                }
            }else{
                msg = "EL RECURSO NO EXISTE";
            }
        }
        return msg;
    }
    
    public void enviarCorreo(Placa placa) throws Exception {
        try{
            String emailsI = "";
            com.tsp.operation.model.beans.SendMail em = new com.tsp.operation.model.beans.SendMail();
            em.setEmailfrom("");
            em.setEmailfrom("procesos@sanchezpolo.com");
            em.setSendername( "INFORMACI�N" );
            em.setRecstatus("A");
            em.setEmailcode("");
            em.setEmailcopyto("");
            em.setEmailsubject("INFORMACI�N A PERSONAL DE TSP");
            em.setEmailbody("Estamos enviando a uds. un informe de la placa que se ha ingresado: "+"\nNumero de la Placa: "+ placa.getPlaca()+ "\nNIT del Propietario: "+placa.getPropietario()+"\nTenedor: "+placa.getTenedor()+" \n\n\nAtentamente,\n\n\nTransporte S�nchez Polo" );
            em.setEmailto( "cnavarro@mail.tsp.com" );
            em.setTipo( "I" );
            model.sendMailService.sendMail(em);
            em.setEmailfrom("");
            em.setEmailfrom("procesos@sanchezpolo.com");
            em.setSendername( "INFORMACI�N" );
            em.setRecstatus("A");
            em.setEmailcode("");
            em.setEmailcopyto("");
            em.setEmailsubject("INFORMACI�N A PERSONAL DE TSP");
            em.setEmailbody("Estamos enviando a uds. un informe de la placa que se ha ingresado: "+"\nNumero de la Placa: "+ placa.getPlaca()+ "\nNIT del Propietario: "+placa.getPropietario()+"\nTenedor: "+placa.getTenedor()+" \n\n\nAtentamente,\n\n\nTransporte S�nchez Polo" );
            em.setEmailto("apolifroni@sanchezpolo.com");
            em.setTipo("E");
            
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
    }
    
}
