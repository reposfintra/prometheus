
/**
* Autor  : Ing. Roberto Rocha P..
* Date  : 10 de Julio de 2007
* Copyrigth Notice : Fintravalores S.A. S.A
* Version 1.0
-->
<%--s
-@(#)
--Descripcion : Action que maneja los avales de Fenalco
**/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.threads.*;
import com.tsp.util.Util;
import com.tsp.operation.model.beans.Usuario;
import javax.servlet.http.*;
import java.sql.SQLException;
import com.tsp.operation.model.*;


public class ElectricaribeVerAction extends Action{
    
    
   //protected HttpServletRequest request;
    public ElectricaribeVerAction() {
    }



    public void run()
    {   String next="/jsp/delectricaribe/datos_electricaribe.jsp";
        HttpSession session  = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        String op=request.getParameter ("opcion");
        String itemillo=request.getParameter ("item");
        String new_num_os=request.getParameter ("new_num_os");
        
        //.out.println("op"+op);
        
	if(op.equals("temporal")||op.equals("temporall"))
        {   String idorden="",consecutiv="", num_oss="";

            if(op.equals("temporall"))
            {   idorden=model.electricaribeVerSvc.getOferta().getIdOrden();
                consecutiv=model.electricaribeVerSvc.getOferta().getConsecutivo();
                num_oss=model.electricaribeVerSvc.getOferta().getNumOs();
            }
            Accord acc=new Accord();
            Oferta ofr=new Oferta();
            if(op.equals("temporall"))
            {   ofr.setIdOrden(idorden);
                ofr.setConsecutivo(consecutiv);
                ofr.setNumOs(num_oss);
            }
            
            ofr.setCuotas(request.getParameter("cuota"));
            ofr.setDetalleInconsistencia(request.getParameter("detalle_inconsistencia"));
            ofr.setFechaOferta(request.getParameter("fecha_oferta"));
            ofr.setEstudioEconomico(request.getParameter("estudio_economico"));
            ofr.setEsquemaComision(request.getParameter("esquema_comision"));
            //ofr.setConsecutivo(request.getParameter("consecutivo"));

            String clix[]=request.getParameter("cliente").split("_");

            ofr.setNombreCliente(clix[0]);
            ofr.setIdCliente(clix[1]);

            String contx[]=request.getParameter("id_contratista").split("_");

            acc.setIdContratista(contx[1]);
            acc.setNombreContratista(contx[0]);

            acc.setAcciones(request.getParameter("acciones"));
            acc.setValorMateriales(request.getParameter("valor_materiales"));
            acc.setValorManoObra(request.getParameter("valor_mano_obra"));
            acc.setValorOtros(request.getParameter("valor_otros"));
            //acc.setFactConformada(request.getParameter("fact_conformada"));
            acc.setAdministracion(request.getParameter("aiu_administracion"));
            acc.setImprevistos(request.getParameter("aiu_imprevistos"));
            acc.setUtilidad(request.getParameter("utilidades"));
            acc.setIdAccion(request.getParameter("item"));
            if(Integer.parseInt((String)request.getParameter("item"))-1<=model.electricaribeVerSvc.getAccords().size())
            {   //.out.println("Entro y agrego el archivo");
                model.electricaribeVerSvc.addAccord(Integer.parseInt((String)request.getParameter("item"))-1,acc);
                
                model.electricaribeVerSvc.registrarCambioEnAccion(Integer.parseInt((String)request.getParameter("item"))-1,acc);
                                
            }
            else
            {   next=next+"Debe introducir un indice valido";
                if(op.equals("temporall"))
                {   next="/jsp/delectricaribe/modificar_electricaribe.jsp"+"Debe introducir un indice valido";
                }
            }
            //.out.println("Tama�o de el accord:"+model.electricaribeVerSvc.getAccords().size());
            try
            {   model.electricaribeVerSvc.setOferta(ofr);
            }
            catch(Exception ee)
            {   System.out.println("Error:"+ee.toString());
            }
            if(op.equals("temporall"))
            {   next="/jsp/delectricaribe/modificar_electricaribe.jsp";
            }

        }
        else
        {   if(op.equals("cancelar")||op.equals("cancelarr"))
            {   model.electricaribeVerSvc.reset();
                if(op.equals("cancelarr"))
                {   next="/jsp/delectricaribe/modificar_electricaribe.jsp";
                }
            }
            else
            {   if(op.equals("commite")||op.equals("commitee"))
                {   
                    model.electricaribeVerSvc.getOferta().setNewNumOs(request.getParameter("new_num_os"));
                    model.electricaribeVerSvc.getOferta().setNumOs(request.getParameter("num_os_old"));
                    try
                    {   if(op.equals("commitee"))
                        {   next="/jsp/delectricaribe/modificar_electricaribe.jsp"+"?respuesta="+model.electricaribeVerSvc.insertarMs(usuario.getLogin(),"CS");
                        }
                        else
                        {   if(op.equals("commitee"))
                            {   next="/jsp/delectricaribe/modificar_electricaribe.jsp"+"?respuesta="+model.electricaribeVerSvc.insertarMs(usuario.getLogin(),"CS");
                            }
                            else{
                                next=next+"?respuesta="+model.electricaribeVerSvc.insertarMs(usuario.getLogin(),"CS");
                            }
                        }
                    }
                    catch(Exception e)
                    {  
                        next=next+"?respuesta="+e.toString();
                        System.out.println("Errorr:"+e.toString());
                    }
                }
                else
                {   if(op.equals("charge"))
                    {   String id_orden=request.getParameter("multiservicios");
                        model.electricaribeVerSvc.CargarArrays(id_orden);
                        //.out.println("va");
                        next="/jsp/delectricaribe/modificar_electricaribe.jsp";
                    }
                    else
                    {   if(op.equals("delete"))
                        {   model.electricaribeVerSvc.delAccord(Integer.parseInt((String)request.getParameter("item"))-1); 
                            next="/jsp/delectricaribe/modificar_electricaribe.jsp";
                        }
                    }
                    if(op.equals("consultarx")) { 
                        next="/jsp/delectricaribe/modificar_electricaribe.jsp?itemx="+request.getParameter("item");
                    }                    
                    
                    
                    if(op.equals("commite_emergencia")) { 
                        next="/jsp/delectricaribe/creacion_emergencia.jsp";
                        
                        
                        Oferta ofr=new Oferta();
                        ofr.setCuotas(request.getParameter("cuota"));
                        ofr.setDetalleInconsistencia(request.getParameter("detalle_inconsistencia"));
                        ofr.setFechaOferta(request.getParameter("fecha_oferta"));
                        ofr.setEstudioEconomico(request.getParameter("estudio_economico"));
                        ofr.setEsquemaComision(request.getParameter("esquema_comision"));
                        String clix[]=request.getParameter("cliente").split("_");
                        ofr.setNombreCliente(clix[0]);
                        ofr.setIdCliente(clix[1]);
                        
                        String[] contratistis=request.getParameterValues("contratista1");
                        for (int j=0;j<contratistis.length;j++){
                            //.out.println("contratistis[j]"+contratistis[j]);
                            Accord acc=new Accord();
                            acc.setIdContratista(contratistis[j]);
                            
                            acc.setAcciones(request.getParameter("acciones"));
                            acc.setValorMateriales(request.getParameter("valor_materiales"));
                            acc.setValorManoObra(request.getParameter("valor_mano_obra"));
                            acc.setValorOtros(request.getParameter("valor_otros"));
                            //acc.setFactConformada(request.getParameter("fact_conformada"));
                            acc.setAdministracion(request.getParameter("aiu_administracion"));
                            acc.setImprevistos(request.getParameter("aiu_imprevistos"));
                            acc.setUtilidad(request.getParameter("utilidades"));
                                                                         
                            model.electricaribeVerSvc.addAccord(j,acc);
                           
                            //.out.println("Tama�o de el accord:"+model.electricaribeVerSvc.getAccords().size());
                            
                        }
                        //.out.println("Tama�o de el accord:"+model.electricaribeVerSvc.getAccords().size());
                        try{
                          model.electricaribeVerSvc.setOferta(ofr);
                        }catch(Exception ee){
                          System.out.println("Error:"+ee.toString());
                        }
                        
                        try{
                          next=next+"?respuesta="+model.electricaribeVerSvc.insertarMs(usuario.getLogin(),"EM");
                        }catch(Exception e){
                          next=next+"?respuesta="+e.toString();
                          System.out.println("Errorr:"+e.toString());
                        }
                        /*
                        String contx[]=request.getParameter("id_contratista").split("_");
                        acc.setIdContratista(contx[1]);
                        acc.setNombreContratista(contx[0]);
                        acc.setAcciones(request.getParameter("acciones"));
                        acc.setValorMateriales(request.getParameter("valor_materiales"));
                        acc.setValorManoObra(request.getParameter("valor_mano_obra"));
                        acc.setValorOtros(request.getParameter("valor_otros"));
                        //acc.setFactConformada(request.getParameter("fact_conformada"));
                        acc.setAdministracion(request.getParameter("aiu_administracion"));
                        acc.setImprevistos(request.getParameter("aiu_imprevistos"));
                        acc.setUtilidad(request.getParameter("utilidades"));
                        */
                        
                    }
                    
                    
                    if(op.equals("commite2")) {   
                      next="/jsp/delectricaribe/predatos_electricaribe.jsp";
                      //inicio de agregar item1
                      Accord acc=new Accord();
                      Oferta ofr=new Oferta();
                      ofr.setCuotas(request.getParameter("cuota"));
                      ofr.setDetalleInconsistencia(request.getParameter("detalle_inconsistencia"));
                      ofr.setFechaOferta(request.getParameter("fecha_oferta"));
                      ofr.setEstudioEconomico(request.getParameter("estudio_economico"));
                      ofr.setEsquemaComision(request.getParameter("esquema_comision"));
                      String clix[]=request.getParameter("cliente").split("_");
                      ofr.setNombreCliente(clix[0]);
                      ofr.setIdCliente(clix[1]);
                      String contx[]=request.getParameter("id_contratista").split("_");
                      acc.setIdContratista(contx[1]);
                      acc.setNombreContratista(contx[0]);
                      acc.setAcciones(request.getParameter("acciones"));
                      acc.setValorMateriales(request.getParameter("valor_materiales"));
                      acc.setValorManoObra(request.getParameter("valor_mano_obra"));
                      acc.setValorOtros(request.getParameter("valor_otros"));
                      //acc.setFactConformada(request.getParameter("fact_conformada"));
                      acc.setAdministracion(request.getParameter("aiu_administracion"));
                      acc.setImprevistos(request.getParameter("aiu_imprevistos"));
                      acc.setUtilidad(request.getParameter("utilidades"));
                      if(Integer.parseInt((String)request.getParameter("item"))-1<=model.electricaribeVerSvc.getAccords().size())                    {
                          //.out.println("Entro y agrego el archivo");
                          model.electricaribeVerSvc.addAccord(Integer.parseInt((String)request.getParameter("item"))-1,acc);
                      }else{
                          next=next+"Debe introducir un indice valido";
                      }
                      //.out.println("Tama�o de el accord:"+model.electricaribeVerSvc.getAccords().size());
                      try{
                          model.electricaribeVerSvc.setOferta(ofr);
                      }catch(Exception ee){
                          System.out.println("Error:"+ee.toString());
                      }
                      //fin de agregar item1
                      //inicio de agregar ms
                      try{
                          next=next+"?respuesta="+model.electricaribeVerSvc.insertarMs(usuario.getLogin(),"CS");
                      }catch(Exception e){
                          next=next+"?respuesta="+e.toString();
                          System.out.println("Errorr:"+e.toString());
                      }
                      //fin de agregar ms
                  }




                }
            }
        }
        //.out.println("nextt"+next);
        try
        {   this.dispatchRequest(next);
        }
        catch(Exception eee)
        {   System.out.println("Error:"+eee.toString());
        }
        
    }
}