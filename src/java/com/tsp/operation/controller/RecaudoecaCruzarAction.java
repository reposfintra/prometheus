package com.tsp.operation.controller;
import java.util.*;
import java.text.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
public class RecaudoecaCruzarAction  extends Action {
    public RecaudoecaCruzarAction() {    }
    public void run() throws javax.servlet.ServletException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = session.getAttribute("Distrito").toString();
        String evento = ( request.getParameter("evento")!= null) ? request.getParameter("evento") : "";
        String next  = "";
        String msj   = "";
        try {
            if (evento.equalsIgnoreCase("CRUZAR_RECAUDO_CXC_ECA")) {
                HIngresosEca hilo = new HIngresosEca();
                hilo.start(model, usuario,"FINV");

                next  = "/jsp/applus/cruzarRecaudoConCxc.jsp?msj=";
                msj   = "Proceso Iniciado.";

            }

            if (evento.equalsIgnoreCase("CRUZAR_RECAUDO_CXC_ECA2")) {
                HIngresosEca2 hilo2 = new HIngresosEca2();
                hilo2.start(model, usuario,"FINV");

                next  = "/jsp/applus/cruzarRecaudoConCxc.jsp?msj=";
                msj   = "Proceso Iniciado.";

            }

            if (evento.equalsIgnoreCase("CRUZAR_RECAUDO_CXC_ECA2_PMS")) {
                HIngresosEcaPms2 hilo2 = new HIngresosEcaPms2();
                hilo2.start(model, usuario,"FINV");
                next  = "/jsp/applus/cruzarRecaudoConCxc.jsp?msj=";
                msj   = "Proceso Iniciado.";
            }

            //inicio de 20100602
            if (evento.equalsIgnoreCase("CRUZAR_RECAUDO_CXC_ECA3_PMS")) {
                HIngresosEcaPms3 hilo3 = new HIngresosEcaPms3();
                hilo3.start(model, usuario,"FINV");
                next  = "/jsp/applus/cruzarRecaudoConCxc3.jsp?msj=";
                msj   = "Proceso Iniciado.";
            }
            //fin de 20100602

            next += msj ;
            this.dispatchRequest(next);
        }catch (Exception e) {
            System.out.println("erorr en action"+e.toString());
            e.printStackTrace();
        }
    }
}