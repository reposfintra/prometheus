/*********************************************************************************
 * Nombre clase :                 ReporteProveedoresFintraAction.java            *
 * Descripcion :                  Clase que maneja los eventos                   *
 *                                relacionados con el programa que busca el      *
 *                                reporte de los proveedores de fintra en la BD. *
 * Autor :                        LREALES                                        *
 * Fecha :                        13 de septiembre de 2006, 08:00 AM             *
 * Version :                      1.0                                            *
 * Copyright :                    Fintravalores S.A.                        *
 ********************************************************************************/

package com.tsp.operation.controller;

import java.util.Vector;
import java.lang.*;
import java.sql.*;
import javax.servlet.ServletException;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ReporteProveedoresFintraAction extends Action {
    
    /** Creates a new instance of ReporteProveedoresFintraAction */
    public ReporteProveedoresFintraAction () { }
    
    public void run () throws ServletException, InformationException {
        
        String next = "/jsp/general/exportar/proveedores_fintra/ReporteProveedoresFintra.jsp";
        
        HttpSession session = request.getSession ();
        Usuario usuario = ( Usuario ) session.getAttribute ( "Usuario" );
        
        String hacer = ( request.getParameter("hacer")!= null )?request.getParameter("hacer").toUpperCase():"";
        
        String fecini = ( request.getParameter("fecini")!= null )?request.getParameter("fecini").toUpperCase():"";
        String fecfin = ( request.getParameter("fecfin")!= null )?request.getParameter("fecfin").toUpperCase():"";
        
        String nit_prov = ( request.getParameter("nit_prov")!= null )?request.getParameter("nit_prov").toUpperCase():"";
        
        String opcion = ( request.getParameter("opcion")!= null )?request.getParameter("opcion"):"";

        try {
            
            Vector datos = model.reporteProveedoresFintraService.getVectorReporte ();
            
            if ( opcion.equals("1") ) {

                model.reporteProveedoresFintraService.reporteTodos ( fecini, fecfin );
                datos = model.reporteProveedoresFintraService.getVectorReporte ();  

            } else if ( opcion.equals("2")  ) {

                model.reporteProveedoresFintraService.reporteEspecifico ( nit_prov, fecini, fecfin );
                datos = model.reporteProveedoresFintraService.getVectorReporte (); 
            
            } /*else {

                next = next + "?msg=Defina un parametro de busqueda!";

            }   */
            
            if ( datos.size () > 0 ) {
                
                if ( hacer.equals( "2" ) ) {
                    
                    HiloReporteProveedoresFintra HRPF = new HiloReporteProveedoresFintra ();
                    HRPF.start ( datos, usuario.getLogin (), fecini, fecfin ); 
                    
                    next = next + "?msg=Archivo exportado a excel!!";
                    
                }
            
            } else{

                next = next + "?msg=Su busqueda no arrojo resultados!";

            }
            
        } catch ( Exception e ) {
            
            throw new ServletException ( "Error en Reporte de Proveedores de FINTRA Action : " + e.getMessage() );
            
        }        
        
        this.dispatchRequest ( next );
        
    }
    
}
