/**
* Autor : Ing. Roberto Rocha P..
* Date  : 10 de Julio de 2007
* Copyrigth Notice : Fintravalores S.A. S.A
* Version 1.0
-->
<%--
-@(#)
--Descripcion : Action que maneja los avales de Fenalco
**/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.threads.*;
import com.tsp.util.Util;
import com.tsp.operation.model.beans.Usuario;
import javax.servlet.http.*;
import java.sql.SQLException;


public class ProcesarRecaudosAction extends Action{
    
   //protected HttpServletRequest request;
    public ProcesarRecaudosAction() {
    }
    Usuario usuario = null;
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        try{
            HttpSession session = request.getSession();
            com.tsp.util.Util.logController(((com.tsp.operation.model.beans.Usuario)session.getAttribute("Usuario")).getLogin(),this.getClass().getName());
            Usuario usuario = (Usuario) session.getAttribute("Usuario");    
            String  base    = "/jsp/cxcobrar/ingreso/";
            String  next    = "procesar_recaudos_fenalco.jsp?msg=";
            String  msj     = "";
            int opcion   = Integer.parseInt(request.getParameter("opcion"));
            if( !model.procesarRecaudosFenalcoService.isProcess() ){
                model.procesarRecaudosFenalcoService.setProcess( true );
                HprocesarRecaudosFenalcoService  hilo = new HprocesarRecaudosFenalcoService();
                hilo.start(model, usuario, opcion);
                msj   = "Proceso de recaudos de Fenalco ha iniciado";
            }else{
              msj   = "Actualmente el proceso se est� realizando, por favor intente mas tarde....";
            }
            dispatchRequest( base + next + msj );
        }catch(Exception e){
            e.printStackTrace();
            System.out.println(e.toString());
            throw new ServletException(e.getMessage());
        }
    }//END RUN

    
}
