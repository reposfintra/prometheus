/************************************************************************
 * Nombre Discrepancia_productoAnularAction.java
 * Descripci�n: Accion para anular un producto de discrepancia.
 * Autor: Jose de la rosa
 * Fecha: 14 de octubre de 2005, 09:43 AM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 **************************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;

/**
 *
 * @author  Jose
 */
public class Discrepancia_productoAnularAction extends Action {
    
    /** Creates a new instance of Discrepancia_productoAnularAction */
    public Discrepancia_productoAnularAction () {
    }
    
    public void run () throws ServletException, com.tsp.exceptions.InformationException {
        String nom_doc = request.getParameter ("nom_doc");
        String clientes = request.getParameter ("client");
        String next="/jsp/cumplidos/discrepancia/MsgAnulado.jsp?reload=ok&nom_doc="+nom_doc+"&client="+clientes;
        HttpSession session = request.getSession ();
        String x = (String) request.getParameter ("x");
        try {
            model.discrepanciaService.deleteItemsDiscrepancia ( Integer.parseInt (x) );
        }catch (Exception e) {
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
    
}
