/*
 * BuscarZonaAction.java
 *
 * Created on 13 de junio de 2005, 10:00 AM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  Henry
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;

public class BuscarZonaUsuarioAction extends Action {
    
    /** Creates a new instance of BuscarZonaAction */
    public BuscarZonaUsuarioAction() {
    }
    
    public void run() throws ServletException {
        String next="/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina");        
        try{       
            //System.out.println(" " +request.getParameter("codigo")+ " " + request.getParameter("nombre"));      
            model.zonaUsuarioService.buscarZonaUsuarios(request.getParameter("codigo"), request.getParameter("nombre")); 
            next+="?mensaje="+request.getParameter("mensaje");
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }         
         // Redireccionar a la p�gina indicada.
       // next = Util.LLamarVentana(next, "Modificar Zona Usuario");
        this.dispatchRequest(next);

    }
    
}
