/*
 * PvuAnularAction.java
 *
 * Created on 21 de julio de 2005, 13:20
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  Rodrigo
 */
public class PvuAnularAction extends Action{
    
    /** Creates a new instance of PvuAnularAction */
    public PvuAnularAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/jsp/trafico/mensaje/MsgAnulado.jsp";
        HttpSession session = request.getSession();        
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String codigo = request.getParameter("c_codigo");
        ////System.out.println(codigo);
        try{
            model.perfilvistausService.anularPerfilVistaUsuario(usuario.getLogin().toUpperCase(),codigo);
            request.setAttribute("mensaje","MsgAnulado");
        }catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);

    }
    
}
