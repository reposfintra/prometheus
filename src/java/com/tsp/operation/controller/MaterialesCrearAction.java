/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.controller;
import com.tsp.exceptions.InformationException;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;
/**
 *
 * @author Rhonalf
 */
public class MaterialesCrearAction extends Action {

    public MaterialesCrearAction() {

    }

    public void run() throws ServletException, InformationException {
        String descr="";
        double pre=0.0;
        String consec="";
        String medida="UNIDADES";
        String tipo="M";
        String cat="";//091222
        String next  = "/jsp/provintegral/compras/buscar_producto.jsp";//091226
        request.setAttribute("msg", "Material ingresado correctamente");
        HttpSession session   = request.getSession();
        Usuario     usuario   = (Usuario) session.getAttribute("Usuario");
        String loginx=usuario.getLogin();
        MaterialesService pserv = new MaterialesService();
        String select   = "";
        ArrayList cats;
        String opcion=request.getParameter("opcion")!=null? request.getParameter("opcion") : "";
        //System.out.println("x "+opcion);
        try{
            if( opcion.equals("consulta") ){
                //este es el codigo que llena el select en la pagina de creacion de materiales
                //se hace por AJAX
                cats = pserv.getCategorias( request.getParameter("tipo").toLowerCase() );

                select = "<input type='radio' name='categoria' value='1'> Existente <select name='catSel' id='catSel'>";

                for(int i = 0; i < cats.size();i++){
                    select += "<option value='" + ((String)cats.get(i)) + "'>" + ((String)cats.get(i)) + "</option>";
                }

                select += "</select>";
            }
            else {
                //Aca se crea o modifica el producto
                descr = request.getParameter("descripcion");
                pre = Double.parseDouble(request.getParameter("precio"));
                consec = request.getParameter("producto");
                consec=pserv.contarProductos();//20100212temporal
                medida = request.getParameter("medida");
                tipo = request.getParameter("tipo");
                if (opcion==null || !(opcion.equals("modificar"))){
                    //Aca se crea
                    if(request.getParameter("categoria")!=null && !(request.getParameter("categoria").equals("")) && !(request.getParameter("categoria").equals("null")) ){
                        if( request.getParameter("categoria").equals("2") ){
                            cat = request.getParameter("nuevaCat");
                        }
                        else{
                            cat = request.getParameter("catSel");
                        }
                        //System.out.println("Categoria "+cat);
                        //System.out.println("En insercion ...");
                        pserv.insertarProducto(descr, pre,consec ,tipo,medida,cat,loginx);//0912226
                        //descripcion, precio, consec, tipo, medida, usuario);
                    }
                    else {
                        request.setAttribute("msg", "Error al ingresar... categoria es null");
                    }
                }

                if (opcion!=null && opcion.equals("modificar")){
                    //Aca se modifica
                    cat = request.getParameter("categoria");
                    System.out.println("Cat2 "+cat);
                    String exproducto=request.getParameter("exid");

                    model.negociosApplusService.modificarProducto(exproducto,consec,descr,pre,tipo,medida,loginx,cat);
                }
            }
            if( opcion.equals("consulta") ){
                //Esto hace parte del llenado del select de mas arriba
                try {

                    //response.setContentType("text/plain");//20100220
                    response.setContentType("text/plain; charset=utf-8");//20100220

                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().println( select );
                }
                catch (Exception ec){
                    System.out.println("Error colocando atributos del response en el action: "+ec.toString());
                }
            }
            else{
                this.dispatchRequest(next);
            }
        }
        catch(Exception ec){
            System.out.println("Error en el run del action: "+ec.toString());
            ec.printStackTrace();
            request.setAttribute("msg", "Error al ingresar...");
        }
    }

}