/********************************************************************
 *      Nombre Clase.................   TurnosIngresarAction.java
 *      Descripci�n..................   Action para Acceder al modulo de turnos en trafico
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  dlamadrid
 */
public class TurnosIngresarAction extends Action{
    
    /** Creates a new instance of TurnosIngresarAction */
    public TurnosIngresarAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next="";
        try {
            HttpSession session = request.getSession ();
            Usuario usuario = (Usuario) session.getAttribute ("Usuario");
            String userlogin=""+usuario.getLogin ();  
            model.usuarioService.getUsuariosPorDpto ("traf");
            TreeMap tm = model.usuarioService.usuariosPorDptoT ();
            String login = (String) tm.get(tm.firstKey());
            //////System.out.println(".............................. LOGIN: " + login);
            model.zonaService.listarZonasxUsuario (login);
            next="/jsp/trafico/turnos/insertar.jsp?marco=no";
            
        }
        catch (Exception e) {
            throw new ServletException (e.getMessage ());
            
        }
        this.dispatchRequest (next);
    }
    
}
