/*
 * TiquetesInsertAction.java
 *
 * Created on 3 de diciembre de 2004, 09:51 AM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  KREALES
 */
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;

public class TiquetesInsertAction extends Action{
    
    /** Creates a new instance of TiquetesInsertAction */
    public TiquetesInsertAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String nit= request.getParameter("nit");
        request.setAttribute("nit", "ECE0D8");
        String next = "/tiquetes/tiketInsert.jsp";
        String por = request.getParameter("porcentaje");
        float porcentaje =0;
        if (por!=null){
            porcentaje = Float.parseFloat(por);
        }
        
        String sucursal = request.getParameter("sucursal");
        int sw=0;
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");        
        try{
            if(model.proveedoracpmService.existProveedor(nit)){
                    Proveedor_Tiquetes pt = new Proveedor_Tiquetes();
                    pt.setNit(nit);
                    pt.setDstrct(usuario.getDstrct());
                    pt.setCreation_user(usuario.getLogin());
                    pt.setCity_code(request.getParameter("ciudad"));
                    pt.setValor(0);
                    pt.setCod_Migracion(request.getParameter("codigo_m").toUpperCase());
                    pt.setPorcentaje(porcentaje);
                    pt.setCodigo(sucursal);
                    try{
                            model.proveedortiquetesService.insertProveedor(pt,usuario.getBase());
                            next=next+"?mensaje=Tiquete Asignado..";
                        }catch (SQLException e){
                                ////System.out.println("Error " + e.getMessage()+ " Codigo "+ e.getErrorCode() );
                                sw=1;
                        }
                        if (sw==1){
                                if( model.proveedortiquetesService.existProveedorAnulado(nit,sucursal) ){
                                        model.proveedortiquetesService.activarProveedor(pt);
                                        next=next+"?mensaje=Tiquete Asignado..";                                        
                                }
                                else{
                                        next=next+"?mensaje=El Proveedor tiene Tiquete..";                                        
                                }
                        }
            }
            else{
                    next=next+"?mensaje=Proveedor No Existe...";
            }
            
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
    
}
