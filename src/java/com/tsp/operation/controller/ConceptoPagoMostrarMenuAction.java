/******************************************************************
 * Nombre ......................ConceptoPagoMostrarMenuAction.java
 * Descripci�n..................Conceptos de pago
 * Autor........................Armando Oviedo
 * Fecha........................Created on 10 de enero de 2006, 11:10 AM
 * Versi�n......................1.0
 * Coyright.....................Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;


import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

public class ConceptoPagoMostrarMenuAction extends Action{
    
    /** Creates a new instance of ConceptoPagoMostrarMenuAction */
    public ConceptoPagoMostrarMenuAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/jsp/cxpagar/Concepto_Pagos/EstructuraMenusCP.jsp";
        try{
            model.ConceptoPagosvc.reiniciar();
            model.ConceptoPagosvc.load();
            model.ConceptoPagosvc.mostrarContenido("0"); 
        }catch(Exception ex){
            ex.printStackTrace();
        }
        this.dispatchRequest(next);
    }
    
}
