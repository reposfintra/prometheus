/*
 * AcpmDeleteAction.java
 *
 * Created on 2 de diciembre de 2004, 11:30 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class ValidarClavePlacaPropAction extends Action{
    
    /** Creates a new instance of AcpmDeleteAction */
    public ValidarClavePlacaPropAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String next ="/jsp/hvida/identidad/identidad.jsp";
        
        String numsol =request.getParameter("numsol");
        String claveDes = request.getParameter("nclave");
        String clave= "NO SE PUDO GENERAR";
        String claveBuena ="NO";
        String mensaje = "CLAVE INVALIDA INTENTE NUEVAMENTE";
        
        String texto1="";
        String texto2="";
        int sw=0;
        if(request.getParameter("c_doc")!=null){
            sw=1;
            String nit =request.getParameter("c_doc");
            String nombre =request.getParameter("c_nom");
            texto1=nit;
            texto2=numsol+nombre;
            ////System.out.println("Texto 1: "+texto1);
            ////System.out.println("Texto 2: "+texto2);
            next ="/jsp/hvida/identidad/identidad.jsp?verificacion="+claveBuena+"&mensajeAut="+mensaje+"&clave=ok";
        }
        else if(request.getParameter("vprop")!=null){
            sw=2;
            String placa = request.getParameter("placa");
            String nitVProp =request.getParameter("vprop");
            String nitNProp =request.getParameter("propietario");
            texto1 = placa + nitVProp.substring(0,nitVProp.length()/2);
            texto2 = numsol+nitNProp + nitVProp.substring(nitVProp.length()/2,nitVProp.length());
            next ="/placas/placaUpdate.jsp?verificacion="+claveBuena+"&mensajeAut="+mensaje+"&clave=ok";
        }
        else{
            sw=3;
            String placa = request.getParameter("placa");
            String nitNProp =request.getParameter("propietario");
            texto1 = placa + nitNProp.substring(0,nitNProp.length()/2);
            texto2 = numsol+nitNProp + nitNProp.substring(nitNProp.length()/2,nitNProp.length());
            next ="/placas/placaInsert.jsp?verificacion="+claveBuena+"&mensajeAut="+mensaje+"&clave=ok";
        }
        
        try{
            clave= com.tsp.util.Util.getClave(texto1,texto2);
            ////System.out.println("La clave es :"+clave);
            
        }catch(UnsupportedEncodingException e){
            throw new ServletException(e.getMessage());
        }
        
        if(claveDes.equals(clave)){
            claveBuena= "SI";
            if(sw==1){
                mensaje = "CLAVE CORRECTA AHORA PUEDE INGRESAR EL NIT";
                next ="/jsp/hvida/identidad/identidad.jsp?verificacion="+claveBuena+"&mensajeAut="+mensaje;
            }
            else if(sw==2){
                mensaje = "CLAVE CORRECTA AHORA PUEDE MODIFICAR EL PROPIETARIO";
                next ="/placas/placaUpdate.jsp?verificacion="+claveBuena+"&mensajeAut="+mensaje;
            }
            else if(sw==3){
                mensaje = "CLAVE CORRECTA AHORA PUEDE INGRESAR LA PLACA";
                next ="/placas/placaInsert.jsp?verificacion="+claveBuena+"&mensajeAut="+mensaje;
            }
        }
        
        this.dispatchRequest(next);
        
    }
    
    
}
