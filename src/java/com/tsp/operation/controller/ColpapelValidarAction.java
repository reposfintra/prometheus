/*******************************************************************
 * Nombre clase: ColpapelValidarAction.java
 * Descripci�n: Accion para realizar despachos de CargaGeneral.
 * Autor: Ing. Karen Reales
 * Fecha: 31 Enero 2006
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class ColpapelValidarAction extends Action{
    static Logger logger = Logger.getLogger(ColpapelValidarAction.class);
    /** Creates a new instance of ColpapelValidarAction */
    public ColpapelValidarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        /**
         *
         * Usuario en Sesion
         */
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        
        /**
         *
         * Inicializacion de atributos para colores en la pagina
         */
        request.setAttribute("placa","filaresaltada");
        request.setAttribute("anticipo","filaresaltada");
        request.setAttribute("cedula","filaresaltada");
        request.setAttribute("fechasal","filaresaltada");
        request.setAttribute("toneladas","filaresaltada");
        request.setAttribute("nombre","No se encontro");
        request.setAttribute("nombreProp","No se encontro");
        request.setAttribute("utilidad","filaresaltada");
        request.setAttribute("trailer","filaresaltada");
        request.setAttribute("c1","filaresaltada");
        request.setAttribute("c2","filaresaltada");
        request.setAttribute("cp1","filaresaltada");
        request.setAttribute("cp2","filaresaltada");
        request.setAttribute("p1","filaresaltada");
        request.setAttribute("p2","filaresaltada");
        request.setAttribute("p3","filaresaltada");
        request.setAttribute("p4","filaresaltada");
        request.setAttribute("p5","filaresaltada");
        request.setAttribute("cfacturar","filaresaltada");
        request.setAttribute("sticker","filaresaltada");
        
        
        /**
         *
         * Inicializacion Variable tipo String
         */
        String next="/colpapel/DespachoValidado.jsp";
        String placa = request.getParameter("placa").toUpperCase();
        String trailer = request.getParameter("trailer").toUpperCase();
        String cedula= "";
        String cedprop ="No se encontro";
        String mensaje="";
        String advertencias="ADVERTENCIAS: ";
        String cf_code="";
        String unit_transp ="";
        String tipo_cont =request.getParameter("tipo_cont");
        String proveedor = "";
        /**
         *
         * Inicializacion Variable tipo Float
         */
        
        float costoRem=0;
        float valorPla =0;
        float cfacturar =0;
        float extraflete =0;
        float pesoreal= 0;
        float pesoMax=0;
        float valorU=0;
        float pmax=request.getParameter("pmax")!=null?Float.parseFloat(request.getParameter("pmax")):0;
        float anticipo=0;
        float unit_cost =0;
        String monedaCia="PES";
        /**
         *
         * Inicializacion Variable tipo int
         */
        int clasificacion = 5;
        int sw=0;
        
        /**
         *
         * Inicializacion Variable tipo boolean
         */
        boolean tractomula=false;
        boolean traile=false;
        boolean placaVetado=false;
        boolean conductorVetado=false;
        String uw = request.getParameter("uw")!=null? request.getParameter("uw"):"";
        String fecha =request.getParameter("fechadesp").length()>10? request.getParameter("fechadesp").substring(0,10):"2006-01-01";
        
        
        
        /**
         *
         * Parametros recogidos de la pagina
         */
        
        logger.info("ANTICIPOS");
        if(request.getParameter("anticipo")!=null){
            logger.info("ANTICIPO ES DISTINTO DE NULL");
            if(!request.getParameter("anticipo").equals("")){
                anticipo=Float.parseFloat(request.getParameter("anticipo"));
                logger.info("ANTICIPO ES DISTINTO DE VACIO Y EL VALOR ES: "+anticipo);
            }
        }
        
        /*java.util.Enumeration enum1;
        String parametro;
        enum1 = request.getParameterNames(); // Leemos todos los atributos del request
        int cant = 0;
        while (enum1.hasMoreElements()) {
            parametro = (String) enum1.nextElement();
            if(parametro.indexOf("anticipo")==0){
                if(!request.getParameter(parametro).equals("")){
                    anticipo = Float.parseFloat(request.getParameter(parametro))+anticipo;
                }
         
            }
        }*/
        
        
        /*
         *Si el anticipo viene de la pagina de modificar hay que sumar
         *todos los anticipos dados, por eso se busca una lista de estos.
         */
        
        Vector anticipos=model.movplaService.getLista();
        float totalAnt=0;
        if(anticipos!=null){
            anticipo = 0;
            logger.info("ESTA EN 0 EL VEC DE ANTICIPO PQ LA LISTA DE MOVPLA NO ES VACIA");
            for(int i=0; i<anticipos.size(); i++){
                Movpla movpla = (Movpla) anticipos.elementAt(i);
                String creacion  = movpla.getCreation_date();
                if(request.getParameter("anticipo"+creacion)!=null){
                    anticipo=Float.parseFloat(request.getParameter("anticipo"+creacion))+anticipo;
                }
                else{
                    anticipo= movpla.getVlr()+anticipo;
                }
            }
            
        }
        else{
            if(request.getParameter("planilla")!=null){
                try{
                    model.movplaService.buscarAnticipos(request.getParameter("planilla"));
                }catch(SQLException ex){
                    ex.printStackTrace();
                }
                anticipos=model.movplaService.getLista();
                for(int i=0; i<anticipos.size(); i++){
                    Movpla movpla = (Movpla) anticipos.elementAt(i);
                    totalAnt= movpla.getVlr()+totalAnt;
                    
                }
                model.movplaService.setLista(null);
            }
        }
        /*
         *Si se aplicaron anticipos de proveedores
         */
        Vector anticipoProv=model.anticiposService.getAnticiposProv();
        float antori=anticipo;
        if(anticipoProv!=null){
            anticipo = 0;
            logger.info("ESTA EN 0 EL VEC DE ANTICIPO PQ LA LISTA DE PROVEEDORES NO ES VACIA");
            for(int i=0; i<anticipoProv.size(); i++){
                Anticipos ant = (Anticipos) anticipoProv.elementAt(i);
                if(request.getParameter("planilla")!=null){
                    if(!ant.getAnticipo_code().equals("01")){
                        anticipo= ant.getValor()+anticipo;
                    }
                }
                else{
                    anticipo= ant.getValor()+anticipo;
                }
                
            }
            if(anticipo <=0){
                anticipo = antori;
            }
            else{
                anticipo=totalAnt+anticipo;
            }
        }
        if(!request.getParameter("toneladas").equals(""))
            pesoreal= Float.parseFloat(request.getParameter("toneladas"));
        
        if(request.getParameter("extraflete")!=null){
            extraflete = Float.parseFloat(request.getParameter("extraflete"));
        }
        
        if(request.getParameter("cfacturar")!=null){
            if(!request.getParameter("cfacturar").equals(""))
                cfacturar = Float.parseFloat(request.getParameter("cfacturar"));
        }
        
        if(request.getParameter("vlrpla")!=null){
            if(!request.getParameter("vlrpla").equals(""))
                valorPla = Float.parseFloat(request.getParameter("vlrpla"));
        }
        
        
        try{
            //MAXIMOS VALORES EN TABLAGEN
            double maximo_valor =0;
            model.tablaGenService.obtenerRegistro("MAXCANSJ",request.getParameter("standard"), "");
            if(model.tablaGenService.getTblgen()!=null){
                try{
                    maximo_valor =Double.parseDouble(model.tablaGenService.getTblgen().getReferencia());
                    System.out.println("Maximo valor "+maximo_valor);
                }catch(NumberFormatException ne){
                    System.out.println("No es un numero "+model.tablaGenService.getTblgen().getReferencia());
                    maximo_valor = 0;
                }
            }
            
            /**
             *
             * Se buscan los valores (costos) del standard job
             */
            model.stdjobcostoService.buscarStandardJobCostoFull(request.getParameter("standard"),request.getParameter("valorpla"));
            if(model.stdjobcostoService.getStandardCosto()!=null){
                
                Stdjobcosto stdjobcosto = model.stdjobcostoService.getStandardCosto();
                unit_transp = stdjobcosto.getUnit_transp();
                pesoMax=stdjobcosto.getPeso_lleno_maximo();
                unit_cost = stdjobcosto.getUnit_cost();
                
                /* Si el valor no es del standard sino uno escrito por el despachador */
                if(!request.getParameter("otro").equals("NO")&&!request.getParameter("otro").equals("")){
                    unit_cost = Float.parseFloat(request.getParameter("otro"));
                }
                
                valorU=unit_cost; /* Valor unitario */
                valorPla =unit_cost*pesoreal;/* Valor de la planilla */
                pmax=stdjobcosto.getporcentaje_maximo_anticipo();/* Porcentaje maximo de anticipo */
                cf_code=stdjobcosto.getCf_code();
                
                if(!stdjobcosto.getCurrency().equalsIgnoreCase(monedaCia)){
                    try{
                        model.tasaService.buscarValorTasa(monedaCia,stdjobcosto.getCurrency(),monedaCia,fecha);
                    }catch(Exception et){
                        throw new ServletException(et.getMessage());
                    }
                    Tasa tasa = model.tasaService.obtenerTasa();
                    if(tasa==null){
                        request.setAttribute("mensaje","Error no existe tasa de cambio");
                        advertencias = advertencias+" No se encontro tasa de cambio para la moneda "+stdjobcosto.getCurrency();
                        request.setAttribute("mensaje",advertencias);
                        sw=1;
                        request.setAttribute("toneladas","filaroja");
                    }
                }
            }
            else{
                valorU=request.getParameter("vlrpla")!=null?Float.parseFloat(request.getParameter("vlrpla")):1; /* Valor unitario */
                unit_transp = ( request.getParameter("unit_transp")!=null )?request.getParameter("unit_transp"):"";
            }
            
            if(model.stdjobdetselService.getStandardDetSel()!=null){
                logger.info("Standard Job Det Set encontrado");
                Stdjobdetsel stdjobdetsel = model.stdjobdetselService.getStandardDetSel();
                String moneda=stdjobdetsel.getCurrency();
                //se busca la tasa para la remesa
                if(!moneda.equals(monedaCia)){
                    
                    try{
                        model.tasaService.buscarValorTasa(monedaCia,moneda,monedaCia,fecha);
                    }catch(Exception et){
                        throw new ServletException(et.getMessage());
                    }
                    Tasa tasa = model.tasaService.obtenerTasa();
                    if(tasa!=null){
                        
                        double vtasa = tasa.getValor_tasa();
                        double vlrrem2= com.tsp.util.Util.redondear(vtasa*costoRem,0);
                        logger.info("Moneda cia "+monedaCia);
                        logger.info("Moneda standard "+moneda);
                        logger.info("Valor a cambiar "+costoRem);
                        logger.info("Valor rem en pesos "+vlrrem2);
                        
                        
                    }else{
                        logger.info("NO EXISTE TASA ");
                        advertencias = advertencias+" No se encontro tasa de cambio para la moneda "+moneda;
                        request.setAttribute("mensaje",advertencias);
                        sw=1;
                        request.setAttribute("cfacturar","filaroja");
                        
                    }
                    
                    
                }
            }
            uw=uw.toUpperCase();
            
            double maxton=maximo_valor==0?50:maximo_valor;
            double maxmt3=maximo_valor==0?100:maximo_valor;
            double maxkl=maximo_valor==0?55000:maximo_valor;
            double maxgl=maximo_valor==0?500000:maximo_valor;
            double maxvi=maximo_valor==0?1:maximo_valor;
            
            if(uw.indexOf("T")==0){//toneladas
                if(cfacturar>maxton){
                    sw=1;
                    request.setAttribute("cfacturar","filaroja");
                }
            }
            if(uw.indexOf("M")==0){//MT3
                if(cfacturar>maxmt3){
                    sw=1;
                    request.setAttribute("cfacturar","filaroja");
                }
            }
            if(uw.indexOf("L")==0){//KILO
                if(cfacturar>maxkl){
                    sw=1;
                    request.setAttribute("cfacturar","filaroja");
                }
            }
            if(uw.indexOf("G")==0){//GALONES
                if(cfacturar>maxgl){
                    sw=1;
                    request.setAttribute("cfacturar","filaroja");
                }
            }
            if(uw.indexOf("V")==0 || uw.indexOf("W")==0){//VIAJES
                if(cfacturar>maxvi){
                    sw=1;
                    request.setAttribute("cfacturar","filaroja");
                }
            }
           /*
            *Se buscan los datos de la placa
            */
            if(request.getParameter("modif")==null){
                // model.tablaGenService.obtenerRegistro("SJENTREGA",request.getParameter("standard"), "");
                //if(model.tablaGenService.getTblgen()==null){
                if(!model.rmtService.tieneEntrega(placa)){
                    //sw = 1;
                    //request.setAttribute("placa","filaroja");
                    advertencias = advertencias+" La placa esta en trafico actualmente.";
                    request.setAttribute("mensaje",advertencias );
                }
                if(!model.cumplidoService.tieneCumplido(placa)){
                    advertencias = advertencias+" La ultima planilla de la placa no ha sido cumplida.";
                    request.setAttribute("mensaje",advertencias );
                }
                //}
            }
            model.placaService.buscaPlaca(placa.toUpperCase());
            if(model.placaService.getPlaca()==null){
                /*Si la placa no existe se envia un error.*/
                sw=1;
                request.setAttribute("placa","filaroja");
                logger.info("ERROR EN LA PLACA....");
            }
            else{
                if(model.placaService.getPlaca()!=null){
                    Placa plac = model.placaService.getPlaca();
                    cedprop = plac.getPropietario();
                    placaVetado=plac.isVetado();
                    proveedor=plac.getProveedor();
                    cedula = request.getParameter("conductor")==null?plac.getConductor():request.getParameter("conductor").equals("")?plac.getConductor():request.getParameter("conductor");
                    request.setAttribute("nombreProp",plac.getNombre());
                    tractomula = plac.isTractomula();
                    /*
                     * Si la clasificacion de la placa es diferente a vacio
                     * se obtiene y se guarda en una variable para luego compararla
                     */
                    if(!plac.getClasificacion().equals(""))
                        clasificacion= Integer.parseInt(plac.getClasificacion());
                    
                    if(cedprop.equals("")){
                        
                        /*Si la cedula del propietario esta en blanco se envia un error.*/
                        sw=1;
                        advertencias = advertencias+" ERROR: La placa no tiene propietario.";
                        request.setAttribute("placa","filaroja");
                        request.setAttribute("mensaje",advertencias);
                    }
                    model.proveedorService.obtenerProveedor(cedprop, usuario.getDstrct());
                    if(model.proveedorService.getProveedor()==null){
                        sw=1;
                        advertencias =advertencias+ " ERROR: El propietario de la placa no esta registrado en la tabla de proveedores.";
                        request.setAttribute("placa","filaroja");
                        request.setAttribute("mensaje",advertencias);
                    }
                    /*if(proveedor.equals("")){
                        Si el codigo de proveedor esta en blanco se envia un error.
                        sw=1;
                        advertencias =advertencias+ " ERROR: El propietario de la placa no tiene codigo de proveedor";
                        request.setAttribute("placa","filaroja");
                        request.setAttribute("mensaje",advertencias);
                    }*/
                    if(cedula.equals("")){
                        /*Si la cedula del conductor esta en blanco se envia un error.*/
                        sw=1;
                        request.setAttribute("cedula","filaroja");
                    }
                    if(!"".equals(plac.getGrupoid())){
                        if(!model.placaService.isGrupoDespacho("000"+request.getParameter("standard").substring(0,3),plac.getGrupoid())){
                            advertencias =advertencias+ " ERROR: La placa no puede ser despachada para el cliente "+"000"+request.getParameter("standard").substring(0,3);
                            request.setAttribute("placa","filaroja");
                            request.setAttribute("mensaje",advertencias);
                            sw=1;
                        }
                    }
                    Date d = plac.getVencSOAT();
                    Date hoy = new Date();
                    
                    /*
                     * Se compara la fecha del vencimiento del SOAT
                     * con la de hoy para verificar que no este vencido.
                     */
                    
                    if(d.toString().equals("0099-01-01")){
                        /*Si la fecha es igual a la default se envia mensaje de advertencia para que se actualice.*/
                        advertencias=advertencias+" ADVERTENCIA: no se ha ingresado la fecha de vencimiento del seguro obligatorio de la placa!";
                    }else{
                        /*Si la fecha es es menor que hoy se envia mensaje de advertencia porque esta vencido el SOAT.*/
                        if(hoy.after(d)){
                            advertencias=advertencias+" ADVERTENCIA: el seguro obligatorio de la placa esta vencido!";
                        }
                    }
                    if(!plac.isTieneFoto()){
                        advertencias =advertencias+ " La placa no tiene foto registrada en la base de datos";
                    }
                    
                }
                /*
                 *
                 *Se buscan los datos del Trailer
                 */
                if(tractomula){
                    /*Si la placa es tractomula se verifica que se halla ingresado el trailer*/
                    
                    if(!trailer.equalsIgnoreCase("NA")){
                        
                        /*Se buscan los datos del trailer*/
                        
                        model.placaService.buscaPlaca(trailer);
                        
                        if(model.placaService.getPlaca()!=null){
                            int swTra = 0;
                            Placa vehiculo = model.placaService.getPlaca();
                            
                            if(!vehiculo.getPropietario().equals("890103161")){
                                /*
                                 * El switch se coloca en 1 si el nit de propietario
                                 * del trailer es de Sanchez Polo
                                 */
                                swTra=1;
                            }
                            /*Se verifica que la placa del trailer tenga como tipo de recurso trailer*/
                            if(!vehiculo.isTraile()){
                                /*Si no es asi se envia un mensaje de error.*/
                                sw=1;
                                advertencias= " ERROR: La placa que usted ha escrito como trailer no es un trailer.!";
                                request.setAttribute("mensaje", advertencias);
                                request.setAttribute("trailer","filaroja");
                            }else{
                                /*Se verifica que tipo selecciono la persona.*/
                                if(request.getParameter("tipo_tra").equals("FINV")){
                                    /*Si el usuario selecciono TSP y el swicth es 1 se muestra error.*/
                                    if(swTra==1){
                                        logger.info("EL TRAILER NO ES DE TSP");
                                        advertencias=" ERROR: El trailer no es de TSP.!";
                                        request.setAttribute("mensaje", advertencias);
                                        sw=1;
                                        request.setAttribute("trailer","filaroja");
                                    }
                                    
                                }else{
                                    /*Si el usuario selecciono TSP y el swicth es 0 se muestra error.*/
                                    if(swTra==0){
                                        sw=1;
                                        request.setAttribute("trailer","filaroja");
                                        advertencias="ADVERTENCIA: El trailer es de TSP usted marco que no. Marque correctamente.!";
                                        request.setAttribute("mensaje", advertencias);
                                    }
                                    
                                }
                                /*Se busca el fitmen en la tabla de fitmen.*/
                                model.fitmenService.buscarFitmen(request.getParameter("trailer").toUpperCase());
                            }
                        }else{
                            /*Si la placa del trailer no existe ne la base de datos se muestra error.*/
                            sw=1;
                            request.setAttribute("trailer","filaroja");
                        }
                    }else{
                        /*Se verifica si el usuario escribio NA en el trailer y el despacho es vacio.*/
                        if(cf_code.indexOf("V")<0){
                            /*Si no es vacio se muestra error porque el trailer es obligatorio*/
                            sw=1;
                            request.setAttribute("trailer","filaroja");
                        }
                        else{
                            advertencias =advertencias+" No ingreso placa de Trailer!";
                        }
                    }
                }
                else{
                    trailer = "NA";
                }
                /*
                 * Datos del Contenedor
                 */
                if("FINV".equals(tipo_cont)){
                    /*Si seleccionaron que los contenedores son de TSP*/
                    if(!request.getParameter("c1").equals("")){
                        /*Se verifica que la placa del contenedor 1 existe en la base de datos*/
                        if(!model.placaService.placaExist(request.getParameter("c1"))){
                            /*Si no es asi se envia error*/
                            sw=1;
                            request.setAttribute("c1","filaroja");
                            logger.info("ERROR EN EL DE CONTENEDOR....");
                        }
                        /*Se verifica que la placa del contenedor 2 existe en la base de datos*/
                        if(!model.placaService.placaExist(request.getParameter("c2"))){
                            sw=1;
                            request.setAttribute("c2","filaroja");
                            logger.info("ERROR EN EL CONTENEDOR....");
                        }
                        
                    }
                }
                
            }
            /**
             *
             * Se buscan los datos del standard job
             */
            model.stdjobdetselService.buscaStandard(request.getParameter("standard"));
            if(model.stdjobdetselService.getStandardDetSel()!=null){
                logger.info("Standard Job Det Set encontrado");
                Stdjobdetsel stdjobdetsel = model.stdjobdetselService.getStandardDetSel();
                costoRem=stdjobdetsel.getVlr_freight()*cfacturar;
                logger.info("Cuenta de ingreso "+stdjobdetsel.getAccount_code_i());
                logger.info("Cuenta de costo "+stdjobdetsel.getAccount_code_c());
                /*
                 * Si la clasificacion de la carga exigida por el standard job es mayor
                 * a la clasificacion actual de la placa debe enviarse un mensaje de advertencia
                 */
                if(clasificacion <= stdjobdetsel.getClasificacion()){
                    advertencias= advertencias+ " La placa no clasifica para transportar este tipo de carga!";
                    //  request.setAttribute("mensaje","Advertencia: La placa no clasifica para transportar este tipo de carga!");
                }
                
            }
            model.conductorService.buscaConductor(cedula);
            if(model.conductorService.getConductor()!=null){
                if(model.conductorService.getConductor()!=null){
                    Conductor conductor = model.conductorService.getConductor();
                    conductorVetado = conductor.isVetado();
                    request.setAttribute("nombre",conductor.getNombre());
                    Date d = conductor.getVecPase();
                    Date hoy = new Date();
                    if("0099-01-01".equals(d.toString())){
                        if(advertencias.equals("")){
                            advertencias=advertencias+ " ADVERTENCIA: no se ha ingresado la fecha de vencimiento del pase de este conductor!";
                        }else{
                            advertencias+=" y no se ha ingresado la fecha de vencimiento del pase de este conductor!";
                        }
                    }else{
                        if(hoy.after(d)){
                            if(advertencias.equals("")){
                                advertencias=advertencias+" ADVERTENCIA: la licencia del conductor esta vencida!";
                            }else{
                                advertencias+=" y la licencia del conductor esta vencida!";
                            }
                        }
                    }
                    if(!conductor.isTieneFoto()){
                        advertencias+=" No ha sido ingresada la foto del conductor!";
                    }
                }
                
            }
            else{
                sw=1;
                logger.info("ERROR EN EL CONDUCTOR....");
                request.setAttribute("cedula","filaroja");
                
            }
            logger.info("Valor planilla "+valorPla);
            float putilidad = ((costoRem - valorPla)/costoRem)*100;
            logger.info("Utilidad "+putilidad );
            
            //ESTE PORCENTAJE HAY Q SACARLO DE LA TABLA...CLIENTE
           /* if(putilidad<5 || putilidad>50){
                logger.info("ERROR EN LA UTILIDAD....");
                sw=1;
                request.setAttribute("utilidad","filaroja");
                mensaje="Verifique todos los datos de anticipo, flete, cantidad a facturar ya que la utilida no se encuentra entre en el porcentaje especificado";
                request.setAttribute("mensaje",mensaje);
            }*/
            
            //VALIDAMOS PRECINTOS
            
            if(request.getParameter("c1precinto")!=null && !request.getParameter("c1precinto").equals("")){
                boolean esta =false;
                if(request.getParameter("planilla")!=null){
                    esta=model.precintosSvc.existe(usuario.getId_agencia(),request.getParameter("c1precinto"),"Precinto",request.getParameter("planilla"));
                }
                else{
                    esta=model.precintosSvc.existe(usuario.getId_agencia(),request.getParameter("c1precinto"),"Precinto");
                }
                if(!esta){
                    request.setAttribute("cp1","filaroja");
                    sw=1;
                }
                
            }
            if(request.getParameter("c2precinto")!=null && !request.getParameter("c2precinto").equals("")){
                boolean esta =false;
                if(request.getParameter("planilla")!=null){
                    esta=model.precintosSvc.existe(usuario.getId_agencia(),request.getParameter("c2precinto"),"Precinto",request.getParameter("planilla"));
                }
                else{
                    esta=model.precintosSvc.existe(usuario.getId_agencia(),request.getParameter("c2precinto"),"Precinto");
                }
                if(!esta){
                    request.setAttribute("cp2","filaroja");
                    sw=1;
                }
            }
            java.util.Enumeration enum1;
            String parametro;
            enum1 = request.getParameterNames();
            while (enum1.hasMoreElements()) {
                parametro = (String) enum1.nextElement();
                if(parametro.indexOf("precintos")>=0 && !request.getParameter(parametro).equals("")){
                    String nombre  = parametro;
                    String valor = request.getParameter(parametro);
                    java.util.Enumeration enum2;
                    enum2 = request.getParameterNames();
                    while (enum1.hasMoreElements()) {
                        parametro = (String) enum1.nextElement();
                        if(parametro.indexOf("precintos")>=0 && !request.getParameter(parametro).equals("")
                        && !nombre.equals(parametro) && request.getParameter(parametro).equals(valor)){
                            String numero = parametro.substring(9);
                            String numero1 = nombre.substring(9);
                            request.setAttribute("p"+numero,"filaroja");
                            request.setAttribute("p"+numero1,"filaroja");
                            sw=1;
                        }
                    }
                    
                    
                }
            }
            if(request.getParameter("c2precinto")!=null && request.getParameter("c1precinto")!=null){
                if(!request.getParameter("c2precinto").equals("") || !request.getParameter("c1precinto").equals("")){
                    if(request.getParameter("c2precinto").equals(request.getParameter("c1precinto"))){
                        sw=1;
                        request.setAttribute("cp2","filaroja");
                        request.setAttribute("cp1","filaroja");
                    }
                }
            }
            if(request.getParameter("precintos")!=null && !request.getParameter("precintos").equals("")){
                boolean esta =false;
                if(request.getParameter("planilla")!=null){
                    esta=model.precintosSvc.existe(usuario.getId_agencia(),request.getParameter("precintos"),"Precinto",request.getParameter("planilla"));
                }
                else{
                    esta=model.precintosSvc.existe(usuario.getId_agencia(),request.getParameter("precintos"),"Precinto");
                }
                if(!esta){
                    request.setAttribute("p1","filaroja");
                    sw=1;
                }
            }
            int k =2;
            while(k<=5){
                if(request.getParameter("precintos"+k)!=null){
                    if(!request.getParameter("precintos"+k).equals("")){
                        boolean esta =false;
                        if(request.getParameter("planilla")!=null){
                            esta=model.precintosSvc.existe(usuario.getId_agencia(),request.getParameter("precintos"+k),"Precinto",request.getParameter("planilla"));
                        }
                        else{
                            esta=model.precintosSvc.existe(usuario.getId_agencia(),request.getParameter("precintos"+k),"Precinto");
                        }
                        if(!esta){  request.setAttribute("p"+k,"filaroja");
                        sw=1;
                        }
                        
                    }
                }
                k++;
            }
            
            if(request.getParameter("sticker")!=null && !request.getParameter("sticker").equals("")){
                String sticker=request.getParameter("sticker");
                if(request.getParameter("planilla")!=null){
                    if(!model.precintosSvc.existe(usuario.getId_agencia(),sticker,"Sticker",request.getParameter("planilla"))){
                        request.setAttribute("sticker","filaroja");
                        sw=1;
                    }
                }else{
                    if(!model.precintosSvc.existe(usuario.getId_agencia(),sticker,"Sticker")){
                        request.setAttribute("sticker","filaroja");
                        sw=1;
                    }
                }
            }
            
            //VERIFICO SI EL CONDUCTOR ESTA VETADO
            if(conductorVetado){
                sw=1;
                request.setAttribute("cedula","filaroja");
                request.setAttribute("mensaje","El conductor esta reportado en el sistema!");
            }
            
            //VERIFICO SI LA PLACA ESTA VETADO
            if(placaVetado){
                sw=1;
                request.setAttribute("placa","filaroja");
                request.setAttribute("mensaje","El propietario o la placa esta reportada en el sistema!");
            }
            
            //
            if(request.getParameter("modif")==null){
                
                /*if(unit_transp.equals("VIA")&&pesoreal>maximo_valor){
                    sw =1;
                    logger.info("Es mayor a un viaje");
                    request.setAttribute("toneladas","filaroja");
                }
                else if(!unit_transp.equals("VIA")&&pesoreal<maximo_valor){
                    logger.info("Es menor  a 10 Toneladas");
                    sw =1;
                    request.setAttribute("toneladas","filaroja");
                }*/
                
                
                if(unit_transp.indexOf("V")==0 || unit_transp.indexOf("W")==0){//viajes
                    if(pesoreal>maxvi){
                        sw=1;
                        request.setAttribute("toneladas","filaroja");
                    }
                    
                }
                if(unit_transp.indexOf("T")==0){//toneladas
                    if(pesoreal>maxton){
                        sw=1;
                        request.setAttribute("toneladas","filaroja");
                    }
                }
                if(unit_transp.indexOf("M")==0){//MT3
                    if(pesoreal>maxmt3){
                        sw=1;
                        request.setAttribute("toneladas","filaroja");
                    }
                }
                if(unit_transp.indexOf("L")==0){//KILO
                    if(pesoreal>maxkl){
                        sw=1;
                        request.setAttribute("toneladas","filaroja");
                    }
                }
                if(unit_transp.indexOf("G")==0){//GALONES
                    if(pesoreal>maxgl){
                        sw=1;
                        request.setAttribute("toneladas","filaroja");
                    }
                }
                
                
               /* if(pesoreal>pesoMax){
                    logger.info("error en peso");
                    sw=1;
                    request.setAttribute("toneladas","filaroja");
                }*/
            }
            
            
            /*Validacion Unidadades*/
                /*uw            - Unidad  Remesa
                  cfacturar     - Cantidad Remesa
                  unit_transp   - Unidad Planilla
                  pesoreal      - Cantidad Planilla
                 */
            
            TablaGen unidad = model.tablaGenService.obtenerInformacionDato("EQUIUNI", unit_transp);
            if( unidad!=null ){
                if( unidad.getReferencia().equals(uw)){
                    if( pesoreal > cfacturar ){//La cantidad de la Planilla excede a la cantidad de la remesa
                        sw=1;
                        request.setAttribute("toneladas","filaroja");
                        request.setAttribute("cfacturar","filaroja");
                    }
                }//Unidades Iguales
            }
            
            request.setAttribute("cliente","filaresaltada");
            if(request.getParameter("cliente")!=null && request.getParameter("modif")!=null){
                System.out.println("CLiente "+request.getParameter("cliente"));
                model.clienteService.informacionCliente(request.getParameter("cliente"));
                if(model.clienteService.getCliente()==null){
                    request.setAttribute("cliente","filaroja");
                    sw=1;
                }
                else{
                    System.out.println("SI ESTA EL CLiente "+request.getParameter("cliente"));
                }
            }
            
            
            /*
             * Se valida si el valor de la planilla es 0
             */
            
            double vanticipo = (pesoreal*valorU)+extraflete;
            logger.info("Valor de anticipo total "+vanticipo);
            logger.info("Valor de anticipo entregado "+anticipo);
            vanticipo = com.tsp.util.Util.redondear( vanticipo * (pmax/100),0);
            if(vanticipo<anticipo){
                sw=1;
                request.setAttribute("anticipo","filaroja");
            }
            logger.info("Max anticipo "+vanticipo);
            logger.info("Peso Max "+pesoMax);
            if(valorU<=0){
                sw=1;
                request.setAttribute("toneladas","filaroja");
            }
            
            request.setAttribute("ad",advertencias);
            
            next="/colpapel/DespachoValidado.jsp?tipo_cont="+tipo_cont+"&trailer="+trailer+"&conductor="+cedula;
            
            if(request.getParameter("modif")!=null)
                next="/colpapel/modificarDespachoValido.jsp?tipo_cont="+tipo_cont+"&trailer="+trailer;
            
            if(request.getParameter("colpapel")!=null)
                next="/colpapel/agregarPlanillaValido.jsp?tipo_cont="+tipo_cont+"&trailer="+trailer+"&conductor="+cedula;
            
            if(sw==1){
                logger.info("Encontre un error ");
                next="/colpapel/DespachoError.jsp?tipo_cont="+tipo_cont+"&conductor="+cedula;
                
                logger.info("Parametro modif" +request.getParameter("modif"));
                
                if(request.getParameter("modif")!=null)
                    next="/colpapel/modificarDespachoError.jsp?tipo_cont="+tipo_cont+"&trailer="+trailer;
                
                if(request.getParameter("colpapel")!=null)
                    next="/colpapel/agregarPlanillaError.jsp?tipo_cont="+tipo_cont+"&conductor="+cedula;
            }
            logger.info("El next= "+next);
            
            
        }catch (SQLException e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
/***************************************************
 * Entregado a karen 12 Feb 2007
 ***************************************************/
