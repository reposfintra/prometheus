/*
 * ReporteCumplidoMigracion.java
 *
 * Created on 24 de octubre de 2005, 12:22 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.threads.*;

/**
 *
 * @author  Jose
 */
public class Reporte_cumplidoMigracionAction extends Action{
    
    /** Creates a new instance of ReporteCumplidoMigracion */
    public Reporte_cumplidoMigracionAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="";
        HttpSession session = request.getSession();
        String fecha = (request.getParameter("c_fecha")!=null)?request.getParameter("c_fecha"):"";
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = (String) session.getAttribute("Distrito");
        try{
            fecha = fecha.substring(0,10);
            ReporteMigracionCumplidos hilo = new ReporteMigracionCumplidos();
            hilo.start(usuario.getLogin(),fecha);
            next = "/jsp/cumplidos/cumplido/ReporteCumplidos.jsp?msg=Su migración ha iniciado y se encuentra en el log de procesos";
            //next = Util.LLamarVentana(next, "Migración Cumplidos");
        }catch (Exception ex){
            throw new ServletException("Error en OpcionesExportarPtoAction .....\n"+ex.getMessage());
        }

        this.dispatchRequest(next);      
    }
    
}
