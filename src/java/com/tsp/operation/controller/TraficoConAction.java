/********************************************************************
 *      Nombre Clase.................   TraficoClasificacionAction.java
 *      Descripci�n..................   Action para Clasificar(Ordenar) los diferentes campos de control trafico
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.11.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  David A
 */
public class TraficoConAction extends Action{
    
    /** Creates a new instance of TraficoConAction */
    public TraficoConAction() {
    }
    static Logger logger = Logger.getLogger(TraficoConAction.class);

    public void run() throws ServletException, InformationException {
        logger.info("FILTRRRRRRRRRRRRRRROOOOOOOOOSSSSSSSS GUARDADOS");
         try{
             
            String user  =""+ request.getParameter ("usuario");
            String linea =""+ request.getParameter ("linea");
            String conf  =""+request.getParameter ("conf");
            String clas  =""+request.getParameter ("clas");
            String oid   =""+request.getParameter ("consultas");
            String zonasUsuario = request.getParameter("zonasUsuario");

            Vector f =new Vector();
            model.traficoService.generarFiltrosC(oid);
            f=model.traficoService.obtVecFiltrosU();
            String filtros="";
            String consulta="";
            for (int i=0;i<f.size();i++){
                Vector fila = (Vector)f.elementAt(i);
                linea = ""+fila.elementAt(4);
                consulta=""+fila.elementAt(2);
                filtros = ""+fila.elementAt(5);
                logger.info("LINEA OBTENIDOS DE: " + oid + " : " + linea);
                logger.info("FILTROS OBTENIDOS DE: " + oid + " : " + filtros);
            }
            
            
            /*if(linea.equals("null")||linea.equals(""))
                linea=model.traficoService.obtlinea();*/
            
            logger.info("Consulta Final ="+consulta);
            logger.info("Linea Final ="+linea);
            Vector colum= new Vector(); 
            colum = model.traficoService.obtenerCampos(linea);
            
            logger.info("Consulta final:"+consulta);
            model.traficoService.generarTabla(consulta, colum);
          
            String next = "/jsp/trafico/controltrafico/vertrafico2.jsp?conf=&linea="+linea+"&reload=&filtro="+filtros+"&zonasUsuario="+zonasUsuario;
            logger.info("next en Filtro"+next);  
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null){
                throw new Exception("No se pudo encontrar "+ next);
            }    
            rd.forward(request, response); 
        }
        catch(Exception e){
          throw new ServletException("Accion:"+ e.getMessage());
        } 
    }
}
