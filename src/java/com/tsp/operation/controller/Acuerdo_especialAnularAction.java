/*******************************************************************
 * Nombre clase: Acuerdo_especialAnularAction.java
 * Descripci�n: Accion para anular un acuerdo especial a la bd.
 * Autor: Ing. Jose de la rosa
 * Fecha: 6 de diciembre de 2005, 02:46 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class Acuerdo_especialAnularAction extends Action{
    
    /** Creates a new instance of Acuerdo_especialAnularAction */
    public Acuerdo_especialAnularAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next="/jsp/trafico/mensaje/MsgAnulado.jsp";
        HttpSession session = request.getSession();        
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = (String)(session.getAttribute ("Distrito"));
        String standar = request.getParameter("c_standar").toUpperCase ();
        String codigo = request.getParameter("c_codigo").toUpperCase ();
        String tip = request.getParameter("tipo");
        try{
            Acuerdo_especial acuerdo = new Acuerdo_especial();
            acuerdo.setStandar (standar);
            acuerdo.setCodigo_concepto (codigo);
            acuerdo.setDistrito (distrito);
            acuerdo.setTipo(tip);
            acuerdo.setUsuario_modificacion(usuario.getLogin().toUpperCase());
            model.acuerdo_especialService.anularAcuerdo_especial (acuerdo);
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);        
    }
    
}
