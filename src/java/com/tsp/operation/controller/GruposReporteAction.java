/*
 * Nombre        GruposReporteAction.java
 * Descripción
 * Autor         Alejandro Payares
 * Fecha         10 de mayo de 2006, 02:05 PM
 * Version       1.0
 * Coyright      Transportes Sanchez Polo SA.
 */

package com.tsp.operation.controller;

import javax.servlet.*;
import com.tsp.operation.model.beans.GruposReporte;
import com.tsp.operation.model.beans.Usuario;

/**
 *
 * @author Alejandro Payares
 */
public class GruposReporteAction extends Action{
    
    /**
     * Crea una nueva instancia de GruposReporteAction
     * @autor  Alejandro Payares
     */
    public GruposReporteAction() {
    }
    
    public void run() throws ServletException {
        String cmd = request.getParameter("cmd");
        String next = "";
        //System.out.println("cmd = "+cmd);
        try {
            Usuario user = (Usuario)request.getSession().getAttribute("Usuario");
            String dstrct = (String)request.getSession().getAttribute("Distrito");
            if ( "list".equals(cmd) ) {
                model.gruposReporteService.buscarReportes();
                next = "/jsp/sot/body/PantallaListaReportes.jsp";
            }
            else if ( "load".equals(cmd) ) {
                String codigoReporte = request.getParameter("codrpt");
                String nombreReporte = request.getParameter("nombreReporte");
                request.setAttribute("gr",model.gruposReporteService.cargarGrupos(codigoReporte));
                next = "/jsp/sot/body/PantallaAdminGruposReporte.jsp?codigoReporte="+codigoReporte+"&nombreReporte="+nombreReporte;
            }
            else if ( "save".equals(cmd) ) {
                String strGrupos = request.getParameter("grupos").replaceAll(",_",",#");
                //System.out.println("strGrupos = "+strGrupos);
                String [] grupos = strGrupos.split(";");
                GruposReporte gr = new GruposReporte();
                gr.setCodigoReporte(request.getParameter("codigoReporte"));
                for( int i=0; i< grupos.length; i++ ){
                    //System.out.println("grupo "+(i+1)+" = "+grupos[i]);
                    String [] vec = grupos[i].split(":");
                    String datosGrupo = vec[0];
                    String strCampos = vec[1];
                    vec = datosGrupo.split(",");
                    gr.agregarGrupo( vec[0], vec[1] );
                    //System.out.println("datosGrupo = "+datosGrupo);
                    //System.out.println("campos = "+strCampos);
                    String [] campos = strCampos.split("-");
                    for( int j=0; j< campos.length; j++ ){
                        gr.agregarCampoAGrupo(vec[0], campos[j],"");
                        //System.out.println("campo "+(j+1)+" = "+campos[j]);
                    }
                }
                model.gruposReporteService.guardarGrupos(gr);
                //System.out.println("listo!");
                next = "/jsp/sot/body/PantallaListaReportes.jsp";
            }
            else if ( "loadCamposGral".equals(cmd) ) {
                String codReporte = request.getParameter("codrpt");
                String nombreReporte = request.getParameter("nombreReporte");
                model.consultasGrlesReportesService.buscarDetalleDeCampos(codReporte);
                model.gruposReporteService.buscarGrupos(codReporte);
                next = "/jsp/sot/body/PantallaCamposGeneralesReporte.jsp?codReporte="+codReporte+"&nombreReporte="+nombreReporte;
            }
            else if ( "agregarCampo".equals(cmd) ) {
                String codigorpt = request.getParameter("codrpt");
                String nomcampo = request.getParameter("nombre");
                String titulo = request.getParameter("titulo");
                String aux[] = request.getParameter("grupos").split(",");
                String grupo = aux[0];
                String colorGrupo = aux[1];
                
                String creation_user = user.getLogin();
                String base = user.getBase();
                model.gruposReporteService.agregarCampo(dstrct, codigorpt, nomcampo, titulo, grupo, colorGrupo, creation_user, base);
                next = "/jsp/sot/body/PantallaAgregarCampo.jsp?mensaje=El campo fue agregado correctamente&codReporte="+codigorpt;
            }
            else if ( "modificarCampo".equals(cmd) ) {
                String codigorpt = request.getParameter("codrpt");
                String nomcampo = request.getParameter("nombre");
                String nombreAntiguo = request.getParameter("nombreAntiguo");
                String titulocampo = request.getParameter("titulo");
                int orden = Integer.parseInt(request.getParameter("orden"));
                String aux[] = request.getParameter("grupos").split(",");
                String grupo = aux[0];
                String colorGrupo = aux[1];
                String user_update = user.getLogin();
                                                    
                model.gruposReporteService.editarCampo(dstrct, orden, orden, codigorpt, nomcampo, nombreAntiguo, titulocampo, grupo, colorGrupo, user_update);
                next = "/jsp/sot/body/PantallaAgregarCampo.jsp?mensaje=El campo '"+titulocampo+"' fue editado correctamente" +
                        "&titulo=Editar Campo"+
                        "&codReporte="+codigorpt+
                        "&nombre="+nomcampo+
                        "&tituloCampo="+titulocampo+
                        "&orden="+orden+
                        "&grupo="+grupo;
            }
            else if ( "deleteFields".equals(cmd) ) {
                String user_update = user.getLogin();
                String codReporte = request.getParameter("reporte");
                String [] campos = request.getParameterValues("fields");
                String [] ordenes = new String[campos.length];
                for( int i=0; i< campos.length; i++ ){
                    String nombre = campos[i];
                    String orden = request.getParameter("orden"+nombre);
                    ordenes[i] = orden;
                }
                model.gruposReporteService.eliminarCampos(codReporte, campos, ordenes, dstrct, user_update);
                model.consultasGrlesReportesService.buscarDetalleDeCampos(codReporte);
                model.gruposReporteService.buscarGrupos(codReporte);
                next = "/jsp/sot/body/PantallaCamposGeneralesReporte.jsp?mensaje="+campos.length+" campos fueron eliminados correctamente&codReporte="+codReporte;
            }
            else if ( "saveCamposGral".equals(cmd) ) {
                String user_update = user.getLogin();
                String codReporte = request.getParameter("reporte");
                String [] campos = request.getParameterValues("campos");
                String [] ordenes = new String[campos.length];
                String [] ordenesAntiguos = new String[campos.length];
                for( int i=0; i< campos.length; i++ ){
                    String nombre = campos[i];
                    String orden = request.getParameter("orden"+nombre);
                    ordenes[i] = orden;
                    ordenesAntiguos[i] = request.getParameter("antiguoOrden"+nombre);
                }
                model.gruposReporteService.guardarCambiosOrdenCampos(codReporte, campos, ordenes, ordenesAntiguos, dstrct, user_update);
                model.consultasGrlesReportesService.buscarDetalleDeCampos(codReporte);
                model.gruposReporteService.buscarGrupos(codReporte);
                next = "/jsp/sot/body/PantallaCamposGeneralesReporte.jsp?mensaje=Los campos fueron guardados correctamente&codReporte="+codReporte;
            }
        }
        catch( Exception ex ){
            ex.printStackTrace();
        }
        this.dispatchRequest(next);
    }
    
}
