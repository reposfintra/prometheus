/*
 * ModificarNovedadAction.java
 *
 * Created on 13 de junio de 2005, 11:10 AM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  Henry
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;

public class ModificarNovedadAction extends Action {
    
    /** Creates a new instance of ModificarNovedadAction */
    public ModificarNovedadAction() {
    }
    
    public void run() throws ServletException {
        String next = "/jsp/trafico/novedad/VerNovedades.jsp?reload=";
        String codigo = (request.getParameter("codigo").toUpperCase());
        String nombre = (request.getParameter("nombre").toUpperCase());        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        try{
            
            Novedad novedad = new Novedad();
            novedad.setCodNovedad(codigo);
            novedad.setNomNovedad(nombre);
            novedad.setUser_update(usuario.getLogin());
            model.novedadService.modificarNovedad(novedad); 
        }
        catch (SQLException e){
               throw new ServletException(e.getMessage());
        }
         
         // Redireccionar a la p�gina indicada.
       this.dispatchRequest(next);
    }
}
 