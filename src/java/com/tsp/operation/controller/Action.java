/**
 */
package com.tsp.operation.controller;

import com.tsp.operation.model.*;
import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * The base class for all state transitions
 */
public abstract class Action {
    protected HttpServletRequest request;
    protected HttpServletResponse response;
    protected ServletContext application;
    protected Model model;
    
    /**
     * Verifica que se puede despachar la acci�n requerida.
     * @param reqDispatcher Despachador de las peticiones.<br>
     *                      Se verificar� que no sea <code>null</code>.
     * @param dispatchToPage p�gina que procesar� la petici�n HTTP.
     * @throws ServletException si el despachador de petici�n es <code>null</code>.
     */
    private void throwCantDispatchRequest(
    RequestDispatcher reqDispatcher, String dispatchToPage
    ) throws ServletException {
        if (reqDispatcher == null) {
            throw new ServletException("Could not find " + dispatchToPage);
        }
    }
    
    /**
     * Despacha la petici�n hacia una p�gina espec�fica, dependiendo
     * de la acci�n ejecutada.
     * @param pageName Nombre de la p�gina.
     * @throws ServletException Si ocurre un error durante la redirecci�n.
     *  o Si no se puede acceder a la p�gina.
     */
    public void dispatchRequest(String pageName) throws ServletException {
        //System.out.println("request dispacher");
        RequestDispatcher rd = application.getRequestDispatcher(pageName);
                this.throwCantDispatchRequest(rd, pageName);
        try{
            rd.forward(request, response);
        }catch(IOException e){
            e.printStackTrace();
            throw new ServletException("No se pudo cargar la pagina " + pageName);
        }
    }
    
    /**
     * Executes the action.  Subclasses should override
     * this method and have it forward the request to the
     * next view component when it completes processing.
     * @throws ServletException Si ocurre un error durante el procesamiento
     *         de la accion.
     * @throws IOException Si no se puede abrir la vista solicitada por esta
     *         accion, la cual corresponde a una p�gina jsp.
     */
    public abstract void run()
    throws ServletException, InformationException;
    
    /**
     * Sets the request.
     * @param request the request.
     */
    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }
    
    /**
     * Sets the response
     * @param response the response
     */
    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }
    
    /**
     * Sets the servlet context.
     * @param application the application.
     */
    public void setApplication(ServletContext application) {
        this.application = application;
    }
    
    /**
     * Sets the model.
     * @param model the model.
     */
    public void setModel(Model model) {
        this.model = model;
    }
    
    public TransaccionService getAddBatch(TransaccionService tService, ArrayList<String> listaQuerys) throws SQLException {
        for (String sql : listaQuerys) {
            tService.getSt().addBatch(sql);
        }
        return tService;
    }
    
     /**
     * Escribe la respuesta de una opcion ejecutada desde AJAX
     *
     * @param respuesta
     * @param contentType recomendado application/json;
     * @throws Exception
     */
    public void printlnResponseAjax(String respuesta, String contentType) throws Exception {

        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);

    }
}
