/*********************************************************************************
 * Nombre clase :                 ReporteCarbonAction.java                       *
 * Descripcion :                  Clase que maneja los eventos relacionados      *
 *                                con el programa que busca para el              *
 *                                reporte de carbon en la BD.                    *
 * Autor :                        LREALES                                        *
 * Fecha :                        4 de noviembre de 2006, 08:00 AM               *
 * Version :                      1.0                                            *
 * Copyright :                    Fintravalores S.A.                        *
 ********************************************************************************/

package com.tsp.operation.controller;

import java.util.Vector;
import java.lang.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.ServletException;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;

public class ReporteCarbonAction extends Action {
    
    /** Creates a new instance of ReporteCarbonAction */
    public ReporteCarbonAction () { }
    
    public void run () throws ServletException, InformationException {
        
        String next = "/jsp/general/exportar/carbon/ReporteCarbon.jsp";
        
        HttpSession session = request.getSession ();
        Usuario usuario = ( Usuario ) session.getAttribute ( "Usuario" );
        
        String hacer = ( request.getParameter ("hacer") != null )?request.getParameter ("hacer").toUpperCase():"";
        
        boolean todos = false;
        String opcion = ( request.getParameter("opcion")!= null )?request.getParameter("opcion"):"";
        todos = opcion.equals("1")?true:false;
        
        String nit_proveedor = ( request.getParameter ("nit_prov") != null )?request.getParameter ("nit_prov"):"";
        
        String fecini = ( request.getParameter ("fecini") != null )?request.getParameter ("fecini"):"";
        String fecfin = ( request.getParameter ("fecfin") != null )?request.getParameter ("fecfin"):"";
        
        try {
            
            Vector datos = model.reporteCarbonService.getVectorReporte ();
            
            model.reporteCarbonService.reporteCarbon ( todos, nit_proveedor, fecini, fecfin );
            datos = model.reporteCarbonService.getVectorReporte ();  

            if ( datos.size () > 0 ) {
                
                if ( hacer.equals ( "2" ) ) {
                    
                    HiloReporteCarbon HRC = new HiloReporteCarbon ();
                    HRC.start ( datos, usuario.getLogin (), fecini, fecfin );
                    
                    next = "/jsp/general/exportar/carbon/ReporteCarbon.jsp?msg=ARCHIVO EXPORTADO A EXCEL! E-Mail enviado a los proveedores correspondientes!";
                    
                }
            
            } else { 
                
                next = "/jsp/general/exportar/carbon/ReporteCarbon.jsp?msg=Su busqueda no arrojo resultados!";
                
            }            
            
        } catch ( Exception e ) {
            
            e.printStackTrace ();
            throw new ServletException ( "Error en Reporte de Carbon Action : " + e.getMessage () );
            
        }        
        
        this.dispatchRequest ( next );
        
    }
    
}