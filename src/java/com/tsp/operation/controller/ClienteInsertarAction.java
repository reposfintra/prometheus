/***********************************************
 * Nombre clase: PlacaInsertAction.java
 * Descripci�n: Accion para ingresar una placa a la bd.
 * Autor: AMENDEZ
 * Fecha: 4 de noviembre de 2004, 02:48 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A.
 **********************************************/

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import java.lang.*;
import com.tsp.operation.model.threads.*;
public class ClienteInsertarAction extends Action{
    
    //static Logger logger = Logger.getLogger(PlacaInsertAction.class);
    
    /** Creates a new instance of PlacaInsertAction */
    public ClienteInsertarAction() {
    }
    
    public void run() throws ServletException, InformationException{
        String valorCodigo = "";
        String opcion = request.getParameter("op")!=null?request.getParameter("op"):"";
        String next = "";
        if(opcion.equals("VERIFICAR_CLIENTE")){
            try{
                String cod_cli = (request.getParameter("cod_cli")!=null)?request.getParameter("cod_cli"):"";
                if (model.clienteService.existeCliente(cod_cli)){
                    next="/jsp/sot/body/Puente.jsp?opcion=si";
                }
                else{
                    next="/jsp/sot/body/Puente.jsp?opcion=no";
                }
            }
            
            catch (Exception e){
                e.printStackTrace();
                throw new ServletException(e.getMessage());
            }
        }
        else{
            next = "/jsp/sot/body/AdicionarCliente.jsp";
            
            String cmd = request.getParameter("cmd");
            BeanGeneral Cliente = new BeanGeneral();
            Date fecha = new Date();
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String fechacrea = format.format(fecha);
            String msg = "";
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario)session.getAttribute("Usuario");
            String distrito = (String) session.getAttribute("Distrito");
            String basess=  usuario.getBase();
            
            
            Cliente.setValor_01((request.getParameter("Estado")!=null)?request.getParameter("Estado"):"");
            Cliente.setValor_02((request.getParameter("CodCliente")!=null)?request.getParameter("CodCliente"):"");
            valorCodigo= Cliente.getValor_02();
            Cliente.setValor_03((request.getParameter("NomCliente")!=null)?request.getParameter("NomCliente"):"");
            Cliente.setValor_04((request.getParameter("Notas")!=null)?request.getParameter("Notas"):"");
            String a = (request.getParameter("agencia")!=null)?request.getParameter("agencia"):"";
            a = a.equals("NADA")?"":a;
            Cliente.setValor_05(a);
            Cliente.setValor_06(basess);
            Cliente.setValor_07((request.getParameter("Texto_oc")!=null)?request.getParameter("Texto_oc"):"");
            Cliente.setValor_08((request.getParameter("CedCliente")!=null)?request.getParameter("CedCliente"):"");
            Cliente.setValor_09((request.getParameter("CedAgente")!=null)?request.getParameter("CedAgente"):"");
            Cliente.setValor_10((request.getParameter("Rentabilidad")!=null && !request.getParameter("Rentabilidad").equals(""))?request.getParameter("Rentabilidad"):"0");
            Cliente.setValor_11((request.getParameter("SopFacturacion")!=null)?request.getParameter("SopFacturacion"):"");
            Cliente.setValor_12((request.getParameter("Fiduciaria")!=null)?request.getParameter("Fiduciaria"):"");
            Cliente.setValor_13((request.getParameter("DisCliente")!=null)?request.getParameter("DisCliente"):"");
            Cliente.setValor_14((request.getParameter("Moneda")!=null)?request.getParameter("Moneda"):"");
            Cliente.setValor_15((request.getParameter("ForPago")!=null)?request.getParameter("ForPago"):"");
            Cliente.setValor_16((request.getParameter("Plazo").equals("")?"0":request.getParameter("Plazo")));
            Cliente.setValor_17((request.getParameter("Zona")!=null)?request.getParameter("Zona"):"");
            Cliente.setValor_18((request.getParameter("BanConsignacion")!=null)?request.getParameter("BanConsignacion"):"");
            Cliente.setValor_19((request.getParameter("SucBanco")!=null)?request.getParameter("SucBanco"):"");
            Cliente.setValor_20((request.getParameter("ForHC")!=null)?request.getParameter("ForHC"):"");
            Cliente.setValor_21((request.getParameter("UniContAsociada")!=null)?request.getParameter("UniContAsociada"):"");
            Cliente.setValor_22((request.getParameter("CodImpCliente")!=null)?request.getParameter("CodImpCliente"):"");
            String agF = (request.getParameter("agenciaFact")!=null)?request.getParameter("agenciaFact"):"";
            agF = agF.equals("NADA")?"":agF;
            Cliente.setValor_23(agF);
            Cliente.setValor_24("");
            Cliente.setValor_25(fechacrea);
            //System.out.println("plazo ======   "+Cliente.getValor_16());
            //MODIFICACION ANEXOS DE PARAMETROS
            Cliente.setValor_26((request.getParameter("Direccion")!=null)?request.getParameter("Direccion"):"");
            Cliente.setValor_27((request.getParameter("Telefono")!=null)?request.getParameter("Telefono"):"");
            Cliente.setValor_28((request.getParameter("NomContacto")!=null)?request.getParameter("NomContacto"):"");
            Cliente.setValor_29((request.getParameter("TelContacto")!=null)?request.getParameter("TelContacto"):"");
            Cliente.setValor_30((request.getParameter("e_mail")!=null)?request.getParameter("e_mail"):"");
            Cliente.setValor_31((request.getParameter("DirFactura")!=null)?request.getParameter("DirFactura"):"");
            Cliente.setValor_32((request.getParameter("ManejoPreFac")!=null)?request.getParameter("ManejoPreFac"):"");
            
            String TPF = (request.getParameter("TiempoPreFac")!=null)?request.getParameter("TiempoPreFac"):"";
            
            Cliente.setValor_33(TPF);
            
            String TLF = (request.getParameter("TiempoLegFac")!=null)?request.getParameter("TiempoLegFac"):"";
            
            Cliente.setValor_34(TLF);
            
            String TRF = (request.getParameter("TiempoReFac")!=null)?request.getParameter("TiempoReFac"):"";
            
            Cliente.setValor_35(TRF);
            
            String DP = (request.getParameter("DiaPago")!=null)?request.getParameter("DiaPago"):"";
            
            Cliente.setValor_36(DP);
            
            Cliente.setValor_37((request.getParameter("ForFacturacion")!=null)?request.getParameter("ForFacturacion"):"");
            Cliente.setValor_39((request.getParameter("Dir_cont")!=null)?request.getParameter("Dir_cont"):"");
            Cliente.setValor_40((request.getParameter("agCobro")!=null)?request.getParameter("agCobro"):"");
            if (usuario != null){
                //Usuario
                Cliente.setValor_38((usuario.getLogin()!=null)?usuario.getLogin():"");
            }
            Cliente.setValor_41((request.getParameter("ForHC")!=null)?request.getParameter("ForHC"):"");
            Cliente.setValor_42((request.getParameter("Rif")!=null)?request.getParameter("Rif"):"");
            Cliente.setValor_43((request.getParameter("Ciudad")!=null)?request.getParameter("Ciudad"):"");
            Cliente.setValor_44((request.getParameter("CiudadFact")!=null)?request.getParameter("CiudadFact"):"");
            Cliente.setValor_45((request.getParameter("Pais")!=null)?request.getParameter("Pais"):"");
            Cliente.setValor_46((request.getParameter("PaisEnv")!=null)?request.getParameter("PaisEnv"):"");
            //by rarp
            
            /*Cliente.setValor_47(Float.valueOf(request.getParameter("treduc")).floatValue());
            Cliente.setValor_48(Float.valueOf(request.getParameter("cust")).floatValue());
            Cliente.setValor_49(Float.valueOf(request.getParameter("rem")).floatValue());

            Cliente.setValor_50((request.getParameter("titcta")!=null)?request.getParameter("titcta"):"");
            Cliente.setValor_51((request.getParameter("cctcta")!=null)?request.getParameter("cctcta"):"");
            Cliente.setValor_52((request.getParameter("tcta")!=null)?request.getParameter("tcta"):"");
            Cliente.setValor_53((request.getParameter("nocta")!=null)?request.getParameter("nocta"):"");*/
            //Cliente.setValor_54((request.getParameter("nita")!=null)?request.getParameter("nita"):"");
            
            //end by rarp
            
            /*funcion select de soporte de facturacion*/
            String [] soportes  = request.getParameterValues("Soporte");
            String sopFacturacion = "";
            try{
                if(soportes == null){
                    System.out.println("es null los soportes");
                }else{
                    sopFacturacion = model.clienteService.cargarSoporteCampoCliente(soportes);
                }
                Cliente.setValor_11(sopFacturacion);
                
                //cargar los parametros del
                List ListaAgencias = model.agenciaService.listarAgenciasXFacturacion();
                TreeMap tm = new TreeMap();
                if(ListaAgencias.size()>0) {
                    Iterator It3 = ListaAgencias.iterator();
                    while(It3.hasNext()) {
                        Ciudad  datos2 =  (Ciudad) It3.next();
                        tm.put("["+datos2.getCodCiu()+"] "+datos2.getNomCiu(), datos2.getCodCiu());
                        //out.print("<option value='"+datos2.getCodCiu()+"'>["+datos2.getCodCiu()+"] "+datos2.getNomCiu()+"</option> \n");
                    }
                }
                request.setAttribute("agencias", ListaAgencias);
                request.setAttribute("agencias_tm", tm);
                String Codigo = Cliente.getValor_02();
                /*//VALIDACION DE EXISTENCIA DEL CLIENTE*/
                
                if(model.clienteService.existeClienteIngresarEstadoLLeno(Codigo)==true){
                    next += "?msg=" + "El Cliente  :  " + Cliente.getValor_02() + " ha sido anulado anteriormente";
                }
                
                else if(!model.clienteService.existeCliente(Codigo)) {
                    String distritoCl = usuario.getDstrct();
                    if(Codigo.equals("final")){
                        next += "?msg= No se puede ingresar mas clientes por favor cambie el prefijo y contador de Cliente";
                    }else{
                        /******************************************************/
                        /* Jose de la rosa */
                        String clien = (request.getParameter("CodCliente")!=null)?request.getParameter("CodCliente"):"";
                        String nit = ( request.getParameter("CedCliente")!=null)?request.getParameter("CedCliente"):"" ;                   
                        if( !model.clienteService.existeNitCliente( clien, nit ) ){
                            //agregamos un nit al cliente
                            model.identidadService.buscarIdentidad( ( request.getParameter("CedCliente")!=null)?request.getParameter("CedCliente"):"", distrito );
                            Identidad id = model.identidadService.obtIdentidad();
                            if ( id != null ){
                                Identidad ident = new Identidad();
                                ident.setTipo_iden  ( id.getTipo_iden() );
                                ident.setCedula     ( ( request.getParameter("CedCliente")!=null)?request.getParameter("CedCliente"):"" );
                                ident.setId_mims    ( id.getId_mims() );
                                ident.setNombre     ( (request.getParameter("NomCliente")!=null)?request.getParameter("NomCliente"):"" );
                                ident.setNom1       ( id.getNom1() );
                                ident.setNom2       ( "" );
                                ident.setApe1       ( "" );
                                ident.setApe2       ( "" );
                                ident.setSexo       ( "" );
                                ident.setFechanac   ( id.getFechanac() );
                                ident.setDireccion  ( ( request.getParameter("Direccion")!=null)?request.getParameter("Direccion"):"" );
                                ident.setCodciu     ( ( request.getParameter("Ciudad")!=null)?request.getParameter("Ciudad"):"" );
                                ident.setCodpais    ( ( request.getParameter("Pais")!=null)?request.getParameter("Pais"):"" );
                                Ciudad ciudad = model.ciudadService.obtenerCiudad( ident.getCodciu( ) );
                                ident.setCoddpto    ( ciudad!=null?ciudad.getdepartament_code():"" );
                                ident.setTelefono1  ( id.getTelefono1() );
                                try{
                                    ident.setTelefono( (request.getParameter("Telefono")!=null)?request.getParameter("Telefono"):"" );
                                }
                                catch (Exception e){
                                    e.printStackTrace();
                                }
                                ident.setCelular    ( id.getCelular() );
                                ident.setE_mail     ( id.getE_mail() );
                                ident.setCargo      ( id.getCargo() );
                                String ncontacto =  ( request.getParameter("NomContacto")!=null)?request.getParameter("NomContacto"):"";
                                if ( ncontacto.length() > 29 )
                                    ncontacto = ncontacto.substring( 0, 29 );
                                ident.setRef1           ( ncontacto );
                                ident.setObservacion    ( ( request.getParameter("Notas")!=null)?request.getParameter("Notas"):"" );
                                
                                ident.setUsuariocrea    ( usuario.getLogin() );
                                ident.setUsuario        ( usuario.getLogin() );
                                ident.setBase           ( usuario.getBase() );
                                ident.setCia            ( distrito );
                                //nuevos campos
                                ident.setLibmilitar     ( id.getLibmilitar() );
                                ident.setExpced         ( id.getExpced() );
                                ident.setLugarnac       ( id.getLugarnac() );
                                ident.setBarrio         ( id.getBarrio() );
                                ident.setEst_civil      ( id.getEst_civil() );
                                ident.setSenalParticular( id.getSenalParticular() );
                                ident.setNomnemo        ( id.getNomnemo() );
                                ident.setClasificacion  ( "00000L" );
                                ident.setVeto           ( id.getVeto() );
                                model.identidadService.modificarIdentidad( ident );
                            }
                            else{
                                Identidad ident = new Identidad();
                                ident.setTipo_iden  ( "NIT" );
                                ident.setCedula     ( ( request.getParameter("CedCliente")!=null)?request.getParameter("CedCliente"):"" );
                                ident.setId_mims    ( "" );
                                ident.setNombre     ( ( request.getParameter("NomCliente")!=null)?request.getParameter("NomCliente"):"" );
                                ident.setNom1       ( "" );
                                ident.setNom2       ( "" );
                                ident.setApe1       ( "" );
                                ident.setApe2       ( "" );
                                ident.setDireccion  ( ( request.getParameter("Direccion")!=null)?request.getParameter("Direccion"):"" );
                                ident.setCodciu     ( ( request.getParameter("Ciudad")!=null)?request.getParameter("Ciudad"):"" );
                                ident.setCodpais    ( ( request.getParameter("Pais")!=null)?request.getParameter("Pais"):"" );
                                Ciudad ciudad = model.ciudadService.obtenerCiudad( ident.getCodciu( ) );
                                ident.setCoddpto    ( ciudad!=null?ciudad.getdepartament_code():"" );
                                ident.setSexo       ( "" );
                                ident.setFechanac   ( "0099-01-01" );
                                ident.setTelefono1  ( "" );
                                try{
                                    ident.setTelefono( (request.getParameter("Telefono")!=null)?request.getParameter("Telefono"):"" );
                                }
                                catch (Exception e){
                                    e.printStackTrace();
                                }
                                ident.setCelular    ( "" );
                                ident.setE_mail     ( "" );
                                ident.setUsuario    ( usuario.getLogin() );
                                ident.setUsuariocrea( usuario.getLogin() );
                                ident.setBase       ( usuario.getBase() );
                                ident.setCia( distrito );
                                ident.setCargo      ( "" );
                                String ncontacto =  ( request.getParameter("NomContacto")!=null)?request.getParameter("NomContacto"):"";
                                if ( ncontacto.length() > 29 )
                                    ncontacto = ncontacto.substring( 0, 29 );
                                ident.setRef1       ( ncontacto );
                                ident.setObservacion( ( request.getParameter("Notas")!=null)?request.getParameter("Notas"):"" );
                                ident.setEst_civil  ( "" );
                                ident.setLibmilitar( "" );
                                ident.setLugarnac   ( "" );
                                ident.setExpced     ( "" );
                                ident.setBarrio     ( "" );
                                ident.setSenalParticular( "" );
                                ident.setNomnemo        ( "" );
                                ident.setClasificacion  ( "00000L" );//Clasificacion para los nit de cliente
                                ident.setVeto           ( "N" );
                                model.identidadService.insertarIdentidad( ident );
                            }
                            /* Jose de la rosa */
                            model.clienteService.agregarCliente(Cliente);
                            model.clienteService.IncrementoDeCodigo(Codigo);
                            if(soportes != null){
                                for(int i = 0 ; i< soportes.length; i++){
                                    String llave = soportes[i];
                                    String codigo= Cliente.getValor_02();
                                    model.clienteService.agregarSoporteCliente(llave, codigo, distritoCl);
                                    model.clienteService.cargarSoporte(soportes);
                                    
                                }//cierra for
                            }//cierra if
                            String adicional = id != null?", se modifico el nit del cliente correctamente.":", se agrego el nit del cliente correctamente.";
                            next += "?msg=" + "El Cliente  :  " + Cliente.getValor_03() + " fue agregado exitosamente"+ adicional;
                        }
                        else{
                            next += "?msg=" + "No se puede crear el cliente por que el nit : " + nit + " ya se encuentra relacionado en otro cliente";
                        }
                    }
                }else{
                    next += "?msg=" + "El Cliente  :   " + Cliente.getValor_02() + " ya existe";
                    
                }
                
            }
            catch (Exception e){
                e.printStackTrace();
                throw new ServletException(e.getMessage());
            }
            
        }
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
        
    }
}
/********************************************************
 * Entregado a Fily 12 Febrero 2007
 *********************************************************/
