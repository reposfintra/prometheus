/*******************************************************************************
 * Nombre clase: Sancion_listarSerchAction.java
 * Descripci�n: Accion para listar las sanciones con o sin fecha de aprobaci�n.
 * Autor: Jose de la rosa
 * Fecha: 20 de octubre de 2005, 10:41 AM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 *******************************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;

public class Sancion_listarSerchAction extends Action{
    
    /** Creates a new instance of Sancion_listarSerchAction */
    public Sancion_listarSerchAction () {
    }
    
    public void run () throws ServletException, com.tsp.exceptions.InformationException {
        String next="";
        HttpSession session = request.getSession ();
        String fecha_fin = (request.getParameter ("c_fecha_fin")!=null)?request.getParameter ("c_fecha_fin").toUpperCase ():"";
        String fecha_inicio = (request.getParameter ("c_fecha_inicio")!=null)?request.getParameter ("c_fecha_inicio").toUpperCase ():"";
        if((fecha_inicio.equals (""))&&(fecha_fin.equals (""))){
            //creaci�n de la fecha actual del sistema
            Date hoy = new Date ();
            SimpleDateFormat s = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
            fecha_fin = s.format (hoy);
            //obtiene losultimos 30 dias de una fecha dada
            fecha_inicio = Util.fechaFinal (fecha_fin,-30);
            fecha_inicio = fecha_inicio.substring (0,10);
            fecha_fin = fecha_fin.substring (0,10);
        }
        try{
            String tipo = request.getParameter ("c_tipo").toUpperCase ();
            if(tipo.equals ("S")){
                model.sancionService.listSancionSinAprobacion ();
                model.sancionService.getSanciones ();
            }
            else{
                model.sancionService.listSancionConAprobacion (fecha_inicio,fecha_fin);
                model.sancionService.getSanciones ();
            }
            next="/jsp/cumplidos/sancion/SancionListarAprobados.jsp";
            next = Util.LLamarVentana (next, "Listar Sanciones");
        }catch (SQLException e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
    
}
