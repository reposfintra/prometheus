/***********************************************************************************
 * Nombre clase : ............... ActividadesaduanerasReporteAction                *
 * Descripcion :................. Controlador del reporte de control de actividades*
 *                                aduaneras                                        * 
 * Autor :....................... Ing. Diogenes Antonio Bastidas Morales           *
 * Fecha :........................ 30 de noviembre de 2005, 11:18 AM               *
 * Version :...................... 1.0                                             *
 * Copyright :.................... Fintravalores S.A.                         *
 ***********************************************************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  dbastidas
 */
public class ActividadesaduanerasReporteAction extends Action{
    
    /** Creates a new instance of ActividadesaduanerasReporte */
    public ActividadesaduanerasReporteAction() {
    }
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
        String fecha1 = request.getParameter("fechai");
        String fecha2 = request.getParameter("fechaf");
        String next = "/jsp/trafico/actividad_Aduanera/actividadesAduaneras.jsp";
        try{
            model.controlactAduaneraService.reporteActividadAduanera(fecha1, fecha2);
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
