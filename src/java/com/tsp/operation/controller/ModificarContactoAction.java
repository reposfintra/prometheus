/*
 * Nombre        ModificarContactoAction.java
 * Autor         Ing. Rodrigo Salazar
 * Fecha         23 de junio de 2005, 10:45 AM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */


package com.tsp.operation.controller;

/**
 *
 * @author  DRIGO
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class ModificarContactoAction extends Action {
    
    /** Creates a new instance of ModificarContactoAction */
    public ModificarContactoAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException {
        String next = "/jsp/trafico/contacto/contacto.jsp?mensaje=MsgModificado";
        String codigo = (request.getParameter("c_codigo").toUpperCase());
        String cod_cia = (request.getParameter("c_cia").toUpperCase());
        String tipo = (request.getParameter("c_tipo").toUpperCase());
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        try{
            
            
            Contacto contacto = new Contacto();
            contacto.setCod_contacto(codigo);
            contacto.setCod_cia(cod_cia);
            contacto.setTipo(tipo);                   
            contacto.setUser_update(usuario.getLogin());
            
            contacto.setNomcontacto(request.getParameter("nomcont"));
            contacto.setNomcopania(request.getParameter("nomcia"));
                
            model.contactoService.modificarContacto(contacto); 
        }
        catch (SQLException e){
               throw new ServletException(e.getMessage());
        }
         
         // Redireccionar a la p�gina indicada.
       this.dispatchRequest(next);
    }
}
 
