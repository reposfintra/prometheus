/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.beans.AfiliadoConvenio;
import com.tsp.operation.model.beans.AfiliadoConvenioRangos;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.Convenio;
import com.tsp.operation.model.beans.ConvenioComision;
import com.tsp.operation.model.beans.ConvenioCxc;
import com.tsp.operation.model.beans.ConveniosRemesas;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.ExcelApplication;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.Vector;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import com.tsp.operation.model.beans.ConvenioCxcFiducias;
import com.tsp.operation.model.beans.ConvenioFiducias;

/**
 *
 * @author maltamiranda
 */
public class GestionConveniosAction extends Action {

    @Override
    public void run() throws ServletException, InformationException {
        boolean redirect = false;
        try {
            HttpSession session = request.getSession();

            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            usuario.setBd("fintra");//20100901
            String tipo = request.getParameter("tipo") != null ? request.getParameter("tipo") : "";//20100901
            String tipoconv = request.getParameter("tipoconv") != null ? request.getParameter("tipoconv") : "";
            String next = "/jsp/fenalco/convenio/GestionConvenios.jsp?tipoconv="+tipoconv;
            String redescuento = request.getParameter("entidad") != null ? request.getParameter("entidad") : "0";
            if (!tipo.equals("NUEVO") && !tipo.equals("MODIFICAR")) {
                Convenio cnv = null;//20100901
                if (tipo.equals("NUEVO_CONVENIO")) {
                    redirect = true;//20100901
                    cnv = loadConvenio(usuario);//20100901
                    model.gestionConveniosSvc.insertar_convenio(usuario.getBd(), cnv,redescuento,tipo);
                    session.removeAttribute("convenio");
                    session.setAttribute("convenio", model.gestionConveniosSvc.buscar_convenio(usuario.getBd(), cnv.getId_convenio()));
                } else {
                    if (tipo.equals("MODIFICAR_CONVENIO")) {
                        redirect = true;//20100901ge
                        cnv = loadConvenio(usuario);//20100901
                        redirect = true;
                        model.gestionConveniosSvc.actualizar_convenio(usuario.getBd(), cnv, tipo);
                        session.removeAttribute("convenio");
                        session.setAttribute("convenio", model.gestionConveniosSvc.buscar_convenio(usuario.getBd(), cnv.getId_convenio()));

                    }
                    if (tipo.equals("exportar")) {//20100901
                        ArrayList<Convenio> lista = null;
                        String respuesta = "<span class='letra'>Archivo generado exitosamente.<br>Revise su lista de archivos.</span>";
                        try {
                            lista = model.gestionConveniosSvc.listarConvenios(usuario.getBd());
                            this.exportarExcel(lista, usuario.getLogin());
                        } catch (Exception e) {
                            respuesta = "<span class='letra'>Ocurrio un error al procesar la peticion.</span>";
                            System.out.println("error al buscar la lista de convenios: " + e.toString());
                            e.printStackTrace();
                        }
                        response.setContentType("text/plain; charset=utf-8");
                        response.setHeader("Cache-Control", "no-cache");
                        response.getWriter().println(respuesta);
                    }
                    if (tipo.equals("buscterc")) {//20100901
                        String columna = request.getParameter("filtro") != null ? request.getParameter("filtro") : "nombre";
                        String cadena = request.getParameter("cadena") != null ? request.getParameter("cadena") : "";
                        cadena = cadena.toUpperCase();
                        ArrayList<String> listacad = null;
                        boolean pasa = true;
                        try {
                            listacad = model.gestionConveniosSvc.buscarDatosTercero(columna, cadena);
                        } catch (Exception e) {
                            pasa = false;
                            System.out.println("Error: " + e.toString());
                            e.printStackTrace();
                        }
                        String[] splitcad = null;
                        String cadresp = "<table style='border-collapse:collapse; width:100%;' border='1'>"
                                + "<tr class='subtitulo1'>"
                                + "<th>Nit</th>"
                                + "<th>Nombre</th>"
                                + "</tr>";
                        if (pasa == true && listacad.size() > 0) {
                            for (int i = 0; i < listacad.size(); i++) {
                                splitcad = (listacad.get(i)).split(";_;");
                                cadresp += "<tr class='fila' style='cursor: pointer;' onclick='asignarValor2(fila,\"" + splitcad[0] + "\",varname,\"" + splitcad[1] + "\");cerrarDiv();'>"
                                        + "<td>" + splitcad[0] + "</td>"
                                        + "<td>" + splitcad[1] + "</td>"
                                        + "</tr>";
                            }
                        } else {
                            cadresp += "<tr class='fila' style='cursor: pointer;' onclick='cerrarDiv();'><td colspan='2' align='center'>No se encontraron resultados</td></tr>";
                        }
                        cadresp += "</table>";
                        response.setContentType("text/plain; charset=utf-8");
                        response.setHeader("Cache-Control", "no-cache");
                        response.getWriter().println(cadresp);
                    }
                    if (tipo.equals("busciudad")) {//20100901
                        String columna = request.getParameter("filtroc") != null ? request.getParameter("filtroc") : "nombre";
                        String cadena = request.getParameter("cadenac") != null ? request.getParameter("cadenac") : "";
                        cadena = cadena.toUpperCase();
                        ArrayList<String> listacad = null;
                        boolean pasa = true;
                        try {
                            listacad = model.gestionConveniosSvc.buscarDatosCiudad(columna, cadena);
                        } catch (Exception e) {
                            pasa = false;
                            System.out.println("Error: " + e.toString());
                            e.printStackTrace();
                        }
                        String[] splitcad = null;
                        String cadresp = "<table style='border-collapse:collapse; width:100%;' border='1'>"
                                + "<tr class='subtitulo1'>"
                                + "<th>Codigo</th>"
                                + "<th>Nombre</th>"
                                + "</tr>";
                        if (pasa == true && listacad.size() > 0) {
                            for (int i = 0; i < listacad.size(); i++) {
                                splitcad = (listacad.get(i)).split(";_;");
                                cadresp += "<tr class='fila' style='cursor: pointer;' onclick='asignarValor(fila,\"" + splitcad[0] + "\");cerrarDiv();'>"
                                        + "<td>" + splitcad[0] + "</td>"
                                        + "<td>" + splitcad[1] + "</td>"
                                        + "</tr>";
                            }
                        } else {
                            cadresp += "<tr class='fila' style='cursor: pointer;' onclick='cerrarDiv();'><td colspan='2' align='center'>No se encontraron resultados</td></tr>";
                        }
                        cadresp += "</table>";
                        response.setContentType("text/plain; charset=utf-8");
                        response.setHeader("Cache-Control", "no-cache");
                        response.getWriter().println(cadresp);
                    }
                    if (tipo.equals("busbanco")) {//20100901
                        String columna = request.getParameter("filtrob") != null ? request.getParameter("filtrob") : "nombre";
                        String cadena = request.getParameter("cadenab") != null ? request.getParameter("cadenab") : "";
                        cadena = cadena.toUpperCase();
                        ArrayList<String> listacad = null;
                        boolean pasa = true;
                        try {
                            listacad = model.gestionConveniosSvc.buscarBancos(columna, cadena);
                        } catch (Exception e) {
                            pasa = false;
                            System.out.println("Error: " + e.toString());
                            e.printStackTrace();
                        }
                        String[] splitcad = null;
                        String cadresp = "<table style='border-collapse:collapse; width:100%;' border='1'>"
                                + "<tr class='subtitulo1'>"
                                + "<th>Codigo</th>"
                                + "<th>Nombre</th>"
                                + "</tr>";
                        if (pasa == true && listacad.size() > 0) {
                            for (int i = 0; i < listacad.size(); i++) {
                                splitcad = (listacad.get(i)).split(";_;");
                                cadresp += "<tr class='fila' style='cursor: pointer;' onclick='asignarValor(fila,\"" + splitcad[0] + "\");cerrarDiv();'>"
                                        + "<td>" + splitcad[0] + "</td>"
                                        + "<td>" + splitcad[1] + "</td>"
                                        + "</tr>";
                            }
                        } else {
                            cadresp += "<tr class='fila' style='cursor: pointer;' onclick='cerrarDiv();'><td colspan='2' align='center'>No se encontraron resultados</td></tr>";
                        }
                        cadresp += "</table>";
                        response.setContentType("text/plain; charset=utf-8");
                        response.setHeader("Cache-Control", "no-cache");
                        response.getWriter().println(cadresp);
                    }
                    if (tipo.equals("buscarusu")) {
                        String codigousu = request.getParameter("usuariocod") != null ? request.getParameter("usuariocod") : "";
                        codigousu = codigousu.toUpperCase();
                        String cadresp = ";_;";
                        try {
                            cadresp = model.gestionConveniosSvc.datosCodigoUsuario(codigousu);
                        } catch (Exception e) {
                            cadresp = ";_;";
                            System.out.println("error: " + e.toString());
                            e.printStackTrace();
                        }
                        response.setContentType("text/plain; charset=utf-8");
                        response.setHeader("Cache-Control", "no-cache");
                        response.getWriter().println(cadresp);
                    }
                    if (tipo.equals("buscarcuenta")) {
                        String cuenta = request.getParameter("cuenta") != null ? request.getParameter("cuenta") : "";
                       boolean sw= false;
                        try {
                            sw = model.gestionConveniosSvc.existeCuenta(cuenta);
                        } catch (Exception e) {
                            System.out.println("error: " + e.toString());
                            e.printStackTrace();
                        }
                        response.setContentType("text/plain; charset=utf-8");
                        response.setHeader("Cache-Control", "no-cache");
                        response.getWriter().println(sw+"");
                    }
                    if (tipo.equals("asignarconv")) {
                        redirect = true;
                        String nit = request.getParameter("id") != null ? request.getParameter("id") : "";
                        String vista = request.getParameter("vista") != null ? request.getParameter("vista") : "1";
                        String trans = request.getParameter("trans") != null ? request.getParameter("trans") : "N";
                        next="/jsp/fenalco/convenio/AsignarConvenioAfiliado.jsp?id="+nit+"&vista="+vista+"&trans="+trans;
                        int maxfilas = request.getParameter("maxfilas") == null ? 0 : Integer.parseInt(request.getParameter("maxfilas"));
                        int fils = request.getParameter("fils") == null ? 0 : Integer.parseInt(request.getParameter("fils"));
                        ArrayList lista1 = new ArrayList();
                        ArrayList lista2 = null;
                        AfiliadoConvenio bean1 = null;
                        AfiliadoConvenioRangos bean2 = null;
                        for (int i = 0; i < fils; i++) {
                            if (request.getParameter("conv" + (i + 1)) != null && !(request.getParameter("conv" + (i + 1)).equals(""))) {
                                bean1 = new AfiliadoConvenio();
                                bean1.setIdProvConvenio(request.getParameter("idprovconv" + (i + 1)) != null ? request.getParameter("idprovconv" + (i + 1)) : "0");
                                bean1.setIdConvenio(request.getParameter("conv" + (i + 1)) != null ? request.getParameter("conv" + (i + 1)) : "0");
                                bean1.setCodSector(request.getParameter("sector" + (i + 1)) != null ? request.getParameter("sector" + (i + 1)) : "0");
                                bean1.setCodSubsector(request.getParameter("subsector" + (i + 1)) != null ? request.getParameter("subsector" + (i + 1)) : "0");
                                bean1.setValorCobertura(request.getParameter("cobertura" + (i + 1)) != null ? (request.getParameter("cobertura" + (i + 1))).replaceAll(",", "") : "0");
                                bean1.setPorcCoberturaFlotante(request.getParameter("cobert_flotante" + (i + 1)) != null ? request.getParameter("cobert_flotante" + (i + 1)).replaceAll(",", "") : "0");
                                bean1.setTasaInteres(request.getParameter("tasa" + (i + 1)) != null ? request.getParameter("tasa" + (i + 1)).replaceAll(",", "") : "0");
                                bean1.setValorCustodia(request.getParameter("custodia" + (i + 1)) != null ? request.getParameter("custodia" + (i + 1)).replaceAll(",", "") : "0");
                                bean1.setComision(request.getParameter("comision" + (i + 1)) != null ? request.getParameter("comision" + (i + 1)) : "false");
                                bean1.setCuentaComision(request.getParameter("cuenta" + (i + 1)) != null ? request.getParameter("cuenta" + (i + 1)) : "0");
                                bean1.setPorcentajeAfiliado(request.getParameter("porc_afil" + (i + 1)) != null ? request.getParameter("porc_afil" + (i + 1)) : "0");
                                bean1.setUsuario(usuario.getLogin());
                                lista2 = new ArrayList();
                                for (int j = 0; j < maxfilas; j++) {
                                    bean2 = new AfiliadoConvenioRangos();
                                    if (request.getParameter("ricomision" + (i + 1) + "_" + (j + 1)) != null && !(request.getParameter("ricomision" + (i + 1) + "_" + (j + 1)).equals(""))) {
                                        bean2.setCuotaIni(request.getParameter("ricomision" + (i + 1) + "_" + (j + 1)) != null && !(request.getParameter("ricomision" + (i + 1) + "_" + (j + 1)).equals(""))
                                                ? request.getParameter("ricomision" + (i + 1) + "_" + (j + 1)) : "0");
                                        bean2.setCuotaFin(request.getParameter("rfcomision" + (i + 1) + "_" + (j + 1)) != null && !(request.getParameter("rfcomision" + (i + 1) + "_" + (j + 1)).equals(""))
                                                ? request.getParameter("rfcomision" + (i + 1) + "_" + (j + 1)) : "0");
                                        bean2.setPorcentajeComision(request.getParameter("pcomision" + (i + 1) + "_" + (j + 1)) != null && !(request.getParameter("pcomision" + (i + 1) + "_" + (j + 1)).equals(""))
                                                ? request.getParameter("pcomision" + (i + 1) + "_" + (j + 1)) : "0");
                                        lista2.add(bean2);
                                    }
                                }
                                bean1.setRangos(lista2);
                                lista1.add(bean1);
                            }
                        }

                        if (lista1.size() > 0) {
                            model.proveedorService.insertConvs(nit, lista1);//20100817
                        }
                    }
                    if (tipo.equals("buscarafil")) {
                        String columna = request.getParameter("filtro") != null ? request.getParameter("filtro") : "nombre";
                        String cadena = request.getParameter("cadena") != null ? request.getParameter("cadena") : "";
                        cadena = cadena.toUpperCase();
                        ArrayList<String> listacad = null;
                        boolean pasa = true;
                        try {
                                listacad = model.gestionConveniosSvc.buscarDatosAfil(columna, cadena);
                        }
                        catch (Exception e) {
                            pasa = false;
                            System.out.println("Error: " + e.toString());
                            e.printStackTrace();
                        }
                        String[] splitcad = null;
                        String cadresp = "<table style='border-collapse:collapse; width:100%;' border='1'>"
                                + "<tr class='subtitulo1'>"
                                + "<th>Codigo</th>"
                                + "<th>Nombre</th>"
                                + "</tr>";
                        if (pasa == true && listacad.size() > 0) {
                            for (int i = 0; i < listacad.size(); i++) {
                                splitcad = (listacad.get(i)).split(";_;");
                                cadresp += "<tr class='fila' style='cursor: pointer;' onclick='asignarValor2(\"" + splitcad[0] + "\",\"" + splitcad[1] + "\");cerrarDiv();'>"
                                        + "<td>" + splitcad[0] + "</td>"
                                        + "<td>" + splitcad[1] + "</td>"
                                        + "</tr>";
                            }
                        } else {
                            cadresp += "<tr class='fila' style='cursor: pointer;' onclick='cerrarDiv();'><td colspan='2' align='center'>No se encontraron resultados</td></tr>";
                        }
                        cadresp += "</table>";
                        response.setContentType("text/plain; charset=utf-8");
                        response.setHeader("Cache-Control", "no-cache");
                        response.getWriter().println(cadresp);
                    }
                    if (tipo.equals("buscarafised")) {
                        String columna = request.getParameter("filtro") != null ? request.getParameter("filtro") : "nombre";
                        String cadena = request.getParameter("cadena") != null ? request.getParameter("cadena") : "";
                        String trans = request.getParameter("trans") != null ? request.getParameter("trans") : "N";
                        cadena = cadena.toUpperCase();
                        ArrayList<String> listacad = null;
                        boolean pasa = true;
                        try {
                            if(trans.equals("S")){
                                listacad = model.gestionConveniosSvc.buscarProveedores(columna, cadena);
                            }else{
                            listacad = model.gestionConveniosSvc.buscarDatosAfil(columna, cadena);
                            }
                        } catch (Exception e) {
                            pasa = false;
                            System.out.println("Error: " + e.toString());
                            e.printStackTrace();
                        }
                        String[] splitcad = null;
                        String cadresp = "<table style='border-collapse:collapse; width:100%;' border='1'>"
                                + "<tr class='subtitulo1'>"
                                + "<th>&nbsp;</th>"
                                + "<th>Codigo</th>"
                                + "<th>Nombre</th>"
                                + "<th>Sede</th>"
                                + "</tr>";
                        if (pasa == true && listacad.size() > 0) {
                            for (int i = 0; i < listacad.size(); i++) {
                                String def = (i == 0) ? "checked" : "";
                                splitcad = (listacad.get(i)).split(";_;");
                                cadresp += "<tr class='fila' style='cursor: pointer;' >"
                                        + "<td > <input type='Radio' " + def + " name='afi'  id='afi' value=' " + splitcad[0] + "'></td>"
                                        + "<td onclick='asignarconv(\"" + splitcad[0] + "\");'>" + splitcad[0] + "</td>"
                                        + "<td onclick='asignarconv(\"" + splitcad[0] + "\");'>" + splitcad[1] + "</td>"
                                        + "<td onclick='asignarconv(\"" + splitcad[0] + "\");'>" + (splitcad[2].equals("S") ? "Si" : "No") + "</td>"
                                        + "</tr>";
                            }
                        } else {
                            cadresp += "<tr class='fila' style='cursor: pointer;' ><td colspan='4' align='center'>No se encontraron resultados</td></tr>";
                        }
                        cadresp += "</table>";
                        response.setContentType("text/plain; charset=utf-8");
                        response.setHeader("Cache-Control", "no-cache");
                        response.getWriter().println(cadresp);
                    }
                    if (tipo.equals("busconv")) {
                        String nitAfil = request.getParameter("nitprov") != null ? request.getParameter("nitprov") : "";
                        boolean pasa = true;
                        ArrayList<String> listacad = null;
                        try {
                            listacad = model.gestionConveniosSvc.datosConveniosSectores(nitAfil);
                        } catch (Exception e) {
                            pasa = false;
                            System.out.println("Error: " + e.toString());
                            e.printStackTrace();
                        }
                        String cadresp = "<table style='border-collapse:collapse; width:100%;' border='1' id='tablaconvs'>"
                                + "<tr class='subtitulo1'>"
                                + "<th>Convenio</th>"
                                + "<th>Sector</th>"
                                + "<th>Subsector</th>"
                                + "</tr>";
                        if (pasa == true && listacad.size() > 0) {
                            String[] splitcad = null;
                            for (int i = 0; i < listacad.size(); i++) {
                                splitcad = (listacad.get(i)).split(";_;");
                                cadresp += "<tr class='fila' style='cursor: pointer;' onclick='asignarValor3(\"" + splitcad[0] + "\",\"" + splitcad[1] + "\",\"" + splitcad[2] + "\",\"" + splitcad[3] + "\");cerrarDiv();'>"
                                        + "<td>" + model.gestionConveniosSvc.nombreConvenio(splitcad[1]) + "</td>"
                                        + "<td>" + model.gestionConveniosSvc.nombreSector(splitcad[2]) + "</td>"
                                        + "<td>" + model.gestionConveniosSvc.nombreSubSector(splitcad[2], splitcad[3]) + "</td>"
                                        + "</tr>";
                            }
                        } else {
                            cadresp += "<tr class='fila' style='cursor: pointer;' onclick='cerrarDiv();'><td colspan='3' align='center'>No se encontraron resultados</td></tr>";
                        }
                        cadresp += "</table>";
                        response.setContentType("text/plain; charset=utf-8");
                        response.setHeader("Cache-Control", "no-cache");
                        response.getWriter().println(cadresp);
                    }
                    if (tipo.equals("datosasign")) {
                        String nitafil = request.getParameter("nitprov") != null ? request.getParameter("nitprov") : "";
                        String codusu = request.getParameter("codusuario") != null ? request.getParameter("codusuario") : "";
                        String agregar = request.getParameter("agregar");
                        codusu = codusu.toUpperCase();
                        boolean pasa = true;
                        ArrayList<String> listacad = null;
                        try {
                            listacad = model.gestionConveniosSvc.asignacionesUsuarioAfil(codusu, nitafil);
                        } catch (Exception e) {
                            pasa = false;
                            System.out.println("error: " + e.toString());
                            e.printStackTrace();
                        }
                        String cadresp = "<table border='0' width='100%' id='tablaconvs1'><tr class='subtitulo1'>"
                                + "<td></td>"
                                + "<td>Convenio</td>"
                                + "<td>Sector</td>"
                                + "<td>Subsector</td>"
                                + "<td>Activo</td>"
                                + "</tr>";
                        if (pasa == true && listacad.size() > 0) {
                            String[] split = null;
                            for (int i = 0; i < listacad.size(); i++) {
                                split = (listacad.get(i)).split(";_;");
                                cadresp += "<tr class='fila'>"
                                        + "<td>"
                                        + "<input type='hidden' id='id_provc" + (i + 1) + "' name='id_provc" + (i + 1) + "' value='" + split[0] + "'>";
                                if (agregar.equals("S")) {

                                    cadresp +=
                                            "<img alt='+' src='" + request.getContextPath() + "/images/botones/iconos/mas.gif' width='15' height='15' style='cursor: pointer' onclick='agregarFila(\"tablaconvs1\")'>";
                                }
                                cadresp +=
                                        "</td>"
                                        + "<td>"
                                        + "<input type='text' name='conv" + (i + 1) + "' id='conv" + (i + 1) + "' value='" + split[1] + "' readonly='readonly'>";
                                if (agregar.equals("S")) {

                                    cadresp +=
                                            "<img alt='' src='" + request.getContextPath() + "/images/botones/iconos/lupa.gif' width='15' height='15' style='cursor: pointer' onclick='verConvs(" + (i + 1) + ");'>";
                                }
                                cadresp += "</td>"
                                        + "<td>"
                                        + "<input type='text' name='sect" + (i + 1) + "' id='sect" + (i + 1) + "' value='" + split[2] + "' readonly='readonly'>"
                                        + "</td>"
                                        + "<td>"
                                        + "<input type='text' name='subsect" + (i + 1) + "' id='subsect" + (i + 1) + "' value='" + split[3] + "' readonly='readonly'>"
                                        + "</td>"
                                        + "<td>"
                                        + "<input type='checkbox' name='activo" + (i + 1) + "' id='activo" + (i + 1) + "' value='S'" + (split[4].equals("S") ? " checked='checked'" : "");
                                if (!agregar.equals("S")) {
                                    cadresp += " disabled";
                                }
                                cadresp += "> </td>"
                                        + "</tr>";
                            }
                        } else {
                            cadresp += "<tr class='fila'>"
                                    + " <td>"
                                    + "<input type='hidden' id='id_provc1' name='id_provc1' value=''>";
                            if (agregar.equals("S")) {

                                cadresp +=
                                        "<img alt='+' src='" + request.getContextPath() + "/images/botones/iconos/mas.gif' width='15' height='15' style='cursor: pointer' onclick='agregarFila(\"tablaconvs\")'>";
                            }
                            cadresp += "</td>"
                                    + "<td>"
                                    + "<input type='text' name='conv1' id='conv1' value='' readonly='readonly'>";
                            if (agregar.equals("S")) {

                                cadresp +=
                                        "<img alt='' src='" + request.getContextPath() + "/images/botones/iconos/lupa.gif' width='15' height='15' style='cursor: pointer' onclick='verConvs(1);'>";
                            }
                            cadresp +=
                                    "</td>"
                                    + "<td>"
                                    + "<input type='text' name='sect1' id='sect1' value='' readonly='readonly'>"
                                    + "</td>"
                                    + "<td>"
                                    + "<input type='text' name='subsect1' id='subsect1' value='' readonly='readonly'>"
                                    + "</td>"
                                    + "<td>"
                                    + "<input type='checkbox' name='activo1' id='activo1' value='S' checked='checked'";
                            if (!agregar.equals("S")) {
                                cadresp += " disabled";
                            }
                            cadresp += ">" + "</td>"
                                    + "</tr></table>";
                        }
                        response.setContentType("text/plain; charset=utf-8");
                        response.setHeader("Cache-Control", "no-cache");
                        response.getWriter().println(cadresp);
                    }
                    if (tipo.equals("insertrel")) {
                        redirect = true;
                        next = "/jsp/fenalco/convenio/usuario_afiliado.jsp";
                        try {
                            String codusu = request.getParameter("codigousuario") != null ? request.getParameter("codigousuario") : "";
                            codusu = codusu.toUpperCase();
                            int filastabla = request.getParameter("filastabla") != null ? Integer.parseInt(request.getParameter("filastabla")) : 0;
                            if (filastabla > 0) {
                                String id_prov_convenio = "";
                                String activo = "";
                                ArrayList<String> listaconvs = new ArrayList<String>();
                                for (int i = 0; i < filastabla; i++) {
                                    id_prov_convenio = request.getParameter("id_provc" + (i + 1)) != null ? request.getParameter("id_provc" + (i + 1)) : "";
                                    activo = request.getParameter("activo" + (i + 1)) != null ? request.getParameter("activo" + (i + 1)) : "N";
                                    listaconvs.add(id_prov_convenio + ";_;" + activo);
                                }
                                model.gestionConveniosSvc.asignarUsuario(codusu, listaconvs, usuario.getLogin());
                            } else {
                                System.out.println("No se recibieron filas para procesar en GestionConveniosAction.java, tipo=insertrel ...");
                            }
                        } catch (Exception e) {
                            System.out.println("Error: " + e.toString());
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                if (tipo.equals("MODIFICAR")) {
                    redirect = true;
                    session.removeAttribute("convenio");
                    session.setAttribute("convenio", model.gestionConveniosSvc.buscar_convenio(usuario.getBd(), request.getParameter("id")));
                }
                if (tipo.equals("NUEVO")) {
                    redirect = true;
                    session.removeAttribute("convenio");
                }
            }
            if (redirect == true) {
                this.dispatchRequest(next);
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServletException(e.getMessage(), e);
        }
    }
    
    private Convenio loadConvenio(Usuario usuario) {
        Convenio cnv = new Convenio(load_cxc(usuario, request.getParameter("id_convenio")), 
	load_remesas(usuario, request.getParameter("id_convenio")), 
	load_comisiones(usuario, request.getParameter("id_convenio")), load_fiducia(usuario, request.getParameter("id_convenio")));
        cnv.setId_convenio(request.getParameter("id_convenio"));       
        cnv.setId_convenio(request.getParameter("id_convenio"));
        cnv.setNombre(request.getParameter("nombre"));
        cnv.setDescripcion(request.getParameter("descripcion"));
        cnv.setNit_convenio(request.getParameter("nit_convenio") != null ?request.getParameter("nit_convenio") :"");
        cnv.setNit_tercero(request.getParameter("nit_tercero") != null ?request.getParameter("nit_tercero"):"");
        cnv.setCuenta_interes(request.getParameter("cuenta_interes"));
        cnv.setCuenta_custodia(request.getParameter("cuenta_custodia") != null ?request.getParameter("cuenta_custodia"):"");
        cnv.setPrefijo_negocio(request.getParameter("prefijo_negocio"));
        cnv.setPrefijo_cxp(request.getParameter("prefijo_cxp"));
        cnv.setCuenta_cxp(request.getParameter("cuenta_cxp"));
        cnv.setHc_cxp(request.getParameter("hc_cxp"));
        cnv.setCuota_gmf(request.getParameter("cuota_gmf") != null ?request.getParameter("cuota_gmf").equals("") ? "0" : request.getParameter("cuota_gmf"):"0");
        cnv.setPrefijo_nc_gmf(request.getParameter("prefijo_gmf") != null ?request.getParameter("prefijo_gmf") :"");
        cnv.setCuenta_gmf(request.getParameter("cuenta_gmf") != null ?request.getParameter("cuenta_gmf") :"");
        cnv.setCuenta_gmf2(request.getParameter("cuenta_gmf2") != null ?request.getParameter("cuenta_gmf2"):"");
        cnv.setPrefijo_nc_aval(request.getParameter("prefijo_aval") != null ?request.getParameter("prefijo_aval"):"");
        cnv.setCuenta_aval(request.getParameter("cuenta_aval") != null ?request.getParameter("cuenta_aval"):"");
        cnv.setPrefijo_diferidos(request.getParameter("prefijo_diferidos") != null ?request.getParameter("prefijo_diferidos"):"");
        cnv.setCuenta_diferidos(request.getParameter("cuenta_diferidos") != null ?request.getParameter("cuenta_diferidos"):"");
        cnv.setHc_diferidos(request.getParameter("hc_diferidos") != null ?request.getParameter("hc_diferidos"):"");
        cnv.setCreation_user(usuario.getLogin());
        cnv.setUser_update(usuario.getLogin());
        cnv.setFactura_tercero(Boolean.parseBoolean(request.getParameter("factura_tercero") != null ?request.getParameter("factura_tercero"):"0"));
        cnv.setDescuenta_gmf((request.getParameter("descuenta_gmf") != null) ? true : false);
        cnv.setDescuenta_aval((request.getParameter("descuenta_aval") != null) ? true : false);
        cnv.setValor_custodia(Double.parseDouble((request.getParameter("valor_custodia") != null ?request.getParameter("valor_custodia"):"0").replace(",", "")));
        cnv.setTasa_interes(Double.parseDouble((request.getParameter("tasa_interes") != null ?request.getParameter("tasa_interes"):"0").replace(",", "")));
        cnv.setPrefijo_endoso(request.getParameter("prefijo_endoso") != null ?request.getParameter("prefijo_endoso"):"");
        cnv.setHc_endoso(request.getParameter("hc_endoso") != null ?request.getParameter("hc_endoso"):"");
        cnv.setPorc_gmf(Double.parseDouble((request.getParameter("porc_gravamen")!= null&&!request.getParameter("porc_gravamen").equals("") ?request.getParameter("porc_gravamen"):"0").replace(",", "")));//2010-09-27
        cnv.setPorc_gmf2(Double.parseDouble((request.getParameter("porc_gravamen2")!= null&&!request.getParameter("porc_gravamen").equals("") ?request.getParameter("porc_gravamen2"):"0").replace(",", "")));//2010-09-27
        
        cnv.setImpuesto(request.getParameter("impuesto")!= null ?request.getParameter("impuesto"):"");
        cnv.setCuenta_ajuste(request.getParameter("cuenta_ajuste")!= null ?request.getParameter("cuenta_ajuste"):"");
        cnv.setMediador_aval(Boolean.parseBoolean(request.getParameter("mediador_aval")!= null ?request.getParameter("mediador_aval"):""));
        cnv.setNit_mediador(request.getParameter("nit_mediador")!= null ?request.getParameter("nit_mediador"):"");
        cnv.setAval_tercero(Boolean.parseBoolean(request.getParameter("aval_tercero")!= null ?request.getParameter("aval_tercero"):""));

        cnv.setCentral(Boolean.parseBoolean(request.getParameter("central")!= null ?request.getParameter("central"):""));
        cnv.setCapacitacion(Boolean.parseBoolean(request.getParameter("capacitacion")!= null ?request.getParameter("capacitacion"):""));
        cnv.setSeguro(Boolean.parseBoolean(request.getParameter("seguro")!= null ?request.getParameter("seguro"):""));
        cnv.setNit_central(request.getParameter("nit_central") != null ? request.getParameter("nit_central") : "");
        cnv.setNit_capacitador(request.getParameter("nit_capacitador") != null ? request.getParameter("nit_capacitador") : "");
        cnv.setNit_asegurador(request.getParameter("nit_asegurador") != null ? request.getParameter("nit_asegurador") : "");
        cnv.setPrefijo_cxc_interes(request.getParameter("prefijo_cxc_interes") != null ? request.getParameter("prefijo_cxc_interes") : "");
        cnv.setPrefijo_cxp_central(request.getParameter("prefijo_cxp_central") != null ? request.getParameter("prefijo_cxp_central") : "");
        cnv.setCuenta_central(request.getParameter("cuenta_central") != null ? request.getParameter("cuenta_central") : "");
        cnv.setCuenta_com_central(request.getParameter("cuenta_com_central") != null ? request.getParameter("cuenta_com_central") : "");
        cnv.setPrefijo_cxc_cat(request.getParameter("prefijo_cxc_cat") != null ? request.getParameter("prefijo_cxc_cat") : "");
        cnv.setCuenta_cat(request.getParameter("cuenta_cat") != null ? request.getParameter("cuenta_cat") : "");
        cnv.setCuenta_capacitacion(request.getParameter("cuenta_capacitacion") != null ? request.getParameter("cuenta_capacitacion") : "");
        cnv.setCuenta_seguro(request.getParameter("cuenta_seguro") != null ? request.getParameter("cuenta_seguro") : "");
        cnv.setCuenta_com_seguro(request.getParameter("cuenta_com_seguro") != null ? request.getParameter("cuenta_com_seguro") : "");
        
        cnv.setValor_central(Double.parseDouble((request.getParameter("valor_central") != null ? request.getParameter("valor_central") : "0").replace(",", "")));
        cnv.setValor_com_central(Double.parseDouble((request.getParameter("valor_com_central") != null ? request.getParameter("valor_com_central") : "0").replace(",", "")));
        cnv.setPorcentaje_cat(Double.parseDouble((request.getParameter("porcentaje_cat") != null ? request.getParameter("porcentaje_cat") : "0").replace(",", "")));
        cnv.setValor_capacitacion(Double.parseDouble((request.getParameter("valor_capacitacion") != null ? request.getParameter("valor_capacitacion") : "0").replace(",", "")));
        cnv.setValor_seguro(Double.parseDouble((request.getParameter("valor_seguro") != null ? request.getParameter("valor_seguro") : "0").replace(",", "")));
        cnv.setPorcentaje_com_seguro(Double.parseDouble((request.getParameter("porcentaje_com_seguro") != null ? request.getParameter("porcentaje_com_seguro") : "0").replace(",", "")));
        cnv.setMonto_minimo(Double.parseDouble((request.getParameter("monto_minimo") != null ? request.getParameter("monto_minimo") : "0").replace(",", "")));
        cnv.setMonto_maximo(Double.parseDouble((request.getParameter("monto_maximo") != null ? request.getParameter("monto_maximo") : "0").replace(",", "")));
        cnv.setPlazo_maximo(request.getParameter("plazo_maximo") != null ? request.getParameter("plazo_maximo") : "0");
        cnv.setTipo(request.getParameter("tipoconv") != null ? request.getParameter("tipoconv") : "");
        cnv.setRedescuento(Boolean.parseBoolean(request.getParameter("redescuento")!= null ?request.getParameter("redescuento"):"true"));
        
        cnv.setAval_anombre(Boolean.parseBoolean(request.getParameter("aval_anombre")!= null ?request.getParameter("aval_anombre"):"false"));
        cnv.setCxp_avalista((request.getParameter("cxp_avalista") != null) ? true : false);
        cnv.setNit_anombre(request.getParameter("nit_anombre")!= null ?request.getParameter("nit_anombre"):"");
        cnv.setCctrl_db_cxc_aval(request.getParameter("cctrl_db_cxc_aval")!= null ?request.getParameter("cctrl_db_cxc_aval"):"");
        cnv.setCctrl_cr_cxc_aval(request.getParameter("cctrl_cr_cxc_aval")!= null ?request.getParameter("cctrl_cr_cxc_aval"):"");
        cnv.setCctrl_iva_cxc_aval(request.getParameter("cctrl_iva_cxc_aval")!= null ?request.getParameter("cctrl_iva_cxc_aval"):"");
        cnv.setPrefijo_cxp_avalista(request.getParameter("prefijo_cxp_avalista")!= null ?request.getParameter("prefijo_cxp_avalista"):"");
        cnv.setHc_cxp_avalista(request.getParameter("hc_cxp_avalista")!= null ?request.getParameter("hc_cxp_avalista"):"");
        cnv.setCuenta_cxp_avalista(request.getParameter("cuenta_cxp_avalista")!= null ?request.getParameter("cuenta_cxp_avalista"):"");
        
        cnv.setCuenta_cxc_aval(request.getParameter("cuenta_cxc_aval")!= null ?request.getParameter("cuenta_cxc_aval"):"");
        cnv.setPrefijo_cxc_aval(request.getParameter("prefijo_cxc_aval")!= null ?request.getParameter("prefijo_cxc_aval"):"");
        cnv.setHc_cxc_aval(request.getParameter("hc_cxc_aval")!= null ?request.getParameter("hc_cxc_aval"):"");
        cnv.setAgencia(request.getParameter("agencia")!= null ?request.getParameter("agencia"):"");
        cnv.setCat(Boolean.parseBoolean(request.getParameter("cat")!= null ?request.getParameter("cat"):"true"));
        cnv.setCuenta_cuota_administracion(request.getParameter("cuenta_administracion")!= null ?request.getParameter("cuenta_administracion"):"");
        cnv.setPrefijo_cuota_admin(request.getParameter("prefijo_cuota_admin")!= null ?request.getParameter("prefijo_cuota_admin"):"");
        cnv.setHc_cuota_admin(request.getParameter("hc_cuota_admin")!= null ?request.getParameter("hc_cuota_admin"):"");
        cnv.setCta_cuota_admin_diferido(request.getParameter("cta_cuota_admin_diferido")!= null ?request.getParameter("cta_cuota_admin_diferido"):"");
        return cnv;
    }

    private ArrayList<ConvenioComision> load_comisiones(Usuario usuario, String id_convenio) {
        ArrayList<ConvenioComision> cnv = new ArrayList<ConvenioComision>();
        ConvenioComision convenioComision = null;
        int n = request.getParameter("tamano_tabla_comisiones")!=null?Integer.parseInt(request.getParameter("tamano_tabla_comisiones")):0;
        for (int i = 0; i < n; i++) {
            try {
            if (!request.getParameter("nombre_tabla_comisiones_" + i).equals("")) {
                convenioComision = new ConvenioComision();
                convenioComision.setId_convenio(id_convenio);
                convenioComision.setId_comision(request.getParameter("id_tabla_comisiones_" + i));
                convenioComision.setNombre(request.getParameter("nombre_tabla_comisiones_" + i));
                convenioComision.setPorcentaje_comision(Double.parseDouble((request.getParameter("porcentaje_tabla_comisiones_" + i)).replace(",", "")));
                convenioComision.setCuenta_comision(request.getParameter("cuenta_tabla_comisiones_" + i));
                if (request.getParameter("rdoTerceroFactura") != null && request.getParameter("rdoTerceroFactura").equals(String.valueOf(i))) {
                    convenioComision.setComision_tercero(true);
                }
                convenioComision.setIndicador_contra((request.getParameter("chkInd_" + i) != null) ? true : false);
                convenioComision.setCuenta_contra(request.getParameter("txtContrapartida_" + i) != null ? request.getParameter("txtContrapartida_" + i) : "");
                convenioComision.setDstrct("FINV");
                convenioComision.setReg_status("");
                convenioComision.setCreation_user(usuario.getLogin());
                convenioComision.setUser_update(usuario.getLogin());
                cnv.add(convenioComision);
            }
            } catch(Exception exception) {
                //exception.printStackTrace();System.out.println("iteracion: "+i);
            }
        }

        return cnv;
    }

    private ArrayList<ConveniosRemesas> load_remesas(Usuario usuario, String id_convenio) {
        ArrayList<ConveniosRemesas> cnv = new ArrayList<ConveniosRemesas>();
        ConveniosRemesas conveniosRemesas = null;
        int n = request.getParameter("tamano_tabla_remesas")!=null?Integer.parseInt(request.getParameter("tamano_tabla_remesas")):0;
        for (int i = 0; i < n; i++) {
            try {
            if (!request.getParameter("ciudad_tabla_remesas_" + i).equals("")) {
                conveniosRemesas = new ConveniosRemesas();
                conveniosRemesas.setReg_status("");
                conveniosRemesas.setDstrct("FINV");
                conveniosRemesas.setId_convenio(id_convenio);
                conveniosRemesas.setId_remesa(request.getParameter("id_tabla_remesas_" + i));
                conveniosRemesas.setCiudad_sede(request.getParameter("ciudad_tabla_remesas_" + i));
                conveniosRemesas.setBanco_titulo(request.getParameter("bancoch_tabla_remesas_" + i));
                conveniosRemesas.setCiudad_titulo(request.getParameter("ciudadch_tabla_remesas_" + i));
                conveniosRemesas.setGenera_remesa(Boolean.parseBoolean(request.getParameter("remesa_tabla_remesas_" + i)));
                conveniosRemesas.setCuenta_remesa(request.getParameter("cuenta_tabla_remesas_" + i));
                conveniosRemesas.setCreation_user(usuario.getLogin());
                conveniosRemesas.setUser_update(usuario.getLogin());
                conveniosRemesas.setPorcentaje_remesa(Double.parseDouble((request.getParameter("porcentaje_tabla_remesas_" + i)).replace(",", "")));
                cnv.add(conveniosRemesas);
            }
            } catch(Exception exception) {
                //exception.printStackTrace();System.out.println("iteracion: "+i);
            }
        }

        return cnv;
    }

   private ArrayList<ConvenioCxc> load_cxc(Usuario usuario, String id_convenio) {
        ArrayList<ConvenioCxc> cnv = new ArrayList<ConvenioCxc>();
        ConvenioCxc convenioCxc = null;
        int n = request.getParameter("tamano_tabla_cxc")!=null?Integer.parseInt(request.getParameter("tamano_tabla_cxc")):0;
        for (int i = 0; i < n; i++) {
            try {
            if (!request.getParameter("tipo_documento_tabla_cxc_" + i).equals("")) {
                convenioCxc = new ConvenioCxc();
                convenioCxc.setReg_status("");
                convenioCxc.setDstrct(usuario.getDstrct());
                convenioCxc.setId_convenio(id_convenio);
                convenioCxc.setTitulo_valor(request.getParameter("tipo_documento_tabla_cxc_" + i));
                convenioCxc.setPrefijo_factura(request.getParameter("prefijo_tabla_cxc_" + i));
                convenioCxc.setCuenta_cxc(request.getParameter("cuenta_tabla_cxc_" + i));
                convenioCxc.setHc_cxc(request.getParameter("hc_tabla_cxc_" + i));
                convenioCxc.setCreation_user(usuario.getLogin());
                convenioCxc.setUser_update(usuario.getLogin());
                convenioCxc.setGenRemesa(request.getParameter("gen_remesa_" + i) != null ? Boolean.parseBoolean(request.getParameter("gen_remesa_" + i)) : false);//2010-09-27
                convenioCxc.setCuenta_prov_cxc(request.getParameter("cuenta_prov_cxc_" + i) != null ? request.getParameter("cuenta_prov_cxc_" + i) : "");
                convenioCxc.setCuenta_prov_cxp(request.getParameter("cuenta_prov_cxp_" + i) != null ? request.getParameter("cuenta_prov_cxp_" + i) : "");

                int m = request.getParameter("tamano_tabla_cxc_fiducia" + i)!=null?Integer.parseInt(request.getParameter("tamano_tabla_cxc_fiducia" + i)):Integer.parseInt("0");
                ArrayList<ConvenioCxcFiducias> cnvf = new ArrayList<ConvenioCxcFiducias>();
                for (int j = 0; j < m; j++) {
                    ConvenioCxcFiducias convenioCxcFid = new ConvenioCxcFiducias();
                    convenioCxcFid.setReg_status("");
                    convenioCxcFid.setDstrct(usuario.getDstrct());
                    convenioCxcFid.setNit_fiducia(request.getParameter("nit_fiducia_tabla_cxc_" + i + "" + j) != null ? request.getParameter("nit_fiducia_tabla_cxc_" + i + "" + j) : "");
                    convenioCxcFid.setPrefijo_cxc_fiducia(request.getParameter("prefijo_tabla_cxc_fiducia_" + i + "" + j) != null ? request.getParameter("prefijo_tabla_cxc_fiducia_" + i + "" + j) : "");
                    convenioCxcFid.setCuenta_cxc_fiducia(request.getParameter("cuenta_tabla_cxc_fiducia_" + i + "" + j) != null ? request.getParameter("cuenta_tabla_cxc_fiducia_" + i + "" + j) : "");
                    convenioCxcFid.setHc_cxc_fiducia(request.getParameter("hc_tabla_cxc_fiducia_" + i + "" + j) != null ? request.getParameter("hc_tabla_cxc_fiducia_" + i + "" + j) : "");
                    convenioCxcFid.setPrefijo_cxc_endoso(request.getParameter("prefijo_tabla_cxc_endoso_" + i + "" + j) != null ? request.getParameter("prefijo_tabla_cxc_endoso_" + i + "" + j) : "");
                    convenioCxcFid.setHc_cxc_endoso(request.getParameter("hc_tabla_cxc_endoso_" + i + "" + j) != null ? request.getParameter("hc_tabla_cxc_endoso_" + i + "" + j) : "");
                    convenioCxcFid.setCuenta_cxc_endoso(request.getParameter("cuenta_tabla_cxc_endoso_" + i + "" + j) != null ? request.getParameter("cuenta_tabla_cxc_endoso_" + i + "" + j) : "");
                    convenioCxcFid.setCreation_user(usuario.getLogin());
                    convenioCxcFid.setUser_update(usuario.getLogin());
                    cnvf.add(convenioCxcFid);
                }
                convenioCxc.setConvenioCxcFiducias(cnvf);
                cnv.add(convenioCxc);
            }
            } catch(Exception exception) {
                //exception.printStackTrace();System.out.println("iteracion: "+i);
            }
        }
        return cnv;
    }
 private ArrayList<ConvenioFiducias> load_fiducia(Usuario usuario, String id_convenio) {
        ArrayList<ConvenioFiducias> cnv = new ArrayList<ConvenioFiducias>();
        ConvenioFiducias convenioFid = null;
        int n = request.getParameter("tamano_tabla_fiducia")!=null?Integer.parseInt(request.getParameter("tamano_tabla_fiducia")):0;
        for (int i = 0; i < n; i++) {
            if (!request.getParameter("nit_fiducia" + i).equals("")) {
                convenioFid = new ConvenioFiducias();
                convenioFid.setReg_status("");
                convenioFid.setDstrct(usuario.getDstrct());
                convenioFid.setId_convenio(id_convenio);
                convenioFid.setNit_fiducia(request.getParameter("nit_fiducia"+ i) != null ? request.getParameter("nit_fiducia"+ i) : "");
                convenioFid.setPrefijo_dif_fiducia(request.getParameter("prefijo_dif_fiducia"+ i) != null ? request.getParameter("prefijo_dif_fiducia"+ i) : "");
                convenioFid.setCuenta_dif_fiducia(request.getParameter("cuenta_dif_fiducia"+ i) != null ? request.getParameter("cuenta_dif_fiducia"+ i) : "");
                convenioFid.setHc_dif_fiducia(request.getParameter("hc_dif_fiducia"+ i) != null ? request.getParameter("hc_dif_fiducia"+ i) : "");
                convenioFid.setPrefijo_end_fiducia(request.getParameter("prefijo_end_fiducia"+ i) != null ? request.getParameter("prefijo_end_fiducia"+ i) : "");
                convenioFid.setHc_end_fiducia(request.getParameter("hc_end_fiducia"+ i) != null ? request.getParameter("hc_end_fiducia"+ i) : "");
                convenioFid.setCreation_user(usuario.getLogin());
                convenioFid.setUser_update(usuario.getLogin());
                cnv.add(convenioFid);
            }
        }
        return cnv;
    }

    private void exportarExcel(ArrayList<Convenio> listadoconv, String userlogin) throws Exception {
        Convenio conv = null;
        ConvenioCxc convcxc = null;
        ConveniosRemesas convrem = null;
        ConvenioComision convcom = null;
        try {
            if (listadoconv != null && listadoconv.size() > 0) {
                String ruta = this.directorioArchivo(userlogin, "listado_convenios", "xls");
                ExcelApplication excel = this.instanciar("convenios");
                this.encabezadoExcel(excel, 1);
                int filaact = 2;
                for (int i = 0; i < listadoconv.size(); i++) {
                    conv = listadoconv.get(i);
                    //escribir ...
                    if (i > 0) {
                        this.encabezadoExcel(excel, filaact);
                        filaact++;
                    }
                    //...
                    int col = 0;
                    excel.setDataCell(filaact, col, conv.getNombre());
                    excel.setCellStyle(filaact, col++, excel.getStyle("estilo3"));
                    excel.setDataCell(filaact, col, conv.getDescripcion());
                    excel.setCellStyle(filaact, col++, excel.getStyle("estilo3"));
                    excel.setDataCell(filaact, col, conv.getNit_convenio());
                    excel.setCellStyle(filaact, col++, excel.getStyle("estilo3"));
                    excel.setDataCell(filaact, col, conv.isFactura_tercero() == true ? "SI" : "NO");
                    excel.setCellStyle(filaact, col++, excel.getStyle("estilo3"));
                    excel.setDataCell(filaact, col, conv.getNit_tercero());
                    excel.setCellStyle(filaact, col++, excel.getStyle("estilo3"));
                    excel.setDataCell(filaact, col, conv.getTasa_interes());
                    excel.setCellStyle(filaact, col++, excel.getStyle("estilo3"));
                    excel.setDataCell(filaact, col, conv.getCuenta_interes());
                    excel.setCellStyle(filaact, col++, excel.getStyle("estilo3"));
                    excel.setDataCell(filaact, col, conv.getValor_custodia());
                    excel.setCellStyle(filaact, col++, excel.getStyle("estilo3"));
                    excel.setDataCell(filaact, col, conv.getCuenta_custodia());
                    excel.setCellStyle(filaact, col++, excel.getStyle("estilo3"));
                    excel.setDataCell(filaact, col, conv.getPrefijo_negocio());
                    excel.setCellStyle(filaact, col++, excel.getStyle("estilo3"));
                    excel.setDataCell(filaact, col, conv.getImpuesto());
                    excel.setCellStyle(filaact, col++, excel.getStyle("estilo3"));
                    excel.setDataCell(filaact, col, conv.getCuenta_ajuste());
                    excel.setCellStyle(filaact, col++, excel.getStyle("estilo3"));
                    excel.setDataCell(filaact, col, conv.getPrefijo_cxp());
                    excel.setCellStyle(filaact, col++, excel.getStyle("estilo3"));
                    excel.setDataCell(filaact, col, conv.getHc_cxp());
                    excel.setCellStyle(filaact, col++, excel.getStyle("estilo3"));
                    excel.setDataCell(filaact, col, conv.getCuenta_cxp());
                    excel.setCellStyle(filaact, col++, excel.getStyle("estilo3"));
                    excel.setDataCell(filaact, col, conv.isDescuenta_gmf() == true ? "SI" : "NO");
                    excel.setCellStyle(filaact, col++, excel.getStyle("estilo3"));
                    excel.setDataCell(filaact, col, conv.getCuota_gmf());
                    excel.setCellStyle(filaact, col++, excel.getStyle("estilo3"));
                    excel.setDataCell(filaact, col, conv.getPrefijo_nc_gmf());
                    excel.setCellStyle(filaact, col++, excel.getStyle("estilo3"));
                    excel.setDataCell(filaact, col, conv.getCuenta_gmf());
                    excel.setCellStyle(filaact, col++, excel.getStyle("estilo3"));
                    excel.setDataCell(filaact, col, conv.isDescuenta_aval() == true ? "SI" : "NO");
                    excel.setCellStyle(filaact, col++, excel.getStyle("estilo3"));
                    excel.setDataCell(filaact, col, conv.getPrefijo_nc_aval());
                    excel.setCellStyle(filaact, col++, excel.getStyle("estilo3"));
                    excel.setDataCell(filaact, col, conv.getCuenta_aval());
                    excel.setCellStyle(filaact, col++, excel.getStyle("estilo3"));
                    excel.setDataCell(filaact, col, conv.getPrefijo_diferidos());
                    excel.setCellStyle(filaact, col++, excel.getStyle("estilo3"));
                    excel.setDataCell(filaact, col, conv.getHc_diferidos());
                    excel.setCellStyle(filaact, col++, excel.getStyle("estilo3"));
                    excel.setDataCell(filaact, col, conv.getCuenta_diferidos());
                    excel.setCellStyle(filaact, col++, excel.getStyle("estilo3"));
                    filaact++;
                    //recorrer ...
                    filaact++;
                    if ((conv.getConvenioCxc() != null) && (conv.getConvenioCxc()).size() > 0) {
                        excel.setDataCell(filaact, 1, "Facturas generadas");
                        excel.setCellStyle(filaact, 1, excel.getStyle("estilo2"));

                        filaact++;
                        excel.setDataCell(filaact, 1, "Tipo titulo valor");
                        excel.setCellStyle(filaact, 1, excel.getStyle("estilo2"));
                        excel.setDataCell(filaact, 2, "Prefijo factura cxc");
                        excel.setCellStyle(filaact, 2, excel.getStyle("estilo2"));
                        excel.setDataCell(filaact, 3, "Cuenta");
                        excel.setCellStyle(filaact, 3, excel.getStyle("estilo2"));
                        excel.setDataCell(filaact, 4, "HC");
                        excel.setCellStyle(filaact, 4, excel.getStyle("estilo2"));
                        filaact++;
                        for (int j = 0; j < (conv.getConvenioCxc()).size(); j++) {
                            convcxc = (conv.getConvenioCxc()).get(j);
                            //escribir..

                            excel.setDataCell(filaact, 1, convcxc.getTitulo_valor());
                            excel.setCellStyle(filaact, 1, excel.getStyle("estilo3"));
                            excel.setDataCell(filaact, 2, convcxc.getPrefijo_factura());
                            excel.setCellStyle(filaact, 2, excel.getStyle("estilo3"));
                            excel.setDataCell(filaact, 3, convcxc.getCuenta_cxc());
                            excel.setCellStyle(filaact, 3, excel.getStyle("estilo3"));
                            excel.setDataCell(filaact, 4, convcxc.getHc_cxc());
                            excel.setCellStyle(filaact, 4, excel.getStyle("estilo3"));
                            convcxc = null;
                            filaact++;
                        }
                        filaact++;
                    }
                    if ((conv.getConvenioComision() != null) && (conv.getConvenioComision()).size() > 0) {
                        excel.setDataCell(filaact, 1, "Comisiones");
                        excel.setCellStyle(filaact, 1, excel.getStyle("estilo2"));
                        filaact++;
                        excel.setDataCell(filaact, 1, "Nombre");
                        excel.setCellStyle(filaact, 1, excel.getStyle("estilo2"));
                        excel.setDataCell(filaact, 2, "Porcentaje");
                        excel.setCellStyle(filaact, 2, excel.getStyle("estilo2"));
                        excel.setDataCell(filaact, 3, "Cuenta");
                        excel.setCellStyle(filaact, 3, excel.getStyle("estilo2"));
                        excel.setDataCell(filaact, 4, "Tercero que factura");
                        excel.setCellStyle(filaact, 4, excel.getStyle("estilo2"));
                        excel.setDataCell(filaact, 5, "Indicador contrapartida");
                        excel.setCellStyle(filaact, 5, excel.getStyle("estilo2"));
                        excel.setDataCell(filaact, 6, "Cuenta contrapartida");
                        excel.setCellStyle(filaact, 6, excel.getStyle("estilo2"));
                        filaact++;
                        for (int j = 0; j < (conv.getConvenioComision()).size(); j++) {
                            convcom = (conv.getConvenioComision()).get(j);
                            //escribir...

                            excel.setDataCell(filaact, 1, convcom.getNombre());
                            excel.setCellStyle(filaact, 1, excel.getStyle("estilo3"));
                            excel.setDataCell(filaact, 2, convcom.getPorcentaje_comision());
                            excel.setCellStyle(filaact, 2, excel.getStyle("estilo3"));
                            excel.setDataCell(filaact, 3, convcom.getCuenta_comision());
                            excel.setCellStyle(filaact, 3, excel.getStyle("estilo3"));
                            excel.setDataCell(filaact, 4, convcom.isComision_tercero() == true ? "SI" : "NO");
                            excel.setCellStyle(filaact, 4, excel.getStyle("estilo3"));
                            excel.setDataCell(filaact, 5, convcom.isIndicador_contra() == true ? "SI" : "NO");
                            excel.setCellStyle(filaact, 5, excel.getStyle("estilo3"));
                            excel.setDataCell(filaact, 6, convcom.getCuenta_contra());
                            excel.setCellStyle(filaact, 6, excel.getStyle("estilo3"));
                            convcom = null;
                            filaact++;
                        }
                        filaact++;
                    }
                    if ((conv.getConveniosRemesas() != null) && (conv.getConveniosRemesas()).size() > 0) {
                        excel.setDataCell(filaact, 1, "Remesas");
                        excel.setCellStyle(filaact, 1, excel.getStyle("estilo2"));
                        filaact++;
                        excel.setDataCell(filaact, 1, "Ciudad sede");
                        excel.setCellStyle(filaact, 1, excel.getStyle("estilo2"));
                        excel.setDataCell(filaact, 2, "Banco titulo");
                        excel.setCellStyle(filaact, 2, excel.getStyle("estilo2"));
                        excel.setDataCell(filaact, 3, "Ciudad titulo");
                        excel.setCellStyle(filaact, 3, excel.getStyle("estilo2"));
                        excel.setDataCell(filaact, 4, "Cuenta");
                        excel.setCellStyle(filaact, 4, excel.getStyle("estilo2"));
                        excel.setDataCell(filaact, 5, "Porcentaje");
                        excel.setCellStyle(filaact, 5, excel.getStyle("estilo2"));
                        excel.setDataCell(filaact, 6, "Remesa");
                        excel.setCellStyle(filaact, 6, excel.getStyle("estilo2"));
                        filaact++;
                        for (int j = 0; j < (conv.getConveniosRemesas()).size(); j++) {
                            convrem = (conv.getConveniosRemesas()).get(j);
                            //escribir ...

                            excel.setDataCell(filaact, 1, convrem.getCiudad_sede());
                            excel.setCellStyle(filaact, 1, excel.getStyle("estilo3"));
                            excel.setDataCell(filaact, 2, convrem.getBanco_titulo());
                            excel.setCellStyle(filaact, 2, excel.getStyle("estilo3"));
                            excel.setDataCell(filaact, 3, convrem.getCiudad_titulo());
                            excel.setCellStyle(filaact, 3, excel.getStyle("estilo3"));
                            excel.setDataCell(filaact, 4, convrem.getCuenta_remesa());
                            excel.setCellStyle(filaact, 4, excel.getStyle("estilo3"));
                            excel.setDataCell(filaact, 5, convrem.getPorcentaje_remesa());
                            excel.setCellStyle(filaact, 5, excel.getStyle("estilo3"));
                            excel.setDataCell(filaact, 6, convrem.isGenera_remesa() == true ? "SI" : "NO");
                            excel.setCellStyle(filaact, 6, excel.getStyle("estilo3"));
                            convrem = null;
                            filaact++;
                        }
                        filaact++;
                    }
                    //hacer espacio para el siguiente ...
                    //filaact++;
                    conv = null;
                    filaact++;
                }
                excel.saveToFile(ruta);
            }
        } catch (Exception e) {
            throw new Exception("Error al general el excel de convenios: " + e.toString());
        }
    }

    /**
     * Crea un objeto tipo ExcelApplication
     * @param descripcion Nombre de la hoja principal del libro
     * @return Objeto ExcelApplication creado
     * @throws Exception Cuando hay error
     */
    private ExcelApplication instanciar(String descripcion) throws Exception {
        ExcelApplication excel = new ExcelApplication();
        try {
            excel.createSheet(descripcion);
            excel.createFont("Titulo", "Arial", (short) 1, true, (short) 12);
            excel.createFont("Subtitulo", "Verdana", (short) 1, true, (short) 10);
            excel.createFont("Contenido", "Verdana", (short) 0, false, (short) 10);

            excel.createColor((short) 11, (byte) 255, (byte) 255, (byte) 255);//Blanco
            excel.createColor((short) 9, (byte) 26, (byte) 126, (byte) 0);//Verde dark
            excel.createColor((short) 10, (byte) 171, (byte) 243, (byte) 169);//Verde light

            excel.createStyle("estilo1", excel.getFont("Titulo"), (short) 10, true, "@");
            excel.createStyle("estilo2", excel.getFont("Subtitulo"), (short) 9, true, "@");
            excel.createStyle("estilo3", excel.getFont("Contenido"), (short) 11, true, "@");

        } catch (Exception e) {
            throw new Exception("Error al instanciar el objeto: " + e.toString());
        }
        return excel;
    }

    /**
     * Genera la ruta en que se guardara el archivo
     * @param user usuario que genera el archivo
     * @param cons consecutivo de la cotizacion
     * @param extension La extension del archivo
     * @return String con la ruta en la que queda el archivo
     * @throws Exception cuando ocurre algun error
     */
    private String directorioArchivo(String user, String cons, String extension) throws Exception {
        String ruta = "";
        try {
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            ruta = rb.getString("ruta") + "/exportar/migracion/" + user;
            SimpleDateFormat fmt = new SimpleDateFormat("yyyMMdd_hhmmss");
            File archivo = new File(ruta);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }
            ruta = ruta + "/" + cons + "_" + fmt.format(new Date()) + "." + extension;
        } catch (Exception e) {
            throw new Exception("Error al generar el directorio: " + e.toString());
        }
        return ruta;
    }

    public void encabezadoExcel(ExcelApplication excel, int filaact) {
        try {
            if (filaact == 1) {
                excel.setDataCell(0, 0, "Convenios");
                excel.setCellStyle(0, 0, excel.getStyle("estilo2"));
            }
            int col = 0;
            excel.setDataCell(filaact, col, "Nombre");
            excel.setCellStyle(filaact, col++, excel.getStyle("estilo2"));
            excel.setDataCell(filaact, col, "Descripcion");
            excel.setCellStyle(filaact, col++, excel.getStyle("estilo2"));
            excel.setDataCell(filaact, col, "Tercero convenio");
            excel.setCellStyle(filaact, col++, excel.getStyle("estilo2"));
            excel.setDataCell(filaact, col, "Factura a traves de terceros?");
            excel.setCellStyle(filaact, col++, excel.getStyle("estilo2"));
            excel.setDataCell(filaact, col, "Tercero que factura");
            excel.setCellStyle(filaact, col++, excel.getStyle("estilo2"));
            excel.setDataCell(filaact, col, "Tasa interes");
            excel.setCellStyle(filaact, col++, excel.getStyle("estilo2"));
            excel.setDataCell(filaact, col, "Cuenta");
            excel.setCellStyle(filaact, col++, excel.getStyle("estilo2"));
            excel.setDataCell(filaact, col, "Valor custodia");
            excel.setCellStyle(filaact, col++, excel.getStyle("estilo2"));
            excel.setDataCell(filaact, col, "Cuenta");
            excel.setCellStyle(filaact, col++, excel.getStyle("estilo2"));
            excel.setDataCell(filaact, col, "Prefijo negocio");
            excel.setCellStyle(filaact, col++, excel.getStyle("estilo2"));
            excel.setDataCell(filaact, col, "Impuesto");
            excel.setCellStyle(filaact, col++, excel.getStyle("estilo2"));
            excel.setDataCell(filaact, col, "Cuenta ajuste");
            excel.setCellStyle(filaact, col++, excel.getStyle("estilo2"));
            excel.setDataCell(filaact, col, "Prefijo facturas cxp");
            excel.setCellStyle(filaact, col++, excel.getStyle("estilo2"));
            excel.setDataCell(filaact, col, "HC");
            excel.setCellStyle(filaact, col++, excel.getStyle("estilo2"));
            excel.setDataCell(filaact, col, "Cuenta");
            excel.setCellStyle(filaact, col++, excel.getStyle("estilo2"));
            excel.setDataCell(filaact, col, "Descuenta gravamen?");
            excel.setCellStyle(filaact, col++, excel.getStyle("estilo2"));
            excel.setDataCell(filaact, col, "Valor");
            excel.setCellStyle(filaact, col++, excel.getStyle("estilo2"));
            excel.setDataCell(filaact, col, "Prefijo");
            excel.setCellStyle(filaact, col++, excel.getStyle("estilo2"));
            excel.setDataCell(filaact, col, "Cuenta");
            excel.setCellStyle(filaact, col++, excel.getStyle("estilo2"));
            excel.setDataCell(filaact, col, "Prefijo descuento de aval?");
            excel.setCellStyle(filaact, col++, excel.getStyle("estilo2"));
            excel.setDataCell(filaact, col, "Prefijo");
            excel.setCellStyle(filaact, col++, excel.getStyle("estilo2"));
            excel.setDataCell(filaact, col, "Cuenta");
            excel.setCellStyle(filaact, col++, excel.getStyle("estilo2"));
            excel.setDataCell(filaact, col, "Prefijo ingresos diferidos");
            excel.setCellStyle(filaact, col++, excel.getStyle("estilo2"));
            excel.setDataCell(filaact, col, "HC");
            excel.setCellStyle(filaact, col++, excel.getStyle("estilo2"));
            excel.setDataCell(filaact, col, "Cuenta");
            excel.setCellStyle(filaact, col++, excel.getStyle("estilo2"));
        } catch (Exception e) {
            System.out.println("error: " + e.toString());
        }
    }
}
