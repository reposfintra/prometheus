/*************************************************************************
 * Nombre:        ConductorBuscarAction.java
 * Descripci�n:   Clase Action para buscar Conductor
 * Autor:         Ing. David Velasquz
 * Fecha:         21 de noviembre de 2004, 02:40 PM
 * Versi�n        1.0
 * Coyright:      Transportes Sanchez Polo S.A.
 *************************************************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  David
 */
public class ConductorBuscarAction extends Action {
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = (String) session.getAttribute("Distrito");
        
        List lista = null;
        String condImg = "";
        Imagen img = null;
        String huella1 ="none.jpg";
        String huella2 ="none.jpg";
        String firma ="none.jpg";
        
        String cedula = request.getParameter("identificacion");
        try{
            model.conductorService.buscar_ConductorCGA(cedula);
            model.identidadService.buscarIdentidad(cedula,"");
            
            //Imagen Conductor
            model.ImagenSvc.searchImagen("1","1","1",null,null,null,"003", "032",
            cedula,null,null,null,null,usuario.getLogin());
            
            lista = model.ImagenSvc.getImagenes();
            if(lista!=null){
                img = (Imagen) lista.get(0);
                condImg = usuario.getLogin()+"/"+img.getNameImagen();
            }
            Conductor cond = model.conductorService.obtConductor();
            
            if(cond != null){
                //Busca la imagen de la Huella 1
                model.ImagenSvc.searchImagen("1","1","1",null,null,null,"015", cond.getHuella_der(),
                cond.getCedula(),null,null,null,null,usuario.getLogin());
                lista = model.ImagenSvc.getImagenes();
                if(lista!=null){
                    img = (Imagen) lista.get(0);
                    huella1 = usuario.getLogin()+"/"+img.getNameImagen();
                    //buscar la decripcion de la Huella 1
                    model.documentoSvc.obtenerDocumento(distrito, cond.getHuella_der());
                    Documento doc = model.documentoSvc.getDocumento();
                    if ( doc != null ){
                        cond.setDesHuella1(doc.getC_document_name());
                    }
                }
                //Busca la imagen de la Huella 2
                model.ImagenSvc.searchImagen("1","1","1",null,null,null,"016", cond.getHuella_izq(),
                cond.getCedula(),null,null,null,null,usuario.getLogin());
                lista = model.ImagenSvc.getImagenes();
                if(lista!=null){
                    img = (Imagen) lista.get(0);
                    huella2 = usuario.getLogin()+"/"+img.getNameImagen();
                    //buscar la decripcion de la Huella 1
                    model.documentoSvc.obtenerDocumento(distrito, cond.getHuella_izq());
                    Documento doc = model.documentoSvc.getDocumento();
                    if ( doc != null ){
                        cond.setDesHuella2(doc.getC_document_name());
                    }
                }
                //Busca la Imagen de la firma
                model.ImagenSvc.searchImagen("1","1","1",null,null,null,"003", "039",
                cond.getCedula(),null,null,null,null,usuario.getLogin());
                lista = model.ImagenSvc.getImagenes();
                if(lista!=null){
                    img = (Imagen) lista.get(0);
                    firma = usuario.getLogin()+"/"+img.getNameImagen();
                }
            }
            
        }
        catch(Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        
        next ="/jsp/trafico/conductor/ConsultaConductor.jsp?cond="+condImg+"&huella="+huella1+"&huella2="+huella2+"&firma="+firma;
        this.dispatchRequest(next);
    }
    
}
