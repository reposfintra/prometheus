package com.tsp.operation.controller;

import com.tsp.operation.model.*;
import java.io.*;
import java.util.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;

public class MenuConsultaOCAction extends Action{
    
     public void run() throws ServletException, InformationException {
        try{
            
        // HttpSession session = request.getSession();
        // Usuario usuario = (Usuario) session.getAttribute("Usuario");
        // String login=usuario.getLogin();
        // if(!login.equals("") ){
      
            String Opcion=request.getParameter("Opcion");
        // LLamamos la pagina JSP
           final String next = "/ConsultaOC.jsp?Opcion="+Opcion;
           RequestDispatcher rd = application.getRequestDispatcher(next);
           if(rd == null)
              throw new Exception("No se pudo encontrar "+ next);
           rd.forward(request, response); 
           
         //}
        }
        catch(Exception e){
          throw new InformationException(e.getMessage());
        }
     }// end run
    
}// end accion
