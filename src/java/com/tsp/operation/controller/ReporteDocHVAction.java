/****
 *  Nombre Clase             ReporteDocHVAction.java
 *  Descripci�n              Genera el reporte de los documentos de hoja de vida
 *  Autor                    Ing.  Diogenes Bastidas M
 *  Fecha                    18 de septiembre de 2006, 03:12 PM
 *  Versi�n                  1.0
 *  Copyright                 Transportes Sanchez Polo S.A.
 ****/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;


public class ReporteDocHVAction extends Action{ 
    
   public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String fechaI = request.getParameter("FechaI");
        String fechaF = request.getParameter("FechaF");
        String agencia = ( request.getParameter("agencia").equals("") ) ? "%":request.getParameter("agencia");
        //Info del usuario
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String next = "/jsp/hvida/reportes/ReporteDocumentosHV.jsp?msg=El Proceso  ha iniciado,  verificar en el log de proceso";                
        ////System.out.println("agencia = "+agencia+"  FechaI = "+fechaI+" FechaF = "+fechaF);
        try{
            HReporteDocumentosHVXLS hilo = new HReporteDocumentosHVXLS();
            hilo.start(fechaI, fechaF, usuario.getLogin(), agencia, usuario.getDstrct());
        }catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
