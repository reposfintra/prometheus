/***************************************
* Nombre Clase ............. GenerarOPAction.java
* Descripci�n  .. . . . . .  Permite Generar los Extractos de pagos en una determinada fecha
* Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
* Fecha . . . . . . . . . .  24/10/2005
* versi�n . . . . . . . . .  1.0
* Copyright ...Transportes Sanchez Polo S.A.
*******************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import com.tsp.exceptions.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.threads.*;



public class GenerarExtractosAction extends Action{
    
  
    public void run() throws ServletException, InformationException {
        
         try{
             
            HttpSession session   = request.getSession();
            Usuario usuario       = (Usuario) session.getAttribute("Usuario");
             
            String msjProcess     = "El proceso de Generaci�n de la Corrida se ha iniciado....";
            String next           = "/jsp/cxpagar/extractos/FiltrosCorridas.jsp?msj=";
            String comentario     = "";
            String evento         = request.getParameter("evento");
            
             
            model.ExtractosSvc.setUsuario( usuario );
            
            if(evento!=null){
                
                
                if( ! model.ExtractosSvc.isActivoProcesos() ){
                
                          // TIPO DE FILTROS PARA GENERAR LA CORRIDA
                          if(evento.equals("FILTROS")){
                                next   = "/jsp/cxpagar/extractos/Generacion.jsp?msj=";
                                model.ExtractosSvc.reset();
                                String  ckDistrito       = "";  //Siempre activo //request.getParameter("ckDistrito");
                                String  ckBanco          = request.getParameter("ckBancos");
                                String  ckProveedores    = request.getParameter("ckProveedor");
                                String  ckFecha          = request.getParameter("ckFechas");
                                String  filtroProveedor  = request.getParameter("TFP");
                                String  ckViaje          = request.getParameter("ckViaje");
                                String  ckPlacas         = request.getParameter("ckPlacas");
                                

                                model.ExtractosSvc.formarFiltros(ckDistrito, ckBanco, ckProveedores,  ckFecha,  filtroProveedor, ckViaje,  ckPlacas );
                          }                
                  
                
                      // DATOS DE LOS FILTROS SELECCIONADOS                  
                         if(evento.equals("DATOSFILTROS")){
                             
                               // Activamos proceso:
                                  model.ExtractosSvc.activarProcesos();
                             
                               // Capturamos valores del request:
                                  String     distrito         =  request.getParameter      ("distrito");
                                  String[]   bancos           =  request.getParameterValues("bancosFiltro");
                                  String[]   sucursales       =  request.getParameterValues("sucursalesFiltro");
                                  String[]   proveedores      =  request.getParameterValues("proveedoresFiltro"); 
                                  String[]   placas           =  request.getParameterValues("placas");  
                                  String     fechaIni         =  request.getParameter      ("fechaIni");
                                  String     fechaFin         =  request.getParameter      ("fechaFin"); 
                                  String     fechaIniViaje    =  request.getParameter      ("fechaIniViaje");
                                  String     fechaFinViaje    =  request.getParameter      ("fechaFinViaje");                                   
                                  String     tipoViajes       =  request.getParameter      ("tipoViajes"); 
                                  String     tpago            =  request.getParameter      ("tpago"); 
                                  String     corrida          =  request.getParameter      ("corrida");
                                             corrida          =  (corrida==null)?"":corrida.trim();   
                                  boolean    cheque_cero      =  (request.getParameter("cheque_cero")!=null)?true:false;
                                             
                             //   Control validaci�n de corrida         
                                  int       swError           =  0;           
                                             
                                             
                             //   Validamos la corrida a la cual se desea incluir los nuevos valores:     
                                  if( !corrida.equals("")  ){
                                       String msj =   model.ExtractosSvc.existeCorrida(distrito, corrida, usuario.getLogin() );
                                       if( !msj.equals("") ){
                                            msjProcess = msj;
                                            swError    = 1;    
                                       }
                                  }
                                  
                                  
                                  
                                  
                                  if(  swError == 0 ) {
                                  
                                          String INICIO       = " 00:00:00";
                                          String FINAL        = " 23:59:59";   
                                          String INICIO_VIAJE = ":00";
                                          String FINAL_VIAJE  = ":00"; 

                                          if( fechaIni!=null)        fechaIni      += INICIO;
                                          if( fechaFin!=null)        fechaFin      += FINAL;                                  
                                          if( fechaIniViaje!=null)   fechaIniViaje += INICIO_VIAJE;
                                          if( fechaFinViaje!=null)   fechaFinViaje += FINAL_VIAJE;

                                      // Guardamos datos de los filtros:
                                          model.ExtractosSvc.setDistritoCorrida     ( distrito      );
                                          model.ExtractosSvc.setBancosCorrida       ( bancos        );
                                          model.ExtractosSvc.setSucursalesCorrida   ( sucursales    );
                                          model.ExtractosSvc.setProveedoresCorrida  ( proveedores   );
                                          model.ExtractosSvc.setPlacasFiltro        ( placas        );
                                          model.ExtractosSvc.setFechaIniCorrida     ( fechaIni      );
                                          model.ExtractosSvc.setFechaFinCorrida     ( fechaFin      );
                                          model.ExtractosSvc.setFechaIniViaje       ( fechaIniViaje );
                                          model.ExtractosSvc.setFechaFinViaje       ( fechaFinViaje );
                                          model.ExtractosSvc.setTipoViaje           ( tipoViajes    );
                                          model.ExtractosSvc.setTipoPago            ( tpago         );

                                                        
                                     //   Proceso:
                                          HCorridas  hilo = new HCorridas();
                                          hilo.start(model, usuario, corrida, cheque_cero );  
                                          
                                        
                                          
                                  }else
                                      model.ExtractosSvc.desactivarProcesos();
                                  
                                  
                               // Mensaje al usuario.  
                                  comentario = msjProcess;
                                                                  
                         }
                          
                          
                          
                          
                      // ESCOJER SUCURSALES BANCOS SELECCIONADOS                  
                         if(evento.equals("SUCURSALES")){
                              String[]   bancos       =  request.getParameterValues("bancosFiltro");
                              model.ExtractosSvc.searchSucursales(bancos);
                              next ="/jsp/cxpagar/extractos/ListSucursales.jsp?opcion=SUCURSALES";
                         }
                           
                          
                          
                     // ESCOJER PLACAS DE LOS NIT SELECCIONADOS                 
                         if(evento.equals("PLACAS")){
                              String[]   nits       =  request.getParameterValues("proveedoresFiltro");
                              model.ExtractosSvc.searchPlacas( nits );                            
                              next ="/jsp/cxpagar/extractos/ListSucursales.jsp?opcion=PLACAS";
                         }
                          
                          
                          
                      // VALIDAMOS EXISTENCIA DE LOS NIT
                         if(evento.equals("VALIDARNIT")){
                              String  nit     =  request.getParameter("nit");
                              String  nombre  =  model.ExtractosSvc.searchName(nit);
                              if( nombre==null)
                                  comentario = "El nit " + nit + " no existe, o no se encuentra registrado en proveedor o nit"  ;
                              next ="/jsp/cxpagar/extractos/Puente.jsp?select=proveedoresFiltro&ele=busquedaNIT&evento="+  evento +"&nit="+ nit +"&nombre="+ nombre +"&msj=";
                         }
                          
                          
                          
                      // VALIDAMOS EXISTENCIA DE LA PLACA   
                         if(evento.equals("VALIDARPLACA")){
                              String  placa       =  request.getParameter("placa");
                              String  msj         =  model.ExtractosSvc.searchPlaca( placa );
                              next  ="/jsp/cxpagar/extractos/Puente.jsp?select=placas&ele=busquedaPLACA&evento="+  evento +"&placa="+ placa +"&msj="+  msj;
                         }
                          
                          
                          
                      // INFO CORRIDA   
                         if(evento.equals("INFOCORRIDA")){
                              Hashtable  info  = null;
                              String     dis   =  request.getParameter("distrito");
                              String     run   =  request.getParameter("corrida");
                              info = model.ExtractosSvc.infoCorrida(dis, run );                              
                              session.setAttribute("infocorrida", info );
                              next  ="/jsp/cxpagar/extractos/Info.jsp";
                         }
                          
                          
                          
                }
                else{
                     comentario="Se esta generando la corrida anterior, por favor intente mas tarde....";
                }
                
            }
            
            
            next += comentario;
            
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new Exception("No se pudo encontrar "+ next);
            rd.forward(request, response);            
        } catch (Exception e){
            e.printStackTrace();
             throw new ServletException(e.getMessage());
        }

        
    }
    
    
    
    
    
    
}
