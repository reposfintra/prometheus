/*
 * ProductoAnularAction.java
 *
 * Created on 17 de octubre de 2005, 11:49 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;

/**
 *
 * @author  Jose
 */
public class ProductoAnularAction extends Action{
    
    /** Creates a new instance of ProductoAnularAction */
    public ProductoAnularAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
    String next="/jsp/trafico/mensaje/MsgAnulado.jsp";
        HttpSession session = request.getSession();
        String codigo = request.getParameter("c_codigo");
        String cliente = request.getParameter ("codcli");
        Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
        String distrito = (String) session.getAttribute ("Distrito");
        try{
            Producto producto = new Producto();
            producto.setCliente(cliente);
            producto.setCodigo(codigo);
            producto.setDistrito (distrito);
            producto.setUsuario_modificacion(usuario.getLogin().toUpperCase());
            model.productoService.anularProducto(producto);
            request.setAttribute("msg","Producto Anulado");
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
