/**************************************************************************
 * Nombre clase: ReporteDrummondAction.java                                *
 * Descripci�n: Clase Recoge los parametros de la jsp y se los envia a el  *
 * hilo HReporteDrummond.java                                              *
 * Autor: Ing. Ivan DArio Gomez Vanegas                                    *
 * Fecha: Created on 1 de octubre de 2005, 08:20 AM                        *
 * Versi�n: Java 1.0                                                       *
 * Copyright: Fintravalores S.A. S.A.                                 *
 ***************************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.threads.*;
import com.tsp.operation.model.DAOS.*;

/**
 *
 * @author Igomez
 */
public class ReporteBancoTransferenciaAction extends Action{
    
    public ReporteBancoTransferenciaAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        String next="";
       /* HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");*/
        next = "/jsp/masivo/reportes/reporteBancosTransferencias.jsp";
        
        try{
            
            model.ReporteBTSvc.buscarNit();
            List listaNit = model.ReporteBTSvc.getListaNit();
            List listaReporte = new LinkedList();
            if(listaNit != null){
                for(int i=0; i< listaNit.size();i++){
                    Hashtable cl= (Hashtable)listaNit.get(i);
                    String nit      = (String) cl.get("nit");
                    String nombre   = (String) cl.get("nombre");
                    
                    List Planillas = model.ReporteBTSvc.buscarPlanillas(nit);
                    if(Planillas != null){
                        int cont = 0;//Pra contar el numero de planillas con valos mayor a cero
                        String listaOC = "";
                        for(int j=0; j< Planillas.size();j++){
                            String oc = (String) Planillas.get(j);
                            model.LiqOCSvc.liquidarOC(oc);
                            Liquidacion liq = model.LiqOCSvc.getLiquidacion();
                            List        facturas    =  liq.getFacturas();
                           
                         //   for(int k=0;k<facturas.size();k++){
				OP    op  = (OP) facturas.get(0);
                                 double saldo = op.getVlrNeto();//liq.getSaldo();    op.getVlrNeto();
                           // }
                            /*LiquidarPlanilla  liq = new LiquidarPlanilla();
                            liq.Liquidar("FINV",oc,"MPE" );*/
                          
                            
                            
                            if(saldo >0){
                                listaOC += "&planillaNO=" + oc;
                               cont ++;     
                            }
                        }
                        if(cont > 0){
                            Hashtable fila = new Hashtable();
                            fila.put("nit",    nit);
                            fila.put("nombre", nombre);
                            fila.put("totaloc",String.valueOf(cont));
                            fila.put("ocs",    listaOC);
                            listaReporte.add(fila);                            
                        }
                    }
                    
                }
            }else{
                next +="?msg = No se encontraron datos.. ";
            }
            
            model.ReporteBTSvc.setListaReporte(listaReporte);
            
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new ServletException("Error en ReporteBancoTransferenciaAction .....\n"+ex.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
}

