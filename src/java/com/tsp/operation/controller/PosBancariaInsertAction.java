/********************************************************************
 *      Nombre Clase.................   PosBancariaRefreshAction.java
 *      Descripci�n..................   Inserta un registro en el archivo posicion_bancaria
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   19.01.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *      modificado...................   egonzalez2014
 *******************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
/**
 *
 * @author  Andres
 */
public class PosBancariaInsertAction extends Action{
       
    /** Creates a new instance of PosBancariaRefreshAction */
    public PosBancariaInsertAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/jsp/cxpagar/posbancaria/PosBancariaInsert.jsp?msg=Proceso realizado existosamente.";
        
        String[] banco = request.getParameterValues("bancos");
        String[] sucursal = request.getParameterValues("sucursal");
        String[] saldoi = request.getParameterValues("campo1");
        String[] saldoa = request.getParameterValues("saldo_ant");
        String[] anticipos = request.getParameterValues("campo3");
        String[] proveedores = request.getParameterValues("campo4");
        String[] cupo = request.getParameterValues("campo5");
        String[] agency_id = request.getParameterValues("agency_id");
        String[] nsaldo = request.getParameterValues("nuevo_saldo");
        
        String agencia = request.getParameter("agencia");
        String fecha = request.getParameter("fecha");
        
        /*request.removeAttribute("agencia");
        request.removeAttribute("banco");
        request.setAttribute("agencia", "-");
        request.setAttribute("banco", "-");*/
        
        
        HttpSession session = request.getSession();
        Usuario user = (Usuario) session.getAttribute("Usuario");
        
        /*////System.out.println("................... bancos: " + banco.length);
        ////System.out.println("................... sucursal: " + sucursal.length);
        ////System.out.println("................... saldoi: " + saldoi.length);
        ////System.out.println("................... pendiente: " + pendiente.length);
        ////System.out.println("................... chequesg: " + chequesg.length);
        ////System.out.println("................... chequesn: " + chequesn.length);
        ////System.out.println("................... cupo: " + cupo.length);        
        ////System.out.println("................... agencias: " + agency_id.length);
        
        ////System.out.println("................... agencia received: " + agencia);*/
        PosBancaria posb = new PosBancaria();
        posb.setCreation_user(user.getLogin());
        posb.setDstrct((String) session.getAttribute("Distrito"));
        posb.setBase(user.getBase());
        posb.setFecha(fecha);
        for( int i=0; i<banco.length; i++){
            posb.setBranch_code(banco[i]);
            posb.setBank_account_no(sucursal[i]);
            posb.setAnticipos(Double.parseDouble(anticipos[i].replaceAll(",","")));
            posb.setProveedores(Double.parseDouble(proveedores[i].replaceAll(",","")));
            posb.setCupo(Double.parseDouble(cupo[i].replaceAll(",","")));
            posb.setSaldo_anterior(Double.parseDouble(saldoa[i].replaceAll(",","")));
            posb.setSaldo_inicial(Double.parseDouble(saldoi[i].replaceAll(",","")));
            posb.setAgency_id(agency_id[i]);
            posb.setNuevo_saldo(Double.parseDouble(nsaldo[i].replaceAll(",","")));
            posb.setReg_status("");
            
            // model.posbancariaSvc.setPosbanc(posb);
            
            
            if( posb.getCupo()!=0 || posb.getSaldo_inicial()!=0 ){
//                    
//                    String reg_status = model.posbancariaSvc.existe(posb.getDstrct(), posb.getAgency_id(), posb.getBranch_code(),
//                            posb.getBank_account_no(), posb.getFecha());
//
//                    if( reg_status==null ){                        
//                        posb.setSaldo_anterior(0);
//                        model.posbancariaSvc.ingresar();
//                    } else {
//                        posb.setReg_status("");
//                        model.posbancariaSvc.actualizar();
//                    }
//
            }
            

        }
        
        this.dispatchRequest(next);
    }
    
}
