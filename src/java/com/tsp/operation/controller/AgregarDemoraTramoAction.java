/*
 * AgregarDemoraTramoAction.java
 *
 * Created on 11 de agosto de 2005, 07:31 AM
 */

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;

/**
 *
 * @author  Andr�s
 */
public class AgregarDemoraTramoAction extends Action{
    
    /** Creates a new instance of AgregarDemoraTramoAction */
    public AgregarDemoraTramoAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        //Atributos de la demora
        String fecha = request.getParameter("fecha");
        String sitio = request.getParameter("sitio");
        String observacion = request.getParameter("observacion");
        String cdemora = request.getParameter("cdemora");
        String cjerarquia = request.getParameter("cjerarquia");
        String duracion = request.getParameter("duracion");
        
        //Instancia de Demora
        Demora demora = new Demora();
        demora.setDuracion(Integer.valueOf(duracion));
        demora.setFecha_demora(fecha);
        demora.setSitio_demora(sitio);
        demora.setObservacion(observacion);
        demora.setCodigo_demora(cdemora);
        demora.setCodigo_jerarquia(cjerarquia);
        
        //request.setAttribute("nit", "ECE0D8");        
        
        //Usuario en sesi�n
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        demora.setUsuario_creacion(usuario.getLogin());
        demora.setBase(usuario.getBase());
        demora.setDistrito((String) session.getAttribute("Distrito"));
        
        //Pr�xima vista
        String next = "/jsp/trafico/demora/asignarDemoraCaravana.jsp";
        
        
        try{
            Vector planillas = model.demorasSvc.planillasTramo();
            
            for(int i=0; i<planillas.size(); i++){
                String numpla = (String) planillas.elementAt(i);                              
                
                demora.setPlanilla(numpla);
                                
                model.demorasSvc.agregarDemora(demora);
            }
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
