/*
 * ListarReportesMovTrafAction.java
 *
 * Created on 17 de septiembre de 2005, 01:44 PM
 *
 * To change this template, choose Tools | Options and locate the template under
 * the Source Creation and Management node. Right-click the template and choose
 * Open. You can then make changes to the template in the Source Editor.
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
/**
 *
 * @author Armando Oviedo
 */
public class ListarReportesMovTrafAction extends Action{
    
    /** Creates a new instance of ListarReportesMovTrafAction */
    public ListarReportesMovTrafAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException{
        String next="/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina");
        try{
            HttpSession session = request.getSession();
            String numpla = request.getParameter("numpla");
            
            if (request.getParameter("pagina").equalsIgnoreCase ("movtraflist.jsp") ){
                model.rmtService.BuscarReportesPlanilla(numpla);
            }
            
            model.rmtService.setPlanilla(numpla);
        }
        catch(Exception ex){
            throw new ServletException(ex.getMessage());
        }
        this.dispatchRequest(next);
    }
   
    
}
