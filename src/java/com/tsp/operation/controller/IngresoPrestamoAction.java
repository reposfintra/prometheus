/***************************************
* Nombre Clase ............. IngresoPrestamoAction.java
* Descripci�n  .. . . . . .  Realiza los eventos para registrar ingresos a los prestamos
* Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
* Fecha . . . . . . . . . .  18/03/2006
* versi�n . . . . . . . . .  1.0
* Copyright ...Transportes Sanchez Polo S.A.
*******************************************/



package com.tsp.operation.controller;




import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.beans.*;



public class IngresoPrestamoAction extends Action{
    
    
    
    public void run() throws ServletException, InformationException {
        
        
         try{
             HttpSession session = request.getSession();
             Usuario usuario = (Usuario) session.getAttribute("Usuario");
             
            String    next      = "/jsp/cxpagar/prestamos/Ingresos.jsp?msj=";
            String    msj       = "";
            
            String    evento    = request.getParameter("evento");
            
            if(evento!=null){
                
                if(evento.equals("BUSCAR")){
                     String    tercero   = request.getParameter("tercero");
                     String    prestamo  = request.getParameter("prestamo");
                     model.IngPrestamoSvc.serachPrestamo( usuario.getDstrct(), tercero, prestamo);
                     
                     Prestamo pt = model.IngPrestamoSvc.getPrestamo();
                     if(pt==null)  next+="No existe prestamo, o no est� aprobado";
                     else          next+="&capturar=yes";
                }
            
                if(evento.equals("SAVE")){
                    String[] cuotas    = request.getParameterValues("cuota");
                    String   banco     = reset( request.getParameter("banco")    );
                    String   sucursal  = reset( request.getParameter("sucursal") );
                    String   cheque    = reset( request.getParameter("cheque")   );
                    String   corrida   = reset( request.getParameter("corrida")  );
                    
                    model.IngPrestamoSvc.cancelarCuotas(cuotas, banco, sucursal, cheque, corrida, usuario.getLogin() );  
                    
                    List lista = model.IngPrestamoSvc.getListAmortizacion();
                    next += "Cuota(s) cancelada(s) exitosamente...";
                    if(lista.size()>0)  
                        next+="&capturar=yes";  
                    
                }
                
                if(evento.equals("ATRAS")){
                    model.IngPrestamoSvc.reset();
                }
                
                
            }
            
            next+=msj;
            
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new Exception("No se pudo encontrar "+ next);
            rd.forward(request, response);
           
            
        }catch(Exception e){
            throw new ServletException( e.getMessage() );
        }
        
        
    }
    
    
    
    
    
    
    public String reset(String val){
        if(val==null)
            val = "";
        return val;
    }
    
    
    
}
