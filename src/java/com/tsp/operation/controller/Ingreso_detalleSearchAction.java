/***********************************************
 * Nombre clase: Ingreso_detalleSearchAction.java
 * Descripción: Accion para ingresar un item de ingreso detalle a la bd.
 * Autor: Jose de la rosa
 * Fecha: 10 de mayo de 2006, 02:11 PM
 * Versión: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 **********************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.operation.model.DAOS.ReestructurarNegociosDAO;
import com.tsp.operation.model.DAOS.impl.ReestructurarNegociosImpl;
import com.tsp.pdf.*;

public class Ingreso_detalleSearchAction extends Action{
    
    /** Creates a new instance of Ingreso_detalleSearchAction */
    public Ingreso_detalleSearchAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/jsp/cxcobrar/ingreso_detalle/ItemsIngresoDetalle.jsp";
        HttpSession session = request.getSession();
        com.tsp.finanzas.contab.model.Model modelcontab = (com.tsp.finanzas.contab.model.Model) session.getAttribute("modelcontab");
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = (String) session.getAttribute("Distrito");
        String opcion = (String) request.getParameter("opcion");
        String pag = request.getParameter("pagina")!=null?(String) request.getParameter("pagina"):"";
        try{
            //busca los datos de la cabecera
            if(opcion.equals("0")){
                Ingreso_detalle cabecera = model.ingreso_detalleService.getCabecera();
                if( cabecera != null ){
                    cabecera.setValor_tasa( 0 );
                    cabecera.setValor_total( 0 );
                }
                model.ingreso_detalleService.resetVecIngreso_detalle();
            }
            else if(opcion.equals("1")){
                session.setAttribute("fin", null);
                String numero_ingreso = (String) request.getParameter("numero_ingreso");
                String tipo_doc = (String) request.getParameter("tipo_doc");
                String moneda_local = model.ingreso_detalleService.monedaLocal(distrito);
                try{
                    //obtengo datos de la cabecera del archivo txt
                    Ingreso_detalle cabecera = model.ingreso_detalleService.leerArchivo("ingreso_detalle"+usuario.getLogin()+".txt", usuario.getLogin());
                    model.ingreso_detalleService.setCabecera(cabecera);
                    
                    //obtengo el vector de items del archivo txt
                    Vector vItems = model.ingreso_detalleService.leerArchivoItems("ingreso_detalle"+usuario.getLogin()+".txt", usuario.getLogin());
                    model.ingreso_detalleService.setVectorIngreso_detalle( vItems );
                    
                }catch(Exception e){
                    model.ingreso_detalleService.setCabecera(null);
                    model.ingreso_detalleService.resetVecIngreso_detalle();
                }
                //valido si no hay datos en el achivo txt
                if(model.ingreso_detalleService.getCabecera() == null || model.ingreso_detalleService.getVecIngreso_detalle() == null ){
                    model.ingreso_detalleService.datosCabecera(usuario.getDstrct(), numero_ingreso, tipo_doc, "C");
                    Ingreso_detalle cabecera = model.ingreso_detalleService.getCabecera();
                    cabecera.setPagina( pag );
                    if( !cabecera.getFecha_contabilizacion().equals("0099-01-01 00:00:00") ){
                        cabecera.setValor_ingreso_me( cabecera.getSaldo_ingreso() );
                        cabecera.setNumero_ingreso( model.ingreso_detalleService.numeroIngresoRI(numero_ingreso, cabecera.getTipo_documento(), distrito ) );//buscar de la serie
                        request.setAttribute("vista","El ingreso: "+numero_ingreso+" se encuentra contabilizado y fué reemplazado por el ingreso: "+cabecera.getNumero_ingreso());
                    }
                    
                }
                else{
                    Ingreso_detalle cabecera = model.ingreso_detalleService.getCabecera();
                    if( !cabecera.getNumero_ingreso().equals(numero_ingreso) ){
                        model.ingreso_detalleService.setCabecera(null);
                        model.ingreso_detalleService.resetVecIngreso_detalle();
                        model.ingreso_detalleService.datosCabecera(usuario.getDstrct(),numero_ingreso, tipo_doc, "C");
                    }
                }
            }
            //valida las facturas
            else if(opcion.equals("2")){
                //vbles
                int j = 0, k = 0, cont =0;
                double retefuente = 0, reteica = 0, total = 0, valor_total = 0, valor_tasa = 0;
                String factura = "", moneda_local = "", mon = "", nmon = "", mensaje;
                boolean sw = false, existe_tasa = false, mas_moneda = false, mon_ext = false;
                Ingreso_detalle ingreso = new Ingreso_detalle();
                
                //objeto de cabecera
                Ingreso_detalle cabecera = model.ingreso_detalleService.getCabecera();
                //vector de items
                Vector a = model.ingreso_detalleService.getVecIngreso_detalle();
                //consulta de moneda local
                moneda_local = model.ingreso_detalleService.monedaLocal(distrito);
                
                k = a==null?0:a.size();
                factura = request.getParameter("c_factura"+ ( k ) )!=null?request.getParameter("c_factura"+ ( k ) ).toUpperCase():"";
                
                //objeto de las facturas
                model.ingreso_detalleService.datosFactura( usuario.getDstrct(), factura, cabecera.getTipo_documento(), cabecera.getCliente());
                Ingreso_detalle ing = model.ingreso_detalleService.getFactura();
                if( ing ==null){
                    request.setAttribute("mensaje","La Factura no existe en la BD");
                }
                else{
                    //vector de facturas guardadas
                    for (int i=0; i < k; i++){
                        Ingreso_detalle item = (Ingreso_detalle)a.elementAt(i);
                        //valido que no existan mas de una moneda diferente a la moneda del ingreso
                        if( !item.getMoneda_factura().equals(cabecera.getMoneda()) ){
                            mas_moneda = true;
                            mon = item.getMoneda_factura();
                        }
                        total = 0;
                        reteica = Double.parseDouble( !request.getParameter("c_valor_rica"+i).equals("")?request.getParameter("c_valor_rica"+i).replaceAll(",",""):"0" );
                        retefuente = Double.parseDouble( !request.getParameter("c_valor_rfte"+i).equals("")?request.getParameter("c_valor_rfte"+i).replaceAll(",",""):"0" );
                        total = Double.parseDouble( !request.getParameter("c_valor_total_factura"+i).equals("")?request.getParameter("c_valor_total_factura"+i).replaceAll(",",""):"0" );
                        item.setCodigo_retefuente( !request.getParameter("c_cod_rfte"+i).equals("")?request.getParameter("c_cod_rfte"+i):"" );
                        item.setCodigo_reteica( !request.getParameter("c_cod_rica"+i).equals("")?request.getParameter("c_cod_rica"+i):"" );
                        item.setPorcentaje_rfte( Double.parseDouble( request.getParameter("c_impuestos_rfte"+i)!=null?request.getParameter("c_impuestos_rfte"+i) :"0" ) );
                        item.setPorcentaje_rica( Double.parseDouble( request.getParameter("c_impuestos_rica"+i)!=null?request.getParameter("c_impuestos_rica"+i) : "0" ) );
                        item.setValor_abono( !request.getParameter("c_valor_abono"+i).equals("")?Double.parseDouble(request.getParameter("c_valor_abono"+i).replaceAll(",","") ):0 );
                        item.setCuenta( !request.getParameter("cuenta"+i).equals("")?request.getParameter("cuenta"+i):"" );
                        item.setValor_retefuente( retefuente );
                        item.setValor_reteica( reteica );
                        item.setValor_total_factura( total );
                        item.setTipo_aux( !request.getParameter("tipo"+i).equals("")?request.getParameter("tipo"+i):"" );
                        item.setAuxiliar( !request.getParameter("auxiliar"+i).equals("")?request.getParameter("auxiliar"+i):"" );
                        
                        valor_total += item.getValor_abono() - ( item.getValor_reteica() + item.getValor_retefuente() );
                    }
                    if( mas_moneda && !ing.getMoneda_factura().equals(cabecera.getMoneda())&& !mon.equals(ing.getMoneda_factura()) ){
                        mon = ing.getMoneda_factura();
                    }
                    else
                        mas_moneda = false;
                    if(!mas_moneda){
                        //Si no existe una factura en el vector
                        //if( sw == false ){
                        //validamos si hay tasa para esa moneda
                        if( request.getParameter("vlr_tasa")!=null && Double.parseDouble( request.getParameter("vlr_tasa").replaceAll(",","") ) > 0 ){
                            valor_tasa =  Double.parseDouble( request.getParameter("vlr_tasa").replaceAll(",","") );
                            existe_tasa = true;
                        }else{
                            if( cabecera.getMoneda().equals("DOL") && ing.getMoneda_factura().equals("BOL") ){
                                if( cabecera.getTasaDB() > 0 ){
                                    valor_tasa = Double.parseDouble( UtilFinanzas.customFormat("#,###.##########", 1 / cabecera.getTasaDB() ,10 ).replaceAll(",","") );
                                    existe_tasa = true;
                                }
                                else
                                    existe_tasa = false;
                            }
                            else if( cabecera.getMoneda().equals("BOL") && ing.getMoneda_factura().equals("DOL") ){
                                if( cabecera.getTasaDB() > 0 ){
                                    valor_tasa = Double.parseDouble( UtilFinanzas.customFormat("#,###.##########", cabecera.getTasaDB() ,10 ).replaceAll(",","") );
                                    existe_tasa = true;
                                }
                                else
                                    existe_tasa = false;
                            }
                            else{
                                try{
                                    //moneda local, moneda origen, moneda destino
                                    nmon = nmon.equals("") ? ing.getMoneda_factura() : nmon;
                                    Tasa t = model.tasaService.buscarValorTasa( moneda_local, nmon, cabecera.getMoneda(), cabecera.getFecha_consignacion() );
                                    if(t!=null){
                                        valor_tasa = Double.parseDouble(  String.valueOf(t.getValor_tasa()).replaceAll(",","") );
                                        existe_tasa = true;
                                    }
                                }catch(Exception ex){
                                    ex.getMessage();
                                }
                            }
                        }
                        if(existe_tasa){
                            //if( cabecera.getValor_ingreso_me() - valor_total > 0 ){
                                // verificacion de cuentas
                                if( !ing.getCuenta().equals("") ){
                                    com.tsp.finanzas.contab.model.beans.PlanDeCuentas plan_cuenta = modelcontab.planDeCuentasService.consultaCuentaModulo( usuario.getDstrct(), ing.getCuenta(), 3);
                                    if(plan_cuenta!=null){
                                        //mensaje = "";
                                        //if(plan_cuenta.getDetalle().equalsIgnoreCase("S") &&  plan_cuenta.getPerteneceAmodulo().equalsIgnoreCase("S") ){
                                        if(plan_cuenta.getSubledger().equalsIgnoreCase("S")){
                                            LinkedList t_aux = new LinkedList();
                                            try{
                                                modelcontab.subledgerService.buscarCuentasTipoSubledger( ing.getCuenta() );
                                                t_aux = modelcontab.subledgerService.getCuentastsubledger();
                                            }catch(Exception e){
                                                t_aux = null;
                                            }
                                            ingreso.setTipos( t_aux );
                                        }else{
                                            modelcontab.subledgerService.setCuentastsubledger(null);
                                            ingreso.setTipos( null );
                                        }
                                        //}
                                        /*if(!plan_cuenta.getPerteneceAmodulo().equalsIgnoreCase("S"))
                                            request.setAttribute("mensaje","La cuenta no pertence a este modulo");*/
                                        
                                    }
                                    else{
                                        modelcontab.subledgerService.setCuentastsubledger(null);
                                        request.setAttribute("mensaje","No existe la Cuenta "+ing.getCuenta());
                                    }
                                }
                                else{
                                    request.setAttribute("mensaje","La factura no tiene CMC o no se encuentra registrado y por lo tanto no presenta cuenta");
                                }
                                //***********************
                                ingreso.setSal_fact_mlocal( ing.getValor_saldo_factura() );
                                ingreso.setFactura( ing.getFactura() );
                                ingreso.setFecha_factura( ing.getFecha_factura() );
                                ingreso.setCuenta( ing.getCuenta() );
                                ingreso.setTipo_aux( ing.getTipo_aux() );
                                ingreso.setAuxiliar( ing.getAuxiliar() );
                                ingreso.setAgencia_facturacion( ing.getAgencia_facturacion());
                                ingreso.setDescripcion_factura( ing.getDescripcion_factura() );
                                ingreso.setMoneda_factura( ing.getMoneda_factura() );
                                ingreso.setCodigo_retefuente( cabecera.getCodigo_retefuente() );
                                ingreso.setCodigo_reteica( !request.getParameter("c_cod_rica"+k).equals("")?request.getParameter("c_cod_rica"+k):"" );
                                reteica = Double.parseDouble( !request.getParameter("c_valor_rica"+k).equals("")?request.getParameter("c_valor_rica"+k):"0" );
                                if( !ing.getMoneda_factura().equals(cabecera.getMoneda()) ){
                                    ingreso.setValor_factura( Util.roundByDecimal( ing.getValor_factura_me() * valor_tasa , 2) );
                                    ingreso.setValor_saldo_factura( Util.roundByDecimal( ing.getValor_saldo_factura_me() * valor_tasa, 2) );
                                    ingreso.setValor_retefuente( Util.roundByDecimal( ( ing.getValor_factura_me() * valor_tasa ) * ( cabecera.getPorcentaje_rfte()/100 ), 2 ) );
                                    //valido si el saldo del ingreso es menor al saldo de la factura
                                    if(  ing.getValor_saldo_factura() > cabecera.getValor_ingreso_me() - valor_total ){
                                        ingreso.setValor_abono( Util.roundByDecimal( cabecera.getValor_ingreso_me() - valor_total + ingreso.getValor_retefuente(), 2 ) );
                                    }
                                    else{
                                        ingreso.setValor_abono( ingreso.getValor_saldo_factura() );
                                    }
                                    ingreso.setValor_total_factura( Util.roundByDecimal( ing.getValor_saldo_factura_me() * valor_tasa, 2) - ingreso.getValor_abono() );
                                }
                                else{
                                    ingreso.setValor_factura( Util.roundByDecimal(ing.getValor_factura_me(), 2 ) );
                                    ingreso.setValor_saldo_factura( Util.roundByDecimal( ing.getValor_saldo_factura_me(), 2 ) );
                                    ingreso.setValor_retefuente( Util.roundByDecimal( ( ing.getValor_factura_me() ) * ( cabecera.getPorcentaje_rfte()/100 ), 2) );
                                    //valido si el saldo del ingreso es menor al saldo de la factura
                                    if( ingreso.getValor_saldo_factura() > cabecera.getValor_ingreso_me() - valor_total ){
                                        ingreso.setValor_abono( Util.roundByDecimal( cabecera.getValor_ingreso_me() - valor_total + ingreso.getValor_retefuente(), 2 ) );
                                    }
                                    else{
                                        ingreso.setValor_abono( ingreso.getValor_saldo_factura() );
                                    }
                                    ingreso.setValor_total_factura( ing.getValor_saldo_factura_me()  - ingreso.getValor_abono() );
                                }
                                
                                ingreso.setValor_reteica( reteica );
                                ingreso.setValor_saldo_fact( ing.getValor_saldo_factura() );
                                ingreso.setValor_factura_me( ing.getValor_factura_me() );
                                ingreso.setValor_saldo_factura_me( ing.getValor_saldo_factura_me() );
                                ingreso.setPorcentaje_rfte( cabecera.getPorcentaje_rfte() );
                                ingreso.setPorcentaje_rica( 0 );
                                ingreso.setValor_tasa_factura( ing.getValor_tasa_factura() );
                                valor_total += ingreso.getValor_abono() - ( ingreso.getValor_reteica() + ingreso.getValor_retefuente() );
                                
                                if( request.getAttribute("mensaje")==null )
                                    model.ingreso_detalleService.setVecIngreso_detalle( ingreso );
                                
                                for (int h=0; h < model.ingreso_detalleService.getVecIngreso_detalle().size(); h++){
                                    Ingreso_detalle it = (Ingreso_detalle)model.ingreso_detalleService.getVecIngreso_detalle().elementAt(h);
                                    if( !cabecera.getMoneda().equals( it.getMoneda_factura() ) )
                                        mon_ext = true;
                                }
                                //swiche si hay monedas diferentes a la moneda del ingreso
                                if( mon_ext ){
                                    cabecera.setValor_tasa( request.getParameter("vlr_tasa")!=null ? Double.parseDouble( request.getParameter("vlr_tasa").replaceAll(",","") ) : valor_tasa );
                                }
                                else{
                                    cabecera.setValor_tasa( 0 );
                                }
                            //}
                            //else
                              //  request.setAttribute("mensaje","No se puede agregar la factura "+ing.getFactura()+" por que el total acumulado facturas supera el valor de consignación");
                        }
                        else
                            request.setAttribute("mensaje","No hay conversión actual en la tasa de la moneda "+nmon+" a la moneda "+cabecera.getMoneda());
                        //}
                        //else
                        //request.setAttribute ("mensaje","La Factura ya existe en un item anterior");
                    }
                    else
                        request.setAttribute("mensaje","No se puede agregar 2 monedas diferentes a la moneda del ingreso!");
                }
                cabecera.setValor_total( valor_total );
            }//END else if(opcion.equals("2")){
            //guarda en el archivo temporal
            else if(opcion.equals("3")){
                //vbles
                Vector consultas = new Vector();
                int j = 0, k = 0;
                double retefuente = 0, reteica = 0, total = 0, valor_total = 0;
                String factura = "", moneda_local = "", nom_money = "";
                Ingreso_detalle ingreso = new Ingreso_detalle();
                boolean mon_ext = false;
                
                //objeto de cabecera
                Ingreso_detalle cabecera = model.ingreso_detalleService.getCabecera();
                //vector de items
                Vector a = model.ingreso_detalleService.getVecIngreso_detalle();
                //consulta de moneda local
                moneda_local = model.ingreso_detalleService.monedaLocal(distrito);
                k = a==null?0:a.size();
                
                //vector de facturas guardadas
                for (int i=0; i < k; i++){
                    Ingreso_detalle item = (Ingreso_detalle)a.elementAt(i);
                    total = 0;
                    reteica = Double.parseDouble( !request.getParameter("c_valor_rica"+i).equals("")?request.getParameter("c_valor_rica"+i).replaceAll(",",""):"0" );
                    retefuente = Double.parseDouble( !request.getParameter("c_valor_rfte"+i).equals("")?request.getParameter("c_valor_rfte"+i).replaceAll(",",""):"0" );
                    total = Double.parseDouble( !request.getParameter("c_valor_total_factura"+i).equals("")?request.getParameter("c_valor_total_factura"+i).replaceAll(",",""):"0" );
                    item.setCodigo_retefuente( !request.getParameter("c_cod_rfte"+i).equals("")?request.getParameter("c_cod_rfte"+i):"" );
                    item.setCodigo_reteica( !request.getParameter("c_cod_rica"+i).equals("")?request.getParameter("c_cod_rica"+i):"" );
                    item.setCuenta( !request.getParameter("cuenta"+i).equals("")?request.getParameter("cuenta"+i):"" );
                    item.setPorcentaje_rfte( Double.parseDouble( request.getParameter("c_impuestos_rfte"+i)!=null?request.getParameter("c_impuestos_rfte"+i) :"0" ) );
                    item.setPorcentaje_rica( Double.parseDouble( request.getParameter("c_impuestos_rica"+i)!=null?request.getParameter("c_impuestos_rica"+i) :"0" ) );
                    item.setValor_abono( !request.getParameter("c_valor_abono"+i).equals("")?Double.parseDouble(request.getParameter("c_valor_abono"+i).replaceAll(",","") ):0 );
                    item.setValor_retefuente( retefuente );
                    item.setValor_reteica( reteica );
                    item.setValor_total_factura( total );
                    item.setTipo_aux( !request.getParameter("tipo"+i).equals("")?request.getParameter("tipo"+i):"" );
                    item.setAuxiliar( !request.getParameter("auxiliar"+i).equals("")?request.getParameter("auxiliar"+i):"" );
                    
                    valor_total += item.getValor_abono() - ( item.getValor_reteica() + item.getValor_retefuente() );
                }
                cabecera.setValor_tasa( request.getParameter("vlr_tasa") !=null ?Double.parseDouble( request.getParameter("vlr_tasa").replaceAll(",","") ) : 0 );
                cabecera.setValor_total(valor_total);
                //validamos si eliminamos una posicion del vector
                if( request.getParameter("pos")!=null){
                    valor_total = 0;
                    model.ingreso_detalleService.deleteVecIngreso_detalle(Integer.parseInt(request.getParameter("pos")));
                    a = model.ingreso_detalleService.getVecIngreso_detalle();
                    for (int i=0; i < a.size(); i++){
                        Ingreso_detalle items = (Ingreso_detalle)a.elementAt(i);
                        valor_total += items.getValor_abono()  - ( items.getValor_reteica() + items.getValor_retefuente() );
                        if( !cabecera.getMoneda().equals( items.getMoneda_factura() ) ){
                            mon_ext = true;
                            nom_money = items.getMoneda_factura();
                        }
                    }
                    cabecera.setValor_total(valor_total);
                    //request.setAttribute("mensaje","Se elimino el archivo exitosamente");
                    if( mon_ext ){
                        //moneda local, moneda origen, moneda destino
                        try{
                            Tasa t = model.tasaService.buscarValorTasa( moneda_local, nom_money, cabecera.getMoneda(), Util.getFechaActual_String(4) );
                            if(t!=null){
                                cabecera.setValor_tasa( request.getParameter("vlr_tasa")!=null ? Double.parseDouble( request.getParameter("vlr_tasa").replaceAll(",","") ) : t.getValor_tasa() );
                            }
                            else
                                cabecera.setValor_tasa( 0 );
                        }catch(Exception ex){
                            cabecera.setValor_tasa( 0 );
                        }
                    }
                    else{
                        cabecera.setValor_tasa( 0 );
                    }
                }
                else{
                    if( request.getParameter("abrir_ajuste")!=null ){
                        next += "?abrir_ajuste=ok";
                        modelcontab.subledgerService.setCuentastsubledger(null);
                    }
                    else{
                        //validamos si entra a grabar en el archivo txt
                        if( request.getParameter("abrir_detalles")==null){
                            //escribir en el archivo txt
                            boolean escrituta = true;
                            try{
                                model.ingreso_detalleService.escribirArchivo(cabecera, a, "ingreso_detalle"+usuario.getLogin()+".txt", usuario.getLogin());
                            }catch(Exception ex){
                                request.setAttribute("mensaje","Error no se pudo almacenar el archivo");
                                escrituta = true;
                            }
                            if(escrituta)
                                request.setAttribute("mensaje","Se almaceno el registro exitosamente");
                        }
                        else{
                            next += "?abrir_detalles=ok";
                            model.ingreso_detalleService.datosListadoFactura( distrito, cabecera.getNit_cliente(), cabecera.getTipo_documento() );
                        }
                    }
                }
            }
            //ingreso de datos del ingreso
            else if(opcion.equals("4")){
                //vbles
                Vector consultas = new Vector();
                int j = 0, k = 0;
                double retefuente = 0, reteica = 0, total = 0, valor_total = 0, valor_tasa = 0, valor_tasa2 = 0;
                double valor_tasa_factura = 0, valor_fact = 0, acumulado = 0, cont=0;
                String factura = "", moneda_local = "", cuentas = "", mensaje = "", msg = "";
                boolean sw = false, existe_tasa = false, existe_cuentas = true;
                
                valor_total = Double.parseDouble( !request.getParameter("c_valor_total").equals("")?request.getParameter("c_valor_total").replaceAll(",",""):"0" );
                //objeto de cabecera
                Ingreso_detalle cabecera = model.ingreso_detalleService.getCabecera();
                //vector de items
                Vector a = model.ingreso_detalleService.getVecIngreso_detalle();
                //consulta de moneda local
                moneda_local = model.ingreso_detalleService.monedaLocal(distrito);
                k = a==null?0:a.size();
                int nume = model.ingreso_detalleService.numero_items(distrito, cabecera.getTipo_documento(),cabecera.getNumero_ingreso());
                for (int i=0; i < k; i++){
                    Ingreso_detalle item = (Ingreso_detalle)a.elementAt(i);
                    if( request.getParameter("vlr_tasa")!=null && Double.parseDouble( request.getParameter("vlr_tasa").replaceAll(",","") ) > 0 ){
                        valor_tasa = Double.parseDouble( request.getParameter("vlr_tasa").replaceAll(",","")  );
                        existe_tasa = true;
                    }
                    else{
                        if( cabecera.getMoneda().equals("DOL") && item.getMoneda_factura().equals("BOL") ){
                            if( cabecera.getTasaDB() > 0 ){
                                valor_tasa = Double.parseDouble( UtilFinanzas.customFormat("#,###.##########",  1 /  cabecera.getTasaDB() ,10 ).replaceAll(",","") );
                                existe_tasa = true;
                            }
                            else
                                existe_tasa = false;
                        }
                        else if( cabecera.getMoneda().equals("BOL") && item.getMoneda_factura().equals("DOL") ){
                            if( cabecera.getTasaDB() > 0 ){
                                valor_tasa = Double.parseDouble( UtilFinanzas.customFormat("#,###.##########", cabecera.getTasaDB() ,10 ).replaceAll(",","") );
                                existe_tasa = true;
                            }
                            else
                                existe_tasa = false;
                        }
                        else{
                            try{
                                //moneda local, moneda origen, moneda destino
                                Tasa t = model.tasaService.buscarValorTasa( moneda_local, item.getMoneda_factura(), cabecera.getMoneda(), cabecera.getFecha_consignacion() );
                                if(t!=null){
                                    valor_tasa = Double.parseDouble( UtilFinanzas.customFormat("#,###.##########",  t.getValor_tasa() ,10 ).replaceAll(",","") );
                                    existe_tasa = true;
                                }
                                else
                                    existe_tasa = false;
                            }catch(Exception ex){
                                existe_tasa = false;
                            }
                        }
                    }
                    if(existe_tasa && valor_tasa != 0 ){
                        total = 0;
                        double valor_tasa1 = 0;
                        try{
                            //moneda local, moneda origen, moneda destino
                            Tasa t = model.tasaService.buscarValorTasa( moneda_local, item.getMoneda_factura(), moneda_local, cabecera.getFecha_consignacion() );
                            Tasa ti = model.tasaService.buscarValorTasa( moneda_local, cabecera.getMoneda(), moneda_local, cabecera.getFecha_consignacion() );
                            if(t!=null){
                                valor_tasa1 = Double.parseDouble( UtilFinanzas.customFormat("#,###.##########",  t.getValor_tasa() ,10 ).replaceAll(",","") );
                                valor_tasa2 = Double.parseDouble( UtilFinanzas.customFormat("#,###.##########",  ti.getValor_tasa() ,10 ).replaceAll(",","") );
                                existe_tasa = true;
                            }
                        }catch(Exception ex){
                            existe_tasa = false;
                        }
                        
                        if( ! item.getMoneda_factura().equals( cabecera.getMoneda() ) ){
                            valor_tasa = Double.parseDouble( request.getParameter("vlr_tasa")!=null ? !request.getParameter("vlr_tasa").equals("")?request.getParameter("vlr_tasa").replaceAll(",",""): ""+valor_tasa : ""+valor_tasa );
                        }
                        reteica = Double.parseDouble( !request.getParameter("c_valor_rica"+i).equals("")?request.getParameter("c_valor_rica"+i).replaceAll(",",""):"0" );
                        retefuente = Double.parseDouble( !request.getParameter("c_valor_rfte"+i).equals("")?request.getParameter("c_valor_rfte"+i).replaceAll(",",""):"0" );
                        total = Double.parseDouble( !request.getParameter("c_valor_total_factura"+i).equals("")?request.getParameter("c_valor_total_factura"+i).replaceAll(",",""):"0" );
                        item.setCuenta( !request.getParameter("cuenta"+i).equals("")?request.getParameter("cuenta"+i).toUpperCase():"" );
                        item.setValor_abono( !request.getParameter("c_valor_abono"+i).equals("")?Double.parseDouble(request.getParameter("c_valor_abono"+i).replaceAll(",","") ):0 );
                        item.setCodigo_retefuente( !request.getParameter("c_cod_rfte"+i).equals("")?request.getParameter("c_cod_rfte"+i).replaceAll(",",""):"" );
                        item.setCodigo_reteica( !request.getParameter("c_cod_rica"+i).equals("")?request.getParameter("c_cod_rica"+i).replaceAll(",",""):"" );
                        double saldo = Double.parseDouble(!request.getParameter("c_valor_total_factura"+i).equals("")?request.getParameter("c_valor_total_factura"+i).replaceAll(",",""):"0");
                        item.setDescripcion_factura( !request.getParameter("c_descripcion"+i).equals("")?request.getParameter("c_descripcion"+i):"" );
                        
                        item.setTipo_aux( !request.getParameter("tipo"+i).equals("")?request.getParameter("tipo"+i).toUpperCase():"" );
                        item.setAuxiliar( !request.getParameter("auxiliar"+i).equals("")?request.getParameter("auxiliar"+i).toUpperCase():"" );
                        
                        item.setValor_retefuente_me( retefuente );
                        item.setValor_reteica_me( reteica );
                        item.setValor_ingreso_me( item.getValor_abono() );
                        
                        item.setValor_retefuente( Math.round( retefuente * valor_tasa2 ) );
                        item.setValor_reteica( Math.round( reteica * valor_tasa2 ) );
                        item.setValor_ingreso( Math.round( item.getValor_abono() * valor_tasa2 ) );
                        
                        // verificacion de cuentas
                        com.tsp.finanzas.contab.model.beans.PlanDeCuentas plan_cuenta = modelcontab.planDeCuentasService.consultaCuentaModulo( usuario.getDstrct(), item.getCuenta(), 3);
                        mensaje = "";
                        if(plan_cuenta!=null){
                            //if(plan_cuenta.getDetalle().equalsIgnoreCase("S") &&  plan_cuenta.getPerteneceAmodulo().equalsIgnoreCase("S") ){
                            if(plan_cuenta.getSubledger().equalsIgnoreCase("S")){
                                try{
                                    modelcontab.subledgerService.buscarCuentasTipoSubledger(item.getCuenta());
                                }catch (Exception e){
                                    e.getMessage();
                                }
                                if(item.getTipo_aux().equals("")){
                                    mensaje +=" requiere de tipo cuenta";
                                    existe_cuentas = false;
                                    if(item.getAuxiliar().equals("")){
                                        mensaje +=" y requiere de auxiliar";
                                    }
                                }
                                else{
                                    if(item.getAuxiliar().equals("")){
                                        mensaje +=" requiere de auxiliar";
                                        existe_cuentas = false;
                                    }
                                }
                            }
                            else
                                modelcontab.subledgerService.setCuentastsubledger(null);
                            /*}
                            if(!plan_cuenta.getPerteneceAmodulo().equalsIgnoreCase("S")){
                                mensaje +=" no pertence a este modulo.";
                                existe_cuentas = false;
                            }*/
                        }
                        else{
                            modelcontab.subledgerService.setCuentastsubledger(null);
                            mensaje =" no existe";
                            existe_cuentas = false;
                        }
                        if( !mensaje.equals("") )
                            msg += " En la factura "+item.getFactura()+" la cuenta"+mensaje+", ";
                        //***********************
                        
                        item.setDistrito(distrito);
                        item.setNit_cliente( cabecera.getNit_cliente() );
                        item.setTipo_documento( cabecera.getTipo_documento() );
                        item.setTipo_doc( !item.getFactura().equals("")?"FAC":"" );
                        item.setDocumento( item.getFactura() );
                        item.setNumero_ingreso( cabecera.getNumero_ingreso() );
                        item.setItem( nume );
                        nume++;
                        item.setCreation_user( usuario.getLogin() );
                        item.setBase( usuario.getBase() );
                        
                        if( ! item.getMoneda_factura().equals( cabecera.getMoneda() ) ){
                            if( (int) ( item.getValor_saldo_factura_me() - ( item.getValor_abono()  / valor_tasa ) ) == 0 ){
                                if( !item.getMoneda_factura().equals("DOL") )
                                    item.setValor_diferencia( Math.round( item.getSal_fact_mlocal()  - item.getValor_ingreso() ) );
                                else
                                    item.setValor_diferencia( Math.round( Util.roundByDecimal( item.getSal_fact_mlocal()  - item.getValor_ingreso(),2 ) ) );
                            }
                            else
                                item.setValor_diferencia( 0 );
                            item.setValor_tasa(valor_tasa);
                        }
                        else{
                            item.setValor_tasa( 1 );
                        }
                        
                        item.setValor_saldo_factura_me( saldo + item.getValor_ingreso_me() );
                        consultas.add( model.ingreso_detalleService.insertarIngresoDetalle(item) );
                        
                        if( !item.getFactura().equals("") ){
                            if(!cabecera.getMoneda().equals( item.getMoneda_factura())){
                                item.setValor_ingreso_me( item.getValor_ingreso() );
                                item.setValor_ingreso( ( item.getValor_abono()  / valor_tasa ) );
                            }
                            else{
                                item.setValor_ingreso_me( item.getValor_abono() * valor_tasa1 );
                                item.setValor_ingreso( item.getValor_abono() );
                            }
                            if( !item.getMoneda_factura().equals("DOL") ){
                                item.setValor_ingreso_me( Math.round( item.getValor_ingreso_me() ) );
                                item.setValor_ingreso(  Math.round( item.getValor_ingreso() ) );
                            }
                            else{
                                item.setValor_ingreso_me( Math.round( item.getValor_ingreso_me() ) );
                                item.setValor_ingreso( Util.roundByDecimal( item.getValor_ingreso(),2 ) );
                            }
                            consultas.add( model.ingreso_detalleService.updateSaldoFactura(item.getValor_ingreso_me(), item.getValor_ingreso(), item.getFactura(), distrito, "FAC") );
                        }
                        //se llenan nuevamente los campos como estaban inicialmente
                        total = 0;
                        reteica = Double.parseDouble( !request.getParameter("c_valor_rica"+i).equals("")?request.getParameter("c_valor_rica"+i).replaceAll(",",""):"0" );
                        retefuente = Double.parseDouble( !request.getParameter("c_valor_rfte"+i).equals("")?request.getParameter("c_valor_rfte"+i).replaceAll(",",""):"0" );
                        total = Double.parseDouble( !request.getParameter("c_valor_total_factura"+i).equals("")?request.getParameter("c_valor_total_factura"+i).replaceAll(",",""):"0" );
                        item.setCodigo_retefuente( !request.getParameter("c_cod_rfte"+i).equals("")?request.getParameter("c_cod_rfte"+i):"" );
                        item.setCodigo_reteica( !request.getParameter("c_cod_rica"+i).equals("")?request.getParameter("c_cod_rica"+i):"" );
                        item.setCuenta( !request.getParameter("cuenta"+i).equals("")?request.getParameter("cuenta"+i):"" );
                        item.setPorcentaje_rfte( Double.parseDouble( request.getParameter("c_impuestos_rfte"+i)!=null?request.getParameter("c_impuestos_rfte"+i).replaceAll(",","") :"0" ) );
                        item.setPorcentaje_rica( Double.parseDouble( request.getParameter("c_impuestos_rica"+i)!=null?request.getParameter("c_impuestos_rica"+i).replaceAll(",","") :"0" ) );
                        item.setValor_abono( !request.getParameter("c_valor_abono"+i).equals("")?Double.parseDouble(request.getParameter("c_valor_abono"+i).replaceAll(",","").replaceAll(",","") ):0 );
                        item.setValor_retefuente( retefuente );
                        item.setValor_reteica( reteica );
                        item.setValor_total_factura( total );
                        item.setTipo_aux( !request.getParameter("tipo"+i).equals("")?request.getParameter("tipo"+i):"" );
                        item.setAuxiliar( !request.getParameter("auxiliar"+i).equals("")?request.getParameter("auxiliar"+i):"" );
                        item.setValor_saldo_factura_me( Double.parseDouble( request.getParameter("c_saldo_factura_ori"+i)!=null?request.getParameter("c_saldo_factura_ori"+i).replaceAll(",","") :"0" ) );
                        item.setValor_saldo_factura( Double.parseDouble( request.getParameter("c_valor_saldo"+i)!=null?request.getParameter("c_valor_saldo"+i).replaceAll(",","") :"0" ) );
                        cabecera.setValor_tasa( valor_tasa );
                    }
                    else
                        request.setAttribute("mensaje","No hay conversión actual en la tasa de la moneda "+ item.getMoneda_factura() +" a la moneda "+ cabecera.getMoneda());
                }
                cabecera.setValor_total( valor_total );
                if( existe_cuentas ){
                    if( cabecera.getValor_ingreso_me() >= valor_total ){
                        boolean swError = false;
                        double nuevo = valor_total;
                        if( ( cabecera.getNumero_ingreso().indexOf("IC") == -1 && cabecera.getTipo_documento().equals("ING") ) || ( cabecera.getNumero_ingreso().indexOf("NC") == -1 && cabecera.getTipo_documento().equals("ICR") ) ){
                            try{
                                //ingresar cabacera
                                Ingreso ingres = new Ingreso();
                                ingres.setDstrct( distrito );
                                ingres.setReg_status( "" );
                                ingres.setCodcli( cabecera.getCliente() );
                                ingres.setNitcli( cabecera.getNit_cliente() );
                                ingres.setConcepto( cabecera.getConcepto() );
                                ingres.setTipo_ingreso( "C" );
                                ingres.setFecha_consignacion( cabecera.getFecha_consignacion() );
                                ingres.setFecha_ingreso( "now()" );
                                ingres.setBranch_code( cabecera.getBanco() );
                                ingres.setBank_account_no( cabecera.getSucursal() );
                                ingres.setCodmoneda( cabecera.getMoneda() );
                                ingres.setAgencia_ingreso( usuario.getId_agencia() );
                                ingres.setPeriodo( "000000" );
                                ingres.setCant_item( cabecera.getCantidad_item() );
                                ingres.setBase( usuario.getBase() );
                                ingres.setTransaccion( 0 );
                                ingres.setTransaccion_anulacion( 0 );
                                ingres.setTasaDolBol( cabecera.getTasaDB() );
                                ingres.setTipo_documento( cabecera.getTipo_documento() );
                                ingres.setCreation_user( usuario.getLogin() );
                                ingres.setCreation_date( "now()" );
                                ingres.setLast_update( "now()" );
                                ingres.setNum_ingreso( cabecera.getNumero_ingreso() );
                                ingres.setUser_update( usuario.getLogin() );
                                ingres.setDescripcion_ingreso( cabecera.getDescripcion() );
                                ingres.setNro_consignacion( cabecera.getNumero_consignacion() );
                                ingres.setCuenta( cabecera.getCuenta() );
                                if( cabecera.getTipo_aux() != null && cabecera.getAuxiliar() != null && ( !cabecera.getTipo_aux().equals("") || !cabecera.getAuxiliar().equals("") ))
                                    ingres.setAuxiliar( cabecera.getTipo_aux()+"-"+cabecera.getAuxiliar() );
                                else
                                    ingres.setAuxiliar( "" );
                                ingres.setVlr_ingreso( valor_total * Double.parseDouble( request.getParameter("vlr_tasa")!=null?request.getParameter("vlr_tasa").replaceAll(",",""):"1"  ) );
                                ingres.setVlr_ingreso_me( valor_total );
                                ingres.setVlr_tasa( cabecera.getVlr_tasa() );
                                ingres.setFecha_tasa( cabecera.getFecha_tasa() );
                                ingres.setVlr_saldo_ing( 0 );
                                ingres.setAbc( cabecera.getAbc() );
                                nuevo = 0;
                                consultas.add( model.ingresoService.insertarIngreso(ingres) );
                                //consultas.add( model.ingreso_detalleService.updateSaldoIngreso( valor_total, distrito, cabecera.getTipo_documento(), cabecera.getNumero_ingreso() ) );
                            }catch(Exception ex){
                                ex.printStackTrace();
                                swError = true;
                                request.setAttribute("mensaje","Se ha presentado un problema al intentar grabar en la BD");
                            }
                        }
                        consultas.add( model.ingreso_detalleService.updateCantidadItemsIngreso( k, cabecera.getNumero_ingreso(), distrito, "C", cabecera.getTipo_documento() ) );
                        consultas.add( model.ingreso_detalleService.updateSaldoConsignacionIngreso( distrito, cabecera.getTipo_documento(), cabecera.getNumero_ingreso() ) );
                        if(!swError){
                            model.despachoService.insertar( consultas );
                            request.setAttribute("mensaje","Se ha registrado el Ingreso satisfactoriamente");
                            //model.ingreso_detalleService.resetVecIngreso_detalle();
                            next +="?fin=ok";
                            session.setAttribute("fin", "ok");
                            //eliminar el archivo de texto
                            try{
                                model.ingreso_detalleService.borrarArchivo( "ingreso_detalle"+usuario.getLogin()+".txt", usuario.getLogin() );
                            }catch(Exception ex){
                                request.setAttribute("mensaje","Error no se pudo borrar el archivo");
                            }
                            boolean mon_ext = false;
                            //verifico si hay moneda diferente
                            for (int i=0; i < model.ingreso_detalleService.getVecIngreso_detalle().size(); i++){
                                Ingreso_detalle item = (Ingreso_detalle) model.ingreso_detalleService.getVecIngreso_detalle().elementAt(i);
                                if( !item.getMoneda_factura().equals( cabecera.getMoneda() ) )
                                    mon_ext = true;
                            }
                            if( mon_ext ){
                                cabecera.setValor_tasa( request.getParameter("vlr_tasa")!=null ? Double.parseDouble( request.getParameter("vlr_tasa").replaceAll(",","") ) : 0 );
                            }
                            else{
                                cabecera.setValor_tasa( 0 );
                            }
                            
                            /**
                             * este fragmento de codigo puede cambiar en el
                             * futuro cuando hcuello haga aplicacion de pagos**
                             */
                            //aqui vamos a lanzar el metodo de la nota de reestructuracion
                            if (cabecera.getNro_extracto() != 0) {
                                ReestructurarNegociosDAO dao = new ReestructurarNegociosImpl(usuario.getBd());
                                String crearNotaAjusteFenalco = dao.crearNotaAjusteFenalco(cabecera.getNro_extracto(), usuario);
                                if (crearNotaAjusteFenalco.equals("OK")) {
                                    request.setAttribute("mensaje", "Se ha registrado el Ingreso satisfactoriamente y se generaron los ajustes y notas del proceso de reestruracion");
                                } else if (crearNotaAjusteFenalco.equals("FAIL")) {
                                    request.setAttribute("mensaje", "Se ha registrado el Ingreso satisfactoriamente, pero no se genero la nota de reestruracion. !!!");
                                } else {
                                    request.setAttribute("mensaje", "Se ha registrado el Ingreso satisfactoriamente\n Error :" + crearNotaAjusteFenalco);
                                }
                            }
                            /**Fin***/
                        }
                    }
                    else
                        request.setAttribute("mensaje","No se puede realizar el ingreso, el total acumulado de las facturas debe ser menor o igual al valor de la consignación");
                }
                else
                    request.setAttribute("mensaje","No se puede realizar el ingreso por que. "+msg);
            }
            //carga los valos seleccionados de la lista de facturas del cliente
            else if(opcion.equals("5")){
                ///next = "/jsp/cxcobrar/ingreso_detalle/ItemsIngreso.jsp?cerrar=ok";
                boolean sw = false, existe_tasa = false, mas_moneda = false, mon_ext = false;
                String []textbox = request.getParameterValues("checkbox");
                String mon = "", mensaje = "", msg = "";
                double total = 0, valor_total = 0, valor_tasa = 0, ntasa = 0;
                int k = 0, cont =0;
                Vector newvec = new Vector();
                Ingreso_detalle ingreso = new Ingreso_detalle();
                //objeto de cabecera
                Ingreso_detalle cabecera = model.ingreso_detalleService.getCabecera();
                Vector vec = model.ingreso_detalleService.getVecFactura();
                Vector a = model.ingreso_detalleService.getVecIngreso_detalle();
                String moneda_local = model.ingreso_detalleService.monedaLocal(distrito);
                k = a==null?0:a.size();
                //valido que no existan mas de una moneda diferente a la moneda del ingreso
                if(textbox!=null){
                    for(int j=0; j<textbox.length; j++){
                        ingreso = (Ingreso_detalle) vec.get( Integer.parseInt( textbox[j] ) );
                        if(!ingreso.getMoneda_factura().equals(cabecera.getMoneda())){
                            if(cont>0){
                                if( !mon.equals(cabecera.getMoneda()) && !mon.equals(ingreso.getMoneda_factura()) ){
                                    mas_moneda = true;
                                }
                                else{
                                    mon = ingreso.getMoneda_factura();
                                    cont++;
                                }
                            }
                            else{
                                mon = ingreso.getMoneda_factura();
                                cont++;
                            }
                        }
                    }
                    if(mas_moneda){
                        next = "/jsp/cxcobrar/ingreso_detalle/ItemsIngreso.jsp";
                        request.setAttribute("mensaje","Solo debe chequear una moneda diferente a la moneda del ingreso!");
                    }
                    else{
                        //valido si existe moneda extranjera y si hay valor de conversion
                        if(!mon.equals("")){
                            if( request.getParameter("vlr_tasa")!=null && Double.parseDouble( request.getParameter("vlr_tasa").replaceAll(",","") ) > 0 ){
                                valor_tasa =  Double.parseDouble( request.getParameter("vlr_tasa").replaceAll(",","") );
                                existe_tasa = true;
                            }
                            else
                                if( cabecera.getMoneda().equals("DOL") && mon.equals("BOL") ){
                                    if( cabecera.getTasaDB() > 0 ){
                                        valor_tasa = Double.parseDouble( UtilFinanzas.customFormat("#,###.##########", 1 / cabecera.getTasaDB() ,10 ).replaceAll(",","") );
                                        existe_tasa = true;
                                    }
                                    else
                                        existe_tasa = false;
                                }
                                else if( cabecera.getMoneda().equals("BOL") && mon.equals("DOL") ){
                                    if( cabecera.getTasaDB() > 0 ){
                                        valor_tasa = Double.parseDouble( UtilFinanzas.customFormat("#,###.##########", cabecera.getTasaDB() ,10 ).replaceAll(",","") );
                                        existe_tasa = true;
                                    }
                                    else
                                        existe_tasa = false;
                                }
                                else{
                                    try{
                                        //moneda local, moneda origen, moneda destino
                                        Tasa t = model.tasaService.buscarValorTasa( moneda_local, mon, cabecera.getMoneda(), cabecera.getFecha_consignacion() );
                                        if(t!=null){
                                            valor_tasa = Double.parseDouble( UtilFinanzas.customFormat("#,###.##########",  t.getValor_tasa() ,10 ).replaceAll(",","") );
                                            existe_tasa = true;
                                        }
                                        else
                                            existe_tasa = false;
                                    }catch(Exception ex){
                                        ex.getMessage();
                                    }
                                }
                        }
                        else{
                            valor_tasa = 1;
                            existe_tasa = true;
                        }
                        ntasa = valor_tasa;
                        valor_total = 0;
                        boolean swMayor = false, Vec_anulados = false;
                        Vector VecTemp = new Vector();
                        for (int i=0; i < k; i++){
                            Vec_anulados = false;
                            Ingreso_detalle item = (Ingreso_detalle)a.elementAt(i);
                            for(int j=0; j<textbox.length; j++){
                                Ingreso_detalle in = (Ingreso_detalle) vec.get( Integer.parseInt( textbox[j] ) );
                                if ( ( item.getFactura().equals( in.getFactura() )  || item.getFactura().equals( "" ) ) && Vec_anulados == false ){
                                    Vec_anulados = true;
                                }
                            }
                            if( Vec_anulados ){
                                VecTemp.add( item );
                            }
                        }
                        model.ingreso_detalleService.resetVecIngreso_detalle();
                        model.ingreso_detalleService.setVectorIngreso_detalle( VecTemp );
                        VecTemp = null;
                        //verifico que exista la factura en los seleccionados
                        for (int i=0; i < model.ingreso_detalleService.getVecIngreso_detalle().size(); i++){
                            Ingreso_detalle item = (Ingreso_detalle) model.ingreso_detalleService.getVecIngreso_detalle().elementAt(i);
                            if( cabecera.getValor_ingreso_me() - valor_total > 0 ){
                                if( item.getValor_abono() > cabecera.getValor_ingreso_me() - valor_total ){
                                    item.setValor_abono( Util.roundByDecimal( cabecera.getValor_ingreso_me() - valor_total + item.getValor_retefuente(), 2 ) );
                                }
                                item.setValor_total_factura( item.getValor_saldo_factura() - item.getValor_abono() );
                                valor_total += item.getValor_abono() - ( item.getValor_reteica() + item.getValor_retefuente() );
                            }
                            else{
                                swMayor = true;
                                request.setAttribute("mensaje","No se puede agregar la factura "+ingreso.getFactura()+" por que el total acumulado facturas supera el valor de consignación");
                            }
                        }
                        //datos chequeados de los items
                        for(int j=0; j<textbox.length; j++){
                            ingreso = (Ingreso_detalle) vec.get( Integer.parseInt( textbox[j] ) );
                            if( ingreso.getMoneda_factura().equals(cabecera.getMoneda() ) )
                                valor_tasa = 1;
                            else
                                valor_tasa = ntasa;
                            sw = false;
                            //verifico que exista la factura en los seleccionados
                            for (int i=0; i < model.ingreso_detalleService.getVecIngreso_detalle().size(); i++){
                                Ingreso_detalle item = (Ingreso_detalle) model.ingreso_detalleService.getVecIngreso_detalle().elementAt(i);
                                if( item.getFactura().equals( ingreso.getFactura() ) && sw == false )
                                    sw = true;
                            }
                            if(!sw){
                                //verifico si existe la tasa
                                if(existe_tasa){
                                   // if( cabecera.getValor_ingreso_me() - valor_total > 0 ){
                                        // verificacion de cuentas
                                        if( !ingreso.getCuenta().equals("") ){
                                            com.tsp.finanzas.contab.model.beans.PlanDeCuentas plan_cuenta = modelcontab.planDeCuentasService.consultaCuentaModulo( usuario.getDstrct(), ingreso.getCuenta(), 3);
                                            mensaje = "";
                                            if(plan_cuenta!=null){
                                                //if(plan_cuenta.getDetalle().equalsIgnoreCase("S") &&  plan_cuenta.getPerteneceAmodulo().equalsIgnoreCase("S") ){
                                                if(plan_cuenta.getSubledger().equalsIgnoreCase("S")){
                                                    LinkedList t_aux = new LinkedList();
                                                    try{
                                                        modelcontab.subledgerService.buscarCuentasTipoSubledger( ingreso.getCuenta() );
                                                        t_aux = modelcontab.subledgerService.getCuentastsubledger();
                                                    }catch(Exception e){
                                                        t_aux = null;
                                                    }
                                                    ingreso.setTipos( t_aux );
                                                }else{
                                                    modelcontab.subledgerService.setCuentastsubledger(null);
                                                    ingreso.setTipos( null );
                                                }
                                                /*}
                                                if(!plan_cuenta.getPerteneceAmodulo().equalsIgnoreCase("S")){
                                                    mensaje +=" La cuenta no pertence a este modulo";
                                                }*/
                                            }
                                            else{
                                                modelcontab.subledgerService.setCuentastsubledger(null);
                                                mensaje =" La cuenta "+ingreso.getCuenta()+" no existe";
                                            }
                                        }
                                        else{
                                            mensaje += " no tiene CMC o no se encuentra registrado y por lo tanto no presenta cuenta";
                                        }
                                        if( !mensaje.equals("") ){
                                            msg += " En la factura "+ingreso.getFactura()+mensaje+", ";
                                            swMayor = true;
                                        }
                                        //***********************
                                        ingreso.setSal_fact_mlocal( ingreso.getValor_saldo_factura() );
                                        ingreso.setCodigo_retefuente( cabecera.getCodigo_retefuente() );
                                        ingreso.setCodigo_reteica( "" );
                                        ingreso.setValor_factura( Util.roundByDecimal( ingreso.getValor_factura_me() * valor_tasa, 2 ) );
                                        ingreso.setValor_abono( Util.roundByDecimal( ingreso.getValor_saldo_factura_me()  * valor_tasa, 2 ) );
                                        ingreso.setValor_saldo_factura( Util.roundByDecimal( ingreso.getValor_saldo_factura_me() * valor_tasa, 2 ) );
                                        ingreso.setValor_retefuente( Util.roundByDecimal( ( ingreso.getValor_factura_me() * valor_tasa ) * ( cabecera.getPorcentaje_rfte()/100 ), 2 ) );
                                        ingreso.setValor_reteica( 0 );
                                        ingreso.setValor_retefuente_me( Util.roundByDecimal( ingreso.getValor_saldo_factura_me() * ( cabecera.getPorcentaje_rfte()/100 ), 2) );
                                        ingreso.setValor_reteica_me( 0 );
                                        if( ingreso.getValor_abono() > cabecera.getValor_ingreso_me() - valor_total ){
                                            ingreso.setValor_abono( Util.roundByDecimal( cabecera.getValor_ingreso_me() - valor_total + ingreso.getValor_retefuente(), 2 ) );
                                        }
                                        ingreso.setValor_total_factura( ingreso.getValor_saldo_factura() - ingreso.getValor_abono() );
                                        valor_total += ingreso.getValor_abono() - ( ingreso.getValor_reteica() + ingreso.getValor_retefuente() );
                                        newvec.add( ingreso );
                                        /*if( cabecera.getValor_ingreso_me() - valor_total < 0 ){
                                            swMayor = true;
                                            request.setAttribute("mensaje","No se puede agregar la factura "+ingreso.getFactura()+" por que el total acumulado facturas supera el valor de consignación");
                                        }
                                    }
                                    else{
                                        swMayor = true;
                                        request.setAttribute("mensaje","No se puede agregar la factura "+ingreso.getFactura()+" por que el total acumulado facturas supera el valor de consignación");
                                    }*/
                                }
                                else{
                                    request.setAttribute("mensaje","No hay conversión actual en la tasa de la moneda " +mon+ " a la moneda "+cabecera.getMoneda());
                                    next = "/jsp/cxcobrar/ingreso_detalle/ItemsIngreso.jsp";
                                }
                            }
                        }
                        if( !swMayor ){
                            //Adiciono los nuevos items que seleccionaron
                            for(int i=0; i< newvec.size(); i++){
                                Ingreso_detalle ingre = (Ingreso_detalle) newvec.get(i);
                                model.ingreso_detalleService.setVecIngreso_detalle( ingre );
                            }
                            newvec = null;
                            cabecera.setValor_total(valor_total);
                            //verifico si hay moneda diferente
                            for (int i=0; i < model.ingreso_detalleService.getVecIngreso_detalle().size(); i++){
                                Ingreso_detalle item = (Ingreso_detalle) model.ingreso_detalleService.getVecIngreso_detalle().elementAt(i);
                                if( !item.getMoneda_factura().equals( cabecera.getMoneda() ) )
                                    mon_ext = true;
                            }
                            if( mon_ext ){
                                cabecera.setValor_tasa( request.getParameter("vlr_tasa")!=null ? Double.parseDouble( request.getParameter("vlr_tasa").replaceAll(",","") ) : ntasa );
                            }
                            else{
                                cabecera.setValor_tasa( 0 );
                            }
                            next = "/jsp/cxcobrar/ingreso_detalle/ItemsIngreso.jsp?cerrar=ok";
                        }
                        else
                            next = "/jsp/cxcobrar/ingreso_detalle/ItemsIngreso.jsp";
                    }
                }
                else{
                    next = "/jsp/cxcobrar/ingreso_detalle/ItemsIngreso.jsp";
                    request.setAttribute("mensaje","Debe seleccionar una factura!");
                }
                if( !msg.equals("") )
                    request.setAttribute("mensaje", msg);
            }
            
            //carga los valos seleccionados de la lista de facturas del cliente
            else if(opcion.equals("6")){
                String mon = "", mensaje = "";
                double total = 0, valor_total = 0, valor_tasa = 0;
                Vector newvec = new Vector();
                //objeto de cabecera
                Ingreso_detalle cabecera = model.ingreso_detalleService.getCabecera();
                //Vector vec = model.ingreso_detalleService.getVecFactura();
                Vector a = model.ingreso_detalleService.getVecIngreso_detalle();
                String moneda_local = model.ingreso_detalleService.monedaLocal(distrito);
                boolean mon_ext = false;
                valor_tasa = cabecera.getValor_tasa();
                // verificacion de cuentas
                Ingreso_detalle ing_det = new Ingreso_detalle();
                ing_det.setFactura( "" );
                ing_det.setCuenta( request.getParameter("cuenta1")!=null?request.getParameter("cuenta1"):"" );
                ing_det.setTipo_aux( request.getParameter("tipo1")!=null?request.getParameter("tipo1"):"" );
                ing_det.setAuxiliar( request.getParameter("auxiliar1")!=null?request.getParameter("auxiliar1"):"" );
                if( !ing_det.getCuenta().equals("") ){
                    com.tsp.finanzas.contab.model.beans.PlanDeCuentas plan_cuenta = modelcontab.planDeCuentasService.consultaCuentaModulo( usuario.getDstrct(), ing_det.getCuenta(), 3);
                    mensaje = "";
                    if(plan_cuenta!=null){
                        if(plan_cuenta.getDetalle().equalsIgnoreCase("S") &&  plan_cuenta.getPerteneceAmodulo().equalsIgnoreCase("S") ){
                            if(plan_cuenta.getSubledger().equalsIgnoreCase("S")){
                                LinkedList t_aux = new LinkedList();
                                try{
                                    modelcontab.subledgerService.buscarCuentasTipoSubledger( ing_det.getCuenta() );
                                    t_aux = modelcontab.subledgerService.getCuentastsubledger();
                                }catch(Exception e){
                                    t_aux = null;
                                }
                                ing_det.setTipos( t_aux );
                                
                                if(ing_det.getTipo_aux().equals("")){
                                    mensaje +=" requiere de tipo cuenta";
                                    if(ing_det.getAuxiliar().equals("")){
                                        mensaje +=" y requiere de auxiliar";
                                    }
                                }
                                else{
                                    if(ing_det.getAuxiliar().equals("")){
                                        mensaje +=" requiere de auxiliar";
                                    }
                                }
                            }else{
                                modelcontab.subledgerService.setCuentastsubledger(null);
                                ing_det.setTipos( null );
                            }
                        }
                        if(!plan_cuenta.getPerteneceAmodulo().equalsIgnoreCase("S")){
                            mensaje +=" La cuenta no pertence a este modulo";
                        }
                    }
                    else{
                        modelcontab.subledgerService.setCuentastsubledger(null);
                        mensaje =" La cuenta "+ing_det.getCuenta()+" no existe";
                    }
                }
                else{
                    mensaje += " no tiene CMC o no se encuentra registrado y por lo tanto no presenta cuenta";
                }
                if( !mensaje.equals("") )
                    mensaje = " En la factura "+ing_det.getFactura()+mensaje+", ";
                
                double totales = !request.getParameter("total").equals("")?Double.parseDouble( request.getParameter("total").replaceAll(",","") ):0;
                ing_det.setValor_saldo_factura_me( totales );
                ing_det.setValor_factura( Util.roundByDecimal( totales, 2 ) );
                ing_det.setValor_saldo_factura( totales );
                ing_det.setValor_abono( totales );
                ing_det.setValor_retefuente_me( 0 );
                ing_det.setValor_reteica_me( 0 );
                ing_det.setValor_retefuente( 0 );
                ing_det.setValor_reteica( 0 );
                ing_det.setValor_total_factura( 0 );
                ing_det.setDescripcion_factura("Ajuste saldo al ingreso Nro. "+cabecera.getNumero_ingreso());
                ing_det.setMoneda_factura( cabecera.getMoneda() );
                ing_det.setFecha_factura( Util.getFechaActual_String(4) );
                ing_det.setValor_factura_me( totales );
                //***********************
                ing_det.setSal_fact_mlocal( totales );
                ing_det.setCodigo_retefuente( "" );
                ing_det.setCodigo_reteica( "" );
                //model.ingreso_detalleService.setVecIngreso_detalle( ing_det );
                newvec.add( ing_det );
                
                //valido que no entro a alguna exepcion
                if( mensaje.equals("") ){
                    //se recorre el nuevo vector para adicionarlo a vector de los items
                    for(int i=0; i< newvec.size(); i++){
                        Ingreso_detalle ingre = (Ingreso_detalle) newvec.get(i);
                        model.ingreso_detalleService.setVecIngreso_detalle( ingre );
                        if( !cabecera.getMoneda().equals( ingre.getMoneda_factura() ) )
                            mon_ext = true;
                    }
                    valor_total = 0;
                    //saco el valor total de los items
                    for( int m = 0 ; m < a.size() ; m++ ){
                        Ingreso_detalle ingre = (Ingreso_detalle) a.get(m);
                        ingre.setValor_total_factura( ingre.getValor_saldo_factura() - ingre.getValor_abono() );
                        valor_total += ingre.getValor_abono() - ( ingre.getValor_reteica() + ingre.getValor_retefuente() );
                    }
                    cabecera.setValor_total(valor_total);
                    next = "/jsp/cxcobrar/ingreso_detalle/AjusteIngreso.jsp?cerrar=ok";
                }
                else{
                    next = "/jsp/cxcobrar/ingreso_detalle/AjusteIngreso.jsp";
                    request.setAttribute("mensaje", mensaje);
                }
            }
            
            if (opcion.equals("imp")){
                try{
                    Ingreso_detalle cabecera = model.ingreso_detalleService.getCabecera();
                    if( cabecera.getTipo_documento().equals("ING") ){
                        IngresoPDF ingreso = new IngresoPDF();
                        ingreso.RemisionPlantilla();
                        ingreso.crearCabecera();
                        ingreso.crearRemision(model, usuario.getDstrct(), cabecera.getNumero_ingreso(), cabecera.getTipo_documento(), "items", usuario.getLogin() );
                        ingreso.generarPDF();
                        next = "/pdf/IngresoPDF.pdf";
                    }
                    else{
                        NotaCreditoPDF nota = new NotaCreditoPDF();
                        nota.RemisionPlantilla();
                        nota.crearCabecera();
                        nota.crearRemision(model, usuario.getDstrct(), cabecera.getNumero_ingreso(), cabecera.getTipo_documento(), "items", usuario.getLogin() );
                        nota.generarPDF();
                        next = "/pdf/NotaCreditoPDF.pdf";
                    }
                    
                }catch(Exception ex){
                    ex.getMessage();
                    ex.printStackTrace();
                }
                
            }
            
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
    
}
