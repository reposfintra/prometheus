/*
 * Nombre        PerfilAgregarAction.java
 * Autor         Ing. Sandra M. Escalante G.
 * Fecha         10 de marzo de 2005, 03:48 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Administrador
 */
public class PerfilAgregarAction extends Action {
    
    /** Creates a new instance of PerfilAgregarAction */
    public PerfilAgregarAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException{        
        String idp = request.getParameter("idp").toUpperCase();        
        String nom = request.getParameter("nom").toUpperCase();
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");        
        String next= request.getParameter("carpeta") + "/" + request.getParameter("pagina");
        String []opciones = request.getParameterValues("opciones");
        
        try{             
            Perfil prf = new Perfil();
            prf.setId(idp);
            prf.setNombre(nom);
            prf.setUser_update(usuario.getLogin());            
            prf.setCreado_por(usuario.getLogin());
            
            try{
                model.perfilService.agregarPerfil(prf);
                
                model.menuService.obtenerTOpciones();
                Vector vopcionesbd = model.menuService.getVMenu(); 
        
                for( int i=0; i< opciones.length;i++){                    
                    ///agrego opcion a po
                    model.perfilService.agregarPerfilOpcion(idp, Integer.parseInt(opciones [i]), usuario.getLogin());
                    //obtengo opcion
                    model.menuService.obtenerOpcion(Integer.parseInt(opciones[i]));
                    Menu op = model.menuService.getMenuOpcion();
                    //verifico si padre ya esta agregado en po
                    int padre = op.getIdpadre();
                    //if( !model.perfilService.estaPerfilOpcion(idp, padre)){
                        while ( padre != 0 ){
                            try{
                                model.perfilService.agregarPerfilOpcion(idp, padre, usuario.getLogin()); 
                                //obtengo nueva opcion (padre)
                            }catch(SQLException ex){
                                ////System.out.println("YA EXISTE");
                            }
                            model.menuService.obtenerOpcion(padre);
                            padre = model.menuService.getMenuOpcion().getIdpadre();
                        }
                    //}
                }
                ////System.out.println("obj creado");
                next = next + "?tipo=Insertar&msg=Perfil agregado exitosamente!";
            } catch(SQLException ex ){  
                if (model.perfilService.existePerfilxPropietarioAnulado(idp)){
                    model.perfilService.activarPerfil(prf);
                    
                    //obtener perfil_ocion con perfil dado
                    model.perfilService.listarPOxPerfil(idp);
                    Vector vpo = model.perfilService.getVPerfil();
                    
                    //Agrego opciones que se encuentran en []opciones y no en PO
                    for ( int j = 0; j < opciones.length; j++){
                        int swesta = 0;
                                             
                        for ( int i = 0; i<vpo.size(); i++ ){
                            PerfilOpcion po = (PerfilOpcion) vpo.elementAt(i);                             
                            if (Integer.parseInt(opciones[j]) == po.getId_opcion()){
                                //ACTIVO OP DE PO 
                                model.perfilService.activarPerfilOpcion(usuario.getLogin(), idp, po.getId_opcion());
                                //obtengo opcion
                                model.menuService.obtenerOpcion(po.getId_opcion());
                                Menu op = model.menuService.getMenuOpcion();
                                int padre = op.getIdpadre();
                                //verifico si padre ya esta en po y esta anulado
                                if( model.perfilService.existePerfilOpcionAnulado(idp, padre)){    
                                    while ( padre != 0 ){
                                        //Activo al padre
                                        model.perfilService.activarPerfilOpcion(usuario.getLogin(), idp, padre);
                                        //obtengo nueva opcion (padre)
                                        model.menuService.obtenerOpcion(padre);
                                        padre = model.menuService.getMenuOpcion().getIdpadre();
                                    }
                                }
                                swesta = 1;
                            }
                            
                        }
                        
                        if ( swesta == 0 ){
                            //AGREGAR OP A PO
                            model.perfilService.agregarPerfilOpcion(idp, Integer.parseInt(opciones[j]), usuario.getLogin());
                            //obtengo opcion
                            model.menuService.obtenerOpcion(Integer.parseInt(opciones[j]));
                            Menu op = model.menuService.getMenuOpcion();
                            //verifico si padre ya esta agregado en po
                            int padre = op.getIdpadre();
                            while ( padre != 0 ){
                                try {                                
                                    model.perfilService.agregarPerfilOpcion(idp, padre, usuario.getLogin()); 
                                }
                                catch(SQLException exe){
                                    model.perfilService.activarPerfilOpcion(usuario.getLogin(), idp, padre);
                                }                                
                                model.menuService.obtenerOpcion(padre);
                                padre = model.menuService.getMenuOpcion().getIdpadre();
                            }
                        }
                        
                    }
                    //////System.out.println("obj activado");
                    next = next + "?tipo=Insertar&msg=Perfil agregado exitosamente!";
                }
                else{
                    //////System.out.println("existe perfil");   
                    next = next + "?tipo=Insertar&msg=Error al insertar perfil " + request.getParameter("idp") + " ya existe!";
                }
                
            }               
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}//end class