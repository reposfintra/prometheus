/******************************************************************************
 * Nombre clase :                   PdfReporteAction.java                     *
 * Descripcion :                    Clase que maneja los eventos              *
 *                                  relacionados con el programa de           *
 *                                  Impresion de Reporte Factura Destinatario *
 * Autor :                          LREALES                                   *
 * Fecha :                          03 de abril de 2006, 03:50 PM             *
 * Version :                        1.0                                       *
 * Copyright :                      Fintravalores S.A.                   *
 *****************************************************************************/

package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import com.tsp.pdf.*;
import java.io.*;

public class PdfReporteAction extends Action{
    
    /** Creates a new instance of HojaReporteFacturaDestinatarioPDF */
    public PdfReporteAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        
        try{
            
            HojaReportes hojaReportes = model.reporteFacDesService.getHojaReportes();
            
            String ruta = application.getRealPath("/");
            File xslt = new File(ruta + "Templates/reporteFacturaDestinatario.xsl");
            File pdf = new File(ruta + "pdf/hrfd.pdf");
            
            Vector info = null;
            
            if ( "PLANILLA".equals( model.reporteFacDesService.getHojaReportes().getTipo() ) )
                info = model.reporteFacDesService.getVectorPlanilla();
            
            if ( "REMESA".equals( model.reporteFacDesService.getHojaReportes().getTipo() ) )
                info = model.reporteFacDesService.getVectorRemesa();
            
            HojaReporteFacturaDestinatarioPDF Hoja_Reporte = new HojaReporteFacturaDestinatarioPDF(); 
            Hoja_Reporte.generarPDF ( xslt, pdf, info );
            
            String next = "/pdf/hrfd.pdf";
            
            RequestDispatcher rd = application.getRequestDispatcher( next );
            
            if( rd == null )
                throw new ServletException( "No se pudo encontrar " + next );
            
            rd.forward( request, response );
            
        } catch( Exception e ){
            
            e.printStackTrace();
            throw new ServletException( "Error en REPORTE PDF .....\n" + e.getMessage() );
        
        }
    }    
}