/*******************************************************************
 * Nombre clase: ClienteBuscarAction.java
 * Descripci�n: Accion para buscar una placa de la bd.
 * Autor: Ing. fily fernandez
 * Fecha: 16 de febrero de 2006, 05:41 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import com.tsp.exceptions.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import java.util.Vector;
import java.lang.*;
import java.sql.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import com.tsp.operation.model.services.*;
/**
 *
 * @author  jdelarosa
 */
public class CorridasReporteAction extends Action{
    
    /** Creates a new instance of PlacaBuscarAction */
    public CorridasReporteAction () {
    }
    
    public void run () throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "/jsp/finanzas/reportes/DetalleConsultas.jsp";
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario)session.getAttribute ("Usuario");
        String usu = usuario.getLogin();
        String Corridas = request.getParameter ("Corridas")!=null?request.getParameter ("Corridas"):"";
        String Propietario = request.getParameter ("Propietario")!=null?request.getParameter ("Propietario"):"";
        String Cedula = request.getParameter ("Cedula")!=null?request.getParameter ("Cedula"):"";
        String Factura = request.getParameter ("Factura")!=null?request.getParameter ("Factura"):"";
        String Tipo_factura = request.getParameter ("Tipo_factura")!=null?request.getParameter ("Tipo_factura"):"";
        String opcion = request.getParameter ("opcion")!=null?request.getParameter ("opcion"):"";
        String val = request.getParameter ("val")!=null?request.getParameter ("val"):"";
        model.corridaService.setCorrida(Corridas);
        model.corridaService.setPropietario(Propietario);
       
        try {
           Vector datos = model.corridaService.getVector();
           Vector datos2 = model.corridaService.getVectorValor();
           if ( opcion.equals("3") ){
                  next = "/controller?estado=Factura&accion=Detalle&documento="+Factura+"&tipo_doc="+Tipo_factura+"&prov="+Cedula+"";
           }
           if (opcion.equals("2") ){
               HReporteCorridasTranferencias cli = new HReporteCorridasTranferencias();
               cli.start(Corridas,Propietario,usu); 
                if (val.equals("1")){
                    next = "/jsp/finanzas/reportes/DetalleConsultasP.jsp"; 
                }else {
                    next = "/jsp/finanzas/reportes/DetalleConsultas.jsp"; 
                }
            }
           if ( opcion.equals("1") ){
                if(Propietario.equals("") && !Corridas.equals("")){
                   model.corridaService.busquedaXCorridas(Corridas);
                   datos = model.corridaService.getVector();
                }   
                
                if(!Propietario.equals("") && Corridas.equals("")){ 
                   model.corridaService.busquedaXPropietario(Propietario);
                   datos = model.corridaService.getVector();
                    /*valor total*/
                   /*****************************************************/
                   model.corridaService.SumaValorCorridas(Propietario);
                   datos2 = model.corridaService.getVectorValor();
                   next = "/jsp/finanzas/reportes/DetalleConsultasP.jsp"; 
                  
                }
                
                if(!Propietario.equals("") && !Corridas.equals("")){
                    System.out.println("entro 1");
                    model.corridaService.cxpro(Propietario,Corridas);
                   datos = model.corridaService.getVector();
                    /*valor total*/
                   /*****************************************************/
                   model.corridaService.SumaValorCorridas(Propietario);
                   datos2 = model.corridaService.getVectorValor();
                   next = "/jsp/finanzas/reportes/DetalleConsultasP.jsp"; 
                   
                }
                
            } 
            if(datos.size() <= 0){
                        next = next + "?msg=Su busqueda no arrojo resultados!";
           }

            
        } catch ( Exception e ) {
            
            throw new ServletException ( "Error en Reporte de CorridasReporteAction : " + e.getMessage() );
            
        }        
        
        this.dispatchRequest ( next );
        
    }
    
}
