/******************************************************************
* Nombre                        FitmenManagerAction.java
* Descripci�n                   Clase Action para la tabla fitmen
* Autor                         ricardo rosero
* Fecha                         10/01/2006
* Versi�n                       1.0
* Coyright                      Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  rrosero
 */
public class FitmenManagerAction extends Action {
    
    /** Creates a new instance of FitmenAction */
    public FitmenManagerAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String pag = "/jsp/masivo/reportes/reporteFitmen.jsp";
        
        SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd");
        Calendar FechaHoy = Calendar.getInstance();
        Date d = FechaHoy.getTime();
        String fecha = s.format(d);

        HttpSession session = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        String user = usuario.getLogin();

        String fechai = request.getParameter("fechai");
        String fechaf = request.getParameter("fechaf");

        FitmenTh hilo = new FitmenTh(); 

        Vector informe = new Vector();
        
        try{
            //System.out.println("Voy a ejecutar el service");
            informe = model.fitmenService.obtenerInformeUbicacion();
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }

        hilo.start(informe, usuario.getLogin(), fechai, fechaf); 
        pag += "?mensaje=ReporteFitmen_" + fecha + ".xsl";
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(pag);

    }
    
}
