/********************************************************************
 *      Nombre Clase.................   ProveedorBuscarAction.java
 *      Descripci�n..................   Busca una identidad con el nit ingresado y verifica si existe un proveedor con ese nit.
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   28.09.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import java.util.logging.Level;

/**
 *
 * @author  Andres
 */
public class ProveedorBuscarAction extends Action{

    /** Creates a new instance of ProveedorBuscarAction */
    public ProveedorBuscarAction() {
    }

    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        //Pr�xima vista
        String evento =  request.getParameter("evento") != null ?  request.getParameter("evento"):"" ;

        String next  = "/jsp/cxpagar/proveedor/ProveedorInsert.jsp";
        String sede =(request.getParameter("sede")!=null ? request.getParameter("sede") : "N");
        String nit = request.getParameter("nit");
        String limpiar = request.getParameter("new");
        String pag=request.getParameter("pag");
        //Usuario en sesi�n
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = (String) session.getAttribute("Distrito");

        try{
            model.identidadService.buscarIdentidad(nit, "");
            Identidad id = model.identidadService.obtIdentidad();
            Proveedor prov = null;
            if(limpiar!=null){
                model.proveedorService.setProveedor(null);
                next  = "/jsp/cxpagar/proveedor/Proveedor.jsp";
            }
            else{
                if( id!=null ){

                    String clasificacion = id.getClasificacion();
                    char[] tipo = clasificacion.toCharArray();
                    if( (tipo[2]=='0') && (tipo[3]=='0') && (tipo[4]=='0') ){
                        next = "/jsp/cxpagar/proveedor/Proveedor.jsp?msg=" +
                        "La clasificacion del Nit no corresponde a Proveedor o Propietario.";
                        model.proveedorService.setProveedor(null);
                        model.identidadService.setIdentidad(null);
                        if(evento.equals("consultar")){
                            this.dispatchRequest("/jsp/cxpagar/proveedor/ProveedorConsultar.jsp?msg=La clasificacion del Nit no corresponde a Proveedor o Propietario.");
                        }
                    }
                    else{
                        prov = model.proveedorService.obtenerProveedor(id.getCedula(), (String) session.getAttribute("Distrito"));
                    }
                }
                else{
                    next = "/jsp/cxpagar/proveedor/Proveedor.jsp?msg=" +
                    "No se encuentra registrado el nit digitado.";
                    model.proveedorService.setProveedor(null);
                }

                if( id != null && (prov==null || ( prov!=null && prov.getEstado().compareTo("A")==0)) ){
                    if(pag!=null&&pag.equals("S")){
                        next = "/jsp/cxpagar/proveedor/Proveedor.jsp?msg=" +
                        "El proveedor no existe o se encuentra anualado";
                         model.proveedorService.setProveedor(null);
                    }else{
                        if(pag!=null&&pag.equals("I")&&( prov!=null && prov.getEstado().compareTo("A")==0)){
                            next = "/jsp/hvida/identidad/identidad.jsp?mensaje=" +
                        "MsgErrorAfl";
                         model.proveedorService.setProveedor(null);
                        }else{
                        model.paisservice.buscarpais(id.getCodpais());
                        String pais = (model.paisservice.obtenerpais()!=null)? model.paisservice.obtenerpais().getCountry_name() : "&nbsp;";
                        model.estadoservice.buscarestado(id.getCodpais(), id.getCoddpto());
                        String dpto = (model.estadoservice.obtenerestado()!=null)? model.estadoservice.obtenerestado().getdepartament_name() : "&nbsp;";
                        model.ciudadService.buscarCiudad(id.getCodpais(), id.getCoddpto(), id.getCodciu());
                        String ciudad = (model.ciudadService.obtenerCiudad()!=null)? model.ciudadService.obtenerCiudad().getNomCiu() : "&nbsp;";

                        id.setCiudad_name(ciudad);
                        id.setEstado_name(dpto);
                        id.setPais_name(pais);

                        model.identidadService.setIdentidad(id);

                        model.servicioBanco.setBanco(new TreeMap());
                        model.servicioBanco.setSucursal(new TreeMap());
                        model.agenciaService.loadAgencias();
                        model.ciudadService.searchTreMapCiudades();
                        model.proveedorService.setProveedor(null);
                        //2006-02-17 Diogenes
                        model.tablaGenService.buscarClasificacionPro();
                        model.tablaGenService.buscarHandle_code();
                        model.tablaGenService.buscarBanco();

                        model.tablaGenService.buscarCon_Proveedor();
                        model.tblgensvc.buscarListaSinDato("TCUENTA", "PROVEEDOR");

                        }
                    }
                }
                else if( prov != null && prov.getEstado().compareTo("A")!=0 ){
                    next  = "/jsp/cxpagar/proveedor/ProveedorUpdate.jsp?mensaje=&norefresh=OK";

                    model.paisservice.buscarpais(id.getCodpais());
                    String pais = (model.paisservice.obtenerpais()!=null)? model.paisservice.obtenerpais().getCountry_name() : "&nbsp;";
                    model.estadoservice.buscarestado(id.getCodpais(), id.getCoddpto());
                    String dpto = (model.estadoservice.obtenerestado()!=null)? model.estadoservice.obtenerestado().getdepartament_name() : "&nbsp;";
                    model.ciudadService.buscarCiudad(id.getCodpais(), id.getCoddpto(), id.getCodciu());
                    String ciudad = (model.ciudadService.obtenerCiudad()!=null)? model.ciudadService.obtenerCiudad().getNomCiu() : "&nbsp;";

                    id.setCiudad_name(ciudad);
                    id.setEstado_name(dpto);
                    id.setPais_name(pais);

                    model.identidadService.setIdentidad(id);
                    String agencia = prov.getC_agency_id().equals("OP")?"":prov.getC_agency_id();

                    //model.proveedorService.buscarTiposProveedor();
                    model.servicioBanco.loadBancos(agencia, distrito);
                    model.servicioBanco.loadSucursalesAgencia(agencia, prov.getC_branch_code(), distrito);
                    model.agenciaService.loadAgencias();
                    model.ciudadService.searchTreMapCiudades();
                    //2006-02-17 Diogenes
                    model.tablaGenService.buscarClasificacionPro();
                    model.tablaGenService.buscarHandle_code();
                    model.tablaGenService.buscarBanco();
                    model.tablaGenService.buscarCon_Proveedor();
                    model.tblgensvc.buscarListaSinDato("TCUENTA", "PROVEEDOR");
                    //elavalle 23/04/2007
                    session.setAttribute("agency", prov.getC_agency_id());
                    session.setAttribute("branch_code", prov.getC_branch_code());
                    session.setAttribute("bank_account", prov.getC_bank_account());

                }

            }
            if(model.tablaGenService.obtenerInformacionDato( "USRPROV",usuario.getLogin()) !=null){

                session.setAttribute("Control","S");
            }

            if(evento.equals("consultar")){
                if(prov!=null){
                    next  = "/jsp/cxpagar/proveedor/AprobarProveedor.jsp";
                }else{
                    next  = "/jsp/cxpagar/proveedor/ProveedorConsultar.jsp?msg=El proveedor no existe";
                }
            }

            if(pag!=null&&(pag.equals("S")||pag.equals("I"))){
                if(pag.equals("I")){
                  next=next+"?pag=S&sede="+sede;
                }else{
                    next=next+"&pag=S&sede="+sede;
                }
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(ProveedorBuscarAction.class.getName()).log(Level.SEVERE, null, ex);
        }


        this.dispatchRequest(next);
    }

}
