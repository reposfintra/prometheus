/***********************************************
 * Nombre clase: SancionInsert.java
 * Descripci�n: Accion para agregar una sanci�n a la bd.
 * Autor: Jose de la rosa
 * Fecha: 19 de octubre de 2005, 06:19 PM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 **********************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

/**
 *
 * @author  Jose
 */
public class SancionInsertAction extends Action{
    
    /** Creates a new instance of SancionInsertAction */
    public SancionInsertAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next = "/jsp/cumplidos/sancion/SancionInsertar.jsp";
        HttpSession session = request.getSession ();
        String tipo_sancion = request.getParameter ("c_tipo_sancion");
        double valor = Double.parseDouble (request.getParameter ("c_valor"));
        String observacion = request.getParameter ("c_observacion");
        String numpla = (request.getParameter ("c_numpla").toUpperCase ());
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String distrito = (String) session.getAttribute ("Distrito");
        int sw=0;
        try{
            Sancion s = new Sancion ();
            s.setNumpla (numpla);
            s.setTipo_sancion (tipo_sancion);
            s.setObservacion (observacion);
            s.setValor (valor);
            s.setUsuario_creacion (usuario.getLogin ());
            s.setUsuario_modificacion (usuario.getLogin ());
            s.setUltima_modificacion (usuario.getLogin ());
            s.setBase (usuario.getBase ());
            s.setDistrito (distrito);
            try{
                request.setAttribute ("msg", "Sanci�n Agregada");
                model.sancionService.insertarSancion (s);
            }
            catch (SQLException e){
                sw=1;
            }
            if ( sw == 1 ){
                
                request.setAttribute ("msg", "Error En El Ingreso");
                
            }
        }catch (Exception e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
    
}
