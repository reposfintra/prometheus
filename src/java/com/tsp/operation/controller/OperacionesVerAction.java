package com.tsp.operation.controller;
import java.util.ArrayList;
import com.tsp.operation.controller.Action;
import java.io.*;
import java.util.*;
import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.threads.*;
import com.tsp.util.Util;
import com.tsp.operation.model.beans.Usuario;
import javax.servlet.http.*;
import java.sql.SQLException;
import com.tsp.operation.model.*;


public class OperacionesVerAction extends Action{
    public OperacionesVerAction()
    {
    }
    public void run() throws ServletException, InformationException
    {   HttpSession session  = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        ArrayList cab=(ArrayList)session.getAttribute("cab");
        ArrayList cont=(ArrayList)session.getAttribute("cont");
/*        String cabecera="<table width='200' border='1'><tr>";
        for(int i=0;i<cab.size();i++)
        {   cabecera=cabecera+"<td  bgcolor='#03A70C' style='color:#FFFFFF'><div align='center'><strong>"+(String)cab.get(i)+"</strong></div></td>";
        }
        cabecera=cabecera+"</tr>";
        String contenido="";
        for(int i=0;i<cont.size();i++)
        {   contenido=contenido+"<tr>";
            for(int j=0;j<((ArrayList)cont.get(i)).size();j++)
            {   contenido=contenido+"<td><div align='center'><strong>"+(String)((ArrayList)cont.get(i)).get(j)+"</strong></div></td>";
            }
            contenido=contenido+"</tr>";
        }
        contenido=contenido+"</table>";
        String archivo_excel=cabecera+contenido;*/
        String cabecera="";
        for(int i=0;i<cab.size();i++)
        {   cabecera=cabecera+(String)cab.get(i)+"\t";
        }
        String contenido="";
        for(int i=0;i<cont.size();i++)
        {   for(int j=0;j<((ArrayList)cont.get(i)).size();j++)
            {   contenido=contenido+(String)((ArrayList)cont.get(i)).get(j)+"\t";
            }
            contenido=contenido+"\r\n";
        }
        String archivo_excel=cabecera+"\r\n"+contenido;
        FileOutputStream fos=null;
        Calendar cal=Calendar.getInstance();                
        java.text.SimpleDateFormat f = new java.text.SimpleDateFormat("yyyyMMdd_HHmmss");
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String ruta = rb.getString("ruta")+"/exportar/migracion/" + usuario.getLogin()+"/ReporteOperaciones_"+f.format(cal.getTime())+".txt";
        try
        {   File outputFile = new File(ruta);
            fos = new FileOutputStream(outputFile);
            fos.write(archivo_excel.getBytes());
            fos.close();
        }
        catch(Exception e)
        {   e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        
        String next = "/jsp/fenalco/negocios/analisisCarteraMsg.jsp";
        this.dispatchRequest(next);
    }
}
