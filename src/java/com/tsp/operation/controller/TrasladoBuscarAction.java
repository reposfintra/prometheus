/*******************************************************************
 * Nombre clase: ClienteBuscarAction.java
 * Descripci�n: Accion para buscar una placa de la bd.
 * Autor: Ing. fily fernandez
 * Fecha: 16 de febrero de 2006, 05:41 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import com.tsp.exceptions.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import java.util.Vector;
import java.lang.*;
import java.sql.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import com.tsp.operation.model.services.*;
/**
 *
 * @author  jdelarosa
 */
public class TrasladoBuscarAction extends Action{
    
    /** Creates a new instance of PlacaBuscarAction */
    public TrasladoBuscarAction () {
    }
    
    public void run () throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String fechaI = request.getParameter ("fechaI")!=null?request.getParameter ("fechaI"):"";
        String fechaF = request.getParameter ("fechaF")!=null?request.getParameter ("fechaF"):"";
        String banco_Destino = request.getParameter ("bancoDes")!=null?request.getParameter ("bancoDes"):"";
        String banco_Origen = (request.getParameter("bancoOri")!=null)?request.getParameter ("bancoOri"):"";
        
          String next = "/jsp/finanzas/contab/comprobante/traslados/vertraslado.jsp?fechaI="+fechaI+"&fechaF="+fechaF+"&banco_Destino="+banco_Destino+"&banco_Origen="+banco_Origen;
        HttpSession session = request.getSession ();
        String opcion = request.getParameter ("opcion")!=null?request.getParameter ("opcion"):"";
        try {
              Vector datos = model.trasladoService.getVector();
              
           if ( opcion.equals("1") ){
                model.trasladoService.BusquedaTraslado(fechaI, fechaF, banco_Origen, banco_Destino);
                datos = model.trasladoService.getVector();
                if ( datos.size () <= 0 ) {    
                     next = "";
                     next = "/jsp/finanzas/contab/comprobante/traslados/tasladoBuscar.jsp?msg=Su busqueda no arrojo resultados!!";
                
                }else{
                    session.setAttribute("fechaI", fechaI);
                    session.setAttribute("fechaF", fechaF);
                    session.setAttribute("banco_Origen", banco_Origen);
                    session.setAttribute("banco_Destino", banco_Destino);
                }  
           } 
           
             
            
        } catch ( Exception e ) {
            
            throw new ServletException ( "Error en la busqueda  de TrasladoBuscarAction : " + e.getMessage() );
            
        }        
        
        this.dispatchRequest ( next );
        
    }
    
}
