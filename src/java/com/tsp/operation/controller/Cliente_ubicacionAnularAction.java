/********************************************************************
 * Nombre clase: Cliente_ubicacionAnularAction.java
 * Descripci�n: Accion para anular un cliente_ubicacion a la bd.
 * Autor: Jose de la rosa
 * Fecha: 20 de enero de 2006, 09:51 AM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class Cliente_ubicacionAnularAction extends Action{
    
    /** Creates a new instance of Cliente_ubicacionAnularAction */
    public Cliente_ubicacionAnularAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next="/jsp/trafico/mensaje/MsgAnulado.jsp";
        HttpSession session = request.getSession();        
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String dstrct = (String) session.getAttribute("Distrito");
        String cliente = request.getParameter("c_cliente");
        String ubicacion = request.getParameter("c_ubicacion");
        try{
            Cliente_ubicacion cli = new Cliente_ubicacion();
            cli.setCliente (cliente);
            cli.setUbicacion (ubicacion);
            cli.setDstrct (dstrct.toUpperCase());
            cli.setUser_update(usuario.getLogin().toUpperCase());
            cli.setCreation_user(usuario.getLogin().toUpperCase());
            model.cliente_ubicacionService.anularCliente_ubicacion(cli);
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);        
    }
    
}
