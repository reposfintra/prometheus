/*
 * VerNovedadAction.java
 *
 * Created on 13 de junio de 2005, 11:30 AM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  Henry
 */

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;

public class NovedadVerTodosAction  extends Action {
    
    /** Creates a new instance of VerNovedadsAction */
    public NovedadVerTodosAction() {
    }    
    
    public void run() throws ServletException {
        String next="/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina");        
        try{ 
            model.novedadService.listarNovedades();
            request.setAttribute("novedad", "COL" );
            request.setAttribute("mensaje", "hola");            
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
