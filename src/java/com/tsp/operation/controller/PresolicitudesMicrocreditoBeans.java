/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.tsp.operation.model.beans.SolicitudAval;
import com.tsp.operation.model.beans.SolicitudPersona;

/**
 *
 * @author dvalencia
 */
public class PresolicitudesMicrocreditoBeans {

    private SolicitudAval sa;
    private SolicitudPersona sp;
    
    public PresolicitudesMicrocreditoBeans() {
    }

    public PresolicitudesMicrocreditoBeans(SolicitudAval sa, SolicitudPersona sp) {
        this.sa = sa;
        this.sp = sp;
    }
    

    public SolicitudAval getSa() {
        return sa;
    }

    public void setSa(SolicitudAval sa) {
        this.sa = sa;
    }

    public SolicitudPersona getSp() {
        return sp;
    }

    public void setSp(SolicitudPersona sp) {
        this.sp = sp;
    }

  
}
