/*
 * ReporteEgresoAction.java
 *
 * Created on 13 de septiembre de 2005, 10:43 AM
 */

package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import com.tsp.operation.model.threads.*;
import com.tsp.finanzas.contab.model.*;
import com.tsp.finanzas.contab.model.beans.*;
/**
 *
 * @author  Administrador
 */
public class CambioChkAction extends Action {
    private java.text.SimpleDateFormat anomes;
    private java.text.SimpleDateFormat fecha;
    private java.util.Date pdate;
    private String periodo;
    private String fechadoc;
    private String tipodoc = "EGR";
    private String monedaD;
    Usuario user = new Usuario();        
    private com.tsp.finanzas.contab.model.Model modelContab;
    private String s_query      = "";
    private String s_banco      = "";
    private String s_sucursal   = "";
    /** Creates a new instance of ReporteEgresoAction */
    public CambioChkAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        try{
            String opcion                 = request.getParameter("Opcion");
            String agencia                = (request.getParameter("agencia")!=null)?request.getParameter("agencia"):"";
            String fechai                 = (request.getParameter("fechai")!=null)?request.getParameter("fechai"):"";
            String fechaf                 = (request.getParameter("fechaf")!=null)?request.getParameter("fechaf"):"";
            String usuario                = (request.getParameter("usuario")!=null)?request.getParameter("usuario"):""; //Modificacion  04/01/2006
            String fecha                  = (request.getParameter("fecha")!=null)?request.getParameter("fecha"):"";
            String b                      = (request.getParameter("b")!=null)?request.getParameter("b"):"";
            String s                      = (request.getParameter("s")!=null)?request.getParameter("s"):"";
            
            String banco                  = (request.getParameter("banck")!=null)?request.getParameter("banck"):"";
            String sucuralbanco           = (request.getParameter("sucuralbanco")!=null)?request.getParameter("sucuralbanco"):"";
            
            String rangoi                 = (request.getParameter("rangoi")!=null)?request.getParameter("rangoi"):"";
            String rangof                 = (request.getParameter("rangof")!=null)?request.getParameter("rangof"):"";
            
            HttpSession Session           = request.getSession();
            user                          = (Usuario)Session.getAttribute("Usuario");
            
            modelContab = new com.tsp.finanzas.contab.model.Model(user.getBd());
            String distrito               = user.getDstrct();
            
            String Mensaje = "";
            String next    = "";
            
            usuario = (usuario.equals(""))? "%":usuario;
            
            String sqlAct1              = "";
            String sqlAct2              = "";
            String sql                  = "";
            String sqlComprobantes      = "";
            
            
            
            model.ciaService.buscarCia(user.getDstrct());
            Compania cia    = model.ciaService.getCompania();
            monedaD         = cia.getMoneda();
            
            boolean  flag   = true;
            
            
            if(opcion.equals("BuscarS")){
                next = "/jsp/cxpagar/ModificacionCheques/Modificacion_cheques.jsp?banco="+banco+"&fecha="+fechai;
            }
            
            
            
            if(opcion.equals("GenerarChk")){ //Modificacion  04/01/2006
                
                String query = "";
                if( !fechai.equals("") && !fechaf.equals("") )
                    query = "e.fecha_cheque BETWEEN " +"'"+ fechai +"'"+ " AND "+"'"+ fechaf+"'";
                else if ( !rangoi.equals("") && !rangof.equals("") )
                    query = "e.document_no BETWEEN "  +"'"+rangoi+"'"+  " AND " +"'"+ rangof +"'";
                
                model.ReporteEgresoSvc.listaChkReenumeracion(banco, sucuralbanco, query);
                
                this.setS_query     ( query );//Asignacion de Query
                this.setS_banco     ( banco );//Banco
                this.setS_sucursal  ( sucuralbanco );//Sucursal
                
                if( model.ReporteEgresoSvc.getListRE() == null || model.ReporteEgresoSvc.getListRE().size() == 0  )
                    Mensaje = "No hay cheques con estos parametros de busqueda";
                next = "/jsp/cxpagar/ModificacionCheques/Modificacion_cheques.jsp?Mensaje="+Mensaje+"&fecha="+fecha+"&banco="+banco+"&sucursal="+sucuralbanco;
                
            }
            
            
            if(opcion.equals("Actualizar")){
                
                java.util.Enumeration parametros;
                
                String parametro = "";
                parametros = request.getParameterNames(); // Leemos todos los atributos del request
                List listaChk = model.ReporteEgresoSvc.getListRE();
                
                /*Armo lista de valores de lista original*/
                Iterator it2 = listaChk.iterator();
                String listaO = "";
                while(it2.hasNext()){
                    ReporteEgreso re = (ReporteEgreso) it2.next();
                    listaO+= "'"+re.getNumeroChk()+"'"+",";
                }
                listaO = listaO.substring(0,listaO.length() - 1);
                
                
                String list         = ""; //Lista de valores temporales
                String cheques      = "";
                String fechas       = "";
                
                
                boolean flagMovpla = false;
                boolean flagEgreso = false;
                
                while (parametros.hasMoreElements()) {
                    parametro = (String) parametros.nextElement();
                    //Validacion del tipo de parametro
                    if( parametro.length() > 5 ){
                        //If campo con el numero de cheque
                        if(parametro.substring(0,5).equals("campo")){
                            //Validacion que tengan nuevo numero de cheque
                            if(!request.getParameter(parametro).equals("")){
                                
                                String fin = (String)Session.getAttribute("fechaInicio");
                                
                                int numero = Integer.parseInt( parametro.substring(5));
                                
                                ReporteEgreso chk = (ReporteEgreso)listaChk.get(numero-1);
                                
                                String valor = request.getParameter(parametro);
                                
                                if( !chk.getNumeroChk().equals(valor) ){
                                    
                                    if( !model.ReporteEgresoSvc.SEARCH(valor, listaO,b, s )){
                                        
                                        //Actualizacion de registros en Movpla
                                        
                                        //Tipo de documento 010 - Factura <> Movpla
                                        String tipodoc = chk.getTipo_doc();
                                        
                                        
                                        /*Actualizacion de Serie de Cheque, cuando el nuevo numero no existe en egreso*/
                                        
                                        if( !model.ReporteEgresoSvc.existe_Egreso( b, s, "'"+valor+"'" )){
                                            Cheque auxobj    = new Cheque();
                                            auxobj.setUsuario_impresion(user.getLogin());
                                            
                                            
                                            
                                            auxobj.setDistrito(distrito);//Distrito
                                            auxobj.setBanco(b);//Banco
                                            auxobj.setSucursal(s);//Sucursal
                                            
                                            Series serie = model.ChequeXFacturaSvc.obtenerSerieActiva( distrito, b, s );//Obtiene el objeto Serie Activa
                                            
                                            if( serie != null  ){
                                                
                                                String aux_valor    = valor;
                                                String pre          = serie.getPrefijo();
                                                aux_valor           = aux_valor.replaceAll( pre , "");
                                                aux_valor.trim();
                                                
                                                //System.out.println( "NUEVO CHEQUE " + aux_valor );                                                                                                
                                                
                                                
                                                if( Util.esNumero( aux_valor ) ){
                                                    
                                                    int lastNumberSerie  = serie.getLast_number();
                                                    int v_valor          = Integer.parseInt(aux_valor);
                                                    
                                                    if( v_valor <  lastNumberSerie ){
                                                        flag    = false;
                                                        Mensaje = " El nuevo numero de cheque debe ser <BR> mayor o igual al proximo  "+
                                                                  " cheque en la secuencia de la serie activa.<BR>                    "+
                                                                  " Proximo # cheque : " +  serie.getPrefijo() + serie.getLast_number();
                                                        break;
                                                    }
                                                    else{
                                                        auxobj.setLastNumber(v_valor);//Nuevo Cheque
                                                    }
                                                }
                                                
                                                else{
                                                    flag    =   false;
                                                    Mensaje +=  "La Conformacion del nuevo numero de cheque no corresponde a la estructura de la Serie Activa";
                                                    break;
                                                }
                                                
                                                
                                                
                                                String primero  = serie.getPrefijo() + serie.getSerial_initial_no();
                                                String ultimo   = serie.getPrefijo() + serie.getSerial_fished_no();
                                                
                                                boolean flag_aux = model.ReporteEgresoSvc.existe_Chk_RangoSerie( "'"+primero+"'", "'"+ultimo+"'", "'"+valor+"'" );
                                                
                                                
                                                if( !model.ReporteEgresoSvc.existe_Chk_RangoSerie( "'"+primero+"'", "'"+ultimo+"'", "'"+valor+"'" )){
                                                    flag    = false;
                                                    Mensaje += "El nuevo numero de Cheque : " + valor + "\n No esta en el rango de la serie Activa \n";
                                                    break;
                                                }//Validacion Existencia de Rango de Fechas;
                                                
                                                else{
                                                    sql              += model.ChequeXFacturaSvc.ActualizarSerie(distrito, b, s, auxobj  ); //Actualiza la Serie de Cheques
                                                    //System.out.println("SQL " + sql);
                                                }// Se valida si el nuevo numero de cheque esta entre el rango de la serie
                                                
                                                
                                                
                                            }//Fin de Validacion
                                            
                                            else{
                                                flag        = false;
                                                Mensaje     = "No existe Serie Activa para este Banco y Sucursal";
                                                break;
                                            }
                                            
                                        }// Fin de Valiadacion de Acutalizacion de Serie
                                        
                                        
                                        sqlAct1 += model.ReporteEgresoSvc.UPDATE(valor, user.getLogin(), chk.getNumeroChk(), b, s, (String)Session.getAttribute("Distrito"), tipodoc, chk );
                                        sqlAct2 += model.ReporteEgresoSvc.UPDATEREGA(valor, user.getLogin(), chk.getNumeroChk(), b, s, (String)Session.getAttribute("Distrito"), tipodoc, chk );
                                        
                                        
                                        if( flag ){
                                            Mensaje = "Los Cheques se han modificado";
                                        }
                                        
                                    }
                                    
                                    else{
                                        flagMovpla = true;
                                        list += valor+"/"+chk.getNumeroChk()+",";
                                        chk.setNuevoNChk(valor);
                                        ReporteEgreso re = model.ReporteEgresoSvc.BUSCAR(valor);
                                        cheques += valor+",";
                                        fechas += re.getFechaChk()+",";
                                        
                                    }
                                    
                                }
                                
                            }
                        }
                    }
                }
                
                if(!flagMovpla){
                    
                    TransaccionService  scv = new TransaccionService(user.getBd());
                    if( flag ){
                        scv.crearStatement();
                        scv.getSt().addBatch(sql);
                        scv.getSt().addBatch(sqlAct1);
                        scv.getSt().addBatch(sqlAct2);
                        
                        scv.execute();
                    }
                }
                
                //model.ReporteEgresoSvc.LISTCHK(fecha,b,s);
                
                /*FIN ACTUALIZACION */
                
                list = (list.length()>0)?list.substring(0,list.length() - 1 ):"";
                if( cheques.length() > 0 && fechas.length() > 0  ){
                    cheques = cheques.substring(0,cheques.length() - 1 );
                    fechas = fechas.substring(0, fechas.length() - 1 );
                    if( cheques.split(",").length > 1 )
                        Mensaje = "No se ejecutaron los cambios, algunos cheques se encuentran registrados, haga aqui click para ver Listado de cheques ya registrados";
                    else
                        Mensaje = "El cheque " + cheques + " ya se encuentra registrado con fecha de " + fechas;
                }
                next = "/jsp/cxpagar/ModificacionCheques/Modificacion_cheques.jsp?Mensaje="+Mensaje+"&fecha="+fecha+"&list="+list+"&cheques="+cheques+"&fechas="+fechas+"&banco="+b+"&sucursal="+s;
                
                System.out.println("QUERY " + this.getS_query() );
                
                model.ReporteEgresoSvc.listaChkReenumeracion(this.getS_banco(), this.getS_sucursal(), this.getS_query() );
                
            }
            
            sqlAct1 = "";
            sqlAct2 = "";
            
            
            
            
            Session.setAttribute("fechaInicio", fechai );
            Session.setAttribute("fechaFin", fechaf );
            
            
            
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en ReporteEgresoAction .....\n"+e.getMessage());
        }
    }
    
    
    public void InsertComprobante( String usuario, String nuevochk, String chk, double valor ) {
        Comprobantes c = new Comprobantes();
        String sql = "";
        Calendar hoy = Calendar.getInstance();
        hoy.add(hoy.DATE, -1);
        
        pdate = hoy.getTime();
        
        anomes = new java.text.SimpleDateFormat("yyyyMM");
        fecha = new java.text.SimpleDateFormat("yyyy-MM-dd");
        
        periodo = anomes.format(pdate);
        fechadoc = fecha.format(pdate);
        
        c.setPeriodo(periodo);
        c.setFechadoc(fechadoc);
        c.setAprobador(usuario);
        c.setDstrct("FINV");
        c.setTipodoc(tipodoc);
        c.setMoneda(monedaD);
        //parametros
        c.setNumdoc(nuevochk);
        c.setSucursal("OP");
        c.setDetalle("CONTAB EGRESO " + chk + " X " + nuevochk);
        c.setTotal_credito(valor);
        c.setTotal_debito(valor);
        c.setTercero("");
        c.setBase("");
        c.setCuenta("");
        c.setTotal_items(2);
        
        modelContab.comprobanteService.setComprobante(c);
        try{
            modelContab.comprobanteService.insert();
            int gtransaccion = modelContab.comprobanteService.getGrupoTransaccion();
            
            c.setGrupo_transaccion(gtransaccion);
            /*1� Registro Nuevo Cheque*/
            c.setDetalle("CONTABILIZACION " + nuevochk + " ANULACION " + chk);
            c.setTotal_debito(valor);
            c.setTotal_credito(0);
            c.setDocumento_interno("003");
            c.setTipodoc_rel("003");
            c.setNumdoc_rel(chk);
            
            modelContab.comprobanteService.setComprobante(c);
            modelContab.comprobanteService.insertItem();
            
            /*2� Registro Cheque Viejo*/
            c.setDetalle("CONTABILIZACION " + nuevochk + " ANULACION " + chk);
            c.setTotal_debito(0);
            c.setTotal_credito(valor);
            c.setDocumento_interno("003");
            c.setTipodoc_rel("003");
            c.setNumdoc_rel(chk);
            
            modelContab.comprobanteService.setComprobante(c);
            modelContab.comprobanteService.insertItem();
            
        }catch(Exception e ){ e.printStackTrace(); }
        
    }
       
    
    /**
     * Getter for property s_query.
     * @return Value of property s_query.
     */
    public java.lang.String getS_query() {
        return s_query;
    }
    
    /**
     * Setter for property s_query.
     * @param s_query New value of property s_query.
     */
    public void setS_query(java.lang.String s_query) {
        this.s_query = s_query;
    }
    
    /**
     * Getter for property s_banco.
     * @return Value of property s_banco.
     */
    public java.lang.String getS_banco() {
        return s_banco;
    }
    
    /**
     * Setter for property s_banco.
     * @param s_banco New value of property s_banco.
     */
    public void setS_banco(java.lang.String s_banco) {
        this.s_banco = s_banco;
    }
    
    /**
     * Getter for property s_sucursal.
     * @return Value of property s_sucursal.
     */
    public java.lang.String getS_sucursal() {
        return s_sucursal;
    }
    
    /**
     * Setter for property s_sucursal.
     * @param s_sucursal New value of property s_sucursal.
     */
    public void setS_sucursal(java.lang.String s_sucursal) {
        this.s_sucursal = s_sucursal;
    }
    
}
