/*
 * MenuConsultaArchivoAction.java
 *
 * Created on 11 de octubre de 2004, 04:50 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.sql.SQLException;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  MARIO FONTALVO
 */
public class MenuRegistroTiempoAction extends Action {
    
    /** Creates a new instance of MenuConsultaArchivoAction */
    public MenuRegistroTiempoAction() {
    }               
     public void run() throws ServletException, InformationException 
    { 
        try
        {   
            model.RegistroTiempoSvc.ReiniciarPlanilla();
            model.RegistroTiempoSvc.ReiniciarListaTBLTiempo();
            model.RegistroTiempoSvc.ReiniciarListaPlanillaTiempos();
            model.RegistroTiempoSvc.ReiniciarListaDelay();
            final String next = "/registrotiempo.jsp";
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
        }
        catch(Exception e)
        {
            throw new ServletException(e.getMessage());
        }
     }
    
}
