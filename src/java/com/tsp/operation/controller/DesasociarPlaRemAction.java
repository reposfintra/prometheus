/***************************************************************************
 * Nombre clase : ............... DesasociarPlaRemAction.java               *
 * Descripcion :................. Accion para desasociar planilla de remesas*
 *                                y viceversa                               *
 * Autor :....................... Ing. Karen Reales                         *
 * Fecha :........................ 22 de Marzo de 2006, 11:38   AM          *
 * Version :...................... 1.0                                      *
 * Copyright :.................... Fintravalores S.A.                  *
 ***************************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class DesasociarPlaRemAction extends Action{
    static Logger logger = Logger.getLogger(DesasociarPlaRemAction.class);
    String nombreR="";
    String nombreD="";
    /** Creates a new instance of PlanillaSearchAction */
    public DesasociarPlaRemAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        Vector plas =new Vector();
        String next="/colpapel/DesasociarPlaRem.jsp";
        String planilla= request.getParameter("planilla").toUpperCase();
        
        try{
            
            
            if(!model.planillaService.tieneFactura(planilla)){
                if(request.getParameter("desasociar")!=null){
                    int sw=0;
                    java.util.Enumeration enum1;
                    String parametro;
                    enum1 = request.getParameterNames();
                    float cant =0;
                    while (enum1.hasMoreElements()) {
                        parametro = (String) enum1.nextElement();
                        if(parametro.indexOf("por")>=0){
                            float por = Float.parseFloat(request.getParameter(parametro));
                            cant = cant+por;
                        }
                    }
                    if(cant!=100 ){
                        sw=1;
                    }
                    if(sw==0){
                        enum1 = request.getParameterNames();
                        while (enum1.hasMoreElements()) {
                            parametro = (String) enum1.nextElement();
                            logger.info("Se va desasociar la planilla "+planilla +" Remesa :"+ parametro);
                            model.remplaService.buscaRemPla(planilla, parametro);
                            if(model.remplaService.getRemPla()!=null){
                                logger.info("Se encontro la asociacion");
                                RemPla r = model.remplaService.getRemPla();
                                r.setPorcent(0);
                                model.remplaService.updateRemPla(r);
                                model.remplaService.anularRemPlaR(r);
                                logger.info("Se anula");
                            }
                        }
                        model.planillaService.listaPlanillas(planilla);
                        plas = model.planillaService.getPlas();
                        for(int i=0; i<plas.size();i++){
                            Planilla plan= (Planilla) plas.elementAt(i);
                            model.remplaService.buscaRemPla(plan.getNumpla(),plan.getNumrem());
                            if(model.remplaService.getRemPla()!=null){
                                RemPla r = model.remplaService.getRemPla();
                                float por = 0;
                                if(request.getParameter("por"+plan.getNumrem())!=null){
                                    por = Float.parseFloat(request.getParameter("por"+plan.getNumrem()));
                                    
                                }
                                r.setPorcent(por);
                                model.remplaService.updateRemPla(r);
                            }
                        }
                        next="/colpapel/DesasociarPlaRem.jsp?mensaje=Se desasocio con exito las remesas seleccionadas de la planilla "+planilla;
                    }
                    else{
                        next="/colpapel/DesasociarPlaRem.jsp?mensaje=El valor total del porcentaje debe ser 100";
                    }
                    
                }
                
                model.planillaService.listaPlanillas(planilla);
                plas = model.planillaService.getPlas();
                Vector nuevaPlas = new Vector();
                for(int i=0; i<plas.size();i++){
                    
                    Planilla plan= (Planilla) plas.elementAt(i);
                    plan.setReg_status("");
                    if(model.remesaService.buscarPlanillas(plan.getNumrem()).size()==1){
                        plan.setReg_status("A");
                        
                    }
                    nuevaPlas.add(plan);
                    
                }
                model.planillaService.setPlanillasVec(nuevaPlas);
            }
            else{
                model.planillaService.setPlanillasVec(null);
                next="/colpapel/DesasociarPlaRem.jsp?mensaje=La planilla "+planilla+" ya tiene generada OP, no puede desasociar remesas.";
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
}
