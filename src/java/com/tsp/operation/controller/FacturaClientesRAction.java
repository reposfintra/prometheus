/********************************************************************
 *      Nombre Clase.................   FacturaBuscarproveedoresAction.java
 *      Descripci�n..................   Action que se encarga de cargar los bancos en la pagina siguiente
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.util.Util;
/**
 *
 * @author  dlamadrid
 */
public class FacturaClientesRAction extends Action{
    
    /** Creates a new instance of FacturaClientesRAction */
    public FacturaClientesRAction () {
    }
    
    public void run () throws ServletException, InformationException {
        try {
            
            String cliente = (request.getParameter ("planilla")==null?"":request.getParameter ("planilla"));
            String x=(request.getParameter ("x")==null?"":request.getParameter ("x"));
            model.remesaService.clientesRemesa (cliente);
            
            int num_items=0;
            
            try {
                num_items  = Integer.parseInt (""+ request.getParameter ("num_items"));
            }
            
            
            catch(java.lang.NumberFormatException e) {
                num_items=1;
            }
           
            String next = "/jsp/cxpagar/facturasxpagar/codigoremesa.jsp?accion=1&op=cargarB&num_items="+num_items+"&x="+x;
            //System.out.println ("next en Filtro"+next);
            this.dispatchRequest (next);
            
        }
        catch(Exception e) {
            throw new ServletException ("Accion:"+ e.getMessage ());
        }
        
    }
    
}
