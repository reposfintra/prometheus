/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.tsp.operation.model.beans.Usuario;
import javax.servlet.http.*;

import com.tsp.operation.model.services.GestionSolicitudAvalService;
import com.tsp.opav.model.services.GestionSolicitudAiresAAEService;
import com.tsp.operation.model.DAOS.Tipo_impuestoDAO;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.Tipo_impuesto;
import com.tsp.util.Util;
import java.util.ArrayList;

/**
 *
 * @author ivargas
 */
public class CausacionAnulacionDocsAction extends Action {

    //Opciones dentro de la clase:
    private final int INTERESES_PENDIENTES = 0;
    private final int CAUSAR_INTERES_CUOTAS = 1;
    private final int CAT_PENDIENTE = 2;
    private final int GENERAR_FACTURAS_CAT = 3;
    private final int ANULAR_FACTURAS_INTERESES = 4;
    private final int CUOTAS_MANEJO_PENDIENTES = 5;
    private final int CAUSAR_CUOTAS_MANEJOS = 6;
    
    GestionSolicitudAiresAAEService aaserv;
    GestionSolicitudAvalService gsaserv;
    String next;

    String login;
    
    boolean redirect = false;
    String cadenaWrite = "";
    Usuario usuario = null;
    ArrayList<BeanGeneral> interesesPendList = new ArrayList<BeanGeneral>();
    ArrayList<BeanGeneral> catPendList = new ArrayList<BeanGeneral>();
    ArrayList<BeanGeneral> cuotaManejoPendList = new ArrayList<BeanGeneral>();

    public CausacionAnulacionDocsAction() {
        this.next = "";
    }

    public void run() throws javax.servlet.ServletException {

        try {
            int op = (request.getParameter("opcion") != null) ? Integer.parseInt(request.getParameter("opcion").toString()) : -1;
            HttpSession session = request.getSession();
            com.tsp.util.Util.logController(((com.tsp.operation.model.beans.Usuario) session.getAttribute("Usuario")).getLogin(), this.getClass().getName());
            usuario = (Usuario) session.getAttribute("Usuario");
            login = usuario.getLogin();
            
            aaserv = new GestionSolicitudAiresAAEService(usuario.getBd());
            gsaserv = new GestionSolicitudAvalService(usuario.getBd());
        
            gsaserv.setDstrct(usuario.getDstrct() != null ? usuario.getDstrct() : "FINV");
            gsaserv.setLoginuser(usuario.getLogin() != null ? usuario.getLogin() : "ADMIN");


            switch (op) {
                case INTERESES_PENDIENTES:
                    interesesPendientes();
                    break;
                case CAUSAR_INTERES_CUOTAS:
                    causarIntereses();
                    break;
                case CAT_PENDIENTE:
                    catPendientes();
                    break;
                case GENERAR_FACTURAS_CAT:
                    generarFacturasCat();
                    break;
                case ANULAR_FACTURAS_INTERESES:
                    anularIntereses();
                    break;
                case CUOTAS_MANEJO_PENDIENTES:
                    cuotasManejoPendientes();
                    break;
                case CAUSAR_CUOTAS_MANEJOS:
                    causarCuotasManejo();
                    break;
                    
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (redirect == false) {
                    this.escribirResponse(cadenaWrite);
                } else {
                    this.dispatchRequest(next);
                }
            } catch (Exception e) {
                System.out.println("Error al redireccionar o escribir respuesta: " + e.toString());
                e.printStackTrace();
            }
        }

    }

    /**
     * Escribe el resultado de la consulta en el response de Ajax
     * @param dato la cadena a escribir
     * @throws Exception cuando hay un error
     */
    protected void escribirResponse(String dato) throws Exception {
        try {
            response.setContentType("text/plain; charset=utf-8");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println(dato);
        } catch (Exception e) {
            throw new Exception("Error al escribir el response: " + e.toString());
        }
    }

    /**
     * Busca las cuotas que estan pendiente por causar el interes
     * @throws Exception
     */
    private void interesesPendientes() throws Exception {
        redirect = false;
        String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
        String vista = request.getParameter("vista") != null ? request.getParameter("vista") : "";
        try {
            interesesPendList = model.Negociossvc.interesesPendientes(negocio);
        } catch (Exception e) {
            System.out.println("error(action): " + e.toString());
            e.printStackTrace();
        }
        cadenaWrite = "<table id='tabladocs' style='border-collapse:collapse; width:100%;' border='1'>";
        cadenaWrite += "    <thead>";
        if (!interesesPendList.isEmpty()) {
            cadenaWrite += "        <tr class='filaazul'>";
            cadenaWrite += "            <th colspan='"+(vista.equals("2")?"4":"3")+"'>Cliente: " + interesesPendList.get(0).getValor_18() + " </th>";
            cadenaWrite += "        </tr>";
            cadenaWrite += "        <tr class='filaazul'>";
            if (vista.equals("2")) {
                cadenaWrite += "        <th>&nbsp;</th>";
            }
            cadenaWrite += "            <th>Numero de Factura</th>";
            cadenaWrite += "            <th>Fecha Vencimiento</th>";
            cadenaWrite += "            <th>Interes a Causar</th>";
            cadenaWrite += "        </tr>";
            cadenaWrite += "    </thead>";
            for (int i = 0; i < interesesPendList.size(); i++) {
                cadenaWrite += "    <tr class='filableach'>";
                if (vista.equals("2")) {
                    cadenaWrite += "        <td width='10' align='left'><input name='ckestado' type='checkbox' id='ckestado" + i + "' value='" + interesesPendList.get(i).getValor_10() + "'></td>";

                }
                cadenaWrite += "        <td align='center'>" + interesesPendList.get(i).getValor_10() + "</td>";
                cadenaWrite += "        <td align='center'>" + interesesPendList.get(i).getValor_14() + "</td>";
                cadenaWrite += "        <td align='center'>" + Util.FormatoMiles(Double.parseDouble(interesesPendList.get(i).getValor_05()) - Double.parseDouble(interesesPendList.get(i).getValor_06())) + "</td>";
                cadenaWrite += "     </tr>";
            }
        } else {
            cadenaWrite += "        <tr class='filaazul'>";
            cadenaWrite += "            <th colspan='"+(vista.equals("2")?"4":"3")+"'>El negocio consultado no tiene cuotas pendientes por causar</th>";
            cadenaWrite += "        </tr>";
            cadenaWrite += "    </thead>";
        }

        cadenaWrite += "</table>";
    }

   /**
     * Causa los intereses del numero de cuotas digitado
     * @throws Exception
     */
    private void causarIntereses() throws Exception {
        
        redirect = false;
        int num_cuotas = request.getParameter("num_cuotas") != null ? Integer.parseInt(request.getParameter("num_cuotas")) : 0;
        
        if (!interesesPendList.isEmpty()) {
            
            if (num_cuotas <= interesesPendList.size()) {
                try {
                    
                    TransaccionService tService = new TransaccionService(usuario.getBd());
                    tService.crearStatement();
                    
                    for (int i = 0; i < num_cuotas; i++) {
                        
                        //Seteamos el usuario que anticipa los intereses
                        interesesPendList.get(i).setValor_19(login);
                        
                        interesesPendList.get(i).setValor_04(interesesPendList.get(i).getValor_12());
                        double valor = (Double.parseDouble(interesesPendList.get(i).getValor_05()) - Double.parseDouble(interesesPendList.get(i).getValor_06()));
                        
                        String documento = model.Negociossvc.UpCP(interesesPendList.get(i).getValor_01());
                        
                        tService.getSt().addBatch(model.Negociossvc.ingresarCXCMicrocreditoPost(interesesPendList.get(i), valor, documento));
                        tService.getSt().addBatch(model.Negociossvc.ingresarDetalleCXCMicrocreditoPost(interesesPendList.get(i), valor, documento, "1"));
                        
                        valor = Double.parseDouble(interesesPendList.get(i).getValor_06()) + valor;
                        tService.getSt().addBatch(model.Negociossvc.updateInteresCausado(valor, interesesPendList.get(i).getValor_12(), interesesPendList.get(i).getValor_13(), interesesPendList.get(i).getValor_07()));
                        
                    }
                    tService.execute();
                    cadenaWrite = "Se han causado " + num_cuotas + " cuota(s)";
                } catch (Exception e) {
                    System.out.println("error en action: " + e.toString());
                    cadenaWrite = e.toString();
                    e.printStackTrace();
                }
            } else {
                cadenaWrite = "Debe digitar un numero de cuotas menor o igual a las que estan pendientes por causar";
            }
        } else {
            cadenaWrite = "El negocio consultado no tiene cuotas pendientes por causar";
        }
    }

    /**
     * Busca las cuotas que estan pendiente por causar el cat
     * @throws Exception
     */
    private void catPendientes() throws Exception {
        redirect = false;
        String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
        try {
            catPendList = model.Negociossvc.catPendientes(negocio);
        } catch (Exception e) {
            System.out.println("error(action): " + e.toString());
            e.printStackTrace();
        }
        cadenaWrite = "<table id='tabladocs' style='border-collapse:collapse; width:100%;' border='1'>";
        cadenaWrite += "    <thead>";
        if (!catPendList.isEmpty()) {
            cadenaWrite += "        <tr class='filaazul'>";
            cadenaWrite += "            <th colspan='3'>Cliente: " + catPendList.get(0).getValor_14() + " </th>";
            cadenaWrite += "        </tr>";
            cadenaWrite += "        <tr class='filaazul'>";
            cadenaWrite += "            <th>Numero de Factura</th>";
            cadenaWrite += "            <th>Fecha Vencimiento</th>";
            cadenaWrite += "            <th>Interes a Causar</th>";
            cadenaWrite += "        </tr>";
            cadenaWrite += "    </thead>";
            for (int i = 0; i < catPendList.size(); i++) {
                cadenaWrite += "    <tr class='filableach'>";
                cadenaWrite += "        <td align='center'>" + catPendList.get(i).getValor_10() + "</td>";
                cadenaWrite += "        <td align='center'>" + catPendList.get(i).getValor_04() + "</td>";
                cadenaWrite += "        <td align='center'>" + Util.FormatoMiles(Double.parseDouble(catPendList.get(i).getValor_05())) + "</td>";
                cadenaWrite += "     </tr>";
            }
        } else {
            cadenaWrite += "        <tr class='filaazul'>";
            cadenaWrite += "            <th colspan='3'>El negocio consultado no tiene facturas cat pendientes</th>";
            cadenaWrite += "        </tr>";
            cadenaWrite += "    </thead>";
        }

        cadenaWrite += "</table>";
    }

    /**
     * genera las facturas cat deacuerdo al numero de cuotas digitado
     * @throws Exception
     */
    private void generarFacturasCat() throws Exception {
        redirect = false;
        int num_cuotas = request.getParameter("num_cuotas") != null ? Integer.parseInt(request.getParameter("num_cuotas")) : 0;
        if (!catPendList.isEmpty()) {
            if (num_cuotas <= catPendList.size()) {
                try {
                    TransaccionService tService = new TransaccionService(usuario.getBd());
                    tService.crearStatement();
                    for (int i = 0; i < num_cuotas; i++) {
                        
                        catPendList.get(i).setValor_04(catPendList.get(i).getValor_12());
                        catPendList.get(i).setValor_19(login);
                        
                        Tipo_impuestoDAO timpuestoDao = new Tipo_impuestoDAO(usuario.getBd());
                        Tipo_impuesto impuesto = timpuestoDao.buscarImpuestoxCodigoxFecha(catPendList.get(i).getValor_09(), catPendList.get(i).getValor_06(), catPendList.get(i).getValor_12());

                        String documento = model.Negociossvc.UpCP(catPendList.get(i).getValor_01());

                        double ivacat = (Double.parseDouble(catPendList.get(i).getValor_05()) * impuesto.getPorcentaje1()) / (100 + impuesto.getPorcentaje1());
                        double catneto = Double.parseDouble(catPendList.get(i).getValor_05()) - ivacat;

                        tService.getSt().addBatch(model.Negociossvc.ingresarCXCMicrocreditoPost(catPendList.get(i), Double.parseDouble(catPendList.get(i).getValor_05()), documento));
                        tService.getSt().addBatch(model.Negociossvc.ingresarDetalleCXCMicrocreditoPost(catPendList.get(i), catneto, documento, "1"));
                        
                        catPendList.get(i).setValor_08(impuesto.getCod_cuenta_contable());
                        
                        tService.getSt().addBatch(model.Negociossvc.ingresarDetalleCXCMicrocreditoPost(catPendList.get(i), ivacat, documento, "2"));
                        tService.getSt().addBatch(model.Negociossvc.updateDocumentoCat(documento, catPendList.get(i).getValor_13(), catPendList.get(i).getValor_07()));
                    }
                    tService.execute();
                    cadenaWrite = "Se han generado " + num_cuotas + " factura(s) cat";
                } catch (Exception e) {
                    System.out.println("error en action: " + e.toString());
                    cadenaWrite = e.toString();
                    e.printStackTrace();
                }
            } else {
                cadenaWrite = "Debe digitar un numero de cuotas menor o igual a las que estan pendientes por generar";
            }
        } else {
            cadenaWrite = "El negocio consultado no tiene facturas cat pendientes";
        }
    }

    private void anularIntereses() throws Exception {
        redirect = false;
        String[] facturas = request.getParameter("ckestado").split("#");
        if (!interesesPendList.isEmpty()) {
            if (facturas.length <= interesesPendList.size()) {
                try {
                    TransaccionService tService = new TransaccionService(usuario.getBd());
                    tService.crearStatement();
                    for (int i = 0; i < facturas.length; i++) {
                        for (int j = 0; j < interesesPendList.size(); j++) {
                            if (facturas[i].equals(interesesPendList.get(j).getValor_10())) {
                                tService.getSt().addBatch(model.Negociossvc.anularFacturaInteres(interesesPendList.get(j).getValor_10()));
                                break;
                            }
                        }
                    }
                    tService.execute();
                    cadenaWrite = "Se han anulado " + facturas.length + " factura(s)";
                } catch (Exception e) {
                    System.out.println("error en action: " + e.toString());
                    cadenaWrite = e.toString();
                    e.printStackTrace();
                }
            } else {
                cadenaWrite = "Debe seleccionar al menos una factura";
            }
        } else {
            cadenaWrite = "El negocio consultado no tiene cuotas pendientes por causar";
        }
    }

    private void cuotasManejoPendientes () throws Exception {
        redirect = false;
        String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
        String vista = request.getParameter("vista") != null ? request.getParameter("vista") : "";
        try {
            cuotaManejoPendList = model.Negociossvc.cuotaManejoPendientes(negocio);
        } catch (Exception e) {
            System.out.println("error(action): " + e.toString());
            e.printStackTrace();
        }
        cadenaWrite = "<table id='tabladocs' style='border-collapse:collapse; width:100%;' border='1'>";
        cadenaWrite += "    <thead>";
        if (!cuotaManejoPendList.isEmpty()) {
            cadenaWrite += "        <tr class='filaazul'>";
            cadenaWrite += "            <th colspan='"+(vista.equals("2")?"4":"3")+"'>Cliente: " + cuotaManejoPendList.get(0).getValor_18() + " </th>";
            cadenaWrite += "        </tr>";
            cadenaWrite += "        <tr class='filaazul'>";
            if (vista.equals("2")) {
                cadenaWrite += "        <th>&nbsp;</th>";
            }
            cadenaWrite += "            <th>Numero de Factura</th>";
            cadenaWrite += "            <th>Fecha Vencimiento</th>";
            cadenaWrite += "            <th>Cuotas de manejo a Causar</th>";
            cadenaWrite += "        </tr>";
            cadenaWrite += "    </thead>";
            for (int i = 0; i < cuotaManejoPendList.size(); i++) {
                cadenaWrite += "    <tr class='filableach'>";
                if (vista.equals("2")) {
                    cadenaWrite += "        <td width='10' align='left'><input name='ckestado' type='checkbox' id='ckestado" + i + "' value='" + cuotaManejoPendList.get(i).getValor_10() + "'></td>";

                }
                cadenaWrite += "        <td align='center'>" + cuotaManejoPendList.get(i).getValor_10() + "</td>";
                cadenaWrite += "        <td align='center'>" + cuotaManejoPendList.get(i).getValor_14() + "</td>";
                cadenaWrite += "        <td align='center'>" + Util.FormatoMiles(Double.parseDouble(cuotaManejoPendList.get(i).getValor_05()) - Double.parseDouble(cuotaManejoPendList.get(i).getValor_06())) + "</td>";
                cadenaWrite += "     </tr>";
            }
        } else {
            cadenaWrite += "        <tr class='filaazul'>";
            cadenaWrite += "            <th colspan='"+(vista.equals("2")?"4":"3")+"'>El negocio consultado no tiene cuotas pendientes por causar</th>";
            cadenaWrite += "        </tr>";
            cadenaWrite += "    </thead>";
        }

        cadenaWrite += "</table>";
    }

    private void causarCuotasManejo() throws Exception {
        redirect = false;
        int num_cuotas = request.getParameter("num_cuotas") != null ? Integer.parseInt(request.getParameter("num_cuotas")) : 0;
        
        if (!cuotaManejoPendList.isEmpty()) {
            
            if (num_cuotas <= cuotaManejoPendList.size()) {
                try {
                    
                    TransaccionService tService = new TransaccionService(usuario.getBd());
                    tService.crearStatement();
                    
                    for (int i = 0; i < num_cuotas; i++) {
                        
                        //Seteamos el usuario que anticipa los intereses
                        cuotaManejoPendList.get(i).setValor_19(login);
                        
                        cuotaManejoPendList.get(i).setValor_04(cuotaManejoPendList.get(i).getValor_12());
                        double valor = (Double.parseDouble(cuotaManejoPendList.get(i).getValor_05()) - Double.parseDouble(cuotaManejoPendList.get(i).getValor_06()));
                        
                        String documento = model.Negociossvc.UpCP(cuotaManejoPendList.get(i).getValor_01());
                        
                        tService.getSt().addBatch(model.Negociossvc.ingresarCXCMicrocreditoPost(cuotaManejoPendList.get(i), valor, documento));
                        tService.getSt().addBatch(model.Negociossvc.ingresarDetalleCXCMicrocreditoPost(cuotaManejoPendList.get(i), valor, documento, "1"));
                        
                        valor = Double.parseDouble(cuotaManejoPendList.get(i).getValor_06()) + valor;
                        tService.getSt().addBatch(model.Negociossvc.updateCuotaManejoCausado(valor, cuotaManejoPendList.get(i).getValor_12(), cuotaManejoPendList.get(i).getValor_13(), cuotaManejoPendList.get(i).getValor_07()));
                        
                    }
                    tService.execute();
                    cadenaWrite = "Se han causado " + num_cuotas + " cuota(s)";
                } catch (Exception e) {
                    System.out.println("error en action: " + e.toString());
                    cadenaWrite = e.toString();
                    e.printStackTrace();
                }
            } else {
                cadenaWrite = "Debe digitar un numero de cuotas menor o igual a las que estan pendientes por causar";
            }
        } else {
            cadenaWrite = "El negocio consultado no tiene cuotas pendientes por causar";
        }
    }
}
