
package com.tsp.operation.controller;

import com.tsp.operation.model.*;
import com.tsp.util.Util;
import java.io.*;
import java.util.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
public class MenuMigracionConductorPropietarioAction extends Action{
    
     public void run() throws ServletException, InformationException {
        try{
           String fecha1 = (request.getParameter("FechaInicial")==null )?"":request.getParameter("FechaInicial"); 
           if(!fecha1.equals("")) fecha1 = Util.formatDateOracle(fecha1); 
           HMigracionConductorPropietario hilo = new HMigracionConductorPropietario();
           hilo.start(model,fecha1);
           
           final String next = "/migracion/mostrar1.jsp?Mensaje='Migracion Conductor/Propietario'";
           RequestDispatcher rd = application.getRequestDispatcher(next);
           if(rd == null)
              throw new Exception("No se pudo encontrar "+ next);
           rd.forward(request, response);
        }
        catch(Exception e){
            throw new ServletException(e.getMessage());
        }
     }
}


