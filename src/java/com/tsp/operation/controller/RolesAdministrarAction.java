/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import static com.sun.corba.se.spi.presentation.rmi.StubAdapter.request;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.controller.Action;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.Usuario;
import java.util.Vector;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Roberto Parra
 */
public class RolesAdministrarAction  extends Action  {
    
    private final int LISTAR_ROLES = 1;   
    private final int CREAR_ROL = 2;   
    private final int FORMULARIO_MODIFICACION_ROLES = 3;
    private final int MODIFICAR_ROLES = 4;
    private final int ELIMINAR_ROL = 5;
    @Override
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
           //System.out.println("Usuario login");
        String next= "";
        try {
            HttpSession session  = request.getSession();
            Usuario usuario = (Usuario)session.getAttribute("Usuario");
            
            String asuarioSesion= usuario.getLogin();
            String dstrct= usuario.getDstrct();
            
            int opcion = Integer.parseInt((request.getParameter("opcion")!=null?request.getParameter("opcion"):"0"));
            String urlpagina = "/"+request.getParameter("carpeta") +"/"+request.getParameter("pagina");
            String descripcion = request.getParameter("descripcion"); 
             String id_rol = request.getParameter("id_rol");
            
         

            switch (opcion) {
                case LISTAR_ROLES :
                     ListarRoles();
                    break;
                case CREAR_ROL:
                    CrearRol(descripcion,dstrct,asuarioSesion);
                    break;
                    
                case FORMULARIO_MODIFICACION_ROLES:
                     mostrarFormModiRol(id_rol);
                    break;
                    
                case MODIFICAR_ROLES:
                      ModificarRol(id_rol,descripcion,dstrct,asuarioSesion);
                    break;
                    
                case ELIMINAR_ROL:
                     EliminarRol(id_rol);
                     break;                    
                default:
                    next = urlpagina;
                    break;
            }
                   
        } catch (Exception e) {
            e.printStackTrace();
        }
  
      // Redireccionar a la p�gina indicada.
        if (!next.equals("")) {
            this.dispatchRequest(next);
        }

    }
    
    
        
        private void ListarRoles() throws Exception {
        try {
            RolesDAO rol = new RolesDAO();
            rol.ListarRoles();
            Vector vRoles = rol.getVRoles();
            Gson gson = new Gson();
            String json = gson.toJson(vRoles);
            //this.printlnResponse(json, "application/json;");
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
   
  
   private void CrearRol(String descripcion,
                         String  dstrct, 
                         String creation_user) throws Exception {
      String respuesta="";
        try {
              RolesDAO rol = new RolesDAO();
            respuesta=  rol.CrearRol(descripcion,
                           dstrct, 
                           creation_user);
            
            //Vector vRoles = rol.getVRoles();
            

        } catch (Exception ex) {
            respuesta="Error al crear el rol";
            ex.printStackTrace();
        }
          Gson gson = new Gson();
          String json = gson.toJson(respuesta);
          this.printlnResponseAjax(json, "application/string;");
    }
   
       private void mostrarFormModiRol(String id_rol) throws Exception {
        try {
            RolesDAO rol = new RolesDAO();
            rol.DatosRol(id_rol);
            Vector vRol = rol.getVRoles();

            Gson gson = new Gson();
            String json = gson.toJson(vRol);
            //this.printlnResponse(json, "application/json;");
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }      
   
        private void ModificarRol(String id_rol,
                                  String descripcion,
                                  String dstrct,
                                  String user_update) throws Exception {
           String respuesta="";
        try {
            RolesDAO rol = new RolesDAO();
            respuesta=  rol.ModificarRol(id_rol,
                               descripcion,
                               dstrct,
                               user_update);
            
         //   Vector vRol = rol.getVRoles();
            

        } catch (Exception ex) {
            respuesta="Error al modificar el rol";
            ex.printStackTrace();
        }
            Gson gson = new Gson();
            String json = gson.toJson(respuesta);
            this.printlnResponseAjax(json, "application/string;");
    } 
   
   
      private void EliminarRol(String id_rol) throws Exception {
        String respuesta="";
          try {
             RolesDAO rol = new RolesDAO();
          respuesta =  rol.EliminarRol(id_rol);

        } catch (Exception ex) {
            ex.printStackTrace();
        }            
           Gson gson = new Gson();
           String json = gson.toJson(respuesta);
           this.printlnResponseAjax(json, "application/string;");
    }
      

}
