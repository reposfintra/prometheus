/**
 * Nombre        ReporteMABEAction.java
 * Descripci�n
 * Autor         Mario Fontalvo Solano
 * Fecha         31 de octubre de 2006, 04:57 PM
 * Version       1.0
 * Coyright      Transportes S�nchez Polo S.A.
 **/

package com.tsp.operation.controller;


import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.threads.HReporteMABE;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.finanzas.presupuesto.model.Model;

public class ReporteMABEAction extends Action {
    
    /** Crea una nueva instancia de  ReporteMABEAction */
    public ReporteMABEAction() {
    }
    
    
    public void run() throws ServletException, InformationException {
        try{
            String next = "/jsp/finanzas/presupuesto/reportes/ReporteMabe.jsp";
            String opcion = defaultString("Opcion","");
            String ano    = defaultString("Ano","");
            String mes    = defaultString("Mes","");
            String cliente= defaultString("Cliente","");
            String tipo   = defaultString("Tipo","");
            HttpSession session = request.getSession();
            Usuario usuario  = (Usuario) session.getAttribute("Usuario");
            Model   modelpto = (com.tsp.finanzas.presupuesto.model.Model) session.getAttribute("modelpto");
            
            if (opcion.equals("Generar")){
                HReporteMABE h = new HReporteMABE();
                h.start(cliente, ano, mes, tipo, usuario, modelpto, model);
                request.setAttribute("msg", "Su reporte se ha iniciado consulte el log del proceso para verificar su estado.");
            }
            
            
            this.dispatchRequest(next);
        }catch (Exception ex){
            ex.printStackTrace();
            throw new ServletException (ex.getMessage());
        }
    }
    
    
    
    /**
     * Funcion para obtener un parametro del objeto request
     * y en caso de no existri este devuelve el segundo parametro
     * @autor mfontalvo
     * @param name, nombre del parametro
     * @param opcion, valor opcional a devolver en caso de que nom exista
     * @return Parametro del request
     */
    private String defaultString(String name, String opcion){
        return (request.getParameter(name)==null?opcion:request.getParameter(name));
    }    
}
