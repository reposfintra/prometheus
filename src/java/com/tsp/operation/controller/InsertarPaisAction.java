/*
 * InsertarPaisAction.java
 *
 * Created on 24 de febrero de 2005, 03:30 PM 
 */

package com.tsp.operation.controller;

/**
 *
 * @author  DIBASMO
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;


public class InsertarPaisAction extends Action {
    
    /** Creates a new instance of InsertarPaisAction */
    public InsertarPaisAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException{
        String codigo = (request.getParameter("c_codigo").toUpperCase());
        String nombre = (request.getParameter("c_nombre").toUpperCase());
        String next = "/jsp/trafico/pais/pais.jsp?mensaje=Agregado";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");  
        int sw=0;
        try{ 
            
              Pais pais = new Pais();
              pais.setCountry_code(codigo);
              pais.setCountry_name(nombre);
              pais.setUser_update(usuario.getLogin());
              pais.setCreation_user(usuario.getLogin());
              pais.setUser_update(usuario.getLogin());
            try{
                model.paisservice.insertarPais(pais);   
            }
            catch (SQLException e){
                ////System.out.println("Error " + e.getMessage()+ " Codigo "+ e.getErrorCode() );
                sw=1;
            }
            if ( sw == 1 ){
                if (model.paisservice.existPaisAnulado(codigo)){
                    model.paisservice.Activarpais(pais);               
                }
                else{
                    next = "/jsp/trafico/pais/pais.jsp?mensaje=Codigo de Pais "+ codigo + " ya existe&sw=0";
                }
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
