/*************************************************************************
 * Nombre ......................ActividadBusquedaAction.java             *
 * Descripci�n..................Clase Action para anular actividad       *
 * Autor........................Ing. Diogenes Antonio Bastidas Morales   *
 * Fecha........................26 de agosto de 2005, 03:52 PM           * 
 * Versi�n......................1.0                                      * 
 * Coyright.....................Transportes Sanchez Polo S.A.            *
 *************************************************************************/ 

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  DIOGENES
 */
public class ActividadBusquedaAction extends Action {
    
    /** Creates a new instance of ActividadBusquedaAction */
    public ActividadBusquedaAction() {
    }
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();//System.out.println("ACCION");
        Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
        String distrito = (String) session.getAttribute("Distrito");
        String next = "/jsp/trafico/actividad/actividades.jsp";
        String cod = request.getParameter("codactividad").toUpperCase()+"%";
        String des = request.getParameter("descorta").toUpperCase()+"%";
    try{//System.out.println("cod " +  cod + " des " + des + " u " + usuario.getCia() );
            model.actividadSvc.listarActividadxBusq(usuario.getCia(),cod,des);
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
