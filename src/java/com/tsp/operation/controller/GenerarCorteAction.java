/*
 * GenerarCorteAction.java
 *
 * Created on 31 de mayo de 2005, 07:35 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  kreales
 */
public class GenerarCorteAction extends Action{
    
    /** Creates a new instance of GenerarCorteAction */
    public GenerarCorteAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String next="";
        String fecpla = request.getParameter("fechai");
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        try{
            
            java.util.Enumeration enum1;
            String parametro;
            
            int year=Integer.parseInt(fecpla.substring(0,4));
            int month=Integer.parseInt(fecpla.substring(5,7))-1;
            int date= Integer.parseInt(fecpla.substring(8,10));
            
            Calendar fecha_ant = Calendar.getInstance();
            fecha_ant.set(year,month,date);
            
            Date d = fecha_ant.getTime();
            SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            String feccort = s.format(d);
            
            enum1 = request.getParameterNames(); // Leemos todos los atributos del request
            while (enum1.hasMoreElements()) {
                parametro = (String) enum1.nextElement();
                //System.out.println("Planilla numero = "+parametro);
                if(parametro.length()>5){
                    model.planillaService.updateCorte(feccort, parametro.substring(5));
                }
            }
            model.planillaService.llenarCorte(usuario.getBase());
            fecpla = model.planillaService.buscarFecCorte(usuario.getBase());
            
            year=Integer.parseInt(fecpla.substring(0,4));
            month=Integer.parseInt(fecpla.substring(5,7))-1;
            date= Integer.parseInt(fecpla.substring(8,10));
            
            fecha_ant = Calendar.getInstance();
            fecha_ant.set(year,month,date+1);
            s = new SimpleDateFormat("yyyy-MM-dd");
            d = fecha_ant.getTime();
            feccort = s.format(d);
            next="/Corte/corte.jsp?fec="+feccort;
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
