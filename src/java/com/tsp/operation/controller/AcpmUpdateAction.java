/*
 * AcpmUpdateAction.java
 *
 * Created on 2 de diciembre de 2004, 10:51 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

/**
 *
 * @author  KREALES
 */
public class AcpmUpdateAction extends Action{
    
    static Logger logger = Logger.getLogger(SalidaValidarAction.class);
    /** Creates a new instance of AcpmUpdateAction */
    public AcpmUpdateAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/acpm/acpmUpdate.jsp?msg=";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String nit = request.getParameter("nit");
        String codigo= request.getParameter("codigo");
        float valore=0;
        float valora=0;
        try{
            
            model.proveedoracpmService.buscaProveedor(nit, codigo);
            if(model.proveedoracpmService.getProveedor()!=null){
                Proveedor_Acpm pacpm= model.proveedoracpmService.getProveedor();
                if(pacpm.getTipo().equals("E")){
                    valore=Float.parseFloat(request.getParameter("emax"));
                }
                else if(pacpm.getTipo().equals("G")){
                    valora=Float.parseFloat(request.getParameter("acpm"));
                }
                pacpm.setCity_code(request.getParameter("ciudad"));
                pacpm.setMax_e(valore);
                pacpm.setValor(valora);
                pacpm.setMoneda(request.getParameter("moneda"));
                pacpm.setCreation_user(usuario.getLogin());
                model.proveedoracpmService.updateProveedor(pacpm);
                next+="Datos modificados exitosamente";
            }
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
    
}
