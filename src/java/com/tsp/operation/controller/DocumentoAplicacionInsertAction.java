/********************************************************************
 *      Nombre Clase.................   DocumentoAplicacionInsertAction.java    
 *      Descripci�n..................   Obtiene las actividades de un documento.
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   19.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class DocumentoAplicacionInsertAction extends Action{
        
        /** Creates a new instance of DocumentoInsertAction */
        public DocumentoAplicacionInsertAction() {
        }
        
        public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
                
                //Usuario en sesi�n
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario) session.getAttribute("Usuario");
                String distrito = (String) session.getAttribute("Distrito");
                
                //Pr�xima vista
                String pag  = "/jsp/masivo/tblapl/DocAppInsert.jsp";
                String next = "";                
                
                String[] docs = request.getParameterValues("c_docSelec");
                String[] apl = request.getParameterValues("c_aplSelec");
                
                DocumentoAplicacion obj = new DocumentoAplicacion();
                obj.setDistrito(distrito);
                obj.setUsuario_creacion(usuario.getLogin());
                obj.setUsuario_modificacion(usuario.getLogin());
                obj.setBase(usuario.getBase());
                obj.setEstado("");
                

                try{           
                        Vector dapls = 
                                model.apl_docSvc.obtenerDocumentosAplicacion(obj.getDistrito(), apl[0]);
                        
                        if( docs!=null){
                        
                                for(int k=0; k<dapls.size(); k++){
                                        boolean esta = false;
                                        DocumentoAplicacion dap = (DocumentoAplicacion) dapls.elementAt(k);

                                        for(int j=0; j<docs.length; j++){
                                                if( dap.getDocumento().matches(docs[j]) )
                                                        esta = true;
                                        }

                                        if(!esta){
                                                dap.setEstado("A");
                                                model.apl_docSvc.actualizarDocumentoAplicacion(dap);
                                        }
                                }

                                for(int i=0; i<docs.length; i++){
                                        obj.setDocumento(docs[i]);
                                        obj.setAplicacion(apl[0]);

                                        boolean existe = model.apl_docSvc.existeAplicacionDocumento(obj.getDistrito(),
                                                obj.getDocumento(), obj.getAplicacion());

                                        if(!existe){
                                                model.apl_docSvc.ingresarDocumentoAplicacion(obj);
                                        }
                                        else{
                                                DocumentoAplicacion dap = model.apl_docSvc.obtenerAplicacionDocumento(
                                                        obj.getDistrito(), obj.getDocumento(), obj.getAplicacion());
                                                if( dap.getEstado().matches("A") )
                                                        model.apl_docSvc.actualizarDocumentoAplicacion(obj);
                                        }
                                }
                                
                        }
                        else{
                                for(int k=0; k<dapls.size(); k++){
                                        DocumentoAplicacion dap = (DocumentoAplicacion) dapls.elementAt(k);
                                        dap.setEstado("A");
                                        model.apl_docSvc.actualizarDocumentoAplicacion(dap);
                                }
                        }
                        
                        pag += "?msg=Documentos asignados a la aplicaci�n modificados exitosamente.";
                        next = com.tsp.util.Util.LLamarVentana(pag, "Asignaci�n de Aplicaciones a Documento.");
                                

                }catch (SQLException e){
                       throw new ServletException(e.getMessage());
                }

                this.dispatchRequest(next);
        }
        
}
