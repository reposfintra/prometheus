/*
 * Nombre        UbicacionIngresarAction.java
 * Autor         Ing. Jesus Cuesta
 * Modificado    Ing Sandra Escalante
 * Fecha         21 de junio de 2005, 02:42 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class BuscarUbicacionesAction extends Action{
    
    /** Creates a new instance of BuscarUbicacionesAction */
    public BuscarUbicacionesAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException{
        
        String next = "/" + request.getParameter("carpeta")+ "/" + request.getParameter("pagina")+"?sw=ok";
        
        HttpSession session = request.getSession();
        try{
            int sw = Integer.parseInt(request.getParameter("sw"));
            int control = 0;
            
            if(sw==1){//listar todos                
                model.ubService.listarUbicaciones();
                control = 1;
            }
            else if(sw==2){//buscar x detalle
                
                String codigo = request.getParameter("c_codigo").toUpperCase();
                String desc = request.getParameter("c_descripcion").toUpperCase();
                String tipo=request.getParameter("c_tipou").toUpperCase();
                String cia=request.getParameter("c_cia").toUpperCase();
                String pais=request.getParameter("c_pais").toUpperCase();
                String estado=request.getParameter("c_estado").toUpperCase();
                String ciudad=request.getParameter("c_ciudad").toUpperCase();
                String contacto = request.getParameter("c_contacto").toUpperCase();                            
                
                Ubicacion u = new Ubicacion();
                u.setCod_ubicacion(codigo);
                u.setDescripcion(desc);
                u.setNomcia(cia);
                u.setNomtipo(tipo);
                u.setNomcontacto(contacto);
                u.setNompais(pais);
                u.setNomestado(estado);
                u.setNomciudad(ciudad);
            
                model.ubService.setUb(u);
                model.ubService.consultarUbicaciones();
                
                control = 1;
            }
            else if(sw==3){//obtener ubicacion                
                String codigo = request.getParameter("codigo");
                String dstrct= request.getParameter("dstrct").toUpperCase();
                model.ubService.buscarUbicacion(codigo,  dstrct);
                model.contactoService.buscarContactoCia(model.ubService.getUb().getCia());
             }
            
            if (control == 1){
                if(model.ubService.getUbicaciones().size() == 0 ) {
                    next = "/jsp/trafico/ubicacion/buscarUbicacion.jsp?mensaje=Su b�squeda no arroj� resultados!";
                }
            }
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
