/*
 * Nombre        ImpoExpoAction.java
 * Autor         Osvaldo P�rez Ferrer
 * Fecha         19 de octubre de 2006, 05:43 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import com.tsp.operation.model.threads.HReporteImportacion;

public class ImpoExpoAction extends Action{
    
    /**
     * Crea una nueva instancia de  ImpoExpoAction
     */
    public ImpoExpoAction(){
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next="/jsp/general/reportes/importacion/lista_impo_expo.jsp";
        String opc = request.getParameter("opcion");
        HttpSession session = request.getSession();
        Usuario u = (Usuario) session.getAttribute("Usuario");
        
        try{
            
            if( opc.equals( "impo") ){
                
                String cod = request.getParameter( "impo" );
                if( cod != null ){
                    
                    model.impoExpoService.obtenerImportacion( u.getDstrct(), cod );
                    next = "/jsp/general/reportes/importacion/lista_impo_expo.jsp?opc=impo";
                    
                }else{
                    next = "/jsp/general/reportes/importacion/lista_impo_expo.jsp";
                }
            }else if( opc.equals( "sinCDR" ) ){
                
                model.impoExpoService.obtenerImpoSinFecha( u.getDstrct());
                next = "/jsp/general/reportes/importacion/lista_impo_expo.jsp?opc=sinCDR";
                
            }else if( opc.equals( "todasImpo" ) ){
                
                model.impoExpoService.obtenerTodasImpoExpo( u.getDstrct(), "IPT" );
                next = "/jsp/general/reportes/importacion/lista_impo_expo.jsp?opc=todasImpo";
                
            }else if( opc.equals( "modify" ) ){
                
                String codigo  = request.getParameter("codigo");
                String llegada = request.getParameter("llegada");
                String salida  = request.getParameter("salida");
                if( codigo != null ){
                    
                    model.impoExpoService.obtenerImportacion( u.getDstrct(), codigo);
                    next = "/jsp/general/reportes/importacion/modificar.jsp";
                    request.setAttribute("llegada", llegada);
                    request.setAttribute("salida", salida);
                }
                
            }
            else if( opc.equals( "guardar" ) ){
                
                String codigo = request.getParameter("codigo");
                String llegada = request.getParameter("llegada");
                String salida = request.getParameter("salida");
                
                model.impoExpoService.grabarFechasCDR( u.getDstrct(), codigo, llegada, salida);
                model.impoExpoService.obtenerImportacion( u.getDstrct(), codigo);
                //model.impoExpoService.obtenerTodasImpoExpo( u.getDstrct(), "IMP" );
                next = "/jsp/general/reportes/importacion/modificar.jsp?mensaje=Se grabaron las fechas para la Importaci�n "+codigo;
                
            }else if ( opc.equals("report") ){
                
                String impos = request.getParameter("impos");
                
                if( impos != null && !impos.equals("") ){
                    
                    HReporteImportacion r = new HReporteImportacion();
                    //r.start( u.getLogin(), impos, u.getDstrct() );
                    r.start( u, impos, model );
                }
                
            }
        }catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
