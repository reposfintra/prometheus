/*
 * TasaIngresarAction.java
 * 
 * Created on 29 de junio de 2005, 04:32 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  DIOGENES
 */
public class TasaIngresarAction extends Action {
    
    /** Creates a new instance of TasaIngresarAction */
    public TasaIngresarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String moneda1 = (request.getParameter("c_combom1").toUpperCase());
        String moneda2 = (request.getParameter("c_combom2").toUpperCase());
        String fec = (request.getParameter("c_fecha").toUpperCase());
        float conver = Float.parseFloat(request.getParameter("c_conver"));
        float compra = Float.parseFloat(request.getParameter("c_compra"));
        float venta = Float.parseFloat(request.getParameter("c_venta"));
        
        String next = "/jsp/trafico/tasa/Tasa.jsp?mensaje=Tasa ingresada exitosamente!";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");  
        int sw=0;
        try{ 
            Tasa tasa = new Tasa();
            tasa.setMoneda1(moneda1);
            tasa.setMoneda2(moneda2);
            tasa.setFecha(fec );
            tasa.setVlr_conver(conver);
            tasa.setCompra(compra);
            tasa.setVenta(venta);
            tasa.setCia(usuario.getCia());
            tasa.setUser_update(usuario.getLogin());
            tasa.setCreation_user(usuario.getLogin());
            tasa.setBase(usuario.getBase());
            try{
                if(model.tasaService.existeTasa(tasa.getMoneda1(),tasa.getMoneda2(),tasa.getFecha(),tasa.getCia())){
                    next = "/jsp/trafico/tasa/Tasa.jsp?mensaje=Tasa ya existe!";    
                }else{
                    model.tasaService.insertarTasa(tasa);
                }
                
            }
            catch (SQLException e){
                sw=1;
            }
            if ( sw == 1 ){
                if ( model.tasaService.existeTasaAnulado(moneda1, moneda2, fec, usuario.getCia()) ){
                    model.tasaService.activarTasa(tasa);
                }
                else{
                    request.setAttribute("sw","0");                    
                    next = "/jsp/trafico/tasa/Tasa.jsp?mensaje= No puede Ingresar valores muy extensos en Conversion, Compra y Venta!";                     
                }
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}
