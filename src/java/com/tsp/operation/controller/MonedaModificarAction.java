/*
 * MonedaModificarAction.java
 *
 * Created on 25 de octubre de 2005, 01:49 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  dbastidas
 */
public class MonedaModificarAction extends Action{
        
        /** Creates a new instance of MonedaModificarAction */
        public MonedaModificarAction() {
        }
        
        public void run() throws ServletException, InformationException {
                String codigo = (request.getParameter("c_codigo").toUpperCase());
                String des = (request.getParameter("c_des").toUpperCase());
                String ini = (request.getParameter("c_ini").toUpperCase());
                String next = "/jsp/cxpagar/moneda/Moneda.jsp?sw=1&mensaje=Moneda Modificada con exito...";
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario) session.getAttribute("Usuario");  
                int sw=0;
                try{
                        Moneda moneda = new Moneda();
                        moneda.setCodMoneda(codigo);
                        moneda.setNomMoneda(des);
                        moneda.setInicial(ini);
                        moneda.setUser_update(usuario.getLogin());
                        moneda.setCreation_user(usuario.getLogin()); 
                        moneda.setEstado("");
                        model.monedaSvc.modificarMoneda(moneda);
            
            
                }catch (SQLException e){
                        throw new ServletException(e.getMessage());
                }
                this.dispatchRequest(next);
        }
        
}
