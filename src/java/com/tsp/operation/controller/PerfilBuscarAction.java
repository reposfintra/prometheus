/*
 * Nombre        PerfilBuscarAction.java
 * Autor         Ing. Sandra M. Escalante G.
 * Fecha         10 de marzo de 2005, 03:48 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Administrador
 */
public class PerfilBuscarAction extends Action {
    
    /** Creates a new instance of PerfilBuscarAction */
    public PerfilBuscarAction() {
    }
        
    public void run() throws javax.servlet.ServletException, InformationException{                  
        String next= request.getParameter("carpeta") + "/" + request.getParameter("pagina");
        
        try{          
            Vector prfs = null;
            int sw= 0;
            
            if(request.getParameter("tipo").equals("todo")) {
		model.perfilService.listarPerfilesxPropietario();
                prfs = model.perfilService.getVPerfil();   
                sw= 1;
            }
            if(request.getParameter("tipo").equals("Busqueda")){
                String idp = request.getParameter("idp").toUpperCase();      
                String nom = request.getParameter("nom").toUpperCase();   
                model.perfilService.buscarPerfilesxPropietarioenDetalle(idp, nom); 
                prfs = model.perfilService.getVPerfil();            
                sw= 1;
            }
           
            if(request.getParameter("tipo").equals("objeto")) {
                 String idp = request.getParameter("idp"); 
                 
                  model.perfilService.obtenerPerfilxPropietario(idp);
                  Perfil prf = model.perfilService.getPerfil();
                  request.setAttribute("perfil",prf);                  

                 next= request.getParameter("carpeta") + "/Perfil.jsp?tipo=Buscar&msg=";
            }
            if ( sw == 1 ){
                if (prfs.size() == 0 ){
                    next=request.getParameter("carpeta") + "/BuscarPerfil.jsp?msg=ok";                
                }
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);                   
    }
}