/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import static com.sun.corba.se.spi.presentation.rmi.StubAdapter.request;
import com.tsp.operation.controller.Action;
import com.tsp.operation.model.DAOS.*;
import com.tsp.operation.model.beans.Unidad_Negocio;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.perfilRiesgo;
import java.util.ArrayList;
import java.util.Vector;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

/**
 *
 * @author JM-DESARROLLO
 */
public class PerfilesRiesgosAction extends Action {
    
    private final int LISTAR_PERFILES = 1;
    private final int ELIMINAR_PERFILES = 2;
    private final int CREAR_PERFILES = 3;
    private final int FORMULARIO_MODIFICACION_PERFILES = 4;
    private final int MODIFICAR_PERFILES = 5;
    private final int LISTAR_UNIDAD_NEGOCIO= 6;
    private final int LISTAR_UNIDAD_NEGOCIO_DE_UN_PERFIL= 7;
    
    
    @Override
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
           //System.out.println("Usuario login");
        String next= "";
        try {
            
            HttpSession session  = request.getSession();
            Usuario datosUsuario = (Usuario)session.getAttribute("Usuario");
            String usuarioSesion= datosUsuario.getLogin();
            String dstrct= datosUsuario.getDstrct();
            int opcion = Integer.parseInt((request.getParameter("opcion")!=null?request.getParameter("opcion"):"0"));

            String urlpagina = "/"+request.getParameter("carpeta") +"/"+request.getParameter("pagina");
             
            String id = request.getParameter("id");
            String monto_maximo = request.getParameter("monto_maximo");
            String monto_minimo = request.getParameter("monto_minimo");
            String cant_max_cred = request.getParameter("cant_max_cred");
            String descripcion = request.getParameter("descripcion");
            if(descripcion != null){
                descripcion=descripcion.toUpperCase();
                }

            String idPerfil = request.getParameter("idperfil");  
            String unidades_negocio = request.getParameter("unidades_selecionadas");  
            String estadoActual = request.getParameter("estadoActual");  
            
            
           // String dstrct = request.getParameter("dstrct");

            switch (opcion) {
                case LISTAR_PERFILES :
                     getPerfilesRiesgos();
                    break;
                    
                case ELIMINAR_PERFILES:
                     eliminarPerfilRiesgos(idPerfil,estadoActual);
                     break;
                     
                case CREAR_PERFILES:
                     crearPerfilesRiesgos(monto_maximo,monto_minimo,cant_max_cred,descripcion,
                                          unidades_negocio/*,libranza,consumo,educativo,microcredito*/,
                                          usuarioSesion,dstrct);
                     break;
                    
                case FORMULARIO_MODIFICACION_PERFILES:
                     mostrarFormModiPerf(idPerfil);
                    break;
                    
                case MODIFICAR_PERFILES:
                     modificarPerfilesRiesgos(idPerfil,monto_maximo,monto_minimo,cant_max_cred,descripcion,
                                              unidades_negocio,/*libranza,consumo,educativo,microcredito,*/usuarioSesion,
                                              dstrct);
                    break;
                case LISTAR_UNIDAD_NEGOCIO:
                    ListarUnidNego();
                    break;
                case LISTAR_UNIDAD_NEGOCIO_DE_UN_PERFIL:
                    ListarUnidNegoPerf(idPerfil);
                break;
                default:

                    next = urlpagina;
                    break;
            }
                   
        } catch (Exception e) {
            e.printStackTrace();
        }
  
      // Redireccionar a la p�gina indicada.
        if (!next.equals("")) {
            this.dispatchRequest(next);
        }

    }

    
        private void getPerfilesRiesgos() throws Exception {
        try {
            PerfilesDeRiesgoDao perfilRiesgo = new PerfilesDeRiesgoDao();
            ArrayList<perfilRiesgo> listaPerfles= perfilRiesgo.ListarPerfiles();
            
            Gson gson = new Gson();
            String json = gson.toJson(listaPerfles);
            //this.printlnResponse(json, "application/json;");
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
        
  
     private void eliminarPerfilRiesgos(String idPerfil,String estadoActual) throws Exception {
        String  respuesta ="";
         try {
            PerfilesDeRiesgoDao perfilRiesgo = new PerfilesDeRiesgoDao();
            
            String nuevoEstado="";
            if(estadoActual.equals("")){
                nuevoEstado="A";
            }else{
                nuevoEstado="";
            };  
            
            respuesta = perfilRiesgo.EliminarPerfiles(idPerfil,nuevoEstado);
           if(respuesta.equals("ok")){
             
              respuesta="Se a eliminado correctamente el perfil";
           }
           
            

        } catch (Exception ex) {
            respuesta="Error al eliminar el perfil";
            ex.printStackTrace();
        }
        Gson gson = new Gson();
        String json = gson.toJson(respuesta);
       this.printlnResponseAjax(json, "application/string;");
    }
     
         private void crearPerfilesRiesgos(String monto_maximo,String monto_minimo,
                                           String cant_max_cred,String descripcion,
                                           String unidades_negocio,
                                           String usuarioSesion,
                                           String dstrct) throws Exception {
       String respuesta="";
        try {

            if(monto_maximo.length()>12 || monto_minimo.length()>12){
               respuesta="Los montos no pueden superar los 12 d�gitos";
            } else if(cant_max_cred.length()>4){
                 respuesta="La cantidad maxima de creditos no puede ser superior a 4 digitos";
            }           
            else if(unidades_negocio.equals("")){
               respuesta="Debe de seleccionar al menos una unidad";
            }else {
               PerfilesDeRiesgoDao perfilRiesgo = new PerfilesDeRiesgoDao();
               respuesta = perfilRiesgo.CrearPerfiles(monto_maximo, monto_minimo, 
                                                        cant_max_cred, descripcion, 
                                                        usuarioSesion, dstrct);
            
               if(respuesta.equalsIgnoreCase("ok")){
                 respuesta =  asignarUnidadNeg(descripcion,unidades_negocio, 
                                                  usuarioSesion,dstrct);
             
                
               }
            }


        } catch (Exception ex) {
            respuesta="Error al crear el perfil";
            ex.printStackTrace();
        }
       Gson gson = new Gson();
       String json = gson.toJson(respuesta);
       this.printlnResponseAjax(json, "application/string;");
    }
   
  private String asignarUnidadNeg(String nombrePerfil,
                               String unidades_negocio,
                               String creation_user,
                               String dstrct) throws Exception {
      
      
            String respuesta="";
        try {
            PerfilesDeRiesgoDao perfilRiesgo = new PerfilesDeRiesgoDao();
             

            String idPerfil =  perfilRiesgo.buscarIdPerfil(nombrePerfil);

             
             if(!"".equals(idPerfil)){
                 perfilRiesgo.EliminarUnidadesAsignadas(idPerfil);
                 if(!unidades_negocio.equals("")){
                   String[] unidades = unidades_negocio.split(",");
                   for (String idunidad : unidades){
                     respuesta =  perfilRiesgo.asignarUnidadNeg(idPerfil,
                                                                idunidad,
                                                                creation_user,
                                                                dstrct);
                     if(!"ok".equals(respuesta)){
                     respuesta="Error al asignar las unidades";
                    }
                   }
                 }else{
                 respuesta="ok";
                 }

             }else{
             respuesta="Error al asignar las unidades";
             }

        } catch (Exception ex) {
              respuesta="Error al asignar las unidades";
            ex.printStackTrace();
        }
        return respuesta;
    }
   
    private void mostrarFormModiPerf(String idPerfil) throws Exception {
        try {
            PerfilesDeRiesgoDao perfilRiesgo = new PerfilesDeRiesgoDao();
            
            ArrayList<perfilRiesgo> listaPerfles= perfilRiesgo.ListarPerfil(idPerfil);


            Gson gson = new Gson();
            String json = gson.toJson(listaPerfles);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }      
         
  private void modificarPerfilesRiesgos( String idPerfil,
                                         String monto_maximo,
                                         String monto_minimo,
                                         String cant_max_cred,
                                         String descripcion,
                                         String unidades_negocio,
                                         String usuarioSesion,
                                         String dstrct) throws Exception {
        String respuesta="";
        try {
            
            if(monto_maximo.length()>12 || monto_minimo.length()>12){
               respuesta="Los montos no pueden superar los 12 d�gitos";
            } else if(cant_max_cred.length()>4){
                 respuesta="La cantidad maxima de creditos no puede ser superior a 4 digitos";
            } else if(unidades_negocio.equals("")){
               respuesta="Debe de seleccionar al menos una unidad";
            }else{
              PerfilesDeRiesgoDao perfilRiesgo = new PerfilesDeRiesgoDao();
              respuesta = perfilRiesgo.ModificarPerfiles(idPerfil,
                                      monto_maximo,
                                      monto_minimo, 
                                      cant_max_cred, 
                                      descripcion, 
                                      usuarioSesion,
                                      dstrct);
             if(respuesta.equals("ok")){
               respuesta = asignarUnidadNeg(descripcion, unidades_negocio,
                                         /*libranza,consumo,educativo,
                                         microcredito,*/usuarioSesion,dstrct);   
             }

             }
             
 

        } catch (Exception ex) {
            respuesta="Error al modificar el perfil";
            ex.printStackTrace();
        }
          Gson gson = new Gson();
          String json = gson.toJson(respuesta);
          this.printlnResponseAjax(json, "application/string;");
    }
  
     
    private void ListarUnidNego() throws Exception {
        try {
            PerfilesDeRiesgoDao perfilRiesgo = new PerfilesDeRiesgoDao();
            ArrayList<Unidad_Negocio> listaunidades= perfilRiesgo.ListarUniNego();
            


            Gson gson = new Gson();
            String json = gson.toJson(listaunidades);
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }  
    
        private void ListarUnidNegoPerf(String idPerfil) throws Exception {
        try {
            PerfilesDeRiesgoDao perfilRiesgo = new PerfilesDeRiesgoDao();
            
            ArrayList<Unidad_Negocio> listaUnidadesDelPerfil= perfilRiesgo.ListarUniNegoPerf(idPerfil);
            
            //Vector vUnidNego = perfilRiesgo.getvUnidadNegocio();

            Gson gson = new Gson();
            String json = gson.toJson(listaUnidadesDelPerfil);
            //this.printlnResponse(json, "application/json;");
            this.printlnResponseAjax(json, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }  
     /*   
    public void printlnResponse(String respuesta, String contentType) throws Exception {

        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);

    }*/
}


