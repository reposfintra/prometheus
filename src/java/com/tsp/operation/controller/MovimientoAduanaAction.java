/*
 * Nombre        MovimientoAduanaAction.java
 * Descripción   Clase que ejecuta las acciones necesarias para elaborar el reporte de 
 *               movimiento de aduana.
 * Autor         Alejandro Payares
 * Fecha         19 de enero de 2006, 03:53 PM
 * Version       1.0
 * Coyright      Transportes Sanchez Polo SA.
 */

package com.tsp.operation.controller;

/**
 * Clase que ejecuta las acciones necesarias para elaborar el reporte de 
 * movimiento de aduana.
 * @author Alejandro Payares
 */
public class MovimientoAduanaAction extends Action{
    
    /**
     * Crea una nueva instancia de MovimientoAduanaAction
     * @autor  Alejandro Payares
     */
    public MovimientoAduanaAction() {
    }
    
    
    /**
     * Ejecuta la accion propia de la clase
     * @throws ServletException si algun problema ocurre al enviar la respuesta al cliente
     * @throws InformationException para enviar mensajes informativos
     */    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "";
        try {
            ////System.out.println("MovimientoAduanaAction llamado");
            String cmd = request.getParameter("cmd");
            if ( "show".equals(cmd) ) {
                String numrem = request.getParameter("remesa");
                String detalle = request.getParameter("detalle");
                model.movAduanaService.buscarMovimientos(numrem);
                next = "/jsp/sot/body/PantallaReporteMovAduana.jsp?numrem="+numrem+"&detalle="+("estadoaduana".equals(detalle)?"estados":"observaciones");
            }
        }
        catch( Exception ex ){
            ex.printStackTrace();
            throw new javax.servlet.ServletException(ex.getMessage());
        }
        finally {
            this.dispatchRequest(next);
        }
    }
    
}
