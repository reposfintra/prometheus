/******************************************************************
* Nombre ......................VehNoRetornadoBuscarAction.java
* Descripci�n..................Clase Action para Vehiculos no retornados
* Autor........................Armando Oviedo
* Fecha........................19/12/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
/**
 *
 * @author  Armando Oviedo
 */
public class VehNoRetornadoBuscarAction extends Action{
    
    /** Creates a new instance of VehNoRetornadoBuscarAction */
    public VehNoRetornadoBuscarAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "/jsp/trafico/VehNoRetornado/VehNoRetornadoListar.jsp";
        try{
            String mensaje = request.getParameter("mensaje");
            if(mensaje!=null){
                if(mensaje.equalsIgnoreCase("listartodos")){                    
                    model.vehnrsvc.buscarTodosVehiculosNoRetornados();
                }
                else if(mensaje.equalsIgnoreCase("listar")){
                    String fecha = request.getParameter("fechar");
                    String placa = request.getParameter("placa");
                    fecha = (fecha==null? "" : fecha);
                    placa = (placa==null? "" : placa);
                    VehNoRetornado tmp = new VehNoRetornado();
                    tmp.setFecha(fecha);
                    tmp.setPlaca(placa.toUpperCase());
                    model.vehnrsvc.setVehiculoNoRetornado(tmp);                    
                    model.vehnrsvc.buscarVehiculosNoRetornados();
                }
                else if(mensaje.equalsIgnoreCase("obtenervnr")){
                    next = "/jsp/trafico/VehNoRetornado/VehNoRetornadoModificar.jsp";
                    String fecha = request.getParameter("fechar");
                    String placa = request.getParameter("placa");
                    VehNoRetornado tmp = new VehNoRetornado();
                    tmp.setFecha(fecha);
                    tmp.setPlaca(placa.toUpperCase());
                    model.vehnrsvc.setVehiculoNoRetornado(tmp);                    
                    model.vehnrsvc.buscarVehiculoNoRetornado();
                    model.vehnrsvc.buscarCausas();
                }
            }
        }catch(SQLException ex){
            ex.printStackTrace();
        }
        this.dispatchRequest(next);
    }
    
}
