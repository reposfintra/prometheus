/*
 * ExportPlanillasOpcionesAction.java
 *
 * Created on 22 de diciembre de 2004, 19:13
 */

package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.exceptions.*;
import java.util.*;

/**
 *
 * @author  Administrador
 */
public class ExportPlanillasOpcionesAction extends Action{
    
    /** Creates a new instance of ExportPlanillasOpcionesAction */
    public ExportPlanillasOpcionesAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException{
        try {
            String Opcion     = request.getParameter("Opcion");
            String Fecha      = request.getParameter("fecha_creacion");
            String Mensaje    = "Se ha iniciado la creacion del archivo MST620, puede verificar el estado en" +
            " Procesos/Seguimiento de Procesos, una vez terminado puede consultar los archivos creados durante el proceso en el Directorio de Archivos";
            String next="/migracion/remesas.jsp";
            //String Directorio   = "/usr/local/jakarta/jakarta-tomcat-4.1.24/webapps/fintravalores/Migraciones/";
            
            ///sandrameg 160905
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");            
            
            String Directorio = "/exportar/masivo/";            
            //CODIGO AGREGADO POR KAREN REALES..
            if(request.getParameter("colpapel")==null){
                if (Opcion.equals("Procesar")){
                    model.PlanillaImpresionSvc.ReiniciarPlanillasExoprt();
                    model.PlanillaImpresionSvc.BuscarPlanillasExport(Fecha);
                    HExport_MS230828 Hilo = new HExport_MS230828();
                    Hilo.start(model.PlanillaImpresionSvc.getPlanillasExport(), Fecha, Directorio);
                    Mensaje = "Su peticion ha sido enviada";
                }
                next = "/migracion/remesas.jsp?Mensaje="+Mensaje;
            }
            else{
                String base = request.getParameter("base");
                String tipo="";    
                if(request.getParameter("CGAGEN")==null){
                    tipo = "1";
                    model.PlanillaImpresionSvc.searchPlanillas(Fecha, base);
                    next = "/migracion/plasColpapel.jsp?Mensaje="+Mensaje;
                }
                else{
                    tipo = "2";
                    model.PlanillaImpresionSvc.searchPlanillas_CGAGEN(Fecha, base);
                    next = "/migracion/plasCGAGEN.jsp?Mensaje="+Mensaje;
                }
                Vector vec = model.PlanillaImpresionSvc.getPlas();
                HExport_MST620 hilo = new HExport_MST620();
                
                //190905
                hilo.start(Fecha, base, vec,model,tipo, usuario.getLogin());
                Mensaje = "Se ha iniciado la creacion del archivo MST620...";
                
            }
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
        }
        catch(Exception e) {
            throw new ServletException(e.getMessage());
        }
        
        
    }
    
}
