/*
 * MonedaBuscarxAction.java
 *
 * Created on 25 de octubre de 2005, 01:55 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  dbastidas
 */
public class MonedaBuscarxAction extends Action{
        
        /** Creates a new instance of MonedaBuscarxAction */
        public MonedaBuscarxAction() {
        }
        public void run() throws ServletException, InformationException {
                String next = "/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina");
                String des = (request.getParameter("c_des").toUpperCase());
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario) session.getAttribute("Usuario");  
                session.setAttribute("nom", des);
                
                this.dispatchRequest(next);
        }
        
}
