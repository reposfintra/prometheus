/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.AuditoriaIngresosDAO;
import com.tsp.operation.model.DAOS.impl.AuditoriaIngresosImpl;
import com.tsp.operation.model.beans.AuditoriaIngresos;
import com.tsp.operation.model.beans.POIWrite;
import com.tsp.operation.model.beans.PrevisualizarExtractos;
import com.tsp.operation.model.beans.ReporteSanciones;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.Utility;
import java.io.File;
import java.lang.reflect.Type;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.ResourceBundle;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;

/**
 *
 * @author lcanchila
 */
public class AuditoriaIngresosAction extends Action {

    private final int CARGAR_AUDITORIA_INGRESOS = 1;
    private final int EXPORTAR_AUDITORIA_INGRESOS = 2;
    private final int CARGAR_REPORTE_COSECHAS= 3;
    private final int EXPORTAR_DETALLE_COSECHAS= 4;
    private final int CARGAR_CUENTAS_CAIDAS= 5;
    private final int DETALLE_CUENTAS_CAIDAS= 6;
    private final int EXPORTAR_DETALLE_CUENTAS_CAIDAS= 7;

    Usuario usuario = null;
    private AuditoriaIngresosDAO dao;
    POIWrite xls;
    private int fila = 0;
    HSSFCellStyle header, titulo1, titulo2, titulo3, titulo4, titulo5, letra, numero, dinero, numeroCentrado, porcentaje, letraCentrada, numeroNegrita, ftofecha;
    private SimpleDateFormat fmt;
    String rutaInformes;
    String nombre;
    String iduser;

    @Override
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            iduser = usuario.getLogin().toUpperCase();
            dao = new AuditoriaIngresosImpl(usuario.getBd());
            int opcion = (request.getParameter("opcion") != null)
                    ? Integer.parseInt(request.getParameter("opcion"))
                    : -1;
            switch (opcion) {
                case CARGAR_AUDITORIA_INGRESOS:
                    this.cargarAuditoriaIngresos();
                    break;
                case EXPORTAR_AUDITORIA_INGRESOS:
                    this.exportarAuditoriaIngresos();
                    break;
                case CARGAR_REPORTE_COSECHAS:
                    this.cargarReporteCosechas();
                    break; 
                case EXPORTAR_DETALLE_COSECHAS:
                    this.exportarDetalleCosecha();
                    break;
                case CARGAR_CUENTAS_CAIDAS:
                    this.cargarCuentasCaidas();
                    break; 
                case DETALLE_CUENTAS_CAIDAS:
                    this.detalleCuentasCaidas();
                    break;  
                case EXPORTAR_DETALLE_CUENTAS_CAIDAS:
                    this.exportarDetalleCuentasCaidas();
                    break;    
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void printlnResponse(String respuesta, String contentType) throws Exception {
        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);
    }

    private void cargarAuditoriaIngresos() throws Exception {
        String periodoIni = request.getParameter("periodoIni");
        String periodoFin = request.getParameter("periodoFin");
        int ini = Integer.parseInt(periodoIni);
        int fin = Integer.parseInt(periodoFin);
        ArrayList<AuditoriaIngresos> lista = new ArrayList<AuditoriaIngresos>();
        for (int i = ini; i <= fin; i++) {
            lista.addAll(dao.cargarAuditoriaIngresos(i, usuario.getLogin()));
        }

        Gson gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
        this.printlnResponse(json, "application/json;");
    }

    private void exportarAuditoriaIngresos() throws SQLException {
        boolean generado = false;
        String url = "";
        String resp1 = "";
        String periodoIni = request.getParameter("periodoIni");
        String periodoFin = request.getParameter("periodoFin");
        int ini = Integer.parseInt(periodoIni);
        int fin = Integer.parseInt(periodoFin);
        ArrayList<AuditoriaIngresos> lista = new ArrayList<AuditoriaIngresos>();
        try {
            for (int i = ini; i <= fin; i++) {
                lista.addAll(dao.cargarAuditoriaIngresos(i, usuario.getLogin()));
            }

            this.generarRUTA();
            this.crearLibro("AuditoriaIngresos_", "AUDITORIA DE INGRESOS");

            String[] cabecera = {
                "PERIODO","COD NEGOCIO", "CEDULA", "NOMBRE CLIENTE", "VENCIMIENTO MAYOR", "FECHA PAGO INGRESO", "FECHA VENC. MAYOR", "DIFERENCIA PAGO", "VR. SALDO FOTO","INTERES MORA", "GASTO COBRANZA", "NUMERO INGRESO", 
                "VR. INGRESO", "VR. CXC INGRESO", "G16252145", "G94350302", "GI010010014205", "GI010130014205", "I16252147", "I94350301","II010010014170","II010130024170", "I010140014170", "I010140014205", "I28150530","I28150531","VR. INTERES X MORA INGRESO", "VR. GASTO COBRANZA INGRESO","CONVENIO"
            };
            short[] dimensiones = new short[]{
                3000,7000, 3000, 9000, 7000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000,5000,5000
            };
            this.generaTitulos(cabecera, dimensiones);

            generado = generaArchivoAuditoriaIngresos(lista);

            System.out.println("exportar " + generado);
            this.cerrarArchivo();
            if (generado) {
                fmt = new SimpleDateFormat("yyyMMdd");
                url = request.getContextPath() + "/exportar/migracion/" + iduser + "/AuditoriaIngresos_" + fmt.format(new Date()) + ".xls";
                resp1 = Utility.getIcono(request.getContextPath(), 5) + "Informe generado con exito.<br/><br/>"
                        + Utility.getIcono(request.getContextPath(), 7) + " <a href='" + url + "' >Ver Informe</a></td>";
            }
            this.printlnResponse(resp1, "text/plain");
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE EXPORTACION DE AUDITORIA INGRESOS. \n " + e.getMessage());
        }
    }

    public void generarRUTA() throws Exception {
        try {

            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            rutaInformes = rb.getString("ruta") + "/exportar/migracion/" + iduser;
            File archivo = new File(rutaInformes);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }

    }

    private void crearLibro(String nameFileParcial, String titulo) throws Exception {
        try {
            fmt = new SimpleDateFormat("yyyMMdd");
            this.crearArchivo(nameFileParcial + fmt.format(new Date()) + ".xls", titulo);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage());
        }
    }

    private void crearArchivo(String nameFile, String titulo) throws Exception {
        try {
            fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            InitArchivo(nameFile);
            xls.obtenerHoja("Base");
            xls.combinarCeldas(0, 0, 0, 8);
            xls.adicionarCelda(0, 0, titulo, header);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en crearArchivo ....\n" + ex.getMessage());
        }
    }

    private void InitArchivo(String nameFile) throws Exception {
        try {
            xls = new com.tsp.operation.model.beans.POIWrite();
            nombre = "/exportar/migracion/" + iduser + "/" + nameFile;
            xls.nuevoLibro(rutaInformes + "/" + nameFile);
            header = xls.nuevoEstilo("Tahoma", 10, true, false, "text", HSSFColor.GREEN.index, xls.NONE, HSSFCellStyle.ALIGN_LEFT);
            titulo1 = xls.nuevoEstilo("Tahoma", 8, true, false, "text", xls.NONE, xls.NONE, xls.NONE);
            titulo2 = xls.nuevoEstilo("Tahoma", 8, true, false, "text", HSSFColor.WHITE.index, HSSFColor.GREEN.index, HSSFCellStyle.ALIGN_CENTER, 2);
            letra = xls.nuevoEstilo("Tahoma", 8, false, false, "", xls.NONE, xls.NONE, xls.NONE);
            dinero = xls.nuevoEstilo("Tahoma", 8, false, false, "#,##0.00", xls.NONE, xls.NONE, xls.NONE);
            porcentaje = xls.nuevoEstilo("Tahoma", 8, false, false, "0.00%", xls.NONE, xls.NONE, xls.NONE);
            ftofecha = xls.nuevoEstilo("Tahoma", 8, false, false, "yyyy-mm-dd", xls.NONE, xls.NONE, xls.NONE);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }

    }

    private void generaTitulos(String[] cabecera, short[] dimensiones) throws Exception {

        try {

            fila = 2;

            for (int i = 0; i < cabecera.length; i++) {
                xls.adicionarCelda(fila, i, cabecera[i], titulo2);
                if (i < dimensiones.length) {
                    xls.cambiarAnchoColumna(i, dimensiones[i]);
                }
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage());
        }
    }

    private void cerrarArchivo() throws Exception {
        try {
            if (xls != null) {
                xls.cerrarLibro();
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            throw new Exception("Error en crearArchivo ....\n" + ex.getMessage());
        }
    }

    private boolean generaArchivoAuditoriaIngresos(ArrayList<AuditoriaIngresos> lista) throws Exception {
        boolean generado = true;
        try {
            // Inicia el proceso de listar a excel
            AuditoriaIngresos auditoria = new AuditoriaIngresos();
            Iterator it = lista.iterator();
            while (it.hasNext()) {

                auditoria = (AuditoriaIngresos) it.next();
                fila++;
                int col = 0;
                xls.adicionarCelda(fila, col++, auditoria.getPeriodo(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getNegocio(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getCedula(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getNombre_cliente(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getVencimiento_mayor(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getFecha_pago_ingreso(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getFecha_vencimiento_mayor(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getDiferencia_pago(), dinero);
                xls.adicionarCelda(fila, col++, auditoria.getValor_saldo_foto(), dinero);
                xls.adicionarCelda(fila, col++, auditoria.getInteres_mora(), dinero);
                xls.adicionarCelda(fila, col++, auditoria.getGasto_cobranza(), dinero);
                xls.adicionarCelda(fila, col++, auditoria.getNum_ingreso(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getValor_ingreso(), dinero);
                xls.adicionarCelda(fila, col++, auditoria.getValor_cxc_ingreso(), dinero);
                xls.adicionarCelda(fila, col++, auditoria.getG16252145(), dinero);
                xls.adicionarCelda(fila, col++, auditoria.getG94350302(), dinero);
                xls.adicionarCelda(fila, col++, auditoria.getGI010010014205(), dinero);
                xls.adicionarCelda(fila, col++, auditoria.getGI010130014205(), dinero);
                xls.adicionarCelda(fila, col++, auditoria.getI16252147(), dinero);
                xls.adicionarCelda(fila, col++, auditoria.getI94350301(), dinero);
                xls.adicionarCelda(fila, col++, auditoria.getII010010014170(), dinero);
                xls.adicionarCelda(fila, col++, auditoria.getII010130024170(), dinero);
                xls.adicionarCelda(fila, col++, auditoria.getI010140014170(), dinero);
                xls.adicionarCelda(fila, col++, auditoria.getI010140014205(), dinero);
                xls.adicionarCelda(fila, col++, auditoria.getI28150530(), dinero);
                xls.adicionarCelda(fila, col++, auditoria.getI28150531(), dinero);
                xls.adicionarCelda(fila, col++, auditoria.getValor_ixm_ingreso(), dinero);
                xls.adicionarCelda(fila, col++, auditoria.getValor_gac_ingreso(), dinero);
                xls.adicionarCelda(fila, col++, auditoria.getConvenio(), letra);

            }

        } catch (Exception ex) {
            generado = false;
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage());
        }
        return generado;
    }

    private void cargarReporteCosechas() throws Exception {
        String periodoIni = request.getParameter("periodoIni");
        String periodoFin = request.getParameter("periodoFin");
        String und_negocio = request.getParameter("unidad_negocio");
        String[] listUnd = und_negocio.split(",");
        ArrayList<AuditoriaIngresos> lista = new ArrayList<AuditoriaIngresos>();
        lista = dao.cargarReporteCosechas(periodoIni,periodoFin,listUnd);
                   
        Gson gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
        this.printlnResponse(json, "application/json;");
    }

    private void exportarDetalleCosecha() throws Exception {
        String periodoIni = request.getParameter("periodoIni");
        String periodoFin = request.getParameter("periodoFin");
        String und_negocio = request.getParameter("unidad_negocio");
        String venc_mayor = request.getParameter("venc_mayor");
        String[] listUnd = und_negocio.split(",");
        ArrayList<AuditoriaIngresos> lista = new ArrayList<AuditoriaIngresos>();
        boolean generado = false;
        String url = "";
        String resp1 = "";
        try {
            for(int i= 0; i < listUnd.length; i++){
                lista.addAll(dao.consultarDetalleCosecha(periodoIni,periodoFin,listUnd[i],venc_mayor));
            }
            this.generarRUTA();
            this.crearLibro("DetalleCosecha_", "DETALLE REPORTE COSECHA");

            String[] cabecera = {
                "CEDULA","NOMBRE","UNIDAD_NEGOCIO","NEGOCIO","AFILIADO","FECHA APROBACION","FECHA DESEMBOLSO","PERIODO DESEMBOLSO","TOTAL DESEMBOLSADO","PLAZO","CUOTA","CUOTAS VENCIDAS","ANALISTA",
                "ASESOR COMERCIAL","COBRADOR TELEFONICO","COBRADOR CAMPO","FECHA ULTIMO PAGO","VENCIMIENTO MAYOR","VENCIMIENTO MAYOR MAXIMO","TRAMO ANTERIOR","FECHA VENCIMIENTO","DIRECCION","TELEFONO",
                "CELULAR","EMAIL","ESTRATO","OCUPACION","DEPARTAMENTO","MUNICIPIO","BARRIO","NOMBRE EMPRESA","CARGO"," VR. COLOCACION","PAGOS","SALDO","SALDO X VENCER"
            };
            short[] dimensiones = new short[]{
                3000,9000, 7000, 3000, 7000, 5000, 5000, 5500, 5500, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 7000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000
            };
            this.generaTitulos(cabecera, dimensiones);

            generado = generaArchivoDetalleCosecha(lista);

            System.out.println("exportar " + generado);
            this.cerrarArchivo();
            if (generado) {
                fmt = new SimpleDateFormat("yyyMMdd");
                url = request.getContextPath() + "/exportar/migracion/" + iduser + "/DetalleCosecha_" + fmt.format(new Date()) + ".xls";
                resp1 = Utility.getIcono(request.getContextPath(), 5) + "Informe generado con exito.<br/><br/>"
                        + Utility.getIcono(request.getContextPath(), 7) + " <a href='" + url + "' >Ver Informe</a></td>";
            }
            this.printlnResponse(resp1, "text/plain");
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE EXPORTACION DE COSECHA. \n " + e.getMessage());
        }
    }

    private boolean generaArchivoDetalleCosecha(ArrayList<AuditoriaIngresos> lista) throws Exception {
        boolean generado = true;
        try {
            // Inicia el proceso de listar a excel
            AuditoriaIngresos auditoria = new AuditoriaIngresos();
            Iterator it = lista.iterator();
            while (it.hasNext()) {

                auditoria = (AuditoriaIngresos) it.next();
                fila++;
                int col = 0;
                xls.adicionarCelda(fila, col++, auditoria.getCedula(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getNombre_cliente(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getUnidad_negocio(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getNegocio(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getAfiliado(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getFecha_aprobacion(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getFecha_desembolso(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getPeriodo(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getTotal_desembolsado(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getPlazo(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getCuota(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getCuotas_vencidas(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getAnalista(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getAsesor_comercial(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getCobrador_telefonico(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getCobrador_campo(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getFecha_ultimo_pago(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getVencimiento_mayor(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getVencimiento_mayor_maximo(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getTramo_anterior(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getFecha_vencimiento_mayor(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getDireccion(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getTelefono(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getCelular(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getEmail(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getEstrato(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getOcupacion(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getDepartamento(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getMunicipio(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getBarrio(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getNombre_empresa(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getCargo(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getColocacion(), dinero);
                xls.adicionarCelda(fila, col++, auditoria.getValor_pagos(), dinero);
                xls.adicionarCelda(fila, col++, auditoria.getSaldo(), dinero);
                xls.adicionarCelda(fila, col++, auditoria.getSaldo_porvencer(), letra);
                

            }

        } catch (Exception ex) {
            generado = false;
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage());
        }
        return generado;
    }

    private void cargarCuentasCaidas() throws Exception {
        String periodo = request.getParameter("periodo");
        String und_negocio = request.getParameter("unidad_negocio");
        ArrayList<AuditoriaIngresos> lista = new ArrayList<AuditoriaIngresos>();
        lista = dao.cargarCuentasCaidas(periodo,und_negocio);
        Gson gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
        this.printlnResponse(json, "application/json;");
    }

    private void detalleCuentasCaidas() throws Exception {
        String periodo = request.getParameter("periodo");
        String und_negocio = request.getParameter("unidad_negocio");
        String venc_mayor = request.getParameter("venc_mayor");
        ArrayList<AuditoriaIngresos> lista = new ArrayList<AuditoriaIngresos>();
        lista = dao.detalleCuentasCaidas(periodo,und_negocio,venc_mayor);
        Gson gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
        this.printlnResponse(json, "application/json;");
    }

    private void exportarDetalleCuentasCaidas() throws SQLException {
        boolean generado = false;
        String url = "";
        String resp1 = "";
        try {
            String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
            if (!json.equals("")) {
                Gson gson = new Gson();
                Type tipoListaFacturas;
                tipoListaFacturas = new TypeToken<ArrayList<AuditoriaIngresos>>() {
                }.getType();
                ArrayList<AuditoriaIngresos> lista = gson.fromJson(json, tipoListaFacturas);
                this.generarRUTA();
                this.crearLibro("ReporteCuentasCaidas_", "REPORTE CUENTAS CAIDAS");

                String[] cabecera = {
                    "Cedula", "Nombre", "Negocio", "Unidad de Negocio", "Convenio", "Valor Debido", "Valor Cuota", "Valor Recaudo","Recaudo Aplicado al Debido"
                    
                };
                short[] dimensiones = new short[]{
                    4000, 11000, 4000, 5000, 3000, 4000, 4000, 4000, 5000
                };
                this.generaTitulos(cabecera, dimensiones);
                generado = generarArchivoExcelCuentasCaidas(lista);

                System.out.println("exportar " + generado);
                this.cerrarArchivo();
                if (generado) {
                    fmt = new SimpleDateFormat("yyyMMdd");
                    url = request.getContextPath() + "/exportar/migracion/" + usuario.getLogin().toUpperCase() + "/ReporteCuentasCaidas_" + fmt.format(new Date()) + ".xls";
                    resp1 = Utility.getIcono(request.getContextPath(), 5) + "Informe generado con exito.<br/><br/>"
                            + Utility.getIcono(request.getContextPath(), 7) + " <a href='" + url + "' >Ver Informe</a></td>";
                }
                this.printlnResponse(resp1, "text/plain");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE EXPORTACION DE CUENTAS CAIDAS. \n " + e.getMessage());
        }
    }

    private boolean generarArchivoExcelCuentasCaidas(ArrayList<AuditoriaIngresos> lista) throws Exception {
        boolean generado = true;
        try {
            // Inicia el proceso de listar a excel
            AuditoriaIngresos auditoria = new AuditoriaIngresos();
            Iterator it = lista.iterator();
            while (it.hasNext()) {

                auditoria = (AuditoriaIngresos) it.next();
                fila++;
                int col = 0;
                xls.adicionarCelda(fila, col++, auditoria.getCedula(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getNombre_cliente(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getNegocio(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getUnidad_negocio(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getConvenio(), letra);
                xls.adicionarCelda(fila, col++, auditoria.getValor_debido(), dinero);
                xls.adicionarCelda(fila, col++, auditoria.getValor_cuota(), dinero);
                xls.adicionarCelda(fila, col++, auditoria.getValor_recaudo(), dinero);
                xls.adicionarCelda(fila, col++, auditoria.getValor_recaudo_debido(), dinero);
            }

        } catch (Exception ex) {
            generado = false;
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage());
        }
        return generado;
    }

}
