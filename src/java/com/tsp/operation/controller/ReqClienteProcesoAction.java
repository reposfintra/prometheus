/*
 * ReqClienteProcesoAction.java
 *
 * Created on 25 de junio de 2005, 08:34 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import javax.swing.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

/**
 *
 * @author  Sandrameg
 */
public class ReqClienteProcesoAction extends Action{
    
    /** Creates a new instance of ReqClienteProcesoAction */
    public ReqClienteProcesoAction() {
    }
    
    public void run() throws ServletException,InformationException{
        
        HttpSession session = request.getSession();
        Usuario u = (Usuario) session.getAttribute("Usuario");
        
        String ff = "", fi = "", ds = "", tp = "", cl="";
        String next = "";
        
        fi = (request.getParameter("fechai")!=null)?request.getParameter("fechai"): Util.getFechaActual_String(4);
        ff = request.getParameter("fechaf");
        ds = u.getDstrct();
        tp = request.getParameter("tipo");  
        
        Util ut= new Util();
        Calendar fchi = ut.crearCalendarDate(fi);
        Calendar fchf;
        
        if ( (tp.equals("nuevo_fron")) || (tp.equals("act_fron")) ){
            fchf = ut.crearCalendarDate(ut.getFechaActual_String(4));
            
            next = "/pags_predo/reqClienteF.jsp?msg=La fecha inicial no puede ser mayor que la fecha actual";
        }
        else {
            fchf = ut.crearCalendarDate(ff);
            next = "/pags_predo/reqCliente.jsp?msg=La fecha final no puede ser menor que la fecha inicial";
        }

        
        
        if (fchf.compareTo(fchi) >= 0){
        
            ReqClienteThread rct = new ReqClienteThread();
            rct.star(u.getLogin(), fi, ff, ds, tp);
        
            String msg = "El Proceso ha iniciado";
         
            if ( (tp.equals("nuevo_pto")) || (tp.equals("act_pto")) ){
                next = "/pags_predo/reqCliente.jsp?msg=" + msg;            
            }
            else if ( (tp.equals("nuevo_fron")) || (tp.equals("act_fron")) ){
                next = "/pags_predo/reqClienteF.jsp?msg=" + msg;            
            }
        }
        
        this.dispatchRequest(next);
    }   
    
   
    
}
