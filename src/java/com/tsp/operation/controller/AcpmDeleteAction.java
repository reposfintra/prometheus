/*
 * AcpmDeleteAction.java
 *
 * Created on 2 de diciembre de 2004, 11:30 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class AcpmDeleteAction extends Action{
    
    /** Creates a new instance of AcpmDeleteAction */
    public AcpmDeleteAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/acpm/acpmDelete.jsp?msg=";
        String nit = request.getParameter("nit");
        String codigo= request.getParameter("codigo");
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String base =usuario.getBase();
        try{
            model.proveedoracpmService.buscaProveedor(nit,codigo);
            if(model.proveedoracpmService.getProveedor()!=null){
                Proveedor_Acpm pacpm= model.proveedoracpmService.getProveedor();
                model.proveedoracpmService.anularProveedor(pacpm);
                next+="Proveedor anulado";
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
        
    }
    
    
}
