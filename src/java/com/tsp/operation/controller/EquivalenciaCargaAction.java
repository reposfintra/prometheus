/********************************************************************
 * Nombre clase: EquivalenciaCargaAction.java
 * Descripción: Accion para ingresar un registro de equivalencia de carga
 * Autor: Jose de la rosa
 * Fecha: 12 de enero de 2007, 02:37 PM
 * Versión: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class EquivalenciaCargaAction extends Action{
    
    /** Creates a new instance of EquivalenciaCargaAction */
    public EquivalenciaCargaAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "";
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String dstrct = (String) session.getAttribute ("Distrito");
        String opcion = request.getParameter ("opcion");
        String codigo_carga = request.getParameter  ("c_codigo_carga")!=null?request.getParameter ("c_codigo_carga").toUpperCase ():"";
        String desc_carga = request.getParameter    ("c_desc_carga")!=null?request.getParameter ("c_desc_carga"):"";
        String carga_legal = request.getParameter   ("c_carga_legal")!=null?request.getParameter ("c_carga_legal").toUpperCase ():"";
        String tipo_carga = request.getParameter    ("c_tipo_carga")!=null?request.getParameter ("c_tipo_carga").toUpperCase ():"";
        double vlr_mercancia = Double.parseDouble   ( request.getParameter ("c_vlr_mercancia")!=null?request.getParameter ("c_vlr_mercancia"):"0" );
        String und_mercancia = request.getParameter ("c_und_mercancia")!=null?request.getParameter ("c_und_mercancia").toUpperCase ():"";
        String desc_corta = request.getParameter    ("c_desc_corta")!=null?request.getParameter ("c_desc_corta"):"";
        int sw = 0;
        try{
            if( opcion.equals("insert") ){
                EquivalenciaCarga eq = new EquivalenciaCarga ();
                eq.setCodigo_carga      ( codigo_carga);
                eq.setDescripcion_carga ( desc_carga);
                eq.setCodigo_legal      ( carga_legal);
                eq.setTipo_carga        ( tipo_carga);
                eq.setValor_mercancia   ( vlr_mercancia);
                eq.setUnidad_mercancia  ( und_mercancia);
                eq.setDescripcion_corta ( desc_corta);
                eq.setDistrito          ( dstrct.toUpperCase ());
                eq.setUsuario           ( usuario.getLogin ().toUpperCase ());
                eq.setBase              ( usuario.getBase ().toUpperCase ());
                try{
                    model.equivalenciaCargaService.insertEquivalenciaCarga(eq);
                }catch(SQLException e){
                    sw = 1;
                }
                if( sw==1 ){
                    if( !model.equivalenciaCargaService.existEquivalenciaCarga(codigo_carga, dstrct) ){
                        model.equivalenciaCargaService.updateEquivalenciaCarga(eq);
                        request.setAttribute ("mensaje","La información ha sido ingresada exitosamente!");
                    }
                    else{
                        request.setAttribute ("mensaje","Error el codigo de carga ya existe!");
                    }
                }
                else{
                    request.setAttribute ("mensaje","La información ha sido ingresada exitosamente!");
                }
                next = "/jsp/masivo/equivalencia/EquivalenciaCargaInsertar.jsp";
                
                model.tablaGenService.buscarRegistros("TUNIDAD");
                request.setAttribute ( "set_unidad", model.tablaGenService.obtenerTablas());
                model.tablaGenService.buscarRegistros("TIPCARGA");
                request.setAttribute ( "set_tipo", model.tablaGenService.obtenerTablas());
                
            }
            
            else if( opcion.equals("listar") ){
                model.equivalenciaCargaService.listEquivalenciaCarga(dstrct, codigo_carga, carga_legal, tipo_carga);
                next = "/jsp/masivo/equivalencia/EquivalenciaCargaListar.jsp";
            }

            else if( opcion.equals("listarMod") ){
                model.equivalenciaCargaService.searchEquivalenciaCarga(codigo_carga, dstrct);
                
                model.tablaGenService.buscarRegistros("TUNIDAD");
                request.setAttribute ( "set_unidad", model.tablaGenService.obtenerTablas());
                model.tablaGenService.buscarRegistros("TIPCARGA");
                request.setAttribute ( "set_tipo", model.tablaGenService.obtenerTablas());
                next = "/jsp/masivo/equivalencia/EquivalenciaCargaModificar.jsp";
            }
            
            else if( opcion.equals("modificar")){
                EquivalenciaCarga eq = new EquivalenciaCarga ();
                eq.setCodigo_carga      ( codigo_carga);
                eq.setDescripcion_carga ( desc_carga);
                eq.setCodigo_legal      ( carga_legal);
                eq.setTipo_carga        ( tipo_carga);
                eq.setValor_mercancia   ( vlr_mercancia);
                eq.setUnidad_mercancia  ( und_mercancia);
                eq.setDescripcion_corta ( desc_corta);
                eq.setDistrito          ( dstrct.toUpperCase ());
                eq.setUsuario           ( usuario.getLogin ().toUpperCase ());
                eq.setBase              ( usuario.getBase ().toUpperCase ());
                request.setAttribute ("mensaje","La información ha sido modificada exitosamente!");
                model.equivalenciaCargaService.updateEquivalenciaCarga(eq);
                next = "/jsp/masivo/equivalencia/EquivalenciaCargaModificar.jsp?reload=ok";
                model.equivalenciaCargaService.searchEquivalenciaCarga(codigo_carga, dstrct);
                
                model.tablaGenService.buscarRegistros("TUNIDAD");
                request.setAttribute ( "set_unidad", model.tablaGenService.obtenerTablas());
                model.tablaGenService.buscarRegistros("TIPCARGA");
                request.setAttribute ( "set_tipo", model.tablaGenService.obtenerTablas());
            }
            
            else{
                EquivalenciaCarga eq = new EquivalenciaCarga ();
                eq.setCodigo_carga      ( codigo_carga );
                eq.setUsuario           ( usuario.getLogin ().toUpperCase ());
                eq.setDistrito          ( dstrct.toUpperCase ());
                model.equivalenciaCargaService.anularEquivalenciaCarga(eq);
                next="/jsp/trafico/mensaje/MsgAnulado.jsp";
            }
        }catch(SQLException e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
    
    
}
