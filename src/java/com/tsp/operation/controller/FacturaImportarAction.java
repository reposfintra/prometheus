/**
 * ******************************************************************
 * Nombre Clase................. FacturaImportarAction.java
 * Descripción.................. Action que se encarga de guardar un los
 * registros de cxp_doc, cxp_imp_doc, cxp_imp_item,cxp_items_doc
 * Autor........................ Acosta Jesid Fecha........................
 * Agosto 2013 Versión...................... 1.0 Copyright....................
 * Fintra *****************************************************************
 */
package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import com.tsp.finanzas.presupuesto.model.beans.Upload;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import com.tsp.util.ExcelApplication;
import org.apache.log4j.Logger;
import com.tsp.util.Util;
import com.tsp.util.UtilFinanzas;

/**
 *
 * @author dlamadrid
 */
public class FacturaImportarAction extends Action {

    /**
     * Creates a new instance of FacturaGuardarAction
     */
    public FacturaImportarAction() {
    }

    public void run() throws ServletException, InformationException {
        try {
            String nomArchivo = (request.getParameter("nombrearchivo") != null)
                    ? request.getParameter("nombrearchivo")
                    : "";
            String maxfila = request.getParameter("maxfila");    
            DecimalFormatSymbols simbolo = new DecimalFormatSymbols();
            simbolo.setDecimalSeparator('.');
            simbolo.setGroupingSeparator(',');
            DecimalFormat formateador = new DecimalFormat("###,###.##", simbolo);

            if (!nomArchivo.equals("")) {
                if (maxfila.equals("-1")) {
                    cargarExcel(nomArchivo);
                } else {
                    leerExcel(nomArchivo);
                }
            } else {
                String detalleRes = "";
                try {

                    detalleRes = "<table align='center'>"
                            + "   <tr>    <td colspan='6' class=\"subtitulo1\" align='center'>Se presentaron estos problemas</td> </tr>"
                            + "   <tr  id='filaT' >"
                            + "       <td class=\"barratitulo\" align='center' width='65'>numero</td>"
                            + "       <td class=\"barratitulo\" align='center' width='100'>cedula</td>"
                            + "       <td class=\"barratitulo\" align='center' width='350'>nombre completo</td>"
                            + "       <td class=\"barratitulo\" align='center' width='100'>valor a pagar</td>"
                            + "       <td class=\"barratitulo\" align='center' width='200'>Mensaje</td>"
                            + "   </tr>";

                    String Modificar = (request.getParameter("Modificar") != null) ? request.getParameter("Modificar") : "";
                    java.util.Date utilDate = new java.util.Date(); //fecha actual
                    long lnMilisegundos = utilDate.getTime();
                    java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(lnMilisegundos);//Fecha Actual en sql
                    int sw = 0;
                    String fechaActual = "" + sqlTimestamp;
                    HttpSession session = request.getSession();
                    //Ivan 21 julio 2006
                    com.tsp.finanzas.contab.model.Model modelcontab = (com.tsp.finanzas.contab.model.Model) session.getAttribute("modelcontab");
                    ////////////////////////////

                    Usuario usuario = (Usuario) session.getAttribute("Usuario");
                    String dstrct = usuario.getDstrct();
                    String base = usuario.getBase();
                    String moneda_usuario = (String) session.getAttribute("Moneda");
                    String tipo_documento = "" + request.getParameter("tipo_documento");
                    String tipo_documento_rel = "" + request.getParameter("tipo_documento_rel");

                    if (tipo_documento_rel.equals("null")) {
                        tipo_documento_rel = "";
                    }
                    String documento_relacionado = "" + request.getParameter("documento_relacionado");
                    if (documento_relacionado.equals("null")) {
                        documento_relacionado = "";
                    }
                    String fecha_documento = "" + request.getParameter("fecha_documento");
                    String banco = "" + request.getParameter("c_banco");
                    double vlr_neto = 0;
                    int plazo = 0;
                    double vlr_total = 0, sumatoria = 0;
                    String sucursal = "" + request.getParameter("c_sucursal");
                    String moneda = "" + request.getParameter("moneda");//moneda de la factura
                    String descripcion = "" + request.getParameter("descripcion");
                    String observacion = "" + request.getParameter("observacion");
                    String usuario_aprobacion = "" + request.getParameter("usuario_aprobacion");
                    String agencia = (request.getParameter("agencia") != null) ? request.getParameter("agencia") : "BQ";
                    String ni = (request.getParameter("num_items") != null) ? request.getParameter("num_items") : "0";
                    String validar = "" + request.getParameter("validar");
                    String CabIva = (request.getParameter("CabIva") != null ? request.getParameter("CabIva").toUpperCase() : "");
                    String CabRiva = (request.getParameter("CabRiva") != null ? request.getParameter("CabRiva").toUpperCase() : "");
                    String CabRica = (request.getParameter("CabRica") != null ? request.getParameter("CabRica").toUpperCase() : "");
                    String CabRfte = (request.getParameter("CabRfte") != null ? request.getParameter("CabRfte").toUpperCase() : "");
                    int num_items = 0;
                    String moneda_banco = (request.getParameter("moneda_banco") != null) ? request.getParameter("moneda_banco") : "";

                    //Ivan Dario 28 Octubre 2006
                    String agenciaBanco = (request.getParameter("agenciaBanco") != null) ? request.getParameter("agenciaBanco") : "";
                    /////////////////////////////////
                    String beneficiario = (request.getParameter("beneficiario") != null) ? request.getParameter("beneficiario") : "";
                    String hc = (request.getParameter("hc") != null) ? request.getParameter("hc") : "";

                    //Ivan 21 sep
                    double saldo_me_anterior = Double.parseDouble((request.getParameter("saldo_me_anterior") != null) ? request.getParameter("saldo_me_anterior") : "0");
                    double saldo_anterior = Double.parseDouble((request.getParameter("saldo_anterior") != null) ? request.getParameter("saldo_anterior") : "0");
                    ///////////////////////////////////////////
                    //double total_neto       =  model.cxpDocService.getNumero(""+ request.getParameter("total_neto"));

                    String fecha_aprobacion = fechaActual;
                    String usu_ap = (request.getParameter("usu_ap") != null ? request.getParameter("usu_ap") : usuario_aprobacion);

                    try {
                        num_items = Integer.parseInt(ni);
                        //vlr_neto    = model.cxpDocService.getNumero(""+ request.getParameter("vlr_neto") );                                               //valor a pagar
                        //vlr_total   = model.cxpDocService.getNumero(""+ request.getParameter("total"));                                                     //valor a pagar
                        plazo = Integer.parseInt("" + request.getParameter("plazo"));
                    } catch (java.lang.NumberFormatException e) {
                        vlr_total = 0;
                        vlr_neto = 0;
                        plazo = 0;
                        num_items = 1;
                    }
                    //String datos = request.getParameter("vDatos"); 

                    int MaxFi = Integer.parseInt(maxfila);
                    String secuencia = model.cxpDocService.getSecuenca_CXP_Doc("CXP");                    //------------------------Generar seraial 
                    String documento;
                    String proveedor;                                                                     //------------------------Varia con el hilo
                    String codigo_cuenta = request.getParameter("cod_cuenta");
                    Proveedor o_proveedor = null;
                    Vector vItems;
                    int b = 0;
                    String next = "";
                    String mensaje_error = "";
                    CXP_Doc factura;
                    boolean Error = false;
                    boolean ErrorCuenta = false;

                    // Modificacion 21 julio 2006
                    boolean Existe = false;
                    boolean ReqAuxiliar = false;
                    Hashtable datoCuenta;
                    LinkedList tbltipo = null;
                    String auxiliar = "";
                    String tipoSubledger = "";
                    if (modelcontab.planDeCuentasService.existCuenta(usuario.getDstrct(), codigo_cuenta)) {                            // comprueba cuenta
                        if (model.cxpDocService.CuentaModuloCXP(usuario.getDstrct(), codigo_cuenta)) {
                            modelcontab.subledgerService.buscarCuentasTipoSubledger(codigo_cuenta);
                            tbltipo = modelcontab.subledgerService.getCuentastsubledger();
                            Existe = true;
                            datoCuenta = model.cxpDocService.buscarCuenta(usuario.getDstrct(), codigo_cuenta);
                            if (datoCuenta != null) {
                                String subledger = (String) datoCuenta.get("subledger");
                                if (subledger.equals("S") && auxiliar.equals("")) {
                                    ReqAuxiliar = true;
                                    Error = true;
                                    mensaje_error += "El auxiliar es obligatorio ";

                                } else if (subledger.equals("N") && !auxiliar.equals("")) {
                                    ReqAuxiliar = true;
                                    Error = true;
                                    mensaje_error += "El auxiliar no es requerido para la cuenta ";
                                }
                            } else {
                                Error = true;
                                mensaje_error += "El numero de cuenta debe ser de detalle";
                            }
                        } else {
                            Error = true;
                            ErrorCuenta = true;
                            mensaje_error += "El numero de cuenta no pertenece al modulo CXP";
                        }

                    } else {
                        Error = true;
                        ErrorCuenta = true;
                        mensaje_error += "El numero de cuenta no existe en la base de datos";
                    }
                    //////////////////////////////////////////////////////////////////////
                    //documento = request.getParameter("detalle");
                    if (Error) {
                        detalleRes += " <tr  id='fila" + 0 + "' class=\"fila\">"
                                + "   <td align='center' colspan='5'>" + mensaje_error + "</td>"
                                + "   </tr>";
                        //return;
                    } else {
                        String id_mims = "";
                        for (int fila = 0; fila < MaxFi; fila++) {
                            //String check = request.getParameter("chk" + fila);
                            if (request.getParameter("chk" + fila) == null) {
                                continue;
                            }
                            documento = secuencia.replace("#", com.tsp.util.Utility.rellenar(String.valueOf((fila+1)), 3));
                            beneficiario = request.getParameter("beneficiario" + fila);
                            vlr_neto = Double.parseDouble(request.getParameter("valor" + fila));
                            proveedor = request.getParameter("proveedor" + fila);
                            sumatoria = sumatoria + vlr_neto;
                            try {
                                o_proveedor = model.proveedorService.obtenerProveedorPorNit(proveedor);
                                if(o_proveedor == null) {
                                    throw new Exception("Proveedor no existe");
                                }
                                if(  o_proveedor.getC_banco_transfer().equalsIgnoreCase("")
                                  || o_proveedor.getC_sucursal_transfer().equalsIgnoreCase("")
                                  || o_proveedor.getC_numero_cuenta().equalsIgnoreCase("")
                                  || o_proveedor.getNit_beneficiario().equalsIgnoreCase("")) {
                                    throw new Exception("Informacion bancaria incompleta");
                                }
                                id_mims = o_proveedor.getC_idMims();
                            } catch (Exception e) {
                                detalleRes += " <tr  id='fila" + fila + "' class=\"fila\">"
                                        + "   <td align='center' width='50'>" + documento + "</td>"
                                        + "   <td align='center' width='100'>"
                                        + "   <input type=\"label\" class=\"textbox\" readonly value=\"" + proveedor + "\">"
                                        + "   </td>"
                                        + "   <td>" + beneficiario + "</td>"
                                        + "   <td align='center' width='100'>"
                                        + "   <input type=\"text\" class=\"textboxFactura\" readonly value=\"" + vlr_neto + "\">"
                                        + "   </td>"
                                        + "   <td>"+e.getMessage()+"</td>"
                                        + "   </tr>";
                                continue;
                            }

                            factura = new CXP_Doc();

                            //Ivan 21 sep
                            factura.setValor_saldo_anterior(saldo_anterior);
                            factura.setValor_saldo_me_anterior(saldo_me_anterior);
                            ////////////////////////

                            factura.setDstrct(dstrct);
                            factura.setProveedor(proveedor);
                            factura.setTipo_documento(tipo_documento);
                            factura.setDocumento(documento);                                        //---------------- asignacion del documeto a factura
                            factura.setDescripcion(descripcion);
                            factura.setAgencia(agencia);
                            //factura.setHandle_code(handle_code);
                            factura.setId_mims(id_mims);
                            factura.setFecha_documento(fecha_documento);
                            factura.setTipo_documento_rel(tipo_documento_rel);
                            factura.setDocumento_relacionado(documento_relacionado);
                            factura.setFecha_aprobacion("0099-01-01 00:00:00");
                            factura.setUsuario_aprobacion("");

                            factura.setClase_documento_rel("4");
                            factura.setTipo_nomina("S");


                            factura.setAprobador(usuario_aprobacion);
                            String fecha_vencimiento = "";
                            Util u = new Util();
                            fecha_vencimiento = u.fechaFinalYYYYMMDD(fecha_documento, plazo);
                            factura.setFecha_vencimiento(fecha_vencimiento);
                            factura.setUltima_fecha_pago("0099-01-01 00:00:00");
                            factura.setBanco(banco);
                            factura.setSucursal(sucursal);
                            factura.setMoneda(moneda);
                            factura.setMoneda_banco(moneda_banco);
                            factura.setTotal_neto(vlr_neto);                      // ----------valor a pagar ------
                            //Ivan DArio 28 Octubre 2006
                            factura.setAgenciaBanco(agenciaBanco);
                            ////////////////////////////////////
                            factura.setBeneficiario(beneficiario);
                            factura.setHandle_code(hc);

                            double valor_local = 0;
                            double valor_tasa = 0;
                            //System.out.println("Moneda usuario "+ moneda_usuario + "Moneda 1 "+moneda+" Moneda 2 "+moneda_usuario);
                            Tasa tasa = model.tasaService.buscarValorTasa(moneda_usuario, moneda, moneda_usuario, fecha_documento);
                            if (tasa != null) {
                                valor_tasa = tasa.getValor_tasa();
                            }

                            factura.setTasa(valor_tasa);
                            factura.setUsuario_contabilizo("");
                            factura.setFecha_contabilizacion("0099-01-01 00:00:00");
                            factura.setUsuario_anulo("");
                            factura.setFecha_anulacion("0099-01-01 00:00:00");
                            factura.setFecha_contabilizacion_anulacion("0099-01-01 00:00:00");
                            factura.setObservacion(observacion);
                            factura.setNum_obs_autorizador(0);
                            factura.setNum_obs_pagador(0);
                            factura.setNum_obs_registra(0);
                            factura.setCreation_user(usuario.getLogin());
                            factura.setUser_update(usuario.getLogin());
                            factura.setBase(base);
                            factura.setPlazo(plazo);
                            factura.setIva(CabIva);
                            factura.setRiva(CabRiva);
                            factura.setRica(CabRica);
                            factura.setRfte(CabRfte);



                            //-------------------------------------------------------------------------------------------------------------------------------------------------------------
                            vItems = new Vector();

                            double totalFac = 0;
                            double totalFacLocal = 0;

                            CXPItemDoc item = new CXPItemDoc();

                            String cod_item = "1";

                            String descripcion_i = descripcion;                                                                  //------------------------Fijo con el hilo
                            String concepto = "";                                                                  //------------------------Fijo con el hilo
                            String codigo_abc = "";
                            String planilla = "";
                            String tipcliarea = "C";
                            String codcliarea = "";
                            String iva = "";                                                                    //------------------------Fijo con el hilo

                            String vlr_riva = "";                                                                      //------------------------Fijo con el hilo
                            String vlr_rica = "";                                                                      //------------------------Fijo con el hilo
                            String vlr_rfte = "";                                                                      //------------------------Fijo con el hilo

                            //Ivan 26 julio 2006
                            String ree = "";
                            String Ref3 = "";
                            String Ref4 = "";
                            String agenciaItem = "BARRANQUILLA";

                            String[] codigos = {"", "", "", "", ""};

                            if (codcliarea.equals("")) {
                                tipcliarea = "";
                            }

                            double valor = 0;
                            double valor_total = 0;
                            try {
                                valor = vlr_neto; //model.cxpDocService.getNumero(""+vlr_neto );                                 // ---- valor a pagar
                                valor_total = vlr_neto; //model.cxpDocService.getNumero(""+vlr_neto );                              // ----- valor a pagar
                            } catch (java.lang.NumberFormatException e) {
                                valor = 0;
                                valor_total = 0;
                            }

                            item.setErrorCuenta(ErrorCuenta);
                            //Ivan 26 julio 2006
                            item.setRef3(Ref3);
                            item.setRef4(Ref4);
                            item.setRee(ree);
                            item.setAgencia(agenciaItem);

                            item.setExiteCuenta(Existe);
                            item.setCodigos(codigos);
                            item.setDstrct(dstrct);
                            item.setProveedor(proveedor);
                            item.setTipo_documento(tipo_documento);
                            item.setDocumento(documento);                                        //---------------- asignacion del documeto a factura
                            item.setItem(com.tsp.util.Utility.rellenar(String.valueOf(cod_item), 3));
                            item.setDescripcion(descripcion_i);
                            item.setConcepto(concepto);

                            item.setVlr(Util.roundByDecimal(valor * valor_tasa, 0));
                            item.setVlr_me(valor);//valor me del item
                            item.setCodigo_cuenta(codigo_cuenta);
                            item.setCodigo_abc(codigo_abc);
                            item.setPlanilla(planilla);
                            item.setCodcliarea(codcliarea);
                            item.setTipcliarea(tipcliarea);
                            item.setCreation_user(usuario.getLogin());
                            item.setUser_update(usuario.getLogin());
                            item.setBase(base);
                            item.setVlr_total(valor_total);
                            item.setIva(iva);

                            item.setPorc_riva(vlr_riva);
                            item.setPorc_rica(vlr_rica);
                            item.setPorc_rfte(vlr_rfte);

                            // ivan 21 julio 2006
                            item.setAuxiliar(auxiliar);
                            item.setTipo(tbltipo);
                            item.setTipoSubledger(tipoSubledger);
                            item.setReqAuxilar(ReqAuxiliar);

                            totalFac = valor;
                            totalFacLocal = valor;
                            Vector vTipoImp = model.TimpuestoSvc.vTiposImpuestos();
                            Vector vImpuestosPorItem = new Vector();
                            Vector vImpuestosCopia = new Vector();

                            for (int x = 0; x < vTipoImp.size(); x++) {

                                CXPImpItem impItem = new CXPImpItem();
                                impItem.setCod_impuesto("");
                                impItem.setTipo_impuesto("");
                                vImpuestosCopia.add(impItem);
                            }

                            item.setVItems(vImpuestosPorItem);
                            item.setVCopia(vImpuestosCopia);
                            vItems.add(item);


                            valor_local = totalFacLocal;
                            factura.setVlr_neto(valor_local);


                            factura.setVlr_total(totalFac);
                            factura.setVlr_total_abonos(0);
                            factura.setVlr_saldo(valor_local);
                            factura.setVlr_neto_me(totalFac);
                            factura.setVlr_total_abonos_me(0);
                            factura.setVlr_saldo_me(totalFac);

                            //factura.setFecha_aprobacion(fecha_aprobacion);
                            //factura.setUsuario_aprobacion(usu_ap);
                            //}


                            if (usuario_aprobacion.equals(usuario.getLogin()) || totalFac < 0) {
                                factura.setFecha_aprobacion(Util.fechaActualTIMESTAMP());
                                factura.setUsuario_aprobacion(usuario_aprobacion);
                            }

                            model.cxpDocService.setFactura(factura);
                            model.cxpItemDocService.setVecCxpItemsDoc(vItems);
                            //Ivan Validacion para cuando es una nota credito
                            double saldo_doc_rel = 0;
                            double resto = 0;

                            if (factura.getTipo_documento().equals("035") || factura.getTipo_documento().equals("036")) {
                                String clase = model.cxpDocService.BuscarClaseDocumento(dstrct, proveedor, factura.getDocumento_relacionado(), factura.getTipo_documento_rel());
                                factura.setClase_documento_rel(clase);

                                if (!Error) {
                                    if (factura.getVlr_saldo_me() < 0) {
                                        Error = true;
                                        String dec = (factura.getTipo_documento().equals("035")) ? "Nota credito" : "Nota debito";
                                        mensaje_error = "El saldo de la " + dec + "  no puede quedar en negativo";
                                    }
                                }
                            }
                            /////////////////////////////////////////////////

                            if (!Error) {
                                if (tasa != null) {
                                    if (model.cxpDocService.existeDoc(dstrct, proveedor, tipo_documento, documento) == false) {
                                        if ((model.cxpDocService.existeDoc(dstrct, proveedor, tipo_documento_rel, documento_relacionado) == true) || documento_relacionado.equals("")) {
                                            if (sw == 0) {
                                                if (o_proveedor != null) {
                                                    b = 1;

                                                    id_mims = "" + o_proveedor.getC_idMims();
                                                    Vector vImpDoc = new Vector();
                                                    for (int i = 0; i < vItems.size(); i++) {
                                                        CXPItemDoc item2 = new CXPItemDoc();
                                                        item2 = (CXPItemDoc) vItems.elementAt(i);
                                                        if (item2.getVItems() != null) {
                                                            //Vector vImpuestosPorItem = item2.getVItems();
                                                            for (int x = 0; x < vImpuestosPorItem.size(); x++) {
                                                                CXPImpItem impuestoI = (CXPImpItem) vImpuestosPorItem.elementAt(x);
                                                                String cod_impuesto_doc = "";
                                                                double por_impuesto_doc = 0;
                                                                double valor_impuesto = 0;
                                                                double valor_impuesto_me = 0;
                                                                cod_impuesto_doc = impuestoI.getCod_impuesto();
                                                                por_impuesto_doc = impuestoI.getPorcent_impuesto();
                                                                valor_impuesto = impuestoI.getVlr_total_impuesto();
                                                                valor_impuesto_me = impuestoI.getVlr_total_impuesto_me();
                                                                CXPImpDoc impDoc = new CXPImpDoc();
                                                                impDoc.setDstrct(dstrct);
                                                                impDoc.setProveedor(proveedor);
                                                                impDoc.setTipo_documento(tipo_documento);
                                                                impDoc.setDocumento(documento);
                                                                impDoc.setCod_impuesto(cod_impuesto_doc);
                                                                impDoc.setPorcent_impuesto(por_impuesto_doc);
                                                                impDoc.setVlr_total_impuesto(valor_impuesto);
                                                                impDoc.setVlr_total_impuesto_me(valor_impuesto_me);
                                                                impDoc.setCreation_user(usuario.getLogin());
                                                                impDoc.setUser_update(usuario.getLogin());
                                                                impDoc.setBase(base);
                                                                vImpDoc.add(impDoc);
                                                            }
                                                        }
                                                    }

                                                    vImpDoc = model.cxpImpDocService.generarVImpuestosDoc(vImpDoc);
                                                    model.cxpImpDocService.setVImpuestosDoc(vImpDoc);

                                                    model.cxpDocService.insertarCXPDoc(factura, vItems, vImpDoc, agenciaBanco);

                                                    continue;
                                                }

                                                if (b != 1) {
                                                    proveedor = "";
                                                    mensaje_error = "El Proveedor no existe ";
                                                }
                                            }
                                        } else {
                                            proveedor = "";
                                            mensaje_error = "El Documento relacionado no existe en el Sistema ";
                                        }
                                    } else {
                                        proveedor = "";
                                        mensaje_error = "El Documento ya existe ";
                                    }
                                } else {
                                    mensaje_error = "No existe el valor de la tasa";
                                }
                            }
                            detalleRes += " <tr  id='fila" + fila + "' class=\"fila\">"
                                        + "   <td align='center' width='50'>" + documento + "</td>"
                                        + "   <td align='center' width='100'>"
                                        + "   <input type=\"label\" class=\"textbox\" readonly value=\"" + proveedor + "\">"
                                        + "   </td>"
                                        + "   <td>" + beneficiario + "</td>"
                                        + "   <td align='center' width='100'>"
                                        + "   <input type=\"text\" class=\"textboxFactura\" readonly value=\"" + vlr_neto + "\">"
                                        + "   </td>"
                                        + "   <td>Proveedor no existe</td>"
                                        + "   </tr>";
                            factura.setVlr_total(vlr_total);
                            factura.setVlr_neto_me(vlr_neto);
                            factura.setVlr_saldo_me(vlr_total);
                            factura.setVlr_neto(vlr_neto);
                            factura.setUsuario_aprobacion(usuario_aprobacion);
                            factura.setAprobador(usu_ap);
                            factura.setFecha_aprobacion(fecha_aprobacion);
                            model.cxpDocService.setFactura(factura);    
                        }
                        model.cxpDocService.getSecuenca_CXP_Doc("nextval");     //incrementa la secuencia  
                    }
                    detalleRes += " <tfoot> "
                                    + " <tr  class='tblTituloFactura'>  <td colspan='3' align='right'><strong>TOTAL:</strong></td> "
                                        + " <td><strong>$" + formateador.format(sumatoria) +"</strong></td>"
                                        + " <td colspan='1'></td> "
                                    + "</tr>"
                               + " </tfoot> ";
                    detalleRes += "</table>";
                    next = "/jsp/cxpagar/facturasxpagar/BuscarFactura.jsp?ms=ingresados&num_items=0&marco=no&importar=SI&lista=" + detalleRes;
                    this.dispatchRequest(next);
                } catch (Exception e) {
                    //rollback
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new ServletException("Accion:" + e.getMessage());
        }
    }

    public void cargarExcel(String nombre) {
        try {
            Usuario us = (Usuario) request.getSession().getAttribute("Usuario");
            String ruta = UtilFinanzas.obtenerRuta("ruta", "/exportar/migracion/"
                    + ((us != null) ? us.getLogin() : "Indefinido"));

            Upload upload = new Upload(ruta + "/", request);
            upload.load();
            String next = "/jsp/cxpagar/facturasxpagar/facturaImportacion.jsp?num_items=0&marco=no&opcion=7&maxFila=0&Modificar=importar&nombrearchivo=" + nombre;
            this.dispatchRequest(next);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void leerExcel(String nombre) {
        try {

            Usuario us = (Usuario) request.getSession().getAttribute("Usuario");
            String ruta = UtilFinanzas.obtenerRuta("ruta", "/exportar/migracion/"
                    + ((us != null) ? us.getLogin() : "Indefinido"));

            ExcelApplication excel = new ExcelApplication();
            excel.openWorkBook(ruta + "/" + nombre);
            ArrayList<ArrayList<String>> datosI = excel.getDatos(0);    
            DecimalFormatSymbols simbolo = new DecimalFormatSymbols();
            simbolo.setDecimalSeparator('.');
            simbolo.setGroupingSeparator(',');
            DecimalFormat formateador = new DecimalFormat("###,###.##", simbolo);
            double sumatoria = 0;

            String detalleRes = "<tr  id='filaT' class='tblTituloFactura'>"
                    + "    <td align='center' width='30'>"
                    + "       <input type='checkbox' id='chkG' checked='true' onchange=\"cambiarCheckBoxes('tab')\"></td>"
                    + "       <td align='center' width='50'>numero</td>"
                    + "       <td align='center' width='100'>cedula</td>"
                    + "       <td align='center'>nombre completo</td>"
                    + "       <td align='center' width='100'>valor a pagar</td>"
                    + "</tr>";
            for (int i = 0; i < datosI.size(); i++) {
                try {
                    ArrayList<String> d = datosI.get(i);

                    detalleRes += "<tr  id='fila" + i + "' class='filaFactura'>"
                            + "   <td align='center' width='30'>"
                            + "   <input type=\"checkbox\" id=\"chk" + i + "\" name=\"chk" + i + "\" checked=\"true\" onchange=\"cambiarCheckBoxes('sub')\"/>"
                            + "   </td>"
                            + "   <td align='center' width='50'>" + i + "</td>"
                            + "   <td align='center' width='100'>"
                            + "   <input type=\"label\" class=\"textbox\" readonly id = \"proveedor" + i + "\" name = \"proveedor" + i + "\"value=\"" + Integer.parseInt(d.get(0)) + "\">"
                            + "   </td>"
                            + "   <td align='center' >"
                            + "   <input type=\"text\" style=\"width:100%\" class=\"textbox\" readonly id = \"beneficiario" + i + "\" name = \"beneficiario" + i + "\" value=\"" + d.get(1) + "\">"
                            + "   </td>"
                            + "   <td align='center' width='100'>"
                            + "   <input type=\"text\" class=\"textboxFactura\" readonly id = \"valor" + i + "\" name = \"valor" + i + "\" value=\"" + Double.parseDouble(d.get(2)) + "\">"
                            + "   </td>"
                            + "   </tr>";
                    sumatoria  = sumatoria +  Double.parseDouble(d.get(2));
                } catch (Exception e) {
                    e.printStackTrace();
                    continue;
                }
            }
            detalleRes += " <tfoot> "
                        + " <tr class='tblTituloFactura'> <td colspan='4' align='right'><strong>TOTAL:</strong></td> "
                        + " <td width='100'><strong>$" + formateador.format(sumatoria) + "</strong></td>"
                        + " </tr>"
                    + " </tfoot> ";
            responser(detalleRes);

        } catch (Exception exce) {
            System.out.println(exce.getMessage());
            exce.printStackTrace();
        }
    }

    public void responser(String respuesta) throws Exception {
        try {
            response.setContentType("text/plain; charset=UTF-8;");
            response.setHeader("Cache-Control", "no-store");
            response.setDateHeader("Expires", 0);
            response.getWriter().println(respuesta);
        } catch (Exception e) {
            throw new Exception("Error al escribir el response: " + e.toString());
        }
    }
    /*
     select char_length(last_value::TEXT) from serie_iddocumento_cxp 
     * --length ultimo numero de la secuencia

     select generar_iddocumento_cxp('CXP', '00') 
     * --llamar funcion 

     select nextval('serie_iddocumento_cxp');  
     * --incrementar secuencia
     */
}
