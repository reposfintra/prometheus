/********************************************************************
 *      Nombre Clase.................   DocumentoInsertAction.java    
 *      Descripci�n..................   Obtiene un registro de la tabla tbldoc    
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   13.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class DocumentoObtenerAction extends Action{
        
        /** Creates a new instance of DocumentoInsertAction */
        public DocumentoObtenerAction() {
        }
        
        public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
                //Pr�xima vista
                String pag  = "/jsp/masivo/tbldoc/DocumentoUpdate.jsp?mensaje=";
                String next = "";
                
                String cia = request.getParameter("cia");
                String doctype = request.getParameter("doctype");
                
                //Usuario en sesi�n
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario) session.getAttribute("Usuario");

                try{   
                        model.documentoSvc.obtenerDocumento(cia, doctype);                 
                        //System.out.println("---------------> Obtener Action");                      
                        //System.out.println("---------------> cia: " + cia);
                        //System.out.println("---------------> doctype:" + doctype);
                        //System.out.println("---------------> documento:" + model.documentoSvc.getDocumento());
                                                
                        next = com.tsp.util.Util.LLamarVentana(pag, "Actualizar Documento");

                }catch (SQLException e){
                       throw new ServletException(e.getMessage());
                }

                this.dispatchRequest(next);
        }
        
}
