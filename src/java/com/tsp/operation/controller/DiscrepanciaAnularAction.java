/***********************************************
 * Nombre clase: DiscrepanciaAnularAction.java
 * Descripci�n: Accion para anular una diacrepancia a la bd.
 * Autor: Jose de la rosa
 * Fecha: 19 de octubre de 2005, 06:40 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 **********************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;

/**
 *
 * @author  Jose
 */
public class DiscrepanciaAnularAction extends Action{
    
    /** Creates a new instance of DiscrepanciaAnularAction */
    public DiscrepanciaAnularAction () {
    }
    
    public void run () throws ServletException, com.tsp.exceptions.InformationException {
        String next="";
        HttpSession session = request.getSession ();
        int num_discre = Integer.parseInt (request.getParameter ("c_num_discre"));
        String numpla = request.getParameter ("c_numpla").toUpperCase ();
        String numrem = request.getParameter ("c_numrem").toUpperCase ();
        String tipo_doc = request.getParameter ("c_tipo_doc").toUpperCase ();
        String documento = request.getParameter ("c_documento").toUpperCase ();
        String tipo_doc_rel = (String) request.getParameter ("c_tipo_doc_rel");
        String documento_rel = (String) request.getParameter ("c_documento_rel");
        String nom_doc = request.getParameter ("nom_doc");
        String clientes = request.getParameter ("client");
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String distrito = (String) session.getAttribute ("Distrito");
        try{
            if ( model.discrepanciaService.existeDiscrepancia (numpla, numrem, tipo_doc, documento, tipo_doc_rel, documento_rel,distrito ) ) {
                Vector consultas = new Vector ();
                Discrepancia dis = new Discrepancia ();
                dis.setNro_discrepancia (num_discre);
                dis.setNro_planilla (numpla);
                dis.setNro_remesa (numrem);
                dis.setTipo_doc_rel (tipo_doc_rel);
                dis.setDocumento_rel (documento_rel);
                dis.setTipo_doc (tipo_doc);
                dis.setDocumento (documento);
                dis.setUsuario_modificacion (usuario.getLogin ().toUpperCase ());
                consultas.add ( model.discrepanciaService.anularDiscrepancia (dis) );
                consultas.add ( model.discrepanciaService.anularDiscrepanciaTodosProducto (dis) );
                model.despachoService.insertar (consultas);
                model.discrepanciaService.resetItemsObjetoDiscrepancia ();
            }
            else{
                model.discrepanciaService.setCabDiscrepancia (null);
                model.discrepanciaService.setItemsDiscrepancia ( null );
            }
            request.setAttribute ("msg","Discrepancia Anulada");
            model.tablaGenService.buscarGrupo (clientes);
            next="/jsp/cumplidos/discrepancia/DiscrepanciaInsertar.jsp?c_numrem="+numrem+"&sw=true&exis=false&campos=true&c_numpla"+numpla+"&c_tipo_doc="+tipo_doc+"&c_documento="+documento+"&paso=false&nom_doc="+nom_doc+"&c_tipo_doc_rel="+tipo_doc_rel+"&c_documento_rel="+documento_rel+"&client="+clientes;
        }catch (SQLException e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
}
