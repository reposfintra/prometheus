/********************************************************************
 *  Nombre Clase.................   PrecintosConsultarAction.java
 *  Descripci�n..................   Action de la tabla precintos
 *  Autor........................   Ing. Andr�s Maturana De La Cruz
 *  Fecha........................   22.12.2005
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
/**
 *
 * @author  Leonardo Parody Ponce
 */
public class PrecintosConsultarAction extends Action{
    
    /** Creates a new instance of PrecintosDeleteAction */
    public PrecintosConsultarAction() {
    }
    public void run() throws ServletException, InformationException {
        String next = "/jsp/trafico/precinto/PrecintosListar.jsp";
        String agc = request.getParameter("agencia");
        String tdoc = request.getParameter("tipo_documento");
        String inicio = request.getParameter("inicio");
        String fin = request.getParameter("fin");
        
        try{
            model.precintosSvc.consultar(agc, tdoc);
            Vector info = model.precintosSvc.getVector();
            Vector lotes = new Vector();
            for( int i=0; i<info.size(); i++){
                Precinto pre = (Precinto) info.elementAt(i);
                String[] serie = pre.getSerie().replaceAll(" ","").split(",");
                String seriei = serie[0];
                String serief = serie[serie.length-1];
                pre.setSeriei(seriei);
                pre.setSerief(serief);
                if( pre.getSeriei().compareTo(pre.getSerief())==0 ){
                    pre.setSerie(pre.getSeriei());
                } else {
                    pre.setSerie(pre.getSeriei() + " - " + pre.getSerief());
                }
                lotes.add(pre);
                /*////System.out.println("......... AGC: " + pre.getAgencia() + " - Usuario: " + pre.getCreation_user() + "" +
                        " - Tipo: " + pre.getTipoDocumento() + " - Fecha: " + pre.getCreation_date() + " - SERIE: " + seriei + " - " + serief);*/
                
            }
            model.precintosSvc.setVector(lotes);
        }catch (SQLException e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
    }
    
}
