/***********************************************
 * Nombre clase: DiscrepanciaCierreAction.java
 * Descripci�n: Accion para cerrar los productos de las discrepancia y si los productos estan
 *              cerrados se cierra la discrepancia
 * Autor: Jose de la rosa
 * Fecha: 4 de noviembre de 2005, 02:49 PM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 **********************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;


public class DiscrepanciaCierreAction extends Action{
    
    /** Creates a new instance of DiscrepanciaCierreAction */
    public DiscrepanciaCierreAction () {
    }
    
    public void run () throws ServletException, com.tsp.exceptions.InformationException {
        String next="";
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String distrito = (String) session.getAttribute ("Distrito");
        Date hoy = new Date ();
        SimpleDateFormat s = new SimpleDateFormat ("yyyy-MM-dd hh:mm:ss");
        String fecha_hoy = s.format (hoy);
            
        int num_discre = Integer.parseInt (request.getParameter ("c_num_discre"));
        String numpla = request.getParameter ("c_numpla").toUpperCase ();
        String numrem = request.getParameter ("c_numrem").toUpperCase ();
        String tipo_doc = request.getParameter ("c_tipo_doc").toUpperCase ();
        String documento = request.getParameter ("c_documento").toUpperCase ();
        String tipo_doc_rel = request.getParameter ("c_tipo_doc_rel").toUpperCase ();
        String documento_rel = request.getParameter ("c_documento_rel").toUpperCase ();        
        
        String nota_debito = request.getParameter ("c_nota_debito");
        String usuario_debito = (request.getParameter ("c_nota_debito")!=null)?usuario.getLogin ():"";
        String fecha_debito = (request.getParameter ("c_nota_debito")!=null)?fecha_hoy:"0099-01-01 00:00:00";
        
        String nota_credito = request.getParameter ("c_nota_credito");
        String usuario_credito = (!request.getParameter ("c_nota_credito").equals (""))?usuario.getLogin ():"";
        String fecha_credito = (!request.getParameter ("c_nota_credito").equals (""))?fecha_hoy:"0099-01-01 00:00:00";
        
        String referencia = (!request.getParameter ("c_referencia").equals (""))?request.getParameter ("c_referencia").toUpperCase ():"";
        String cant = (!request.getParameter ("c_cantidad").equals (""))?request.getParameter ("c_cantidad"):"0";
        double cantidad = Double.parseDouble (cant);
        String observacion = request.getParameter ("c_observacion"); 
        
        try{
            Discrepancia dis = new Discrepancia();
            dis.setNro_discrepancia (num_discre);
            dis.setNro_planilla (numpla);
            dis.setNro_remesa (numrem);
            dis.setTipo_doc (tipo_doc);
            dis.setDocumento (documento);
            dis.setTipo_doc_rel (tipo_doc_rel);
            dis.setDocumento_rel (documento_rel); 
            
            dis.setObservacion_cierre (observacion);
            
            dis.setUsuario_cierre (usuario.getLogin ());
            dis.setFecha_cierre (fecha_hoy);
                        
            dis.setCantidad_autorizada (cantidad);
            dis.setReferencia (referencia);
            
            dis.setNota_debito (nota_debito);
            dis.setUsuario_nota_debito (usuario_debito);
            dis.setFecha_nota_debito (fecha_debito);
            
            dis.setNota_credito (nota_credito);
            dis.setUsuario_nota_credito (usuario_credito); 
            dis.setFecha_nota_credito (fecha_credito);
            model.discrepanciaService.insertarCierreDiscrepanciaDocumento (dis);
          
            request.setAttribute ("msgDiscrepancia", "Discrepancia Cerrada Satisfactoriamente");
            next="/jsp/cumplidos/discrepancia/discrepanciaConsultaCierre.jsp?c_num_discre="+num_discre+"&c_documento="+documento+"&c_tipo_doc="+tipo_doc+"&c_numrem="+numrem+"&sw=true&campos=false&c_numpla="+numpla+"&exis=false&campos=false&paso=false&c_tipo_doc_rel="+tipo_doc_rel+"&c_documento_rel="+documento_rel;
            model.planillaService.obtenerInformacionPlanilla2 (numpla);
            model.planillaService.obtenerNombrePropietario2 (numpla);
            model.planillaService.consultaPlanillaRemesaDiscrepancia(numpla, numrem, tipo_doc, documento, num_discre );
            model.discrepanciaService.searchDiscrepancia(numpla, numrem, tipo_doc, documento, tipo_doc_rel, documento_rel,distrito );
            model.discrepanciaService.listDiscrepanciaProducto(num_discre ,numpla,numrem, tipo_doc,documento,tipo_doc_rel,documento_rel,distrito);
            Discrepancia d = model.discrepanciaService.getDiscrepancia ();
            model.ubService.buscarUbicacion (d.getUbicacion (), usuario.getCia ());
        }catch (SQLException e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
    
}
