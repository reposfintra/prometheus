/***********************************************************************************
 * Nombre clase : ............... RepInfoOtMimPosManageAction.java                                   *
 * Descripcion :................. clase que maneja las acciones                               *
 * Autor :....................... Ing. Ivan Gomez                                  *
 * Fecha :....................... 22 de octubre de 2005, 11:02 AM                   *
 * Version :..................... 1.0                                              *
 * Copyright :................... Fintravalores S.A.                          *
 ***********************************************************************************/

package com.tsp.operation.controller;

import org.apache.log4j.Logger;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;


/**
 *
 * @author amendez
 */
public class RepInfoOtMimPosManageAction extends Action{

    static Logger logger = Logger.getLogger(RepInfoOtMimPosManageAction.class);

    /** Creates a new instance of Accion */
    public RepInfoOtMimPosManageAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "";
        String cmd = request.getParameter("cmd");
        String fecini = request.getParameter("fecini");
        String fecfin = request.getParameter("fecfin");
        String contentType = request.getParameter("tipoPage");
        if(cmd == null)
            next = "/jsp/general/reporte/PantallaFiltroRepInfoOtMimVsPg.jsp";
        else{
             HttpSession session = request.getSession();
             Usuario usuario = (Usuario)session.getAttribute("Usuario");
             if (contentType.equals("HTML") || contentType.equals("EXCEL")){
                 try{
                     model.repInfoOtMimPosSvc.genRepInfoOtMimVsPos(fecini, fecfin);
                 }
                 catch(SQLException e){
                     throw new ServletException(e.getMessage());
                 }
                 logger.info(usuario.getNombre() + "EJECUTO REPORTE DE INFORMACION DE OTS MIM VS POSTGRES");
                 next = "/jsp/general/reporte/ReporteInfoOtMimVsPg.jsp?tipoPage="+contentType;
             }
             else if(contentType.equals("file")){
                  next = "/jsp/general/reporte/ReporteInfoOtMimVsPg.jsp?op=file";
             }
        }
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
    }
}