/************************************************************************************
 * Nombre clase : ............... EscoltaProveedorSearchAction.java                 *
 * Descripcion :................. Clase que maneja los eventos relacionados con la  *
 *                                busqueda de proveedores de escolta                * 
 * Autor :....................... Ing. Henry A.Osorio Gonz�lez                      *
 * Fecha :....................... 28 de Noviembre de 2005, 11:30 AM                 *
 * Version :..................... 1.0                                               *
 * Copyright :................... Fintravalores S.A.                           *
 ***********************************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;

public class EscoltaProveedorSearchAction extends Action{
    
    public void run() throws ServletException {
        String next = "/jsp/masivo/proveedor_escolta/proveedorSearch.jsp?msg=";        
        String nit = request.getParameter("nit");
        String origen = request.getParameter("origen");        
        try{ 
            model.proveedorEscoltaService.buscarProveedorEscoltas(nit,origen);            
            Vector datos = model.proveedorEscoltaService.getVectorProveedores();
            if (datos.size()==0) {
                next+="No se encontro ningun proveedor";
            }
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }        
        this.dispatchRequest(next);
    }
    
}
