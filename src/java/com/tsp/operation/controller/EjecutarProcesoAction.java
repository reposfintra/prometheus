/******************************************************************
* Nombre                        EjecutarProcesoAction.java
* Descripci�n                   Clase Action para la tabla est_carga_placa
* Autor                         ricardo rosero
* Fecha                         18/01/2006
* Versi�n                       1.0
* Coyright                      Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  rrosero
 */
public class EjecutarProcesoAction extends Action {
    
    /** Creates a new instance of EjecutarProcesoAction */
    public EjecutarProcesoAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String pag = "/jsp/masivo/estadisticas/ejecutarProceso.jsp";
        
        SimpleDateFormat s = new SimpleDateFormat("yyyyMMdd");
        Calendar FechaHoy = Calendar.getInstance();
        Date d = FechaHoy.getTime();
        String fecha = s.format(d);

        HttpSession session = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        String user = usuario.getLogin();

        String fechai = request.getParameter("fechai");
        String fechaf = request.getParameter("fechaf");

        try{
            //System.out.println("Voy a ejecutar el service");
            model.ecpService.eliminarEstCargaPlaca();
            model.ecpService.insertar();
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }

        pag += "?Mensaje=Proceso ejecutado satisfactoriamente!!";
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(pag);

    }
    
}