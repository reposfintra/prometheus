/***************************************************************************
 * Nombre clase : ............... FlotaManagerAction.java                  *
 * Descripcion :................. Clase que maneja los eventos             *
 *                                relacionados con el programa de          *
 *                                Reporte de egreso y Modificacion         *
 *                                de cheques                               *
 * Autor :....................... Ing. Juan Manuel Escandon Perez          *
 * Fecha :........................ 6 de diciembre de 2005, 08:56 AM        *
 * Version :...................... 1.0                                     *
 * Copyright :.................... Fintravalores S.A.                 *
 ***************************************************************************/
package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
/**
 *
 * @author  JuanM
 */
public class FlotaManagerAction extends Action{
    private static String pla;
    private static String std;
    /** Creates a new instance of FlotaManagerAction */
    public FlotaManagerAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        try{
            
            /*
            *Declaracion de variables
            */
            String Opcion                 = (request.getParameter("Opcion")!=null)?request.getParameter("Opcion"):"";
            String std_job                = (request.getParameter("standard")!=null)?request.getParameter("standard"):"";
            String placa                  = (request.getParameter("placa")!=null)?request.getParameter("placa").toUpperCase ():"";
            
            HttpSession Session           = request.getSession();
            Usuario user                  = new Usuario();
            user                          = (Usuario)Session.getAttribute("Usuario");
            String usuario                = user.getLogin();
            String dstrct                 = user.getDstrct();
            String base                   = user.getBase();
            
            String []LOV                  = request.getParameterValues("LOV");
            
            std_job                       = (std_job.equals("")) ? "%" : std_job;
            placa                         = (placa.equals("")) ? "%"  : placa;
            
            String next = "";
            String Mensaje = "";
            
            /*
            *Manejo de eventos
            */
            if (Opcion.equals("Guardar")){
                if(model.placaService.placaExist(placa)){
                    if(!model.FlotaSvc.Buscar(std_job,placa)){
                        model.FlotaSvc.Insert(dstrct, base, std_job, placa, usuario);
                        Mensaje = "El Registro ha sido agregado";
                    }
                    else
                        Mensaje = "El Registro ya existe";
                }
                else
                    Mensaje = "El Vehiculo no existe ";
            }
            
            if (Opcion.equals("Anular")){
                for(int i=0;i<LOV.length;i++){
                    this.Conversion(LOV[i]);
                    model.FlotaSvc.UpdateEstado("A", std, pla, usuario);
                }
                Mensaje = "El Registro ha sido anulado";
            }
            
            if (Opcion.equals("Activar")){
                for(int i=0;i<LOV.length;i++){
                    this.Conversion(LOV[i]);
                    model.FlotaSvc.UpdateEstado("", std, pla, usuario);
                }
                Mensaje = "El Registro ha sido activado";
            }
            
            if (Opcion.equals("Eliminar")){
                for(int i=0;i<LOV.length;i++){
                    this.Conversion(LOV[i]);
                    model.FlotaSvc.Delete(std, pla);
                }
                Mensaje = "El Registro ha sido eliminado";
            }
            if (Opcion.equals("Listado")){
                Session.setAttribute("valor", placa+"/"+std_job);
                model.FlotaSvc.List(placa, std_job);
                List list = model.FlotaSvc.getList();
                if( list.size() == 0 || list == null )
                    Mensaje = "Su busqueda no arrojo resultados!";
            }
            
            if ("Anular|Activar|Eliminar".indexOf(Opcion) != -1) {
                 String valor = (String)Session.getAttribute("valor");
                 model.FlotaSvc.List(valor.split("/")[0], valor.split("/")[1]);
                 next ="/jsp/equipos/flota/ListaFlota.jsp?Mensaje="+Mensaje;
            }
            
            if ("Listado".indexOf(Opcion) != -1) {
                //System.out.println("Opcion " + Opcion);
                model.FlotaSvc.List(placa, std_job);
                next ="/jsp/equipos/flota/ListaFlota.jsp?Mensaje="+Mensaje;
                
            }
            
            if ("Guardar".indexOf(Opcion) != -1) {
                //System.out.println("Opcion " + Opcion);
                next ="/jsp/equipos/flota/Flota.jsp?Mensaje="+Mensaje;
            }
            
            TreeMap t = new TreeMap();
            model.stdjobdetselService.setCiudadesDest(t);
            model.stdjobdetselService.setCiudadesOri(t);
            model.stdjobdetselService.setStdjobTree(t);
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en FlotaManagerAction .....\n"+e.getMessage());
        }
        
    }
    
    public static void Conversion(String f){
        String vF [] = f.split("/");
        pla = vF[0];
        std = vF[1];
    }
    
}
