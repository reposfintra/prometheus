/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.threads.*;
import com.tsp.util.Util;
import com.tsp.operation.model.beans.Usuario;
import javax.servlet.http.*;
import java.sql.SQLException;
import com.tsp.operation.model.*;



/**
 *
 * @author Alvaro
 */
public class PrefacturaRegistrarAction  extends Action {


   //protected HttpServletRequest request;
    public PrefacturaRegistrarAction () {
    }


    public void run() throws ServletException, InformationException {


       System.out.println("Iniciando el registro de la prefactura") ;

       HttpSession session  = request.getSession();
       Usuario usuario      = (Usuario)session.getAttribute("Usuario");
       String evento        = request.getParameter ("evento");
       String[] id_accion   = request.getParameterValues("ckestado");

       String login         = usuario.getLogin();
       String msj           = "";
       String next          = "";

       try{
            model.applusService.buscaContratista(usuario.getNitPropietario());//090509
       }catch(Exception e){
           System.out.println("error buscando contratistaaaa");
       }
       
       // MANEJO DE EVENTO MARCAR_PREFACTURADO EN prefacturasMarcar.Jsp para marcar prefacturadas las seleccionadas
       if(evento.equals("MARCAR_PREFACTURADO")) {

           if( id_accion!= null){

                try {
                    Contratista contratista = model.applusService.getContratista();
                    List listaPrefactura    = model.applusService.getPrefactura();

                    // CALCULO DE GENERACION DE PREFACTURA A ASIGNAR

                    int secuencia_prefactura = contratista.getSecuencia_prefactura() + 1;
                    String proxima_prefactura = contratista.getId_contratista() + '-' + Util.llenarConCerosALaIzquierda( secuencia_prefactura, 6 );
                    // Actualiza la secuencia de prefactura
                    model.applusService.setSecuenciaPrefactura(contratista.getId_contratista());


                    // LOCALIZACION DEL PORCENTAJE IVA

                    double porcentajeIva    = model.applusService.getPorcentaje("IVA");
                    String codigoIva        = model.applusService.getCodigoImpuesto("IVA");

                    double porcentajeRetMat = model.applusService.getPorcentaje("RMAT");
                    String codigoRetMat     = model.applusService.getCodigoImpuesto("RMAT");

                    double porcentajeRetMao = model.applusService.getPorcentaje("RMAO");
                    String codigoRetMao     = model.applusService.getCodigoImpuesto("RMAO");

                    double porcentajeRetOtr = model.applusService.getPorcentaje("ROTR");
                    String codigoRetOtr     = model.applusService.getCodigoImpuesto("ROTR");

                    // PROCESAR CADA ACCION SELECCIONADA
                    for(int i=0;i<id_accion.length;i++){

                        //int accion = Integer.parseInt(id_accion[i]);
                        String accion = id_accion[i];
                        double comision_ejecutiva = 0;
                        double comision_canal     = 0;
                        double iva                = 0;
                        double valorRetMat        = 0;
                        double valorRetMao        = 0;
                        double valorRetOtr        = 0;

                        Iterator it = listaPrefactura.iterator();
                        boolean localizado = false;

                        // Localizar valores a prefacturar de la accion seleccionada en la lista de acciones del contratista
                        while (it.hasNext() && (localizado == false) ) {

                            Prefactura prefactura = (Prefactura)it.next();
                            //int accion_no =  Integer.parseInt(prefactura.getId_accion()) ;
                            String accion_no =  prefactura.getId_accion() ;
                            if (accion_no.equals(accion)){
                                // Id_Accion de la lista concuerda con la selecciona en pantalla
                                double total_prev1_mat = prefactura.getTotal_prev1_mat();
                                double total_prev1_mob = prefactura.getTotal_prev1_mob();
                                double total_prev1_otr = prefactura.getTotal_prev1_otr();
                                double administracion  = prefactura.getAdministracion();
                                double imprevisto      = prefactura.getImprevisto();
                                double utilidad        = prefactura.getUtilidad();


                                double total_prev1     = total_prev1_mat + total_prev1_mob + total_prev1_otr +
                                                         administracion + imprevisto + utilidad;

                                comision_ejecutiva    = -prefactura.getComision_ejecutiva();
                                comision_canal        = -prefactura.getComision_canal();
                                double subtotal       =  total_prev1 + comision_ejecutiva + comision_canal ;
                                double base_iva       =  prefactura.getBase_iva();

                                iva                   =  Util.redondear2(base_iva*porcentajeIva/100, 2);
                                valorRetMat           = -Util.redondear2(total_prev1_mat*porcentajeRetMat/100, 2);
                                valorRetMao           = -Util.redondear2(total_prev1_mob*porcentajeRetMao/100, 2);
                                valorRetOtr           = -Util.redondear2(total_prev1_otr*porcentajeRetOtr/100, 2);
                                double subtotal1=subtotal+iva;
                                double porcentajeFactoring = prefactura.getPorcentaje_factoring();
                                //if (prefactura.getId_contratista().equals("CC008") || prefactura.getId_contratista().equals("CC036") || prefactura.getId_contratista().equals("CC017")){
                                if ((prefactura.getId_contratista().equals("CC008") || prefactura.getId_contratista().equals("CC036") || prefactura.getId_contratista().equals("CC017")) && prefactura.getCuotas_reales()==1){//091105
                                    porcentajeFactoring =0;
                                }
                                double porcentajeFormula   = prefactura.getPorcentaje_formula();

                                double valorFactoring      = -Util.redondear2(subtotal1*porcentajeFactoring/100, 2);
                                double valorFormula        = -Util.redondear2(subtotal1*porcentajeFormula/100, 2);

                                //inicio090511
                                double valorFormulaProvintegral=0;
                                try{
                               
                                //se va a agregar una columna en la prefactura llamada valor_formula_provintegral en caso que sea modelo anterior
                                
                                if (model.negociosApplusService.getEsquemaComision( prefactura.getId_orden()).equals("MODELO_ANTERIOR")){
                                    valorFormula=valorFormula/2.00;
                                    valorFormulaProvintegral=valorFormula;
                                }
                                }catch(Exception eee){
                                    System.out.println("eeeeeeeaccion prefac registra"+eee.toString()+"__"+eee.getMessage());
                                }
                                //fin090511
                                
                                localizado = true;

                                // Actualiza las acciones prefacturadas
                                model.applusService.setAccion (accion, proxima_prefactura,comision_ejecutiva, comision_canal,
                                                               codigoIva,iva,login,codigoRetMat,valorRetMat,codigoRetMao,
                                                               valorRetMao,codigoRetOtr,valorRetOtr,
                                                               porcentajeIva,porcentajeRetMat,porcentajeRetMao,porcentajeRetOtr,valorFactoring,
                                                               porcentajeFormula,valorFormula,base_iva,valorFormulaProvintegral);
                            }
                        }
                    }

                    // Refresca la lista de prefacturas
                    model.applusService.buscaPrefactura(usuario.getNitPropietario());
                    listaPrefactura = model.applusService.getPrefactura();
                    if(listaPrefactura.size()!=0) {
                        model.applusService.buscaContratista(usuario.getNitPropietario());
                        contratista = model.applusService.getContratista();
                        secuencia_prefactura = contratista.getSecuencia_prefactura() + 1;
                        proxima_prefactura = contratista.getId_contratista() + '-' + Util.llenarConCerosALaIzquierda( secuencia_prefactura, 6 );
                        next = "/jsp/applus/prefactura/prefacturasMarcar.jsp";

                    }
                    else {
                        msj   = "El contratista no tiene registros disponibles...";
                        next  = "/jsp/applus/prefactura/accesoPrefactura.jsp?msj=";
                    }
                    next += msj ;
                    this.dispatchRequest(next);

                }
                catch (SQLException e){

                    throw new ServletException(e.getMessage());
                }
           }


       }



    }
}

