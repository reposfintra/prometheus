/********************************************************************
 *      Nombre Clase.................   Tipo_impuestoListar Action.java
 *      Descripci�n..................   Administrador de los eventos del programa Tipo de Impuesto
 *      Autor........................   Ing. Juan Manuel Escand�n
 *      Fecha........................   30 de septiembre de 2005, 09:35 AM
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/


package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
/**
 *
 * @author  JuanM
 */
public class Tipo_impuestoListarAction extends Action{
    private static String tipo;
    private static String fecvig;
    private static String cod;
    private static String age;
    private static String conc;
    /** Creates a new instance of Tipo_impuestoManagerAction */
    public Tipo_impuestoListarAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        String next = "/jsp/cxpagar/Tipo_impuestos/Tipo_impuestoListar.jsp";
        
        try{
            model.TimpuestoSvc.ReiniciarDato();
            model.ciudadService.ListaAgencias();
            
            
            String TipoI = (request.getParameter("TipoI")!=null)?request.getParameter("TipoI"):"";
            String fconcepto = (request.getParameter("fconcepto")!=null)?request.getParameter("fconcepto"):"";
            fconcepto = (fconcepto.equals(""))?"%":fconcepto;
            String filtroagencias = (request.getParameter("filtroagencias")!=null)?request.getParameter("filtroagencias"):"";
            String Mensaje = "";
            
            //////System.out.println("..................... LISTAR TAMATU: " + TipoI );
            
            model.TimpuestoSvc.LIST(TipoI,filtroagencias,fconcepto);
            List list = model.TimpuestoSvc.getList();
            if( list.size() == 0 || list == null )
                Mensaje = "No hay registros de ese tipo de impuesto";
            
            next += "?Mensaje="+Mensaje+"&impuesto="+TipoI;
            
            /*RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);*/
                                    
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en Tipo_impuestoManagerAction .....\n"+e.getMessage());
        }
        
        this.dispatchRequest(next);
    }  
   
}
