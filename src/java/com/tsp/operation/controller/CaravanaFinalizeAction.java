/**
 *
 */
package com.tsp.operation.controller;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;

/**
 * Searches the database for puchase orders matching the
 * purchase search argument
 */
public class CaravanaFinalizeAction extends Action
{
  static Logger logger = Logger.getLogger(CaravanaFinalizeAction.class);
  
  /**
   * Executes the action
   */
  public void run() throws ServletException, InformationException {
      String next = null;
   
      HttpSession session = request.getSession();
      Usuario usuario = (Usuario)session.getAttribute("Usuario");
      
      model.caravanaService.finCreacionCaravana();
      session.removeAttribute("planVj");
      session.removeAttribute("caravana");
      logger.info(usuario.getNombre() + " Fin Carvana con:");
    
      // Redireccionar a la p�gina indicada.
      next = "/controller?estado=PlanViaje&accion=QryAg";
      this.dispatchRequest(next);
  }
}
