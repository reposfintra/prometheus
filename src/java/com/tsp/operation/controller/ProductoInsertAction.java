/*
 * ProductoInsertAction.java
 *
 * Created on 17 de octubre de 2005, 11:33 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

/**
 *
 * @author  Jose
 */
public class ProductoInsertAction extends Action{
    
    /** Creates a new instance of ProductoInsertAction */
    public ProductoInsertAction () {
    }
    public void run () throws ServletException, InformationException {
        String next = "/jsp/cumplidos/producto/ProductoInsertar.jsp";
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String codigo = request.getParameter ("c_codigo").toUpperCase ();
        String descripcion = request.getParameter ("c_descripcion");
        String unidad = request.getParameter ("c_unidad");
        String cliente = request.getParameter ("client");
        String distrito = (String) session.getAttribute ("Distrito");
        String lupa = request.getParameter("lupa")!=null?next+="?lupa=ok&clien="+cliente:"";
        int sw=0;
        try{
            Producto producto = new Producto ();
            producto.setDescripcion (descripcion);
            producto.setCodigo (codigo);
            producto.setCliente (cliente);
            producto.setUnidad (unidad);
            producto.setUsuario_modificacion (usuario.getLogin ());
            producto.setUsuario_creacion (usuario.getLogin ());
            producto.setBase (usuario.getBase ());
            producto.setDistrito (distrito);
            if( model.clienteService.existeCliente (cliente) ){
                try{
                    model.productoService.insertProducto (producto);
                    request.setAttribute ("msg", "Producto Ingresado");
                }catch(SQLException e){
                    sw=1;
                }
                if(sw==1){
                    if(!model.productoService.existProducto (codigo, distrito, cliente)){
                        model.productoService.updateProducto (producto);
                        request.setAttribute ("msg", "Producto Ingresado");
                    }
                    else{
                        request.setAttribute ("msg", "Error El Codigo del Producto ya existe en la Base de Datos, utilice otro c�digo");
                    }
                }
            }
            else{
                request.setAttribute ("msg", "Error No existe el cliente");
            }
        }catch(SQLException e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
    
}
