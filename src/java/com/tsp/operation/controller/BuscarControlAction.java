/*******************************************************************
 * Nombre clase: ClienteBuscarAction.java
 * Descripci�n: Accion para buscar una placa de la bd.
 * Autor: Ing. fily fernandez
 * Fecha: 16 de febrero de 2006, 05:41 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import com.tsp.exceptions.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import java.util.Vector;
import java.lang.*;
import java.sql.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import com.tsp.operation.model.services.*;
/**
 *
 * @author  jdelarosa
 */
public class BuscarControlAction extends Action{
    
    /** Creates a new instance of PlacaBuscarAction */
    public BuscarControlAction () {
    }
    
    public void run () throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String cod = request.getParameter ("cod_cli")!=null?request.getParameter ("cod_cli"):"";
        String ori = request.getParameter ("ciudadOri")!=null?request.getParameter ("ciudadOri"):"";
        String des = request.getParameter ("ciudadDest")!=null?request.getParameter ("ciudadDest"):"";
        String age = (request.getParameter ("agencia")!=null)?request.getParameter ("agencia"):"";
        
        String codigo = cod.toUpperCase();
        String origen = ori.toUpperCase();
        String destino = des.toUpperCase();
        String agencia = age.toUpperCase();
        
       String next = "/jsp/trafico/planviaje/control_excepcion/VerReporteControl.jsp?codigo="+codigo+"&origen="+origen+"&destino="+destino+"&agencia="+agencia;
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario)session.getAttribute ("Usuario");
        String usu = usuario.getLogin();
        agencia = agencia.equals("NADA")?"":agencia;
        String opcion = request.getParameter ("opcion")!=null?request.getParameter ("opcion"):"";
        try {
              Vector datos = model.control_excepcionService.getVector();
              
           if ( opcion.equals("1") ){
               model.control_excepcionService.bucarControlExcepcion(codigo, origen, destino, agencia);
               datos = model.control_excepcionService.getVector();
               System.out.println("vector  "+ datos.size());
                if ( datos.size () <= 0 ) {    
                     next = next + "&msg=Su busqueda no arrojo resultados!";
                
                }  
           } 
            if ( opcion.equals("2") ) {
                //HReporteCliente cli = new HReporteCliente();
                //cli.start(Codigo, Nit, Nombre, agencia, usu);
               // next = next + "?msg=Defina un parametro de busqueda!";
            }
             
            
        } catch ( Exception e ) {
            
            throw new ServletException ( "Error en Reporte de BuscarControl_ExcepcionAction : " + e.getMessage() );
            
        }        
        
        this.dispatchRequest ( next );
        
    }
    
}
