/*
 * Nombre        UnidadUpdateAction.java
 * Autor         Ing Jose de la Rosa
 * Modificado    Ing Sandra Escalante
 * Fecha         26 de junio de 2005, 11:15 AM
 * Versión       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class UnidadUpdateAction extends Action{
    
    /** Creates a new instance of UnidadUpdateAction */
    public UnidadUpdateAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String next="/jsp/trafico/unidad/UnidadModificar.jsp?lista=ok&reload=ok";
        
        HttpSession session = request.getSession();        
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        String codigo = request.getParameter("c_codigo");
        String descripcion = request.getParameter("c_descripcion");
	String tipo = request.getParameter("c_tipo");
        String atipo = request.getParameter("c_atipo");
        
        try{
            
            Unidad unidad = new Unidad();
            unidad.setCodigo(codigo);
            unidad.setDescripcion(descripcion.toUpperCase());
            unidad.setUser_update(usuario.getLogin().toUpperCase());
            
            try{
                
                model.unidadTrfService.updateUnidad(codigo, descripcion.toUpperCase(), usuario.getLogin().toUpperCase(),tipo, usuario.getBase(),atipo);                                
                model.unidadTrfService.serchUnidad(codigo, tipo);
                request.setAttribute("mensaje","Modificación exitosa!");                
                
            }catch(SQLException e){
                
                e.printStackTrace();
                model.unidadTrfService.serchUnidad(codigo, atipo);
                request.setAttribute("mensaje","Unidad ya existe!");
                
            }
            
            Unidad unidad2 = model.unidadTrfService.getUnidad();
            request.setAttribute("unidad",unidad2);
            
        }catch (SQLException e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}
