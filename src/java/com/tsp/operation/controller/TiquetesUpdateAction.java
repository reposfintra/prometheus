/*
 * TiquetesUpdateAction.java
 *
 * Created on 3 de diciembre de 2004, 10:11 AM
 */

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  KREALES
 */
public class TiquetesUpdateAction extends Action{
    
    /** Creates a new instance of TiquetesUpdateAction */
    public TiquetesUpdateAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/tiquetes/tiketUpdate.jsp?mensaje=ok";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String nit = request.getParameter("nit");
        String sucursal = request.getParameter("sucursal");
        try{
            model.proveedortiquetesService.buscaProveedor(nit,sucursal);
            if(model.proveedortiquetesService.getProveedor()!=null){
                Proveedor_Tiquetes pa= model.proveedortiquetesService.getProveedor();
                pa.setCity_code(request.getParameter("ciudad"));
                pa.setDstrct(request.getParameter("distrito"));
                pa.setCreation_user(usuario.getLogin());
                model.proveedortiquetesService.updateProveedor(pa);
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
