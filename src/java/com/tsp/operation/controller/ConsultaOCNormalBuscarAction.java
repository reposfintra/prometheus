package com.tsp.operation.controller;
/*  @author  fvillacob */

import java.io.*;
import java.util.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.Util;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.exceptions.*;

public class ConsultaOCNormalBuscarAction extends Action{
    
    public void run() throws ServletException, InformationException {
        
        try {
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String login=usuario.getLogin();
            int tipo = Integer.parseInt(request.getParameter("tipo"));
            String next="";
            /*Modificado el 2006-04-11 por nueva accion para consulta de OC*/
            
            String distrito = request.getParameter("distrito")!=null ? request.getParameter("distrito"):"FINV";
            String planilla = request.getParameter("planilla")!=null ? request.getParameter("planilla"):"";
            String oc=request.getParameter("numeroOC")!=null         ? request.getParameter("numeroOC").toUpperCase():"";
            String ot = request.getParameter("numeroOT")!=null       ? request.getParameter("numeroOT").toUpperCase():"";
            
            /*comentario=model.PlanillasSvc.searchOC(distrito,oc,usuario.getBase(),ot);
            Planillas p = model.PlanillasSvc.getPlanilla();
            if(p!=null){
                model.remesaService.buscaRemesa(p.getRemesa());
                //System.out.println("Buscar la remesa "+p.getRemesa());
            }
            if(model.remesaService.getRemesa()==null){
                comentario= "No se encontro la remesa";
            }*/
            /*Reedireccionando a la nueva Accion.*/
            next= "/controller?estado=Consultar&accion=PlanillaRemesa&planilla="+planilla+"&distrito="+distrito+"&numeroOC="+oc+"&numeroOT="+ot;          
          
            // LLamamos la pagina JSP
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new Exception("No se pudo encontrar "+ next);
            rd.forward(request, response);
      
    }
    catch(Exception e){
        e.printStackTrace();
        throw new ServletException(e.getMessage());
    }
    
}// end run

}// en accion
