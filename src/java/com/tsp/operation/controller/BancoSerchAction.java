/*******************************************************************
 * Nombre clase: BancoSerchAction.java
 * Descripci�n: Accion para buscar y mostrar un listado o la
 *              pag de modificar de los banco.
 * Autor: Ing. Jose de la rosa
 * Fecha: 28 de septiembre de 2005, 02:18 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;

/**
 *
 * @author  Jose
 */
public class BancoSerchAction extends Action {
    
    /** Creates a new instance of BancoSerchAction */
    public BancoSerchAction () {
    }
    
    public void run () throws ServletException, com.tsp.exceptions.InformationException {
        String next="";
        HttpSession session = request.getSession ();
        String distrito = "";
        String banco = "";
        String sucursal = "";
        String cuenta = "";
        String listar = (String) request.getParameter ("listar");
        String codigo_cuenta = "";
        String agencia = "";
        String posbancaria = "";
        try {
            if (listar.equals ("True")) {
                next="/jsp/cxpagar/banco/BancoListar.jsp";
                //next = Util.LLamarVentana (next, "Listar Banco");
                String sw = (String) request.getParameter ("sw");
                if(sw.equals ("True")) {
                    distrito = request.getParameter ("c_distrito").toUpperCase ();
                    banco = (request.getParameter ("c_banco").toUpperCase ());
                    sucursal = (request.getParameter ("c_sucursal").toUpperCase ());
                    cuenta = (request.getParameter ("c_cuenta").toUpperCase ());
                    codigo_cuenta = (request.getParameter ("c_codigo_cuenta")!=null)?request.getParameter ("c_codigo_cuenta").toUpperCase ():"";
                    agencia = (request.getParameter ("c_agencia")!=null)?request.getParameter ("c_agencia").toUpperCase ():"";
                    posbancaria = (request.getParameter ("posbancaria")!=null)?request.getParameter ("posbancaria").toUpperCase ():""; 
                } else {
                    distrito = "";
                    banco = "";
                    sucursal = "";
                    cuenta = "";
                    listar = "";
                    codigo_cuenta = "";
                    agencia = "";
                    posbancaria = "";
                }
                /*session.setAttribute ("codigo_cuenta_ban", codigo_cuenta);
                session.setAttribute ("agencia_ban", agencia);
                session.setAttribute ("distrito_ban", distrito);
                session.setAttribute ("banco_ban", banco);
                session.setAttribute ("sucursal_ban", sucursal);
                session.setAttribute ("cuenta_ban", cuenta);
                session.setAttribute ("posbancaria", posbancaria);*/
                
                model.servicioBanco.searchDetalleBanco(distrito, banco, sucursal, cuenta, agencia, codigo_cuenta, posbancaria);
            }
            else {
                distrito = request.getParameter ("c_distrito").toUpperCase ();
                banco = (request.getParameter ("c_banco").toUpperCase ());
                sucursal = (request.getParameter ("c_sucursal").toUpperCase ());
                cuenta = (request.getParameter ("c_cuenta").toUpperCase ());
                model.servicioBanco.searchBanco (distrito, banco, sucursal, cuenta);
                Banco banc = model.servicioBanco.getBannco ();
                next="/jsp/cxpagar/banco/BancoModificar.jsp?msg=";
                //next = Util.LLamarVentana (next, "Modificar Banco");
                model.agenciaService.cargarAgencias();
                model.monedaSvc.cargarMonedas();
            }
        }catch (SQLException e) {
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
        
    }
    
}
