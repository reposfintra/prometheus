
/**
* Autor  : Ing. Roberto Rocha P..
* Date  : 10 de Julio de 2007
* Copyrigth Notice : Fintravalores S.A. S.A
* Version 1.0
-->
<%--
-@(#)
--Descripcion : Action que maneja los avales de Fenalco
**/
package com.tsp.operation.controller;

import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.beans.Usuario;
import javax.servlet.http.*;
import com.tsp.operation.model.threads.*;


public class ClientesVerAction extends Action{

    public ClientesVerAction(){
    }

    public void run(){
        String next="/jsp/delectricaribe/datos_electricaribe.jsp";
        HttpSession session  = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        String op=request.getParameter ("opcion");
        ClientesVerService clsrv=new ClientesVerService(usuario.getBd());
        if(op.equals("creacliente")||op.equals("modcliente")){
            ClienteEca cle=new ClienteEca();
            String situacion=(request.getParameter("detalle_inconsistencia")!=null)?request.getParameter("detalle_inconsistencia"):"";
            cle.setId_cliente(request.getParameter("cliente"));
            cle.setCiudad((request.getParameter("ciudad")!=null && !(request.getParameter("ciudad").equals("")))?request.getParameter("ciudad"):request.getParameter("ciudadd"));
            cle.setNombre_contacto(request.getParameter("nombre_contacto"));
            cle.setCelular_representante(request.getParameter("celular_representante"));
            cle.setDepartamento((request.getParameter("zona")!=null && !(request.getParameter("zona").equals("")))?request.getParameter("zona"):request.getParameter("zonaa"));
            cle.setDireccion(request.getParameter("direccion"));
            cle.setNit(request.getParameter("nit"));
            cle.setCargo_contacto(request.getParameter("cargo_contacto"));
            cle.setNombre(request.getParameter("nombre"));
            cle.setNombre_representante(request.getParameter("nombre_representante"));
            cle.setSector((request.getParameter("sector")!=null && !(request.getParameter("sector").equals("")))?request.getParameter("sector"):request.getParameter("sectorr"));
            cle.setTel1(request.getParameter("telefono_contacto"));
            cle.setTel2(request.getParameter("celular_contacto"));
            cle.setTel_representante(request.getParameter("telefono_representante"));
            cle.setTipo((request.getParameter("tipo_identificacion")!=null &&  !(request.getParameter("tipo_identificacion").equals("")))?request.getParameter("tipo_identificacion"):request.getParameter("tipo_identificacionn"));
            cle.setId_ejecutivo((request.getParameter("ejecutivo_cta")!=null &&  !(request.getParameter("ejecutivo_cta").equals("")))?(request.getParameter("ejecutivo_cta").split(","))[0]:request.getParameter("ejecutivo_ctaa"));

            boolean sw=true;
            if(request.getParameter("ejecutivo_cta").equals("")){
                sw=false;
            }

            cle.setNics(request.getParameterValues("nicc"));

            /* Este codigo aca es para poner la parte
             * de la validacion para los padres de
             * los clientes
             */
            if(request.getParameter("padre").equals("")){
                cle.setId_padre(cle.getId_cliente());
            }
            else{
                cle.setId_padre(request.getParameter("padre"));
            }

            /* Esta parte del codigo aca es para ver
             * el cliente es un cliente oficial
             */
            if(request.getParameter("cliespec").equals("true")){
                cle.setOficial(true);
            }
            else{
                cle.setOficial(false);
            }

            if(op.equals("creacliente")){
                try{
                    String resp=clsrv.insertarCl(cle,usuario.getLogin());
                    clsrv.buscarCl(cle);
                    next=next+"?mensaje="+resp+"&ciudadd="+cle.getCiudad()+"&nombre_contacto="+cle.getNombre_contacto()
                            +"&celular_representante="+cle.getCelular_representante()+"&zonaa="+cle.getDepartamento()
                            +"&direccion="+cle.getDireccion()
                            +"&nit="+cle.getNit()+"&cargo_contacto="+cle.getCargo_contacto()
                            +"&nombre="+cle.getNombre()+"&nombre_representante="+cle.getNombre_representante()
                            +"&sectorr="+cle.getSector()+"&telefono_contacto="+cle.getTel1()+"&celular_contacto="+cle.getTel2()+"&creation_user="+cle.getCreation_user()
                            +"&telefono_representante="+cle.getTel_representante()+"&tipo_identificacionn="+cle.getTipo()
                            +"&ejecutivo_ctaa="+cle.getId_ejecutivo()+"&detalle_inconsistenciaa="+situacion+"&login_ejecutivo="+clsrv.getLoginEjecutivo(cle.getId_cliente())
                            +"&padre="+cle.getNombre_padre()+"&cliespec="+cle.isOficial();
                    if(!cle.getId_cliente().equals("")){
                        next=next+"&idusuario="+cle.getId_cliente();
                    }
                    session.setAttribute("nics", cle.getNics());
                    System.out.println(next);
                }
                catch(Exception e){
                    e.printStackTrace();
                    System.out.println("Error:"+e.toString());
                }
            }

            if(op.equals("modcliente"))
            {   OfertaElca ofeca=new OfertaElca();
                ofeca.setId_solicitud(request.getParameter("idsolicitud"));
                AccionesEca acceca=new AccionesEca();
                try{
                    String resp=clsrv.updateCl(cle,usuario.getLogin(),sw);
                    clsrv.buscarCl(cle);
                    if(ofeca.getId_solicitud()!=null && !ofeca.getId_solicitud().equals(""))
                    {   clsrv.buscarOf(ofeca);
                        acceca.setId_solicitud(ofeca.getId_solicitud());
                    }
                    next=next+"?mensaje="+resp+"&ciudadd="+cle.getCiudad()+"&nombre_contacto="+cle.getNombre_contacto()
                            +"&celular_representante="+cle.getCelular_representante()+"&zonaa="+cle.getDepartamento()
                            +"&direccion="+cle.getDireccion()
                            +"&nit="+cle.getNit()+"&cargo_contacto="+cle.getCargo_contacto()
                            +"&nombre="+cle.getNombre()+"&nombre_representante="+cle.getNombre_representante()
                            +"&sectorr="+cle.getSector()+"&telefono_contacto="+cle.getTel1()+"&celular_contacto="+cle.getTel2()
                            +"&telefono_representante="+cle.getTel_representante()+"&tipo_identificacionn="+cle.getTipo()+"&creation_user="+cle.getCreation_user()
                            +"&ejecutivo_ctaa="+cle.getId_ejecutivo()+"&detalle_inconsistenciaa="+situacion+"&login_ejecutivo="+clsrv.getLoginEjecutivo(cle.getId_cliente())
                            +"&padre="+((cle.getId_padre().equals(cle.getId_cliente()))?"":cle.getNombre_padre())+"&cliespec="+cle.isOficial();
                    if(!cle.getId_cliente().equals(""))
                    {   next=next+"&idusuario="+cle.getId_cliente();
                    }
                    if(ofeca.getId_solicitud()!=null && !ofeca.getId_solicitud().equals(""))
                    {   next=next+"&idsolicitud="+ofeca.getId_solicitud()+"&historico="+ofeca.getDescripcion()
                                +"&fechainicio="+ofeca.getCreation_date().substring(0,11)+"&nicsof="+ofeca.getNic()+"&sol_creation_user="+ofeca.getCreation_user()+"&tipo_solicitudd="+ofeca.getTipo_solicitud();

                    }
                    session.setAttribute("nics", cle.getNics());
                    session.setAttribute("acciones", clsrv.buscarAcc(acceca,usuario.getNitPropietario()));
                    System.out.println(next);
                }
                catch(Exception e)
                {   e.printStackTrace();
                    System.out.println("Error:"+e.toString());
                }
            }

        }
        else
        {   if(op.equals("buscarcliente"))
            {
                try{//linea movida en 090929

                    String nicc=request.getParameter("nicc");//090929
                    String idclie=request.getParameter("idclie");//090929
                    if (idclie==null || idclie.equals("")){//090929
                        idclie=model.negociosApplusService.getIdClie(nicc);//090929
                    }//090929

                    ClienteEca cle=new ClienteEca();
                    //cle.setId_cliente(request.getParameter("clientee"));//090929
                    cle.setId_cliente(idclie);//090929

                    next=next+"?mensaje="+clsrv.buscarCl(cle)+"&ciudadd="+cle.getCiudad()+"&nombre_contacto="+cle.getNombre_contacto()
                            +"&celular_representante="+cle.getCelular_representante()+"&zonaa="+cle.getDepartamento()
                            +"&direccion="+cle.getDireccion()
                            +"&nit="+cle.getNit()+"&cargo_contacto="+cle.getCargo_contacto()
                            +"&nombre="+cle.getNombre()+"&nombre_representante="+cle.getNombre_representante()
                            +"&sectorr="+cle.getSector()+"&telefono_contacto="+cle.getTel1()+"&celular_contacto="+cle.getTel2()
                            +"&telefono_representante="+cle.getTel_representante()+"&tipo_identificacionn="+cle.getTipo()+"&creation_user="+cle.getCreation_user()
                            +"&ejecutivo_ctaa="+cle.getId_ejecutivo()+"&login_ejecutivo="+clsrv.getLoginEjecutivo(cle.getId_cliente())
                            +"&padre="+((cle.getId_padre().equals(cle.getId_cliente()))?"":cle.getNombre_padre())+"&cliespec="+cle.isOficial();
                    if(!cle.getId_cliente().equals(""))
                    {   next=next+"&idusuario="+cle.getId_cliente();
                    }
                    session.setAttribute("nics", cle.getNics());
                    System.out.println(next);
                }
                catch(Exception e)
                {   e.printStackTrace();
                    System.out.println("Error:"+e.toString());
                }
            }

        }
        if(op.equals("creasolicitud")){
            OfertaElca ofeca=new OfertaElca();
            ClienteEca cle=new ClienteEca();
            ofeca.setId_cliente(request.getParameter("cliente"));
            ofeca.setNic(request.getParameter("nicsoff"));
            ofeca.setDescripcion(request.getParameter("detalle_inconsistencia"));
            ofeca.setTipo_solicitud(request.getParameter("tipo_solicitud"));
            ofeca.setOficial("0");
            ofeca.setAviso( request.getParameter("inpAviso") );
            cle.setId_cliente(ofeca.getId_cliente());
            String resp="";
            try{
                clsrv.buscarCl(cle);
                resp=clsrv.insertarOf(ofeca,usuario.getLogin());
                clsrv.buscarOf(ofeca);

            next=next+"?mensaje="+resp+"&ciudadd="+cle.getCiudad()+"&nombre_contacto="+cle.getNombre_contacto()
                    +"&celular_representante="+cle.getCelular_representante()+"&zonaa="+cle.getDepartamento()
                    +"&direccion="+cle.getDireccion()
                    +"&nit="+cle.getNit()+"&cargo_contacto="+cle.getCargo_contacto()
                    +"&nombre="+cle.getNombre()+"&nombre_representante="+cle.getNombre_representante()
                    +"&sectorr="+cle.getSector()+"&telefono_contacto="+cle.getTel1()+"&celular_contacto="+cle.getTel2()
                    +"&telefono_representante="+cle.getTel_representante()+"&tipo_identificacionn="+cle.getTipo()+"&creation_user="+cle.getCreation_user()
                    +"&ejecutivo_ctaa="+cle.getId_ejecutivo()+"&historico="+ofeca.getDescripcion()+"&login_ejecutivo="+clsrv.getLoginEjecutivo(cle.getId_cliente())
                    +"&fechainicio="+ofeca.getCreation_date().substring(0,11)+"&nicsof="+ofeca.getNic()+"&sol_creation_user="+ofeca.getCreation_user()
                    +"&tipo_solicitudd="+ofeca.getTipo_solicitud()+"&tasa_trans="+ofeca.getOficial();
                    if(!cle.getId_cliente().equals(""))
                    {   next=next+"&idusuario="+cle.getId_cliente();
                    }
                    if(!ofeca.getId_solicitud().equals(""))
                    {   next=next+"&idsolicitud="+ofeca.getId_solicitud();
                    }
                    session.setAttribute("nics", cle.getNics());

                    /***************************Envio de Correo***************************/
                    HSendMail2 hSendMail2=new HSendMail2();
                    hSendMail2.start("imorales@fintravalores.com",
                            clsrv.getMails("3",  request.getParameter("idsolicitud"),""),
                            "", "", "Nueva Solicitud "+ofeca.getId_solicitud(),
                            "Se ha creado una solicitud nueva identificada con el codigo "+ofeca.getId_solicitud()+", asociada al cliente "+cle.getNombre(),usuario.getLogin());
                    /************************Fin Envio de Correo**************************/


            }
            catch(Exception e)
            {   e.printStackTrace();
                System.out.println("Error:"+e.toString());
            }

        }
        else
        {   AccionesEca acceca=new AccionesEca();
            if(op.equals("modsolicitud"))
            {   OfertaElca ofeca=new OfertaElca();
                ClienteEca cle=new ClienteEca();
                ofeca.setId_solicitud(request.getParameter("idsolicitud"));
                ofeca.setDescripcion(" ("+usuario.getLogin()+"): "+request.getParameter("detalle_inconsistencia"));
                ofeca.setTipo_solicitud(request.getParameter("tipo_solicitud"));
                ofeca.setOficial("0");
                ofeca.setAviso( request.getParameter("inpAviso") );
                cle.setId_cliente(request.getParameter("cliente"));
                acceca.setId_solicitud(ofeca.getId_solicitud());
                String resp="";
                try{
                    clsrv.buscarCl(cle);
                    resp=clsrv.updateOf(ofeca,usuario.getLogin());
                    clsrv.buscarOf(ofeca);

                next=next+"?mensaje="+resp+"&ciudadd="+cle.getCiudad()+"&nombre_contacto="+cle.getNombre_contacto()
                        +"&celular_representante="+cle.getCelular_representante()+"&zonaa="+cle.getDepartamento()
                        +"&direccion="+cle.getDireccion()
                        +"&nit="+cle.getNit()+"&cargo_contacto="+cle.getCargo_contacto()
                        +"&nombre="+cle.getNombre()+"&nombre_representante="+cle.getNombre_representante()
                        +"&sectorr="+cle.getSector()+"&telefono_contacto="+cle.getTel1()+"&celular_contacto="+cle.getTel2()+"&login_ejecutivo="+clsrv.getLoginEjecutivo(cle.getId_cliente())
                        +"&telefono_representante="+cle.getTel_representante()+"&tipo_identificacionn="+cle.getTipo()+"&creation_user="+cle.getCreation_user()
                        +"&ejecutivo_ctaa="+cle.getId_ejecutivo()+"&historico="+ofeca.getDescripcion()+"&sol_creation_user="+ofeca.getCreation_user()
                        +"&fechainicio="+ofeca.getCreation_date().substring(0,11)+"&nicsof="+ofeca.getNic()+"&tipo_solicitudd="+ofeca.getTipo_solicitud()
                        +"&tasa_trans="+ofeca.getOficial();
                        if(!cle.getId_cliente().equals(""))
                        {   next=next+"&idusuario="+cle.getId_cliente();
                        }
                        if(!ofeca.getId_solicitud().equals(""))
                        {   next=next+"&idsolicitud="+ofeca.getId_solicitud();
                        }
                        session.setAttribute("nics", cle.getNics());
                        session.setAttribute("acciones", clsrv.buscarAcc(acceca,usuario.getNitPropietario()));

                    /***************************Envio de Correo***************************/
                    HSendMail2 hSendMail2=new HSendMail2();
                    hSendMail2.start("imorales@fintravalores.com",
                            clsrv.getMails("3",  request.getParameter("idsolicitud"),""),
                            "", "", "Nueva Solicitud "+ofeca.getId_solicitud(),
                            "Se ha modificado la solicitud identificada con el codigo "+ofeca.getId_solicitud()+", asociada al cliente "+cle.getNombre(),usuario.getLogin());
                    /*************************Fin Envio de Correo*************************/

                }
                catch(Exception e)
                {   e.printStackTrace();
                    System.out.println("Error:"+e.toString());
                }
            }
            else
            {   if(op.equals("buscarsolicitud"))
                {   OfertaElca ofeca=new OfertaElca();
                    ClienteEca cle=new ClienteEca();
                    ofeca.setId_solicitud(request.getParameter("idsolicitud"));
                    String resp="";
                    try{
                        resp=clsrv.buscarOf(ofeca);
                        acceca.setId_solicitud(ofeca.getId_solicitud());
                        cle.setId_cliente(ofeca.getId_cliente());
                        clsrv.buscarCl(cle);

                    next=next+"?mensaje="+resp+"&ciudadd="+cle.getCiudad()+"&nombre_contacto="+cle.getNombre_contacto()
                            +"&celular_representante="+cle.getCelular_representante()+"&zonaa="+cle.getDepartamento()
                            +"&direccion="+cle.getDireccion()
                            +"&nit="+cle.getNit()+"&cargo_contacto="+cle.getCargo_contacto()
                            +"&nombre="+cle.getNombre()+"&nombre_representante="+cle.getNombre_representante()
                            +"&sectorr="+cle.getSector()+"&telefono_contacto="+cle.getTel1()+"&celular_contacto="+cle.getTel2()+"&login_ejecutivo="+clsrv.getLoginEjecutivo(cle.getId_cliente())
                            +"&telefono_representante="+cle.getTel_representante()+"&tipo_identificacionn="+cle.getTipo()+"&creation_user="+cle.getCreation_user()
                            +"&ejecutivo_ctaa="+cle.getId_ejecutivo()+"&historico="+ofeca.getDescripcion()+"&sol_creation_user="+ofeca.getCreation_user()
                            +"&fechainicio="+ofeca.getCreation_date().substring(0,11)+"&nicsof="+ofeca.getNic()+"&tipo_solicitudd="+ofeca.getTipo_solicitud();
                            if(!cle.getId_cliente().equals(""))
                            {   next=next+"&idusuario="+cle.getId_cliente();
                            }
                            if(!ofeca.getId_solicitud().equals(""))
                            {   next=next+"&idsolicitud="+ofeca.getId_solicitud();
                            }
                            session.setAttribute("nics", cle.getNics());
                            session.setAttribute("acciones", clsrv.buscarAcc(acceca,usuario.getNitPropietario()));

                    }
                    catch(Exception e)
                    {   e.printStackTrace();
                        System.out.println("Error:"+e.toString());
                    }
                }
            }
        }
        if(op.equals("creaaccion"))
        {   OfertaElca ofeca=new OfertaElca();
            ClienteEca cle=new ClienteEca();
            AccionesEca acceca=new AccionesEca();
            acceca.setDescripcion(request.getParameter("descripcion"));
            acceca.setContratista(request.getParameter("contratista"));
            ofeca.setId_solicitud(request.getParameter("idsolicitud"));
            acceca.setId_solicitud(ofeca.getId_solicitud());
            String resp="";
            try{
                resp=clsrv.insertarAcc(acceca,usuario.getLogin());
                clsrv.buscarOf(ofeca);
                cle.setId_cliente(ofeca.getId_cliente());
                clsrv.buscarCl(cle);


            next=next+"?mensaje="+resp+"&ciudadd="+cle.getCiudad()+"&nombre_contacto="+cle.getNombre_contacto()
                    +"&celular_representante="+cle.getCelular_representante()+"&zonaa="+cle.getDepartamento()
                    +"&direccion="+cle.getDireccion()
                    +"&nit="+cle.getNit()+"&cargo_contacto="+cle.getCargo_contacto()
                    +"&nombre="+cle.getNombre()+"&nombre_representante="+cle.getNombre_representante()
                    +"&sectorr="+cle.getSector()+"&telefono_contacto="+cle.getTel1()+"&celular_contacto="+cle.getTel2()+"&login_ejecutivo="+clsrv.getLoginEjecutivo(cle.getId_cliente())
                    +"&telefono_representante="+cle.getTel_representante()+"&tipo_identificacionn="+cle.getTipo()+"&creation_user="+cle.getCreation_user()
                    +"&ejecutivo_ctaa="+cle.getId_ejecutivo()+"&historico="+ofeca.getDescripcion()+"&sol_creation_user="+ofeca.getCreation_user()+"&tipo_solicitudd="+ofeca.getTipo_solicitud()
                    +"&fechainicio="+ofeca.getCreation_date().substring(0,11)+"&nicsof="+ofeca.getNic();
                    if(!cle.getId_cliente().equals(""))
                    {   next=next+"&idusuario="+cle.getId_cliente();
                    }
                    if(!ofeca.getId_solicitud().equals(""))
                    {   next=next+"&idsolicitud="+ofeca.getId_solicitud();
                    }
                    session.setAttribute("nics", cle.getNics());
                    session.setAttribute("acciones", clsrv.buscarAcc(acceca,usuario.getNitPropietario()));

                    /***************************Envio de Correo***************************/
                    HSendMail2 hSendMail2=new HSendMail2();

                    String[] datos_mensaje=clsrv.getDatosMensaje(acceca.getId_accion());//091203

                    hSendMail2.start("opav_asignacion@fintravalores.com",
                            //clsrv.getMails("3",  request.getParameter("idsolicitud"),acceca.getContratista())
                            //linea que reemplaza a la anterior en 091201:
                            datos_mensaje[3]
                            ,"", "", "Solicitud "+ofeca.getId_solicitud(),
                            "Se�ores " +datos_mensaje[0]+", Zona "+datos_mensaje[1]+": se ha creado la solicitud "+ofeca.getId_solicitud()+" para el cliente  "+datos_mensaje[2]+". Favor consultar en http://www.fintravalores.com/consorcio y gestionar lo mas pronto posible.",usuario.getLogin());

                    /*hSendMail2.start("imorales@fintravalores.com",
                            clsrv.getMails("3",  request.getParameter("idsolicitud"),acceca.getContratista()),
                            "", "", "Nueva Solicitud "+ofeca.getId_solicitud(),
                            "Se ha creado una nueva accion identificada con el codigo "+acceca.getId_accion()+", asociada al contratista "+acceca.getContratista(),usuario.getLogin());*/
                    /*************************Fin Envio de Correo*************************/


            }
            catch(Exception e)
            {   e.printStackTrace();
                System.out.println("Error:"+e.toString());
            }
        }
        else
        {   if(op.equals("eliminaaccion"))
            {   OfertaElca ofeca=new OfertaElca();
                ClienteEca cle=new ClienteEca();
                AccionesEca acceca=new AccionesEca();
                acceca.setDescripcion(request.getParameter("descripcion"));
                acceca.setContratista(request.getParameter("contratista"));
                acceca.setId_accion(request.getParameter("idaccion"));
                ofeca.setId_solicitud(request.getParameter("idsolicitud"));
                acceca.setId_solicitud(ofeca.getId_solicitud());
                String resp="";
                try{
                    resp=clsrv.delAcc(acceca,usuario.getLogin());
                    clsrv.buscarOf(ofeca);
                    cle.setId_cliente(ofeca.getId_cliente());
                    clsrv.buscarCl(cle);


                next=next+"?mensaje="+resp+"&ciudadd="+cle.getCiudad()+"&nombre_contacto="+cle.getNombre_contacto()
                        +"&celular_representante="+cle.getCelular_representante()+"&zonaa="+cle.getDepartamento()
                        +"&direccion="+cle.getDireccion()
                        +"&nit="+cle.getNit()+"&cargo_contacto="+cle.getCargo_contacto()
                        +"&nombre="+cle.getNombre()+"&nombre_representante="+cle.getNombre_representante()+"&login_ejecutivo="+clsrv.getLoginEjecutivo(cle.getId_cliente())
                        +"&sectorr="+cle.getSector()+"&telefono_contacto="+cle.getTel1()+"&celular_contacto="+cle.getTel2()+"&sol_creation_user="+ofeca.getCreation_user()
                        +"&telefono_representante="+cle.getTel_representante()+"&tipo_identificacionn="+cle.getTipo()+"&creation_user="+cle.getCreation_user()
                        +"&ejecutivo_ctaa="+cle.getId_ejecutivo()+"&historico="+ofeca.getDescripcion()
                        +"&fechainicio="+ofeca.getCreation_date().substring(0,11)+"&nicsof="+ofeca.getNic()+"&tipo_solicitudd="+ofeca.getTipo_solicitud();
                        if(!cle.getId_cliente().equals(""))
                        {   next=next+"&idusuario="+cle.getId_cliente();
                        }
                        if(!ofeca.getId_solicitud().equals(""))
                        {   next=next+"&idsolicitud="+ofeca.getId_solicitud();
                        }
                        session.setAttribute("nics", cle.getNics());
                        session.setAttribute("acciones", clsrv.buscarAcc(acceca,usuario.getNitPropietario()));
                        /***************************Envio de Correo***************************/
                        HSendMail2 hSendMail2=new HSendMail2();
                        hSendMail2.start("imorales@fintravalores.com",
                                clsrv.getMails("3",  request.getParameter("idsolicitud"),acceca.getContratista()),
                                "", "", "Eliminar Solicitud "+ofeca.getId_solicitud(),
                                "Se ha eliminado la accion identificada con el codigo "+acceca.getId_accion()+", asociada al contratista "+acceca.getContratista(),usuario.getLogin());
                        /*************************Fin Envio de Correo*************************/

                }
                catch(Exception e)
                {   e.printStackTrace();
                    System.out.println("Error:"+e.toString());
                }
            }
        }

        if(op.equals("refrescarx")){
            OfertaElca ofeca=new OfertaElca();
            ClienteEca cle=new ClienteEca();
            AccionesEca acceca=new AccionesEca();
            acceca.setDescripcion(request.getParameter("descripcion"));
            acceca.setContratista(request.getParameter("contratista"));
            ofeca.setId_solicitud(request.getParameter("idsolicitud"));
            acceca.setId_solicitud(ofeca.getId_solicitud());
            String resp="";
            try{
                //resp=clsrv.insertarAcc(acceca,usuario.getLogin());
                resp="pagina refrescada.";
                clsrv.buscarOf(ofeca);
                cle.setId_cliente(ofeca.getId_cliente());
                clsrv.buscarCl(cle);


            /*next=next+"?mensaje="+resp+"&ciudadd="+cle.getCiudad()+"&nombre_contacto="+cle.getNombre_contacto()
                    +"&celular_representante="+cle.getCelular_representante()+"&zonaa="+cle.getDepartamento()
                    +"&direccion="+cle.getDireccion()
                    +"&nit="+cle.getNit()+"&cargo_contacto="+cle.getCargo_contacto()
                    +"&nombre="+cle.getNombre()+"&nombre_representante="+cle.getNombre_representante()
                    +"&sectorr="+cle.getSector()+"&telefono_contacto="+cle.getTel1()+"&celular_contacto="+cle.getTel2()+"&login_ejecutivo="+clsrv.getLoginEjecutivo(cle.getId_cliente())
                    +"&telefono_representante="+cle.getTel_representante()+"&tipo_identificacionn="+cle.getTipo()+"&creation_user="+cle.getCreation_user()
                    +"&ejecutivo_ctaa="+cle.getId_ejecutivo()+"&historico="+ofeca.getDescripcion()+"&sol_creation_user="+ofeca.getCreation_user()+"&tipo_solicitudd="+ofeca.getTipo_solicitud()
                    +"&fechainicio="+ofeca.getCreation_date().substring(0,11)+"&nicsof="+ofeca.getNic();*/

            next=next+"?mensaje="+resp+"&ciudadd="+cle.getCiudad()+"&nombre_contacto="+cle.getNombre_contacto()
                    +"&celular_representante="+cle.getCelular_representante()+"&zonaa="+cle.getDepartamento()
                    +"&direccion="+cle.getDireccion()
                    +"&nit="+cle.getNit()+"&cargo_contacto="+cle.getCargo_contacto()
                    +"&nombre="+cle.getNombre()+"&nombre_representante="+cle.getNombre_representante()
                    +"&sectorr="+cle.getSector()+"&telefono_contacto="+cle.getTel1()+"&celular_contacto="+cle.getTel2()+"&login_ejecutivo="+clsrv.getLoginEjecutivo(cle.getId_cliente())
                    +"&telefono_representante="+cle.getTel_representante()+"&tipo_identificacionn="+cle.getTipo()+"&creation_user="+cle.getCreation_user()
                    +"&ejecutivo_ctaa="+cle.getId_ejecutivo()+"&historico="+ofeca.getDescripcion()+"&sol_creation_user="+ofeca.getCreation_user()+"&tipo_solicitudd="+ofeca.getTipo_solicitud()
                    +"&fechainicio="+((ofeca.getCreation_date()== null || ofeca.getCreation_date().equals("")) ? "":ofeca.getCreation_date().substring(0,11))+"&nicsof="+ofeca.getNic();//091215


                    if(!cle.getId_cliente().equals(""))
                    {   next=next+"&idusuario="+cle.getId_cliente();
                    }
                    if(!ofeca.getId_solicitud().equals(""))
                    {   next=next+"&idsolicitud="+ofeca.getId_solicitud();
                    }
                    session.setAttribute("nics", cle.getNics());
                    session.setAttribute("acciones", clsrv.buscarAcc(acceca,usuario.getNitPropietario()));

            }
            catch(Exception e)
            {
                System.out.println("error refrescando"+e.toString()+"__"+e.getMessage());
                e.printStackTrace();
                System.out.println("Error:"+e.toString());
            }
        }

        try
        {   this.dispatchRequest(next);
        }
        catch(Exception eee)
        {   eee.printStackTrace();
            System.out.println("Error:"+eee.toString());
        }
    }

}