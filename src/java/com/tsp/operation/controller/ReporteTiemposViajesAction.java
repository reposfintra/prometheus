/**
 * Nombre       ReporteTiemposViajesAction.java
 * Autor        Ing Jose de la Rosa
 * Fecha        23 de agosto de 2006, 11:49 AM
 * versi�n      1.0
 * Copyright    Transportes Sanchez Polo S.A.
 **/

package com.tsp.operation.controller;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;
import com.tsp.util.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  EQUIPO13
 */
public class ReporteTiemposViajesAction extends Action{
    
    /** Creates a new instance of ReporteTiemposViajesAction */
    public ReporteTiemposViajesAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next = "/jsp/general/exportar/ReporteTiemposViaje.jsp";
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String distrito = (String) session.getAttribute ("Distrito");
        String fchi = (String) request.getParameter ("fechaInicio");
        String fchf = (String) request.getParameter ("fechaFinal");
        String cliente = (String) request.getParameter ("cliente");
        String[]   tviajes           =  request.getParameterValues("checkbox");
        String tviaje = "";
        if( tviajes!=null ){
            tviaje = model.ExtractosSvc.getToString( tviajes );
        }
        else
            tviaje= "";
        try{
            ReporteTiemposViajesRemesaThreads hilo = new ReporteTiemposViajesRemesaThreads(model);
            hilo.start( fchi, fchf, usuario.getLogin (), distrito, cliente, tviaje);
            next += "?msg=La generacion del Reporte ha iniciado...<br>Para realizar el seguimiento del proceso haga clic en el vinculo&ruta=PROCESOS/Seguimiento de Procesos";
        }catch (Exception ex){
            throw new ServletException ("Error en ReporteTiemposViajesAction......\n"+ex.getMessage ());
        }
        this.dispatchRequest (next);        
    }
    
}
