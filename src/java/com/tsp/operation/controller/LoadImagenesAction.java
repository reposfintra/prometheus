/***************************************
    * Nombre Clase ............. LoadImagenesAction.java
    * Descripci�n  .. . . . . .  Permite manejar eventos y cargr datos para  las imagenes
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  04/03/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...Transportes Sanchez Polo S.A.
    *******************************************/




package com.tsp.operation.controller;




import java.io.*;
import java.util.*;
import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.threads.*;



public class LoadImagenesAction extends Action{
    
  
    
    public void run() throws ServletException, InformationException {
        
        
         try{
            
                
                String next    = request.getParameter("pagina");   
                String evento  = request.getParameter("evento");   
                
                if( evento!=null){
                    
                    if( evento.equals("RESET") ){
                        model.ImagenSvc.reset();
                        
                        model.ImagenSvc.searchActividades();
                        model.ImagenSvc.searchRelacion();
                        model.ImagenSvc.searchDocumentos();
                        model.ImagenSvc.searchAgencias();                        
                        
                        String  act      = request.getParameter("actividad");
                        String  tipoDoc  = request.getParameter("tipoDocumento");
                        String  doc      = request.getParameter("documento");
                        String  proc     = request.getParameter("procedencia");                        
                        
                        if ( act     != null)   model.ImagenSvc.setActividad    ( act     );
                        if ( tipoDoc != null)   model.ImagenSvc.setTipoDocumento( tipoDoc );
                        if ( doc     != null)   model.ImagenSvc.setDocumento    ( doc     );
                        if ( proc    != null)   model.ImagenSvc.setProcedencia  ( proc    );
                        
                    }
                    
                    if( evento.equals("ATRAS") ){
                        String procedencia = request.getParameter("procedencia");
                        model.ImagenSvc.atras();  
                        model.ImagenSvc.setProcedencia( procedencia );
                    }
                    
                    
                    if( evento.equals("CLEAR") ){
                        String procedencia = request.getParameter("procedencia");
                        if( procedencia.equals("DESPACHO")  )
                            model.ImagenSvc.atras();
                        else
                            model.ImagenSvc.reset(); 
                        model.ImagenSvc.setProcedencia( procedencia );
                    }
                    
                    
                    
                }
                
                
                
                
                
                
                RequestDispatcher rd = application.getRequestDispatcher(next);
                if(rd == null)
                    throw new Exception("No se pudo encontrar "+ next);
                rd.forward(request, response);         
            
        } catch (Exception e){
             e.printStackTrace();
             throw new ServletException(e.getMessage());
        }
         
         
    }
    
}
