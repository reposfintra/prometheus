/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.GarantiasCreditosDAO;
import com.tsp.operation.model.DAOS.impl.GarantiasCreditosImpl;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.GarantiasCreditosAutommotor;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.UtilFinanzas;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

/**
 *
 * @author edgargonzalezmendoza
 */
public class GarantiasCreditosAction extends Action{
    
    final int CARGAR_ARCHIVO_FASECOLDA = 1;
    final int CARGAR_MARCA_VEHICULO = 2;
    final int CARGAR_CALSE_VEHICULO = 3;
    final int INFO_FASECOLDA = 4;
    final int GUARDAR_GARANTIAS_AUTOMOTOR = 5;
    final int LISTAR_GARANTIAS_AUTOMOTOR = 6;
    final int EDITAR_GARANTIAS_AUTOMOTOR = 7;
    final int CARGAR_ASEGURADORA = 8;
    final int VISUALIZAR_PRE_CARGA_FASECOLDA = 9;
    final int CREAR_MAESTROS_FASECOLDA = 10;
    final int LIMPIAR_PRECARGA = 11;
    
    Usuario usuario=null;
    GarantiasCreditosDAO gcdao;

    @Override
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            gcdao = new GarantiasCreditosImpl(usuario.getBd());
            int opcion = Integer.parseInt(request.getParameter("opcion"));
           

            switch (opcion) {
                case CARGAR_ARCHIVO_FASECOLDA:
                    cargarArchivo();
                    break;
                case CARGAR_MARCA_VEHICULO:
                    cargarMarca();
                    break;
                case CARGAR_CALSE_VEHICULO:
                    cargarCombox();
                    break;
                case INFO_FASECOLDA:
                    infoFasecolda();
                    break;
                case GUARDAR_GARANTIAS_AUTOMOTOR:
                    guardarGarantiaAutomotor(); 
                    break;
                case LISTAR_GARANTIAS_AUTOMOTOR:
                     listarGarantiasAutomotor();
                    break;
                case EDITAR_GARANTIAS_AUTOMOTOR:
                    editarInfoFasecolda();
                    break;
                case CARGAR_ASEGURADORA:
                   cargarAseguradoras(); 
                    break;
                case VISUALIZAR_PRE_CARGA_FASECOLDA:
                    visualizarPrecarga();
                    break;
                case CREAR_MAESTROS_FASECOLDA:
                    crearMaestrosFasecolda();
                    break;
                case LIMPIAR_PRECARGA:
                    limpiarPrecargaFasecolda(); 
                    break;    
                
            }

        } catch (Exception ex) {
            Logger.getLogger(GarantiasCreditosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void cargarArchivo() throws Exception {

        String respuestaJson = "{}";
        try {

            String ruta = UtilFinanzas.obtenerRuta("ruta", "/exportar/migracion/"
                    + ((usuario != null) ? usuario.getLogin() : "anonimo"));

            //Obtenemos campos de formulario (Tanto de texto como archivo) (enctype=multipart/form-data)
            MultipartRequest mreq = new MultipartRequest(request, ruta, 4096*2048, new DefaultFileRenamePolicy());
            String filetype = mreq.getContentType("archivo");
          
            //Validamos la extensi�n del archivo que va a cargarse
            if (filetype.equals("application/excel") || filetype.equals("application/vnd.ms-excel")
                    ||filetype.equals("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) {

                    String filename = mreq.getFilesystemName("archivo");
                    TransaccionService tservice = new TransaccionService(usuario.getBd());
                    tservice.crearStatement();
                    tservice = getAddBatch(tservice, gcdao.cargarArchivoPreProceso(ruta + "/" + filename, usuario, filename));
                    tservice.execute();
                    tservice.closeAll();
                    
                 respuestaJson = "{\"respuesta\":\"OK\"}";

            } else {
                mreq.getFile("archivo").delete();
               
                respuestaJson = "{\"error\":\"La extensi�n del archivo es inv�lida. Verifique.\"}";
            }

        } catch (Exception ex) {
            ex.printStackTrace();
            respuestaJson = "{\"error\":\"Lo sentimos no se pudo subir el archivo.\"}";
        } finally {

            this.printlnResponse(respuestaJson, "application/json;");
        }

    }
    
    private void cargarMarca() throws Exception{
       String q= request.getParameter("q")!=null?request.getParameter("q"):""; 
       printlnResponse(gcdao.cargarMarca(q), "application/json;");
    
    }
    
    private void cargarCombox() throws Exception {
        
        String parametro1 = request.getParameter("parametro1") != null ? request.getParameter("parametro1") : "";
        String parametro2 = request.getParameter("parametro2") != null ? request.getParameter("parametro2") : "";
        String marca = request.getParameter("marca") != null ? request.getParameter("marca") : "";
        int option = Integer.parseInt(request.getParameter("option"));
        printlnResponse(gcdao.cargarCombox(parametro1, parametro2,marca, option), "application/json;");
        
    }
    
    private void infoFasecolda() throws Exception {
        
        String marca = request.getParameter("marca") != null ? request.getParameter("marca") : "";
        String clase = request.getParameter("clase") != null ? request.getParameter("clase") : "";
        String ref1 = request.getParameter("ref1") != null ? request.getParameter("ref1") : "";
        String ref2 = request.getParameter("ref2") != null ? request.getParameter("ref2") : "";
        String ref3 = request.getParameter("ref3") != null ? request.getParameter("ref3") : "";
//        String servicio= request.getParameter("servicio") != null ? request.getParameter("servicio") : "";
        printlnResponse(gcdao.infoFasecolda(marca, clase, ref1, ref2, ref3), "application/json;");
         
    }
    
    private void guardarGarantiaAutomotor() throws Exception {
        
        String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
        String identificacion = request.getParameter("identificacion") != null ? request.getParameter("identificacion") : "";
        String nombre = request.getParameter("nombre") != null ? request.getParameter("nombre") : "";
        String nro_propiedad = request.getParameter("nro_propiedad") != null ? request.getParameter("nro_propiedad") : "";
        String chasis = request.getParameter("chasis") != null ? request.getParameter("chasis") : "";
        String motor = request.getParameter("motor") != null ? request.getParameter("motor") : "";
        
        String nr_poliza = request.getParameter("nr_poliza") != null ? request.getParameter("nr_poliza") : "";
        String fecha_exp = request.getParameter("fecha_exp") != null ? request.getParameter("fecha_exp") : "";
        String fecha_ven = request.getParameter("fecha_ven") != null ? request.getParameter("fecha_ven") : "";
        String negocio_poliza = request.getParameter("negocio_poliza") != null ? request.getParameter("negocio_poliza") : "";
        
        String placa = request.getParameter("placa") != null ? request.getParameter("placa") : "";
        String marca = request.getParameter("marca") != null ? request.getParameter("marca") : "";
        String clase = request.getParameter("clase") != null ? request.getParameter("clase") : "";
        String servicio = request.getParameter("servicio") != null ? request.getParameter("servicio") : "";
        String referencia1 = request.getParameter("referencia1") != null ? request.getParameter("referencia1") : "";
        String referencia2 = request.getParameter("referencia2") != null ? request.getParameter("referencia2") : "";
        String referencia3 = request.getParameter("referencia3") != null ? request.getParameter("referencia3") : "";
        String pais = request.getParameter("pais") != null ? request.getParameter("pais") : "";
        String modelo = request.getParameter("modelo") != null ? request.getParameter("modelo") : "";
        String aseguradora = request.getParameter("aseguradora") != null ? request.getParameter("aseguradora") : "";
        String codigo_fasecolda = request.getParameter("codigo_fasecolda") != null ? request.getParameter("codigo_fasecolda") : "";
        double valor_fasecolda = request.getParameter("valor_fasecolda") != null ? Double.parseDouble(request.getParameter("valor_fasecolda")) : 0;
        
        GarantiasCreditosAutommotor gca=new GarantiasCreditosAutommotor();
        gca.setNegocio(negocio);
        gca.setIdentificacion(identificacion);
        gca.setNombre(nombre);
        gca.setNro_propiedad(nro_propiedad);
        gca.setChasis(chasis);
        gca.setMotor(motor);
        gca.setNr_poliza(nr_poliza);
        gca.setFecha_exp(fecha_exp);
        gca.setFecha_ven(fecha_ven);
        gca.setNegocio_poliza(negocio_poliza);
        gca.setPlaca(placa);
        gca.setMarca(marca);
        gca.setClase(clase);
        gca.setServicio(servicio);
        gca.setReferencia1(referencia1);
        gca.setReferencia2(referencia2);
        gca.setReferencia3(referencia3);
        gca.setPais(pais);
        gca.setModelo(modelo);
        gca.setAseguradora(aseguradora);
        gca.setCodigo_fasecolda(codigo_fasecolda);
        gca.setValor_fasecolda(valor_fasecolda);
        
        printlnResponse( gcdao.guardarGarantiaAutomotor(gca, usuario), "application/json;");

    }
    
    
    private void listarGarantiasAutomotor() throws Exception {
        String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
        printlnResponse(gcdao.ListaGarantiaAutomotor(negocio, usuario), "application/json;");
    }
    
    private void editarInfoFasecolda() throws Exception{
        
        int id= request.getParameter("id") != null ? Integer.parseInt(request.getParameter("id")) : 0;
        String estado= request.getParameter("reg_status") != null ? request.getParameter("reg_status") : "";
        String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
        String nro_propiedad = request.getParameter("tarjeta_propiedad") != null ? request.getParameter("tarjeta_propiedad") : "";
        String chasis = request.getParameter("nro_chasis") != null ? request.getParameter("nro_chasis") : "";
        String motor = request.getParameter("nro_motor") != null ? request.getParameter("nro_motor") : "";
        String codigo_fasecolda = request.getParameter("codigo_fasecolda") != null ? request.getParameter("codigo_fasecolda") : "";
        String modelo = request.getParameter("modelo") != null ? request.getParameter("modelo") : "";
        String placa = request.getParameter("placa") != null ? request.getParameter("placa") : "";
        String marca = request.getParameter("marca") != null ? request.getParameter("marca") : "";
        String clase = request.getParameter("clase") != null ? request.getParameter("clase") : "";
        String servicio = request.getParameter("servicio") != null ? request.getParameter("servicio") : "";
        String referencia1 = request.getParameter("referencia1") != null ? request.getParameter("referencia1") : "";
        String referencia2 = request.getParameter("referencia2") != null ? request.getParameter("referencia2") : "";
        String referencia3 = request.getParameter("referencia3") != null ? request.getParameter("referencia3") : "";
        String pais = request.getParameter("pais") != null ? request.getParameter("pais") : "";
        String aseguradora = request.getParameter("aseguradora") != null ? request.getParameter("aseguradora") : "";
        double valor_fasecolda = request.getParameter("valor_fasecolda") != null ? Double.parseDouble(request.getParameter("valor_fasecolda")) : 0;
        
        GarantiasCreditosAutommotor gca=new GarantiasCreditosAutommotor();
        gca.setEstado(estado.equals("Activo") ? "" : "A");
        gca.setNegocio(negocio);
        gca.setNro_propiedad(nro_propiedad);
        gca.setChasis(chasis);
        gca.setMotor(motor);
        gca.setPlaca(placa);
        gca.setMarca(marca);
        gca.setClase(clase);
        gca.setServicio(servicio);
        gca.setReferencia1(referencia1);
        gca.setReferencia2(referencia2);
        gca.setReferencia3(referencia3);
        gca.setPais(pais);
        gca.setModelo(modelo);
        gca.setAseguradora(aseguradora);
        gca.setCodigo_fasecolda(codigo_fasecolda);
        gca.setValor_fasecolda(valor_fasecolda);
        
        printlnResponse(gcdao.actualizarGarantias(gca,1, usuario), "application/json;");
        
    }
    
    
    private void cargarAseguradoras() throws Exception {        
        this.printlnResponse(gcdao.cargaraSeguradora(usuario), "application/text;");        
    }
    
    private void visualizarPrecarga() throws Exception {        
        this.printlnResponse(gcdao.visualizarPrecarga(usuario), "application/text;");        
    }
    
    private void crearMaestrosFasecolda() throws Exception {        
        this.printlnResponse(gcdao.crearMaestrosFasecolda(usuario), "application/text;");        
    }
    
    private void limpiarPrecargaFasecolda() throws Exception {        
         this.printlnResponse(gcdao.limpiarPrecargaFasecolda(usuario), "application/text;");        
    }


    /**
     * Escribe la respuesta de una opcion ejecutada desde AJAX
     *
     * @param respuesta
     * @param contentType
     * @throws Exception
     */
    private void printlnResponse(String respuesta, String contentType) throws Exception {

        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);

    }
    
}
