/***************************************************************************
 * Nombre clase : ............... ReporteOCAnticipoAction.java             *
 * Descripcion :................. Clase que maneja los eventos             *
 *                                relacionados con el programa de          *
 *                                Reporte de OC con anticipos sin tr�fico  *
 * Autor :....................... Ing. Iv�n Dar�o Devia Acosta             *
 * Fecha :........................ 23 de noviembre de 2006, 11:36 PM       *
 * Version :...................... 1.0                                     *
 * Copyright :.................... Fintravalores S.A.                 *
 ***************************************************************************/
package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import com.tsp.operation.model.threads.*;
/**
 *
 * @author  idevia
 */
public class ReporteOCAnticipoAction extends Action{
    
    
    public ReporteOCAnticipoAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        try{
            
            /*
             *Declaracion de variables
             */
            String Opcion                 = (request.getParameter("Opcion")!=null)?request.getParameter("Opcion"):"";           
            String fechainicial           = (request.getParameter("fechaini")!=null)?request.getParameter("fechaini"):"";
            String fechafinal             = (request.getParameter("fechafin")!=null)?request.getParameter("fechafin"):"";
            
            HttpSession Session           = request.getSession();
            Usuario user                  = new Usuario();
            user                          = (Usuario)Session.getAttribute("Usuario");            
            String dstrct                 = user.getDstrct();            
            
            String next                   = "";
            String Mensaje                = "";
           
            
            /*
             *Manejo de eventos
             */
            
            if (Opcion.equals("Listado")){
                model.reporteOcAnt.List(fechainicial, fechafinal, dstrct);
                next = "/jsp/masivo/reportes/ReporteOCAnticipoSinTrafico/ReporteOCAnticipoMostrarDatos.jsp";//Mostrar
            }
            
            if(Opcion.equals("Excel")){
                ReporteOCAnticipoTh hilo = new ReporteOCAnticipoTh();
                hilo.start(model.reporteOcAnt.getList(),fechainicial,fechafinal,user);
                Mensaje = "El proceso se ha iniciado exitosamente";
                next = "/jsp/masivo/reportes/ReporteOCAnticipoSinTrafico/ReporteOCAnticipo.jsp?Mensaje="+Mensaje;
                
            }
            
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en ReporteOCAnticipoAction .....\n"+e.getMessage());
        }
        
    }
    
}
