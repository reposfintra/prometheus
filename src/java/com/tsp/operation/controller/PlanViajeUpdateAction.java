/**
 *
 */
package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import java.util.*;
import java.text.*;



/**
 * Searches the database for puchase orders matching the
 * purchase search argument
 */
public class PlanViajeUpdateAction extends Action {
    static Logger logger = Logger.getLogger(PlanViajeUpdateAction.class);
    
    /**
     * Executes the action
     */
    public void run() throws ServletException, InformationException {
        String next = "";
        String retorno = null;
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        Calendar fecha = Calendar.getInstance();
        SimpleDateFormat FFormat = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat TFormat = new SimpleDateFormat("HHmm");
        String last_mod_date = FFormat.format(fecha.getTime());
        String last_mod_time = TFormat.format(fecha.getTime());
        PlanViaje planVj = new PlanViaje();
        NitSot conductor = null;
        NitSot propietario = null;
        
        planVj.setPlanilla(request.getParameter("planilla"));
        planVj.setPlaca(request.getParameter("placa"));
        planVj.setProducto(request.getParameter("producto"));
        planVj.setTrailer(request.getParameter("trailer"));
        planVj.setContenedor(request.getParameter("contenedor"));
        planVj.setFecha(request.getParameter("fecha"));
        planVj.setDestinatario(request.getParameter("destinatario"));
        planVj.setRuta(request.getParameter("ruta"));
        planVj.setCedcon(request.getParameter("cedcon"));
        planVj.setAl1(request.getParameter("al1"));
        planVj.setAt1(request.getParameter("at1"));
        planVj.setAl2(request.getParameter("al2"));
        planVj.setAt2(request.getParameter("at2"));
        planVj.setAl3(request.getParameter("al3"));
        planVj.setAt3(request.getParameter("at3"));
        planVj.setPl1(request.getParameter("pl1"));
        planVj.setPt1(request.getParameter("pt1"));
        planVj.setPl2(request.getParameter("pl2"));
        planVj.setPt2(request.getParameter("pt2"));
        planVj.setPl3(request.getParameter("pl3"));
        planVj.setPt3(request.getParameter("pt3"));
        planVj.setRadio(request.getParameter("radio"));
        planVj.setCelular(request.getParameter("celular"));
        planVj.setAvantel(request.getParameter("avantel"));
        planVj.setTelefono(request.getParameter("telefono"));
        planVj.setCazador(request.getParameter("cazador"));
        planVj.setMovil(request.getParameter("movil"));
        planVj.setOtro(request.getParameter("otro"));
        planVj.setQlo(request.getParameter("qlo"));
        planVj.setQto(request.getParameter("qto"));
        planVj.setQld(request.getParameter("qld"));
        planVj.setQtd(request.getParameter("qtd"));
        planVj.setQl1(request.getParameter("ql1"));
        planVj.setQt1(request.getParameter("qt1"));
        planVj.setQl2(request.getParameter("ql2"));
        planVj.setQt2(request.getParameter("qt2"));
        planVj.setQl3(request.getParameter("ql3"));
        planVj.setQt3(request.getParameter("qt3"));
        planVj.setTl1(request.getParameter("tl1"));
        planVj.setTt1(request.getParameter("tt1"));
        planVj.setTl2(request.getParameter("tl2"));
        planVj.setTt2(request.getParameter("tt2"));
        planVj.setTl3(request.getParameter("tl3"));
        planVj.setTt3(request.getParameter("tt3"));
        planVj.setNomfam(request.getParameter("nomfam"));
        planVj.setPhonefam(request.getParameter("phonefam"));
        planVj.setNitpro(request.getParameter("nitpro"));
        planVj.setComentario1(request.getParameter("comentario1"));
        planVj.setComentario2(request.getParameter("comentario2"));
        planVj.setComentario3(request.getParameter("comentario3"));
        planVj.setLast_mod_date(last_mod_date);
        planVj.setLast_mod_time(last_mod_time);
        planVj.setLast_mod_user(usuario.getLogin());
        planVj.setCia(request.getParameter("cia"));
        
        retorno = request.getParameter("retorno");
        if (retorno  == null)
            retorno = "N";
        planVj.setRetorno(retorno);
        
        try {
            if(!model.planViajeService.tieneMovimiento(planVj.getPlanilla())){ 
                //busca el nit del conductor para crearlo o actualizarlo
                if (model.nitService.searchNit(planVj.getCedcon())){
                    conductor = model.nitService.getNit();
                    conductor.setNombre(request.getParameter("nomcon"));
                    conductor.setDireccion(request.getParameter("dircon"));
                    conductor.setTelefono(request.getParameter("phonecon"));
                    conductor.setCodciu(request.getParameter("ciucon"));
                    model.nitService.updateNit(conductor);
                }
                else{
                    conductor = new NitSot();
                    conductor.setCedula(request.getParameter("cedcon"));
                    conductor.setNombre(request.getParameter("nomcon"));
                    conductor.setDireccion(request.getParameter("dircon"));
                    conductor.setTelefono(request.getParameter("phonecon"));
                    conductor.setCodciu(request.getParameter("ciucon"));
                    model.nitService.insertNit(conductor);
                }
                
                //busca el nit del propetrio para crearlo o actualizarlo
                if (model.nitService.searchNit(planVj.getCedcon())){
                    propietario = model.nitService.getNit();
                    propietario.setNombre(request.getParameter("nompro"));
                    propietario.setDireccion(request.getParameter("dirpro"));
                    propietario.setTelefono(request.getParameter("phonepro"));
                    propietario.setCodciu(request.getParameter("ciupro"));
                }
                else{
                    propietario = new NitSot();
                    propietario.setCedula(request.getParameter("cedpro"));
                    propietario.setNombre(request.getParameter("nompro"));
                    propietario.setDireccion(request.getParameter("dirpro"));
                    propietario.setTelefono(request.getParameter("phonepro"));
                    propietario.setCodciu(request.getParameter("ciupro"));
                    model.nitService.insertNit(propietario);
                }
                
                model.planViajeService.updatePlanVj(planVj);
                
                logger.info(usuario.getNombre() + " Modific� planViaje con: Planilla = " + planVj.getPlanilla());
                request.setAttribute("mensaje", "� PLAN VIAJE " + planVj.getPlanilla() + " MODIFICADO EXITOSAMENTE !");
            }else{
                request.setAttribute("mensaje", "� PLAN VIAJE " + planVj.getPlanilla() + " NO SE MODIFICO PORQUE LA PLANILLA TIENE MOVIMIENTOS EN TRAFICO  !");
            }
            
            next = "/planviaje/PantallaFiltroPlanViaje.jsp";
        }
        catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
    }
}
