/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.ConfigProductosEDSDAO;
import com.tsp.operation.model.DAOS.impl.ConfigProductosEDSImpl;
import com.tsp.operation.model.beans.Usuario;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;

/**
 *
 * @author egonzalez
 */
public class ConfigProductosEDSAction extends Action {

    private final int CONSULTAR_PRODUCTOS = 0;
    private final int MODIFICAR_PRODUCTOS = 1;
    private final int CARGAR_COMBO = 2;
    private final int GUARDAR_RANGOS = 3;
    private final int LISTAR_RANGOS = 4;
    private final int LISTAR_HISTORICO = 5;

    private JsonObject respuesta;
    private ConfigProductosEDSDAO dao;
    private boolean ctrlRequest = true;

    @Override
    public void run() throws ServletException, InformationException {
        try {
            dao = new ConfigProductosEDSImpl(((Usuario) request.getSession().getAttribute("Usuario")).getBd());
            int opcion = (request.getParameter("opcion") != null)
                    ? Integer.parseInt(request.getParameter("opcion"))
                    : -1;
            switch (opcion) {
                case CONSULTAR_PRODUCTOS:
                    consultar();
                    break;
                case MODIFICAR_PRODUCTOS:
                    modificar();
                    break;
                case CARGAR_COMBO:
                    cargar_combo();
                    break;
                case GUARDAR_RANGOS:
                    guardarRangos();
                    break;
                case LISTAR_RANGOS:
                    listarRangos();
                    break;
                case LISTAR_HISTORICO:
                    listarHistrico();
                    break;
                default:
                    throw new Exception("La opcion de orden " + opcion + " no valida");
            }
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
        } finally {
            if (ctrlRequest) {
                try {
                    response.setContentType("application/json; charset=utf-8");
                    response.setHeader("Cache-Control", "no-cache");
                    response.getWriter().println((new Gson()).toJson(respuesta));
                } catch (Exception e) {
                }
            }
        }
    }

    private void consultar() {
        ctrlRequest = true;
        JsonObject info = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
        respuesta = dao.consultar_productos(info);
    }

    private void modificar() {
        ctrlRequest = true;
        JsonObject info = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
        info.addProperty("usuario", ((Usuario) request.getSession().getAttribute("Usuario")).getLogin());
        respuesta = dao.modificar_productos(info);
    }

    private void cargar_combo() {
        ctrlRequest = true;
        JsonObject info = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
        respuesta = dao.cargar_combo(info);
    }

    public void guardarRangos() {
        ctrlRequest = false;
        String json = "{\"respuesta\":\"Guardado:)\"}";
        try {
            JsonObject obj = (JsonObject) (new JsonParser()).parse(request.getParameter("info"));
            obj.addProperty("usuario", ((Usuario) request.getSession().getAttribute("Usuario")).getLogin());
            obj.addProperty("cia", ((Usuario) request.getSession().getAttribute("Usuario")).getCia());
            json = this.dao.guardarRangos(obj);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void listarRangos() {
        try {
            ctrlRequest = false;
            Gson gson = new Gson();
            String id_config = request.getParameter("id_config");
            String json = "{\"info\":" + gson.toJson(dao.listarRangos(id_config)) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(ConfigProductosEDSAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

//    public void guardarHistoricoRangos() {
//        ctrlRequest = false;
//        String json = "{\"respuesta\":\"Guardado:)\"}";
//        try {
//            JsonObject obj = (JsonObject) (new JsonParser()).parse(request.getParameter("info"));
//            obj.addProperty("usuario", ((Usuario) request.getSession().getAttribute("Usuario")).getLogin());
//            json = this.dao.guardarHistoricoRangos(obj);
//        } catch (Exception e) {
//            e.printStackTrace();
//            json = "{\"respuesta\":\"ERROR:)\"}";
//        } finally {
//            try {
//                this.printlnResponse(json, "application/json;");
//            } catch (Exception ex) {
//                ex.printStackTrace();
//            }
//        }
//    }
    
    public void listarHistrico() {
        try {
            ctrlRequest = false;
            Gson gson = new Gson();
            String id_config = request.getParameter("id_config");
            String json = "{\"rows\":" + gson.toJson(dao.listarHistrico(id_config)) + "}";
            this.printlnResponse(json , "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(ConfigProductosEDSAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    /**
     * Escribe la respuesta de una opcion ejecutada desde AJAX
     *
     * @param respuesta
     * @param contentType
     * @throws Exception
     */
    private void printlnResponse(String respuesta, String contentType) throws Exception {

        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);

    }
}
