/*******************************************************************
 * Nombre clase: OCargaSearchAction.java
 * Descripci�n: Accion para realizar la buscqueda de una orden de carga
 * Autor: Ing. Karen Reales
 * Fecha: 12 de mayo 2006
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class OCargaSearchAction extends Action{
    static Logger logger = Logger.getLogger(OCargaValidarAction.class);
    
    public OCargaSearchAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        /**
         *
         * Usuario en Sesion
         */
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        /**
         *
         * Inicializacion Variable tipo String
         */
        String next="/jsp/masivo/ordencargue/Modificar.jsp";
        String orden = request.getParameter("orden");
        logger.info("orden "+orden);
        try{
            if(request.getParameter("anular")!=null){
                next= "/jsp/masivo/ordencargue/Anular.jsp";
            }
            model.imprimirOrdenService.buscarOrdenDeCarga(usuario.getDstrct(), orden);
            if(model.imprimirOrdenService.getHojaOrden()==null){
                next="/jsp/masivo/ordencargue/InicioModificar.jsp?mensaje=La orden de carga no existe o esta anulada";
                if(request.getParameter("anular")!=null){
                    next= "/jsp/masivo/ordencargue/InicioAnular.jsp?mensaje=La orden de carga no existe o esta anulada";
                }
            }
            else{
                if(request.getParameter("anular")!=null){
                    HojaOrdenDeCarga oc = model.imprimirOrdenService.getHojaOrden();
                    String remitente =model.remidestService.getNombre(oc.getRemitente());
                    String conductor =model.nitService.obtenerNombre(oc.getConductor());
                    oc.setRemitente(remitente);
                    oc.setConductor(conductor);
                    model.imprimirOrdenService.setHoc(oc);
                }
            }
            
            logger.info("El next= "+next);
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
