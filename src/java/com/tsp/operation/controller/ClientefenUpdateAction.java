/***********************************************
 * Nombre clase: PlacaInsertAction.java
 * Descripci�n: Accion para ingresar una placa a la bd.
 * Autor: AMENDEZ
 * Fecha: 4 de noviembre de 2004, 02:48 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A.
 **********************************************/

package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonElement;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import java.lang.*;
import com.tsp.operation.model.threads.*;
import java.util.logging.Level;
import java.util.logging.Logger;
public class ClientefenUpdateAction extends Action{

    public ClientefenUpdateAction() {
    }

    public void run() throws ServletException, InformationException {
        Gson gson = new Gson();
        JsonObject Respuesta = new JsonObject();
        String next = "/jsp/fenalco/clientes/edit_clientes.jsp";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String op = request.getParameter("op");
        if (op.equals("1")) {
            String res = "";
            String res2 = "";
            BeanGeneral Cliente = new BeanGeneral();
            Cliente.setValor_01((request.getParameter("NomCliente") != null) ? request.getParameter("NomCliente") : "");//nomblecliente
            Cliente.setValor_02((request.getParameter("tipoid") != null) ? request.getParameter("tipoid") : "");//tippo doc
            Cliente.setValor_03((request.getParameter("Direccion") != null) ? request.getParameter("Direccion") : "");
            Cliente.setValor_04((request.getParameter("Telefono") != null) ? request.getParameter("Telefono") : "");
            Cliente.setValor_05((request.getParameter("c_dir") != null) ? request.getParameter("c_dir") : "");
            Cliente.setValor_06((request.getParameter("codc") != null) ? request.getParameter("codc") : "");
            Cliente.setValor_07((request.getParameter("Celular") != null) ? request.getParameter("Celular") : "");

            Cliente.setValor_08((request.getParameter("expcc") != null) ? request.getParameter("expcc") : ""); //expedicion de cc
            Cliente.setValor_09((request.getParameter("replegal") != null) ? request.getParameter("replegal") : "");//representante legal
            Cliente.setValor_10((request.getParameter("ccreplegal") != null) ? request.getParameter("ccreplegal") : "");//cc rep legal


            Cliente.setValor_12(usuario.getCedula());//nit del afiliado
            try {
                model.clienteService.UpdateClienteFen(Cliente);
                next = next + "?msg=Actualizacion Terminada con exito";
            } catch (Exception e) {
                next = next + "?msg=No se puede Actualizar el cliente ";
                e.printStackTrace();
            }
            Cliente = null;
        } else {
            if (op.equals("2")) {
                BeanGeneral bg = new BeanGeneral();
                bg.setValor_01((request.getParameter("codc") != null) ? request.getParameter("codc") : "");
                bg.setValor_02(usuario.getCedula());//nit del afiliado
                bg.setValor_03(usuario.getLogin());
                try {
                    model.clienteService.DelClienteFen(bg);
                    next = next + "?msg=Eliminacion Realizada con exito";
                } catch (Exception e) {
                    next = next + "?msg=No se puede Eliminar el cliente ";
                    e.printStackTrace();
                }
                bg = null;
            } else if (op.equals("3")) {

                try {
                    //buscar cliente.

                    String nit = (request.getParameter("nit") != null) ? request.getParameter("nit") : "";
                    System.err.println("esto es el nit:" + nit);

                    //metodo para buscar el cliente
                    BeanGeneral beanGeneral = model.clienteService.buscarClienteXnit(nit);
                    String resp = "";
                    if (beanGeneral != null) {

                        resp += beanGeneral.getValor_01() + ";";
                        resp += beanGeneral.getValor_02() + ";";
                        resp += beanGeneral.getValor_03() + ";";
                        resp += beanGeneral.getValor_04() + ";";
                        resp += beanGeneral.getValor_05() + ";";
                        resp += beanGeneral.getValor_06() + ";";
                        resp += beanGeneral.getValor_07() + ";";
                        resp += beanGeneral.getValor_08() + ";";
                        resp += beanGeneral.getValor_09() + ";";
                        resp += beanGeneral.getValor_55() + ";";
                        resp += beanGeneral.getValor_54() + ";";


                    } else {
                        resp = "N";
                    }
                    
                    escribirResponse(resp);

                } catch (SQLException ex) {
                    Logger.getLogger(ClientefenUpdateAction.class.getName()).log(Level.SEVERE, null, ex);
                } catch (Exception ex) {
                    Logger.getLogger(ClientefenUpdateAction.class.getName()).log(Level.SEVERE, null, ex);
                }

            }else if(op.equals("4")){
                
                //modificar cliente
                String nit = (request.getParameter("nit") != null) ? request.getParameter("nit") : "";
                String codigo = (request.getParameter("codigo") != null) ? request.getParameter("codigo") : "";
                String name = (request.getParameter("name") != null) ? request.getParameter("name") : "";
                String direccion = (request.getParameter("direccion") != null) ? request.getParameter("direccion") : "";
                String telefono = (request.getParameter("telefono") != null) ? request.getParameter("telefono") : "";

                String celular = (request.getParameter("celular") != null) ? request.getParameter("celular") : "";
                String barrio = (request.getParameter("barrio") != null) ? request.getParameter("barrio") : "";
                String codciu = (request.getParameter("codciu") != null) ? request.getParameter("codciu") : "";
                String cooddpto = (request.getParameter("cooddpto") != null) ? request.getParameter("cooddpto") : "";
                String email = (request.getParameter("email") != null) ? request.getParameter("email") : "";
                String observaciones = (request.getParameter("observaciones") != null) ? request.getParameter("observaciones") : "";
                String negocios = (request.getParameter("negocios") != null) ? request.getParameter("negocios") : "{}";
                boolean resp=false;
                JsonParser parser = new JsonParser();
                
                JsonObject gsonnegocio = parser.parse(negocios).getAsJsonObject();
                JsonArray Arraynegocio = gsonnegocio.getAsJsonArray("rows");
               
                 
               
                
                 if(!nit.equals("")){
                     try {
                         //actualizamos el cliente.
                             boolean status= model.clienteService.actualizarClienteXnit(nit, codigo, name,direccion,telefono,celular,barrio,codciu,cooddpto,email,observaciones, usuario);
                            
                            if (Arraynegocio != null ){  
                              for (JsonElement obj : Arraynegocio) {

                                JsonObject paymentObj = obj.getAsJsonObject();

                                String cod_neg  = paymentObj.get("cod_neg").getAsString();
                                String num_sol  = paymentObj.get("num_sol").getAsString();
                                String ext_email  = paymentObj.get("ext_email").getAsString();
                                String ext_corr  = paymentObj.get("ext_corr").getAsString();
                                model.clienteService.actualizarSolicitud(num_sol,ext_email,ext_corr,nit);
                
                               }
                            } 
                             if(status){
                             escribirResponse("S");
                             }else{
                             escribirResponse("N");
                             }
                    
                     } catch (SQLException ex) {
                         Logger.getLogger(ClientefenUpdateAction.class.getName()).log(Level.SEVERE, null, ex);
                     } catch (Exception ex) {
                         Logger.getLogger(ClientefenUpdateAction.class.getName()).log(Level.SEVERE, null, ex);
                     }
                     
                 
                 }
            }else if(op.equals("5")){
                
            String nit = (request.getParameter("nit") != null) ? request.getParameter("nit") : "";
           
            
                try {
                    this.printlnResponseAjax( model.clienteService.buscarNegocios(nit), "application/json;");
                } catch (Exception ex) {
                    Logger.getLogger(ClientefenUpdateAction.class.getName()).log(Level.SEVERE, null, ex);
                }
            }else if(op.equals("6")){
                
                //Llenado de datos
                String nit = (request.getParameter("nit") != null) ? request.getParameter("nit") : "";
                String codigo = (request.getParameter("codigo") != null) ? request.getParameter("codigo") : "";
                String name = (request.getParameter("name") != null) ? request.getParameter("name") : "";
                String direccion = (request.getParameter("direccion") != null) ? request.getParameter("direccion") : "";
                String telefono = (request.getParameter("telefono") != null) ? request.getParameter("telefono") : "";

                String celular = (request.getParameter("celular") != null) ? request.getParameter("celular") : "";
                String barrio = (request.getParameter("barrio") != null) ? request.getParameter("barrio") : "";
                String codciu = (request.getParameter("codciu") != null) ? request.getParameter("codciu") : "";
                String cooddpto = (request.getParameter("cooddpto") != null) ? request.getParameter("cooddpto") : "";
                String email = (request.getParameter("email") != null) ? request.getParameter("email") : "";
                String observaciones = (request.getParameter("observaciones") != null) ? request.getParameter("observaciones") : "";
                String negocios = (request.getParameter("negocios") != null) ? request.getParameter("negocios") : "{}";
                
                JsonParser parser = new JsonParser();
                
                JsonObject gsonnegocio = parser.parse(negocios).getAsJsonObject();
                JsonArray Arraynegocio = gsonnegocio.getAsJsonArray("rows");
               
                 
               
                
                 if(!nit.equals("")){
                     try {
                         
                         //validaciones
                         JsonObject json = model.clienteService.buscarEmail(email);
                         if(((json.get("existe").getAsBoolean()) && (json.get("nit").getAsString()).equals(nit)) || (!json.get("existe").getAsBoolean())){
                                //actualizamos el cliente.
                                boolean status= model.clienteService.actualizarClienteXnit(nit, codigo, name,direccion,telefono,celular,barrio,codciu,cooddpto,email,observaciones, usuario);

                                if (Arraynegocio != null ){  
                                  for (JsonElement obj : Arraynegocio) {

                                    JsonObject paymentObj = obj.getAsJsonObject();

                                    String cod_neg  = paymentObj.get("cod_neg").getAsString();
                                    String num_sol  = paymentObj.get("num_sol").getAsString();
                                    String ext_email  = paymentObj.get("ext_email").getAsString();
                                    String ext_corr  = paymentObj.get("ext_corr").getAsString();
                                    model.clienteService.actualizarSolicitud(num_sol,ext_email,ext_corr,nit);

                                   }
                                } 
                                Respuesta.addProperty("mensaje", "Usuario actualizado con exito");
                                 
                         }else{
                             Respuesta.addProperty("error", "Existio un problema el correo suministrado esta en uso por otro usuario");
                         }
                         
                         this.printlnResponseAjax( gson.toJson(Respuesta), "application/json;");
                        
                    
                     } catch (SQLException ex) {
                         Logger.getLogger(ClientefenUpdateAction.class.getName()).log(Level.SEVERE, null, ex);
                     } catch (Exception ex) {
                         Logger.getLogger(ClientefenUpdateAction.class.getName()).log(Level.SEVERE, null, ex);
                     }
                     
                 
                 }
            }
        }
        
        if(!op.equals("3") && !op.equals("5") && !op.equals("6")){
         
             this.dispatchRequest(next);
        
        }
       

    }
    
    /**
     * Escribe el resultado de la consulta en el response de Ajax
     * @param dato la cadena a escribir
     * @throws Exception cuando hay un error
     */
    protected void escribirResponse(String dato) throws Exception {
        try {
            response.setContentType("text/plain; charset=utf-8");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println(dato);
        } catch (Exception e) {
            throw new Exception("Error al escribir el response: " + e.toString());
        }
    }

}
