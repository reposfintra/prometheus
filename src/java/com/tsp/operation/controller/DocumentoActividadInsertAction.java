/********************************************************************
 *      Nombre Clase.................   DocumentoActividadInsertAction.java    
 *      Descripci�n..................   Obtiene las actividades de un documento.
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   19.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class DocumentoActividadInsertAction extends Action{
        
        /** Creates a new instance of DocumentoInsertAction */
        public DocumentoActividadInsertAction() {
        }
        
        public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
                
                //Usuario en sesi�n
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario) session.getAttribute("Usuario");
                String distrito = (String) session.getAttribute("Distrito");
                
                //Pr�xima vista
                String pag  = "/jsp/masivo/tbldoc_act/DocumentoActividadInsert.jsp";
                String next = "";                
                
                String[] doc = request.getParameterValues("c_docSelec");
                String[] acts = request.getParameterValues("c_actSelec");
                
                DocumentoActividad obj = new DocumentoActividad();
                obj.setDistrito(distrito);
                obj.setUsuario_creacion(usuario.getLogin());
                obj.setUsuario_modificacion(usuario.getLogin());
                obj.setBase(usuario.getBase());
                obj.setEstado("");
                

                try{           
                        Vector dacts = 
                                model.actividad_documentoSvc.obtenerActividadesDocumento(obj.getDistrito(), doc[0]);
                        
                        if( acts!=null){
                        
                                for(int k=0; k<dacts.size(); k++){
                                        boolean esta = false;
                                        DocumentoActividad dac = (DocumentoActividad) dacts.elementAt(k);

                                        for(int j=0; j<acts.length; j++){
                                                if( dac.getActividad().matches(acts[j]) )
                                                        esta = true;
                                        }

                                        if(!esta){
                                                dac.setEstado("A");
                                                model.actividad_documentoSvc.actualizarDocumentoActividad(dac);
                                        }
                                }

                                for(int i=0; i<acts.length; i++){
                                        obj.setDocumento(doc[0]);
                                        obj.setActividad(acts[i]);

                                        boolean existe = model.actividad_documentoSvc.existeActividadDocumento(obj.getDistrito(),
                                                obj.getDocumento(), obj.getActividad());

                                        if(!existe){
                                                model.actividad_documentoSvc.ingresarDocumentoActividad(obj);
                                        }
                                        else{
                                                DocumentoActividad dact = model.actividad_documentoSvc.obtenerActividadDocumento(
                                                        obj.getDistrito(), obj.getDocumento(), obj.getActividad());
                                                if( dact.getEstado().matches("A") )
                                                        model.actividad_documentoSvc.actualizarDocumentoActividad(obj);
                                        }
                                }
                                
                        }
                        else{
                                for(int k=0; k<dacts.size(); k++){
                                        DocumentoActividad dac = (DocumentoActividad) dacts.elementAt(k);
                                        dac.setEstado("A");
                                        model.actividad_documentoSvc.actualizarDocumentoActividad(dac);
                                }
                        }
                        
                        pag += "?msg=Actividades asignadas al documento modificadas exitosamente.";
                        next = com.tsp.util.Util.LLamarVentana(pag, "Asignaci�n de Actividades a Documento.");
                                

                }catch (SQLException e){
                       throw new ServletException(e.getMessage());
                }

                this.dispatchRequest(next);
        }
        
}
