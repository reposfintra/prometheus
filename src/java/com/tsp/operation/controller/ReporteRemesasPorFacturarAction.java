/********************************************************************************
 * Nombre clase :                 ReporteRemesasPorFacturarAction.java          *
 * Descripcion :                  Clase que maneja los eventos                  *
 *                                relacionados con el programa que busca el     *
 *                                reporte de las remesas por facturar en la BD. *
 * Autor :                        LREALES                                       *
 * Fecha :                        17 de Julio de 2006, 05:30 PM                 *
 * Version :                      1.0                                           *
 * Copyright :                    Fintravalores S.A.                       *
 *******************************************************************************/

package com.tsp.operation.controller;

import java.util.Vector;
import java.lang.*;
import java.sql.*;
import javax.servlet.ServletException;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.apache.log4j.*;

public class ReporteRemesasPorFacturarAction extends Action{
    
    Logger logger = Logger.getLogger(this.getClass());
    
    /** Creates a new instance of ReporteRemesasPorFacturarAction */
    public ReporteRemesasPorFacturarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String next = "/jsp/general/exportar/ReporteRemesasPorFacturar.jsp";
        
        HttpSession session = request.getSession();
        Usuario usuario = ( Usuario ) session.getAttribute( "Usuario" );
        
        String distrito = usuario.getDstrct();
        
        boolean ejecutar_hilo = false;
        
        String fini = ( request.getParameter ("fecini") != null )?request.getParameter ("fecini").toUpperCase():"";
        String ffin = ( request.getParameter ("fecfin") != null )?request.getParameter ("fecfin").toUpperCase():"";
        String aged = ( request.getParameter ("agencia_duenia") != null )?request.getParameter ("agencia_duenia").toUpperCase():"";        
        String agef = ( request.getParameter ("agencia_facturadora") != null )?request.getParameter ("agencia_facturadora").toUpperCase():"";
        String anual = ( request.getParameter ("anual") != null )?request.getParameter ("anual").toUpperCase():"";//AMATURANA 09.01.07
        String anio = ( request.getParameter ("anio") != null )?request.getParameter ("anio").toUpperCase():"";//AMATURANA 09.01.07
        
        logger.info( "?? anual: " + anual );
        logger.info( "?? anio: " + anio );
        
        try{
            
            if( anual.equals("NO") ){
                if ( fini.equals("") && !ffin.equals("") ) {
                    
                    next = next + "?msg=Defina la fecha de inicio de busqueda para poder continuar...";
                    
                } else if ( !fini.equals("") && ffin.equals("") ) {
                    
                    next = next + "?msg=Defina la fecha de fin de busqueda para poder continuar...";
                    
                } else if ( !fini.equals("") && !ffin.equals("") ) {
                    
                    String fecha1 = fini.replace( "-", "" );
                    String fecha2 = ffin.replace( "-", "" );
                    
                    int fech1 = Integer.parseInt( fecha1 );
                    int fech2 = Integer.parseInt( fecha2 );
                    
                    if ( fech2 < fech1 ) {
                        
                        next = next + "?msg=La fecha final debe ser mayor que la fecha inicial!!";
                        
                    } else {
                        
                        ejecutar_hilo = true;
                        
                    }
                    
                } else if ( fini.equals("") && ffin.equals("") ) {
                    
                    next = next + "?msg=Defina las fechas del reporte para poder realizar la busqueda...";
                    
                }
            } else {
                
                if ( anio.equals("") ) {
                    
                    next = next + "?msg=Defina el a�o del reporte para poder realizar la busqueda...";
                    
                } else {
                    
                     ejecutar_hilo = true;
                    
                }
                
            }
            
            if ( ejecutar_hilo ) {
                                
                aged = aged.equals("NADA")?"":aged;
                agef = agef.equals("NADA")?"":agef;
                
                HiloReporteRemesasPorFacturar HRRPF = new HiloReporteRemesasPorFacturar( usuario.getLogin(), distrito, fini, ffin, aged, agef, anual, anio, model );

                next = next + "?msg=Proceso iniciado exitosamente!";
                
            }
            
        } catch ( Exception e ) {
            
            e.printStackTrace();
            throw new ServletException( "Error en Reporte de Remesas Por Facturar Action : " + e.getMessage() );
            
        }        
        
        this.dispatchRequest( next );
        
    }
    
}