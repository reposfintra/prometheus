/*
 * TblEstadoInsert.java
 *
 * Created on 5 de octubre de 2005, 09:55 AM
 */

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;

/**
 *
 * @author  Andres
 */
public class TblEstadoAnularAction extends Action {
        
        /** Creates a new instance of TblEstadoInsert */
        public TblEstadoAnularAction() {
        }
        
        public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
                
                String next = "";
                String pag = "/jsp/masivo/tblestado/EstadoUpdate.jsp";
                
                String tipo = request.getParameter("c_tipo");
                String codestado = request.getParameter("c_codestado");    
                                
                try{
                        //TblEstado est = model.tbl_estadoSvc.obtenerEstado(tipo, codestado, "");
                        HttpSession session = request.getSession();
                        Usuario usuario = (Usuario) session.getAttribute("Usuario");
                        
                        model.tbl_estadoSvc.anularEstado(tipo, codestado, usuario.getLogin());
                        
                        pag+="?comentario=El c�digo de estado fue anulado exitosamente.&mensaje=MsgModificado&estado=Anulado";
                        
                        next = com.tsp.util.Util.LLamarVentana(pag, "Actualizar Estado");
                        
                }catch (SQLException e){
                       throw new ServletException(e.getMessage());
                }

                this.dispatchRequest(next);
                
        }
        
}
