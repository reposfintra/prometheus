   /***************************************
    * Nombre Clase ............. ConsultarSoftwareAction.java
    * Descripci�n  .. . . . . .  Maneja los eventos para la consulta de software
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  14/01/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...Transportes Sanchez Polo S.A.
    *******************************************/


package com.tsp.operation.controller;



import java.io.*;
import java.util.*;
import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class ConsultarSoftwareAction extends Action{
    
    
    
    /**
     * M�todos que ejecuta la accion
     * @autor.......fvillacob
     * @Exception... ServletException, InformationException 
     * @version.....1.0.     
     **/
    public void run() throws ServletException, InformationException {
                
        try{
            HttpSession session = request.getSession();
            Usuario     usuario = (Usuario)session.getAttribute("Usuario");
            String      user    = usuario.getLogin();      
            
            String next          = "/jsp/general/inventario/ConsultaSoftware.jsp?msj=";
            String msj           = "";
            String opcion        = request.getParameter("evento");
            
            if(opcion!=null){
                opcion = opcion.toUpperCase();
                if(opcion.equals("LOADLIST")){
                    String equipo = request.getParameter("equipo");
                    String sitio  = request.getParameter("sitio");
                    model.ConsultaSoft.setEquipo(equipo);
                    model.ConsultaSoft.setSitio (sitio);
                    model.ConsultaSoft.searchPrograms();
                }
                
                
                if(opcion.equals("SEARCH")){
                    String   id         = request.getParameter("programa") ;
                    Programa programa   = model.ConsultaSoft.getPrograma(id);
                    List listRev        = model.RevisionSoft.searchRevisionesPrograma(programa);
                    List listFile       = model.RevisionSoft.searchFilesPrograma(programa);                     
                    programa.setListRevisiones( listRev );
                    programa.setListFile      (listFile);  
                    model.RevisionSoft.setPrograma(programa);
                    request.setAttribute("programa",programa);
                    next          = "/jsp/general/inventario/ConsultarPrograma.jsp";                 
                }
                
                
                
                 if(opcion.equals("SEARCHFILE")){
                     next          = "/jsp/general/inventario/ConsultaArchivos.jsp"; 
                     String file = request.getParameter("file");
                     List lista = model.RevisionSoft.getPrograma().getListFile();
                     if(lista!=null){
                         for(int i=0;i<lista.size();i++){
                             InventarioSoftware  inv =(InventarioSoftware )lista.get(i);
                             if(inv.getId()== Integer.parseInt(file) ){
                                 request.setAttribute("fileInventario",inv); 
                                 break;
                             }
                         }
                     }
                 }
                
                
                if(opcion.equals("RESET")){
                    model.ConsultaSoft.reset();
                }
                
                
            }else
                model.ConsultaSoft.reset();
      
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new Exception("No se pudo encontrar "+ next);
            rd.forward(request, response);            
        } catch (Exception e){
             throw new ServletException(e.getMessage());
        }
        
    }
    
}
