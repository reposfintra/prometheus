/*
 * TblEstadoInsert.java
 *
 * Created on 5 de octubre de 2005, 09:55 AM
 */

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;

/**
 *
 * @author  Andres
 */
public class TblEstadoUpdateAction extends Action {
        
        /** Creates a new instance of TblEstadoInsert */
        public TblEstadoUpdateAction() {
        }
        
        public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
                
                String next = "";
                String pag = "/jsp/masivo/tblestado/EstadoUpdate.jsp";
                
                String tipo = request.getParameter("c_tipo");
                String codestado = request.getParameter("c_codestado");    
                
                TblEstado estado = new TblEstado();
                estado.setCodestado(request.getParameter("c_ncodestado"));
                estado.setColor(request.getParameter("c_color"));
                estado.setColor_letra(request.getParameter("c_color_letra"));
                estado.setDescripcion(request.getParameter("c_descripcion"));
                estado.setFondo(request.getParameter("c_fondo"));
                estado.setTipo(request.getParameter("c_ntipo"));      
                estado.setFilename(request.getParameter("c_filename"));
                
                try{
                        //TblEstado est = model.tbl_estadoSvc.obtenerEstado(tipo, codestado, "");
                        HttpSession session = request.getSession();
                        Usuario usuario = (Usuario) session.getAttribute("Usuario");
                        estado.setUsuario_modificacion(usuario.getLogin());
                        
                        if ( !estado.getTipo().matches(tipo) || !estado.getCodestado().matches(codestado) ){
                                if( !model.tbl_estadoSvc.existeEstado(estado.getTipo(), estado.getCodestado()) ){
                                        model.tbl_estadoSvc.updateEstado(estado, tipo, codestado);
                                        pag+="?comentario=El c�digo de estado fue modificado exitosamente.&mensaje=MsgModificado";
                                }
                                else{
                                        pag+="?comentario=El c�digo de estado ya existe.";
                                }
                        }
                        else{
                                model.tbl_estadoSvc.updateEstado(estado, tipo, codestado);
                                pag+="?comentario=El c�digo de estado fue modificado exitosamente.&mensaje=MsgModificado";
                        }
                        
                        session.setAttribute("estado", estado);
                        next = com.tsp.util.Util.LLamarVentana(pag, "Actualizar Estado");
                        
                }catch (SQLException e){
                       throw new ServletException(e.getMessage());
                }

                this.dispatchRequest(next);
                
        }
        
}
