/*******************************************************************
 * Nombre clase: Flota_directaSearchAction.java
 * Descripci�n: Accion para buscar una flota directa a la bd.
 * Autor: Ing. Jose de la rosa
 * Fecha: 2 de diciembre de 2005, 03:06 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

public class Flota_directaSearchAction extends Action{
    
    /** Creates a new instance of Flota_directaSearchAction */
    public Flota_directaSearchAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next="";
        String nit = "";
        String placa = "";
        HttpSession session = request.getSession ();
        String listar = (String) request.getParameter ("listar");
        try{
            if (listar.equals ("True")){
                next="/jsp/masivo/flota_directa/flota_directaListar.jsp";
                String sw = (String) request.getParameter ("sw");
                if(sw.equals ("True")){
                    nit = (String) request.getParameter ("c_nit").toUpperCase ();
                    placa = (String) request.getParameter ("c_placa").toUpperCase ();
                }
                else{
                    nit = "";
                    placa = "";
                }
                session.setAttribute ("nit", nit);
                session.setAttribute ("placa", placa);
            }
            else{
                nit = (String) request.getParameter ("c_nit").toUpperCase ();
                placa = (String) request.getParameter ("c_placa").toUpperCase ();
                model.flota_directaService.searchFlota_directa (nit, placa);
                model.flota_directaService.getFlota_directa ();
                next="/jsp/masivo/flota_directa/flota_directaModificar.jsp";
            }
        }catch (SQLException e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
        
    }
    
}
