/********************************************************************
 *      Nombre Clase.................   ProveedorUpdateActualizarAction.java
 *      Descripci�n..................   Actualiza la vista ProveedorUpdate.jsp
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   30.09.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

/**
 *
 * @author  Andres
 */
public class ProveedorUpdateActualizarAction extends Action {

    /** Creates a new instance of ProveedorUpdateIngresarAction */
    public ProveedorUpdateActualizarAction() {
    }

    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        //Pr�xima vista
        String next  = "/jsp/cxpagar/proveedor/ProveedorUpdate.jsp?mensaje=";
        HttpSession session = request.getSession();

        Proveedor prov = new Proveedor();
        prov.setC_agency_id(( request.getParameter("c_agency_id") ==null ) ? "" : request.getParameter("c_agency_id") );
        prov.setC_autoretenedor_iva( ( request.getParameter("c_autoretenedor_iva") ==null )? "N" : request.getParameter("c_autoretenedor_iva")) ;
        prov.setC_agente_retenedor(( request.getParameter("c_agente_retenedor") ==null ) ? "N" : request.getParameter("c_agente_retenedor"));
        prov.setC_autoretenedor_ica( ( request.getParameter("c_autoretenedor_ica") ==null ) ? "N" : request.getParameter("c_autoretenedor_ica"));
        prov.setC_autoretenedor_rfte( ( request.getParameter("c_autoretenedor_rfte") ==null  ) ? "N" : request.getParameter("c_autoretenedor_rfte"));
        prov.setC_banco_transfer( ( request.getParameter("c_banco_transfer")==null  ) ? "" : request.getParameter("c_banco_transfer"));
        prov.setC_branch_code( ( request.getParameter("c_branch_code")==null  ) ? "" : request.getParameter("c_branch_code"));
        prov.setC_clasificacion( ( request.getParameter("c_clasificacion")==null  ) ? "" : request.getParameter("c_clasificacion"));
        prov.setC_codciudad_cuenta( ( request.getParameter("c_codciudad_cuenta")==null  ) ? "" : request.getParameter("c_codciudad_cuenta"));
        prov.setC_gran_contribuyente( (  request.getParameter("c_gran_contribuyente")==null ) ? "" : request.getParameter("c_gran_contribuyente"));
        prov.setC_idMims( ( request.getParameter("c_idMims") ==null ) ? "" : request.getParameter("c_idMims"));
        prov.setC_nit( (  request.getParameter("c_nit")==null ) ? "" : request.getParameter("c_nit"));
        prov.setC_payment_name( (  request.getParameter("c_payment_name")==null ) ? "" : request.getParameter("c_payment_name").toUpperCase());
        prov.setC_numero_cuenta( (request.getParameter("c_numero_cuenta") ==null  ) ? "" : request.getParameter("c_numero_cuenta") );
        prov.setC_sucursal_transfer( (  request.getParameter("c_sucursal_transfer")==null ) ? "" : request.getParameter("c_sucursal_transfer"));
        prov.setC_tipo_cuenta( ( request.getParameter("c_tipo_cuenta") ==null ) ? "" : request.getParameter("c_tipo_cuenta"));
        prov.setC_tipo_doc( (  request.getParameter("c_tipo_doc")==null ) ? "" :request.getParameter("c_tipo_doc"));//(  ) ? "" :
        prov.setHandle_code((request.getParameter("handle_code")==null)?"":request.getParameter("handle_code"));
        prov.setPlazo(Integer.parseInt( (request.getParameter("plazo")==null)?"":request.getParameter("plazo") ));
        //nuevo 18-03-2006
        prov.setCedula_cuenta( (request.getParameter("cedula_cuenta")==null)?"":request.getParameter("cedula_cuenta").toUpperCase() );
        prov.setNombre_cuenta( (request.getParameter("nombre_cuenta")==null)?"":request.getParameter("nombre_cuenta").toUpperCase() );
        prov.setTipo_pago( (request.getParameter("tipo_pago")==null)?"":request.getParameter("tipo_pago") );
        //jose 2006-09-28
        prov.setNit_beneficiario ( (request.getParameter ("nit_ben")==null)?"":request.getParameter ("nit_ben") );
        prov.setNom_beneficiario ( (request.getParameter ("nom_ben")==null)?"":request.getParameter ("nom_ben") );
         //01-02-2007
        prov.setConcept_code( request.getParameter("concept_code")==null?"":request.getParameter("concept_code") );
        prov.setCmc(request.getParameter("handle_code")==null?"":request.getParameter("handle_code"));
        prov.setAprobado(request.getParameter("aprobado")==null?"N":request.getParameter("aprobado"));
        prov.setRegimen(request.getParameter("regimen")==null?"":request.getParameter("regimen"));
        prov.setCodfen((request.getParameter("codigo")==null?"":request.getParameter("codigo"))+(request.getParameter("codigo1")==null?"":request.getParameter("codigo1"))+(request.getParameter("codigo2")==null?"":request.getParameter("codigo2")));

        session.setAttribute ("proveedor", prov);

        model.proveedorService.setProveedor(prov);
        String cia = (String) session.getAttribute("Distrito");

        String clas_iden = request.getParameter("cla_iden");
        char[] tipo = clas_iden.toCharArray();

        try{
            String agencia = prov.getC_agency_id().equals("OP")?"":prov.getC_agency_id();
            if( request.getParameter("agc")!=null ){
                model.servicioBanco.loadBancos(agencia, cia);
                model.servicioBanco.setSucursal(new TreeMap());
                if( prov.getC_branch_code().length()!=0){
                    model.servicioBanco.loadSucursalesAgencia(agencia, prov.getC_branch_code(), cia);
                }
            } else if( prov.getC_branch_code().length()!=0){
                model.servicioBanco.loadSucursalesAgencia(agencia, prov.getC_branch_code(), cia);
            } else {
                model.servicioBanco.setSucursal(new TreeMap());
            }

             //modificacion 2006-11-10
            if(tipo[4]=='A'){
                prov.setHandle_code("FC");
            }

            //modificacion 2006-02-17 Diogenes
            model.ciudadService.buscarCiudad(prov.getC_agency_id());
            Ciudad ciu = model.ciudadService.obtenerCiudad();
            model.tablaGenService.buscarBanco();
            model.tblgensvc.buscarListaSinDato ("TCUENTA", "PROVEEDOR");
            if(ciu.getPais().equals("VE")){
                prov.setC_agente_retenedor("S");//ojo nuevo
                prov.setC_gran_contribuyente("S");
                prov.setC_autoretenedor_iva("N");
                prov.setC_autoretenedor_ica("N");
                prov.setC_autoretenedor_rfte("N");
                if(tipo[4]=='0'){
                    prov.setHandle_code("02");
                }
            }else{
                prov.setC_agente_retenedor("N");//ojo nuevo
                prov.setC_gran_contribuyente("N");
                prov.setC_autoretenedor_iva("S");
                prov.setC_autoretenedor_ica("S");
                prov.setC_autoretenedor_rfte("S");
                if(tipo[4]=='0'){
                    prov.setHandle_code("01");
                }
            }
            next=next+"&sw="+ciu.getPais();
            //*******
            prov.setC_branch_code("");
            prov.setC_bank_account("");
        }catch (SQLException e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }

        this.dispatchRequest(next);
    }

}
