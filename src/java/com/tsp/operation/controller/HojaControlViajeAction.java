/*****************************************************************************
 * Nombre clase :                   HojaControlViajeAction.java              *
 * Descripcion :                    Clase que maneja los eventos             *
 *                                  relacionados con el programa de          *
 *                                  Impresion de Hoja de Control de Viaje    *
 * Autor :                          Ing. Juan Manuel Escandon Perez          *
 * Fecha :                          29 de diciembre de 2005, 03:06 PM        *
 * Version :                        1.0                                      *
 * Copyright :                      Fintravalores S.A.                  *
 *****************************************************************************/
package com.tsp.operation.controller;


import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import com.tsp.pdf.*;
import java.io.*;

public class HojaControlViajeAction extends Action{
    
    /** Creates a new instance of HojaControlViajeAction */
    public HojaControlViajeAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        try{
            
            String next = "";
            String numpla                 = (request.getParameter("numpla")!=null)?request.getParameter("numpla"):"";
            HttpSession session = request.getSession();
            
            if( model.planillaService.existPlanilla(numpla) ){
                model.hojaControlViajeSvc.Impresion(numpla);
                HojaControlViaje hvc = model.hojaControlViajeSvc.getDato();
                
                model.tablaGenService.DatosTablasGenerales("EC", hvc.getClase(), "PLACA");
                Vector vec = model.tablaGenService.getVectorTablasGenerales();
                
                if( vec.size() > 0 && vec != null ){
                    TreeMap tm = (TreeMap)vec.elementAt(0);
                    hvc.setTipo_trailer((String)tm.get("descripcion"));
                }
                else{
                    hvc.setTipo_trailer("");
                }
                
                model.hojaControlViajeSvc.setDato(hvc);
                /* CREACION DE ARCHIVOS XSLT(ARBOL) Y PDF */
                String ruta = application.getRealPath("/");
                File xslt = new File(ruta + "Templates/hvc2.xsl");
                File pdf = new File(ruta + "pdf/hvc.pdf");
                
                session.setAttribute("ArchivoXSLT", xslt );
                session.setAttribute("ArchivoPDF", pdf );
                session.setAttribute("Planilla", numpla);
                
                next = "/jsp/masivo/control_viaje/hvc.jsp";
                
            }
            else{
                String Mensaje = "La Planilla no existe";
                next = "/jsp/masivo/control_viaje/buscarplanilla.jsp?Mensaje="+Mensaje;
            }
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en HojaControlViajeAction .....\n"+e.getMessage());
        }
    }
    
}
