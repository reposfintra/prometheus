/************************************************************************
 * Nombre Discrepancia_productoUpdateAction.java
 * Descripci�n: Accion para actulizar un producto de discrepancia.
 * Autor: Jose de la rosa
 * Fecha: 14 de octubre de 2005, 09:53 AM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 **************************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;

public class Discrepancia_productoUpdateAction extends Action {
    
    /** Creates a new instance of Discrepancia_productoUpdateAction */
    public Discrepancia_productoUpdateAction () {
    }
    
    public void run () throws ServletException, com.tsp.exceptions.InformationException {
        String nom_doc = request.getParameter ("nom_doc");
        String next="/jsp/cumplidos/discrepancia/DiscrepanciaModificar.jsp?reload=ok&nom_doc="+nom_doc;
        HttpSession session = request.getSession ();
        int num_discre = Integer.parseInt (request.getParameter ("c_num_discre"));
        String numpla = request.getParameter ("c_numpla");
        String numrem = request.getParameter ("c_numrem");
        String tipo_doc = request.getParameter ("c_tipo_doc");
        String documento = request.getParameter ("c_documento");
        String cod_producto = request.getParameter ("c_cod_producto").toUpperCase ();
        String descripcion = request.getParameter ("c_descripcion").toUpperCase ();
        String unidad = request.getParameter ("c_unidad");
        double cantidad = Double.parseDouble (request.getParameter ("c_cantidad"));
        double valor = Double.parseDouble (request.getParameter ("c_valor"));
        String cod_discrepancia = request.getParameter ("c_cod_discrepancia");
        String fecha_creacion = request.getParameter ("c_fecha_creacion");
        String tipo_doc_rel = (String) request.getParameter ("c_tipo_doc_rel");
        String documento_rel = (String) request.getParameter ("c_documento_rel");
        String causa = request.getParameter ("c_causa");
        String clientes = request.getParameter ("client");
        String x = request.getParameter ("x");
        String responsable = request.getParameter ("c_responsable");
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String distrito = (String) session.getAttribute ("Distrito");
        try {
            Discrepancia dis = new Discrepancia ();
            Discrepancia d = (Discrepancia) model.discrepanciaService.getItemsDiscrepancia ().get ( Integer.parseInt (x) );
            dis.setNro_discrepancia ( d.getNro_discrepancia () );
            dis.setNro_planilla (numpla);
            dis.setNro_remesa (numrem);
            dis.setTipo_doc (tipo_doc);
            dis.setTipo_doc_rel (tipo_doc_rel);
            dis.setDocumento_rel (documento_rel);
            dis.setDocumento (documento);
            dis.setCod_producto (cod_producto);
            dis.setDescripcion (descripcion);
            dis.setUnidad (unidad);
            dis.setFecha_creacion (fecha_creacion);
            dis.setCantidad (cantidad);
            dis.setCod_discrepancia (cod_discrepancia);
            dis.setCausa_dev (causa);
            dis.setGrupo ( d.getGrupo () );
            dis.setUbicacion ( d.getUbicacion () );
            dis.setCod_Cliente ( d.getCod_Cliente () );
            dis.setContacto ( d.getContacto () );
            dis.setValor (valor);
            dis.setResponsable (responsable);
            dis.setUsuario_modificacion (usuario.getLogin ());
            dis.setUsuario_creacion (usuario.getLogin ());
            
            model.discrepanciaService.setDiscrepancia (dis);
            model.discrepanciaService.addItemsObjetoDiscrepancia( Integer.parseInt (x), dis );
            //model.discrepanciaService.updateDiscrepanciaProducto (dis);
            request.setAttribute ("msg", "Producto Modificado");
            
        }catch (Exception e) {
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
}
