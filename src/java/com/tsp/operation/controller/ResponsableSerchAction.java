/*
 * ResponsableSerchAction.java
 *
 * Created on 28 de octubre de 2005, 04:47 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

/**
 *
 * @author  Jose
 */
public class ResponsableSerchAction extends Action{
    
    /** Creates a new instance of ResponsableSerchAction */
    public ResponsableSerchAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="";
        String codigo = "";
        String descripcion = "";
        HttpSession session = request.getSession();
        String listar = (String) request.getParameter("listar");
        try{
            if (listar.equals("True")){
                next="/jsp/trafico/responsable/responsableListar.jsp";
                next = Util.LLamarVentana(next, "Listar Responsable");
                String sw = (String) request.getParameter("sw");
                if(sw.equals("True")){
                    codigo = (String) request.getParameter("c_codigo").toUpperCase();
                    descripcion = (String) request.getParameter("c_descripcion");
                }
                else{
                    codigo = "";
                    descripcion = "";
                }
                session.setAttribute("codigo", codigo);
                session.setAttribute("descripcion", descripcion);
            }
            else{
                codigo = (String) request.getParameter("c_codigo").toUpperCase();
                model.responsableService.serchResponsable(codigo);
                model.responsableService.getResponsable();
                next="/jsp/trafico/responsable/responsableModificar.jsp";
                next = Util.LLamarVentana(next, "Listar Responsable");
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
