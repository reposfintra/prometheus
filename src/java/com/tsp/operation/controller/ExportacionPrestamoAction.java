/**
 * Nombre        ExportacionPresupuestoAction.java
 * Descripci�n   Opciones de Exportacion de presupuesto
 * Autor         Mario Fontalvo Solano
 * Fecha         26 de marzo de 2006, 12:26 PM
 * Version       1.0
 * Coyright      Transportes S�nchez Polo S.A.
 **/

package com.tsp.operation.controller;


import com.tsp.operation.model.threads.HExportacionesPrestamos;
import com.tsp.operation.model.beans.Usuario;
import javax.servlet.*;
import javax.servlet.http.*;

public class ExportacionPrestamoAction extends  Action {
    
    /** Crea una nueva instancia de  ExportacionPresupuestoAction */
    public ExportacionPrestamoAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        try{
            
            String      msg     = "";
            HttpSession session = request.getSession();
            Usuario     usuario = (Usuario) session.getAttribute("Usuario");
            String      tipo = defaultString("tipo","");
            
            HExportacionesPrestamos h = new HExportacionesPrestamos();
            h.start(tipo, model, usuario);
            if ( h.runMain() ){
                String url = "<a href='"+ request.getContextPath() + h.getNombre() + "'>aqu�</a>";
                msg = "Archivo generado con exito, haga click "+ url +" para descargarlo.";
            }
            else
                msg = "No se pudo generar el archivo correctamente";
            request.setAttribute("msg"   , msg);
            String next = "/jsp/cxpagar/prestamos/descargar.jsp";            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if (rd==null)
                throw new Exception ("No se pudo encontrar " + next);
            rd.forward(request, response);
            
            
        }catch (Exception ex){
            throw new ServletException ("Error en ....\n"  + ex.getMessage());
        }
    }

    
    /**
     * Funcion para obtener un parametro del objeto request
     * y en caso de no existir este devuelve el segundo parametro
     * @autor mfontalvo
     * @param name, nombre del parametro
     * @param opcion, valor opcional a devolver en caso de que nom exista
     * @return Parametro del request
     */
    private String defaultString(String name, String opcion){
        return (request.getParameter(name)==null?opcion:request.getParameter(name));
    }
        
}
