/********************************************************************
 *      Nombre Clase.................   Campos_jspIngresarAction.java    
 *      Descripci�n..................   Almacena un arreglo de campos y los ingresa en el 
 *                                      respectivo archivo.
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   25.11.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  rodrigo
 */
public class Campos_jspIngresarAction extends Action{
    
     public Campos_jspIngresarAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException {
        //get data
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");    
        String next="/jsp/trafico/permisos/campos_jsp/Campos_jspListed.jsp";
        
        String pag = request.getParameter("cod_jsp");
        String[] campos = request.getParameterValues("campos");
        Vector fields = new Vector();
        try{
            if ( campos != null ){
                for( int i=0; i<campos.length; i++){
                    String[] fld = campos[i].split("~");
                    campos_jsp c = new campos_jsp();
                    c.setCampo(fld[0]);
                    c.setTipo_campo(fld[1]);
                    c.setPagina(pag);
                    c.setCreation_user(usuario.getLogin());
                    c.setUser_update(usuario.getLogin());
                    c.setCia((String) session.getAttribute("Distrito"));
                    //System.out.println(".....................CAMPO" + c.getCampo() + ", EXISTE: " + (model.camposjsp.existcampos_jsp(pag, fld[0])));
                    if( !model.camposjsp.existcampos_jsp(pag, fld[0]) ){
                        model.camposjsp.insertcampos_jsp(c);
                    }
                }                              
            }   
            
            model.jspService.serchJsp(pag);
            Jsp jsp = model.jspService.getJsp();
            fields = model.camposjsp.searchDetallecampos_jsps(pag);
            session.setAttribute("fields", fields);
            session.setAttribute("pag", jsp.getRuta() + jsp.getNombre());
            session.setAttribute("cod_jsp", pag);
            
            //System.out.println("------------> Campos_jspIngresarAction");
            //System.out.println("------------> Pagina: " + pag );
            //System.out.println("------------> Campos: " + fields.size() );
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        
        }
        this.dispatchRequest(next);
    }
}//end class
