    /*******************************************************************
 * Nombre clase: Descuento_claseInsertAction.java
 * Descripción: Accion para ingresar un descuento por clase equipo a la bd.
 * Autor: Ing. Jose de la rosa
 * Fecha: 7 de diciembre de 2005, 02:23 PM
 * Versión: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class Descuento_claseInsertAction extends Action{
    
    /** Creates a new instance of Descuento_claseInsertAction */
    public Descuento_claseInsertAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next = "/jsp/equipos/descuento_clase/descuento_claseInsertar.jsp";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");   
        String dstrct = (String) session.getAttribute("Distrito");
        String codigo = request.getParameter("c_codigo").toUpperCase ();
        String mes = request.getParameter("c_mes");
        String ano = request.getParameter("c_ano");
        mes = ano+"-"+mes;
        String frecuencia = request.getParameter("c_frecuencia");
        double valor = Double.parseDouble ( !request.getParameter("c_valor").equals ("")? request.getParameter("c_valor").replaceAll (",","") : "0"  );
        String clase = request.getParameter("c_clase").toUpperCase ();
        float porcentaje_descuento = Float.parseFloat ( !request.getParameter("c_porcentaje").equals ("") ? request.getParameter("c_porcentaje").replaceAll (",","") : "0" );
        int sw=0;
        try{
            Descuento_clase descuento = new Descuento_clase();
            descuento.setClase_equipo (clase);
            descuento.setValor_mes (valor);
            descuento.setMes_proceso (mes);
            descuento.setFrecuencia (frecuencia);
            descuento.setCodigo_concepto (codigo);
            descuento.setPorcentaje_descuento (porcentaje_descuento);
            descuento.setUsuario_modificacion(usuario.getLogin().toUpperCase());
            descuento.setUsuario_creacion(usuario.getLogin().toUpperCase());
            descuento.setDistrito (dstrct);
            descuento.setBase(usuario.getBase().toUpperCase());
            try{
                model.descuento_claseService.insertDescuento_clase (descuento);
            }catch(SQLException e){
                sw=1;
            }
            if(sw==1){
                if(!model.descuento_claseService.existDescuento_clase (clase, dstrct,codigo)){
                    model.descuento_claseService.updateDescuento_clase (descuento);
                    request.setAttribute("mensaje","La información ha sido ingresada exitosamente!");
                }
                else{
                    request.setAttribute("mensaje","Error la información ya existe!");
                }
            }
            else{
                request.setAttribute("mensaje","La información ha sido ingresada exitosamente!");
            }
        }catch(SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);           
    }
    
}
