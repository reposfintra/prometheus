/*
 * PvuSerchAction.java
 *
 * Created on 19 de julio de 2005, 15:45
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.Util;


/**
 *
 * @author  Rodrigo
 */
public class PvuSerchAction extends Action{
    
    /** Creates a new instance of PvuSerchAction */
    public PvuSerchAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="";
        String pag = "";
        HttpSession session = request.getSession();        
        String listar = (String) request.getParameter("listar");
        String codigo = (String) request.getParameter("c_codigo");
        String pagina = (String) request.getParameter("c_pagina"); 
        try{                
            if (listar.equals("True")){                
                next="/jsp/trafico/permisos/jsp/JspListar.jsp";                    
                Vector jsps = model.perfilvistausService.searchDetallePerfilVistaUsuarios(codigo);               
                request.setAttribute("pvus",jsps);
                //next = Util.LLamarVentana(pag, "");
            }
            else{
                model.perfilvistausService.searchDetallePerfilVistaUsuarios(codigo);
                PerfilVistaUsuario pvu = model.perfilvistausService.getPerfilVistaUsuario();
                model.perfilvistausService.setPerfilVistaUsuario(pvu);
                //request.setAttribute("pvu",pvu);                
                pag="/jsp/trafico/permisos/perfil_vista_usuario/PvuModificar.jsp";
                next = pag;               
            }                        
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);    
    }
    
}
