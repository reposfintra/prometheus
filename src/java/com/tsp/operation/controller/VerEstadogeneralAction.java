/*
 * VerEstadogeneralAction.java
 *
 * Created on 4 de marzo de 2005, 06:30 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;

/**
 *
 * @author  DIBASMO
 */
public class VerEstadogeneralAction extends Action {
    
    /** Creates a new instance of VerEstadogeneralAction */
    public VerEstadogeneralAction() {
    }
    
    public void run() throws javax.servlet.ServletException {
        String next="/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina")+"?mensaje=general";
        HttpSession session = request.getSession();
      //  String lenguaje = (String) session.getAttribute("idioma");
      //  try{ 
      //      model.idiomaService.cargarIdioma(lenguaje,request.getParameter("pagina"));
      //      Properties pro = model.idiomaService.getIdioma();
      //  }catch (SQLException e){
      //      throw new ServletException(e.getMessage());
      //  }
        this.dispatchRequest(next);
    }
    
}
