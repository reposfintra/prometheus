/*
 * AcpmDeleteAction.java
 *
 * Created on 2 de diciembre de 2004, 11:30 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class AutorizarCambioPlacaPropAction extends Action{
    
    /** Creates a new instance of AcpmDeleteAction */
    public AutorizarCambioPlacaPropAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next ="/jsp/hvida/identidad/identidadAutorizacion.jsp";
        String numsol =request.getParameter("numsol");
        String despachador = request.getParameter("despachador");
        String clave= "NO SE PUDO GENERAR";
        String email = request.getParameter("email");
        String texto1="";
        String texto2="";
        String body ="";
        
        Email e = new Email();
        if(email!=null){
            e.setEmailfrom("autorizaciones@sanchezpolo.com");
            
            e.setSenderName("SISTEMA MASIVO");
            e.setEmailto(email);
            e.setEmailcopyto("");
        }
        if(request.getParameter("nit")!=null){
            e.setEmailsubject("CREACION DE PROPIETARIO AUTORIZADO");
            String nit =request.getParameter("nit");
            String nombre =request.getParameter("nombre");
            texto1=nit;
            texto2=numsol+nombre;
            //System.out.println("Texto 1: "+texto1);
            //System.out.println("Texto 2: "+texto2);
            body = "CREACION DE PROPIETARIO AUTORIZADA\n" +
            "\nNIT    : "+nit+"" +
            "\nNOMBRE : "+nombre+"" +
            "\nDESPACHADOR : "+despachador+"" +
            "\nSOLICITUD NUMERO : " +numsol+
            "\n\n\nCLAVE: ";
            next = "/jsp/hvida/identidad/identidadAutorizacion.jsp?clave=";
            
            
        }
        else if(request.getParameter("VProp")!=null) {
            e.setEmailsubject("MODIFICACION DE PROPIETARIO AUTORIZADO");
            String placa = request.getParameter("placa");
            String nitVProp =request.getParameter("VProp");
            String nitNProp =request.getParameter("NProp");
            texto1 = placa + nitVProp.substring(0,nitVProp.length()/2);
            texto2 = numsol+nitNProp + nitVProp.substring(nitVProp.length()/2,nitVProp.length());
            body = "MODIFICACION DE PLACA - PROPIETARIO\n" +
            "\nPLACA    : "+placa+"" +
            "\nNUEVO PROPIETARIO: "+nitNProp+" - " +request.getParameter("nombreNProp")+
            "\nPROPIETARIO ACTUAL : "+nitVProp+" - " +request.getParameter("nombreVProp")+
            "\nDESPACHADOR : "+despachador+"" +
            "\nSOLICITUD NUMERO : " +numsol+
            "\n\n\nCLAVE: ";
            next = "/placas/placaAutorizacion.jsp?clave=";
        }
        else{
            e.setEmailsubject("CREACION DE NUEVA PLACA AUTORIZADA");
            String placa = request.getParameter("placa");
            String nitNProp =request.getParameter("NProp");
            texto1 = placa + nitNProp.substring(0,nitNProp.length()/2);
            texto2 = numsol+nitNProp + nitNProp.substring(nitNProp.length()/2,nitNProp.length());
            body = "CREACION DE NUEVA PLACA AUTORIZADA\n" +
            "\nPLACA    : "+placa+"" +
            "\nNUEVO PROPIETARIO: "+nitNProp+" - " +request.getParameter("nombreNProp")+
            "\nDESPACHADOR : "+despachador+"" +
            "\nSOLICITUD NUMERO : " +numsol+
            "\n\n\nCLAVE: ";
            next = "/placas/placaAutorizacionInsert.jsp?clave=";
        }
        try{
            clave= com.tsp.util.Util.getClave(texto1,texto2);
            //System.out.println("La clave es :"+clave);
        }catch(UnsupportedEncodingException ex){
            throw new ServletException(ex.getMessage());
        }
        e.setEmailbody(body+clave);
        next = next + clave;
        //System.out.println("Ahora voy a seguir con el email a enviar");
        
        //LLAMO EL HILO QUE ENVIA EMAILS
        if(email!=null){
            //System.out.println("Voy a correr el hilo de sendmail");
            com.tsp.operation.model.threads.SendMail s = new com.tsp.operation.model.threads.SendMail();
            s.start(e);
        }
        if(request.getParameter("manual")!=null)
            next = "/placas/placaAutorizacionManual.jsp?clave="+clave;
        
        
        this.dispatchRequest(next);
        
        
    }
    
    
}
