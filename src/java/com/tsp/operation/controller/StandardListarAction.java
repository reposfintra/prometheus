/*
 * StandardListarAction.java
 *
 * Created on 22 de noviembre de 2004, 09:46 AM
 */

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  KREALES
 */
public class StandardListarAction  extends Action{
    
    /** Creates a new instance of StandardListarAction */
    public StandardListarAction(){
    }
    
    public void run() throws javax.servlet.ServletException, InformationException {
        
        String next="/despacho/StandardTiempos.jsp";
        Usuario usuario= (Usuario) request.getAttribute("Usuario");
        request.setAttribute("Usuario", usuario);
              
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
    }
    
}
