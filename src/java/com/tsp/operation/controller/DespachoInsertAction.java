/*
 * DespachoInsertAction.java
 *
 * Created on 28 de marzo de 2005, 09:39 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class DespachoInsertAction extends Action{
        //2009-09-02

    String grupo="ninguno";
    float valor=0;
    float valorT=0;
    float valorA=0;
    float costo=0;
    float maxant=0;
    String dest="";
    String origen="";
    float acpmmax=0;
    float peajeamax=0;
    float peajebmax=0;
    float peajecmax=0;
    float aacpm=0;
    float apeaje=0;
    float aanticipo=0;
    Remesa rem;
    Stdjobcosto std;
    Usuario usuario;
    String no_rem="";
    float totalAjuste =0;
    boolean efectivo=false;
    
    /** Creates a new instance of DespachoInsertAction */
    public DespachoInsertAction() {
    }
    
    
    
    
    public void buscarValores(String proveedorA, float cantA, String proveedorT, float peajea,float peajeb,float peajec )throws ServletException{
        
        try{
            String [] nitcod = null;
            nitcod = proveedorA.split("/");
            String sucursal = "";
            if(nitcod.length>1){
                sucursal=nitcod[1];
            }
            float valora=0, valorb=0, valorc=0;
            efectivo=false;
            model.proveedoracpmService.buscaProveedor(nitcod[0],sucursal);
            if(model.proveedoracpmService.getProveedor()!=null){
                Proveedor_Acpm pacpm = model.proveedoracpmService.getProveedor();
                float vacpm= pacpm.getValor();
                valorA= vacpm * cantA;
                //System.out.println("--------SERVICIO DE LA BOMBA...."+pacpm.getTipo());
                if(pacpm.getTipo().equals("E")){
                    if(pacpm.getMax_e()>0){
                        efectivo=true;
                    }
                }
                
            }
            
            model.peajeService.buscar("A");
            if(model.peajeService.get()!=null){
                Peajes p = model.peajeService.get();
                float vt = p.getValor();
                valora = vt * peajea;
            }
            model.peajeService.buscar("B");
            if(model.peajeService.get()!=null){
                Peajes p = model.peajeService.get();
                float vt = p.getValor();
                valorb = vt * peajeb;
            }
            
            model.peajeService.buscar("C");
            if(model.peajeService.get()!=null){
                Peajes p = model.peajeService.get();
                float vt = p.getValor();
                valorc = vt * peajec;
            }
            
            valorT = valora+valorb+valorc;
            
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
    }
    
    
    public void buscarValor(String sj, String placa, float pesoreal)throws ServletException{
        
        try{
            model.stdjobcostoService.buscaStdJobCosto(sj);
            if(model.stdjobcostoService.getStandardCosto()!=null){
                this.std = model.stdjobcostoService.getStandardCosto();
                maxant=std.getporcentaje_maximo_anticipo();
                costo = std.getUnit_cost();
                valor= pesoreal * costo;
            }
            
            model.plkgruService.buscaPlkgru(placa,sj);
            if(model.plkgruService.getPlkgru()!=null){
                Plkgru plk = model.plkgruService.getPlkgru();
                grupo=plk.getGroupcode();
                model.stdjobplkcostoService.buscaStd(grupo);
                if(model.stdjobplkcostoService.getStd()!=null){
                    Stdjobplkcosto std = model.stdjobplkcostoService.getStd();
                    costo = std.getUnit_cost();
                    valor= pesoreal * costo;
                }
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
    }
    
    
    
    public void buscarAjuste(String pacpm, String pant, String ppeaje,float valorA,float anticipo,float valorT)throws ServletException{
        
        try{
            //System.out.println("-------------------BUSCAR AJUSTE------------");
            float porcentaje = 0;
            String [] nitcod = null;
            nitcod = pacpm.split("/");
            String sucursal = "";
            if(nitcod.length>1){
                sucursal=nitcod[1];
            }
            
            //SE BUSCA EL AJUSTE DEL ACPM
            //System.out.println("-------------------BUSCAR AJUSTE DEL ACPM------------");
            //System.out.println("Codigo proveedor ACPM "+nitcod[0]+sucursal);
            model.proveedoracpmService.buscaProveedor(nitcod[0], sucursal);
            if(model.proveedoracpmService.getProveedor()!=null){
                porcentaje = model.proveedoracpmService.getProveedor().getPorcentaje()/100;
                //System.out.println("Porcentaje del ACPM  "+porcentaje);
                aacpm= valorA *porcentaje;
            }
            
            //SE BUSCA EL AJUSTE DEL PEAJE
            //System.out.println("-------------------BUSCAR AJUSTE DEL PEAJE------------");
            nitcod = ppeaje.split("/");
            sucursal = "";
            if(nitcod.length>1){
                sucursal=nitcod[1];
            }
            //System.out.println("Codigo proveedor peaje "+nitcod[0]+sucursal);
            model.proveedortiquetesService.buscaProveedor(nitcod[0] ,sucursal);
            if(model.proveedortiquetesService.getProveedor()!=null){
                //System.out.println("Encontre el proveedor de Peajes");
                porcentaje = model.proveedortiquetesService.getProveedor().getPorcentaje()/100;
                //System.out.println("Porcentaje del peaje  "+porcentaje);
                apeaje = anticipo*porcentaje;
            }
            //SE BUSCA EL AJUSTE DEL ANTICIPO
            nitcod = pant.split("/");
            sucursal = "";
            if(nitcod.length>1){
                sucursal=nitcod[1];
            }
            model.proveedoranticipoService.buscaProveedor(nitcod[0],sucursal);
            //System.out.println("Codigo proveedor anticipo "+nitcod[0]+sucursal);
            if(model.proveedoranticipoService.getProveedor()!=null){
                //System.out.println("Encontre el proveedor de Anticipo");
                porcentaje = model.proveedoranticipoService.getProveedor().getPorcentaje()/100;
                //System.out.println("Porcentaje del anticipo  "+porcentaje);
                aanticipo = valorT*porcentaje;
            }
            
            totalAjuste = aacpm+apeaje+aanticipo;
            
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
    }
    
    public void run() throws ServletException,InformationException {
        
        String mensajes="";
        String destino="";
        String origen="";
        String nit="";
        String numpla="";
        String numre="";
        String next;
        String unit_of_work="";
        String moneda="";
        String cf_code="";
        float costoRem=0;
        float costoPla=0;
        float pesoreal=0;
        HttpSession session = request.getSession();
        String fecha="";
        String nombre="";
        String descripcion="";
        String unit_transp="";
        String sj = request.getParameter("standard");
        String placa = request.getParameter("placa").toUpperCase();
        float full_weight_m=Float.parseFloat(request.getParameter("pesolm"));
        float empty_weight_m=Float.parseFloat(request.getParameter("pesovm"));
        float pesoVacio = Float.parseFloat(request.getParameter("pesov"));
        float full_w=Float.parseFloat(request.getParameter("pesoll"));
        float anticipo = Float.parseFloat(request.getParameter("anticipo"));
        String proveedorAVec[]= request.getParameter("proveedora").split("/");
        String proveedorA= proveedorAVec[0];
        float gacpm=Float.parseFloat(request.getParameter("gacpm"));
        String proveedorAcpm= request.getParameter("proveedorAcpm");
        int peajea = Integer.parseInt(request.getParameter("peajea"));
        int peajeb = Integer.parseInt(request.getParameter("peajeb"));
        int peajec = Integer.parseInt(request.getParameter("peajec"));
        String proveedorTVec[] = request.getParameter("tiquetes").split("/");
        String proveedorT = proveedorTVec[0];
        String traffic_code =request.getParameter("trafico");
        String fecdsp = request.getParameter("fecdsp");
        
        String sucursalA="";
        if(proveedorAVec.length>1){
            sucursalA = proveedorAVec[1];
        }
        String sucursalT="";
        if(proveedorTVec.length>1){
            sucursalT = proveedorTVec[1];
        }
        
        
        String remision = request.getParameter("remision");
        
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        Planilla pla= new Planilla();
        Remesa rem = new Remesa();
        Plaaux plaaux= new Plaaux();
        RemPla rempla= new RemPla();
        
        //System.out.println("Sucursales  Anticipo: "+sucursalA+" Tiquetes: "+sucursalT);
        
        try{
            model.tService.crearStatement();
            model.stdjobdetselService.buscaStandardDetSel(sj);
            if(model.stdjobdetselService.getStandardDetSel()!=null){
                Stdjobdetsel stdjobdetsel = model.stdjobdetselService.getStandardDetSel();
                destino=stdjobdetsel.getDestination_code();
                origen= stdjobdetsel.getOrigin_code();
                unit_of_work= stdjobdetsel.getUnit_of_work();
                moneda=stdjobdetsel.getCurrency();
                costoRem=stdjobdetsel.getVlr_freight();
                descripcion = stdjobdetsel.getSj_desc();
            }
            
            model.stdjobcostoService.buscaStdJobCosto(request.getParameter("standard"));
            if(model.stdjobcostoService.getStandardCosto()!=null){
                Stdjobcosto stdjobcosto = model.stdjobcostoService.getStandardCosto();
                cf_code=stdjobcosto.getCf_code();
                unit_transp=stdjobcosto.getUnit_transp();
                if(cf_code==null){
                    cf_code="";
                }
            }
            
            model.placaService.buscaPlaca(placa);
            if(model.placaService.getPlaca()!=null){
                Placa placao = model.placaService.getPlaca();
                nit=placao.getPropietario();
                
            }
            //Se asigna una serie a la planilla.
            model.seriesService.obtenerSerie( "001", usuario);
            if(model.seriesService.getSerie()!=null){
                Series serie = model.seriesService.getSerie();
                model.tService.getSt().addBatch(model.seriesService.asignarSerie(serie,usuario));
                numpla=serie.getPrefix()+serie.getLast_number();
                //System.out.println("Encontro la serie de planilla");
            }
            model.seriesService.obtenerSerie( "002", usuario);
            if(model.seriesService.getSerie()!=null){
                Series serie = model.seriesService.getSerie();
                model.tService.getSt().addBatch(model.seriesService.asignarSerie(serie,usuario));
                numre=serie.getPrefix()+serie.getLast_number();
                //System.out.println("Encontro la serie de remesa");
            }
            
            if(model.conductorService.conductorExist(request.getParameter("conductor"))){
                model.conductorService.buscaConductor(request.getParameter("conductor"));
                if(model.conductorService.getConductor()!=null){
                    Conductor conductor = model.conductorService.getConductor();
                    nombre=conductor.getNombre();
                }
                
            }
            
            pesoreal=full_w-pesoVacio;
            this.buscarValor(sj,placa, pesoreal);
            
            pla.setAgcpla(usuario.getId_agencia());
            pla.setCia(usuario.getDstrct());
            pla.setDespachador(usuario.getLogin());
            pla.setDespla(destino);
            pla.setNitpro(nit);
            pla.setNumpla(numpla);
            pla.setOripla(origen);
            pla.setPlatlr(request.getParameter("trailer").toUpperCase());
            pla.setPlaveh(placa);
            pla.setCedcon(request.getParameter("conductor"));
            pla.setTipoviaje("NOR");
            pla.setMoneda(moneda);
            pla.setOrden_carga(request.getParameter("orden"));
            pla.setNomCond(nombre);
            pla.setPesoreal(pesoreal);
            pla.setVlrpla(valor);
            pla.setRuta_pla(origen+""+destino);
            pla.setPrecinto("");
            pla.setUnit_transp(unit_transp);
            pla.setMoneda(moneda);
            pla.setUnit_cost(0);
            pla.setFecdsp(fecdsp);
            pla.setTraffic_code(traffic_code);
            model.planillaService.setPlanilla(pla);
            model.tService.getSt().addBatch(model.planillaService.ActualizarTrafficTag(usuario.getBase()));
            
            //Agrego la planilla
            
            //model.planillaService.insertPlanilla(pla);
            
            model.tService.getSt().addBatch(model.planillaService.insertPlanilla(pla,usuario.getBase()));
            
            rem.setAgcRem(usuario.getId_agencia());
            rem.setCia(usuario.getDstrct());
            rem.setCliente(request.getParameter("standard").substring(0,3));
            rem.setDesRem(destino);
            rem.setNumRem(numre);
            rem.setOriRem(origen);
            rem.setStdJobNo(request.getParameter("standard"));
            rem.setTipoViaje("NA");
            rem.setUnidcam(moneda);
            rem.setUnitOfWork(unit_of_work);
            rem.setUsuario(usuario.getLogin());
            rem.setVlrRem(costoRem);
            rem.setPesoReal(pesoreal);
            rem.setDocInterno("");
            rem.setRemitente("");
            rem.setDestinatario("");
            rem.setObservacion("");
            rem.setDescripcion(descripcion);
            rem.setRemision("CS"+remision);
            //Agrego la Remesa.
            model.tService.getSt().addBatch(model.remesaService.insertRemesa(rem,usuario.getBase()));
            
            
            rempla.setPlanilla(numpla);
            rempla.setRemesa(numre);
            rempla.setDstrct(usuario.getDstrct());
            rempla.setFecha("'now()'");
            //Agrego la Remesa-Planilla
            
            model.tService.getSt().addBatch(model.remplaService.insertRemesa(rempla,usuario.getBase()));
            
            plaaux.setDstrct(usuario.getDstrct());
            plaaux.setFull_weight(0);
            plaaux.setPlanilla(numpla);
            plaaux.setAcpm_code("");
            plaaux.setFull_weight_m(full_weight_m);
            plaaux.setEmpty_weight_m(empty_weight_m);
            plaaux.setCreation_user(usuario.getLogin());
            //Agrego la planilla auxiliar.
            
            model.tService.getSt().addBatch(model.plaauxService.insertPlaaux(plaaux,usuario.getBase()));
            
            List listTabla = model.tbltiempoService.getTblTiemposAll(request.getParameter("standard"));
            Iterator itTbla=listTabla.iterator();
            
            while(itTbla.hasNext()){
                
                Tbltiempo tbl = (Tbltiempo) itTbla.next();
                String tblcode = tbl.getTimeCode();
                int secuence = tbl.getSecuence();
                fecha = request.getParameter(tbl.getTimeCode());
                
                Planilla_Tiempo pla_tiempo= new Planilla_Tiempo();
                
                pla_tiempo.setCf_code(cf_code);
                pla_tiempo.setCreation_user(usuario.getLogin());
                pla_tiempo.setDate_time_traffic(fecha);
                pla_tiempo.setDstrct(usuario.getDstrct());
                pla_tiempo.setPla(numpla);
                pla_tiempo.setSecuence(secuence);
                pla_tiempo.setSj(request.getParameter("standard"));
                pla_tiempo.setTime_code(tblcode);
                model.tService.getSt().addBatch(model.pla_tiempoService.insertTiempo(pla_tiempo,usuario.getBase()));
            }
            model.tService.execute();
            
            
            //AQUI EMPIEZAN LAS MODIFICACIONES
            model.tService.crearStatement();
            
            pesoreal=full_w-pesoVacio;
            
            rem.setPesoReal(pesoreal);
            rem.setVlrRem(costoRem*pesoreal);
            model.tService.getSt().addBatch(model.remesaService.updateRemesa(rem));
            
            
            
            model.planillaService.bucaPlanilla(numpla);
            //System.out.println("planilla no " + numpla);
            if(model.planillaService.getPlanilla()!=null){
                //System.out.println("Actualizo la planilla " +numpla);
                pla= model.planillaService.getPlanilla();
                //System.out.println("Valor de la pla " +valor);
                //System.out.println("Peso real " +pesoreal);
                pla.setNumpla(numpla);
                pla.setVlrpla(valor);
                pla.setPesoreal(pesoreal);
                pla.setGroup_code(grupo);
                pla.setUnit_cost(costo);
                
                // pla.setFeccum("'now()'");
                nit=pla.getNitpro();
                //Hago el update de la planilla.
                
                model.tService.getSt().addBatch(model.planillaService.updatePlanilla(pla));
                
            }
            
            model.remplaService.buscaRemPla(numpla);
            if(model.remplaService.getRemPla()!=null){
                RemPla rp = model.remplaService.getRemPla();
                rp.setFecha("'now()'");
                rp.setQuvrcm(pesoreal);
                rp.setObservacion("");
                rp.setPlanilla(numpla);
                rp.setRemesa(this.no_rem);
                model.tService.getSt().addBatch(model.remplaService.cumplirRemPla(rp));
            }
            
            String [] nitcod = null;
            nitcod = proveedorAcpm.split("/");
            
            float totalPeajes = peajea+peajeb+peajec;
            this.buscarValores(proveedorAcpm, gacpm, proveedorT,peajea,peajeb,peajec);
            
            //System.out.println("Efectivo es "+efectivo);
            if(efectivo == true){
                anticipo = anticipo + valorA;
                gacpm=0;
                valorA=0;
            }
            
            //Hago el update de la planilla aux.
            String sucursal="";
            if(nitcod.length>1){
                sucursal = nitcod[1];
            }
            
            plaaux= new Plaaux();
            plaaux.setPlanilla(numpla);
            plaaux.setAcpm_gln(gacpm);
            plaaux.setAcpm_supplier(nitcod[0]);
            plaaux.setAcpm_code(sucursal);
            plaaux.setCreation_user(usuario.getLogin());
            plaaux.setEmpty_weight(pesoVacio);
            plaaux.setTicket_a((float)peajea);
            plaaux.setTicket_b((float)peajeb);
            plaaux.setTicket_c((float)peajec);
            plaaux.setTicket_supplier(proveedorT);
            plaaux.setTiket_code(sucursalT);
            plaaux.setFull_weight(full_w);
            model.tService.getSt().addBatch(model.plaauxService.updatePlaaux(plaaux));
            
            Movpla movpla= new Movpla();
            if(anticipo!=0){
                
                movpla.setAgency_id(usuario.getId_agencia());
                movpla.setCurrency(std.getCurrency());
                movpla.setDstrct(usuario.getDstrct());
                movpla.setPla_owner(nit);
                movpla.setPlanilla(numpla);
                movpla.setSupplier(placa);
                movpla.setVlr(anticipo);
                movpla.setDocument_type("001");
                movpla.setConcept_code("01");
                movpla.setAp_ind("S");
                movpla.setProveedor(proveedorA);
                movpla.setCreation_user(usuario.getLogin());
                movpla.setVlr_for(0);
                movpla.setSucursal(sucursalA);
                movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+10000));
                
//                model.tService.getSt().addBatch(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                
            }
            //Inserto el movpla del acpm
            
            sucursal ="";
            if(nitcod.length>1){
                sucursal = nitcod[1];
            }
            if(valorA!=0){
                movpla= new Movpla();
                movpla.setAgency_id(usuario.getId_agencia());
                movpla.setCurrency(std.getCurrency());
                movpla.setDstrct(usuario.getDstrct());
                movpla.setPla_owner(nit);
                movpla.setPlanilla(numpla);
                movpla.setSupplier(placa);
                movpla.setVlr(valorA);
                movpla.setDocument_type("001");
                movpla.setConcept_code("02");
                movpla.setAp_ind("S");
                movpla.setProveedor(nitcod[0]);
                movpla.setSucursal(sucursal);
                movpla.setCreation_user(usuario.getLogin());
                movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+10));
//                model.tService.getSt().addBatch(model.movplaService.insertMovPla(movpla,usuario.getBase()));
            }
            //inserto el movpla de los tiquetes
            if(valorT!=0){
                
                movpla= new Movpla();
                movpla.setAgency_id(usuario.getId_agencia());
                movpla.setCurrency(std.getCurrency());
                movpla.setDstrct(usuario.getDstrct());
                movpla.setPla_owner(nit);
                movpla.setPlanilla(numpla);
                movpla.setSupplier(placa);
                movpla.setVlr(valorT);
                movpla.setDocument_type("001");
                movpla.setConcept_code("03");
                movpla.setAp_ind("S");
                movpla.setProveedor(proveedorT);
                movpla.setSucursal(sucursalT);
                movpla.setCreation_user(usuario.getLogin());
                movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+11));
//                model.tService.getSt().addBatch(model.movplaService.insertMovPla(movpla,usuario.getBase()));
            }
            this.buscarAjuste(proveedorAcpm,proveedorA+"/"+sucursalA,proveedorT+"/"+sucursalT,valorA,anticipo,valorT);
            
            if(totalAjuste!=0){
                
                //inserto el movpla del ajuste del acpm
                movpla= new Movpla();
                movpla.setAgency_id(usuario.getId_agencia());
                movpla.setCurrency(std.getCurrency());
                movpla.setDstrct(usuario.getDstrct());
                movpla.setPla_owner(nit);
                movpla.setPlanilla(numpla);
                movpla.setSupplier(placa);
                movpla.setVlr(totalAjuste);
                movpla.setDocument_type("001");
                movpla.setConcept_code("09");
                movpla.setAp_ind("S");
                movpla.setProveedor(proveedorA);
                movpla.setSucursal(sucursalA);
                movpla.setCreation_user(usuario.getLogin());
                movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+12));
//                model.tService.getSt().addBatch(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                
            }
            //AGREGAMOS LOS ANTICIPOS DINAMICOS..
            model.anticiposService.vecAnticipos(usuario.getBase(),sj);
            Vector ants = model.anticiposService.getAnticipos();
            for (int k=0 ; k<ants.size();k++){
                Anticipos ant = (Anticipos) ants.elementAt(k);
                model.anticiposService.searchAnticipos(usuario.getDstrct(), ant.getAnticipo_code(), sj);
                if(model.anticiposService.getAnticipo()!=null){
                    ant = model.anticiposService.getAnticipo();
                    float value = 0;
                    if(request.getParameter(ant.getAnticipo_code())!=null){
                        value = Float.parseFloat(request.getParameter(ant.getAnticipo_code()));
                    }
                    String prov="";
                    String nitPA="";
                    String sucursalPA="";
                    if(request.getParameter("provee"+ant.getAnticipo_code())!=null){
                        prov = request.getParameter("provee"+ant.getAnticipo_code());
                        String vec[]=prov.split("/");
                        nitPA = vec[0];
                        if(vec.length>1){
                            sucursalPA = vec[1];
                        }
                    }
                    movpla= new Movpla();
                    movpla.setAgency_id(usuario.getId_agencia());
                    movpla.setCurrency(std.getCurrency());
                    movpla.setDstrct(usuario.getDstrct());
                    movpla.setPla_owner(nit);
                    movpla.setPlanilla(numpla);
                    movpla.setSupplier(placa);
                    movpla.setVlr(value);
                    movpla.setDocument_type("001");
                    movpla.setConcept_code(ant.getAnticipo_code());
                    movpla.setAp_ind(ant.getIndicador());
                    movpla.setProveedor(nitPA);
                    movpla.setSucursal(sucursalPA);
                    movpla.setCreation_user(usuario.getLogin());
                    movpla.setCreation_date(""+new java.sql.Timestamp(System.currentTimeMillis()+12+k));
//                    model.tService.getSt().addBatch(model.movplaService.insertMovPla(movpla,usuario.getBase()));
                }
            }
            
            model.tService.execute();
            
            next="/controller?estado=Menu&accion=ImprimirCarbon&total=0&opcion=no&planilla="+numpla;
            
            
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
        
    }
}
