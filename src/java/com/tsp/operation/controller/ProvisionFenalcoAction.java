/********************************************************************
 *      Nombre Clase.................   FacturaLArchivoAction.JAVA
 *      Descripci�n..................   Action que se encarga de inicializar el programa de documentos por pagar
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.util.Util;
/**
 *
 * @author  dlamadrid modified by jemartinez
 */
public class ProvisionFenalcoAction extends Action{
    
    private String[] facturas;
    
    private int offset_fenalco = 0;
    private int offset_fintra = 0;
    private int pagina = 0;
    
    private int opcion = 0;
    private final static int CXP = 0;
    private final static int PAGINA = 1;
    int estado = 0;
    
    /** Creates a new instance of FacturaLArchivoAction */
    public ProvisionFenalcoAction() {
    }
    
    
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            
            //ivan 21 julio 2006
            com.tsp.finanzas.contab.model.Model modelcontab = (com.tsp.finanzas.contab.model.Model) session.getAttribute("modelcontab");
            
            String age = (usuario.getId_agencia().equals("OP")?"":usuario.getId_agencia());
            model.servicioBanco.loadBancos(age, usuario.getCia());
            
            String Modificar = (request.getParameter("Modificar")!=null)?request.getParameter("Modificar"):"";
            model.cxpDocService.obtenerAgencias();
            model.cxpDocService.BuscarHC();
            
            String userlogin=""+usuario.getLogin();
            String next = "";
            String agencia_usuario = "";
            CXP_Doc factura;
            Vector vItems;
            String maxfila = "1";
            //System.out.println("MODIFICARRRRR ---->"+Modificar);
            model.agenciaService.loadAgencias();
            model.tblgensvc.buscarAutXCP(); 
            
            
            //jemartinez
            //Recibe el valor de el numero del titulo en el sistema y el valor del titulo
            
            facturas = request.getParameterValues("titulo");//Obtiene los parametros por POST
            if(facturas!=null){
                //BeanGeneral beanGeneral = new BeanGeneral();//Crea un BeanGeneral para guardar la informacion
                //beanGeneral.setV5(facturas);//Establece los valores de los titulos
                //model.provisionFenalcoService.setBeanTitulosService(beanGeneral);//Guarda el bean el el servicio asociado
                
            }
            
            try{
                factura = model.cxpDocService.leerArchivo("factura"+userlogin+".txt", userlogin);
                model.cxpDocService.setFactura(factura);
                maxfila = (factura.getMaxfila()!=null)?factura.getMaxfila():"1";
                agencia_usuario = factura.getAgencia();
                session.setAttribute("id_agencia", agencia_usuario );
                //System.out.println("MAXFILA ACTION -> "+maxfila);
                vItems = model.cxpDocService.leerArchivoItems("factura"+userlogin+".txt", userlogin);
                // Modificacion 21 julio 2006
                for(int i=0; i< vItems.size();i++){
                    CXPItemDoc item = (CXPItemDoc)vItems.get(i);
                    if(item != null){
                        
                        LinkedList tbltipo = null;
                        if(modelcontab.planDeCuentasService.existCuenta(usuario.getDstrct(),item.getCodigo_cuenta())){
                            modelcontab.subledgerService.busquedaCuentasTipoSubledger(usuario.getDstrct(),item.getCodigo_cuenta());
                            tbltipo = modelcontab.subledgerService.getCuentastsubledger();
                        }
                        
                        item.setTipo(tbltipo);
                    }
                }
                
                
                model.cxpItemDocService.setVecCxpItemsDoc(vItems);
            }catch(Exception e){
                model.cxpDocService.setFactura(null);
                model.cxpItemDocService.setVecCxpItemsDoc(null);
            }
            
            
            /*Las opciones son:
             * 0: cxp
             * 1: siguiente pagina
             * jemartinez
             */
            //System.out.println("Pagina: ");
            opcion = Integer.parseInt(request.getParameter("opcion"));
            
            if(request.getParameter("no_pagina")!=null){
                pagina = Integer.parseInt(request.getParameter("no_pagina"))+1;
            }
            
            if(opcion==this.CXP){
                //Facturas es usada tanto para cxp_doc como para cxp_items_doc
                int estado = 0;
                if(facturas!=null){
                    double sumaProvisiones = 0;
                    String[] temp = null;
                    for(int i=0;i<facturas.length;i++){
                        temp = facturas[i].split(";");
                        sumaProvisiones = sumaProvisiones + Math.round(Double.parseDouble(temp[3]));//Ya incluye el iva
                    }
                    
                     String infoProveedor = model.provisionFenalcoService.obtenerInformacionProveedorService();//banco;sucursal;agencia
                    
                    //nit, item, no_factura, provision+iva, usuario, numero_aval, factura, nombre_proveedor,fecha_factura, suma_provision+iva, banco, sucursal, agencia, plazo
                    String informacion = facturas[0]+";"+sumaProvisiones+";"+infoProveedor+";"+10;

                    try{
                         model.provisionFenalcoService.setCxpDocService(informacion);
                        //System.out.println("Exito cxp_doc");
                        estado=1;
                    }catch(java.lang.Exception e){
                        System.out.println("Service: "+e.toString());
                    }
                    
                    if(estado==0){
                         String provisionAntigua = model.provisionFenalcoService.obtenerValorCxpDocService(facturas[0].split(";")[2]);//noFactura
                         sumaProvisiones = sumaProvisiones + Double.parseDouble(provisionAntigua);
                         model.provisionFenalcoService.actualizarValorCxpDocService(facturas[0].split(";")[2],Math.round(sumaProvisiones)+"");
                     //   System.out.println("Exito actualizando valor cxp");
                    }
                   
                    try{
                         model.provisionFenalcoService.setCxpItemsDocService(facturas);
                        //System.out.println("Exito cxp_items_doc");
                        estado=2;
                    }catch(java.lang.Exception e){
                        System.out.println("Service: "+e.toString());
                    }

                    if(estado!=2){
                        next = "/jsp/finanzas/contab/adminComprobantes/provisionFenalco.jsp?marco=no&cxp=no";//cxp error
                    }else{
                        model.provisionFenalcoService.setEstadoProvisionService(facturas, (facturas[0].split(";"))[2]);
                        //System.out.println((facturas[0].split(";"))[2]);
                        //System.out.println("Exito al actualizar facturas");
                        next = "/jsp/finanzas/contab/adminComprobantes/provisionFenalco.jsp?marco=no&cxp=yes";//cxp hecha
                    }
                    
                }else{
                    next = "/jsp/finanzas/contab/adminComprobantes/provisionFenalco.jsp?marco=no";
                }
            }

            if(opcion==this.PAGINA){
                offset_fenalco = Integer.parseInt(request.getParameter("offset_fenalco"));
                offset_fintra = Integer.parseInt(request.getParameter("offset_fintra"));
                //System.out.println("Action offset_fintra: "+this.offset_fintra+"Action offset_fenalco: "+this.offset_fenalco);
                next = "/jsp/finanzas/contab/adminComprobantes/provisionFenalco.jsp?marco=no&offset_fenalco="+offset_fenalco+"&offset_fintra="+offset_fintra+"&no_pagina="+pagina;
            }

            agencia_usuario = usuario.getId_agencia();
            session.setAttribute("id_agencia",agencia_usuario );
            String agContable ="";
            String unidadC ="";
            

            Vector VecAgencias = model.cxpDocService.getAgencias();
            for(int i=0; i< VecAgencias.size();i++){
		Agencia ag = (Agencia) VecAgencias.get(i);
                if(agencia_usuario.equals(ag.getId_agencia())){
                    agContable =  ag.getAgenciaContable();
                    unidadC    =  ag.getUnidadNegocio();
                }
            }
            
            session.setAttribute("agContable",agContable);
            session.setAttribute("unidadC",unidadC);
            
            
            this.dispatchRequest(next);
            
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new ServletException("Accion:"+ e.getMessage());
        }
    }
}
