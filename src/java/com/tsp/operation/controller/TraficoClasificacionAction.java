/********************************************************************
 *      Nombre Clase.................   TraficoClasificacionAction.java
 *      Descripci�n..................   Action para Clasificar(Ordenar) los diferentes campos de control trafico
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.11.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

import org.apache.log4j.*;
/**
 *
 * @author  David A
 */
public class TraficoClasificacionAction extends Action {
    
    Logger logger = Logger.getLogger(this.getClass());
    
    /** Creates a new instance of TraficoClasificacionAccion */
    public TraficoClasificacionAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next="";
        try{
            String filtroP = "" + request.getParameter("filtroP");
            String []camposS   = request.getParameterValues ("camposS");
            String []camposE   = request.getParameterValues ("camposE");
            String var4        = request.getParameter ("var4");
            String orden       = request.getParameter ("orden");
            String Opcion      = request.getParameter ("Opcion");
            String user        = request.getParameter ("usuario");
            String linea       = ""+ request.getParameter ("linea");
            String conf        = ""+request.getParameter ("conf");
            String var1        = ""+request.getParameter ("var1");
            String var3        = ""+request.getParameter ("var3");
            String var2        = ""+request.getParameter ("var2");
            String filtro      = ""+request.getParameter ("filtro");
            
            filtro=model.traficoService.decodificar (filtro);
            model.traficoService.generarVZonas (user);
            ////System.out.println ("Genrerar zonas ");
            Vector zonas = model.traficoService.obdtVectorZonas ();
            String validacion=" zona='-1'";
            
            if ((zonas == null)||zonas.size ()<=0){}else{
                validacion = model.traficoService.validacion (zonas);
            }
            
            ////System.out.println ("\nValidacion final: "+ validacion);
            if(var3.equals ("null") ){
                var3=model.traficoService.obtVar3 ();
            }
            if(linea.equals ("null")|| linea.equals ("")){
                linea=model.traficoService.obtlinea ();
            }
            if (conf.equals ("null")||conf.equals ("")){
                conf = model.traficoService.obtConf ();
            }
            if (filtro.equals ("null")||filtro.equals ("")){
                filtro = "";
                filtro= "where "+validacion+filtro;
            }
            else{
                filtro=filtro;
            }
            conf = conf +" ,color_letra,CASE WHEN reg_status='D' THEN get_ultimaobservaciontrafico(planilla,'detencion') ELSE ult_observacion END AS obs,neintermedias ";
            model.traficoService.listarColTrafico (var3);
            Vector col = model.traficoService.obtColTrafico ();
            model.traficoService.listarColtrafico2 (col);
            
            ////System.out.println ("\nFiltro final: "+ filtro);
            
            if (camposE!=null ||camposS != null){
                ////System.out.println ("\n campos Origen: "+ camposE);
                ////System.out.println ("\n Opcion: "+Opcion+ "  campos Destino: "+camposS);
                for(int i=0;i<=camposS.length-1;i++){
                    String campo   = camposS[i];
                    ////System.out.println ("Campo "+i+": "+campo);
                }
            }
            
            ////System.out.println ("conf en clasificacion: "+ conf);
            
            String clasificacion="";
            int f=1;
            if (camposS!=null){
                clasificacion = model.traficoService.getClasificacion (camposS,orden);
            }
            else{
                f=2;
                String v4[]= new String[1];
                v4[0]=var4;
                clasificacion = model.traficoService.getClasificacion (v4,orden);
            }
            
            String consulta      = model.traficoService.getConsulta (conf,filtro,clasificacion);
            
            
            ////System.out.println ("pasa por configuracion "+clasificacion+"  y consulta: "+ consulta);
            
            Vector colum= new Vector ();
            
            colum = model.traficoService.obtenerCampos (linea);
            
            ////System.out.println ("linea="+linea+" y obtiene colmunas"+colum.size ());
            model.traficoService.listarCamposTrafico (linea);
            model.traficoService.generarTabla (consulta, colum);
            ////System.out.println ("genera tabla ");
            
            
            ConfTraficoU vista = new ConfTraficoU ();
        vista.setConfiguracion (conf.replaceAll("-_-","'"));
            vista.setFiltro (filtro);
            vista.setLinea (linea);
            vista.setClas (clasificacion);
            vista.setVar1 (var1);
            vista.setVar2 (var2);
            vista.setVar3 (var3);
            vista.setCreation_user (user);
            model.traficoService.actualizarConfiguracion (vista); 
            
            filtro=model.traficoService.codificar (filtro);
            
            logger.info("FILTRO: " + filtro);
            
            if (f==1){
                next = "/jsp/trafico/controltrafico/vertrafico2.jsp?var3="+var3+"&var1="+var1+"&var2="+var2+"&filtro="+filtro+"&linea="+linea+"&conf="+conf+"&clas="+clasificacion+"&reload="+"&filtroP="+filtroP;
            }
            else{
                next = "/jsp/trafico/controltrafico/vertrafico2.jsp?var3="+var3+"&var1="+var1+"&var2="+var2+"&filtro="+filtro+"&linea="+linea+"&conf="+conf+"&clas="+clasificacion+""+"&filtroP="+filtroP;
            }
            ////System.out.println ("next en clasificacion"+next);
            
        }
        catch(Exception e){
            throw new ServletException ("Accion:"+ e.getMessage ());
        }
        this.dispatchRequest (next);
    }
}
