/***********************************************************************************
 * Nombre clase : ............... CXPagarAutorizarAction.java                            
 * Descripcion :................. Clase que maneja Las Acciones Para la Autorizacion de Facturas
 * Autor :....................... Ing. Henry A.Osorio Gonz�lez                     
 * Fecha :.......................  Created on 10 de octubre de 2005, 11:17 AM               
 * Version :..................... 1.0                                              
 * Copyright :................... Fintravalores S.A.                          
 ***********************************************************************************/



package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  hosorio
 */
public class CXPagarAutorizarAction extends Action{
    
    /** Creates a new instance of CXPBuscarFacturas */
    public CXPagarAutorizarAction() {
    }
     public void run() throws javax.servlet.ServletException, InformationException {
        String next="/jsp/cxpagar/facturas/autorizacionFacturas.jsp?msg=";
        HttpSession session = request.getSession();
        Vector cuentas      = (Vector) model.cxpDocService.getVectorFacturas();
        String Agencia      = (String)session.getAttribute("agencia");
        String Auto         = (String)session.getAttribute("Autorizadas");
        boolean Autorizadas = (Auto.equals("true"))?true:false;
        String Fecini       = (String)session.getAttribute("Fecini");
        String Fecfin       = (String)session.getAttribute("Fecfin");
        String pos = "",msg=""; 
        try{   
           Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
           for (int i=0; i<cuentas.size(); i++){
               pos = (request.getParameter("check"+i)!=null)?request.getParameter("check"+i):"";
               if (!pos.equals("")) {
                   CXP_Doc cxpdoc = (CXP_Doc) cuentas.elementAt(Integer.parseInt(pos)); 
                   cxpdoc.setFecha_aprobacion(Util.getFechaActual_String(6));
                   cxpdoc.setUsuario_aprobacion(usuario.getLogin());
                   if(cxpdoc.getNum_obs_autorizador()!=cxpdoc.getNum_obs_pagador ()) {
                       msg = "ERROR. No se autorizaron todos los pagos, existen observaciones sin cerrar&Autorizadas="+Autorizadas; 
                   } else {
                       model.cxpDocService.autorizarCuentaPorPagar(cxpdoc);                       
                       model.cxpDocService.buscarFacturasxUsuario(usuario.getLogin(),usuario.getDstrct(),Agencia, Fecini, Fecfin, Autorizadas);  
                       msg ="Cuentas por pagar autorizadas exitosamente&Autorizadas="+Autorizadas; 
                   }
               }
           }
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }         
         // Redireccionar a la p�gina indicada.
        next+=msg;
        this.dispatchRequest(next);
    }
    
}
