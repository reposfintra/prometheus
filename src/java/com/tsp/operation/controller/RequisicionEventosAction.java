package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tsp.operation.model.services.RequisicionesService;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.RequisicionesFormularioTask;
import com.tsp.operation.model.beans.RequisicionesListadoBeans;
import com.tsp.operation.model.beans.formularioSaldo;
import com.tsp.operation.model.services.RequisicionesService;
import com.tsp.util.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import javax.servlet.*;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RequisicionEventosAction extends Action {

    RequisicionesService rqs; // = new RequisicionesService();
    HttpSession session;

    public RequisicionEventosAction() {
    }

    public void run() throws ServletException, InformationException {

        String evento = "";
        String respuesta = "";

        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        rqs = new RequisicionesService(usuario.getBd());
        
        String dstrct = usuario.getDstrct();
        String iduser = usuario.getLogin();

        try {

            evento = request.getParameter("evento");

            int proceso = (request.getParameter("proceso") != null) ? Integer.parseInt(request.getParameter("proceso")) : 0;
            int tiporeq = (request.getParameter("tiporeq") != null) ? Integer.parseInt(request.getParameter("tiporeq")) : 0;
            String asunto = (request.getParameter("asunto") != null) ? request.getParameter("asunto") : "";
            String descripcion = (request.getParameter("descripcion") != null) ? request.getParameter("descripcion") : "";
            int prioridad = (request.getParameter("prioridad") != null) ? Integer.parseInt(request.getParameter("prioridad")) : 0;

            if (evento.equals("INSERT")) {

                try {        

                    respuesta = guardarRequisicion(iduser,dstrct,evento);
                    response.getWriter().println(respuesta);
                    /*respuesta = rqs.InsertarRequisicion(dstrct, tiporeq, proceso, iduser, asunto, descripcion, prioridad);
                    response.getWriter().println(respuesta);*/

                } catch (Exception e) {

                    System.out.println("Error en action ListaPoliticasReporte: " + e.toString());
                    e.printStackTrace();

                }

            } else if (evento.equals("GUARDARACCIONES")) {

                try {
                    
                    String formulario = request.getParameter("formulario") != null ? request.getParameter("formulario") : "";
                    int tipo_tarea = Integer.parseInt(request.getParameter("tipo_tarea") != null ? request.getParameter("tipo_tarea") : "0");
                    int idReqTask = Integer.parseInt(request.getParameter("idReqTask") != null ? request.getParameter("idReqTask") : "0");
                    
                    String respuestaJson = "{}";
                    Gson gson = new Gson();

                    //Convertimos el json a un array con los datos del formulario
                    Type tipoRequisicionesFormularioSaldo = new TypeToken<ArrayList<RequisicionesFormularioTask>>() {
                    }.getType();
                      ArrayList<RequisicionesFormularioTask> listForm = gson.fromJson(formulario, tipoRequisicionesFormularioSaldo);

                    Map<String, String> resultsMap = new HashMap<String, String>();
                    for (RequisicionesFormularioTask FormBeanRq : listForm) {
                        resultsMap.put(FormBeanRq.getName(), FormBeanRq.getValue());
                    }

                    //int index = 1;
                    //ArrayList ListadoReqs = null;
                    ArrayList ListadoTareas = new ArrayList();
                    int NumObjects = listForm.size()/7;
                    
                    String DinamicIndex = "";
                    int z=0;
                    int iDtarea = 0;
                            
                    for (int i = 0; i < NumObjects; i++) {
                        
                        z = (i+1)*5;
                        RequisicionesFormularioTask get = listForm.get(z); 
                        
                        DinamicIndex = get.getName().substring(get.getName().length()-1,get.getName().length());
                        
                        String get1 = resultsMap.get("tarea" + DinamicIndex);
                        String get2 = resultsMap.get("CalendarInputFie" + DinamicIndex);
                        String get3 = resultsMap.get("CalendarInputFin" + DinamicIndex);
                        String get4 = resultsMap.get("horas" + DinamicIndex);
                        String get5 = resultsMap.get("CalendarInputFci" + DinamicIndex);
                        String get6 = resultsMap.get("reproceso" + DinamicIndex);
                        String get7 = resultsMap.get("IdTareaRq" + DinamicIndex);
                        
                        iDtarea = !get7.equals("") ? Integer.parseInt(get7) : 0;

                        RequisicionesListadoBeans Lstsk = new RequisicionesListadoBeans();

                        Lstsk.setDescripcionTask(get1);
                        Lstsk.setFchInicioTask(get2);
                        Lstsk.setFchFinTask(get3);
                        Lstsk.setHourTask(get4);
                        Lstsk.setFchTerminacion(get5);
                        Lstsk.setHoraReprocesoTask(get6);
                        Lstsk.setIdTask(iDtarea);

                        ListadoTareas.add(Lstsk);
                        System.out.println(DinamicIndex);
                    }
                    
                    rqs.InsertActividadesRequisicion(ListadoTareas, usuario, tipo_tarea, idReqTask);
                    printlnResponse("{\"mensaje\":\"listo\"}", "application/json");
                    
                    //Forma Edgar
                    //TransaccionService tservice = new TransaccionService();
                    //tservice.crearStatement();
                    //tservice = getAddBatch(tservice,rqs.InsertActividadesRequisicion(ListadoTareas, usuario, tipo_tarea, idReqTask));
                    //tservice.execute();
                    //tservice.getSt().executeBatch();
                    //tservice.getSt().addBatch(ps.getSql());
                    //tservice.closeAll();

                    //respuesta =  rqs.InsertActividadesRequisicion(dstrct, tiporeq, proceso, iduser,asunto, descripcion);
                    //response.getWriter().println(respuesta);
                    
                } catch (Exception e) {

                    System.out.println("Error en action -> CrearAccionesReq: " + e.toString());
                    e.printStackTrace();
                    throw new Exception(e.getMessage());
                }

            } else if (evento.equals("GUARDARONE")) {
                
                try {
                    
                    int idReqTask = Integer.parseInt(request.getParameter("idReqTask") != null ? request.getParameter("idReqTask") : "0");
                    int IdActionRq = Integer.parseInt(request.getParameter("IdActionRq") != null ? request.getParameter("IdActionRq") : "0");
                    String DscAction = request.getParameter("DscAction") != null ? request.getParameter("DscAction") : "";
                    int IdEstadoRq = Integer.parseInt(request.getParameter("IdEstadoRq") != null ? request.getParameter("IdEstadoRq") : "0");
                    int tipo_tarea = Integer.parseInt(request.getParameter("tipo_tarea") != null ? request.getParameter("tipo_tarea") : "0");
                    
                    rqs.GuardarAccionRequisicion(usuario, idReqTask, IdActionRq, DscAction, IdEstadoRq, tipo_tarea);
                    printlnResponse("{\"mensaje\":\"listo\"}", "application/json");

                } catch (Exception e) {
                    
                    System.out.println("Error en action -> GuardarOne: " + e.toString());
                    e.printStackTrace();
                    throw new Exception(e.getMessage());

                }                
                
            } else if (evento.equals("DELETE")) {

            } else if (evento.equals("UPDATE")) {

                try {                    
                    respuesta = guardarRequisicion(iduser,dstrct,evento);
                    response.getWriter().println(respuesta);
                   /* int ReqId = Integer.parseInt(request.getParameter("IdReq"));
                    respuesta = rqs.ActualizarRequisicion(dstrct, tiporeq, proceso, iduser, asunto, descripcion, prioridad, ReqId);
                    response.getWriter().println(respuesta);*/

                } catch (Exception e) {

                    System.out.println("Error en action ListaPoliticasReporte: " + e.toString());
                    e.printStackTrace();

                }

            } else if (evento.equals("UPDATE_RESPONSABLE")) {

                try {

                    int RqidResponsable = (request.getParameter("RqResponsable") != null) ? Integer.parseInt(request.getParameter("RqResponsable")) : 0;
                    String UserResponsable = (request.getParameter("UsuarioResponsable") != null) ? request.getParameter("UsuarioResponsable") : "";

                    respuesta = rqs.ActualizarRqResponsable(UserResponsable, RqidResponsable);
                    response.getWriter().println(respuesta);

                } catch (Exception e) {

                    System.out.println("Error en action Actualizando Responsable Requerimiento: " + e.toString());
                    e.printStackTrace();

                }

            } else if (evento.equals("VERCRONOGRAMA")) {

                respuesta = rqs.listarTareas(iduser);
                this.printlnResponse(respuesta, "application/json;");

            } else if (evento.equals("VERCRONO_PROCESO")) {

                String idProceso = request.getParameter("idproceso");
                int anio = (request.getParameter("anio") != null) ? Integer.parseInt(request.getParameter("anio")) : 0;
                int mes = (request.getParameter("mes") != null) ? Integer.parseInt(request.getParameter("mes")) : 0;
                respuesta = rqs.listarTareas(idProceso, anio, mes);
                this.printlnResponse(respuesta, "application/json;");

            } else if (evento.equals("GRILLAPRIORIDADREQUISICION")) {
                Gson gson = new Gson();
                respuesta = "{\"page\":1,\"rows\":" + gson.toJson(rqs.listarPrioridadRequisicion()) + "}";
                this.printlnResponse(respuesta, "application/json;");

            } else if (evento.equals("GUARDARPRIORIDADREQUISICION")) {
                String json = "{\"respuesta\":\"Guardado:)\"}";
                try {
                    String descripcionp = request.getParameter("descripcion");
                    String color = request.getParameter("color");
                    String usuarios = ((Usuario) request.getSession().getAttribute("Usuario")).getLogin();
                    respuesta = rqs.guardarPrioridadRequisicion(descripcionp, color, usuarios);
                    response.getWriter().println(respuesta);
                } catch (Exception e) {
                    json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
                }
            } else if (evento.equals("UPDATEPRIORIDADREQUISICION")) {
                String descripcionp = request.getParameter("descripcion");
                String color = request.getParameter("color");
                String id = request.getParameter("id");
                String usuarios = ((Usuario) request.getSession().getAttribute("Usuario")).getLogin();
                respuesta = rqs.editarPrioridadRequisicion(descripcionp, color, usuarios, id);
                response.getWriter().println(respuesta);

            } else if (evento.equals("CAMBIARESTADOPRIOREQ")) {
                String id = request.getParameter("id");
                String usuarios = ((Usuario) request.getSession().getAttribute("Usuario")).getLogin();
                respuesta = rqs.cambiarestadoPriRequisicion(usuarios, id);
                response.getWriter().println(respuesta);

            } else if (evento.equals("GRILLAESTADOREQUISICION")) {//***************************
                Gson gson = new Gson();
                respuesta = "{\"page\":1,\"rows\":" + gson.toJson(rqs.listarEstadoRequisicion()) + "}";
                this.printlnResponse(respuesta, "application/json;");

            } else if (evento.equals("GUARDARESTADOREQUISICION")) {
                String json = "{\"respuesta\":\"Guardado:)\"}";
                try {
                    String descripcionp = request.getParameter("descripcion");
                    String usuarios = ((Usuario) request.getSession().getAttribute("Usuario")).getLogin();
                    respuesta = rqs.guardarEstadoRequisicion(descripcionp, usuarios);
                    response.getWriter().println(respuesta);
                } catch (Exception e) {
                    json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
                }

            } else if (evento.equals("EDITARESTADOREQUISICION")) {
                String descripcionp = request.getParameter("descripcion");
                String id = request.getParameter("id");
                String usuarios = ((Usuario) request.getSession().getAttribute("Usuario")).getLogin();
                respuesta = rqs.editarEstadoRequisicion(descripcionp, usuarios, id);
                response.getWriter().println(respuesta);

            } else if (evento.equals("CAMBIARESTADOREQ")) {
                String id = request.getParameter("id");
                String usuarios = ((Usuario) request.getSession().getAttribute("Usuario")).getLogin();
                respuesta = rqs.cambiarestadoRequisicion(usuarios, id);
                response.getWriter().println(respuesta);

            } else if (evento.equals("GRILLATIPOTAREA")) {//***************************
                Gson gson = new Gson();
                respuesta = "{\"page\":1,\"rows\":" + gson.toJson(rqs.listarTipoTarea()) + "}";
                this.printlnResponse(respuesta, "application/json;");

            } else if (evento.equals("GUARDARTIPOTAREA")) {
                String json = "{\"respuesta\":\"Guardado:)\"}";
                try {
                    String descripcionp = request.getParameter("descripcion");
                    String usuarios = ((Usuario) request.getSession().getAttribute("Usuario")).getLogin();
                    respuesta = rqs.guardarTipoTarea(descripcionp, usuarios);
                    response.getWriter().println(respuesta);
                } catch (Exception e) {
                    json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
                }

            } else if (evento.equals("EDITARTIPOTAREA")) {
                String descripcionp = request.getParameter("descripcion");
                String id = request.getParameter("id");
                String usuarios = ((Usuario) request.getSession().getAttribute("Usuario")).getLogin();
                respuesta = rqs.editarTipoTarea(descripcionp, usuarios, id);
                response.getWriter().println(respuesta);

            } else if (evento.equals("CAMBIARESTADOTIPOTAREA")) {
                String id = request.getParameter("id");
                String usuarios = ((Usuario) request.getSession().getAttribute("Usuario")).getLogin();
                respuesta = rqs.cambiarestadoTipoTarea(usuarios, id);
                response.getWriter().println(respuesta);

            } else if (evento.equals("GRILLAESTADOTIPOTAREA")) {//***************************
                Gson gson = new Gson();
                respuesta = "{\"page\":1,\"rows\":" + gson.toJson(rqs.listarEstadoTipoTarea()) + "}";
                this.printlnResponse(respuesta, "application/json;");

            } else if (evento.equals("GUARDARESTADOTIPOTAREA")) {
                String json = "{\"respuesta\":\"Guardado:)\"}";
                try {
                    String descripcionp = request.getParameter("descripcion");
                    String usuarios = ((Usuario) request.getSession().getAttribute("Usuario")).getLogin();
                    respuesta = rqs.guardarEstadoTipoTarea(descripcionp, usuarios);
                    response.getWriter().println(respuesta);
                } catch (Exception e) {
                    json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
                }

            } else if (evento.equals("EDITARESTADOTIPOTAREA")) {
                String descripcionp = request.getParameter("descripcion");
                String id = request.getParameter("id");
                String usuarios = ((Usuario) request.getSession().getAttribute("Usuario")).getLogin();
                respuesta = rqs.editarEstadoTipoTarea(descripcionp, usuarios, id);
                response.getWriter().println(respuesta);

            } else if (evento.equals("CAMBIARESTTIPOTAREA")) {
                String id = request.getParameter("id");
                String usuarios = ((Usuario) request.getSession().getAttribute("Usuario")).getLogin();
                respuesta = rqs.cambiarestTipoTarea(usuarios, id);
                response.getWriter().println(respuesta);

            } else if (evento.equals("GRILLATIPOREQUISICION")) {//***************************
                Gson gson = new Gson();
                respuesta = "{\"page\":1,\"rows\":" + gson.toJson(rqs.listarTipoRequisicion()) + "}";
                this.printlnResponse(respuesta, "application/json;");

            } else if (evento.equals("GUARDARTIPOREQUISICION")) {
                String json = "{\"respuesta\":\"Guardado:)\"}";
                try {
                    String descripcionp = request.getParameter("nomtiporeq");
                    String eficacia = request.getParameter("eficacia");
                    String eficiencia = request.getParameter("eficiencia");
                    String usuarios = ((Usuario) request.getSession().getAttribute("Usuario")).getLogin();
                    respuesta = rqs.guardarTipoRequisicion(descripcionp, usuarios, eficacia, eficiencia);
                    response.getWriter().println(respuesta);
                } catch (Exception e) {
                    json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
                }

            } else if (evento.equals("EDITARTIPOREQUISICION")) {
                String descripcionp = request.getParameter("descripcion");
                String id = request.getParameter("id");
                String eficacia = request.getParameter("eficacia");
                String eficiencia = request.getParameter("eficiencia");
                String usuarios = ((Usuario) request.getSession().getAttribute("Usuario")).getLogin();
                respuesta = rqs.editarTipoRequisicion(descripcionp, usuarios, id, eficacia, eficiencia);
                response.getWriter().println(respuesta);

            } else if (evento.equals("CAMBIARESTADOTIPOREQ")) {
                String id = request.getParameter("id");
                String usuarios = ((Usuario) request.getSession().getAttribute("Usuario")).getLogin();
                respuesta = rqs.cambiarEstadoTipoRequisicion(usuarios, id);
                response.getWriter().println(respuesta);
            } else if (evento.equals("CONSULTAR_ARCHIVO")){
                    String idReq= (request.getParameter("IdReq") != null) ? request.getParameter("IdReq") : ""; 
                    String nomarchivo = (request.getParameter("nomarchivo") != null) ? request.getParameter("nomarchivo") : "";
   
                    boolean archivoCargado = rqs.almacenarArchivoEnCarpetaUsuario(idReq,iduser,nomarchivo);
                 
                    if (archivoCargado){
                        respuesta = nomarchivo;                      
                        response.getWriter().println(respuesta);
                    }else{
                        respuesta = ""; 
                        response.getWriter().println(respuesta);
                    }     
                } else if (evento.equals("BORRAR_ARCHIVO")){
                    String idReq= (request.getParameter("IdReq") != null) ? request.getParameter("IdReq") : ""; 
                    String nomarchivo = (request.getParameter("nomarchivo") != null) ? request.getParameter("nomarchivo") : "";
                    
                    boolean archivoBorrado = rqs.eliminarArchivo(idReq,nomarchivo);
                    respuesta =  (archivoBorrado) ? "OK" : "";
                    response.getWriter().println(respuesta);
                     
                }else if (evento.equals("VER_ACTIVIDADES_USUARIO")) {

                    String idProceso = request.getParameter("idproceso");
                    int idUsuario = (request.getParameter("idusuario") != null) ? Integer.parseInt(request.getParameter("idusuario")) : 0;
                    int anio = (request.getParameter("anio") != null) ? Integer.parseInt(request.getParameter("anio")) : 0;
                    int mes = (request.getParameter("mes") != null) ? Integer.parseInt(request.getParameter("mes")) : 0;
                    respuesta = rqs.listarActividadesUsuario(idProceso, idUsuario, anio, mes);
                    this.printlnResponse(respuesta, "application/json;");

                }

        } catch (Exception e) {

            System.out.println("Error en el action Requisicion: " + e.toString());
            e.printStackTrace();
            try {
                printlnResponse("{\"error\":\""+e.getMessage()+"\"}", "application/json");
            } catch (Exception ex) {
                Logger.getLogger(RequisicionEventosAction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

    private void printlnResponse(String respuesta, String contentType) throws Exception {

        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);
    }
    
    public String guardarRequisicion(String login, String dstrct, String accion) {
        
        int proceso=0, tiporeq=0, prioridad=0;
        String asunto="", descripcion="",num_req="";
        String respuesta = "", GeneradoPor=login, Co_partner="";
        
        try {
            if (request.getParameter("IdReq") != null) num_req = request.getParameter("IdReq");
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");//se consiguen los datos de db.properties

            String directorioArchivos = rb.getString("rutaImagenes") + "gestionadministrativa/requisiciones/";//se establece la ruta de la imagen
            
            //this.createDir(directorioArchivos );
            if (ServletFileUpload.isMultipartContent(request)) {

                ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
                List fileItemsList = servletFileUpload.parseRequest(request);

                //// Itero para obtener todos los FileItem
                Iterator it = fileItemsList.iterator();
                String nombre_archivo;
                String fieldtem = "", valortem = "", idxx = "";
                String tipitoxx = "requisicion";
             
                while (it.hasNext()) {
                    
                    FileItem fileItem = (FileItem) it.next();
                    
                    if ( (fileItem.isFormField()) ) {
                        
                        fieldtem = fileItem.getFieldName();                   
                        valortem = fileItem.getString();
                        
                        if (fieldtem.equals("proceso")){
                            proceso = Integer.parseInt(valortem);
                        }

                        if (fieldtem.equals("tipo_req")){
                            tiporeq = Integer.parseInt(valortem);
                        }

                        if (fieldtem.equals("asunto")){
                            asunto = valortem;                               
                        }

                        if (fieldtem.equals("descripcion")){
                            descripcion = valortem;
                        }
                        
                        if (fieldtem.equals("prioridad")){
                            prioridad = Integer.parseInt(valortem);                            
                        }
                        
                        if (fieldtem.equals("cmb_partners")){
                            Co_partner = valortem;
                        }

                        if (fieldtem.equals("cmb_generado_por")){
                            GeneradoPor = valortem;
                        }
                    }

                    if ( !(fileItem.isFormField()) ) {
                        
                        if ( num_req.equals("") && accion.equals("INSERT") ) num_req = rqs.InsertarRequisicion(dstrct, tiporeq, proceso, GeneradoPor, asunto, descripcion, prioridad, Co_partner);
                                             
                        //// Nombre del archivo en el cliente. Algunos navegadores (por ej. IE 6)
                        //// incluyen el path completo, lo que puede implicar separar path
                        //// de nombre.
                       
                        if (fileItem.getName() != "" && !fileItem.getName().isEmpty() && fileItem.getSize() > 0) {
                            
                            String nombreArchivo = fileItem.getName();

                            String[] nombrearreglo = nombreArchivo.split("\\\\");
                            String nombreArchivoremix = nombrearreglo[(nombrearreglo.length - 1)];
                            String rutaArchivo = directorioArchivos + num_req;
                            nombre_archivo = nombreArchivoremix;

                            this.createDir(rutaArchivo);

                            File archivo = new File(rutaArchivo + "/" + nombre_archivo);

                            if (archivo.exists()) {
                                fileItem.write(archivo);
                                idxx = "Actualizar";
                                // guardarRegistroEnBd(num_req, directorioArchivos, nombre_archivo, idxx, tipitoxx, login);
                            } else {
                                fileItem.write(archivo);
                                // guardarRegistroEnBd(num_req, directorioArchivos, nombre_archivo, idxx, tipitoxx, login);

                            }

                        }     
                      
                    }

                }     
            }
            if ( num_req.equals("") && accion.equals("INSERT") ) num_req = rqs.InsertarRequisicion(dstrct, tiporeq, proceso, GeneradoPor, asunto, descripcion, prioridad, Co_partner);
            if (accion.equals("UPDATE"))  rqs.ActualizarRequisicion(dstrct, tiporeq, proceso, login, asunto, descripcion, prioridad, Integer.parseInt(num_req));
            
            respuesta="OK";
        } catch (Exception ee) {
            respuesta="ER";
            System.out.println("eroror:" + ee.toString() + "__" + ee.getMessage());
        }finally{
           return respuesta;
        }
    }

  public void guardarRegistroEnBd(String num_req,String ruta,String nomArchivo,String idxx2,String tipito, String login){
        try{
          
            Dictionary fields   = new Hashtable();
            fields.put("actividad",     "023" );
            fields.put("tipoDocumento", "031"   );
            fields.put("documento",     num_req);
            fields.put("agencia",       "OP"   );
            fields.put("usuario",       login);
            fields.put("archivo", nomArchivo );

           this.insertRegistroArchivo(ruta,  fields,idxx2,tipito);


        }catch(Exception ee){
            System.out.println("eeeche"+ee.toString()+"_"+ee.getMessage());
        }
    }


    public String insertRegistroArchivo(String ruta,  Dictionary fields,String idxx3,String tipito) throws ServletException{
        String estado   = "";
        String fileName = (String)fields.get("archivo");
       
        try{

        model.negociosApplusService.insertRegistroArchivo(ruta,fields,idxx3,tipito,"");
        estado = "Archivo "+ fileName +" ha sido guardado <br>";

        }catch(Exception e){
            System.out.println("errorrcito"+e.toString()+"__"+e.getMessage());
         estado =  "Error al insertar registro en BD "+  fileName  +"<br>" + e.getMessage() ;
        }
        return estado;
  }
    
    public void createDir(String dir) throws Exception {
        try {
            File f = new File(dir);
            if (!f.exists()) {
                f.mkdir();
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

 
}
