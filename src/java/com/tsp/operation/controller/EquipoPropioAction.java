/********************************************************************
 *      Nombre Clase.................   EquipoPropioAction.java
 *      Descripci�n..................   Action para Equipos propios
 *      Autor........................   Osvaldo P�rez Ferrer
 *      Fecha........................   29 de Junio de 2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

/*
 * EquipoPropioAction.java
 *
 * Created on 29 de junio de 2006, 04:45 PM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  Osvaldo
 */

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.Util;

public class EquipoPropioAction extends Action{
    
    /** Creates a new instance of EquipoAction */
    public EquipoPropioAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "";
        HttpSession session = request.getSession();
        Usuario u = (Usuario) session.getAttribute("Usuario");
        String opc = request.getParameter("opcion");
        
        String placa = request.getParameter("placa");
        String clase = "";
        String num = request.getParameter("numero");
        
        
        try{
            if(opc.equals("add")){
                
                next = "/jsp/equipos/equipo_propio/equipoPropioIngresar.jsp";
                clase = request.getParameter("clase");
                
                placa = placa.toUpperCase();
                
                int eplaca = model.tablaGenService.existeRegistroCode("EQPROPIO",placa);
                int enumero = model.tablaGenService.existeRegistroDato("EQPROPIO",num);
                if(model.placaService.existePlaca(placa)){
                    if(eplaca == 0){
                        if(enumero == 0){
                            model.tablaGenService.agregarRegistro("EQPROPIO", placa, clase, "", u.getLogin(), num);
                            request.setAttribute("mensaje","Equipo "+placa+" agregado existosamente!");
                        }
                        else if(enumero == 1){
                            request.setAttribute("mensaje","Ya existe un equipo con numero "+num);
                        }
                        else{
                            model.tablaGenService.obtenerRegistro("EQPROPIO", "", num);
                            TablaGen t = model.tablaGenService.getTblgen();
                            model.tablaGenService.actualizarRegistro(t.getOid(), "EQPROPIO", placa, clase, "", u.getLogin(), num);
                        }
                    }
                    else if(eplaca == 1){
                        request.setAttribute("mensaje","Ya existe un equipo con placa "+placa);
                    }
                    else{
                        model.tablaGenService.obtenerRegistro("EQPROPIO", placa, "");
                        TablaGen t = model.tablaGenService.getTblgen();
                        model.tablaGenService.actualizarRegistro(t.getOid(), "EQPROPIO", placa, clase, "", u.getLogin(), num);
                    }
                }
                else{
                    request.setAttribute("mensaje","La placa "+placa+" no se encuentra registrada");
                }
            }
            else if(opc.equals("update")){
                
                clase = request.getParameter("clase");
                next = "/jsp/equipos/equipo_propio/equipoPropioModificar.jsp";
                model.tablaGenService.obtenerRegistro("EQPROPIO", placa, "");
                model.tablaGenService.buscarRegistrosOrdenados("EQPROPIO");
                model.tablaGenService.buscarRegistrosNoAnulados("CLAEQUI");
                next = "/jsp/equipos/equipo_propio/equipoPropioModificar.jsp";
                
            }
            else if(opc.equals("save")){
                model.tablaGenService.buscarRegistrosNoAnulados("CLAEQUI");
                model.tablaGenService.obtenerRegistro("EQPROPIO", placa, "");
                TablaGen t = model.tablaGenService.getTblgen();
                
                clase = request.getParameter("clase");
                next = "/jsp/equipos/equipo_propio/equipoPropioModificar.jsp";
                int enumero = model.tablaGenService.existeRegistroDato("EQPROPIO",num);
                
                if(enumero == 0 || (num.equals(t.getDato())) ){
                    model.tablaGenService.actualizarRegistro(t.getOid(), "EQPROPIO", placa, clase, "", u.getLogin(), num);
                    model.tablaGenService.obtenerRegistro("EQPROPIO", placa, "");
                    model.tablaGenService.buscarRegistrosNoAnulados("CLAEQUI");
                    request.setAttribute("mensaje", "Se modific� el equipo "+placa);
                    request.setAttribute("modified","");                    
                    request.setAttribute("clase", clase);
                }else if(enumero == 1){
                    request.setAttribute("mensaje", "Ya existe un equipo con n�mero "+num);
                }
                
            }
            else if(opc.equals("delete")){
                next = "/jsp/equipos/equipo_propio/equipoPropioModificar.jsp";
                
                model.tablaGenService.obtenerRegistro("EQPROPIO", placa, "");
                TablaGen t = model.tablaGenService.getTblgen();
                if(t.getOid().length() > 0){
                    String[] oid = new String[1];
                    oid[0] = t.getOid();
                    
                    model.tablaGenService.eliminarRegistros(oid);
                    request.setAttribute("mensaje", "Se ha eliminado el Equipo Epropio "+placa);
                    request.setAttribute("modified","");
                }
            }else if(opc.equals("filter")){
                clase = request.getParameter("clase");
                if(clase != null){
                    model.tablaGenService.obtenerEquipos(clase);
                    next = "/jsp/equipos/equipo_propio/equipoPropioListar.jsp";
                }
            }
            else if(opc.equals("filter_again")){                                
                   model.tablaGenService.buscarRegistrosNoAnulados("CLAEQUI");                        
                    next = "/jsp/equipos/equipo_propio/equipoPropioFiltro.jsp";                
            }
        }catch(SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
    
}
