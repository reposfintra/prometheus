/********************************************************************
 *  Nombre Clase.................   PrecintosDeleteAction.java
 *  Descripci�n..................   Action de la tabla precintos
 *  Autor........................   Ing. Leonardo Parody Ponce
 *  Fecha........................   22.12.2005
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
/**
 *
 * @author  Leonardo Parody Ponce
 */
public class PrecintosDeleteAction extends Action{
        
        /** Creates a new instance of PrecintosDeleteAction */
        public PrecintosDeleteAction() {
        }
        public void run() throws ServletException, InformationException {
                ////System.out.println("ESTOY EN EL ACTION");
                List precintos = new LinkedList();
                
                String next = "/jsp/trafico/precinto/PrecintosEliminar.jsp";
                String inicioS = request.getParameter("inicio");
                String finS = request.getParameter("fin");
                long inicio = Long.parseLong(inicioS);
                long fin = Long.parseLong(finS);
                int ini = Integer.parseInt(inicioS);
                int fi = Integer.parseInt(finS);
                String agencia = request.getParameter("agencia");
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario)session.getAttribute("Usuario");
                String base = usuario.getBase();
                String dstrct = usuario.getDstrct();
                String nombre = usuario.getNombre();
                String tipo_documento = request.getParameter("tipo_documento");
                ////System.out.println("inicio = "+inicio+"  fin = "+fin);
                int cont1 = 0;
                int cont2 = 0;
                String inicio1 = "";
                String inicio2 = "";
                String fin1 = "";
                String fin2 = "";
                
                try{   
                        String list="";
                        int cont = 0;
                        List series = model.precintosSvc.ExisteSerie(inicio, fin, tipo_documento);
                        for (int i=ini; i<=fi; i++){
                             cont = 0;
                             for (int j=0; j<series.size(); j++) {
                                     ////System.out.println("Precinto Existente = "+series.get(j));
                                     if (series.get(j).equals(""+i)){
                                         cont++;
                                     }
                             }
                             if (cont==0) {
                                     ////System.out.println("precinto inexistente = "+i);
                                list += ", "+i;
                                if(cont2 == 0){
                                    inicio2= ""+i;
                                    cont2++;
                                }else{
                                    fin2 = ""+i;
                                }
                             }
                        }
                        if ( (list.length() == 0) ) {
                                List series1 = model.precintosSvc.SerieAsignada(inicio, fin, tipo_documento);
                                for (int i=0; i<series.size(); i++){
                                        list += ", "+(String)series.get(i);
                                        if (cont1 == 0){
                                            inicio1 = (String)series.get(i);
                                            cont1++;
                                        }else{
                                            fin1 = (String)series.get(i);
                                        }
                                }
                                if ( series1.size() == 0 ) {
                                        for (long i=inicio; i<=fin; i++){
                                                model.precintosSvc.EliminarPrecinto(""+i, tipo_documento);
                                        }
                                        next +="?msg=Serie Eliminada Satisfactoriamente";
                                }else{
                                        next +="?msg=Los "+tipo_documento+"s : del "+inicio1+ " - "+fin1+" ya tienen planillas asignadas";
                                }
                        }else{
                                next +="?msg=Los "+tipo_documento+"s : del "+inicio2+ "  -  "+fin2+" no existen";
                        }

                 
                        ////System.out.println("agregue a la tabla");
                }catch (SQLException e){
                        e.printStackTrace();
                        throw new ServletException(e.getMessage());
                }
                 // Redireccionar a la p�gina indicada.
                this.dispatchRequest(next);
        }
        
}
