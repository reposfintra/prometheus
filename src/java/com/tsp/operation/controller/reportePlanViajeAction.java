/*
 * reportePlanViajeAction.java
 *
 * Created on 24 de agosto de 2006, 03:51 PM
 */
/********************************************************************
 *  Nombre Clase.................   reportePlanViajeAction.java
 *  Descripci�n..................   Action para generar reporte plan de viaje
 *  Autor........................   David Pi�a Lopez
 *  Fecha........................   24 de agosto de 2006
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import com.tsp.operation.model.threads.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;

/**
 *
 * @author  David
 */
public class reportePlanViajeAction extends Action {
    
    /** Creates a new instance of reportePlanViajeAction */
    public reportePlanViajeAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "";
        String fechaInicio = "";
        String fechaFin = "";
        String user="";
        boolean todas;
        try{
            String mensaje="Generaci�n de reporte Plan de viaje Iniciado.";
            HttpSession session = request.getSession ();
            Usuario usuario = (Usuario)session.getAttribute ("Usuario");
            user = usuario.getLogin();            
            String fechainicio = request.getParameter("fechai");
            String fechafin = request.getParameter("fechaf");
                            
            request.setAttribute("mensaje", mensaje);
            
            reportePlanDeViaje hilo = new reportePlanDeViaje();
            hilo.start( fechainicio, fechafin, user );
            
            next = "/jsp/trafico/ReportePlanViaje/reportePlandeViaje.jsp";
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest( next );
    }
    
}
