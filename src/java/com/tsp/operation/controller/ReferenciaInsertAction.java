/*************************************************************************
 * Nombre:        ReferenciaInsertAction.java          
 * Descripci�n:   Clase Action para modificar actividad    
 * Autor:         Ing. Henry Osorio 
 * Fecha:         26 de septiembre de 2005, 03:51 PM                             
 * Versi�n        1.0                                       
 * Coyright:      Transportes Sanchez Polo S.A.            
 *************************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.Util;

public class ReferenciaInsertAction extends Action{
    
    /** Creates a new instance of ReferenciaInsertAction */
    public ReferenciaInsertAction() {
    }
    public void run() throws ServletException {
        String ref = (request.getParameter("tiporeferencia")!=null)?request.getParameter("tiporeferencia"):"";
        String doc = (request.getParameter("documento")!=null)?request.getParameter("documento"):"";
        String tipo = (request.getParameter("ref")!=null)?request.getParameter("ref"):"";
        String opc = (request.getParameter("opcion")!=null)?request.getParameter("opcion"):"";
        String next = "/jsp/hvida/referencias/referenciaInsert.jsp?doc="+doc+"&tipo="+ref+"&ref="+tipo+"&msg=";
        String msg = "";
        String recargar = (request.getParameter("recargar")!=null)?request.getParameter("recargar"):"";
        Referencia referencia = new Referencia();
        try{
            if(opc.equals("LANZAR")){
                next+="&recargar="+recargar;
                model.tablaGenService.buscarRegistros("RELACION");
                model.ciudadService.searchTreMapCiudades();
                next = Util.LLamarVentana(next, "Creaci�n de Referencias");
            } else {
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario) session.getAttribute("Usuario");
                referencia.setClase(doc);
                referencia.setTiporef(ref);
                ////System.out.println("CLASe: "+referencia.getClase());
                referencia.setFecref(Util.getFechaActual_String(6));
                referencia.setDstrct_code((String)session.getAttribute("Distrito"));
                referencia.setBase(usuario.getBase());
                referencia.setCreation_user(usuario.getLogin());
                referencia.setCreation_date("now()");
                referencia.setNomempresa(request.getParameter("nombre"));
                referencia.setTelefono(request.getParameter("telefono"));
                referencia.setCiudadtel(request.getParameter("telciu"));
                referencia.setReferencia((request.getParameter("referencia")!=null)?request.getParameter("referencia"):"");
                referencia.setRelacion((request.getParameter("relacion")!=null)?request.getParameter("relacion"):"");
                referencia.setOtra_relacion((request.getParameter("otra_rel")!=null)?request.getParameter("otra_rel"):"");
                referencia.setDireccion((request.getParameter("direccion")!=null)?request.getParameter("direccion"):"");
                referencia.setCiudaddir((request.getParameter("ciudaddir")!=null)?request.getParameter("ciudaddir"):"");
                //request para empresa donde ha cargado propietario
                if(ref.equals("CO")){
                    model.referenciaService.insertarReferenciaConductorPropietario(referencia);
                    if( request.getParameter("emp").equals("OK") ){
                        referencia.setTiporef("EC");
                        referencia.setNomempresa(request.getParameter("nombreEC"));
                        referencia.setTelefono(request.getParameter("telefonoEC"));
                        referencia.setCiudadtel(request.getParameter("telciuEC"));
                        referencia.setDireccion(request.getParameter("direccionEC"));
                        referencia.setCiudaddir(request.getParameter("ciudaddirEC"));
                        referencia.setContacto(request.getParameter("contactoEC"));
                        referencia.setCargo_contacto(request.getParameter("cargoEC"));
                        referencia.setReferencia(request.getParameter("referenciaEC"));
                        model.referenciaService.insertarReferenciaEmpresaCarga(referencia);
                    }
                    msg = "Referencia agregada exitosamente";
                }
                else if (ref.equals("EA")) {
                    referencia.setContacto(request.getParameter("contacto"));
                    referencia.setCargo_contacto(request.getParameter("cargo"));
                    if (model.referenciaService.existeEmpresaAfiliada(referencia)>=1) {
                        msg = "ya existe una referencia para este vehiculo";
                    }else {
                        model.referenciaService.insertarReferenciaEmpresaAfiliada(referencia);
                        msg = "Referencia agregada exitosamente";
                    }
                }
                else if (ref.equals("PR")) {
                    model.referenciaService.insertarReferenciaConductorPropietario(referencia);
                    msg = "Referencia agregada exitosamente";
                }
            }
            next+=msg;
        }catch (Exception e){
            e.printStackTrace();
            //throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}
