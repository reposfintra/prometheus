/*
 * PropietarioUpdateAction.java
 *
 * Created on 15 de junio de 2005, 02:28 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

/**
 *
 * @author  Sandrameg
 */
public class PropietarioUpdateAction extends Action {
    
    /** Creates a new instance of PropietarioUpdateAction */
    public PropietarioUpdateAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/propietarios/propietarioUpdate.jsp";
        
        String dstrct = request.getParameter("dstrct").toUpperCase();
        String cedula = request.getParameter("ced").toUpperCase();
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");        
        
        try{    
            
            model.propService.buscarxCedulaxDistrito(cedula,dstrct);
            
            if(model.propService.getPropietario() != null){
                Propietario p = new Propietario();
                p.setCedula(cedula);
                p.setP_nombre(request.getParameter("pnombre").toUpperCase());
                p.setS_nombre(request.getParameter("snombre").toUpperCase());
                p.setP_apellido(request.getParameter("papellido").toUpperCase());
                p.setS_apellido(request.getParameter("sapellido").toUpperCase());
                p.setdstrct(dstrct);
                p.setUser_update(usuario.getLogin());
                
                model.propService.update(p);

                request.setAttribute("prop", p);
                next = next + "?msg=exitoM";            
            }            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
