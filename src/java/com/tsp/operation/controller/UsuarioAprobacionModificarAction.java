/*****************************************************************
 * Nombre:        UsuarioAprobacionInsertarAction.java
 * Descripci�n:   Clase Action para insertar Usuario aprobacion    
 * Autor:         Ing. Diogenes Antonio Bastidas Morales   
 * Fecha:         18 de enero de 2006, 05:11 PM                                
 * Versi�n        1.0                                       
 * Coyright:      Transportes Sanchez Polo S.A.            
 ********************************************************************/


package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
 
public class UsuarioAprobacionModificarAction extends Action {
    
    /** Creates a new instance of UsuarioAprobacionModificarAction */
    public UsuarioAprobacionModificarAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
         HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
        String distrito = (String) session.getAttribute("Distrito");
        String next = "/jsp/general/usuarioAprobacion/usuarioAprobacion.jsp?sw=1";
        Date fecha = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String now = format.format(fecha);
        int sw=0;
        
        UsuarioAprobacion usuapro = new UsuarioAprobacion();
        usuapro.setId_agencia(request.getParameter("agencia"));
        usuapro.setTabla(request.getParameter("tabla").toUpperCase());
        usuapro.setUsuario_aprobacion(request.getParameter("usuario"));
        usuapro.setId_agenciaAct(request.getParameter("agenciaAct"));
        usuapro.setTablaAct(request.getParameter("tablaAct").toUpperCase());
        usuapro.setUsuario_aprobacionAct(request.getParameter("usuarioAct"));
        usuapro.setUser_update(usuario.getLogin());
        usuapro.setLast_update(now);
        usuapro.setCreation_user(usuario.getLogin());
        usuapro.setCreation_date(now);
        usuapro.setDstrct(usuario.getDstrct());
        usuapro.setBase(usuario.getBase());

        try{
            if(!model.usuaprobacionService.existeUsuariosAprobacion(request.getParameter("agencia"),request.getParameter("tabla").toUpperCase(), request.getParameter("usuario")) ){
                model.usuaprobacionService.modificarUsuario(usuapro);
                model.usuaprobacionService.buscarUsuarioAprobacion(request.getParameter("agencia"),request.getParameter("tabla").toUpperCase(), request.getParameter("usuario"));
                next = next+"&mensaje=Usuario aprobacion Modificado exitosamente!";
            }
            else{
                 next = next+"&mensaje=Usuario aprobacion ya existe!";
            }
        }catch (SQLException e){
            
        }
        this.dispatchRequest(next);
    }
    
}
