/********************************************************************
 *      Nombre Clase.................   InformeFacturacionGenerarAction.java
 *      Descripci�n..................   Anula un registro en la tabla tblapl
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   13.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.Util;

public class InformeFacturacionGenerarAction extends Action{
    
    /** Creates a new instance of DocumentoInsertAction */
    public InformeFacturacionGenerarAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        //Pr�xima vista
        //String pag  = "/informes/informeFacturacion2.jsp";
        String pag  = "/informes/informeFacturacionResumen.jsp";
        String next = "";
        
        //Usuario en sesi�n
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        try{
            String fechai = request.getParameter("fechai");
            String fechaf = request.getParameter("fechaf");
            String ruta = request.getParameter("ruta");
            
            String rutaparam = ruta;
            
            if ( ruta.matches("TR") ){
                rutaparam = "127%";
            }
            
            ////System.out.println("�������������������> ruta: " + ruta);
            
            //fechai += " 7:00:00.0";
            //fechaf = Util.fechaFinal(fechaf + " 6:59:00", 1);
            
            Vector vec = model.planillaService.obtenerInformeFacturacion(fechai, fechaf, rutaparam);
            
            Vector resumen = this.generarResumen(vec, fechai, fechaf);
            
            session.setAttribute("InformeFact", vec);
            session.setAttribute("fechaiIFact", fechai);
            session.setAttribute("fechafIFact", fechaf);
            session.setAttribute("resumenFact", resumen);
            
            next = com.tsp.util.Util.LLamarVentana(pag, "Informe para Facturaci�n");
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
    private Vector generarResumen(Vector informe, String fechai, String fechaf) throws SQLException{
        Vector resum = new Vector();
        Vector res = new Vector();
        
        double tonelaje = 0;
        double tonelaje_ttl = 0;
        long viajes_ttl = 0;
        long viajes_d = 0;
        long viajes = 0;
        
        for( int i= 0; i< informe.size(); i++){
            InformeFact info = (InformeFact) informe.elementAt(i);
            Vector linea = info.getRemisiones();
            
            tonelaje = 0;
            viajes = 0;
            Hashtable ht = new Hashtable();
            
            String std_job = info.getStd_job();
            model.stdjobdetselService.buscaStandard(std_job);
            Stdjobdetsel stdj = model.stdjobdetselService.getStandardDetSel();
            String desc = stdj.getSj_desc();
            model.tablaGenService.setTblgen(null);
            model.tablaGenService.buscarDatos("ALIASSJ", std_job);
            desc = model.tablaGenService.getTblgen()!=null ? 
                model.tablaGenService.getTblgen().getDescripcion() : "NR";
            desc = desc.toLowerCase().indexOf("carbon ")!=-1 ? "<br>" + desc.substring(7) : desc;    
            
            int col = 0;
            
            ht.put("sj_no", std_job);
            ht.put("sj_desc", desc);
            
            for( int j=0; j<linea.size(); j++){
                Vector obj = (Vector) linea.elementAt(j);
                Vector rems = (Vector) obj.elementAt(0);
                String tonelaje_d = (String) obj.elementAt(1);
                
                viajes += rems.size();
                viajes_d = rems.size();
                
                col = 0;
                String[] fechar = {"",""};
                for( int k=0; k<rems.size(); k++){
                    RemisionIFact remision = new RemisionIFact();
                    remision = (RemisionIFact) rems.elementAt(k);
                    tonelaje += remision.getTonelaje().doubleValue();
                    fechar = remision.getFecha().split(" ");
                    ht.put(fechar[0], "" + com.tsp.util.Util.redondear( Double.valueOf(tonelaje_d).doubleValue() , 2));
                    break;
                }
                
                //ht.put("ton", "" + com.tsp.util.Util.redondear( Double.valueOf(tonelaje_d).doubleValue() , 2));
                
                tonelaje_ttl += Double.valueOf(tonelaje_d).doubleValue();//tonelaje;
                viajes_ttl += viajes;
            }
            
            res.add(ht);
            
        }
        
        if( informe.size()>0 ){
            String fecha0 = fechai;
            String fecha1 = com.tsp.util.UtilFinanzas.addFecha(fechaf, 1, 2);
            
            Vector linea = new Vector();
            for( int i=0; i<res.size(); i++){
                Hashtable ht = (Hashtable) res.elementAt(i);
                if(i == 0) linea.add("&nbsp;");
                String desc = ht.get("sj_desc").toString();
                linea.add(ht.get("sj_no").toString() + " " + (desc.length()>18 ? desc.substring(0, 18) : desc));
            }
            resum.add(linea);
            
            fecha0 = fechai;
            fecha1 = com.tsp.util.UtilFinanzas.addFecha(fechaf, 1, 2);
            do{
                linea = new Vector();
                linea.add(fecha0);
                for( int i=0; i<res.size(); i++){
                    Hashtable ht = (Hashtable) res.elementAt(i);
                    linea.add(ht.get(fecha0)!=null? ht.get(fecha0).toString() : "0.0");
                }
                resum.add(linea);
                fecha0= com.tsp.util.UtilFinanzas.addFecha(fecha0, 1, 2);
            }while( Integer.parseInt(fecha0.replaceAll("-",""))<Integer.parseInt(fecha1.replaceAll("-","")) );
        }
        
        return resum;
    }
    
}
