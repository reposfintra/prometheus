/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.controller;

import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.services.Smservice;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;

/**
 *
 * @author jpinedo
 */

public class SmsAction extends Action {

    public void run(){
        String next = "/jsp/sms/reporte_sms.jsp";
        String opcion = "";
        String cadenatabla = "";
        String img="<img src='" + request.getContextPath() +"/images/images.jpg' width='15' height='15' align='left' title='Ver Historial ' />";
        Smservice smsserv = null;
        boolean redirect = false;
        List   lista_sms   = new LinkedList();
        try {
            opcion = request.getParameter("opcion")!= null ? request.getParameter("opcion"):"0";
             String   fecha_inicial = (request.getParameter("fecha_inicial")==null)?"": request.getParameter("fecha_inicial") ;
             String   fecha_final = (request.getParameter("fecha_final")==null)?"": request.getParameter("fecha_final") ;
             String   nit = (request.getParameter("nit")==null)?"": request.getParameter("nit") ;
             String   cell = (request.getParameter("cell")==null)?"": request.getParameter("cell") ;
             String   tipo = (request.getParameter("tipo")==null)?"": request.getParameter("tipo") ;
             String   estado = (request.getParameter("estado_sms")==null)?"": request.getParameter("estado_sms") ;


            HttpSession sesion = request.getSession();
            Usuario user = (Usuario)sesion.getAttribute("Usuario");
            smsserv = new Smservice(user.getBd());

            int op=Integer.parseInt(opcion);
            switch (op)
            {
                 case 1:/*listar sms*/
                    sesion.removeAttribute("lista_sms");
                    redirect=true;
                    lista_sms =smsserv.ListarSms(nit, cell, estado, tipo, fecha_inicial, fecha_final);
                    //next="/jsp/finanzas/contab/gestion_de_hc/GestionHc.jsp?msj="+"xx";
                   sesion.setAttribute("lista_sms",lista_sms);
                break;

              
            }
            
                if(redirect==true) this.dispatchRequest(next);
        }
        catch (Exception e) {
            try {
                throw new Exception("Error al generar el archivo xls: " + e.toString());
            } catch (Exception ex) {
                Logger.getLogger(SmsAction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        //System.out.println("fin proceso xls");
    }

}
