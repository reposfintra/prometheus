/*
 * OpcionesReportesAction.java
 *
 * Created on 31 de julio de 2005, 12:18
 */

package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
/**
 *
 * @author  mario
 */
public class OpcionesReportesAction extends Action {
    
    /** Creates a new instance of OpcionesReportesAction */
    public OpcionesReportesAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        try{
            String Opcion   = coalesce(request.getParameter("Opcion"),"");
            String Distrito = coalesce(request.getParameter("Distrito"),"");
            String Agencia  = coalesce(request.getParameter("Agencia"),"");
            String Cliente  = coalesce(request.getParameter("Cliente"),"");
            String Estandar = coalesce(request.getParameter("Estandar"),"");
            String Ano      = coalesce(request.getParameter("Ano"),"");
            String Mes      = coalesce(request.getParameter("Mes"),"");
            String Tipo     = coalesce(request.getParameter("Tipo"),"");
            
            String next = "/jsp/jsppto/ReportesPto.jsp";
            if (Tipo!=null){
                if (Tipo.equals("0")) next = "/jsp/jsppto/ReportesAgencia.jsp";
                if (Tipo.equals("1")) next = "/jsp/jsppto/ReportesCliente.jsp";
                if (Tipo.equals("2")) next = "/jsp/jsppto/ReportesStandar.jsp";
                if (Tipo.equals("3")) next = "/jsp/masivo/reportes/CostosOperativos/ReportesStandar.jsp";
                if (Tipo.equals("4")) next = "/jsp/masivo/reportes/CostosOperativos/ReportesCostosOperativos.jsp";
            }
            
            
            if (Opcion!=null){
                if (Opcion.equals("Generar")){
  //                  model.ReportesSvc.Presupuestado_VS_Ejecutado(Distrito, Agencia, Cliente, Estandar, Ano, Mes);
                }
                if (Opcion.equals("GenerarCostos")){
  //                  model.ReportesSvc.PresupuestoCostosOperativos(Distrito, Agencia, Cliente, Estandar, Ano, Mes);
                }
                
            }
            
            
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if (rd == null) 
                throw new ServletException ("\nNo se pudo encontrar " + next);
            rd.forward(request, response);
            
        }catch (Exception ex){
            throw new ServletException ("\nError en OpcionesReporteAction ...\n"+ex.getMessage());
        }
    }
    
    public String coalesce(String str1, String str2){
        return (str1==null?str2:str1);
    }
    
}
