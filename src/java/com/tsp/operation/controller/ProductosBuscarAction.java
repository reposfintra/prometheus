/*
 * ProductosBuscarAction.java
 *
 * Created on 30 de julio de 2009, 14:48
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.services.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  Rhonalf
 */
public class ProductosBuscarAction extends Action {
    
    /** Creates a new instance of ProductosBuscarAction */
    public ProductosBuscarAction() { 
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String parametro ="";
        int filtro=0;
        String next  = "/jsp/provintegral/compras/producto_resultado.jsp";
        request.setAttribute("msg", "Producto buscado!");
        MaterialesService pserv = new MaterialesService();
        try{
            parametro = request.getParameter("parametro")!=null? request.getParameter("parametro"):"";
            filtro = request.getParameter("filtro")!=null? Integer.parseInt(request.getParameter("filtro")):0;
            pserv.buscarPor(filtro, parametro);
            request.setAttribute("resultado",pserv.getResultado()); 
        }
        catch(Exception ec){
            System.out.println("Error en el run del action: "+ec.toString());
        }
        this.dispatchRequest(next);
    }
    
}
