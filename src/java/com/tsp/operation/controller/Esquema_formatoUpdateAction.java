/*******************************************************************
 * Nombre clase: Esquema_formatoUpdateAction.java
 * Descripci�n: Accion para actualizar un acuerdo especial a la bd.
 * Autor: Ing. Jose de la rosa
 * Fecha: 19 de octubre de 2006, 10:31 AM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/


package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;


/**
 *
 * @author  EQUIPO13
 */
public class Esquema_formatoUpdateAction extends Action{
    
    /** Creates a new instance of Esquema_formatoUpdateAction */
    public Esquema_formatoUpdateAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next = "/jsp/masivo/formato/Esquema_formatoModificar.jsp";
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String dstrct = (String) session.getAttribute ("Distrito");
        try{
            String mensaje = request.getParameter ("opcion");
            if(mensaje!=null){
                String codigo_programa = request.getParameter ("c_codigo");
                String nombre_campo = request.getParameter ("c_nombre");
                if(mensaje.equalsIgnoreCase ("modificar")){
                    String titulo = request.getParameter ("c_titulo");
                    String tipo = request.getParameter ("c_tipo");
                    int inicio = Integer.parseInt ( request.getParameter ("c_pos_inicial").equals ("")?"0":request.getParameter ("c_pos_inicial") );
                    int fin = Integer.parseInt ( request.getParameter ("c_pos_final").equals ("")?"0":request.getParameter ("c_pos_final") );
                    int orden = Integer.parseInt ( request.getParameter ("c_orden").equals ("")?"0":request.getParameter ("c_orden") );
                    if( !model.esquema_formatoService.ordenEsquema_formato ( dstrct.toUpperCase (), codigo_programa, nombre_campo, orden ) ){
                        if( !model.esquema_formatoService.rangoEsquema_formato (dstrct.toUpperCase (), codigo_programa, nombre_campo, inicio, fin) ){
                            Esquema_formato esquema = new Esquema_formato ();
                            esquema.setCodigo_programa (codigo_programa);
                            esquema.setNombre_campo (nombre_campo);
                            esquema.setPosicion_final (fin);
                            esquema.setPosicion_inicial (inicio);
                            esquema.setOrden (orden);
                            esquema.setTitulo (titulo);
                            esquema.setTipo (tipo);
                            esquema.setDistrito (dstrct.toUpperCase ());
                            esquema.setUsuario (usuario.getLogin ().toUpperCase ());
                            request.setAttribute ("mensaje","La informaci�n ha sido modificada exitosamente!");
                            model.esquema_formatoService.updateEsquema_formato (esquema);
                            next += "?reload=ok";
                        }else
                            request.setAttribute ("mensaje","Error el la posici�n inicial o final, esta dentro de un rango ya grabado!");
                    }else
                        request.setAttribute ("mensaje","Error el orden ya existe!");
                    model.esquema_formatoService.searchEsquema_formato (codigo_programa, dstrct.toUpperCase (), nombre_campo);
                    model.tablaGenService.buscarRegistros ("TFORTIP");
                    request.setAttribute ( "set_tipo", model.tablaGenService.obtenerTablas ());
                    model.tablaGenService.buscarRegistros ("TBLFORMA");
                }
                else{
                    Esquema_formato esquema = new Esquema_formato ();
                    esquema.setCodigo_programa (codigo_programa);
                    esquema.setNombre_campo (nombre_campo);
                    esquema.setDistrito (dstrct.toUpperCase ());
                    esquema.setUsuario (usuario.getLogin ().toUpperCase ());
                    model.esquema_formatoService.anularEsquema_formato (esquema);
                    next="/jsp/trafico/mensaje/MsgAnulado.jsp";
                }
            }
        }catch (SQLException e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
        
    }
    
}
