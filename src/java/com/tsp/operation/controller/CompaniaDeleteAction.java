/*
 * CompaniaDeleteAction.java
 *
 * Created on 21 de enero de 2005, 10:48 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  KREALES
 */
public class CompaniaDeleteAction extends Action{
    
    /** Creates a new instance of CompaniaDeleteAction */
    public CompaniaDeleteAction() {
    }
    
    public void run() throws ServletException,InformationException{
        
        String next="/compania/companiaDelete.jsp?men=";
        String dstrct               =   request.getParameter("dstrct").toUpperCase();
        try{
            
            model.ciaService.buscarCia(dstrct);
            
            if(request.getParameter("anular")!=null){
                
                
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario) session.getAttribute("Usuario");
                
                String user_update          =   usuario.getLogin();
                
                Compania cia = model.ciaService.getCompania();
                cia.setuser_update(user_update);
                model.ciaService.setCia(cia);                 
                model.ciaService.anularCia(cia);
                next="/compania/companiaDelete.jsp?men=Modificacion exitosa&mensaje=El distrito "+dstrct+" ha sido anulado correctamente!";
                
            }
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
