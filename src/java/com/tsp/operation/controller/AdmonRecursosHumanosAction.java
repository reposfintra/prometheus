/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.AdmonRecursosHumanosDAO;
import com.tsp.operation.model.DAOS.impl.AdmonRecursosHumanosImpl;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.Usuario;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import com.tsp.operation.model.beans.POIWrite;
import com.tsp.util.ExcelApiUtil;
import com.tsp.util.Util;
import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import com.google.gson.JsonParser;
import fintra.apache.poi.POIXMLException;
import java.io.IOException;
import java.sql.SQLException;

/**
 *
 * @author dvalencia
 */
public class AdmonRecursosHumanosAction extends Action {

    Usuario usuario = null;

    private AdmonRecursosHumanosDAO dao;

    POIWrite xls;
    private int fila = 0;
    HSSFCellStyle header, titulo1, titulo2, titulo3, titulo4, titulo5, letra, numero, dinero, dinero2, numeroCentrado, porcentaje, letraCentrada, numeroNegrita, ftofecha;
    private SimpleDateFormat fmt;
    String filename;
    String rutaInformes;
    String nombre;
    List listado;
    String path;
    String reponseJson = "{}";

    private final int CARGAR_TIPO_PROVEEDOR = 1;
    private final int GUARDAR_TIPO_PROVEEDOR = 2;
    private final int ACTUALIZAR_TIPO_PROVEEDOR = 3;
    private final int CAMBIAR_ESTADO_TIPO_PROVEEDOR = 4;
    private final int CARGAR_EPS = 5;
    private final int GUARDAR_EPS = 6;
    private final int ACTUALIZAR_EPS = 7;
    private final int CAMBIAR_ESTADO_EPS = 8;
    private final int CARGAR_ARL = 9;
    private final int GUARDAR_ARL = 10;
    private final int ACTUALIZAR_ARL = 11;
    private final int CAMBIAR_ESTADO_ARL = 12;
    private final int CARGAR_AFP = 13;
    private final int CARGAR_AFP_CES = 154;
    private final int GUARDAR_AFP = 14;
    private final int ACTUALIZAR_AFP = 15;
    private final int CAMBIAR_ESTADO_AFP = 16;
    private final int CARGAR_CCF = 17;
    private final int GUARDAR_CCF = 18;
    private final int ACTUALIZAR_CCF = 19;
    private final int CAMBIAR_ESTADO_CCF = 20;
    private final int CARGAR_DEPENDENCIAS = 21;
    private final int GUARDAR_DEPENDENCIAS = 22;
    private final int ACTUALIZAR_DEPENDENCIAS = 23;
    private final int CAMBIAR_ESTADO_DEPENDENCIAS = 24;
    private final int CARGAR_CARGOS = 25;
    private final int GUARDAR_CARGOS = 26;
    private final int ACTUALIZAR_CARGOS = 27;
    private final int CAMBIAR_ESTADO_CARGOS = 28;
    private final int CARGAR_TIPOS_CONTRATO = 29;
    private final int GUARDAR_TIPOS_CONTRATO = 30;
    private final int ACTUALIZAR_TIPOS_CONTRATO = 31;
    private final int CAMBIAR_ESTADO_TIPOS_CONTRATO = 32;
    private final int CARGAR_TIPOS_RIESGOS = 33;
    private final int GUARDAR_TIPOS_RIESGOS = 34;
    private final int ACTUALIZAR_TIPOS_RIESGOS = 35;
    private final int CAMBIAR_ESTADO_TIPOS_RIESGOS = 36;
    private final int CARGAR_EMPLEADOS = 37;
    private final int GUARDAR_EMPLEADOS = 38;
    private final int ACTUALIZAR_EMPLEADOS = 39;
    private final int CAMBIAR_ESTADO_EMPLEADOS = 40;
    private final int CARGAR_TIPO_DOCUMENTO = 41;
    private final int CARGAR_ESTADO_CIVIL = 42;
    private final int CARGAR_DPTO_EXP = 43;
    private final int CARGAR_CIUDAD_EXP = 44;
    private final int CARGAR_NIVEL_ESTUDIO = 45;
    private final int CARGAR_DPTO_NAC = 46;
    private final int CARGAR_CIUDAD_NAC = 47;
    private final int CARGAR_NIVELES_JERARQUICOS = 48;
    private final int GUARDAR_NIVELES_JERARQUICOS = 49;
    private final int ACTUALIZAR_NIVELES_JERARQUICOS = 50;
    private final int CAMBIAR_ESTADO_NIVELES_JERARQUICOS = 51;
    private final int CARGAR_PROFESIONES = 52;
    private final int GUARDAR_PROFESIONES = 53;
    private final int ACTUALIZAR_PROFESIONES = 54;
    private final int CAMBIAR_ESTADO_PROFESIONES = 55;
    private final int CARGAR_DPTO_RES = 56;
    private final int CARGAR_CIUDAD_RES = 57;
    private final int CARGAR_BANCOS = 58;
    private final int CARGAR_HV = 59;
    private final int VER_ARCHIVOS = 60;
    private final int MOSTRAR_ARCHIVOS = 61;
    private final int CARGAR_MACROPROCESOS = 62;
    private final int CARGAR_PROCESOS = 63;
    private final int CARGAR_LINEAS_NEGOCIO = 64;
    private final int CARGAR_PRODUCTOS = 65;
    private final int CARGAR_TIPO_PROVEEDOR2 = 66;
    private final int EXPORTAR_EXCEL = 67;
    private final int CARGAR_INCAPACIDADES = 68;
    private final int CARGAR_TIPOS_NOVEDAD = 69;
    private final int CARGAR_ENFERMEDADES = 70;
    private final int CARGAR_RAZON = 71;
    private final int CARGAR_NOMBRE = 72;
    private final int CARGAR_JEFESDIRECTOS = 73;
    private final int GUARDAR_NOVEDADES_INCAPACIDAD = 74;
    private final int ACTUALIZAR_NOVEDADES_INCAPACIDAD = 75;
    private final int CAMBIARESTADO_NOVEDAD = 76;
    private final int GUARDAR_SOLICITUD_VACACIONES = 77;
    private final int APROBAR_SOLICITUD_VACACIONES = 78;
    private final int RECHAZAR_SOLICITUD_VACACIONES = 79;
    private final int VISTOBUENO_NOVEDADES_INCAPACIDAD = 80;
    private final int CARGAR_ARCHIVO_NOVEDAD = 81;
    private final int VER_ARCHIVOS_NOVEDAD = 82;
    private final int MOSTRAR_ARCHIVOS_NOVEDAD = 83;
    private final int MARCAR_NOVEDAD_TRAMITADA = 84;
    private final int CALCULAR_DIAS_SIN_FESTIVOS = 85;
    private final int MARCAR_NOVEDAD_NO_TRAMITE = 86;
    private final int CARGAR_TIPOS_HORAS_EXTRAS = 87;
    private final int CARGAR_HORAS_EXTRAS = 88;
    private final int GUARDAR_HORAS_EXTRAS = 89;
    private final int ACTUALIZAR_HORAS_EXTRAS = 90;
    private final int CAMBIARESTADO_HORAS_EXTRAS = 91;
    private final int CARGAR_HORAS_EXTRAS_PORAPROBAR = 92;
    private final int APROBAR_HORAS_EXTRAS = 93;
    private final int CARGAR_HORAS_EXTRAS_APROBADAS = 94;
    private final int VISTOBUENO_HORAS_EXTRAS = 95;
    private final int CARGAR_OPCIONES_MENU = 96;
    private final int CARGAR_TIPO_INCAPACIDAD = 97;
    private final int CARGAR_ENTIDAD_OTORGA_INCAPACIDAD = 98;
    private final int CARGAR_OPCIONES_MENU_GESTION = 99;
    private final int CARGAR_INCAPACIDADES2 = 100;
    private final int CARGAR_OPCIONES_MENU_VISTOBUENO = 101;
    private final int CARGAR_LISTA_ENFERMEDADES = 102;
    private final int CARGAR_NUMERO_SOLICITUD_NOVEDAD = 103;
    private final int CARGAR_PROCESO_ACTUAL_TRABAJADOR = 104;
    private final int CARGAR_PROCESOS_ASIGNACION = 105;
    private final int CARGAR_TRAZABILIDAD_EMPLEADO = 106;
    private final int CARGAR_INCAPACIDADES3 = 107;
    private final int CARGAR_TIPO_VACACIONES = 108;
    private final int CARGAR_VACACIONES = 109; 
    private final int CALCULAR_DIAS_FESTIVOS = 110;
    private final int CARGAR_SALDO = 111;
    private final int CARGAR_PASIVO_VACACIONAL = 112;
    private final int MOSTRAR_FECHA_INGRESO = 113;
    private final int MOSTRAS_EPS = 114;
    private final int MOSTRAS_ARL = 115;
    private final int CARGAR_SALARIO = 116;
    //private final int ACTUALIZAR_PERIODO_ACTUAL = 117;
    private final int CARGAR_VACACIONES_POR_APROBAR = 118;
    private final int CARGAR_VACACIONES_POR_VISTO_BUENO = 119;
    private final int VISTOBUENO_NOVEDADES_VACACIONES = 120;
    private final int EXPORTAR_EXCEL_PASIVO = 121;
    private final int EXPORTAR_EXCEL_ACUMULADO = 122;
    private final int MOSTRAR_PERIODO = 123;
    private final int CARGAR_PERMISOS = 124;
    private final int CARGAR_TIPO_PERMISOS = 125;
    private final int GUARDAR_NOVEDADES_PERMISOS = 126;
    private final int CARGAR_PERMISOS_POR_APROBAR = 127;
    private final int APROBAR_SOLICITUD_PERMISOS = 128;
    private final int RECHAZAR_SOLICITUD_PERMISOS = 129;
    private final int CARGAR_PERMISOS_POR_VISTO_BUENO = 130;
    private final int VISTOBUENO_NOVEDADES_PERMISOS = 131;
    private final int CARGAR_CAUSALES_RETIRO = 132;
    private final int CARGAR_BARRIOS = 133;
    private final int CARGAR_TIPO_LICENCIAS = 134;
    private final int CARGAR_CLASES_PERMISO = 135;
    private final int GUARDAR_NOVEDADES_LICENCIAS = 136;
    private final int CARGAR_CLASES_LICENCIA = 137;
    private final int CARGAR_LICENCIAS = 138;
    private final int CARGAR_LICENCIAS2 = 139;
    private final int CARGAR_LICENCIAS_POR_VISTOBUENO = 140;
    private final int VISTOBUENO_NOVEDADES_LICENCIAS = 141;
    private final int CARGAR_TIPO_OTRAS_LICENCIAS = 142;
    private final int CARGAR_OTRAS_LICENCIAS = 143;
    private final int GUARDAR_NOVEDADES_OTRAS_LICENCIAS = 144;
    private final int CARGAR_OTRAS_LICENCIAS_POR_APROBAR = 145;
    private final int APROBAR_SOLICITUD_LICENCIAS = 146;
    private final int RECHAZAR_SOLICITUD_LICENCIAS = 147;
    private final int CARGAR_OTRAS_LICENCIAS_POR_VISTOBUENO = 148;
    private final int EXPORTAR_INCAPACIDADES = 149;
    private final int EXPORTAR_VACACIONES = 150;
    private final int EXPORTAR_PERMISOS = 151;
    private final int EXPORTAR_LICENCIAS = 152;
    private final int EXPORTAR_OTRAS_LICENCIAS = 153;
    private final int GENERAR_ARCHIVO_DISTRIBUCION = 155;
    private final int EDITAR_NOVEDADES = 156;
    private final int ANULAR_NOVEDADES = 157;
   

    @Override
    public void run() throws ServletException, InformationException {
        try {

            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            dao = new AdmonRecursosHumanosImpl(usuario.getBd());
            int opcion = (request.getParameter("opcion") != null ? Integer.parseInt(request.getParameter("opcion")) : -1);
            switch (opcion) {
                case CARGAR_TIPO_PROVEEDOR:
                    cargarTipoProveedor();
                    break;
                case GUARDAR_TIPO_PROVEEDOR:
                    guardarTipoProveedor();
                    break;
                case ACTUALIZAR_TIPO_PROVEEDOR:
                    actualizarTipoProveedor();
                    break;
                case CAMBIAR_ESTADO_TIPO_PROVEEDOR:
                    cambiarEstadoTipoProveedor();
                    break;
                case CARGAR_EPS:
                    cargarEPS();
                    break;
                case GUARDAR_EPS:
                    guardarEPS();
                    break;
                case ACTUALIZAR_EPS:
                    actualizarEPS();
                    break;
                case CAMBIAR_ESTADO_EPS:
                    cambiarEstadoEPS();
                    break;
                case CARGAR_ARL:
                    cargarARL();
                    break;
                case GUARDAR_ARL:
                    guardarARL();
                    break;
                case ACTUALIZAR_ARL:
                    actualizarARL();
                    break;
                case CAMBIAR_ESTADO_ARL:
                    cambiarEstadoARL();
                    break;
                case CARGAR_AFP:
                    cargarAFP();
                    break;
                case CARGAR_AFP_CES:
                    cargarAFP_CES();
                    break;
                case GUARDAR_AFP:
                    guardarAFP();
                    break;
                case ACTUALIZAR_AFP:
                    actualizarAFP();
                    break;
                case CAMBIAR_ESTADO_AFP:
                    cambiarEstadoAFP();
                    break;
                case CARGAR_CCF:
                    cargarCCF();
                    break;
                case GUARDAR_CCF:
                    guardarCCF();
                    break;
                case ACTUALIZAR_CCF:
                    actualizarCCF();
                    break;
                case CAMBIAR_ESTADO_CCF:
                    cambiarEstadoCCF();
                    break;
                case CARGAR_DEPENDENCIAS:
                    cargarDependencias();
                    break;
                case GUARDAR_DEPENDENCIAS:
                    guardarDependencias();
                    break;
                case ACTUALIZAR_DEPENDENCIAS:
                    actualizarDependencias();
                    break;
                case CAMBIAR_ESTADO_DEPENDENCIAS:
                    cambiarEstadoDependencias();
                    break;
                case CARGAR_CARGOS:
                    cargarCargos();
                    break;
                case GUARDAR_CARGOS:
                    guardarCargos(usuario.getBd());
                    break;
                case ACTUALIZAR_CARGOS:
                    actualizarCargos();
                    break;
                case CAMBIAR_ESTADO_CARGOS:
                    cambiarEstadoCargos();
                    break;
                case CARGAR_TIPOS_CONTRATO:
                    cargarTiposContrato();
                    break;
                case GUARDAR_TIPOS_CONTRATO:
                    guardarTiposContrato();
                    break;
                case ACTUALIZAR_TIPOS_CONTRATO:
                    actualizarTiposContrato();
                    break;
                case CAMBIAR_ESTADO_TIPOS_CONTRATO:
                    cambiarEstadoTiposContrato();
                    break;
                case CARGAR_TIPOS_RIESGOS:
                    cargarTiposRiesgos();
                    break;
                case GUARDAR_TIPOS_RIESGOS:
                    guardarTiposRiesgos();
                    break;
                case ACTUALIZAR_TIPOS_RIESGOS:
                    actualizarTiposRiesgos();
                    break;
                case CAMBIAR_ESTADO_TIPOS_RIESGOS:
                    cambiarEstadoTiposRiesgos();
                    break;
                case CARGAR_EMPLEADOS:
                    cargarEmpleados();
                    break;
                case GUARDAR_EMPLEADOS:
                    guardarEmpleados(usuario.getBd());
                    break;
                case ACTUALIZAR_EMPLEADOS:
                    actualizarEmpleados();
                    break;
                case CAMBIAR_ESTADO_EMPLEADOS:
                    cambiarEstadoEmpleados();
                    break;
                case CARGAR_TIPO_DOCUMENTO:
                    cargarTipoDoc();
                    break;
                case CARGAR_ESTADO_CIVIL:
                    cargarEstadoCivil();
                    break;
                case CARGAR_DPTO_EXP:
                    cargarDptoExp();
                    break;
                case CARGAR_CIUDAD_EXP:
                    cargarCiudadExp();
                    break;
                case CARGAR_NIVEL_ESTUDIO:
                    cargarNivelEstudio();
                    break;
                case CARGAR_DPTO_NAC:
                    cargarDptoNac();
                    break;
                case CARGAR_CIUDAD_NAC:
                    cargarCiudadNac();
                    break;
                case CARGAR_NIVELES_JERARQUICOS:
                    cargarNivelesJerarquicos();
                    break;
                case GUARDAR_NIVELES_JERARQUICOS:
                    guardarNivelesJerarquicos();
                    break;
                case ACTUALIZAR_NIVELES_JERARQUICOS:
                    actualizarNivelesJerarquicos();
                    break;
                case CAMBIAR_ESTADO_NIVELES_JERARQUICOS:
                    cambiarEstadoNivelesJerarquicos();
                    break;
                case CARGAR_PROFESIONES:
                    cargarProfesiones();
                    break;
                case GUARDAR_PROFESIONES:
                    guardarProfesiones();
                    break;
                case ACTUALIZAR_PROFESIONES:
                    actualizarProfesiones();
                    break;
                case CAMBIAR_ESTADO_PROFESIONES:
                    cambiarEstadoProfesiones();
                    break;
                case CARGAR_DPTO_RES:
                    cargarDptoResidencia();
                    break;
                case CARGAR_CIUDAD_RES:
                    cargarCiudadResidencia();
                    break;
                case CARGAR_BANCOS:
                    cargarBancos();
                    break;
                case CARGAR_HV:
                    cargarHV();
                    break;
                case VER_ARCHIVOS:
                    this.getNombresArchivos();
                    break;
                case MOSTRAR_ARCHIVOS:
                    this.almacenarArchivoEnCarpetaUsuario();
                    break;
                case CARGAR_MACROPROCESOS:
                    cargarMacroprocesos();
                    break;
                case CARGAR_PROCESOS:
                    cargarProcesos();
                    break;
                case CARGAR_LINEAS_NEGOCIO:
                    cargarLineasNegocio();
                    break;
                case CARGAR_PRODUCTOS:
                    cargarProductos();
                    break;
                case CARGAR_TIPO_PROVEEDOR2:
                    cargarTipoProveedor2();
                    break;
                case EXPORTAR_EXCEL:
                    exportarExcel();
                    break;
                case CARGAR_INCAPACIDADES:
                    cargarIncapacidades(usuario.getLogin());
                    break;
                case CARGAR_TIPOS_NOVEDAD:
                    cargarTipoNovedad();
                    break;
                case CARGAR_ENFERMEDADES:
                    cargarEnfermedades();
                    break;
                case CARGAR_RAZON:
                    cargarRazon();
                    break;
                case CARGAR_NOMBRE:
                    cargarNombre();
                    break;
                case CARGAR_JEFESDIRECTOS:
                    cargarJefes(usuario.getBd());
                    break;
                case GUARDAR_NOVEDADES_INCAPACIDAD:
                    guardarNovedadesIncapacidad();
                    break;
                case ACTUALIZAR_NOVEDADES_INCAPACIDAD:
                    actualizarNovedadesIncapacidad();
                    break;
                case CAMBIARESTADO_NOVEDAD:
                    cambiarEstadoNovedad();
                    break;
                case GUARDAR_SOLICITUD_VACACIONES:
                    guardarNovedadesVacaciones();
                    break;
                case APROBAR_SOLICITUD_VACACIONES:
                    aprobarSolicitudVacaciones();
                    break;
                case RECHAZAR_SOLICITUD_VACACIONES:
                    rechazarSolicitudVacaciones();
                    break;
                case VISTOBUENO_NOVEDADES_INCAPACIDAD:
                    vistoBuenoNovedadIncapacidad();
                    break;
                case CARGAR_ARCHIVO_NOVEDAD:
                    cargarArchivo();
                    break;
                case VER_ARCHIVOS_NOVEDAD:
                    this.getNombresArchivosNovedades();
                    break;
                case MOSTRAR_ARCHIVOS_NOVEDAD:
                    this.almacenarArchivoEnCarpetaUsuarioNovedades();
                    break;
                case MARCAR_NOVEDAD_TRAMITADA:
                    novedadTramitada();
                    break;
                case CALCULAR_DIAS_SIN_FESTIVOS:
                    calcularDiasFestivos();
                    break;
                case MARCAR_NOVEDAD_NO_TRAMITE:
                    novedadNoTramite();
                    break;
                case CARGAR_TIPOS_HORAS_EXTRAS:
                    cargarTiposHorasExtras();
                    break;
                case CARGAR_HORAS_EXTRAS:
                    cargarHorasExtras(usuario.getLogin());
                    break;
                case GUARDAR_HORAS_EXTRAS:
                    guardarHorasExtras();
                    break;
                case ACTUALIZAR_HORAS_EXTRAS:
                    actualizarHorasExtras();
                    break;
                case CAMBIARESTADO_HORAS_EXTRAS:
                    cambiarEstadoHorasExtras();
                    break;
                case CARGAR_HORAS_EXTRAS_PORAPROBAR:
                    cargarHorasExtrasPorAprobar(usuario.getLogin());
                    break;
                case APROBAR_HORAS_EXTRAS:
                    aprobarHorasExtras();
                    break;
                case CARGAR_HORAS_EXTRAS_APROBADAS:
                    cargarHorasExtrasAprobadas();
                    break;
                case VISTOBUENO_HORAS_EXTRAS:
                    vistoBuenoHorasExtras();
                    break;
                case CARGAR_OPCIONES_MENU:
                    cargarOpcionesMenu(usuario.getLogin());
                    break;
                case CARGAR_TIPO_INCAPACIDAD:
                    cargarTipoIncapacidad();
                    break;
                case CARGAR_ENTIDAD_OTORGA_INCAPACIDAD:
                    cargarEPSoARL();
                    break;
                case CARGAR_LISTA_ENFERMEDADES:
                    cargarListaEnfermedades();
                    break;
                case CARGAR_NUMERO_SOLICITUD_NOVEDAD:
                    numeroSolicitudNovedad();
                    break;
                case CARGAR_PROCESO_ACTUAL_TRABAJADOR:
                    pocesoActualTrabajador();
                    break;
                case CARGAR_PROCESOS_ASIGNACION:
                    pocesoActual();
                    break;
                case CARGAR_TRAZABILIDAD_EMPLEADO:
                    cargarTrazabilidad();
                    break;
                case CARGAR_OPCIONES_MENU_GESTION:
                    cargarOpcionesMenuGestion(usuario.getLogin());
                    break;
                case CARGAR_INCAPACIDADES2:
                    cargarIncapacidades2(usuario.getBd());
                    break;
                case CARGAR_INCAPACIDADES3:
                    cargarIncapacidades3();
                    break;
                case CARGAR_OPCIONES_MENU_VISTOBUENO:
                    cargarOpcionesMenuVistoBueno(usuario.getLogin());
                    break; 
                case CARGAR_TIPO_VACACIONES:
                    cargarTipoVacaciones();
                    break; 
                case CARGAR_VACACIONES:
                    cargarVacaciones(usuario.getLogin());
                    break;
                case CALCULAR_DIAS_FESTIVOS:
                    calcularDiasSinFestivos();
                    break;    
                case CARGAR_SALDO:
                    cargarSaldo();
                    break;
                case CARGAR_PASIVO_VACACIONAL:
                    cargarPasivoVacacional();
                    break;
                case MOSTRAR_FECHA_INGRESO:
                    cargarFechaIngreso();
                    break;
                case MOSTRAS_EPS:
                    cargarNombreEPS();
                    break;
                case MOSTRAS_ARL:
                    cargarNombreARL();
                    break;
                case CARGAR_SALARIO:
                    cargarSalario();
                    break;
//                case ACTUALIZAR_PERIODO_ACTUAL:
//                    actualizarPeriodoActual();
//                    break;
                case CARGAR_VACACIONES_POR_APROBAR:
                    cargarVacacionesPorAprobar(usuario.getBd());
                    break; 
                case CARGAR_VACACIONES_POR_VISTO_BUENO:
                    cargarVacacionesPorVistoBueno(usuario.getBd());
                    break;
                case VISTOBUENO_NOVEDADES_VACACIONES:
                    vistoBuenoNovedadVacaciones();
                    break;
                case EXPORTAR_EXCEL_PASIVO:
                    exportarExcelPasivo();
                    break;
                case EXPORTAR_EXCEL_ACUMULADO:
                    exportarExcelAcumulado();
                    break;
                case MOSTRAR_PERIODO:
                    mostrarPeriodo();
                    break;
                case CARGAR_PERMISOS:
                    cargarPermisos(usuario.getLogin());
                    break;  
                case CARGAR_TIPO_PERMISOS:
                    cargarTipoPermisos();
                    break;     
                case GUARDAR_NOVEDADES_PERMISOS:
                    guardarNovedadesPermisos();
                    break;
                case CARGAR_PERMISOS_POR_APROBAR:
                    cargarPermisosPorAprobar(usuario.getBd());
                    break;
                case APROBAR_SOLICITUD_PERMISOS:
                    aprobarSolicitudPermisos();
                    break;
                case RECHAZAR_SOLICITUD_PERMISOS:
                    rechazarSolicitudPermisos();
                    break;
                case CARGAR_PERMISOS_POR_VISTO_BUENO:
                    cargarPermisosPorVistoBueno(usuario.getBd());
                    break;  
                case VISTOBUENO_NOVEDADES_PERMISOS:
                    vistoBuenoNovedadPermisos();
                    break;    
                case CARGAR_CAUSALES_RETIRO:
                    cargarCausalesRetiro();
                    break;
                case CARGAR_BARRIOS:
                    cargarBarrios();
                    break;
                case CARGAR_TIPO_LICENCIAS:
                    cargarTipoLicencias();
                    break;
                case CARGAR_CLASES_PERMISO:
                    cargarClasesPermiso();
                    break;
                case GUARDAR_NOVEDADES_LICENCIAS:
                    guardarNovedadesLicencias();
                    break; 
                case CARGAR_CLASES_LICENCIA:
                    cargarClasesLicencias();
                    break;
                case CARGAR_LICENCIAS:
                    cargarLicencias(usuario.getBd());
                    break;    
                case CARGAR_LICENCIAS2:
                    cargarLicenciasPorAprobar(usuario.getBd());
                    break;
                case CARGAR_LICENCIAS_POR_VISTOBUENO:
                    cargarLicenciasPorVistoBueno(usuario.getBd());
                    break;  
                case VISTOBUENO_NOVEDADES_LICENCIAS:
                    vistoBuenoNovedadLicencia();
                    break;
                case CARGAR_TIPO_OTRAS_LICENCIAS:    
                    cargarTipoOtrasLicencias();
                    break;
                case CARGAR_OTRAS_LICENCIAS:
                    cargarOtrasLicencias(usuario.getLogin());
                    break;  
                case GUARDAR_NOVEDADES_OTRAS_LICENCIAS:
                    guardarNovedadesOtrasLicencias();
                    break;
                case CARGAR_OTRAS_LICENCIAS_POR_APROBAR:
                    cargarOtrasLicenciasPorAprobar(usuario.getBd());
                    break;  
                case APROBAR_SOLICITUD_LICENCIAS:
                    aprobarSolicitudLicencias();
                    break;
                case RECHAZAR_SOLICITUD_LICENCIAS: 
                    rechazarSolicitudLicencias();
                    break;
                case CARGAR_OTRAS_LICENCIAS_POR_VISTOBUENO:
                    cargarOtrasLicenciasPorVistoBueno(usuario.getBd());
                    break;
                case EXPORTAR_INCAPACIDADES:
                    exportarExcelIncapacidades(usuario.getBd());
                    break;
                case EXPORTAR_VACACIONES:
                    exportarExcelVacaciones(usuario.getBd());
                    break;   
                case EXPORTAR_PERMISOS:
                    exportarExcelPermisos(usuario.getBd());
                    break;
                case EXPORTAR_LICENCIAS:
                    exportarExcelLicencias(usuario.getBd());
                    break;
                case EXPORTAR_OTRAS_LICENCIAS:
                    exportarExcelOtrasLicencias(usuario.getBd());
                    break;
                case GENERAR_ARCHIVO_DISTRIBUCION:
                    generarArchivoDistribucion();
                    break;
                case EDITAR_NOVEDADES:
                    editarNovedad();
                    break;
                case ANULAR_NOVEDADES:
                    anularNovedad();
                    break;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    private void cargarTipoProveedor() {
        try {
            this.printlnResponse(dao.cargarTipoProveedor(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void guardarTipoProveedor() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String codigo = request.getParameter("codigo");
            String descripcion = Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1");
            this.dao.guardarTipoProveedor(usuario, codigo, descripcion);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void actualizarTipoProveedor() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String id = request.getParameter("id");
            String codigo = request.getParameter("codigo");
            String descripcion = Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1");
            this.dao.actualizarTipoProveedor(usuario, codigo, descripcion, id);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void cambiarEstadoTipoProveedor() {
        Gson gson = new Gson();
        String json = "";
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            json = "{\"rows\":" + gson.toJson(dao.cambiarEstadoTipoProveedor(usuario, id)) + "}";
        } catch (Exception e) {
            json = "{\"respuesta\":\"ERROR\"}";
            e.printStackTrace();
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void printlnResponse(String respuesta, String contentType) throws Exception {

        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);
    }

    private void cargarEPS() {
        try {
            this.printlnResponse(dao.cargarEPS(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void guardarEPS() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String descripcion = Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1");
            this.dao.guardarEPS(usuario, descripcion);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void actualizarEPS() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String id = request.getParameter("id");
            String descripcion = Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1");
            this.dao.actualizarEPS(usuario, descripcion, id);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void cambiarEstadoEPS() {
        Gson gson = new Gson();
        String json = "";
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            json = "{\"rows\":" + gson.toJson(dao.cambiarEstadoEPS(usuario, id)) + "}";
        } catch (Exception e) {
            json = "{\"respuesta\":\"ERROR\"}";
            e.printStackTrace();
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void cargarARL() {
        try {
            this.printlnResponse(dao.cargarARL(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void guardarARL() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String descripcion = Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1");
            this.dao.guardarARL(usuario, descripcion);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void actualizarARL() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String id = request.getParameter("id");
            String descripcion = Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1");
            this.dao.actualizarARL(usuario, descripcion, id);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void cambiarEstadoARL() {
        Gson gson = new Gson();
        String json = "";
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            json = "{\"rows\":" + gson.toJson(dao.cambiarEstadoARL(usuario, id)) + "}";
        } catch (Exception e) {
            json = "{\"respuesta\":\"ERROR\"}";
            e.printStackTrace();
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void cargarAFP() {
        try {
            this.printlnResponse(dao.cargarAFP(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
    
    private void cargarAFP_CES() {
        try {
            this.printlnResponse(dao.cargarAFP_CES(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void guardarAFP() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String descripcion = Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1");
            this.dao.guardarAFP(usuario, descripcion);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void actualizarAFP() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String id = request.getParameter("id");
            String descripcion = Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1");
            this.dao.actualizarAFP(usuario, descripcion, id);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void cambiarEstadoAFP() {
        Gson gson = new Gson();
        String json = "";
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            json = "{\"rows\":" + gson.toJson(dao.cambiarEstadoAFP(usuario, id)) + "}";
        } catch (Exception e) {
            json = "{\"respuesta\":\"ERROR\"}";
            e.printStackTrace();
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void cargarCCF() {
        try {
            this.printlnResponse(dao.cargarCCF(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void guardarCCF() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String descripcion = Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1");
            this.dao.guardarCCF(usuario, descripcion);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void actualizarCCF() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String id = request.getParameter("id");
            String descripcion = Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1");
            this.dao.actualizarCCF(usuario, descripcion, id);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void cambiarEstadoCCF() {
        Gson gson = new Gson();
        String json = "";
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            json = "{\"rows\":" + gson.toJson(dao.cambiarEstadoCCF(usuario, id)) + "}";
        } catch (Exception e) {
            json = "{\"respuesta\":\"ERROR\"}";
            e.printStackTrace();
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void cargarDependencias() {
        try {
            this.printlnResponse(dao.cargarDependencias(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void guardarDependencias() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String descripcion = Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1");
            this.dao.guardarDependencias(usuario, descripcion);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void actualizarDependencias() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String id = request.getParameter("id");
            String descripcion = Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1");
            this.dao.actualizarDependencias(usuario, descripcion, id);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void cambiarEstadoDependencias() {
        Gson gson = new Gson();
        String json = "";
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            json = "{\"rows\":" + gson.toJson(dao.cambiarEstadoDependencias(usuario, id)) + "}";
        } catch (Exception e) {
            json = "{\"respuesta\":\"ERROR\"}";
            e.printStackTrace();
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void cargarCargos() {
        try {
            this.printlnResponse(dao.cargarCargos(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void guardarCargos(String bd) {
        String json = "";
        try {
            String descripcion = Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1");
            String codigo = Util.setCodificacionCadena(request.getParameter("codigo"), "ISO-8859-1");
          json= dao.guardarCargos(usuario, descripcion, codigo);
            if(json.equals("{\"respuesta\":\"Guardado\"}")){
                if(dao.guardarCargosTablaTemp(bd)){
//                    loadCargo(bd);
                }
                
            }
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void actualizarCargos() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String id = request.getParameter("id");
            String descripcion = Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1");
            String codigo = Util.setCodificacionCadena(request.getParameter("codigo"), "ISO-8859-1");
            this.dao.actualizarCargos(usuario, descripcion, codigo, id);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void cambiarEstadoCargos() {
        Gson gson = new Gson();
        String json = "";
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            json = "{\"rows\":" + gson.toJson(dao.cambiarEstadoCargos(usuario, id)) + "}";
        } catch (Exception e) {
            json = "{\"respuesta\":\"ERROR\"}";
            e.printStackTrace();
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void cargarTiposContrato() {
        try {
            this.printlnResponse(dao.cargarTiposContrato(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void guardarTiposContrato() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String descripcion = Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1");
            this.dao.guardarTiposContrato(usuario, descripcion);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void actualizarTiposContrato() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String id = request.getParameter("id");
            String descripcion = Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1");
            this.dao.actualizarTiposContrato(usuario, descripcion, id);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void cambiarEstadoTiposContrato() {
        Gson gson = new Gson();
        String json = "";
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            json = "{\"rows\":" + gson.toJson(dao.cambiarEstadoTiposContrato(usuario, id)) + "}";
        } catch (Exception e) {
            json = "{\"respuesta\":\"ERROR\"}";
            e.printStackTrace();
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void cargarTiposRiesgos() {
        try {
            this.printlnResponse(dao.cargarTiposRiesgos(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void guardarTiposRiesgos() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String intensidad = Util.setCodificacionCadena(request.getParameter("intensidad"), "ISO-8859-1");
            String porcentaje = request.getParameter("porcentaje");
            String actividades = Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1");
            this.dao.guardarTiposRiesgos(usuario, intensidad, porcentaje, actividades);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void actualizarTiposRiesgos() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String id = request.getParameter("id");
            String intensidad = Util.setCodificacionCadena(request.getParameter("intensidad"), "ISO-8859-1");
            String porcentaje = request.getParameter("porcentaje");
            String actividades = Util.setCodificacionCadena(request.getParameter("actividades"), "ISO-8859-1");
            this.dao.actualizarTiposRiesgos(usuario, intensidad, porcentaje, actividades, id);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void cambiarEstadoTiposRiesgos() {
        Gson gson = new Gson();
        String json = "";
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            json = "{\"rows\":" + gson.toJson(dao.cambiarEstadoTiposRiesgos(usuario, id)) + "}";
        } catch (Exception e) {
            json = "{\"respuesta\":\"ERROR\"}";
            e.printStackTrace();
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void cargarEmpleados() {
        try {
            this.printlnResponse(dao.cargarEmpleados(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cambiarEstadoEmpleados() {
        Gson gson = new Gson();
        String json = "";
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            json = "{\"rows\":" + gson.toJson(dao.cambiarEstadoEmpleados(usuario, id)) + "}";
        } catch (Exception e) {
            json = "{\"respuesta\":\"ERROR\"}";
            e.printStackTrace();
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void guardarEmpleados(String bd) {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String proceso_meta = request.getParameter("macroproceso");
            String proceso_interno = request.getParameter("proceso");
            String linea_negocio = request.getParameter("linea_negocio");
            String producto = request.getParameter("producto");
            String nivel_jerarquico = request.getParameter("nivel_jerarquico");
            String tipo_contrato = request.getParameter("tipo_contrato");
            String nombre_completo = Util.setCodificacionCadena(request.getParameter("nombre_completo"), "ISO-8859-1");
            String tipo_doc = request.getParameter("tipo_doc");
            String duracion = request.getParameter("duracion");
            String identificacion = request.getParameter("identificacion");
            String sexo = request.getParameter("sexo");
            String fecha_expedicion = request.getParameter("fecha_expedicion");
            String dpto_expedicion = request.getParameter("dpto_expedicion");
            String ciudad_expedicion = request.getParameter("ciudad_expedicion");
            String libreta_militar = request.getParameter("libreta_militar");
            String salario = request.getParameter("salario");
            String fecha_ingreso = request.getParameter("fecha_ingreso");
            String banco = request.getParameter("banco");
            String tipo_cuenta = request.getParameter("tipo_cuenta");
            String no_cuenta = request.getParameter("no_cuenta");
            String cargo = request.getParameter("cargo");
            String riesgo = request.getParameter("riesgo");
            String eps = request.getParameter("eps");
            String afp = request.getParameter("afp");
            String arl = request.getParameter("arl");
            String cesantias = request.getParameter("cesantias");
            String ccf = request.getParameter("ccf");
            String dpto = request.getParameter("dpto");
            String ciudad = request.getParameter("ciudad");
            String direccion = Util.setCodificacionCadena(request.getParameter("direccion"), "ISO-8859-1");
            String barrio = request.getParameter("barrio");
            String telefono = request.getParameter("telefono");
            String celular = request.getParameter("celular");
            String email = request.getParameter("email");
            String fecha_nacimiento = request.getParameter("fecha_nacimiento");
            String dpto_nacimiento = request.getParameter("dpto_nacimiento");
            String ciudad_nacimiento = request.getParameter("ciudad_nacimiento");
            String estado_civil = request.getParameter("estado_civil");
            String nivel_estudio = request.getParameter("nivel_estudio");
            String profesion = request.getParameter("profesion");
            String personas_a_cargo = request.getParameter("personas_a_cargo");
            String num_de_hijos = request.getParameter("num_de_hijos");
            String total_grupo_familiar = request.getParameter("total_grupo_familiar");
            String tipo_vivienda = request.getParameter("tipo_vivienda");
            String fecha_retiro = request.getParameter("fecha_retiro");
            String causal_retiro = request.getParameter("causal_retiro");
            String observaciones = Util.setCodificacionCadena(request.getParameter("observaciones"), "ISO-8859-1");

//            this.dao.guardarEmpleados(usuario,nivel_jerarquico,tipo_contrato,duracion,nombre_completo,tipo_doc,identificacion,sexo,fecha_expedicion,dpto_expedicion,ciudad_expedicion,
//                    libreta_militar,salario,fecha_ingreso,banco,tipo_cuenta,no_cuenta,cargo,riesgo,eps,afp,arl,cesantias,ccf,dpto,ciudad,direccion, 
//                    telefono,celular,email,fecha_nacimiento,dpto_nacimiento,ciudad_nacimiento,estado_civil,nivel_estudio,profesion,personas_a_cargo,num_de_hijos, 
//                    total_grupo_familiar,fecha_retiro,observaciones);
            TransaccionService tService = new TransaccionService(this.usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.guardarEmpleados(usuario, proceso_meta, proceso_interno, linea_negocio, producto, nivel_jerarquico, tipo_contrato, duracion, nombre_completo, tipo_doc, identificacion, sexo, fecha_expedicion, dpto_expedicion, ciudad_expedicion,
                    libreta_militar, salario, fecha_ingreso, banco, tipo_cuenta, no_cuenta, cargo, riesgo, eps, afp, arl, cesantias, ccf, dpto, ciudad, direccion,
                    barrio, telefono, celular, email, fecha_nacimiento, dpto_nacimiento, ciudad_nacimiento, estado_civil, nivel_estudio, profesion, personas_a_cargo, num_de_hijos,
                    total_grupo_familiar,tipo_vivienda, fecha_retiro, causal_retiro, observaciones));
            tService.getSt().addBatch(this.dao.guardarProveedor(usuario, nombre_completo, tipo_doc, identificacion, banco, tipo_cuenta, no_cuenta));
            tService.getSt().addBatch(this.dao.guardarNit(usuario, nombre_completo, identificacion, direccion, ciudad, dpto, telefono, celular, email, sexo, fecha_nacimiento, tipo_doc, estado_civil, ciudad_nacimiento, libreta_militar, ciudad_expedicion));
            tService.getSt().addBatch(this.dao.insertarTrazabilidadEmpleados(usuario, proceso_meta, proceso_interno, linea_negocio, producto, nivel_jerarquico, tipo_contrato, duracion, nombre_completo, tipo_doc, identificacion, sexo, fecha_expedicion, dpto_expedicion, ciudad_expedicion,
                    libreta_militar, salario, fecha_ingreso, banco, tipo_cuenta, no_cuenta, cargo, riesgo, eps, afp, arl, cesantias, ccf, dpto, ciudad, direccion,
                    barrio, telefono, celular, email, fecha_nacimiento, dpto_nacimiento, ciudad_nacimiento, estado_civil, nivel_estudio, profesion, personas_a_cargo, num_de_hijos,
                    total_grupo_familiar, tipo_vivienda, fecha_retiro,  causal_retiro, observaciones));
            tService.execute();
            
          
             if(json.equals("{\"respuesta\":\"Guardado\"}")){
                if(dao.guardarEmpleadosTablaTemp(bd)){
//                    loadTerceroEmp(bd);
                }
                
            }

        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void actualizarEmpleados() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String proceso_meta = request.getParameter("macroproceso");
            String proceso_interno = request.getParameter("proceso");
            String linea_negocio = request.getParameter("linea_negocio");
            String producto = request.getParameter("producto");
            String nivel_jerarquico = request.getParameter("nivel_jerarquico");
            String tipo_contrato = request.getParameter("tipo_contrato");
            String nombre_completo = Util.setCodificacionCadena(request.getParameter("nombre_completo"), "ISO-8859-1");
            String tipo_doc = request.getParameter("tipo_doc");
            String duracion = request.getParameter("duracion");
            String identificacion = request.getParameter("identificacion");
            String sexo = request.getParameter("sexo");
            String fecha_expedicion = request.getParameter("fecha_expedicion");
            String dpto_expedicion = request.getParameter("dpto_expedicion");
            String ciudad_expedicion = request.getParameter("ciudad_expedicion");
            String libreta_militar = request.getParameter("libreta_militar");
            String salario = request.getParameter("salario");
            String fecha_ingreso = request.getParameter("fecha_ingreso");
            String banco = request.getParameter("banco");
            String tipo_cuenta = request.getParameter("tipo_cuenta");
            String no_cuenta = request.getParameter("no_cuenta");
            String cargo = request.getParameter("cargo");
            String riesgo = request.getParameter("riesgo");
            String eps = request.getParameter("eps");
            String afp = request.getParameter("afp");
            String arl = request.getParameter("arl");
            String cesantias = request.getParameter("cesantias");
            String ccf = request.getParameter("ccf");
            String dpto = request.getParameter("dpto");
            String ciudad = request.getParameter("ciudad");
            String direccion = request.getParameter("direccion");
            String barrio = request.getParameter("barrio");
            String telefono = request.getParameter("telefono");
            String celular = request.getParameter("celular");
            String email = request.getParameter("email");
            String fecha_nacimiento = request.getParameter("fecha_nacimiento");
            String dpto_nacimiento = request.getParameter("dpto_nacimiento");
            String ciudad_nacimiento = request.getParameter("ciudad_nacimiento");
            String estado_civil = request.getParameter("estado_civil");
            String nivel_estudio = request.getParameter("nivel_estudio");
            String profesion = request.getParameter("profesion");
            String personas_a_cargo = request.getParameter("personas_a_cargo");
            String num_de_hijos = request.getParameter("num_de_hijos");
            String total_grupo_familiar = request.getParameter("total_grupo_familiar");
            String tipo_vivienda = request.getParameter("tipo_vivienda");
            String fecha_retiro = request.getParameter("fecha_retiro");
            String causal_retiro = request.getParameter("causal_retiro");
            String observaciones = request.getParameter("observaciones");
            String idEmpleado = request.getParameter("id");

//            this.dao.guardarEmpleados(usuario,nivel_jerarquico,tipo_contrato,duracion,nombre_completo,tipo_doc,identificacion,sexo,fecha_expedicion,dpto_expedicion,ciudad_expedicion,
//                    libreta_militar,salario,fecha_ingreso,banco,tipo_cuenta,no_cuenta,cargo,riesgo,eps,afp,arl,cesantias,ccf,dpto,ciudad,direccion, 
//                    telefono,celular,email,fecha_nacimiento,dpto_nacimiento,ciudad_nacimiento,estado_civil,nivel_estudio,profesion,personas_a_cargo,num_de_hijos, 
//                    total_grupo_familiar,fecha_retiro,observaciones);
            TransaccionService tService = new TransaccionService(this.usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.actualizarEmpleados(usuario, proceso_meta, proceso_interno, linea_negocio, producto, nivel_jerarquico, tipo_contrato, duracion, nombre_completo, tipo_doc, identificacion, sexo, fecha_expedicion, dpto_expedicion, ciudad_expedicion,
                    libreta_militar, salario, fecha_ingreso, banco, tipo_cuenta, no_cuenta, cargo, riesgo, eps, afp, arl, cesantias, ccf, dpto, ciudad, direccion,
                    barrio,telefono, celular, email, fecha_nacimiento, dpto_nacimiento, ciudad_nacimiento, estado_civil, nivel_estudio, profesion, personas_a_cargo, num_de_hijos,
                    total_grupo_familiar, tipo_vivienda, fecha_retiro, causal_retiro, observaciones, idEmpleado));
            tService.getSt().addBatch(this.dao.actualizarProveedor(usuario, nombre_completo, tipo_doc, identificacion, banco, tipo_cuenta, no_cuenta));
            tService.getSt().addBatch(this.dao.actualizarNit(usuario, nombre_completo, identificacion, direccion, ciudad, dpto, telefono, celular, email, sexo, fecha_nacimiento, tipo_doc, estado_civil, ciudad_nacimiento, libreta_militar, ciudad_expedicion));
            tService.getSt().addBatch(this.dao.insertarTrazabilidadEmpleados(usuario, proceso_meta, proceso_interno, linea_negocio, producto, nivel_jerarquico, tipo_contrato, duracion, nombre_completo, tipo_doc, identificacion, sexo, fecha_expedicion, dpto_expedicion, ciudad_expedicion,
                    libreta_militar, salario, fecha_ingreso, banco, tipo_cuenta, no_cuenta, cargo, riesgo, eps, afp, arl, cesantias, ccf, dpto, ciudad, direccion,
                    barrio,telefono, celular, email, fecha_nacimiento, dpto_nacimiento, ciudad_nacimiento, estado_civil, nivel_estudio, profesion, personas_a_cargo, num_de_hijos,
                    total_grupo_familiar, tipo_vivienda, fecha_retiro, causal_retiro, observaciones));
            tService.execute();

        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void cargarTipoDoc() {
        try {
            this.printlnResponse(dao.cargarTipoDoc(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarEstadoCivil() {
        try {
            this.printlnResponse(dao.cargarEstadoCivil(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarDptoExp() {
        try {

            this.printlnResponse(dao.cargarDptoExp(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarCiudadExp() {
        try {
            String dpto_expedicion = request.getParameter("dpto_expedicion");
            this.printlnResponse(dao.cargarCiudadExp(dpto_expedicion), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarNivelEstudio() {
        try {
            this.printlnResponse(dao.cargarNivelEstudio(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarDptoNac() {
        try {
            this.printlnResponse(dao.cargarDptoNac(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarCiudadNac() {
        try {
            String dpto_nacimiento = request.getParameter("dpto_nacimiento");
            this.printlnResponse(dao.cargarCiudadNac(dpto_nacimiento), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarNivelesJerarquicos() {
        try {
            this.printlnResponse(dao.cargarNivelesJerarquicos(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void guardarNivelesJerarquicos() {
        String json = "";
        try {
            String descripcion = Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1");
            this.dao.guardarNivelesJerarquicos(usuario, descripcion);
            json = "{\"respuesta\":\"Guardado\"}";
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void actualizarNivelesJerarquicos() {
        String json = "";
        try {
            String id = request.getParameter("id");
            String descripcion = Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1");
            this.dao.actualizarNivelesJerarquicos(usuario, descripcion, id);
            json = "{\"respuesta\":\"Guardado\"}";
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void cambiarEstadoNivelesJerarquicos() {
        Gson gson = new Gson();
        String json = "";
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            json = "{\"rows\":" + gson.toJson(dao.cambiarEstadoNivelesJerarquicos(usuario, id)) + "}";
        } catch (Exception e) {
            json = "{\"respuesta\":\"ERROR\"}";
            e.printStackTrace();
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void cargarProfesiones() {
        try {
            this.printlnResponse(dao.cargarProfesiones(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void guardarProfesiones() {
        String json = "";
        try {
            String descripcion = Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1");
            this.dao.guardarProfesiones(usuario, descripcion);
            json = "{\"respuesta\":\"Guardado\"}";
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void actualizarProfesiones() {
        String json = "";
        try {
            String id = request.getParameter("id");
            String descripcion = Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1");
            this.dao.actualizarProfesiones(usuario, descripcion, id);
            json = "{\"respuesta\":\"Guardado\"}";
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void cambiarEstadoProfesiones() {
        Gson gson = new Gson();
        String json = "";
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            json = "{\"rows\":" + gson.toJson(dao.cambiarEstadoProfesiones(usuario, id)) + "}";
        } catch (Exception e) {
            json = "{\"respuesta\":\"ERROR\"}";
            e.printStackTrace();
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void cargarDptoResidencia() {
        try {
            this.printlnResponse(dao.cargarDptoResidencia(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarCiudadResidencia() {
        try {
            String dpto = request.getParameter("dpto");
            this.printlnResponse(dao.cargarCiudadResidencia(dpto), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarBancos() {
        try {
            this.printlnResponse(dao.cargarBancos(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarHV() {
        String respuesta = "{}";
        String identificacion = "";
        try {

            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");//se consiguen los datos de db.properties

            String directorioArchivos = rb.getString("rutaImagenes") + "gestionadministrativa/HV/";//se establece la ruta del archivo

            //this.createDir(directorioArchivos );
            if (ServletFileUpload.isMultipartContent(request)) {

                ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
                List fileItemsList = servletFileUpload.parseRequest(request);

                //// Itero para obtener todos los FileItem
                Iterator it = fileItemsList.iterator();
                String nombre_archivo;
                String fieldtem = "", valortem = "";

                while (it.hasNext()) {

                    FileItem fileItem = (FileItem) it.next();

                    if ((fileItem.isFormField())) {

                        fieldtem = fileItem.getFieldName();
                        valortem = fileItem.getString();

                        if (fieldtem.equals("identificacion1")) {
                            identificacion = valortem;
                        }

                    }

                    if (!(fileItem.isFormField())) {

                        //// Nombre del archivo en el cliente. Algunos navegadores (por ej. IE 6)
                        //// incluyen el path completo, lo que puede implicar separar path
                        //// de nombre.
                        if (fileItem.getName() != "" && !fileItem.getName().isEmpty() && fileItem.getSize() > 0) {

                            String nombreArchivo = fileItem.getName();

                            String[] nombrearreglo = nombreArchivo.split("\\\\");
                            String nombreArchivoremix = nombrearreglo[(nombrearreglo.length - 1)];
                            String rutaArchivo = directorioArchivos + identificacion;
                            nombre_archivo = nombreArchivoremix;

                            this.createDir(rutaArchivo);

                            File archivo = new File(rutaArchivo + "/" + nombre_archivo);

                            fileItem.write(archivo);
                        }

                    }

                }
            }
            //dao.actualizarReferencia(identificacion);
            respuesta = "{\"respuesta\":\"OK\"}";

        } catch (Exception ee) {
            respuesta = "{\"respuesta\":\"" + ee.getMessage() + "\"}";
            //respuesta = "{\"respuesta\":\"" + ee.getMessage() + "\"}";
            System.out.println("eroror:" + ee.toString() + "__" + ee.getMessage());
        } finally {
            try {
                this.printlnResponseAjax(respuesta, "application/json;");
            } catch (Exception ex) {
                java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void createDir(String dir) throws Exception {
        try {
            File f = new File(dir);
            if (!f.exists()) {
                f.mkdir();
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    private void getNombresArchivos() {
        java.util.List lista = null;
        Gson gson = new Gson();
        try {
            String identificacion = (request.getParameter("identificacion1") != null) ? request.getParameter("identificacion1") : "";
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String rutaOrigen = rb.getString("rutaImagenes") + "gestionadministrativa/HV/";
            this.printlnResponseAjax(gson.toJson(dao.searchNombresArchivos(rutaOrigen, identificacion)), "application/json");
        } catch (Exception e) {
            System.out.println("errorrr::" + e.toString() + "__" + e.getMessage());
        }
    }

    public void almacenarArchivoEnCarpetaUsuario() throws Exception {
        try {
            String identificacion = (request.getParameter("identificacion1") != null) ? request.getParameter("identificacion1") : "";
            String nomarchivo = (request.getParameter("nomarchivo") != null) ? request.getParameter("nomarchivo") : "";
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String rutaOrigen = rb.getString("rutaImagenes") + "gestionadministrativa/HV/";
            String rutaDestino = rb.getString("ruta") + "/images/multiservicios/" + usuario.getLogin() + "/";
            if (dao.almacenarArchivoEnCarpetaUsuario(identificacion, ("" + rutaOrigen + identificacion), ("" + rutaDestino), nomarchivo)) {
                this.printlnResponseAjax("{\"respuesta\":\"SI\",\"login\":\"" + usuario.getLogin() + "\"}", "application/json");
            } else {
                this.printlnResponseAjax("{\"respuesta\":\"NO\"}", "application/json");
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    private void cargarMacroprocesos() {
        try {
            this.printlnResponse(dao.cargarMacroprocesos(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarProcesos() {
        try {
            String macroproceso = request.getParameter("macroproceso");
            this.printlnResponse(dao.cargarProcesos(macroproceso), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarLineasNegocio() {
        try {
            String proceso = request.getParameter("proceso");
            this.printlnResponse(dao.cargarLineasNegocio(proceso), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarProductos() {
        try {
            String linea_negocio = request.getParameter("linea_negocio");
            this.printlnResponse(dao.cargarProductos(linea_negocio), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarTipoProveedor2() {
        try {
            this.printlnResponse(dao.cargarTipoProveedor2(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void exportarExcel() throws Exception {
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(dao.listarEmpleados());
        //JsonArray asJsonArray = (JsonArray) new JsonParser().parse();
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Listado_Empleados", titulo = "LISTADO EMPLEADOS";

        cabecera = new String[]{"ESTADO", "MACROPROCESO", "PROCESO", "LINEA NEGOCIO", "PRODUCTO", "NIVEL JERARQUICO", "TIPO CONTRATO", "DURACION", "NOMBRE COMPLETO", "TIPO DOCUMENTO", "IDENTIFICACION",
            "FECHA EXPEDICION", "DPTO EXPEDICION", "CIUDAD EXPEDICION", "LIBRETA MILITAR", "SALARIO", "FECHA INGRESO", "BANCO", "TIPO CUENTA", "CUENTA", "DPTO RESIDENCIA", "CIUDAD RESIDENCIA", "CARGO",
            "TIPO RIESGO", "EPS", "AFP", "ARL", "CESANTIAS", "CCF", "DIRECCION", "BARRIO","TELEFONO", "CELULAR", "EMAIL", "FECHA NACIMIENTO", "DPTO NACIMIENTO", "CIUDAD NACIMIENTO", "SEXO", "ESTADO CIVIL",
            "NIVEL ESTUDIO", "PROFESION", "PERSONAS A CARGO", "NO. HIJOS", "TOTAL GRUPO FAMILIAR", "FECHA RETIRO", "OBSERVACIONES"};

        dimensiones = new short[]{
            5000, 5000, 7000, 7000, 7000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 7000, 5000,
            5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 7000, 5000,
            5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 7000, 5000, 5000, 5000
        };

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        this.printlnResponseAjax(reponseJson, "text/plain");
    }

    private void cargarIncapacidades(String usuario) {
        try {
            String fechaInicio = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";
            String fechaFin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
            String tipo_novedad = request.getParameter("tipo_novedad1") != null ? request.getParameter("tipo_novedad1") : "";
            String identificacion = request.getParameter("identificacion2") != null ? request.getParameter("identificacion2") : "";
            String status = request.getParameter("status") != null ? request.getParameter("status") : "";
            this.printlnResponse(dao.cargarIncapacidades(fechaInicio, fechaFin, identificacion, tipo_novedad, status, usuario), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarTipoNovedad() {
        try {
            this.printlnResponse(dao.cargarTipoNovedad(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarEnfermedades() {
        try {
            String cod_enfermedad = request.getParameter("cod_enfermedad");
            this.printlnResponse(dao.cargarEnfermedades(cod_enfermedad), "application/text;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarRazon() {
        try {
            String tipo_novedad = request.getParameter("tipo_novedad");
            this.printlnResponse(dao.cargarRazon(tipo_novedad), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarNombre() {
        try {
            String identificacion = request.getParameter("identificacion");
            this.printlnResponse(dao.cargarNombre(identificacion), "application/text;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarJefes(String bd) {
        try {
            this.printlnResponse(dao.cargarJefes(usuario, usuario.getBd()), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void guardarNovedadesIncapacidad() {
        try {
            JsonObject objeto = new JsonObject();
            String json = request.getParameter("informacion") != null ? request.getParameter("informacion") : "{}";
            JsonObject obj = (JsonObject) (new JsonParser()).parse(json);
            String resp = dao.guardarNovedadesIncapacidad(obj, usuario);
            this.printlnResponseAjax(resp, "application/json;");

            //Envio de email 
            if (resp.contains("Guardado")) {
                JsonObject jobj = dao.getInfoCuentaEnvioCorreo();
                objeto = obj.getAsJsonArray("info").get(0).getAsJsonObject();
                String empleado = dao.cargarNombre(objeto.get("identificacion").getAsString());
                //String tipo_novedad = dao.cargarNombreTipoNovedad(objeto.get("tipo_novedad").getAsString());
                String MyMessage = "El empleado " + empleado + " ha realizado un registro de Incapacidad. Favor ingrese al m�dulo de Gesti�n de Solicitudes para ver el detalle.";
                String mailsToSend = dao.getInfoCuentaEnvioCorreoDestino(objeto.get("jefe_directo").getAsString());
                String asunto = "Nuevo registro de Incapacidad";
                Thread hiloEmail = new Thread(new EmailSenderService(jobj, "RRHH", MyMessage, mailsToSend, asunto, usuario), "hilo");
                hiloEmail.start();
            }
            if (resp.contains("Guardado")){
                JsonObject jobj = dao.getInfoCuentaEnvioCorreo();
                objeto = obj.getAsJsonArray("info").get(0).getAsJsonObject();
                String empleado = dao.cargarNombre(objeto.get("identificacion").getAsString());
                String MyMessage = "El empleado " + empleado + " ha realizado un registro de Incapacidad. Favor ingrese al sistema para ver el detalle.";
                String mailsToSend = dao.getInfoCuentaEnvioCorreoDestino2();
                String asunto = "Nuevo registro de Incapacidad";
                Thread hiloEmail = new Thread(new EmailSenderService(jobj, "RRHH", MyMessage, mailsToSend, asunto, usuario), "hilo");
                hiloEmail.start();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void actualizarNovedadesIncapacidad() {
        String json = "";
        try {
            String id = request.getParameter("id");
            String tipo_novedad = request.getParameter("tipo_novedad");
            String origen = request.getParameter("origen");
            String razon = request.getParameter("razon");
            String identificacion = request.getParameter("identificacion");
            String fecha_solicitud = request.getParameter("fecha_solicitud");
            String fecha_ini = request.getParameter("fecha_ini");
            String fecha_fin = request.getParameter("fecha_fin");
            String duracion_dias = request.getParameter("duracion_dias");
//            String hora_ini = request.getParameter("hora_ini");
//            String hora_fin = request.getParameter("hora_fin");
//            String duracion_horas = request.getParameter("duracion_horas");
            String cod_enfermedad = request.getParameter("cod_enfermedad");
            String jefe_directo = request.getParameter("jefe_directo");
            String descripcion = Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1");
//            String dias_disfrute = request.getParameter("dias_disfrute");
//            String dias_a_pagar = request.getParameter("dias_a_pagar");
//            String recobro = request.getParameter("recobro");
//            String proceso_nuevo = request.getParameter("proceso_nuevo");
            this.dao.actualizarNovedadesIncapacidad(usuario, tipo_novedad, origen, razon, identificacion, fecha_solicitud, fecha_ini, fecha_fin, duracion_dias, cod_enfermedad, jefe_directo, descripcion, id);
            json = "{\"respuesta\":\"Guardado\"}";

        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void cambiarEstadoNovedad() {
        Gson gson = new Gson();
        String json = "";
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            json = "{\"rows\":" + gson.toJson(dao.cambiarEstadoNovedad(usuario, id)) + "}";
        } catch (Exception e) {
            json = "{\"respuesta\":\"ERROR\"}";
            e.printStackTrace();
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void guardarNovedadesVacaciones() {
        try {
            JsonObject objeto = new JsonObject();
            String json = request.getParameter("informacion") != null ? request.getParameter("informacion") : "{}";
            JsonObject obj = (JsonObject) (new JsonParser()).parse(json);
            String resp = dao.guardarNovedadesVacaciones(obj, usuario);
            this.printlnResponseAjax(resp, "application/json;");

            //Envio de email 
            if (resp.contains("Guardado")) {
                JsonObject jobj = dao.getInfoCuentaEnvioCorreo();
                objeto = obj.getAsJsonArray("info").get(0).getAsJsonObject();
                String empleado = dao.cargarNombre(objeto.get("identificacion").getAsString());
                //String tipo_novedad = dao.cargarNombreTipoNovedad(objeto.get("tipo_novedad").getAsString());
                String MyMessage = "El empleado " + empleado + " ha realizado una Solicitud de Vacaciones. Favor ingrese al m�dulo de Gesti�n de Solicitudes para ver el detalle.";
                String mailsToSend = dao.getInfoCuentaEnvioCorreoDestino(objeto.get("jefe_directo").getAsString());
                String asunto = "Nueva Solicitud de Vacaciones";
                Thread hiloEmail = new Thread(new EmailSenderService(jobj, "RRHH", MyMessage, mailsToSend, asunto, usuario), "hilo");
                hiloEmail.start();
            }
//            if (resp.contains("Guardado")){
//                JsonObject jobj = dao.getInfoCuentaEnvioCorreo();
//                objeto = obj.getAsJsonArray("info").get(0).getAsJsonObject();
//                String empleado = dao.cargarNombre(objeto.get("identificacion").getAsString());
//                String MyMessage = "El empleado " + empleado + " ha realizado un registro de Incapacidad. Favor ingrese al sistema para ver el detalle.";
//                String mailsToSend = dao.getInfoCuentaEnvioCorreoDestino2();
//                String asunto = "Nuevo registro de Incapacidad";
//                Thread hiloEmail = new Thread(new EmailSenderService(jobj, "RRHH", MyMessage, mailsToSend, asunto, usuario), "hilo");
//                hiloEmail.start();
//            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    
    private void aprobarSolicitudVacaciones() {
        String json = "";
        try {
            String id = request.getParameter("id");
            String comentario = Util.setCodificacionCadena(request.getParameter("comentario"), "ISO-8859-1");
            String identificacion = request.getParameter("identificacion");
            String duracion_dias = request.getParameter("duracion_dias");
            String dias_compensados = request.getParameter("dias_compensados");
            String tipo_novedad = request.getParameter("tipo_novedad");
            String jefe_directo = request.getParameter("jefe_directo");
            //this.dao.aprobarSolicitudVacaciones(usuario, comentario, id);
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.aprobarSolicitudVacaciones(usuario, comentario, id));
            tService.getSt().addBatch(this.dao.actualizarPasivo(usuario, identificacion, duracion_dias, dias_compensados));
            tService.execute();
            json = "{\"respuesta\":\"Guardado\"}";
            if (json.contains("Guardado")) {
                //Envio de Correo a Recursos Humanos
                    JsonObject jobj = dao.getInfoCuentaEnvioCorreo();
                    String empleado = dao.cargarNombre(identificacion);
                    String tipo_novedad1 = dao.cargarNombreTipoNovedad(tipo_novedad);
                    String jefe_directo1 = dao.cargarNombreJefeDirecto(jefe_directo);
                    String MyMessage = "El empleado " + empleado + " ha realizado una solicitud de " + tipo_novedad1 + ", la cual ha sido aprobada por " + jefe_directo1 + ". Favor ingrese al m�dulo de Visto Bueno de Solicitudes para ver el detalle.";
                    String mailsToSend = dao.getInfoCuentaEnvioCorreoDestino2();
                    String asunto = "Nueva Solicitud de Vacaciones Aprobada por Jefe de Area";
                    Thread hiloEmail = new Thread(new EmailSenderService(jobj, "RRHH", MyMessage, mailsToSend, asunto, usuario), "hilo");
                    hiloEmail.start();
                //Envio correo al empleado 
                    String MyMessage1 = "Su Solicitud de Vacaciones ha sido aprobada por " + jefe_directo1 + ". Favor ingrese al m�dulo de Registro de Solicitudes para ver el detalle.";
                    String mailsToSend1 = dao.getInfoCuentaEnvioCorreoDestino3(identificacion);
                    String asunto1 = "Solicitud de Vacaciones Aprobada por Jefe de Area";
                    Thread hiloEmail1 = new Thread(new EmailSenderService(jobj, "RRHH", MyMessage1, mailsToSend1, asunto1, usuario), "hilo");
                    hiloEmail1.start();
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void rechazarSolicitudVacaciones() {
        String json = "";
        try {
            String id = request.getParameter("id");
            String comentario = Util.setCodificacionCadena(request.getParameter("comentario"), "ISO-8859-1");
            String identificacion = request.getParameter("identificacion");
            String tipo_novedad = request.getParameter("tipo_novedad");
            String jefe_directo = request.getParameter("jefe_directo");
            this.dao.rechazarSolicitudVacaciones(usuario, comentario, id);
            json = "{\"respuesta\":\"Guardado\"}";
            if (json.contains("Guardado")) {
                //Envio de Correo a Recursos Humanos
                    JsonObject jobj = dao.getInfoCuentaEnvioCorreo();
//                    String empleado = dao.cargarNombre(identificacion);
//                    String tipo_novedad1 = dao.cargarNombreTipoNovedad(tipo_novedad);
                    String jefe_directo1 = dao.cargarNombreJefeDirecto(jefe_directo);
//                    String MyMessage = "El empleado " + empleado + " ha realizado una solicitud de " + tipo_novedad1 + ", la cual ha sido rechazada por " + jefe_directo1 + ". Favor ingrese al m�dulo de Solicitudes Pendientes por Visto Bueno para poder visualizarla.";
//                    String mailsToSend = dao.getInfoCuentaEnvioCorreoDestino2();
//                    String asunto = "Nueva solicitud de Novedad Aprobada por Jefe de Area";
//                    Thread hiloEmail = new Thread(new EmailSenderService(jobj, "RRHH", MyMessage, mailsToSend, asunto, usuario), "hilo");
//                    hiloEmail.start();
                //Envio correo al empleado 
                    String MyMessage1 = "Su Solicitud de Vacaciones ha sido rechazada por " + jefe_directo1 + ". Favor ingrese al m�dulo de Registro de Solicitudes para ver el detalle.";
                    String mailsToSend1 = dao.getInfoCuentaEnvioCorreoDestino2();
                    String asunto1 = "Solicitud de Vacaciones Rechazada por Jefe de Area";
                    Thread hiloEmail1 = new Thread(new EmailSenderService(jobj, "RRHH", MyMessage1, mailsToSend1, asunto1, usuario), "hilo");
                    hiloEmail1.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void vistoBuenoNovedadIncapacidad() {
        String json = "";
        try {
            String id = request.getParameter("id");
            //String remunerado = "SI";
            String observaciones = Util.setCodificacionCadena(request.getParameter("observaciones"), "ISO-8859-1");
            Double valor_recobro_eps = Double.parseDouble(request.getParameter("valor_recobro_eps"));
            Double valor_recobro_arl = Double.parseDouble(request.getParameter("valor_recobro_arl"));
            String numsolicitud = request.getParameter("numsolicitud");
            String identificacion = request.getParameter("identificacion");
            this.dao.vistoBuenoNovedadIncapacidad(usuario, observaciones, id,valor_recobro_eps, valor_recobro_arl);
            String document_type = "FAC";
            json = "{\"respuesta\":\"Guardado\"}";
            if (json.contains("Guardado")) {
                String empleado = dao.cargarNombre(identificacion);
                TransaccionService tService = new TransaccionService(usuario.getBd());
                tService.crearStatement();
                
                if (valor_recobro_eps != 0.00){
                    String documento = dao.UpCP(document_type);
                    String nit = request.getParameter("nit_eps");
                    Double valor = valor_recobro_eps;
                    String descripcion = "COBRO DE INCAPACIDAD DE " + empleado + " A EPS";
                    tService.getSt().addBatch(this.dao.ingresarCXCRecobro(documento, nit, descripcion, valor,numsolicitud));
                    tService.getSt().addBatch(this.dao.ingresarDetalleCXCRecobro(documento, nit, descripcion, valor));
                }
                
                if (valor_recobro_arl != 0.00){
                    String documento = dao.UpCP(document_type);
                    String nit = request.getParameter("nit_arl");
                    Double valor = valor_recobro_arl;
                    String descripcion = "COBRO DE INCAPACIDAD DE " + empleado + " A ARL";
                    tService.getSt().addBatch(this.dao.ingresarCXCRecobro(documento, nit, descripcion, valor,numsolicitud));
                    tService.getSt().addBatch(this.dao.ingresarDetalleCXCRecobro(documento, nit, descripcion, valor));
                }
                tService.execute();
//                JsonObject jobj = dao.getInfoCuentaEnvioCorreo();
//                String tipo_novedad1 = dao.cargarNombreTipoNovedad(tipo_novedad);
//                String jefe_directo1 = dao.cargarNombreJefeDirecto(jefe_directo);
//                String MyMessage = "Se informa que su solicitud de " + tipo_novedad1 + ", ha sido aprobada por " + jefe_directo1 + " y el �rea de Recursos Humanos. Favor ingrese al m�dulo de Registro de Solicitudes de Novedad para poder visualizarla.";
//                String mailsToSend = dao.getInfoCuentaEnvioCorreoDestino3(solicitante);
//                String asunto = "Solicitud de Novedad Aprobada";
//                Thread hiloEmail = new Thread(new EmailSenderService(jobj, "RRHH", MyMessage, mailsToSend, asunto, usuario), "hilo");
//                hiloEmail.start();
//
                          
            }
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void cargarArchivo() {
        String respuesta = "{}";
        String identificacion = "";
        try {

            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");//se consiguen los datos de db.properties

            String directorioArchivos = rb.getString("rutaImagenes") + "gestionadministrativa/Novedades/";//se establece la ruta del archivo

            //this.createDir(directorioArchivos );
            if (ServletFileUpload.isMultipartContent(request)) {

                ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
                List fileItemsList = servletFileUpload.parseRequest(request);

                //// Itero para obtener todos los FileItem
                Iterator it = fileItemsList.iterator();
                String nombre_archivo;
                String fieldtem = "", valortem = "";

                while (it.hasNext()) {

                    FileItem fileItem = (FileItem) it.next();

                    if ((fileItem.isFormField())) {

                        fieldtem = fileItem.getFieldName();
                        valortem = fileItem.getString();

                        if (fieldtem.equals("identificacion1")) {
                            identificacion = valortem;
                        }

                    }

                    if (!(fileItem.isFormField())) {

                        //// Nombre del archivo en el cliente. Algunos navegadores (por ej. IE 6)
                        //// incluyen el path completo, lo que puede implicar separar path
                        //// de nombre.
                        if (fileItem.getName() != "" && !fileItem.getName().isEmpty() && fileItem.getSize() > 0) {

                            String nombreArchivo = fileItem.getName();

                            String[] nombrearreglo = nombreArchivo.split("\\\\");
                            String nombreArchivoremix = nombrearreglo[(nombrearreglo.length - 1)];
                            String rutaArchivo = directorioArchivos + identificacion;
                            nombre_archivo = nombreArchivoremix;

                            this.createDir(rutaArchivo);

                            File archivo = new File(rutaArchivo + "/" + nombre_archivo);

                            fileItem.write(archivo);
                        }

                    }

                }
            }
            //dao.actualizarReferencia(identificacion);
            respuesta = "{\"respuesta\":\"OK\"}";

        } catch (Exception ee) {
            respuesta = "{\"respuesta\":\"" + ee.getMessage() + "\"}";
            //respuesta = "{\"respuesta\":\"" + ee.getMessage() + "\"}";
            System.out.println("eroror:" + ee.toString() + "__" + ee.getMessage());
        } finally {
            try {
                this.printlnResponseAjax(respuesta, "application/json;");
            } catch (Exception ex) {
                java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private void getNombresArchivosNovedades() {
        java.util.List lista = null;
        Gson gson = new Gson();
        try {
            String identificacion = (request.getParameter("identificacion1") != null) ? request.getParameter("identificacion1") : "";
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String rutaOrigen = rb.getString("rutaImagenes") + "gestionadministrativa/Novedades/";
            this.printlnResponseAjax(gson.toJson(dao.getNombresArchivosNovedades(rutaOrigen, identificacion)), "application/json");
        } catch (Exception e) {
            System.out.println("errorrr::" + e.toString() + "__" + e.getMessage());
        }
    }

    public void almacenarArchivoEnCarpetaUsuarioNovedades() throws Exception {
        try {
            String identificacion = (request.getParameter("identificacion1") != null) ? request.getParameter("identificacion1") : "";
            String nomarchivo = (request.getParameter("nomarchivo") != null) ? request.getParameter("nomarchivo") : "";
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            String rutaOrigen = rb.getString("rutaImagenes") + "gestionadministrativa/Novedades/";
            String rutaDestino = rb.getString("ruta") + "/images/multiservicios/" + usuario.getLogin() + "/";
            if (dao.almacenarArchivoEnCarpetaUsuarioNovedades(identificacion, ("" + rutaOrigen + identificacion), ("" + rutaDestino), nomarchivo)) {
                this.printlnResponseAjax("{\"respuesta\":\"SI\",\"login\":\"" + usuario.getLogin() + "\"}", "application/json");
            } else {
                this.printlnResponseAjax("{\"respuesta\":\"NO\"}", "application/json");
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }


    private void novedadTramitada() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String id = request.getParameter("id");
            this.dao.novedadTramitada(usuario, id);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void calcularDiasFestivos() {
        String json = "";
        try {
            String fechaIni = request.getParameter("fecha_ini");
            String fechaFin = request.getParameter("fecha_fin");
            //String duracion_dias = request.getParameter("duracion_dias");
            json = "{\"respuesta\":\"" + this.dao.calcularDiasFestivos(fechaIni, fechaFin) + "\"}";
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void novedadNoTramite() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String id = request.getParameter("id");
            this.dao.novedadNoTramite(usuario, id);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void cargarTiposHorasExtras() {
        try {
            this.printlnResponse(dao.cargarTiposHorasExtras(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarHorasExtras(String usuario) {
        try {
            this.printlnResponse(dao.cargarHorasExtras(usuario), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void guardarHorasExtras() {

        try {
            JsonObject objeto = new JsonObject();
            String json = request.getParameter("informacion") != null ? request.getParameter("informacion") : "{}";
            JsonObject obj = (JsonObject) (new JsonParser()).parse(json);
            String resp = dao.guardarHorasExtras(obj, usuario);
            this.printlnResponseAjax(resp, "application/json;");

            //Envio de email 
            if (resp.contains("Guardado")) {
                JsonObject jobj = dao.getInfoCuentaEnvioCorreo();
                objeto = obj.getAsJsonArray("info").get(0).getAsJsonObject();
                String empleado = dao.cargarNombre(objeto.get("identificacion").getAsString());
                String tipo_hora = dao.cargarNombreTipoHora(objeto.get("tipo_hora").getAsString());
                String MyMessage = "El empleado " + empleado + " ha realizado una solicitud de Horas Extras " + tipo_hora + ". Favor ingrese al m�dulo de Gesti�n de Solicitudes para poder visualizarla.";
                String mailsToSend = dao.getInfoCuentaEnvioCorreoDestino(objeto.get("jefe_directo").getAsString());

                String asunto = "Nueva solicitud de Horas Extras";
                Thread hiloEmail = new Thread(new EmailSenderService(jobj, "RRHH", MyMessage, mailsToSend, asunto, usuario), "hilo");
                hiloEmail.start();
            }

        } //        try {
        //            String json = request.getParameter("informacion") != null ? request.getParameter("informacion") : "{}";
        //            JsonObject obj = (JsonObject) (new JsonParser()).parse(json);
        //            this.printlnResponseAjax(dao.guardarHorasExtras(obj, usuario), "application/json;");
        //
        //        } 
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void actualizarHorasExtras() {
        String json = "";
        try {
            String id = request.getParameter("id");
            String tipo_hora = request.getParameter("tipo_hora");
            String identificacion = request.getParameter("identificacion");
            String fecha_solicitud = request.getParameter("fecha_solicitud");
            String fecha_trabajo = request.getParameter("fecha_trabajo");
            String hora_ini = request.getParameter("hora_ini");
            String hora_fin = request.getParameter("hora_fin");
            String duracion_horas = request.getParameter("duracion_horas");
            String jefe_directo = request.getParameter("jefe_directo");
            String descripcion = Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1");
            this.dao.actualizarHorasExtras(usuario, tipo_hora, identificacion, fecha_solicitud, fecha_trabajo,
                    hora_ini, hora_fin, duracion_horas, jefe_directo, descripcion, id);
            json = "{\"respuesta\":\"Guardado\"}";
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void cambiarEstadoHorasExtras() {
        Gson gson = new Gson();
        String json = "";
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            json = "{\"rows\":" + gson.toJson(dao.cambiarEstadoHorasExtras(usuario, id)) + "}";
        } catch (Exception e) {
            json = "{\"respuesta\":\"ERROR\"}";
            e.printStackTrace();
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void cargarHorasExtrasPorAprobar(String usuario) {
        try {
            this.printlnResponse(dao.cargarHorasExtrasPorAprobar(usuario), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void aprobarHorasExtras() {
        String json = "";
        try {
            String id = request.getParameter("id");
            String aprobado = request.getParameter("aprobado");
            String comentario = Util.setCodificacionCadena(request.getParameter("comentario"), "ISO-8859-1");
            String identificacion = request.getParameter("identificacion");

            String tipo_hora = request.getParameter("tipo_hora");
            String jefe_directo = request.getParameter("jefe_directo");
            this.dao.aprobarHorasExtras(usuario, aprobado, comentario, id);
            json = "{\"respuesta\":\"Guardado\"}";
            if (json.contains("Guardado")) {
                if (aprobado.contains("S")) {
                    JsonObject jobj = dao.getInfoCuentaEnvioCorreo();
                    String empleado = dao.cargarNombre(identificacion);
                    String tipo_hora1 = dao.cargarNombreTipoHora(tipo_hora);
                    String jefe_directo1 = dao.cargarNombreJefeDirecto(jefe_directo);
                    String MyMessage = "El empleado " + empleado + " ha realizado una solicitud de Horas Extras " + tipo_hora1 + ", la cual ha sido aprobada por " + jefe_directo1 + ". Favor ingrese al m�dulo de Listado de Solicitudes para poder visualizarla.";
                    String mailsToSend = dao.getInfoCuentaEnvioCorreoDestino2();
                    String asunto = "Nueva solicitud de Horas Extras Aprobada por Jefe de Area";
                    Thread hiloEmail = new Thread(new EmailSenderService(jobj, "RRHH", MyMessage, mailsToSend, asunto, usuario), "hilo");
                    hiloEmail.start();
                }
            }
        } //        try {
        //            String id = request.getParameter("id");
        //            String aprobado = request.getParameter("aprobado");
        //            String comentario = Util.setCodificacionCadena(request.getParameter("comentario"), "ISO-8859-1");
        //            this.dao.aprobarHorasExtras(usuario, aprobado, comentario, id);
        //        }
        catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void cargarHorasExtrasAprobadas() {
        try {
            this.printlnResponse(dao.cargarHorasExtrasAprobadas(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void vistoBuenoHorasExtras() {
        String json = "";
        try {
            String id = request.getParameter("id");
            String remunerado = request.getParameter("remunerada");
            String observaciones = Util.setCodificacionCadena(request.getParameter("observaciones"), "ISO-8859-1");
            String tipo_hora = request.getParameter("tipo_hora");
            String jefe_directo = request.getParameter("jefe_directo");
            String solicitante = request.getParameter("identificacion");
            this.dao.vistoBuenoHorasExtras(usuario, remunerado, observaciones, id);
            json = "{\"respuesta\":\"Guardado\"}";
            if (json.contains("Guardado")) {
                JsonObject jobj = dao.getInfoCuentaEnvioCorreo();
                String tipo_hora1 = dao.cargarNombreTipoHora(tipo_hora);
                String jefe_directo1 = dao.cargarNombreJefeDirecto(jefe_directo);
                String MyMessage = "Se informa que su solicitud de Horas Extras " + tipo_hora1 + ", ha sido aprobada por " + jefe_directo1 + " y el �rea de Recursos Humanos. Favor ingrese al m�dulo de Listado de Solicitudes Aprobadas para poder visualizarla.";
                String mailsToSend = dao.getInfoCuentaEnvioCorreoDestino3(solicitante);
                String asunto = "Solicitud de Horas Extras Aprobada";
                Thread hiloEmail = new Thread(new EmailSenderService(jobj, "RRHH", MyMessage, mailsToSend, asunto, usuario), "hilo");
                hiloEmail.start();

            }
        } //        try {
        //            String id = request.getParameter("id");
        //            String remunerado = request.getParameter("remunerada");
        //            String observaciones = Util.setCodificacionCadena(request.getParameter("observaciones"), "ISO-8859-1");
        //            this.dao.vistoBuenoHorasExtras(usuario, remunerado, observaciones, id);
        //        }
        catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void cargarOpcionesMenu(String usuario) {
        try {
            this.printlnResponse(dao.cargarOpcionesMenu(usuario), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    
    private void cargarListaEnfermedades() {
        try {
            this.printlnResponse(dao.cargarListaEnfermedades(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void numeroSolicitudNovedad() {
        String json = "";
        try {
            json = "{\"respuesta\":\"" + this.dao.numeroSolicitudNovedad() + "\"}";
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void pocesoActualTrabajador() {
        String json = "";
        String identificacion = request.getParameter("identificacion");
        try {
            json = this.dao.pocesoActualTrabajador(identificacion);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void pocesoActual() {
        try {
            this.printlnResponse(dao.pocesoActual(usuario), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarTipoIncapacidad() {
        try {
            this.printlnResponse(dao.cargarTipoIncapacidad(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarEPSoARL() {
        try {
            String origen = request.getParameter("origen") != null ? request.getParameter("origen") : "";
            this.printlnResponse(dao.cargarEPSoARL(origen), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarTrazabilidad() {
        try {
            String identificacion2 = request.getParameter("identificacion2") != null ? request.getParameter("identificacion2") : "";
            this.printlnResponse(dao.cargarTrazabilidad(identificacion2), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarOpcionesMenuGestion(String usuario) {
        try {
            this.printlnResponse(dao.cargarOpcionesMenuGestion(usuario), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarIncapacidades2(String bd) {
        try {
            String fechaInicio = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";
            String fechaFin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
            String tipo_novedad = request.getParameter("tipo_novedad1") != null ? request.getParameter("tipo_novedad1") : "";
            String identificacion = request.getParameter("identificacion2") != null ? request.getParameter("identificacion2") : "";
            //String status = request.getParameter("status") != null ? request.getParameter("status") : "";
            this.printlnResponse(dao.cargarIncapacidades2(fechaInicio, fechaFin, identificacion, tipo_novedad, usuario, usuario.getBd()), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarIncapacidades3() {
        try {
            String fechaInicio = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";
            String fechaFin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
            String tipo_novedad = request.getParameter("tipo_novedad1") != null ? request.getParameter("tipo_novedad1") : "";
            String identificacion = request.getParameter("identificacion2") != null ? request.getParameter("identificacion2") : "";
            String remunerado = request.getParameter("remunerada1") != null ? request.getParameter("remunerada1") : "";
            this.printlnResponse(dao.cargarIncapacidades3(fechaInicio, fechaFin, identificacion, tipo_novedad, remunerado), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarOpcionesMenuVistoBueno(String usuario) {
        try {
            this.printlnResponse(dao.cargarOpcionesMenuVistoBueno(usuario), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarTipoVacaciones() {
        try {
            this.printlnResponse(dao.cargarTipoVacaciones(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarVacaciones(String usuario) {
        try {
            String fechaInicio = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";
            String fechaFin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
            String tipo_novedad = request.getParameter("tipo_novedad1") != null ? request.getParameter("tipo_novedad1") : "";
            String identificacion = request.getParameter("identificacion2") != null ? request.getParameter("identificacion2") : "";
            String status = request.getParameter("status") != null ? request.getParameter("status") : "";
            this.printlnResponse(dao.cargarVacaciones(fechaInicio, fechaFin, identificacion, tipo_novedad, status, usuario), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void calcularDiasSinFestivos() {
        String json = "";
        try {
            String fechaIni = request.getParameter("fecha_ini");
            //String fechaFin = request.getParameter("fecha_fin");
            String duracion_dias = request.getParameter("duracion_dias");
            json = "{\"respuesta\":\"" + this.dao.calcularDiasSinFestivos(fechaIni, duracion_dias) + "\"}";
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }
    
    private void cargarSaldo() {
        try {
            String identificacion = request.getParameter("identificacion");
            this.printlnResponse(dao.cargarSaldo(identificacion), "application/text;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarPasivoVacacional() {
        try {
            String identificacion = request.getParameter("identificacion2") != null ? request.getParameter("identificacion2") : "";
            this.printlnResponse(dao.cargarPasivoVacacional(identificacion), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
    
    private void cargarFechaIngreso() {
        try {
            String identificacion = request.getParameter("identificacion");
            this.printlnResponse(dao.cargarFechaIngreso(identificacion), "application/text;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
    
    
    private void cargarNombreEPS(){
        try {
            String identificacion = request.getParameter("identificacion");
            this.printlnResponse(dao.cargarNombreEPS(identificacion), "application/text;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarNombreARL() {
        try {
            String identificacion = request.getParameter("identificacion");
            this.printlnResponse(dao.cargarNombreARL(identificacion), "application/text;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarSalario() {
        try {
            String identificacion = request.getParameter("identificacion");
            this.printlnResponse(dao.cargarSalario(identificacion), "application/text;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

//    private void actualizarPeriodoActual() {
//        try {
//            String identificacion = request.getParameter("identificacion2") != null ? request.getParameter("identificacion2") : "";
//            this.printlnResponse(dao.actualizarPeriodoActual(identificacion), "application/text;");
//        } catch (Exception ex) {
//            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
//            ex.printStackTrace();
//        }
//    }

    private void cargarVacacionesPorAprobar(String bd) {
        try {
            String fechaInicio = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";
            String fechaFin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
            String tipo_novedad = request.getParameter("tipo_novedad1") != null ? request.getParameter("tipo_novedad1") : "";
            String identificacion = request.getParameter("identificacion2") != null ? request.getParameter("identificacion2") : "";
            String status = request.getParameter("status") != null ? request.getParameter("status") : "";
            this.printlnResponse(dao.cargarVacacionesPorAprobar(fechaInicio, fechaFin, identificacion, status, tipo_novedad, usuario, usuario.getBd()), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarVacacionesPorVistoBueno(String bd) {
        try {
            String fechaInicio = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";
            String fechaFin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
            String tipo_novedad = request.getParameter("tipo_novedad1") != null ? request.getParameter("tipo_novedad1") : "";
            String identificacion = request.getParameter("identificacion2") != null ? request.getParameter("identificacion2") : "";
            String remunerado = request.getParameter("remunerada1") != null ? request.getParameter("remunerada1") : "";
            this.printlnResponse(dao.cargarVacacionesPorVistoBueno(fechaInicio, fechaFin, identificacion, tipo_novedad, remunerado, usuario.getBd()), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void vistoBuenoNovedadVacaciones() {
        String json = "";
        try {
            String id = request.getParameter("id");
            String observaciones = Util.setCodificacionCadena(request.getParameter("observaciones"), "ISO-8859-1");
            //String numsolicitud = request.getParameter("numsolicitud");
            String identificacion = request.getParameter("identificacion");
            String duracion_dias = request.getParameter("duracion_dias");
            String dias_compensados = request.getParameter("dias_compensados");
            String jefe_directo = request.getParameter("jefe_directo");
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.vistoBuenoNovedadVacaciones(usuario, observaciones, id));
            //tService.getSt().addBatch(this.dao.actualizarPasivo(usuario, identificacion, duracion_dias, dias_compensados));
            tService.execute();
            json = "{\"respuesta\":\"Guardado\"}";
            if (json.contains("Guardado")) {
                JsonObject jobj = dao.getInfoCuentaEnvioCorreo();
                String jefe_directo1 = dao.cargarNombreJefeDirecto(jefe_directo);
                String MyMessage = "Se informa que su solicitud de Vacaciones, ha sido aprobada por " + jefe_directo1 + " y el �rea de Recursos Humanos. Favor ingrese al m�dulo de Registro de Solicitudes de Novedad para poder visualizarla.";
                String mailsToSend = dao.getInfoCuentaEnvioCorreoDestino3(identificacion);
                String asunto = "Solicitud de Vacaciones Aprobada";
                Thread hiloEmail = new Thread(new EmailSenderService(jobj, "RRHH", MyMessage, mailsToSend, asunto, usuario), "hilo");
                hiloEmail.start();

                      
            }
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void exportarExcelPasivo() throws Exception {
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Reporte_Pasivo", titulo = "REPORTE PASIVO VACACIONAL";

        cabecera = new String[]{"Fecha Ingreso", "Identificacion", "Nombre", "Periodo Inicial", "Periodo Final", "Saldo Inicial", "No. Dias",
            "Dias Causados", "Dias Disfrutados","Dias Compensados", "Saldo Final"};

        dimensiones = new short[]{
            5000, 5000, 9000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000
        };

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        this.printlnResponse(reponseJson, "text/plain");
    }
    
    private void exportarExcelAcumulado() throws Exception {
        String identificacion = request.getParameter("identificacion");
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(dao.calcularAcumuladoPasivo(identificacion));
        //JsonArray asJsonArray = (JsonArray) new JsonParser().parse();
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Acumulado_Pasivo", titulo = "ACUMULADO PASIVO";

        cabecera = new String[]{"Identificacion", "Nombre", "No. Dias","Dias Causados","Dias Disfrutados","Dias Compensados", "Saldo Final"};

        dimensiones = new short[]{
            5000, 9000, 5000, 5000, 5000, 5000, 5000
        };

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        this.printlnResponseAjax(reponseJson, "text/plain");
    }

    private void mostrarPeriodo() {
        try {
            String identificacion = request.getParameter("identificacion");
            this.printlnResponse(dao.mostrarPeriodo(identificacion), "application/text;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarPermisos(String usuario) {
        try {
            String fechaInicio = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";
            String fechaFin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
            String tipo_novedad = request.getParameter("tipo_novedad1") != null ? request.getParameter("tipo_novedad1") : "";
            String identificacion = request.getParameter("identificacion2") != null ? request.getParameter("identificacion2") : "";
            String status = request.getParameter("status") != null ? request.getParameter("status") : "";
            this.printlnResponse(dao.cargarPermisos(fechaInicio, fechaFin, identificacion, tipo_novedad, status, usuario), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarTipoPermisos() {
        try {
            this.printlnResponse(dao.cargarTipoPermisos(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
    
    private void guardarNovedadesPermisos() {
        try {
            JsonObject objeto = new JsonObject();
            String json = request.getParameter("informacion") != null ? request.getParameter("informacion") : "{}";
            JsonObject obj = (JsonObject) (new JsonParser()).parse(json);
            String resp = dao.guardarNovedadesPermisos(obj, usuario);
            this.printlnResponseAjax(resp, "application/json;");

            //Envio de email 
            if (resp.contains("Guardado")) {
                JsonObject jobj = dao.getInfoCuentaEnvioCorreo();
                objeto = obj.getAsJsonArray("info").get(0).getAsJsonObject();
                String empleado = dao.cargarNombre(objeto.get("identificacion").getAsString());
                //String tipo_novedad = dao.cargarNombreTipoNovedad(objeto.get("tipo_novedad").getAsString());
                String MyMessage = "El empleado " + empleado + " ha realizado una solicitud de Permiso. Favor ingrese al m�dulo de Gesti�n de Solicitudes para ver el detalle.";
                String mailsToSend = dao.getInfoCuentaEnvioCorreoDestino(objeto.get("jefe_directo").getAsString());
                String asunto = "Nueva solicitud de Permiso";
                Thread hiloEmail = new Thread(new EmailSenderService(jobj, "RRHH", MyMessage, mailsToSend, asunto, usuario), "hilo");
                hiloEmail.start();
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargarPermisosPorAprobar(String bd) {
        try {
            String fechaInicio = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";
            String fechaFin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
            String tipo_novedad = request.getParameter("tipo_novedad1") != null ? request.getParameter("tipo_novedad1") : "";
            String identificacion = request.getParameter("identificacion2") != null ? request.getParameter("identificacion2") : "";
            String status = request.getParameter("status") != null ? request.getParameter("status") : "";
            this.printlnResponse(dao.cargarPermisosPorAprobar(fechaInicio, fechaFin, identificacion, status, tipo_novedad, usuario, usuario.getBd()), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void aprobarSolicitudPermisos() {
        String json = "";
        try {
            String id = request.getParameter("id");
            String comentario = Util.setCodificacionCadena(request.getParameter("comentario"), "ISO-8859-1");
            String identificacion = request.getParameter("identificacion");
            String tipo_novedad = request.getParameter("tipo_novedad");
            String jefe_directo = request.getParameter("jefe_directo");
            this.dao.aprobarSolicitudPermisos(usuario, comentario, id);
            json = "{\"respuesta\":\"Guardado\"}";
            if (json.contains("Guardado")) {
                //Envio de Correo a Recursos Humanos
                    JsonObject jobj = dao.getInfoCuentaEnvioCorreo();
                    String empleado = dao.cargarNombre(identificacion);
                    String tipo_novedad1 = dao.cargarNombreTipoNovedad(tipo_novedad);
                    String jefe_directo1 = dao.cargarNombreJefeDirecto(jefe_directo);
                    String MyMessage = "El empleado " + empleado + " ha realizado una solicitud de " + tipo_novedad1 + ", la cual ha sido aprobada por " + jefe_directo1 + ". Favor ingrese al m�dulo de Visto Bueno de Solicitudes para ver el detalle.";
                    String mailsToSend = dao.getInfoCuentaEnvioCorreoDestino2();
                    String asunto = "Nueva Solicitud de " + tipo_novedad1 + " Aprobada por Jefe de Area";
                    Thread hiloEmail = new Thread(new EmailSenderService(jobj, "RRHH", MyMessage, mailsToSend, asunto, usuario), "hilo");
                    hiloEmail.start();
                //Envio correo al empleado 
                    String MyMessage1 = "Su Solicitud de " + tipo_novedad1 + " ha sido aprobada por " + jefe_directo1 + ". Favor ingrese al m�dulo de Registro de Solicitudes para ver el detalle.";
                    String mailsToSend1 = dao.getInfoCuentaEnvioCorreoDestino3(identificacion);
                    String asunto1 = "Solicitud de " + tipo_novedad1 + " Aprobada por Jefe de Area";
                    Thread hiloEmail1 = new Thread(new EmailSenderService(jobj, "RRHH", MyMessage1, mailsToSend1, asunto1, usuario), "hilo");
                    hiloEmail1.start();
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void rechazarSolicitudPermisos() {
        String json = "";
        try {
            String id = request.getParameter("id");
            String comentario = Util.setCodificacionCadena(request.getParameter("comentario"), "ISO-8859-1");
            String identificacion = request.getParameter("identificacion");
            String tipo_novedad = request.getParameter("tipo_novedad");
            String jefe_directo = request.getParameter("jefe_directo");
            this.dao.rechazarSolicitudPermisos(usuario, comentario, id);
            json = "{\"respuesta\":\"Guardado\"}";
            if (json.contains("Guardado")) {
                //Envio de Correo a Recursos Humanos
                    JsonObject jobj = dao.getInfoCuentaEnvioCorreo();
                    String empleado = dao.cargarNombre(identificacion);
                    String tipo_novedad1 = dao.cargarNombreTipoNovedad(tipo_novedad);
                    String jefe_directo1 = dao.cargarNombreJefeDirecto(jefe_directo);
                    String MyMessage = "El empleado " + empleado + " ha realizado una solicitud de " + tipo_novedad1 + ", la cual ha sido rechazada por " + jefe_directo1 + ". Favor ingrese al m�dulo de Visto Bueno de Solicitudes para ver el detalle.";
                    String mailsToSend = dao.getInfoCuentaEnvioCorreoDestino2();
                    String asunto = "Nueva solicitud de " + tipo_novedad1 + " Rechazada por Jefe de Area";
                    Thread hiloEmail = new Thread(new EmailSenderService(jobj, "RRHH", MyMessage, mailsToSend, asunto, usuario), "hilo");
                    hiloEmail.start();
                //Envio correo al empleado 
                    String MyMessage1 = "Su Solicitud de " + tipo_novedad1 + " ha sido rechazada por " + jefe_directo1 + ". Favor ingrese al m�dulo de Registro de Solicitudes para ver el detalle.";
                    String mailsToSend1 = dao.getInfoCuentaEnvioCorreoDestino2();
                    String asunto1 = "Solicitud de " + tipo_novedad1 + " Rechazada por Jefe de Area";
                    Thread hiloEmail1 = new Thread(new EmailSenderService(jobj, "RRHH", MyMessage1, mailsToSend1, asunto1, usuario), "hilo");
                    hiloEmail1.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void cargarPermisosPorVistoBueno(String bd) {
        try {
            String fechaInicio = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";
            String fechaFin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
            String tipo_novedad = request.getParameter("tipo_novedad1") != null ? request.getParameter("tipo_novedad1") : "";
            String identificacion = request.getParameter("identificacion2") != null ? request.getParameter("identificacion2") : "";
            String remunerado = request.getParameter("remunerada1") != null ? request.getParameter("remunerada1") : "";
            this.printlnResponse(dao.cargarPermisosPorVistoBueno(fechaInicio, fechaFin, identificacion, tipo_novedad, remunerado, usuario.getBd() ), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void vistoBuenoNovedadPermisos() {
        String json = "";
        String remunerado = "";
        try {
            String id = request.getParameter("id");
            String observaciones = Util.setCodificacionCadena(request.getParameter("observaciones"), "ISO-8859-1");
            String pagar = request.getParameter("pagar");
            String identificacion = request.getParameter("identificacion");
            String jefe_directo = request.getParameter("jefe_directo");
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.vistoBuenoNovedadPermisos(usuario, observaciones, pagar, id));
            tService.execute();
            json = "{\"respuesta\":\"Guardado\"}";
            if (pagar.equals("S")) {
                remunerado = "remunerado";
            }
            else
            {
                remunerado = "no remunerado";
            }
            if (json.contains("Guardado")) {
                JsonObject jobj = dao.getInfoCuentaEnvioCorreo();
                String MyMessage = "Se informa que su solicitud de Permiso, ha sido visado como " + remunerado + " por el �rea de Recursos Humanos. Favor ingrese al m�dulo de Registro de Solicitudes de Novedad para poder visualizarla.";
                String mailsToSend = dao.getInfoCuentaEnvioCorreoDestino3(identificacion);
                String asunto = "Solicitud de Permiso Visada";
                Thread hiloEmail = new Thread(new EmailSenderService(jobj, "RRHH", MyMessage, mailsToSend, asunto, usuario), "hilo");
                hiloEmail.start();

                      
            }
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void cargarCausalesRetiro() {
        try {
            this.printlnResponse(dao.cargarCausalesRetiro(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarBarrios() {
        try {
            String ciudad = request.getParameter("ciudad");
            this.printlnResponse(dao.cargarBarrios(ciudad), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarTipoLicencias() {
        try {
            this.printlnResponse(dao.cargarTipoLicencias(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarClasesPermiso() {
        try {
            this.printlnResponse(dao.cargarClasesPermiso(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
    }
    }

    private void guardarNovedadesLicencias() {
        try {
            JsonObject objeto = new JsonObject();
            String json = request.getParameter("informacion") != null ? request.getParameter("informacion") : "{}";
            JsonObject obj = (JsonObject) (new JsonParser()).parse(json);
            String resp = dao.guardarNovedadesLicencias(obj, usuario);
            this.printlnResponseAjax(resp, "application/json;");

            //Envio de email 
            if (resp.contains("Guardado")) {
                JsonObject jobj = dao.getInfoCuentaEnvioCorreo();
                objeto = obj.getAsJsonArray("info").get(0).getAsJsonObject();
                String empleado = dao.cargarNombre(objeto.get("identificacion").getAsString());
                //String tipo_novedad = dao.cargarNombreTipoNovedad(objeto.get("tipo_novedad").getAsString());
                String MyMessage = "El empleado " + empleado + " ha realizado un registro de Licencia Ley Maria. Favor ingrese al m�dulo de Gesti�n de Solicitudes para ver el detalle.";
                String mailsToSend = dao.getInfoCuentaEnvioCorreoDestino(objeto.get("jefe_directo").getAsString());
                String asunto = "Nuevo registro de Licencia Ley Maria";
                Thread hiloEmail = new Thread(new EmailSenderService(jobj, "RRHH", MyMessage, mailsToSend, asunto, usuario), "hilo");
                hiloEmail.start();
            }
            if (resp.contains("Guardado")){
                JsonObject jobj = dao.getInfoCuentaEnvioCorreo();
                objeto = obj.getAsJsonArray("info").get(0).getAsJsonObject();
                String empleado = dao.cargarNombre(objeto.get("identificacion").getAsString());
                String MyMessage = "El empleado " + empleado + " ha realizado un registro de Licencia Ley Maria. Favor ingrese al sistema para ver el detalle.";
                String mailsToSend = dao.getInfoCuentaEnvioCorreoDestino2();
                String asunto = "Nuevo registro de Licencia Ley Maria";
                Thread hiloEmail = new Thread(new EmailSenderService(jobj, "RRHH", MyMessage, mailsToSend, asunto, usuario), "hilo");
                hiloEmail.start();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargarClasesLicencias() {
        try {
            String tipo_novedad = request.getParameter("tipo_novedad");
            this.printlnResponse(dao.cargarClasesLicencias(tipo_novedad), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarLicencias(String bd) {
        try {
            String fechaInicio = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";
            String fechaFin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
            String tipo_novedad = request.getParameter("tipo_novedad1") != null ? request.getParameter("tipo_novedad1") : "";
            String identificacion = request.getParameter("identificacion2") != null ? request.getParameter("identificacion2") : "";
            String status = request.getParameter("status") != null ? request.getParameter("status") : "";
            this.printlnResponse(dao.cargarLicencias(fechaInicio, fechaFin, identificacion, tipo_novedad, status, usuario , usuario.getBd()), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarLicenciasPorAprobar(String bd) {
        try {
            String fechaInicio = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";
            String fechaFin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
            String tipo_novedad = request.getParameter("tipo_novedad1") != null ? request.getParameter("tipo_novedad1") : "";
            String identificacion = request.getParameter("identificacion2") != null ? request.getParameter("identificacion2") : "";
            String status = request.getParameter("status") != null ? request.getParameter("status") : "";
            this.printlnResponse(dao.cargarLicenciasPorAprobar(fechaInicio, fechaFin, identificacion, status, tipo_novedad, usuario, usuario.getBd()), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarLicenciasPorVistoBueno(String bd) {
        try {
            String fechaInicio = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";
            String fechaFin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
            String tipo_novedad = request.getParameter("tipo_novedad1") != null ? request.getParameter("tipo_novedad1") : "";
            String identificacion = request.getParameter("identificacion2") != null ? request.getParameter("identificacion2") : "";
            String remunerado = request.getParameter("remunerada1") != null ? request.getParameter("remunerada1") : "";
            this.printlnResponse(dao.cargarLicenciasPorVistoBueno(fechaInicio, fechaFin, identificacion, tipo_novedad, remunerado, usuario.getBd() ), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void vistoBuenoNovedadLicencia() {
        String json = "";
        String remunerado = "";
        try {
            String id = request.getParameter("id");
            String observaciones = Util.setCodificacionCadena(request.getParameter("observaciones"), "ISO-8859-1");
            String identificacion = request.getParameter("identificacion");
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.vistoBuenoNovedadLicencia(usuario, observaciones, id));
            tService.execute();
            json = "{\"respuesta\":\"Guardado\"}";
            if (json.contains("Guardado")) {
                JsonObject jobj = dao.getInfoCuentaEnvioCorreo();
                String MyMessage = "Se informa que su solicitud de Licencia, ha sido visado  por el �rea de Recursos Humanos. Favor ingrese al m�dulo de Registro de Solicitudes de Novedad para poder visualizarla.";
                String mailsToSend = dao.getInfoCuentaEnvioCorreoDestino3(identificacion);
                String asunto = "Solicitud de Licencia Visada";
                Thread hiloEmail = new Thread(new EmailSenderService(jobj, "RRHH", MyMessage, mailsToSend, asunto, usuario), "hilo");
                hiloEmail.start();

                      
            }
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void cargarTipoOtrasLicencias() {
        try {
            this.printlnResponse(dao.cargarTipoOtrasLicencias(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarOtrasLicencias(String bd) {
        try {
            String fechaInicio = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";
            String fechaFin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
            String tipo_novedad = request.getParameter("tipo_novedad1") != null ? request.getParameter("tipo_novedad1") : "";
            String identificacion = request.getParameter("identificacion2") != null ? request.getParameter("identificacion2") : "";
            String status = request.getParameter("status") != null ? request.getParameter("status") : "";
            this.printlnResponse(dao.cargarOtrasLicencias(fechaInicio, fechaFin, identificacion, tipo_novedad, status, usuario , usuario.getBd()), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void guardarNovedadesOtrasLicencias() {
        try {
            JsonObject objeto = new JsonObject();
            String json = request.getParameter("informacion") != null ? request.getParameter("informacion") : "{}";
            JsonObject obj = (JsonObject) (new JsonParser()).parse(json);
            String resp = dao.guardarNovedadesLicencias(obj, usuario);
            this.printlnResponseAjax(resp, "application/json;");

            //Envio de email 
            if (resp.contains("Guardado")) {
                JsonObject jobj = dao.getInfoCuentaEnvioCorreo();
                objeto = obj.getAsJsonArray("info").get(0).getAsJsonObject();
                String empleado = dao.cargarNombre(objeto.get("identificacion").getAsString());
                String tipo_licencia= dao.cargarNombreTipoLicencia(objeto.get("razon").getAsString());
                String MyMessage = "El empleado " + empleado + " ha realizado una Solicitud de Licencia " +tipo_licencia+ ". Favor ingrese al m�dulo de Gesti�n de Solicitudes para ver el detalle.";
                String mailsToSend = dao.getInfoCuentaEnvioCorreoDestino(objeto.get("jefe_directo").getAsString());
                String asunto = "Nueva Solicitud de Licencia "+ tipo_licencia;
                Thread hiloEmail = new Thread(new EmailSenderService(jobj, "RRHH", MyMessage, mailsToSend, asunto, usuario), "hilo");
                hiloEmail.start();
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cargarOtrasLicenciasPorAprobar(String bd) {
        try {
            String fechaInicio = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";
            String fechaFin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
            String tipo_novedad = request.getParameter("tipo_novedad1") != null ? request.getParameter("tipo_novedad1") : "";
            String identificacion = request.getParameter("identificacion2") != null ? request.getParameter("identificacion2") : "";
            String status = request.getParameter("status") != null ? request.getParameter("status") : "";
            this.printlnResponse(dao.cargarOtrasLicenciasPorAprobar(fechaInicio, fechaFin, identificacion, status, tipo_novedad, usuario, usuario.getBd()), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void aprobarSolicitudLicencias() {
        String json = "";
        try {
            String id = request.getParameter("id");
            String comentario = Util.setCodificacionCadena(request.getParameter("comentario"), "ISO-8859-1");
            String identificacion = request.getParameter("identificacion");
            //String tipo_novedad = request.getParameter("tipo_novedad");
            String razon = request.getParameter("razon");
            String jefe_directo = request.getParameter("jefe_directo");
            this.dao.aprobarSolicitudLicencias(usuario, comentario, id);
            json = "{\"respuesta\":\"Guardado\"}";
            if (json.contains("Guardado")) {
                //Envio de Correo a Recursos Humanos
                    JsonObject jobj = dao.getInfoCuentaEnvioCorreo();
                    String empleado = dao.cargarNombre(identificacion);
                    //String tipo_novedad1 = dao.cargarNombreTipoNovedad(tipo_novedad);
                    String tipo_licencia1= dao.cargarNombreTipoLicencia(razon);
                    String jefe_directo1 = dao.cargarNombreJefeDirecto(jefe_directo);
                    String MyMessage = "El empleado " + empleado + " ha realizado una solicitud de Licencia " + tipo_licencia1 + ", la cual ha sido aprobada por " + jefe_directo1 + ". Favor ingrese al m�dulo de Visto Bueno de Solicitudes para ver el detalle.";
                    String mailsToSend = dao.getInfoCuentaEnvioCorreoDestino2();
                    String asunto = "Nueva Solicitud de Licencia" + tipo_licencia1 + " Aprobada por Jefe de Area";
                    Thread hiloEmail = new Thread(new EmailSenderService(jobj, "RRHH", MyMessage, mailsToSend, asunto, usuario), "hilo");
                    hiloEmail.start();
                //Envio correo al empleado 
                    String MyMessage1 = "Su Solicitud de Licencia " + tipo_licencia1 + " ha sido aprobada por " + jefe_directo1 + ". Favor ingrese al m�dulo de Registro de Solicitudes para ver el detalle.";
                    String mailsToSend1 = dao.getInfoCuentaEnvioCorreoDestino3(identificacion);
                    String asunto1 = "Solicitud de Licencia" + tipo_licencia1 + " Aprobada por Jefe de Area";
                    Thread hiloEmail1 = new Thread(new EmailSenderService(jobj, "RRHH", MyMessage1, mailsToSend1, asunto1, usuario), "hilo");
                    hiloEmail1.start();
            }
            
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void rechazarSolicitudLicencias() {
         String json = "";
        try {
            String id = request.getParameter("id");
            String comentario = Util.setCodificacionCadena(request.getParameter("comentario"), "ISO-8859-1");
            String identificacion = request.getParameter("identificacion");
            String tipo_novedad = request.getParameter("tipo_novedad");
            String jefe_directo = request.getParameter("jefe_directo");
            this.dao.rechazarSolicitudLicencias(usuario, comentario, id);
            json = "{\"respuesta\":\"Guardado\"}";
            if (json.contains("Guardado")) {
                //Envio de Correo a Recursos Humanos
                    JsonObject jobj = dao.getInfoCuentaEnvioCorreo();
                    String empleado = dao.cargarNombre(identificacion);
                    String tipo_novedad1 = dao.cargarNombreTipoNovedad(tipo_novedad);
                    String jefe_directo1 = dao.cargarNombreJefeDirecto(jefe_directo);
                    String MyMessage = "El empleado " + empleado + " ha realizado una solicitud de " + tipo_novedad1 + ", la cual ha sido rechazada por " + jefe_directo1 + ". Favor ingrese al m�dulo de Visto Bueno de Solicitudes para ver el detalle.";
                    String mailsToSend = dao.getInfoCuentaEnvioCorreoDestino2();
                    String asunto = "Nueva solicitud de " + tipo_novedad1 + " Rechazada por Jefe de Area";
                    Thread hiloEmail = new Thread(new EmailSenderService(jobj, "RRHH", MyMessage, mailsToSend, asunto, usuario), "hilo");
                    hiloEmail.start();
                //Envio correo al empleado 
                    String MyMessage1 = "Su Solicitud de " + tipo_novedad1 + " ha sido rechazada por " + jefe_directo1 + ". Favor ingrese al m�dulo de Registro de Solicitudes para ver el detalle.";
                    String mailsToSend1 = dao.getInfoCuentaEnvioCorreoDestino2();
                    String asunto1 = "Solicitud de " + tipo_novedad1 + " Rechazada por Jefe de Area";
                    Thread hiloEmail1 = new Thread(new EmailSenderService(jobj, "RRHH", MyMessage1, mailsToSend1, asunto1, usuario), "hilo");
                    hiloEmail1.start();
            }
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void cargarOtrasLicenciasPorVistoBueno(String bd) {
        try {
            String fechaInicio = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";
            String fechaFin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
            String tipo_novedad = request.getParameter("tipo_novedad1") != null ? request.getParameter("tipo_novedad1") : "";
            String identificacion = request.getParameter("identificacion2") != null ? request.getParameter("identificacion2") : "";
            String remunerado = request.getParameter("remunerada1") != null ? request.getParameter("remunerada1") : "";
            this.printlnResponse(dao.cargarOtrasLicenciasPorVistoBueno(fechaInicio, fechaFin, identificacion, tipo_novedad, remunerado, usuario.getBd() ), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdmonRecursosHumanosAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }
    
    private void exportarExcelIncapacidades(String bd) throws Exception {
        String fechaInicio = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";  
        String fechaFin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
        String tipo_novedad = request.getParameter("tipo_novedad1") != null ? request.getParameter("tipo_novedad1") : "";
        String identificacion = request.getParameter("identificacion2") != null ? request.getParameter("identificacion2") : "";
        String remunerado = request.getParameter("remunerada1") != null ? request.getParameter("remunerada1") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(dao.listarIncapacidades(fechaInicio, fechaFin, identificacion, tipo_novedad, remunerado, usuario.getBd()));
        //JsonArray asJsonArray = (JsonArray) new JsonParser().parse();
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Listado_Incapacidades_Empleados", titulo = "LISTADO INCAPACIDADES";

        cabecera = new String[]{"NUMERO_SOLICITUD", "TIPO NOVEDAD", "ORIGEN", "ENTIDAD", "FECHA", "IDENTIFICACION", "NOMBRE", "FECHA INICIAL", "DURACION", "FECHA FINAL", "CODIGO ENFERMEDAD",
            "ENFERMEDAD", "DESCRIPCION", "JEFE DIRECTO", "FECHA VISTO BUENO", "USUARIO VISTO BUENO", "OBSERVACIONES", "FECHA DE CREACION", "USUARIO CREACION", "VALOR RECOBRO ARL",
            "VALOR RECOBRO EPS", "SALARIO", "EPS","ARL","MACROPROCESO","PROCESO"};

        dimensiones = new short[]{
            5000, 5000, 7000, 7000, 7000, 5000, 5000, 5000, 9000, 5000, 5000, 5000, 5000, 7000, 9000,
            9000, 9000, 5000, 5000, 5000, 5000, 5000
        };

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        this.printlnResponseAjax(reponseJson, "text/plain");
    }

    private void exportarExcelVacaciones(String bd) throws Exception{
        String fechaInicio = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";  
        String fechaFin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
        String tipo_novedad = request.getParameter("tipo_novedad1") != null ? request.getParameter("tipo_novedad1") : "";
        String identificacion = request.getParameter("identificacion2") != null ? request.getParameter("identificacion2") : "";
        String remunerado = request.getParameter("remunerada1") != null ? request.getParameter("remunerada1") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(dao.listarVacaciones(fechaInicio, fechaFin, identificacion, tipo_novedad, remunerado, usuario.getBd()));
        //JsonArray asJsonArray = (JsonArray) new JsonParser().parse();
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Listado_Vacaciones_Empleados", titulo = "LISTADO VACACIONES";

        cabecera = new String[]{"NUMERO_SOLICITUD", "TIPO NOVEDAD", "FECHA", "IDENTIFICACION", "NOMBRE", "FECHA INICIAL", "DIAS DISFRUTE", "DIAS COMPENSADOS", "FECHA FINAL", "DESCRIPCION", "JEFE DIRECTO", 
            "FECHA VISTO BUENO", "USUARIO VISTO BUENO", "OBSERVACIONES", "FECHA DE CREACION", "USUARIO CREACION","MACROPROCESO","PROCESO"};

        dimensiones = new short[]{
            5000, 5000, 5000, 7000, 9000, 5000, 5000, 5000,5000, 9000, 7000, 5000, 5000, 9000, 5000, 5000
        };

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        this.printlnResponseAjax(reponseJson, "text/plain");
    }
    
    private void exportarExcelPermisos(String bd) throws Exception{
        String fechaInicio = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";  
        String fechaFin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
        String tipo_novedad = request.getParameter("tipo_novedad1") != null ? request.getParameter("tipo_novedad1") : "";
        String identificacion = request.getParameter("identificacion2") != null ? request.getParameter("identificacion2") : "";
        String remunerado = request.getParameter("remunerada1") != null ? request.getParameter("remunerada1") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(dao.listarPermisos(fechaInicio, fechaFin, identificacion, tipo_novedad, remunerado, usuario.getBd()));
        //JsonArray asJsonArray = (JsonArray) new JsonParser().parse();
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Listado_Permisos_Empleados", titulo = "LISTADO PERMISOS";

        cabecera = new String[]{"NUMERO_SOLICITUD", "TIPO NOVEDAD", "RAZON","FECHA", "IDENTIFICACION", "NOMBRE", "FECHA INICIAL", "DURACION", "FECHA FINAL", "HORA INICIO","HORAS","HORA FINAL",
            "DESCRIPCION", "JEFE DIRECTO", 
            "FECHA VISTO BUENO", "USUARIO VISTO BUENO", "OBSERVACIONES", "FECHA DE CREACION", "USUARIO CREACION","MACROPROCESO","PROCESO"};

        dimensiones = new short[]{
            5000, 5000, 5000, 5000, 7000, 9000, 5000, 5000, 5000, 5000, 5000, 5000,9000, 7000, 5000, 5000, 9000, 5000, 5000
        };

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        this.printlnResponseAjax(reponseJson, "text/plain");
    }

    private void exportarExcelLicencias(String bd) throws Exception{
        String fechaInicio = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";  
        String fechaFin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
        String tipo_novedad = request.getParameter("tipo_novedad1") != null ? request.getParameter("tipo_novedad1") : "";
        String identificacion = request.getParameter("identificacion2") != null ? request.getParameter("identificacion2") : "";
        String remunerado = request.getParameter("remunerada1") != null ? request.getParameter("remunerada1") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(dao.listarLicencias(fechaInicio, fechaFin, identificacion, tipo_novedad, remunerado, usuario.getBd()));
        //JsonArray asJsonArray = (JsonArray) new JsonParser().parse();
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Listado_Licencias_Empleados", titulo = "LISTADO LICENCIAS";

        cabecera = new String[]{"NUMERO_SOLICITUD", "TIPO NOVEDAD", "RAZON", "FECHA", "IDENTIFICACION", "NOMBRE", "FECHA INICIAL", "DURACION", "FECHA FINAL", 
            "DESCRIPCION", "JEFE DIRECTO", 
            "FECHA VISTO BUENO", "USUARIO VISTO BUENO", "OBSERVACIONES", "FECHA DE CREACION", "USUARIO CREACION","VALOR A PAGAR","MACROPROCESO","PROCESO"};

        dimensiones = new short[]{
            5000, 5000, 5000, 5000, 7000, 9000, 5000, 5000, 5000, 9000, 7000, 5000, 5000, 9000, 5000, 5000,5000
        };

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        this.printlnResponseAjax(reponseJson, "text/plain");
    }
    
    private void exportarExcelOtrasLicencias(String bd) throws Exception{
        String fechaInicio = request.getParameter("fecha_inicio") != null ? request.getParameter("fecha_inicio") : "";  
        String fechaFin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
        String tipo_novedad = request.getParameter("tipo_novedad1") != null ? request.getParameter("tipo_novedad1") : "";
        String identificacion = request.getParameter("identificacion2") != null ? request.getParameter("identificacion2") : "";
        String remunerado = request.getParameter("remunerada1") != null ? request.getParameter("remunerada1") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(dao.listarOtrasLicencias(fechaInicio, fechaFin, identificacion, tipo_novedad, remunerado, usuario.getBd()));
        //JsonArray asJsonArray = (JsonArray) new JsonParser().parse();
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Listado_Licencias_Empleados", titulo = "LISTADO LICENCIAS";

        cabecera = new String[]{"NUMERO_SOLICITUD", "TIPO NOVEDAD", "RAZON", "FECHA", "IDENTIFICACION", "NOMBRE", "FECHA INICIAL", "DURACION", "FECHA FINAL", 
            "DESCRIPCION", "JEFE DIRECTO", 
            "FECHA VISTO BUENO", "USUARIO VISTO BUENO", "OBSERVACIONES", "FECHA DE CREACION", "USUARIO CREACION","VALOR A PAGAR","MACROPROCESO","PROCESO"};

        dimensiones = new short[]{
            5000, 5000, 5000, 5000, 5000, 7000, 9000, 5000, 5000, 5000, 9000, 7000, 5000, 5000, 9000, 5000, 5000,5000
        };

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        this.printlnResponseAjax(reponseJson, "text/plain");
    }
//    private static String loadCargo(java.lang.String bd) {
//        com.fintra.apt.Wsapoteosys_Service service = new com.fintra.apt.Wsapoteosys_Service();
//        com.fintra.apt.Wsapoteosys port = service.getWsapoteosysPort();
//        return port.loadCargo(bd);
//    }
    
    
//    private static String loadTerceroEmp(java.lang.String bd) {
//        com.fintra.apt.Wsapoteosys_Service service = new com.fintra.apt.Wsapoteosys_Service();
//        com.fintra.apt.Wsapoteosys port = service.getWsapoteosysPort();
//        return port.loadTerceroEmp(bd);
//    }

    private void generarArchivoDistribucion() throws Exception {
        JsonObject json = new JsonObject();
        
        try {
            if (ServletFileUpload.isMultipartContent(request)) {

                ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
                List fileItemsList = servletFileUpload.parseRequest(request);

                Iterator it = fileItemsList.iterator();

                if (it.hasNext()) {

                    FileItem fileItem = (FileItem) it.next();

                    if (!fileItem.isFormField()) {
                        boolean periodos = Boolean.parseBoolean(request.getParameter("periodos"));
                        if (dao.leerArchivoNomina(fileItem.getInputStream(), usuario.getLogin(), usuario.getDstrct()) && dao.generarArchivoDistribucion(periodos, request.getParameter("periodo"), usuario.getLogin())) {
                            json.addProperty("respuesta", "Se gener� el archivo de distribuci�n �xitosamente. Por favor revise su carpeta de archivos.");
                        } else {
                            json.addProperty("erro", "No se pudo generar el archivo. Comun�quese con soporte.");
                        }
                    }
                } else {
                    json.addProperty("error", "Debe cargar un archivo.");
                }
            } else {
                json.addProperty("error", "Petici�n con formato incorrecto.");
            }
        } catch (IOException e) {
            System.err.println(e.getCause());
            System.err.println("Error generando el archivo de distribucion: " + e.getMessage());
            json.addProperty("error", "Error leyendo/escribiendo el archivo.");
        } catch (SQLException e) {
            System.err.println(e.getCause());
            System.err.println("Error generando el archivo de distribucion en: " + e.getLocalizedMessage() + " -- " + e.getMessage());
            json.addProperty("error", "Error en las consultas a la base de datos.");
        }       
        this.printlnResponseAjax(json.toString(), "application/json");
    }
    
    private void editarNovedad() {
        try {
            JsonObject objeto = new JsonObject();
            String json = request.getParameter("informacion") != null ? request.getParameter("informacion") : "{}";
            JsonObject obj = (JsonObject) (new JsonParser()).parse(json);
            String resp = dao.editarNovedadesPermisos(obj, usuario);
            this.printlnResponseAjax(resp, "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void anularNovedad() {
        try {
            JsonObject objeto = new JsonObject();
            String json = request.getParameter("informacion") != null ? request.getParameter("informacion") : "{}";
            JsonObject obj = (JsonObject) (new JsonParser()).parse(json);
            String resp = dao.anularNovedadesPermisos(obj, usuario);
            this.printlnResponseAjax(resp, "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}

