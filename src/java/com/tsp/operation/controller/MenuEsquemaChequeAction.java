
package com.tsp.operation.controller;

import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import java.io.*;
import java.util.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;

public class MenuEsquemaChequeAction extends Action{
    
    
    public void run() throws ServletException, InformationException {
        try{
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String  login   = usuario.getLogin();
            if(!login.equals("") ){
                
                //--- variables de bloqueos
                
                String activo        = "false";
                String desactivo     = "true";
                
                String bloqueoInsert = activo;
                String bloqueoSearch = activo;
                String bloqueoUpdate = desactivo;
                String bloqueoDelete = desactivo;
                
                model.EsquemaChequeSvc.reset();
                model.EsquemaChequeSvc.searchBancos();
                
                
                // LLamamos la pagina JSP
                final String next = "/cheques/EsquemaCheque.jsp?estadoInsert="+ bloqueoInsert +"&estadoSearch="+bloqueoSearch+"&estadoDelete="+ bloqueoDelete+"&estadoUpdate="+bloqueoUpdate ;
                RequestDispatcher rd = application.getRequestDispatcher(next);
                if(rd == null)
                    throw new Exception("No se pudo encontrar "+ next);
                rd.forward(request, response);
                
            }
        }
        catch(Exception e){ throw new InformationException(e.getMessage());}
        
        
    }
    
}
