/*
 * AnticiposUpdateAction.java
 *
 * Created on 20 de abril de 2005, 04:58 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  kreales
 */
public class AnticiposUpdateAction extends Action{
    
    /** Creates a new instance of AnticiposUpdateAction */
    public AnticiposUpdateAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String next ="/anticipos/anticipoUpdate.jsp";
        String codant       = request.getParameter("codant").toUpperCase();
        String desant       = request.getParameter("descant");
        String dist         = request.getParameter("distrito");
        String sj           = request.getParameter("sj");
        String tipo_des     = request.getParameter("tipo_des");
        float valor         = Float.parseFloat(request.getParameter("valor"));
        String moneda       = request.getParameter("moneda");
        String indicador    = request.getParameter("indicador");
        String migracion    = request.getParameter("migracion");
        HttpSession session = request.getSession();
        Usuario usuario     = (Usuario) session.getAttribute("Usuario");
        
        Anticipos ant = new Anticipos();
        ant.setAnticipo_code(codant);
        ant.setAnticipo_desc(desant);
        ant.setDstrct(dist);
        ant.setSj(sj);
        ant.setTipo_s(tipo_des);
        ant.setCreation_user(usuario.getLogin());
        ant.setCodmigra(migracion);
        ant.setValor(valor);
        ant.setMoneda(moneda);
        ant.setIndicador(indicador);
        
        try{
            model.anticiposService.searchAnticipos(dist, codant,sj);
            if(model.anticiposService.getAnticipo()!=null){
                model.anticiposService.setAnticipos(ant);
                model.anticiposService.updateAnticipo();
                model.anticiposService.vecAnticipos(usuario.getBase());
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
}
