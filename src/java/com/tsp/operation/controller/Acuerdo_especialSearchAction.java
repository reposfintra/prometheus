/*******************************************************************
 * Nombre clase: Acuerdo_especialSearchAction.java
 * Descripci�n: Accion para buscar un acuerdo especial a la bd.
 * Autor: Ing. Jose de la rosa
 * Fecha: 6 de diciembre de 2005, 03:01 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

public class Acuerdo_especialSearchAction extends Action{
    
    /** Creates a new instance of Acuerdo_especialSearchAction */
    public Acuerdo_especialSearchAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next="";
        String standar = "";
        String codigo = "";
        String tipo = "";//tipo acuerdo
        String tip= request.getParameter("tipo");
        HttpSession session = request.getSession ();
        String distrito = (String)(session.getAttribute ("Distrito"));
        String listar = (String) request.getParameter ("listar");
        
        try{
            if (listar.equals ("True")){
                next="/jsp/equipos/acuerdo_especial/acuerdo_especialListar.jsp";
                String sw = (String) request.getParameter ("sw");
                if(sw.equals ("True")){
                    tipo = request.getParameter("c_tipo");
                    standar = (String) request.getParameter ("c_standar").toUpperCase ();
                    codigo = (String) request.getParameter ("c_codigo").toUpperCase ();
                }
                else{
                    tipo = "";
                    standar = "";
                    codigo = "";
                }
                session.setAttribute ("standar", standar);
                session.setAttribute ("codigo", codigo);
                session.setAttribute ("tipo", tipo);
                session.setAttribute ("tip",tip );
            }
            else{
                codigo = (String) request.getParameter ("c_codigo").toUpperCase ();
                standar = (String) request.getParameter ("c_standar").toUpperCase ();
                model.acuerdo_especialService.searchAcuerdo_especial(standar, distrito, codigo,tip);
                Acuerdo_especial a = model.acuerdo_especialService.getAcuerdo_especial();
                model.concepto_equipoService.searchConcepto_equipo(a.getCodigo_concepto());
                next="/jsp/equipos/acuerdo_especial/acuerdo_especialModificar.jsp";
            }
        }catch (SQLException e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);        
    }
    
}
