/*
 * PropietarioInsertAction.java
 *
 * Created on 14 de junio de 2005, 10:42 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

/**
 *
 * @author  Sandrameg
 */
public class PropietarioInsertAction extends Action {
    
    /** Creates a new instance of PropietarioInsertAction */
    public PropietarioInsertAction() {
    }
    
    public void run() throws ServletException, InformationException {
        System.out.println("llego");
        String next="/propietarios/propietarioInsert.jsp?msg=Propietario Registrado";
        
        String dstrct = request.getParameter("dstrct").toUpperCase();
        String cedula = request.getParameter("ced").toUpperCase();
        String pnombre = request.getParameter("pnombre").toUpperCase();
        String snombre = request.getParameter("snombre").toUpperCase();
        String papellido = request.getParameter("papellido").toUpperCase();
        String sapellido = request.getParameter("sapellido").toUpperCase();
        
        System.out.println("paso get's");
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
                
        try{
            
            System.out.println(" los datos  -"+dstrct+ " -  "+ 
        cedula+ " -  "+
        pnombre+ " -  "+
        snombre+ " -  "+
        papellido+ " -  "+
        sapellido+ " -  "+
        usuario.getLogin()+ " -  "+
        usuario.getLogin()+ " -  "+usuario.getBase());
            
            Propietario prop = new Propietario();
            prop.setCedula(cedula);
            prop.setP_nombre(pnombre);
            prop.setS_nombre(snombre);
            prop.setP_apellido(papellido);
            prop.setS_apellido(sapellido);
            prop.setdstrct(dstrct);
            prop.setCreation_user(usuario.getLogin());
            prop.setUser_update(usuario.getLogin());
            prop.setBase(usuario.getBase());
            
            model.propService.buscarxCedulaxDistrito(cedula,dstrct);
            
            if(model.propService.getPropietario() == null){
                model.propService.insert(prop);
            }
            else{
                String error = "Ya existe el propietario con cedula " + cedula;
                next="/propietarios/propietarioInsertError.jsp?msg=" + error ;
            }
            
        }catch (SQLException e){
            e.printStackTrace();
            throw new ServletException("PropietarioInsertAction  run"+e.getMessage());
        }
        this.dispatchRequest(next);
    }
}
