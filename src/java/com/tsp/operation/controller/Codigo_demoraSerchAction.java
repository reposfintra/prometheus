/*
 * Codigo_demoraSerchAction.java
 *
 * Created on 26 de junio de 2005, 03:43 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
/**
 *
 * @author  Jose
 */
public class Codigo_demoraSerchAction extends Action{
    
    /** Creates a new instance of Codigo_demoraSerchAction */
    public Codigo_demoraSerchAction() {
    }
    
    public void run() throws ServletException, InformationException{
        HttpSession session = request.getSession();       
        String listar = (String) request.getParameter("listar");
        String next="";
        String base = "";
        String codigo = "";
        String descripcion = "";
        try{                
            if (listar.equals("True")){     
                next="/jsp/trafico/demora/Codigo_demoraListar.jsp";                    
                next = Util.LLamarVentana(next, "Listar Codigo Demora");
                String sw = (String) request.getParameter("sw");
                if(sw.equals("True")){
                    base = request.getParameter("c_base").toUpperCase();
                    codigo = (String) request.getParameter("c_codigo").toUpperCase();
                    descripcion = (String) request.getParameter("c_descripcion");
                }
                else{
                    base = "";
                    codigo = "";
                    descripcion = "";
                }
                session.setAttribute("codigo", codigo);
                session.setAttribute("descripcion", descripcion);
                session.setAttribute("base", base);
            }
            else{
                codigo = (String) request.getParameter("c_codigo").toUpperCase();
                model.codigo_demoraService.serchCodigo_demora(codigo);
                Codigo_demora codigo_demora = model.codigo_demoraService.getCodigo_demora();
                next="/jsp/trafico/demora/Codigo_demoraModificar.jsp";
                next = Util.LLamarVentana(next, "Modificar Codigo Demora");

            }            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
