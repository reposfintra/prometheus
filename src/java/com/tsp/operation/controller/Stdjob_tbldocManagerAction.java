/***************************************************************************
 * Nombre clase : ............... Stdjob_tbldocAction.java                 *
 * Descripcion :................. Clase que maneja los eventos             *
 *                                relacionados con el programa de          *
 *                                Standart job tipo de documentos          *
 * Autor :....................... Ing. Juan Manuel Escandon Perez          *
 * Fecha :........................ 28 de octubre de 2005, 10:42 AM         *
 * Version :...................... 1.0                                     *
 * Copyright :.................... Fintravalores S.A.                 *
 ***************************************************************************/


package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;

public class Stdjob_tbldocManagerAction extends Action {
        
        /*
         *Declaracion de variables globales
         */
        private static String doc;
        private static String st;
        /** Creates a new instance of Stdjob_tbldocAction */
        public Stdjob_tbldocManagerAction() {
        }
        
        public void run() throws ServletException, com.tsp.exceptions.InformationException {
                try{
                        
                        /*
                         *Declaracion de variables
                         */
                        
                        String Opcion                 = (request.getParameter("Opcion")!=null) ? request.getParameter("Opcion") : "";
                        String document_type          = (request.getParameter("document_type")!=null) ? request.getParameter("document_type") : "";
                        String stdjob                 = (request.getParameter("standard")!=null) ? request.getParameter("standard") : "";
                        String original               = (request.getParameter("original")!=null) ? request.getParameter("original") : "";
                        String original2              = (request.getParameter("original2")!=null) ? request.getParameter("original2") : "";
                        
                        HttpSession Session           = request.getSession();
                        Usuario user                  = new Usuario();
                        user                          = (Usuario)Session.getAttribute("Usuario");
                        String usuario                = user.getLogin();
                        String dstrct                 = user.getDstrct();
                        
                        String []LOV                  = request.getParameterValues("LOV");
                        String Mensaje                = "";
                        
                        String next = "";
                        
                        /*
                         *Manejo de eventos
                         */
                        
                        if (Opcion.equals("Guardar")){
                                if(!model.stdjob_tbldocService.Exist(document_type, stdjob))
                                        model.stdjob_tbldocService.Insert(dstrct, document_type, stdjob, usuario);
                                Mensaje = "El Registro ha sido agregado";
                        }
                        else
                                Mensaje = "El Registro ya existe";
                        
                        if (Opcion.equals("Modificar")){
                                if(model.stdjob_tbldocService.Exist(original.split(",")[0],original.split(",")[1])){
                                        model.stdjob_tbldocService.Update(original.split(",")[0], usuario, original.split(",")[1]);
                                        Mensaje = "El Registro ha sido modificado";
                                }
                        }
                        
                        if (Opcion.equals("Nuevo"))
                                model.stdjob_tbldocService.ReiniciarDato();
                        
                        if (Opcion.equals("Ocultar Lista")){
                                model.stdjob_tbldocService.ReiniciarLista();
                        }
                        
                        
                        if (Opcion.equals("Anular")){
                                for(int i=0;i<LOV.length;i++){
                                        this.Conversion(LOV[i]);
                                        model.stdjob_tbldocService.UpdateStatus("A", usuario, doc, st );
                                }
                                Mensaje = "El Registro ha sido anulado";
                        }
                        
                        
                        if (Opcion.equals("Activar")){
                                for(int i=0;i<LOV.length;i++){
                                        this.Conversion(LOV[i]);
                                        model.stdjob_tbldocService.UpdateStatus("", usuario, doc, st );
                                }
                                Mensaje = "El Registro ha sido activado ";
                        }
                        if (Opcion.equals("Eliminar")){
                                for(int i=0;i<LOV.length;i++){
                                        this.Conversion(LOV[i]);
                                        model.stdjob_tbldocService.Delete(doc, st );
                                }
                                Mensaje = "El Registro ha sido eliminado ";
                        }
                        
                        
                        if(Opcion.equals("Seleccionar")){
                                this.Conversion(original2);
                                model.stdjob_tbldocService.Search(doc, stdjob);
                        }
                        
                        if (Opcion.equals("Listado")){
                                model.stdjob_tbldocService.List();
                        }
                        
                        
                        next ="/jsp/cxpagar/Stdjob_tbldoc/Stdjob_tbldoc.jsp?Mensaje="+Mensaje;
                        
                        if ("Listado|Modificar|Eliminar|Anular|Activar".indexOf(Opcion) != -1) {
                                model.stdjob_tbldocService.List();
                                next ="/jsp/cxpagar/Stdjob_tbldoc/Listado.jsp?Mensaje="+Mensaje;
                        }
                        
                        
                        RequestDispatcher rd = application.getRequestDispatcher(next);
                        if(rd == null)
                                throw new ServletException("No se pudo encontrar "+ next);
                        rd.forward(request, response);
                        
                }catch(Exception e){
                        e.printStackTrace();
                        throw new ServletException("Error en Stdjob_tbldocAction .....\n"+e.getMessage());
                }
        }
        
        /**
         * Metodo Conversion, recibe un String, le realiza un split, y inicializa
         * dos variables privadas de tipo global
         * @autor : Ing. Juan Manuel Escandon Perez
         * @param : String
         * @version : 1.0
         */
        public static void Conversion(String f){
                String vF [] = f.split("/");
                doc = vF[0];
                st = vF[1];
        }
        
}
