/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.impl.FiltroLibranzaImpl;
import com.tsp.operation.model.beans.Usuario;
import javax.servlet.ServletException;
import com.tsp.operation.model.DAOS.FiltroLibranzaDAO;

/**
 *
 * @author root
 */
public class FormularioFiltroLibranzaAction extends Action {

    private final int BUSCAR_PERSONA = 1;
    private final int INFO_CONVENIO = 2;
    private final int GUARDAR_FILTRO = 3;
    private final int GET_CONVENIOS = 4;
    private final int GET_OCUPACIONES = 5;
    private final int GET_DESC_LEY = 6;
    private final int GET_EXTRAPRIMA = 7;
    private final int GET_ESTADO_CIVIL = 8;
    private final int GET_FILTRO_X_ID = 9;
    private final int FORMALIZAR_LIBRANZA = 10;
    private final int GET_IDFILTRO_X_CEDULA = 12;
    private final int GET_EMPRESAS_PAGADURIA = 13;
    private final int GET_DOCUMENTOS_REQUERIDOS = 14;

    private JsonObject respuesta = null;
    private Usuario usuario;
    private FiltroLibranzaDAO fcDAO;

    @Override
    public void run() throws ServletException, InformationException {
        try {
            usuario = (Usuario) request.getSession().getAttribute("Usuario");
            fcDAO = new FiltroLibranzaImpl(usuario.getBd());

            int opcion = (request.getParameter("opcion") != null)
                    ? Integer.parseInt(request.getParameter("opcion"))
                    : -1;
            switch (opcion) {
                case BUSCAR_PERSONA:
                    String nit = request.getParameter("identificacion") != null ? request.getParameter("identificacion") : "";
                    respuesta = fcDAO.buscarPersona(nit);
                    break;
                case INFO_CONVENIO:
                    String cnl = request.getParameter("convenio") != null ? request.getParameter("convenio") : "";
                    respuesta = fcDAO.infoConvenio(cnl);
                    break;
                case GUARDAR_FILTRO:
                    respuesta = new JsonObject();
                    JsonObject jo = request.getParameter("form") != null 
                                  ? (JsonObject) (new JsonParser()).parse(request.getParameter("form"))
                                  : null;
                    jo.addProperty("usuario", usuario.getLogin());
                    String[] myJsonData = request.getParameterValues("listadodocs[]");                    
                    respuesta.add("solicitud", fcDAO.GuardarFiltro(jo,myJsonData,usuario));
                    //Consulta DataCredito
                    respuesta.add("hdc", fcDAO.consultarHDC(jo, usuario.getToken_api()));
                    break;
                case GET_OCUPACIONES:
                    respuesta = fcDAO.getOcupLaboral();
                    break;
                case GET_ESTADO_CIVIL:
                    respuesta = fcDAO.getEstadoCivil();
                    break;
                case GET_CONVENIOS:
                    String ol = request.getParameter("ocuLab") != null ? request.getParameter("ocuLab") : "";
                    respuesta = fcDAO.getConvenios(ol);
                    break;
                case GET_DESC_LEY:
                    String ocup = request.getParameter("ocupacion") != null ? request.getParameter("ocupacion") : "";
                    double salario = request.getParameter("salario") != null ? Double.parseDouble(request.getParameter("salario")) : 0;
                    respuesta = fcDAO.getDescuentosLey(ocup, salario);
                    break;
                case GET_EXTRAPRIMA:
                    String ocupacion = request.getParameter("ocupacion") != null ? request.getParameter("ocupacion") : "";
                    int edad = request.getParameter("edad") != null ? Integer.parseInt(request.getParameter("edad")) : 0;
                    respuesta = new JsonObject();
                    respuesta.addProperty("extraprima", fcDAO.getPorcExtraprima(ocupacion, edad));
                    break;
                case GET_FILTRO_X_ID:
                    String idFiltro = request.getParameter("id_filtro") != null ? request.getParameter("id_filtro") : "";
                    respuesta = fcDAO.getFiltroLibranza(idFiltro);
                    break;
                case FORMALIZAR_LIBRANZA:
                    String resp = fcDAO.formalizarLibranza();
                    this.printlnResponseAjax(resp, "application/json;");
                    break;
                case GET_IDFILTRO_X_CEDULA:
                    String identificacion = request.getParameter("identificacion") != null ? request.getParameter("identificacion") : "";
                    respuesta = fcDAO.getIdFiltroLibranza(identificacion);                   
                    break;
                case GET_EMPRESAS_PAGADURIA:
                    String id_conf_libranza = request.getParameter("id_conf_libranza") != null ? request.getParameter("id_conf_libranza") : "";
                    respuesta = fcDAO.getEmpresasPagaduria(id_conf_libranza);
                    break;
                case GET_DOCUMENTOS_REQUERIDOS:
                    String idFiltroLibranza = request.getParameter("id_filtro_libranza") != null ? request.getParameter("id_filtro_libranza") : "";
                    respuesta = fcDAO.getDocumentosRequeridos(idFiltroLibranza);
                    break;
                default:
                    throw new Exception("La opcion de orden " + opcion + " no valida");
            }
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
            exc.printStackTrace();
        } finally {
            try {
                response.setContentType("application/json; charset=utf-8");
                response.setHeader("Cache-Control", "no-cache");
                response.getWriter().println((new Gson()).toJson(respuesta));
            } catch (Exception e) {
            }
        }
    }
    
}
