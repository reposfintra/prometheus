/*
 * TBLActividadInsertAction.java
 *
 * Created on 13 de octubre de 2005, 03:19 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
/**
 *
 * @author  Leonardo Parody
 */
public class TBLActividadInsertAction extends Action{
    
    /** Creates a new instance of TBLActividadInsertAction */
    public TBLActividadInsertAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException{
        ////System.out.println("ACTION");
        String next="";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        String user = usuario.getLogin();
        String distrito = (String) session.getAttribute("Distrito");
        String flag = request.getParameter("flag");
        String titulo="";
        try{
                if (flag.equals("mostrar")){
                    
                    next = "/jsp/masivo/actividades/TblactividadesModificar.jsp?cod="+request.getParameter("cod")+"&dis="+distrito;
                    titulo = "Tipo de Actividad";
                    next=Util.LLamarVentana(next, titulo);
                    
                }else if (flag.equals("M")){  
                    
                    TBLActividades actividades = new TBLActividades();
                    actividades.setCodigo(request.getParameter("c_codigo"));
                    actividades.setDescripcion(request.getParameter("c_descripcion"));
                    actividades.setSigla(request.getParameter("c_sigla"));
                    actividades.setCreation_user(user);
                    actividades.setDstrct_code(distrito);   
                    actividades.setUser_update(user);
                    model.tLBActividadesServices.InsertarActividad(actividades);
                    next = "/jsp/masivo/actividades/Mensaje.jsp?msg=Actividad Modificada&reload=ok";
                    next=Util.LLamarVentana(next, "Tipo de Actividad");
                    
                } else if(flag.equals("N")){
                    
                    TBLActividades actividades = new TBLActividades();
                    actividades.setCodigo(request.getParameter("c_codigo"));
                    actividades.setDescripcion(request.getParameter("c_descripcion"));
                    actividades.setSigla(request.getParameter("c_sigla"));
                    actividades.setCreation_user(user);
                    actividades.setDstrct_code(distrito);   
                    actividades.setUser_update(user);
                    boolean accion = model.tLBActividadesServices.ExisteTBLActividad(actividades.getCodigo(), actividades.getDstrct_code());
                    if (accion == true){
                        if (model.tLBActividadesServices.ObtenerTBLActividad(actividades.getCodigo(), actividades.getDstrct_code()).getReg_status().equals("A")){
                            ////System.out.println("Entre al si existe y esta anulado-------------------------");
                            model.tLBActividadesServices.InsertarActividad(actividades);
                            next = "/jsp/masivo/actividades/TblactividadesIngresar.jsp?msg= Actividad Reactivada y modificada";
                        }else{
                            next = "/jsp/masivo/actividades/TblactividadesIngresar.jsp?msg=Ya Existe este C�digo de Actividad";
                        }
                    }else{
                         model.tLBActividadesServices.InsertarActividad(actividades);
                         next = "/jsp/masivo/actividades/TblactividadesIngresar.jsp?msg=Nueva Actividad Ingresada";
                         
                    }
                
                }else if (flag.equals("A")){
                
                    TBLActividades actividades = new TBLActividades();
                    actividades.setCodigo(request.getParameter("c_codigo"));
                    actividades.setDescripcion(request.getParameter("c_descripcion"));
                    actividades.setSigla(request.getParameter("c_sigla"));
                    actividades.setCreation_user(user);
                    actividades.setDstrct_code(distrito);   
                    actividades.setUser_update(user);
                    model.tLBActividadesServices.AnularActividad(actividades);
                    next = "/jsp/masivo/actividades/Mensaje.jsp?msg=Actividad Anulada&reload=sw";
                    
                }
            
        }catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    
   
    }
}
