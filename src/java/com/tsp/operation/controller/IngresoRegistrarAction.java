/*************************************************************************
 * Nombre:        IngresoRegistrarAction.java
 * Descripci�n:   Clase Action para registrar el ingreso de pagos de clientes
 *                y las suscursales del banco
 * Autor:         Ing. Diogenes Antonio Bastidas Morales
 * Fecha:         11 de mayo de 2006, 11:53 AM
 * Versi�n        1.0
 * Coyright:      Transportes Sanchez Polo S.A.
 *************************************************************************/



package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.pdf.IngresoPDF;
import com.tsp.pdf.NotaCreditoPDF;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import com.tsp.finanzas.contab.model.*;
/**
 *
 * @author  dbastidas
 */
public class IngresoRegistrarAction extends Action {
    
    /** Creates a new instance of IngresoRegistrarAction */
    public IngresoRegistrarAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        com.tsp.finanzas.contab.model.Model modelcontab = (com.tsp.finanzas.contab.model.Model) session.getAttribute("modelcontab");
        String opcion = ( request.getParameter("opcion") != null )?request.getParameter("opcion"):"";
        double tasaDB = Double.parseDouble( ( request.getParameter("tasa") != null )? !request.getParameter("tasa").equals("")? request.getParameter("tasa").replaceAll(",",""):"0":"0" );
        String abc = ( request.getParameter("abc") != null )?request.getParameter("abc").toUpperCase():"";
        String next   = "";
        String mon_local = "";
        String mon_ingreso = "";
        double valor_con = 0;
        String fecha_actual = Util.getFechaActual_String(4);
        String auxiliar = "";
        
        if( !opcion.equals("imp" ) && !opcion.equals("impitems")){
            next = "/" + request.getParameter("carpeta")+ "/" + request.getParameter("pagina");
            mon_local = "";
            mon_ingreso = request.getParameter("moneda");
            valor_con = Double.parseDouble(request.getParameter("valor"));
        }
        
        String num_ingreso = "";
        boolean sw = true;
        try{
            
            if( opcion.equals("") ){
                String sucursal = request.getParameter("sucursal")!=null?request.getParameter("sucursal"):"";
                String vec[] = sucursal.split("/");
                if( vec != null && vec.length > 1 )
                    sucursal = vec[0];
                Ingreso ing = new Ingreso();
                ing.setDstrct(usuario.getDstrct());
                ing.setReg_status("");
                ing.setCodcli(request.getParameter("cliente").toUpperCase());
                ing.setNitcli(request.getParameter("nit"));
                ing.setConcepto(request.getParameter("concepto"));
                ing.setTipo_ingreso("C");
                ing.setFecha_consignacion(request.getParameter("fecha")!=null?request.getParameter("fecha"):fecha_actual);
                ing.setFecha_ingreso(fecha_actual);
                ing.setBranch_code(request.getParameter("banco"));
                ing.setBank_account_no(sucursal);
                ing.setCodmoneda(mon_ingreso);
                ing.setAgencia_ingreso(usuario.getId_agencia());
                ing.setPeriodo("000000");
                ing.setCant_item(0);
                ing.setAbc(abc);
                ing.setBase(usuario.getBase());
                ing.setTransaccion(0);
                ing.setTransaccion_anulacion(0);
                ing.setTipo_documento(request.getParameter("tipodoc"));
                ing.setCreation_user(usuario.getLogin());
                ing.setCreation_date( Util.fechaActualTIMESTAMP());
                ing.setLast_update(Util.fechaActualTIMESTAMP());
                ing.setUser_update(usuario.getLogin());
                ing.setDescripcion_ingreso(request.getParameter("descripcion"));
                ing.setNro_consignacion("");
                ing.setCuenta(request.getParameter( "cuenta1" ).toUpperCase() );
                ing.setTasaDolBol(tasaDB);
                ing.setCmc(request.getParameter("ForHC"));
                ing.setNro_extracto(Integer.parseInt((String) (request.getParameter("nro_extracto") != null ? (!request.getParameter("nro_extracto").equals("") ? request.getParameter("nro_extracto") : "0") : "0")));
                LinkedList t_aux = new LinkedList();
                try{
                    modelcontab.subledgerService.buscarCuentasTipoSubledger( ing.getCuenta() );
                    t_aux = modelcontab.subledgerService.getCuentastsubledger();
                }catch(Exception e){
                    t_aux = null;
                }
                ing.setTipos( t_aux );
                String aux = request.getParameter( "auxiliar1" )!=null?request.getParameter( "auxiliar1" ):"";
                String tipo = request.getParameter( "tipo1" ) != null?request.getParameter( "tipo1" ):"";
                auxiliar = ( aux.equals("") && tipo.equals("") )?"" :request.getParameter( "tipo1" )+"-"+request.getParameter( "auxiliar1" ) ;
                ing.setAuxiliar( auxiliar );
                
                mon_local = (String) session.getAttribute("Moneda");
                Tasa tasa = model.tasaService.buscarValorTasa( mon_local, mon_ingreso, mon_local, request.getParameter("fecha")!=null?request.getParameter("fecha"):fecha_actual );
                if(tasa!=null){
                    ing.setVlr_ingreso( (!mon_local.equals("DOL"))? (int)  Math.round( tasa.getValor_tasa() * valor_con ) : tasa.getValor_tasa() * valor_con  );
                    ing.setVlr_ingreso_me( valor_con );
                    ing.setVlr_saldo_ing( valor_con ); 
                    ing.setVlr_tasa( tasa.getValor_tasa() );
                    ing.setFecha_tasa( tasa.getFecha() );
                }
                else{
                    sw = false;
                }
                if(sw){
                    //valido que si se graba un ingreso no se permita volver a grabar si no se reestaura
                    if(!model.ingresoService.isTemporal()){
                        //valido si el es nota credito y el codigo abc es vacio permita grabar o si tiene codigo se valide con la tabla abc o si es ingreso
                        if( ( request.getParameter("tipodoc").equals("ICR") && abc.equals("") ) ||
                        ( request.getParameter("tipodoc").equals("ICR") && model.tblgensvc.existeRegistro("ABC", abc) ) ||
                        !request.getParameter("tipodoc").equals("ICR") ){
                            
                            //valido cuando es ingreso
                            if( request.getParameter("tipodoc").equals("ING") ){
                                Banco ban = model.servicioBanco.obtenerBanco( ing.getDstrct(), ing.getBranch_code(), ing.getBank_account_no());
                                //valido cuando es ingreso y la moneda del ingreso sea igual a la moneda del banco
                                if( ban.getMoneda().equals( ing.getCodmoneda() ) ){
                                    num_ingreso = model.ingresoService.buscarSerie( request.getParameter("tipodoc")+"C" );
                                    //valido que el ingreso no sea vacio
                                    if(!num_ingreso.equals("") ){
                                        ing.setNum_ingreso(num_ingreso);//buscar de la serie
                                        TransaccionService tService = new TransaccionService(usuario.getBd());
                                        
                                        tService.crearStatement();
                                        tService.getSt().addBatch( model.ingresoService.insertarIngreso(ing) );
                                        tService.execute();
                                        
                                        ing.setBank_account_no(request.getParameter("sucursal")!=null?request.getParameter("sucursal"):"");
                                        model.ingresoService.setIngreso(ing);
                                        model.ingresoService.setTemporal(true);
                                        model.ingresoService.setNro_ing(num_ingreso);
                                        next+="?msg="+model.ingresoService.getNro_ing()+"&msg2="+request.getParameter("nombre");
                                        session.setAttribute("msg",model.ingresoService.getNro_ing());
                                        session.setAttribute("msg2",request.getParameter("nombre"));
                                    }else{
                                        session.setAttribute("msg","serie");
                                        session.setAttribute("msg2","");
                                        
                                        this.dispatchRequest(next+"?msg=serie");
                                    }
                                }
                                else{
                                    request.setAttribute("mensaje","La moneda "+ ing.getCodmoneda()+" no pertenece al banco "+ing.getBranch_code()+" "+ing.getBank_account_no());
                                    model.ingresoService.setTemporal(false);
                                }
                            }
                            //valido cuando es nota credito
                            else{
                                num_ingreso = model.ingresoService.buscarSerie( request.getParameter("tipodoc")+"C" );
                                //valido que la nota credito no sea vacio
                                if(!num_ingreso.equals("") ){
                                    ing.setNum_ingreso(num_ingreso);//buscar de la serie
                                    TransaccionService tService = new TransaccionService(usuario.getBd());
                                    
                                    tService.crearStatement();
                                    tService.getSt().addBatch( model.ingresoService.insertarIngreso(ing) );
                                    tService.execute();
                                    
                                    ing.setBank_account_no(request.getParameter("sucursal")!=null?request.getParameter("sucursal"):"");
                                    model.ingresoService.setIngreso(ing);
                                    model.ingresoService.setTemporal(true);
                                    model.ingresoService.setNro_ing(num_ingreso);
                                    next+="?msg="+model.ingresoService.getNro_ing()+"&msg2="+request.getParameter("nombre");
                                    session.setAttribute("msg",model.ingresoService.getNro_ing());
                                    session.setAttribute("msg2",request.getParameter("nombre"));
                                }else{
                                    session.setAttribute("msg","serie");
                                    session.setAttribute("msg2","");
                                    
                                    this.dispatchRequest(next+"?msg=serie");
                                }
                            }
                            
                        }
                        else{
                            request.setAttribute("mensaje","No existe el codigo ABC "+abc+" en la tabla");
                            model.ingresoService.setTemporal(false);
                        }
                    }
                }
                else{
                    session.setAttribute("msg","error");
                }
            }//Fin de validacion
            
            
            
            else if (opcion.equals("imp")){
                try{
                    
                    String numero_ingreso   = ( request.getParameter("num") != null )?request.getParameter("num"):"";
                    String tipo_docuento    = ( request.getParameter("tipodoc") != null )?request.getParameter("tipodoc"):"";
                    IngresoPDF ingreso = new IngresoPDF();
                    ingreso.RemisionPlantilla();
                    ingreso.crearCabecera();
                    ingreso.crearRemision(model, usuario.getDstrct(), numero_ingreso, tipo_docuento, "", usuario.getLogin() );
                    ingreso.generarPDF();
                    
                }catch(Exception ex){
                    ex.getMessage();
                    ex.printStackTrace();
                }
                
                next = "/pdf/IngresoPDF.pdf";
            }
            
            else if (opcion.equals("impitems")){
                try{
                    
                    String numero_ingreso   = ( request.getParameter("num") != null )?request.getParameter("num"):"";
                    String tipo_documento    = ( request.getParameter("tipodoc") != null )?request.getParameter("tipodoc"):"";
                    
                    if( tipo_documento.equals("ING") ){
                        IngresoPDF ingreso = new IngresoPDF();
                        ingreso.RemisionPlantilla();
                        ingreso.crearCabecera();
                        ingreso.crearRemision(model, usuario.getDstrct(), numero_ingreso, tipo_documento, "items", usuario.getLogin() );
                        ingreso.generarPDF();
                        next = "/pdf/IngresoPDF.pdf";
                    }
                    else{
                        NotaCreditoPDF nota = new NotaCreditoPDF();
                        nota.RemisionPlantilla();
                        nota.crearCabecera();
                        nota.crearRemision(model, usuario.getDstrct(), numero_ingreso, tipo_documento, "items", usuario.getLogin() );
                        nota.generarPDF();
                        next = "/pdf/NotaCreditoPDF.pdf";
                    }
                }catch(Exception ex){
                    ex.getMessage();
                    ex.printStackTrace();
                }
                
                
            }
            
            this.dispatchRequest(next);
        }catch (Exception e) {
            e.printStackTrace();
        }
        
    }
    
}
