/*
 * CodextrafleteAction.java
 *
 * Created on 10 de julio de 2005, 05:07 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

import org.apache.log4j.Logger;

/**
 *
 * @author  JBARRIOS
 */
public class CodextrafleteInsertAction extends Action {
    
    Logger logger = Logger.getLogger(this.getClass());
    
    /** Creates a new instance of CodextrafleteAction */
    public CodextrafleteInsertAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException {
        String opcion = request.getParameter("opcion");
        String codigo= request.getParameter("codigo");
        int op =  Integer.parseInt(opcion);
        String next = "/jsp/masivo/extraflete/codextrafleteInsert.jsp";
        Codextraflete codextra = new Codextraflete();
        codextra.setCodextraflete(codigo);
        String descripcion, cuenta, agencia, elemento, ajuste, unidades, tipo;
        HttpSession session = request.getSession();
        
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        com.tsp.finanzas.contab.model.Model modelcontab = (com.tsp.finanzas.contab.model.Model) session.getAttribute("modelcontab");

        switch(op){
            case 1: //Caso de Insercion...
                
                logger.info("... caso de inserci�n");
                
                descripcion = request.getParameter("descripcion");
                cuenta      = request.getParameter("cuenta");
                agencia     = request.getParameter("agencia");
                elemento    = request.getParameter("elemento");
                ajuste      = request.getParameter("ajuste");
                unidades    = request.getParameter("unidades");
                tipo        = request.getParameter("tipo");
                next = "/jsp/masivo/extraflete/codextrafleteInsert.jsp?mensaje=El registro ha sido insertado";
                codextra.setDescripcion(descripcion.toUpperCase());
                codextra.setCuenta(cuenta.toUpperCase());
                codextra.setAgencia(agencia.toUpperCase());
                codextra.setElemento(elemento.toUpperCase());
                codextra.setAjuste(ajuste);
                codextra.setUnidades(unidades);
                codextra.setTipo(tipo);
                codextra.setCreation_user(usuario.getLogin());
                codextra.setBase(usuario.getBase());
                
                //codextra.setCreation_user("JBARRIOS");
                //codextra.setBase("JMB");
                /*Ingreso en tblcon. Ver Constructor TblConceptos*/
                TblConceptos concepto = new TblConceptos();                
                concepto.setDstrct(session.getAttribute("Distrito").toString());
                concepto.setConcept_code(codextra.getCodextraflete());
                concepto.setConcept_desc(codextra.getDescripcion());
                concepto.setAccount(codextra.getElemento());                  
                concepto.setCreation_user(usuario.getLogin());
                concepto.setBase(usuario.getBase());
                try{
                    
                    //Cargar las agencias
                    List ListaAgencias = model.ciudadService.ListarAgencias();
                    TreeMap tm = new TreeMap();
                    if(ListaAgencias.size()>0) {
                        Iterator It3 = ListaAgencias.iterator();
                        while(It3.hasNext()) {
                            Ciudad  datos2 =  (Ciudad) It3.next();
                            tm.put("["+datos2.getCodCiu()+"] "+datos2.getNomCiu(), datos2.getCodCiu());
                            //out.print("<option value='"+datos2.getCodCiu()+"'>["+datos2.getCodCiu()+"] "+datos2.getNomCiu()+"</option> \n");
                        }
                    }
                    request.setAttribute("agencias_tm", tm);
                    
                    String regst = model.codextrafleteService.existCodExtraflete(codigo);
                    String regst0 = model.ConceptosSvc.existeConcepto( concepto.getDstrct(), concepto.getConcept_code() );
                    logger.info("? existe concepto: " + regst0);
                    
                    if( codextra.getCuenta().equals("") || modelcontab.planDeCuentasService.existCuenta( concepto.getDstrct(), codextra.getCuenta()) ){
                        
                        if( codextra.getElemento().equals("") || modelcontab.planDeCuentasService.existElementoDelGasto( codextra.getElemento() ) ){
                            
                            if( regst!=null ){
                                if( !regst.equals("") ){
                                    if( regst0!=null ){
                                        if( !regst0.equals("") ){
                                            model.ConceptosSvc.updateConcepto( concepto );
                                            model.codextrafleteService.updateCodextraflete(codextra);
                                        } else {
                                            request.setAttribute("codextra", codextra);
                                            next = "/jsp/masivo/extraflete/codextrafleteInsert.jsp?mensaje=EL c�digo  "+ codigo +" del  extra flete ya existe en la tabla conceptos.";
                                        }
                                    } else {
                                        model.ConceptosSvc.insertConcepto(concepto);
                                        model.codextrafleteService.insertCodextraflete(codextra);
                                    }
                                    //model.ConceptosSvc.insertConcepto(concepto);
                                    logger.info("... modificacion x duplicaci�n de llave. reg anulado.");
                                } else {
                                    request.setAttribute("codextra", codextra);
                                    next = "/jsp/masivo/extraflete/codextrafleteInsert.jsp?mensaje=Codigo  "+ codigo +"  extra flete ya existe";
                                }
                            } else {
                                if( regst0!=null ){
                                    if( !regst0.equals("") ){
                                        model.ConceptosSvc.updateConcepto( concepto );
                                        model.codextrafleteService.insertCodextraflete(codextra);
                                    } else {
                                        request.setAttribute("codextra", codextra);
                                        next = "/jsp/masivo/extraflete/codextrafleteInsert.jsp?mensaje=EL c�digo  "+ codigo +" del  extra flete ya existe en la tabla conceptos.";
                                    }
                                } else {
                                    model.ConceptosSvc.insertConcepto(concepto);
                                    model.codextrafleteService.insertCodextraflete(codextra);
                                }
                            }
                            
                        } else {
                            request.setAttribute("codextra", codextra);
                            next = "/jsp/masivo/extraflete/codextrafleteInsert.jsp?mensaje=EL elemento  "+ codextra.getElemento() +" no se encuentra registrado.";
                        }
                        
                    } else {
                        request.setAttribute("codextra", codextra);
                        next = "/jsp/masivo/extraflete/codextrafleteInsert.jsp?mensaje=La cuenta  "+ codextra.getCuenta() +" no se encuentra registrada.";
                    }
                    
                }catch (Exception e){
                    e.printStackTrace();
                    throw new ServletException(e.getMessage());
                    
                }
                
                break;
                
            case 2:   //Caso para Consultar..
                try{
                    
                    //Cargar las agencias
                    List ListaAgencias = model.ciudadService.ListarAgencias();
                    TreeMap tm = new TreeMap();
                    if(ListaAgencias.size()>0) {
                        Iterator It3 = ListaAgencias.iterator();
                        while(It3.hasNext()) {
                            Ciudad  datos2 =  (Ciudad) It3.next();
                            tm.put("["+datos2.getCodCiu()+"] "+datos2.getNomCiu(), datos2.getCodCiu());
                            //out.print("<option value='"+datos2.getCodCiu()+"'>["+datos2.getCodCiu()+"] "+datos2.getNomCiu()+"</option> \n");
                        }
                    }
                    request.setAttribute("agencias_tm", tm);
                    
                    model.codextrafleteService.consultarCodextraflete(codextra);
                    if(model.codextrafleteService.getconsultar()!=null){
                        next = "/jsp/masivo/extraflete/buscarCodextrafletes.jsp?retorno=ok";
                        request.setAttribute("codextra", model.codextrafleteService.getconsultar());
                    }
                    else
                        next = "/jsp/masivo/extraflete/buscarCodextrafletes.jsp?mensaje=Codigo  "+ codigo +"  no existe";
                }catch (Exception m){
                    m.printStackTrace();
                    throw new ServletException(m.getMessage());
                }
                
                break;
            case 3:   //Caso para Modificar..
                descripcion = request.getParameter("descripcion");
                cuenta      = request.getParameter("cuenta");
                agencia     = request.getParameter("agencia");
                elemento    = request.getParameter("elemento");
                ajuste      = request.getParameter("ajuste");
                unidades    = request.getParameter("unidades");
                tipo        = request.getParameter("tipo");
                //codextra.setCreation_user(usuario.getLogin());
                
                
                codextra.setDescripcion(descripcion.toUpperCase());
                codextra.setCuenta(cuenta);
                codextra.setAgencia(agencia);
                codextra.setElemento(elemento);
                codextra.setAjuste(ajuste);
                codextra.setUnidades(unidades);
                codextra.setTipo(tipo);
                codextra.setCreation_user(usuario.getLogin());
                try{
                    
                    //Cargar las agencias
                    List ListaAgencias = model.ciudadService.ListarAgencias();
                    TreeMap tm = new TreeMap();
                    if(ListaAgencias.size()>0) {
                        Iterator It3 = ListaAgencias.iterator();
                        while(It3.hasNext()) {
                            Ciudad  datos2 =  (Ciudad) It3.next();
                            tm.put("["+datos2.getCodCiu()+"] "+datos2.getNomCiu(), datos2.getCodCiu());
                            //out.print("<option value='"+datos2.getCodCiu()+"'>["+datos2.getCodCiu()+"] "+datos2.getNomCiu()+"</option> \n");
                        }
                    }
                    
                    request.setAttribute("agencias_tm", tm);
                    
                    if( codextra.getCuenta().equals("") || modelcontab.planDeCuentasService.existCuenta( session.getAttribute("Distrito").toString(), codextra.getCuenta()) ){
                        
                        if( codextra.getElemento().equals("") || modelcontab.planDeCuentasService.existElementoDelGasto( codextra.getElemento() ) ){
                            
                            model.codextrafleteService.updateCodextraflete(codextra);
                            //System.out.println("CodExtra: "+codextra.getCodextraflete());
                            model.ConceptosSvc.updateConcepto2(codextra.getDescripcion(), codextra.getElemento(), codextra.getCodextraflete());
                            model.codextrafleteService.consultarCodextraflete(codextra);
                            next = "/jsp/masivo/extraflete/buscarCodextrafletes.jsp?mensaje=Modificacion Exitosa...";
                        } else {
                            next = "/jsp/masivo/extraflete/buscarCodextrafletes.jsp?retorno=ok&mensaje=EL elemento  "+ codextra.getElemento() +" no se encuentra registrado.";
                        }
                    } else {
                        next = "/jsp/masivo/extraflete/buscarCodextrafletes.jsp?retorno=ok&mensaje=La cuenta  "+ codextra.getCuenta() +" no se encuentra registrada.";
                    }
                    
                    model.codextrafleteService.consultarCodextraflete(codextra);
                    request.setAttribute("codextra", model.codextrafleteService.getconsultar());
                }catch (Exception m){
                    m.printStackTrace();
                    throw new ServletException(m.getMessage());
                }
                
                break;
                
            case 4:   //Caso para Anulacion..
                //codextra.setCreation_user(usuario.getLogin());
                codextra.setCreation_user(usuario.getLogin());
                try{
                    
                    //Cargar las agencias
                    List ListaAgencias = model.ciudadService.ListarAgencias();
                    TreeMap tm = new TreeMap();
                    if(ListaAgencias.size()>0) {
                        Iterator It3 = ListaAgencias.iterator();
                        while(It3.hasNext()) {
                            Ciudad  datos2 =  (Ciudad) It3.next();
                            tm.put("["+datos2.getCodCiu()+"] "+datos2.getNomCiu(), datos2.getCodCiu());
                            //out.print("<option value='"+datos2.getCodCiu()+"'>["+datos2.getCodCiu()+"] "+datos2.getNomCiu()+"</option> \n");
                        }
                    }
                    
                    request.setAttribute("agencias_tm", tm);
                    
                    model.codextrafleteService.deleteCodextraflete(codextra);
                    model.ConceptosSvc.anular(session.getAttribute("Distrito").toString(),codigo);
                    next = "/jsp/masivo/extraflete/buscarCodextrafletes.jsp?mensaje=Anulacion Exitosa...";
                }catch (Exception m){
                    throw new ServletException(m.getMessage());
                }
                
                break;
                
            case 5:   //Caso para listar todos los codigos
                try{
                    model.codextrafleteService.listarCodextraflete();
                    next = "/jsp/masivo/extraflete/Listadocodigosextraflete.jsp";
                }catch (SQLException m){
                    throw new ServletException(m.getMessage());
                }
                
                break;
                
        }
        this.dispatchRequest(next);
        
    }
    
    
}
