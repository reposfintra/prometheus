/**
 *
 */
package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import java.util.*;
import java.text.*;



/**
 * Searches the database for puchase orders matching the
 * purchase search argument
 */
public class HReporteCreateAction extends Action {
    static Logger logger = Logger.getLogger(HReporteCreateAction.class);
    
    /**
     * Executes the action
     */
    public void run() throws ServletException, InformationException {
        String next = null;
        ReporteConductor rConductor = null;
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");

        rConductor = new ReporteConductor();
        rConductor.setPlanilla(request.getParameter("planilla"));
        rConductor.setPlaca_vehiculo(request.getParameter("placa_vehiculo"));
        rConductor.setPlaca_unidad_carga(request.getParameter("placa_unidad_carga"));
        rConductor.setContenedores(request.getParameter("contenedores"));
        rConductor.setPrecinto(request.getParameter("precinto"));
        rConductor.setRuta(request.getParameter("ruta"));
        rConductor.setComentario(request.getParameter("comentarios"));
        rConductor.setConductor(request.getParameter("conductor"));
        rConductor.setCia(request.getParameter("cia"));
        rConductor.setUsuario(usuario.getLogin());
        rConductor.setCreation_user(usuario.getLogin());
        rConductor.setAgcUsuario(usuario.getId_agencia());

        try{
            model.hReporteService.createHjReporte(rConductor);
            rConductor.setUsuario(usuario.getNombre());
            model.hReporteService.generarReporteHojaConductor(rConductor);
            next = "/hojareporte/PantallaReporteHR.jsp";
            logger.info(usuario.getNombre() + " Cre� Hoja de Reporte con planilla = " + rConductor.getPlanilla());
        }
        catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);    
    }
}
