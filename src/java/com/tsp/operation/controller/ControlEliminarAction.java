/*******************************************************************
 * Nombre clase: PlacaBuscarAction.java
 * Descripci�n: Accion para buscar una placa de la bd.
 * Autor: Ing. Jose de la rosa
 * Fecha: 16 de febrero de 2006, 05:41 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import com.tsp.exceptions.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
/**
 *
 * @author  jdelarosa
 */
public class ControlEliminarAction extends Action{
    
    /** Creates a new instance of PlacaBuscarAction */
    public ControlEliminarAction() {
    }
    
    public void run () throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "/jsp/trafico/planviaje/control_excepcion/ModificarControl_execpcion.jsp?reload=ok";
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario)session.getAttribute ("Usuario");
        String cod = request.getParameter ("codigo")!=null?request.getParameter ("codigo"):"";
        String ori = request.getParameter ("origen")!=null?request.getParameter ("origen"):"";
        String des = request.getParameter ("destino")!=null?request.getParameter ("destino"):"";
        String age = (request.getParameter ("agencia")!=null)?request.getParameter ("agencia"):"";
        
        String codigo = cod.toUpperCase();
        String origen = ori.toUpperCase();
        String destino = des.toUpperCase();
        String agencia = age.toUpperCase();
                
        Vector vector = new Vector();
        try{
            System.out.println("codigo"+ codigo + "origen  "+origen+ "destino  "+destino+ "agencia  "+agencia);
            model.control_excepcionService.eliminarControl(codigo, origen, destino, agencia);
               next+="&msg=Se a Eliminado Satisfactoriamente";
           
           
        }catch (Exception e){
            e.printStackTrace ();
        }
        
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest (next);
    }
}
