/***************************************
 * Nombre Clase ............. AnticiposPagosTercerosAction.java
 * Descripci�n  .. . . . . .  Maneja los eventos para los anticipos por terceros
 * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
 * Fecha . . . . . . . . . .  04/08/2006
 * versi�n . . . . . . . . .  1.0
 * Copyright ...Transportes Sanchez Polo S.A.
 *******************************************/



package com.tsp.operation.controller;



import java.io.*;
import java.util.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.threads.HArchivosTransferenciaBancos;
import com.tsp.operation.model.threads.HExportarExel_Reporte_de_cumplidos;
import javax.servlet.*;
import javax.servlet.http.*;




public class AnticiposPagosTercerosAction extends Action{
    
    
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        try{
            HttpSession session = request.getSession();
            com.tsp.util.Util.logController(((com.tsp.operation.model.beans.Usuario)session.getAttribute("Usuario")).getLogin(),this.getClass().getName());
            Usuario     usuario       = (Usuario)session.getAttribute("Usuario");
            String      user          = usuario.getLogin();
            String      distrito      = usuario.getDstrct();
            String      propietario_p = (String) request.getParameter("propietario_p");
            String      proveedor     =  model.AnticiposPagosTercerosSvc.getProveedorUser(user);
            
            String      valu        = request.getParameter("infoCTA");
            String      next     = "/anticipos/ListaPagosTerceros.jsp?vista=";
            String      vista    = "";
            String      msj      = "";
            String[]    bancos   ;
            
            
            
            String evento        = request.getParameter("evento");
            
            System.out.println("evento :"  + evento);
            if(evento!=null){
                
                if(evento.equals("ANULAR"))
                {   String [] ids=request.getParameterValues("id");
                    String [] obss=new String [ids.length];
                    for (int i=0;i<ids.length;i++)
                    {   obss[i]=request.getParameter("obs"+ids[i]);
                        //System.out.println("id="+ids[i]+", obs= "+obss[i]);
                    }
                    model.AnticiposPagosTercerosSvc.AnularAnticipos(user,ids,obss);
                }
                
                if( evento.equals("REFRESCAR")){
                    String  accion = request.getParameter("vista");
                    
                    if( accion.equals("TRANSFERIR")){
                        String desBanco="";
                        String Ncuenta ="";
                        String Tcuenta ="";
                        if ( valu != null ) {
                            String[] vec        =  valu.split("-");
                            desBanco   =  vec[1];
                            Ncuenta    =  vec[2];
                           // Tcuenta    =  vec[3]; //tipo cuenta
                        }
                        if(Ncuenta.equals(""))
                            Ncuenta="69225845948";
                        model.AnticiposPagosTercerosSvc.searchListaCuentasTercero(proveedor);
                        model.AnticiposPagosTercerosSvc.refrescarAnticipos_por_Transferir(desBanco,Ncuenta, propietario_p,usuario);
                        List lista  =  model.AnticiposPagosTercerosSvc.getListPorTransferir();
                        request.setAttribute("listaAnticipos",  lista );
                        vista       = "TRANSFERIR";
                    }
                    
                    if( accion.equals("APROBAR")){
                        model.AnticiposPagosTercerosSvc.refrescarAnticipos_por_Aprobar();
                        List    lista =  model.AnticiposPagosTercerosSvc.getListPorAprobar();
                        request.setAttribute("listaAnticipos",  lista );
                        vista         = "APROBAR";
                    }
                    
                    
                }
                
                
                
                if( evento.equals("INITCONSULTA") ){
                    model.AnticiposPagosTercerosSvc.loadAgencias();
                    next    = "/anticipos/ConsultaPagosTerceros.jsp?vista=";
                }
                
                ////////////////////////////////////////
                // Julio Barros Rueda 29-12-2006      //
                ////////////////////////////////////////
                if( evento.equals("ELIMINARTRANSACCION") ){
                    String   infoBanco   =  request.getParameter("infoCTA");
                    String[] vec         =  infoBanco.split("-");
                    String   desBanco    =  vec[1];
                    String   cta         =  vec[2];
                    //String Tcuenta       =  vec[3];
                    String   secuencia  =  request.getParameter("sec");
                    ////System.out.println("A la Lista "+secuencia);
                    List ParaReversar  =  model.AnticiposPagosTercerosSvc.Listar_OC_Fec(secuencia);
                    ////System.out.println("ParaReversar  -> "+ParaReversar.size());
                    if (ParaReversar != null && ParaReversar.size() > 0 ){
                        ////System.out.println("revertir <------------->");
                        msj = Revertir_Pronto_Pago(ParaReversar,secuencia,usuario);
                    }else{
                        msj = "No se Pudo Reversar el Prontopago";
                        
                    }
                    model.AnticiposPagosTercerosSvc.refrescarAnticipos_por_Transferir(desBanco,cta, propietario_p,usuario);
                    List lista  =  model.AnticiposPagosTercerosSvc.getListPorTransferir();
                    request.setAttribute("listaAnticipos",  lista );
                    vista       = "TRANSFERIR&Ncuenta"+cta;
                }
                ////////////////////////////////////////
                
                
                
                
                
                
                
                
                
                
                ////////////////////////////////////////
                // Julio Barros Rueda 19-12-2006      //
                ////////////////////////////////////////
                if( evento.equals("REPORTECUMPLIDOS") ){
                    
                    String   fechai  =  request.getParameter("fechai");
                    String   fechaf  =  request.getParameter("fechaf");
                    
                    HExportarExel_Reporte_de_cumplidos  hilo2 =  new  HExportarExel_Reporte_de_cumplidos();
                    hilo2.init(model,usuario,fechai,fechaf);
                    
                    msj = "Se esta generando el Archivo";
                    
                    next="/anticipos/ReportedeCumplidos.jsp?msj="+msj;//
                    
                    
                }
                ////////////////////////////////////////
                
                
                
                
                // Aprobaci�n:
                if( evento.equals("BUSCARPORAPROBAR") ){
                    
                    String ckAgencia      =  request.getParameter("ckAgencia");
                    String Agencia        =  request.getParameter("Agencia");
                    String ckPropietario  =  request.getParameter("ckPropietario");
                    String Propietario    =  request.getParameter("Propietario");
                    String ckPlanilla     =  request.getParameter("ckPlanilla");
                    String Planilla       =  request.getParameter("Planilla");
                    String ckPlaca        =  request.getParameter("ckPlaca");
                    String Placa          =  request.getParameter("Placa");
                    String ckConductor    =  request.getParameter("ckConductor");
                    String Conductor      =  request.getParameter("Conductor");
                    
                    
                    model.AnticiposPagosTercerosSvc.getAnticipos_por_Aprobar(distrito, proveedor, ckAgencia, Agencia, ckPropietario, Propietario, ckPlanilla, Planilla, ckPlaca, Placa, ckConductor, Conductor );
                    List    lista =  model.AnticiposPagosTercerosSvc.getListPorAprobar();
                    request.setAttribute("listaAnticipos",  lista );
                    vista         = "APROBAR";
                    if( lista.size()==0 )
                        msj = "No hay registros para ser aprobados";
                }
                
                
                
                if( evento.equals("APROBAR") ){
                    String[] anticipos  =  request.getParameterValues("anticipo");
                    List     seleccion  =  model.AnticiposPagosTercerosSvc.getSeleccion( anticipos , "AP");
                    String[]   vecSql        =  model.AnticiposPagosTercerosSvc.aprobar(seleccion, user ).split(";");
                    TransaccionService  svc =  new  TransaccionService(usuario.getBd());
                    svc.crearStatement();
                   
                    for (String sql : vecSql) {
                        svc.getSt().addBatch(sql);
                    }
                    svc.execute();
                    List lista          =  model.AnticiposPagosTercerosSvc.getListPorAprobar();
                    request.setAttribute("listaAnticipos",  lista );
                    vista       = "APROBAR";
                    msj         = "Anticipos aprobados exitosamente...";
                    if( lista.size()==0 )
                        msj += "No hay mas registros para ser aprobados";
                }
                
                
                
                // Transferencia:
                if( evento.equals("BUSCARAPROBADAS") ){
                    
                    //System.out.println(" BUSCARAPROBADAS    : "+valu);
                    //System.out.println(" PROVEEDOR ANTICIPO : "+proveedor);
                    //System.out.println(" PROPIETARIO P : "+propietario_p);
                    
                    String   Ncuenta="";
                    if ( valu != null ) {
                        String[] vec        =  valu.split("-");
                        String   desBanco   =  vec[1];
                        Ncuenta             =  vec[2];
                        //String Tcuenta       =  vec[3];
                        //System.out.println(" bam   "+desBanco);
                        //System.out.println("Ncuenta  "+Ncuenta);
                        model.AnticiposPagosTercerosSvc.getAnticipos_por_Transferir(distrito, proveedor,desBanco,Ncuenta, propietario_p, usuario );
                        vista       = "TRANSFERIR&Ncuenta="+Ncuenta;
                    }else{
                        if(Ncuenta.equals(""))
                            Ncuenta="69225845948";
                        //System.out.println(" Los nulos   ");
                        model.AnticiposPagosTercerosSvc.searchListaCuentasTercero(proveedor);
                        model.AnticiposPagosTercerosSvc.getAnticipos_por_Transferir(distrito, proveedor,"BANCOLOMBIA",Ncuenta, propietario_p, usuario);
                        vista       = "TRANSFERIR";
                    }
                    ////System.out.println(" Los null   ");
                    List lista  =  model.AnticiposPagosTercerosSvc.getListPorTransferir();
                    request.setAttribute("listaAnticipos",  lista );
                    if( lista.size()==0 )
                        msj = "No hay registros para ser tranferidos...";
                }
                
                
                if( evento.equals("LISTABANCOS") ){
                    String desBanco="";
                    if( valu == null ){
                        desBanco="BANCOLOMBIA";
                    }
                    if ( valu != null ) {
                        desBanco   =  valu;
                    }
                    String    secuencia = request.getParameter("secue");
                    String propietario  =  request.getParameter("nit");
                    String id           =  request.getParameter("anticipo");
                    //System.out.println(propietario);
                    List   cuentas      =  model.AnticiposPagosTercerosSvc.getListaCuentas(propietario, "ALL");
                    String nombre       =  model.AnticiposPagosTercerosSvc.getNameNIT(propietario);
                    String global       =  request.getParameter("global");
                    
                    request.setAttribute("listaBancos",  cuentas );
                    next    = "/anticipos/ListaBancosPropietario.jsp?nit="+ propietario +"&anticipo="+ id +"&nombre="+ nombre +"&global="+ global +"&secu="+ secuencia +"&infoCTA="+desBanco+"&vista=";//
                }
                
                if( evento.equals("LISTABANCOS_TRANSACCION") ){
                    String desBanco     = "";
                    String Ncuenta      = request.getParameter("Ncuenta");
                    String secuencia    = request.getParameter("secue");
                    String propietario  = request.getParameter("nit");
                    String id           = request.getParameter("anticipo");
                    //List   cuentas    = model.AnticiposPagosTercerosSvc.getListaCuentas(propietario, "ALL");
                    String nombre       = model.AnticiposPagosTercerosSvc.getNameNIT(propietario);
                    String global       = request.getParameter("global");
                    //request.setAttribute("listaBancos",  cuentas );
                    next    = "/anticipos/ListaBancosTransaccion.jsp?nit="+ propietario +"&anticipo="+ id +"&nombre="+ nombre +"&global="+ global +"&secu="+ secuencia +"&infoCTA="+desBanco+"&vista=";//
                }
                
                if( evento.equals("ASIGNARBANCO_TRANSACCION") ){
                    ////System.out.println("arranco");
                    String Banco        ="";
                    String sql          ="";
                    String   Ncuenta    ="";
                    if ( valu != null ) {
                        String[] vec        =  valu.split("-");
                        Banco               =  vec[1];
                        Ncuenta    =  vec[2];
                    }
                    
                   
                    String secuencia    = request.getParameter("secue");
                    String propietario  = request.getParameter("nit");
                    String global       = request.getParameter("global");
                    String anticipo_I   =  request.getParameter("anticipo");
                    TreeMap t           = new TreeMap();
                    int centinela       =   0;
                    List lista2         =new LinkedList();
                    List lista  =  model.AnticiposPagosTercerosSvc.getListPorTransferir();
                    //if(global.equals("S") || (!secuencia.equals("0") && !secuencia.equals("") && secuencia!=null ) )  {
                    ArrayList<String> listaQuerys =new ArrayList<>();
                    for(int i=0; i<lista.size(); i++){
                        AnticiposTerceros anticipo = (AnticiposTerceros) lista.get(i);
                        if( (""+anticipo.getSecuencia()).equals(secuencia) ){
                            ////System.out.println(i+"  Secuencia = "+secuencia+" nit = "+propietario+" Banco = "+Banco+" anticipo.getId() = "+anticipo.getId());
                            if ( centinela != 0 ){
                                String[] listQ= model.AnticiposPagosTercerosSvc.aplicarValores2(propietario, ""+anticipo.getId()).split(";");
                                listaQuerys.addAll(Arrays.asList(listQ));
                            }else{
                                String[] listQ=model.AnticiposPagosTercerosSvc.aplicarValores(propietario,""+anticipo.getId(),Banco).split(";");
                                listaQuerys.addAll(Arrays.asList(listQ));
                            }
                            if ( valu != null ) {
                                String[] vec        =  valu.split("-");
                                anticipo.setBanco_transferencia(vec[0]);
                                anticipo.setCuenta_transferencia(vec[2]);
                                anticipo.setTcta_transferencia(vec[3]);
                                
                            }
                            t.put(""+anticipo.getId(), anticipo.getPlanilla());
                            centinela++;
                            lista2.add(anticipo);
                        }else{
                            if( (""+anticipo.getId()).equals(anticipo_I) ){
                                 String[] listQ=model.AnticiposPagosTercerosSvc.aplicarValores(propietario,anticipo_I,Banco).split(";");
                                 listaQuerys.addAll(Arrays.asList(listQ));
                                if ( valu != null ) {
                                    String[] vec        =  valu.split("-");
                                    anticipo.setBanco_transferencia(vec[0]);
                                    anticipo.setCuenta_transferencia(vec[2]);
                                    anticipo.setTcta_transferencia(vec[3]);
                                    
                                }
                                t.put(anticipo_I, anticipo.getPlanilla());
                                centinela++;
                                lista2.add(anticipo);
                            }
                        }
                    }
                     
                      TransaccionService tservice = new TransaccionService(usuario.getBd());
                      tservice.crearStatement();
                      tservice = getAddBatch(tservice, listaQuerys);
                      tservice.execute();
                      tservice.closeAll();
                     
                    
                    sql=model.AnticiposPagosTercerosSvc.cuenta_a_transferir(lista2);
                    
                    try  {
                        model.tService.crearStatement();
                        model.tService.getSt().addBatch(sql);
                        model.tService.execute();
                    } catch (Exception e) {
                        e.printStackTrace();
                        throw new Exception(e.getMessage());
                    } finally{
                        model.tService.closeAll();
                    }
                    //System.out.println("Ncuenta   "+Ncuenta);
                    model.AnticiposPagosTercerosSvc.refrescarAnticipos_por_Transferir("DOS",Ncuenta, propietario_p,usuario);
                    lista  =  model.AnticiposPagosTercerosSvc.getListPorTransferir();
                    request.setAttribute("listaAnticipos",  lista );
                    vista       = "TRANSFERIR&desBanco="+Banco+"&Ncuenta="+Ncuenta;
                    
                }
                if( evento.equals("ASIGNARBANCO") ){
                    TreeMap t = new TreeMap();
                    String secuencia    =   request.getParameter("secue");
                    String propietario  =   request.getParameter("nit");
                    String secCta       =   request.getParameter("sec");//secuemcia de la cuenta
                    String global       =   request.getParameter("global");
                    String Bank         =   request.getParameter("desBanco");
                    String  Ncuenta     =   request.getParameter("Ncuenta");
                    //System.out.println("Ncuenta  Asig "+Ncuenta);
                    int centinela       =   0;
                    
                    //  para que la informacion se cargue por beneficiario globalmente o individual julio barros 14-11-2006 //
                    if(global.equals("S") || (!secuencia.equals("0") && !secuencia.equals("") && secuencia!=null ) )  {
                        List lista  =  model.AnticiposPagosTercerosSvc.getListPorTransferir();
                        ArrayList<String> listaQuerys =new ArrayList<>();
                        for(int i=0; i<lista.size(); i++){
                            AnticiposTerceros anticipo = (AnticiposTerceros) lista.get(i);
                            if(global.equals("S")){
                                if( anticipo.getPla_owner().equals(propietario) ){
                                    if( t.get(""+anticipo.getId())==null || t.get(""+anticipo.getId()).equals("") ){
                                            String[] listQ =model.AnticiposPagosTercerosSvc.asignarCTA(propietario, secCta, ""+anticipo.getId(), Bank ).split(";");
                                            listaQuerys.addAll(Arrays.asList(listQ));                                       
                                        t.put(""+anticipo.getId(), anticipo.getPlanilla());
                                        i--;
                                        if ( centinela != 0 )
                                            listaQuerys.add(model.AnticiposPagosTercerosSvc.aplicarValores2(propietario, ""+anticipo.getId()));
                                        centinela++;
                                    }
                                }
                            }else{
                                if( (""+anticipo.getSecuencia()).equals(secuencia) ){
                                    if( t.get(""+anticipo.getId())==null || t.get(""+anticipo.getId()).equals("") ){
                                          String[] listQ =model.AnticiposPagosTercerosSvc.asignarCTA(propietario, secCta, ""+anticipo.getId(),Bank ).split(";");
                                          listaQuerys.addAll(Arrays.asList(listQ)); 
                                        t.put(""+anticipo.getId(), anticipo.getPlanilla());
                                        i--;
                                        if ( centinela != 0 )
                                            listaQuerys.add(model.AnticiposPagosTercerosSvc.aplicarValores2(propietario, ""+anticipo.getId()));
                                        centinela++;
                                    }
                                }
                            }
                        }
                        
                        //vamos aplicar valores a todo en una sola conexion.
                        TransaccionService tservice = new TransaccionService(usuario.getBd());
                        tservice.crearStatement();
                        tservice = getAddBatch(tservice, listaQuerys);
                        tservice.execute();
                        tservice.closeAll();
                        
                        
                        model.AnticiposPagosTercerosSvc.refrescarAnticipos_por_Transferir("DOS",Ncuenta, propietario_p,usuario);
                        lista  =  model.AnticiposPagosTercerosSvc.getListPorTransferir();
                        request.setAttribute("listaAnticipos",  lista );
                        vista       = "TRANSFERIR&desBanco="+Bank;
                    }else{
                        
                        String anticipo     =  request.getParameter("anticipo");
                        String[] listQ =model.AnticiposPagosTercerosSvc.asignarCTA(propietario, secCta, anticipo,Bank).split(";");
                        //vamos aplicar valores a todo en una sola conexion.
                        TransaccionService tservice = new TransaccionService(usuario.getBd());
                        tservice.crearStatement();
                        for (String listQ1 : listQ) {
                             tservice.getSt().addBatch(listQ1);
                        }                       
                        tservice.execute();
                        tservice.closeAll();
                        
                        
                        model.AnticiposPagosTercerosSvc.refrescarAnticipos_por_Transferir(Bank,Ncuenta, propietario_p ,usuario);
                        List lista  =  model.AnticiposPagosTercerosSvc.getListPorTransferir();
                        request.setAttribute("listaAnticipos",  lista );
                        vista       = "TRANSFERIR";
                    }
                }
                
                if( evento.equals("ASIGNARDESCUENTO") ){
                    String propietario  =  request.getParameter("nit");
                    String anticipo     =  request.getParameter("anticipo");
                    double valor        =  Double.parseDouble( request.getParameter("valor") );
                    String Ncuenta      =  request.getParameter("Ncuenta");
                    
                    String desBanco     =  "";
                    if( valu == null ){
                        desBanco="BANCOLOMBIA";
                    }
                    
                    if ( valu != null ) {
                        desBanco   =  valu;
                    }
                    
                    String asignarDescuento = model.AnticiposPagosTercerosSvc.asignarDescuento(propietario,anticipo, valor,desBanco);
                    TransaccionService tservice = new TransaccionService(usuario.getBd());
                        tservice.crearStatement();                      
                        tservice.getSt().addBatch(asignarDescuento);                                          
                        tservice.execute();
                        tservice.closeAll();
                        
                    model.AnticiposPagosTercerosSvc.refrescarAnticipos_por_Transferir(desBanco,Ncuenta,propietario_p,usuario);
                    
                    List lista  =  model.AnticiposPagosTercerosSvc.getListPorTransferir();
                    request.setAttribute("listaAnticipos",  lista );
                    vista       = "TRANSFERIR";
                }
                
                
                if( evento.equals("TRANSFERIR") ){
                    String[] anticipos  =  request.getParameterValues("anticipo");
                    String   infoBanco  =  request.getParameter("infoCTA");
                    String[] vec        =  infoBanco.split("-");
                    String   banco      =  vec[0];
                    String   desBanco   =  vec[1];
                    String   cta        =  vec[2];
                    String   tipoCta    =  vec[3];
                    
                    List     seleccion  =  model.AnticiposPagosTercerosSvc.getSeleccion( anticipos , "TR");
                    String   name       =  model.AnticiposPagosTercerosSvc.getNameProveedor();
                    
                    msj                 = "El archivo para transferir al banco "+  desBanco +" est� siendo generado....";
                    
                    if( seleccion.size()>0){
                        HArchivosTransferenciaBancos  hilo =  new  HArchivosTransferenciaBancos();
                       // System.out.println("model_"+model+"_usuario_"+usuario+"_proveedor_"+proveedor);
                        //System.out.println("name_"+name);
                        //System.out.println("banco_"+banco);
                        //System.out.println("desBanco_"+desBanco);
                        //System.out.println("cta_"+cta);
                        //System.out.println("tipoCta_"+tipoCta);
                        hilo.start(model, usuario, anticipos,  seleccion, proveedor, name, banco,desBanco,  cta, tipoCta );
                    }
                    else
                        msj             = "No hay registros para realizar transferencias";
                    
                    List lista          =  model.AnticiposPagosTercerosSvc.getListPorTransferir();
                    request.setAttribute("listaAnticipos",  lista );
                    vista               = "TRANSFERIR"+"&Ncuenta="+cta;
                    
                    if( lista.size()==0 )
                        msj += "No hay mas registros para ser transferidos";
                    
                }
            }
            if(evento.equals("modbanco")){
                    int filas = request.getParameter("misfilas")!=null ? Integer.parseInt(request.getParameter("misfilas")):0;
                    ArrayList lista = null;
                    if(filas>0){
                        lista = new ArrayList();
                        for (int i = 0; i < filas; i++) {
                            if(request.getParameter("chx_b"+i)!=null){
                                lista.add(request.getParameter("chx_b"+i) + ";-;"+request.getParameter("bank"+i));
                            }
                        }
                        msj = model.AnticiposPagosTercerosSvc.updateBank(lista);
                    }
                    else{
                        System.out.println("No se recibieron filas para modificar...");
                        msj = "No se recibieron filas para modificar...";
                    }
                    vista       = "TRANSFERIR";
                }//rhonalf 2010-05-13

            
            if(!evento.equals("ANULAR")){
                next += vista +"&msj="+ msj;

                RequestDispatcher rd = application.getRequestDispatcher(next);
                if(rd == null)
                    throw new Exception("No se pudo encontrar "+ next);
                rd.forward(request, response);
            }
            else
            {   response.setContentType("text/plain");
                response.setHeader("Cache-Control", "no-cache");
                response.getWriter().println("El proceso ha terminado exitosamente");

            }
            
        } catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        
    }
    
    
    
    
    
    
    
   public void ejecutar(String dataBaseName, String sql)throws Exception{
        try{
            TransaccionService  svc =  new  TransaccionService(dataBaseName);
            svc.crearStatement();
            svc.getSt().addBatch(sql);
            svc.execute();
            
        }catch(Exception e){
            throw new Exception( e.getMessage() );
        }
    }
    
    
    
    private String Revertir_Pronto_Pago(List ParaReversar,String secuencia,Usuario usuario) throws Exception{
        String      propietario_p = (String) request.getParameter("propietario_p");
        String      msj      = "No se Pudo Reversar el Prontopago";
        String   infoBanco   =  request.getParameter("infoCTA");
        String[] vec         =  infoBanco.split("-");
        String   desBanco    =  vec[1];
        String   cta         =  vec[2];
        boolean estadoProceso = true;
        try{
            try{
                model.tService.crearStatement();
                for (int i=0; i<ParaReversar.size();i++){
                    Movpla movpla = new Movpla();
                    movpla=model.AnticiposPagosTercerosSvc.searchMovPla(ParaReversar.get(i).toString(),secuencia);
                    String sqlMovpla =  model.movplaService.insertMovPla2(movpla, usuario.getBase());
                    //System.out.println("SQL 01--> "+sqlMovpla);
                    //model.tService.getSt().addBatch(sqlMovpla);
                }
                String Elim_Ex_ExD = model.AnticiposPagosTercerosSvc.Eliminar_Extracto_ExtractoDetalle(secuencia);
                //System.out.println("SQL 02--> "+Elim_Ex_ExD);
               // model.tService.getSt().addBatch(Elim_Ex_ExD);
                if (estadoProceso){
                    model.tService.getSt().addBatch(Elim_Ex_ExD);
                    model.tService.execute();
                }
            } catch (Exception e) {
                e.printStackTrace();
                estadoProceso = false;
                throw new Exception(e.getMessage());
            } finally{
                model.tService.closeAll();
            }
            if( estadoProceso ){
                model.AnticiposPagosTercerosSvc.refrescarAnticipos_por_Transferir(desBanco,cta, propietario_p,usuario);
                List lista  =  model.AnticiposPagosTercerosSvc.getListPorTransferir();
                request.setAttribute("listaAnticipos",  lista );
                msj = "El Pronto Pago Fue Reversado Correctamente";
                
            }else if (!estadoProceso){
                model.AnticiposPagosTercerosSvc.refrescarAnticipos_por_Transferir(desBanco,cta, propietario_p,usuario);
                List lista  =  model.AnticiposPagosTercerosSvc.getListPorTransferir();
                request.setAttribute("listaAnticipos",  lista );
                msj = "No se Pudo Reversar el Prontopago";
            }
        } catch (Exception ex){
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
        return msj;
        
    }
    
}
