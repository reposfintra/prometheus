/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.controller;
import java.util.*;
import javax.servlet.http.*;
import com.tsp.operation.model.services.*;
/**
 *
 * @author Rhonalf
 */
public class CotizacionBuscarAction extends Action {
    public CotizacionBuscarAction(){

    }

    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next  = "/jsp/delectricaribe/cotizacion/lista.jsp";
        request.setAttribute("msg", "");
        try {
            String cons = request.getParameter("consecutivo")!=null? request.getParameter("consecutivo"):"";
            CotizacionService cserv = new CotizacionService();
            ArrayList listica = cserv.buscar(cons, "id_accion");
            request.setAttribute("listado", listica);
        }
        catch(Exception e){
            System.out.println("Error en el run de cotizacion buscar action: "+e.toString());
            request.setAttribute("msg", "Error en la busqueda");
        }
        this.dispatchRequest(next);
    }

}
