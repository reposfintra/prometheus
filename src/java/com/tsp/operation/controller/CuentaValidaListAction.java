/*******************************************************************
 *      Nombre Clase.................   CuentaValidaUpdateAction.java
 *      Descripci�n..................   Lista Cuentas Validas
 *      Autor........................   Ing. Leonardo Parody
 *      Fecha........................   19.01.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  EQUIPO12
 */
public class CuentaValidaListAction extends Action{
    
    /** Creates a new instance of CuentaValidaListAction */
    public CuentaValidaListAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next = "/jsp/finanzas/Cuentas_Validas/CuentasValidasList.jsp";
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String user = usuario.getLogin ();
        String base = usuario.getBase ();
        String dstrct = usuario.getDstrct ();
        String clase = request.getParameter ("clase");
        String agencia = request.getParameter ("agencia");
        String cliente_area = request.getParameter ("cliente_area");
        String unidad = request.getParameter ("unidad");
        String elemento = request.getParameter ("elemento");
        List cuentas;
        try {
            cuentas = model.cuentavalidaservice.ListCuentaValida ( clase,  agencia,  cliente_area,  elemento,  unidad);
            request.setAttribute ("cuentas", cuentas);
            
        }catch(SQLException e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
}
