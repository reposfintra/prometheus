/*
 * AnularZonaAction.java
 *
 * Created on 13 de junio de 2005, 10:01 AM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  Henry
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;

public class AnularZonaUsuarioAction extends Action{
    
    /** Creates a new instance of AnularZonaAction */
    public AnularZonaUsuarioAction() {
    }
    
    public void run() throws ServletException {
        String next = "/jsp/trafico/zona/VerZonasUsuario.jsp?reload=";
        String codigo = (request.getParameter("codigo"));
        String nombre = (request.getParameter("nombre"));
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        try{
            if(!model.zonaUsuarioService.existeZonaUsuario(codigo,nombre)){
                next = "jsp/trafico/zona/zonaUsuario.jsp?mensaje=Error_Anular";    
            }
            else{
                ZonaUsuario zona = new ZonaUsuario();
                zona.setCodZona(codigo);                
                zona.setNomUsuario(nombre);
                zona.setUser_update(usuario.getLogin());
                model.zonaUsuarioService.eliminarZonaUsuario(zona);               
                next = next + "Anulado";
            }
        }
        catch (SQLException e){
               throw new ServletException(e.getMessage());
        }
         
         // Redireccionar a la p�gina indicada.
       this.dispatchRequest(next);
    }
    
}
