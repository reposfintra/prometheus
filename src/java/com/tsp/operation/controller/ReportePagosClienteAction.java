/*******************************************************************
 * Nombre clase: ClienteBuscarAction.java
 * Descripci�n: Accion para buscar una placa de la bd.
 * Autor: Ing. fily fernandez
 * Fecha: 16 de febrero de 2006, 05:41 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;
import com.tsp.finanzas.contab.model.beans.*;
import com.tsp.finanzas.contab.model.services.*;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import java.lang.*;
import com.tsp.operation.model.threads.*;
import java.util.Date.*;

/**
 *
 * @author  jdelarosa
 */
public class ReportePagosClienteAction extends Action{
    
    /** Creates a new instance of PlacaBuscarAction */
    public ReportePagosClienteAction () {
    }
    
    public void run () throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String FechaI = request.getParameter ("FechaI")!=null?request.getParameter ("FechaI"):"";
        String FechaF = request.getParameter ("FechaF")!=null?request.getParameter ("FechaF"):"";
        String codcli = request.getParameter("codcli")!=null?request.getParameter ("codcli"):"";
        String op = request.getParameter ("op")!=null?request.getParameter ("op"):"";
        String next = "";
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario)session.getAttribute ("Usuario");
        String usu = usuario.getLogin();
        
        try{
            
            if(op.equals("1")){
             next="/jsp/cxcobrar/reportes/ReportePagosCliente.jsp?fecI="+FechaI+"&fecF="+FechaF+"&codcli="+codcli;
           
             model.facturaService.getVector();
             model.facturaService.ReportePagosClientes(FechaI, FechaF, codcli);
             Vector datos = model.facturaService.getVector(); 
             if ( datos.size () <= 0 ) {    
                     next = next + "&msg=Su busqueda no arrojo resultados!";
                
                }  
            }
            if(op.equals("2")){
                HReporteFactPagosCliente fc = new HReporteFactPagosCliente();
                fc.start(FechaI, FechaF, usu, codcli, usuario.getBd());                
                next="/jsp/cxcobrar/reportes/ReportePagosCliente.jsp?fecI="+FechaI+"&fecF="+FechaF+"&codcli="+codcli;
              
            }
              
        } catch ( Exception e ) {
            e.printStackTrace();
            throw new ServletException ( "Error en la reporte pagos cliente : " + e.getMessage() );
            
        }        
        
        this.dispatchRequest ( next );
        
    }
    
}
