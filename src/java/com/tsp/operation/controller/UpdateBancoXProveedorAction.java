/********************************************************************
 *      Nombre Clase.................   UpdateBancoXProveedorAction .java
 *      Descripci�n..................   Clase encargada de los servicios para la actualizacion de bancos por proveedor
 *      Autor........................   Ivan gomez
 *      Fecha........................   4 de mayo del 2007
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.util.Util;
/**
 *
 * @author  Ivan Dario Gomez
 */
public class UpdateBancoXProveedorAction extends Action{
    
    /** Creates a new instance of FacturaServiciosAction */
    public UpdateBancoXProveedorAction() {
    }
    
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String distrito =  usuario.getDstrct();
            String opcion = (request.getParameter("OP")!= null)?request.getParameter("OP"):"";
            String next ="";
            System.out.println("FacturaServiciosAction");
            System.out.println("OPCION------------------->" + opcion);
            
                
            if(opcion.equals("UPDATE_BANCO")){
               
                String banco    = request.getParameter("banco");
                String sucursal = request.getParameter("sucursal");
                String proveedor   = request.getParameter("proveedor");
                boolean act_proveedor = (request.getParameter("act_proveedor") != null)?true:false;
                
                
                System.out.println("ACTUALIZAR PROVEEDORR--->" + act_proveedor);
                Proveedor o_proveedor   = model.proveedorService.obtenerProveedorPorNit(proveedor);
                if(o_proveedor!=null){ 
                     int numFac = model.cxpDocService.updateBancoXProveedor(usuario.getDstrct(), proveedor,  banco, sucursal,act_proveedor);  
                     next = "/jsp/cxpagar/facturasxpagar/UpdateBancoXProveedor.jsp?mensaje=Se Actualizaron "+numFac+" Facturas Con Exito..."; 
                }else{
                    next = "/jsp/cxpagar/facturasxpagar/UpdateBancoXProveedor.jsp?mensaje=El proveedor  "+proveedor+" no existe en la base de datos";
                }
                
               
                
               
                
            }
            
            
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new Exception("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
            
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new ServletException("Accion:"+ e.getMessage());
        }
        
    }
    
}
