/*
 * ConsultarNitAction.java
 *
 * Created on 27 de septiembre de 2006, 04:59 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

/**
 *
 * @author  EQUIPO13
 */
public class ConsultarNitAction  extends Action{
    
    /** Creates a new instance of ConsultarNitAction */
    public ConsultarNitAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException {
        
        String nit = request.getParameter("nit");
        String next="/consultas/consultaNitRespuesta.jsp?";
        try{
            if(request.getParameter("nit")!=null)
                model.conductorService.consultaConductor(nit);
            else{
                Proveedor prov = new Proveedor();
                prov.setC_agency_id(( request.getParameter("c_agency_id") ==null ) ? "" : request.getParameter("c_agency_id") );
                prov.setC_autoretenedor_iva( ( request.getParameter("c_autoretenedor_iva") ==null )? "" : request.getParameter("c_autoretenedor_iva")) ;
                prov.setC_agente_retenedor(( request.getParameter("c_agente_retenedor") ==null ) ? "" : request.getParameter("c_agente_retenedor"));
                prov.setC_autoretenedor_ica( ( request.getParameter("c_autoretenedor_ica") ==null ) ? "" : request.getParameter("c_autoretenedor_ica"));
                prov.setC_autoretenedor_rfte( ( request.getParameter("c_autoretenedor_rfte") ==null  ) ? "" : request.getParameter("c_autoretenedor_rfte"));
                prov.setC_banco_transfer( ( request.getParameter("c_banco_transfer")==null  ) ? "" : request.getParameter("c_banco_transfer"));
                prov.setC_branch_code( ( request.getParameter("c_branch_code")==null  ) ? "" : request.getParameter("c_branch_code"));
                prov.setC_clasificacion( ( request.getParameter("c_clasificacion")==null  ) ? "" : request.getParameter("c_clasificacion"));
                prov.setC_codciudad_cuenta( ( request.getParameter("c_codciudad_cuenta")==null  ) ? "" : request.getParameter("c_codciudad_cuenta"));
                prov.setC_gran_contribuyente( (  request.getParameter("c_gran_contribuyente")==null ) ? "" : request.getParameter("c_gran_contribuyente"));
                prov.setC_idMims( ( request.getParameter("c_idMims") ==null ) ? "" : request.getParameter("c_idMims"));
                prov.setC_nit( (  request.getParameter("c_nit")==null ) ? "" : request.getParameter("c_nit"));
                prov.setC_payment_name( (  request.getParameter("c_payment_name")==null ) ? "" : request.getParameter("c_payment_name").toUpperCase());
                prov.setC_numero_cuenta( (request.getParameter("c_numero_cuenta") ==null  ) ? "" : request.getParameter("c_numero_cuenta") );
                prov.setC_sucursal_transfer( (  request.getParameter("c_sucursal_transfer")==null ) ? "" : request.getParameter("c_sucursal_transfer"));
                prov.setC_tipo_cuenta( ( request.getParameter("c_tipo_cuenta") ==null ) ? "" : request.getParameter("c_tipo_cuenta"));
                prov.setC_tipo_doc( (  request.getParameter("c_tipo_doc")==null ) ? "" :request.getParameter("c_tipo_doc"));//(  ) ? "" :
                prov.setHandle_code((request.getParameter("handle_code")==null)?"":request.getParameter("handle_code"));
                prov.setPlazo(Integer.parseInt( (request.getParameter("plazo")==null)?"":request.getParameter("plazo") ));
                //nuevo 18-03-2006
                prov.setCedula_cuenta( (request.getParameter("cedula_cuenta")==null)?"":request.getParameter("cedula_cuenta").toUpperCase() );
                prov.setNombre_cuenta( (request.getParameter("nombre_cuenta")==null)?"":request.getParameter("nombre_cuenta").toUpperCase() );
                prov.setTipo_pago( (request.getParameter("tipo_pago")==null)?"":request.getParameter("tipo_pago") );
                //jose 2006-09-27
                prov.setNit_beneficiario( (request.getParameter("nit_ben")==null)?"":request.getParameter("nit_ben") );
                prov.setNom_beneficiario( (request.getParameter("nom_ben")==null)?"":request.getParameter("nom_ben") );
                
                //01-02-2007
                prov.setConcept_code( request.getParameter("concept_code")==null?"":request.getParameter("concept_code") );
                prov.setCmc(request.getParameter("handle_code")==null?"":request.getParameter("handle_code"));
                prov.setAprobado(request.getParameter("aprobado")==null?"N":request.getParameter("aprobado"));
                
                model.proveedorService.setProveedor(prov);
                //jose 2006-09-27
                if(!request.getParameter("nit_ben").equals("")){
                    model.nitService.searchNit( request.getParameter("nit_ben") );
                    if(model.nitService.getNit()!=null){
                        prov.setNit_beneficiario( model.nitService.getNit().getCedula() );
                        prov.setNom_beneficiario( model.nitService.getNit().getNombre() );
                    }
                    else{
                        prov.setNit_beneficiario( "" );
                        prov.setNom_beneficiario( "" );
                        request.setAttribute("msg", "El nit no existe en la BD");
                    }
                }
                else{
                    prov.setNit_beneficiario( "" );
                    prov.setNom_beneficiario( "" );
                }
                next  = request.getParameter("carpeta")+ request.getParameter("pagina");
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
