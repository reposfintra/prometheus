/*******************************************************************
 * Nombre clase: FechasRemesaDestinatarioAction.java
 * Descripci�n: Accion para ingresa y buscar todo de remesas destinatario
 * Autor: Ing. Jose de la rosa
 * Fecha: 2 de mayo de 2007, 04:25 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class FechasRemesaDestinatarioAction{// extends Action{
    /*
    /** Creates a new instance of FechasRemesaDestinatarioAction 
    public FechasRemesaDestinatarioAction() {/*
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/jsp/masivo/remitente_destinatario/RemesaDestinatario.jsp";
        HttpSession session	= request.getSession();
        Usuario usuario		= (Usuario) session.getAttribute("Usuario");
        String dstrct		= (String) session.getAttribute("Distrito");
        String opcion		= request.getParameter("opcion")!=null?request.getParameter("opcion"):"";
        String planilla		= request.getParameter("c_planilla")!=null?request.getParameter("c_planilla"):"";
        String fechaI           = "";
        String fechaF           = "";
        String sql              = "";
        try{
            if(opcion.equals ("1")){
                model.remesaService.searchRemesaDestinatario( planilla );
                if( model.remesaService.getRemesas() == null || model.remesaService.getRemesas().size() == 0 )
                    request.setAttribute ("mensaje", "No existen datos para la planilla "+planilla);
            }
            else if(opcion.equals ("2")){
                if( model.remesaService.getRemesas()!= null ){
                    for( int j = 0; j < model.remesaService.getRemesas().size(); j++ ){
                        Remesa r = (Remesa) model.remesaService.getRemesas().get( j );
                        fechaI = request.getParameter("fechaI"+j)!=null? !request.getParameter("fechaI"+j).equals("")? request.getParameter("fechaI"+j):"0099-01-01":"0099-01-01";
                        fechaF = request.getParameter("fechaF"+j)!=null? !request.getParameter("fechaF"+j).equals("")? request.getParameter("fechaF"+j):"0099-01-01":"0099-01-01";
                        if( ( !fechaI.equals("") && !fechaI.equals("0099-01-01") ) || ( !fechaF.equals("") && !fechaF.equals("0099-01-01") ) )
                            sql += model.remesaService.updateRemesaDestinatario(fechaI, fechaF, usuario.getLogin(), r.getNumrem(), r.getDesrem() );
                    }
                    if( !sql.equals("") ){
                        model.tService.crearStatement();
                        model.tService.getSt().addBatch(sql);
                        model.tService.execute();
                        request.setAttribute ("mensaje", "Se modifico correctamente los datos");
                        next += "?c_planilla="+planilla;
                        model.remesaService.searchRemesaDestinatario( planilla );
                    }
                }
            }else{
                next += "?c_planilla=";
                model.remesaService.setRemesas( null );
            }
        }catch(Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }*/
    
}
