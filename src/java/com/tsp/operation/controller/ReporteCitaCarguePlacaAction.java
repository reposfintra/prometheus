/**************************************************************************
 * Nombre clase: ReporteCitaCarguePlacaAction.java                         *
 * Descripci�n: Clase que ejecuta el reporte de citas de cargue de una     *
 * placa con el hilo HReporteCitaCargue.java                               *
 * Autor: Ing. Karen Reales                                                *
 * Fecha: Febero 15 2006                                                   *
 * Versi�n: Java 1.0                                                       *
 * Copyright: Fintravalores S.A. S.A.                                 *
 ***************************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.threads.*;
import com.tsp.operation.model.DAOS.*;
import java.net.URL;


public class ReporteCitaCarguePlacaAction extends Action{
    
    public ReporteCitaCarguePlacaAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        String next="";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String FecIni  = request.getParameter("fechai");
        Properties dbProps = new Properties();
        
        try{
            HReporteCitaCarguePlaca hilo =  new HReporteCitaCarguePlaca();
            hilo.start(FecIni,usuario.getLogin());
            next = "/jsp/masivo/reportes/ReporteCitaCarguePlaca.jsp?msg=Su reporte ha iniciado...";
            
            model.citacargueService.setUltimafechaCreacion();
            
           
        }catch (Exception ex){
            throw new ServletException("Error en ReporteExportarExcel Citas cargue placa .....\n"+ex.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
}
