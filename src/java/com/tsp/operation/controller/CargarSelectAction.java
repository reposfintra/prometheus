/*
 * CargarSelectAction.java
 *
 * Created on 17 de octubre de 2005, 07:56 AM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Diogenes Bastidas
 */
public class CargarSelectAction extends Action{
    
    /** Creates a new instance of CargarSelectAction */
    public CargarSelectAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String em = (request.getParameter("empleado")!=null)?request.getParameter("empleado"):"0";
        String c = (request.getParameter("conductor")!=null)?request.getParameter("conductor"):"0";
        String p = (request.getParameter("propietario")!=null)?request.getParameter("propietario"):"0";
        String v = (request.getParameter("proveedor")!=null)?request.getParameter("proveedor"):"0";
        String a = (request.getParameter("procarbon")!=null)?request.getParameter("procarbon"):"0";
        String tipodoc = em+c+p+v+a;
        /***/
        try{
            if(request.getParameter("tel")!=null){
                //System.out.println("Entra");
                model.ciudadservice.listarCiudadesxpais(request.getParameter("pais"));
            }
            else{
                model.estadoservice.listarEstado(request.getParameter("pais"));
                model.ciudadservice.listarCiudades(request.getParameter("pais"), request.getParameter("est"));
            }
            
        } catch (SQLException e ) {            
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        String next = "/" + request.getParameter("carpeta")+ "/" + request.getParameter("pagina")+"?sw=1&clas="+tipodoc+"&men=";
        HttpSession session = request.getSession();
        
        //System.out.println("next "+next);
        
        this.dispatchRequest(next);
    }
    
}