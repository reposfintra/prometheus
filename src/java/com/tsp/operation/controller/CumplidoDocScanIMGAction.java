/********************************************************************
 *      Nombre Clase.................   CumplidoDocScanIMGAction.java    
 *      Descripci�n..................   Reporte de documentos escaneados de cumplido
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   06.05.2006
 *      Versi�n......................   1.1
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;
import java.io.*;
import com.tsp.exceptions.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;


/**
 *
 * @author  dbastidas
 */
public class CumplidoDocScanIMGAction extends Action {
    
    /** Creates a new instance of BuscarImagenAction */
    public CumplidoDocScanIMGAction() {
    }
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        
        String codact = "014";
        String tipodoc = null;
        String doc = request.getParameter("numpla");
        String next="";
        /*//System.out.println("......");
        //System.out.println("Actividad "+codact );
        //System.out.println("Tipo "+ tipodoc );
        //System.out.println("Documento "+doc );*/
        
        try {
            
            model.ImagenSvc.searchImagen("1",null,"1",null,null,null,codact, null,doc,null,null,null,null,usuario.getLogin());
            List lista = model.ImagenSvc.getImagenes();
            ////System.out.println("................. DOC SCAN ENCONTRADOS: " + (lista!=null? lista.size() : "0"));
            Imagen img = null;
            String foto ="";
            String img_docs = "";
            if (lista==null){
                img_docs = "<span class=letraTitulo>No registra documentos digitalizados.</span>";
            }
            else {
                for( int i=0; i<lista.size(); i++){
                    img = new Imagen();
                    img = (Imagen) lista.get(i);
                    foto = request.getContextPath()+"/documentos/imagenes/"+ usuario.getLogin()+"/"+img.getNameImagen();
                    
                    model.documentoSvc.obtenerDocumento((String) session.getAttribute("Distrito"), img.getTipoDocumento());
                    String tipo = model.documentoSvc.getDocumento()!=null ? model.documentoSvc.getDocumento().getC_document_name()
                            : "NR";
                            
                    String popup = "window.open('" + foto + "','DocScan','width=400,height=400,scrollbars=no,resizable=no,top=10,left=65');";
                    img_docs += "<tr class=filagris><td>" + 
                            "<a href=JavaScript:void(0); onClick=" + popup + ">" + img.getNameImagen() + "</a>" + "</td>" 
                            + "<td>" + tipo + "</td></tr>";
                }    
                
                if( img_docs.length()!=0 ){
                    img_docs = "<table width=100% align=center border=1 cellpadding=2 cellspacing=1 bordercolor=#999999 bgcolor=#F7F5F4>"
                        + "<tr class=tblTitulo><td align=center >Imagen</td><td align=center >Tipo de Documento</td></tr>"  + img_docs + "</table>";
                } else {
                    img_docs = "<span class=letraTitulo>No registra documentos digitalizados.</span>";
                }
            }
            
            ////System.out.println("............... IMAGENES : " + img_docs);
            request.setAttribute("docsImg", img_docs);
            //next = "/imagen/VerImagen.jsp?foto="+foto+"&tipoAct="+codact+"&tipoDoc="+tipodoc+"&documento="+doc;
            String texto = request.getParameter("numrem") + " - Planilla " + doc;
            next = "/jsp/cumplidos/reportes/RepDocScanLista.jsp?scan=OK&numrem=" + texto;
            
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
