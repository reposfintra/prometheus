/*******************************************************************
 * Nombre clase: OCargaValidarAction.java
 * Descripci�n: Accion para realizar validar ordenes de carga.
 * Autor: Ing. Karen Reales
 * Fecha: 12 de mayo 2006
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class OCargaValidarAction extends Action{
    static Logger logger = Logger.getLogger(OCargaValidarAction.class);
    
    public OCargaValidarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        logger.info("ERROR EN LA PLACA....");
        /**
         *
         * Usuario en Sesion
         */
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        
        /**
         *
         * Inicializacion de atributos para colores en la pagina
         */
        request.setAttribute("placa","filaresaltada");
        request.setAttribute("cedula","filaresaltada");
        request.setAttribute("trailer","filaresaltada");
        request.setAttribute("c1","filaresaltada");
        request.setAttribute("c2","filaresaltada");
        request.setAttribute("cp1","filaresaltada");
        request.setAttribute("cp2","filaresaltada");
        request.setAttribute("p1","filaresaltada");
        request.setAttribute("p2","filaresaltada");
        request.setAttribute("p3","filaresaltada");
        request.setAttribute("p4","filaresaltada");
        request.setAttribute("p5","filaresaltada");
        request.setAttribute("nombre","NO SE ENCONTRO");
        
        
        /**
         *
         * Inicializacion Variable tipo String
         */
        String next="/jsp/masivo/ordencargue/OrdenCargueValidado.jsp";
        String placa = request.getParameter("placa").toUpperCase();
        String trailer = request.getParameter("trailer").toUpperCase();
        String cedula= "";
        String cedprop ="No se encontro";
        String mensaje="";
        String advertencias="Todos los datos han sido ingresados con exito";
        String cf_code="";
        String unit_transp ="";
        String tipo_cont =request.getParameter("tipo_cont");
        String proveedor = "";
        String destinatario=request.getParameter("destinatarios").equals("a")?"":request.getParameter("destinatarios");
        String ocarga = request.getParameter("orden")!=null?request.getParameter("orden"):"";
        
        /**
         *
         * Inicializacion Variable tipo Float
         */
        
        /**
         *
         * Inicializacion Variable tipo int
         */
        int clasificacion = 5;
        int sw=0;
        
        /**
         *
         * Inicializacion Variable tipo boolean
         */
        boolean tractomula=false;
        boolean traile=false;
        boolean placaVetado=false;
        boolean conductorVetado=false;
        
        
        try{
            
            
           /*
            *
            *Se buscan los datos de la placa
            */
            
            model.placaService.buscaPlaca(placa.toUpperCase());
            if(model.placaService.getPlaca()==null){
                /*Si la placa no existe se envia un error.*/
                sw=1;
                request.setAttribute("placa","filaroja");
                logger.info("ERROR EN LA PLACA....");
            }
            else{
                if(model.placaService.getPlaca()!=null){
                    Placa plac = model.placaService.getPlaca();
                    cedprop = plac.getPropietario();
                    placaVetado=plac.isVetado();
                    proveedor=plac.getProveedor();
                    cedula = request.getParameter("conductor")==null?plac.getConductor():request.getParameter("conductor").equals("")?plac.getConductor():request.getParameter("conductor");
                    request.setAttribute("nombreProp",plac.getNombre());
                    tractomula = plac.isTractomula();
                    
                    /*
                     * Si la clasificacion de la placa es diferente a vacio
                     * se obtiene y se guarda en una variable para luego compararla
                     */
                    if(!plac.getClasificacion().equals(""))
                        clasificacion= Integer.parseInt(plac.getClasificacion());
                    
                    if(cedprop.equals("")){
                        
                        /*Si la cedula del propietario esta en blanco se envia un error.*/
                        sw=1;
                        advertencias = "ERROR: La placa no tiene propietario.";
                        request.setAttribute("placa","filaroja");
                        request.setAttribute("mensaje",advertencias);
                    }
                    if(cedula.equals("")){
                        /*Si la cedula del conductor esta en blanco se envia un error.*/
                        sw=1;
                        request.setAttribute("cedula","filaroja");
                    }
                    
                    
                    Date d = plac.getVencSOAT();
                    Date hoy = new Date();
                    
                    /*
                     * Se compara la fecha del vencimiento del SOAT
                     * con la de hoy para verificar que no este vencido.
                     */
                    
                    if(d.toString().equals("0099-01-01")){
                        /*Si la fecha es igual a la default se envia mensaje de advertencia para que se actualice.*/
                        advertencias="ADVERTENCIA: no se ha ingresado la fecha de vencimiento del seguro obligatorio de la placa!";
                    }else{
                        /*Si la fecha es es menor que hoy se envia mensaje de advertencia porque esta vencido el SOAT.*/
                        if(hoy.after(d)){
                            advertencias="ADVERTENCIA: el seguro obligatorio de la placa esta vencido!";
                        }
                    }
                    if(!plac.isTieneFoto()){
                        advertencias =advertencias+ " La placa no tiene foto registrada en la base de datos";
                    }
                    
                }
                /*
                 *
                 *Se buscan los datos del Trailer
                 */
                if(tractomula){
                    /*Si la placa es tractomula se verifica que se halla ingresado el trailer*/
                    // System.out.println("Es tractomula");
                    if(!trailer.equalsIgnoreCase("NA") && !trailer.equalsIgnoreCase("")){
                        //   System.out.println("El trailer no es na ni vacio "+trailer);
                        
                        /*Se buscan los datos del trailer*/
                        
                        model.placaService.buscaPlaca(trailer);
                        
                        if(model.placaService.getPlaca()!=null){
                            int swTra = 0;
                            Placa vehiculo = model.placaService.getPlaca();
                            
                            if(!vehiculo.getPropietario().equals("890103161")){
                                /*
                                 * El switch se coloca en 1 si el nit de propietario
                                 * del trailer es de Sanchez Polo
                                 */
                                swTra=1;
                            }
                            /*Se verifica que la placa del trailer tenga como tipo de recurso trailer*/
                            if(!vehiculo.isTraile()){
                                /*Si no es asi se envia un mensaje de error.*/
                                sw=1;
                                advertencias="ERROR: La placa que usted ha escrito como trailer no es un trailer.!";
                                request.setAttribute("mensaje", advertencias);
                                request.setAttribute("trailer","filaroja");
                            }else{
                                /*Se verifica que tipo selecciono la persona.*/
                                if(request.getParameter("tipo_tra").equals("FINV")){
                                    /*Si el usuario selecciono TSP y el swicth es 1 se muestra error.*/
                                    if(swTra==1){
                                        logger.info("EL TRAILER NO ES DE TSP");
                                        advertencias="ERROR: El trailer no es de TSP.!";
                                        request.setAttribute("mensaje", advertencias);
                                        sw=1;
                                        request.setAttribute("trailer","filaroja");
                                    }
                                    
                                }else{
                                    /*Si el usuario selecciono TSP y el swicth es 0 se muestra error.*/
                                    if(swTra==0){
                                        sw=1;
                                        request.setAttribute("trailer","filaroja");
                                        advertencias="ADVERTENCIA: El trailer es de TSP usted marco que no. Marque correctamente.!";
                                        request.setAttribute("mensaje", advertencias);
                                    }
                                    
                                }
                                /*Se busca el fitmen en la tabla de fitmen.*/
                                model.fitmenService.buscarFitmen(request.getParameter("trailer").toUpperCase());
                            }
                        }else{
                            /*Si la placa del trailer no existe ne la base de datos se muestra error.*/
                            sw=1;
                            request.setAttribute("trailer","filaroja");
                            request.setAttribute("mensaje", "LA PLACA DEL TRAILER NO EXISTE");
                        }
                    }
                    
                }
                else{
                    trailer = "NA";
                }
                /*
                 * Datos del Contenedor
                 */
                if("FINV".equals(tipo_cont)){
                    /*Si seleccionaron que los contenedores son de TSP*/
                    if(!request.getParameter("c1").equals("")){
                        /*Se verifica que la placa del contenedor 1 existe en la base de datos*/
                        if(!model.placaService.placaExist(request.getParameter("c1"))){
                            /*Si no es asi se envia error*/
                            sw=1;
                            request.setAttribute("c1","filaroja");
                            logger.info("ERROR EN EL DE CONTENEDOR....");
                        }
                        /*Se verifica que la placa del contenedor 2 existe en la base de datos*/
                        if(!model.placaService.placaExist(request.getParameter("c2"))){
                            sw=1;
                            request.setAttribute("c2","filaroja");
                            logger.info("ERROR EN EL CONTENEDOR....");
                        }
                        
                    }
                }
                
            }
            model.conductorService.buscaConductor(cedula);
            if(model.conductorService.getConductor()!=null){
                if(model.conductorService.getConductor()!=null){
                    Conductor conductor = model.conductorService.getConductor();
                    conductorVetado = conductor.isVetado();
                    request.setAttribute("nombre",conductor.getNombre());
                    Date d = conductor.getVecPase();
                    Date hoy = new Date();
                    if("0099-01-01".equals(d.toString())){
                        if(advertencias.equals("")){
                            advertencias="ADVERTENCIA: no se ha ingresado la fecha de vencimiento del pase de este conductor!";
                        }else{
                            advertencias+=" y no se ha ingresado la fecha de vencimiento del pase de este conductor!";
                        }
                    }else{
                        if(hoy.after(d)){
                            if(advertencias.equals("")){
                                advertencias="ADVERTENCIA: la licencia del conductor esta vencida!";
                            }else{
                                advertencias+=" y la licencia del conductor esta vencida!";
                            }
                        }
                    }
                    if(!conductor.isTieneFoto()){
                        advertencias+=" No ha sido ingresada la foto del conductor!";
                    }
                }
                
            }
            else{
                sw=1;
                logger.info("ERROR EN EL CONDUCTOR....");
                request.setAttribute("cedula","filaroja");
                
            }
            //VALIDAMOS PRECINTOS
            
            if(request.getParameter("c1precinto")!=null && !request.getParameter("c1precinto").equals("")){
                boolean esta =false;
                boolean estaocargue =false;
                if(request.getParameter("planilla")!=null){
                    esta=model.precintosSvc.existe(usuario.getId_agencia(),request.getParameter("c1precinto"),"Precinto",request.getParameter("planilla"));
                    
                }
                else{
                    esta=model.precintosSvc.existe(usuario.getId_agencia(),request.getParameter("c1precinto"),"Precinto");
                    estaocargue = model.precintosSvc.existeOCarga(usuario.getId_agencia(), request.getParameter("c1precinto"),usuario.getDstrct(),ocarga);
                }
                if(!esta){
                    request.setAttribute("cp1","filaroja");
                    sw=1;
                }
                if(estaocargue){
                    request.setAttribute("cp1","filaroja");
                    sw=1;
                }
                
            }
            
            if(request.getParameter("c2precinto")!=null && !request.getParameter("c2precinto").equals("")){
                boolean esta =false;
                boolean estaocargue =false;
                if(request.getParameter("planilla")!=null){
                    esta=model.precintosSvc.existe(usuario.getId_agencia(),request.getParameter("c2precinto"),"Precinto",request.getParameter("planilla"));
                }
                else{
                    esta=model.precintosSvc.existe(usuario.getId_agencia(),request.getParameter("c2precinto"),"Precinto");
                    estaocargue = model.precintosSvc.existeOCarga(usuario.getId_agencia(), request.getParameter("c2precinto"),usuario.getDstrct(),ocarga);
                }
                if(!esta){
                    request.setAttribute("cp2","filaroja");
                    sw=1;
                }
                if(estaocargue){
                    request.setAttribute("cp2","filaroja");
                    sw=1;
                }
            }
            
            
            java.util.Enumeration enum1;
            String parametro;
            enum1 = request.getParameterNames();
            while (enum1.hasMoreElements()) {
                parametro = (String) enum1.nextElement();
                if(parametro.indexOf("precintos")>=0 && !request.getParameter(parametro).equals("")){
                    String nombre  = parametro;
                    String valor = request.getParameter(parametro);
                    java.util.Enumeration enum2;
                    enum2 = request.getParameterNames();
                    while (enum1.hasMoreElements()) {
                        parametro = (String) enum1.nextElement();
                        if(parametro.indexOf("precintos")>=0 && !request.getParameter(parametro).equals("")
                        && !nombre.equals(parametro) && request.getParameter(parametro).equals(valor)){
                            String numero = parametro.substring(9);
                            String numero1 = nombre.substring(9);
                            request.setAttribute("p"+numero,"filaroja");
                            request.setAttribute("p"+numero1,"filaroja");
                            sw=1;
                        }
                    }
                    
                    
                }
            }
            if(request.getParameter("c2precinto")!=null && request.getParameter("c1precinto")!=null){
                if(!request.getParameter("c2precinto").equals("") || !request.getParameter("c1precinto").equals("")){
                    if(request.getParameter("c2precinto").equals(request.getParameter("c1precinto"))){
                        sw=1;
                        request.setAttribute("cp2","filaroja");
                        request.setAttribute("cp1","filaroja");
                    }
                }
            }
            
            if(request.getParameter("precintos")!=null && !request.getParameter("precintos").equals("")){
                boolean esta =false;
                boolean estaocargue =false;
                if(request.getParameter("planilla")!=null){
                    esta=model.precintosSvc.existe(usuario.getId_agencia(),request.getParameter("precintos"),"Precinto",request.getParameter("planilla"));
                }
                else{
                    esta=model.precintosSvc.existe(usuario.getId_agencia(),request.getParameter("precintos"),"Precinto");
                    estaocargue = model.precintosSvc.existeOCarga(usuario.getId_agencia(), request.getParameter("precintos"),usuario.getDstrct(),ocarga);
                }
                if(!esta){
                    request.setAttribute("p1","filaroja");
                    sw=1;
                }
                if(estaocargue){
                    request.setAttribute("p1","filaroja");
                    sw=1;
                }
            }
            int k =2;
            while(k<=5){
                if(request.getParameter("precintos"+k)!=null){
                    if(!request.getParameter("precintos"+k).equals("")){
                        boolean esta =false;
                        boolean estaocargue =false;
                        if(request.getParameter("planilla")!=null){
                            esta=model.precintosSvc.existe(usuario.getId_agencia(),request.getParameter("precintos"+k),"Precinto",request.getParameter("planilla"));
                        }
                        else{
                            esta=model.precintosSvc.existe(usuario.getId_agencia(),request.getParameter("precintos"+k),"Precinto");
                            estaocargue = model.precintosSvc.existeOCarga(usuario.getId_agencia(), request.getParameter("precintos"+k),usuario.getDstrct(),ocarga);
                        }
                        if(!esta){
                            request.setAttribute("p"+k,"filaroja");
                            sw=1;
                        }
                        if(estaocargue){
                            request.setAttribute("p"+k,"filaroja");
                            sw=1;
                        }
                    }
                }
                k++;
            }
            
            
            
            
            //VERIFICO SI EL CONDUCTOR ESTA VETADO
            if(conductorVetado){
                sw=1;
                request.setAttribute("cedula","filaroja");
                request.setAttribute("mensaje","El conductor esta reportado en el sistema!");
            }
            
            //VERIFICO SI LA PLACA ESTA VETADO
            if(placaVetado){
                sw=1;
                request.setAttribute("placa","filaroja");
                request.setAttribute("mensaje","El propietario o la placa esta reportada en el sistema!");
            }
            
            request.setAttribute("ad",advertencias);
            
            next="/jsp/masivo/ordencargue/OrdenCargueValidado.jsp?tipo_cont="+tipo_cont+"&trailer="+trailer+"&conductor="+cedula;
            if(request.getParameter("modif")!=null){
                next="/jsp/masivo/ordencargue/ModificarValidado.jsp?tipo_cont="+tipo_cont+"&trailer="+trailer+"&conductor="+cedula;
            }
            String contenedores= request.getParameter("c1");
            contenedores = !contenedores.equals("")?contenedores+",":contenedores;
            contenedores = contenedores+ request.getParameter("c2");
            HojaOrdenDeCarga hojaOrden = new HojaOrdenDeCarga();
            hojaOrden.setDstrct( usuario.getDstrct() );
            hojaOrden.setCodciu(usuario.getId_agencia());//AGENCIA
            hojaOrden.setRemitente(request.getParameter("remitentes"));
            hojaOrden.setConductor(cedula);
            hojaOrden.setPlaca(placa);
            hojaOrden.setTrailer(trailer);
            hojaOrden.setContenido( request.getParameter("contenido") );
            hojaOrden.setStd_job_no( request.getParameter("standard"));
            hojaOrden.setEntregar( request.getParameter("entregar") );
            hojaOrden.setObservacion(request.getParameter("observacion"));
            hojaOrden.setPrecinto1( request.getParameter("precintos")==null?"":request.getParameter("precintos")  );
            hojaOrden.setPrecinto2(  request.getParameter("precintos2")==null?"":request.getParameter("precintos2")  );
            hojaOrden.setPrecinto3(  request.getParameter("precintos3")==null?"":request.getParameter("precintos3") );
            hojaOrden.setPrecinto4(  request.getParameter("precintos4")==null?"":request.getParameter("precintos4") );
            hojaOrden.setPrecinto5(  request.getParameter("precintos5")==null?"":request.getParameter("precintos5") );
            hojaOrden.setCreation_user(usuario.getLogin());
            hojaOrden.setBase( usuario.getBase() );
            hojaOrden.setPrecintoc1( request.getParameter("c1precinto")==null?"":request.getParameter("c1precinto") );
            hojaOrden.setPrecintoc2(request.getParameter("c2precinto")==null?"":request.getParameter("c2precinto"));
            hojaOrden.setContenedores( contenedores );
            hojaOrden.setTipocont(request.getParameter("tipo_cont").substring(0,3) );
            hojaOrden.setTipotrailer(request.getParameter("tipo_tra"));
            hojaOrden.setFecha_cargue(request.getParameter("fechadesp"));
            hojaOrden.setDestinatarios(destinatario);
            hojaOrden.setNombre(request.getParameter("nombre")!=null?request.getParameter("nombre").toUpperCase():"");
            hojaOrden.setTelefono(request.getParameter("telefono")!=null?request.getParameter("telefono").toUpperCase():"");
            
            if(sw==1){
                next="/jsp/masivo/ordencargue/OrdenCargueError.jsp?tipo_cont="+tipo_cont+"&conductor="+cedula;
                if(request.getParameter("modif")!=null){
                    next="/jsp/masivo/ordencargue/ModificarError.jsp?tipo_cont="+tipo_cont+"&conductor="+cedula;
                }
            }
            else if(sw!=1 && request.getParameter("insert")!=null){
                String orden = "";
                model.seriesService.obtenerSerie( "OCA", usuario);
                logger.info("ENCONTRE LA SERIE DE REMESAS");
                if(model.seriesService.getSerie()!=null){
                    Series serie = model.seriesService.getSerie();
                    model.seriesService.asignarSeries(serie,usuario);
                    orden=serie.getPrefix()+serie.getLast_number();
                    
                }
                if(!orden.equals("")){
                    Vector consultas=new Vector();
                    hojaOrden.setOrden(orden);
                    Vector doc = model.RemDocSvc.getDocumentos();
                    if( doc != null ){
                        logger.info("Si hay documentos");
                        String insertDocRel=model.RemDocSvc.insertOCargue_docto(doc, usuario.getDstrct(), orden, destinatario, usuario.getLogin());
                        String  inserts2[]=insertDocRel.split(";");
                        for(int j = 0; j<inserts2.length;j++){
                            consultas.add(inserts2[j]);
                            logger.info(inserts2[j]);
                        }
                    }else{
                        logger.info("No hay documentos");
                    }
                    model.despachoService.insertar(consultas);
                    model.imprimirOrdenService.setHoc(hojaOrden);
                    model.imprimirOrdenService.insert();
                    
                    next ="/jsp/masivo/ordencargue/preliminar.jsp?orden="+orden;
                }
            }
            else if(sw!=1 && request.getParameter("modificar")!=null){
                
                String orden = request.getParameter("orden");
                hojaOrden.setOrden(orden);
                model.imprimirOrdenService.setHoc(hojaOrden);
                model.imprimirOrdenService.update();
                next ="/jsp/masivo/ordencargue/InicioModificar.jsp?mensaje=La orden "+orden+" ha sido modificada con exito";
                //  model.imprimirOrdenService.insert();
                
            }
            logger.info("El next= "+next);
            
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
