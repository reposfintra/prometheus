/*
 * ChequeReimpresionAction.java
 *
 * Created on 26 de febrero de 2007, 05:18 PM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  Osvaldo P�rez Ferrer
 */

import java.io.*;
import java.util.*;
import com.tsp.exceptions.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;

public class ChequeReimpresionAction extends Action{
    
    /** Creates a new instance of ChequeReimpresionAction */
    public ChequeReimpresionAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        
        try{
            
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            
            
            String   next          = "/jsp/cxpagar/impresion/REImpresionCheque.jsp";
            String   msj           = "";
            String   evento        = request.getParameter("evento");
            
            if( evento != null ){
                
                model.ChequeXFacturaSvc.setUsuario( usuario );
                
                if( evento.equals("BANCOS") ){
                    
                    boolean hayTasa = model.tasaService.isTasaHoy( usuario.getDstrct() );
                    
                    if( hayTasa ){
                        model.ChequeXFacturaSvc.resetCheque();
                        model.ChequeXFacturaSvc.setVector( new Vector() );
                        model.ChequeFactXCorridasSvc.loadBancos( usuario.getDstrct(), usuario.getId_agencia() );
                        model.ChequeFactXCorridasSvc.loadSucursales(usuario.getDstrct(), usuario.getId_agencia() );
                    }else
                        next  = "/jsp/cxpagar/Mensaje.jsp?msj=" + model.tasaService.getMsj();                    
                    
                }else if( evento.equals("LISTAR") ){
                    
                    model.ChequeXFacturaSvc.resetCheque();
                    
                    String banco    = request.getParameter("banco");
                    String sucursal = request.getParameter("sucursal");
                    String ini      = request.getParameter("ini");
                    String fin      = request.getParameter("fin");
                    
                    model.ChequeXFacturaSvc.obtenerChequesReimprimibles( usuario.getDstrct(), banco, sucursal, ini, fin ) ;
                    
                    msj = "?msj=search";
                }else if( evento.equals("REIMPRIMIR") ){
                    
                    String ids = request.getParameter("ids");
                    
                    String banco    = request.getParameter("banco");
                    String sucursal = request.getParameter("sucursal");
                    model.ChequeXFacturaSvc.formarChequesReimprimibles( ids.split(","), banco, sucursal );
                    
                    model.ChequeXFacturaSvc.loadNextCheque();
                    
                    next = "/jsp/cxpagar/impresion/viewChequeXFacturaReimpresion.jsp";
                    
                }else if( evento.equals("NEXT") ){
                    
                    Cheque cheque    = model.ChequeXFacturaSvc.getCheque();
                    msj = "?msj=El cheque n�mero "+ cheque.getNumero() + " del banco " +  cheque.getBanco() +" "+ cheque.getSucursal() +" por la suma de "+  com.tsp.util.Util.customFormat( cheque.getVlrPagar() )  + " " + cheque.getMoneda() +" a nombre de  "+   cheque.getNombre()  +", ha sido Reimpreso...";
                    
                    model.ChequeXFacturaSvc.marcarReimpreso( cheque );
                    model.ChequeXFacturaSvc.setVector( new Vector() );
                    
                    model.ChequeXFacturaSvc.loadNextCheque();
                    if( model.ChequeXFacturaSvc.getCheque() != null ){
                        next = "/jsp/cxpagar/impresion/viewChequeXFacturaReimpresion.jsp";
                    }
                    
                }else if( evento.equals("PRINT") ){
                    
                    String laser     = request.getParameter("laser")!=null? request.getParameter("laser") : "" ;
                    next = "/jsp/cxpagar/impresion/ChequeCMS.jsp?reimpresion=&laser="+laser;
                    
                }
            }
            
            next += msj;
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new Exception("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }catch (Exception ex){
            throw new ServletException(ex.getMessage());
        }
        
    }
    
}
