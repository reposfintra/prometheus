/*
 * BuscarNovedadAction.java
 *
 * Created on 13 de junio de 2005, 10:00 AM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  Henry
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;

public class BuscarNovedadAction extends Action {
    
    /** Creates a new instance of BuscarNovedadAction */
    public BuscarNovedadAction() {
    }
    
    public void run() throws ServletException {
        String next="/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina");        
        try{ 
            /*model.idiomaService.cargarIdioma(lenguaje,request.getParameter("pagina"));
            Properties pro = model.idiomaService.getIdioma();*/            
            model.novedadService.buscarNovedad(request.getParameter("codigo") ); 
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
         
         // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);

    }
    
}
