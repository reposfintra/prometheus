/********************************************************************
 *      Nombre Clase.................   TraficoValidarViaAction.java
 *      Descripci�n..................   Action que se encarga de validar si a una planilla se le puede hacer cambio de via
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import javax.servlet.*;
import javax.servlet.http.*;


/**
 *
 * @author  dlamadrid
 */
public class TraficoValidarViaAction extends Action {
    
    /** Creates a new instance of TraficoValidarViaAction */
    public TraficoValidarViaAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next="";
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String user=""+usuario.getLogin ();
        ////System.out.println ("Usuario:"+user);
        String planilla = request.getParameter ("planilla");
        String fechaS = request.getParameter ("fechaS");
        String ms="";
        boolean sw=false;
        try{
            String via = model.traficoService.obtenerVia (planilla);
            String ppc = model.traficoService.obtenerPPC (planilla);
            String secuencia= model.traficoService.obtenerSecuencia (via);
            if(secuencia!=null){
                sw= model.traficoService.validarVia (ppc, secuencia);
                if (sw==false){
                    ms="No se puede cambiar la Fecha de Salida ";
                    next= "/jsp/trafico/controltrafico/errorsalida.jsp?marco=no&ms="+ms;
                }
                else{
                    next= "/jsp/trafico/atraso/insertarSalida.jsp?planilla="+planilla+"&fechaS="+fechaS;
                    model.tblgensvc.searchDatosTablaCausasDemora ();
                }
            }
            else{
                ms="La via no se encuentra registrada en la Base de Datos";
                next= "/jsp/trafico/controltrafico/errorsalida.jsp?marco=no&ms="+ms;
            }
        }
        catch (SQLException e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
    
}
