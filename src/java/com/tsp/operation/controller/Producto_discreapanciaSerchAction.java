/*
 * Producto_discreapanciaSerchAction.java
 *
 * Created on 17 de octubre de 2005, 04:01 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  Jose
 */
public class Producto_discreapanciaSerchAction extends Action{
    
    /** Creates a new instance of Producto_discreapanciaSerchAction */
    public Producto_discreapanciaSerchAction() {
    }
    
    public void run() throws ServletException, InformationException {
    
        String next="";
        HttpSession session = request.getSession();   
        int num_discre = Integer.parseInt(request.getParameter("c_num_discre"));
        String numpla = request.getParameter("c_numpla");
        String numrem = request.getParameter("c_numrem");
        String tipo_doc = request.getParameter("c_tipo_doc");
        String documento = request.getParameter("c_documento");
        String tipo_doc_rel = (String) request.getParameter ("c_tipo_doc_rel");
        String documento_rel = (String) request.getParameter ("c_documento_rel");        
        String cod_producto = request.getParameter("c_cod_producto").toUpperCase();
        String cod_discrepancia = request.getParameter("c_cod_discrepancia");
        String distrito = (String) session.getAttribute ("Distrito");
        String cliente = request.getParameter("client");
        try{ 
             model.productoService.serchProducto(cod_producto,distrito, cliente);
             Producto pro = model.productoService.getProducto();
            if( pro != null && pro.getCliente ().equals (cliente) ){
                next="/jsp/cumplidos/discrepancia/DiscrepanciaInsertar.jsp?c_numrem="+numrem+"&sw=true&exis=true&campos=true&c_numpla"+numpla+"&c_tipo_doc="+tipo_doc+"&c_documento="+documento+"&paso=true&c_tipo_doc_rel="+tipo_doc_rel+"&c_documento_rel="+documento_rel+"&client"+cliente;
            }
            else{
                request.setAttribute("msg","No Existe El Producto");
                next="/jsp/cumplidos/discrepancia/DiscrepanciaInsertar.jsp?c_numrem="+numrem+"&sw=true&exis=true&campos=true&c_numpla"+numpla+"&c_tipo_doc="+tipo_doc+"&c_documento="+documento+"&paso=false&c_tipo_doc_rel="+tipo_doc_rel+"&c_documento_rel="+documento_rel+"&client"+cliente;
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);    
    }
    
}
