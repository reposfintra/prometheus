/*
 * FormularioInsertarAction.java
 *
 * Created on 26 de noviembre de 2006, 05:48 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
/**
 *
 * @author  dbastidas
 */
public class FormularioInsertarAction extends Action {
    
    /** Creates a new instance of FormularioInsertarAction */
    public FormularioInsertarAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        HttpSession session = request.getSession();
        String next="/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina");
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String formato  = request.getParameter("formato");
        String campo_val = "";
        String tabla = "";
        int i=0;
        boolean sw = true;
        
        Vector campos = model.formato_tablaService.getVecCampos();
        for( i=0; i < campos.size() ; i++){
            Formato_tabla reg = (Formato_tabla) campos.get(i);
            tabla = reg.getTabla();
            if( reg.getTipo_campo().equals("DO") || reg.getTipo_campo().equals("INT") ){
                reg.setValor_campo( request.getParameter(reg.getCampo_tabla())!=null?request.getParameter(reg.getCampo_tabla()).toUpperCase():"0" );
            }else if(reg.getTipo_campo().equals("HOUR")){
                reg.setValor_campo( request.getParameter(reg.getCampo_tabla())!=null?request.getParameter(reg.getCampo_tabla()).substring(0,10):"0" );
            }
            else{
                reg.setValor_campo( request.getParameter(reg.getCampo_tabla())!=null?request.getParameter(reg.getCampo_tabla()).toUpperCase():"" );
            }
        }
        try{
            System.out.println("cantidad "+campos.size());
            //agrego los campos de para factura migracion
            if(tabla.equals("factura_migracion")){
                Formato_tabla reg = new Formato_tabla();
                reg.setTabla(tabla);
                reg.setCampo_tabla("formato");
                reg.setTipo_campo("TXT");
                reg.setValor_campo(formato);
                reg.setPrimaria("S");
                reg.setValidar("N");
                campos.add(reg);
                //district
                reg = new Formato_tabla();
                reg.setTabla(tabla);
                reg.setCampo_tabla("dstrct");
                reg.setTipo_campo("TXT");
                reg.setValor_campo(usuario.getDstrct());
                reg.setPrimaria("S");
                reg.setValidar("N");
                campos.add(reg);
                //usur creation
                reg = new Formato_tabla();
                reg.setTabla(tabla);
                reg.setCampo_tabla("creation_user");
                reg.setTipo_campo("TXT");
                reg.setValor_campo(usuario.getLogin());
                reg.setPrimaria("N");
                reg.setValidar("N");
                campos.add(reg);
                //fecha creacion
                reg = new Formato_tabla();
                reg.setTabla(tabla);
                reg.setCampo_tabla("creation_date");
                reg.setTipo_campo("TXT");
                reg.setValor_campo(Util.getFechaActual_String(6));
                reg.setPrimaria("N");
                reg.setValidar("N");
                campos.add(reg);
            }
            try{
                System.out.println("Agrega los campos de control, Nueva cantidad "+campos.size());
                //creo los metodos de validacion
                model.formularioService.crearMetodosValidacion(campos);
                Vector validacion =  model.formularioService.getValidacion();
                //realizo la verificacion de en la BD
                for( i=0; i< validacion.size() ; i++){
                    Hashtable reg_val = (Hashtable) validacion.get(i);
                    if( !model.formularioService.validarCampo( (String) reg_val.get("SQL")) ){
                        campo_val = (String) reg_val.get("Titulo");
                        break;
                    }
                }
            }catch (Exception e){
                next="/jsp/general/form_tabla/Mensaje.jsp?formato="+formato+"&tabla="+tabla +
                "&mensaje=El formato no esta bien creado, por favor verifiquelo";
                sw=false;
                this.dispatchRequest(next);
            }
            
            if(!campo_val.equals("")){
                next+="?mensaje=No existe "+campo_val;
            }
            else{
                //verifico si existe el registro en la BD
                if( !model.formularioService.existeRegistro(model.formularioService.getDatos(), model.formularioService.getTabla(), model.formularioService.getLlaves())){
                    try{
                        model.formularioService.insertar(campos);
                        //Limpio el fromulario para una nueva captura
                        for( i=0; i < campos.size() ; i++){
                            Formato_tabla reg = (Formato_tabla) campos.get(i);
                            reg.setValor_campo("");
                        }
                        next+="?mensaje=La informacion ha sido almacenada exitosamente";
                    }catch (Exception e){
                        next="/jsp/general/form_tabla/Mensaje.jsp?formato="+formato+"&tabla="+tabla +
                        "&mensaje=El formato no esta bien creado, por favor verifiquelo";
                        sw=false;
                        this.dispatchRequest(next);
                    }
                }
                else{
                    next+="?mensaje=Ya esta la informacion almacenada ";
                }
            }
            if(tabla.equals("factura_migracion")){
                model.formato_tablaService.setVecCampos(model.formularioService.eliminarCamposFacturaMigracion(campos));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        if(sw){
            this.dispatchRequest(next);
        }
    }
    
}
