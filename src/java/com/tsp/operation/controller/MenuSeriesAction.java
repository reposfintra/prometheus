/*
 * MenuSeriesAction.java
 *
 * Created on 1 de diciembre de 2004, 03:25 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import org.apache.log4j.*;
import java.util.TreeMap;
/**
 *
 * @author  mcelin
 */
public class MenuSeriesAction extends Action {
    
    Logger logger = Logger.getLogger(this.getClass());
    
    /** Creates a new instance of MenuSeriesAction */
    public MenuSeriesAction() {
    }
    
    public void run() throws ServletException, InformationException { 
        try {  
            
            String Pagina  = request.getParameter("Pagina");
            String Mensaje = "";
            if(Pagina.equals("series/series")){
                model.SeriesSvc.Reinicial();
                model.SeriesSvc.Buscar_Ciudades_Js();
                model.SeriesSvc.Buscar_Documento_Js();            
                model.SeriesSvc.Buscar_Banco_Js();
                model.SeriesSvc.Buscar_Agencia_Js();
                model.SeriesSvc.Buscar_Cuenta_Js();
                //AMATURANA 13.04.2007
                TreeMap conceptos = model.tablaGenService.loadTableType("CONCHEQUES");
                model.SeriesSvc.setConceptos(conceptos);
                model.SeriesSvc.loadUsuariosJs();
            }
            else{
                model.SeriesSvc.Buscar_Ciudades();
                model.SeriesSvc.Buscar_Documento();
            }
            model.SeriesSvc.Reinicial();
            String next = "/"+Pagina+".jsp?Mensaje="+Mensaje;
            next = Util.LLamarVentana(next, "Serie");
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
        }
        catch(Exception e) {
            throw new ServletException(e.getMessage());
        }
     }

    
}
