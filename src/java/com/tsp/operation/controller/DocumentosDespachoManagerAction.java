/*
 *  Nombre clase    :  DocumentosDespachoManagerAction.java
 *  Descripcion     :
 *  Autor           : Ing. Juan Manuel Escand�n P�rez
 *  Fecha           : 4 de octubre de 2006, 08:52 AM
 *  Version         : 1.0
 *  Copyright       : Fintravalores S.A.
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class DocumentosDespachoManagerAction extends Action{
    
    /** Crea una nueva instancia de  DocumentosDespachoManagerAction */
    public DocumentosDespachoManagerAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        try{
            
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            
            
            String nombre = request.getParameter("nombre");
            String numrem = request.getParameter("numrem");
            
            
            String Mensaje = "";
            
            Vector original = new Vector();
            
            Vector vec = new Vector();
            
            
            int cant = 0;
            String destinatario = request.getParameter("destinatario");
            
            
            java.util.Enumeration enum1;
            String parametro;
            enum1 = request.getParameterNames(); // Leemos todos los atributos del request
            while (enum1.hasMoreElements()) {
                parametro = (String) enum1.nextElement();                                
                
                if(parametro.indexOf("_")<0){
                    if(parametro.substring(0,2).equals("dp")){
                        if(!request.getParameter(parametro).equals("")){
                            int numero =Integer.parseInt( parametro.substring(2));
                            //SE ENCONTRARON LOS PADRES, SE CREAUN OBJETO DE TIPO REMESA DOC QUE OBTIENE LOS DATOS DEL PADRE                            
                            
                            remesa_docto rem = new remesa_docto();
                            rem.setDestinatario(destinatario);
                            rem.setTipo_doc(request.getParameter("tp"+numero));
                            rem.setDocumento(request.getParameter(parametro));
                            
                            Vector hijos = new Vector();
                            // RECORRO NUEVAMENTE LOS PARAMETROS PARA OBTENER LOS HIJOS DE ESTE OBJETO
                            java.util.Enumeration enum2;
                            String parametro2;
                            enum2 = request.getParameterNames(); // Leemos todos los atributos del request
                            while (enum2.hasMoreElements()) {
                                parametro2 = (String) enum2.nextElement();
                                if(parametro2.indexOf("tp"+numero+"_TipodocRel")>=0){
                                    String vecS [] = parametro2.split("_");
                                    String numeroH = vecS[1].substring(10);
                                    String hijo2 = "tp"+numero+"_DocumentoRel"+numeroH;
                                    remesa_docto remH = new remesa_docto();
                                    remH.setTipo_doc(request.getParameter(parametro2));
                                    remH.setDocumento(request.getParameter(hijo2));
                                    hijos.add(remH);
                                    
                                }
                            }
                            rem.setHijos(hijos);
                            vec.add(rem);
                            cant++;
                        }
                    }
                    
                }
                
            }
            
            
            
            if(vec.size()<5){
                for(int i=vec.size(); i<5; i++){
                    remesa_docto rem = new remesa_docto();
                    rem.setDestinatario(request.getParameter("destinatario"));
                    rem.setTipo_doc("");
                    rem.setDocumento("");
                    Vector hijos = new Vector();
                    remesa_docto remH = new remesa_docto();
                    remH.setTipo_doc("");
                    remH.setDocumento("");
                    rem.setHijos(hijos);
                    vec.add(rem);
                }
                
                // this.dispatchRequest(next);
            }
            model.RemDocSvc.setDocumentos(vec);
            
            
            for( int i = 0; i < vec.size(); i++ ){
                
                remesa_docto r = (remesa_docto)vec.elementAt(i);
                
                for( int j = 0; j < r.getHijos().size(); j++ ){
                    remesa_docto r2 = (remesa_docto)r.getHijos().elementAt(j);
                }
            }
            
            model.RemDocSvc.DELETEREL(numrem, request.getParameter("destinatario"));
            
            model.RemDocSvc.INSERTDOCREL(vec, usuario.getDstrct(), numrem, request.getParameter("destinatario"),usuario.getLogin());
                       
            model.RemDocSvc.ListDocumentosDestinatarios(numrem, destinatario);
            
            Mensaje = "Registros insertados y/o modificados Exitosamente";
            
            String next="/jsp/masivo/Documentos_Despacho/ModificarDocRel.jsp?nombre="+nombre+"&Mensaje="+Mensaje;
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en ReporteEgresoAction .....\n"+e.getMessage());
        }
        
    }
    
}
