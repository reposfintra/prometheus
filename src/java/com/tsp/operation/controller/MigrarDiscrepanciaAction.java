/*
 * MigrarDiscrepanciaAction.java
 *
 * Created on 14 de octubre de 2005, 03:21 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import com.tsp.operation.model.threads.*;

/**
 *
 * @author  dbastidas
 */
public class MigrarDiscrepanciaAction extends Action {
    
    /** Creates a new instance of MigrarDiscrepanciaAction */
    public MigrarDiscrepanciaAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next="";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        try{
            ReporteMigracionDiscrepancia reporteMig = new ReporteMigracionDiscrepancia();
            reporteMig.start(usuario.getLogin());
            next = "/migracion/migracionDiscrepancia.jsp?msg=Su reporte ha iniciado y se encuentra en el log de procesos";
           // next = Util.LLamarVentana(next, "Migrar discrepancia");
            
            
        }catch (Exception ex){
            throw new ServletException("Error en OpcionesExportarPtoAction .....\n"+ex.getMessage());
        }

        this.dispatchRequest(next);  

    }
    
}
