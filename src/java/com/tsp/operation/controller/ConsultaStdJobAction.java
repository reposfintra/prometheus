/*
 * ConsultaStdJobAction.java
 *
 * Created on 4 de enero de 2005, 11:38 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class ConsultaStdJobAction extends Action {
    
    /** Creates a new instance of ConsultaStdJobAction */
    public ConsultaStdJobAction() { }
    
    public void run() throws ServletException, InformationException {
        
        String next="/consultas/consultasRedirect.jsp?stdjob=ok";
        
        String sj = ( request.getParameter("sj") != null ) ? request.getParameter("sj").toUpperCase() : "";
        String cliente = ( request.getParameter("cliente") != null ) ? request.getParameter("cliente").toUpperCase() : "";
        String esta = ( request.getParameter("esta") != null ) ? request.getParameter("esta").toUpperCase() : "";
        String tipo = ( request.getParameter("tipo") != null ) ? request.getParameter("tipo").toUpperCase() : "";
        
        try{
            
            if (model.stdjobdetselService.getStdjob() == null ) {
                
                model.stdjobdetselService.consultaStandardDetSel( sj, cliente, esta, tipo );
                
            } else {
                
                model.stdjobdetselService.datosStandard( sj );
                
                if( model.stdjobdetselService.getStandardDetSel() != null ) {
                    
                    request.setAttribute( "standar", model.stdjobdetselService.getStandardDetSel() );
                    request.setAttribute( "costos", model.stdjobdetselService.getCostos() );
                    
                    next="/consultas/datosStdjob.jsp";
                    
                }
                
            }
            
        } catch ( SQLException e ) {
            
            e.printStackTrace();
            throw new ServletException( e.getMessage() );
            
        }
        
        this.dispatchRequest( next );
        
    }
     
}