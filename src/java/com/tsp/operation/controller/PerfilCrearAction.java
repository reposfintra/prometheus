/*
 * Nombre        PerfilCrearAction.java
 * Autor         Ing. Luigi frieri.
 * Fecha         07 febrero 2007
 * Versión       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Administrador
 */
public class PerfilCrearAction extends Action {
    
    /** Creates a new instance of CrearPerfilAction */
    public PerfilCrearAction() {
    }
        
    public void run() throws javax.servlet.ServletException, InformationException{                  
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");        
        String idp = (request.getParameter("idp")!=null)?request.getParameter("idp").toUpperCase():"";        
        String nom = (request.getParameter("nom")!=null)?request.getParameter("nom").toUpperCase():"";  
        String opcion = (request.getParameter("opcion")!=null)?request.getParameter("opcion"):"";
        String next= "";
        
        try{          
            //
             if(opcion.equals("")){
                model.PerfilesGenService.buscarPerfiles();
                next="/jsp/trafico/perfil/CrearPerfil.jsp";
             }else{
                   if (model.perfilService.existePerfilxPropietario(idp)){
                      next="/jsp/trafico/perfil/CrearPerfil.jsp?msg=Error al insertar perfil, " + request.getParameter("idp") + " ya existe!";
                   }else{
                        
                        Perfil prf = new Perfil();
                        prf.setId(idp);
                        prf.setNombre(nom);
                        prf.setUser_update(usuario.getLogin());            
                        prf.setCreado_por(usuario.getLogin());
                        
                        if(model.perfilService.existePerfilxPropietarioAnulado(idp)){
                           
                            model.perfilService.activarPerfil(prf);
                            String []Perfiles  = request.getParameterValues("Soportes");
                            
                            Vector vpo = new Vector();
                            
                            for ( int i = 0; i<Perfiles.length; i++ ){
                                
                                model.perfilService.listarPerfilOpcion(Perfiles[i]);
                                vpo = model.perfilService.getVPerfil();
                                PerfilOpcion temp = new PerfilOpcion();
                                for ( int j = 0; j<vpo.size(); j++ ){
                                    temp = (PerfilOpcion) vpo.elementAt(j);
                                    if(!model.perfilService.estaPerfilOpcion(idp,temp.getId_opcion())){
                                       model.perfilService.agregarPerfilOpcion(idp, temp.getId_opcion(), usuario.getLogin());                                        
                                    }else{
                                        model.perfilService.activarPerfilOpcion(usuario.getLogin(), idp, temp.getId_opcion());
                                    }
                                    
                                    
                                }
                                
                            }
                            
                            
                        }else{
                            model.perfilService.agregarPerfil(prf);
                            String []Perfiles  = request.getParameterValues("Soportes");
                            
                            Vector vpo = new Vector();
                            
                            for ( int i = 0; i<Perfiles.length; i++ ){
                                
                                model.perfilService.listarPerfilOpcion(Perfiles[i]);
                                vpo = model.perfilService.getVPerfil();
                                PerfilOpcion temp = new PerfilOpcion();
                                for ( int j = 0; j<vpo.size(); j++ ){
                                    temp = (PerfilOpcion) vpo.elementAt(j);                                 
                                    model.perfilService.agregarPerfilOpcion(idp, temp.getId_opcion(), usuario.getLogin());                                                                                                            
                                }
                                
                            }
                            
                        }

                        next="/jsp/trafico/perfil/CrearPerfil.jsp?msg=El perfil se creo exitosamenteˇˇ";
                   }
             }
        }catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);                   
    }
}