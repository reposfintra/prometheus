/*
 * PeajesUpdateAction.java
 *
 * Created on 6 de diciembre de 2004, 08:46 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import com.tsp.exceptions.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  KREALES
 */
public class PeajesUpdateAction extends Action{
    
    /** Creates a new instance of PeajesUpdateAction */
    public PeajesUpdateAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String Mensaje = "El registro ha sido modificado";
        String next = "/peajes/peajeUpdate.jsp?Mensaje="+Mensaje;
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String codigo = request.getParameter("tiket");
        
        try{
            model.peajeService.buscar(codigo);
            if(model.peajeService.get()!=null){
                Peajes p= model.peajeService.get();
                p.setDecripcion(request.getParameter("descripcion"));
                p.setMoneda(request.getParameter("moneda"));
                p.setUser(usuario.getLogin());
                p.setValue(Float.parseFloat(request.getParameter("valor")));
                p.setDstrct(request.getParameter("distrito"));
                model.peajeService.modificar(p);
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
    
}
