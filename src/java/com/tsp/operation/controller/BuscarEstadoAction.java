/*
 * BuscarEstadoAction.java
 *
 * Created on 3 de marzo de 2005, 06:35 PM
 */

package com.tsp.operation.controller;

/**
 *
 * @author  DIBASMO
 */

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

public class BuscarEstadoAction extends Action  {
    
    /** Creates a new instance of BuscarEstadoAction */
    public BuscarEstadoAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException{
        String next="/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina");
        HttpSession session = request.getSession();
        try{ 
            model.paisservice.buscarpais(request.getParameter("codpais") );
            model.estadoservice.buscarestado(request.getParameter("codpais"), request.getParameter("codigo") ); 
            next=next+"?sw=1";
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
         
         // Redireccionar a la p�gina indicada.
        this.dispatchRequest(Util.LLamarVentana(next,"Modificar Estado"));
    }
    
}
