/*****************************************************************************
 * Nombre clase :                   ImpresionPlanillasPDFAction.java         *
 * Descripcion :                    Clase que maneja los eventos             *
 *                                  relacionados con el programa de          *
 *                                  Impresion de Planillas                   *
 * Autor :                          Ing. Juan Manuel Escandon Perez          *
 * Fecha :                          1 de septiembre de 2005, 06:29 PM        *
 * Version :                        1.0                                      *
 * Copyright :                      Fintravalores S.A.                  *
 *****************************************************************************/
package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.pdf.*;
import java.util.*;
import com.tsp.operation.model.threads.*;

public class ImpresionPlanillasPDFAction extends Action{
    
    public ImpresionPlanillasPDFAction() {
    }
    
    public void run() throws ServletException, InformationException {
        try{
            ////System.out.println("ENTRO ACTION");
            HttpSession session = request.getSession();
            Usuario     usuario = (Usuario) session.getAttribute("Usuario");
            DatosPlanillaImpPDF datos;
            DatosRemesaImpPDF   remaux;
            
            String Distrito = usuario.getDstrct();
            String Numpla = (request.getParameter("numpla")!=null)?request.getParameter("numpla").toUpperCase():"";
            String opcion = (request.getParameter("opcion")!=null)?request.getParameter("opcion"):"";
            String Mensaje = "";
            String next = "";
            List listaPlanillas = new LinkedList();
            List listaaux = new LinkedList();
            String []LOV  = request.getParameterValues("Impresa");
            int flag      = 0;
            
            List listaPlanviaje = new LinkedList();
            
            //////System.out.println("OPCION************************ " + opcion);
            if(opcion.equals("multiple")){
                listaaux = (List)session.getAttribute("datosRemesa");
                
                Iterator it2 = listaaux.iterator();
                while(it2.hasNext()){
                    remaux = (DatosRemesaImpPDF)it2.next();
                    
                    Numpla = remaux.getNumpla()!=null?remaux.getNumpla():"";
                    
                    if( !Numpla.equals("")){
                        
                        model.PlanillaImpresionSvc.DatosCIA(Distrito);
                        model.PlanillaImpresionSvc.DatosPlanilla(Numpla);
                        
                        model.PlanillaImpresionSvc.ActualizarFechaImpresion(Numpla);
                        
                        DatosPlanillaImpPDF dato = model.PlanillaImpresionSvc.getDatos();
                        
                        if( dato.getPlaviaje().equals("S") ){
                            
                            DatosPlanillaImpPDF planillaDato = this.Cargar(dato);
                            
                            listaPlanillas.add(planillaDato);
                            
                        }
                        
                        else{
                            //Codigo de log
                            listaPlanviaje.add(dato);
                        }
                    }
                    
                }
                
                
                
                /* CREACION DE ARCHIVOS XSLT(ARBOL) Y PDF */
                if(  listaPlanillas.size() > 0  ){
                    String ruta = application.getRealPath("/");
                    File xslt = new File(ruta + "Templates/p2.xsl");
                    File pdf = new File(ruta + "pdf/p2.pdf");
                    
                    PlanillaPDF planilla_pdf = new PlanillaPDF();
                    planilla_pdf.generarPDF(xslt, pdf,listaPlanillas);
                    
                    next = "/pdf/p2.pdf";
                }
                else{
                    model.RemesaSvc.buscaRemesa_no_impresa("",usuario.getLogin(),2);
                    next = "/impresiones_pdf/remesanoimpPDF.jsp";
                }
                
                if( listaPlanviaje.size() > 0 ){
                    LogPlanillas log = new LogPlanillas();
                    log.start( usuario.getLogin(), listaPlanviaje );
                }
                
                
            }//End if ( multiple )
            
            if( opcion.equals("conremesas") ){
                
                for( int i = 0 ; i < LOV.length ; i++ ){
                    Numpla = LOV[i];
                    model.PlanillaImpresionSvc.DatosCIA(Distrito);
                    model.PlanillaImpresionSvc.DatosPlanilla(Numpla);
                    
                    model.PlanillaImpresionSvc.ActualizarFechaImpresion(Numpla);
                    DatosPlanillaImpPDF dato = model.PlanillaImpresionSvc.getDatos();
                    
                    dato = this.Cargar(dato);
                    
                    listaPlanillas.add(dato);
                    
                }
                next = "/impresiones_pdf/PDFJoinPlanilla.jsp";
                
                String ruta = application.getRealPath("/");
                File xslt = new File(ruta + "Templates/p2.xsl");
                File pdf = new File(ruta + "pdf/p2.pdf");
                
                session.setAttribute("ArchivoXSLTPlanilla", xslt );
                session.setAttribute("ArchivoPDFPlanilla", pdf );
                session.setAttribute("datosPlanilla", listaPlanillas);
                
            }
            
            if(opcion.equals("sinremesas")){
                
                for( int i = 0 ; i < LOV.length ; i++ ){
                    Numpla = LOV[i];
                    model.PlanillaImpresionSvc.DatosCIA(Distrito);
                    model.PlanillaImpresionSvc.DatosPlanilla(Numpla);
                    
                    model.PlanillaImpresionSvc.ActualizarFechaImpresion(Numpla);
                    DatosPlanillaImpPDF dato = model.PlanillaImpresionSvc.getDatos();
                    
                    dato = this.Cargar(dato);
                    
                    listaPlanillas.add(dato);
                    
                }
                
                String ruta = application.getRealPath("/");
                File xslt = new File(ruta + "Templates/p2.xsl");
                File pdf = new File(ruta + "pdf/p2.pdf");
                
                PlanillaPDF planilla_pdf = new PlanillaPDF();
                planilla_pdf.generarPDF(xslt, pdf,listaPlanillas);
                
                next = "/pdf/p2.pdf";
                
                
            }
            
            if (opcion.equals("Buscar")){
                if( model.planillaService.existPlanillaBaseCol(Numpla) ){
                    model.PlanillaImpresionSvc.DatosCIA(Distrito);
                    model.PlanillaImpresionSvc.DatosPlanilla(Numpla);
                    DatosPlanillaImpPDF dato = model.PlanillaImpresionSvc.getDatos();
                    
                    if( dato.getPlaviaje().equals("S") ){
                        
                        if ( !dato.getReg_status().equals("P") ){
                            
                            dato = this.Cargar(dato);
                            listaPlanillas.add(dato);
                            
                            /* CREACION DE ARCHIVOS XSLT(ARBOL) Y PDF */
                            String ruta = application.getRealPath("/");
                            File xslt = new File(ruta + "Templates/p2.xsl");
                            File pdf = new File(ruta + "pdf/p2.pdf");
                            
                            model.PlanillaImpresionSvc.ActualizarFechaImpresion(Numpla);
                            
                            PlanillaPDF planilla_pdf = new PlanillaPDF();
                            planilla_pdf.generarPDF(xslt, pdf,listaPlanillas);
                            
                            next = "/pdf/p2.pdf";
                        }
                        else{
                            Mensaje =      " Esta Planilla que requiere \n   "+
                            " una autorización de la administración de riesgos, \n      "+
                            " consulte el correo para su obtener su autorización        ";
                            next = "/impresiones_pdf/buscarplanillaPDF.jsp?Mensaje="+Mensaje;
                        }
                    }
                    
                    else{
                        Mensaje = "La Planilla no Tiene plan de viaje, debe crearse primero";
                        next = "/impresiones_pdf/buscarplanillaPDF.jsp?Mensaje="+Mensaje;
                    }
                }
                else{
                    Mensaje = "La Planilla no existe";
                    next = "/impresiones_pdf/buscarplanillaPDF.jsp?Mensaje="+Mensaje;
                }
                
                
            }//Fin de la Opcion Buscar
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }
        catch(Exception e) {
            throw new ServletException(e.getMessage());
        }
    }
    public DatosPlanillaImpPDF Cargar( DatosPlanillaImpPDF datosPla ){
        String remesa = "";
        String doc_internos = "";
        String texto_oc = "";
        String desc = "";
        String observaciones = "";
        String soporte = "";
        DatosDescuentosImp datosdescuentos;
        
        int longitud = (datosPla.getRemesas() != null )?datosPla.getRemesas().size():0;
        for( int i = 0 ; i < longitud ; i++ ){
            DatosRemesaImp rem = (DatosRemesaImp)datosPla.getRemesas().get(i);
            remesa += rem.getNumeroRemesa()+",";
            doc_internos = rem.getDocumentoInterno();
            observaciones += rem.getObservaciones()+",";
            List descuentos = rem.getDescuentos();
            desc = "Conceptos descuentos : \n";
            for (int j = 0 ; j < descuentos.size(); j++ ){
                datosdescuentos = (DatosDescuentosImp)descuentos.get(j);
                desc += datosdescuentos.getConcepto() + "  " + datosdescuentos.getValor() + "\n";
            }
            TreeMap soportes = rem.getSoportes();
            Set key = soportes.keySet();
            Iterator it = key.iterator();
            while(it.hasNext()){
                soporte += (String)soportes.get(it.next()) + ",";
            }
            
            
        }
        //observaciones
        if(remesa.length() > 0 )
            remesa = remesa.substring(0, remesa.length()-1);
        if(doc_internos.length() > 0 )
            doc_internos = doc_internos.substring(0, doc_internos.length()-1);
        if(observaciones.length() > 0)
            observaciones = observaciones.substring(0, observaciones.length()-1);
        if(soporte.length() > 0)
            soporte = soporte.substring(0, soporte.length()-1);
        
        
        
        
        Item2 item = new Item2( remesa ,"","","","","","","",desc,"","");
        Vector detalle=new Vector();
        detalle.add(item);
        
        
        String leyenda2 = " Segun documento(s) internos del Cliente : \n";
        leyenda2 = ( !doc_internos.equals("") )?leyenda2 + doc_internos: "";
        
        String contenedores = "";
        if( datosPla.getContenedores() != null  ){
            contenedores = (!datosPla.getContenedores().equals(""))?" Contenedore(s) :  " + datosPla.getContenedores():"";
        }
        leyenda2 += contenedores;
        
        Vector copias=new Vector();
        copias.add("CONDUCTOR ORIGINAL");
        copias.add("CONDUCTOR COPIA");
        copias.add("EGRESO ANTICIPO");
        datosPla.setLeyenda(leyenda2);
        datosPla.setDetalle(detalle);
        datosPla.setCopias(copias);
        datosPla.setObservaciones(observaciones);
        datosPla.setSoportes(soporte);
        return datosPla;
    }
    
}
