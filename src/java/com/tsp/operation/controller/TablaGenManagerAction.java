/*
 * TablaGenManagerAction.java
 *
 * Created on 15 de diciembre de 2005, 08:48 AM
 */

package com.tsp.operation.controller;

import org.apache.log4j.Logger;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.TablaGen;


import java.io.*;

/**
 * Clase que atiende a todas las operaciones referentes a la administraci�n de tablagen
 * @author  Alejandro Payares
 */
public class TablaGenManagerAction extends Action {
    
    /**
     * Objeto para registrar datos en el log
     */
    private Logger logger;
    
    /** Creates a new instance of TablaGenManagerAction */
    public TablaGenManagerAction() {
        logger = Logger.getLogger(this.getClass());
    }
    
    
    /**
     * Metodo invocado por el servlet controlador para ejecutar la acci�n.
     */
    public void run() throws javax.servlet.ServletException {
        String next = "";
        TablaGen bean = null;
        String cmd = request.getParameter("cmd");
        String tipo = request.getParameter("table_type");
        try {
            Usuario user = (Usuario) request.getSession().getAttribute("Usuario");
            if ( cmd == null ) {
                String p = (String) request.getSession().getAttribute("Perfil");
                model.tablaGenService.buscarTablasPerfiles(p);
                next = "/jsp/sot/body/PantallaTablagenList.jsp";
            }
            else if ( "mng".equals(cmd) ) {
                String tabla = request.getParameter("type");
                model.tablaGenService.buscarRegistros(tabla);
                model.tablaGenService.setDato("type",tabla);
                next = "/jsp/sot/body/PantallaTablaGenManager.jsp";
            }
            else if ( "agregarTabla".equals(cmd) ) {
                String descripcion = request.getParameter("descripcion");
                user = (Usuario) request.getSession().getAttribute("Usuario");
                String obs = request.getParameter("dato");
                bean = new TablaGen();
                bean.setDescripcion(descripcion);
                bean.setTable_type(tipo);
                bean.setDato(obs);
                model.tablaGenService.agregarTabla(tipo.toUpperCase(), descripcion, user.getLogin(), obs);
                next = "/jsp/sot/body/PantallaTablaGenNueva.jsp?titulo=Agregar Tabla&mensaje=La tabla fu� creada correctamente";
                request.setAttribute("bean",new TablaGen());
                logger.info(  user.getNombre() + " cre� tabla en tablagen table_type = "+tipo+" y descripcion = "+descripcion);
            }
            else if ( "agregar".equals(cmd) ) {
                String codigo = request.getParameter("table_code");
                String referencia = request.getParameter("referencia");
                String descripcion = request.getParameter("descripcion");
                user = (Usuario) request.getSession().getAttribute("Usuario");
                bean = new TablaGen();
                bean.setDescripcion(descripcion);
                bean.setReferencia(referencia);
                bean.setTable_code(codigo);
                bean.setTable_type(tipo);
                //06-01-07
                int tam = Integer.parseInt(request.getParameter("size"));
                StringBuffer dato=new StringBuffer();
                String tem="";
                int lon = 0;
                for (int i=0; i < tam ; i++){
                    tem = request.getParameter("ley"+i);
                    lon = Integer.parseInt(request.getParameter("lon"+i));
                    StringBuffer sb = new StringBuffer(tem);
                    for( int j=tem.length(); j<lon; j++){
                        sb.append(" ");
                    }
                    dato.append(sb);
                }
                model.tablaGenService.agregarRegistro(tipo, codigo, referencia, descripcion, user.getLogin(),dato.toString());
                next = "/jsp/sot/body/PantallaTablaGenNuevoRegistro.jsp?titulo=AGREGAR REGISTRO&mensaje=El registro fu� agregado correctamente&type="+tipo;
                request.setAttribute("bean",new TablaGen());
                logger.info(  user.getNombre() + " agreg� registro en tablagen table_type = "+tipo+" y table_code = "+codigo+" y referencia = "+referencia+" y descripcion = "+descripcion);
            }
            else if ( "loadTable".equals(cmd) ) {
                request.setAttribute("bean",model.tablaGenService.obtenerTabla(tipo));
                next = "/jsp/sot/body/PantallaTablaGenNueva.jsp?titulo=EDITAR TABLA "+tipo+"&old_type="+tipo;
            }
            else if ( "load".equals(cmd) ) {
                String oid = request.getParameter("oid");
                TablaGen t = model.tablaGenService.obtenerRegistro(oid);
                request.setAttribute("bean",t);
                next = "/jsp/sot/body/PantallaTablaGenNuevoRegistro.jsp?titulo=EDITAR REGISTRO&type="+t.getTable_type();
            }
            else if ( "modificarTabla".equals(cmd) ) {
                String descripcion = request.getParameter("descripcion");
                String obs = request.getParameter("dato");
                String tipoViejo = request.getParameter("old_type");
                String aux = tipo;
                tipo = tipoViejo;
                model.tablaGenService.editarTabla(aux.toUpperCase(), descripcion, tipoViejo, user.getLogin(),obs);
                bean = new TablaGen();
                bean.setDescripcion(descripcion);
                bean.setTable_type(aux);
                model.tablaGenService.buscarTablas();
                next = "/jsp/sot/body/PantallaTablagenList.jsp?mensaje=La tabla '"+tipoViejo+"' fue modificada correctamente!";
                request.setAttribute("bean",new TablaGen());
                logger.info(  user.getNombre() + " modific� tabla "+tipoViejo+" en tablagen table_type = "+tipo+" y descripcion = "+descripcion);
            }
            else if ( "modificar".equals(cmd) ) {
                String codigo = request.getParameter("table_code");
                String referencia = request.getParameter("referencia");
                String descripcion = request.getParameter("descripcion");
                String oid = request.getParameter("oid");
                bean = new TablaGen();
                bean.setDescripcion(descripcion);
                bean.setReferencia(referencia);
                bean.setTable_code(codigo);
                bean.setTable_type(tipo);
                bean.setOid(oid);
                //06-01-07
                int tam = Integer.parseInt(request.getParameter("size"));
                StringBuffer dato=new StringBuffer();
                String tem="";
                
                int lon = 0;
                for (int i=0; i < tam ; i++){
                    tem = request.getParameter("ley"+i);
                    lon = Integer.parseInt(request.getParameter("lon"+i));
                    StringBuffer sb = new StringBuffer(tem);
                    for( int j=tem.length(); j<lon; j++){
                        sb.append(" ");
                    }
                    dato.append(sb);
                }
                model.tablaGenService.editarRegistro(oid, tipo, codigo, referencia, descripcion, user.getLogin(),dato.toString());
                model.tablaGenService.buscarRegistros(tipo);
                next = "/jsp/sot/body/PantallaTablaGenManager.jsp?type="+tipo+"&mensaje=El registro fu� editado correctamente";
                request.setAttribute("bean",new TablaGen());
                logger.info(  user.getNombre() + " modific� registro en tablagen oid = "+oid+" y table_type = "+tipo+" y table_code = "+codigo+" y referencia = "+referencia+" y descripcion = "+descripcion);
            }
            else if ( "eliminar".equals(cmd) ) {
                String oids [] = request.getParameterValues("select");
                model.tablaGenService.eliminarRegistros(oids);
                model.tablaGenService.buscarRegistros(tipo);
                StringBuffer sb = new StringBuffer();
                for( int i=0; i< oids.length; i++ ){
                    sb.append(oids[i]+",");
                }
                sb.deleteCharAt(sb.length()-1);
                if ( oids.length == 1 ) {
                    next = "/jsp/sot/body/PantallaTablaGenManager.jsp?type="+tipo+"&mensaje=El registro seleccionado fue eliminado correctamente!";
                }
                else {
                    next = "/jsp/sot/body/PantallaTablaGenManager.jsp?type="+tipo+"&mensaje=Los registros seleccionados fueron eliminados correctamente!";
                }
                logger.info(  user.getNombre() + " elimin� uno o m�s registros en tablagen oids = "+sb+" y table_type = "+tipo);
            }
            else if ( "droptable".equals(cmd) ) {
                String tablas [] = request.getParameterValues("select");
                model.tablaGenService.eliminarTablas(tablas);
                model.tablaGenService.buscarTablas();
                StringBuffer sb = new StringBuffer();
                for( int i=0; i< tablas.length; i++ ){
                    sb.append(tablas[i]+",");
                }
                sb.deleteCharAt(sb.length()-1);
                if( tablas.length == 1 ){
                    next = "/jsp/sot/body/PantallaTablagenList.jsp?mensaje=La tabla '" + sb + "' fue eliminada correctamente!";
                }
                else {
                    next = "/jsp/sot/body/PantallaTablagenList.jsp?mensaje=Las tablas '" + sb + "' fueron eliminadas correctamente!";
                }
                logger.info(  user.getNombre() + " elimin� una o m�s tablas en tablagen tablas = "+sb);
            }
        }
        catch( com.tsp.exceptions.InformationException ex ){
            if ( "agregar".equals(cmd) ) {
                next = "/jsp/sot/body/PantallaTablaGenNuevoRegistro.jsp?titulo=AGREGAR REGISTRO&type="+tipo+"&mensaje="+ex.getMessage();
            }
            else if ( "modificar".equals(cmd) ) {
                next = "/jsp/sot/body/PantallaTablaGenNuevoRegistro.jsp?titulo=EDITAR REGISTRO&type="+tipo+"&mensaje="+ex.getMessage();
            }
            else if ( "agregarTabla".equals(cmd) ){
                next = "/jsp/sot/body/PantallaTablaGenNueva.jsp?titulo=Agregar Tabla&mensaje="+ex.getMessage();
            }
            else if ( "modificarTabla".equals(cmd) ){
                next = "/jsp/sot/body/PantallaTablaGenNueva.jsp?titulo=EDITAR TABLA "+tipo+"&mensaje="+ex.getMessage();
            }
            request.setAttribute("bean", bean);
        }
        catch( Exception ex ){
            ex.printStackTrace();
            logger.error(ex.getMessage());
            throw new javax.servlet.ServletException("Error en TablaGenManagerAction: "+ex.getMessage());
        }
        this.dispatchRequest(next);
    }
    
   
    
}
