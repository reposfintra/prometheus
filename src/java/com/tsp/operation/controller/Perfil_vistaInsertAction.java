/*
 * Perfil_vistaInsertAction.java
 *
 * Created on 14 de julio de 2005, 8:18
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

/**
 *
 * @author  Rodrigo
 */
public class Perfil_vistaInsertAction extends Action{
    
    /** Creates a new instance of UnidadInsertAction */
    public Perfil_vistaInsertAction() {
    }
    public void run() throws javax.servlet.ServletException, InformationException {
        String pag = "/jsp/trafico/permisos/perfil_vista/perfil_vistaIngresar.jsp";
        String next = "";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String perfil = (String) request.getParameter("c_perfil");
        String pagina = (String) request.getParameter("c_pagina");
        String vis,edi;
        int sw=0;
        Vector campos;
        
        pag += "?mensaje=Ingreso de relaci�n de campos exitosa!";
        next = pag;
        
        //tammatu 25.10.2005
        String consulta = (String) session.getAttribute("consulta_rcampos");
        if( consulta != null ){
            pag = "/jsp/trafico/permisos/perfil_vista/perfil_vistaConsultar.jsp";
            pag += "?mensaje=Ingreso de relaci�n de campos exitosa!";
            next = pag;
            session.removeAttribute("consulta_rcampos");
        }
        
        try{
            campos = model.camposjsp.listarCampos_jsp(pagina);
            Perfil_vista pv = new Perfil_vista();
            campos_jsp campo;
            String camp;
            model.perfil_vistaService.eliminarPerfil_vista(perfil,pagina);
            for (int i = 0; i < campos.size(); i++){
                ////System.out.println(i);
                campo = (campos_jsp) campos.elementAt(i);
                camp = campo.getCampo();
                pv.setPerfil(perfil);
                pv.setPagina(pagina);
                pv.setCampo(camp);
                pv.setCia(usuario.getCia().toUpperCase());
                pv.setUser_update(usuario.getLogin().toUpperCase());
                pv.setCreation_user(usuario.getLogin().toUpperCase());
                vis = (String) request.getParameter(camp+"V");
                edi = (String) request.getParameter(camp+"E");
                
                if((vis!=null)||(edi!=null)){
                    //try{
                        pv.setVisible(Null(vis));
                        pv.setEditable(Null(edi));
                        model.perfil_vistaService.insertPerfil_vista(pv);
                    /*}
                    catch(SQLException e){
                        sw=1;
                    }*/
                }
            }
            
            //if(sw==1){
               /* if(!model.unidadService.existUnidad(codigo)){
                    model.unidadService.updateUnidad(codigo, descripcion, usuario.getLogin().toUpperCase());
                    request.setAttribute("mensaje","MsgAgregado");
                }
                else{
                    request.setAttribute("mensaje","MsgErrorIng");
                }
            }
            else{
                request.setAttribute("mensaje","MsgAgregado");
            }*/
            
            
        }catch(SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    public String Null(String s){
        if (s!=null){
            return s;
        }
        return " ";
    }
    
}
