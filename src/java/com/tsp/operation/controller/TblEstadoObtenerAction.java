/*
 * TblEstadoInsert.java
 *
 * Created on 5 de octubre de 2005, 09:55 AM
 */

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;

/**
 *
 * @author  Andres
 */
public class TblEstadoObtenerAction extends Action {
        
        /** Creates a new instance of TblEstadoInsert */
        public TblEstadoObtenerAction() {
        }
        
        public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
                
                String next = "";
                String pag = "/jsp/masivo/tblestado/EstadoUpdate.jsp?mensaje=";
                
                String tipo = request.getParameter("tipo");
                String codestado = request.getParameter("codestado");                
                String ruta = request.getRealPath("/") + "vista_previa\\";
                //String ruta = model.tbl_estadoSvc.getURL()+"\\vista_previa\\";
                ////System.out.println("-----> Se escribi� rl archivo en: " + ruta );
                
                try{
                        TblEstado est = model.tbl_estadoSvc.obtenerEstado(tipo, codestado, ruta);
                        HttpSession session = request.getSession();
                        session.setAttribute("estado", est);
                        next = com.tsp.util.Util.LLamarVentana(pag, "Actualizar Estado");
                        
                }catch (SQLException e){
                       throw new ServletException(e.getMessage());
                }

                this.dispatchRequest(next);
                
        }
        
}
