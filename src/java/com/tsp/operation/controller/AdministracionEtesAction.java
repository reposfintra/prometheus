/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.AdministracionEdsDAO;
import com.tsp.operation.model.DAOS.AdministracionEtesDAO;
import com.tsp.operation.model.DAOS.impl.AdministracionEdsImpl;
import com.tsp.operation.model.DAOS.impl.AdministracionEtesImpl;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.POIWrite;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.Util;
import com.tsp.util.Utility;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;

/**
 *
 * @author mariana
 */
public class AdministracionEtesAction extends Action {

    private final int CARGAR_TIPO_TRAMA = 0;
    private final int CARGAR_LOG = 1;
    private final int CARGAR_JSON_TRAMA = 2;
    private final int CARGAR_EMPRESAS = 3;
    private final int CARGAR_TABLA_PRODUCTOS_TRANS = 5;
    private final int GUARDAR_PRODUCTOS_TRANS = 6;
    private final int CAMBIAR_ESTADO_PRODUCTO_TRANS = 7;
    private final int ACTUALIZAR_PRODUCTO_TRANS = 8;
    private final int CARGAR_TRANSPORTADORAS = 9;
    private final int GUARDAR_RELACION_PRO_TRANS = 10;
    private final int CARGAR_PRODUCTO = 11;
    private final int CARGAR_REL_PRODUCTOS_TRANS = 12;
    private final int CARGAR_GRILLA_TIPO_DESCUENTO = 13;
    private final int CARGAR_TIPO_DESCUENTO = 14;
    private final int CAMBIAR_ESTADO_REL_TRANS_PRO = 15;
    private final int GUARDAR_TIPO_DESCUENTO = 16;
    private final int ACTUALIZAR_TIPO_DESCUENTO = 17;
    private final int CAMBIAR_ESTADO_TIPO_DESCUENTO = 18;
    private final int CARGAR_GRILLA_TRANSPORTADORAS = 19;
    private final int GUARDAR_TRANSPORTADORAS = 20;
    private final int CARGAR_PAIS = 21;
    private final int CARGAR_DEPARTAMENTO = 22;
    private final int CARGAR_CIUDAD = 23;
    private final int VERIFICAR_USUARIO = 24;
    private final int CARGAR_PERIODICIDAD = 25;
    private final int ACTUALIZAR_TRANSPORTADORAS = 26;
    private final int CAMBIAR_ESTADO_TRANSPORTADORAS = 27;
    private final int CARGAR_HC = 28;
    private final int CXC_GENERADAS_TRANSPORTADORAS = 29;
    private final int CARGAR_FECHAS_CXC_GENERADAS_TRANS = 30;
    private final int EXPORTAR_EXCEL = 31;
    private final int CXC_DETALLES_GENERADAS_TRANSPORTADORAS = 32;
    private final int EXPORTAR_EXCEL_CXC_DETALLE = 33;
    private final int CARGAR_GRILLA_AGENCIAS = 34;
    private final int GUARDAR_AGENCIAS = 35;
    private final int ACTUALIZAR_AGENCIAS = 36;
    private final int CAMBIAR_ESTADO_AGENCIA = 37;
    private final int VERIFICAR_TRANSPORTADORA = 38;
    private final int AUTORIZAR_VENTA = 39;
    private AdministracionEtesDAO dao;
    private AdministracionEdsDAO daoeds;

    POIWrite xls;
    private int fila = 0;
    HSSFCellStyle header, titulo1, titulo2, titulo3, titulo4, titulo5, letra, numero, dinero, dinero2, numeroCentrado, porcentaje, letraCentrada, numeroNegrita, ftofecha;
    private SimpleDateFormat fmt;
    String rutaInformes;
    String nombre;
    Usuario usuario = null;

    @Override
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            dao = new AdministracionEtesImpl(usuario.getBd());
            int opcion = (request.getParameter("opcion") != null ? Integer.parseInt(request.getParameter("opcion")) : -1);
            switch (opcion) {
                case CARGAR_TIPO_TRAMA:
                    cargarTipo();
                    break;
                case CARGAR_LOG:
                    buscarLog();
                    break;
                case CARGAR_JSON_TRAMA:
                    cargarJson();
                    break;
                case CARGAR_EMPRESAS:
                    cargarEmpresas();
                    break;
                case CARGAR_TABLA_PRODUCTOS_TRANS:
                    cargarGrillaProductosTrans();
                    break;
                case GUARDAR_PRODUCTOS_TRANS:
                    guardarProductoTrans();
                    break;
                case CAMBIAR_ESTADO_PRODUCTO_TRANS:
                    cambiarEstadoProductoTrans();
                    break;
                case ACTUALIZAR_PRODUCTO_TRANS:
                    actualizarProductoTrans();
                    break;
                case CARGAR_TRANSPORTADORAS:
                    cargarTransportadoras();
                    break;
                case GUARDAR_RELACION_PRO_TRANS:
                    guardarRelProTrans();
                    break;
                case CARGAR_PRODUCTO:
                    cargarProductosTrans();
                    break;
                case CARGAR_REL_PRODUCTOS_TRANS:
                    cargarGrillaRelProTrans();
                    break;
                case CARGAR_TIPO_DESCUENTO:
                    cargarTipoDescuento();
                    break;
                case CAMBIAR_ESTADO_REL_TRANS_PRO:
                    cambiarReltranProd();
                    break;
                case CARGAR_GRILLA_TIPO_DESCUENTO:
                    cargarGrillaTipoDescuento();
                    break;
                case GUARDAR_TIPO_DESCUENTO:
                    guardarTipoDescuento();
                    break;
                case ACTUALIZAR_TIPO_DESCUENTO:
                    actualizarTipoDescuento();
                    break;
                case CAMBIAR_ESTADO_TIPO_DESCUENTO:
                    cambiarEstadoTipoDescuento();
                    break;
                case CARGAR_GRILLA_TRANSPORTADORAS:
                    cargarGrillaTransportadoras();
                    break;
                case GUARDAR_TRANSPORTADORAS:
                    guardarTransportadoras();
                    break;
                case CARGAR_PAIS:
                    cargarPais();
                    break;
                case CARGAR_DEPARTAMENTO:
                    cargarDepartamento();
                    break;
                case CARGAR_CIUDAD:
                    cargarCiudad();
                    break;
                case VERIFICAR_USUARIO:
                    verificar_usuario();
                    break;
                case CARGAR_PERIODICIDAD:
                    cargarPeriodicidad();
                    break;
                case ACTUALIZAR_TRANSPORTADORAS:
                    daoeds = new AdministracionEdsImpl(this.usuario.getBase());
                    actualizarTransportadoras();
                    break;
                case CAMBIAR_ESTADO_TRANSPORTADORAS:
                    cambiarEstadoTransportadoras();
                    break;
                case CARGAR_HC:
                    cargarHC();
                    break;
                case CXC_GENERADAS_TRANSPORTADORAS:
                    cargarCxcGeneradasTransportadoras();
                    break;
                case CARGAR_FECHAS_CXC_GENERADAS_TRANS:
                    cargarFechaCXC();
                    break;
                case EXPORTAR_EXCEL:
                    exportarExcelCXC();
                    break;
                case CXC_DETALLES_GENERADAS_TRANSPORTADORAS:
                    cargarCxcDetallesGeneradasTransportadoras();
                    break;
                case EXPORTAR_EXCEL_CXC_DETALLE:
                    exportarExcelCXCDetalle();
                    break;
                case CARGAR_GRILLA_AGENCIAS:
                    cargarGrillaAgenciasTrans();
                    break;
                case GUARDAR_AGENCIAS:
                    guardarAgencia();
                    break;
                case ACTUALIZAR_AGENCIAS:
                    actualizarAgenciaTrans();
                    break;
                case CAMBIAR_ESTADO_AGENCIA:
                    cambiarEstadoAgencia();
                    break;
                case VERIFICAR_TRANSPORTADORA:
                    verificarusuarioProp();
                    break;
                case AUTORIZAR_VENTA:
                    AutorizarVenta();
                    break;
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cargarTipo() {
        try {
            this.printlnResponse(dao.cargarTipoManifiesto(), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEtesAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarEmpresas() {
        try {
            String perfilu = (String) request.getSession().getAttribute("Perfil");
            String usuario = ((Usuario) request.getSession().getAttribute("Usuario")).getLogin();
            this.printlnResponse(dao.cargarEmpresas(usuario, perfilu), "application/json;");

        } catch (Exception ex) {
            Logger.getLogger(AdministracionEtesAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void buscarLog() {
        try {
            Gson gson = new Gson();
            JsonObject obj = new JsonObject();
            String fechainicio = request.getParameter("fechainicio") != null ? request.getParameter("fechainicio") : "";
            String fechafinal = request.getParameter("fechafinal") != null ? request.getParameter("fechafinal") : "";
            String tipo = request.getParameter("tipo") != null ? request.getParameter("tipo") : "";
            String empresa = request.getParameter("empresa") != null ? request.getParameter("empresa") : "";
            String estado = request.getParameter("estadoT") != null ? request.getParameter("estadoT") : "";
            String planillaP = request.getParameter("planillaP") != null ? request.getParameter("planillaP") : "";

            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.buscarLogTrama(fechainicio, fechafinal, tipo, empresa, estado, planillaP)) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cargarJson() {
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            String json = dao.cargarjsonTrama(id);
            this.printlnResponse(json, "application/text;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cargarGrillaProductosTrans() {
        try {
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.cargarGrillaProductosTrans()) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEtesAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void guardarProductoTrans() {
        String json = "{\"respuesta\":\"Guardado:)\"}";
        try {
            String cia = ((Usuario) request.getSession().getAttribute("Usuario")).getDstrct();
            String nomproducto = request.getParameter("nomproducto");
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.guardarProductosEds(cia, nomproducto));
            tService.execute();
            tService.closeAll();
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void cambiarEstadoProductoTrans() {
        try {

            String id = request.getParameter("id");
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.cambiarEstadoProductoTrans(id));
            tService.execute();
            tService.closeAll();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void actualizarProductoTrans() {
        try {
            String nombrepro = request.getParameter("nombreproducto");
            String id = request.getParameter("id");
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.actualizarProductoTrans(nombrepro, id));
            tService.execute();
            tService.closeAll();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cargarTransportadoras() {
        try {

            this.printlnResponse(dao.cargarTransportadoras(), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEtesAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void guardarRelProTrans() {
        String json = "{\"respuesta\":\"Guardado:)\"}";
        try {
            JsonObject obj = (JsonObject) (new JsonParser()).parse(request.getParameter("informacion"));
            obj.addProperty("usuario", ((Usuario) request.getSession().getAttribute("Usuario")).getLogin());
            obj.addProperty("cia", ((Usuario) request.getSession().getAttribute("Usuario")).getCia());
            json = this.dao.guardarRelProductosTransportadoras(obj);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"ERROR:)\"}";
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void cargarGrillaRelProTrans() {
        try {

            Gson gson = new Gson();
            String idtrans = request.getParameter("transid");
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.cargarGrillaRelProTrans(idtrans)) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEtesAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarProductosTrans() {
        try {
            this.printlnResponse(dao.cargarProductosTrans(), "application/json;");

        } catch (Exception ex) {
            Logger.getLogger(AdministracionEtesAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarTipoDescuento() {
        try {
            this.printlnResponse(dao.cargarTipoDescuento(), "application/json;");

        } catch (Exception ex) {
            Logger.getLogger(AdministracionEtesAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarGrillaTipoDescuento() {
        try {
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.cargarGrillaTipoDescuento()) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEtesAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cambiarReltranProd() {
        try {
            String id = request.getParameter("id");
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.cambiarEstadoTransPro(id));
            tService.execute();
            tService.closeAll();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void guardarTipoDescuento() {
        String json = "{\"respuesta\":\"Guardado:)\"}";
        try {
            String cia = ((Usuario) request.getSession().getAttribute("Usuario")).getDstrct();
            String nomtipdes = request.getParameter("nomtipdes");
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.guardarTipoDescuento(cia, nomtipdes));
            tService.execute();
            tService.closeAll();
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void actualizarTipoDescuento() {
        try {
            String nombretipodes = request.getParameter("nombretipodes");
            String id = request.getParameter("id");
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.actualizarTipoDescuento(id, nombretipodes));
            tService.execute();
            tService.closeAll();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cambiarEstadoTipoDescuento() {
        try {
            String id = request.getParameter("id");
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.cambiarEstadoTipoDescuento(id));
            tService.execute();
            tService.closeAll();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cargarGrillaTransportadoras() {
        try {
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.cargarGrillaTransportadoras()) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEtesAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void guardarTransportadoras() {
        String json = "{\"respuesta\":\"Guardado:)\"}";
        try {
            String nombretran = request.getParameter("nombretran");
            String nittran = request.getParameter("nittran");
            String direcciontran = request.getParameter("direcciontran");
            String correo = request.getParameter("correo");
            String telefonotran = request.getParameter("telefonotran");
            String nombreencargado = request.getParameter("nombreencargado");
            String identificacion = request.getParameter("identificacion");
            String idusuario = request.getParameter("idusu");
            String passw = Util.encript(application.getInitParameter("deskey"), request.getParameter("pas1"));
            String cia = ((Usuario) request.getSession().getAttribute("Usuario")).getDstrct();
            String base = ((Usuario) request.getSession().getAttribute("Usuario")).getBase();
            String perfil = "TRANSPORTAD";
            String usuario = ((Usuario) request.getSession().getAttribute("Usuario")).getLogin();
            String estadousu = "A";
            String cambioclav = request.getParameter("cambioclav");
            String pais = request.getParameter("pais");
            String ciudad = request.getParameter("ciudad");
            String periodicidad = request.getParameter("periodicidad");
            String hc = request.getParameter("hc");
            String tipodoc = request.getParameter("tipodoc");
            String cuporot = request.getParameter("cuporot");

            TransaccionService tService = new TransaccionService(this.usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.daoeds.guardarUsuario(nombretran, direcciontran, correo, telefonotran, pais, nittran, ciudad, estadousu, idusuario, passw, cia, cambioclav, base, perfil));
            tService.getSt().addBatch(this.daoeds.guardarUsuario(cia, idusuario, usuario, base));
            tService.getSt().addBatch(this.daoeds.guardarUsuarioPerfil(perfil, idusuario, usuario, cia));
            tService.getSt().addBatch(this.dao.guardarTransportadoras(nombretran, nittran, direcciontran, correo, nombreencargado, identificacion, usuario, cia, periodicidad, idusuario, cuporot));
            tService.getSt().addBatch(this.daoeds.guardarProveedor(cia, nittran, nombretran, usuario, tipodoc, "PJ", "N", "N", "N", "N", "N", base, "", "", "", "", "", "", "", hc, "", "", "", "", ""));
            tService.getSt().addBatch(this.daoeds.guardarCliente(nombretran, base, nittran, cia, hc, direcciontran, telefonotran, nombreencargado, telefonotran, correo, direcciontran, tipodoc, ciudad, pais, usuario));
            tService.execute();
            tService.closeAll();
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void cargarPais() {
        try {
            this.printlnResponse(dao.cargarPais(), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEtesAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarDepartamento() {
        try {
            String pais = request.getParameter("pais");
            this.printlnResponse(dao.cargarDepartamento(pais), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEtesAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarCiudad() {
        try {
            String departamento = request.getParameter("departamento");
            this.printlnResponse(dao.cargarCiudad(departamento), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEtesAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void verificar_usuario() {
        try {
            daoeds = new AdministracionEdsImpl(this.usuario.getBd());
            String idusuario = request.getParameter("idusuario");
            String json = daoeds.verificarUsuario(idusuario);
            this.printlnResponse(json, "text/plain;");

        } catch (Exception ex) {
            Logger.getLogger(AdministracionEtesAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarPeriodicidad() {
        try {

            this.printlnResponse(dao.cargarPeriodicidad(), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEtesAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void actualizarTransportadoras() {
        try {
            String id = request.getParameter("id");
            String nombretran = request.getParameter("nombretran");
            String nittran = request.getParameter("nittran");
            String direcciontran = request.getParameter("direcciontran");
            String correo = request.getParameter("correo");
            String telefonotran = request.getParameter("0000");
            String nombreencargado = request.getParameter("nombreencargado");
            String identificacion = request.getParameter("identificacion");
            String idusuario = request.getParameter("idusu");
            String passw = Util.encript(application.getInitParameter("deskey"), request.getParameter("pas1"));
            String usuario = ((Usuario) request.getSession().getAttribute("Usuario")).getLogin();
            String pais = request.getParameter("pais");
            String ciudad = request.getParameter("ciudad");
            String periodicidad = request.getParameter("periodicidad");
            String editusu = request.getParameter("iditusu");
            String hc = request.getParameter("hc");
            String tipodoc = request.getParameter("tipodoc");
            String codcli = request.getParameter("codcli");
            String cuporot = request.getParameter("cuporot");
            TransaccionService tService = new TransaccionService(this.usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.daoeds.actualizarUsuario(nombretran, direcciontran, correo, telefonotran, pais, ciudad, passw, idusuario));
//            tService.getSt().addBatch(this.daoeds.actualizarUsuarioProyecto(idusuario, usuario, idusuario));
//            tService.getSt().addBatch(this.daoeds.actualizarUsuarioPerfil(idusuario, usuario, idusuario));
            tService.getSt().addBatch(this.daoeds.actualizarProveedor(nittran, nombretran, usuario));
            tService.getSt().addBatch(this.dao.actualizarTransportadoras(nombretran, direcciontran, correo, nombreencargado, identificacion, idusuario, periodicidad, usuario, cuporot, id));
            tService.getSt().addBatch(this.daoeds.actualizarCliente(nombretran, hc, direcciontran, telefonotran, nombretran, telefonotran, correo, direcciontran, ciudad, pais, usuario, codcli));
            tService.execute();
            tService.closeAll();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cambiarEstadoTransportadoras() {
        try {
            String id = request.getParameter("id");
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.cambiarEstadoTransportadoras(id));
            tService.execute();
            tService.closeAll();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cargarHC() {
        try {
            daoeds = new AdministracionEdsImpl(this.usuario.getBd());
            this.printlnResponse(daoeds.cargarHC(), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarCxcGeneradasTransportadoras() {
        try {

            Gson gson = new Gson();
            String transpotadora = request.getParameter("transportadora");
            String fecha = request.getParameter("fecha");
            String fecha_fin = request.getParameter("fechafin");
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.cargarCxcGeneradasTransportadoras(transpotadora, fecha, fecha_fin)) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEtesAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarCxcDetallesGeneradasTransportadoras() {
        try {

            Gson gson = new Gson();
            String transpotadora = request.getParameter("transportadora");
            String fecha = request.getParameter("fecha");
            String cxc = request.getParameter("cxc");
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.cargarCxcDetallesGeneradasTransportadoras(transpotadora, fecha, cxc)) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEtesAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarFechaCXC() {
        try {

            String transpotadora = request.getParameter("transportadora");
            this.printlnResponse(dao.cargarFechasCXCTrans(transpotadora), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEtesAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarGrillaAgenciasTrans() {
        try {

            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.cargarGrillaAgenciasTrans()) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEtesAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void guardarAgencia() {
        String json = "{\"respuesta\":\"Guardado:)\"}";
        try {
            String nombreagencia = request.getParameter("nombreagencia");
            String transportadoras = request.getParameter("transportadoras");
            String correo = request.getParameter("correo");
            String direccion = request.getParameter("direccion");
            String ciudad = request.getParameter("ciudad");
            String usuario = ((Usuario) request.getSession().getAttribute("Usuario")).getLogin();
            TransaccionService tService = new TransaccionService(this.usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.guardarAgencia(nombreagencia, transportadoras, correo, direccion, ciudad, usuario));
            tService.execute();
            tService.closeAll();
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void actualizarAgenciaTrans() {
        try {
            String id = request.getParameter("id");
            String nombreagencia = request.getParameter("nombreagencia");
            String transportadoras = request.getParameter("transportadoras");
            String correo = request.getParameter("correo");
            String direccion = request.getParameter("direccion");
            String ciudad = request.getParameter("ciudad");
            String usuario = ((Usuario) request.getSession().getAttribute("Usuario")).getLogin();
            TransaccionService tService = new TransaccionService(this.usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.actualizarAgencia(nombreagencia, transportadoras, correo, direccion, ciudad, usuario, id));
            tService.execute();
            tService.closeAll();

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cambiarEstadoAgencia() {
        try {
            String id = request.getParameter("id");
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.cambiarEstadoAgencia(id));
            tService.execute();
            tService.closeAll();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void verificarusuarioProp() {
        try {
            String nittrans = request.getParameter("nittrans");
            String json = dao.verificarUsuarioTrans(nittrans);
            this.printlnResponse(json, "text/plain;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void AutorizarVenta() {
        try {
            String id = request.getParameter("transid");
            String idusuario = request.getParameter("idusuario");
            String estado_usuario = request.getParameter("estadousuario");
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.AutorizarVenta(id));
            tService.getSt().addBatch(this.dao.desactivarUsuario(estado_usuario, idusuario));
            tService.execute();
            tService.closeAll();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void exportarExcelCXC() throws Exception {
        try{
        String resp1 = "";
        String url = "";
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        //Set<Map.Entry<String, JsonElement>> entrySet = object.entrySet();
        this.generarRUTA();
        this.crearLibro("CxcGeneradas_", "CXC GENERADAS");
        String[] cabecera = {"Nit", "Nombre Cliente", "Valor Factura", "Valor Abono", "Fecha vencimiento", "Fecha Ultimo Pago", "Usuario de Creacion", "Numero de Factura", "Fecha Corrida", "Numero de Planilla"};

        short[] dimensiones = new short[]{
            3000, 7000, 5000, 5000, 7000, 7000, 5000, 5000, 5000, 5000, 5000
        };
        this.generaTitulos(cabecera, dimensiones);

        for (int i = 0; i < asJsonArray.size(); i++) {
            JsonObject objects = (JsonObject) asJsonArray.get(i);
            fila++;
            int col = 0;
            xls.adicionarCelda(fila, col++, objects.get("nitcli").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("nomcli").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("facvlr").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("facabono").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("facfechvencimiento").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("facfechultimopago").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("usercreacion").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("facdocumento").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("fechacorrida").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("numplanilla").getAsString(), letra);

        }
//        }
        this.cerrarArchivo();

        fmt = new SimpleDateFormat("yyyMMdd HH:mm");
        url = request.getContextPath() + "/exportar/migracion/" + usuario.getLogin().toUpperCase() + "/CxcGeneradas_" + fmt.format(new Date()) + ".xls";
        resp1 = Utility.getIcono(request.getContextPath(), 5) + "SE ha generado con exito.<br/><br/>"
                + Utility.getIcono(request.getContextPath(), 7) + " <a href='" + url + "' >Ver Documento</a></td>";

        this.printlnResponse(resp1, "text/plain");
    } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    private void exportarExcelCXCDetalle() throws Exception {
        try{
             String resp1 = "";
        String url = "";
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        //Set<Map.Entry<String, JsonElement>> entrySet = object.entrySet();
        this.generarRUTA();
        this.crearLibro("detalles_", "DETALLES CXC GENERADAS");
        String[] cabecera = {"Cxc Corrida", "Planilla", "Reanticipo", "Valor", "Fecha de Creacion", "Fecha Corrida", "Fecha Vencimiento"};

        short[] dimensiones = new short[]{
            3000, 3000, 7000, 5000, 5000, 7000, 7000
        };
        this.generaTitulos(cabecera, dimensiones);
        int total = 0;
        for (int i = 0; i < asJsonArray.size(); i++) {
            JsonObject objects = (JsonObject) asJsonArray.get(i);
            fila++;
            int col = 0;
            xls.adicionarCelda(fila, col++, objects.get("cxc_corrida").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("planilla").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("tipomanifiesto").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("facvlr").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("fechcreacion").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("fechacorrida").getAsString(), letra);
            xls.adicionarCelda(fila, col++, objects.get("fechvencimiento").getAsString(), letra);
        }

//        }
        this.cerrarArchivo();

        fmt = new SimpleDateFormat("yyyMMdd HH:mm");
        url = request.getContextPath() + "/exportar/migracion/" + usuario.getLogin().toUpperCase() + "/detalles_" + fmt.format(new Date()) + ".xls";
        resp1 = Utility.getIcono(request.getContextPath(), 5) + "Se ha generado con exito.<br/><br/>"
                + Utility.getIcono(request.getContextPath(), 7) + " <a href='" + url + "' >Ver Documento</a></td>";

        this.printlnResponse(resp1, "text/plain");
        }catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    private void generarRUTA() throws Exception {
        try {

            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            rutaInformes = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File(rutaInformes);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    private void crearLibro(String nameFileParcial, String titulo) throws Exception {
        try {
            fmt = new SimpleDateFormat("yyyMMdd HH:mm");
            this.crearArchivo(nameFileParcial + fmt.format(new Date()) + ".xls", titulo);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage());
        }
    }

    private void generaTitulos(String[] cabecera, short[] dimensiones) throws Exception {
        try {

            fila = 0;

            for (int i = 0; i < cabecera.length; i++) {
                xls.adicionarCelda(fila, i, cabecera[i], titulo2);
                if (i < dimensiones.length) {
                    xls.cambiarAnchoColumna(i, dimensiones[i]);
                }
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage());
        }
    }

    private void cerrarArchivo() throws Exception {
        try {
            if (xls != null) {
                xls.cerrarLibro();
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            throw new Exception("Error en crearArchivo ....\n" + ex.getMessage());
        }
    }

    private void crearArchivo(String nameFile, String titulo) throws Exception {
        try {
            InitArchivo(nameFile);
            xls.obtenerHoja("hoja1");
            // xls.combinarCeldas(0, 0, 0, 8);
            // xls.adicionarCelda(0,0, titulo, header);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en crearArchivo ....\n" + ex.getMessage());
        }
    }

    private void InitArchivo(String nameFile) throws Exception {
        try {
            xls = new com.tsp.operation.model.beans.POIWrite();
            nombre = "/exportar/migracion/" + usuario.getLogin() + "/" + nameFile;
            xls.nuevoLibro(rutaInformes + "/" + nameFile);
            header = xls.nuevoEstilo("Tahoma", 10, true, false, "text", HSSFColor.GREEN.index, xls.NONE, HSSFCellStyle.ALIGN_LEFT);
            titulo1 = xls.nuevoEstilo("Tahoma", 8, true, false, "text", xls.NONE, xls.NONE, xls.NONE);
            titulo2 = xls.nuevoEstilo("Tahoma", 8, true, false, "text", HSSFColor.WHITE.index, HSSFColor.GREEN.index, HSSFCellStyle.ALIGN_CENTER, 2);
            letra = xls.nuevoEstilo("Tahoma", 8, false, false, "", xls.NONE, xls.NONE, xls.NONE);
            dinero = xls.nuevoEstilo("Tahoma", 8, false, false, "#,##0.00", xls.NONE, xls.NONE, xls.NONE);
            dinero2 = xls.nuevoEstilo("Tahoma", 8, false, false, "#,##0", xls.NONE, xls.NONE, xls.NONE);
            porcentaje = xls.nuevoEstilo("Tahoma", 8, false, false, "0.00%", xls.NONE, xls.NONE, xls.NONE);
            ftofecha = xls.nuevoEstilo("Tahoma", 8, false, false, "yyyy-mm-dd", xls.NONE, xls.NONE, xls.NONE);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }

    }

    /**
     * Escribe la respuesta de una opcion ejecutada desde AJAX
     *
     * @param respuesta
     * @param contentType
     * @throws Exception
     */
    private void printlnResponse(String respuesta, String contentType) throws Exception {

        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);
    }
}
