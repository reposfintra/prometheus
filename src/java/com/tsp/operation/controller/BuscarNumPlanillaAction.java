 /*
  * BuscarNumPlanillaAction.java
  *
  * Created on 12 de septiembre de 2005, 06:38 PM
  *
  * To change this template, choose Tools | Options and locate the template under
  * the Source Creation and Management node. Right-click the template and choose
  * Open. You can then make changes to the template in the Source Editor.
  */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*; 
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

/**
 *
 * @author Armando Oviedo
 */
public class BuscarNumPlanillaAction extends Action{
    /** Creates a new instance of BuscarNumPlanillaAction */
    public BuscarNumPlanillaAction() {
    }
    public void run() throws javax.servlet.ServletException, InformationException{
        String next="/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina");
        try{
            String numpla = request.getParameter("numpla").toUpperCase();
            boolean esdm = model.rmtService.esDespachoManual(numpla);
            if(esdm){
                model.rmtService.BuscarPlanillaDM(numpla);
                
            }
            else{
                model.rmtService.BuscarPlanilla(numpla);
            }
            
            DatosPlanillaRMT dp = model.rmtService.getDatosPlanillaRMT();
            
            if(dp!=null){
                
                model.rmtService.buscarClientes(numpla);
                model.stdjobdetselService.buscaStandard(dp.getStdjob());
                String front = "";
                if(model.stdjobdetselService.getStandardDetSel()!=null){
                    Stdjobdetsel s = model.stdjobdetselService.getStandardDetSel();
                    String wo = s.getWoType();
                    if(wo.equals("RM")){
                        front = "PA" ;
                    }
                    else if(wo.equals("RC")){
                        front = "CU" ;
                    }
                    else if(wo.equals("RC")){
                        front = "IP";
                    }
                }
                
                //RM ES PARAGUACHON
                //RC ES CUCUTA-SAN ANTONIO
                //RE ES IPIALES - TULCAN
                dp.setFrontera(front);
                model.rmtService.BuscarPlanillasCaravana();//kreales
                model.tblgensvc.buscarLista("REP", "REP_TRA");
                model.rmtService.BuscarTiposubicacion();
                model.rmtService.BuscarUbicaciones("PC");
                model.rmtService.SearchLastCreatedReport();
                if(model.rmtService.getUltRMT()!=null) {
                    model.rmtService.getNextPC();
                }
                model.rmtService.buscarPlanillas(dp.getNumpla(), dp.getPto_control_proxreporte());
            }
            
            next+="?tiporeporte=En Via&tubicacion=PC";
            
            if(request.getParameter("observacionb")!=null){
                next="/jsp/trafico/Movimiento_trafico/ingresarObservacion.jsp";
            }
        }
        catch(SQLException ex){
            ex.printStackTrace();
            throw new ServletException(ex.getMessage());
        }
        this.dispatchRequest(next);
    }
}
