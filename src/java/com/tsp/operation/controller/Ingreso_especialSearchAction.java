/*******************************************************************
 * Nombre clase: Ingreso_especialSearchAction.java
 * Descripci�n: Accion para buscar un acuerdo especial a la bd.
 * Autor: Ing. Jose de la rosa
 * Fecha: 7 de diciembre de 2005, 08:23 AM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

public class Ingreso_especialSearchAction extends Action{
    
    /** Creates a new instance of Ingreso_especialSearchAction */
    public Ingreso_especialSearchAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next="";
        String codigo = "";
        String clase = "";
        String tipo = "";
        HttpSession session = request.getSession ();
        String dstrct = (String) session.getAttribute ("Distrito");
        String listar = (String) request.getParameter ("listar");
        try{
            if (listar.equals ("True")){
                next="/jsp/equipos/ingreso_especial/ingreso_especialListar.jsp";
                String sw = (String) request.getParameter ("sw");
                if(sw.equals ("True")){
                    tipo = request.getParameter ("c_tipo_acuerdo").toUpperCase ();
                    codigo = request.getParameter ("c_codigo_concepto").toUpperCase ();
                    clase = request.getParameter ("c_clase_equipo").toUpperCase ();
                }
                else{
                    tipo = "";
                    codigo = "";
                    clase = "";
                }
                model.ingreso_especialService.searchDetalleIngreso_especial(tipo,codigo,clase,dstrct);
            }
            else{
                tipo = request.getParameter ("c_tipo_acuerdo").toUpperCase ();
                codigo = request.getParameter ("c_codigo_concepto").toUpperCase ();
                clase = request.getParameter ("c_clase_equipo").toUpperCase ();
                model.ingreso_especialService.searchIngreso_especial (tipo, codigo, clase, dstrct);
                Ingreso_especial in = model.ingreso_especialService.getIngreso_especial ();
                model.concepto_equipoService.searchConcepto_equipo (in.getCodigo_concepto());
                next="/jsp/equipos/ingreso_especial/ingreso_especialModificar.jsp";
            }
        }catch (SQLException e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
    
}
