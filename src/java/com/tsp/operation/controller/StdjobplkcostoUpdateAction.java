/*
 * StdjobplkcostoUpdateAction.java
 *
 * Created on 3 de febrero de 2005, 11:36 AM
 */

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  KREALES
 */
public class StdjobplkcostoUpdateAction extends Action{
    
    /** Creates a new instance of StdjobplkcostoUpdateAction */
    public StdjobplkcostoUpdateAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String next                 =   "/tarifas_diferenciales/sjgrupUpdate.jsp";
        String grupo                =   request.getParameter("grupo").toUpperCase();
        
        
        try{
            
            if(request.getParameter("buscar")!=null){
                
                model.stdjobplkcostoService.buscaStd(grupo);
                if(model.stdjobplkcostoService.getStd()!=null){
                    request.setAttribute("std", model.stdjobplkcostoService.getStd());
                }
              
                
            }
            else{
                String dstrct               =   request.getParameter("distrito").toUpperCase();
                String activacion           =   request.getParameter("activacion");
                String ind_trip             =   request.getParameter("ind").toUpperCase();
                float costo                 =   Float.parseFloat(request.getParameter("costo"));
                float unit_cost             =   Float.parseFloat(request.getParameter("unit_cost"));
                String unit_transp          =   request.getParameter("unit_transp").toUpperCase();
                String moneda               =   request.getParameter("moneda").toUpperCase();
                
                HttpSession session         =   request.getSession();
                Usuario usuario             =   (Usuario) session.getAttribute("Usuario");
                String user_update          =   usuario.getLogin();
                
                request.setAttribute("error", "#99cc99");
                Stdjobplkcosto stdjobplkcosto = new Stdjobplkcosto();
                stdjobplkcosto.setDstrct(dstrct);
                stdjobplkcosto.setGroup_code(grupo);
                stdjobplkcosto.setActivation_date(activacion);
                stdjobplkcosto.setInd_trip(ind_trip);
                stdjobplkcosto.setCosto_unitario(costo);
                stdjobplkcosto.setUnit_cost(unit_cost);
                stdjobplkcosto.setUnit_transp(unit_transp);
                stdjobplkcosto.setCurrency(moneda);
                stdjobplkcosto.setCreation_user(usuario.getLogin());
                
                
                model.stdjobplkcostoService.setStd(stdjobplkcosto);
                model.stdjobplkcostoService.update();
                
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
    
}
