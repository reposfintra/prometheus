/*************************************************************************
 * Nombre ......................InfoactModificarAction.java              *
 * Descripci�n..................Clase Action para modificar INFOactividad    *
 * Autor........................Ing. Diogenes Antonio Bastidas Morales   *
 * Fecha........................3 de septiembre de 2005, 12:49 PM        *
 * Versi�n......................1.0                                      *
 * Coyright.....................Transportes Sanchez Polo S.A.            *
 *************************************************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException; 
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
/**
 *
 * @author  Diogenes
 */
public class InfoactModificarAction extends Action {
    
    /** Creates a new instance of InfoactModificarAction */
    public InfoactModificarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina");
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String mensaje= "";
        
        Date fecha = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String now = format.format(fecha);
        double dur,tiem;
        String correos = "";
        
        InfoActividad infact = new InfoActividad();
        infact.setCodCliente( request.getParameter("cliente") );
        infact.setCodActividad(request.getParameter("acti"));
        infact.setNumpla(request.getParameter("numpla"));
        infact.setNumrem(request.getParameter("numrem"));
        
        infact.setFecini(Util.formatoFechaTimestamp(request.getParameter("fecini")) );
        infact.setFecfin(Util.formatoFechaTimestamp(request.getParameter("fecfin")) );
        
        
        if( request.getParameter("duracion")!=null && !request.getParameter("duracion").equals("") )
            dur=  Double.parseDouble(request.getParameter("duracion"));
        else
            dur=0d;
        infact.setDuracion(dur);
        
        infact.setDocumento((request.getParameter("documento")!=null)?request.getParameter("documento"):"");
        
        if (request.getParameter("tiempo")!=null && !request.getParameter("tiempo").equals("") )
            tiem=Double.parseDouble(request.getParameter("tiempo"));
        else
            tiem=0d;
        infact.setTiempoDemora(tiem);
        infact.setCausaDemora((request.getParameter("cdemora")!=null)?request.getParameter("cdemora"):"");
        infact.setResdemora((request.getParameter("resdem")!=null)?request.getParameter("resdem"):"");
        //Nuevos Campos
        infact.setCantrealizada((request.getParameter("cantrealizada")!=null)?request.getParameter("cantrealizada"):"");
        infact.setCantplaneada((request.getParameter("cantplaneada")!=null)?request.getParameter("cantplaneada"):"");
        
        //Nuevos Campos 19-12-2005
        infact.setReferencia1((request.getParameter("referencia1")!=null)?request.getParameter("referencia1"):"");
        infact.setReferencia2((request.getParameter("referencia2")!=null)?request.getParameter("referencia2"):"");
        infact.setRefnumerica1(Integer.parseInt( (request.getParameter("refnumerica1")!=null)?request.getParameter("refnumerica1"):"0") );
        infact.setRefnumerica2(Integer.parseInt((request.getParameter("refnumerica2")!=null)?request.getParameter("refnumerica2"):"0") );
        //***
        
        infact.setFeccierre((!request.getParameter("feccierre").equals("")&&request.getParameter("feccierre")!=null)?request.getParameter("feccierre"):"0099-01-01 00:00:00");
        String obs = (request.getParameter("c_obse")!=null)?request.getParameter("c_obse"):"";
        String obs1 = (request.getParameter("obs")!=null)?request.getParameter("obs"):"";
        infact.setObservacion(obs1 +" " +obs);
        infact.setCreation_user(usuario.getLogin());
        infact.setUser_update(usuario.getLogin());
        infact.setBase(usuario.getBase());
        infact.setDstrct(usuario.getCia());
        infact.setLast_update(now);
        infact.setCreation_date(now);
        infact.setEstado("");
        
        try{
            
            model.actividadSvc.buscarActividad(request.getParameter("acti"), usuario.getCia());
            Actividad act = model.actividadSvc.ObtActividad();
            model.infoactService.modificarInfoActividad(infact);
            
            
            boolean a = model.infoactService.buscarInfoActividad(request.getParameter("acti"), request.getParameter("cliente"), usuario.getCia(), request.getParameter("numpla"), request.getParameter("numrem"));
            next=next+"?act="+a+"&men=Modificado";
            //Modifico Ingreso Trafico
            if (tiem > 0){
                model.rmtService.sumarDemoraAct(tiem,request.getParameter("numpla"));
                
                //Ingreso en reporte Trafico
                RepMovTrafico rmt = new RepMovTrafico();
                rmt.setDstrct(usuario.getCia());
                rmt.setReg_Status("");
                rmt.setNumpla(request.getParameter("numpla"));
                rmt.setObservacion(request.getParameter("desact"));
                rmt.setTipo_procedencia("ADU");
                rmt.setUbicacion_procedencia(usuario.getId_agencia());
                rmt.setTipo_reporte("En Via");
                rmt.setFechareporte(now);
                rmt.setLast_update(now);
                rmt.setUpdate_user(usuario.getLogin());
                rmt.setCreation_date(now);
                rmt.setCreation_user(usuario.getLogin());
                rmt.setBase(usuario.getBase());
                rmt.setZona(usuario.getId_agencia());
                rmt.setCausa("");
                rmt.setClasificacion("");
                ////System.out.println("Busca Reporte");
                model.rmtService.BuscarReporteMovTraf(request.getParameter("numpla"));
                RepMovTrafico rmt1 = model.rmtService.getReporteMovTraf();
                if (rmt1 != null ){
                    ////System.out.println("Entro if");
                    rmt.setFec_rep_pla(rmt1.getFecha_prox_reporte());
                }
                else{
                    ////System.out.println("Entro else");
                    rmt.setFec_rep_pla("0099-01-01 00:00:00");
                }
                
                ////System.out.println("Inserta el reporte " );
                model.tService.crearStatement();
                model.tService.getSt().addBatch(model.rmtService.addRMT(rmt));
                model.tService.execute();
            }
            
            
            //verifico los campos que tiene la actividad para crear el mensaje
            if( act.getFecinicio().equals("S") ){
                mensaje= "\n\n "+act.getLargaFecini()+": " +infact.getFecini();
            }
            if (act.getFecfinal().equals("S")){
                mensaje+="\n "+act.getLargaFecfin()+": " +infact.getFecfin();
            }
            if(act.getDuracion().equals("S")){
                mensaje+="\n "+act.getLargaDuracion()+": " +infact.getDuracion();
            }
            if(act.getDocumento().equals("S")){
                mensaje+="\n "+act.getLargaDocumento()+": " +infact.getDocumento();
            }
            if(act.getTiempodemora().equals("S")){
                mensaje+="\n "+act.getLargaTiempodemora()+": " +infact.getTiempoDemora();
            }
            if(act.getCausademora().equals("S")){
                mensaje+="\n "+act.getLargaCausademora()+": " +infact.getCausaDemora();
            }
            if( act.getResdemora().equals("S") ){
                mensaje+="\n "+act.getLargaResdemora()+": " +infact.getResdemora();
            }
            if( act.getCantrealizada().equals("S") ){
                mensaje+="\n "+act.getLarga_cantrealizada()+": " +infact.getCantrealizada();
            }
            if( act.getCantplaneada().equals("S") ){
                mensaje+="\n "+act.getLarga_cantplaneada()+": " +infact.getCantplaneada();
            }
            if(act.getFeccierre().equals("S")){
                mensaje+="\n "+act.getLargaFeccierre()+": " +infact.getFeccierre();
            }
            if(act.getObservacion().equals("S")){
                mensaje+="\n "+act.getLargaObservacion()+": " +infact.getObservacion();
            }
            /*Enviar Email*/
            String mailInt="", mailExt="";
            boolean swInt = false, swExt = false;
            LinkedList lista = model.tablaGenService.obtenerInfoTablaGen("EMAIL", act.getPerfil());
            if(lista!=null){
                Iterator it = lista.iterator();
                while(it.hasNext()){
                    TablaGen t = (TablaGen)it.next();
                    ////System.out.println("-"+t.getDescripcion());
                    //si el email del cliente es interno
                    if(t.getReferencia().equals("I")){
                        //verifico si exsite una arroba en la descripcion
                        if( t.getDescripcion().indexOf("@") != -1){
                            mailInt += t.getDescripcion()+";";
                        }
                        //se obtiene el login del usuario y se busca el email
                        else{
                            model.usuarioService.searchUsuario( t.getDescripcion().toUpperCase() );
                            Usuario u = model.usuarioService.getUsuario();
                            if(!u.getEmail().equals(""))
                                mailInt += u.getEmail()+";";
                        }
                        swInt = true;
                    }
                    //si el email del cliente es externo
                    else{
                        //verifico si exsite una arroba en la descripcion
                        if( t.getDescripcion().indexOf("@") != -1){
                            mailExt += t.getDescripcion()+";";
                        }
                        else{
                            //se obtiene el login del usuario y se busca el email
                            model.usuarioService.searchUsuario( t.getDescripcion().toUpperCase() );
                            Usuario u = model.usuarioService.getUsuario();
                            if(!u.getEmail().equals(""))
                                mailExt += u.getEmail()+";";
                        }
                        swExt = true;
                    }
                }
                ////System.out.println("interno "+mailInt);
                ////System.out.println("externo "+mailExt);
                SendMail e = new SendMail();
                e.setEmailfrom("procesos@sanchezpolo.com");
                e.setSendername( "ACTIVIDAD "+ act.getDesLarga() + "MODIFICADA" );
                e.setRecstatus("A");
                e.setEmailcode("");
                e.setEmailcopyto("");
                e.setEmailsubject("ACTIVIDAD "+ act.getDesLarga() + "MODIFICADA");
                e.setEmailbody( mensaje );
                if(swInt==true){
                    mailInt = mailInt.substring(0, mailInt.length() - 1 );
                    e.setEmailto(mailInt);
                    e.setTipo("I");
                    model.sendMailService.sendMail(e);
                }
                if(swExt==true){
                    mailExt = mailExt.substring(0, mailExt.length() - 1 );
                    e.setEmailto(mailExt);
                    e.setTipo("E");
                    model.sendMailService.sendMail(e);
                }
                
            }
            
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
