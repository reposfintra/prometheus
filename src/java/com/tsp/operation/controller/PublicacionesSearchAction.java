/*
 * PublicacionesSearchAction.java
 *
 * Created on 11 de Abril de 2006, 10:42 AM
 */
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

/**
 * @author  Amartinez
 */
public class PublicacionesSearchAction  extends Action{
    
    
    static Logger logger = Logger.getLogger (SalidaValidarAction.class);
    /** Creates a new instance of AcpmSearchAction */
    public PublicacionesSearchAction () {
        
    }
    public void run () throws ServletException, InformationException {
        HttpSession session = request.getSession ();
        String distrito = (String) session.getAttribute ("Distrito");
        String perfil = (String) session.getAttribute ("Perfil");
        String inicio = (request.getParameter ("inicio")!=null)?request.getParameter ("inicio"):"";
        String next="/jsp/publicacion/InicioPublicacion.jsp";
        try{
            model.publicacionService.buscarPublicaciones (distrito, perfil, model.publicacionService.getUsuario());

            if(inicio.equals ("ok")){
                next="/inicio.jsp";
                //model.publicacionService.buscarPublicaciones (distrito, perfil, model.publicacionService.getUsuario());
                //model.publicacionService.contadorPublicaciones ();
            }
            else
                model.publicacionService.contadorPublicaciones ();
        }catch (Exception e){
            e.printStackTrace ();
        }
        this.dispatchRequest (next);
    }
    
}
