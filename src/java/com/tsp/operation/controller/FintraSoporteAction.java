/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.FintraSoporteDAO;
import com.tsp.operation.model.DAOS.impl.FintraSoporteImpl;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.FintraSoporteBeans;
import com.tsp.operation.model.beans.FintraSoporteBeansFacturacion;
import com.tsp.operation.model.beans.JXLRead;
import com.tsp.operation.model.beans.Usuario;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import com.tsp.operation.model.beans.POIWrite;
import com.tsp.util.EnviarSms;
import com.tsp.util.ExcelApiUtil;
import com.tsp.util.Util;
import com.tsp.util.UtilFinanzas;
import com.tsp.util.Utility;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.logging.Logger;
import org.apache.poi.hssf.util.HSSFColor;

/**
 *
 * @author mariana
 */
public class FintraSoporteAction extends Action {

    Usuario usuario = null;
    private final int CARGAR_COMPROBANTES = 1;
    private final int ACTUALIZAR_COMPROBANTES = 2;
    private final int ELIMINAR_COMPROBANTES = 3;
    private final int SUBIR_EXCEL = 4;
    private final int ACTUALIZAR_DOCUEMNTOS_OPERATIVOS = 5;
    private final int LIMPIAR_GRILLA_CAMBIO_OPERATIVO = 6;
    private final int CARGAR_DOCUMENTOS_PENDIENTES = 7;
    private final int CARGAR_TIPOS_DOCUMENTOS = 8;
    private final int CARGAR_DOCUMENTOS = 9;
    private final int EXPORTAR_EXCEL_DOCUMENTOS_OPERATIVOS = 10;
    private final int SUBIR_EXCEL_PREAPROBADOS = 11;
    private final int ACTUALIZAR_BASE_PREAPROBADOS_UNIVERSIDAD = 12;
    private final int CARGAR_ESTADO_OFERTAS = 13;
    private final int CARGAR_OFERTAS = 14;
    private final int CARGAR_ACCIONES = 15;
    private final int CARGAR_DISTRIBUCIONES = 16;
    private final int EXPORTAR_EXCEL_OFERTAS = 17;
    private final int EXPORTAR_EXCEL_ACCIONES = 18;
    private final int GUARDAR_DISTRIBUCION_MULTISERVICIO = 19;
    private final int CARGAR_TIPO_MULTISERVICIO = 20;
    private final int BUSCAR_MULTISERVICIO = 21;
    private final int GUARDAR_EXCLUIR_NUM_OS = 22;
    private final int CARGAR_EXCLUIDOS = 23;
    private final int GUARDAR_INCLUIDOS = 24;
    private final int OBTENER_UNIDADES_NEGOCIO = 25;
    private final int VERIFICAR_TIPO_SOLICITUD = 26;
    private final int CARGAR_TIPO_PROYECTO = 27;
    private final int GUARDAR_TIPO_PROYECTO = 28;
    private final int CARGAR_DISTRIBUCIONES_ASOC = 29;
    private final int CARGAR_TIPO_PROYECT_ASOC = 30;
    private final int GUARDAR_PROYECTO_SOLICITUD = 31;
    private final int QUITAR_PROYECTO_SOLICITUD = 32;
    private final int ACTUALIZAR_PROYECTO = 33;
    private final int CARGAR_DISTRIBUCIONES_LIBRES = 34;
    private final int CARGAR_SOLICITUDES_LIBRES = 35;
    private final int ANULAR_TIPO_PROYECTO = 36;
    private final int CARGAR_DISTRIBUCIONES_RELACIONADAS = 37;
    private final int GUARDAR_DISTRIBUCIONES_SOLICITUD = 38;
    private final int QUITAR_DISTRIBUCIONES_SOLICITUD = 39;
    private final int CARGAR_TIPO_CLIENTE = 40;
    private final int GUARDAR_TIPO_CLIENTE = 41;
    private final int ACTUALIZAR_TIPO_CLIENTE = 42;
    private final int CARGAR_LINEA_NEGOCIO = 43;
    private final int ACTUALIZAR_BASE_PREAPROBADOS = 44;
    private final int ANULAR_TIPO_CLIENTE = 45;
    private final int BUSCAR_MULTISERVICIO_CXP = 46;
    private final int BUSCAR_CLIENTE_MULTISERVICIO = 47;
    private final int CARGAR_ENDOSO_FACTURAS = 48;
    private final int CARGAR_NIT_CUSTODIA = 49;
    private final int CARGAR_CUTODIADOR = 50;
    private final int GUARDAR_FACTURA_ENDOSO = 51;
    private final int ACTUALIZAR_FACTURA_ENDOSO = 52;
    private final int BUSCAR_NEGOCIOS_POLIZA = 53;
    private final int EXPORTAR_EXCEL_REPORTE_POLIZAS = 54;
    private final int CARGAR_DOCUMENTOS_AJUSTE_PESO = 55;
    private final int SUBIR_ARCHIVO_AJUSTE_PESO = 56;
    private final int LIMPIAR_DOCUMENTOS_PENDIENTE_AJUSTE_PESO = 57;
    private final int AJUSTAR_PESO = 58;
    private final int OBTENER_UNIDADES_NEGOCIO_FC = 59;
    private final int GENERAR_PREAPROBADOS_FINTRACREDIT = 60;
    private final int GUARDAR_PREAPROBADOS_FINTRACREDIT = 61;
    private final int EXPORTAR_EXCEL_REPORTE_PREAPROBADOS = 62;
    private final int CONSULTAR_PREAPROBADOS_FINTRACREDIT = 63;
    private final int VALIDAR_USUARIO_GENERACION = 64;
    private final int CARGAR_CONTROL_CUENTAS_AP = 65;
    private final int CAMBIAR_ESTADO_CONTROL_CUENTAS_AP = 66;
    private final int CARGAR_UNIDADES_NEGOCIO = 67;
    private final int GUARDAR_CONTROL_CUENTAS_AP = 68;
    private final int UPDATE_CONTROL_CUENTAS_AP = 69;
    private final int CARGAR_COMBO_POLIZAS = 70;// 55;
    private final int CARGAR_COMBO_ASEGURADORAS = 71;// 56;
    private final int CARGAR_COMBO_TODAS_ASEGURADORAS = 72;// 57;
    private final int CARGAR_COMBO_CONVENIOS = 73;//58;
    private final int VERIFICAR_CODIGO_FASECOLDA = 74;//59;
    private final int CARGAR_ID_CONFIG_POLIZA = 75;//60;
    private final int ACTUALIZAR_PGV = 76;//61;
    private final int EXPORTAR_PDF_POLIZAS = 77;//62;
    private final int MARCAR_PROCESO_JURIDICO_PGV = 78;//63;
    private final int CARGAR_OBSERVACIONES = 79;
    private final int CARGAR_POLIZAS = 80;
    private final int GUARDAR_POLIZA = 81;
    private final int ACTUALIZAR_POLIZA = 82; 
    private final int CAMBIAR_ESTADO_POLIZA = 83;
    private final int CARGAR_CONVENIOS = 84;
    private final int ACTUALIZAR_CONVENIO = 85;
    private final int ACTUALIZAR_CONVENIOS_UNIDAD_NEGOCIO = 86;
    private final int CARGAR_CUOTAS_MANEJO = 87;
    private final int CAMBIAR_ESTADO_CUOTAS_MANEJO = 88;
    private final int ACTUALIZAR_CUOTA_MANEJO = 89;
    private final int REPORTE_COSTOS = 90;
    private final int DETALLE_COSTOS = 91;
    private final int EXPORTAR_EXCEL_DETALLE_COSTOS = 92;
    private final int EXPORTAR_EXCEL_COSTOS = 93; 
    private final int LISTAR_INSUMOS = 94;
    private final int ACTUALIZAR_INSUMO= 95;
    private final int CARGAR_VALORES= 96;
    private final int ACTUALIZAR_VALORES_PREDETERMINADOS= 97;
    private final int CARGAR_SUBCATEGORIAS= 98;
    private final int ACTUALIZAR_SUBCATEGORIA= 99;
    private final int CARGAR_CATEGORIAS= 100;
    private final int ACTUALIZAR_CATEGORIA= 101;
    private final int CARGAR_APU= 102;
    private final int ACTUALIZAR_APU= 103;
    private final int CARGAR_ESPECIFICACIONES= 104;
    private final int ACTUALIZAR_ESPECIFICACION= 105;   
    private final int CONSULTAR_CLASIFICACION_CLIENTES = 106;
    private final int EXPORTAR_EXCEL_CLASIFICACION_CLIENTES = 107;
    private final int OBTENER_UNIDADES_NEGOCIO_EDUCATIVO = 108;
    private final int CARGAR_IMPUESTO_ANIO = 109;
    private final int CARGAR_ANTICIPOS_ESTACIONES = 110;
    private final int AUTORIZAR_ANTICIPO = 111;
    private final int CARGAR_NEGOCIOS_PARA_RECOLECCION = 112;
    private final int MARCAR_RECOLECCION_FIRMAS = 113;
    private final int BUSCAR_NEGOCIO_AREACTIVAR = 114;
    private final int REACTIVAR_NEGOCIO = 115;
    private final int CARGAR_NUEVAS_POLIZAS = 116;
    private final int INSERTAR_NUEVAS_POLIZAS = 117;
    private final int ACTUALIZAR_NUEVAS_POLIZAS = 118;
    private final int CAMBIAR_ESTADO_NUEVAS_POLIZAS = 119;
    private final int CARGAR_ASEGURADORAS = 120;
    private final int INSERTAR_ASEGURADORAS = 121;
    private final int ACTUALIZAR_ASEGURADORAS = 122;
    private final int CAMBIAR_ESTADO_ASEGURADORAS = 123;
    private final int CARGAR_TIPO_COBRO = 124;
    private final int INSERTAR_TIPO_COBRO = 125;
    private final int ACTUALIZAR_TIPO_COBRO = 126;
    private final int CAMBIAR_ESTADO_TIPO_COBRO = 127;
    private final int CARGAR_TIPO_VALOR_POLIZA = 128;
    private final int INSERTAR_VALOR_POLIZA = 129;
    private final int ACTUALIZAR_VALOR_POLIZA = 130;
    private final int CAMBIAR_ESTADO_VALOR_POLIZA = 131;
    private final int CARGAR_UNIDAD_NEGOCIO = 132;
    private final int CARGAR_CONFIGURACION_POLIZA = 133;
    private final int INSERTAR_CONFIGURACION_POLIZA = 134;
    private final int ACTUALIZAR_CONFIGURACION_POLIZA = 135;
    private final int CAMBIAR_ESTADO_CONFIGURACION_POLIZA = 136;
    private final int CARGAR_SUCURSALES = 137;
    private final int CARGAR_CIUDADES = 138;
    private final int CARGAR_FILTRO_CONFIGURACION_POLIZA = 139;
    private final int APLICAR_CONFIGURACION_POLIZAS_SUCURSALES = 140; //APLICAR LAS CONFIGURACIONES A TODAS LAS SUCURSALES
    private final int CARGAR_ARCHIVO_FACTURACION = 141;
    private final int CARGAR_EXTRACTOS_POR_GENERAR  = 142;
    private final int CARGAR_COMBO_EXTRACTOS_POR_GENERAR  = 143;
    private final int GENERAR_EXTRACTOS_DIGITALES = 144;
    private final int CARGAR_EXTRACTOS_POR_ENVIAR_SMS = 145;
    private final int ENVIAR_SMS_EXTRACTOS = 146;
    private final int CARGAR_COMBO_EXTRACTOS_POR_ENVIAR_SMS = 147;

    private FintraSoporteDAO dao;

    POIWrite xls;
    private int fila = 0;
    HSSFCellStyle header, titulo1, titulo2, titulo3, titulo4, titulo5, letra, numero, dinero, dinero2, numeroCentrado, porcentaje, letraCentrada, numeroNegrita, ftofecha;
    private SimpleDateFormat fmt;
    String filename;
    String rutaInformes;
    String nombre;
    List listado;
    String path;
    String reponseJson = "{}";
    String unidad_negocio;

    @Override
    public void run() throws ServletException, InformationException {
        try {

            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            dao = new FintraSoporteImpl(usuario.getBd());
            int opcion = (request.getParameter("opcion") != null ? Integer.parseInt(request.getParameter("opcion")) : -1);
            switch (opcion) {
                case CARGAR_COMPROBANTES:
                    cargarComprobante();
                    break;
                case ACTUALIZAR_COMPROBANTES:
                    actualizarComprobante();
                    break;
                case ELIMINAR_COMPROBANTES:
                    eliminarComprobante();
                    break;
                case SUBIR_EXCEL:
                    subirArchivo();
                    break;
                case ACTUALIZAR_DOCUEMNTOS_OPERATIVOS:
                    actualizarDocumentos();
                    break;
                case LIMPIAR_GRILLA_CAMBIO_OPERATIVO:
                    limpiar();
                    break;
                case CARGAR_DOCUMENTOS_PENDIENTES:
                    cargarDocumentosPendientes();
                    break;
                case CARGAR_TIPOS_DOCUMENTOS:
                    cargarTiposDocumentos();
                    break;
                case CARGAR_DOCUMENTOS:
                    cargarDocumentos();
                    break;
                case EXPORTAR_EXCEL_DOCUMENTOS_OPERATIVOS:
                    exportardocumentosOperativos();
                    break;
                case SUBIR_EXCEL_PREAPROBADOS:
                    subirArchivoImportacionPreAprob();
                    break;
                case ACTUALIZAR_BASE_PREAPROBADOS_UNIVERSIDAD:
                    unidad_negocio = (request.getParameter("unidad_negocio") != null) ? request.getParameter("unidad_negocio") : "0";
                    actualizarPreAprobadosUniversidad(unidad_negocio);
                    break;
                case CARGAR_ESTADO_OFERTAS:
                    cargarEstadoOfertas();
                    break;
                case CARGAR_OFERTAS:
                    cargarOfertas();
                    break;
                case CARGAR_ACCIONES:
                    cargarAcciones();
                    break;
                case CARGAR_DISTRIBUCIONES:
                    cargarDistribuciones();
                    break;
                case EXPORTAR_EXCEL_OFERTAS:
                    exportarExcelOfertas();
                    break;
                case EXPORTAR_EXCEL_ACCIONES:
                    exportarExcelAcciones();
                    break;
                case GUARDAR_DISTRIBUCION_MULTISERVICIO:
                    guardarDistribucionesOfertas();
                    break;
                case CARGAR_TIPO_MULTISERVICIO:
                    cargarTipoMultiservicio();
                    break;
                case BUSCAR_MULTISERVICIO:
                    buscarMultiservicio();
                    break;
                case GUARDAR_EXCLUIR_NUM_OS:
                    guardarExcluidos();
                    break;
                case CARGAR_EXCLUIDOS:
                    cargarExcluidos();
                    break;
                case GUARDAR_INCLUIDOS:
                    guardarIncluidos();
                    break;
                case OBTENER_UNIDADES_NEGOCIO:
                    cargarUnidadesNegocio();
                    break;
                case VERIFICAR_TIPO_SOLICITUD:
                    verificarTipoSolicitud();
                    break;
                case CARGAR_TIPO_PROYECTO:
                    cargarTipoProyecto();
                    break;
                case GUARDAR_TIPO_PROYECTO:
                    guardarTipoPtoyecto();
                    break;
                case CARGAR_DISTRIBUCIONES_ASOC:
                    cargarDistribucionesASoc();
                    break;
                case CARGAR_TIPO_PROYECT_ASOC:
                    cargarTipoProyectASoc();
                    break;
                case GUARDAR_PROYECTO_SOLICITUD:
                    guardarRelacionTipoProyectSoli();
                    break;
                case QUITAR_PROYECTO_SOLICITUD:
                    quitarRelacionTipoProyectSoli();
                    break;
                case ACTUALIZAR_PROYECTO:
                    actualizarTipoPtoyecto();
                    break;
                case CARGAR_DISTRIBUCIONES_LIBRES:
                    cargarDistribucionesLibres();
                    break;
                case CARGAR_SOLICITUDES_LIBRES:
                    cargarSolicitudesLibres();
                    break;
                case ANULAR_TIPO_PROYECTO:
                    anularTipoPtoyecto();
                    break;
                case CARGAR_DISTRIBUCIONES_RELACIONADAS:
                    cargarDistribucionASoc();
                    break;
                case GUARDAR_DISTRIBUCIONES_SOLICITUD:
                    guardarRelacionDistrSoli();
                    break;
                case QUITAR_DISTRIBUCIONES_SOLICITUD:
                    quitarRelacionDistrSoli();
                    break;
                case CARGAR_TIPO_CLIENTE:
                    cargarTipoCliente();
                    break;
                case GUARDAR_TIPO_CLIENTE:
                    guardarTipoCliente();
                    break;
                case ACTUALIZAR_TIPO_CLIENTE:
                    actualizarTipoCliente();
                    break;
                case CARGAR_LINEA_NEGOCIO:
                    cargarLineaNegocio();
                    break;
                case ACTUALIZAR_BASE_PREAPROBADOS:
                    unidad_negocio = (request.getParameter("unidad_negocio") != null) ? request.getParameter("unidad_negocio") : "0";
                    actualizarPreAprobados(unidad_negocio);
                    break;
                case ANULAR_TIPO_CLIENTE:
                    anularTipoCliente();
                    break;
                case BUSCAR_MULTISERVICIO_CXP:
                    buscarMultiservicioAsoc();
                    break;
                case BUSCAR_CLIENTE_MULTISERVICIO:
                    buscarClienteMultiservicio();
                    break;
                case CARGAR_ENDOSO_FACTURAS:
                    cargarEndosoFacturas();
                    break;
                case CARGAR_NIT_CUSTODIA:
                    CustodiadaPor();
                    break;
                case CARGAR_CUTODIADOR:
                    Custodiador();
                    break;
                case GUARDAR_FACTURA_ENDOSO:
                    guardarFacturaEndoso();
                    break;
                case ACTUALIZAR_FACTURA_ENDOSO:
                    actualizarFacturaEndoso();
                    break;
                case BUSCAR_NEGOCIOS_POLIZA:
                    cargarNegociosPoliza();
                    break;
                case EXPORTAR_EXCEL_REPORTE_POLIZAS:
                    exportarReportePolizas();
                    break;
                case CARGAR_DOCUMENTOS_AJUSTE_PESO:
                    cargarDocumentosAjustepeso();
                    break;
                case SUBIR_ARCHIVO_AJUSTE_PESO:
                    subirArchivoAjustePeso();
                    break;
                case LIMPIAR_DOCUMENTOS_PENDIENTE_AJUSTE_PESO:
                    limpiarAjustePeso();
                    break;
                case AJUSTAR_PESO:
                    AjustarPeso();
                    break;
                case OBTENER_UNIDADES_NEGOCIO_FC:
                    cargarUnidadesNegocioFintraCredit();
                    break;
                case GENERAR_PREAPROBADOS_FINTRACREDIT:
                    cargarPreAprobadosGenerados();
                    break;
                case GUARDAR_PREAPROBADOS_FINTRACREDIT:
                    guardarPreAprobadosFintraCredit();
                    break;
                case EXPORTAR_EXCEL_REPORTE_PREAPROBADOS:
                    exportarReportePreAprobados();
                    break;
                case CONSULTAR_PREAPROBADOS_FINTRACREDIT:
                    consultarPreAprobados();
                    break;
                case VALIDAR_USUARIO_GENERACION:
                    isAllowToGeneratePreAprobado();
                    break;
                case CARGAR_CONTROL_CUENTAS_AP:
                    CargarControlCuentas();
                    break;
                case CAMBIAR_ESTADO_CONTROL_CUENTAS_AP:
                    cambiarEstadoControlCuentas();
                    break;
                case CARGAR_UNIDADES_NEGOCIO:
                    cargarUnidadNegocio();
                    break;
                case GUARDAR_CONTROL_CUENTAS_AP:
                    guardarControlCuentas();
                    break;
                case UPDATE_CONTROL_CUENTAS_AP:
                    updateControlCuentas();
                    break;
                case CARGAR_COMBO_POLIZAS:
                    cargarComboTipoPolizas();
                    break;
                case CARGAR_COMBO_ASEGURADORAS:
                    cargarComboAseguradoras();
                    break;
                case CARGAR_COMBO_TODAS_ASEGURADORAS:
                    cargarComboTodasAseguradoras();
                    break;
                case CARGAR_COMBO_CONVENIOS:
                    cargarComboConvenios();
                    break;
                case VERIFICAR_CODIGO_FASECOLDA:
                    verificarCodigoFasecolda();
                    break;
                case CARGAR_ID_CONFIG_POLIZA:
                    cargarConfigPoliza();
                    break;
                case ACTUALIZAR_PGV:
                    ActualizarPGV();
                    break;
                case EXPORTAR_PDF_POLIZAS:
                    exportarPdfPolizas();
                    break;
                case MARCAR_PROCESO_JURIDICO_PGV:
                    marcarPGV();
                    break;
                case CARGAR_OBSERVACIONES:
                    cargarObservaciones();
                    break;
                case CARGAR_POLIZAS:
                    cargarPolizas();
                    break;
                case GUARDAR_POLIZA:
                    guardarPoliza();
                    break;
                case ACTUALIZAR_POLIZA:
                    actualizarPoliza();
                    break;
                case CAMBIAR_ESTADO_POLIZA:
                    cambiarEstadoPoliza();
                    break;
                case CARGAR_CONVENIOS:
                    cargarConvenios();
                    break;
                case ACTUALIZAR_CONVENIO:
                    actualizarConvenio();
                    break;    
                case ACTUALIZAR_CONVENIOS_UNIDAD_NEGOCIO:
                    actualizarConveniosxUnidadNegocio();
                    break;
                case CARGAR_CUOTAS_MANEJO:
                    CargarCuotaManejo();
                    break;
                case CAMBIAR_ESTADO_CUOTAS_MANEJO:
                    cambiarEstadoCuotaManejo();
                    break;
                case ACTUALIZAR_CUOTA_MANEJO:
                    actualizarCuotaManejo();
                    break;
                case REPORTE_COSTOS:
                    reportecostos();
                    break;
                case DETALLE_COSTOS:
                    detallecostos();
                    break;
                case EXPORTAR_EXCEL_DETALLE_COSTOS:
                    exportarExcelDetalleCostos();
                    break;
                case EXPORTAR_EXCEL_COSTOS:
                    exportarExcel();
                    break;
                case LISTAR_INSUMOS:
                    listarInsumos();
                    break;
                case ACTUALIZAR_INSUMO:
                    ActualizarInsumo();
                    break;
                case CARGAR_VALORES:
                    cargarValores();
                    break;
                case ACTUALIZAR_VALORES_PREDETERMINADOS:
                    actualizarValores();
                    break;
                case CARGAR_SUBCATEGORIAS:
                    cargarSubCategorias();
                    break;
                case ACTUALIZAR_SUBCATEGORIA:
                    actualizarSubcategoria();
                    break;
                case CARGAR_CATEGORIAS:
                    cargarCategorias();
                    break;
                case ACTUALIZAR_CATEGORIA:
                    actualizarCategoria();
                    break;
                case CARGAR_APU:
                    cargarApu();
                    break;
                case ACTUALIZAR_APU:
                    actualizarApu();
                    break;
                case CARGAR_ESPECIFICACIONES:
                    cargarEspecificaciones();
                    break;
                case ACTUALIZAR_ESPECIFICACION:
                    actualizarEspecificacion();
                    break;
                case CONSULTAR_CLASIFICACION_CLIENTES:
                    consultarClasificacionClientes();
                    break;
                case EXPORTAR_EXCEL_CLASIFICACION_CLIENTES:
                    exportarExcelClasificacionClientes();
                    break;
                case OBTENER_UNIDADES_NEGOCIO_EDUCATIVO:
                    cargarUnidadesNegocioEducativo();
                    break;
                case CARGAR_IMPUESTO_ANIO:
                    cargarAnioImpuesto();
                    break;
                case CARGAR_ANTICIPOS_ESTACIONES:
                    cargarAnticipos();
                    break;
                case AUTORIZAR_ANTICIPO:
                    autorizarAnticipo();
                    break;
                case CARGAR_NEGOCIOS_PARA_RECOLECCION:
                    mostrarPresolicitud();
                    break; 
                case MARCAR_RECOLECCION_FIRMAS:
                    marcarRecoleccionFirmas();
                    break;
                case BUSCAR_NEGOCIO_AREACTIVAR:
                    buscarNegocioAreactivar();
                    break;
                case REACTIVAR_NEGOCIO:
                    reactivarNegocio();
                    break;
                case CARGAR_NUEVAS_POLIZAS:
                    cargarNuevasPolizas();
                    break;
                case INSERTAR_NUEVAS_POLIZAS:
                    insertarNuevaPoliza();
                    break;
                case ACTUALIZAR_NUEVAS_POLIZAS:
                    actualizarNuevaPoliza();
                    break;
                case CAMBIAR_ESTADO_NUEVAS_POLIZAS:
                    cambiarEstadoNuevaPoliza();
                    break;
                case CARGAR_ASEGURADORAS:
                    cargarAseguradoras();
                    break;
                case INSERTAR_ASEGURADORAS:
                    insertarAseguradoras();
                    break;
                case ACTUALIZAR_ASEGURADORAS:
                    actualizarAseguradoras();
                    break;
                case CAMBIAR_ESTADO_ASEGURADORAS:
                    cambiarEstadoAseguradora();
                    break;
                case CARGAR_TIPO_COBRO:
                    cargarTipoCobro();
                    break;
                case INSERTAR_TIPO_COBRO:
                    insertarTipoCobro();
                    break;
                case ACTUALIZAR_TIPO_COBRO:
                    actualizarTipoCobro();
                    break;
                case CAMBIAR_ESTADO_TIPO_COBRO:
                    cambiarEstadoTipoCobro();
                    break;
                case CARGAR_TIPO_VALOR_POLIZA:
                    cargarValorPoliza();
                    break;
                case INSERTAR_VALOR_POLIZA:
                    insertarValorPoliza();
                    break;
                case ACTUALIZAR_VALOR_POLIZA:
                    actualizarValorPoliza();
                    break;
                case CAMBIAR_ESTADO_VALOR_POLIZA:
                    cambiarEstadoValorPoliza();
                    break;
                case CARGAR_CONFIGURACION_POLIZA:
                    cargarConfiguracionPoliza();
                    break;
                case INSERTAR_CONFIGURACION_POLIZA:
                    insertarConfiguracionPoliza();
                    break;
                case ACTUALIZAR_CONFIGURACION_POLIZA:
                    actualizarConfiguracionPoliza();
                    break;
                case CAMBIAR_ESTADO_CONFIGURACION_POLIZA:
                    cambiarEstadoConfiguracionPoliza();
                    break;
                case CARGAR_SUCURSALES:
                    cargarSucursales();
                    break;              
                case CARGAR_CIUDADES:
                    cargarCiudades();
                    break;
                case CARGAR_FILTRO_CONFIGURACION_POLIZA:
                    cargarFiltroConfiguracionPoliza();
                    break;
                case APLICAR_CONFIGURACION_POLIZAS_SUCURSALES:
                    aplicarConfiguraciones();
                    break;
                case CARGAR_ARCHIVO_FACTURACION:
                    subirArchivoFacturacion();
                    break;
                case CARGAR_EXTRACTOS_POR_GENERAR:
                    cargarExtractosPorGenerar();
                    break;
                case CARGAR_COMBO_EXTRACTOS_POR_GENERAR:
                    cargarComboExtractosPorGenerar();
                    break;
                case GENERAR_EXTRACTOS_DIGITALES:
                    generarExtractosDigitales();
                    break;
                case CARGAR_EXTRACTOS_POR_ENVIAR_SMS:
                    cargarExtractosPorEnviarSms();
                    break;
                case ENVIAR_SMS_EXTRACTOS:
                    enviarSmsExtracto();
                    break;
                case CARGAR_COMBO_EXTRACTOS_POR_ENVIAR_SMS:
                    cargarComboExtractosPorEnviarSms();
                    break;


            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cargarComprobante() {
        try {
            Gson gson = new Gson();
            String comprobante = request.getParameter("comprobante");
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.cargarComprobante(comprobante)) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(FintraSoporteAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void actualizarComprobante() {
        Gson gson = new Gson();
        String json = "";
        try {
            String comprobante = request.getParameter("comprobante");
            json = dao.actualizarComprobante(comprobante);
            this.printlnResponse(json, "application/json;");

        } catch (Exception ex) {
            json = "{\"respuesta\":\"ERROR\"}";
            ex.printStackTrace();
        }
    }

    public void eliminarComprobante() {
        String json = "{\"respuesta\":\"Eliminado\"}";
        try {
            String comprobante = request.getParameter("comprobante");
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.eliminarComprobanteCabecera(comprobante));
            tService.getSt().addBatch(this.dao.eliminarComprobanteDetalle(comprobante));
            tService.execute();
            tService.closeAll();
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            json = "{\"respuesta\":\"ERROR\"}";
            ex.printStackTrace();
        }
    }

    public void subirArchivo() throws FileNotFoundException, IOException {
        MultipartRequest mreq = null;
        try {

            String ruta = UtilFinanzas.obtenerRuta("ruta", "/exportar/migracion/"
                    + ((usuario != null) ? usuario.getLogin() : "anonimo"));

            //Obtenemos campos de formulario (Tanto de texto como archivo) (enctype=multipart/form-data)
            mreq = new MultipartRequest(request, ruta, 4096 * 2048, new DefaultFileRenamePolicy());
            String filetypeCredito = mreq.getContentType("examinar");
            String filename = mreq.getFilesystemName("examinar");
            ruta += "/" + filename;

            Gson gson = new Gson();
            File archivo = new File(ruta);
            listado = new LinkedList();
            ArrayList<FintraSoporteBeans> lista = new ArrayList<>();
            JXLRead xls = new JXLRead(archivo.getAbsolutePath());
            xls.obtenerHoja("documentos");
            String[] datos = null;
            int numfil = xls.numeroFilas();

            for (int i = 1; i < numfil; i++) {
                FintraSoporteBeans beansoport = new FintraSoporteBeans();
                beansoport.setTipo(xls.obtenerDato(i, 0));
                beansoport.setDocumento(xls.obtenerDato(i, 1));
                beansoport.setTipo_documento(xls.obtenerDato(i, 2));
                beansoport.setNit(xls.obtenerDato(i, 3));
                beansoport.setBanco(xls.obtenerDato(i, 4));
                beansoport.setSucursal(xls.obtenerDato(i, 5));
                beansoport.setCambio(xls.obtenerDato(i, 6));
                beansoport.setFecha_nueva((xls.obtenerDato(i, 7)).equals("") ? "0099-01-01" : (xls.obtenerDato(i, 7)));
                beansoport.setPeriodo(xls.obtenerDato(i, 8));
                beansoport.setPeriodo_nuevo(xls.obtenerDato(i, 9));
                beansoport.setGrupo_transaccion(xls.obtenerDato(i, 10));

                lista.add(beansoport);
            }

            String info = "{\"json\":" + gson.toJson(lista) + "}";
            guardarExcelTemp(info);
            String json = "{\"json\":\"OK\"}";
            this.printlnResponse(json, "application/json;");
            xls.cerrarLibro();
            mreq.getFile("examinar").delete();
        } catch (Exception e) {
            mreq.getFile("examinar").delete();
            e.printStackTrace();
            System.out.println(e);
        }
    }

    public void guardarExcelTemp(String info) {
        try {
            Gson gson = new Gson();
            JsonObject obj = (JsonObject) (new JsonParser()).parse(info);
            String json = this.dao.guardarExcelTemp(obj, usuario);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void actualizarDocumentos() {
        Gson gson = new Gson();
        String json = "";
        try {
            json = "{\"page\":1,\"rows\":" + gson.toJson(dao.actualizarDocumentos(usuario)) + "}";
            this.printlnResponse(json, "application/json;");

        } catch (Exception ex) {
            json = "{\"respuesta\":\"ERROR\"}";
            ex.printStackTrace();
        }
    }

    public void limpiar() {
        Gson gson = new Gson();
        String json = "";
        try {
            json = "{\"rows\":" + gson.toJson(dao.limpiar()) + "}";
            this.printlnResponse(json, "application/json;");

        } catch (Exception ex) {
            json = "{\"respuesta\":\"ERROR\"}";
            ex.printStackTrace();
        }
    }

    public void cargarDocumentosPendientes() {
        try {
            Gson gson = new Gson();
            String json = "{\"json\":" + gson.toJson(dao.cargarDocumentosPendientes()) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(FintraSoporteAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarTiposDocumentos() {
        try {
            Gson gson = new Gson();
            String json = "{\"json\":" + gson.toJson(dao.cargarTiposDocumentos()) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(FintraSoporteAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarDocumentos() {
        try {
            Gson gson = new Gson();
            String fecha_inicio = request.getParameter("fecha_inicio");
            String fecha_final = request.getParameter("fecha_fin");
            String user = request.getParameter("user");
            String tipo_documento = request.getParameter("tipo_documento");
            String json = "{\"json\":" + gson.toJson(dao.cargarDocumentos(fecha_inicio, fecha_final, user, tipo_documento)) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void distribucionesMultiservicio() {
        try {
            String nombreMultiservicio = request.getParameter("nombre_multiservicio");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cargarEstadoOfertas() {
        try {
            this.printlnResponse(dao.cargarEstadoOfertas(), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarOfertas() {
        try {
            // Gson gson = new Gson();
            String estado_oferta = request.getParameter("estado_oferta") == null ? "" : request.getParameter("estado_oferta");
            String fechaInicio = request.getParameter("fechainicio") == null ? "" : request.getParameter("fechainicio");
            String fechaFin = request.getParameter("fechafin") == null ? "" : request.getParameter("fechafin");
            String multiservicio = request.getParameter("multiservicio") == null ? "" : request.getParameter("multiservicio");
            String cliente = request.getParameter("cliente") == null ? "" : request.getParameter("cliente");
            String lineaNeg = request.getParameter("lineanegocio") == null ? "" : request.getParameter("lineanegocio");
            String[] lineasNegocios;
            lineasNegocios = lineaNeg.split(",");
            String lineanegocio = "";
            int longitud = lineasNegocios.length;
            for (int i = 0; i < longitud - 1; i++) {
                lineanegocio = lineanegocio + "'" + lineasNegocios[i] + "',";
            }
            lineanegocio = lineanegocio + "'" + lineasNegocios[longitud - 1] + "'";

            // String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.cargarOfertas(estado_oferta, fechaInicio, fechaFin, multiservicio, cliente, lineanegocio)) + "}";
            this.printlnResponse(dao.cargarOfertas(estado_oferta, fechaInicio, fechaFin, multiservicio, cliente, lineanegocio), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(FintraSoporteAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarAcciones() {
        try {
            Gson gson = new Gson();
            String id_solicitud = request.getParameter("id_solicitud") == null ? "" : request.getParameter("id_solicitud");
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.cargarAcciones(id_solicitud)) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(FintraSoporteAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarDistribuciones() {
        try {
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.cargarDistribuciones()) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(FintraSoporteAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarDistribucionesLibres() {
        try {
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.cargarDistribucionesLibres()) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(FintraSoporteAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void buscarMultiservicio() {
        try {
            Gson gson = new Gson();
            String multiservicio = request.getParameter("multiservicio") == null ? "" : request.getParameter("multiservicio");
            String cliente = request.getParameter("cliente") == null ? "" : request.getParameter("cliente");
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.buscarMultiservicio(multiservicio, cliente)) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(FintraSoporteAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void buscarMultiservicioAsoc() {
        try {
            // String multiservicio = request.getParameter("multiservicio") == null ? "" : request.getParameter("multiservicio");
            String json = dao.buscarMultiservicio(usuario);
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(FintraSoporteAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void buscarClienteMultiservicio() {
        try {
            Gson gson = new Gson();
            String multiservicio = request.getParameter("multiservicio") == null ? "" : request.getParameter("multiservicio");
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.buscarClienteMultiservicio(multiservicio)) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(FintraSoporteAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void guardarExcluidos() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            JsonArray obj = (JsonArray) (new JsonParser()).parse(request.getParameter("listJson"));
            json = this.dao.guardarExcluidos(usuario, obj);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void guardarIncluidos() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            JsonArray obj = (JsonArray) (new JsonParser()).parse(request.getParameter("listJson"));
            json = this.dao.guardarIncluidos(usuario, obj);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void guardarDistribucionesOfertas() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String distribucion = request.getParameter("distribucion");
            String opav = request.getParameter("opav");
            String fintra = request.getParameter("fintra");
            String interv = request.getParameter("interv");
            String provin = request.getParameter("provin");
            String eca = request.getParameter("eca");
            String iva = request.getParameter("iva");
            String vlr_agregado = request.getParameter("vlr_agregado");
            String tipo_numos = request.getParameter("tipo_numos") == null ? "" : request.getParameter("tipo_numos");
            TransaccionService tService = new TransaccionService(this.usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.guardarDistribuciones(usuario, distribucion));
            tService.getSt().addBatch(this.dao.guardarDistribuciones(usuario, distribucion, tipo_numos));
            tService.execute();
            this.dao.guardarDistribuciones(usuario, distribucion, opav, fintra, interv, provin, eca, iva, vlr_agregado);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void cargarTipoMultiservicio() {
        try {
            this.printlnResponse(dao.cargarTipoMultiservicio(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(FintraSoporteAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void verificarTipoSolicitud() {
        try {
            String distribucion = request.getParameter("distribucion");
            this.printlnResponse(dao.verificarTipoSolicitud(distribucion), "text/plain;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(FintraSoporteAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarTipoProyecto() {
        try {
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.cargarTipoProyecto()) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(FintraSoporteAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void guardarTipoPtoyecto() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String tipo_proyecto = request.getParameter("tipo_proyecto");
            this.dao.guardarTipoProyecto(usuario, tipo_proyecto);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void actualizarTipoPtoyecto() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String tipo_proyecto = request.getParameter("tipo_proyecto");
            String cod_proyecto = request.getParameter("cod_proyecto");
            this.dao.actualizarTipoProyecto(usuario, tipo_proyecto, cod_proyecto);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void anularTipoPtoyecto() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String cod_proyecto = request.getParameter("cod_proyecto");
            this.dao.anularTipoProyecto(usuario, cod_proyecto);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void cargarDistribucionesASoc() {
        try {
            Gson gson = new Gson();
            // String cod_proyecto = request.getParameter("cod_proyecto");
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.cargarDistribucionesASoc()) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(FintraSoporteAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarTipoProyectASoc() {
        try {
            Gson gson = new Gson();
            String cod_proyecto = request.getParameter("cod_proyecto");
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.cargarTipoProyectASoc(cod_proyecto)) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(FintraSoporteAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void guardarRelacionTipoProyectSoli() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String cod_proyecto = request.getParameter("cod_proyecto");
            JsonArray obj = (JsonArray) (new JsonParser()).parse(request.getParameter("listJson"));
            json = this.dao.guardarRelacionTipoProyectSoli(usuario, obj, cod_proyecto);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void quitarRelacionTipoProyectSoli() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String cod_proyecto = request.getParameter("cod_proyecto");
            JsonArray obj = (JsonArray) (new JsonParser()).parse(request.getParameter("listJson"));
            json = this.dao.quitarRelacionTipoProyectSoli(obj, cod_proyecto);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void cargarSolicitudesLibres() {
        try {
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.cargarSolicitudesLibres()) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(FintraSoporteAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarDistribucionASoc() {
        try {
            Gson gson = new Gson();
            String cod_distribucion = request.getParameter("cod_distribucion");
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.cargarDistribucionASoc(cod_distribucion)) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(FintraSoporteAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void quitarRelacionDistrSoli() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String cod_distribucion = request.getParameter("cod_distribucion");
            JsonArray obj = (JsonArray) (new JsonParser()).parse(request.getParameter("listJson"));
            json = this.dao.quitarRelacionDistrSoli(obj, cod_distribucion);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void guardarRelacionDistrSoli() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String cod_distribucion = request.getParameter("cod_distribucion");
            JsonArray obj = (JsonArray) (new JsonParser()).parse(request.getParameter("listJson"));
            json = this.dao.guardarRelacionDistrSoli(obj, cod_distribucion);
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public void cargarTipoCliente() {
        try {
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.cargarTipoCliente()) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(FintraSoporteAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void guardarTipoCliente() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String tipocliente = request.getParameter("tipocliente");
            this.dao.guardarTipoCliente(usuario, tipocliente);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void actualizarTipoCliente() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String tipoCliente = request.getParameter("tipoCliente");
            String cod_tipoCliente = request.getParameter("cod_tipoCliente");
            this.dao.actualizarTipoCliente(usuario, tipoCliente, cod_tipoCliente);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void cargarEndosoFacturas() {
        try {
            this.printlnResponse(dao.cargarEndosoFacturas(), "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void CustodiadaPor() {
        try {
            this.printlnResponse(dao.CustodiadaPor(), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void Custodiador() {
        try {
            String nit = request.getParameter("nit");
            this.printlnResponse(dao.Custodiador(nit), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void guardarFacturaEndoso() {
        String json = "{\"respuesta\":\"Guardado:)\"}";
        try {

            String nomproces = request.getParameter("nomproces");
            String nit = request.getParameter("nit");
            String custodiador = request.getParameter("custodiador");
            String cuenta = request.getParameter("cuenta");
            String cmc = request.getParameter("cmc");
            this.dao.guardarFacturaEndoso(nomproces, nit, custodiador, cuenta, cmc, usuario);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void actualizarFacturaEndoso() {
        String json = "{\"respuesta\":\"Guardado:)\"}";
        try {

            String nomproces = request.getParameter("nomproces");
            String nit = request.getParameter("nit");
            String custodiador = request.getParameter("custodiador");
            String cuenta = request.getParameter("cuenta");
            String cmc = request.getParameter("cmc");
            String id = request.getParameter("id");
            this.dao.actualizarFacturaEndoso(nomproces, nit, custodiador, cuenta, cmc, usuario, id);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void cargarLineaNegocio() {
        try {
            this.printlnResponse(dao.cargarLineaNegocio(), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(FintraSoporteAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void anularTipoCliente() {
        String json = "{\"respuesta\":\"Actualizado\"}";
        try {
            String cod_tipoCliente = request.getParameter("cod_tipoCliente");
            this.dao.anularTipoCliente(usuario, cod_tipoCliente);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void cargarExcluidos() {
        try {
            Gson gson = new Gson();
            String multiservicio = request.getParameter("multiservicio") == null ? "" : request.getParameter("multiservicio");
            String id_cliente = request.getParameter("id_cliente") == null ? "" : request.getParameter("id_cliente");
            String json = "{\"page\":1,\"rows\":" + gson.toJson(dao.cargarExcluidos(multiservicio, id_cliente)) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cargarNegociosPoliza() {
        try {
            String polizas = request.getParameter("polizas") == null ? "" : request.getParameter("polizas");
            String tipo_poliza = request.getParameter("tipo_poliza") == null ? "" : request.getParameter("tipo_poliza");
            String aseguradora = request.getParameter("aseguradora") == null ? "" : request.getParameter("aseguradora");
            String convenio = request.getParameter("convenio") == null ? "" : request.getParameter("convenio");
            String cliente = request.getParameter("cliente") == null ? "" : request.getParameter("cliente");
            String estado = request.getParameter("estados") == null ? "" : request.getParameter("estados");
            String nombre_query = null;
            if (polizas.equals("SEGURO DE AUTOMOVIL")) {
                nombre_query = "SQL_CARGAR_NEGOCIOS_POLIZA";
            }
            this.printlnResponse(dao.cargarNegociosPoliza(polizas, tipo_poliza, aseguradora, convenio, cliente, estado, nombre_query), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(FintraSoporteAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarDocumentosAjustepeso() {
        try {
            this.printlnResponse(dao.cargarDocumentosAjustepeso(), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(FintraSoporteAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void subirArchivoAjustePeso() throws FileNotFoundException, IOException {
        MultipartRequest mreq = null;
        try {

            String ruta = UtilFinanzas.obtenerRuta("ruta", "/exportar/migracion/"
                    + ((usuario != null) ? usuario.getLogin() : "anonimo"));

            //Obtenemos campos de formulario (Tanto de texto como archivo) (enctype=multipart/form-data)
            mreq = new MultipartRequest(request, ruta, 4096 * 2048, new DefaultFileRenamePolicy());
            String filetypeCredito = mreq.getContentType("examinar");
            String filename = mreq.getFilesystemName("examinar");
            ruta += "/" + filename;

            Gson gson = new Gson();
            File archivo = new File(ruta);
            listado = new LinkedList();
            ArrayList<FintraSoporteBeans> lista = new ArrayList<>();
            JXLRead xls = new JXLRead(archivo.getAbsolutePath());
            xls.obtenerHoja("documentos");
            String[] datos = null;
            int numfil = xls.numeroFilas();

            for (int i = 1; i < numfil; i++) {
                FintraSoporteBeans beansoport = new FintraSoporteBeans();
                beansoport.setFactura(xls.obtenerDato(i, 0));
                beansoport.setCodcli(xls.obtenerDato(i, 1));
                beansoport.setDescripcion(xls.obtenerDato(i, 2));
                beansoport.setConcepto(xls.obtenerDato(i, 3));
                beansoport.setValor((xls.obtenerDato(i, 4)).equals("") ? "0.0" : (xls.obtenerDato(i, 4)));
                beansoport.setCuenta_ajuste(xls.obtenerDato(i, 5));
                beansoport.setBanco(xls.obtenerDato(i, 6));
                beansoport.setSucursal(xls.obtenerDato(i, 7));
                beansoport.setFecha_consignacion((xls.obtenerDato(i, 8)).equals("") ? "0099-01-01" : (xls.obtenerDato(i, 8)));
                lista.add(beansoport);
            }
            String info = "{\"json\":" + gson.toJson(lista) + "}";
            guardarExcelAjustePeso(info);
            String json = "{\"json\":\"OK\"}";
            this.printlnResponse(json, "application/json;");
            xls.cerrarLibro();
            mreq.getFile("examinar").delete();
        } catch (Exception e) {
            mreq.getFile("examinar").delete();
            e.printStackTrace();
            System.out.println(e);
        }
    }

    public void guardarExcelAjustePeso(String info) {
        try {
            Gson gson = new Gson();
            JsonObject obj = (JsonObject) (new JsonParser()).parse(info);
            String json = this.dao.guardarExcelAjustepeso(obj, usuario);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void limpiarAjustePeso() {
        Gson gson = new Gson();
        String json = "";
        try {
            json = "{\"rows\":" + gson.toJson(dao.limpiarAjustePeso()) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            json = "{\"respuesta\":\"ERROR\"}";
            ex.printStackTrace();
        }
    }

    public void AjustarPeso() {
        Gson gson = new Gson();
        String json = "";
        try {
            json = "{\"page\":1,\"rows\":" + gson.toJson(dao.AjustarPeso()) + "}";
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            json = "{\"respuesta\":\"ERROR\"}";
            ex.printStackTrace();
        }
    }

      public void CargarControlCuentas(){
        try {
            this.printlnResponse(dao.CargarControlCuentas(), "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cambiarEstadoControlCuentas() {
        Gson gson = new Gson();
        String json = "";
        try {
            String id = request.getParameter("id");
            json = "{\"rows\":" + gson.toJson(dao.cambiarEstadoControlCuentas(id)) + "}";
        } catch (Exception e) {
            json = "{\"respuesta\":\"ERROR\"}";
            e.printStackTrace();
        }
    }

    public void cargarUnidadNegocio() {
        try {
            this.printlnResponse(dao.cargarUnidadNegocio(), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(FintraSoporteAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void guardarControlCuentas() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String tiponegocio = request.getParameter("tiponegocio");
            String ixm = request.getParameter("ixm");
            String gac = request.getParameter("gac");
            String cabing = request.getParameter("cabing");
            String deting = request.getParameter("deting");
            this.dao.guardarControlCuentas(usuario, tiponegocio, ixm, gac, cabing, deting);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void updateControlCuentas() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String tiponegocio = request.getParameter("tiponegocio");
            String ixm = request.getParameter("ixm");
            String gac = request.getParameter("gac");
            String cabing = request.getParameter("cabing");
            String deting = request.getParameter("deting");
            String id = request.getParameter("id");
            String cod_controlcuentas = request.getParameter("cod_controlcuentas");
            this.dao.updateControlCuentas(usuario, tiponegocio, ixm, gac, cabing, deting, id, cod_controlcuentas);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void cargarComboTipoPolizas() {
        try {
            this.printlnResponse(dao.cargarComboTipoPolizas(), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarComboAseguradoras() {
        try {
            String poliza = request.getParameter("poliza");
            this.printlnResponse(dao.cargarComboAseguradoras(poliza), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarComboConvenios() {
        try {
            this.printlnResponse(dao.cargarComboConvenios(), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void cargarComboTodasAseguradoras() {
        try {
            this.printlnResponse(dao.cargarComboTodasAseguradoras(), "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void verificarCodigoFasecolda() {
        try {
            String codigo_fasecolda = request.getParameter("codigo_fasecolda");
            this.printlnResponse(dao.verificarCodigoFasecolda(codigo_fasecolda), "text/plain;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void cargarConfigPoliza() {
        try {
            String poliza = request.getParameter("poliza") == null ? "" : request.getParameter("poliza");
            String aseguradora = request.getParameter("aseguradora") == null ? "" : request.getParameter("aseguradora");
            this.printlnResponse(dao.cargarConfigPoliza(poliza, aseguradora), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(FintraSoporteAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    public void ActualizarPGV() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String id_config_poliza = request.getParameter("id_config_poliza");
            String neg_vehiculo = request.getParameter("neg_vehiculo");
            String nomcliente = request.getParameter("nomcliente");
            String nit = request.getParameter("nit");
            String afiliado = request.getParameter("afiliado");
            String placa = request.getParameter("placa");
            String servicio = request.getParameter("servicio");
            String Vlrpoliza = request.getParameter("Vlrpoliza");
            String fecha_inicio = request.getParameter("fecha_inicio");
            String fecha_fin = request.getParameter("fecha_fin");
            String codigo_fasecolda = request.getParameter("codigo_fasecolda");
            String tipo = request.getParameter("tipo");

            this.dao.ActualizarPGV(usuario, id_config_poliza, neg_vehiculo, nomcliente, nit, afiliado, placa, servicio, Vlrpoliza, fecha_inicio, fecha_fin, codigo_fasecolda, tipo);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void marcarPGV() {
        String json = "";
        try {
            String neg_vehiculo = request.getParameter("negocio_padre");
            String fecha_fin = request.getParameter("fecha_fin");
            String respuesta = this.dao.marcarPGV(usuario, neg_vehiculo, fecha_fin);
            json = "{\"respuesta\":\"" + respuesta + "\"}";
            //  this.dao.marcarPGV(usuario, neg_vehiculo, fecha_fin);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    public void exportarPdfPolizas() {
        String json = "{\"respuesta\":\"GENERADO\"}";
        try {
            JsonObject obj = (JsonObject) (new JsonParser()).parse(request.getParameter("info"));
            dao.datosPdf(usuario, obj);

            this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        }
    }

    public void cargarObservaciones() {
        try {
            Gson gson = new Gson();
            String id_solicitud = request.getParameter("id_solicitud") == null ? "" : request.getParameter("id_solicitud");
            this.printlnResponse(dao.cargarObservaciones(id_solicitud), "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(FintraSoporteAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void exportarExcelOfertas() throws Exception {
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Reporte_ofertas", titulo = "REPORTE OFERTAS";

        cabecera = new String[]{"Id solicitud", "Nombre oferta", "Alcance", "Id cliente", "Nombre cliente", "Id cliente padre", "Nombe cliente padre", "Nic", "Nombre responsable", "Nombre interventor", "Tipo solicitud", "Fecha oferta", "Fecha inicial",
            "Fecha aceptacion pagos", "Multi-servicio", "Fecha orden trabajo", "Avance esperado", "Avance registrado", "Observaciones", "Fecha ultimo seguimiento", "Dias cronograma", "Fecha finalizacion cronograma", "Esquema", "Valor oferta", "Costo contratista", "Materia", "Mano de obra", "Transporte", "% Administracion",
            "Administracion", "% Imprevisto", "Imprevito", "% Utilidad", "Utilidad", "Bonificacion", "Opav", "Fintra", "Interventoria", "Provintegral", "Eca", "Base iva contratista", "Iva contratista", "Iva bonificacion", "Iva opav",
            "Iva fintra", "Iva interventoria", "Iva provintegral", "Iva eca", "Financiar sin iva", "Iva", "Financiar con iva"};

        dimensiones = new short[]{
            3000, 5000, 3000, 3000, 5000, 3000, 5000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000,
            3000, 3000, 5000, 3000, 5000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000,
            3000, 3000, 5000, 3000, 5000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000,
            3000, 3000, 5000, 3000, 5000, 3000
        };

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        this.printlnResponse(reponseJson, "text/plain");
    }

    private void exportarExcelAcciones() throws Exception {
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Reporte_acciones", titulo = "REPORTE ACCIONES";

        cabecera = new String[]{"Contratista", "Nombre contratista", "Nombre cliente", "Id accion", "estado", "Materia", "Mano de obra", "Transporte", "% Administracion",
            "Administracion", "% Imprevisto", "Imprevito", "% Utilidad", "Utilidad", "Descripcion", "Bonificacion", "Opav", "Fintra", "Interventoria",
            "Provintegral", "Eca", "Base iva contratista", "Iva contratista", "Iva bonificacion", "Iva opav", "Iva fintra", "Iva interventoria",
            "Iva provintegral", "Iva eca", "Financiar sin iva", "Iva", "Financiar con iva"};

        dimensiones = new short[]{
            3000, 5000, 5000, 3000, 3000, 5000, 3000, 5000, 3000, 3000, 3000, 3000, 3000, 3000, 3000,
            3000, 3000, 3000, 5000, 3000, 5000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000, 3000,
            3000, 3000
        };

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        this.printlnResponse(reponseJson, "text/plain");
    }

    private void exportardocumentosOperativos() throws Exception {
        try {
            String resp1 = "";
            String url = "";
            String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
            JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
            //Set<Map.Entry<String, JsonElement>> entrySet = object.entrySet();
            this.generarRUTA();
            this.crearLibro("DocumentosOperativos_", "Informacion");
            String[] cabecera = {"Tipo", "Documento", "Tipo Documento", "Nit", "Banco", "Sucursal", "Fecha Cotabilizacion", "Transaccion", "Periodo"};

            short[] dimensiones = new short[]{
                5000, 5000, 5000, 5000, 5000, 5000, 7000, 5000, 3000
            };
            this.generaTitulos(cabecera, dimensiones);

            for (int i = 0; i < asJsonArray.size(); i++) {
                JsonObject objects = (JsonObject) asJsonArray.get(i);
                fila++;
                int col = 0;
                xls.adicionarCelda(fila, col++, objects.get("tipo").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("documento").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("tipo_documento").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("nit").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("banco").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("sucursal").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("fecha_contabilizacion").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("transaccion").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("periodo").getAsString(), letra);

            }

            this.cerrarArchivo();

            fmt = new SimpleDateFormat("yyyMMdd HH:mm");
            url = request.getContextPath() + "/exportar/migracion/" + usuario.getLogin().toUpperCase() + "/DocumentosOperativos_" + fmt.format(new Date()) + ".xls";
            resp1 = Utility.getIcono(request.getContextPath(), 5) + "Se ha generado con exito.<br/><br/>"
                    + Utility.getIcono(request.getContextPath(), 7) + " <a href='" + url + "' >Ver Documento</a></td>";

            this.printlnResponse(resp1, "text/plain");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    private void generarRUTA() throws Exception {
        try {

            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            rutaInformes = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File(rutaInformes);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
    }

    private void crearLibro(String nameFileParcial, String titulo) throws Exception {
        try {
            fmt = new SimpleDateFormat("yyyMMdd HH:mm");
            this.crearArchivo(nameFileParcial + fmt.format(new Date()) + ".xls", titulo);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage());
        }
    }

    private void generaTitulos(String[] cabecera, short[] dimensiones) throws Exception {
        try {

            fila = 0;

            for (int i = 0; i < cabecera.length; i++) {
                xls.adicionarCelda(fila, i, cabecera[i], titulo2);
                if (i < dimensiones.length) {
                    xls.cambiarAnchoColumna(i, dimensiones[i]);
                }
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage());
        }
    }

    private void cerrarArchivo() throws Exception {
        try {
            if (xls != null) {
                xls.cerrarLibro();
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            throw new Exception("Error en crearArchivo ....\n" + ex.getMessage());
        }
    }

    private void crearArchivo(String nameFile, String titulo) throws Exception {
        try {
            InitArchivo(nameFile);
            xls.obtenerHoja("hoja1");
            // xls.combinarCeldas(0, 0, 0, 8);
            // xls.adicionarCelda(0,0, titulo, header);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en crearArchivo ....\n" + ex.getMessage());
        }
    }

    private void InitArchivo(String nameFile) throws Exception {
        try {
            xls = new com.tsp.operation.model.beans.POIWrite();
            nombre = "/exportar/migracion/" + usuario.getLogin() + "/" + nameFile;
            xls.nuevoLibro(rutaInformes + "/" + nameFile);
            header = xls.nuevoEstilo("Tahoma", 10, true, false, "text", HSSFColor.GREEN.index, xls.NONE, HSSFCellStyle.ALIGN_LEFT);
            titulo1 = xls.nuevoEstilo("Tahoma", 8, true, false, "text", xls.NONE, xls.NONE, xls.NONE);
            titulo2 = xls.nuevoEstilo("Tahoma", 8, true, false, "text", HSSFColor.WHITE.index, HSSFColor.GREEN.index, HSSFCellStyle.ALIGN_CENTER, 2);
            letra = xls.nuevoEstilo("Tahoma", 8, false, false, "", xls.NONE, xls.NONE, xls.NONE);
            dinero = xls.nuevoEstilo("Tahoma", 8, false, false, "#,##0.00", xls.NONE, xls.NONE, xls.NONE);
            dinero2 = xls.nuevoEstilo("Tahoma", 8, false, false, "#,##0", xls.NONE, xls.NONE, xls.NONE);
            porcentaje = xls.nuevoEstilo("Tahoma", 8, false, false, "0.00%", xls.NONE, xls.NONE, xls.NONE);
            ftofecha = xls.nuevoEstilo("Tahoma", 8, false, false, "yyyy-mm-dd", xls.NONE, xls.NONE, xls.NONE);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }

    }

    public void subirArchivoImportacionPreAprob() throws FileNotFoundException, IOException, Exception {
        MultipartRequest mreq = null;
        String resp = "";
        try {

            String ruta = UtilFinanzas.obtenerRuta("ruta", "/exportar/migracion/"
                    + ((usuario != null) ? usuario.getLogin() : "anonimo"));

            //Obtenemos campos de formulario (Tanto de texto como archivo) (enctype=multipart/form-data)
            String id_und_negocio = (request.getParameter("und_negocio") != null) ? request.getParameter("und_negocio") : "";
            mreq = new MultipartRequest(request, ruta, 4096 * 2048, new DefaultFileRenamePolicy());
            String filetype = mreq.getContentType("examinar");
            String filename = mreq.getFilesystemName("examinar");
            ruta += "/" + filename;

            Gson gson = new Gson();
            File archivo = new File(ruta);
            listado = new LinkedList();
            JXLRead xls = new JXLRead(archivo.getAbsolutePath());
            xls.obtenerHoja("Hoja1");
            String[] datos = null;
            int numfil = xls.numeroFilas();
            JsonArray arr = new JsonArray();
            JsonObject fila;

            for (int i = 1; i < numfil; i++) {
                fila = new JsonObject();
                fila.addProperty("nit", xls.obtenerDato(i, 0));
                fila.addProperty("nombre", xls.obtenerDato(i, 1));
                fila.addProperty("und_negocio", xls.obtenerDato(i, 2));
                fila.addProperty("negocio", xls.obtenerDato(i, 3));
                fila.addProperty("valor_ultimo_credito", xls.obtenerDato(i, 4));
                fila.addProperty("valor_aprobado", xls.obtenerDato(i, 5));
                if (!id_und_negocio.equals("12")) {
                    fila.addProperty("incremento", xls.obtenerDato(i, 6));
                    fila.addProperty("afiliado", xls.obtenerDato(i, 7));
                    fila.addProperty("fecha_desembolso", xls.obtenerDato(i, 8));
                    fila.addProperty("periodo_desembolso", xls.obtenerDato(i, 9));
                    fila.addProperty("fecha_vencimiento", xls.obtenerDato(i, 10));
                    fila.addProperty("direccion", xls.obtenerDato(i, 11));
                    fila.addProperty("telefono", xls.obtenerDato(i, 12));
                    fila.addProperty("departamento", xls.obtenerDato(i, 13));
                    fila.addProperty("ciudad", xls.obtenerDato(i, 14));
                    fila.addProperty("barrio", xls.obtenerDato(i, 15));
                    fila.addProperty("valor_saldo", xls.obtenerDato(i, 16));
                }

                arr.add(fila);
            }
            resp = "{\"success\":true,\"rows\":" + gson.toJson(arr) + "}";
        } catch (Exception e) {
            resp = "{\"success\":false,\"error\":" + e.getMessage() + "}";
            e.printStackTrace();
            System.out.println(e);
        } finally {
            this.printlnResponse(resp, "application/json;");
            xls.cerrarLibro();
            mreq.getFile("examinar").delete();
        }
    }

    public void actualizarPreAprobadosUniversidad(String id_und_negocio) throws Exception {
        String json = "";
        try {
            String listadoDocs = request.getParameter("listadoDocs");

            JsonParser jsonParser = new JsonParser();
            JsonObject jdocs = (JsonObject) jsonParser.parse(listadoDocs);
            JsonArray jsonArrDocs = jdocs.getAsJsonArray("preaprobados");
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            //Eliminamos base preaprobados existente
            tService.getSt().addBatch(dao.eliminarPreAprobadosUniversidad());
            //Actualizamos hist�rico aprobados para el periodo actual
            tService.getSt().addBatch(dao.anularHistPreAprobUnivActual(usuario));
            for (int i = 0; i < jsonArrDocs.size(); i++) {
                String negocio = jsonArrDocs.get(i).getAsJsonObject().get("negocio").getAsJsonPrimitive().getAsString();
                String nit = jsonArrDocs.get(i).getAsJsonObject().get("nit").getAsJsonPrimitive().getAsString();
                Double valor_ult_credito = jsonArrDocs.get(i).getAsJsonObject().get("valor_ult_credito").getAsJsonPrimitive().getAsDouble();
                Double valor_aprobado = jsonArrDocs.get(i).getAsJsonObject().get("valor_aprobado").getAsJsonPrimitive().getAsDouble();
                tService.getSt().addBatch(dao.insertarPreAprobadosUniversidad(negocio, nit, valor_ult_credito, valor_aprobado, usuario));
            }
            //Insertamos informaci�n cargada en tabla hist�rico preaprobados
            tService.getSt().addBatch(dao.insertarHistPreAprobadosUniversidad(id_und_negocio, usuario));

            tService.execute();
            tService.closeAll();
            json = "{\"respuesta\":\"OK\"}";
            this.printlnResponse(json, "application/json;");

        } catch (Exception ex) {
            json = "{\"respuesta\":\"ERROR\",\"message\":" + ex.getMessage() + "}";
            this.printlnResponse(json, "application/json;");
            ex.printStackTrace();
        }
    }

    public void actualizarPreAprobados(String id_und_negocio) throws Exception {
        String json = "";
        try {
            String listadoDocs = request.getParameter("listadoDocs");

            JsonParser jsonParser = new JsonParser();
            JsonObject jdocs = (JsonObject) jsonParser.parse(listadoDocs);
            JsonArray jsonArrDocs = jdocs.getAsJsonArray("preaprobados");
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            //Eliminamos base preaprobados existente para la unidad de negocio seleccionada
            tService.getSt().addBatch(dao.eliminarPreAprobados(id_und_negocio));
            //Actualizamos hist�rico aprobados para el periodo actual y unidad de negocio seleccionada
            tService.getSt().addBatch(dao.anularHistPreAprobadosActual(id_und_negocio, usuario));
            for (int i = 0; i < jsonArrDocs.size(); i++) {
                String nit = jsonArrDocs.get(i).getAsJsonObject().get("nit").getAsJsonPrimitive().getAsString();
                String nombre_cliente = jsonArrDocs.get(i).getAsJsonObject().get("nombre").getAsJsonPrimitive().getAsString();
                Double valor_ult_credito = jsonArrDocs.get(i).getAsJsonObject().get("valor_ult_credito").getAsJsonPrimitive().getAsDouble();
                Double valor_aprobado = jsonArrDocs.get(i).getAsJsonObject().get("valor_aprobado").getAsJsonPrimitive().getAsDouble();
                Double incremento = jsonArrDocs.get(i).getAsJsonObject().get("incremento").getAsJsonPrimitive().getAsDouble();
                String und_negocio = jsonArrDocs.get(i).getAsJsonObject().get("und_negocio").getAsJsonPrimitive().getAsString();
                String negocio = jsonArrDocs.get(i).getAsJsonObject().get("negocio").getAsJsonPrimitive().getAsString();
                String afiliado = jsonArrDocs.get(i).getAsJsonObject().get("afiliado").getAsJsonPrimitive().getAsString();
                String fecha_desembolso = jsonArrDocs.get(i).getAsJsonObject().get("fecha_desembolso").getAsJsonPrimitive().getAsString();
                String periodo_desembolso = jsonArrDocs.get(i).getAsJsonObject().get("periodo_desembolso").getAsJsonPrimitive().getAsString();
                String fecha_vencimiento = jsonArrDocs.get(i).getAsJsonObject().get("fecha_vencimiento").getAsJsonPrimitive().getAsString();
                String direccion = jsonArrDocs.get(i).getAsJsonObject().get("direccion").getAsJsonPrimitive().getAsString();
                String telefono = jsonArrDocs.get(i).getAsJsonObject().get("telefono").getAsJsonPrimitive().getAsString();
                String departamento = jsonArrDocs.get(i).getAsJsonObject().get("departamento").getAsJsonPrimitive().getAsString();
                String ciudad = jsonArrDocs.get(i).getAsJsonObject().get("ciudad").getAsJsonPrimitive().getAsString();
                String barrio = jsonArrDocs.get(i).getAsJsonObject().get("barrio").getAsJsonPrimitive().getAsString();
                Double valor_saldo = jsonArrDocs.get(i).getAsJsonObject().get("valor_saldo").getAsJsonPrimitive().getAsDouble();
                tService.getSt().addBatch(dao.insertarPreAprobados(id_und_negocio, nit, nombre_cliente, valor_ult_credito, valor_aprobado, incremento, und_negocio, negocio, afiliado, fecha_desembolso, periodo_desembolso, fecha_vencimiento, departamento, ciudad, direccion, barrio, telefono, valor_saldo, usuario));
            }
            //Insertamos informaci�n cargada en tabla hist�rico preaprobados
            tService.getSt().addBatch(dao.insertarHistPreAprobados(id_und_negocio, usuario));

            tService.execute();
            tService.closeAll();
            json = "{\"respuesta\":\"OK\"}";
            this.printlnResponse(json, "application/json;");

        } catch (Exception ex) {
            json = "{\"respuesta\":\"ERROR\",\"message\":" + ex.getMessage() + "}";
            this.printlnResponse(json, "application/json;");
            ex.printStackTrace();
        }
    }

    private void exportarReportePolizas() throws Exception {
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Reporte_polizas", titulo = "REPORTE POLIZAS";
        cabecera = new String[]{"Negocio padre", "Negocio policiza", "Nombre ", "Nit", "Codigo fasecolda", "Plazo", "Vlr negocio",
            "Vlr desembolso", "Afiliado", "Fecha primera cuota", "Fecha ultima cuota", "Fecha preaviso"};
        dimensiones = new short[]{
            4000, 5000, 7000, 3000, 5000, 5000, 3000, 5000, 7000, 5000, 5000, 5000
        };

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        this.printlnResponse(reponseJson, "text/plain");
    }

    private void cargarUnidadesNegocio() {
        try {
            String json = dao.cargarUnidadesNegocio(" WHERE ref_1 = 'NA'");
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void cargarUnidadesNegocioFintraCredit() {
        try {
            String json = dao.cargarUnidadesNegocio(" WHERE ref_4 != ''");
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void cargarPreAprobadosGenerados() {

        try {
            String unidad_negocio = request.getParameter("unidad_negocio") != null ? request.getParameter("unidad_negocio") : "0";
            String json = dao.generarPreAprobados("SQL_GENERAR_PREAPROBADOS", unidad_negocio, "");
            this.printlnResponse(json, "application/json;");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void guardarPreAprobadosFintraCredit() throws Exception {
        String json = "";
        try {
            String id_und_negocio = request.getParameter("unidad_negocio") != null ? request.getParameter("unidad_negocio") : "0";
            String listadoDocs = request.getParameter("listadoDocs");

            JsonParser jsonParser = new JsonParser();
            JsonObject jdocs = (JsonObject) jsonParser.parse(listadoDocs);
            JsonArray jsonArrDocs = jdocs.getAsJsonArray("preaprobados");
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            for (int i = 0; i < jsonArrDocs.size(); i++) {
                String negocio = jsonArrDocs.get(i).getAsJsonObject().get("negocio").getAsJsonPrimitive().getAsString();
                String periodo = jsonArrDocs.get(i).getAsJsonObject().get("periodo").getAsJsonPrimitive().getAsString();
                String cedula_deudor = jsonArrDocs.get(i).getAsJsonObject().get("cedula_deudor").getAsJsonPrimitive().getAsString();
                String nombre_deudor = jsonArrDocs.get(i).getAsJsonObject().get("nombre_deudor").getAsJsonPrimitive().getAsString();
                String telefono = jsonArrDocs.get(i).getAsJsonObject().get("telefono").getAsJsonPrimitive().getAsString();
                String celular = jsonArrDocs.get(i).getAsJsonObject().get("celular").getAsJsonPrimitive().getAsString();
                String direccion = jsonArrDocs.get(i).getAsJsonObject().get("direccion").getAsJsonPrimitive().getAsString();
                String barrio = jsonArrDocs.get(i).getAsJsonObject().get("barrio").getAsJsonPrimitive().getAsString();
                String ciudad = jsonArrDocs.get(i).getAsJsonObject().get("ciudad").getAsJsonPrimitive().getAsString();
                String email = jsonArrDocs.get(i).getAsJsonObject().get("email").getAsJsonPrimitive().getAsString();
                String cedula_codeudor = jsonArrDocs.get(i).getAsJsonObject().get("cedula_codeudor").getAsJsonPrimitive().getAsString();
                String nombre_codeudor = jsonArrDocs.get(i).getAsJsonObject().get("nombre_codeudor").getAsJsonPrimitive().getAsString();
                String telefono_codeudor = jsonArrDocs.get(i).getAsJsonObject().get("telefono_codeudor").getAsJsonPrimitive().getAsString();
                String celular_codeudor = jsonArrDocs.get(i).getAsJsonObject().get("celular_codeudor").getAsJsonPrimitive().getAsString();
                String cuotas = jsonArrDocs.get(i).getAsJsonObject().get("cuotas").getAsJsonPrimitive().getAsString();
                String id_convenio = jsonArrDocs.get(i).getAsJsonObject().get("id_convenio").getAsJsonPrimitive().getAsString();
                String afiliado = jsonArrDocs.get(i).getAsJsonObject().get("afiliado").getAsJsonPrimitive().getAsString();
                String tipo = jsonArrDocs.get(i).getAsJsonObject().get("tipo").getAsJsonPrimitive().getAsString();
                String fecha_desembolso = jsonArrDocs.get(i).getAsJsonObject().get("fecha_desembolso").getAsJsonPrimitive().getAsString();
                String fecha_ult_pago = jsonArrDocs.get(i).getAsJsonObject().get("fecha_ult_pago").getAsJsonPrimitive().getAsString();
                String dias_pagos = jsonArrDocs.get(i).getAsJsonObject().get("dias_pagos").getAsJsonPrimitive().getAsString();
                Double vr_negocio = jsonArrDocs.get(i).getAsJsonObject().get("vr_negocio").getAsJsonPrimitive().getAsDouble();
                Double valor_factura = jsonArrDocs.get(i).getAsJsonObject().get("valor_factura").getAsJsonPrimitive().getAsDouble();
                Double valor_saldo = jsonArrDocs.get(i).getAsJsonObject().get("valor_saldo").getAsJsonPrimitive().getAsDouble();
                Double porcentaje = jsonArrDocs.get(i).getAsJsonObject().get("porcentaje").getAsJsonPrimitive().getAsDouble();
                String altura_mora = jsonArrDocs.get(i).getAsJsonObject().get("altura_mora").getAsJsonPrimitive().getAsString();
                Double valor_preaprobado = jsonArrDocs.get(i).getAsJsonObject().get("valor_preaprobado").getAsJsonPrimitive().getAsDouble();
                tService.getSt().addBatch(dao.insertarPreAprobadosFintraCredit(id_und_negocio, periodo, negocio, cedula_deudor, nombre_deudor, telefono, celular, direccion, barrio, ciudad, email, cedula_codeudor, nombre_codeudor, telefono_codeudor, celular_codeudor, cuotas, id_convenio, afiliado, tipo, fecha_desembolso, fecha_ult_pago, dias_pagos, vr_negocio, valor_factura, valor_saldo, porcentaje, altura_mora, valor_preaprobado));
            }

            tService.execute();
            tService.closeAll();
            json = "{\"respuesta\":\"OK\"}";
            this.printlnResponse(json, "application/json;");

        } catch (Exception ex) {
            json = "{\"respuesta\":\"ERROR\",\"message\":" + ex.getMessage() + "}";
            this.printlnResponse(json, "application/json;");
            ex.printStackTrace();
        }
    }

    private void exportarReportePreAprobados() throws Exception {
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Reporte_preaprobados", titulo = "REPORTE PREAPROBADOS";
        cabecera = new String[]{"Unidad Negocio", "Periodo", "Negocio", "Cedula Deudor", "Nombre Deudor", "Telefono", "Celular", "Direccion",
            "Barrio", "Ciudad", "Email", "Cedula Codeudor", "Nombre Codeudor", "Telefono Codeudor", "Celular Codeudor", "Cuotas", "Id Convenio", "Afiliado",
            "Tipo", "F. Desembolso", "F. ult. Pago", "Dias ult. Pago", "Vr. Negocio", "Vr. Factura", "Vr. Saldo", "Porcentaje", "Altura Mora", "Vr. PreAprobado"};

        dimensiones = new short[]{
            4000, 4000, 5000, 5000, 8000, 5000, 5000, 5000, 5000, 5000, 6000, 5000, 8000, 5000,  4000, 4000, 7000, 5000, 5000, 5000, 5000, 4000, 5000, 5000, 5000,
            5000, 5000, 5000
        };

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        this.printlnResponse(reponseJson, "text/plain");
    }

    private void consultarPreAprobados() {

        try {
            String unidad_negocio = request.getParameter("unidad_negocio") != null ? request.getParameter("unidad_negocio") : "0";
            String periodo = request.getParameter("periodo") != null ? request.getParameter("periodo") : "";
            String json = dao.generarPreAprobados("SQL_LISTAR_PREAPROBADOS",unidad_negocio,periodo);
            this.printlnResponse(json, "application/json;");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void isAllowToGeneratePreAprobado() {
        try {
            String resp = "{}";
            if (dao.isAllowToGeneratePreAprobados(usuario.getLogin())) {
                resp = "{\"respuesta\":\"SI\"}";
                this.printlnResponse(resp, "application/json;");
            } else {
                resp = "{\"respuesta\":\"NO\"}";
                this.printlnResponse(resp, "application/json;");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void CargarCuotaManejo() {
        try {
            this.printlnResponseAjax(dao.CargarCuotaManejo(), "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cambiarEstadoCuotaManejo() {
        Gson gson = new Gson();
        String json = "";
        try {
            String id = request.getParameter("id");
            json = "{\"rows\":" + gson.toJson(dao.cambiarEstadoCuotaManejo(id)) + "}";
        } catch (Exception e) {
            json = "{\"respuesta\":\"ERROR\"}";
            e.printStackTrace();
        }
    }

    public void actualizarCuotaManejo() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String id = request.getParameter("id");
            String valor = request.getParameter("valor");
            this.dao.actualizarCuotaManejo(usuario, id, valor);
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }
    
    /**
     * Escribe la respuesta de una opcion ejecutada desde AJAX
     *
     * @param respuesta
     * @param contentType
     * @throws Exception
     */
    private void printlnResponse(String respuesta, String contentType) throws Exception {

        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);
    }

    /**
     * Metodo que indica si elo array contiene o no datos
     *
     * @autor mfontalvo
     * @param datos, Arreglo de String
     * @return boolean , estado del array
     */
    private boolean isEmpty(String[] datos) {
        boolean empty = true;
        if (datos != null) {
            for (int i = 0; i < datos.length && empty; i++) {
                if (datos[i] != null && !datos[i].trim().equals("")) {
                    empty = false;
                }
            }
        }
        return empty;
    }

    private void cargarPolizas() {
        try {
            this.printlnResponseAjax(dao.CargarPolizas(), "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public void guardarPoliza() {
        String json = "";
        try {
            String nombre_poliza = request.getParameter("nombre");
            String descripcion = request.getParameter("desc");
            this.dao.guardarPoliza(usuario, nombre_poliza, descripcion);
            json = "{\"respuesta\":\"Guardado\"}";
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void actualizarPoliza() {
        String json = "";
        try {
            String id = request.getParameter("id");
            String nombre_poliza = request.getParameter("nombre");
            String descripcion = request.getParameter("desc");
            this.dao.actualizarPoliza(usuario, nombre_poliza, descripcion, id);
            json = "{\"respuesta\":\"Guardado\"}";
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }
    
    public void cambiarEstadoPoliza() {
        Gson gson = new Gson();
        String json = "";
        try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            //String nombre_poliza = request.getParameter("nombre");
            //String descripcion = request.getParameter("desc");
            json = "{\"rows\":" + gson.toJson(dao.cambiarEstadoPoliza(usuario, id)) + "}";
        } catch (Exception e) {
            json = "{\"respuesta\":\"ERROR\"}";
            e.printStackTrace();
        }finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
        
        /*try {
            String id = request.getParameter("id") != null ? request.getParameter("id") : "";
            this.reponseJson = dao.cambiarEstadoPoliza(id, usuario);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(FintraSoporteAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }*/
    }

    private void cargarConvenios() {
    
    try {    
            //String unidad_negocio  = request.getParameter("unidad_negocio").replaceAll("-_-", "#");
            String unidad_negocio = request.getParameter("unidad_negocio");
            String json ="";
            json = (String) dao.cargarConvenios(unidad_negocio);
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(FintraSoporteAction.class.getName()).log(Level.SEVERE, null, ex);
        }    
    }
    
    
    

     
    private void actualizarConvenio(){
    String json = "{\"respuesta\":\"Guardado\"}";
    try {
            String id_convenio = request.getParameter("id");
            String convenio= request.getParameter("convenio");
            String tasa_interes = request.getParameter("tasa");
            String tasa_usura = request.getParameter("tasa_usura");
            String tasa_compra_cartera = request.getParameter("tasa_compra_cartera");
            String tasa_aval = request.getParameter("tasa_aval");
            String tasa_max_fintra = request.getParameter("tasa_max_fintra");
            String tasa_sic_ea = request.getParameter("tasa_sic_ea");
            TransaccionService tService = new TransaccionService(this.usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.actualizarConvenio(usuario, convenio, tasa_interes, tasa_usura, tasa_compra_cartera, id_convenio,tasa_aval,tasa_max_fintra,tasa_sic_ea));
            tService.getSt().addBatch(this.dao.historicoConvenios(usuario, convenio, tasa_interes, tasa_usura, tasa_compra_cartera, id_convenio,tasa_aval,tasa_max_fintra,tasa_sic_ea));
            tService.execute();
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void actualizarConveniosxUnidadNegocio() {
        String json = "{\"respuesta\":\"Guardado\"}";
        try {
            String unidad_negocio = request.getParameter("unidad_negocio");
            String tasa_interes = request.getParameter("tasa");
            String tasa_usura = request.getParameter("tasam");
            String tasa_compra_cartera = request.getParameter("tasac");
            String tasa_avalc = request.getParameter("tasa_avalc");
            TransaccionService tService = new TransaccionService(this.usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.actualizarConveniosxUnidadNegocio(usuario, unidad_negocio, tasa_interes, tasa_usura, tasa_compra_cartera,tasa_avalc));
            tService.getSt().addBatch(this.dao.historicoConveniosMasivo(usuario, unidad_negocio, tasa_interes, tasa_usura, tasa_compra_cartera,tasa_avalc));
            tService.execute();
        } catch (Exception e) {
             e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void reportecostos() {
        try {
           this.printlnResponseAjax(dao.reportecostos(), "application/json");
        }  catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void exportarExcelDetalleCostos() throws Exception {
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Reporte_detalle_costos_", titulo = "REPORTE DETALLE COSTOS";

        cabecera = new String[]{"Empresa", "Tipo referencia", "Multiservicio", "Nit proveedor", "Nombre proveedor","Fecha documento","Tipo documento","Documento",
                "Descripcion","Valor antes Iva","Valor iva","Total factura con iva","Valor pagado", "Total abonos","Fecha pago","Codigo orden","Valor orden compra", "Codigo cuenta"};

        dimensiones = new short[]{
            7000, 5000, 5000, 5000, 7000, 5000, 7000, 7000, 7000, 7000, 3000, 5000, 5000, 5000, 3000,
            5000, 5000, 5000
        };

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        this.printlnResponseAjax(reponseJson, "text/plain");
    }

    private void detallecostos() {
//        try {
//           this.printlnResponseAjax(dao.detallecostos(), "application/json");
//        }  catch (Exception e) {
//            e.printStackTrace();
//        }
//        
//    }
      try {    
            String multiservicio = request.getParameter("multiservicio");
            String id_solicitud = request.getParameter("id_solicitud");
            String json ="";
            json = (String) dao.detallecostos(multiservicio, id_solicitud);
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(FintraSoporteAction.class.getName()).log(Level.SEVERE, null, ex);
        }      
    }

    private void exportarExcel() throws Exception {
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Reporte_Costos_", titulo = "REPORTE COSTOS";

        cabecera = new String[]{"Multiservicio", "Id solicitud", "Nombre proyecto", "Nombre cliente", "Valor contratista antes iva", "Utilizado", "Rentabilidad", "% Administracion", "% Imprevisto",
            "% Utilidad", "Valor administracion", "Valor imprevisto", "Valor utilidad", "Valor contrato antes iva", "% Iva", "Valor iva", "% OPAV", "% FINTRA", "% PROVINTEGRAL","% INTERVENTORIA",
        "Total esquema","Sub total esquema","% ECA","Sub total eca","Total comision","Total"};

        dimensiones = new short[]{
            5000, 5000, 7000, 7000, 7000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 7000, 5000,
            5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000
        };

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        this.printlnResponseAjax(reponseJson, "text/plain");
    }

    private void listarInsumos() {
        try {
            this.printlnResponseAjax(dao.listarInsumos(), "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
        private void ActualizarInsumo() {
        String json = "";
        try {
            String id = request.getParameter("id");
            //Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1");
            String codigo_material = request.getParameter("codigo_material");
            String descripcion = Util.setCodificacionCadena(request.getParameter("descripcion"), "ISO-8859-1");
            this.dao.ActualizarInsumo(usuario, id, codigo_material, descripcion);
            json = "{\"respuesta\":\"Guardado\"}";
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }
        
    private void cargarValores() {
        try {
            this.printlnResponseAjax(dao.cargarValores(), "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }    

    private void actualizarValores() {
        String json = "";
        try {
            String id = request.getParameter("id");
            String valor_xdefecto = Util.setCodificacionCadena(request.getParameter("valor_xdefecto"), "ISO-8859-1");
            this.dao.actualizarValores(usuario, id, valor_xdefecto);
            json = "{\"respuesta\":\"Guardado\"}";
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void cargarSubCategorias() {
        try {
            this.printlnResponseAjax(dao.cargarSubCategorias(), "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void actualizarSubcategoria() {
        String json = "";
        try {
            String id = request.getParameter("id");
            String nombre = Util.setCodificacionCadena(request.getParameter("nombre"), "ISO-8859-1");
            this.dao.actualizarSubcategoria(usuario, id, nombre);
            json = "{\"respuesta\":\"Guardado\"}";
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }
    
    private void cargarCategorias() {
        try {
            this.printlnResponseAjax(dao.cargarCategorias(), "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void actualizarCategoria() {
        String json = "";
        try {
            String id = request.getParameter("id");
            String nombre = Util.setCodificacionCadena(request.getParameter("nombre"), "ISO-8859-1");
            this.dao.actualizarCategoria(usuario, id, nombre);
            json = "{\"respuesta\":\"Guardado\"}";
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void cargarApu() {
        try {
            this.printlnResponseAjax(dao.cargarApu(), "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void actualizarApu() {
        String json = "";
        try {
            String id = request.getParameter("id");
            String nombre = Util.setCodificacionCadena(request.getParameter("nombre"), "ISO-8859-1");
            this.dao.actualizarApu(usuario, id, nombre);
            json = "{\"respuesta\":\"Guardado\"}";
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }

    private void cargarEspecificaciones() {
        try {
            this.printlnResponseAjax(dao.cargarEspecificaciones(), "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void actualizarEspecificacion() {
        String json = "";
        try {
            String id = request.getParameter("id");
            String nombre = Util.setCodificacionCadena(request.getParameter("nombre"), "ISO-8859-1");
            this.dao.actualizarEspecificacion(usuario, id, nombre);
            json = "{\"respuesta\":\"Guardado\"}";
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponseAjax(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }
    
    private void consultarClasificacionClientes() {

        try {
            String unidad_negocio = request.getParameter("unidad_negocio") != null ? request.getParameter("unidad_negocio") : "0";
            String periodo = request.getParameter("periodo") != null ? request.getParameter("periodo") : "";
            String json = dao.consultarClasificacionClientes(unidad_negocio,periodo);
            this.printlnResponse(json, "application/json;");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void exportarExcelClasificacionClientes() throws Exception{
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        String[] cabecera = null;
        short[] dimensiones=null;
        String nombreArchivo="Reporte_clasificacion_clientes", titulo="REPORTE CLASIFICACION CLIENTES";
        
        cabecera = new String[]{"Id Unidad", "Periodo", "Negocio", "Cedula Deudor", "Nombre Deudor", "Telefono", "Celular", "Direccion", "Barrio", "Ciudad","Email", "Cedula Codeudor",
                                "Nombre Codeudor", "Telefono Codeudor", "Celular Codeudor", "Id Convenio", "Afiliado","Tipo", "F. Desembolso", "Clasificaci�n", "F. ult. Pago",
                                "Dia ult. pago", "Altura Mora M�x.", "Altura Mora Actual", "# Cuotas", "Cuotas Pagadas", "Cuotas x Pagar", "Cuotas Restantes",
                                "Vr. Negocio", "Vr. Factura", "Vr. Saldo", "% Cumpl.", "Vr. PreAprobado"};

        dimensiones = new short[]{
            4000, 4000, 5000, 5000, 8000, 5000, 5000, 5000, 5000, 5000, 6000, 5000,
            8000, 5000,  4000, 4000, 7000, 6000, 5000, 5000, 5000, 4000, 
            5000, 5000, 4000, 4000, 4000, 4000, 
            5000, 5000,5000, 4000, 6000
        };

      
        ExcelApiUtil apiUtil =new ExcelApiUtil(usuario);
        String resp =  apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        this.printlnResponse(resp, "text/plain");
    
    }
    
    private void cargarUnidadesNegocioEducativo() {
        try {
            String json = dao.cargarUnidadesNegocio(" WHERE id IN(2,8)");
            this.printlnResponse(json, "application/json;");
        } catch (Exception ex) {
            Logger.getLogger(ReestructuracionNegociosAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void cargarAnioImpuesto() {
        try {
            String anioImpuesto = request.getParameter("anio_impuesto") == null ? "" : request.getParameter("anio_impuesto");
            this.printlnResponseAjax(dao.cargarAnioImpuesto(anioImpuesto), "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarAnticipos() {
        try {
            String conductor = request.getParameter("conductor") != null ? request.getParameter("conductor") : "";
            String placa = request.getParameter("placa") != null ? request.getParameter("placa") : "";
            String planilla = request.getParameter("planilla") != null ? request.getParameter("planilla") : "";
            //this.printlnResponseAjax(dao.cargarAnticipos(fechaini, fechafin), "application/json");
            this.printlnResponseAjax(dao.cargarAnticipos(conductor, placa, planilla), "application/json");
        } catch (Exception e) {
            e.printStackTrace();
        }    
    }
    
    public void autorizarAnticipo() {
        Gson gson = new Gson();
        String json = "";
        try {
            String id = request.getParameter("id");
            json = "{\"rows\":" + gson.toJson(dao.autorizarAnticipo(id)) + "}";
        } catch (Exception e) {
            json = "{\"respuesta\":\"ERROR\"}";
            e.printStackTrace();
        }
    }

    private void mostrarPresolicitud() {
        try {
            String numero_solicitud = (request.getParameter("numero_solicitud") != null) ? request.getParameter("numero_solicitud") : "";
            this.printlnResponseAjax(dao.mostrarPresolicitud(numero_solicitud), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void marcarRecoleccionFirmas() {
        try {
            String numero_solicitud = (request.getParameter("numero_solicitud") != null) ? request.getParameter("numero_solicitud") : "";
            this.printlnResponseAjax(dao.marcarRecoleccionFirmas(numero_solicitud,  usuario), "application/json;");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void buscarNegocioAreactivar() {
         try {
            String cod_neg = (request.getParameter("cod_neg") != null) ? request.getParameter("cod_neg") : "";
            String fechaInicio = request.getParameter("fecha_ini") != null ? request.getParameter("fecha_ini") : "";
            String fechaFin = request.getParameter("fecha_fin") != null ? request.getParameter("fecha_fin") : "";
            this.printlnResponseAjax(dao.buscarNegocioAreactivar(cod_neg, fechaInicio, fechaFin), "application/json;");

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

 

    private void reactivarNegocio(){
        String json = "{\"respuesta\":\"Guardado\"}";
     try {
            String cod_neg = (request.getParameter("cod_neg") != null) ? request.getParameter("cod_neg") : "";
            String comentario = (request.getParameter("comentario") != null) ? request.getParameter("comentario") : "";
            TransaccionService tService = new TransaccionService(this.usuario.getBd());
            tService.crearStatement();
            tService.getSt().addBatch(this.dao.reactivarNegocio(usuario, cod_neg));
            tService.getSt().addBatch(this.dao.reactivarSolicitud(usuario, cod_neg));
            tService.getSt().addBatch(this.dao.insertarTrazabilidad(usuario, cod_neg, comentario));
            tService.execute();
        } catch (Exception e) {
            e.printStackTrace();
            json = "{\"respuesta\":\"" + e.getMessage() + "\"}";
        } finally {
            try {
                this.printlnResponse(json, "application/json;");
            } catch (Exception ex) {
                ex.printStackTrace();
                System.out.println(ex);
            }
        }
    }
    
    private void cargarNuevasPolizas() throws Exception {
        this.printlnResponseAjax(dao.cargarNuevasPolizas(), "application/json;");
    }

    private void insertarNuevaPoliza() throws Exception {
        String descripcion = (request.getParameter("descripcion") != null) ? request.getParameter("descripcion") : "";
        
        JsonObject response = new JsonObject();
        try {
            dao.insertarNuevasPolizas(usuario, descripcion);
            response.addProperty("respuesta", "Guardado");
        } catch (SQLException ex) {
            if (ex.getSQLState().equals("23505")) {
                response.addProperty("error", "Ya existe una p�liza con esta configuraci�n");
            } else {
                response.addProperty("error", "Error al guardar.");
            }
        }
        this.printlnResponseAjax(new Gson().toJson(response), "application/json;");
    }

    private void actualizarNuevaPoliza() throws Exception {
        String descripcion = (request.getParameter("descripcion") != null) ? request.getParameter("descripcion") : "";
        int id = request.getParameter("id") != null ? Integer.parseInt(request.getParameter("id")) : 0;

        JsonObject response = new JsonObject();
        if (dao.actualizarNuevasPolizas(usuario, descripcion, id)) {
            response.addProperty("respuesta", "Guardado");
        } else {
            response.addProperty("respuesta", "Error al guardar.");
        }
        this.printlnResponseAjax(new Gson().toJson(response), "application/json;");
    }

    private void cambiarEstadoNuevaPoliza() throws Exception {
        int id = (request.getParameter("id") != null) ? Integer.parseInt(request.getParameter("id")) : 0;
        String estado = (request.getParameter("status") != null) ? request.getParameter("status") : "";
        JsonObject response = new JsonObject();

        if (dao.cambiarEstadoNuevasPolizas(usuario.getLogin(), id, estado)) {
            response.addProperty("respuesta", "Guardado");
        } else {
            response.addProperty("respuesta", "Error al guardar.");
        }
        this.printlnResponseAjax(new Gson().toJson(response), "application/json;");
    }
    
    private void cargarConfiguracionPoliza() throws Exception {
        int sucursal = (request.getParameter("sucursal") != null) ? Integer.parseInt(request.getParameter("sucursal")) : 0;
        int unidadNegocio = (request.getParameter("unidad_negocio") != null) ? Integer.parseInt(request.getParameter("unidad_negocio")) : 0;
        this.printlnResponseAjax(dao.cargarConfiguracionPoliza(sucursal, unidadNegocio), "application/json;");
    }
    
    private void insertarConfiguracionPoliza() throws Exception {
        int sucursal = (request.getParameter("sucursal") != null) ? Integer.parseInt(request.getParameter("sucursal")) : 0;
        int poliza = (request.getParameter("poliza") != null) ? Integer.parseInt(request.getParameter("poliza")) : 0;
        int aseguradora = (request.getParameter("aseguradora") != null) ? Integer.parseInt(request.getParameter("aseguradora")) : 0;
        int tipoCobro = (request.getParameter("tipo_cobro") != null) ? Integer.parseInt(request.getParameter("tipo_cobro")) : 0;
        int valorPoliza = (request.getParameter("valor_poliza") != null) ? Integer.parseInt(request.getParameter("valor_poliza")) : 0;
        int unidadNegocio = (request.getParameter("unidad_negocio") != null) ? Integer.parseInt(request.getParameter("unidad_negocio")) : 0;

        JsonObject response = new JsonObject();
        try {
            dao.insertarConfiguracionPoliza(usuario, poliza, aseguradora, tipoCobro, valorPoliza, unidadNegocio, sucursal);
            response.addProperty("respuesta", "Guardado");
        } catch (SQLException ex) {
            if(ex.getSQLState().equals("23505")) {
                response.addProperty("error", "Ya existe una p�liza con esta configuraci�n");            
            } else {
                response.addProperty("error", "Error al guardar.");
            }
        }           
        this.printlnResponseAjax(new Gson().toJson(response), "application/json;");
    }

    private void actualizarConfiguracionPoliza() throws Exception {
        int sucursal = (request.getParameter("sucursal") != null) ? Integer.parseInt(request.getParameter("sucursal")) : 0;
        int poliza = (request.getParameter("poliza") != null) ? Integer.parseInt(request.getParameter("poliza")) : 0;
        int aseguradora = (request.getParameter("aseguradora") != null) ? Integer.parseInt(request.getParameter("aseguradora")) : 0;
        int tipoCobro = (request.getParameter("tipo_cobro") != null) ? Integer.parseInt(request.getParameter("tipo_cobro")) : 0;
        int valorPoliza = (request.getParameter("valor_poliza") != null) ? Integer.parseInt(request.getParameter("valor_poliza")) : 0;
        int unidadNegocio = (request.getParameter("unidad_negocio") != null) ? Integer.parseInt(request.getParameter("unidad_negocio")) : 0;
        int id = request.getParameter("id") != null ? Integer.parseInt(request.getParameter("id")) : 0;

        JsonObject response = new JsonObject();
        try {
            dao.actualizarConfiguracionPoliza(usuario, poliza, aseguradora, tipoCobro, valorPoliza, unidadNegocio, sucursal, id);
            response.addProperty("respuesta", "Guardado");
        } catch (SQLException ex) {
            if (ex.getSQLState().equals("23505")) {
                response.addProperty("error", "Ya existe una p�liza con esta configuraci�n");            
            } else {
                response.addProperty("error", "Error al guardar.");
            }
        }
        this.printlnResponseAjax(new Gson().toJson(response), "application/json;");
    }

    private void cambiarEstadoConfiguracionPoliza() throws Exception {
        int id = (request.getParameter("id") != null) ? Integer.parseInt(request.getParameter("id")) : 0;
        String estado = (request.getParameter("status") != null) ? request.getParameter("status") : "";
        JsonObject response = new JsonObject();

        if (dao.cambiarEstadoConfiguracionPoliza(usuario.getLogin(), id, estado)) {
            response.addProperty("respuesta", "Guardado");
        } else {
            response.addProperty("respuesta", "Error al guardar.");
        }
        this.printlnResponseAjax(new Gson().toJson(response), "application/json;");
    }    
    
    private void cargarAseguradoras() throws Exception {
        this.printlnResponseAjax(dao.cargarAseguradoras(), "application/json;");
    }

    private void insertarAseguradoras() throws Exception {
        String descripcion = (request.getParameter("descripcion") != null) ? request.getParameter("descripcion") : "";
        long nit = request.getParameter("nit") != null ? Long.parseLong(request.getParameter("nit")) : 0;
        float retorno = request.getParameter("retorno") != null ? Float.parseFloat(request.getParameter("retorno")) : 0;
        int plazoPago = request.getParameter("plazo_pago") != null ? Integer.parseInt(request.getParameter("plazo_pago")) : 0;
        int prr = request.getParameter("prr") != null ? Integer.parseInt(request.getParameter("prr")) : 0;
        JsonObject response = new JsonObject();
        try {
            dao.insertarAseguradoras(usuario, descripcion, nit, retorno, plazoPago, prr);
            response.addProperty("respuesta", "Guardado");
        } catch (SQLException ex) {
            if(ex.getSQLState().equals("23505")) {
                response.addProperty("error", "Ya existe una p�liza con esta configuraci�n");            
            } else {
                response.addProperty("error", "Error al guardar.");
            }
        }  
        this.printlnResponseAjax(new Gson().toJson(response), "application/json;");
    }

    private void actualizarAseguradoras() throws Exception {
        String descripcion = (request.getParameter("descripcion") != null) ? request.getParameter("descripcion") : "";
        int id = request.getParameter("id") != null ? Integer.parseInt(request.getParameter("id")) : 0;        
        float retorno = request.getParameter("retorno") != null ? Float.parseFloat(request.getParameter("retorno")) : 0;
        int plazoPago = request.getParameter("plazo_pago") != null ? Integer.parseInt(request.getParameter("plazo_pago")) : 0;
        int prr = request.getParameter("prr") != null ? Integer.parseInt(request.getParameter("prr")) : 0;

        JsonObject response = new JsonObject();
        try {
            dao.actualizarAseguradoras(usuario, descripcion, retorno, plazoPago, prr, id);
            response.addProperty("respuesta", "Guardado");
        } catch (SQLException ex) {
            if(ex.getSQLState().equals("23505")) {
                response.addProperty("error", "Ya existe una p�liza con esta configuraci�n");            
            } else {
                response.addProperty("error", "Error al guardar.");
            }
        }  
        this.printlnResponseAjax(new Gson().toJson(response), "application/json;");
    }

    private void cambiarEstadoAseguradora() throws Exception {
        int id = (request.getParameter("id") != null) ? Integer.parseInt(request.getParameter("id")) : 0;
        String estado = (request.getParameter("status") != null) ? request.getParameter("status") : "";
        JsonObject response = new JsonObject();

        if (dao.cambiarEstadoAseguradora(usuario.getLogin(), id, estado)) {
            response.addProperty("respuesta", "Guardado");
        } else {
            response.addProperty("respuesta", "Error al guardar.");
        }
        this.printlnResponseAjax(new Gson().toJson(response), "application/json;");
    }

    private void cargarTipoCobro() throws Exception {
        this.printlnResponseAjax(dao.cargarTipoCobro(), "application/json");
    }

    private void insertarTipoCobro() throws Exception {
        String descripcion = (request.getParameter("descripcion") != null) ? request.getParameter("descripcion") : "";
        String tipo = (request.getParameter("tipo") != null) ? request.getParameter("tipo") : "";
        String financiacion = (request.getParameter("financiacion") != null) ? request.getParameter("financiacion") : "";
        
        JsonObject response = new JsonObject();
        try {
            dao.insertarTipoCobro(usuario, descripcion, tipo, financiacion);
            response.addProperty("respuesta", "Guardado");
        } catch (SQLException ex) {
            if(ex.getSQLState().equals("23505")) {
                response.addProperty("error", "Ya existe una p�liza con esta configuraci�n");            
            } else {
                response.addProperty("error", "Error al guardar.");
            }
        }  
        this.printlnResponseAjax(new Gson().toJson(response), "application/json;");
    }

    private void actualizarTipoCobro() throws Exception {
        String descripcion = (request.getParameter("descripcion") != null) ? request.getParameter("descripcion") : "";
        String tipo = (request.getParameter("tipo") != null) ? request.getParameter("tipo") : "";
        String financiacion = (request.getParameter("financiacion") != null) ? request.getParameter("financiacion") : "";
        int id = (request.getParameter("id") != null) ? Integer.parseInt(request.getParameter("id")) : 0;
        
        JsonObject response = new JsonObject();
        try {
            dao.actualizarTipoCobro(usuario, descripcion, tipo, financiacion, id);
            response.addProperty("respuesta", "Guardado");
        } catch (SQLException ex) {
            if(ex.getSQLState().equals("23505")) {
                response.addProperty("error", "Ya existe una p�liza con esta configuraci�n");            
            } else {
                response.addProperty("error", "Error al guardar.");
            }
        }
        this.printlnResponseAjax(new Gson().toJson(response), "application/json;");
    }

    private void cambiarEstadoTipoCobro() throws Exception {
        String estado = (request.getParameter("status") != null) ? request.getParameter("status") : "";
        int id = (request.getParameter("id") != null) ? Integer.parseInt(request.getParameter("id")) : 0;
        
        JsonObject response = new JsonObject();
        if (dao.cambiarEstadoTipoCobro(usuario.getLogin(), id, estado)) {
            response.addProperty("respuesta", "ok");
        } else {
            response.addProperty("respuesta", "Error al guardar.");
        }
        this.printlnResponseAjax(new Gson().toJson(response), "application/json;");
    }

    private void cargarValorPoliza() throws Exception  {
        this.printlnResponseAjax(dao.cargarValorPoliza(), "application/json");
    }

    private void insertarValorPoliza() throws Exception {
        String descripcion = (request.getParameter("descripcion") != null) ? request.getParameter("descripcion") : "";
        String tipo = (request.getParameter("tipo") != null) ? request.getParameter("tipo") : "";
        String calcularSobre = (request.getParameter("calcular_sobre") != null) ? request.getParameter("calcular_sobre") : "";
        float valorPorcentaje = (request.getParameter("valor_porcentaje") != null) ? Float.parseFloat(request.getParameter("valor_porcentaje")) : 0;
        float valorAbsoluto = (request.getParameter("valor_absoluto") != null && !(request.getParameter("valor_absoluto").equals(""))) ? Float.parseFloat(request.getParameter("valor_absoluto")) : 0;
        boolean iva = request.getParameter("iva") != null ? Boolean.parseBoolean(request.getParameter("iva")) : false;
        
        JsonObject response = new JsonObject();
        try {
            dao.insertarValorPoliza(usuario, descripcion, tipo, calcularSobre, valorPorcentaje, valorAbsoluto, iva);
            response.addProperty("respuesta", "Guardado");
        } catch (SQLException ex) {
            if(ex.getSQLState().equals("23505")) {
                response.addProperty("error", "Ya existe una p�liza con esta configuraci�n");            
            } else {
                response.addProperty("error", "Error al guardar.");
            }
        }
        this.printlnResponseAjax(new Gson().toJson(response), "application/json;");
    }

    private void actualizarValorPoliza() throws Exception {
        String descripcion = (request.getParameter("descripcion") != null) ? request.getParameter("descripcion") : "";
        String tipo = (request.getParameter("tipo") != null) ? request.getParameter("tipo") : "";
        String calcularSobre = (request.getParameter("calcular_sobre") != null) ? request.getParameter("calcular_sobre") : "";
        float valorPorcentaje = (request.getParameter("valor_porcentaje") != null) ? Float.parseFloat(request.getParameter("valor_porcentaje")) : 0;
        float valorAbsoluto = (request.getParameter("valor_absoluto") != null) && !(request.getParameter("valor_absoluto").equals(""))  ? Float.parseFloat(request.getParameter("valor_absoluto")) : 0f;
        boolean iva = request.getParameter("iva") != null ? Boolean.parseBoolean(request.getParameter("iva")) : false;
        int id = (request.getParameter("id") != null) ? Integer.parseInt(request.getParameter("id")) : 0;
        
        JsonObject response = new JsonObject();
        try {
            dao.actualizarValorPoliza(usuario, descripcion, tipo, calcularSobre, valorPorcentaje, valorAbsoluto, iva, id);
            response.addProperty("respuesta", "Guardado");
        } catch (SQLException ex) {
            if(ex.getSQLState().equals("23505")) {
                response.addProperty("error", "Ya existe una p�liza con esta configuraci�n");            
            } else {
                response.addProperty("error", "Error al guardar.");
            }
        }
        this.printlnResponseAjax(new Gson().toJson(response), "application/json;");
    }

    private void cambiarEstadoValorPoliza() throws Exception {
        String estado = (request.getParameter("status") != null) ? request.getParameter("status") : "";
        int id = (request.getParameter("id") != null) ? Integer.parseInt(request.getParameter("id")) : 0;
        
        JsonObject response = new JsonObject();
        if (dao.cambiarEstadoValorPoliza(usuario.getLogin(), id, estado)) {
            response.addProperty("respuesta", "ok");
        } else {
            response.addProperty("respuesta", "Error al guardar.");
        }
        this.printlnResponseAjax(new Gson().toJson(response), "application/json;");
    }
    
    private void cargarSucursales() throws Exception {
        int unidadNegocio = (request.getParameter("unidad") != null || request.getParameter("unidad") != "") ? Integer.parseInt(request.getParameter("unidad")) : 0;
        String ciudad = (request.getParameter("ciudad") != null) ? request.getParameter("ciudad") : "";
        String departamento = (request.getParameter("departamento") != null) ? request.getParameter("departamento") : "";
        this.printlnResponseAjax(dao.cargarSucursalesUnidad(unidadNegocio, ciudad, departamento), "application/json;");
    }
      
    private void cargarCiudades() {
        JsonArray lista = new JsonArray();
        try {            
            JsonObject json;            
            Map<String, String> map = dao.cargarCiudades(request.getParameter("dpto") != null ? request.getParameter("dpto"): "");
            
            for (Map.Entry<String, String> entry : map.entrySet()) {                
                json = new JsonObject();
                json.addProperty("id", entry.getKey());
                json.addProperty("nombre", entry.getValue());
                lista.add(json);
            }
            this.printlnResponseAjax(new Gson().toJson(lista), "appliaction/json");
        } catch (Exception e) {
            System.err.println("Error at " + e.getClass().getSimpleName() + "-" + e.getMessage());
        }
    }
    
    private void cargarFiltroConfiguracionPoliza() {
        Gson gson = new Gson();
        JsonObject json = new JsonObject();
        JsonObject temp;
        JsonArray lista;
        try {
            lista = new JsonArray();
            Map<String, String> map = dao.cargarDepartamentos();
            for (Map.Entry<String, String> entry : map.entrySet()) {
                temp = new JsonObject();
                temp.addProperty("id", entry.getKey());
                temp.addProperty("nombre", entry.getValue());
                lista.add(temp);
            }
            json.add("departamentos", lista);
            lista = new JsonArray();
            json.add("unidades", new JsonParser().parse(dao.cargarUnidadNegocio()));
            this.printlnResponseAjax(gson.toJson(json), "application/json");
        } catch (Exception e) {
            System.err.println("Error at " + e.getClass().getSimpleName() + "-" + e.getMessage());
        }
    }
    
    private void aplicarConfiguraciones() {
        try {
            String jsonString = "";
            String[] ids;
                    
            BufferedReader br = request.getReader();
            for (String linea = br.readLine(); linea != null; linea = br.readLine()) {
                jsonString += linea;
            }
            JsonObject json = (JsonObject) new JsonParser().parse(jsonString);
            JsonArray array = json.get("configuraciones").getAsJsonArray();
            ids = new String[array.size()];

            for (int i = 0; i < ids.length; i++) {
                ids[i] = array.get(i).getAsString();
            }
            try {
                if (dao.aplicarConfiguracionesPolizas(ids, usuario.getLogin()) > 0) {
                    this.printlnResponse("{\"respuesta\": \"Se aplicaron todas las configuraciones con �xito.\"}", "application/json");
                }
            } catch (SQLException ex) {
                if (ex.getSQLState().equals("23505")) {
                    this.printlnResponse("{\"respuesta\": \"Las polizas seleccionadas ya existen en otras sucursales.\"}", "application/json");
                } else {
                    this.printlnResponse("{\"respuesta\": \"Error al aplicar las polizas a otras sucursales\", \"mensaje\":\""+ ex.getMessage() + "\"}", "application/json");
                }
            }
        } catch (Exception e) {
            System.err.println("Error at " + e.getClass().getSimpleName() + "-" + e.getMessage());
        }
    }
    
    
    public void subirArchivoFacturacion() throws FileNotFoundException, IOException {
        MultipartRequest mreq = null;
        String json = "";
        String lote ="";
        try {

            String ruta = UtilFinanzas.obtenerRuta("ruta", "/exportar/migracion/" + ((usuario != null) ? usuario.getLogin() : "anonimo"));

            //Obtenemos campos de formulario (Tanto de texto como archivo) (enctype=multipart/form-data)
            mreq = new MultipartRequest(request, ruta, 4096 * 2048, new DefaultFileRenamePolicy());
            String filetypeCredito = mreq.getContentType("file_excel");
            String filename = mreq.getFilesystemName("file_excel");
            ruta += "/" + filename;
            
            String periodo = mreq.getParameter("periodo");
            String ciclo = mreq.getParameter("ciclo");            

            Gson gson = new Gson();
            File archivo = new File(ruta);
            listado = new LinkedList();
            ArrayList<FintraSoporteBeansFacturacion> lista = new ArrayList<>();
            JXLRead xls = new JXLRead(archivo.getAbsolutePath());            
            xls.obtenerHoja("registros");
            String[] datos = null;
            int numfil = xls.numeroFilas();
            int cantreg = numfil - 1;
            //Creacion del lote
            lote = "LOT" + periodo + ciclo + cantreg;
            

            for (int i = 1; i < numfil; i++) {
                FintraSoporteBeansFacturacion beansoport = new FintraSoporteBeansFacturacion();
                
                  beansoport.setId(xls.obtenerDato(i,0));
                  beansoport.setCod_rop(xls.obtenerDato(i,1));
                  beansoport.setCod_rop_barcode(xls.obtenerDato(i,2));
                  beansoport.setGenerado_el(xls.obtenerDato(i,3).equals("") ? "0099-01-01" : (xls.obtenerDato(i,3)));
                  beansoport.setVencimiento_rop(xls.obtenerDato(i,4).equals("") ? "0099-01-01" : (xls.obtenerDato(i,4)));
                  beansoport.setNegocio(xls.obtenerDato(i,5));
                  beansoport.setCedula(xls.obtenerDato(i,6));
                  beansoport.setNombre_cliente(xls.obtenerDato(i,7));
                  beansoport.setDireccion(xls.obtenerDato(i,8));
                  beansoport.setDepartamento(xls.obtenerDato(i,9));
                  beansoport.setCiudad(xls.obtenerDato(i,10));
                  beansoport.setBarrio(xls.obtenerDato(i,11));
                  beansoport.setAgencia(xls.obtenerDato(i,12));
                  beansoport.setLinea_producto(xls.obtenerDato(i,13));
                  beansoport.setCuotas_vencidas(xls.obtenerDato(i,14));
                  beansoport.setCuotas_pendientes(xls.obtenerDato(i,15));
                  beansoport.setDias_vencidos(xls.obtenerDato(i,16));
                  beansoport.setFch_ultimo_pago(xls.obtenerDato(i,17).equals("") ? "0099-01-01" : (xls.obtenerDato(i,17)));
                  beansoport.setSubtotal_rop(xls.obtenerDato(i, 18).replace(".", ""));
                  beansoport.setTotal_sanciones(xls.obtenerDato(i,19).replace(".", ""));
                  beansoport.setTotal_descuentos(xls.obtenerDato(i,20).replace(".", ""));
                  beansoport.setTotal_rop(xls.obtenerDato(i,21).replace(".", ""));
                  beansoport.setTotal_abonos(xls.obtenerDato(i,22).replace(".", ""));
                  beansoport.setObservacion(xls.obtenerDato(i,23));
                  beansoport.setMsg_paguese_antes(xls.obtenerDato(i,24));
                  beansoport.setMsg_estado_credito(xls.obtenerDato(i,25));
                  beansoport.setCapital(xls.obtenerDato(i,26).replace(".", ""));
                  beansoport.setInteres_financiacion(xls.obtenerDato(i,27).replace(".", ""));
                  beansoport.setSeguro(xls.obtenerDato(i,28).replace(".", ""));
                  beansoport.setInteres_xmora(xls.obtenerDato(i,29).replace(".", ""));
                  beansoport.setGastos_cobranza(xls.obtenerDato(i,30).replace(".", ""));
                  beansoport.setDscto_capital(xls.obtenerDato(i,31).replace(".", ""));
                  beansoport.setDscto_interes_financiacion(xls.obtenerDato(i,32).replace(".", ""));
                  beansoport.setDscto_interes_xmora(xls.obtenerDato(i,33).replace(".", ""));
                  beansoport.setDscto_gastos_cobranza(xls.obtenerDato(i,34).replace(".", ""));
                  beansoport.setDscto_seguro(xls.obtenerDato(i,35).replace(".", ""));
                  beansoport.setKsubtotal_corriente(xls.obtenerDato(i,36).replace(".", ""));
                  beansoport.setKsubtotal_vencido(xls.obtenerDato(i,37).replace(".", ""));
                  beansoport.setKsubtotalneto(xls.obtenerDato(i,38).replace(".", ""));
                  beansoport.setKdescuentos(xls.obtenerDato(i,39).replace(".", ""));
                  beansoport.setKtotal(xls.obtenerDato(i,40).replace(".", ""));
                  beansoport.setItems(xls.obtenerDato(i,41));
                  beansoport.setEstablecimiento_comercio(xls.obtenerDato(i,42));
                  beansoport.setTelefono(xls.obtenerDato(i,43));
                  beansoport.setEmail(xls.obtenerDato(i,44));
                  beansoport.setExtracto_email(xls.obtenerDato(i,45));
//                  beansoport.setGenerado(xls.obtenerDato(i,46));
                  beansoport.setPeriodo(periodo);
                  beansoport.setCiclo(ciclo);
                  beansoport.setLote(lote);

                lista.add(beansoport);
            }

            String info = "{\"json\":" + gson.toJson(lista) + "}";
            json = guardarExcelFacturacion(info);
//            String json = "{\"json\":\"OK\"}";
//            this.printlnResponse(json, "application/json;");
            xls.cerrarLibro();
            mreq.getFile("file_excel").delete();
        } catch (Exception e) {
            mreq.getFile("file_excel").delete();
            e.printStackTrace();
            System.out.println(e);
            json = "{\"respuesta\":\"ERROR\"}";
        }finally{            
            try {
                if (json.equals("ERROR")){
                    json = "{\"respuesta\":\"Error al generar lote\"}";
                }else{
                json = "{\"respuesta\":\""+lote+"\"}";
                }
                 this.printlnResponse(json, "application/json;");
            } catch (Exception e) {
            }           
        }
    }
    
    
    public String guardarExcelFacturacion(String info) {
        String json = "";
        try {
            Gson gson = new Gson();
            JsonObject obj = (JsonObject) (new JsonParser()).parse(info);
            json = this.dao.guardarExcelFacturacion(obj, usuario);
//            respuesta = "{\"respuesta\":\"CARGADO\"}";
        } catch (Exception ex) {
            json = "{\"respuesta\":\"ERROR\"}";
            java.util.logging.Logger.getLogger(AdministracionEdsAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
        return json;
    }
    
    
    private void cargarExtractosPorGenerar() {
       String lote_generado =(request.getParameter("lote_generado") != null ? request.getParameter("lote_generado") : "");
       try {    
       this.printlnResponseAjax(dao.cargarExtractosPorGenerar(lote_generado), "application/json");
       } catch (Exception e) {
            Logger.getLogger("").log(Level.SEVERE, null, e);
        }
    }

    private void cargarComboExtractosPorGenerar() {
        try {    
       this.printlnResponseAjax(dao.cargarComboExtractosPorGenerar(), "application/json");
       } catch (Exception e) {
            Logger.getLogger("").log(Level.SEVERE, null, e);
        }
    }

    private void generarExtractosDigitales() {
         String lote_generado =(request.getParameter("lote_generado") != null ? request.getParameter("lote_generado") : "");
         try {    
       this.printlnResponseAjax("{\"status\": 200, \"data\":\""+dao.generarExtractosDigitales(lote_generado,usuario)+"\"}", "application/json");
      } catch (Exception e) {
            Logger.getLogger("").log(Level.SEVERE, null, e);
        }
    }

    private void cargarExtractosPorEnviarSms() {
     String lote_generado =(request.getParameter("lote_generado") != null ? request.getParameter("lote_generado") : "");
       try {    
    this.printlnResponseAjax(dao.cargarExtractosPorEnviarSms(lote_generado), "application/json");
    } catch (Exception e) {
         Logger.getLogger("").log(Level.SEVERE, null, e);
     }
    }

    private void enviarSmsExtracto() throws Exception {
      String lote_generado =(request.getParameter("lote_generado") != null ? request.getParameter("lote_generado") : "");
      EnviarSms sms =new EnviarSms(lote_generado,usuario);
        Thread t = new Thread(sms);
        t.start();
        this.printlnResponse("{\"data\": \"Envio de extractos por sms Iniciado\"}", "application/json");
    }

    private void cargarComboExtractosPorEnviarSms() {
        try {    
       this.printlnResponseAjax(dao.cargarComboExtractosPorEnviarSms(), "application/json");
       } catch (Exception e) {
            Logger.getLogger("").log(Level.SEVERE, null, e);
        }}

}
    
