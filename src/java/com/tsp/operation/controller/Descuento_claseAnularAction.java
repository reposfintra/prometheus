    /*******************************************************************
 * Nombre clase: Descuento_claseAnularAction.java
 * Descripci�n: Accion para anular un descuento por clase equipo a la bd.
 * Autor: Ing. Jose de la rosa
 * Fecha: 7 de diciembre de 2005, 02:31 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class Descuento_claseAnularAction extends Action{
    
    /** Creates a new instance of Descuento_claseAnularAction */
    public Descuento_claseAnularAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next="/jsp/trafico/mensaje/MsgAnulado.jsp";
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String distrito = (String) session.getAttribute ("Distrito");
        String clase = request.getParameter ("c_clase").toUpperCase ();
        String concepto = request.getParameter ("c_codigo").toUpperCase ();
        try{
            Descuento_clase descuento = new Descuento_clase ();
            descuento.setClase_equipo (clase);
            descuento.setCodigo_concepto (concepto);
            descuento.setUsuario_modificacion (usuario.getLogin ().toUpperCase ());
            descuento.setDistrito (distrito);
            model.descuento_claseService.anularDescuento_clase (descuento);
        }catch (SQLException e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
    
}
