/********************************************************************
 *      Nombre Clase.................   FacturaAProveedoresAction.java
 *      Descripci�n..................   Action que se encarga de generar un vector con los resgitros de proveedores por numero de nit
 *      Autor........................   David Lamadrid
 *      Fecha........................   12 de octubre de 2005, 06:36 PM
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.util.Util;
/**
 *
 * @author  dlamadrid
 */
//logger
import org.apache.log4j.Logger;

public class FactRecurrAProveedoresAction extends Action{
    
    Logger log = Logger.getLogger(this.getClass());
    
    
    /** Creates a new instance of FacturaAProveedoresAction */
    public FactRecurrAProveedoresAction() {
    }
    
    public void run () throws ServletException, InformationException {
        try {
            //ivan 21 julio 2006
            HttpSession session = request.getSession();
            com.tsp.finanzas.contab.model.Model modelcontab = (com.tsp.finanzas.contab.model.Model) session.getAttribute("modelcontab");
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            
            
            //Ivan Dario Gomez 10-05-2006
            String OP = (request.getParameter("OP")!=null)?request.getParameter("OP"):"";
            String maxfila          =   request.getParameter("maxfila");
            String tipo_documento   =   ""+ request.getParameter ("tipo_documento");
            String documento        =   ""+ request.getParameter ("documento");
            String proveedor        =   ""+ request.getParameter ("proveedor");
            String tipo_documento_rel       =   ""+ request.getParameter ("tipo_documento_rel");
            String documento_relacionado    =   ""+ request.getParameter ("documento_relacionado");
            String fecha_documento  =   ""+request.getParameter ("fecha_documento");
            String banco            =   ""+ request.getParameter ("c_banco");
            double vlr_neto         =   model.factrecurrService.getNumero (""+ request.getParameter ("vlr_neto"));
            double vlr_total        =   model.factrecurrService.getNumero (""+ request.getParameter ("total"));
            String hidden           =   (request.getParameter("hidden")!=null)?request.getParameter("hidden"):"";
            String validar          =   ""+ request.getParameter ("validar");
            String agencia          =   request.getParameter ("agencia");
            String CabIva           =   (request.getParameter("CabIva")!=null?request.getParameter("CabIva"):"");
            String CabRiva          =   (request.getParameter("CabRiva")!=null?request.getParameter("CabRiva"):"");
            String CabRica          =   (request.getParameter("CabRica")!=null?request.getParameter("CabRica"):"");
            String CabRfte          =   (request.getParameter("CabRfte")!=null?request.getParameter("CabRfte"):"");
            int plazo               =   0;
            int num_items           =   0;
            
            /**************************************************************************************************************/
            int num_cuotas = (request.getParameter("ncuotas")!=null && request.getParameter("ncuotas").length()!=0? Integer.parseInt(request.getParameter("ncuotas")) : 0);
            String fecha_inicio_transfer = (request.getParameter("fecha_inicio")!=null?request.getParameter("fecha_inicio"):"");
            String fecha_inicio_transferO = (request.getParameter("fecha_inicio2")!=null?request.getParameter("fecha_inicio2"):"");
            String frecuencia = (request.getParameter("CabRfte")!=null?request.getParameter("frecuencia"):"");
            
            log.info("NCUOTAS: " + num_cuotas);
            log.info("FECHA INICIO TRANSFER: " + fecha_inicio_transfer);
            log.info("FRECUENCIA: " + frecuencia);
            /**************************************************************************************************************/
            
            try{
                num_items  = Integer.parseInt (""+ request.getParameter ("num_items"));
                log.info("Numero de Items "+num_items);
                plazo = Integer.parseInt (""+ request.getParameter ("plazo"));
            }
            catch(java.lang.NumberFormatException e){
                vlr_neto=0;
                plazo=0;
                num_items=1;
            }
            
            Vector vItems = new Vector ();
            int MaxFi = Integer.parseInt(maxfila);
            for (int i=1;i <= MaxFi; i++){              
              String filaTabla = request.getParameter("valor1"+i);  
              CXPItemDoc item = new CXPItemDoc ();
              if ( filaTabla!=null  ){
                
                String descripcion = ""+request.getParameter("desc"+i);
                String concepto=""+ request.getParameter ("descripcion_i"+i);
                String codigo_cuenta=""+ request.getParameter ("codigo_cuenta"+i);
                String codigo_abc=""+ request.getParameter ("codigo_abc"+i).toUpperCase();
                String planilla=""+ request.getParameter ("planilla"+i);
                //Ivan 26 julio 2006
                String ree  = ""+ request.getParameter ("REE"+i).toUpperCase();
                String Ref3 = ""+ request.getParameter ("Ref3"+i);
                String Ref4 = ""+ request.getParameter ("Ref4"+i);
                String agenciaItem = ""+ request.getParameter("agencia"+i);
                ////////////////////////////////////////////////////////////
                String cod1 = request.getParameter("cod1"+i).toUpperCase(); 
                String cod2 = request.getParameter("cod2"+i);
                String cod3 = request.getParameter("cod3"+i);
                String cod4 = request.getParameter("cod4"+i);
                String cod5 = request.getParameter("cod5"+i);
                String [] codigos = {cod1,cod2,cod3,cod4,cod5};
                // Modificacion 21 julio 2006
                LinkedList tbltipo = null;
                String auxiliar      = request.getParameter("auxiliar"+i); 
                String tipoSubledger = request.getParameter("tipo"+i); 
                if(modelcontab.planDeCuentasService.existCuenta(usuario.getDstrct(),codigo_cuenta)){   
                    modelcontab.subledgerService.busquedaCuentasTipoSubledger(usuario.getDstrct(),codigo_cuenta); 
                    tbltipo = modelcontab.subledgerService.getCuentastsubledger(); 
                }
                //////////////////////////////////////////////////////////////////////
                
                double valor =   0;
                double valor_t = 0;
                
                String tipcliarea= ""+ request.getParameter ("cod_oc"+i);
                String codcliarea= ""+ request.getParameter ("oc"+i);
                String descliarea= ""+ request.getParameter ("doc"+i);
                String iva       = ""+ request.getParameter ("iva"+i);
                
                valor   =   model.factrecurrService.getNumero (""+ request.getParameter ("valor1"+i));
                valor_t =   model.factrecurrService.getNumero (""+ request.getParameter ("valorNeto"+i));
                
                //Ivan 26 julio 2006
                item.setRef3(Ref3);
                item.setRef4(Ref4);
                item.setRee(ree);
                item.setAgencia(agenciaItem);
                ///////////////////////////////
                
                item.setCodigos(codigos);
                item.setConcepto (concepto);
                item.setDescripcion(descripcion);
                item.setCodigo_cuenta (codigo_cuenta);
                item.setCodigo_abc (codigo_abc);
                item.setPlanilla (planilla);
                item.setVlr_total(valor_t);
                item.setVlr_me(valor);
                item.setIva(iva);
                
                item.setTipcliarea (tipcliarea);
                item.setCodcliarea (codcliarea);
                item.setDescliarea (descliarea);
                
                // ivan 21 julio 2006
                item.setAuxiliar(auxiliar);
                item.setTipo(tbltipo);
                item.setTipoSubledger(tipoSubledger);
                /////////////////////////////////               
                                
                Vector vTipoImp= model.TimpuestoSvc.vTiposImpuestos ();
                Vector vImpuestosPorItem= new Vector ();
                for(int x=0;x<vTipoImp.size ();x++){
                    CXPImpItem impuestoItem = new CXPImpItem ();
                    String cod_impuesto = ""+ request.getParameter ("impuesto"+x+""+i);
                    impuestoItem.setCod_impuesto (cod_impuesto);
                    log.info("impuesto"+cod_impuesto);
                    vImpuestosPorItem.add (impuestoItem);
                }
                item.setVItems ( vImpuestosPorItem);
                item.setVCopia(vImpuestosPorItem);
                vItems.add (item);
                
              }else{
                  vItems.add (item);
              }
            }
            
            model.factrecurrService.setVecRxpItemsDoc (vItems);
        
            String sucursal =""+ request.getParameter ("c_sucursal");
            String moneda=""+ request.getParameter ("moneda");
            String descripcion =""+ request.getParameter ("descripcion");
            String observacion =""+ request.getParameter ("observacion");
            String usuario_aprobacion=""+request.getParameter ("usuario_aprobacion");
            String prov    = "";
            String ban     = "";
            String sucu    = "";
            int    plaz    = 0;
            String moneda_banco   ="";
            //Ivan Dario 28 Octubre 2006
            String agenciaBanco ="";
            String beneficiario="";
            String hc="";
            
            Proveedor o_proveedor = model.proveedorService.obtenerProveedorPorNit (proveedor);
            int b =0;
            if(o_proveedor != null) {
                 prov         = o_proveedor.getC_nit();
                 ban          = o_proveedor.getC_branch_code();
                 sucu         = o_proveedor.getC_bank_account();
                 plaz         = o_proveedor.getPlazo();
                 moneda_banco = o_proveedor.getCurrency();
                  //Ivan DArio 28 Octubre 2006
                 agenciaBanco = o_proveedor.getC_agency_id();
                 beneficiario = o_proveedor.getNom_beneficiario();
                 hc           = o_proveedor.getC_hc();
                
               
                b=1;
            }
            
            if (b!=1) {
               
            }
            
            CXP_Doc factura = new CXP_Doc ();
            factura.setTipo_documento (tipo_documento);
            factura.setDocumento (documento);
            factura.setProveedor (proveedor);
            factura.setTipo_documento_rel (tipo_documento_rel);
            factura.setDocumento_relacionado (documento_relacionado);
            factura.setFecha_documento (fecha_documento);
            factura.setBanco (banco);
            factura.setVlr_neto (vlr_total);
            factura.setVlr_total(vlr_neto);
            factura.setMoneda (moneda);
            factura.setSucursal (sucursal);
            factura.setDescripcion (descripcion);
            factura.setObservacion (observacion);
            factura.setUsuario_aprobacion (usuario_aprobacion);
            factura.setPlazo (plazo);
            factura.setIva(CabIva);
            factura.setRiva(CabRiva);
            factura.setRica(CabRica);
            factura.setRfte(CabRfte);
            factura.setMoneda_banco(moneda_banco);
            
             //Ivan DArio 28 Octubre 2006
            factura.setAgenciaBanco(agenciaBanco);
            ////////////////////////////////////
            factura.setBeneficiario(beneficiario);
            factura.setHandle_code(hc);
            
            /**************************************************************/
            factura.setNum_cuotas(num_cuotas);
            factura.setFecha_inicio_transfer(fecha_inicio_transfer);
            factura.setFrecuencia(frecuencia);
            factura.setFecha_inicio_transferO(fecha_inicio_transferO);
            /**************************************************************/
            
            
            model.factrecurrService.setFactura (factura);
            String next ="";
            if(OP.equals("ENTER")){
                // model.proveedorService.obtenerProveedoresPorNit (proveedor);
                 next = "/controller?estado=FactRecurr&accion=CargarP&proveedor="+prov+"&banco="+ban+"&sucursal="+sucu+"&plazo="+plaz+"&ag="+validar+"&maxfila="+maxfila+"&moneda_banco="+moneda_banco+"&agenciaBanco="+agenciaBanco+"&beneficiario="+beneficiario+"&hc="+hc;
            }else{
                 next = "/jsp/cxpagar/facturasrec/buscarnit.jsp?validar="+validar+"&maxfila="+maxfila;
            }
            
            log.info("NEXT: "+next);
            
            RequestDispatcher rd = application.getRequestDispatcher (next);
            if(rd == null)
                throw new Exception ("No se pudo encontrar "+ next);
            rd.forward (request, response);
            
            
        }
        catch(Exception e) {
            throw new ServletException ("Accion:"+ e.getMessage ());
        }
        
    }
    
}
