/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.UnidadesNegocioDAO;
import com.tsp.operation.model.DAOS.impl.UnidadesNegocioImpl;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.Convenio;
import com.tsp.operation.model.beans.UnidadesNegocio;
import com.tsp.operation.model.beans.Usuario;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

/**
 *
 * @author desarrollo
 */
public class UnidadesNegocioAction extends Action {

    private final int CARGAR_UNIDADES_NEGOCIO = 1;
    private final int CREAR_UNIDAD_NEGOCIO = 2;
    private final int CARGAR_CONVENIOS = 3;
    private final int ASOCIAR_CONVENIOS = 4;
    private final int CARGAR_CONVENIOS_NEGOCIO = 5;
    private final int CARGAR_DATOS_UNIDAD_NEGOCIO = 6;
    private final int ANULAR_CONVENIO_UNIDAD_NEGOCIO = 7;
    private final int ACTUALIZAR_UNIDAD_NEGOCIO = 8;
    private UnidadesNegocioDAO dao;
    Usuario usuario = null;

    @Override
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            dao = new UnidadesNegocioImpl();
            int opcion = (request.getParameter("opcion") != null)
                    ? Integer.parseInt(request.getParameter("opcion"))
                    : -1;
            switch (opcion) {
                case CARGAR_UNIDADES_NEGOCIO:
                    this.cargarUnidadesNegocio();
                    break;
                case CREAR_UNIDAD_NEGOCIO:
                    this.crearUnidadNegocio();
                    break;  
                case CARGAR_CONVENIOS:
                    this.cargarConvenios();
                    break;     
                case ASOCIAR_CONVENIOS:
                    this.asociarConvenios();
                    break; 
                case CARGAR_CONVENIOS_NEGOCIO:
                    this.cargarConveniosNegocio();
                    break;
                case CARGAR_DATOS_UNIDAD_NEGOCIO:
                    this.cargarDatosUnidadNegocio();
                    break; 
                case ANULAR_CONVENIO_UNIDAD_NEGOCIO:
                    this.anularConvenioUnidadNegocio();
                    break;
                case ACTUALIZAR_UNIDAD_NEGOCIO:
                    this.actualizarUnidadNegocio();
                    break;     
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        } 
    }
    
    private void cargarUnidadesNegocio() {
        try {
            ArrayList<UnidadesNegocio> lista = dao.cargarUnidadesNegocio();
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    /** 
     * envia respuesta al cliente
     **/
    public void printlnResponse(String respuesta, String contentType) throws Exception {
        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);

    }

    private void crearUnidadNegocio() {
        try{
            String nomUnidad = request.getParameter("nomUnidad");
            String codUnidad = request.getParameter("codUnidad");
            String codCentral = request.getParameter("codCentral");
            if(!dao.existeUnidadNegocio(nomUnidad,codUnidad)){
                dao.guardarUnidadNegocio(nomUnidad,codUnidad,codCentral);
                this.printlnResponse("OK", "application/text;");
            }else{
                 this.printlnResponse("Error", "application/text;");
            }
        }catch(Exception e){
            e.printStackTrace();
    }
    
    }

    private void cargarConvenios() {
        try {
            ArrayList<Convenio> lista = dao.cargarConvenios();
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void asociarConvenios() {
        String nomUnidad = request.getParameter("nomUnidad");
        String listado[] = request.getParameter("listado").split(",");
        try{
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            int idUnidad = dao.obtenerIdUnidadNegocio(nomUnidad);
            for(int i = 0; i < listado.length; i++){
                tService.getSt().addBatch(dao.insertarRelConvenio(idUnidad,listado[i]));
            }
            tService.execute();
            this.printlnResponse("OK", "application/text;");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void cargarConveniosNegocio() {
        String idUnidad = request.getParameter("idUnidad");
        try {
            ArrayList<Convenio> lista = dao.cargarConveniosNegocio(idUnidad);
            Gson gson = new Gson();
            String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
            this.printlnResponse(json, "application/json;");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void cargarDatosUnidadNegocio() {
       String idUnidad = request.getParameter("idUnidad");
        try {
            UnidadesNegocio unidad = dao.cargarDatosUnidadNegocio(idUnidad);
            Gson gson = new Gson();
            String json = gson.toJson(unidad);
            this.printlnResponse(json, "application/json;");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void anularConvenioUnidadNegocio() {
        String idUnidad = request.getParameter("idUnidad");
        String idConvenio = request.getParameter("idConvenio");
        try {
            dao.anularConvenioUnidadNegocio(idUnidad,idConvenio);
            this.printlnResponse("OK", "application/text;");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void actualizarUnidadNegocio() {
        String idUnidad = request.getParameter("idUnidad");
        String nomUnidad = request.getParameter("nomUnidad");
        String codCentralR = request.getParameter("codCentralR");
        String codigoUnd = request.getParameter("codigoUnd");
        try {
            dao.actualizarUnidadNegocio(idUnidad,nomUnidad,codCentralR,codigoUnd);
            this.printlnResponse("OK", "application/text;");
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
