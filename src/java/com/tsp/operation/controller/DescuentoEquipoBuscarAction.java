/********************************************************************
* Nombre ......................DescuentoEquipoBuscarAction.java     *
* Descripci�n..................Clase Action de descuento de equipos *
* Autor........................Armando Oviedo                       *
* Fecha Creaci�n...............06/12/2005                           *
* Modificado por...............LREALES                              *
* Fecha Modificaci�n...........22/05/2006                           *
* Versi�n......................1.0                                  *
* Coyright.....................Transportes Sanchez Polo S.A.        *
********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;


public class DescuentoEquipoBuscarAction extends Action{
    
    /** Creates a new instance of DescuentoEquipoBuscarAction */
    public DescuentoEquipoBuscarAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        
        String next = "/jsp/equipos/descuentos_equipo/ModificarDescuentoEquipo.jsp";
        
        try{
            
            String mensaje = request.getParameter("mensaje");
            
            if( mensaje != null ){
                
                if( mensaje.equalsIgnoreCase("listartodos") ){
                    
                    next = "/jsp/equipos/descuentos_equipo/ListarDescuentos.jsp";
                    model.descuentoequiposvc.BuscarTodosDescuentos();
                    
                }
                
                else if( mensaje.equalsIgnoreCase("listar") ){
                    
                    String codigo = request.getParameter("codigo").toUpperCase();
                    
                    DescuentoEquipo tmp = new DescuentoEquipo();
                    tmp.setCodigo( codigo );
                    
                    model.descuentoequiposvc.setDE( tmp );
                    model.descuentoequiposvc.BuscarDescuentoEquipo();
                    
                }
                
            }
            
        } catch( SQLException e ){
            
            e.printStackTrace();
            throw new ServletException( e.getMessage () );
            
        }
        
        this.dispatchRequest( next );
        
    }
    
}