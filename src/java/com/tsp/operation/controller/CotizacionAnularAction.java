/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.controller;
import java.util.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
/**
 *
 * @author Rhonalf
 */
public class CotizacionAnularAction extends Action {

    public CotizacionAnularAction(){

    }

    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next  = "/jsp/delectricaribe/cotizacion/ver_cotizacion.jsp";
        //String next  = "/jsp/delectricaribe/cotizacion/buscar_cotizacion.jsp";
        
        
        //request.setAttribute("msg", "Cotizacion(es) anulada(s)!");
        request.setAttribute("msg", "Cotizacion anulada!");
        HttpSession session = request.getSession();//090922
        Usuario usuario = (Usuario) session.getAttribute("Usuario"); //090922
        String loginx=usuario.getLogin();
        ArrayList envio = new ArrayList();
        try{
            CotizacionService cs = new CotizacionService();
            String consec = request.getParameter("codigo0");//091026
            envio.add(consec);//091024
            cs.anularOrden(envio, loginx);//091024
        }
        catch(Exception e){
            System.out.println("Error en el run del action: "+e.toString());
            request.setAttribute("msg", "Error en la anulacion");
        }
        this.dispatchRequest(next);
    }

}
