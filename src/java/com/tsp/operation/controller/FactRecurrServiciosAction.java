/********************************************************************
 *      Nombre Clase.................   FacturaServiciosAction .java
 *      Descripci�n..................   Action que se encarga de generar el arbol de unservicio en particular
 *      Autor........................   David Lamadrid
 *      Fecha........................   1 de noviembre de 2005, 05:00 PM
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.util.Util;
/**
 *
 * @author  dlamadrid
 */
public class FactRecurrServiciosAction extends Action{
    
    /** Creates a new instance of FacturaServiciosAction */
    public FactRecurrServiciosAction() {
    }
    
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session = request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String distrito =  usuario.getDstrct();
            String opcion = (request.getParameter("OP")!= null)?request.getParameter("OP"):"";
            String next ="";
            if(opcion.equals("BUSCAR_IMPUESTO")){
                String Tipo   = request.getParameter("tipo");
                String Codigo = request.getParameter("codigo");
                String Id     = request.getParameter("Id");
                Tipo_impuesto datos = new  Tipo_impuesto();
                datos = model.TimpuestoSvc.buscarImpuestoPorCodigos(Tipo,Codigo,distrito,""); 
                next = "/jsp/cxpagar/facturasrec/BuscarCodigoImpuesto.jsp?Id="+Id;
                
            }else if(opcion.equals("ENTER")){
                String concepto = request.getParameter("concepto");
                String Id = request.getParameter("Id");
                model.ConceptoPagosvc.BuscarConcepto(concepto);
                List listaConcepto = model.ConceptoPagosvc.getListaConcepto();
                for(int j=0;j<listaConcepto.size();j++){
                    ConceptoPago tmp = (ConceptoPago) listaConcepto.get(j);
                    //System.out.println("DECRIPCION--"+tmp.getDescripcion());
                }
                next = "/jsp/cxpagar/facturasrec/BuscarConcepto.jsp?Id="+Id;
            
             }else if(opcion.equals("BUSCAR_REFERENCIA")){
                String concepto = request.getParameter("concepto");
                String Id = request.getParameter("Id");
                String cre = request.getParameter("cre");
                
                model.ConceptoPagosvc.BuscarReferencia3(concepto,cre);
                next = "/jsp/cxpagar/facturasrec/AgregarCuenta.jsp?Id="+Id+"&cre="+cre;
                
            }else{
                String nombreServicio =""+ request.getParameter("servicio");
                String controlador =""+ request.getParameter("controlador");
                String codigo =""+ request.getParameter("codigo");
                controlador= controlador.replaceAll("~", "&");
                String ag=usuario.getId_agencia();
                //System.out.println("ag /////////////////////////"+ag);
                model.ConceptoPagosvc.load(nombreServicio,ag);
                
                next = "/jsp/cxpagar/facturasrec/contenedorMenu.jsp?codigo="+codigo;
                //System.out.println("next en Filtro"+next);
            }
            
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new Exception("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
            
        }
        catch(Exception e) {
            e.printStackTrace();
            throw new ServletException("Accion:"+ e.getMessage());
        }
        
    }
    
}
