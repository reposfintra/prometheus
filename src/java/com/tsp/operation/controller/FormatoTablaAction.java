/*******************************************************************
 * Nombre clase: FormatoTablaAction.java
 * Descripci�n: Accion para ingresa y buscar todo de formato tablas.
 * Autor: Ing. Jose de la rosa
 * Fecha: 25 de noviembre de 2006, 04:52 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class FormatoTablaAction extends Action{
    
    /** Creates a new instance of FormatoTablaAction */
    public FormatoTablaAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next = "/jsp/masivo/formato_tabla/FormatoTablaIngreso.jsp";
        HttpSession session	= request.getSession ();
        Usuario usuario		= (Usuario) session.getAttribute ("Usuario");
        String dstrct		= (String) session.getAttribute ("Distrito");
        String opcion		= request.getParameter ("opcion")!=null?request.getParameter ("opcion"):"";
        String tabla		= request.getParameter ("c_tabla")!=null?request.getParameter ("c_tabla"):"";
        String formato		= request.getParameter ("c_formato")!=null?request.getParameter ("c_formato"):"";
        try{
            if(opcion.equals ("1")){
                model.formato_tablaService.formatoTabla ("TNOMFOR");
                model.tablaGenService.buscarRegistros ("TFORTIP");
                session.setAttribute ("vecTemp",null);
                try{
                    model.formato_tablaService.existeTabla (tabla);
                }catch(Exception ex){
                    ex.getMessage ();
                }
                if ( model.formato_tablaService.getVecftabla () == null || ( model.formato_tablaService.getVecftabla () != null && model.formato_tablaService.getVecftabla ().size () == 0 ) )
                    request.setAttribute ("mensaje","No hay campos para la tabla: "+tabla);
            }
            else if(opcion.equals ("2")){
                Vector vec    = model.formato_tablaService.getVecftabla ();
                if( session.getAttribute ("vecTemp")==null ){
                    Vector veTemp = new Vector ();
                    for(int i=0; i< vec.size (); i++){
                        Formato_tabla formatoTE = (Formato_tabla) ((Formato_tabla) vec.get (i)).clone ();
                        veTemp.add (formatoTE);
                    }
                    session.setAttribute ("vecTemp",veTemp);
                }
                String []textbox = request.getParameterValues ("checkbox");
                String []primaria = request.getParameterValues ("c_primaria");
                boolean sw = false, swTabla = true, swCampo = true;
                Vector consulta = new Vector ();
                String nombres_formatos = "", nombres_tablas = "", nombres_campos = "";
                if(textbox!=null){
                    Vector camp = new Vector ();
                    for (int j=0; j < textbox.length; j++){
                        int i = Integer.parseInt ( textbox[j] );
                        String tipo1 = "";
                        String primarias        = request.getParameter ("primaria"+i)!=null?request.getParameter ("primaria"+i):"";
                        String campo            = request.getParameter ("c_campo"+i)!=null?request.getParameter ("c_campo"+i):"";
                        String tipo             = request.getParameter ("c_tipo"+i)!=null?request.getParameter ("c_tipo"+i):"";
                        double longitud         = Double.parseDouble   ( request.getParameter ("c_longitud"+i)!=null?!request.getParameter ("c_longitud"+i).equals ("")?request.getParameter ("c_longitud"+i):"0":"0" );
                        int orden               = Integer.parseInt     ( request.getParameter ("c_orden"+i)!=null?!request.getParameter ("c_orden"+i).equals ("")?request.getParameter ("c_orden"+i):"0":"0" );
                        String campoJSP         = request.getParameter ("c_campoJSP"+i)!=null?request.getParameter ("c_campoJSP"+i):"";
                        String titulo           = request.getParameter ("c_titulo"+i)!=null?request.getParameter ("c_titulo"+i):"";
                        String validar          = request.getParameter ("c_validar"+i)!=null?request.getParameter ("c_validar"+i):"";
                        String tabla_val        = request.getParameter ("c_tabla_val"+i)!=null?request.getParameter ("c_tabla_val"+i):"";
                        String campo_val        = request.getParameter ("c_campo_val"+i)!=null?request.getParameter ("c_campo_val"+i):"";
                        String c_default        = request.getParameter ("c_default"+i)!=null?request.getParameter ("c_default"+i):"";
                        Formato_tabla formatabla = (Formato_tabla)vec.get (i);
                        formatabla.setCampo_tabla (campo);
                        formatabla.setTipo_campo (tipo);
                        if( tipo.equals ("numeric") ){
                            String m = "";
                            m = m.valueOf ( longitud );
                            String []def = m.split ("\\.");
                            if (def.length==2){
                                if( Integer.parseInt (def[1]) > 0 )
                                    tipo1 = model.formato_tablaService.converTipoCampo ("TEQTIPO", "double");
                                else
                                    tipo1 = model.formato_tablaService.converTipoCampo ("TEQTIPO", tipo);
                            }
                            else
                                tipo1 = model.formato_tablaService.converTipoCampo ("TEQTIPO", tipo);
                        }
                        else
                            tipo1 = model.formato_tablaService.converTipoCampo ("TEQTIPO", tipo);
                        formatabla.setTipo_campo_equivalente (tipo1);
                        formatabla.setOrden (orden);
                        formatabla.setLongitud ( tipo.equals ("moneda")?15.2:longitud );
                        formatabla.setFormato (formato);
                        formatabla.setTabla (tabla);
                        formatabla.setCampo_jsp (campoJSP);
                        formatabla.setTitulo (titulo);
                        formatabla.setValidar (validar);
                        if( formatabla.getValidar ().equals ("S") ){
                            if( !model.formato_tablaService.estaCampo (tabla_val, campo_val) ){
                                nombres_campos = nombres_campos + campo_val;
                                swCampo = false;
                            }
                            if( !model.formato_tablaService.estaTabla (tabla_val) ){
                                nombres_tablas = nombres_tablas + tabla_val;
                                swTabla = false;
                            }
                        }
                        formatabla.setTabla_val (tabla_val);
                        formatabla.setCampo_val (campo_val);
                        formatabla.setValor_campo (c_default);
                        formatabla.setFecha_creacion ("now()");
                        formatabla.setUsuario_creacion ( usuario.getLogin ().toString () );
                        formatabla.setBase ( usuario.getBase () );
                        formatabla.setDistrito ( dstrct );
                        formatabla.setUltima_modificacion ("0099-01-01 00:00:00");
                        formatabla.setUsuario_modificacion ("");
                        if(primaria!=null){
                            boolean sw1 = false;
                            for (int m=0; m < primaria.length; m++){
                                if( i == Integer.parseInt ( primaria[m] ) )
                                    sw1=true;
                            }
                            if( sw1 )
                                formatabla.setPrimaria ("S");
                            else
                                formatabla.setPrimaria ("N");
                        }
                        else
                            formatabla.setPrimaria ( primarias );
                        camp.add (campo);
                        consulta.add ( model.formato_tablaService.insertFormato_tabla ( formatabla ) );
                        
                    }
                    consulta.add ( model.formato_tablaService.updateTabla ("TNOMFOR", formato, tabla ) );
                    request.setAttribute ("VecCamp", camp);
                }
                if(swTabla)
                    if(swCampo){
                        model.despachoService.insertar (consulta);
                        request.setAttribute ("mensaje","Datos ingresado exitosamente.");
                        next += "?c_tabla=";
                        model.formato_tablaService.formatoTabla ("TNOMFOR");
                        model.formato_tablaService.setVecftabla (null);
                    }
                    else
                        request.setAttribute ("mensajeScript","No se pudo grabar porque los campos "+nombres_campos +" no existen en las tablas a validar.");
                else
                    request.setAttribute ("mensajeScript","No se pudo grabar porque las tablas "+nombres_tablas +" no existen en la BD.");
            }
            else if(opcion.equals ("3")){
                model.tablaGenService.buscarRegistros ("TFORTIP");
                model.formato_tablaService.formatoTablaModificar ("TNOMFOR");
                model.formato_tablaService.existeTablaModificar ( dstrct, formato );
                Vector vecMod = model.formato_tablaService.getVecftablaMod ();
                String tablas = "";
                for(int m=0; m<vecMod.size ();m++){
                    Formato_tabla formatoMod = (Formato_tabla)vecMod.get (m);
                    if(!formatoMod.getTabla ().equals (""))
                        tablas = formatoMod.getTabla ();
                }
                next = "/jsp/masivo/formato_tabla/FormatoTablaModificar.jsp?c_tabla="+tablas;
                try{
                    model.formato_tablaService.existeTabla (tablas);
                }catch(Exception ex){
                    ex.getMessage ();
                }
                if ( model.formato_tablaService.getVecftablaMod () == null || (model.formato_tablaService.getVecftablaMod () != null && model.formato_tablaService.getVecftablaMod ().size () == 0 ) ){
                    request.setAttribute ("mensaje","No existen campos almacenados con ese nombre de tabla y con ese formato");
                    model.formato_tablaService.setVecftabla (null);
                }
            }
            else if(opcion.equals ("4")){
                next = "/jsp/masivo/formato_tabla/FormatoTablaModificar.jsp";
                Vector vec    = model.formato_tablaService.getVecftabla ();
                String []textbox = request.getParameterValues ("checkbox");
                String []primaria = request.getParameterValues ("c_primaria");
                String fecha_creacion = request.getParameter ("fecha_creacion")!=null?request.getParameter ("fecha_creacion"):"now()";
                boolean sw = false, swTabla = true, swCampo = true;
                Vector consulta = new Vector ();
                String nombres_formatos = "", nombres_tablas = "", nombres_campos = "";
                if(textbox!=null){
                    model.formato_tablaService.resetVecftablaMod ();
                    consulta.add ( model.formato_tablaService.deleteTabla ( dstrct, formato, tabla ) );
                    Vector VecMod = new Vector ();
                    for (int j=0; j < textbox.length; j++){
                        int i = Integer.parseInt ( textbox[j] );
                        String tipo1 = "";
                        String primarias        = request.getParameter ("primaria"+i)!=null?request.getParameter ("primaria"+i):"";
                        String campo            = request.getParameter ("c_campo"+i)!=null?request.getParameter ("c_campo"+i):"";
                        String tipo             = request.getParameter ("c_tipo"+i)!=null?request.getParameter ("c_tipo"+i):"";
                        double longitud         = Double.parseDouble   ( request.getParameter ("c_longitud"+i)!=null?!request.getParameter ("c_longitud"+i).equals ("")?request.getParameter ("c_longitud"+i):"0":"0" );
                        int orden               = Integer.parseInt     ( request.getParameter ("c_orden"+i)!=null?!request.getParameter ("c_orden"+i).equals ("")?request.getParameter ("c_orden"+i):"0":"0" );
                        String campoJSP         = request.getParameter ("c_campoJSP"+i)!=null?request.getParameter ("c_campoJSP"+i):"";
                        String titulo           = request.getParameter ("c_titulo"+i)!=null?request.getParameter ("c_titulo"+i):"";
                        String validar          = request.getParameter ("c_validar"+i)!=null?request.getParameter ("c_validar"+i):"";
                        String tabla_val        = request.getParameter ("c_tabla_val"+i)!=null?request.getParameter ("c_tabla_val"+i):"";
                        String campo_val        = request.getParameter ("c_campo_val"+i)!=null?request.getParameter ("c_campo_val"+i):"";
                        String c_default        = request.getParameter ("c_default"+i)!=null?request.getParameter ("c_default"+i):"";
                        Formato_tabla formatabla = (Formato_tabla)vec.get (i);
                        formatabla.setCampo_tabla (campo);
                        formatabla.setTipo_campo (tipo);
                        if( tipo.equals ("numeric") ){
                            String m = "";
                            m = m.valueOf ( longitud );
                            String []def = m.split ("\\.");
                            if (def.length==2){
                                if( Integer.parseInt (def[1]) > 0 )
                                    tipo1 = model.formato_tablaService.converTipoCampo ("TEQTIPO", "double");
                                else
                                    tipo1 = model.formato_tablaService.converTipoCampo ("TEQTIPO", tipo);
                            }
                            else
                                tipo1 = model.formato_tablaService.converTipoCampo ("TEQTIPO", tipo);
                        }
                        else
                            tipo1 = model.formato_tablaService.converTipoCampo ("TEQTIPO", tipo);
                        formatabla.setTipo_campo_equivalente (tipo1);
                        formatabla.setOrden (orden);
                        formatabla.setLongitud ( tipo.equals ("moneda")?15.2:longitud );
                        formatabla.setFormato (formato);
                        formatabla.setTabla (tabla);
                        formatabla.setCampo_jsp (campoJSP);
                        formatabla.setValor_campo (c_default);
                        formatabla.setTitulo (titulo);
                        formatabla.setValidar (validar);
                        if( formatabla.getValidar ().equals ("S") ){
                            if( !model.formato_tablaService.estaCampo (tabla_val, campo_val) ){
                                nombres_campos = nombres_campos + campo_val;
                                swCampo = false;
                            }
                            if( !model.formato_tablaService.estaTabla (tabla_val) ){
                                nombres_tablas = nombres_tablas + tabla_val;
                                swTabla = false;
                            }
                        }
                        formatabla.setTabla_val (tabla_val);
                        formatabla.setCampo_val (campo_val);
                        formatabla.setFecha_creacion (fecha_creacion);
                        formatabla.setUsuario_creacion ( usuario.getLogin ().toString () );
                        formatabla.setBase ( usuario.getBase () );
                        formatabla.setDistrito ( dstrct );
                        formatabla.setUltima_modificacion ("now()");
                        formatabla.setUsuario_modificacion (usuario.getLogin ().toString ());
                        if(primaria!=null){
                            boolean sw1 = false;
                            for (int m=0; m < primaria.length; m++){
                                if( i == Integer.parseInt ( primaria[m] ) )
                                    sw1=true;
                            }
                            if( sw1 )
                                formatabla.setPrimaria ("S");
                            else
                                formatabla.setPrimaria ("N");
                        }
                        else
                            formatabla.setPrimaria ( primarias );
                        VecMod.add (formatabla);
                        consulta.add ( model.formato_tablaService.insertFormato_tabla ( formatabla ) );
                    }
                    model.formato_tablaService.setVecftablaMod (VecMod);
                }
                if(swTabla)
                    if(swCampo){
                        model.despachoService.insertar (consulta);
                        request.setAttribute ("mensajeScript","Datos Modificado exitosamente.");
                        try{
                            model.formato_tablaService.existeTabla (tabla);
                        }catch(Exception ex){
                            ex.getMessage ();
                        }
                        model.tablaGenService.buscarRegistros ("TFORTIP");
                        model.formato_tablaService.formatoTablaModificar ("TNOMFOR");
                        model.formato_tablaService.existeTablaModificar ( dstrct, formato );
                        if ( model.formato_tablaService.getVecftablaMod () == null ){
                            request.setAttribute ("mensaje","No existen campos almacenados con ese nombre de tabla y con ese formato");
                            model.formato_tablaService.setVecftabla (null);
                        }
                    }
                    else
                        request.setAttribute ("mensajeScript","No se pudo grabar porque los campos "+nombres_campos +" no existen en las tablas a validar.");
                else
                    request.setAttribute ("mensajeScript","No se pudo grabar porque las tablas "+nombres_tablas +" no existen en la BD.");
            }
            else{
                Vector consulta = new Vector ();
                next = "/jsp/masivo/formato_tabla/FormatoTablaModificar.jsp?c_tabla=&c_formato=";
                consulta.add ( model.formato_tablaService.deleteTabla ( dstrct, formato, tabla ) );
                consulta.add ( model.formato_tablaService.updateTabla ( "TNOMFOR", formato, "" ) );
                model.despachoService.insertar (consulta);
                model.formato_tablaService.formatoTablaModificar ("TNOMFOR");
                model.formato_tablaService.setVecftabla (null);
                request.setAttribute ("mensaje","Datos Eliminados exitosamente.");
            }
        }catch(SQLException e){
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
}
