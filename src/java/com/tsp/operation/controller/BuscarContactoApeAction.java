/*
 * BuscarContactoApe.java
 *
 * Created on 22 de junio de 2005, 09:01 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  DRIGO
 */
public class BuscarContactoApeAction extends Action {
    
    /** Creates a new instance of BuscarContactonomAction */
    public BuscarContactoApeAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException {
        String pagina = request.getParameter("pagina");
        String next="/"+request.getParameter("carpeta")+"/"+pagina;
        String c_cto = (request.getParameter("c_codigo").toUpperCase())+"%";
        String c_cia = (request.getParameter("c_cia").toUpperCase())+"%";
        String c_tipo = (request.getParameter("c_tipo").toUpperCase())+"%";
        HttpSession session = request.getSession();    

        try{ 
            model.contactoService.buscarContactoNombre(c_cto, c_cia, c_tipo);
            Vector VecContactos = model.contactoService.obtcontactos();
            if ( VecContactos.size() == 0){
                next = "/jsp/trafico/mensaje/ErrorBusq.jsp?ruta=/jsp/trafico/contacto/BuscarContacto.jsp";
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
         
         // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);

    }
    
}
