/*
 * BuscarPaisAction.java
 *
 * Created on 28 de febrero de 2005, 04:29 PM 
 */

package com.tsp.operation.controller;

/**
 *
 * @author  DIBASMO
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

public class BuscarPaisAction extends Action {
    
    /** Creates a new instance of BuscarPaisAction */
    public BuscarPaisAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException{
        String next="/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina")+"?sw=1";
        HttpSession session = request.getSession();
        Pais pais;
        try{ 
           
            model.paisservice.buscarpais(request.getParameter("codigo") ); 
            
            next = Util.LLamarVentana(next, "Modificar Pais");
            
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
         
         // Redireccionar a la p�gina indicada.
         this.dispatchRequest(next);

    }
    
}
