/*
 * Nombre        ReporteTiemposDeViajeConductoresAction.java
 * Descripci�n   Ejecuta las acciones necesarias para elaborar el reporte de tiempos de viajes.
 * Autor         Alejandro Payares
 * Fecha         24 de enero de 2006, 03:56 PM
 * Version       1.0
 * Coyright      Transportes Sanchez Polo SA.
 */

package com.tsp.operation.controller;

import com.tsp.operation.model.threads.HExportarExcel_TiemposDeViajesConductores;
import com.tsp.operation.model.beans.Usuario;

/**
 * Ejecuta las acciones necesarias para elaborar el reporte de tiempos de viajes.
 * @author Alejandro Payares
 */
public class ReporteTiemposDeViajeConductoresAction extends Action{
    
    /**
     * Crea una nueva instancia de ReporteTiemposDeViajeConductoresAction
     * @autor  Alejandro Payares
     */
    public ReporteTiemposDeViajeConductoresAction() {
    }
    
    /**
     * Ejecuta la accion propia de la clase
     * @throws ServletException si algun problema ocurre al enviar la respuesta al cliente
     * @throws InformationException para enviar mensajes informativos
     */    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "";
        try {  
            String cmd = request.getParameter("cmd");
            Usuario user = (Usuario)request.getSession().getAttribute("Usuario");
            if ( cmd == null ) {
                model.ciudadService.searchTreMapCiudades();
                next = "/jsp/sot/body/PantallaFiltroReporteTiempoConductores.jsp";
                ////System.out.println("Se cargaron las ciudades");
            }
            else if ( "show".equals(cmd) ) {
                String inicio = request.getParameter("inicio");
                String fin = request.getParameter("fin");
                String cliente = request.getParameter("cliente");
                String nombreCliente = request.getParameter("nombreCliente");
                String placa = request.getParameter("placa");
                String origen = request.getParameter("origen");
                String destino = request.getParameter("destino");
                String nombreOrigen = request.getParameter("nombreOrigen");
                String nombreDestino = request.getParameter("nombreDestino");
                String formatoSalida = request.getParameter("formatoSalida");
                if ( "web".equals(formatoSalida) ) {
                    model.repTiempoDeViajeConductores.buscarDatosReporte(inicio, fin, cliente, placa, origen, destino);
                    next = "/jsp/sot/reports/ReporteTiempoDeViajesConductores.jsp?inicio="+inicio+"&fin="+fin+"&origen="+nombreOrigen+"&destino="+nombreDestino+"&cliente="+nombreCliente;
                }
                else {
                    String argumentos [] = new String[HExportarExcel_TiemposDeViajesConductores.NUMERO_DE_PARAMETROS];
                    argumentos[HExportarExcel_TiemposDeViajesConductores.CLIENTE] = cliente;
                    argumentos[HExportarExcel_TiemposDeViajesConductores.DESTINO] = destino;
                    argumentos[HExportarExcel_TiemposDeViajesConductores.FECHA_FIN] = fin;
                    argumentos[HExportarExcel_TiemposDeViajesConductores.FECHA_INICIO] = inicio;
                    argumentos[HExportarExcel_TiemposDeViajesConductores.NOMBRE_CLIENTE] = nombreCliente;
                    argumentos[HExportarExcel_TiemposDeViajesConductores.NOMBRE_DESTINO] = nombreDestino;
                    argumentos[HExportarExcel_TiemposDeViajesConductores.NOMBRE_ORIGEN] = nombreOrigen;
                    argumentos[HExportarExcel_TiemposDeViajesConductores.ORIGEN] = origen;
                    argumentos[HExportarExcel_TiemposDeViajesConductores.PLACA] = placa;
                    HExportarExcel_TiemposDeViajesConductores htvc = new HExportarExcel_TiemposDeViajesConductores(model, user, argumentos);
                    model.ciudadService.searchTreMapCiudades();
                    htvc.generar();
                    next = "/jsp/sot/body/PantallaFiltroReporteTiempoConductores.jsp?mensaje=El archivo excel est� siendo generado, cuando termine puede consultarlo en el manejador de archivos";
                }
            }
        }
        catch( Exception ex ){
            ex.printStackTrace();
        }
        this.dispatchRequest(next);
    }
    
}
