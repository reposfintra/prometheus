 /********************************************************************
 *      Nombre Clase.................   FinalizarDemoraTramoAction.java 
 *      Descripci�n..................   Finaliza una demora
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   31.08.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;

/**
 *
 * @author  fvillacob
 */
public class FinalizarDemoraTramoAction extends Action{
    
    /** Creates a new instance of FinalizarDemoraTramoAction */
    public FinalizarDemoraTramoAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        HttpSession session = request.getSession();
        String numcaravana = request.getParameter("numcaravana");
        String next = "/jsp/trafico/demora/consultarDemoraTramoParam.jsp";
        
        //Usuario en sesi�n
        Usuario usuario = (Usuario) session.getAttribute("Usuario");    
        
        try{
            Vector planillas = model.demorasSvc.planillasTramo();
            Vector dems = new Vector();
            String numpla = ""; 
            for(int i=0; i<planillas.size(); i++){
                numpla = (String) planillas.elementAt(i);
                Demora dem = model.demorasSvc.obtenerDemora(numpla);
                if(dem!=null && dem.getFinalizada().matches("no"))
                    dems.add(dem);
            }
            Vector dems2 = new Vector();
            for(int i=0; i<dems.size(); i++){                
                Demora dem = new Demora();
                dem =  (Demora) dems.elementAt(i);
                dem.setUsuario_finalizacion(usuario.getLogin());
                dem.setUsuario_modificacion(usuario.getLogin());            
                model.demorasSvc.finalizarDemora(dem); 
                dem.setFinalizada("si");
                dems2.add(dem);
            }                      
            session.removeAttribute("Demoras");
            session.setAttribute("Demoras", dems2);
            next += "?msg=Demoras registradas finalizada.";
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
