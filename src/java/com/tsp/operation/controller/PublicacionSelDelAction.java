/******************************************************************************
 * Nombre clase :                   PublicacionSelDelAction.java              *
 * Descripcion :                    Clase que maneja los eventos              *
 *                                  relacionados con el programa de           *
 *                                  Insertar una Publicacion en la BD.        *
 * Autor :                          LREALES                                   *
 * Fecha :                          21 de abril de 2006, 04:52 PM             *
 * Version :                        1.0                                       *
 * Copyright :                      Fintravalores S.A.                   *
 *****************************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import com.tsp.exceptions.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.Util;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.threads.*;
import java.text.*;
import java.sql.SQLException;
import com.tsp.operation.model.*;

public class PublicacionSelDelAction extends Action {
    
    /** Creates a new instance of PublicacionSelDelAction */
    public PublicacionSelDelAction () {
    }

    public void run () throws ServletException, InformationException {
        
        String next = "/jsp/publicacion/eliminarPublicacion/EliminarPublicacion1.jsp";
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = (String) session.getAttribute("Distrito");
        String creation_user = usuario.getLogin().toUpperCase();
        
        try{
            
            String fecha_creacion = ( request.getParameter("fecha_creacion") != null )?request.getParameter("fecha_creacion").toUpperCase() :"";
            int num_dir = ( Integer.parseInt( request.getParameter("num_dir") ) != 0 )?Integer.parseInt( request.getParameter("num_dir") ) :0;
            
            model.publicacionService.obtenerPublicacion( distrito, num_dir, fecha_creacion, creation_user );
            
            next = "/jsp/publicacion/eliminarPublicacion/EliminarPublicacion2.jsp?m=no";
            
        } catch ( Exception e ){
                        
            throw new ServletException( e.getMessage () );
            
        }
        
        this.dispatchRequest( next );
        
    }
    
}