/***********************************************
 * Nombre clase: DiscrepanciaInsertSoporteAction.java
 * Descripci�n: Accion para ingresar un docuemnto de soporte a la tabla control soporte.
 * Autor: Jose de la rosa
 * Fecha: 17 de enero de 2006, 04:15 PM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 **********************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class DiscrepanciaInsertSoporteAction extends Action{
    public static String soporte = "";
    public static String descripcion = "";
    /** Creates a new instance of DiscrepanciaInsertSoporteAction */
    public DiscrepanciaInsertSoporteAction () {
    }
    public void run () throws ServletException, InformationException {
        HttpSession session = request.getSession ();
        String next = "", mensaje = "";
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String distrito = (String) session.getAttribute ("Distrito");
        String numpla = request.getParameter ("c_numpla").toUpperCase ();
        String numrem = request.getParameter ("c_numrem").toUpperCase ();
        String opc = request.getParameter ("opc");
        try {
            if(opc.equals ("true")){
                model.cumplidoService.listarControlSoporte (numrem);
                next = "/jsp/cumplidos/discrepancia/DiscrepanciaInsertarSoporte.jsp?c_numpla=" + numpla+"&numrem="+numrem+"&sw=true";
                model.discrepanciaService.listDiscrepanciaGeneral (numpla, distrito);
            }
            else{
                Vector consultas = new Vector ();
                Cumplido cumplido = new Cumplido ();
                String observacion = "Documento ";
                String []LOV = request.getParameterValues ("LOV");
                String []LOVC = request.getParameterValues ("LOVC");
                int sw=0;
                boolean sw1,sw2,swN;
                model.cumplidoService.listarControlSoporte (numrem);
                Vector vecc = model.cumplidoService.getCumplidos ();
                if(vecc != null){
                    for(int i=0; i<vecc.size ();i++){
                        Cumplido c = (Cumplido)vecc.elementAt (i);
                        sw1=false;
                        if(LOV!=null){
                            if(LOV.length>0){
                                for(int j=0;j<LOV.length;j++){
                                    this.Conversion (LOV[j]);
                                    if( soporte.equals ( c.getSoporte () ) ){
                                        sw1=true;
                                    }
                                }
                            }
                        }
                        swN=false;
                        if(LOVC!=null){
                            if(LOVC.length>0){
                                for(int k=0;k<LOVC.length;k++){
                                    this.Conversion (LOVC[k]);
                                    if( soporte.equals ( c.getSoporte () ) ){
                                        swN=true;
                                    }
                                }
                            }
                        }
                        if(swN==false){
                            cumplido.setSoporte (c.getSoporte ());
                            cumplido.setRemesa (numrem);
                            cumplido.setUsuario_modificacion (usuario.getLogin ());
                            cumplido.setUsuario_creacion (usuario.getLogin ());
                            cumplido.setBase (usuario.getBase ());
                            cumplido.setDistrict (distrito);
                            cumplido.setEntregado_conductor ( (sw1==true)?"S":"N" );
                            if(! model.cumplidoService.existeControlSoporte(distrito, numrem, c.getSoporte ()) ){
                                consultas.add ( model.cumplidoService.insertarControl_proceso (cumplido) );
                                mensaje = "Soporte Incompleto Ingresado";
                            }
                            else
                                mensaje = "Ya Existen unos de los Registros";
                        }
                    }
                }
                sw2=true;
                if(vecc!=null){
                    for(int i=0; i<vecc.size ();i++){
                        Cumplido c = (Cumplido)vecc.elementAt (i);
                        sw1=false;
                        if(LOV!=null){
                            if(LOV.length>0){
                                for(int j=0;j<LOV.length;j++){
                                    this.Conversion (LOV[j]);
                                    if( soporte.equals ( c.getSoporte () ) ){
                                        sw1=true;
                                    }
                                }
                            }
                        }
                        swN=false;
                        if(LOVC!=null){
                            if(LOVC.length>0){
                                for(int k=0;k<LOVC.length;k++){
                                    this.Conversion (LOVC[k]);
                                    if( soporte.equals ( c.getSoporte () ) ){
                                        swN=true;
                                    }
                                }
                            }
                        }
                        if(sw1==false && swN==false){
                            observacion += c.getComent () + ",";
                            sw2=false;
                        }
                    }
                }
                if(sw2==false){
                    observacion = observacion.substring (0, observacion.length ()-1);
                    Discrepancia dis = new Discrepancia ();
                    dis.setNro_planilla (numpla);
                    dis.setNro_remesa ("");
                    dis.setTipo_doc_rel ("");
                    dis.setDocumento_rel ("");
                    dis.setTipo_doc ("");
                    dis.setDocumento ("");
                    dis.setUbicacion ("");
                    dis.setContacto ("");
                    dis.setNumero_rechazo ("");
                    dis.setReportado ("");
                    dis.setFecha_devolucion ("0099-01-01 00:00:00");
                    dis.setRetencion ("S");
                    dis.setObservacion (observacion);
                    dis.setUsuario_modificacion (usuario.getLogin ());
                    dis.setUsuario_creacion (usuario.getLogin ());
                    dis.setBase (usuario.getBase ());
                    dis.setDistrito (distrito);
                    model.discrepanciaService.setDiscrepancia (dis);
                    request.setAttribute ("msg", "Discrepancia Agregada");
                    consultas.add ( model.discrepanciaService.insertarDiscrepancia (dis) );
                    model.discrepanciaService.searchDiscrepancia (numpla, "", "", "", "", "", distrito );
                    Discrepancia d = model.discrepanciaService.getDiscrepancia ();
                    dis.setNro_discrepancia (model.discrepanciaService.numeroDiscrepancia());
                    dis.setCod_producto ("");
                    dis.setUnidad ("");
                    dis.setValor (0);
                    dis.setDescripcion ("");
                    dis.setCantidad (0);
                    dis.setCausa_dev ("");
                    dis.setResponsable ("");
                    dis.setUsuario_modificacion (usuario.getLogin ());
                    dis.setUsuario_creacion (usuario.getLogin ());
                    dis.setBase (usuario.getBase ());
                    dis.setDistrito (distrito);
                    dis.setGrupo ("");
                    model.discrepanciaService.setDiscrepancia (dis);
                    dis.setCod_discrepancia ("SI");
                    consultas.add ( model.discrepanciaService.insertarDiscrepanciaProducto (dis) );
                    model.despachoService.insertar (consultas);
                    mensaje += ", Discrepancia Ingresada";
                    
                }
                next="/jsp/cumplidos/discrepancia/DiscrepanciaInsertarGeneral.jsp?c_numpla"+numpla;
                model.discrepanciaService.listDiscrepanciaGeneral (numpla, distrito);
                model.planillaService.obtenerInformacionPlanilla2 (numpla);
                model.planillaService.obtenerNombrePropietario2 (numpla);
                request.setAttribute ("msg",mensaje);
            }
        }catch (SQLException e) {
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
    public static void Conversion (String f){
        String vF [] = f.split (",");
        soporte = vF[0];
        descripcion = vF[1];
    }
}
