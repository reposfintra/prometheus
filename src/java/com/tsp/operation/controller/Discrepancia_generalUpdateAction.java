/************************************************************************
 * Nombre clase: Discrepancia_generalUpdateAction.java
 * Descripci�n: Accion para actualizar las discrepancias.
 * Autor: Jose de la rosa
 * Fecha: 21 de noviembre de 2005, 09:57 AM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 **************************************************************************/

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.util.*;
public class Discrepancia_generalUpdateAction extends Action{
    
    /** Creates a new instance of Discrepancia_generalUpdateAction */
    public Discrepancia_generalUpdateAction () {
    }
    
    public void run () throws ServletException, InformationException {
        HttpSession session = request.getSession ();
        String numpla = request.getParameter ("c_numpla").toUpperCase ();
        String nro_rechazo = (request.getParameter ("c_nro_rechazo")!=null)?request.getParameter ("c_nro_rechazo"):"";
        String retencion = request.getParameter ("c_retencion");
        String observacion = (request.getParameter ("c_observacion")!=null)?request.getParameter ("c_observacion"):"";
        String unidad = request.getParameter ("c_unidad");
        String fecha = request.getParameter ("c_fecha");
        double cantidad = Double.parseDouble (request.getParameter ("c_cantidad"));
        String cod_discrepancia = request.getParameter ("c_cod_discrepancia");
        double valor = 0;
        String num = (request.getParameter ("c_num_discre")!=null)?request.getParameter ("c_num_discre"):"0";
        int num_discre = Integer.parseInt (num);
        if( request.getParameter ("c_valor")!=null && !request.getParameter ("c_valor").equals ("") ){
            valor = Double.parseDouble (request.getParameter ("c_valor"));
        }
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String distrito = (String) session.getAttribute ("Distrito");
        String next = "";
        int sw=0;
        try {
            Discrepancia dis = new Discrepancia ();
            dis.setNro_planilla (numpla);
            dis.setNro_discrepancia (num_discre);
            dis.setRetencion (retencion);
            dis.setObservacion (observacion);
            dis.setUsuario_modificacion (usuario.getLogin ());
            model.discrepanciaService.setDiscrepancia (dis);
            model.discrepanciaService.updateDiscrepanciaGeneral (dis);
            dis.setNro_planilla (numpla);
            dis.setNro_discrepancia (num_discre);
            dis.setUnidad (unidad);
            dis.setValor (valor);
            dis.setCantidad (cantidad);
            dis.setFecha_creacion (fecha);
            dis.setUsuario_modificacion (usuario.getLogin ());
            dis.setCod_discrepancia (cod_discrepancia);
            model.discrepanciaService.setDiscrepancia (dis);
            model.discrepanciaService.updateDiscrepanciaProductoGeneral (dis);
            next="/jsp/cumplidos/discrepancia/DiscrepanciaModificarGeneral.jsp?reload=ok&c_numpla"+numpla;
            model.discrepanciaService.searchDiscrepanciaGeneral (numpla, num_discre, distrito);
            model.planillaService.obtenerInformacionPlanilla2(numpla);
            model.planillaService.obtenerNombrePropietario2(numpla);
            request.setAttribute ("msg","Discrepancia Modificada");
        }catch (SQLException e) {
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
    
}
