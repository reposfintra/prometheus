/***********************************************************************************
 * Nombre clase : ............... RemiDestUpdateAction.java                        *
 * Descripcion :................. Clase que maneja los eventos relacionados con la *
 *                                actualizacion de los Remitentes y Destinatarios  * 
 * Autor :....................... Ing. Henry A.Osorio Gonz�lez                     *
 * Fecha :....................... 20 de noviembre de 2005, 9:30 AM                 *
 * Version :..................... 1.0                                              *
 * Copyright :................... Fintravalores S.A.                          *
 ***********************************************************************************/

package com.tsp.operation.controller;


import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class RemiDestUpdateAction extends Action{    
    
    public RemiDestUpdateAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException {                
        
        String next = "/jsp/masivo/remitente_destinatario/rem_destUpdate.jsp?msg=";
        HttpSession session = request.getSession();
        String msg = "";
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        RemiDest remdest = new RemiDest();
        String codigo     =  request.getParameter("codigo");        
        String nombre  =  request.getParameter("nombre");
        String direccion     =  request.getParameter("direccion");               
        String estado     =  request.getParameter("est");               
        String referencia = request.getParameter("referencia");
        
         //Ivan Dario 30 Oct 2006
        String nombre_contacto = request.getParameter("nombre_contacto");  
        String telefono = request.getParameter("telefono"); 
        
        
        /* Seteando el objeto remidest */
        remdest.setCodigo(codigo);
        remdest.setNombre(request.getParameter("nombre"));
        remdest.setDireccion(request.getParameter("direccion"));
        remdest.setReferencia(referencia);
        remdest.setUser_update(usuario.getLogin());
        remdest.setLast_update("now()");
        remdest.setEstado(estado); //Estado del registro A=Activo
        
        //Ivan Dario 30 Oct 2006
        remdest.setNombre_contacto(nombre_contacto);
        remdest.setTelefono(telefono);
        
        
        ////System.out.println(estado);
        if (estado.equals("")){
            remdest.setReg_status("A");
        } else {
            remdest.setReg_status("");
        }
        try{ 
            model.remidestService.updateRemitenteDestinatario(remdest);
            model.remidestService.listarRemitentesDestinatarios(codigo);
            msg = "Registro actualizado Exitosamente!";                                        
            if (estado.equals("")){
                msg = "Registro anulado Exitosamente";
            }
        }catch (SQLException e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }         
        next+=msg+"&pos=0";
        this.dispatchRequest(next);
        
    }
    
}
