/*************************************************************************
 * Nombre:        ConsultarPlanillaRemesaAction.java            *
 * Descripci�n:   Clase buscar la informacion de la planilla - remesa    *
 * Autor:         Ing. Diogenes Antonio Bastidas Morales   *
 * Fecha:         27 de marzo de 2006, 04:04 PM                              *
 * Versi�n        1.0                                      *
 * Coyright:      Transportes Sanchez Polo S.A.            *
 *************************************************************************/

package com.tsp.operation.controller;


import java.io.*;
import java.util.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.Util;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.exceptions.*;

public class ConsultarPlanillaRemesaAction extends Action{ 
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String login=usuario.getLogin();
        String next="";
        String distrito = request.getParameter("distrito")!=null ? request.getParameter("distrito"):"FINV";
        String planilla = request.getParameter("planilla");
        
        String oc=request.getParameter("numeroOC").toUpperCase();
        
        String ot = request.getParameter("numeroOT").toUpperCase();
        
        try{
            if(!login.equals("") ){
                String comentario="";
                
                model.PlanillasSvc.consultarOC(distrito,oc,usuario.getBase());
                
                Planillas p = model.PlanillasSvc.getPlanilla();
                if(p!=null){
                    model.remesaService.consultarInfoRemesa(ot,distrito);
                }
                else{
                    comentario= "No se encontro la Planilla";
                }
                if(model.remesaService.getRemesa()==null){
                    comentario= "No se encontro la remesa";
                }
                next= "/datosplanilla/MostrarOCNormal.jsp?marco=no&comentario="+comentario;
                model.plremSvc.obtenerDatosPlarem(oc,ot);
                model.RemDocSvc.buscarDestinatariosRemesa(ot, distrito);
                model.RemDocSvc.buscarDocumentosRemesa(ot,distrito);
                //Cuando la Remesa no tiene Planilla se redirecciona a Informaci�n de la remesa
                if ( p==null ) {
                    next = "/colpapel/infoRemesa.jsp?numrem="+ot;
                }
              
                // busqueda del centro de costos
                /*
                if (p!=null && model.remesaService.getRemesa()!=null ){
                    model.remesaService.obtenerCentroCostos(p, model.remesaService.getRemesa());
                }*/ 
            }
        }    
    catch(Exception e){
        e.printStackTrace();
        throw new ServletException(e.getMessage());
    } 
    this.dispatchRequest(next);
    
}// end run

}// en accion
