/**
 *
 */
package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.sql.*;
import java.util.*;
import java.text.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;

/**
 * Searches the database for puchase orders matching the
 * purchase search argument
 */
public class CaravanaPlanViajeAddAction extends Action {
  static Logger logger = Logger.getLogger(CaravanaPlanViajeAddAction.class);
  
  /**
   * Executes the action
   */
  public void run() throws ServletException, InformationException {
      String cmd = request.getParameter("cmd");
      String next = "";
      String retorno = null;
    
      HttpSession session = request.getSession();
      Usuario usuario = (Usuario)session.getAttribute("Usuario");
      Calendar fecha = Calendar.getInstance();
      SimpleDateFormat FFormat = new SimpleDateFormat("yyyyMMdd");
      SimpleDateFormat TFormat = new SimpleDateFormat("HHmm");
      String last_mod_date = FFormat.format(fecha.getTime());
      String last_mod_time = TFormat.format(fecha.getTime());
      PlanViaje planVj = (PlanViaje)session.getAttribute("planVj");
      Caravana caravana = (Caravana)session.getAttribute("caravana");
      NitSot conductor = null;
      NitSot propietario = null;
      
      planVj.setCia(request.getParameter("cia"));
      planVj.setPlanilla(request.getParameter("planilla"));
      planVj.setPlaca(request.getParameter("placa"));
      planVj.setProducto(request.getParameter("producto"));
      planVj.setTrailer(request.getParameter("trailer"));
      planVj.setContenedor(request.getParameter("contenedor"));
      planVj.setCodtipocarga(request.getParameter("codtipocarga"));
      
      planVj.setFecha(request.getParameter("fecha"));
      planVj.setDestinatario(request.getParameter("destinatario"));
      planVj.setRuta(request.getParameter("ruta"));
      planVj.setCedcon(request.getParameter("cedcon"));
      
      planVj.setRadio(request.getParameter("radio"));
      planVj.setCelular(request.getParameter("celular"));
      planVj.setAvantel(request.getParameter("avantel"));
      planVj.setTelefono(request.getParameter("telefono"));
      planVj.setCazador(request.getParameter("cazador"));
      planVj.setMovil(request.getParameter("movil"));
      planVj.setOtro(request.getParameter("otro"));
      
      planVj.setNomfam(request.getParameter("nomfam"));
      planVj.setPhonefam(request.getParameter("phonefam"));
      planVj.setNitpro(request.getParameter("nitpro"));
      planVj.setComentario1(request.getParameter("comentario1"));
      planVj.setComentario2(request.getParameter("comentario2"));
      planVj.setComentario3(request.getParameter("comentario3"));
      planVj.setLast_mod_date(last_mod_date);
      planVj.setLast_mod_time(last_mod_time);
      planVj.setLast_mod_user(usuario.getLogin());
      planVj.setUsuario(usuario.getLogin());
      
      retorno = request.getParameter("retorno");
      if (retorno == null)
          retorno = "N";
      
      planVj.setRetorno(retorno);
      
      try {
        //busca el nit del conductor para crearlo o actualizarlo
        if (model.nitService.searchNit(planVj.getCedcon())){
          conductor = model.nitService.getNit();
          conductor.setNombre(request.getParameter("nomcon"));
          conductor.setDireccion(request.getParameter("dircon"));
          conductor.setTelefono(request.getParameter("phonecon"));
          conductor.setCodciu(request.getParameter("ciucon"));
          model.nitService.updateNit(conductor);
        }
        else{
          conductor = new NitSot();
          conductor.setCedula(request.getParameter("cedcon"));
          conductor.setNombre(request.getParameter("nomcon"));
          conductor.setDireccion(request.getParameter("dircon"));
          conductor.setTelefono(request.getParameter("phonecon"));
          conductor.setCodciu(request.getParameter("ciucon"));
          conductor.setCellular("");
          conductor.setCoddpto("");
          conductor.setCodpais("");
          conductor.setE_mail("");
          conductor.setFechanac("0099-01-01");
          conductor.setId_mims("");
          conductor.setSexo("");
          conductor.setUsuariocrea("");
          conductor.setUsuario("");
          model.nitService.insertNit(conductor);
        }
        
        //busca el nit del propetrio para crearlo o actualizarlo
        if (model.nitService.searchNit(planVj.getNitpro())){
          propietario = model.nitService.getNit();
          propietario.setNombre(request.getParameter("nompro"));
          propietario.setDireccion(request.getParameter("dirpro"));
          propietario.setTelefono(request.getParameter("phonepro"));
          propietario.setCodciu(request.getParameter("ciupro"));
          model.nitService.updateNit(propietario);
        }
        else{
          propietario = new NitSot();
          propietario.setCedula(request.getParameter("nitpro"));
          propietario.setNombre(request.getParameter("nompro"));
          propietario.setDireccion(request.getParameter("dirpro"));
          propietario.setTelefono(request.getParameter("phonepro"));
          propietario.setCodciu(request.getParameter("ciupro"));
          propietario.setCellular("");
          propietario.setCoddpto("");
          propietario.setCodpais("");
          propietario.setE_mail("");
          propietario.setFechanac("0099-01-01");
          propietario.setId_mims("");
          propietario.setSexo("");
          propietario.setUsuariocrea("");
          propietario.setUsuario("");
          model.nitService.insertNit(propietario);
        }
        
        caravana.setPlanilla(planVj.getPlanilla());
        caravana.setUsuario(usuario.getLogin());
        caravana.setCia(planVj.getCia());
        
        model.planViajeService.createPlanVj(planVj);
        model.caravanaService.crearCaravana(caravana);
        
        planVj.setProducto("");
        planVj.setTrailer("");
        planVj.setContenedor("");
        planVj.setFecha("");
        planVj.setDestinatario("");
        planVj.setRuta("");
        planVj.setCedcon("");
        
        planVj.setRadio("");
        planVj.setCelular("");
        planVj.setAvantel("");
        planVj.setTelefono("");
        planVj.setCazador("");
        planVj.setMovil("");
        planVj.setOtro("");
        
        planVj.setNomfam("");
        planVj.setPhonefam("");
        planVj.setComentario1("");
        planVj.setComentario2("");
        planVj.setComentario3("");
        
        session.setAttribute("planVj", planVj);
        logger.info(usuario.getNombre() + " Cre� CARAVANA No = " + caravana + " con Planilla = " + planVj.getPlanilla());
      }
      catch (Exception e){
        throw new ServletException(e.getMessage());
      }
    
      next = "/caravana/ConfirmacionAdicionPlanillaCaravana.jsp";
    // Redireccionar a la p�gina indicada.
    this.dispatchRequest(next);
  }
}
