/*
 * InformacionCaravanaAction.java
 *
 * Created on 18 de agosto de 2005, 08:25 PM
 */

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;

/**
 *
 * @author  Andr�s
 */
public class InformacionCaravanaAction extends Action {
    
    /** Creates a new instance of InformacionCaravanaAction */
    public InformacionCaravanaAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String numcaravana = request.getParameter("numcaravana");
        //Info del usuario
        //HttpSession session = request.getSession();
        //Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        //Pr�xima vista
        String next = "/jsp/trafico/demora/asignarDemoraCaravanaParam.jsp";
        
        try{
            /*model.plSvc.bucaPlanilla(numpla);
            Planilla pla = model.plSvc.getPla();
            ////System.out.println("numpla" + pla.getNumpla());*/
            if( model.demorasSvc.existeCaravana(Integer.valueOf(numcaravana)) )            
                next = next + "?numcaravana=" + numcaravana;
            else
                next = "/jsp/trafico/demora/asignarDemoraCaravana.jsp?msg=" +
                    "N�mero de caravana no existe.";
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
