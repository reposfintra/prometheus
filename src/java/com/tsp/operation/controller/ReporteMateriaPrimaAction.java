/*
 * ReporteMateriaPrimaAction.java
 *
 * Created on 2 de agosto de 2006, 05:01 PM
 */

/********************************************************************
 *  Nombre Clase.................   ReporteMateriaPrimaAction.java
 *  Descripci�n..................   Action para generar el reporte de materia prima
 *  Autor........................   David Pi�a Lopez
 *  Fecha........................   2 de agosto de 2006, 05:01 PM
 *  Versi�n......................   1.0
 *  Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;

/**
 *
 * @author  David
 */
public class ReporteMateriaPrimaAction extends Action {
    
    /** Creates a new instance of ReporteMateriaPrimaAction */
    public ReporteMateriaPrimaAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        
        String next = "";        
        String user="";
        boolean todas;
        try{
            String cliente      = request.getParameter( "cliente" )     ==null?"":request.getParameter( "cliente" );
            String tipodoc      = request.getParameter( "tipodoc" )     ==null?"":request.getParameter( "tipodoc" );
            String codigo       = request.getParameter( "impo" )        ==null?"":request.getParameter( "impo" );
            String pendiente    = request.getParameter( "pendiente" )   ==null?"":request.getParameter( "pendiente" );
            String sin_cdr      = request.getParameter( "sin_cdr" )     ==null?"":request.getParameter( "sin_cdr" );
            String tipo         = request.getParameter( "tipo" )        ==null?"":request.getParameter( "tipo" );
            String inicio       = request.getParameter( "inicio" )      ==null?"":request.getParameter( "inicio" );
            String fin          = request.getParameter( "fin" )         ==null?"":request.getParameter( "fin" );
            
            HttpSession session = request.getSession ();
            Usuario usuario = (Usuario)session.getAttribute ("Usuario");
            
            
            
            
            if( !tipo.equals ("") ){
                model.impoExpoService.obtenerImpoExpoCliente( usuario.getDstrct(), cliente, tipodoc, codigo, pendiente, sin_cdr, inicio, fin );
                next = "/jsp/masivo/reportes/ListaImpoExpoMateriaPrima.jsp";
            }
            else{
                reporteImportacionesMateriaPrima hilo = new reporteImportacionesMateriaPrima(); 
                hilo.start( codigo, usuario.getLogin(), model );
                next = "/jsp/masivo/reportes/ReporteImpoExpoMateriaPrima.jsp";
                request.setAttribute("mensaje", "Proceso Generaci�n de Reporte de Materia Prima Iniciado");
            }
            
        }catch (Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
    }
    
}
