/*
 * Nombre        AsignacionNitsPropietarioAction.java
 * Descripci�n   clase que controla las acciones para la asignacion de nits a propietarios
 * Autor         Ivan Dario Gomez Vanegas
 * Fecha         17 de febrero de 2006, 14:10
 * Version       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.controller;

import java.io.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;
import javax.servlet.jsp.*;
import javax.servlet.http.*;
import com.tsp.operation.model.*;
import com.tsp.util.Utility;
import java.util.*;
import com.tsp.operation.model.threads.*;


/**
 * @author  Ivan Gomez
 */
public class AsignacionNitsPropietarioAction extends Action {
    
    static Logger logger = Logger.getLogger(AsignacionNitsPropietarioAction.class);
    
    
    
    public void run() throws ServletException, InformationException {
        String next = null;
        
        String Login = request.getParameter("Usuario");
        if( Login == null ) {
             try {
                  model.UbicaVehicuPorpSvc.BuscarPropietarios(); 
             } catch (Exception e) {
                  e.printStackTrace();
             }
             next = "/jsp/general/AsignacionNitsPropietario.jsp";
        }else{
           try{
               String Opcion = request.getParameter("Opcion");
               if(Opcion.equals("BUSCAR")){
                   model.UbicaVehicuPorpSvc.BuscarNits(Login); 
                   next = "/jsp/general/AsignacionNitsPropietario.jsp?Asignar=Asignar";
               }else if(Opcion.equals("UPDATE")){
                  String Nits  = (request.getParameter("Nits")!= null)?request.getParameter("Nits"):""; 
                  model.UbicaVehicuPorpSvc.UpdateNit(Nits, Login);
                  next = "/jsp/general/AsignacionNitsPropietario.jsp?Msg=Cambios realizados con exito";
                  
               }
           } catch(Exception e){
                e.printStackTrace();
           }    
        }
            
        
        //System.out.println("redirecionando hacia: "+next);
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);
    }
}

