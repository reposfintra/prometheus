/********************************************************************
 *      Nombre Clase.................   MigracionOTFitDefAction.java
 *      Descripci�n..................   Acci�n que genera el archivo de migraci�n Fitmen-Defitmen
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   21.07.2006
 *      Versi�n......................   1.1
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.Util;

/**
 *
 * @author  Andr�s
 */
public class MigracionOTFitDefAction extends Action{
    
    /** Creates a new instance of MigracionOTFitDefAction */
    public MigracionOTFitDefAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String next="/migracion/migracionOTFitDef.jsp?msg=";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String cia = (String) session.getAttribute("Distrito");
        
        Vector lineas = new Vector();
        
        try{
            lineas = model.migFitDefSvc.migrarOTFitDef(cia);
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        
        //sandameg 190905
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String path = rb.getString("ruta");
        
        String nombre_archivo = "";
        String ruta = path + "/exportar/migracion/" + usuario.getLogin() + "/";

        String fecha_actual = Util.getFechaActual_String(6);

        //ruta += fecha_actual.substring(0,4) + fecha_actual.substring(5,7);
        nombre_archivo = ruta + "OTFITDEF " + fecha_actual.replace(':','_').replace('/','_') + ".csv";
        
        HGenerarCSV hilo = new HGenerarCSV();
        hilo.start(lineas, ruta, nombre_archivo);                           
        
        String msg = "Migraci�n de los Fitmen-Defitmen se realiz� de manera exitosa.";
        this.dispatchRequest(next+msg);
    }
}
