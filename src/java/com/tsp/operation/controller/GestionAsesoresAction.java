package com.tsp.operation.controller;

import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.Asesores;
import com.tsp.operation.model.beans.BeanGeneral;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.services.AsesoresService;
import com.tsp.util.Util;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;

/**
 * Controlador para el programa de manejo de la trazabilidad del negocio
 * @author ivargas - geotech
 */
public class GestionAsesoresAction extends Action {

    HttpSession session;
    Usuario usuario;
    String next = "";
    AsesoresService asesorServ;
    ArrayList<BeanGeneral> negocios = new ArrayList();

    @Override
    public void run() throws ServletException, InformationException {
        try {
            session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            
            asesorServ = new AsesoresService(usuario.getBd());
            String opcion = request.getParameter("opcion");
            boolean redirect = true;
            String strRespuesta = "";

            if (opcion.equals("BUSCAR_USUA")) {
                strRespuesta = buscarUsuario();
                redirect = false;
            } else if (opcion.equals("GRABAR")) {
                this.grabar();
                redirect = true;
            }
            if (opcion.equals("NEGOCIOS_ASESOR")) {
                strRespuesta = negociosAsesor();
                redirect = false;
            }
            if (opcion.equals("ACUALIZAR_ASESOR_NEGOCIOS")) {
                strRespuesta = actualizarAsesorNegocios();
                redirect = false;
            }

            if (redirect) {
                this.dispatchRequest(next);
            } else {
                response.setContentType("text/plain");
                response.setHeader("Cache-Control", "no-store");
                response.setDateHeader("Expires", 0);
                response.getWriter().print(strRespuesta);
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new ServletException("Error en PerfilesAsociarAction: " + e.getMessage());
        }

    }

    /**
     * Busca un usuario a partir de su id
     * @return identificacion y nombre del usuario
     * @throws SQLException 
     */
    public String buscarUsuario() throws Exception {

        String codigousu = request.getParameter("usuariocod") != null ? request.getParameter("usuariocod") : "";
        codigousu = codigousu.toUpperCase();
        String cadresp = ";_;";
        try {
            cadresp = model.gestionConveniosSvc.datosCodigoUsuario(codigousu);
        } catch (Exception e) {
            cadresp = ";_;";
            System.out.println("error: " + e.toString());
            e.printStackTrace();
        }

        return cadresp;
    }

    /**
     * Graba la trazabilidad de una de las etapas del proceso
     * @return pagina de redireccion o mensaje de error
     * @throws SQLException
     */
    public String grabar() throws Exception {
        next = "/jsp/fenalco/convenio/Asesores.jsp?agregar=S";
        ArrayList<Asesores> listamod = new ArrayList();
        ArrayList<Asesores> listains = new ArrayList();
        ArrayList<Asesores> listaant =asesorServ.buscarAsesores();

        
        try {
            int filastabla = request.getParameter("filastabla") != null ? Integer.parseInt(request.getParameter("filastabla")) : 0;
            if (filastabla > 0) {
                for (int i = 0; i < filastabla; i++) {
                    Asesores asesor= new Asesores();
                    asesor.setIdusuario(request.getParameter("id" + i) != null ? request.getParameter("id" + i).toUpperCase() : "");
                    asesor.setRegStatus(request.getParameter("activo" + i) != null ? "" : "A");
                    asesor.setCreationUser(usuario.getLogin());
                    asesor.setDstrct(usuario.getDstrct());
                    if(i<listaant.size()){
                        if(listaant.get(i).getIdusuario().equals(asesor.getIdusuario())&&!listaant.get(i).getRegStatus().equals(asesor.getRegStatus())){
                            listamod.add(asesor);
                        }
                    }else{
                        if(!asesor.getIdusuario().equals("")){
                            listains.add(asesor);
                        }
                    }
                }
                asesorServ.grabarAsesores(listamod, listains);
              next+="&msg=Asesores guardados";
            } else {
                next+="&msg=No hay filas para guardar";
            }
        } catch (Exception e) {
            System.out.println("Error: " + e.toString());
            next+="&msg="+ e.toString();
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Busca las cuotas que estan pendiente por causar el interes
     * @throws Exception
     */
    private String negociosAsesor() throws Exception {
        String cadenaWrite = "";
        
        String codigousu = request.getParameter("asesor") != null ? request.getParameter("asesor") : "";
        codigousu = codigousu.toUpperCase();
        try {
            negocios = asesorServ.negociosAsesor(codigousu);
        } catch (Exception e) {
            System.out.println("error(action): " + e.toString());
            e.printStackTrace();
        }
        cadenaWrite = "<table id='tabladocs' style='border-collapse:collapse; width:100%;' border='1'>";
        cadenaWrite += "    <thead>";
        if (!negocios.isEmpty()) {
            cadenaWrite += "        <tr class='filaazul'>";
            cadenaWrite += "            <th>Cedula Cliente</th>";
            cadenaWrite += "            <th>Cliente</th>";
            cadenaWrite += "            <th>Fecha Desembolso</th>";
            cadenaWrite += "            <th>Codigo Negocio</th>";
            cadenaWrite += "            <th>Valor Negocio</th>";
            cadenaWrite += "            <th>Saldo Facturas</th>";
            cadenaWrite += "        </tr>";
            cadenaWrite += "    </thead>";
            for (int i = 0; i < negocios.size(); i++) {
                cadenaWrite += "    <tr class='filableach'>";
                cadenaWrite += "        <td align='center'>" + negocios.get(i).getValor_01() + "</td>";
                cadenaWrite += "        <td align='center'>" + negocios.get(i).getValor_02() + "</td>";
                cadenaWrite += "        <td align='center'>" + negocios.get(i).getValor_03() + "</td>";
                cadenaWrite += "        <td align='center'>" + negocios.get(i).getValor_04() + "</td>";
                cadenaWrite += "        <td align='center'>" + Util.FormatoMiles(Double.parseDouble(negocios.get(i).getValor_05())) + "</td>";
                cadenaWrite += "        <td align='center'>" + negocios.get(i).getValor_06() + "</td>";
                cadenaWrite += "     </tr>";
            }
        } else {
            cadenaWrite += "        <tr class='filaazul'>";
            cadenaWrite += "            <th colspan='6'>El asesor no tiene negocios con cartera activa</th>";
            cadenaWrite += "        </tr>";
            cadenaWrite += "    </thead>";
        }

        cadenaWrite += "</table>";

        return cadenaWrite;
    }

    private String actualizarAsesorNegocios() throws Exception {

        String asesor = request.getParameter("asesor_new") != null ? request.getParameter("asesor_new") : "";
        String cadenaWrite = "";
        if (!negocios.isEmpty()) {
            try {
                TransaccionService tService = new TransaccionService(usuario.getBd());
                tService.crearStatement();
                for (int i = 0; i < negocios.size(); i++) {
                    tService.getSt().addBatch(asesorServ.updateAsesorFormulario(negocios.get(i).getValor_07(), asesor));
                }
                tService.execute();
                cadenaWrite = "Se han actualizado " + negocios.size() + " negocio(s)";
            } catch (Exception e) {
                System.out.println("error en action: " + e.toString());
                cadenaWrite = e.toString();
                e.printStackTrace();
            }

        } else {
            cadenaWrite = "El  Asesor consultado no tiene negocios con cartera activa";
        }
        negocios.clear();
        return cadenaWrite;
    }
}
