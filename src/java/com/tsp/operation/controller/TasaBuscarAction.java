/*
 * TasaBuscarAction.java
 *
 * Created on 30 de junio de 2005, 12:54 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
/**
 *
 * @author  DIOGENES
 */
public class TasaBuscarAction extends Action {
    
    /** Creates a new instance of TasaBuscarAction */
    public TasaBuscarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina")+"?sw=1";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        String moneda1 = request.getParameter("c_combom1");
        String moneda2 = request.getParameter("c_combom2");
        String fec = request.getParameter("c_fecha");
        try{ 
            
            model.tasaService.buscarTasa(moneda1, moneda2, fec,usuario.getCia());
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
         
         // Redireccionar a la p�gina indicada.
        this.dispatchRequest(Util.LLamarVentana(next,"Modificar Tasa"));
    }
    
}
