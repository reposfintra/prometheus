/*
 * BancoCambiarAction.java
 *
 * Created on 14 de junio de 2005, 09:12 AM
 */

package com.tsp.operation.controller;


import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  kreales
 */
public class BancoCambiarAction extends Action{
    
    /** Creates a new instance of BancoCambiarAction */
    public BancoCambiarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        String next="/controller?estado=Menu&accion=ImpresionCheques&opcion=no&total=0";
        
        String planilla    = request.getParameter("planilla");
        String bancoAnt    = request.getParameter("bancoAnt");
        String bancoNew    = request.getParameter("banco");
        String moneda      = request.getParameter("moneda");
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        try{
            String banvec[] = bancoNew.split("/");
            String banco=banvec[0];
            String cuenta=banvec[1];
            String monedaCia = "PES";
            
            Banco bancoOj = model.servicioBanco.obtenerBanco(usuario.getDstrct(),banco ,cuenta);
            String moneda2 = bancoOj.getMoneda();
            double vlrtasa = 1;
            
            java.util.Date fecha_d = new java.util.Date();
            java.text.SimpleDateFormat s =  new java.text.SimpleDateFormat("yyyy-MM-dd");
            String fecha = s.format(fecha_d);
            
            try{
                model.tasaService.buscarValorTasa(monedaCia,moneda,moneda2,fecha);
            }catch(Exception et){
                throw new SQLException(et.getMessage());
            }
            
            Tasa tasa = model.tasaService.obtenerTasa();
            if(tasa==null && !moneda.equals(moneda2)){
                next ="/cheques/cambiarBanco.jsp?mensaje=No se pudo cambiar el banco, no se encontro tasa de cambio.";
            }
            else{
                model.ImprimirChequeSvc.cambiarBanco(planilla, bancoAnt, bancoNew, usuario.getDstrct(),moneda);
            }
            
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
        
    }
    
}
