/**
* Autor : Ing. Roberto Rocha P..
* Date  : 31 Enero 2008 de 2007
* Copyrigth Notice : Fintravalores S.A. S.A
* Version 1.0
-->
<%--
-@(#)
--Descripcion : Action  que maneja las observaciones por Factura CXC
**/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.threads.*;
import com.tsp.util.Util;
import com.tsp.operation.model.beans.Usuario;
import javax.servlet.http.*;
import java.sql.SQLException;
import com.tsp.operation.model.*;


public class FacturaObservacionAction  extends Action{
    
   //protected HttpServletRequest request;
    public FacturaObservacionAction () {
    }
    
    /*modificado por javier en 20080610*/
    public void run() throws ServletException, InformationException
    { 

       String men="";//Mensaje
       String obs="";//Observacion

       HttpSession session  = request.getSession();
       String next="";
       Usuario usuario = (Usuario)session.getAttribute("Usuario");//Nombre de usuario actual
       int op=Integer.parseInt(request.getParameter("op"));//opcion de mensaje
       String numfac=request.getParameter("factu");//No factura
       
       String codcli = request.getParameter("cod_cliente");
       String replicar = request.getParameter("replicar");//Opcion de replicar mensaje
       
       if(replicar==null){
            replicar="0";
       }
       
       BeanGeneral bg=new BeanGeneral();
       switch(op)
       {
            case 1:  
                next="/jsp/cxcobrar/reportes/RespObsPagador.jsp?num="+numfac+"&cod_cliente="+codcli;
            break;
            case 2:

                 obs=request.getParameter("pagador");//id=pagador es la observacion hecha en el formulario de la pagina RespObsPagador.jsp
                 bg.setValor_01(numfac);//No de la Factura 
                 bg.setValor_02(obs);//La observacion a insertar
                 bg.setValor_03(usuario.getLogin());//Login usuario actual del sistema
                 bg.setValor_04(usuario.getLogin());//Login usuario actual del sistema
                 
                 bg.setValor_05(codcli);//Codigo del cliente seleccionado
                             
                 if(replicar.equals("0")){
                    //Este caso es cuando se ingresa una observacion para una unica factura 
                    //System.out.println("Replicar NO");
                    try{   
                        men = model.FactObsvc.insert(bg);
                    }
                    catch (Exception e){
                        e.printStackTrace();
                        men="Error Durante la insercion "+e.getMessage();
                    }
                    next="/jsp/cxcobrar/reportes/RespObsPagador.jsp?num="+numfac+"&cod_cliente="+codcli+"&msg="+men;
                 }else{
                    //Este caso ocurre cuando desde respObsPagador.jsp se escoje almancenar la observacion como 
                    //general para todas las facturas de un cliente 
                    // System.out.println("Replicar SI");
                    try{
                        men = model.FactObsvc.insertAll(bg);
                    }catch(Exception e){
                        e.printStackTrace();
                        men=e.getMessage();
                    }
                    next="/jsp/cxcobrar/reportes/RespObsPagador.jsp?num="+numfac+"&cod_cliente="+codcli+"&msg="+men;
                 }
                
            break;
            
       }
       
       this.dispatchRequest(next);
    }
   
    
 
    
}
