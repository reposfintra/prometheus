/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.GarantiasComunitariasDAO;
import com.tsp.operation.model.DAOS.impl.GarantiasComunitariasImpl;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.POIWrite;
import com.tsp.operation.model.beans.ParametrosBeans;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.threads.HIndemnizarFianzaMicro;
import com.tsp.util.ExcelApiUtil;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.tsp.util.Util;
import java.io.File;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;
/**
 *
 * @author mcastillo
 */
public class GarantiasComunitariasAction extends Action{
    
    private final int LISTAR_TIPO_DOCUMENTOS = 0;
    private final int CARGAR_CONFIG_DOCS_GARANTIAS_COMUNITARIAS = 1;
    private final int GUARDAR_CONFIG_DOCS_GARANTIAS_COMUNITARIAS = 2;
    private final int CARGAR_GRILLA_EQUIVALENCIAS = 3;  
    private final int CARGAR_CONFIGURACION_FACTOR_POR_MILLON = 4;
    private final int CARGAR_COMBO_UNIDAD_NEGOCIO = 5;  
    private final int GUARDAR_CONFIG_FACTOR_POR_MILLON = 6;
    private final int ACTUALIZAR_CONFIG_FACTOR_POR_MILLON = 7;
    private final int ACTIVAR_INACTIVAR_CONFIG_FACTOR_POR_MILLON = 8;
    private final int BUSCAR_NEGOCIOS_PROCESAR_FIANZA = 9;
    private final int PROCESAR_NEGOCIOS_PENDIENTES_FIANZA = 10;
    private final int BUSCAR_FACTURAS_INDEMNIZACION = 11;
    private final int BUSCAR_FACTURAS_DESISTIMIENTO = 12;
    private final int INDEMNIZAR_FACTURAS = 13;
    private final int DESISTIR_FACTURAS = 14;
    private final int EXPORTAR_FACTURAS_INDEMNIZADAS=15;
    private final int EXPORTAR_EXCEL = 16;
    private final int GENERAR_CUENTA_COBRO_GARANTIAS = 17;
    private final int GENERAR_NOTIFICACION_CLIENTE = 18;
    
    private final int CARGAR_RESUMEN_NEGOCIOS_INDEMNIZADOS = 19;
    private final int CARGAR_REPORTE_CARTERA = 20;
    private final int CARGAR_REPORTE_DESEMBOLSOS = 21;
    private final int CARGAR_REPORTE_RECLAMACIONES = 22;
    private final int CARGAR_REPORTE_PREPAGOS = 23;
    private final int CARGAR_REPORTE_RECUPERACIONES = 24;
    private final int EXPORTAR_EXCEL_CARTERA = 25;
    private final int EXPORTAR_EXCEL_DESEMBOLSOS = 26;
    private final int EXPORTAR_EXCEL_RECLAMACIONES = 27;
    private final int EXPORTAR_EXCEL_PREPAGOS = 28;
    private final int EXPORTAR_EXCEL_RECUPERACIONES = 29;
    private final int CARGAR_COMBO_EMPRESAS_FIANZA = 30;  
    private final int CARGAR_COMBO_VENCIMIENTOS = 31; 
    private final int VER_DETALLE_FACTURAS_NEGOCIO = 32;
    private final int CARGAR_NEGOCIOS_PENDIENTES_VISTO_BUENO = 33;
    private final int GENERAR_VISTO_BUENO_FIANZA = 34;
    private final int CARGAR_COMBO_AGENCIA_UN = 35;
    private final int INDEMNIZAR_NEGOCIOS = 36;    
    private final int EXPORTAREXCELINDEMNIZADO = 37;
    private final int VER_NEGOCIOS_INDEMNIZADOS=38;    
    private final int EXPORTAR_PDF=39;
    private final int CARGAR_PROVEEDOR_FIANZA = 40;
    private final int BUSCAR_PROVEEDOR_FIANZA = 41;
    private final int GUARDAR_PROVEEDOR_FIANZA = 42;
    private final int CAMBIAR_ESTADO_PROVEEDOR_FIANZA = 43;
    private final int EXPORTAR_CXP_CONSOLIDADA = 44;
    private final int GENERAR_VISTO_BUENO_POLIZA = 45;
    private final int CARGAR_COMBO_PRODUCTO = 46;
    private final int CARGAR_COMBO_COBERTURA = 47;
    
    GarantiasComunitariasDAO dao;
    Usuario usuario = null;
    String reponseJson = "{}";
    String typeResponse = "application/json;";
    POIWrite xls;
    HSSFCellStyle header  , titulo1, titulo2, titulo3 , titulo4, titulo5, letra, numero, dinero, numeroCentrado, porcentaje, letraCentrada, numeroNegrita, ftofecha;
    String   rutaInformes;
    private SimpleDateFormat fmt;
     private int fila = 0;
     String   nombre;

    @Override
    public void run() throws ServletException, InformationException {
        try{
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            int opcion = Integer.parseInt(request.getParameter("opcion"));         
            dao = new GarantiasComunitariasImpl(usuario.getBd());

            switch (opcion) {
                case LISTAR_TIPO_DOCUMENTOS:
                    listarTipoDocumentos();
                    break;
                case CARGAR_CONFIG_DOCS_GARANTIAS_COMUNITARIAS:
                    cargarConfigDocsGarantiasComunitarias();
                    break;
                case GUARDAR_CONFIG_DOCS_GARANTIAS_COMUNITARIAS:
                    guardarConfigDocsGarantiasComunitarias();
                    break;
                case CARGAR_GRILLA_EQUIVALENCIAS:
                    this.cargarEquivalencias();
                    break;
                case CARGAR_CONFIGURACION_FACTOR_POR_MILLON:
                    cargarConfigFactor();
                    break;     
                case CARGAR_COMBO_UNIDAD_NEGOCIO:
                    cargarCboUnidad();
                    break;         
                case GUARDAR_CONFIG_FACTOR_POR_MILLON:
                    guardarConfigFactor();
                    break;
                case ACTUALIZAR_CONFIG_FACTOR_POR_MILLON:
                    actualizarConfigFactor();
                    break;
                case ACTIVAR_INACTIVAR_CONFIG_FACTOR_POR_MILLON:
                    activaInactivaConfigFactor();
                    break;
                case BUSCAR_NEGOCIOS_PROCESAR_FIANZA:
                    buscarNegociosProcesarFianza();
                    break;
                case PROCESAR_NEGOCIOS_PENDIENTES_FIANZA:
                    procesarNegociosPendientesFianza();
                    break;
                case BUSCAR_FACTURAS_INDEMNIZACION:
                    buscarFacturasIndemnizacion();
                    break;
                case BUSCAR_FACTURAS_DESISTIMIENTO:
                    buscarFacturasDesistimiento();
                    break;   
                case INDEMNIZAR_FACTURAS:
                    indemnizarFacturas();
                    break;
                case DESISTIR_FACTURAS:
                    desistirFacturas();
                    break;               
                case EXPORTAR_FACTURAS_INDEMNIZADAS:
                    verFacturasIndemnizadas();
                    break;
                case EXPORTAR_EXCEL:
                    exportarExcel();
                    break;
                    
                case GENERAR_CUENTA_COBRO_GARANTIAS:
                    //generarCxCGarantias();
                    break;
                case GENERAR_NOTIFICACION_CLIENTE:
                    generarNotificacionCliente();
                    break;
                    
                case CARGAR_RESUMEN_NEGOCIOS_INDEMNIZADOS:
                    this.cargarResumenIndemnizados();
                    break;                   
                case CARGAR_REPORTE_CARTERA:
                    this.cargarReporteCartera();
                    break;               
                case CARGAR_REPORTE_DESEMBOLSOS:
                    this.cargarReporteDesembolsos();
                    break;
                case CARGAR_REPORTE_RECLAMACIONES:
                    this.cargarReporteReclamaciones();
                    break;
                case CARGAR_REPORTE_PREPAGOS:
                    this.cargarReportePrepagos();
                    break;
                case CARGAR_REPORTE_RECUPERACIONES:
                    this.cargarReporteRecuperaciones();
                    break;                    
                case EXPORTAR_EXCEL_CARTERA:
                    this.exportarExcelCartera();
                    break;
                case EXPORTAR_EXCEL_DESEMBOLSOS:
                    this.exportarExcelDesembolsos();
                    break;
                case EXPORTAR_EXCEL_RECLAMACIONES:
                    this.exportarExceReclamaciones();
                    break;
                case EXPORTAR_EXCEL_PREPAGOS:
                    this.exportarExcelPrepagos();
                    break;
                case EXPORTAR_EXCEL_RECUPERACIONES:
                    this.exportarExcelRecuperaciones();
                    break;
                case CARGAR_COMBO_EMPRESAS_FIANZA:
                    cargarCboEmpresasFianza();
                    break;       
                case CARGAR_COMBO_VENCIMIENTOS:
                    cargarCboVencimientos();
                    break;
                case VER_DETALLE_FACTURAS_NEGOCIO:
                    listarDetalleFacturasNegocio();
                    break;
                case CARGAR_NEGOCIOS_PENDIENTES_VISTO_BUENO:
                    this.cargarNegociosPendientesVistoBueno();
                    break;   
                case GENERAR_VISTO_BUENO_FIANZA:
                    this.generarVistoBueno();
                    break;
                case CARGAR_COMBO_AGENCIA_UN:
                    this.cargarCboAgenciaUnidad();
                    break;
                case INDEMNIZAR_NEGOCIOS:
                    this.indemnizarNegocios();
                    break;
                case EXPORTAREXCELINDEMNIZADO:
                    this.exportarExcelIndemnizado();
                    break;
                case VER_NEGOCIOS_INDEMNIZADOS:
                    this.verNegociosIndemnizados();
                    break;
                case EXPORTAR_PDF:
                    exportaPdf(usuario.getLogin());
                    break;
                case CARGAR_PROVEEDOR_FIANZA:
                    this.cargarProveedorFianza();
                    break;
                case BUSCAR_PROVEEDOR_FIANZA:
                    this.buscarProveedorFianza();
                    break;
                case GUARDAR_PROVEEDOR_FIANZA:
                    this.guardarProveedorFianza();
                    break;
                case CAMBIAR_ESTADO_PROVEEDOR_FIANZA:
                    this.cambiarEstadoProveedorFianza();
                    break;
                case EXPORTAR_CXP_CONSOLIDADA:
                    this.ExportarCxpConsolidada();
                    break;
                case GENERAR_VISTO_BUENO_POLIZA:
                    this.generarVistoBuenoPoliza();
                    break;
                case CARGAR_COMBO_PRODUCTO:
                    this.cargarCboProducto();
                    break;
                case CARGAR_COMBO_COBERTURA:
                    this.cargarCboCobertura();
                    break;
                default:
                    this.printlnResponseAjax("{\"error\":\"Peticion invalida 404 recurso no encontrado\"}", "application/json;");
                    break;
                   
            }
            
        } catch (Exception ex) {
            reponseJson = "{\"error\":\"Algo salio mal al procesar su solicitud\",\"exception\":\"" + ex.getMessage() + "\"}";
            ex.printStackTrace();
        } finally {
            try {
                this.printlnResponseAjax(reponseJson, typeResponse);
            } catch (Exception ex1) {
                Logger.getLogger(GarantiasComunitariasAction.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
    }
    
    private void listarTipoDocumentos() {
        try {
            String mostrarTodos = request.getParameter("mostrarTodos") != null ? request.getParameter("mostrarTodos") : "N";
            this.reponseJson = dao.listarTipoDocumentos(mostrarTodos);
        } catch (Exception ex) {
            Logger.getLogger(GarantiasComunitariasAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarConfigDocsGarantiasComunitarias() {
        try {
            this.reponseJson = dao.cargarConfigDocs();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void guardarConfigDocsGarantiasComunitarias() {
        try {
            String tipo_doc = request.getParameter("tipo_doc") != null ? request.getParameter("tipo_doc") : "";
            String content = request.getParameter("content") != null ? Util.setCodificacionCadena(request.getParameter("content"), "ISO-8859-1") : "";
            this.reponseJson = dao.guardarConfigDocs(tipo_doc, content, usuario);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(GarantiasComunitariasAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }

    private void cargarEquivalencias() {
        try {
           this.reponseJson = dao.cargarEquivalencias();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void cargarConfigFactor() {
        try {
            this.reponseJson = dao.cargarConfigFactor();            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarCboUnidad() {
        try {         
            this.reponseJson =  dao.cargarComboUnidad();           
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void cargarCboEmpresasFianza() {
        try {
            this.reponseJson = dao.cargarCboEmpresasFianza();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
     private void cargarCboVencimientos() {
        try {         
            this.reponseJson =  dao.cargarCboVencimientos();           
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
        
    private void guardarConfigFactor() {
        try {
            String codigo_convenio = (request.getParameter("codigo_convenio") != null) ? request.getParameter("codigo_convenio") : "";
            String id_unidad = (request.getParameter("id_unidad") != null) ? request.getParameter("id_unidad") : "";
            String nit_empresa_fianza = (request.getParameter("nit_empresa_fianza") != null) ? request.getParameter("nit_empresa_fianza") : "";
            String plazo_inicial = (request.getParameter("plazo_inicial") != null) ? request.getParameter("plazo_inicial") : "0";
            String plazo_final = (request.getParameter("plazo_final") != null) ? request.getParameter("plazo_final") : "0";
            String porc_comision = (request.getParameter("porc_comision") != null) ? request.getParameter("porc_comision") : "0";
            String valor_comision = (request.getParameter("valor_comision") != null) ? request.getParameter("valor_comision") : "0";
            String iva = (request.getParameter("iva") != null) ? request.getParameter("iva") : "0";
            String financiado = (request.getParameter("financiado") != null) ? request.getParameter("financiado") : "";
            String producto = (request.getParameter("producto") != null) ? request.getParameter("producto") : "0";
            String cobertura = (request.getParameter("cobertura") != null) ? request.getParameter("cobertura") : "0";
            
            if (!dao.existePlazoFactor(id_unidad, plazo_inicial, plazo_final,nit_empresa_fianza,producto,cobertura)) {
                this.reponseJson =  dao.guardarConfigFactor(codigo_convenio, id_unidad, nit_empresa_fianza, plazo_inicial, plazo_final, porc_comision, valor_comision, iva, usuario, financiado,producto,cobertura);
            } else {
                this.reponseJson = "{\"error\":\" No se cre� la nueva configuraci�n. El plazo ingresado no es v�lido � ya existe en el sistema\"}";
            }              
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void actualizarConfigFactor() {
        try {
            String id = request.getParameter("id");
            String codigo_convenio = (request.getParameter("codigo_convenio") != null) ? request.getParameter("codigo_convenio") : "";
            String id_unidad = (request.getParameter("id_unidad") != null) ? request.getParameter("id_unidad") : "";
            String nit_empresa_fianza = (request.getParameter("nit_empresa_fianza") != null) ? request.getParameter("nit_empresa_fianza") : "";
            String plazo_inicial = (request.getParameter("plazo_inicial") != null) ? request.getParameter("plazo_inicial") : "0";
            String plazo_final = (request.getParameter("plazo_final") != null) ? request.getParameter("plazo_final") : "0";
            String porc_comision = (request.getParameter("porc_comision") != null) ? request.getParameter("porc_comision") : "0";
            String valor_comision = (request.getParameter("valor_comision") != null) ? request.getParameter("valor_comision") : "0";
            String iva = (request.getParameter("iva") != null) ? request.getParameter("iva") : "0";
            String financiado = (request.getParameter("financiado") != null) ? request.getParameter("financiado") : "";
            String porcentaje_aval = (request.getParameter("porcentaje_aval") != null) ? request.getParameter("porcentaje_aval") : "0";
            String porcentaje_aval_finan = (request.getParameter("porcentaje_aval_finan") != null) ? request.getParameter("porcentaje_aval_finan") : "0";
            String producto = (request.getParameter("producto") != null) ? request.getParameter("producto") : "0";
            String cobertura = (request.getParameter("cobertura") != null) ? request.getParameter("cobertura") : "0";
            
            if (!dao.existePlazoFactor(id, id_unidad, plazo_inicial, plazo_final,nit_empresa_fianza, producto, cobertura)) {
                this.reponseJson = dao.actualizarConfigFactor(id, codigo_convenio, id_unidad, nit_empresa_fianza, plazo_inicial, plazo_final, porc_comision, valor_comision, iva, usuario, financiado,porcentaje_aval,porcentaje_aval_finan,producto,cobertura); 
            } else {
                this.reponseJson = "{\"error\":\" No es posible actualizar la configuraci�n.  El plazo ingresado no es v�lido � ya existe en el sistema\"}";
            }     
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void activaInactivaConfigFactor() {
        try {
            String idConfigFactor = request.getParameter("id");
            this.reponseJson = dao.activaInactivaConfigFactor(idConfigFactor, usuario);          
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    

    private void buscarNegociosProcesarFianza() {
        try {
            String id_unidad = (request.getParameter("id_unidad_negocio") != null) ? request.getParameter("id_unidad_negocio") : "";
            String agencia = (request.getParameter("agencia") != null) ? request.getParameter("agencia") : "";
            String nit_empresa = (request.getParameter("empresa_fianza") != null) ? request.getParameter("empresa_fianza") : "";
            String periodo = (request.getParameter("periodo_corte") != null) ? request.getParameter("periodo_corte") : "";
            this.reponseJson = dao.buscarNegociosProcesarFianza(id_unidad, nit_empresa, periodo, agencia);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void procesarNegociosPendientesFianza() {
        try {
            String id_unidad = (request.getParameter("id_unidad_negocio") != null) ? request.getParameter("id_unidad_negocio") : "";
            String agencia = (request.getParameter("agencia") != null) ? request.getParameter("agencia") : "";
            String nit_empresa = request.getParameter("empresa_fianza") != null ? request.getParameter("empresa_fianza") : "";
            String periodo = (request.getParameter("periodo_corte") != null) ? request.getParameter("periodo_corte") : "";                
            String valor_cxp_fianza = (request.getParameter("valor_cxp_fianza") != null) ? request.getParameter("valor_cxp_fianza") : "";          
            String listadoNegocios = request.getParameter("listadoNegocios");           
            JsonParser jsonParser = new JsonParser();
            JsonObject jneg = (JsonObject) jsonParser.parse(listadoNegocios);
            JsonArray jsonArrNeg = jneg.getAsJsonArray("negocios"); 
            List<String> x = new ArrayList<String>();
            for (int i = 0; i < jsonArrNeg.size(); i++) {
                String id = jsonArrNeg.get(i).getAsJsonObject().get("id").getAsJsonPrimitive().getAsString();
                x.add(id);               
            }
            String[] Array = x.toArray(new String[0]);
            String[] split = dao.generarCxPFianza(id_unidad,agencia, nit_empresa, periodo, Array, valor_cxp_fianza, usuario.getLogin()).split(";");
            this.reponseJson =  "{\"respuesta\":\"" + split[0] + "\",\"numCxP\":\""+split[1]+"\"}";             
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    private void buscarFacturasIndemnizacion() {

        String mora = (request.getParameter("mora") != null && !request.getParameter("mora").equals("")) ? request.getParameter("mora") : "90";
        String lineaNegocio = request.getParameter("linea_negocio") != null ? request.getParameter("linea_negocio") : "";
        String foto = request.getParameter("foto") != null ? request.getParameter("foto") : "";
        String cartera_en = request.getParameter("cartera_en") != null ? request.getParameter("cartera_en") : "";
        String empresa_fianza = request.getParameter("empresa_fianza") != null ? request.getParameter("empresa_fianza") : "";
        String cedula_cliente = request.getParameter("cedula_cliente") != null ? request.getParameter("cedula_cliente") : "";
        String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
        String monto_inicial = (request.getParameter("monto_inicial") != null && !request.getParameter("monto_inicial").equals("")) ? request.getParameter("monto_inicial") : "";
        String monto_final = (request.getParameter("monto_final") != null && !request.getParameter("monto_final").equals("")) ? request.getParameter("monto_final") : "";
        String aplica_aceleracion = request.getParameter("acelerar_pagare") != null ? request.getParameter("acelerar_pagare") : "false";
        String aplica_gac = request.getParameter("aplica_gac") != null ? request.getParameter("aplica_gac") : "false";
        String aplica_ixm = request.getParameter("aplica_ixm") != null ? request.getParameter("aplica_ixm") : "false";
        String query="";
        
        ParametrosBeans beans = new ParametrosBeans();       
        beans.setStringParameter1(foto);
        beans.setStringParameter2(lineaNegocio);
        beans.setStringParameter3(empresa_fianza);
        beans.setStringParameter4(mora);
        beans.setStringParameter5(cedula_cliente);
        beans.setStringParameter6(negocio);
        beans.setStringParameter7(monto_inicial);
        beans.setStringParameter8(monto_final); 
        beans.setStringParameter9(aplica_aceleracion);
        beans.setStringParameter10(aplica_gac);      
        beans.setStringParameter11(aplica_ixm);      
        
        if (lineaNegocio.equals("1")) {
             query = "BUSCAR_FACTURAS_INDEMNIZACION_MICRO";
             query = beans.getStringParameter9().equals("true") ? "BUSCAR_FACTURAS_INDEMNIZACION_MICRO_AP" : query;
         } else {
             query = "BUSCAR_FACTURAS_INDEMNIZACION_EDU_CONS";
             query = beans.getStringParameter9().equals("true") ? "BUSCAR_FACTURAS_INDEMNIZACION_EDU_CONS_AP" : query;
         }
        
        this.reponseJson = dao.getSelectDataBaseJson(query, beans, usuario);

    }

    private void buscarFacturasDesistimiento() {

        String lineaNegocio = request.getParameter("linea_negocio") != null ? request.getParameter("linea_negocio") : "";
        String cartera_en = request.getParameter("cartera_en") != null ? request.getParameter("cartera_en") : "";

        ParametrosBeans beans = new ParametrosBeans();
        beans.setStringParameter1(lineaNegocio);
        beans.setStringParameter2(cartera_en);

        this.reponseJson = dao.getSelectDataBaseJson("FACTURAS_POR_DESISTIR", beans, usuario);

    }
    
    private synchronized void indemnizarFacturas() {
        try {
            String json = request.getParameter("listJson") != null ? request.getParameter("listJson") : "";
            JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
            String empresa_fianza = request.getParameter("empresa_fianza") != null ? request.getParameter("empresa_fianza") : "";
            String mora = (request.getParameter("mora") != null && !request.getParameter("mora").equals("")) ? request.getParameter("mora") : "90";
            String periodo_corte = request.getParameter("perido_corte") != null ? request.getParameter("perido_corte") : "";
            String aplica_aceleracion = request.getParameter("acelerar_pagare") != null ? request.getParameter("acelerar_pagare") : "false";
            String aplica_gac = request.getParameter("aplica_gac") != null ? request.getParameter("aplica_gac") : "false";

            Thread hiloFianza = new Thread(new HIndemnizarFianzaMicro(empresa_fianza, mora, periodo_corte, Boolean.parseBoolean(aplica_aceleracion), Boolean.parseBoolean(aplica_gac), asJsonArray, usuario), "hilo");
            hiloFianza.start();
            this.reponseJson = "{\"respuesta\":\"OK\"}";         
        
        } catch (Exception ex) {
            this.reponseJson = "{\"error\":\"Lo sentimos no se pudo guardar la informacion.\"}";
            ex.printStackTrace();
            Logger.getLogger(GarantiasComunitariasAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private synchronized void desistirFacturas() {
        try {
            String json = request.getParameter("listJson") != null ? request.getParameter("listJson") : "";
            JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);

            TransaccionService tservice = new TransaccionService(usuario.getBd());
            tservice.crearStatement();
            for (int i = 0; i < asJsonArray.size(); i++) {
                JsonObject objects = (JsonObject) asJsonArray.get(i);
                tservice.getSt().addBatch(dao.getInsertFacturaXdesistir("SQL_GUARDAR_FACTURAS_DESISTIDAS", objects, usuario));
            }
            tservice.execute();
            tservice.closeAll();

            //ejecutamos el proceso para crear el comprobante diario desistimiento.
            this.reponseJson = "{\"respuesta\":\"" + dao.crearComprobanteDiarioDesistimiento(usuario)+ "\"}";           

        } catch (SQLException ex) {
            this.reponseJson = "{\"error\":\"Lo sentimos no se pudo guardar la informacion.\"}";
            ex.printStackTrace();
            Logger.getLogger(ContabilidadGeneralAction.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception ex) {
            this.reponseJson = "{\"error\":\"Lo sentimos no se pudo guardar la informacion.\"}";
            ex.printStackTrace();
            Logger.getLogger(GarantiasComunitariasAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
     private void exportarExcel() throws Exception{
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        String[] cabecera = null;
        short[] dimensiones=null;
        String nombreArchivo="Reporte_factura", titulo="REPORTE FACTURAS";
        
        cabecera = new String[]{"Linea Negocio", "Pagar�", "Identificacion", "Nombre Cliente", "Negocio",
            "Altura Mora", "Dias mora", "Vlr factura", "Saldo Capital", "Saldo Seguro", "Int. Corriente", "Cat", "Cuota Admin", "Int. de Mora", "GAC", "Saldo Total"};

        dimensiones = new short[]{
            5000, 3000, 5000, 7000, 5000, 3000, 
            6000, 3000, 5000, 5000, 5000, 5000, 5000, 3000, 5000, 5000, 5000};
      
        ExcelApiUtil apiUtil =new ExcelApiUtil(usuario);
        reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        typeResponse = "text/plain";
    }
    
    private void verFacturasIndemnizadas() throws Exception{
        ParametrosBeans beans = new ParametrosBeans();
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(dao.getSelectDataBaseJson("VER_FACTURAS_INDENIZADAS", beans, usuario));
        
        String[] cabecera = null;
        short[] dimensiones=null;
        String nombreArchivo="Reporte_factura_indemnizadas", titulo="REPORTE FACTURAS";
        
        cabecera = new String[]{"Foto o Periodo", "Cod cliente", "Identificacion", "Nombre cliente", "Negocio", "Plazo en Cuotas",
                                "Vlr Indemnizado", "Fecha venc. 1era Couta", "Fecha Indemnizacion", "Linea negocio"};
        
        dimensiones = new short[]{
            4000, 3000, 5000, 9000, 5000, 5000, 5000, 6000, 6000, 7000
        };

      
        ExcelApiUtil apiUtil =new ExcelApiUtil(usuario);
        reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
        typeResponse = "text/plain";
    
    }
    
    private void generarCxCGarantias(JsonArray asJsonArray) {    

        try {
            String mora = (request.getParameter("mora") != null && !request.getParameter("mora").equals("")) ? request.getParameter("mora") : "90";            
            String fecha_corte = request.getParameter("fecha_corte") != null ? request.getParameter("fecha_corte") : "";
            
            /*String json = request.getParameter("listJson") != null ? request.getParameter("listJson") : "";
            JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);*/
            
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");//se consiguen los datos de db.properties
            String directorioArchivos = rb.getString("ruta") + "/images/multiservicios/" + usuario.getLogin() + "/";//se establece la ruta de la imagen            
            this.createDir(directorioArchivos);
           // this.reponseJson = dao.generarCxCGarantias(asJsonArray, directorioArchivos, fecha_corte, mora, usuario.getLogin());           
        } catch (Exception e) {
            this.reponseJson = "{\"error\":\"Error al generar pdf cuenta de cobro garantias.\"}";
            e.printStackTrace();
        }

    }
    
     private void generarNotificacionCliente() {

        try {      
            String nit_cliente = (request.getParameter("nit_cliente") != null) ? request.getParameter("nit_cliente") : "";
            String negocio = (request.getParameter("negocio") != null) ? request.getParameter("negocio") : "";
            String mora = (request.getParameter("mora") != null) ? request.getParameter("mora") : "";
            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");//se consiguen los datos de db.properties
            String directorioArchivos = rb.getString("ruta") + "/images/multiservicios/" + usuario.getLogin() + "/";//se establece la ruta de la imagen 
          
             this.createDir(directorioArchivos);
             this.reponseJson =  dao.generarNotificacionCliente(nit_cliente, negocio, mora, directorioArchivos, usuario);          
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    private void cargarResumenIndemnizados() {
        try {      
           String periodo = (request.getParameter("periodo_corte") != null) ? request.getParameter("periodo_corte") : "";
           this.reponseJson = dao.cargarResumenIndemnizados(periodo);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void cargarReporteCartera() {
        try {
           String id_unidad = (request.getParameter("id_unidad_negocio") != null) ? request.getParameter("id_unidad_negocio") : "";
           String nit_empresa_fianza = (request.getParameter("nit_empresa_fianza") != null) ? request.getParameter("nit_empresa_fianza") : "";
           String periodo = (request.getParameter("periodo_corte") != null) ? request.getParameter("periodo_corte") : "";
           this.reponseJson = dao.cargarReportesFianza("SQL_REPORTE_CARTERA_FIANZA",id_unidad, nit_empresa_fianza, periodo, usuario);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void cargarReporteDesembolsos() {
        try {
           String id_unidad = (request.getParameter("id_unidad_negocio") != null) ? request.getParameter("id_unidad_negocio") : "";
           String nit_empresa_fianza = (request.getParameter("nit_empresa_fianza") != null) ? request.getParameter("nit_empresa_fianza") : "";
           String periodo = (request.getParameter("periodo_corte") != null) ? request.getParameter("periodo_corte") : "";
           this.reponseJson = dao.cargarReportesFianza("SQL_REPORTE_OPERACIONES_DESEMBOLSADAS_FIANZA",id_unidad, nit_empresa_fianza, periodo, usuario);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void cargarReporteReclamaciones() {
        try {
            String id_unidad = (request.getParameter("id_unidad_negocio") != null) ? request.getParameter("id_unidad_negocio") : "";
            String nit_empresa_fianza = (request.getParameter("nit_empresa_fianza") != null) ? request.getParameter("nit_empresa_fianza") : "";
            String periodo = (request.getParameter("periodo_corte") != null) ? request.getParameter("periodo_corte") : "";
            this.reponseJson = dao.cargarReportesFianza("SQL_REPORTE_RECLAMACIONES_FIANZA",id_unidad, nit_empresa_fianza, periodo, usuario);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
     
    private void cargarReportePrepagos() {
        try {
           String id_unidad = (request.getParameter("id_unidad_negocio") != null) ? request.getParameter("id_unidad_negocio") : "";
           String nit_empresa_fianza = (request.getParameter("nit_empresa_fianza") != null) ? request.getParameter("nit_empresa_fianza") : "";
           String periodo = (request.getParameter("periodo_corte") != null) ? request.getParameter("periodo_corte") : "";
           this.reponseJson = dao.cargarReportesFianza("SQL_REPORTE_PREPAGOS_FIANZA",id_unidad, nit_empresa_fianza, periodo, usuario);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
      
       private void cargarReporteRecuperaciones() {
        try {
           this.reponseJson = "{}";
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
       
    private void exportarExcelCartera() throws Exception {
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Reporte_Cartera_", titulo = "REPORTE CARTERA";

        cabecera = new String[]{"C.C/DNI/NIT/RUC del Cliente","N�mero de pagar�","N�mero de Cr�dito/Ref", "Valor/Importe Saldo capital", "Valor/Importe Intereses corrientes", "Valor/Importe Intereses mora", "Fecha de corte"};

        dimensiones = new short[]{
            7000, 5000, 6000, 7000, 8000, 7000, 5000
        };

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        this.reponseJson =  apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);       
    }   
    
    private void exportarExcelDesembolsos() throws Exception {
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Reporte_Desembolso_", titulo = "REPORTE OPERACIONES DESEMBOLSADAS";

        cabecera = new String[]{"Nro. de Cr�dito/Ref", "Numero de Convenio", "Linea de cr�dito", "Nombre de la entidad", "Nit/RUC de la Entidad", "Sucursal", "Mes de desembolso", "Nombre Persona Natural/Jur�dica", "Apellidos Persona Natural/Jur�dica", "C.C/DNI/NIT/RUC del Cliente", 
                                "Sucursal de desembolso", "Tel�fono del cliente", "Direcci�n del cliente", "Ciudad/Provincia/Municipio", "Departamento/Regi�n", "Celular del cliente", "N�mero de Pagar�", "Valor/Importe del cr�dito", "Destino/Tipo Cr�dito", "Valor/Importe Cuota", "Plazo(meses)", "Fecha Desembolso",
                                "Fecha Vencimiento", "Correo electr�nico Cliente", "Periodo gracia", "Amortizaci�n capital", "Amortizaci�n periodo", "Amortizaci�n modalidad", "Tasa inter�s", "Porcentaje comisi�n", "Modalidad comisi�n", "Tipo comisi�n", "Valor/Importe Comisi�n",
                                "Valor/Importe Impuesto de Comisi�n", "Valor/Importe Total Comisi�n", "Especificaci�n de la Garant�a", "Garantia adicional Nombre persona natural/Jur�dica", "Garantia adicional Apellidos persona", "Garant�a C.C/DNI/NIT/RUC", "Garantia adicional Sucursal persona",
                                "Garantia adicional Tel�fono", "Garantia adicional Direcci�n", "Garant�a Provincia/Ciudad/Municipio", "Garantia adicional Departamento/Regi�n", "Observaciones"};

        dimensiones = new short[]{
            5000, 5000, 7000, 8000, 6000, 7000, 7000, 8000, 8000, 7000,
            7000, 6000, 8000, 7000, 7000, 6000, 5000, 7000, 6000, 7000, 5000, 6000,
            7000, 7000, 5000, 7000, 7000, 6000, 6000, 5000, 7000, 6000, 7000,
            8500, 7000, 7000,12000, 9000, 7000, 9500, 7000, 8000, 9000, 9500, 9000
        };

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        this.reponseJson =  apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);       
    }
    
    private void exportarExceReclamaciones() throws Exception {
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Reporte_Reclamaciones_", titulo = "REPORTE RECLAMACIONES";

        cabecera = new String[]{"Nit Entidad", "Id Cliente", "No Pagar�", "No Cr�dito", "Tipo Reclamaci�n", "F. Inicio Mora", "D�as Mora", "Valor Capital", "Intereses Corriente", "Intereses Mora", "No Cuotas"};

        dimensiones = new short[]{
            6000, 6000, 5000, 5000, 6000, 7000, 5000, 7000, 6000, 6000, 5000
        };

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        this.reponseJson =  apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);       
    }
    
    private void exportarExcelPrepagos() throws Exception {
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Reporte_Prepagos_", titulo = "REPORTE PREPAGOS";

        cabecera = new String[]{"Nit Entidad", "Id Cliente", "No Pagar�", "No Cr�dito", "Fecha Prepago"};

        dimensiones = new short[]{
            6000, 6000, 5000, 5000, 7000
        };

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        this.reponseJson =  apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);       
    }
    
    private void exportarExcelRecuperaciones() throws Exception {
        String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "Reporte_Recuperaciones_", titulo = "REPORTE RECUPERACIONES";

        cabecera = new String[]{"No Pagar�", "No Cr�dito", "Fecha Pago", "Valor Recuperado", "Valor Transferir GC", "Valor GC", "Valor Entidad", "Valor Reserva Entidad"};

        dimensiones = new short[]{
            5000, 5000, 6000, 7000, 7000, 7000, 7000, 7000
        };

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        this.reponseJson =  apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);       
    }
    
    public void createDir(String dir) throws Exception {
        try {
            File f = new File(dir);
            if (!f.exists()) {
                f.mkdir();
            }
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }
    
    
   private void listarDetalleFacturasNegocio() {
        try {
            
            String mora = (request.getParameter("mora") != null && !request.getParameter("mora").equals("")) ? request.getParameter("mora") : "90";
            String lineaNegocio = request.getParameter("linea_negocio") != null ? request.getParameter("linea_negocio") : "";
            String foto = request.getParameter("foto") != null ? request.getParameter("foto") : "";
            String cartera_en = request.getParameter("cartera_en") != null ? request.getParameter("cartera_en") : "";
            String empresa_fianza = request.getParameter("empresa_fianza") != null ? request.getParameter("empresa_fianza") : "";
            String cedula_cliente = request.getParameter("cedula_cliente") != null ? request.getParameter("cedula_cliente") : "";
            String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
            String monto_inicial = (request.getParameter("monto_inicial") != null && !request.getParameter("monto_inicial").equals("")) ? request.getParameter("monto_inicial") : "";
            String monto_final = (request.getParameter("monto_final") != null && !request.getParameter("monto_final").equals("")) ? request.getParameter("monto_final") : "";
            String aplica_aceleracion = request.getParameter("acelerar_pagare") != null ? request.getParameter("acelerar_pagare") : "false";
            String aplica_gac = request.getParameter("aplica_gac") != null ? request.getParameter("aplica_gac") : "false";
            String aplica_ixm = request.getParameter("aplica_ixm") != null ? request.getParameter("aplica_ixm") : "false";
            String cod_neg = request.getParameter("cod_neg") != null ? request.getParameter("cod_neg") : ""; 
            String query="";
            
            ParametrosBeans beans = new ParametrosBeans();       
            beans.setStringParameter1(foto);
            beans.setStringParameter2(lineaNegocio);
            beans.setStringParameter3(empresa_fianza);
            beans.setStringParameter4(mora);
            beans.setStringParameter5(cedula_cliente);
            beans.setStringParameter6(negocio);
            beans.setStringParameter7(monto_inicial);
            beans.setStringParameter8(monto_final); 
            beans.setStringParameter9(aplica_aceleracion);
            beans.setStringParameter10(aplica_gac);      
            beans.setStringParameter11(aplica_ixm);      

            if (lineaNegocio.equals("1")) {
                query = "SQL_CARGAR_DETALLE_FACTURAS_NEGOCIO";
            } else {
                query = "SQL_CARGAR_DETALLE_FACTURAS_NEGOCIO_EDU_CONS";
            }
            
           this.reponseJson = dao.cargarDetalleFacturasNegocio(cod_neg,beans,query);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
   
    private void cargarNegociosPendientesVistoBueno() {
        try {
           String id_unidad = (request.getParameter("id_unidad_negocio") != null) ? request.getParameter("id_unidad_negocio") : "";       
           this.reponseJson = dao.cargarNegociosVistoBuenoFianza(id_unidad);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void generarVistoBueno() {
        try {
            String json = request.getParameter("listJson") != null ? request.getParameter("listJson") : "";
            JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);     
            TransaccionService tservice = new TransaccionService(usuario.getBd());
            tservice.crearStatement();
            for (int i = 0; i < asJsonArray.size(); i++) {
                JsonObject objects = (JsonObject) asJsonArray.get(i);
                String id = objects.get("id").getAsJsonPrimitive().getAsString();
                String negocio = objects.get("negocio").getAsJsonPrimitive().getAsString();
                tservice.getSt().addBatch(dao.generarVistoBuenoFianza(negocio, usuario));
            }
            tservice.execute();
            tservice.closeAll();
           
            this.reponseJson = "{\"respuesta\":\"OK\"}";         
        
        } catch (Exception ex) {
            this.reponseJson = "{\"error\":\"Lo sentimos no se pudo guardar la informacion.\"}";
            ex.printStackTrace();
            Logger.getLogger(GarantiasComunitariasAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void generarVistoBuenoPoliza() {
        try {
            String json = request.getParameter("listJson") != null ? request.getParameter("listJson") : "";
            JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);     
            TransaccionService tservice = new TransaccionService(usuario.getBd());
            tservice.crearStatement();
            for (int i = 0; i < asJsonArray.size(); i++) {
                JsonObject objects = (JsonObject) asJsonArray.get(i);
                String id = objects.get("id").getAsJsonPrimitive().getAsString();
                String negocio = objects.get("negocio").getAsJsonPrimitive().getAsString();
                tservice.getSt().addBatch(dao.generarVistoBuenoPoliza(negocio, usuario));
            }
            tservice.execute();
            tservice.closeAll();
           
            this.reponseJson = "{\"respuesta\":\"OK\"}";         
        
        } catch (Exception ex) {
            this.reponseJson = "{\"error\":\"Lo sentimos no se pudo guardar la informacion.\"}";
            ex.printStackTrace();
            Logger.getLogger(GarantiasComunitariasAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
     private void cargarCboAgenciaUnidad() {
        try {       
            String id_unidad = (request.getParameter("id_unidad_negocio") != null) ? request.getParameter("id_unidad_negocio") : "0";
            this.reponseJson =  dao.cargarAgenciaUnidiad(Integer.parseInt(id_unidad));           
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
     
         private void indemnizarNegocios() {
        try {
            String json = request.getParameter("listJson") != null ? request.getParameter("listJson") : "";
            //JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
            JsonObject obj = (JsonObject) (new JsonParser()).parse(json);
            this.reponseJson = dao.indemnizarNegocios(obj, usuario);
        
        } catch (Exception ex) {
            this.reponseJson = "{\"error\":\"Lo sentimos no se pudo guardar la informacion.\"}";
            ex.printStackTrace();
            Logger.getLogger(GarantiasComunitariasAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void exportarExcelIndemnizado() throws Exception {
        JsonArray asJsonArray = (JsonArray) new JsonParser().parse(dao.cargarNegociosIndemnizados()); 
        String[] cabecera = null;
        short[] dimensiones = null;
        String nombreArchivo = "reporte_negocios_indemnizado", titulo = "REPORTE NEGOCIOS INDEMNIZADO";

        cabecera = new String[]{"NIT/RUC de la Entidad","C.C / DNI / NIT / RUC del Cliente","N�mero de Pagar�","Nro. de cr�dito","Tipo de Reclamaci�n",
        "Fecha comienzo de la mora","Dias de Mora","Valor a reclamar (capital)","Valor a reclamar Inter�ses Corriente","Valor a reclamar Inter�s de Mora",
        "Valor Otros Cargos","N�mero de Cuotas a Reclamar"};

        dimensiones = new short[]{
            5000, 5000, 6000, 7000, 7000, 7000, 7000, 7000, 7000, 7000
        };

        ExcelApiUtil apiUtil = new ExcelApiUtil(usuario);
        this.reponseJson = apiUtil.crearArchivoExcel(nombreArchivo, titulo, asJsonArray, dimensiones, cabecera, request);
    }

    private void verNegociosIndemnizados() {
        try {
            String periodo = request.getParameter("periodo") != null ? request.getParameter("periodo") : "";
            this.reponseJson = dao.verNegociosIndemnizados(periodo);
        } catch (Exception ex) {
            Logger.getLogger(GarantiasComunitariasAction.class.getName()).log(Level.SEVERE, null, ex);
            ex.printStackTrace();
        }
    }


    private void exportaPdf(String usuario) {
       // String json = "{\"respuesta\":\"GENERADO\"}";
        try {
            String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
            this.printlnResponseAjax(dao.generarPdf(usuario, negocio), "application/json;");
            
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void printlnResponseAjax(String respuesta, String contentType) throws Exception {

        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);

    }

    private void cargarProveedorFianza() {
        try {
            this.reponseJson = dao.cargarProveedorFianza();            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void buscarProveedorFianza() {
        try {
            String campo = request.getParameter("campo") != null ? request.getParameter("campo") : "";
            String texto = request.getParameter("texto") != null ? request.getParameter("texto") : "";
            this.reponseJson = dao.buscarProveedorFianza(campo, texto);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void guardarProveedorFianza() {
        String nit = (request.getParameter("nit") != null) ? request.getParameter("nit") : "";
        this.reponseJson = dao.guardarProveedorFianza(nit, usuario);
    }
    
    private void cambiarEstadoProveedorFianza() {
        
        String id = request.getParameter("id") != null ? request.getParameter("id") : "0";
        this.reponseJson = dao.cambiarEstadoProveedorFianza(id, usuario);
    }
    
     private void ExportarCxpConsolidada() throws Exception{
          try {
         String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
            JsonArray asJsonArray = (JsonArray) new JsonParser().parse(json);
            this.generarRUTA();
            this.crearLibro("Cxp Consolidada", "Reporte");
        
         String[] cabecera = new String[]{"Nit Cliente", "Nombre", "Doc. Relacionado", "Negocio", "No Cuotas",
            "Fecha Vencimiento", "Valor Negocio", "Valor Desembolso", "Valor Fianza"};

        short[] dimensiones = new short[]{
            5000, 5000, 5000, 5000, 5000, 5000, 
            5000, 5000, 5000
        };
          this.generaTitulos(cabecera, dimensiones);
          
            for (int i = 0; i < asJsonArray.size(); i++) {
                JsonObject objects = (JsonObject) asJsonArray.get(i);
                fila++;
                int col = 0;
//                xls.adicionarCelda(fila, col++, objects.get("id").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("nit_cliente").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("nombre_cliente").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("documento_rel").getAsString(), letra);
                xls.adicionarCelda(fila, col++, objects.get("negocio").getAsString(), dinero);
                xls.adicionarCelda(fila, col++, objects.get("num_cuotas").getAsString(), letra);
//                xls.adicionarCelda(fila, col++, objects.get("id_convenio").getAsString(), letra);              
                xls.adicionarCelda(fila, col++, objects.get("fecha_vencimiento").getAsString(), letra);               
                xls.adicionarCelda(fila, col++, objects.get("valor_negocio").getAsFloat(), dinero);
                xls.adicionarCelda(fila, col++, objects.get("valor_desembolsado").getAsFloat(), dinero);
                xls.adicionarCelda(fila, col++, objects.get("valor_fianza").getAsFloat(), dinero);
               
            }

            this.cerrarArchivo();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }
      
        
    }
     
     public void generarRUTA() throws Exception{
        try{

            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            rutaInformes = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File( rutaInformes );
            if (!archivo.exists()) archivo.mkdirs();

        }catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }

    }
      private void crearLibro(String nameFileParcial, String titulo) throws Exception {
        try{
            fmt= new SimpleDateFormat("yyyMMdd");
            this.crearArchivo(nameFileParcial + fmt.format( new Date() ) +".xls", titulo);

        }catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage() );
        }
    }
      
      private void crearArchivo(String nameFile, String titulo) throws Exception{
        try{
            fmt = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            InitArchivo(nameFile);
            xls.obtenerHoja("Base");
            xls.combinarCeldas(0, 0, 0, 8);
            xls.adicionarCelda(0,0, titulo, header);
        }catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
      
      private void InitArchivo(String nameFile) throws Exception{
        try{
            xls          = new com.tsp.operation.model.beans.POIWrite();
            nombre       = "/exportar/migracion/" + usuario.getLogin() + "/" + nameFile;
            xls.nuevoLibro( rutaInformes + "/" + nameFile );
            header         = xls.nuevoEstilo("Tahoma", 10, true  , false, "text"  , HSSFColor.BLUE.index , xls.NONE , HSSFCellStyle.ALIGN_LEFT);
            titulo1        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , xls.NONE  , xls.NONE , xls.NONE);
            titulo2        = xls.nuevoEstilo("Tahoma", 8 , true  , false, "text"  , HSSFColor.WHITE.index, HSSFColor.BLUE.index , HSSFCellStyle.ALIGN_CENTER, 2);
            letra          = xls.nuevoEstilo("Tahoma", 8 , false , false, ""      , xls.NONE , xls.NONE , xls.NONE);
            dinero         = xls.nuevoEstilo("Tahoma", 8 , false , false, "#,##0.00" , xls.NONE , xls.NONE , xls.NONE);
            porcentaje     = xls.nuevoEstilo("Tahoma", 8 , false , false, "0.00%" , xls.NONE , xls.NONE , xls.NONE);
            ftofecha       = xls.nuevoEstilo("Tahoma", 8 , false , false, "yyyy-mm-dd" , xls.NONE , xls.NONE , xls.NONE);

        }catch (Exception ex){
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }

   }
      
       private void generaTitulos(String[] cabecera, short[] dimensiones) throws Exception {

        try {

            fila = 2;

            for (int i = 0; i < cabecera.length; i++) {
                xls.adicionarCelda(fila, i, cabecera[i], titulo2);
                if (i < dimensiones.length) {
                    xls.cambiarAnchoColumna(i, dimensiones[i]);
                }
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage());
        }
    }
       
        private void cerrarArchivo() throws Exception {
        try{
            if (xls!=null)
                xls.cerrarLibro();
        }catch (Exception ex){
            System.out.println(ex.getMessage());
            throw new Exception( "Error en crearArchivo ....\n"  + ex.getMessage() );
        }
    }
        
        private void cargarCboProducto() {
            try {         
                this.reponseJson =  dao.cargarCboProducto();           
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        private void cargarCboCobertura() {
            try {         
                this.reponseJson =  dao.cargarCboCobertura();           
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
}
