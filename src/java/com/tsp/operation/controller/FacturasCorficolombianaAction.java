package com.tsp.operation.controller;

import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.threads.HExportarExcel;
//import com.tsp.operation.model.threads.HGenerarArchivoCorficolombiana;
import javax.servlet.http.HttpSession;

public class FacturasCorficolombianaAction extends Action {
    //protected HttpServletRequest request;

    public FacturasCorficolombianaAction() {
    }

    public void run() throws ServletException, InformationException {
        //String id_reporte, tipo, periodo,reporte;
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        /*id_reporte=(request.getParameter("reporte")!=null)?request.getParameter("reporte").split(";;;")[0]:"";
        reporte=(request.getParameter("reporte")!=null)?request.getParameter("reporte").split(";;;")[1]:"";
        periodo=request.getParameter("periodo");
        tipo=request.getParameter("tipo");*/
        String parametro = request.getParameter("parametro");
        String next = "/jsp/consorcio/tabla_nms.jsp";

        try {
            /*response.setContentType("text/plain");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println(model.corficolombianaSvc.getConsulta(id_reporte, tipo, periodo));*/

            /*if(parametro.equals("EXPORTAR")){
            HExportarExcel hilo = new HExportarExcel();
            hilo.start(model,model.corficolombianaSvc.getResultado(), usuario,"REPORTE CORFICOLOMBIANA");
            response.setContentType("text/plain");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println("El proceso ha sido iniciado, porfavor verifique el estado en el log de procesos");
            }
            else{*/
            if (parametro.equals("GENERAR_FACTURAS")) {
                //HGenerarFacturasCorficolombiana hilo = new HGenerarFacturasCorficolombiana();
                //model.corficolombianaSvc.getConsulta(id_reporte, tipo, periodo,parametro);

                //hilo.start(model,model.corficolombianaSvc.getResultado(), usuario,reporte);
                //hilo.start(model,model.corficolombianaSvc.getResultado(), usuario);
                String[] facturas = request.getParameterValues("fact");
                if (facturas != null && facturas.length > 0) {
                    String msj = model.facturascorficolombianaSvc.generarFacturas(facturas, usuario.getLogin());
                } else {
                    model.facturascorficolombianaSvc.generarArchivo(usuario.getLogin(),"REPFIDECA");
                }
                model.facturascorficolombianaSvc.getConsultaNms();
                //response.setContentType("text/plain");
                //response.setHeader("Cache-Control", "no-cache");

                //response.getWriter().println("El proceso ha sido iniciado, porfavor verifique el estado en el log de procesos");
                //this.dispatchRequest(next+"?parametro="+parametro);
                this.dispatchRequest(next);
            } else {
                if (parametro.equals("BUSCAR_FACTURAS")) {
                    next = "/jsp/consorcio/tabla_res.jsp";
                    model.facturascorficolombianaSvc.getConsultaRes();
                    this.dispatchRequest(next);
                } else {
                    if (parametro.equals("GENERAR_REPORTE")) {
                        String[] facturas = request.getParameterValues("fact");
                        String fecha = request.getParameter("fecha");
                        if (facturas != null && facturas.length > 0) {
                            String msj = model.facturascorficolombianaSvc.generarReporte(facturas, usuario.getLogin(),fecha);
                        } else {
                            model.facturascorficolombianaSvc.generarArchivo(usuario.getLogin(),"REPFIDECAR");
                        }
                        next = "/jsp/consorcio/tabla_res.jsp";
                        model.facturascorficolombianaSvc.getConsultaRes();
                        this.dispatchRequest(next);
                    } else {
                        //model.corficolombianaSvc.getConsulta(id_reporte, tipo, periodo,parametro);
                        model.facturascorficolombianaSvc.getConsultaNms();
                        //this.dispatchRequest(next+"?parametro="+parametro);
                        this.dispatchRequest(next);
                    }
                }

            }
            //}
        } catch (Exception e) {
            System.out.println("error en FacturasCorficolombianaAction:" + e.toString() + "__" + e.getMessage());
            e.printStackTrace();
        }
    }
}
