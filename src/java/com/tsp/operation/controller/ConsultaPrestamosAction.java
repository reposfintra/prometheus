/**
 * Nombre        ConsultaPrestamosAction.java
 * Descripci�n   Clase para manejar los eventos de consulta sobre los prestamos
 * Autor         Mario Fontalvo Solano
 * Fecha         12 de febrero de 2006, 12:56 PM
 * Version       1.0
 * Coyright      Transportes S�nchez Polo S.A.
 **/


package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.beans.Prestamo;
import com.tsp.operation.model.beans.Amortizacion;
import com.tsp.operation.model.beans.FuncionesFinancieras;
import com.tsp.operation.model.beans.Usuario;
import java.util.List;

public class ConsultaPrestamosAction extends Action{
    
    /** Crea una nueva instancia de  ConsultaPrestamosAction */
    public ConsultaPrestamosAction() {
    }
    
    
    /**
     * Metodo principal del Action
     * @autor mfontalvo
     * @throws ServletException.
     * @throws InformationException.
     */
    public void run() throws ServletException, InformationException {
        try{
            
            HttpSession session = request.getSession();
            Usuario     usuario = (Usuario) session.getAttribute("Usuario");
            
            String opcion    = defaultString("Opcion","");
            String index     = defaultString("Index","");
            String vista     = defaultString("TipoListado","");
            String next      = "/jsp/cxpagar/prestamos/consultas/ConsultaPrestamos.jsp";
            String LOV[]     = request.getParameterValues("LOV");
            String fechas[]  = request.getParameterValues("fechas");
            
            ////////////////////////////////////////////////////////////////////
            if (opcion.equals("Consultar")){
                String where = construirWhere().toString();
                session.setAttribute("paramWhere", where);
                model.PrestamoSvc.listaPrestamos(where.toString());
                model.PrestamoSvc.generarTotalesPrestamo();
                if (model.PrestamoSvc.getListaPrestamos().isEmpty()){
                    next += "?Mensaje=No se encontraron registros para sus parametros";
                }else{
                    next = "/jsp/cxpagar/prestamos/consultas/ListadoPrestamos.jsp";
                }
                
                if (vista.equals("Programa")){
                    model.PrestamoSvc.isUsuarioAprobadorPrestamo(usuario.getDstrct(), usuario.getLogin());
                    if ( model.PrestamoSvc.isAprobador() )
                        session.setAttribute("PerfilPrestamo","AP");
                    else    
                        session.setAttribute("PerfilPrestamo","CO");
                }
                else
                    next = "/jsp/cxpagar/prestamos/consultas/ListadoResumenBeneficiario.jsp";
                
            }
            
            
            ////////////////////////////////////////////////////////////////////
            else if (opcion.equals("Amortizacion")){
                next  = "/jsp/cxpagar/prestamos/Amortizacion.jsp";
                Prestamo pt = (Prestamo) model.PrestamoSvc.getListaPrestamos().get( Integer.parseInt(index) );
                
                // si aun no se han cargado las amortizaciones
                if (pt.getAmortizacion()==null || pt.getAmortizacion().isEmpty()){
                    if (pt.getAprobado().equals("S")){
                        model.PrestamoSvc.searchAmortizacionPorPrestamo( pt.getDistrito(), pt.getId() );
                        pt.setAmortizacion( model.PrestamoSvc.getListaAmortizaciones() );
                    }
                    else
                        pt.setAmortizacion( FuncionesFinancieras.getAmortizacion(pt) );
                }
                model.PrestamoSvc.setPrestamo (pt);
                
                
                request.setAttribute("indexSel", index);
                request.setAttribute("prestamo", pt   );
            }
            
            
            ////////////////////////////////////////////////////////////////////
            else if (opcion.equals("ActualizarAmortizacion")){
                int cuotas = Integer.parseInt( defaultString ("cuotas","0" ));
                next  = "/jsp/cxpagar/prestamos/Amortizacion.jsp";
                Prestamo pt = (Prestamo) model.PrestamoSvc.getListaPrestamos().get(Integer.parseInt(index));
                actualizarPrestamo(pt, fechas);
                if ( cuotas != pt.getCuotas() ) {
                    pt.setCuotas( cuotas );
                    FuncionesFinancieras.modificarPrestamoInteresAnticipadoPorCuotas(pt);
                } else {
                    FuncionesFinancieras.modificarPrestamoInteresAnticipadoPorFechas(pt);
                }

                // si el prestamo ya esta aprobado lo reliquida solo con las que puede modificar
                if (pt.getAprobado().equals("S")){
                    model.PrestamoSvc.reliquidacionAmortizaciones(pt, usuario.getLogin());
                    model.PrestamoSvc.actualizarInteresPrestamo(pt, usuario.getLogin());
                    model.PrestamoSvc.actualizarCuotasPrestamo (pt, usuario.getLogin());
                    next += "?Mensaje=Cambios grabados en la base de datos.";
                }else{
                    next += "?Mensaje=Amortizaciones Actualizadas, recuerde que estos cambios solo se guardan cuando usted apruebe el prestamo";
                }
                
                model.PrestamoSvc.setPrestamo (pt);
                
                request.setAttribute("indexSel", index);
                request.setAttribute("prestamo", pt   );
            }            
            
            ////////////////////////////////////////////////////////////////////
            else if (opcion.equals("RestablecerAmortizacion")){
                next  = "/jsp/cxpagar/prestamos/Amortizacion.jsp";
                Prestamo pt = (Prestamo) model.PrestamoSvc.getListaPrestamos().get(Integer.parseInt(index));
                if (pt.getAprobado().equals("N")){
                    pt.setAmortizacion( FuncionesFinancieras.getAmortizacion(pt) );
                }
                model.PrestamoSvc.setPrestamo (pt);
                request.setAttribute("indexSel", index);
                request.setAttribute("prestamo", pt   );
                next += "?Mensaje=Amortizaciones restablecidas.";
            }           
            
            ////////////////////////////////////////////////////////////////////
            else if (opcion.equals("AprobarPrestamo")){
                next = "/jsp/cxpagar/prestamos/consultas/ListadoPrestamos.jsp";
                if (LOV!=null){
                    this.aprobarPrestamos(LOV, usuario.getLogin() );
                    String where = (String) session.getAttribute("paramWhere");
                    model.PrestamoSvc.listaPrestamos(where.toString());
                    next += "?Mensaje=Prestamos aprobados, su lista se ha recargado nuevamente.";
                }
            } 
            
            
            ////////////////////////////////////////////////////////////////////
            else if (opcion.equals("EliminarPrestamo")){
                next = "/jsp/cxpagar/prestamos/consultas/ListadoPrestamos.jsp";
                if (LOV!=null){
                    this.eliminarPrestamos(LOV);
                    String where = (String) session.getAttribute("paramWhere");
                    model.PrestamoSvc.listaPrestamos(where.toString());
                    next += "?Mensaje=Prestamos eliminados, su lista se ha recargado nuevamente.";
                }
            }
            
            ////////////////////////////////////////////////////////////////////
            else if (opcion.equals("ActualizarListadoPrestamos")){
                next = "/jsp/cxpagar/prestamos/consultas/ListadoPrestamos.jsp";
                String where = (String) session.getAttribute("paramWhere");
                model.PrestamoSvc.listaPrestamos(where.toString());
            }            
            
            
            ////////////////////////////////////////////////////////////////////
            // consulta de proveedores
            else if (opcion.equals("ConsultaProveedores")){
                String op = defaultString ("op","");
                if (op.equals("")){
                    if (model.PrestamoSvc.getBeneficiarios()==null || model.PrestamoSvc.getBeneficiarios().isEmpty())
                        model.PrestamoSvc.loadBeneficiarios( usuario.getId_agencia() );
                } else if (op.equals("reload"))
                    model.PrestamoSvc.loadBeneficiarios( usuario.getId_agencia() );
                next    = "/jsp/cxpagar/prestamos/consultas/ConsultaProveedores.jsp";
            }
                                
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new Exception("No se pudo encontrar "+ next);
            rd.forward(request, response);
        }
        catch(Exception e){
            e.printStackTrace();
            throw new ServletException(e.getMessage());
        }
    }
    
    
    
    /**
     * Funcion que construye las restricciones del listado a consultar
     * @autor mfontalvo
     * @return restreccione para el listado
     * @see defaultString(String).
     * @see initWhere(StringBuffer).
     */
    private StringBuffer construirWhere(){
        
        StringBuffer where = new StringBuffer();
        
        // clasificacion
        String opClasificacion = defaultString("opClasificacion","");
        String Clasificacion   = defaultString("Clasificacion","");
        if (opClasificacion.equals("ok")){
            where = initWhere(where);
            where.append(" p.clasificacion = '"+ Clasificacion  +"' ");
        }
        
        
        // distrito
        String opDistrito = defaultString("opDistrito","");
        String Distrito   = defaultString("Distrito","");
        if (opDistrito.equals("ok")){
            where = initWhere(where);
            where.append(" p.dstrct = '"+ Distrito  +"' ");
        }
        
        
        // estado
        String opEstado = defaultString("opEstado","");
        String Estado   = defaultString("Estado","");
        if (opEstado.equals("ok")){
            if ("P|V".indexOf(Estado)!=-1){
                where.append(" inner join fin.amortizaciones a on ( a.dstrct = p.dstrct and a.prestamo = p.id and  a.item = p.cuotas) WHERE ");
                if (Estado.equalsIgnoreCase("P"))
                    where.append( " a.estado_pago_ter = '50'  ");
                else
                    where.append( " a.estado_pago_ter != '50' ");
            }
            else{               
                where = initWhere(where);
                where.append(" p.aprobado = '"+ Estado  +"' ");
            }
        }
        
        // tercero
        String opTercero = defaultString("opTercero","");
        String Tercero   = defaultString("Tercero","");
        if (opTercero.equals("ok")){
            where = initWhere(where);
            where.append(" p.tercero = '"+ Tercero  +"' ");
        }
        
        // beneficiario
        String opBeneficiario = defaultString("opBeneficiario","");
        String Beneficiario   = defaultString("Beneficiario","");
        if (opBeneficiario.equals("ok")){
            where = initWhere(where);
            where.append(" p.beneficiario = '"+ Beneficiario  +"' ");
        }
        
        // tipo prestamo
        String   opTipoPrestamo = defaultString("opTipoPrestamo","");
        String[] TipoPrestamo   = request.getParameterValues("TipoPrestamo");
        if (opTipoPrestamo.equals("ok") && TipoPrestamo!=null && TipoPrestamo.length>0){
            where = initWhere(where);
            
            where.append(" p.tipoprestamo in ( ");
            for (int i=0;i<TipoPrestamo.length;i++){
                where.append("'"+ TipoPrestamo[i] +"'");
                if (  (i+1) != TipoPrestamo.length )
                    where.append(",");
            }
            where.append(")");
        }
        
        // periodos prestamo
        String opPeriodo = defaultString("opPeriodo","");
        String Periodo   = defaultString("Periodo","");
        if (opPeriodo.equals("ok")){
            where = initWhere(where);
            where.append(" p.periodos = '"+ Periodo  +"' ");
        }
        
        // fecha entrega
        String opFecha = defaultString("opFecha","");
        String FechaInicial   = defaultString("FechaInicial","");
        String FechaFinal     = defaultString("FechaFinal"  ,"");
        if (opFecha.equals("ok")){
            where = initWhere(where);
            where.append(" p.entregadinero between '"+ FechaInicial  +"' and '"+ FechaFinal +"'");
        }
        
        
        
        // usuario en session
        String opUsuario = defaultString("opUsuario","");
        String Usuario   = defaultString("Usuario","");
        if (opUsuario.equals("ok")){
            where = initWhere(where);
            where.append(" p.creation_user = '"+ Usuario +"' ");
        }
        
        
        // id del prestamo
        String opId = defaultString("opId","");
        String Id   = defaultString("Id","");
        if (opId.equals("ok")){
            where = initWhere(where);
            where.append(" p.id = '"+ Id +"'");
        }
        
        
        if (!(Estado.equals("P") || Estado.equals("V")) ){
            if (!where.toString().trim().equals("") ){
                where = new StringBuffer( " WHERE " + where.toString() );
            }
        }
        
        
        return where;
    }
    
    /**
     * Funcion para obtener un parametro del objeto request
     * y en caso de no existri este devuelve el segundo parametro
     * @autor mfontalvo
     * @param name, nombre del parametro
     * @param opcion, valor opcional a devolver en caso de que nom exista
     * @return Parametro del request
     */
    private String defaultString(String name, String opcion){
        return (request.getParameter(name)==null?opcion:request.getParameter(name));
    }
    
    
    /**
     * Funcion que inicializa un StringBuffer donde se almacena las restricciones
     * del listado a buscar
     * @autor mfontalvo
     * @param where, restricciones
     * @return restricciones
     */
    private StringBuffer initWhere( StringBuffer where ) {
        return (StringBuffer) (where.toString().equals("")? new StringBuffer("") : where.append(" and ") );
    }
    
    
    /**
     * Metodo para aprobar los prestamos contenidos dentro de una lista
     * @autor mfontalvo
     * @param index[], arregloe de los indeces de la lista que se van a aprobar
     * @param usuario, usuario en session
     * @throws Exception.
     */
    private void aprobarPrestamos(String []index, String usuario) throws Exception {
        try{
            for (int i=0;i<index.length; i++){
                
                String []datos = index[i].split("~");
                model.PrestamoSvc.searchPrestamo(datos[0] , Integer.parseInt(datos[1]));
                Prestamo ac = (Prestamo) model.PrestamoSvc.getListaPrestamos().get( Integer.parseInt(datos[2]) );
                Prestamo uv = model.PrestamoSvc.getPrestamo();
                if (uv!=null && !uv.getAprobado().equals("S")){
                    if (ac.getAmortizacion()!=null && !ac.getAmortizacion().isEmpty()){
                        
                        model.PrestamoSvc.aprobarPrestamo(ac, usuario);
                        if ((ac.getTipoPrestamo().equals("IA") || ac.getTipoPrestamo().equals("CV")) && ac.getIntereses()!=uv.getIntereses() )
                           model.PrestamoSvc.actualizarInteresPrestamo(ac, usuario);
                        if ((ac.getTipoPrestamo().equals("IA") || ac.getTipoPrestamo().equals("CV"))&& ac.getCuotas()   !=uv.getCuotas   () )
                           model.PrestamoSvc.actualizarCuotasPrestamo (ac, usuario);
                        
                    }
                    else{
                        uv.setAmortizacion( FuncionesFinancieras.getAmortizacion(uv) );
                        model.PrestamoSvc.aprobarPrestamo(uv, usuario);
                    }
                }
                
            }
        }catch (Exception ex){
            throw new Exception("Error en el procedimiento de aprobarPrestamos [ConsultaPrestamosAction] ...\n" + ex.getMessage());
        }
    }
    
    
    
    /**
     * Metodo para eliminar los prestamos contenidos dentro de una lista
     * @autor mfontalvo
     * @param index[], arregloe de los indeces de la lista que se van a aprobar
     * @throws Exception.
     */
    private void eliminarPrestamos(String []index) throws Exception {
        try{
            for (int i=0;i<index.length; i++){
                String []datos = index[i].split("~");
                model.PrestamoSvc.eliminarPrestamo( datos[0] , Integer.parseInt(datos[1]));
            }
        }catch (Exception ex){
            throw new Exception("Error en el procedimiento de eliminarPrestamos [ConsultaPrestamosAction] ...\n" + ex.getMessage());
        }
    }    
    
    /**
     * Metodo para actualizar las amortizaciones de un prestamo
     * @autor mfontalvo
     * @param fechas[], arreglo de las nuevas fechas de las amorizaciones
     * @param pt, prestamo a modificar
     * @throws Exception.
     */
    private void actualizarPrestamo(Prestamo pt, String []fechas) throws Exception {
        try{
            for (int i=0; fechas!=null && i<fechas.length  && i<pt.getAmortizacion().size(); i++){
                String fecha = fechas[i];
                Amortizacion am = (Amortizacion) pt.getAmortizacion().get(i);
                if (pt.getAprobado().equals("S")){
                    if ( am.getFechaMigracion().equals("0099-01-01 00:00:00") &&  
                         am.getEstadoDescuento().equals("") && 
                         am.getEstadoPagoTercero().equals("")) {
                            am.setFecha(new java.util.Date ( fecha.replaceAll("-", "/") ) );
                            am.setSeleccionada(true);
                    }
                }
                else{
                    am.setFecha(new java.util.Date ( fecha.replaceAll("-", "/") ) );
                    am.setSeleccionada(true);
                }
            }
        }catch (Exception ex){
            throw new Exception("Error en el procedimiento de actualizarPrestamo [ConsultaPrestamosAction] ...\n" + ex.getMessage());
        }
    }    
    
    
    
    
    private void CancelacionPrestamo (Prestamo pt){
        
        List amortizaciones = pt.getAmortizacion();
        double capitalPendiente = 0;
        for (int i=0; i<amortizaciones.size() ; i++){
            Amortizacion am = (Amortizacion) amortizaciones.get(i);
            if ( am.getFechaMigracion().equals("0099-01-01 00:00:00") &&  
                 am.getEstadoDescuento().equals("") && 
                 am.getEstadoPagoTercero().equals("")) {
                 capitalPendiente += am.getCapital();   
            }
        }        
        
    }
    
}
