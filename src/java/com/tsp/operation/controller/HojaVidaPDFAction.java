/********************************************************************
 *      Nombre Clase.................   HojaVidaPDFAction.java
 *      Descripci�n..................   Impresi�n en Formato PDF de lla Hoja de Vida
 *      Autor........................   Ing. Andr�s Maturana De La Cruz
 *      Fecha........................   24 Abril del 2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes S�nchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;


import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import com.tsp.pdf.*;
import java.io.*;

public class HojaVidaPDFAction extends Action{
    
    /** Creates a new instance of HojaControlViajeAction */
    public HojaVidaPDFAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        try{
            
            String next = "";
            
            String ruta = application.getRealPath("/");
            //File xslt = new File(ruta + "Templates/hvc2.xsl");
            File xslt = new File(ruta + "Templates/HojaVida.xsl");
            File pdf = new File(ruta + "pdf/hv.pdf");
            
            request.setAttribute("ArchivoXSLT", xslt );
            request.setAttribute("ArchivoPDF", pdf );
            
            next = "/jsp/hvida/hvpdf.jsp";
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en HojaControlViajeAction .....\n"+e.getMessage());
        }
    }
    
}
