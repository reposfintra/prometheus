/*
 * AutoAnticipoSearchAction.java
 *
 * Created on 18 de junio de 2005, 03:43 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  Sandrameg
 */
public class AutoAnticipoSearchAction extends Action {
    
    /** Creates a new instance of AutoAnticipoSearchAction */
    public AutoAnticipoSearchAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/anticipo_placas/anticipopSearch.jsp";        
        String placa = request.getParameter("pl").toUpperCase();
        
        try{
            model.autoAnticipoService.buscar(placa);
            
            if(model.autoAnticipoService.getAuto_Anticipo()!= null){
                request.setAttribute("autoant", model.autoAnticipoService.getAuto_Anticipo());
                next = next + "?msg=exitoB";            
            }
            else {                
                next = next + "?msg=errorB&pl=" + placa;
                //System.out.println("ERROR " + next );
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}
