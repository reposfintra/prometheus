/************************************************************************
 * Nombre Discrepancia_productoInsertAction.java
 * Descripci�n: Accion para ingresar un producto de discrepancia.
 * Autor: Jose de la rosa
 * Fecha: 13 de octubre de 2005, 05:29 PM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 **************************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.*;


public class Discrepancia_productoInsertAction extends Action {
    
    /** Creates a new instance of Discrepancia_productoInsertAction */
    public Discrepancia_productoInsertAction() {
    }
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        String hoy          = Utility.getHoy("-");
        int num_discre = Integer.parseInt(request.getParameter("c_num_discre"));
        String numpla = request.getParameter("c_numpla");
        String clientes = request.getParameter("client");
        
        String nom_doc = request.getParameter("nom_doc");
        String numrem = request.getParameter("c_numrem");
        String tipo_doc = request.getParameter("c_tipo_doc");
        String nom_des = request.getParameter("c_nomdes");
        String documento = request.getParameter("c_documento");
        String cod_producto = request.getParameter("c_cod_producto").toUpperCase();
        String descripcion = request.getParameter("c_descripcion");
        String ubicacion = request.getParameter("c_ubicacion");
        String contacto = request.getParameter("c_contacto");
        String unidad = request.getParameter("c_unidad");
        double cantidad = Double.parseDouble(request.getParameter("c_cantidad")!=null?!request.getParameter("c_cantidad").equals("")?request.getParameter("c_cantidad"):"0":"0");
        double valor = 0;
        
        
        if( request.getParameter("c_valor")!=null && !request.getParameter("c_valor").equals("") ){
            valor = Double.parseDouble(request.getParameter("c_valor"));
        }
        else{
            valor = 0;
        }
        String opcion = request.getParameter("opcion")!=null?request.getParameter("opcion"):"";
        String tipo_doc_rel = (String) request.getParameter("c_tipo_doc_rel");
        String documento_rel = (String) request.getParameter("c_documento_rel");
        String cod_discrepancia = request.getParameter("c_cod_discrepancia");
        String causa = request.getParameter("c_causa");
        String responsable = request.getParameter("c_responsable");
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = (String) session.getAttribute("Distrito");
        String grupo = request.getParameter("grupo")!=null?request.getParameter("grupo"):"";
        String next = "";
        int sw=0;
        
        try {
            
            if(opcion.equals("1")){
                Vector consultas = new Vector();
                Discrepancia discrep = model.discrepanciaService.getCabDiscrepancia();
                if ( !model.discrepanciaService.existeDiscrepancia(discrep.getNro_planilla(), discrep.getNro_remesa(), discrep.getTipo_doc(), discrep.getDocumento(), discrep.getTipo_doc_rel(), discrep.getDocumento_rel(), distrito ) ) {
                    model.discrepanciaService.setDiscrepancia(discrep);
                    consultas.add( model.discrepanciaService.insertarDiscrepancia( discrep ) );
                }
                else{
                    discrep.setUsuario_modificacion(usuario.getLogin().toUpperCase());
                    consultas.add( model.discrepanciaService.updateDiscrepancia( discrep ) );
                }
                
                model.discrepanciaService.listDiscrepanciaProducto( discrep.getNro_discrepancia(), discrep.getNro_planilla(), discrep.getNro_remesa(), discrep.getTipo_doc(), discrep.getDocumento(), discrep.getTipo_doc_rel(), discrep.getDocumento_rel(), distrito);
                Vector vec = model.discrepanciaService.getDiscrepancias();
                boolean sw1=false;
                
                if( vec != null ){
                    for ( int j = 0 ; j < vec.size() ; j++ ){
                        Discrepancia di = ( Discrepancia ) vec.get( j );
                        sw1 = false;
                        
                        for ( int i = 0 ; i < model.discrepanciaService.getItemsDiscrepancia().size() ; i++ ){
                            Discrepancia disc = ( Discrepancia ) model.discrepanciaService.getItemsDiscrepancia().get( i );
                            if( disc.getNro_planilla().equals( di.getNro_planilla()) && disc.getNro_remesa().equals( di.getNro_remesa() ) &&
                            disc.getNro_discrepancia() == di.getNro_discrepancia() && disc.getTipo_doc().equals( di.getTipo_doc() ) &&
                            disc.getDocumento().equals( di.getDocumento() ) && disc.getTipo_doc_rel().equals( di.getTipo_doc_rel() ) &&
                            disc.getDocumento_rel().equals( di.getDocumento_rel() ) && disc.getFecha_creacion().equals( di.getFecha_creacion() ) &&
                            disc.getCod_producto().equals( di.getCod_producto() ) && disc.getCod_discrepancia().equals( di.getCod_discrepancia() )
                            ){
                                sw1=true;
                            }
                        }
                        if (!sw1) {
                            di.setUsuario_modificacion(usuario.getLogin());
                            consultas.add( model.discrepanciaService.anularDiscrepanciaProducto(di) );
                        }
                    }
                }
                
                InfoPlanilla inf = model.planillaService.obtenerInformacionPlanilla(numpla);
                for ( int i = 0 ; i < model.discrepanciaService.getItemsDiscrepancia().size() ; i++ ){
                    Discrepancia disc = ( Discrepancia ) model.discrepanciaService.getItemsDiscrepancia().get( i );
                    model.productoService.serchProducto( disc.getCod_producto() , distrito, disc.getCliente());
                    Producto p = model.productoService.getProducto();                    
                                        
                    
                    model.discrepanciaService.searchDiscrepanciaProducto( disc.getNro_discrepancia(), disc.getNro_planilla(), disc.getNro_remesa(), disc.getTipo_doc(), disc.getDocumento(), disc.getTipo_doc_rel(), disc.getDocumento_rel(), disc.getCod_producto(), disc.getCod_discrepancia(), disc.getFecha_creacion(), distrito );
                    if ( model.discrepanciaService.getDiscrepancia() != null ) {
                        disc.setUsuario_modificacion(usuario.getLogin().toUpperCase());
                        consultas.add( model.discrepanciaService.updateDiscrepanciaProducto(disc) );
                    }
                    else{
                        disc.setUsuario_modificacion(usuario.getLogin());
                        disc.setUsuario_creacion(usuario.getLogin());
                        disc.setBase(usuario.getBase());
                        disc.setDistrito(distrito);
                        //disc.setFecha_creacion( " cast('now()' as timestamp)  + cast('"+i+" second' as interval) ");
                        model.discrepanciaService.setDiscrepancia(disc);
                        consultas.add( model.discrepanciaService.insertarDiscrepanciaProducto(disc) );
                        consultas.add( model.discrepanciaService.insertarDiscrepanciaInventario(disc) );
                    }
                }
                
                
                model.despachoService.insertar(consultas);
                
                model.discrepanciaService.searchDiscrepancia(numpla, numrem, tipo_doc, documento, tipo_doc_rel, documento_rel, distrito );
                Discrepancia d = model.discrepanciaService.getDiscrepancia();
                model.discrepanciaService.resetItemsObjetoDiscrepancia();
                model.discrepanciaService.listDiscrepanciaProducto(d.getNro_discrepancia(),numpla,numrem, tipo_doc,documento,tipo_doc_rel,documento_rel, distrito );
                model.discrepanciaService.setItemsDiscrepancia( model.discrepanciaService.getDiscrepancias() );
                
                /*Hilo de correo*/
                SendMailManager hilo = new SendMailManager();
                hilo.start(usuario.getLogin(), model.discrepanciaService.getListaDiscrepancia());
                /*Fin hilo correo*/
                
                request.setAttribute("msg","Se agrego los datos exitosamente");
                next="/jsp/cumplidos/discrepancia/DiscrepanciaInsertar.jsp?c_numrem="+numrem+"&sw=true&exis=true&campos=true&c_numpla="+numpla+"&c_tipo_doc="+tipo_doc+"&c_documento="+documento+"&paso=false&nom_doc="+nom_doc+"&c_tipo_doc_rel="+tipo_doc_rel+"&c_documento_rel="+documento_rel;
            }
            
            else{
                model.remesaService.datosRemesa(numrem);
                Remesa rem = model.remesaService.getRemesa();
                model.productoService.serchProducto(cod_producto, distrito, rem.getCodcli());
                if( model.productoService.getProducto() != null && model.productoService.getProducto().getCliente().equals(rem.getCodcli()) ) {
                    Discrepancia dis = new Discrepancia();
                    dis.setNro_discrepancia(num_discre);
                    dis.setContacto( rem.getCodcli() );
                    dis.setNro_planilla(numpla);
                    dis.setGrupo(grupo);
                    dis.setNro_remesa(numrem);
                    dis.setTipo_doc(tipo_doc);
                    dis.setTipo_doc_rel(tipo_doc_rel);
                    dis.setDocumento_rel(documento_rel);
                    dis.setDocumento(documento);
                    dis.setCod_producto(cod_producto);
                    dis.setDescripcion(descripcion);
                    dis.setUnidad(unidad);
                    dis.setValor(valor);
                    dis.setUbicacion(ubicacion);
                    dis.setCantidad(cantidad);
                    dis.setCod_discrepancia(cod_discrepancia);
                    dis.setCausa_dev(causa);
                    dis.setResponsable(responsable);
                    dis.setNom_documento(nom_doc);
                    dis.setNom_destinatario(nom_des);
                    dis.setFecha_creacion( Util.fechaActualTIMESTAMP());
                    dis.setUsuario_modificacion(usuario.getLogin());
                    dis.setUsuario_creacion(usuario.getLogin());
                    dis.setBase(usuario.getBase());
                    dis.setDistrito(distrito);
                    model.discrepanciaService.addItemsDiscrepancia(dis);
                    //model.discrepanciaService.setDiscrepancia (dis);
                    request.setAttribute("msg", "Producto Ingresado");
                    
                    
                    if( dis.getCod_discrepancia().equals("RE") ){
                        /*Codigo Correo*/
                        if ("9|10|11|12|13|35|48|49|50|51|56|61|62|63|66".indexOf(dis.getCausa_dev()) != -1) {
                            model.discrepanciaService.addListaDiscrepancia(dis);
                        }
                    }
                    /*Fin Codigo Correo*/
                    
                    
                    next="/jsp/cumplidos/discrepancia/DiscrepanciaInsertar.jsp?c_numrem="+numrem+"&sw=true&exis=true&campos=true&c_numpla="+numpla+"&c_tipo_doc="+tipo_doc+"&c_documento="+documento+"&paso=false&nom_doc="+nom_doc+"&c_tipo_doc_rel="+tipo_doc_rel+"&c_documento_rel="+documento_rel;
                }
                else {
                    request.setAttribute("msg","No Existe El Producto");
                    next="/jsp/cumplidos/discrepancia/DiscrepanciaInsertar.jsp?c_numrem="+numrem+"&sw=true&exis=true&campos=true&c_numpla="+numpla+"&c_tipo_doc="+tipo_doc+"&c_documento="+documento+"&paso=false&nom_doc="+nom_doc+"&c_tipo_doc_rel="+tipo_doc_rel+"&c_documento_rel="+documento_rel;
                }
                
                
                
                model.planillaService.obtenerInformacionPlanilla2(numpla);
                model.planillaService.obtenerNombrePropietario2(numpla);
                model.planillaService.consultaPlanillaRemesa(numpla);
                model.remesaService.consultaRemesaDiscrepancia(numrem,numpla);
            }
        }catch (SQLException e) {
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
    
}
