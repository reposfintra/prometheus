/******************************************************************************
 * Nombre clase :                   CXP_DocAnularAction.java                  *
 * Descripcion :                    Clase que maneja los eventos              *
 *                                  relacionados con el programa de Anular    *
 *                                  un Documento de Cuentas por Pagar Action  *
 * Autor :                          Leonardo Parody                           *
 * Fecha Creado :                   19 de octubre de 2005, 04:15 PM           *
 * Modificado por:                  LREALES                                   *
 * Fecha Modificado:                17 de mayo de 2006, 08:15 AM              *
 * Version :                        1.0                                       *
 * Copyright :                      Fintravalores S.A.                   *
 *****************************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.threads.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import org.apache.log4j.*;

public class CXP_DocAnularAction  extends Action{
    
    Logger logger = Logger.getLogger(this.getClass());
    
    /** Creates a new instance of CXP_DocAnularAction */
    public CXP_DocAnularAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException{
        
        String next="";
        HttpSession session = request.getSession();
        Usuario usuario = ( Usuario )session.getAttribute( "Usuario" );
        String user = usuario.getLogin().toUpperCase();
        String distrito = usuario.getDstrct().toUpperCase();
        String flag = request.getParameter( "flag" );
        String titulo="";
        CXP_Doc cXP_Docu = new CXP_Doc();
        CXP_Doc cXP_Docu2 = new CXP_Doc();
        CXP_Doc cXP_Docu3 = new CXP_Doc();
        
        boolean sw = false;
        boolean sw2 = false;
        
        com.tsp.finanzas.contab.model.Model modFin = new com.tsp.finanzas.contab.model.Model(usuario.getBd());
        
        //String c_documento = ( request.getParameter( "c_documento" ) != null )?request.getParameter( "c_documento" ).toUpperCase() :"";
        String c_documento = ( request.getParameter( "c_documento" ) != null )?request.getParameter( "c_documento" ) :"";//AMATURANA
        c_documento = c_documento.replaceAll( "-_-", "#" );
        String tipo_documento = ( request.getParameter( "tipo_documento" ) != null )?request.getParameter( "tipo_documento" ).toUpperCase() :"";
        String c_proveedor = ( request.getParameter( "c_proveedor" ) != null )?request.getParameter( "c_proveedor" ).toUpperCase() :"";
        //String documento = ( request.getParameter( "documento" ) != null )?request.getParameter( "documento" ).toUpperCase() :"";
        String documento = ( request.getParameter( "documento" ) != null )?request.getParameter( "documento" ) :"";//AMATURANA
        String descripcion = ( request.getParameter( "descripcion" ) != null )?request.getParameter( "descripcion" ) :"";
        String r = ( request.getParameter( "r" ) != null )?request.getParameter( "r" ) :"";
        
        String validar = "";
        
        logger.info("?documento: " + c_documento);
        
        try{
            
            String nombre = model.cxpDocService.SQL_Nombre_Proveedor( distrito, c_proveedor );
            
            if ( flag.equals( "mostrar" ) ){
                
                sw = model.cxpDocService.ExisteCXP_Doc( distrito, c_proveedor, tipo_documento, c_documento );
                
                if ( sw == true ){
                    
                    cXP_Docu = model.cxpDocService.ConsultarCXP_Doc( distrito, c_proveedor, tipo_documento, c_documento );
                    
                    request.setAttribute( "CXP", cXP_Docu );
                    
                    if( !model.cxpDocService.existeEnPrecheque( cXP_Docu.getDstrct(), cXP_Docu.getProveedor(), cXP_Docu.getTipo_documento(), cXP_Docu.getDocumento()) ) {//Osvaldo
                        
                        if ( cXP_Docu.getCheque().equals( "" ) ){
                            
                            if ( cXP_Docu.getCorrida().equals( "" ) ){
                                
                                if ( cXP_Docu.getVlr_saldo() == 0 ){
                                    
                                    next = "/jsp/cxpagar/facturasxpagar/Mensaje.jsp?msg=La Factura no puede ser anulada! Por estar cancelada!!";
                                    
                                } else if ( ( cXP_Docu.getVlr_saldo() < cXP_Docu.getVlr_neto() ) && ( cXP_Docu.getVlr_saldo() != 0 ) ){
                                    
                                    next = "/jsp/cxpagar/facturasxpagar/Mensaje.jsp?msg=La Factura no puede ser anulada! Por ser abonada!!";
                                    
                                } else {
                                    if ( !cXP_Docu.getTipo_documento_rel().equals( "" ) && !cXP_Docu.getDocumento_relacionado().equals( "" ) ) {
                                        sw2 = model.cxpDocService.ExisteCXP_Doc( distrito, c_proveedor, cXP_Docu.getTipo_documento_rel(), cXP_Docu.getDocumento_relacionado() );
                                        if ( !sw2 ) {
                                            r = "false";
                                            next = "/jsp/cxpagar/facturasxpagar/anularfactura.jsp?flag=detalle";
                                        } else {
                                            r = "true";
                                            next = "/jsp/cxpagar/facturasxpagar/anularfactura.jsp?flag=detalle";
                                        }
                                    } else {
                                        r = "true";
                                        next = "/jsp/cxpagar/facturasxpagar/anularfactura.jsp?flag=detalle";
                                    }
                                }
                            } else {
                                next = "/jsp/cxpagar/facturasxpagar/Mensaje.jsp?msg=La Factura no puede ser anulada! Por encontrarse en una corrida!!";
                            }
                        } else {
                            next = "/jsp/cxpagar/facturasxpagar/Mensaje.jsp?msg=La Factura no puede ser anulada! Por poseer un cheque!!";
                        }
                    }else{
                        next = "/jsp/cxpagar/facturasxpagar/Mensaje.jsp?msg=La Factura no puede ser anulada por encontrarse en Cheque Pendiente por Imprimir";
                    }
                } else{                    
                    next = "/jsp/cxpagar/facturasxpagar/Mensaje.jsp?msg=La Factura Digitada NO EXISTE!!";                    
                }
                
            } else if ( flag.equals( "anular" ) ){
                 
                if ( r.equals( "true" ) ) {
                    
                    cXP_Docu = model.cxpDocService.ConsultarCXP_Doc( distrito, c_proveedor, tipo_documento, c_documento );
                    cXP_Docu2 = model.cxpDocService.ConsultarCXP_Doc( distrito, c_proveedor, cXP_Docu.getTipo_documento_rel(), cXP_Docu.getDocumento_relacionado() );
                    cXP_Docu.setUsuario_anulo(usuario.getLogin());
                    cXP_Docu2.setUsuario_anulo(usuario.getLogin());
 
                    //NOTA DEBITO
                     //NOTA DEBITO
                    if ( cXP_Docu.getTipo_documento().equals( "ND" ) ){
                        
                        cXP_Docu2.setVlr_total_abonos( cXP_Docu2.getVlr_total_abonos() + cXP_Docu.getVlr_neto() );
                        cXP_Docu2.setVlr_saldo( cXP_Docu2.getVlr_saldo() - cXP_Docu.getVlr_neto() );
                        
                        cXP_Docu2.setVlr_total_abonos_me( cXP_Docu2.getVlr_total_abonos_me()+ cXP_Docu.getVlr_neto_me() );
                        cXP_Docu2.setVlr_saldo_me( cXP_Docu2.getVlr_saldo_me() - cXP_Docu.getVlr_neto_me() );
                        
                        validar = "anular";
                        
                    }
                    //NOTA CREDITO
                    else if ( cXP_Docu.getTipo_documento().equals( "NC" ) ){
                        
                        cXP_Docu2.setVlr_total_abonos( cXP_Docu2.getVlr_total_abonos() - cXP_Docu.getVlr_neto() );
                        cXP_Docu2.setVlr_saldo( cXP_Docu2.getVlr_saldo() + cXP_Docu.getVlr_neto() );
                        
                        cXP_Docu2.setVlr_total_abonos_me( cXP_Docu2.getVlr_total_abonos_me() - cXP_Docu.getVlr_neto_me() );
                        cXP_Docu2.setVlr_saldo_me( cXP_Docu2.getVlr_saldo_me() + cXP_Docu.getVlr_neto_me() );
                        
                        validar = "anular";
                        
                    }
                    //FACTURA NORMAL
                    else{
                        
                        validar = "anular";
                        
                    }
                    
                } else if ( r.equals( "false" ) ) {
                    
                    validar = "anular";
                    
                } else{
                    
                    next = "/jsp/cxpagar/facturasxpagar/Mensaje.jsp?msg=Error en el proceso de anulacion de la factura!";
                    
                }
                
                if ( validar.equals( "anular" ) ){
                    
                    //************************* 30 junio ***************************
                    //LREALES
                    //cXP_Docu = model.cxpDocService.ConsultarCXP_Doc( distrito, c_proveedor, tipo_documento, c_documento );
                    //CLASE DE DOCUMENTO = 0
                    
                    
                    
                    logger.info("?anular op" + cXP_Docu.getClase_documento() + " : " + cXP_Docu.getClase_documento().equals("0"));
                    if ( cXP_Docu.getClase_documento().equals("0") ){
                        logger.info("?anular op: " + cXP_Docu.getDocumento());
                        model.cxpDocService.ConsultarCXP_Items_Doc( distrito, c_proveedor, tipo_documento, c_documento );
                        Vector vector_items = model.cxpDocService.getVector_items();
                        int cont_items = vector_items.size();
                        
                        for ( int ci = 0; ci < cont_items; ci++ ){
                            
                            Hashtable ht_items = ( Hashtable ) vector_items.get( ci );
                            
                            Vector vector_tblcon = new Vector();
                            
                            vector_tblcon.add( ht_items );
                            
                            model.cxpDocService.ConsultarTblCon( ht_items );
                            vector_tblcon = model.cxpDocService.getVector_tblcon();
                            int cont_tblcon = vector_tblcon.size();
                            
                            for ( int ct = 0; ct < cont_tblcon; ct++ ){
                                
                                Hashtable ht_tblcon = ( Hashtable ) vector_tblcon.get( ct );
                                
                                String tipocuenta_ht = ( String ) ht_tblcon.get( "tipocuenta_ht" );
                                
                                if ( tipocuenta_ht.equals("Q") ){
                                    
                                    model.cxpDocService.ConsultarMovPla( ht_items );
                                    Vector vector_movpla = model.cxpDocService.getVector_movpla();
                                    int cont_movpla = vector_movpla.size();
                                    
                                    for ( int cm = 0; cm < cont_movpla; cm++ ){
                                        
                                        Hashtable ht_movpla = ( Hashtable ) vector_movpla.get( cm );
                                        
                                        String concept_code_htm = ( String ) ht_movpla.get( "concept_code_ht" );
                                        String pla_owner_htm = ( String ) ht_movpla.get( "pla_owner_ht" );
                                        String planilla_htm = ( String ) ht_movpla.get( "planilla_ht" );
                                        String vlr_disc_htm = ( String ) ht_movpla.get( "vlr_disc_ht" );
                                        String vlr_htm = ( String ) ht_movpla.get( "vlr_ht" );
                                        String vlr_for_htm = ( String ) ht_movpla.get( "vlr_for_ht" );
                                        String user_update_htm = ( String ) ht_movpla.get( "user_update_ht" );
                                        String creation_date_htm = ( String ) ht_movpla.get( "creation_date_ht" );
                                        String fech_anul_htm = ( String ) ht_movpla.get( "fech_anul_ht" );
                                        String user_anul_htm = ( String ) ht_movpla.get( "user_anul_ht" );
                                        
                                        Vector vector_movpla2 = new Vector();
                                        
                                        vector_movpla2.add( ht_movpla );
                                        
                                        if ( fech_anul_htm.equals("") || fech_anul_htm.equals("0099-01-01 00:00:00") ){
                                            
                                            double vlr1 = Double.parseDouble( vlr_disc_htm );
                                            double vlr2 = Double.parseDouble( vlr_htm );
                                            double vlr3 = Double.parseDouble( vlr_for_htm );
                                            
                                            if ( ( vlr1 >= 0 ) && ( vlr2 >= 0 ) && ( vlr3 >= 0 ) ){
                                                logger.info("... valores positivos");
                                                double vlr_disc_1 = vlr1 * ( -1 );
                                                double vlr_2 = vlr2 * ( -1 );
                                                double vlr_for_3 = vlr3 * ( -1 );
                                                
                                                String vlr_disc_htd = "" + vlr_disc_1;
                                                String vlr_htd = "" + vlr_2;
                                                String vlr_for_htd = "" + vlr_for_3;
                                                
                                                model.cxpDocService.DuplicarMovPla( ht_movpla, vlr_disc_htd, vlr_htd, vlr_for_htd );
                                                
                                                user_update_htm = user;
                                                user_anul_htm = user;
                                                
                                                //model.cxpDocService.AnularMovPla( vlr_disc_htm, vlr_htm, vlr_for_htm, user_update_htm, user_anul_htm, concept_code_htm, planilla_htm, pla_owner_htm, creation_date_htm );
                                                //AMATURANA 24.04.2007
                                                model.cxpDocService.AnularMovPla( vlr_disc_htm, vlr_htm, vlr_for_htm, user, user, concept_code_htm, planilla_htm, pla_owner_htm, creation_date_htm );
                                            }
                                            
                                        }
                                        
                                    }
                                    
                                }
                                
                            }
                            
                        }
                        
                    }
                    
                    //**************************************************************
                    
                    cXP_Docu3.setTipo_documento( tipo_documento );
                    cXP_Docu3.setDocumento( c_documento );
                    cXP_Docu3.setProveedor( c_proveedor );
                    cXP_Docu3.setDstrct( distrito );
                    cXP_Docu3.setUser_update( user );
                    
                    // esto es para que busque la cxp.
                    cXP_Docu = model.cxpDocService.ConsultarCXP_Doc( distrito, c_proveedor, tipo_documento, c_documento );
                    
                    model.cxpDocService.AnularCXP_Doc( cXP_Docu3 );
                    model.cxpDocService.ActualizarSaldosDocumento(cXP_Docu2);
                    /* ANULAR FACTURA */
                    /*  */
                    boolean sw_cxp_items_doc = model.cxpDocService.ExisteCXP_Items_Doc( distrito, c_proveedor, tipo_documento, c_documento );
                    if ( sw_cxp_items_doc )
                        model.cxpDocService.AnularCXP_Items_Doc( cXP_Docu3 );
                    
                    boolean sw_cxp_imp_doc = model.cxpDocService.ExisteCXP_Imp_Doc( distrito, c_proveedor, tipo_documento, c_documento );
                    if ( sw_cxp_imp_doc )
                        model.cxpDocService.AnularCXP_Imp_Doc( cXP_Docu3 );
                    
                    boolean sw_cxp_imp_item = model.cxpDocService.ExisteCXP_Imp_Item( distrito, c_proveedor, tipo_documento, c_documento );
                    if ( sw_cxp_imp_item )
                        model.cxpDocService.AnularCXP_Imp_Item( cXP_Docu3 );
                    /*  */
                    next = "/jsp/cxpagar/facturasxpagar/Mensaje.jsp?msg=La Factura Fue Anulada Exitosamente!";
                    
                    TransaccionService tService = new TransaccionService(usuario.getBd());
                    ArrayList <String> query =null;
                    /*Descontabilizacion*/
                    logger.info("?fecha contab: " + cXP_Docu.getFecha_contabilizacion());
                    if( !cXP_Docu.getFecha_contabilizacion().equals("0099-01-01 00:00:00") ){
                        
                        int grupo_tran = 0;
                        query = modFin.comprobanteService.descontabilizar(distrito, cXP_Docu.getTipo_documento(), cXP_Docu.getDocumento(), cXP_Docu.getClase_documento(), cXP_Docu.getTransaccion(), usuario );
                        grupo_tran  =  modFin.comprobanteService.getGrupTrans();
                        logger.info(".. generacio reversion: " + query);
                        query.add(modFin.comprobanteService.descontabilizarFactura(usuario.getLogin(), grupo_tran, distrito, cXP_Docu.getProveedor(), cXP_Docu.getTipo_documento(), cXP_Docu.getDocumento()) );
                        
                    }//Fin de Descontabilizacion
                    
                    /*Verificacion OP*/
                    if( cXP_Docu.getClase_documento().equals("0") || cXP_Docu.getClase_documento().equals("1") ){
                        //Desmarcar movimientos planilla y sus movimientos
                        String [] vectorQuerys= modFin.comprobanteService.descontabilizacionOP( cXP_Docu.getDocumento(), cXP_Docu.getDocumento().replaceAll("OP", "") ).split(";");
                        for (String vectorQuery : vectorQuerys) {
                            query.add(vectorQuery);
                        }
                       
                    }//Fin validacion OP
          
                    if (query != null) {
                        logger.info("?sql descontab: " + query);
                        tService.crearStatement();
                        tService = getAddBatch(tService, query);
                        tService.execute();
                    }
                }
                
            }
            
            session.setAttribute( "nombre", nombre );
            session.setAttribute( "r", r );
            
        } catch ( Exception e ){
            e.printStackTrace();
            throw new ServletException( e.getMessage() );
            
        }
        
        this.dispatchRequest( next );
        
    }
    
}