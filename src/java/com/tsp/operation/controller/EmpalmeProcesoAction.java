/*
 * EmpalmeProcesoAction.java
 *
 * Created on 26 de junio de 2005, 12:38 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Sandrameg
 */
public class EmpalmeProcesoAction extends Action {
    
    /** Creates a new instance of EmpalmeProcesoAction */
    public EmpalmeProcesoAction() {
    }
    
    public void run() throws ServletException,InformationException {
       
        HttpSession session = request.getSession();
        Usuario u = (Usuario) session.getAttribute("Usuario");
        
        EmpalmeProcesosThread ept = new EmpalmeProcesosThread(u.getLogin(), u.getDstrct());
        ept.star();
                
        String msg = "Proceso iniciado";
        String next = "/pags_predo/procesos.jsp?msg=" + msg;        
        this.dispatchRequest(next);

    }   
}