/********************************************************************
 *      Nombre Clase.................   TraficoConfiguracionAction.java
 *      Descripci�n..................   Action que se encarga de generar una configuracion en el programa de control trafico
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.Util;
/**
 *
 * @author  David A
 */
public class TraficoConfiguracionAction extends Action{
    
    /** Creates a new instance of TraficoConfiguracionAction */
    public TraficoConfiguracionAction () {
    }
    
    public void run () throws ServletException, InformationException {
        try{
            String  []camposS = request.getParameterValues ("camposS");
            String user       = request.getParameter ("usuario");
            String clas       =""+ request.getParameter ("clas");
            String filtro     =""+request.getParameter ("filtro");
            String var1       =""+request.getParameter ("var1");
            String var2       =""+request.getParameter ("var2");
            String var3       =""+request.getParameter ("var3");
            String conf       =""+request.getParameter ("conf");
            String nombre     =""+request.getParameter ("nombre");
            String linea      =""+request.getParameter ("linea");
            String filtroP = "" + request.getParameter("filtroP");
            
            filtro=model.traficoService.decodificar (filtro);
            
            /*  Modificado por: Andr�s Maturana
             *  Fecha: 02.05.2006 */
            model.traficoService.gennerarVZonasTurno(user);
            //model.traficoService.generarVZonas (user);
            ////System.out.println ("Genrerar zonas ");
            Vector zonas = model.traficoService.obdtVectorZonas ();
            String validacion=" zona='-1'";
            
            if ((zonas == null)||zonas.size ()<=0){}else{
                validacion = model.traficoService.validacion (zonas);
            }
            ////System.out.println ("\nValidacion final: "+ validacion);
            
            if(var3.equals ("null") ){
                var3=model.traficoService.obtVar3 ();
            }
            if (clas.equals ("null")||clas.equals ("")){
                clas="";
            }
            if (filtro.equals ("null")||filtro.equals ("")){
                filtro = "";
                filtro= "where "+validacion+filtro;
            }
            else{
                filtro=filtro;
            }
            
            model.traficoService.listarColTrafico (var3);
            Vector col = model.traficoService.obtColTrafico ();
            model.traficoService.listarColtrafico2 (col);
            
            int n=1;
            String configuracion ="";
            
            if(camposS != null){
                Vector config = new Vector(); //Andr�s Maturaba D. 
                for(int i=0;i<=camposS.length-1;i++){
                    String campo   = camposS[i];
                    config.add(campo);//Andr�s Maturaba D. 
                    ////System.out.println ("Campo "+i+": "+campo);
                }
                configuracion = model.traficoService.getConfiguracion (camposS);
                model.traficoService.setConfigActual(config);//Andr�s Maturaba D. 
                
                configuracion =configuracion+" ,color_letra,CASE WHEN reg_status='D' THEN get_ultimaobservaciontrafico(planilla,'detencion') ELSE ult_observacion END AS obs,neintermedias ";
                linea="";
                for(int i=0;i<=camposS.length-1;i++){
                    linea  = linea+"-"+camposS[i];
                }
                
                ////System.out.println ("linea: "+ linea);
                linea="-tipo_despacho-planilla-placa-ult_observacion"+linea;//AMATURANA 11.08.2006
            }
            else {
                if (conf.equals ("")){
                    conf = model.traficoService.obtConf ();
                }
                ////System.out.println ("conf antes de eleiminar"+conf);
                conf = conf.replaceAll (nombre+",", "");
                ////System.out.println ("Nombre"+nombre+",");
                linea = linea.replaceAll (nombre+"-", "");
                ////System.out.println ("linea"+linea);
                ////System.out.println ("conf despues de eleiminar"+conf);
                configuracion =conf;
                n=2;
                var2=linea.replaceAll ("-planilla","");
                var2=linea.replaceAll ("-tipo_despacho","");
                var2=linea.replaceAll ("-placa","");
                var2=linea.replaceAll ("-ult_observacion","");//AMATURANA 11.08.006
                
                ////System.out.println ("var2"+var2);
                var1= model.traficoService.obtlinea ();
                ////System.out.println ("var1"+var1);
                Vector lin= new Vector ();
                lin = model.traficoService.obtenerCampos (var2);
                for (int i=0;i< lin.size ();i++ ){
                    var1= var1.replaceAll ("-"+lin.elementAt (i), "");
                }
                ////System.out.println ("var1 despues de reemplasar "+var1);
                var1= var1.replaceAll ("-planilla", "");
                var1= var1.replaceAll ("-tipo_despacho", "");
                var1= var1.replaceAll ("-placa", "");
                var1= var1.replaceAll ("-ult_observacion","");//AMATURANA 11.08.006
                linea = linea.replaceAll ("-"+nombre, "");
            }
            
            
            
            //configuracion=configuracion+",demora,cedcon,tipo_despacho,planilla,placa,fecha_ult_reporte,fecha_prox_reporte,cedcon,fecha_salida,cel_cond,oid,nomcond ";
            configuracion=configuracion+",demora,cedcon,tipo_despacho,planilla,placa,ult_observacion,fecha_ult_reporte,fecha_prox_reporte,cedcon,fecha_salida,cel_cond,oid,nomcond,neintermedias ";
            ////System.out.println ("cofiguracion: "+ configuracion);
            String consulta      = model.traficoService.getConsulta (configuracion,filtro,clas);
            ////System.out.println ("consulta: "+ consulta);
            
            
            
            Vector colum= new Vector ();
            colum = model.traficoService.obtenerCampos (linea);
            
            ////System.out.println ("linea="+linea+" y obtiene colmunas"+colum.size ());
            
            
            model.traficoService.listarCamposTrafico (linea);
            
            model.traficoService.generarTabla (consulta, colum);
            ////System.out.println ("genera tabla ");
            
            ConfTraficoU vista = new ConfTraficoU ();
            vista.setConfiguracion (configuracion.replaceAll("-_-","'"));
            vista.setFiltro (filtro);
            vista.setLinea (linea);
            vista.setClas (clas);
            vista.setVar1 (var1);
            vista.setVar2 (var2);
            vista.setVar3 (var3);
            vista.setCreation_user (user);
            model.traficoService.actualizarConfiguracion (vista);
            
            filtro=model.traficoService.codificar (filtro);
            String next ="";
            if (n==1){
                next= "/jsp/trafico/controltrafico/vertrafico2.jsp?var3="+var3+"&var1="+var1+"&var2="+var2+"&filtro="+filtro+"&linea="+linea+"&conf="+configuracion.replaceAll("'","-_-")+"&clas="+clas+"&reload="+"&filtroP="+filtroP;
            }
            else{
                next= "/jsp/trafico/controltrafico/vertrafico2.jsp?var3="+var3+"&var1="+var1+"&var2="+var2+"&filtro="+filtro+"&linea="+linea+"&conf="+configuracion+"&clas="+clas+"&filtroP="+filtroP;
            }
            
            ////System.out.println ("next "+next);
            RequestDispatcher rd = application.getRequestDispatcher (next);
            if(rd == null)
                throw new Exception ("No se pudo encontrar "+ next);
            rd.forward (request, response);
        }
        catch(Exception e){
            e.printStackTrace();
            throw new ServletException ("Accion:"+ e.getMessage ());
        }
    }
    
}
