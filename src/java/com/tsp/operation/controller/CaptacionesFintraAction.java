/***************************************
    * Nombre Clase ............. Captaciones.java
    * Descripci�n  .. . . . . .  Permite Liq. PP
    * Autor  . . . . . . . . . . JULIO BARROS RUEDA
    * Fecha . . . . . . . . . .  06/06/2007
    * versi�n . . . . . . . . .  1.0
    * Copyright ...Transportes Sanchez Polo S.A.
    ******************************************
 */

package com.tsp.operation.controller;


import java.io.*;
import java.util.*;
import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.threads.*;
import com.tsp.util.Util;
import com.tsp.operation.model.beans.Usuario;
import javax.servlet.http.*;



public class CaptacionesFintraAction extends Action{
    //2009-09-02
    
    private  boolean estadoProceso = true;
    Usuario usuario = null;
    
    public void run() throws ServletException, InformationException {
        try{
                HttpSession session  = request.getSession();
                String next          = "/jsp/captaciones/captacion.jsp";            
                usuario              = (Usuario) session.getAttribute("Usuario"); 
                String retorno       = "";
                
                String propietario   = request.getParameter("Nit");
                String opcion        = request.getParameter("opcion");
                String us            = request.getParameter("us")!=null ? request.getParameter("us"):"";
                
                //String evento        = request.getParameter("evento");
                
                if(propietario==null)
                    propietario="";
                
                
               if (opcion.equals("CargarDatos")){
                   //System.out.println("cargar datos"); 
                    Propietario     Propietario      = new Propietario();
                    next +="?vista=Operacion&Nit="+propietario;
                    model.CaptacionesFintraSvc.buscarPropietario(propietario); 
                    
                }
               
                else if( opcion.equals("REFRESCAR")){
                        String      accion = request.getParameter("vista");
                        String      valu        = request.getParameter("infoCTA");
                        String      distrito    = usuario.getDstrct();
                        String      prov        = model.AnticiposPagosTercerosSvc.getProveedorUser(usuario.getLogin());
                        
                        next          = "/jsp/captaciones/ListaPagosTerceros.jsp?vista=";
                        
                        String desBanco="";
                        String Ncuenta ="";
                        if ( valu != null ) {
                            String[] vec        =  valu.split("-");
                            desBanco   =  vec[1];
                            Ncuenta    =  vec[2];
                        }
                        if(Ncuenta.equals(""))
                            Ncuenta="69225845948";
                        model.AnticiposPagosTercerosSvc.searchListaCuentasTercero(prov);
                       // model.AnticiposPagosTercerosSvc.refrescarAnticipos_por_Transferir2(desBanco,Ncuenta);
                        List lista  =  model.AnticiposPagosTercerosSvc.getListPorTransferir();                        
                        request.setAttribute("listaAnticipos",  lista );                        
                        next     += "TRANSFERIR";    
                    }
                else if(opcion.equals("VistaPreviaCaptaciones"))
                {

                    
                    String nit           = request.getParameter("parametro");
                    String fechai        = request.getParameter("fechai");
                    String fechaf        = request.getParameter("fechaf");
                    model.CaptacionesFintraSvc.buscarPropietario(nit);
                    model.CaptacionesFintraSvc.MovimientosProveedorCaptaciones(nit,fechai,fechaf);
                    Vector datos         = model.CaptacionesFintraSvc.getCaptaciones();
                    if (datos!=null && !datos.isEmpty()){

                    if(us.equals("SI"))
                    {
                       next = "/jsp/captaciones/ListaCaptacionesPropietario_us.jsp?msg=no se encontraron datos para las restricciones definidas...";
                    }
                    else
                    {
                     next = "/jsp/captaciones/ListaCaptacionesPropietario.jsp?msg=no se encontraron datos para las restricciones definidas...";
                    }

                      
                    }else{
                        next +="?msg=No se encontraron datos para el provedor "+nit+" entre las fechas establecidas";
                    }                  
                    
                }
                else if(opcion.equals("VistaCaptacionesMesAMes")){
                    next = "/jsp/captaciones/consultaMes_a_Mes.jsp";
                    String fechai        = request.getParameter("fechai");
                    String fechaf        = request.getParameter("fechaf");
                    int mes1 = Integer.parseInt(fechai.substring(5,7));
                    int mes2 = Integer.parseInt(fechaf.substring(5,7));
                    int total = mes2-mes1;
                    model.CaptacionesFintraSvc.ProveedoresCaptacionesMesAMes(fechai,fechaf);
                    Vector datos         = model.CaptacionesFintraSvc.getCaptacionesM();
                    if (datos!=null && !datos.isEmpty()){
                        System.out.println("si encontro ");
                        next = "/jsp/captaciones/ListaMes_A_MesPropietario.jsp?fechai="+mes1+"&mesesT="+total+"&msg=no se encontraron datos para las restricciones definidas...";
                    }else{
                        next +="?msg=No se encontraron datos para proveedores entre las fechas establecidas";
                    }                  
                    
                }
                else if(opcion.equals("EXCEL")){
                    next = "/jsp/captaciones/ConsultarCaptaciones.jsp";

                    //Usuario usuario            = (Usuario) session.getAttribute("Usuario");
                    //usuario.getCedula();
                    /*
                    String nit           = request.getParameter("parametro");
                    String fechai        = request.getParameter("fechai");
                    String fechaf        = request.getParameter("fechaf");
                    model.CaptacionesFintraSvc.buscarPropietario(nit);
                    model.CaptacionesFintraSvc.MovimientosProveedorCaptaciones(nit,fechai,fechaf);*/
                    Vector datos         = model.CaptacionesFintraSvc.getCaptaciones();
                    if (datos!=null && !datos.isEmpty()){
                        
                        HExportarExel_Reporte_Captaciones_Prop  hilo =  new  HExportarExel_Reporte_Captaciones_Prop();
                        hilo.init(model,usuario,us);

                        next = "/jsp/captaciones/ListaCaptacionesPropietario.jsp?msg=Se esta generando el Archivo";
                   if(us.equals("S"))
                   {
                    HExportarExel_Reporte_Captaciones_Prop  hilo_us =  new  HExportarExel_Reporte_Captaciones_Prop();
                    hilo_us.init(model,usuario,us);
                    next = "/jsp/captaciones/ListaCaptacionesPropietario_us.jsp?msg=Se esta generando el Archivo";
                   }
                    }
                    else
                    {
                        next +="?msg=No se encontraron datos para el provedor "; 
                    }                  
                    
                }
                else if(opcion.equals("EjecutarOperacion")){
                    
                    //System.out.println("ejecutar en inicio");
                    Propietario     Propietario      = new Propietario();
                    Propietario = model.CaptacionesFintraSvc.getPropietario();
                    String operacion  = request.getParameter("tDocumento");
                    String fechai     = request.getParameter("fechainicio");
                    String ClaseM     = request.getParameter("cMovimiento");
                    double Monto      = Math.round(Double.parseDouble(request.getParameter("vlr")));
                    String Nmovi      = request.getParameter("nMovimiento");
                    double NInteres   = Double.parseDouble(request.getParameter("PorInt"));      
                    String Nfrecuencia= request.getParameter("frecu");
                    String Ndocumento = request.getParameter("nodocumento");
                    //String Banco      = request.getParameter("");
                    //String Sucursal   = request.getParameter("");
                    retorno  = Realizar_Operacion(Propietario,operacion,fechai,ClaseM,Monto,Nmovi,NInteres,Nfrecuencia,Ndocumento,usuario.getLogin(),usuario.getBase()); 
                    next +="?retorno="+retorno;
                    
                }// Transferencia:    
                    if( opcion.equals("BUSCARAPROBADAS") ){
                        //System.out.println("buscar probadas");
                        String      valu        = request.getParameter("infoCTA");
                        String      distrito    = usuario.getDstrct();
                        String      prov        = model.AnticiposPagosTercerosSvc.getProveedorUser(usuario.getLogin());
                        //System.out.println("prov  "+prov);

                        //System.out.println("transferencia");
                        next          = "/jsp/captaciones/ListaPagosTerceros.jsp?vista="; 
                        String msj    = "";
                        String   Ncuenta="";
                        if ( valu != null ) {
                            String[] vec        =  valu.split("-");
                            String   desBanco   =  vec[1];
                                     Ncuenta    =  vec[2];
                            ////System.out.println(" bam   "+desBanco);
                            ////System.out.println("Ncuenta  "+Ncuenta);
//                            model.AnticiposPagosTercerosSvc.getAnticipos_por_Transferir_C(distrito, prov,desBanco,Ncuenta);
                            next +="TRANSFERIR&Ncuenta="+Ncuenta;  
                        }else{
                            if(Ncuenta.equals(""))
                                Ncuenta="69225845948";
                            ////System.out.println(" Los nulos   ");
                            model.AnticiposPagosTercerosSvc.searchListaCuentasTercero(prov);
//                            model.AnticiposPagosTercerosSvc.getAnticipos_por_Transferir_C(distrito, prov,"BANCOLOMBIA",Ncuenta);
                            next +="TRANSFERIR";  
                        }
                        //////System.out.println(" Los null   ");
                        List lista  =  model.AnticiposPagosTercerosSvc.getListPorTransferir();                        
                        request.setAttribute("listaAnticipos",  lista );                        
                        if( lista.size()==0 )
                            msj = "No hay registros para ser tranferidos...";
                    
                        next +="&msj="+ msj;
                    
                    }
                
                
                    if( opcion.equals("LISTABANCOS") ){  
                        
                        String      valu        = request.getParameter("infoCTA");
                        String      distrito    = usuario.getDstrct();
                        String      prov        = model.AnticiposPagosTercerosSvc.getProveedorUser(usuario.getLogin());
                        String propietarioc     =  request.getParameter("nit");
                        //System.out.println("propietarioc  "+propietarioc);
                        String desBanco="";
                        if( valu == null ){
                            desBanco="BANCOLOMBIA";
                        }
                        if ( valu != null ) {
                            desBanco   =  valu;
                        }    
                        String    secuencia = request.getParameter("secue");
                        String id           =  request.getParameter("anticipo"); 
//                        List   cuentas      =  model.AnticiposPagosTercerosSvc.getListaCuentas2(propietarioc, "ALL");
                        String nombre       =  model.AnticiposPagosTercerosSvc.getNameNIT(propietarioc);
                        String global       =  request.getParameter("global");
                        
//                        request.setAttribute("listaBancos",  cuentas );
                        next    = "/jsp/captaciones/ListaBancosPropietario.jsp?nit="+ propietarioc +"&anticipo="+ id +"&nombre="+ nombre +"&global="+ global +"&secu="+ secuencia +"&infoCTA="+desBanco+"&vista=";//
                    }
                
                
                
                     else if( opcion.equals("LISTABANCOS_TRANSACCION") ){

                        String desBanco     = "";
                        String Ncuenta      = request.getParameter("Ncuenta");
                        String secuencia    = request.getParameter("secue");
                        String id           = request.getParameter("anticipo");
                        String propietarioc =  request.getParameter("nit");
                        
                        //List   cuentas    = model.AnticiposPagosTercerosSvc.getListaCuentas(propietario, "ALL");                        
                        String nombre       = model.AnticiposPagosTercerosSvc.getNameNIT(propietarioc);
                        String global       = request.getParameter("global");
                        //request.setAttribute("listaBancos",  cuentas );
                        next    = "/jsp/captaciones/ListaBancosTransaccion.jsp?nit="+ propietarioc +"&anticipo="+ id +"&nombre="+ nombre +"&global="+ global +"&secu="+ secuencia +"&infoCTA="+desBanco+"&vista=";//
                    }
                
                    else if( opcion.equals("ASIGNARBANCO") ){
                        TreeMap t = new TreeMap();
                        String      valu        = request.getParameter("infoCTA");
                        String secuencia    =   request.getParameter("secue");
                        String propietarioc  =   request.getParameter("nit");
                        String secCta       =   request.getParameter("sec");//secuemcia de la cuenta 
                        String global       =   request.getParameter("global");
                        String Bank         =   request.getParameter("desBanco");
                        String  Ncuenta     =   request.getParameter("Ncuenta");
                        
                        String id           = request.getParameter("anticipo");
                        
                        ////System.out.println("Ncuenta  Asig "+Ncuenta);
                        int centinela       =   0;
                        next    = "/jsp/captaciones/ListaPagosTerceros.jsp?nit="+ propietarioc +"&anticipo="+ id +"&global="+ global +"&infoCTA="+valu+"&vista=";//
                        //  para que la informacion se cargue por beneficiario globalmente o individual julio barros 14-11-2006 //
                        if(global.equals("S") || (!secuencia.equals("0") && !secuencia.equals("") && secuencia!=null ) )  {
			    List lista  =  model.AnticiposPagosTercerosSvc.getListPorTransferir();
                             ArrayList<String> listaQuerys =new ArrayList<>();
                            for(int i=0; i<lista.size(); i++){
                                AnticiposTerceros anticipo = (AnticiposTerceros) lista.get(i);
                                if(global.equals("S")){
                                    if( anticipo.getPla_owner().equals(propietarioc) ){
                                         if( t.get(""+anticipo.getId())==null || t.get(""+anticipo.getId()).equals("") ){
                                                String[] listQ=model.AnticiposPagosTercerosSvc.asignarCTA(propietarioc, secCta, ""+anticipo.getId(), Bank ).split(";");
                                                listaQuerys.addAll(Arrays.asList(listQ));
                                            t.put(""+anticipo.getId(), anticipo.getPlanilla());
                                            i--;
                                            if ( centinela != 0 )
//                                                model.AnticiposPagosTercerosSvc.aplicarValores4(propietarioc, ""+anticipo.getId());
                                            centinela++;
                                         }
                                     }
                                }else{
                                    //System.out.println("a la que no es global");
                                    if( (""+anticipo.getSecuencia()).equals(secuencia) ){
                                         if( t.get(""+anticipo.getId())==null || t.get(""+anticipo.getId()).equals("") ){
                                                String[] listQ=model.AnticiposPagosTercerosSvc.asignarCTA(propietarioc, secCta, ""+anticipo.getId(),Bank ).split(";");
                                                listaQuerys.addAll(Arrays.asList(listQ));
                                            t.put(""+anticipo.getId(), anticipo.getPlanilla());
                                            i--;
                                            if ( centinela != 0 )
//                                                model.AnticiposPagosTercerosSvc.aplicarValores4(propietarioc, ""+anticipo.getId());
                                            centinela++;
                                         }
                                     }
                                }
                            }
                            
                               //vamos aplicar valores a todo en una sola conexion.
                                TransaccionService tservice = new TransaccionService(usuario.getBd());
                                tservice.crearStatement();
                                tservice = getAddBatch(tservice, listaQuerys);
                                tservice.execute();
                                tservice.closeAll();
                                
//                            model.AnticiposPagosTercerosSvc.refrescarAnticipos_por_Transferir2("DOS",Ncuenta);
                            lista  =  model.AnticiposPagosTercerosSvc.getListPorTransferir();
                            request.setAttribute("listaAnticipos",  lista );
                            next       += "TRANSFERIR&desBanco="+Bank;                            
                        }else{
                            ////System.out.println("entro al else");
                            ////System.out.println("propietario   "+propietarioc+" secCta  "+ secCta+" id  "+id+"  Bank  "+ Bank);
//                            model.AnticiposPagosTercerosSvc.asignarCTA2(propietarioc, secCta, id, Bank);
//                            model.AnticiposPagosTercerosSvc.refrescarAnticipos_por_Transferir2(Bank,Ncuenta);
                            List lista  =  model.AnticiposPagosTercerosSvc.getListPorTransferir();                        
                            request.setAttribute("listaAnticipos",  lista );                        
                            next       += "TRANSFERIR";
                        }
                    }
                
                    else if( opcion.equals("ASIGNARBANCO_TRANSACCION") ){
                        
                        next          = "/jsp/captaciones/ListaPagosTerceros.jsp?vista="; 
                        
                        String      valu        = request.getParameter("infoCTA");
                        String      distrito    = usuario.getDstrct();
                        String      prov        = model.AnticiposPagosTercerosSvc.getProveedorUser(usuario.getLogin());
                        String propietarioc     =  request.getParameter("nit");
                        //////System.out.println("arranco");
                        String Banco        ="";
                        String sql          ="";
                        String   Ncuenta    ="";
                        if ( valu != null ) {
                            String[] vec        =  valu.split("-");
                            Banco               =  vec[1];
                                     Ncuenta    =  vec[2];
                        }
                        
                        model.tService.crearStatement2();
                        String secuencia    = request.getParameter("secue");
                        String global       = request.getParameter("global");
                        String anticipo_I   =  request.getParameter("anticipo");
                        
                        TreeMap t           = new TreeMap();
                        int centinela       =   0;
                        List lista2         =new LinkedList();
                        List lista  =  model.AnticiposPagosTercerosSvc.getListPorTransferir();
                        //if(global.equals("S") || (!secuencia.equals("0") && !secuencia.equals("") && secuencia!=null ) )  {
                            for(int i=0; i<lista.size(); i++){
                                AnticiposTerceros anticipo = (AnticiposTerceros) lista.get(i);
                                    if( (""+anticipo.getSecuencia()).equals(secuencia) ){
                                            //////System.out.println(i+"  Secuencia = "+secuencia+" nit = "+propietario+" Banco = "+Banco+" anticipo.getId() = "+anticipo.getId()); 
                                            if ( centinela != 0 ){
//                                                model.AnticiposPagosTercerosSvc.aplicarValores4(propietarioc, ""+anticipo.getId());
                                            }else{
//                                                model.AnticiposPagosTercerosSvc.aplicarValores3(propietarioc,""+anticipo.getId(),Banco);
                                                
                                            }
                                            if ( valu != null ) {
                                                String[] vec        =  valu.split("-");
                                                anticipo.setBanco_transferencia(vec[0]);
                                                anticipo.setCuenta_transferencia(vec[2]);
                                                anticipo.setTcta_transferencia(vec[3]);
                                                
                                            }
                                            t.put(""+anticipo.getId(), anticipo.getPlanilla());                                           
                                            centinela++;
                                            lista2.add(anticipo);
                                   }else{
                                       if( (""+anticipo.getId()).equals(anticipo_I) ){
//                                            model.AnticiposPagosTercerosSvc.aplicarValores3(propietario,anticipo_I,Banco);
                                            if ( valu != null ) {
                                                String[] vec        =  valu.split("-");
                                                anticipo.setBanco_transferencia(vec[0]);
                                                anticipo.setCuenta_transferencia(vec[2]);
                                                anticipo.setTcta_transferencia(vec[3]);
                                                
                                            }
                                            t.put(anticipo_I, anticipo.getPlanilla());                                           
                                            centinela++;
                                            lista2.add(anticipo);
                                       }
                                   }
                             }
//                             sql=model.AnticiposPagosTercerosSvc.cuenta_a_transferir2(lista2);
                             //////System.out.println("sql nuevo==> "+sql);
                             model.tService.getSt().addBatch(sql);
                        /*}else{
                            //////System.out.println(" y en el else que");
                        }*/
                        try  {
                            model.tService.execute2();
                        } catch (Exception e) {
                            e.printStackTrace();
                            throw new Exception(e.getMessage());
                        } finally{
                            model.tService.closeAll2();
                        }
                        ////System.out.println("Ncuenta   "+Ncuenta);
//                        model.AnticiposPagosTercerosSvc.refrescarAnticipos_por_Transferir2("DOS",Ncuenta);
                        lista  =  model.AnticiposPagosTercerosSvc.getListPorTransferir();
                        request.setAttribute("listaAnticipos",  lista );
                        next       += "TRANSFERIR&desBanco="+Banco+"&Ncuenta="+Ncuenta;
                        
                    }
                    
                    else if( opcion.equals("ASIGNARDESCUENTO") ){  
                        
                        next          = "/jsp/captaciones/ListaPagosTerceros.jsp?vista=";
                        
                        String      valu        = request.getParameter("infoCTA");
                        String      distrito    = usuario.getDstrct();
                        String      prov        = model.AnticiposPagosTercerosSvc.getProveedorUser(usuario.getLogin());
                        String propietarioc     =  request.getParameter("nit");
                        
                        String anticipo     =  request.getParameter("anticipo");
                        double valor        =  Double.parseDouble( request.getParameter("valor") );
                        String Ncuenta      =  request.getParameter("Ncuenta");
                        
                        String desBanco     =  "";
                        if( valu == null ){
                            desBanco="BANCOLOMBIA";
                        }
                        
                        if ( valu != null ) {
                            desBanco   =  valu;
                        }   
                        
//                        model.AnticiposPagosTercerosSvc.asignarDescuento2(propietarioc,anticipo, valor,desBanco);
//                        model.AnticiposPagosTercerosSvc.refrescarAnticipos_por_Transferir2(desBanco,Ncuenta);
                        
                        List lista  =  model.AnticiposPagosTercerosSvc.getListPorTransferir();                        
                        request.setAttribute("listaAnticipos",  lista );                        
                        next       += "TRANSFERIR";
                    }
                
                else if( opcion.equals("TRANSFERIR") ){
                    
                        //System.out.println("llego a transferir");
                        next          = "/jsp/captaciones/ListaPagosTerceros.jsp?vista=";
                        String msj    = "";
                        
                        String      prov        = model.AnticiposPagosTercerosSvc.getProveedorUser(usuario.getLogin());
                        
                        String[] anticipos  =  request.getParameterValues("anticipo");
                        String   infoBanco  =  request.getParameter("infoCTA");
                        String[] vec        =  infoBanco.split("-");
                        String   banco      =  vec[0];
                        String   desBanco   =  vec[1];
                        String   cta        =  vec[2];
                        String   tipoCta    =  vec[3];  
                        
                        List     seleccion  =  model.AnticiposPagosTercerosSvc.getSeleccion( anticipos , "TR");                        
                        String   name       =  model.AnticiposPagosTercerosSvc.getNameProveedor();
                        
                        msj                 = "El archivo para transferir al banco "+  desBanco +" est� siendo generado....";
                        
                        if( seleccion.size()>0){
                            HArchivosTransferenciaBancos  hilo =  new  HArchivosTransferenciaBancos();
                            hilo.start(model, usuario, anticipos,  seleccion, prov, name, banco,desBanco,  cta, tipoCta );
                        }
                        else
                            msj             = "No hay registros para realizar transferencias";
                        
                        List lista          =  model.AnticiposPagosTercerosSvc.getListPorTransferir();
                        request.setAttribute("listaAnticipos",  lista );
                        next              += "TRANSFERIR"+"&Ncuenta="+cta;
                        
                        if( lista.size()==0 )
                            msj += "No hay mas registros para ser transferidos";
                        
                        
                        next +="&msj="+ msj;
                        
                    }
                
                //System.out.println("se paso ****************************************************");
                RequestDispatcher rd = application.getRequestDispatcher(next);
                if(rd == null)
                    throw new Exception("No se pudo encontrar "+ next);
                //System.out.println(""+retorno);
                rd.forward(request, response); 
        } catch (Exception e){
             e.printStackTrace();
             throw new ServletException(e.getMessage());
        }
    }
    
    /**
     * Funcion para calcular todos los datos de una nueva captacion
     * @autor jbarros
     * @param name, nombre del parametro
     * @param opcion, valor opcional a devolver en caso de que nom exista
     * @return Parametro del request
     */
    private String Realizar_Operacion(Propietario propietario,String operacion,String fechai,String ClaseM,double Monto,String Nmovi,double NInteres,String Nfrecuencia,String Ndocumento,String usuario,String base) throws Exception{
        ////System.out.println("realizar operacion");
        estadoProceso = true;
        model.tService.crearStatement2();
        String retorno       = "";
        
        try{
 
            String sql01 ="";
            String sql03 ="";
            double vlr_interes =0;
            double retefuente =0;
            double rete_ica =0;
            Captacion captacion=new Captacion();
            String fecha_corte = "";
            int    dias_tras = 0;
            double vlr_Tcapital = 0 ;
            
            String fecha_A   = propietario.getInicio();
            double capital_A = propietario.getCapital();
            String tipodoc_A = propietario.getTipo_doc();
            String docu_A    = propietario.getDocumento();
            
            if ( capital_A == 0 ){//Cliente nuevo o saldo en Cero
                if(tipodoc_A == null){
                    //por ser nuevo coloco un dia antes del inicio para que la resta de 0
                    propietario.setInicio(com.tsp.util.Util.fechaMenosUnDia(fechai));//fecha_A 
                    //propietario.setInicio(fechai);//fecha_A
                    //propietario.setFrecuencia_capital("");//FrecuenciA
                    propietario.setTipo_doc("");//tipodoc_A
                    propietario.setDocumento("");//docu_A
                }   
            }
            
            ////// Calculos de fechas y valores //////
            
            fecha_corte = com.tsp.util.Util.fechaMenosUnDia(fechai);
            //System.out.println("las dos fechas  "+fecha_corte+" 00:00:00   -  "+fecha_A);
            
            dias_tras   = com.tsp.util.Util.diasTranscurridos(fecha_A,fecha_corte+" 00:00:00") + 1 ;
	   
            //System.out.println("mand  "+dias_tras);
            if(dias_tras < 0)dias_tras = 0;
            //System.out.println("paso operacion  "+dias_tras+"   I   "+propietario.getTasa_capital());
            vlr_interes = Math.round(((double)dias_tras/30.0)*((double)propietario.getTasa_capital()/100.0)*(double)propietario.getCapital());
            //System.out.println("valor intereses   "+vlr_interes);

            if(!propietario.getPerfil().equals("INVERSIONISTA US"))
            retefuente  = Math.round((7.0/100.0)*vlr_interes);


            if(!propietario.getPerfil().equals("INVERSIONISTA US"))
           // rete_ica  = Math.round((0.5/100.0)*vlr_interes);
            rete_ica  = 0;

            vlr_Tcapital= Math.round(propietario.getCapital()+vlr_interes);
            
            /////////////////////////////////////////
            
            ////System.out.println("   "+operacion+"   "+fechai+"   "+ClaseM+"   "+Monto+"   "+Nmovi);
            
            captacion.setDstrct("FINV");
            captacion.setProveedor             (propietario.getCedula());
            captacion.setTipo_operacion        ("M");        // si  es M o B
            captacion.setTipo_documento        (operacion);  // si  es Captacion o Reembolso
            captacion.setDocumento             (Ndocumento);      // en numero de la transaccion o docuemnto interno
            captacion.setFecha_documento       ("now()");
            captacion.setFecha_inicio          (fechai);     // la fecha de inicio de la operacion
            captacion.setVlr_capital           (Monto);      // el valor de la transaccion
            captacion.setMoneda                ("PES");
            captacion.setInteres               (NInteres);
            captacion.setFrecuencia            (Nfrecuencia);
            captacion.setClase_ingreso         (ClaseM);     // se es transferencia , cheque, efectivo, etc
            captacion.setRef1                  (Nmovi);      //elnumero del cheque, tramasferencia etc
            captacion.setRef2                  ("");
            captacion.setRef3                  ("");
            captacion.setFecha_liquidacion     ("now()");
            captacion.setFecha_corte           (fecha_corte);
            captacion.setVlr_intereses         (vlr_interes);
            captacion.setVlr_retefuente        (retefuente);
            captacion.setVlr_reteica           (rete_ica);
            captacion.setVlr_total_capital     (vlr_Tcapital);
            captacion.setDocumento_previo      ("");
            captacion.setTipo_documento_previo ("");
            
            
            captacion.setCreation_user(usuario);
            captacion.setBase(base);
            
            //System.out.println("a calcular el nuevo valor");
            
            
            
            
            if( operacion.equals("01") || operacion.equals("04") ){
                //System.out.println("captacion");
                // calculo de valor del nuevo capital
                double vlr_Ncapital = 0;
                vlr_Ncapital = propietario.getCapital()+vlr_interes-retefuente-rete_ica+Monto;
                captacion.setVlr_nuevo_capital(vlr_Ncapital);
                sql01  = Operacion_Captacion(captacion);
                if(operacion.equals("01"))
                    sql01 +="UPDATE series SET last_number = last_number +"+1+", last_update = now(), user_update = '"+usuario+"' WHERE document_type = 'CAPTACION' AND reg_status != 'A' AND last_number <= serial_fished_no;";
                
                if(operacion.equals("04"))
                    sql01 +="UPDATE series SET last_number = last_number +"+1+", last_update = now(), user_update = '"+usuario+"' WHERE document_type = 'FINDEMES' AND reg_status != 'A' AND last_number <= serial_fished_no;";
                sql01 += Operacion_Base(captacion,usuario,base,fechai);
                //System.out.println("sql01 "+sql01);
                model.tService.getSt().addBatch(sql01);
                
            }else if(operacion.equals("03")){
                //System.out.println("reembolso");
                // calculo de valor del nuevo capital
                double vlr_Ncapital = 0;
                vlr_Ncapital = propietario.getCapital()+vlr_interes-retefuente-rete_ica-Monto;
                captacion.setVlr_nuevo_capital(vlr_Ncapital);
                sql03 = Operacion_Reembolso(captacion);
                sql03+="UPDATE series SET last_number = last_number +"+1+", last_update = now(), user_update = '"+usuario+"' WHERE document_type = 'REEMBOLSO' AND reg_status != 'A' AND last_number <= serial_fished_no;";
                sql03+= Operacion_Base(captacion,usuario,base,fechai);
                sql03+= model.CaptacionesFintraSvc.InsertAnticipo(captacion);
                model.tService.getSt().addBatch(sql03);
                //System.out.println("sql013 "+sql03);
                
            }else{
                estadoProceso = false;
            }
            if (estadoProceso){
                ////System.out.println("RRRUNNNNNNNNNNNNN");
                model.tService.execute2();
            }
            }catch(Exception ex){
                ex.printStackTrace();
                estadoProceso = false;
                throw new Exception("ERROR DURANTE LA INSERCCION DE LA CAPTACION" + ex.getMessage());
            }finally{
                model.tService.closeAll2();
            }   
            if( estadoProceso ){
                retorno = "save|Captacion grabada en la base de datos|";
            }else if (!estadoProceso ){
                retorno = "error|no puede realizar el proceso, la Operacion '"+operacion+"' no existe";
            }
        return retorno;
    }
    
    
    
    
    
    /**
     * Funcion para calcular todos los datos de una nueva captacion
     * @autor jbarros
     * @param name, nombre del parametro
     * @param opcion, valor opcional a devolver en caso de que nom exista
     * @return Parametro del request
     */
    private String Operacion_Captacion(Captacion captacion) throws Exception{
        String sql  = "";
        sql = model.CaptacionesFintraSvc.Registrar_Operacion(captacion);
        return sql;
    }
        
    
    /**
     * Funcion para calcular todos los datos de una nueva captacion
     * @autor jbarros
     * @param name, nombre del parametro
     * @param opcion, valor opcional a devolver en caso de que nom exista
     * @return Parametro del request
     */
    private String Operacion_Reembolso(Captacion captacion) throws Exception{
        String sql  = "";
        sql = model.CaptacionesFintraSvc.Registrar_Operacion(captacion);
        return sql;
    }
    
    
    /**
     * Funcion para calcular todos los datos de una nueva captacion
     * @autor jbarros
     * @param name, nombre del parametro
     * @param opcion, valor opcional a devolver en caso de que nom exista
     * @return Parametro del request
     */
    private String Operacion_Base(Captacion captacion,String usuario,String Base,String fechai) throws Exception{
        String sql  = "";
        String base = "";
        sql +="update captaciones set tipo_operacion = 'M' where proveedor='"+captacion.getProveedor()+"' and tipo_operacion = 'B' and dstrct = 'FINV';";
        base = ""+model.CaptacionesFintraSvc.ConsecutivoCaptaciones("BASE");
        captacion.setDocumento_previo      (captacion.getDocumento());
        captacion.setTipo_documento_previo (captacion.getTipo_documento());
        captacion.setTipo_operacion        ("B");        // si  es M o B
        captacion.setTipo_documento        ("02");  // si  es Captacion o Reembolso
        captacion.setDocumento             (base);      // en numero de la transaccion o docuemnto interno
        captacion.setFecha_documento       ("now()");
        captacion.setFecha_inicio          (fechai);     // la fecha de inicio de la operacion
        captacion.setVlr_capital           (captacion.getVlr_nuevo_capital()); // el valor de la transaccion
        captacion.setMoneda                ("PES");
        //captacion.setInteres(NInteres);
        //captacion.setFrecuencia(Nfrecuencia);
        captacion.setClase_ingreso         ("");     // se es transferencia , cheque, efectivo, etc
        captacion.setRef1                  ("");      //elnumero del cheque, tramasferencia etc
        captacion.setRef2                  ("");
        captacion.setRef3                  ("");
        captacion.setFecha_liquidacion     ("0099-01-01 00:00:00");
        captacion.setFecha_corte           ("0099-01-01 00:00:00");
        captacion.setVlr_intereses         (0);
        captacion.setVlr_retefuente        (0);
        captacion.setVlr_reteica           (0);
        captacion.setVlr_total_capital     (0);
        sql += model.CaptacionesFintraSvc.Registrar_Operacion(captacion); 
        sql +="UPDATE series SET last_number = last_number +"+1+", last_update = now(), user_update = '"+usuario+"' WHERE document_type = 'BASE' AND reg_status != 'A' AND last_number <= serial_fished_no;";
        return sql;
    }
     
}