/******
 * Nombre:        Jerarquia_tiempoAnularAction.java              
 * Descripci�n:   Clase Action para anular jerarquia tiempo   
 * Autor:         Ing. Jose de la Rosa 
 * Fecha:         26 de junio de 2005, 04:26 PM                               
 * Versi�n        1.0                                       
 * Coyright:      Transportes Sanchez Polo S.A.            
 *******/
 


package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;


public class Jerarquia_tiempoAnularAction extends Action{
    
    /** Creates a new instance of Jerarquia_tiempoAnularAction */
    public Jerarquia_tiempoAnularAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/jsp/trafico/mensaje/MsgAnulado.jsp";
        HttpSession session = request.getSession();       
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String actividad = request.getParameter("c_actividad");
        String responsable = request.getParameter("c_responsable");
        String demora = request.getParameter("c_demora");
        try{
            if(model.jerarquia_tiempoService.existJerarquia_tiempo(actividad,responsable,demora)){
                Jerarquia_tiempo jerarquia_tiempo = new Jerarquia_tiempo();
                jerarquia_tiempo.setActividad(actividad);
                jerarquia_tiempo.setResponsable(responsable);
                jerarquia_tiempo.setDemora(demora);
                jerarquia_tiempo.setUser_update(usuario.getLogin().toUpperCase());
                jerarquia_tiempo.setCia(((String)session.getAttribute("Distrito")).toUpperCase());
                jerarquia_tiempo.setBase(usuario.getBase());
                model.jerarquia_tiempoService.anularJerarquia_tiempo(jerarquia_tiempo);
                request.setAttribute("mensaje","MsgAnulado");                
            }
            else{
                request.setAttribute("mensaje","MsgErrorIng");
            }
            model.jerarquia_tiempoService.serchJerarquia_tiempo(actividad,responsable,demora);
            Jerarquia_tiempo jerarquia_tiempo2 = model.jerarquia_tiempoService.getJerarquia_tiempo();
            request.setAttribute("jerarquia_tiempo",jerarquia_tiempo2);
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
    
}
