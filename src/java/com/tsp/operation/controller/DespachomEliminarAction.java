 /*
 * DespachomEliminarAction.java
 *
 * Created on 22 de noviembre de 2005, 03:18 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  dlamadrid
 */
public class DespachomEliminarAction extends Action
{
    
    /** Creates a new instance of DespachomEliminarAction */
    public DespachomEliminarAction ()
    {
    }
    
    public void run () throws ServletException, InformationException
    {
        String next="";
        try
        {
            String d=""+request.getParameter ("d");
            String p=""+request.getParameter ("p");
            String f=""+request.getParameter ("f");
            String np= (""+request.getParameter ("np"));
            
            model.despachoManualService.eliminarDespacho (d, p, f,np);
            model.despachoManualService.listaDespacho ();
            next="/jsp/trafico/despacho_manual/despacho.jsp?";
        }
        catch (Exception e)
        {
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
        
    }
}
