/*
 * InsertarZonaAction.java
 *
 * Created on 13 de junio de 2005, 11:00 AM
 */
package com.tsp.operation.controller;
/**
 *
 * @author  Henry
 */
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.Model;

public class InsertarZonaUsuarioAction extends Action{
    
    /** Creates a new instance of InsertarZonaAction */
    public InsertarZonaUsuarioAction() {
    }
    
    public void run() throws ServletException {
        String codigo = (request.getParameter("codigo").toUpperCase());
        String nombre = (request.getParameter("nombre"));
        String next = "/jsp/trafico/zona/zonaUsuario.jsp?mensaje=";        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
        try{ 
            if (model.zonaUsuarioService.existeZonaUsuario(codigo,nombre))
                next = "/jsp/trafico/zona/zonaUsuario.jsp?mensaje=ErrorA";
            else if (!model.zonaUsuarioService.existeZona(codigo))
                next = "/jsp/trafico/zona/zonaUsuario.jsp?mensaje=ErrorB";
            else if (!model.zonaUsuarioService.existeUsuario(nombre))
                next = "/jsp/trafico/zona/zonaUsuario.jsp?mensaje=ErrorC";
            else{
                ZonaUsuario zona = new ZonaUsuario();
                zona.setCodZona(codigo);
                zona.setNomUsuario(nombre);                
                zona.setBase(usuario.getBase());
                zona.setCreation_user(usuario.getLogin());
                model.zonaUsuarioService.insertarZonaUsuario(zona);
                next = next+"Agregado";
            }
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
