/**
 *
 */
package com.tsp.operation.controller;

import java.io.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;

/**
 * Searches the database for puchase orders matching the
 * purchase search argument
 */
public class CaravanaPlanViajeSearchAction extends Action {
  static Logger logger = Logger.getLogger(PlanViajeSearchAction.class);
  
  /**
   * Executes the action
   */
  public void run() throws ServletException, InformationException {
    String cmd = request.getParameter("cmd");
    String next = null;
    PlanViaje planViaje = null;
    PlanViaje planVjm = null;
    if(cmd == null){
      next = "/caravana/PantallaCaravanaPlanilla.jsp";
    }
    else{
      HttpSession session = request.getSession();
      
      // Perform search
      String [] args = new String[2];
      
      args[0] = request.getParameter("cia");
      args[1] = request.getParameter("planilla");
      
      if (args[1] != null)
        args[1] = args[1].trim().toUpperCase();
      else
        args[1] = "";
      
      try {
            Usuario usuario = (Usuario)session.getAttribute("Usuario");
            PlanViaje planVj = (PlanViaje)session.getAttribute("planVj");
            if (model.planViajeService.planillaExist(args[0], args[1])){
                if (model.planViajeService.planViajeExist(args[0], args[1])){
                   request.setAttribute("mensaje", "� EL PLAN DE VIAJE YA FUE CREADO !");
                   next = "/caravana/PantallaCaravanaPlanilla.jsp";
                }
                else{
                    model.planViajeService.getPlanVjInfoNew(args[0], args[1]);
                    planViaje = model.planViajeService.getPlanViaje();

                    planVj.setPlanilla(planViaje.getPlanilla());
                    planVj.setPlaca(planViaje.getPlaca());
                    planVj.setContenedor(planViaje.getContenedor());
                    planVj.setTrailer(planViaje.getTrailer());
                    planVj.setProducto(planViaje.getProducto());
                    planVj.setCodtipocarga(planViaje.getCodtipocarga());
                    planVj.setTipocarga(planViaje.getTipocarga());
                    planVj.setDestinatario(planViaje.getDestinatario());
                    planVj.setRuta(planViaje.getRuta());

                    planVj.setCedcon(planViaje.getCedcon());
                    planVj.setNomcon(planViaje.getNomcon());
                    planVj.setDircon(planViaje.getDircon());
                    planVj.setPhonecon(planViaje.getPhonecon());
                    planVj.setCiucon(planViaje.getCiucon());

                    planVj.setNitpro(planViaje.getNitpro());
                    planVj.setNompro(planViaje.getNompro());
                    planVj.setDirpro(planViaje.getDirpro());
                    planVj.setPhonepro(planViaje.getPhonepro());
                    planVj.setCiupro(planViaje.getCiupro());

                    model.viaService.getViasRelPg(planVj.getRuta());
                    //VERIICA QUE LA VIA EXISTA EN CASO CONTRARIO DISPARA UN EXCEPCION
                    if (model.viaService.getCbxVias().size() == 0){
                        logger.info("[ER0000] LA VIA '"+planVj.getRuta()+"' ASOCIADA A LA PLANILLA '"+args[1]+"' NO HA SIDO CREADA");
                        throw new InformationException("[ER0000] LA VIA '"+planVj.getRuta()+"' ASOCIADA A LA PLANILLA '"+args[1]+"' NO HA SIDO CREADA");           
                    }
                    session.setAttribute("planVj", planVj);
                    logger.info(usuario.getNombre() + " ejecuto planViaje con: Planilla " + args[1]);
                    next = "/caravana/PantallaCaravanaInfo.jsp";
                }
            }
            else{
                 request.setAttribute("mensaje", "� LA PLANILLA DIGITADA NO HA SIDO CREADA !");
                 next = "/caravana/PantallaCaravanaPlanilla.jsp";
            }
      }
      catch (SQLException e){
        throw new ServletException(e.getMessage());
      }
    }
    // Redireccionar a la p�gina indicada.
    this.dispatchRequest(next);
  }
}