/*
 * ReportePlanillaXLS.java
 *
 * Created on 7 de octubre de 2005, 11:34 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.threads.*;

/**
 *
 * @author  Jose
 */
public class Reporte_planillaXlsAction extends Action{
    
    /** Creates a new instance of ReportePlanillaXLS */
    public Reporte_planillaXlsAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        String next="";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        try{
            com.tsp.operation.model.threads.ReporteExportarPlanillaXLS hilo = new com.tsp.operation.model.threads.ReporteExportarPlanillaXLS();
            hilo.start(usuario.getLogin());
            next = "/datosplanilla/ReportePlanilla.jsp?msg=Su reporte ha iniciado y se encuentra en el log de procesos";
            next = Util.LLamarVentana(next, "Consulta Reporte Planilla");            
        }catch (Exception ex){
            throw new ServletException("Error en OpcionesExportarPtoAction .....\n"+ex.getMessage());
        }

        this.dispatchRequest(next);  
    }
    
}
