/*
 * MenuPlanillaImpresion.java
 *
 * Created on 17 de diciembre de 2004, 8:12
 */

package com.tsp.operation.controller;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Administrador
 */
public class MenuPlanillaImpresionAction extends Action{
    
    /** Creates a new instance of MenuPlanillaImpresion */
    public MenuPlanillaImpresionAction() {
    }
    
    public void run() throws javax.servlet.ServletException, InformationException {
        try
        {   
            
            HttpSession session = request.getSession();
            Usuario     usuario = (Usuario) session.getAttribute("Usuario");
            String      Login   = usuario.getLogin();
            String      Agencia = usuario.getId_agencia();
                  
            //String Agencia = "A01";
            model.PlanillaImpresionSvc.ReiniciarCIA();
            model.PlanillaImpresionSvc.ReiniciarPlanillas();
            model.PlanillaImpresionSvc.BuscarDatosCIA();
            model.PlanillaImpresionSvc.BuscarPlaNoImp(Agencia,usuario.getBase(),usuario.getLogin());
            final String next = "/impresion/planillasnoimp.jsp";
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
        }
        catch(Exception e)
        {
            throw new ServletException(e.getMessage());
        }
    }
    
}
