/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.JsonObject;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.services.NegocioTrazabilidadService;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author egonzalez
 */
public class EmailSenderService implements Runnable{
    
    private final String host;
    private final String port;
    private final String user;
    private final String password;  
    String proceso, Mymensaje,  emailTo, asunto, usuario;
    NegocioTrazabilidadService trazaS;    
   
    public EmailSenderService() {
        this.host ="smtp.mailgun.org";//"smtpout.secureserver.net";
        this.port = "25";
        this.user="no_responder@fintra.co";//"egonzalez@fintra.co";
        this.password= "5H63e749Uos605H";       
      
    }

    public EmailSenderService(NegocioTrazabilidadService traza, String proceso, String Mymensaje, String emailTo, String asunto, String usuario) {      
        this.trazaS = traza;
        JsonObject jobj = this.trazaS.getInfoCuentaEnvioCorreo();
        this.host = (jobj.get("servidor")!= null) ? jobj.get("servidor").getAsString():"";
        this.port = (jobj.get("puerto")!= null) ? jobj.get("puerto").getAsString():"";
        this.user=(jobj.get("usuario")!= null) ? jobj.get("usuario").getAsString():"";
        this.password= (jobj.get("clave")!= null) ? jobj.get("clave").getAsString():""; 
        this.proceso = proceso;
        this.Mymensaje = (jobj.get("bodyMessage")!= null) ? jobj.get("bodyMessage").getAsString().replace("bodyMessage", Mymensaje):"";
        this.emailTo = emailTo;
        this.asunto = asunto;
        this.usuario = usuario;
    }
    
    public EmailSenderService(JsonObject jobj, String proceso, String Mymensaje, String emailTo, String asunto, Usuario usuario) {
        this.trazaS = new NegocioTrazabilidadService(usuario.getBd());
        this.host = (jobj.get("servidor")!= null) ? jobj.get("servidor").getAsString():"";
        this.port = (jobj.get("puerto")!= null) ? jobj.get("puerto").getAsString():"";
        this.user=(jobj.get("usuario")!= null) ? jobj.get("usuario").getAsString():"";
        this.password= (jobj.get("clave")!= null) ? jobj.get("clave").getAsString():""; 
        this.proceso = proceso;
        this.Mymensaje = (jobj.get("bodyMessage")!= null) ? jobj.get("bodyMessage").getAsString().replace("bodyMessage", Mymensaje):"";       
        this.emailTo = emailTo;
        this.asunto = asunto;
        this.usuario = usuario.getLogin();
    }    

    @Override
    public void run() {
        try {
            sendEmail(this.Mymensaje, this.emailTo, this.asunto);         
            Thread.sleep(20000);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }catch(Exception e){
           e.printStackTrace();
           //insertar en una tabla                
           this.trazaS.insertErrorCorreoStandBy(this.proceso, "Error enviando correo con asunto: " +this.asunto+ " a los siguientes destinatarios: " + this.emailTo , e.getMessage(), this.usuario);
           
        }
    }

    
    
    /**
     * Envia un email para el codigo de activacion. Lista de email separados por coma(,)
     * @param Mymensaje
     * @param emailTo
     * @param asunto
     * @return
     */
    public void sendEmail(String Mymensaje, String emailTo, String asunto)throws Exception{


        Properties props = new Properties();
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "25");
        props.put("mail.smtp.starttls.enable", "false");
        props.put("mail.transport.protocol", "smtp");

         Session session=Session.getInstance(props);
        //Compose the message
        try {            
            //Cuerpo del correo 
            
            MimeMessage message = new MimeMessage(session);               
            
            message.setFrom(new InternetAddress(user));
            message.setRecipients(Message.RecipientType.TO, this.getInternetAddresses(emailTo));
            message.setSubject(asunto);
            message.setText(this.Mymensaje,"ISO-8859-1","html");

            //send the message
            Transport t = session.getTransport();
            t.connect(props.getProperty("mail.smtp.host"), user, password);
            t.sendMessage(message, message.getAllRecipients());
            t.close();
            
        } catch (MessagingException e) {
            e.printStackTrace();
            throw new Exception("Error enviando email" + e.getMessage());
        }finally{        
            session=null;
        }
    }    
    
    private InternetAddress[] getInternetAddresses(String recipient ) throws AddressException{
        String[] recipientList = recipient.split(",");
        InternetAddress[] recipientAddress = new InternetAddress[recipientList.length];
        int counter = 0;

        for (String recipient1 : recipientList) {
            recipientAddress[counter] = new InternetAddress(recipient1.trim());
            counter++;
        }
        
        return recipientAddress;
    }
    
}
