/********************************************************************
 *      Nombre Clase.................   TraficoRAction.java
 *      Descripci�n..................   Action para Actualizar las Accion en control trafico
 *                                      Actualiza las zonas que puede ver el operador
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.11.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  dlamadrid
 */
public class TraficoRAction extends Action {
    
    /** Creates a new instance of TraficoRAction */
    public TraficoRAction() {
    }
    static Logger logger = Logger.getLogger(TraficoRAction.class);
    
    public void run() throws ServletException, InformationException {
        
        try{
            
            String user  = request.getParameter("usuario");
            String zona  = request.getParameter("zonasUsuario");
            String validacion =" zona='"+zona+"'";
            
            logger.info("zonas "+validacion );
            int sw=0;
            
            if(zona==null){
                sw=1;
            }
            if(zona!=null){
                if(zona.equals("")){
                    sw=1;
                }
            }
            if(sw==1){
                model.traficoService.obtenerZonasPorUsuario(user);
                model.traficoService.gennerarVZonasTurno(user);
                
                //model.traficoService.obtenerZonasPorUsuario (user);
                //model.traficoService.generarVZonas (user);
                logger.info("Genrerar zonas ");
                Vector zonas = model.traficoService.obdtVectorZonas();
                validacion=" zona='-1'";
                logger.info("genera zonas ");
                if ((zonas != null)||zonas.size()>0){
                    validacion = model.traficoService.validacion(zonas);
                }
                if ((zonas == null)||zonas.size()<=0){}else{
                    validacion = model.traficoService.validacion(zonas);
                }
            }
            
            
            java.util.Date utilDate = new java.util.Date(); //fecha actual
            long lnMilisegundos = utilDate.getTime();
            java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(lnMilisegundos);
            
            model.traficoService.actualizarZonas(validacion, sqlTimestamp);
            
            String clas          =""+ request.getParameter("clas");
            String filtro        =""+request.getParameter("filtro");
            String configuracion =""+request.getParameter("conf");
            String var1          =""+request.getParameter("var1");
            String var2          =""+request.getParameter("var2");
            String var3          =""+request.getParameter("var3");
            String linea         =""+request.getParameter("linea");
            String tiempo        =""+request.getParameter("tiempor");
            
            Vector v = new Vector();
            v = model.traficoService.vAtributos();
            
            model.traficoService.listarColTrafico(var3);
            Vector col = model.traficoService.obtColTrafico();
            model.traficoService.listarColtrafico2(col);
            col= model.traficoService.obtColTrafico();
            
            filtro=model.traficoService.decodificar(filtro);
            
            logger.info("\nValidacion final: "+ validacion);
            if (clas.equals("null")||clas.equals(""))
                clas="";
            if (configuracion.equals("null")||configuracion.equals(""))
                configuracion = model.traficoService.obtConf();
            
            if (filtro.equals("null")||filtro.equals("")){
                filtro = "";
                filtro= "where "+validacion+filtro;
            }
            else
                filtro=filtro;
            
            String consulta      = model.traficoService.getConsulta(configuracion,filtro,clas);
            logger.info("consulta: "+ consulta);
            
            
            Vector colum= new Vector();
            
            colum = model.traficoService.obtenerCampos(linea);
            
            model.traficoService.generarTabla(consulta, colum);
            logger.info("genera tabla ");
            filtro=model.traficoService.codificar(filtro);
            logger.info("entra en next ");
            String next = "/jsp/trafico/controltrafico/vertrafico2.jsp?var1="+var1+"&var2="+var2+"&var3="+var3+"&filtro="+filtro+"&linea="+linea+"&conf="+configuracion+"&clas="+clas+"&tiempo="+tiempo;
            
            logger.info("sale del next ");
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new Exception("No se pudo encontrar "+ next);
            rd.forward(request, response);
        }
        catch(Exception e){
            throw new ServletException("Accion:"+ e.getMessage());
        }
        
    }
    
}
