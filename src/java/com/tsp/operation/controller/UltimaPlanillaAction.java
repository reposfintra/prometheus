/*
 * UltimaPlanillaAction.java
 *
 * Created on 12 de agosto de 2005, 08:52 PM
 */

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;

/**
 *
 * @author  Andr�s
 */
public class UltimaPlanillaAction extends Action{
    
    /** Creates a new instance of UltimaPlanillaAction */
    public UltimaPlanillaAction() {
        
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        //Pr�xima vista
        String next = "/jsp/trafico/demora/asignarDemoraPlanillaParam.jsp";
        String plaveh = request.getParameter("plaveh");
        
        try{
            String numpla = model.demorasSvc.ultimaPlanillaVehiculo(plaveh);
            next += "?numpla=" + numpla;
            if(numpla.length()==0)
                next = "/jsp/trafico/demora/asignarDemoraPlaca.jsp?msg="
                    + "No se encuentran planillas asociadas al numero de placa: " + plaveh; 
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
