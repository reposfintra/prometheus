/*
 * MenuConsultaArchivoAction.java
 *
 * Created on 11 de octubre de 2004, 04:50 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.sql.SQLException;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  MARIO FONTALVO
 */
public class MenuReporteRegistroTiempoAction extends Action {
    
    /** Creates a new instance of MenuConsultaArchivoAction */
    public MenuReporteRegistroTiempoAction() {
    }               
     public void run() throws ServletException, InformationException 
    { 
        try
        {   
            model.RegistroTiempoSvc.ReiniciarListaPlanillas();
            final String next = "/registrotiempo/reporteregistrotiempo.jsp";
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
        }
        catch(Exception e)
        {
            throw new ServletException(e.getMessage());
        }
     }
    
}
