/*
* Nombre        DevolucionesSearchAction.java
* Descripci�n   Ejecuta las acciones para elaborar el reporte de devoluciones
* Autor         Ivan Herazo
* Fecha         13 de abril de 2004, 11:56 AM
* Versi�n       1.0
* Coyright      Transportes Sanchez Polo S.A.
*/

package com.tsp.operation.controller;

import java.io.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.threads.*;
import java.util.*;

/**
 * Ejecuta las acciones para elaborar el reporte de devoluciones
 * @author  iherazo
 */
public class DevolucionesSearchDiscrepAction extends Action {
    
  public void run() throws ServletException, InformationException
  {
    String next = null;
    String cmd = request.getParameter("cmd");
    if( cmd == null )
      next = "/jsp/sot/body/PantallaFiltroDevolucionesDiscrep.jsp";
    else{
      try
      {
        HttpSession session = request.getSession();
        Usuario userLoggedIn = (Usuario) session.getAttribute("Usuario");
        if( userLoggedIn == null )
            throw new SessionExpiredException();
        String [] searchParams = new String[7];
        searchParams[0] = request.getParameter("cliente");
        
        searchParams[1] = request.getParameter("fechaini");
        searchParams[2] = request.getParameter("fechafin");
        searchParams[3] = request.getParameter("tipoDevolucion");
        searchParams[4] = request.getParameter("nroPedido");
        String aux = request.getParameter("todoOrigen");
        searchParams[5] = aux != null && aux.equals("false")?request.getParameter("origen"):"ALL";
        aux = request.getParameter("todoDestino");
        searchParams[6] = aux != null && aux.equals("false")?request.getParameter("destino"):"ALL";
        //System.out.println("todo destino: "+request.getParameter("todoDestino"));
        String view = request.getParameter("displayInExcel");
        next        = "/jsp/sot/reports/ReporteDevolucionesDiscrep.jsp";
         
        if(view!=null && !view.equals("File"))        
           model.reporteDevolucionesDiscrepService.buscarDevoluciones(userLoggedIn, searchParams);
        else{
             model.reporteDevolucionesDiscrepService.buscarDevoluciones(userLoggedIn, searchParams);
            //  next+="?displayInExcel=true";
           // Capturamos parametros
            Hashtable header = new Hashtable();
            header.clear();            
            String nroPedido      = request.getParameter("nroPedido");
            String fechaIni       = request.getParameter("fechaini");
            String tipoDevolucion = request.getParameter("tipoDevolucion");            
            if( nroPedido.equals("") ){
                header.put("cliente", request.getParameter("nombreCliente"));              
               if( !fechaIni.equals("") ){
                    header.put("fechaini", request.getParameter("fechaini"));
                    header.put("fechafin", request.getParameter("fechafin"));      
               }              
               if( tipoDevolucion.equals("ALL") )          header.put("tipoDevolucion", "Todos" );
               else if( tipoDevolucion.equals("OPEN") )    header.put("tipoDevolucion", "Abiertos" );
               else if( tipoDevolucion.equals("CLOSED") )  header.put("tipoDevolucion", "Cerrados" );
            }else
              header.put("nroPedido", nroPedido);

           HExportarExcelDevolucionesDiscrep   hilo = new HExportarExcelDevolucionesDiscrep();
           hilo.start(model,userLoggedIn, header, searchParams);
           next="/jsp/sot/body/PantallaFiltroDevolucionesDiscrep.jsp?comentario=Su proceso de Exportaci�n de Devoluciones se ha iniciado...Puede consultar en Manejo de Archivos";
        }
       
      }catch(Exception SQLE){
          SQLE.printStackTrace();
        throw new ServletException(SQLE.getMessage());
      }
    }
    
    this.dispatchRequest( next );
  }
}
