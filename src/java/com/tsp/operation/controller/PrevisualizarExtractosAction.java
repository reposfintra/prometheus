/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.PrevisualzarExtractosDAO;
import com.tsp.operation.model.DAOS.impl.PrevisualzarExtractosImpl;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.POIWrite;
import com.tsp.operation.model.beans.PrevisualizarExtractos;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.Utility;
import com.tsp.util.cron.CausacionInteresMC;
import com.tsp.util.cron.FacturasCAT;
import java.io.File;
import java.lang.reflect.Type;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.ResourceBundle;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.util.HSSFColor;
import com.tsp.util.cron.CausacionCuotaManejo;

/**
 *
 * @author lcanchila
 */
public class PrevisualizarExtractosAction extends Action {

    private final int CARGAR_PREVISUALIZACION_EXTRACTO_MICRO = 2;
    private final int CARGAR_PREVISUALIZACION_EXTRACTO_FENALCO = 3;
    private final int GENERAR_ARCHIVO_EXTRACTO = 4;
    private final int GENERAR_EXTRACTO = 5;
    private final int INCLUIR_REGISTROS = 6;
    private final int LIMPIAR_REGISTROS = 7;
    private final int EXPORTAR_PREVISUALIZACION = 8;
    private final int GENERAR_MI_CAT = 9;
    private final int TOMAR_FOTO_CICLO = 10;
    private final int COPIAR_SANCIONES_MES = 11;
    private final int BUSCAR_SANCIONES_MES = 12;
    private final int EDITAR_SANCIONES_MES = 13;
    private final int GENERAR_SANCIONES_MES = 14;
    private final int BUSCAR_CONDONACIONES_MES = 15;
    private final int TOMA_FOTO_MES = 16;
    private final int GENERAR_CUOTA_MANEJO = 17;
    private final int GENERAR_CAT_MI_MES = 18;

    Usuario usuario = null;
    private String txtResp;
    private String tipo;
    private PrevisualzarExtractosDAO dao;
    POIWrite xls;
    private int fila = 0;
    HSSFCellStyle header, titulo1, titulo2, titulo3, titulo4, titulo5, letra, numero, dinero, dinero2, numeroCentrado, porcentaje, letraCentrada, numeroNegrita, ftofecha;
    private SimpleDateFormat fmt;
    String rutaInformes;
    String nombre;

    @Override
    public void run() throws ServletException, InformationException {
        try {
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            tipo = "application/json";
            this.txtResp = "";
            dao = new PrevisualzarExtractosImpl(usuario.getBd());

            int opcion = (request.getParameter("opcion") != null)
                    ? Integer.parseInt(request.getParameter("opcion"))
                    : -1;
            switch (opcion) {
                case CARGAR_PREVISUALIZACION_EXTRACTO_MICRO:
                    this.cargaPrevisualizacionExtractoMicro();
                    break;
                case CARGAR_PREVISUALIZACION_EXTRACTO_FENALCO:
                    this.cargaPrevisualizacionExtractoFenalco();
                    break;
                case GENERAR_ARCHIVO_EXTRACTO:
                    this.generarArchivoExtracto();
                    break;
                case GENERAR_EXTRACTO:
                    this.generarExtracto();
                    break;
                case INCLUIR_REGISTROS:
                    this.incluirRegistros();
                    break;
                case LIMPIAR_REGISTROS:
                    this.limpiarRegistros();
                    break;
                case EXPORTAR_PREVISUALIZACION:
                    this.exportarPrevisualizacion();
                    break;
                case GENERAR_MI_CAT:
                    this.generarMi_Cat();
                    break;
                case TOMAR_FOTO_CICLO:
                    this.tomar_foto_ciclo();
                    break;
                case COPIAR_SANCIONES_MES:
                    this.copiar_sanciones_mes();
                    break;
                case BUSCAR_SANCIONES_MES:
                    this.buscar_sanciones_mes();
                    break;
                case EDITAR_SANCIONES_MES:
                    this.editar_sanciones_mes();
                    break;
                case GENERAR_SANCIONES_MES:
                    this.generar_sanciones_mes();
                    break;
                case BUSCAR_CONDONACIONES_MES:
                    this.buscar_condonaciones_mes();
                    break;
                case TOMA_FOTO_MES:
                    tomar_foto_mes();
                    break;
                case GENERAR_CUOTA_MANEJO:
                    this.generarCuotaManejo();
                    break;
                case GENERAR_CAT_MI_MES:
                    this.generarMi_CatMes();
                    break;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void responser() {
        try {
            response.setContentType(this.tipo + "; charset=utf-8");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println(txtResp);
        } catch (Exception e) {
        }
    }

    public void printlnResponse(String respuesta, String contentType) throws Exception {
        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);
    }

    private void cargaPrevisualizacionExtractoMicro() throws Exception {
        String periodo = request.getParameter("periodo");
        String unegocio = request.getParameter("uneg");
        String vencMayor = request.getParameter("vencMayor");
        String ciclo = request.getParameter("ciclo");
        String fechaHoy = request.getParameter("fechaHoy");
        String[] vencimientos = vencMayor.split(",");
        ArrayList<PrevisualizarExtractos> lista = new ArrayList<PrevisualizarExtractos>();
        lista = dao.previsualizarExtractoMicro(periodo, "Visualizar", usuario.getLogin(), unegocio, vencimientos, ciclo, fechaHoy);

        Gson gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
        this.printlnResponse(json, "application/json;");
    }

    private void cargaPrevisualizacionExtractoFenalco() throws Exception {
        String periodo = request.getParameter("periodo");
        String unegocio = request.getParameter("uneg");
        String ciclo = request.getParameter("ciclo");
        String vencMayor = request.getParameter("vencMayor");
        String fechaHoy = request.getParameter("fechaHoy");
        String[] listUnd = unegocio.split(",");
        String[] vencimientos = vencMayor.split(",");
        ArrayList<PrevisualizarExtractos> lista = new ArrayList<PrevisualizarExtractos>();
        for (int i = 0; i < listUnd.length; i++) {
            lista.addAll(dao.previsualizarExtractoFenalco(periodo, listUnd[i], "Visualizar", usuario.getLogin(), vencimientos, ciclo, fechaHoy));
        }
        Gson gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
        this.printlnResponse(json, "application/json;");
    }

    private void generarArchivoExtracto() throws SQLException {
        String periodo = request.getParameter("periodo");
        String unegocio = request.getParameter("uneg");
        String ciclo = request.getParameter("ciclo");
        String vencMayor = request.getParameter("vencMayor");
        String[] vencimientos = vencMayor.split(",");
        String[] listUnd = unegocio.split(",");
        ArrayList<PrevisualizarExtractos> lista = new ArrayList<PrevisualizarExtractos>();
        boolean generado = false;
        String url = "";
        String resp1 = "";
        try {
            this.generarRUTA();
            this.crearLibro("GeneracionExtracto_", "GENERACION EXTRACTO");
            if (listUnd[0].equals("1")) {
                String[] cabecera = {
                    "id", "cod_rop", "cod_rop_barcode", "generado_el", "vencimiento_rop", "negocio", "cedula", "nombre_cliente", "direccion", "departamento", "ciudad", "barrio", "agencia", "linea_producto", "cuotas_vencidas",
                    "cuotas_pendientes", "dias_vencidos", "fch_ultimo_pago", "subtotal_rop", "total_sanciones", "total_descuentos", "total_rop", "total_abonos", "observacion", "msg_paguese_antes", "msg_estado_credito",
                    "capital", "interes_financiacion", "cat", "interes_xmora", "gastos_cobranza", "dscto_capital", "dscto_interes_financiacion", "dscto_interes_xmora", "dscto_gastos_cobranza", "dscto_cat", "ksubtotal_corriente", 
                    "ksubtotal_vencido", "ksubtotalneto", "kdescuentos", "ktotal", "telefono","email","extracto_email","periodo_generacion_fact","periodo_facturacion","ciclo_facturacion","periodo_desembolso"
                };
                short[] dimensiones = new short[]{
                    3000, 4000, 4000, 4000, 4000, 4000, 5000, 9000, 9000, 5000, 5000, 5000, 6000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 10000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000
                };
                this.generaTitulos(cabecera, dimensiones);
                lista = dao.exportarExtractoMicro(periodo, listUnd[0], vencMayor, vencimientos, ciclo);

                generado = generaArchivoExtractoMicro(lista);
            } else {
                for (int i = 0; i < listUnd.length; i++) {
                    lista.addAll(dao.exportarExtractoFenalco(periodo, listUnd[i], vencMayor, vencimientos, ciclo));
                }
                String[] cabecera = {
                    "id", "cod_rop", "cod_rop_barcode", "generado_el", "vencimiento_rop", "negocio", "cedula", "nombre_cliente", "direccion", "departamento", "ciudad", "barrio", "agencia", "linea_producto", "cuotas_vencidas",
                    "cuotas_pendientes", "dias_vencidos", "fch_ultimo_pago", "subtotal_rop", "total_sanciones", "total_descuentos", "total_rop", "total_abonos", "observacion", "msg_paguese_antes", "msg_estado_credito",
                    "capital", "interes_financiacion", "seguro", "interes_xmora", "gastos_cobranza", "dscto_capital", "dscto_interes_financiacion", "dscto_interes_xmora", "dscto_gastos_cobranza", "dscto_seguro", "ksubtotal_corriente", 
                    "ksubtotal_vencido", "ksubtotalneto", "kdescuentos", "ktotal", "establecimiento_comercio", "telefono","email","extracto_email","periodo_generacion_fact","periodo_facturacion","ciclo_facturacion","periodo_desembolso"
                };
                short[] dimensiones = new short[]{
                    3000, 4000, 4000, 4000, 4000, 4000, 5000, 9000, 9000, 5000, 5000, 5000, 6000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 10000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 6000, 5000, 5000, 5000, 5000, 5000, 5000, 5000
                };
                this.generaTitulos(cabecera, dimensiones);

                generado = generaArchivoExtractoFenalco(lista);
            }
            System.out.println("exportar " + generado);
            this.cerrarArchivo();
            if (generado) {
                fmt = new SimpleDateFormat("yyyMMdd HH:mm");
                url = request.getContextPath() + "/exportar/migracion/" + usuario.getLogin().toUpperCase() + "/GeneracionExtracto_" + fmt.format(new Date()) + ".xls";
                resp1 = Utility.getIcono(request.getContextPath(), 5) + "Informe generado con exito.<br/><br/>"
                        + Utility.getIcono(request.getContextPath(), 7) + " <a href='" + url + "' >Ver Informe</a></td>";
            }
            this.printlnResponse(resp1, "text/plain");
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE EXPORTACION DE SANCIONES. \n " + e.getMessage());
        }
    }

    public void generarRUTA() throws Exception {
        try {

            ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
            rutaInformes = rb.getString("ruta") + "/exportar/migracion/" + usuario.getLogin();
            File archivo = new File(rutaInformes);
            if (!archivo.exists()) {
                archivo.mkdirs();
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception(ex.getMessage());
        }

    }

    private void crearLibro(String nameFileParcial, String titulo) throws Exception {
        try {
            fmt = new SimpleDateFormat("yyyMMdd HH:mm");
            this.crearArchivo(nameFileParcial + fmt.format(new Date()) + ".xls", titulo);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage());
        }
    }

    private void generaTitulos(String[] cabecera, short[] dimensiones) throws Exception {

        try {

            fila = 0;

            for (int i = 0; i < cabecera.length; i++) {
                xls.adicionarCelda(fila, i, cabecera[i], titulo2);
                if (i < dimensiones.length) {
                    xls.cambiarAnchoColumna(i, dimensiones[i]);
                }
            }

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo ...\n" + ex.getMessage());
        }
    }

    private void cerrarArchivo() throws Exception {
        try {
            if (xls != null) {
                xls.cerrarLibro();
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            throw new Exception("Error en crearArchivo ....\n" + ex.getMessage());
        }
    }

    private void crearArchivo(String nameFile, String titulo) throws Exception {
        try {
            InitArchivo(nameFile);
            xls.obtenerHoja("Base");
            //xls.combinarCeldas(0, 0, 0, 8);
            // xls.adicionarCelda(0,0, titulo, header);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en crearArchivo ....\n" + ex.getMessage());
        }
    }

    private void InitArchivo(String nameFile) throws Exception {
        try {
            xls = new com.tsp.operation.model.beans.POIWrite();
            nombre = "/exportar/migracion/" + usuario.getLogin() + "/" + nameFile;
            xls.nuevoLibro(rutaInformes + "/" + nameFile);
            header = xls.nuevoEstilo("Tahoma", 10, true, false, "text", HSSFColor.GREEN.index, xls.NONE, HSSFCellStyle.ALIGN_LEFT);
            titulo1 = xls.nuevoEstilo("Tahoma", 8, true, false, "text", xls.NONE, xls.NONE, xls.NONE);
            titulo2 = xls.nuevoEstilo("Tahoma", 8, true, false, "text", HSSFColor.WHITE.index, HSSFColor.GREEN.index, HSSFCellStyle.ALIGN_CENTER, 2);
            letra = xls.nuevoEstilo("Tahoma", 8, false, false, "", xls.NONE, xls.NONE, xls.NONE);
            dinero = xls.nuevoEstilo("Tahoma", 8, false, false, "#,##0.00", xls.NONE, xls.NONE, xls.NONE);
            dinero2 = xls.nuevoEstilo("Tahoma", 8, false, false, "#,##0", xls.NONE, xls.NONE, xls.NONE);
            porcentaje = xls.nuevoEstilo("Tahoma", 8, false, false, "0.00%", xls.NONE, xls.NONE, xls.NONE);
            ftofecha = xls.nuevoEstilo("Tahoma", 8, false, false, "yyyy-mm-dd", xls.NONE, xls.NONE, xls.NONE);

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en InitArchivo .... \n" + ex.getMessage());
        }

    }

    private boolean generaArchivoExtractoMicro(ArrayList<PrevisualizarExtractos> lista) throws Exception {
        boolean generado = true;
        try {
            // Inicia el proceso de listar a excel
            PrevisualizarExtractos extracto = new PrevisualizarExtractos();
            Iterator it = lista.iterator();
            while (it.hasNext()) {
                extracto = (PrevisualizarExtractos) it.next();
                fila++;
                int col = 0;
                xls.adicionarCelda(fila, col++, extracto.getId(), letra);
                xls.adicionarCelda(fila, col++, extracto.getCod_rop(), letra);
                xls.adicionarCelda(fila, col++, extracto.getCod_rop_barcode(), letra);
                xls.adicionarCelda(fila, col++, extracto.getGenerado_el(), letra);
                xls.adicionarCelda(fila, col++, extracto.getVencimiento_rop(), letra);
                xls.adicionarCelda(fila, col++, extracto.getNegasoc(), letra);
                xls.adicionarCelda(fila, col++, extracto.getNit(), letra);
                xls.adicionarCelda(fila, col++, extracto.getNom_cli(), letra);
                xls.adicionarCelda(fila, col++, extracto.getDireccion(), letra);
                xls.adicionarCelda(fila, col++, extracto.getDepartamento(), letra);
                xls.adicionarCelda(fila, col++, extracto.getCiudad(), letra);
                xls.adicionarCelda(fila, col++, extracto.getBarrio(), letra);
                xls.adicionarCelda(fila, col++, extracto.getAgencia(), letra);
                xls.adicionarCelda(fila, col++, extracto.getLinea_producto(), letra);
                xls.adicionarCelda(fila, col++, extracto.getTotal_cuotas_vencidas(), letra);
                xls.adicionarCelda(fila, col++, extracto.getCuotas_pendientes(), letra);
                xls.adicionarCelda(fila, col++, extracto.getMin_dias_ven(), letra);
                xls.adicionarCelda(fila, col++, extracto.getFecha_ultimo_pago(), letra);
                xls.adicionarCelda(fila, col++, extracto.getSubtotal(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getTotal_sanciones(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getTotal_dscto_det(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getTotal_det(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getTotal_abonos(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getObservaciones(), letra);
                xls.adicionarCelda(fila, col++, extracto.getMsg_paguese_antes(), letra);
                xls.adicionarCelda(fila, col++, extracto.getMsg_estado(), letra);
                xls.adicionarCelda(fila, col++, extracto.getCapital(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getInt_cte(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getCat(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getInt_mora(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getGxc(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getDscto_capital(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getDscto_int_cte(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getDscto_int_mora(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getDscto_gxc(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getDscto_cat(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getSubtotal_corriente(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getSubtotal_vencido(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getSubtotal(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getTotal_descuento(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getTotal(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getTelefono(), letra);
                xls.adicionarCelda(fila, col++, extracto.getEmail(), letra);
                xls.adicionarCelda(fila, col++, extracto.getExtracto_email(), letra);
                xls.adicionarCelda(fila, col++, extracto.getPeriodo_generacion_fact(), letra);
                xls.adicionarCelda(fila, col++, extracto.getPeriodo_facturacion(), letra);
                xls.adicionarCelda(fila, col++, extracto.getCiclo_facturacion(), letra);
                xls.adicionarCelda(fila, col++, extracto.getPeriodo_desembolso(), letra);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo .... \n" + ex.getMessage());
        }
        return generado;
    }

    private boolean generaArchivoExtractoFenalco(ArrayList<PrevisualizarExtractos> lista) throws Exception {
        boolean generado = true;
        try {
            // Inicia el proceso de listar a excel
            PrevisualizarExtractos extracto = new PrevisualizarExtractos();
            Iterator it = lista.iterator();
            while (it.hasNext()) {
                extracto = (PrevisualizarExtractos) it.next();
                fila++;
                int col = 0;
                xls.adicionarCelda(fila, col++, extracto.getId(), letra);
                xls.adicionarCelda(fila, col++, extracto.getCod_rop(), letra);
                xls.adicionarCelda(fila, col++, extracto.getCod_rop_barcode(), letra);
                xls.adicionarCelda(fila, col++, extracto.getGenerado_el(), letra);
                xls.adicionarCelda(fila, col++, extracto.getVencimiento_rop(), letra);
                xls.adicionarCelda(fila, col++, extracto.getNegasoc(), letra);
                xls.adicionarCelda(fila, col++, extracto.getNit(), letra);
                xls.adicionarCelda(fila, col++, extracto.getNom_cli(), letra);
                xls.adicionarCelda(fila, col++, extracto.getDireccion(), letra);
                xls.adicionarCelda(fila, col++, extracto.getDepartamento(), letra);
                xls.adicionarCelda(fila, col++, extracto.getCiudad(), letra);
                xls.adicionarCelda(fila, col++, extracto.getBarrio(), letra);
                xls.adicionarCelda(fila, col++, extracto.getAgencia(), letra);
                xls.adicionarCelda(fila, col++, extracto.getLinea_producto(), letra);
                xls.adicionarCelda(fila, col++, extracto.getTotal_cuotas_vencidas(), letra);
                xls.adicionarCelda(fila, col++, extracto.getCuotas_pendientes(), letra);
                xls.adicionarCelda(fila, col++, extracto.getMin_dias_ven(), letra);
                xls.adicionarCelda(fila, col++, extracto.getFecha_ultimo_pago(), letra);
                xls.adicionarCelda(fila, col++, extracto.getSubtotal(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getTotal_sanciones(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getTotal_dscto_det(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getTotal_det(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getTotal_abonos(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getObservaciones(), letra);
                xls.adicionarCelda(fila, col++, extracto.getMsg_paguese_antes(), letra);
                xls.adicionarCelda(fila, col++, extracto.getMsg_estado(), letra);
                xls.adicionarCelda(fila, col++, extracto.getCapital(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getInt_cte(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getCat(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getInt_mora(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getGxc(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getDscto_capital(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getDscto_int_cte(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getDscto_int_mora(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getDscto_gxc(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getDscto_seguro(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getSubtotal_corriente(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getSubtotal_vencido(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getSubtotal(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getTotal_descuento(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getTotal(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getEst_comercio(), letra);
                xls.adicionarCelda(fila, col++, extracto.getTelefono(), letra);
                xls.adicionarCelda(fila, col++, extracto.getEmail(), letra);
                xls.adicionarCelda(fila, col++, extracto.getExtracto_email(), letra);
                xls.adicionarCelda(fila, col++, extracto.getPeriodo_generacion_fact(), letra);
                xls.adicionarCelda(fila, col++, extracto.getPeriodo_facturacion(), letra);
                xls.adicionarCelda(fila, col++, extracto.getCiclo_facturacion(), letra);
                xls.adicionarCelda(fila, col++, extracto.getPeriodo_desembolso(), letra);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo .... \n" + ex.getMessage());
        }
        return generado;
    }

    private void generarExtracto() throws Exception {
        String periodo = request.getParameter("periodo");
        String unegocio = request.getParameter("uneg");
        String ciclo = request.getParameter("ciclo");
        String vencMayor = request.getParameter("vencMayor");
        String fechaHoy = request.getParameter("fechaHoy");
        String[] vencimientos = vencMayor.split(",");
        String[] listUnd = unegocio.split(",");
        String resp = "";
        ArrayList<PrevisualizarExtractos> lista = new ArrayList<PrevisualizarExtractos>();
        try {
            for (int i = 0; i < listUnd.length; i++) {
                if (!dao.hayRegistrosAGenerar(periodo, listUnd[i], Integer.parseInt(ciclo))) {
                    resp = "No hay registros";
                } else {
                    if (listUnd[i].equals("1")) {
                        lista = dao.previsualizarExtractoMicro(periodo, "Generar", usuario.getLogin(), listUnd[i], vencimientos, ciclo, fechaHoy);
                    } else {
                        lista = dao.previsualizarExtractoFenalco(periodo, listUnd[i], "Generar", usuario.getLogin(), vencimientos, ciclo, fechaHoy);
                    }
                    resp = "OK";
                }
            }
            this.printlnResponse(resp, "application/text;");
        } catch (Exception ex) {
            this.printlnResponse("ERROR", "application/text;");
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarExtracto .... \n" + ex.getMessage());
        }
    }

    private void incluirRegistros() throws Exception {
        String lista[] = request.getParameter("listado").split(";");
        String periodo = request.getParameter("periodo");
        String ciclo = request.getParameter("ciclo");
        String[] vec = new String[3];
        TransaccionService tservice = new TransaccionService(usuario.getBd());
        try {
            tservice.crearStatement();
            for (int i = 0; i < lista.length; i++) {
                vec = lista[i].split(",");
                tservice.getSt().addBatch(dao.insertarNegociosExtracto(vec[1], vec[2], periodo, vec[0], usuario.getLogin(), Integer.parseInt(ciclo)));
            }

            tservice.execute();
            this.printlnResponse("OK", "application/text;");

        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarExtracto .... \n" + ex.getMessage());
        }

    }

    private void limpiarRegistros() throws Exception {
        String periodo = request.getParameter("periodo");
        String unegocio = request.getParameter("uneg");
        String ciclo = request.getParameter("ciclo");
        String[] listUnd = unegocio.split(",");
        try {
            for (int i = 0; i < listUnd.length; i++) {
                dao.limpiarRegistros(periodo, listUnd[i], Integer.parseInt(ciclo));
            }
            this.printlnResponse("OK", "application/text;");
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarExtracto .... \n" + ex.getMessage());
        }
    }

    private void exportarPrevisualizacion() throws SQLException {
        String unegocio = request.getParameter("uneg");
        String[] listUnd = unegocio.split(",");
        boolean generado = false;
        String url = "";
        String resp1 = "";
        try {
            String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
            if (!json.equals("")) {
                Gson gson = new Gson();
                Type tipoListaFacturas;
                tipoListaFacturas = new TypeToken<ArrayList<PrevisualizarExtractos>>() {
                }.getType();
                ArrayList<PrevisualizarExtractos> lista = gson.fromJson(json, tipoListaFacturas);
                this.generarRUTA();
                this.crearLibro("PrevisualizacionExtracto_", "PREVISUALIZACION EXTRACTO");

                if (listUnd[0].equals("1")) {
                    String[] cabecera = {
                        "id", "cod_rop", "cod_rop_barcode", "generado_el", "vencimiento_rop", "negocio", "cedula", "nombre_cliente", "direccion", "departamento", "ciudad", "barrio", "agencia", "linea_producto", "cuotas_vencidas",
                        "cuotas_pendientes", "dias_vencidos", "fch_ultimo_pago", "subtotal_rop", "total_sanciones", "total_descuentos", "total_rop", "total_abonos", "observacion", "msg_paguese_antes", "msg_estado_credito",
                        "capital", "interes_financiacion", "cat", "interes_xmora", "gastos_cobranza", "dscto_capital", "dscto_interes_financiacion", "dscto_interes_xmora", "dscto_gastos_cobranza", "dscto_cat", "ksubtotal_corriente", "ksubtotal_vencido", "ksubtotalneto", "kdescuentos", "ktotal"
                    };
                    short[] dimensiones = new short[]{
                        2000, 2000, 2000, 2000, 4000, 4000, 5000, 9000, 9000, 5000, 5000, 5000, 6000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 10000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000
                    };
                    this.generaTitulos(cabecera, dimensiones);
                    generado = generarArchivoExcelMicro(lista);
                } else {
                    String[] cabecera = {
                        "id", "cod_rop", "cod_rop_barcode", "generado_el", "vencimiento_rop", "negocio", "cedula", "nombre_cliente", "direccion", "departamento", "ciudad", "barrio", "agencia", "linea_producto", "cuotas_vencidas",
                        "cuotas_pendientes", "dias_vencidos", "fch_ultimo_pago", "subtotal_rop", "total_sanciones", "total_descuentos", "total_rop", "total_abonos", "observacion", "msg_paguese_antes", "msg_estado_credito",
                        "capital", "interes_financiacion", "seguro", "interes_xmora", "gastos_cobranza", "dscto_capital", "dscto_interes_financiacion", "dscto_interes_xmora", "dscto_gastos_cobranza", "dscto_seguro", "ksubtotal_corriente", "ksubtotal_vencido", "ksubtotalneto", "kdescuentos", "ktotal", "establecimiento_comercio"
                    };
                    short[] dimensiones = new short[]{
                        2000, 2000, 2000, 2000, 4000, 4000, 5000, 9000, 9000, 5000, 5000, 5000, 6000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 10000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 5000, 6000
                    };
                    this.generaTitulos(cabecera, dimensiones);
                    generado = generaArchivoExcelFenalco(lista);
                }

                System.out.println("exportar " + generado);
                this.cerrarArchivo();
                if (generado) {
                    fmt = new SimpleDateFormat("yyyMMdd HH:mm");
                    url = request.getContextPath() + "/exportar/migracion/" + usuario.getLogin().toUpperCase() + "/PrevisualizacionExtracto_" + fmt.format(new Date()) + ".xls";
                    resp1 = Utility.getIcono(request.getContextPath(), 5) + "Informe generado con exito.<br/><br/>"
                            + Utility.getIcono(request.getContextPath(), 7) + " <a href='" + url + "' >Ver Informe</a></td>";
                }
                this.printlnResponse(resp1, "text/plain");
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            throw new SQLException("ERROR DURANTE EXPORTACION DE SANCIONES. \n " + e.getMessage());
        }
    }

    private boolean generarArchivoExcelMicro(ArrayList<PrevisualizarExtractos> lista) throws Exception {
        boolean generado = true;
        try {
            // Inicia el proceso de listar a excel
            PrevisualizarExtractos extracto = new PrevisualizarExtractos();
            Iterator it = lista.iterator();
            while (it.hasNext()) {
                extracto = (PrevisualizarExtractos) it.next();
                fila++;
                int col = 0;
                xls.adicionarCelda(fila, col++, "", letra);
                xls.adicionarCelda(fila, col++, "", letra);
                xls.adicionarCelda(fila, col++, "", letra);
                xls.adicionarCelda(fila, col++, "", letra);
                xls.adicionarCelda(fila, col++, extracto.getVencimiento_rop(), letra);
                xls.adicionarCelda(fila, col++, extracto.getNegasoc(), letra);
                xls.adicionarCelda(fila, col++, extracto.getNit(), letra);
                xls.adicionarCelda(fila, col++, extracto.getNom_cli(), letra);
                xls.adicionarCelda(fila, col++, extracto.getDireccion(), letra);
                xls.adicionarCelda(fila, col++, extracto.getDepartamento(), letra);
                xls.adicionarCelda(fila, col++, extracto.getCiudad(), letra);
                xls.adicionarCelda(fila, col++, extracto.getBarrio(), letra);
                xls.adicionarCelda(fila, col++, extracto.getAgencia(), letra);
                xls.adicionarCelda(fila, col++, extracto.getLinea_producto(), letra);
                xls.adicionarCelda(fila, col++, extracto.getTotal_cuotas_vencidas(), letra);
                xls.adicionarCelda(fila, col++, extracto.getCuotas_pendientes(), letra);
                xls.adicionarCelda(fila, col++, extracto.getMin_dias_ven(), letra);
                xls.adicionarCelda(fila, col++, extracto.getFecha_ultimo_pago(), letra);
                xls.adicionarCelda(fila, col++, extracto.getSubtotal(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getTotal_sanciones(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getTotal_dscto_det(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getTotal_det(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getTotal_abonos(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getObservaciones(), letra);
                xls.adicionarCelda(fila, col++, extracto.getMsg_paguese_antes(), letra);
                xls.adicionarCelda(fila, col++, extracto.getMsg_estado(), letra);
                xls.adicionarCelda(fila, col++, extracto.getCapital(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getInt_cte(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getCat(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getInt_mora(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getGxc(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getDscto_capital(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getDscto_int_cte(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getDscto_int_mora(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getDscto_gxc(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getDscto_cat(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getSubtotal_corriente(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getSubtotal_vencido(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getSubtotal(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getTotal_descuento(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getTotal(), dinero2);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo .... \n" + ex.getMessage());
        }
        return generado;
    }

    private boolean generaArchivoExcelFenalco(ArrayList<PrevisualizarExtractos> lista) throws Exception {
        boolean generado = true;
        try {
            // Inicia el proceso de listar a excel
            PrevisualizarExtractos extracto = new PrevisualizarExtractos();
            Iterator it = lista.iterator();
            while (it.hasNext()) {
                extracto = (PrevisualizarExtractos) it.next();
                fila++;
                int col = 0;
                xls.adicionarCelda(fila, col++, "", letra);
                xls.adicionarCelda(fila, col++, "", letra);
                xls.adicionarCelda(fila, col++, "", letra);
                xls.adicionarCelda(fila, col++, "", letra);
                xls.adicionarCelda(fila, col++, extracto.getVencimiento_rop(), letra);
                xls.adicionarCelda(fila, col++, extracto.getNegasoc(), letra);
                xls.adicionarCelda(fila, col++, extracto.getNit(), letra);
                xls.adicionarCelda(fila, col++, extracto.getNom_cli(), letra);
                xls.adicionarCelda(fila, col++, extracto.getDireccion(), letra);
                xls.adicionarCelda(fila, col++, extracto.getDepartamento(), letra);
                xls.adicionarCelda(fila, col++, extracto.getCiudad(), letra);
                xls.adicionarCelda(fila, col++, extracto.getBarrio(), letra);
                xls.adicionarCelda(fila, col++, extracto.getAgencia(), letra);
                xls.adicionarCelda(fila, col++, extracto.getLinea_producto(), letra);
                xls.adicionarCelda(fila, col++, extracto.getTotal_cuotas_vencidas(), letra);
                xls.adicionarCelda(fila, col++, extracto.getCuotas_pendientes(), letra);
                xls.adicionarCelda(fila, col++, extracto.getMin_dias_ven(), letra);
                xls.adicionarCelda(fila, col++, extracto.getFecha_ultimo_pago(), letra);
                xls.adicionarCelda(fila, col++, extracto.getSubtotal(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getTotal_sanciones(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getTotal_dscto_det(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getTotal_det(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getTotal_abonos(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getObservaciones(), letra);
                xls.adicionarCelda(fila, col++, extracto.getMsg_paguese_antes(), letra);
                xls.adicionarCelda(fila, col++, extracto.getMsg_estado(), letra);
                xls.adicionarCelda(fila, col++, extracto.getCapital(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getInt_cte(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getCat(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getInt_mora(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getGxc(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getDscto_capital(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getDscto_int_cte(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getDscto_int_mora(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getDscto_gxc(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getDscto_seguro(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getSubtotal_corriente(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getSubtotal_vencido(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getSubtotal(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getTotal_descuento(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getTotal(), dinero2);
                xls.adicionarCelda(fila, col++, extracto.getEst_comercio(), letra);
            }
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarArchivo .... \n" + ex.getMessage());
        }
        return generado;
    }

    private void generarMi_Cat() throws Exception {
        String resp = "";
        String periodo = request.getParameter("periodo") != null ? request.getParameter("periodo") : "";
        String ciclo = request.getParameter("ciclo") != null ? request.getParameter("ciclo") : "";
        try {

            if (!periodo.equals("") && !ciclo.equals("")) {
                
                FacturasCAT cat = new FacturasCAT();
                cat.init(ciclo, periodo);

                CausacionInteresMC mi = new CausacionInteresMC();
                mi.init(3, ciclo, periodo);

                resp = "OK";
            }
            
            this.printlnResponse(resp, "application/text;");
        } catch (Exception ex) {
            this.printlnResponse("Error", "application/text;");
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en tomar_foto_ciclo .... \n" + ex.getMessage());
        }
    }

    private void tomar_foto_ciclo() throws Exception {
        String periodo = request.getParameter("periodo");
        String ciclo = request.getParameter("ciclo");
        String resp = "";
        try {
            if (dao.isFotoGenerada(periodo, ciclo)) {
                resp = "La foto ya fue generada para este ciclo";
            } else {
                resp = dao.tomarFotoCiclo(periodo, ciclo);
            }
            this.printlnResponse(resp, "application/text;");
        } catch (Exception ex) {
            this.printlnResponse("Error", "application/text;");
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en tomar_foto_ciclo .... \n" + ex.getMessage());
        }
    }

    private void copiar_sanciones_mes() throws Exception {
        String periodo = request.getParameter("periodo");
        String resp = "";
        try {
            if (dao.isSancionesGeneradas(periodo, "1")) {
                resp = "Las sanciones ya fueron generadas para este periodo";
            } else {
                resp = dao.copiarSancionesMes(periodo, usuario.getLogin());
            }
            this.printlnResponse(resp, "application/text;");
        } catch (Exception ex) {
            this.printlnResponse("Error", "application/text;");
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en tomar_foto_ciclo .... \n" + ex.getMessage());
        }
    }

    private void buscar_sanciones_mes() throws Exception {
        String periodo = request.getParameter("periodo");
        String tipo = request.getParameter("tipo");
        String unegocio = request.getParameter("listUnd");
        String[] listUnd = unegocio.split(";");
        ArrayList<PrevisualizarExtractos> lista = new ArrayList<PrevisualizarExtractos>();
        lista = dao.obtenerDetalleSanciones(periodo, tipo, listUnd);
        Gson gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
        this.printlnResponse(json, "application/json;");
    }

    private void editar_sanciones_mes() throws Exception {
        String rangoIni = request.getParameter("rango_ini");
        String rangoFIn = request.getParameter("rango_fin");
        String porcentaje = request.getParameter("porcentaje");

        PrevisualizarExtractos sanc = new PrevisualizarExtractos();
        sanc.setRango_ini(Integer.parseInt(rangoIni));
        sanc.setRango_fin(Integer.parseInt(rangoFIn));
        sanc.setPorcentaje(porcentaje);

        Gson gson = new Gson();
        String json = gson.toJson(sanc);
        this.printlnResponse(json, "application/json;");

    }

    private void generar_sanciones_mes() throws Exception {
        String periodo = request.getParameter("periodo");
        String tipo = request.getParameter("tipo");
        String resp = "";
        int sw = 0;
        ArrayList listSql = null;
        TransaccionService tservice = new TransaccionService(usuario.getBd());
        try {
            String json = request.getParameter("listado") != null ? request.getParameter("listado") : "";
            tservice.crearStatement();
            if (!json.equals("")) {
                Gson gson = new Gson();
                Type tipoListaFacturas;
                tipoListaFacturas = new TypeToken<ArrayList<PrevisualizarExtractos>>() {
                }.getType();
                ArrayList<PrevisualizarExtractos> lista = gson.fromJson(json, tipoListaFacturas);
                if (tipo.equals("1")) {
                    if (dao.isSancionesGeneradas(periodo, tipo)) {
                        resp = "Las sanciones ya fueron generadas para este periodo";
                    } else {
                        listSql = dao.insertSancionesMes(usuario.getLogin(), periodo, lista, tipo);
                        sw = 1;
                    }

                } else {
                    if (dao.isSancionesGeneradas(periodo, tipo)) {
                        resp = "Las condonaciones ya fueron generadas para este periodo";
                    } else {
                        listSql = dao.insertCondonacionesMes(usuario.getLogin(), periodo, lista, tipo);
                        sw = 1;
                    }
                }
                if (sw == 1) {
                    Iterator i = listSql.iterator();
                    while (i.hasNext()) {
                        String sql = (String) i.next();
                        tservice.getSt().addBatch(sql);
                    }
                    tservice.execute();
                    resp = "OK";
                }
                this.printlnResponse(resp, "application/text;");
            }
        } catch (Exception ex) {
            this.printlnResponse("Error", "application/text;");
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarExtracto .... \n" + ex.getMessage());
        }

    }

    private void buscar_condonaciones_mes() throws Exception {
        String periodo = request.getParameter("periodo");
        String tipo = request.getParameter("tipo");
        String unegocio = request.getParameter("listUnd");
        String[] listUnd = unegocio.split(";");
        ArrayList<PrevisualizarExtractos> lista = new ArrayList<PrevisualizarExtractos>();
        lista = dao.obtenerDetalleCondonaciones(periodo, tipo, listUnd);
        Gson gson = new Gson();
        String json = "{\"page\":1,\"rows\":" + gson.toJson(lista) + "}";
        this.printlnResponse(json, "application/json;");
    }
    
    private void tomar_foto_mes() throws Exception {

        String resp = "";
        try {           
            resp = dao.tomarFotoMes();            
            this.printlnResponse(resp, "application/text;");
        } catch (Exception ex) {
            this.printlnResponse("Error", "application/text;");
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en tomar_mes .... \n" + ex.getMessage());
        }
    }
    
    private void generarCuotaManejo() throws Exception {
        String resp = "";
        String periodo = request.getParameter("periodo") != null ? request.getParameter("periodo") : "";
        String ciclo = request.getParameter("ciclo") != null ? request.getParameter("ciclo") : "";
        try {

            if (!periodo.equals("") && !ciclo.equals("")) {
                
                CausacionCuotaManejo cmanejo = new CausacionCuotaManejo();
                cmanejo.init(ciclo, periodo);
              
                resp = "OK";
            }
            
            this.printlnResponse(resp, "application/text;");
        } catch (Throwable ex) {
            this.printlnResponse("Error", "application/text;");
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en generarCuotaManejo .... \n" + ex.getMessage());
        }
    }
    
    private void generarMi_CatMes() throws Exception {
        String resp = "";
//        String periodo = request.getParameter("periodo") != null ? request.getParameter("periodo") : "";
//        String ciclo = request.getParameter("ciclo") != null ? request.getParameter("ciclo") : "";
        String periodo = "";
        String ciclo = "";
        try {
            FacturasCAT cat = new FacturasCAT();
            cat.init(ciclo, periodo);
            CausacionInteresMC mi = new CausacionInteresMC();
            mi.init(2, ciclo, periodo);
            resp = "OK";

            this.printlnResponse(resp, "application/text;");
        } catch (Exception ex) {
            this.printlnResponse("Error", "application/text;");
            System.out.println(ex.getMessage());
            ex.printStackTrace();
            throw new Exception("Error en tomar_foto_ciclo .... \n" + ex.getMessage());
        }
    }
    

}
