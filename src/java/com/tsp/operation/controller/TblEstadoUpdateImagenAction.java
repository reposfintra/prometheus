/*
 * TblEstadoInsert.java
 *
 * Created on 5 de octubre de 2005, 09:55 AM
 */

package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;

/**
 *
 * @author  Andres
 */
public class TblEstadoUpdateImagenAction extends Action {
        
        /** Creates a new instance of TblEstadoInsert */
        public TblEstadoUpdateImagenAction() {
        }
        
        public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
                
                String next = "";
                String pag = "/jsp/masivo/tblestado/EstadoUpdateImagen.jsp";
                
                String tipo = request.getParameter("tipo");
                String codestado = request.getParameter("codestado");                
                                
                pag += "?tipo=" + tipo + "&codestado=" + codestado + "&mensaje=";
                next = com.tsp.util.Util.LLamarVentana(pag, "Actualizar Estado");
                
                this.dispatchRequest(next);
                
        }
        
}
