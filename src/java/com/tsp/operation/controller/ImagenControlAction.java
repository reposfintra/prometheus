/*
 * BuscarImagenAction.java
 *
 * Created on 19 de octubre de 2005, 03:57 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import com.tsp.exceptions.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;


/**
 *
 * @author  dbastidas
 */
public class ImagenControlAction extends Action {
    
    /** Creates a new instance of BuscarImagenAction */
        public ImagenControlAction() {
    }
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        String codact = request.getParameter("actividad");
        String tipodoc = request.getParameter("tipoDocumento");
        String doc = request.getParameter("documento");
        String cmd = request.getParameter("cmd")!=null?request.getParameter("cmd"):"";
        String next="";
        ////System.out.println("");
        ////System.out.println("Actividad "+codact );
        ////System.out.println("Tipo "+ tipodoc );
        ////System.out.println("Documento "+doc );
        String foto ="";
        String fecha="";
        try {
            
            
            if ( model.ImagenSvc.existeImagen(codact,tipodoc,doc) ){
                model.ImagenSvc.searchImagen("1","1","1",null,null,null,codact, tipodoc,
                                                   doc,null,null,null,null,usuario.getLogin());
                List lista = model.ImagenSvc.getImagenes();                   
                Imagen img = null;
                String ruta="";
                if (lista==null){
                    foto = "";
                } 
                else {
                    img = (Imagen) lista.get(0);
                    foto = request.getContextPath()+"/documentos/imagenes/"+ usuario.getLogin()+"/"+img.getNameImagen();
                    ruta = "/documentos/imagenes/"+ usuario.getLogin()+"/"+img.getNameImagen();
                    fecha = img.getFecha_creacion();
                }
                next = "/imagen/VerImagen.jsp?foto="+foto+"&tipoAct="+codact+"&tipoDoc="+tipodoc+"&documento="+doc;
                if(cmd.equals("noAdmin")){
                    next = "/imagen/VerImagenNolink.jsp?foto="+foto+"&tipoAct="+codact+"&tipoDoc="+tipodoc+"&documento="+doc+"&fecha="+fecha+"&ruta="+ruta;
                }
            }
            else{
                 next = Util.LLamarVentana( "/imagen/Manejo.jsp?documento="+doc+"&actividad="+codact+"&tipoDocumento="+tipodoc, "Agregar Imagen" );
                 if(cmd.equals("noAdmin")){
                    next = "/imagen/VerImagenNoExiste.jsp";
                }
            }
            
                
            
                
            ////System.out.println("Next "+next);
        }catch (Exception e){
               e.printStackTrace();
               throw new ServletException(e.getMessage());
         } 
         this.dispatchRequest(next);
    }
    
}
