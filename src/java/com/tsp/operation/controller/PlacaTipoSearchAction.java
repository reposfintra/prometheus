/*******************************************************************
 * Nombre clase: PlacaTipoSearchAction.java
 * Descripci�n: Accion para buscar una placa y tipo de la bd.
 * Autor: Ing. Andres Martinez
 * Fecha: 10 de Abril de 2006, 05:20 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import com.tsp.exceptions.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
/**
 *
 * @author  Amartinez
 */
public class PlacaTipoSearchAction extends Action{
    
    /** Creates a new instance of PlacaTipoSearchAction */
    public PlacaTipoSearchAction() {
    }
    
    public void run () throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "";
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario)session.getAttribute ("Usuario");
        String idplaca = request.getParameter ("idplaca").toUpperCase ();
        try{
            String tipo = model.placaService.buscaTipoXPlaca(idplaca);
            if(tipo.equals ("T"))
                next = "/placas/trailerUpdate.jsp";
            else if(tipo.equals ("C"))
                next = "/placas/placasUpdate.jsp";
            else
                next = "/placas/placaUpdate.jsp";
            
            model.placaService.buscaPlacaTipo (idplaca, tipo);
            Placa placa = model.placaService.getPlaca ();
            model.tablaGenService.buscarTipo ( tipo);
            model.tablaGenService.buscarClase ( tipo);
            model.tablaGenService.buscarRines ();
            model.tablaGenService.buscarModelo ();
            model.tablaGenService.buscarPiso ();
            model.tablaGenService.buscarTitulo ();
            model.tablaGenService.buscarClasificacion ();
            model.tablaGenService.buscarGrupos ();
            if ( placa == null)
                next+="?msg=No se encontro ningun resultado";
            else
                request.setAttribute ("placa", placa);
        }catch (Exception e){
            e.printStackTrace ();
            throw new ServletException (e.getMessage ());
        }
        
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest (next);
    }
}
