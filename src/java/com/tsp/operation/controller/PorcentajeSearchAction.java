/*
 * PorcentajeSearchAction.java
 *
 * Created on 21 de julio de 2006, 02:34 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
//import com.tsp.operation.model.threads.ExportarPorcentajes;
import com.tsp.util.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

/**
 *
 * @author  Amartinez
 */
public class PorcentajeSearchAction extends Action {
  
 // static Logger logger = Logger.getLogger(BuscarClienteFacturaAction.class);
  /** Creates a new instance of BuscarClienteFactura */
  public PorcentajeSearchAction() {
  }
  
  public void run() throws ServletException, InformationException {
      try{
          HttpSession session = request.getSession();
          Usuario usuario = (Usuario) session.getAttribute("Usuario");
          
          String buscar= (request.getParameter ("cargarDocumento")!=null)?request.getParameter ("cargarDocumento"):"";
          String fechaInicio = (request.getParameter ("fechaInicio")!=null)?request.getParameter ("fechaInicio"):"";
          String fechaFinal = (request.getParameter ("fechaFinal")!=null)?request.getParameter ("fechaFinal"):"";

         
          model.PorcentajeSvc.setExportarPorcentajes(fechaInicio,fechaFinal,model, usuario);
          //model.PorcentajeSvc.Start();
          String next="/jsp/masivo/reportes/Porcentajes.jsp?Mensaje=Su proceso ha iniciado exitosamente en Rango fechas("+fechaInicio+"/"+fechaFinal+")";
          ////System.out.println(next); 
      this.dispatchRequest(next);
      }catch (Exception e){
          e.printStackTrace();
          throw new ServletException(e.getMessage());
      }
  } 
}

