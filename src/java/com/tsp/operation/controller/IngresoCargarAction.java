/*************************************************************************
 * Nombre:        IngresoBuscarClienteAction.java
 * Descripci�n:   Clase Action para buscar la informacion del cliente
 *                y las suscursales del banco
 * Autor:         Ing. Diogenes Antonio Bastidas Morales
 * Fecha:         10 de mayo de 2006, 03:23 PM
 * Versi�n        1.0
 * Coyright:      Transportes Sanchez Polo S.A.
 *************************************************************************/


package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  dbastidas
 */
public class IngresoCargarAction extends Action{
    
    /** Creates a new instance of IngresoBuscarClienteAction */
    public IngresoCargarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        com.tsp.finanzas.contab.model.Model modelcontab = (com.tsp.finanzas.contab.model.Model) session.getAttribute("modelcontab");
        String next = "/" + request.getParameter("carpeta")+ "/" + request.getParameter("pagina");
        String evento =request.getParameter("evento");
        String age = "";
        String cliente =request.getParameter("cliente");
        String mon_local =request.getParameter("moneda");
        String tipo = request.getParameter("tipo1")!=null ?request.getParameter("tipo1"):"";
        String cuenta = request.getParameter("cuenta1")!=null ?request.getParameter("cuenta1"):"";
        String modificar = request.getParameter("modificar")!=null ?request.getParameter("modificar"):"";
        String feccon = request.getParameter("feccon")!=null ?request.getParameter("feccon"):"";
        String mostrar = request.getParameter("mostrar");
        String subingreso = request.getParameter("subingreso");
        
        try{
            
            if(evento.equals("cliente")){
                String cuentas = request.getParameter("cuentas")!=null ?request.getParameter("cuentas"):"";
                model.clienteService.informacionCliente(cliente.toUpperCase());
                Cliente clin  = model.clienteService.getCliente();
                if( clin != null){
                    /*if (!clin.getBranch_code().equals("")){
                        model.servicioBanco.loadSucursalesAgenciaCuenta(age, clin.getBranch_code().replaceAll("\n",""), usuario.getDstrct());
                    }*/
                    next +="?sw=ok&moneda="+mon_local+"&cuentas="+cuentas+"&mostrar="+mostrar+"&subingreso="+subingreso;
                }else{
                    next +="?sw=no&moneda="+mon_local+"&cuentas="+cuentas+"&mostrar="+mostrar+"&subingreso="+subingreso;
                }
                if(!tipo.equals("")){
                    modelcontab.subledgerService.buscarCuentasTipoSubledger(cuenta);
                }
                
            }
            else  if(evento.equals("sucursal")){
                //jose 2007-02-15
                model.servicioBanco.loadSucursalesAgenciaCuenta(age, request.getParameter("banco"), usuario.getDstrct());
                next +="?moneda="+mon_local+"&cuentas=&modificar="+modificar+"&feccon="+feccon+"&mostrar="+mostrar+"&subingreso="+subingreso;
                if(!tipo.equals("")){
                    modelcontab.subledgerService.buscarCuentasTipoSubledger(cuenta);
                }
            }
            else  if(evento.equals("Identificacion")){
                model.identidadService.buscarIdentidad(request.getParameter("identificacion"),usuario.getCia());
                if(model.identidadService.obtIdentidad() != null ){
                    next +="?sw=ok";
                }else{
                    next +="?sw=no";
                }
            }
            
        }catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
}
