/********************************************************************
 *      Nombre Clase.................   TraficoConsultasAction.java
 *      Descripci�n..................   Action que se encarga de insertar un filtro creado por el usuario en el sistema
 *      Autor........................   David Lamadrid
 *      Fecha........................   20.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  David A
 */
public class TraficoConsultasAction extends Action{
    
    /** Creates a new instance of TraficoConsultasAction */
    public TraficoConsultasAction () {
    }
    
    public void run () throws ServletException, InformationException {
        try{
            
            String user    =""+ request.getParameter ("usuario");
            String linea   =""+ request.getParameter ("linea");
            String conf    =""+ request.getParameter ("conf");
            String clas    =""+ request.getParameter ("clas");
            String nombre  =""+ request.getParameter ("nombre");
            String campo   =""+ request.getParameter ("campo");
            
            String []filtros   = request.getParameterValues ("filtro");
            
            String cadena="";
            for(int i=0; i<filtros.length; i++){
                cadena = cadena+filtros[i];
            }
            ////System.out.println ("Cadena de Salida"+cadena);
            
            Vector vectorFiltro = new Vector ();
            vectorFiltro=model.traficoService.obtenerVectorFiltros (filtros);
            cadena="";
            for(int i=0;i<vectorFiltro.size ();i++){
                cadena=cadena+vectorFiltro.elementAt (i);
            }
            ////System.out.println ("Vector de filtros: "+cadena);
            
            cadena="";
            Vector vectorFiltroO = new Vector ();
            vectorFiltroO=model.traficoService.ordenarVectorFiltro (vectorFiltro);
            cadena="";
            for(int i=0;i<vectorFiltroO.size ();i++){
                cadena=cadena+vectorFiltroO.elementAt (i);
            }
            ////System.out.println ("Vector de filtros Ordenado: "+cadena);
            
            String filtro= model.traficoService.filtro (vectorFiltroO);
            ////System.out.println ("Filtro final: "+ filtro);
            
            model.traficoService.generarVZonas (user);
            ////System.out.println ("Genrerar zonas ");
            Vector zonas = model.traficoService.obdtVectorZonas ();
            String validacion="where codzona='-1'";
            if ((zonas == null)||zonas.size ()<=0){}else{
                validacion = model.traficoService.validacion (zonas);
            }
            ////System.out.println ("\nValidacion final: "+ validacion);
            
            if(filtro.length ()>1){
                filtro="where "+validacion +" and "+filtro;
            }    
            if(filtro.equals ("")){
                filtro=validacion+filtro;
            }
            
            
            
            if(linea.equals ("null") ){
                linea="-caravana-cedcon-destino-fecha_despacho-fecha_prox_reporte-fecha_ult_reporte-ult_observacion-origen-placa-planilla-pto_control_proxreporte-user_update-zona";
            }
            if(conf.equals ("null")||conf.equals ("")){
                conf = "select oid,caravana,cedcon,destino,fecha_despacho,fecha_prox_reporte,fecha_ult_reporte,ult_observacion,origen,placa,planilla,pto_control_proxreporte,user_update,zona ";
            }
            if(clas.equals ("null")||clas.equals ("")){
                clas="";
            }
            
            Vector colum= new Vector ();
            colum = model.traficoService.obtenerCampos (linea);
            
            String consulta = model.traficoService.getConsulta (conf, filtro, clas);
            ////System.out.println ("Consulta final:"+consulta);
            
            FiltrosUsuarios u = new FiltrosUsuarios ();
            u.setIdUsuario (user);
            u.setConsulta (consulta);
            u.setLinea (linea);
            u.setNombre (nombre);
            model.traficoService.insertarFiltro (u);
            
            
            String next = "/controltrafico/filtro.jsp?linea="+linea+"&conf="+conf+"&clas="+clas;
            ////System.out.println ("next en Consultas"+next);
            RequestDispatcher rd = application.getRequestDispatcher (next);
            if(rd == null){
                throw new Exception ("No se pudo encontrar "+ next);
            }
            rd.forward (request, response);
        }
        catch(Exception e){
            throw new ServletException ("Accion:"+ e.getMessage ());
        }
        
        
    }
    
}
