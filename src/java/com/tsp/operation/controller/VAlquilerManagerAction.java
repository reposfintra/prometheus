/*
 * VAlquilerManagerAction.java
 *
 * Created on 4 de septiembre de 2005, 11:23 AM
 */

package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
/**
 *
 * @author  Jm
 */
public class VAlquilerManagerAction extends Action{
    private static String pla;
    private static String std;
    /** Creates a new instance of VAlquilerManagerAction */
    public VAlquilerManagerAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        try{
            String Opcion                 = (request.getParameter("Opcion")!=null)?request.getParameter("Opcion"):"";
            String placa                  = (request.getParameter("placa")!=null)?request.getParameter("placa"):"";
            String std_job_no             = (request.getParameter("standard")!=null)?request.getParameter("standard"):"";
            String sel                    = (request.getParameter("sel")!=null)?request.getParameter("sel"):"";
            HttpSession Session           = request.getSession();
            Usuario user                  = new Usuario();
            user                          = (Usuario)Session.getAttribute("Usuario");
            String usuario                = user.getLogin();
            String distrito               = user.getDstrct();
            
            String []LOV                  = request.getParameterValues("LOV");
            String Mensaje                = "";
            
            String next ="";
            
            if (Opcion.equals("Guardar")){
                if(model.placaService.placaExist(placa)){
                    
                    if(!model.VAlquilerSvc.EXISTE(placa,std_job_no)){
                        model.VAlquilerSvc.INSERT(distrito, placa, std_job_no, usuario);
                        Mensaje = "El Registro ha sido agregado";
                    }
                    
                    else
                        Mensaje = "El Registro ya existe";
                }
                else
                    Mensaje = "El Vehiculo no existe ";
            }
            
            
            if (Opcion.equals("Modificar")){
                if(model.VAlquilerSvc.EXISTE(placa,std_job_no)){
                    model.VAlquilerSvc.UPDATE(placa, std_job_no, usuario, "", "");
                    Mensaje = "El Registro ha sido modificado";
                }
            }
            
            if (Opcion.equals("Nuevo"))
                model.VAlquilerSvc.ReiniciarDato();
            
            if (Opcion.equals("Ocultar Lista")){
                model.VAlquilerSvc.ReiniciarLista();
            }
            
            if (Opcion.equals("Anular")){
                //model.VExternosSvc.ReiniciarDato();
                for(int i=0;i<LOV.length;i++){
                    this.Conversion(LOV[i]);
                    model.VAlquilerSvc.UPDATEESTADO("A", pla, std, usuario);
                }
                Mensaje = "El Registro ha sido anulado";
            }
            if (Opcion.equals("Activar")){
                //model.VA
                for(int i=0;i<LOV.length;i++){
                    this.Conversion(LOV[i]);
                    model.VAlquilerSvc.UPDATEESTADO("", pla, std, usuario);
                }
                Mensaje = "El Registro ha sido activado ";
            }
            if (Opcion.equals("Eliminar")){
                //model.VExternosSvc.ReiniciarDato();
                for(int i=0;i<LOV.length;i++){
                    this.Conversion(LOV[i]);
                    model.VAlquilerSvc.DELETE(pla, std);
                }
                Mensaje = "El Registro ha sido eliminado ";
            }
            
            
            
            if(Opcion.equals("Seleccionar")){
                this.Conversion(sel);
                model.VAlquilerSvc.SEARCH(pla, std);
            }
            
            if (Opcion.equals("Listado")){
                //model.SalidaCSvc.ReiniciarLista();
                model.VAlquilerSvc.LIST();
                
            }
            
            
            
            if ("Anular|Activar".indexOf(Opcion) != -1) {
                // model.VAlquilerSvc.LIST();
                //  model.SalidaCSvc.paginacion.setLista(model.SalidaCSvc.getLista());
                next ="/VAlquiler/ListaVehiculos.jsp?Mensaje="+Mensaje;
            }
            else
                next ="/VAlquiler/VAlquiler.jsp?Mensaje="+Mensaje;
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en VAlquilerManagerAction .....\n"+e.getMessage());
        }
        
    }
    
    public static void Conversion(String f){
        String vF [] = f.split("/");
        pla = vF[0];
        std = vF[1];
    }
    
}
