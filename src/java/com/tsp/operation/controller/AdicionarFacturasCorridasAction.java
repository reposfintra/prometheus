  /***************************************
    * Nombre Clase ............. AdminSoftwareAction.java
    * Descripci�n  .. . . . . .  Maneja los eventos para la adminitracion de software
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  10/01/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...Transportes Sanchez Polo S.A.
    *******************************************/




package com.tsp.operation.controller;




import java.io.*;
import java.util.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.*;


public class AdicionarFacturasCorridasAction extends Action{
    
   
    
    public void run() throws ServletException, InformationException {
        
         try{
                HttpSession session    = request.getSession();
                Usuario     usuario    = (Usuario)session.getAttribute("Usuario");
                String      user       = usuario.getLogin();
                String      agencia    = usuario.getId_agencia();
                String      distrito   = usuario.getDstrct();
                

                String      next       = "/jsp/cxpagar/corridas/ListaFacturas.jsp";
                String      msj        = "";
                
                String      evento     = request.getParameter("evento");  
                
                if( evento!= null ){
                
                       if( evento.equals("BUSCAR")){                
                
                                String      corrida        = request.getParameter("corrida");
                                String      banco          = request.getParameter("banco");
                                String      sucursal       = request.getParameter("sucursal");
                                String      proveedor      = request.getParameter("proveedor");
                                String      nombre         = request.getParameter("name");


                             // Buscamos Facturas:
                                String      filtros        =  getWhere( banco, sucursal , agencia, proveedor );
                             

                                model.ExtractosSvc.getFacturas_Adicionales( distrito, filtros  );
                                List        facturas       =  model.ExtractosSvc.getFacturasAdicionales();
                                for(int i=0; i<facturas.size();i++){
                                    Corrida  factura  = (Corrida) facturas.get(i) ;
                                    factura.setCorrida(corrida); 
                                    factura.setNombre   (nombre);
                                    factura.setId(i);
                                }
                                
                                if( facturas.size()==0  )
                                     msj = "No hay facturas disponibles para ser agregadas a la corrida...";
                                
                                next +="?msj=" + msj ;
                                
                                
                       }
                       
                       
                       
                       
                       if( evento.equals("ADD")){
                           
                             String      corrida        = request.getParameter("corrida");
                             String      banco          = request.getParameter("banco");
                             String      sucursal       = request.getParameter("sucursal");
                             String      proveedor      = request.getParameter("proveedor");
                             String      nombre         = request.getParameter("name");
                                
                             String[]    seleccion   = request.getParameterValues("factura");
                             List        facturas    = model.ExtractosSvc.loadSeleccion(seleccion);
                             String fecha = Util.getFechaActual_String(9);
                             next ="/controller?estado=Corrida&accion=Search&cmd=busquedaFactura&nrocorrida="+ corrida +"&banco="+ banco +"&sucursal="+ sucursal +"&nitPro="+ proveedor +"&nombre="+nombre;
                             
                             if(facturas.size()>0){
                                 for(int i=0;i<facturas.size();i++){
                                     Corrida  factura  = (Corrida) facturas.get(i); 
                                     String SQL     = "";
                                     SQL +=  model.ExtractosSvc.insert        ( factura, corrida, user,fecha );
                                     SQL +=  model.ExtractosSvc.updateFacturas( factura, corrida, user );

                                  // Ejecutamos el sql:
                                     model.tService.crearStatement();
                                     model.tService.getSt().addBatch(SQL);
                                     model.tService.execute();
                                 }
                                 next +="&msg=Facturas adicionadas a la corrida....";
                             } 
                             else
                                 next +="&msg=Deber� seleccionar facturas para adicionar a la corrida....";
                             
                       }
                
                
                }
                
                
                RequestDispatcher rd = application.getRequestDispatcher( next );
                if(rd == null)
                    throw new Exception("No se pudo encontrar "+ next);
                rd.forward(request, response); 
            
        } catch (Exception e){
             throw new ServletException(e.getMessage());
        }
        
        
    }
    
    
    
    
    
     /**
     * M�todo que forma el SQL de los filtros seleccionados, teniendo en cuenta la agencia del usuario
     * @autor.......fvillacob
     * @throws......Exception
     * @version.....1.0.
     **/
    public String  getWhere( String  banco, String sucursal, String agencia, String propietario )throws Exception{
        String  where = "";
        try{
        
             agencia  =  ( agencia.equals( "OP" )  ) ? " like '%' " :  " ='" + agencia  +"'"; 
             
             where += "  AND  a.proveedor            = '" +  propietario        +  "'" ;
             where += "  AND  a.banco||a.sucursal    = '" +  banco + sucursal   +  "'" ;
             where += "  AND  a.agencia " + agencia;
              
                   
            
        }catch(Exception e){
            throw new Exception ( e.getMessage() );
        }
        return  where;
    }
    
    
    
    
}
