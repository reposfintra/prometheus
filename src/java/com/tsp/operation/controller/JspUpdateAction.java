/********************************************************************
 *      Nombre Clase.................   JspUpdateAction.java    
 *      Descripci�n..................   Actualiza un campo en el archivo de campos_jsp.
 *      Autor........................   Ing. Rodrigo Salazar
 *      Fecha........................   18.07.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

/**
 *
 * @author  Rodrigo
 */
public class JspUpdateAction extends Action{
    
    /** Creates a new instance of JspUpdateAction */
    public JspUpdateAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next="/jsp/trafico/permisos/jsp/JspModificar.jsp?lista=ok&reload=ok&mensaje=Pagina Modificada Exitosamente";
        HttpSession session = request.getSession();        
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String codigo = request.getParameter("c_codigo");
        String nombre = request.getParameter("c_nombre");
        String ruta = request.getParameter("c_ruta");
        String descripcion = request.getParameter("c_descripcion");
        try{
            Jsp js = new Jsp();
            js.setCodigo(codigo);
            js.setNombre(nombre);
            js.setRuta(ruta);
            js.setDescripcion(descripcion);
            js.setUser_update(usuario.getLogin().toUpperCase());
            js.setCia(usuario.getCia().toUpperCase());
            model.jspService.modificarJsp(js);
            model.jspService.serchJsp(codigo);
            Jsp jsp2 = model.jspService.getJsp();
            request.setAttribute("jsp",jsp2);
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);    
    }    
}
