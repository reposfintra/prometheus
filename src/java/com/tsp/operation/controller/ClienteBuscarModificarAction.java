/*******************************************************************
 * Nombre clase: PlacaBuscarAction.java
 * Descripci�n: Accion para buscar una placa de la bd.
 * Autor: Ing. Jose de la rosa
 * Fecha: 16 de febrero de 2006, 05:41 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import com.tsp.exceptions.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
/**
 *
 * @author  jdelarosa
 */
public class ClienteBuscarModificarAction extends Action{
    
    /** Creates a new instance of PlacaBuscarAction */
    public ClienteBuscarModificarAction () {
    }
    
    public void run () throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "";
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario)session.getAttribute ("Usuario");
        String Codigo = request.getParameter ("Codigo")!=null?request.getParameter ("Codigo").toUpperCase():"";
        Vector vector = new Vector();
        try{
            

                /******************************************************/
            model.clienteService.buscarClienteModificar(Codigo);
           
            vector = model.clienteService.getVector();
            next = "/jsp/sot/body/ModificarCliente.jsp";
            if ( vector.size()> 0){
                next+="?opcion=1";
                model.clienteService.setCedula("");
                model.clienteService.setTreeMapClientes();
                
               BeanGeneral info = (BeanGeneral) vector.elementAt(0);
               List ListaAgencias = model.clienteService.listarAgecniaFacturacion();
                TreeMap tm = new TreeMap();
                String agenciaF = info.getValor_23();
                 if(agenciaF.equals("OP")){
                    agenciaF = "";
                }
                if(ListaAgencias.size()>0) {
                    Iterator It3 = ListaAgencias.iterator();
                    while(It3.hasNext()) {
                        Ciudad  datos2 =  (Ciudad) It3.next();
                        tm.put("["+datos2.getCodCiu()+"] "+datos2.getNomCiu(), datos2.getCodCiu());
                        //out.print("<option value='"+datos2.getCodCiu()+"'>["+datos2.getCodCiu()+"] "+datos2.getNomCiu()+"</option> \n");
                    }
                }
                    request.setAttribute("agencias", ListaAgencias);
                    request.setAttribute("agencias_tm", tm);
                 
                    model.ciudadService.loadCiudadesC(info.getValor_45());
                    System.out.println("pais de factura  "+info.getValor_46());
                    model.ciudadService.loadCiudadesCE(info.getValor_46());
                    model.servicioBanco.loadBancos(agenciaF, (String) session.getAttribute("Distrito"));
                    model.servicioBanco.setSucursal(new TreeMap());

                    model.servicioBanco.loadSucursalesAgencia(agenciaF, info.getValor_18(), (String) session.getAttribute("Distrito"));
                    TreeMap SucBanco = model.servicioBanco.getSucursal();
                        
            
            }else {
                next = "/jsp/sot/body/BusquedaCliente.jsp";
                next+="?msg=No se encontro ningun resultado";
            }
           
        }catch (Exception e){
            e.printStackTrace ();
        }
        
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest (next);
    }
}
