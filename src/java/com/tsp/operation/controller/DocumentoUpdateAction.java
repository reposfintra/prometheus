/********************************************************************
 *      Nombre Clase.................   DocumentoInsertAction.java    
 *      Descripci�n..................   Actualiza un registro en la tabla tbldoc    
 *      Autor........................   Tito Andr�s Maturana
 *      Fecha........................   13.10.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;

public class DocumentoUpdateAction extends Action{
        
        /** Creates a new instance of DocumentoInsertAction */
        public DocumentoUpdateAction() {
        }
        
        public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
                //Pr�xima vista
                String pag  = "/jsp/masivo/tbldoc/DocumentoUpdate.jsp?mensaje=";
                String next = "";
                
                //Usuario en sesi�n
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario) session.getAttribute("Usuario");

                try{   
                        Documento doc = new Documento();
                        doc.setBase(usuario.getBase());//request.getParameter("
                        doc.setC_banco(request.getParameter("c_banco"));
                        doc.setC_document_name(request.getParameter("c_document_name"));
                        doc.setC_document_type(request.getParameter("c_document_type"));
                        doc.setC_ndocument_type(request.getParameter("c_ndocument_type"));
                        doc.setC_ref_1((request.getParameter("c_ref_1")==null)? "" : request.getParameter("c_ref_1") );
                        doc.setC_sigla(request.getParameter("c_sigla"));
                        doc.setDistrito(request.getParameter("c_cia"));
                        doc.setUsuario_modificacion(usuario.getLogin());
                        doc.setEstado("");
                        
                        
                        model.documentoSvc.obtenerDocumento(doc.getDistrito(), doc.getC_ndocument_type());
                        Documento existe = model.documentoSvc.getDocumento();
                        
                        if ( existe!=null && !doc.getC_document_type().matches(doc.getC_ndocument_type()) ){
                                pag += "?msg=No se puede procesar la informaci�n. Ya existe el documento.";                                                                
                        }
                        else{
                                model.documentoSvc.actualizarDocumento(doc);      
                                pag+= "MsgModificado&msg=Se ha modificado el documento exitosamente.";
                        }
                        
                        model.documentoSvc.setDocumento(doc);
                        
                        next = com.tsp.util.Util.LLamarVentana(pag, "Actualizar Documento");

                }catch (SQLException e){
                       throw new ServletException(e.getMessage());
                }

                this.dispatchRequest(next);
        }
        
}
