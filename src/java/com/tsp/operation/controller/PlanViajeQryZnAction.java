/**
 *
 */
package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import java.util.*;
import java.text.*;



/**
 * Searches the database for puchase orders matching the
 * purchase search argument
 */
public class PlanViajeQryZnAction extends Action {
    static Logger logger = Logger.getLogger(PlanViajeQryZnAction.class);
    
    /**
     * Executes the action
     */
    public void run() throws ServletException, InformationException {
        String next = "";
        String fechaini;
        String fechafin;
        String zona;
      
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        Calendar fecha = Calendar.getInstance();
        SimpleDateFormat FFormat = new SimpleDateFormat("yyyyMMdd"); 
        fechaini = request.getParameter("fechaini");
        fechafin = request.getParameter("fechafin");
        zona = request.getParameter("zona");

        fechaini = fechaini.replace("-", "");
        
        fechafin = fechafin.replace("-", "");
        
        if (fechaini.equals("")){
            fechaini = FFormat.format(fecha.getTime());
        }

        if (fechafin.equals("")){
            fechafin = FFormat.format(fecha.getTime());
        }

        if (zona.equals("")){
            zona = "%";
        }

        try{
           model.planViajeService.planViajeQryZn(fechaini, fechafin, zona, usuario.getDpto());
           next = "/planviaje/PantallaFiltroPlanViaje.jsp";
           logger.info(usuario.getNombre() + " ejecuto consulta planViaje con:Fecha Inicial: "+fechaini+", Fecha Final: "+fechafin+", Zona: " + zona);
        }
        catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);    
    }
}
