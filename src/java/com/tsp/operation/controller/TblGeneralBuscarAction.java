/******************************************************************
* Nombre ......................TblGeneralBuscarAction.java
* Descripci�n..................Clase Action para descuento de equipos
* Autor........................Armando Oviedo
* Fecha........................15/12/2005
* Versi�n......................1.0
* Coyright.....................Transportes Sanchez Polo S.A.
*******************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;
/**
 *
 * @author  Armando Oviedo
 */
public class TblGeneralBuscarAction extends Action{
    
    /** Creates a new instance of TblGeneralBuscarAction */
    public TblGeneralBuscarAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "/jsp/masivo/tblgeneral/item.jsp";
        try{
            String mensaje = request.getParameter("mensaje");
            if(mensaje!=null){
                if(mensaje.equalsIgnoreCase("listartodos")){
                    next = "/jsp/masivo/tblgeneral/TblGeneralListar.jsp";
                  //  model.tblgensvc.buscarTodosTblGeneral();
                }
                else if(mensaje.equalsIgnoreCase("listar2")){
                    String codigo = request.getParameter("codigo");
                    String codtabla = request.getParameter("codtabla");
                    TblGeneral tmp = new TblGeneral();
                    tmp.setCodigo(codigo);
                    tmp.setCodigoTabla(codtabla);
                    model.tblgensvc.setTblGeneral(tmp);
                   // model.tblgensvc.listarPorCodigo(); 
                    next = "/jsp/masivo/tblgeneral/items.jsp?codtabla="+tmp.getCodigoTabla()+"&codigo="+tmp.getCodigo();
                    ////System.out.println("Next--> " + next);
                }
                else if(mensaje.equalsIgnoreCase("listar")){
                    String codigo = request.getParameter("codigo");
                    String codtabla = request.getParameter("codtabla");
                    TblGeneral tmp = new TblGeneral();
                    tmp.setCodigo(codigo);
                    tmp.setCodigoTabla(codtabla);
                    model.tblgensvc.setTblGeneral(tmp);
                   // model.tblgensvc.listarPorCodigo(); 
                    next = next + "?codtabla="+tmp.getCodigoTabla()+"&codigo="+tmp.getCodigo();
                    ////System.out.println("Next--> " + next);
                }
                
            }
        }
        catch(Exception ex){
            ex.printStackTrace();
        }
        this.dispatchRequest(next);
    }
    
}
