

package com.tsp.operation.controller;
import com.lowagie.text.Document;
import com.lowagie.text.Font;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.tsp.util.Util;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.util.ExcelApplication;
import java.io.FileOutputStream;
import java.text.DecimalFormat;
import javax.servlet.http.HttpSession;
/**
 *
 * @author rhonalf
 */
public class GestionCarteraAction extends Action {

    GestionCarteraService cserv = null;
    Usuario user;

    @Override
    public void run(){
        String next = "/jsp/fenalco/clientes/";
        String opcion = "";
        String cadenatabla = "";
        String img="<img src='" + request.getContextPath() +"/images/images.jpg' width='15' height='15' align='left' title='Ver Historial ' />";
        
        boolean redirect = false;
        try {
            opcion = request.getParameter("opcion")!= null ? request.getParameter("opcion"):"";
            HttpSession misesion = request.getSession();
            user = (Usuario) misesion.getAttribute("Usuario");
            cserv = new GestionCarteraService(user.getBd());
            if(opcion.equals("buscacli")){
                redirect = false;
                ArrayList<ClienteNeg> lista = null;
                ClienteNeg cliente = null;
                String filtro = request.getParameter("filtro")!= null ? request.getParameter("filtro"):"nit";
                String cadena = request.getParameter("cadena")!= null ? request.getParameter("cadena"):"";
                String tipodoc = request.getParameter("titulo")!= null ? request.getParameter("titulo"):"";
                String afil = request.getParameter("afiliado")!= null ? request.getParameter("afiliado"):"";
                String conv = request.getParameter("convenio")!= null ? request.getParameter("convenio"):"";
                String sect = request.getParameter("sector")!= null ? request.getParameter("sector"):"";
                String subsect = request.getParameter("subsector")!= null ? request.getParameter("subsector"):"";
                String tiposal = request.getParameter("tiposal")!= null ? request.getParameter("tiposal"):"1";
                String fecha = request.getParameter("fecha")!= null ? request.getParameter("fecha"):"";
                boolean pasa = true;
                try {
                    String parms = "";
                    if(conv.equals("")==false) parms += " AND n.id_convenio="+conv;
                    if(afil.equals("")==false) parms += " AND n.nit_tercero="+afil;
                    if(sect.equals("")==false) parms += " AND n.cod_sector='"+sect+"'";
                    if(subsect.equals("")==false) parms += " AND n.cod_subsector='"+subsect+"'";
                    String tiposal2 = "";
                    if(tiposal.equals("1")) tiposal2 = "AND f.valor_saldo>0";
                    lista = cserv.buscarDatos(filtro, cadena, tipodoc,parms,tiposal2,fecha);
                }
                catch (Exception e) {
                    pasa = false;
                    System.out.println("Error al llenar la lista de clientes: "+e.toString());
                    e.printStackTrace();
                }
                
                cadenatabla = "<table cellspacing='1' id='tabla' width='100%' style='cursor:pointer; background-color:#003D00'>" + //<!-- mod -->
                                            "<thead class='fixedHeader'>" +
                                                "<tr class='subtitulo1'>" +

                                                    "<th>Codigo</th>" +
                                                    "<th>Nit</th>" +
                                                    "<th>Nombre</th>" +
                                                    "<th>Telefono</th>" +
                                                    "<th>E-mail</th>" +
                                                    "<th>Direccion</th>" +
                                                    "<th>Dias venc.</th>" +
                                                    "<th>Saldo venc.</th>" +
                                                    "<th>Fecha venc.</th>" +
                                                    "<th>Ult. gestion</th>" +
                                                    "<th>Prox. accion</th>" +
                                                "</tr>" +
                                            "</thead>";
                cadenatabla += "<tbody >";
                if(pasa==true && lista.size()>0){
                    for (int i = 0; i < lista.size(); i++) {
                        cliente = lista.get(i);
                        cadenatabla += "<tr class='xfila' id=\""+"tr"+i+"\" onmouseover='Tr_Format(\""+"tr"+i+"\");'  onmouseout='Tr_No_Format(\""+"tr"+i+"\");'   >" + //<!-- mod -->
                        "<td class='codigo' onclick='cargarHistorial(\""+ cliente.getNit()  +"\",\""+ cliente.getCodcli()  +"\");'style='padding-left:20px;' >" +  img  + cliente.getCodcli() +"</td>" +
                       "<td style='padding-left:20px;' onclick='cargarNegocios(\""+ cliente.getNit() +"\",\""+ cliente.getNomcli() +"\");' >"+ cliente.getNit() +"</td>" +
                       "<td style='padding-left:20px;' onclick='cargarNegocios(\""+ cliente.getNit() +"\",\""+ cliente.getNomcli() +"\");' >"+ cliente.getNomcli() +"</td>" +
                       "<td style='padding-left:20px;' onclick='cargarNegocios(\""+ cliente.getNit() +"\",\""+ cliente.getNomcli() +"\");' >"+ cliente.getTelefono() +"</td>" +
                       "<td style='padding-left:20px;' onclick='cargarNegocios(\""+ cliente.getNit() +"\",\""+ cliente.getNomcli() +"\");'>"+ cliente.getEmail() +"</td>" +
                       "<td style='padding-left:20px;' >"+ cliente.getDireccion() +"</td>" +
                       "<td style='padding-left:20px;' >"+ (cliente.getDvenc()==0?"":cliente.getDvenc()) +"</td>" +
                       "<td align='right'>"+ Util.customFormat(cliente.getSaldo_vencido()) +"</td>" +
                       "<td style='padding-left:20px;' >"+ cliente.getFecha_next_venc() +"</td>" +
                       "<td style='padding-left:20px;'>"+ cliente.getFecha_ult_gestion() +"</td>" +
                       "<td  onclick='cargarNegocios(\""+ cliente.getNit() +"\",\""+ cliente.getNomcli() +"\");' >"+ cliente.getFecha_prox_gestion() +"</td>" +
                       "</tr>";
                        cliente = null;
                    }
                }
                else{
                    cadenatabla += "<tr class='xfila'><td colspan='10' align='center'>No se encontraron registros</td></tr>"; //<!-- mod -->
                }
                cadenatabla += "</tbody></table>";
            }
            else if(opcion.equals("buscaneg")){
                redirect = false;
                ArrayList<Negocios> lista = null;
                Negocios negocio = null;
                String tipodoc = request.getParameter("titulo")!= null ? request.getParameter("titulo"):"";
                String nit = request.getParameter("nit")!= null ? request.getParameter("nit"):"";
                String fecha = request.getParameter("fecha")!= null ? request.getParameter("fecha"):"";
                boolean pasa = true;
                // question = ( to ) ? be : ! be; -- W. Shakespeare
                try {
                    lista = cserv.listaNegocios(nit, tipodoc,fecha);
                }
                catch (Exception e) {
                    pasa = false;
                    System.out.println("Error al llenar la lista de negocios: "+e.toString());
                    e.printStackTrace();
                }
                cadenatabla = "<table id='tablaneg' width='100%' style='border-collapse: collapse; cursor:pointer;'>" + //new mod
                                            "<thead>" +
                                                "<tr class='subtitulo1'>" +
                                                    "<th style='border: 0.1em solid black;'>Negocio</th>" +
                                                    "<th style='border: 0.1em solid black;'>Tipo</th>" +
                                                    "<th style='border: 0.1em solid black;'>Afiliado</th>" +
                                                    "<th style='border: 0.1em solid black;'>Cuotas</th>" +
                                                    "<th style='border: 0.1em solid black;'>Pagas</th>" +
                                                    "<th style='border: 0.1em solid black;'>Vencidas</th>" +
                                                    "<th style='border: 0.1em solid black;'>Vigentes</th>" +
                                                    "<th style='border: 0.1em solid black;'>Cartera</th>" +
                                                    "<th style='border: 0.1em solid black;'>Desembolso</th>" +
                                                    "<th style='border: 0.1em solid black;'>Saldo</th>" +
                                                    "<th style='border: 0.1em solid black;'>Cuota</th>" +
                                                    "<th style='border: 0.1em solid black;'>Vencido</th>" +
                                                    "<th style='border: 0.1em solid black;'>A Pagar</th>" +
                                                    "<th style='border: 0.1em solid black;'>+Vencida</th>" +
                                                    "<th style='border: 0.1em solid black;'>Fecha</th>" +//2010-10-04
                                                    "<th style='border: 0.1em solid black;'>Vencimiento</th>" +
                                                    "<th style='border: 0.1em solid black;'>Dias</th>" +
                                                "</tr>" +
                                            "</thead>";
                cadenatabla += "<tbody>";
                if(pasa==true && lista.size()>0){
                    DecimalFormat form = new DecimalFormat("#,###,###,###.##");
                    double desembolso = 0;
                    double cartera = 0;
                    double saldo = 0;
                    double cuotas = 0;
                    double vencido = 0;
                    double pagar = 0;
                    for (int i = 0; i < lista.size(); i++) {
                        negocio = lista.get(i);
                        String venc_c = "0";
                        String venc_v = "0";
                        String pagas_c = "0";
                        String vig_c = "0";
                        try {
                            BeanGeneral bx = cserv.datosFacturasNegocio(negocio.getCod_negocio());
                            try {
                                String[] split;
                                split = (bx.getValor_01()).split(";_;");
                                pagas_c = split[0];
                                if(split[1].equals("Vencidas")){
                                    venc_v = split[2];
                                }
                                split = (bx.getValor_02()).split(";_;");
                                venc_c = split[0];
                                if(split[1].equals("Vencidas")){
                                    venc_v = split[2];
                                }
                                split = (bx.getValor_03()).split(";_;");
                                vig_c = split[0];
                                if(split[1].equals("Vencidas")){
                                    venc_v = split[2];
                                }
                            }
                            catch (Exception e) {
                                System.out.println("error al procesar valores: "+e.toString());
                                e.printStackTrace();
                            }
                        }
                        catch (Exception e) {
                            System.out.println("error buscando datos: "+e.toString());
                            e.printStackTrace();
                        }
                        try {
                            desembolso += negocio.getVr_desem();
                            cartera += negocio.getTotpagado();
                            saldo += negocio.getSaldoneg();
                            cuotas += negocio.getValor_cuota();
                            vencido += negocio.getSaldo_ven();
                            pagar += negocio.getSaldo_pagar();
                        }
                        catch (Exception e) {
                            System.out.println("error sumando datos: "+e.toString());
                            e.printStackTrace();
                        }
                        cadenatabla += "<tr class='xfila' onclick='window.open(\""+(request.getContextPath())+"/jsp/fenalco/clientes/facturas_cartera.jsp?codigoneg="+ (negocio.getCod_negocio()) +"&cartera="+(negocio.getTotpagado())+"\",\"\", \"fullscreen=yes, scrollbars=auto\");'  style='text-decoration: none; cursor:pointer;'>" + //<!-- mod -->
                                                    "<td style='border: 0.1em solid black;'>"+ negocio.getCod_negocio() +"</td>" +
                                                    "<td style='border: 0.1em solid black;'>"+ negocio.getConcepto() +"</td>" +
                                                    "<td style='border: 0.1em solid black;'>"+ negocio.getNombreafil() +"</td>" +
                                                    "<td align='right' style='border: 0.1em solid black;'>"+ negocio.getCuotas() +"</td>" +
                                                    "<td align='right' style='border: 0.1em solid black;'>"+ negocio.getCuotas_pagas() +"</td>" +
                                                    "<td align='right' style='border: 0.1em solid black;'>"+ negocio.getCuotas_ven() +"</td>" +
                                                    "<td align='right' style='border: 0.1em solid black;'>"+ negocio.getCuotas_vig() +"</td>" +
                                                    "<td align='right' style='border: 0.1em solid black;'>"+ form.format(negocio.getTotpagado()) +"</td>" +
                                                    "<td align='right' style='border: 0.1em solid black;'>"+ form.format(negocio.getVr_desem()) +"</td>" +
                                                    "<td align='right' style='border: 0.1em solid black;'>"+ form.format(negocio.getSaldoneg()) +"</td>" +
                                                    "<td align='right' style='border: 0.1em solid black;'>"+ form.format(negocio.getValor_cuota()) +"</td>" +
                                                    "<td align='right' style='border: 0.1em solid black;'>"+ form.format(negocio.getSaldo_ven()) +"</td>" +
                                                    "<td align='right' style='border: 0.1em solid black;'>"+ form.format(negocio.getSaldo_pagar()) +"</td>" +
                                                    "<td style='border: 0.1em solid black;'>"+ negocio.getFactura_vencer() +"</td>" +
                                                    "<td style='border: 0.1em solid black;'>"+ negocio.getFecha_neg() +"</td>" + //2010-10-04
                                                    "<td style='border: 0.1em solid black;'>"+ negocio.getFecha_vencimiento() +"</td>" +
                                                    "<td align='right' style='border: 0.1em solid black;'>"+ negocio.getNodocs() +"</td>" +
                                               "</tr>";
                        negocio = null;
                    }
                    //ahora se totalizan los valores
                    cadenatabla += "</tbody><tfoot>"
                                        + "<tr class='xfila' style='border: 0em solid black;'>"
                                            + "<td style='border: 0em solid black;'>&nbsp;</td>"
                                            + "<td style='border: 0em solid black;'>&nbsp;</td>"
                                            + "<td style='border: 0em solid black;'>&nbsp;</td>"
                                            + "<td style='border: 0em solid black;'>&nbsp</td>"
                                            + "<td style='border: 0em solid black;'>&nbsp</td>"
                                            + "<td style='border: 0em solid black;'>&nbsp</td>"/**/
                                            + "<td align='left' style='border: 0.1em solid black; font-weigth: bold;'>TOTALES</td>"
                                            //+ "<td align='right' style='border: 0.1em solid black; font-weigth: bold;'>Cartera</td>"
                                            + "<td align='right' style='border: 0.1em solid black;'>"+form.format(cartera)+"</td>"
                                            //+ "<td align='right' style='border: 0.1em solid black; font-weigth: bold;'>Desembolso</td>"
                                            + "<td align='right' style='border: 0.1em solid black;'>"+form.format(desembolso)+"</td>"
                                            //+ "<td align='right' style='border: 0.1em solid black; font-weigth: bold;'>Saldo</td>"
                                            + "<td align='right' style='border: 0.1em solid black;'>"+form.format(saldo)+"</td>"
                                            //+ "<td align='right' style='border: 0.1em solid black; font-weigth: bold;'>Cuota</td>"
                                            + "<td align='right' style='border: 0.1em solid black;'>"+form.format(cuotas)+"</td>"
                                            //+ "<td align='right' style='border: 0.1em solid black; font-weigth: bold;'>Vencido</td>"
                                            + "<td align='right' style='border: 0.1em solid black;'>"+form.format(vencido)+"</td>"
                                            //+ "<td align='right' style='border: 0.1em solid black; font-weigth: bold;'>Pagar</td>"
                                            + "<td align='right' style='border: 0.1em solid black;'>"+form.format(pagar)+"</td>"
                                            + "<td style='border: 0em solid black;'>&nbsp;</td>"
                                            + "<td style='border: 0em solid black;'>&nbsp;</td>"
                                            + "<td style='border: 0em solid black;'>&nbsp;</td>"
                                            + "<td style='border: 0em solid black;'>&nbsp;</td>"/**/
                                       + "</tr>"
                                   + "</tfoot></table>";
                    //preguntamos si el nit pertenece a cliente "especial"
                    if(cserv.clienteEspecial(nit)){
                        //aqui genero la fila con el link si es el cliente "especial" para consultar todo lo que sea pf,ff
                        cadenatabla += "<div style='height:0.2em;'></div><table width='100%' style='border-collapse: collapse; padding: 0.1em;'>"
                                        + "<tr class='xfila'>"
                                                + "<td align='center' style='border: 0.1em solid black;'>"
                                                    + "<span onclick='window.open(\""+request.getContextPath()+"/jsp/fenalco/clientes/facturas_cartera.jsp?verpfs=true&filt=VIGENTE\",\"\", \"fullscreen=yes, scrollbars=auto\");'  style='text-decoration: none; color: green; cursor:pointer;'>"
                                                    + "Titulos pendientes de indemnizacion"
                                                    + "</span>"
                                                + "</td>"
                                            + "</tr>";
                        cadenatabla += "</table>";
                    }
                }
                else{
                    cadenatabla += "<tr class='xfila'><td colspan='10' align='center'>No se encontraron registros</td></tr>";
                    cadenatabla += "</tbody></table>";
                }
                //cadenatabla += "</tbody></table>";
            }
            else if(opcion.equals("facturafiltro")){
                redirect=false;
                ArrayList<factura> lista = null;
                String filtro = request.getParameter("filtro")!= null ? request.getParameter("filtro"):"";
                String cadena = request.getParameter("cadena")!= null ? request.getParameter("cadena"):"";
                boolean pasa = true;
                factura fact = null;
                try {
                    lista = cserv.facturasNegocio(cadena, filtro);
                }
                catch (Exception e) {
                    pasa = false;
                    System.out.println("Error al llenar la lista de facturas: "+e.toString());
                    e.printStackTrace();
                }
                //String cadAsc = "background:url("+request.getContextPath()+"/images/asc.gif) 7px no-repeat;";
                String cadAsc = "class='asc'";
                //String cadSort = "background:url("+request.getContextPath()+"/images/sort.gif) 7px no-repeat;";
                String cadSort = " class='head'";
                cadenatabla = "<div>" +
                                        "<table border='0' width='100%' style='border-collapse: collapse'>" +
                                            "<tr class='fila'>" +
                                                "<th>Filtro</th>" +
                                                "<th colspan='11' align='left'>" +
                                                    "<select id='filtro' name='filtro' onchange='facturafiltro(\""+cadena+"\",this.selectedIndex);'>" +
                                                        //"<option value=''"+(filtro.equals("")?" selected":"")+">...</option>" +
                                                        "<option value='VIGENTE'"+(filtro.equals("VIGENTE")?" selected":"")+">Vigentes</option>" +
                                                        "<option value='CANCELADA'"+(filtro.equals("CANCELADA")?" selected":"")+">Canceladas</option>" +
                                                        "<option value='TODAS'"+(filtro.equals("TODAS")?" selected":"")+">Todas</option>"+
                                                        "<option value='SINIESTRADA'"+(filtro.equals("SINIESTRADA")?" selected":"")+">Siniestrada</option>"+
                                                        "<option value='NEGOCIADA'"+(filtro.equals("NEGOCIADA")?" selected":"")+">Negociada</option>"+
                                                    "</select>" +
                                                    "<span class='gestionspan' onclick='winGestionFact2();'>Gestionar Factura(s)</span>" +
                                                "</th>" +
                                            "</tr>" +
                                            "<tr class='subtitulo1' align='center' style='cursor:pointer;'>" +
                                                "<th width='8%' onclick='organizar(\""+cadena+"\",\"f.documento\",\"ASC\",1);' "+cadSort+">Factura</th>" +
                                                "<th width='7%' onclick='organizar(\""+cadena+"\",\"fd.numero_remesa\",\"ASC\",2);' "+cadSort+">Relacionada</th>" +
                                                "<th width='6%' onclick='organizar(\""+cadena+"\",\"f.num_doc_fen\",\"ASC\",3);' "+cadSort+">No. titulo</th>" +
                                                "<th width='7%' onclick='organizar(\""+cadena+"\",\"f.fecha_vencimiento\",\"DESC\",4);' "+cadAsc+">Vencimiento</th>" +
                                                "<th width='7%' onclick='organizar(\""+cadena+"\",\"dias_mora\",\"ASC\",5);' "+cadSort+">Dias mora</th>" +
                                                "<th width='9%' onclick='organizar(\""+cadena+"\",\"ndiasf\",\"ASC\",6);' "+cadSort+">Venc. prejuridico</th>" +
                                                "<th width='6%' onclick='organizar(\""+cadena+"\",\"estadofact\",\"ASC\",7);' "+cadSort+">Estado</th>" +
                                                "<th width='7%' onclick='organizar(\""+cadena+"\",\"f.fecha_ultimo_pago\",\"ASC\",8);' "+cadSort+">Ult. abono</th>" +
                                                "<th width='7%' onclick='organizar(\""+cadena+"\",\"f.valor_factura\",\"ASC\",10);' "+cadSort+">Valor</th>" +
                                                "<th width='7%' onclick='organizar(\""+cadena+"\",\"f.valor_abono\",\"ASC\",9);' "+cadSort+">Abonos</th>" +
                                                "<th width='8%' onclick='organizar(\""+cadena+"\",\"f.valor_saldo\",\"ASC\",11);' "+cadSort+">Saldo</th>" +
                                                "<th width='6%'>% cobro</th>" +
                                                "<th width='8%'>Int. mora</th>" +
                                                "<th width='14%'>Vr cobro</th>" +
                                            "</tr>" +
                                        "</table>";
                cadenatabla += "<div style='overflow: auto; height: 265px;'>" +
                                            "<table border='0' width='100%' style='border-collapse: collapse'>" +
                                                "<tbody>";
                if(pasa==true && lista.size()>0){
                    DecimalFormat form = new DecimalFormat("#,###,###,###.##");
                    for (int i = 0; i < lista.size(); i++) {
                        fact = lista.get(i);
                         double cobro=0;
                         double tcobro=0;
                             if(fact.getDias_mora()<=0){
                                 cobro=0;
                                 }else if(fact.getDias_mora()>0&&fact.getDias_mora()<4){
                                 cobro=2;
                                 }
                               else if(fact.getDias_mora()>3&&fact.getDias_mora()<8){
                                 cobro=15;
                                 }
                               else if(fact.getDias_mora()>7&&fact.getDias_mora()<51){
                                 cobro=20;
                                 }
                               else if(fact.getDias_mora()>50&&fact.getDias_mora()<91){
                                 cobro=25;
                                 }
                               else if(fact.getDias_mora()>90){
                                 cobro=40;
                                 }
                               tcobro = (cobro / 100)*fact.getValor_saldo();
                        cadenatabla += "<tr class='xfila'>" +
                                                    "<td width='7%'>" +
                                                        "<input type='checkbox' id='fact"+i+"' name='fact"+i+"' value='"+fact.getDocumento()+"'>" +
                                                        "<img alt='mostrar ingresos' src='"+request.getContextPath()+"/images/botones/iconos/expand.gif' name='imgexpand"+i+"' id='imgexpand"+i+"' onclick='expandir("+i+",\""+ fact.getDocumento() + "\")' style='cursor:pointer'>" +
                                                        "<span style='cursor:pointer;' onclick='winGestionFact(\""+fact.getDocumento()+"\",\""+Math.ceil(fact.getValor_saldo()+fact.getValor_saldome()+tcobro)+"\" );'>"+ fact.getDocumento() + "</span>" +
                                                    "</td>" +
                                                    "<td width='8%' align='center'>"+ fact.getFactura() +"</td>" +
                                                    "<td width='7%' align='center'>"+ fact.getFisico() +"</td>" +
                                                    "<td width='9%' align='center'>"+ fact.getFecha_vencimiento() +"</td>" +
                                                    "<td width='7%' align='center'>"+ fact.getDias_mora() +"</td>" +
                                                    "<td width='11% align='center'>"+ fact.getFecha_ven_prejurid() +"</td>" +
                                                    "<td width='6%' align='center'>"+ fact.getEstado() +"</td>" +
                                                    "<td width='9%' align='center'>"+ fact.getFecha_ultimo_pago() +"</td>" +
                                                    "<td width='9%' align='right'>$ "+ form.format(fact.getValor_factura()) +"</td><input type='hidden' id='vcuot"+i+"' name='vcuot"+i+"' value='"+ fact.getValor_factura() +"'>" +
                                                    "<td width='7%' align='right'>$ "+ form.format(fact.getValor_abono()) +"</td>" +
                                                    "<td width='9%' align='right'>$ "+ form.format(fact.getValor_saldo()) +"</td>" +
                                                    "<td width='8%' align='right'><input type='text' id='porc"+i+"' name='porc"+i+"' value='"+cobro+"' onkeyup='calculate(this.value,"+i+");' size='5' style='text-align: right;'></td>" +
                                                    "<td width='10%' align='right'><input type='text' id='morax"+i+"' name='morax"+i+"' value='"+form.format(Math.ceil(fact.getValor_saldome()))+"' size='10' style='text-align: right;' readonly><input type='hidden' id='mora"+i+"' name='mora"+i+"' value='"+ fact.getValor_saldome() +"'></td>" +
                                                    "<td width='12%' align='right'><input type='text' id='vx"+i+"' name='vx"+i+"' value='"+form.format(Math.ceil(fact.getValor_saldo() + fact.getValor_saldome()+tcobro))+"' size='10' style='text-align: right;' readonly></td>" +
                                                "</tr>" +
                                                "<tr><td></td><td colspan='13'><div id='diving"+i+"' style='display:none; padding:0px;'></div></td></tr>";
                        fact = null;
                    }
                }
                else{
                    cadenatabla += "<tr class='xfila'><td colspan='14' align='center'>No se encontraron registros</td></tr>";
                }
                cadenatabla += "</tbody></table></div>";
                if(pasa==true && lista.size()>0){
                    cadenatabla += "<div align='center'>"
                                            + "<img alt='generar xls' src='"+request.getContextPath()+"/images/botones/exportarExcel.gif' id='imgexc2' name='imgxls' onMouseOver='botonOver(this);' onClick='generarDoc2(\"xls\",\""+cadena+"\");' onMouseOut='botonOut(this);' style='cursor:pointer'>"
                                            + "<img alt='generar pdf' src='"+request.getContextPath()+"/images/botones/generarPdf.gif' id='imgpdf2' name='imgxls' onMouseOver='botonOver(this);' onClick='generarDoc2(\"pdf\",\""+cadena+"\");' onMouseOut='botonOut(this);' style='cursor:pointer'>"
                                       + "</div>"
                                       + "<div id='mensaje2' align='center'></div>";
                }
                cadenatabla += "</div>";
            }
            else if(opcion.equals("facturafiltropf")){
                redirect=false;
                ArrayList<factura> lista = null;
                String filtro = request.getParameter("filtro")!= null ? request.getParameter("filtro"):"";
                String cadena = request.getParameter("cadena")!= null ? request.getParameter("cadena"):"";
                String col = request.getParameter("col")!= null ? request.getParameter("col"):"f.documento";
                String orden = request.getParameter("orden")!= null ? request.getParameter("orden"):"ASC";
                String num = request.getParameter("num")!= null ? request.getParameter("num"):"1";
                boolean pasa = true;
                factura fact = null;
                filtro = "VIGENTE";
                //String cadAsc = "background:url("+request.getContextPath()+"/images/asc.gif) 7px no-repeat;";
                String cadAsc = "class='asc'";
                //String cadSort = "background:url("+request.getContextPath()+"/images/sort.gif) 7px no-repeat;";
                String cadSort = "class='head'";
                //String cadDesc = "background:url("+request.getContextPath()+"/images/desc.gif) 7px no-repeat;";
                String cadDesc = "class='desc'";
                try {
                    lista = cserv.facturasPf2(filtro, orden, col);
                }
                catch (Exception e) {
                    pasa = false;
                    System.out.println("Error al llenar la lista de facturas: "+e.toString());
                    e.printStackTrace();
                }
                cadenatabla = "<div>" +
                                        "<table border='0' width='100%' style='border-collapse: collapse'>" +
                                            "<tr class='fila'>" +
                                                "<th>Filtro</th>" +
                                                "<th colspan='11' align='left'>" +
                                                    "<select id='filtro' name='filtro' onchange='facturafiltro2(this.selectedIndex);'>" +
                                                        "<option value='VIGENTE'"+(filtro.equals("VIGENTE")?" selected":"")+">Vigentes</option>" +
                                                    "</select>" +
                                                    "<span class='gestionspan' onclick='winGestionFact2();'>Gestionar Factura(s)</span>" +
                                                "</th>" +
                                            "</tr>" +
                                            "<tr class='subtitulo1' align='center' style='cursor:pointer;'>" +
                                                "<th width='8%' onclick='organizar2(\"f.documento\",\""+(num.equals("1")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",1);' "+(num.equals("1")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+">Factura</th>" +
                                                "<th width='7%' onclick='organizar2(\"fd.numero_remesa\",\""+(num.equals("2")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",2);' "+(num.equals("2")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+">Relacionada</th>" +
                                                "<th width='6%' onclick='organizar2(\"f.num_doc_fen\",\""+(num.equals("3")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",3);' "+(num.equals("3")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+">No. titulo</th>" +
                                                "<th width='7%' onclick='organizar2(\"f.fecha_vencimiento\",\""+(num.equals("4")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",4);' "+(num.equals("4")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+">Vencimiento</th>" +
                                                "<th width='7%' onclick='organizar2(\"dias_mora\",\""+(num.equals("5")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",5);' "+(num.equals("5")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+">Dias mora</th>" +
                                                "<th width='9%' onclick='organizar2(\"ndiasf\",\""+(num.equals("6")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",6);' "+(num.equals("6")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+">Venc. prejuridico</th>" +
                                                "<th width='6%' onclick='organizar2(\"estadofact\",\""+(num.equals("7")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",7);' "+(num.equals("7")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+">Estado</th>" +
                                                "<th width='7%' onclick='organizar2(\"f.fecha_ultimo_pago\",\""+(num.equals("8")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",8);' "+(num.equals("8")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+">Ult. abono</th>" +
                                                "<th width='7%' onclick='organizar2(\"f.valor_factura\",\""+(num.equals("10")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",10);' "+(num.equals("10")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+">Valor</th>" +
                                                "<th width='7%' onclick='organizar2(\"f.valor_abono\",\""+(num.equals("9")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",9);' "+(num.equals("9")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+">Abonos</th>" +
                                                "<th width='8%' onclick='organizar2(\"f.valor_saldo\",\""+(num.equals("11")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",11);' "+(num.equals("11")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+">Saldo</th>" +
                                                "<th width='6%'>% cobro</th>" +
                                                "<th width='8%'>Int. mora</th>" +
                                                "<th width='14%'>Vr. cobro</th>" +
                                            "</tr>" +
                                        "</table>";
                cadenatabla += "<div style='overflow: auto; height: 265px;'>" +
                                            "<table border='0' width='100%' style='border-collapse: collapse'>" +
                                                "<tbody>";
                if(pasa==true && lista.size()>0){
                    DecimalFormat form = new DecimalFormat("#,###,###,###.##");
                    for (int i = 0; i < lista.size(); i++) {
                        fact = lista.get(i);
                        cadenatabla += "<tr class='xfila'>" +
                                                    "<td width='7%'>" +
                                                        "<input type='checkbox' id='fact"+i+"' name='fact"+i+"' value='"+fact.getDocumento()+"'>" +
                                                        "<img alt='mostrar ingresos' src='"+request.getContextPath()+"/images/botones/iconos/expand.gif' name='imgexpand"+i+"' id='imgexpand"+i+"' onclick='expandir("+i+",\""+ fact.getDocumento() + "\")' style='cursor:pointer'>" +
                                                        "<span style='cursor:pointer;' onclick='winGestionFact(\""+fact.getDocumento()+"\",\""+Math.ceil(fact.getValor_saldo()+fact.getValor_saldome())+"\");'>"+ fact.getDocumento() + "</span>" +
                                                    "</td>" +
                                                    "<td width='8%' align='center'>"+ fact.getFactura() +"</td>" +
                                                    "<td width='7%' align='center'>"+ fact.getFisico() +"</td>" +
                                                    "<td width='9%' align='center'>"+ fact.getFecha_vencimiento() +"</td>" +
                                                    "<td width='7%' align='center'>"+ fact.getDias_mora() +"</td>" +
                                                    "<td width='11% align='center''>"+ fact.getFecha_ven_prejurid() +"</td>" +
                                                    "<td width='6%' align='center'>"+ fact.getEstado() +"</td>" +
                                                    "<td width='9%' align='center'>"+ fact.getFecha_ultimo_pago() +"</td>" +
                                                    "<td width='9%' align='right'>$ "+ form.format(fact.getValor_factura()) +"</td><input type='hidden' id='vcuot"+i+"' name='vcuot"+i+"' value='"+ fact.getValor_factura() +"'>" +
                                                    "<td width='7%' align='right'>$ "+ form.format(fact.getValor_abono()) +"</td>" +
                                                    "<td width='9%' align='right'>$ "+ form.format(fact.getValor_saldo()) +"</td>" +
                                                    "<td width='8%' align='right'><input type='text' id='porc"+i+"' name='porc"+i+"' value='0' onkeyup='calculate(this.value,"+i+");' size='5' style='text-align: right;'></td>" +
                                                    "<td width='10%' align='right'><input type='text' id='morax"+i+"' name='morax"+i+"' value='"+form.format(Math.ceil(fact.getValor_saldome()))+"' size='10' style='text-align: right;' readonly><input type='hidden' id='mora"+i+"' name='mora"+i+"' value='"+ fact.getValor_saldome() +"'></td>" +
                                                    "<td width='12%' align='right'><input type='text' id='vx"+i+"' name='vx"+i+"' value='"+form.format(Math.ceil(fact.getValor_saldo() + fact.getValor_saldome()))+"' size='10' style='text-align: right;' readonly></td>" +
                                                "</tr>" +
                                                "<tr><td></td><td colspan='13'><div id='diving"+i+"' style='display:none; padding:0px;'></div></td></tr>";
                        fact = null;
                    }
                }
                else{
                    cadenatabla += "<tr class='xfila'><td colspan='14' align='center'>No se encontraron registros</td></tr>";
                }
                cadenatabla += "</tbody></table></div>";
                /*if(pasa==true && lista.size()>0){
                    cadenatabla += "<div align='center'>"
                                            + "<img alt='generar xls' src='"+request.getContextPath()+"/images/botones/exportarExcel.gif' id='imgexc2' name='imgxls' onMouseOver='botonOver(this);' onClick='generarDoc2(\"xls\",\""+cadena+"\");' onMouseOut='botonOut(this);' style='cursor:pointer'>"
                                            + "<img alt='generar pdf' src='"+request.getContextPath()+"/images/botones/generarPdf.gif' id='imgpdf2' name='imgxls' onMouseOver='botonOver(this);' onClick='generarDoc2(\"pdf\",\""+cadena+"\");' onMouseOut='botonOut(this);' style='cursor:pointer'>"
                                       + "</div>"
                                       + "<div id='mensaje2' align='center'></div>";
                }*/
                cadenatabla += "</div>";
            }
            else if(opcion.equals("ingfactura")){
                redirect=false;
                ArrayList<Ingreso> lista = null;
                Ingreso ing = null;
                String cadena = request.getParameter("cadena")!= null ? request.getParameter("cadena"):"";
                boolean pasa = true;
                try {
                    lista = cserv.ingresosFactura(cadena);
                }
                catch (Exception e) {
                    pasa = false;
                    System.out.println("Error al llenar la lista de ingresos de facturas: "+e.toString());
                    e.printStackTrace();
                }
                cadenatabla="<table border='1' style='border-collapse: collapse'>" +
                        "<thead>" +
                            "<tr class='subtitulo1'>" +
                                "<td>Ingreso</td>" +
                                "<td>Fecha</td>" +
                                "<td>Banco</td>" +
                                "<td>Valor abonado</td>" +
                            "</tr>" +
                        "</thead><tbody>";
                if(pasa==true && lista.size()>0){
                    DecimalFormat form = new DecimalFormat("#,###,###,###.##");
                    for (int i = 0; i < lista.size(); i++) {
                        ing = lista.get(i);
                        cadenatabla += "<tr class='xfila' align='center'>" +
                                                    "<td>"+ ing.getNum_ingreso() +"</td>" +
                                                    "<td>"+ ing.getFecha_consignacion() +"</td>" +
                                                    "<td>"+ ing.getBranch_code() +"</td>" +
                                                    "<td align='right'>$ "+form.format( ing.getVlr_ingreso()) +"</td>";
                        cadenatabla += "</tr>";
                        ing = null;
                    }
                }
                else{
                    cadenatabla += "<tr class='xfila'><td colspan='4' align='center'>No se encontraron registros</td></tr>";
                }
                cadenatabla += "</tbody></table>";
            }
            else if(opcion.equals("facturafiltrosrt")){
                redirect=false;
                ArrayList<factura> lista = null;
                String filtro = request.getParameter("filtro")!= null ? request.getParameter("filtro"):"";
                String cadena = request.getParameter("cadena")!= null ? request.getParameter("cadena"):"";
                String col = request.getParameter("col")!= null ? request.getParameter("col"):"f.documento";
                String orden = request.getParameter("orden")!= null ? request.getParameter("orden"):"ASC";
                String num = request.getParameter("num")!= null ? request.getParameter("num"):"1";
                boolean pasa = true;
                factura fact = null;
                try {
                    lista = cserv.facturasNegocioSort(cadena, filtro, col, orden);
                }
                catch (Exception e) {
                    pasa = false;
                    System.out.println("Error al llenar la lista de facturas: "+e.toString());
                    e.printStackTrace();
                }
                //String cadAsc = "background:url("+request.getContextPath()+"/images/asc.gif) 7px no-repeat;";
                String cadAsc = "class='asc'";
                //String cadSort = "background:url("+request.getContextPath()+"/images/sort.gif) 7px no-repeat;";
                String cadSort = "class='head'";
                //String cadDesc = "background:url("+request.getContextPath()+"/images/desc.gif) 7px no-repeat;";
                String cadDesc = "class='desc'";
                cadenatabla = "<div>" +
                                        "<table border='0' width='100%' style='border-collapse: collapse'>" +
                                            "<tr class='fila'>" +
                                                "<th>Filtro</th>" +
                                                "<th colspan='11' align='left'>" +
                                                    "<select id='filtro' name='filtro' onchange='facturafiltro(\""+cadena+"\",this.selectedIndex);'>" +
                                                        //"<option value=''"+(filtro.equals("")?" selected":"")+">...</option>" +
                                                        "<option value='VIGENTE'"+(filtro.equals("VIGENTE")?" selected":"")+">Vigentes</option>" +
                                                        "<option value='CANCELADA'"+(filtro.equals("CANCELADA")?" selected":"")+">Canceladas</option>" +
                                                        "<option value='TODAS'"+(filtro.equals("TODAS")?" selected":"")+">Todas</option>"+
                                                        "<option value='SINIESTRADA'"+(filtro.equals("SINIESTRADA")?" selected":"")+">Siniestrada</option>"+
                                                        "<option value='NEGOCIADA'"+(filtro.equals("NEGOCIADA")?" selected":"")+">Negociada</option>"+
                                                    "</select>" +
                                                    "<span class='gestionspan' onclick='winGestionFact2();'>Gestionar Factura(s)</span>" +
                                                "</th>" +
                                            "</tr>" +
                                            "<tr class='subtitulo1' align='center' style='cursor:pointer;'>" +
                                                "<th width='8%' onclick='organizar(\""+cadena+"\",\"f.documento\",\""+(num.equals("1")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",1);' "+(num.equals("1")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+">Factura</th>" +
                                                "<th width='7%' onclick='organizar(\""+cadena+"\",\"fd.numero_remesa\",\""+(num.equals("2")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",2);' "+(num.equals("2")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+">Relacionada</th>" +
                                                "<th width='6%' onclick='organizar(\""+cadena+"\",\"f.num_doc_fen\",\""+(num.equals("3")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",3);' "+(num.equals("3")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+">No. titulo</th>" +
                                                "<th width='7%' onclick='organizar(\""+cadena+"\",\"f.fecha_vencimiento\",\""+(num.equals("4")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",4);' "+(num.equals("4")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+">Vencimiento</th>" +
                                                "<th width='7%' onclick='organizar(\""+cadena+"\",\"dias_mora\",\""+(num.equals("5")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",5);' "+(num.equals("5")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+">Dias mora</th>" +
                                                "<th width='9%' onclick='organizar(\""+cadena+"\",\"ndiasf\",\""+(num.equals("6")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",6);' "+(num.equals("6")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+">Venc. prejuridico</th>" +
                                                "<th width='6%' onclick='organizar(\""+cadena+"\",\"estadofact\",\""+(num.equals("7")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",7);' "+(num.equals("7")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+">Estado</th>" +
                                                "<th width='7%' onclick='organizar(\""+cadena+"\",\"f.fecha_ultimo_pago\",\""+(num.equals("8")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",8);' "+(num.equals("8")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+">Ult. abono</th>" +
                                                "<th width='7%' onclick='organizar(\""+cadena+"\",\"f.valor_factura\",\""+(num.equals("10")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",10);' "+(num.equals("10")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+">Valor</th>" +
                                                "<th width='7%' onclick='organizar(\""+cadena+"\",\"f.valor_abono\",\""+(num.equals("9")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",9);' "+(num.equals("9")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+">Abonos</th>" +
                                                "<th width='8%' onclick='organizar(\""+cadena+"\",\"f.valor_saldo\",\""+(num.equals("11")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",11);' "+(num.equals("11")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+">Saldo</th>" +
                                                "<th width='6%'>% cobro</th>" +
                                                "<th width='8%'>Int. mora</th>" +
                                                "<th width='14%'>Vr. cobro</th>" +
                                            "</tr>" +
                                        "</table>";
                cadenatabla += "<div style='overflow: auto; height: 265px;'>" +
                                            "<table border='0' width='100%' style='border-collapse: collapse'>" +
                                                "<tbody>";
                if(pasa==true && lista.size()>0){
                    DecimalFormat form = new DecimalFormat("#,###,###,###.##");
                    for (int i = 0; i < lista.size(); i++) {
                        fact = lista.get(i);
                        cadenatabla += "<tr class='xfila'>" +
                                                    "<td width='7%'>" +
                                                        "<input type='checkbox' id='fact"+i+"' name='fact"+i+"' value='"+fact.getDocumento()+"'>" +
                                                        "<img alt='mostrar ingresos' src='"+request.getContextPath()+"/images/botones/iconos/expand.gif' name='imgexpand"+i+"' id='imgexpand"+i+"' onclick='expandir("+i+",\""+ fact.getDocumento() + "\")' style='cursor:pointer'>" +
                                                        "<span style='cursor:pointer;' onclick='winGestionFact(\""+fact.getDocumento()+"\",\""+Math.ceil(fact.getValor_saldo()+fact.getValor_saldome())+"\");'>"+ fact.getDocumento() + "</span>" +
                                                    "</td>" +
                                                    "<td width='8%' align='center'>"+ fact.getFactura() +"</td>" +
                                                    "<td width='7%' align='center'>"+ fact.getFisico() +"</td>" +
                                                    "<td width='9%' align='center'>"+ fact.getFecha_vencimiento() +"</td>" +
                                                    "<td width='7%' align='center'>"+ fact.getDias_mora() +"</td>" +
                                                    "<td width='11% align='center''>"+ fact.getFecha_ven_prejurid() +"</td>" +
                                                    "<td width='6%' align='center'>"+ fact.getEstado() +"</td>" +
                                                    "<td width='9%' align='center'>"+ fact.getFecha_ultimo_pago() +"</td>" +
                                                    "<td width='9%' align='right'>$ "+ form.format(fact.getValor_factura()) +"</td><input type='hidden' id='vcuot"+i+"' name='vcuot"+i+"' value='"+ fact.getValor_factura() +"'>" +
                                                    "<td width='7%' align='right'>$ "+ form.format(fact.getValor_abono()) +"</td>" +
                                                    "<td width='9%' align='right'>$ "+ form.format(fact.getValor_saldo()) +"</td>" +
                                                    "<td width='8%' align='right'><input type='text' id='porc"+i+"' name='porc"+i+"' value='0' onkeyup='calculate(this.value,"+i+");' size='5' style='text-align: right;'></td>" +
                                                    "<td width='10%' align='right'><input type='text' id='morax"+i+"' name='morax"+i+"' value='"+form.format(Math.ceil(fact.getValor_saldome()))+"' size='10' style='text-align: right;' readonly><input type='hidden' id='mora"+i+"' name='mora"+i+"' value='"+ fact.getValor_saldome() +"'></td>" +
                                                    "<td width='12%' align='right'><input type='text' id='vx"+i+"' name='vx"+i+"' value='"+form.format(Math.ceil(fact.getValor_saldo() + fact.getValor_saldome()))+"' size='10' style='text-align: right;' readonly></td>" +
                                                "</tr>" +
                                                "<tr><td></td><td colspan='13'><div id='diving"+i+"' style='display:none; padding:0px;'></div></td></tr>";
                        fact = null;
                    }

                }
                else{
                    cadenatabla += "<tr class='xfila'><td colspan='14' align='center'>No se encontraron registros</td></tr>";
                }
                cadenatabla += "</tbody></table></div>";
                if(pasa==true && lista.size()>0){
                    cadenatabla += "<div align='center'>"
                                            + "<img alt='generar xls' src='"+request.getContextPath()+"/images/botones/exportarExcel.gif' id='imgexc2' name='imgxls' onMouseOver='botonOver(this);' onClick='generarDoc2(\"xls\",\""+cadena+"\");' onMouseOut='botonOut(this);' style='cursor:pointer'>"
                                            + "<img alt='generar pdf' src='"+request.getContextPath()+"/images/botones/generarPdf.gif' id='imgpdf2' name='imgxls' onMouseOver='botonOver(this);' onClick='generarDoc2(\"pdf\",\""+cadena+"\");' onMouseOut='botonOut(this);' style='cursor:pointer'>"
                                       + "</div>"
                                       + "<div id='mensaje2' align='center'></div>";
                }
                cadenatabla += "</div>";
            }
            else if(opcion.equals("facturasing")){
                redirect=false;
                ArrayList<factura> lista = null;
                String cadena = request.getParameter("cadena")!= null ? request.getParameter("cadena"):"";
                boolean pasa = true;
                factura fact = null;
                try {
                    lista = cserv.facturasIngreso(cadena);
                }
                catch (Exception e) {
                    pasa = false;
                    System.out.println("Error al llenar la lista de facturas del ingreso: "+e.toString());
                    e.printStackTrace();
                }
                cadenatabla = "<table border='1' width='100%' style='border-collapse: collapse'>" +
                                            "<thead>" +
                                                "<tr class='subtitulo1'>" +
                                                "<td>Factura</td>" +
                                                "<td>Relacionado</td>" +
                                                "<td>No. titulo</td>" +
                                                "<td>Valor abonado</td>" +
                                                "<td>Valor cuota</td>" +
                                                "<td>Valor saldo</td>" +
                                                "</tr>" +
                                            "</thead><tbody>";
                if(pasa==true && lista.size()>0){
                    DecimalFormat form = new DecimalFormat("#,###,###,###.##");
                    for (int i = 0; i < lista.size(); i++) {
                        fact = lista.get(i);
                        cadenatabla+="<tr class='xfila'>" +
                                                    "<td>"+ fact.getDocumento() +"</td>" +
                                                    "<td>"+ fact.getFactura() +"</td>" +
                                                    "<td>"+ fact.getFisico() +"</td>" +
                                                    "<td align='right'>$ "+ form.format(fact.getValor_abono())+"</td>" +
                                                    "<td align='right'>$ "+ form.format(fact.getValor_factura()) +"</td>" +
                                                    "<td align='right'>$ "+ form.format(fact.getValor_saldo()) +"</td>" +
                                                "</tr>";
                        fact = null;
                    }
                }
                else{
                    cadenatabla += "<tr class='fila'><td colspan='6' align='center'>No se encontraron registros</td></tr>";
                }
                cadenatabla += "</tbody></table>";
            }
            else if(opcion.equals("printpdf")){
                redirect=false;
                cadenatabla = "<span class='fila'>Archivo generado correctamente.<br>Revise su directorio de archivos.</span>";
                String cadena = request.getParameter("cadena")!= null ? request.getParameter("cadena"):"";
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario)session.getAttribute("Usuario");
                ArrayList<BeanGeneral> estado_cuenta = null;
                try {
                    estado_cuenta = cserv.estadoCuenta(cadena, "fecha_documento,substring(documento FROM '[0-9]+')", "");
                }
                catch (Exception e) {
                    System.out.println("error: "+e.toString());
                    e.printStackTrace();
                }
                ArrayList<String> listahead = null;
                try {
                    listahead = cserv.cabeceraTabla(cadena);
                    String[] vec = null;
                    try{
                        vec = (cserv.nitNegocio(cadena)).split(";_;");
                    }
                    catch(Exception e){
                        System.out.println("Error al buscar datos para estado de cuenta cabecera: "+e.toString());
                        e.printStackTrace();
                    }
                    this.generarPdf(usuario.getLogin(), estado_cuenta, listahead, vec);
                }
                catch (Exception e) {
                    cadenatabla = "<span class='fila'>Ocurrio un error al generar el archivo</span>";
                    System.out.println("Error al generar el pdf del estado de cuenta: "+e.toString());
                    e.printStackTrace();
                }
            }
            else if(opcion.equals("printxls")){
                redirect=false;
                cadenatabla = "<span class='fila'>Archivo generado correctamente.<br>Revise su directorio de archivos.</span>";
                String cadena = request.getParameter("cadena")!= null ? request.getParameter("cadena"):"";
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario)session.getAttribute("Usuario");
                ArrayList<BeanGeneral> estado_cuenta = null;
                try {
                    estado_cuenta = cserv.estadoCuenta(cadena, "fecha_documento,substring(documento FROM '[0-9]+')", "");
                }
                catch (Exception e) {
                    System.out.println("error: "+e.toString());
                    e.printStackTrace();
                }
                ArrayList<String> listahead = null;
                try {
                    listahead = cserv.cabeceraTabla(cadena);
                    String[] vec = null;
                    try{
                        vec = (cserv.nitNegocio(cadena)).split(";_;");
                    }
                    catch(Exception e){
                        System.out.println("Error al buscar datos para estado de cuenta cabecera: "+e.toString());
                        e.printStackTrace();
                    }
                    this.generarXls(usuario.getLogin(), estado_cuenta, listahead, vec);
                }
                catch (Exception e) {
                    cadenatabla = "<span class='fila'>Ocurrio un error al generar el archivo</span>";
                    System.out.println("Error al generar el xls del estado de cuenta: "+e.toString());
                    e.printStackTrace();
                }
            }
            else if(opcion.equals("print2pdf")){
                redirect=false;
                cadenatabla = "Archivo generado correctamente.Revise su directorio de archivos.";
                String cadena = request.getParameter("cadena")!= null ? request.getParameter("cadena"):"";
                String col = request.getParameter("col")!= null ? request.getParameter("col"):"f.fecha_vencimiento";
                String orden = request.getParameter("orden")!= null ? request.getParameter("orden"):"ASC";
                String filtro = request.getParameter("filtro")!= null ? request.getParameter("filtro"):"";
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario)session.getAttribute("Usuario");
                ArrayList<factura> listafacts=null;
                try {
                    listafacts = cserv.facturasNegocioSort(cadena, filtro, col, orden);
                    //listafacts = cserv.facturasNegocio(cadena);
                    this.generarPdf2(usuario.getLogin(), listafacts, cadena);
                }
                catch (Exception e) {
                    cadenatabla = "Ocurrio un error al generar el archivo";
                    System.out.println("Error al generar el pdf del estado de cuenta: "+e.toString());
                    e.printStackTrace();
                }
            }
            else if(opcion.equals("print2xls")){
                redirect=false;
                cadenatabla = "Archivo generado correctamente.Revise su directorio de archivos.";
                String cadena = request.getParameter("cadena")!= null ? request.getParameter("cadena"):"";
                String col = request.getParameter("col")!= null ? request.getParameter("col"):"f.fecha_vencimiento";
                String orden = request.getParameter("orden")!= null ? request.getParameter("orden"):"ASC";
                String filtro = request.getParameter("filtro")!= null ? request.getParameter("filtro"):"";
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario)session.getAttribute("Usuario");
                ArrayList<factura> listafacts=null;
                try {
                    listafacts = cserv.facturasNegocioSort(cadena, filtro, col, orden);
                    //listafacts = cserv.facturasNegocio(cadena);
                    this.generarXls2(usuario.getLogin(), listafacts, cadena);
                }
                catch (Exception e) {
                    cadenatabla = "Ocurrio un error al generar el archivo";
                    System.out.println("Error al generar el xls del estado de cuenta: "+e.toString());
                    e.printStackTrace();
                }
            }
            else if(opcion.equals("print3pdf")){
                redirect=false;
                cadenatabla = "Archivo generado correctamente.Revise su directorio de archivos.";
                String cadena = request.getParameter("cadena")!= null ? request.getParameter("cadena"):"facts_pf";
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario)session.getAttribute("Usuario");
                ArrayList<factura> listafacts=null;
                try {
                    listafacts = cserv.facturasPf2("VIGENTE","ASC","f.documento");
                    this.generarPdf2(usuario.getLogin(), listafacts, cadena);
                }
                catch (Exception e) {
                    cadenatabla = "Ocurrio un error al generar el archivo";
                    System.out.println("Error al generar el pdf del estado de cuenta: "+e.toString());
                    e.printStackTrace();
                }
            }
            else if(opcion.equals("print3xls")){
                redirect=false;
                cadenatabla = "Archivo generado correctamente.Revise su directorio de archivos.";
                String cadena = request.getParameter("cadena")!= null ? request.getParameter("cadena"):"facts_pf";
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario)session.getAttribute("Usuario");
                ArrayList<factura> listafacts=null;
                try {
                    listafacts = cserv.facturasPf2("VIGENTE","ASC","f.documento");
                    this.generarXls2(usuario.getLogin(), listafacts, cadena);
                }
                catch (Exception e) {
                    cadenatabla = "Ocurrio un error al generar el archivo";
                    System.out.println("Error al generar el xls del estado de cuenta: "+e.toString());
                    e.printStackTrace();
                }
            }
          else if (opcion.equals("print4pdf")) {
                redirect = false;
                cadenatabla = "Archivo generado correctamente.\n Revise su directorio de archivos.";
                String cadena = request.getParameter("cadena") != null ? request.getParameter("cadena") : "";
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario) session.getAttribute("Usuario");
                ArrayList<factura> listafactscli = null;
                try {
                    listafactscli = cserv.facturasNegocioClie(cadena);
                } catch (Exception e) {
                    System.out.println("error: " + e.toString());
                    e.printStackTrace();
                }
                ArrayList<String> listahead = null;
                try {
                    listahead = cserv.cabeceraTabla(cadena);
                    if (listahead.size() > 0) {

                        if (!cserv.generarPdf4(usuario.getLogin(), listafactscli, listahead)) {
                            cadenatabla = "Ocurrio un error al generar el archivo";
                        }
                    } else {
                        cadenatabla = "El negocio no tiene convenio";
                    }
                } catch (Exception e) {
                    cadenatabla = "Ocurrio un error al generar el archivo";
                    System.out.println("Error al generar el pdf del estado de cuenta: " + e.toString());
                    e.printStackTrace();
                }
            }
            else if (opcion.equals("printcarta")) {
                redirect = false;
                cadenatabla = "Archivo generado correctamente.\n Revise su directorio de archivos.";
                String cod_carta = request.getParameter("cod_carta") != null ? request.getParameter("cod_carta") : "";
                String neg = request.getParameter("neg") != null&&!request.getParameter("neg").equals("") ? request.getParameter("neg") : "";
                double saldo = request.getParameter("saldo") != null&&!request.getParameter("saldo").equals("") ? Double.parseDouble(request.getParameter("saldo").replaceAll(",", ""))  : 0;
                double mora= request.getParameter("mora") != null&&!request.getParameter("mora").equals("") ? Double.parseDouble(request.getParameter("mora").replaceAll(",", ""))  : 0;
                double cobro = request.getParameter("cobro") != null&&!request.getParameter("cobro").equals("") ? Double.parseDouble(request.getParameter("cobro").replaceAll(",", ""))  : 0;
                int dias = request.getParameter("dias") != null&&!request.getParameter("dias").equals("") ? Integer.parseInt(request.getParameter("dias"))  : 0;
                HttpSession session = request.getSession();
                Usuario usuario = (Usuario) session.getAttribute("Usuario");
                ArrayList<String> textos = null;
                try {
                    textos = cserv.textoCarta(cod_carta);
                    
                    if (textos.size() > 0) {

                        if (!cserv.generarCarta(cod_carta,usuario.getLogin(), textos, neg, saldo, mora, cobro,dias)) {
                            cadenatabla = "Ocurrio un error al generar la carta";
                        }
                    } else {
                        cadenatabla = "No existen contenido para esta carta";
                    }
                } catch (Exception e) {
                    cadenatabla = "Ocurrio un error al generar la carta";
                    System.out.println("Error al generar el pdf del estado de cuenta: " + e.toString());
                    e.printStackTrace();
                }
            }
            else if (opcion.equals("enviarestado")) {
                redirect = false;
                cadenatabla = "";
                String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
                String email = request.getParameter("email") != null ? request.getParameter("email") : "";
               
                try {
                   if(!email.equals("")){

                        cadenatabla=cserv.enviarEstadoCuenta(negocio, email);

                    }else{
                        cadenatabla = "El cliente no posee email para el negocio en gestion";
                    }
                        
                   
                } catch (Exception e) {
                    cadenatabla = "Ocurrio un error al generar el archivo";
                    System.out.println("Error al enviar estado de cuenta " + e.toString());
                    e.printStackTrace();
                }
            }
            else if(opcion.equals("insert")){
                redirect=true;
                String documento = request.getParameter("factura")!=null ? request.getParameter("factura") : "";
                String facts = request.getParameter("facts")!=null? request.getParameter("facts"):"";
                String valor = request.getParameter("valor")!=null? request.getParameter("valor"):"";
                next += "gestion_cartera.jsp?documento=" + documento + "&facts="+facts+"&valor="+valor;
                try {
                    String observacion = request.getParameter("observacion")!=null ? request.getParameter("observacion") : "";

                    String login = user.getLogin();
                    String tipo_gestion = request.getParameter("tipo_gestion")!=null ? request.getParameter("tipo_gestion") : "0";
                    String fecha_prox_gestion = request.getParameter("fecha_prox_gestion")!=null ? request.getParameter("fecha_prox_gestion") : "0099-01-01 00:00:00";
                    String prox_accion = request.getParameter("prox_accion")!=null ? request.getParameter("prox_accion") : "0";
                    String estado_cliente = request.getParameter("estado_cliente")!=null ? request.getParameter("estado_cliente") : "01";
                    String codcli = request.getParameter("codcli")!=null ? request.getParameter("codcli") : "";
                    String tipo = request.getParameter("tipo_dato")!=null ? request.getParameter("tipo_dato") : "";
                    String dato = request.getParameter("dato")!=null ? request.getParameter("dato") : "";
                    if(documento.indexOf(",")>0){
                        String[] docsplit = documento.split(",");
                        for (int i = 0; i < docsplit.length; i++) {
                            cserv.insObservacion(docsplit[i], observacion, login, tipo_gestion, fecha_prox_gestion, prox_accion,tipo,dato);
                        }
                    }
                    else{
                        cserv.insObservacion(documento, observacion, login, tipo_gestion, fecha_prox_gestion, prox_accion, tipo, dato);
                    }
                    cserv.actualizarEstadoCartera(codcli, estado_cliente);
                    //actualizar registro
                }
                catch (Exception e) {
                    System.out.println("Error al insertar registros en el historial: "+e.toString());
                    e.printStackTrace();
                }
            } else if (opcion.equals("insert2")) {
                redirect = false;
                
             ///   next += "gestion_cartera.jsp?documento=" + documento + "&facts=" + facts + "&valor=" + valor;
                try {
                    String observacion = request.getParameter("observacion") != null ? request.getParameter("observacion") : "";

                    String login = user.getLogin();                   
                    String tipo_gestion = request.getParameter("tipo_gestion") != null ? request.getParameter("tipo_gestion") : "0";
                    if (tipo_gestion.equals("19")){
                        String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
                        Double valor_pagar = request.getParameter("valor_pagar") != null ? Double.parseDouble(request.getParameter("valor_pagar")) : 0;
                        String fecha_pagar = request.getParameter("fecha_pagar") != null ? request.getParameter("fecha_pagar") : "";
                        String ciudad = request.getParameter("ciudad") != null ? request.getParameter("ciudad") : "";
                        String barrio = request.getParameter("barrio") != null ? request.getParameter("barrio") : ""; 
                        String direccion = request.getParameter("direccion") != null ? request.getParameter("direccion") : "";
                        cserv.insCompromisoPago(negocio, observacion, valor_pagar, fecha_pagar, ciudad, barrio, direccion, login);
                        cadenatabla = "OK";
                    }else{
                        String documento = request.getParameter("factura") != null ? request.getParameter("factura") : "";
                        String facts = request.getParameter("facts") != null ? request.getParameter("facts") : "";
                        String valor = request.getParameter("valor") != null ? request.getParameter("valor") : "";
                        
                        String hora= request.getParameter("hora");
                        String minutos=request.getParameter("minutos")+":00";
                        
                        String fecha_prox_gestion = request.getParameter("fecha_prox_gestion") != null ? request.getParameter("fecha_prox_gestion") : "0099-01-01 00:00:00";
                        fecha_prox_gestion = fecha_prox_gestion + " " + hora + ":" + minutos;
                        String prox_accion = request.getParameter("prox_accion") != null ? request.getParameter("prox_accion") : "0";
                        String estado_cliente = request.getParameter("estado_cliente") != null ? request.getParameter("estado_cliente") : "01";
                        String codcli = request.getParameter("codcli") != null ? request.getParameter("codcli") : "";
                        String tipo = request.getParameter("tipo_dato") != null ? request.getParameter("tipo_dato") : "";
                        String dato = request.getParameter("dato") != null ? request.getParameter("dato") : "";
                        if (documento.indexOf(",") > 0) {
                            String[] docsplit = documento.split(",");
                            for (int i = 0; i < docsplit.length; i++) {
                                cserv.insObservacion(docsplit[i], observacion, login, tipo_gestion, fecha_prox_gestion, prox_accion, tipo, dato);
                                cadenatabla = "OK";
                            }
                        } else {
                            cserv.insObservacion(documento, observacion, login, tipo_gestion, fecha_prox_gestion, prox_accion, tipo, dato);
                            cadenatabla = "OK";
                        }
                        cserv.actualizarEstadoCartera(codcli, estado_cliente);
                    }
              
                    //actualizar registro
                } catch (Exception e) {
                    cadenatabla="FAIL";
                    System.out.println("Error al insertar registros en el historial: " + e.toString());
                    e.printStackTrace();
                }
            } else if(opcion.equals("vertodo")){
                redirect=false;
                String documento =  request.getParameter("documento")!=null ? request.getParameter("documento") : "";
                ArrayList<BeanGeneral> listaobs = null;
                BeanGeneral obs = null;
                boolean pasa = true;
                try {
                    listaobs = cserv.observacionesNeg(documento);
                }
                catch (Exception e) {
                    pasa = false;
                    System.out.println("Error al ver la gestion del negocio: "+e.toString());
                    e.printStackTrace();
                }
                cadenatabla = "<table border='1' style='border-collapse: collapse;' width='100%'>" +
                                            "<thead>" +
                                                "<tr class='subtitulo1'>" +
                                                    "<th>Fecha</th>" +
                                                    "<th>Usuario</th>" +
                                                    "<th>Factura</th>" +
                                                    "<th>Gestion</th>" +
                                                    "<th>Prox. accion</th>" +
                                                    "<th>Fecha prox. accion</th>" +
                                                    "<th>Observacion</th>" +
                                                "</tr>" +
                                            "</thead><tbody style='height: 340px; overflow: auto;'>";
                if(pasa==true && listaobs.size()>0){
                    for (int i = 0; i < listaobs.size(); i++) {
                        obs = listaobs.get(i);
                        cadenatabla+="<tr class='filaazul'>" +
                                                    "<td>" + obs.getValor_02() + "</td>" +
                                                    "<td>" + obs.getValor_07() + "</td>" +
                                                    "<td>" + obs.getValor_06() + "</td>" +
                                                    "<td>" + cserv.nombreGestion(obs.getValor_03(),"TIPOGEST") + "</td>" +
                                                    "<td>" + cserv.nombreGestion(obs.getValor_05(),"PRXACCION") + "</td>" +
                                                    "<td>" + obs.getValor_04() + "</td>" +
                                                    "<td><span onclick='verObs(\"" + obs.getValor_08() + "\");' style='text-decoration: none; color: green; cursor: pointer;'>Clic para ver</span></td>" +
                                                "</tr>";
                        obs = null;
                    }
                }
                else{
                    cadenatabla += "<tr class='filaazul'><td colspan='7' align='center'></td></tr>";
                }
                cadenatabla += "</tbody></table>";
            }
            else if(opcion.equals("sortingresos")){
                redirect=false;
                ArrayList<Ingreso> lista = null;
                String cadena = request.getParameter("cadena")!= null ? request.getParameter("cadena"):"";
                String col = request.getParameter("col")!= null ? request.getParameter("col"):"f.documento";
                String orden = request.getParameter("orden")!= null ? request.getParameter("orden"):"ASC";
                String num = request.getParameter("num")!= null ? request.getParameter("num"):"1";
                boolean pasa = true;
                //String cadAsc = "background:url("+request.getContextPath()+"/images/asc.gif) 7px no-repeat;";
                String cadAsc = "class='asc'";
                //String cadSort = "background:url("+request.getContextPath()+"/images/sort.gif) 7px no-repeat;";
                String cadSort = "class='head'";
                //String cadDesc = "background:url("+request.getContextPath()+"/images/desc.gif) 7px no-repeat;";
                String cadDesc = "class='desc'";
                cadenatabla = "<div>"
                                      +"<table border='0' width='60%' style='border-collapse: collapse;'>"
                                        +"<tr class='subtitulo1' align='center'>"
                                            +"<th width='6%' "+(num.equals("1")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+" onclick='sortIngresos(\""+cadena+"\",\"descripcion\",\""+(num.equals("1")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",\"1\");'>Tipo</th>"
                                            +"<th width='15%' "+(num.equals("2")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+" onclick='sortIngresos(\""+cadena+"\",\"num_ingreso\",\""+(num.equals("2")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",\"2\");'>Consecutivo</th>"
                                            +"<th width='14%' "+(num.equals("3")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+" onclick='sortIngresos(\""+cadena+"\",\"fecha_creacion\",\""+(num.equals("3")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",\"3\");'>Creacion</th>"
                                            +"<th width='17%' "+(num.equals("4")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+" onclick='sortIngresos(\""+cadena+"\",\"fecha_consignacion\",\""+(num.equals("4")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",\"4\");'>Consignacion</th>"
                                            +"<th width='27%' "+(num.equals("5")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+" onclick='sortIngresos(\""+cadena+"\",\"branch_code\",\""+(num.equals("5")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",\"5\");'>Banco</th>"
                                            +"<th width='12%' "+(num.equals("6")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+" onclick='sortIngresos(\""+cadena+"\",\"vlr_ingreso\",\""+(num.equals("6")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",\"6\");'>Valor</th>"
                                            +"<th width='9%' "+(num.equals("7")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+" onclick='sortIngresos(\""+cadena+"\",\"estado\",\""+(num.equals("7")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",\"7\");'>Estado</th>"
                                        +"</tr>"
                                      +"</table>"
                                  +"</div>"
                                  + "<div style='overflow: auto; height: 320px;'>"
                                    + "<table border='0' width='60%' style='border-collapse: collapse;'>"
                                        + "<tbody>";
                try {
                    lista = cserv.ingresosNegocioSort(cadena, col, orden);
                    if(lista!=null && lista.size()>0) pasa = true;
                    else pasa = false;
                }
                catch (Exception e) {
                    pasa = false;
                    System.out.println("Error en action: "+e.toString());
                    e.printStackTrace();
                }
                if(pasa==true){
                    Ingreso ingreso = null;
                    DecimalFormat form = new DecimalFormat("#,###,###,###.##");
                    for(int i=0;i<lista.size();i++){
                        ingreso = lista.get(i);
                        cadenatabla += "<tr class='xfila'>"
                                                +"<td width='6%' align='center'>"+ingreso.getConcepto()+"</td>"
                                                +"<td width='15%' align='center'>"
                                                    +"<img alt='mostrar facturas' src='"+request.getContextPath()+"/images/botones/iconos/expand.gif' id='imgexpandx"+i+"' name='imgexpandx"+i+"' onclick='expandir2("+i+",\""+ingreso.getNum_ingreso()+"\")' style='cursor:pointer'>"+ingreso.getNum_ingreso()
                                                +"</td>"
                                                +"<td width='14%' align='center'>"+ingreso.getCreation_date()+"</td>"
                                                +"<td width='17%' align='center'>"+ingreso.getFecha_consignacion()+"</td>"
                                                +"<td width='27%' align='center'>"+ingreso.getBranch_code()+"</td>"
                                                +"<td width='12%' align='right'>$ "+form.format(ingreso.getVlr_ingreso())+"</td>"
                                                +"<td width='9%' align='center'>"+ingreso.getReg_status()+"</td>"
                                            +"</tr>"
                                            +"<tr><td colspan='7'><div id='divfacts"+i+"' style='display:none; padding:0px;'></div></td></tr>";
                    }
                }
                else{
                    cadenatabla += "<tr class='xfila'><td colspan='7'>No se encontraron registros</td></tr>";
                }
                cadenatabla += "</tbody></table></div>";
            }
            else if(opcion.equals("sortingresos2")){
                redirect=false;
                ArrayList<Ingreso> lista = null;
                String col = request.getParameter("col")!= null ? request.getParameter("col"):"f.documento";
                String orden = request.getParameter("orden")!= null ? request.getParameter("orden"):"ASC";
                String num = request.getParameter("num")!= null ? request.getParameter("num"):"1";
                boolean pasa = true;
                String cadAsc = "class='asc'";
                String cadSort = "class='head'";
                String cadDesc = "class='desc'";
                cadenatabla = "<div>"
                                      +"<table border='0' width='60%' style='border-collapse: collapse;'>"
                                        +"<tr class='subtitulo1' align='center'>"
                                            +"<th width='6%' "+(num.equals("1")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+" onclick='sortIngresos2(\"tdc.descripcion\",\""+(num.equals("1")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",\"1\");'>Tipo</th>"
                                            +"<th width='15%' "+(num.equals("2")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+" onclick='sortIngresos2(\"i.num_ingreso\",\""+(num.equals("2")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",\"2\");'>Consecutivo</th>"
                                            +"<th width='14%' "+(num.equals("3")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+" onclick='sortIngresos2(\"fecha_creacion\",\""+(num.equals("3")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",\"3\");'>Creacion</th>"
                                            +"<th width='17%' "+(num.equals("4")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+" onclick='sortIngresos2(\"i.fecha_consignacion\",\""+(num.equals("4")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",\"4\");'>Consignacion</th>"
                                            +"<th width='27%' "+(num.equals("5")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+" onclick='sortIngresos2(\"i.branch_code\",\""+(num.equals("5")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",\"5\");'>Banco</th>"
                                            +"<th width='12%' "+(num.equals("6")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+" onclick='sortIngresos2(\"i.vlr_ingreso\",\""+(num.equals("6")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",\"6\");'>Valor</th>"
                                            +"<th width='9%' "+(num.equals("7")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+" onclick='sortIngresos2(\"estado\",\""+(num.equals("7")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",\"7\");'>Estado</th>"
                                        +"</tr>"
                                      +"</table>"
                                  +"</div>"
                                  + "<div style='overflow: auto; height: 320px;'>"
                                    + "<table border='0' width='60%' style='border-collapse: collapse;'>"
                                        + "<tbody>";
                try {
                    lista = cserv.ingresosPf(col, orden);
                    if(lista!=null && lista.size()>0) pasa = true;
                    else pasa = false;
                }
                catch (Exception e) {
                    pasa = false;
                    System.out.println("Error en action: "+e.toString());
                    e.printStackTrace();
                }
                if(pasa==true){
                    Ingreso ingreso = null;
                    DecimalFormat form = new DecimalFormat("#,###,###,###.##");
                    for(int i=0;i<lista.size();i++){
                        ingreso = lista.get(i);
                        cadenatabla += "<tr class='xfila'>"
                                                +"<td width='6%' align='center'>"+ingreso.getConcepto()+"</td>"
                                                +"<td width='15%' align='center'>"
                                                    +"<img alt='mostrar facturas' src='"+request.getContextPath()+"/images/botones/iconos/expand.gif' id='imgexpandx"+i+"' name='imgexpandx"+i+"' onclick='expandir2("+i+",\""+ingreso.getNum_ingreso()+"\")' style='cursor:pointer'>"+ingreso.getNum_ingreso()
                                                +"</td>"
                                                +"<td width='14%' align='center'>"+ingreso.getCreation_date()+"</td>"
                                                +"<td width='17%' align='center'>"+ingreso.getFecha_consignacion()+"</td>"
                                                +"<td width='27%' align='center'>"+ingreso.getBranch_code()+"</td>"
                                                +"<td width='12%' align='right'>$ "+form.format(ingreso.getVlr_ingreso())+"</td>"
                                                +"<td width='9%' align='center'>"+ingreso.getReg_status()+"</td>"
                                            +"</tr>"
                                            +"<tr><td colspan='7'><div id='divfacts"+i+"' style='display:none; padding:0px;'></div></td></tr>";
                    }
                }
                else{
                    cadenatabla += "<tr class='xfila'><td colspan='7'>No se encontraron registros</td></tr>";
                }
                cadenatabla += "</tbody></table></div>";
            }
            else if(opcion.equals("sortestado")){
                redirect=false;
                ArrayList<BeanGeneral> lista = null;
                String cadena = request.getParameter("cadena")!= null ? request.getParameter("cadena"):"";
                String col = request.getParameter("col")!= null ? request.getParameter("col"):"f.documento";
                String orden = request.getParameter("orden")!= null ? request.getParameter("orden"):"ASC";
                String num = request.getParameter("num")!= null ? request.getParameter("num"):"1";
                boolean pasa = true;
                double suma1=0;
                double suma2=0;
                double suma3=0;
                double suma4=0;
                String cadAsc = "class='asc'";
                String cadSort = "class='head'";
                String cadDesc = "class='desc'";
                cadenatabla = "<div>"
                                      +"<table border='0' width='100%' style='border-collapse: collapse;'>"
                                        +"<tr class='subtitulo1' align='center'>"
                                            +"<td width='117px' align='center' "+(num.equals("1")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+" onclick='ordenarEstadoCartera(\""+(num.equals("1")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",\"fecha_documento\",\""+cadena+"\",1);'>Fecha</td>"
                                            +"<td width='100px' align='center' "+(num.equals("2")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+" onclick='ordenarEstadoCartera(\""+(num.equals("2")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",\"documento\",\""+cadena+"\",2);'>Documento</td>"
                                            +"<td width='100px' align='center' "+(num.equals("3")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+" onclick='ordenarEstadoCartera(\""+(num.equals("3")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",\"tipo\",\""+cadena+"\",3);'>Tipo</td>"
                                            +"<td width='120px' align='center' "+(num.equals("4")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+" onclick='ordenarEstadoCartera(\""+(num.equals("4")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",\"valor_factura\",\""+cadena+"\",4);'>Valor factura</td>"
                                            +"<td width='120px' align='center' "+(num.equals("5")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+" onclick='ordenarEstadoCartera(\""+(num.equals("5")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",\"valor_ingreso\",\""+cadena+"\",5);'>Valor ingreso</td>"
                                            +"<td width='160px' align='center' "+(num.equals("6")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+" onclick='ordenarEstadoCartera(\""+(num.equals("6")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",\"vr_ajuste_ingreso\",\""+cadena+"\",6);'>Vr. ajuste ingreso</td>"
                                            +"<td width='120px' align='center' "+(num.equals("7")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+" onclick='ordenarEstadoCartera(\""+(num.equals("7")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",\"valor_aplicado\",\""+cadena+"\",7);'>Vr. aplicado</td>"
                                            +"<td width='260px' align='center' "+(num.equals("8")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+" onclick='ordenarEstadoCartera(\""+(num.equals("8")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",\"cuenta_ajuste\",\""+cadena+"\",8);'>Cuenta de ajuste</td>"
                                            +"<td "+(num.equals("9")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+" onclick='ordenarEstadoCartera(\""+(num.equals("9")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",\"cuenta_cabecera\",\""+cadena+"\",9);'>Cuenta cabecera</td>"
                                        +"</tr>"
                                      +"</table>"
                                  +"</div>"
                                  + "<div style='overflow: auto; height: 250px;'>"
                                    + "<table border='0' width='100%' style='border-collapse: collapse;'>"
                                        + "<tbody>";
                try {
                    lista = cserv.estadoCuenta(cadena, col, orden);
                    if(lista!=null && lista.size()>0) pasa = true;
                    else pasa = false;
                }
                catch (Exception e) {
                    pasa = false;
                    System.out.println("Error en action: "+e.toString());
                    e.printStackTrace();
                }
                if(pasa==true){
                    BeanGeneral beang = null;
                    for(int i=0;i<lista.size();i++){
                        beang = lista.get(i);
                        suma1 += Double.parseDouble(beang.getValor_04().replaceAll(",", ""));
                        suma2 += Double.parseDouble(beang.getValor_05().replaceAll(",", ""));
                        suma3 += Double.parseDouble(beang.getValor_06().replaceAll(",", ""));
                        suma4 += Double.parseDouble(beang.getValor_07().replaceAll(",", ""));
                        cadenatabla += "<tr class='xfila'>"
                                                +"<td width='119px' align='center'>"+beang.getValor_01()+"</td>"
                                                +"<td width='100px' align='center'>"+beang.getValor_02()+"</td>"
                                                +"<td width='100px' align='center'>"+beang.getValor_03()+"</td>"
                                                +"<td width='120px' align='right'>"+beang.getValor_04()+"</td>"
                                                +"<td width='120px' align='right'>"+beang.getValor_05()+"</td>"
                                                +"<td width='160px' align='right'>"+beang.getValor_06()+"</td>"
                                                +"<td width='120px' align='right'>"+beang.getValor_07()+"</td>"
                                                +"<td width='260px' align='center'>"+beang.getValor_08()+"</td>"
                                                +"<td align='center'>"+beang.getValor_09()+"</td>"
                                            +"</tr>";
                    }
                }
                else{
                    cadenatabla += "<tr class='xfila'><td colspan='9'>No se encontraron registros</td></tr>";
                }

                cadenatabla += "</tbody></table></div>";
                cadenatabla += "<div style='height:0.2em;'></div>"
                        + "<div id='totales'>"
                            + "<table width='100%' style='border-collapse: collapse;'>"
                                + "<tr class='xfila'>"
                                    +"<td width='117px' align='center' style='border: 0em solid black;'>&nbsp;</td>"
                                    +"<td width='100px' align='center' style='border: 0em solid black;'>&nbsp;</td>"
                                    +"<td width='100px' align='center' style='border: 0.1em solid black;'>Totales</td>"
                                    +"<td width='120px' align='right' style='border: 0.1em solid black;'>"+Util.customFormat(suma1)+"</td>"
                                    +"<td width='120px' align='right' style='border: 0.1em solid black;'>"+Util.customFormat(suma2)+"</td>"
                                    +"<td width='160px' align='right' style='border: 0.1em solid black;'>"+Util.customFormat(suma3)+"</td>"
                                    +"<td width='120px' align='right' style='border: 0.1em solid black;'>"+Util.customFormat(suma4)+"</td>"
                                    +"<td width='260px' align='center' style='border: 0em solid black;'>&nbsp;</td>"
                                    +"<td align='center' style='border: 0em solid black;'>&nbsp;</td>"
                                +"</tr>"
                            + "</table>"
                        + "</div>"
                        + "<div style='height:0.2em;'></div>";
                cadenatabla += "<div align='center'>"
                                        + "<div id='mensaje' align='center'></div>"
                                        +"<img alt='generar xls' src='"+request.getContextPath()+"/images/botones/exportarExcel.gif' id='imgexc' name='imgxls' onMouseOver='botonOver(this);' onClick='generarDoc(\"xls\",\""+cadena+"\");' onMouseOut='botonOut(this);' style='cursor:pointer'>"
                                        +"<img alt='generar pdf' src='"+request.getContextPath()+"/images/botones/generarPdf.gif' id='imgpdf' name='imgpdf' onMouseOver='botonOver(this);' onClick='generarDoc(\"pdf\",\""+cadena+"\");' onMouseOut='botonOut(this);' style='cursor:pointer'>"
                                    +"</div>"
                                    +"<br>"
                                    +"<div id='mensaje' align='center'></div>";
            }
            else if(opcion.equals("sortestadopf")){
                redirect=false;
                ArrayList<BeanGeneral> lista = null;
                String col = request.getParameter("col")!= null ? request.getParameter("col"):"f.documento";
                String orden = request.getParameter("orden")!= null ? request.getParameter("orden"):"ASC";
                String num = request.getParameter("num")!= null ? request.getParameter("num"):"1";
                boolean pasa = true;
                String cadAsc = "class='asc'";
                String cadSort = "class='head'";
                String cadDesc = "class='desc'";
                double suma1=0;
                double suma2=0;
                double suma3=0;
                double suma4=0;
                cadenatabla = "<div>"
                                      +"<table border='0' width='100%' style='border-collapse: collapse;'>"
                                        +"<tr class='subtitulo1' align='center'>"
                                            +"<td width='117px' align='center' "+(num.equals("1")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+" onclick='ordenarEstadoCartera2(\""+(num.equals("1")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",\"fecha_documento\",1);'>Fecha</td>"
                                            +"<td width='100px' align='center' "+(num.equals("2")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+" onclick='ordenarEstadoCartera2(\""+(num.equals("2")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",\"documento\",,2);'>Documento</td>"
                                            +"<td width='100px' align='center' "+(num.equals("3")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+" onclick='ordenarEstadoCartera2(\""+(num.equals("3")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",\"tipo\",,3);'>Tipo</td>"
                                            +"<td width='120px' align='center' "+(num.equals("4")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+" onclick='ordenarEstadoCartera2(\""+(num.equals("4")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",\"valor_factura\",4);'>Valor factura</td>"
                                            +"<td width='120px' align='center' "+(num.equals("5")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+" onclick='ordenarEstadoCartera2(\""+(num.equals("5")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",\"valor_ingreso\",5);'>Valor ingreso</td>"
                                            +"<td width='160px' align='center' "+(num.equals("6")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+" onclick='ordenarEstadoCartera2(\""+(num.equals("6")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",\"vr_ajuste_ingreso\",6);'>Vr. ajuste ingreso</td>"
                                            +"<td width='120px' align='center' "+(num.equals("7")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+" onclick='ordenarEstadoCartera2(\""+(num.equals("7")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",\"valor_aplicado\",7);'>Vr. aplicado</td>"
                                            +"<td width='260px' align='center' "+(num.equals("8")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+" onclick='ordenarEstadoCartera2(\""+(num.equals("8")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",\"cuenta_ajuste\",8);'>Cuenta de ajuste</td>"
                                            +"<td "+(num.equals("9")?(orden.equals("ASC")?cadAsc:cadDesc):cadSort)+" onclick='ordenarEstadoCartera2(\""+(num.equals("9")?(orden.equals("ASC")?"DESC":"ASC"):"ASC")+"\",\"cuenta_cabecera\",9);'>Cuenta cabecera</td>"
                                        +"</tr>"
                                      +"</table>"
                                  +"</div>"
                                  + "<div style='overflow: auto; height: 250px;'>"
                                    + "<table border='0' width='100%' style='border-collapse: collapse;'>"
                                        + "<tbody>";
                try {
                    lista = cserv.estadoCuentaPf(col, orden);
                    if(lista!=null && lista.size()>0) pasa = true;
                    else pasa = false;
                }
                catch (Exception e) {
                    pasa = false;
                    System.out.println("Error en action: "+e.toString());
                    e.printStackTrace();
                }
                if(pasa==true){
                    BeanGeneral beang = null;
                    for(int i=0;i<lista.size();i++){
                        beang = lista.get(i);
                        suma1 += Double.parseDouble(beang.getValor_04().replaceAll(",", ""));
                        suma2 += Double.parseDouble(beang.getValor_05().replaceAll(",", ""));
                        suma3 += Double.parseDouble(beang.getValor_06().replaceAll(",", ""));
                        suma4 += Double.parseDouble(beang.getValor_07().replaceAll(",", ""));
                        cadenatabla += "<tr class='xfila'>"
                                                +"<td width='119px' align='center'>"+beang.getValor_01()+"</td>"
                                                +"<td width='100px' align='center'>"+beang.getValor_02()+"</td>"
                                                +"<td width='100px' align='center'>"+beang.getValor_03()+"</td>"
                                                +"<td width='120px' align='right'>"+beang.getValor_04()+"</td>"
                                                +"<td width='120px' align='right'>"+beang.getValor_05()+"</td>"
                                                +"<td width='160px' align='right'>"+beang.getValor_06()+"</td>"
                                                +"<td width='120px' align='right'>"+beang.getValor_07()+"</td>"
                                                +"<td width='260px' align='center'>"+beang.getValor_08()+"</td>"
                                                +"<td align='center'>"+beang.getValor_09()+"</td>"
                                            +"</tr>";
                    }
                }
                else{
                    cadenatabla += "<tr class='xfila'><td colspan='9'>No se encontraron registros</td></tr>";
                }
                cadenatabla += "</tbody></table></div>";
                cadenatabla += "<div style='height:0.2em;'></div>"
                        + "<div id='totales'>"
                            + "<table width='100%' style='border-collapse: collapse;'>"
                                + "<tr class='xfila'>"
                                    +"<td width='117px' align='center' style='border: 0em solid black;'>&nbsp;</td>"
                                    +"<td width='100px' align='center' style='border: 0em solid black;'>&nbsp;</td>"
                                    +"<td width='100px' align='center' style='border: 0.1em solid black;'>Totales</td>"
                                    +"<td width='120px' align='right' style='border: 0.1em solid black;'>"+Util.customFormat(suma1)+"</td>"
                                    +"<td width='120px' align='right' style='border: 0.1em solid black;'>"+Util.customFormat(suma2)+"</td>"
                                    +"<td width='160px' align='right' style='border: 0.1em solid black;'>"+Util.customFormat(suma3)+"</td>"
                                    +"<td width='120px' align='right' style='border: 0.1em solid black;'>"+Util.customFormat(suma4)+"</td>"
                                    +"<td width='260px' align='center' style='border: 0em solid black;'>&nbsp;</td>"
                                    +"<td align='center' style='border: 0em solid black;'>&nbsp;</td>"
                                +"</tr>"
                            + "</table>"
                        + "</div>";
                        /*+ "<div style='height:0.2em;'></div>"
                                    +"<br><div align='center'>"
                                        +"<img alt='generar xls' src='"+request.getContextPath()+"/images/botones/exportarExcel.gif' id='imgexc' name='imgxls' onMouseOver='botonOver(this);' onClick='generarDoc(\"xls\");' onMouseOut='botonOut(this);' style='cursor:pointer'>"
                                        +"<img alt='generar pdf' src='"+request.getContextPath()+"/images/botones/generarPdf.gif' id='imgpdf' name='imgpdf' onMouseOver='botonOver(this);' onClick='generarDoc(\"pdf\");' onMouseOut='botonOut(this);' style='cursor:pointer'>"
                                    +"</div>"
                                    +"<br>"
                                    +"<div id='mensaje' align='center'></div>";*/
            }
            else
            {
                if(opcion.equals("actualiza_dato"))
                {
                  redirect=false;
                String tipo = request.getParameter("tipo")!= null ? request.getParameter("tipo"):"";
                String codigo = request.getParameter("codigo")!= null ? request.getParameter("codigo"):"";
                String dato = request.getParameter("dato")!= null ? request.getParameter("dato"):"";
                String msg="";
                boolean pasa = true;
                    try {
                         cadenatabla = cserv.actualiza_dato(tipo, codigo, dato);
                    }
                    catch (Exception e) {
                        pasa = false;
                        System.out.println("Error en action: "+e.toString());
                        cadenatabla="Error en action: "+e.toString();
                        e.printStackTrace();
                    }

                }
                else
                {
                    if(opcion.equals("inserta_dato"))
                    {
                        redirect=false;
                        String tipo = request.getParameter("tipo")!= null ? request.getParameter("tipo"):"";
                        String codigo = request.getParameter("codigo")!= null ? request.getParameter("codigo"):"";
                        String dato = request.getParameter("dato")!= null ? request.getParameter("dato"):"";
                        boolean pasa = true;
                            try
                            {
                                 cadenatabla = cserv.inserta_dato(tipo, dato,user.getLogin());
                            }
                            catch (Exception e) {
                                pasa = false;
                                System.out.println("Error en action: "+e.toString());
                                cadenatabla="Error en action: "+e.toString();
                                e.printStackTrace();
                            }

                        }
                        else
                        {
                            redirect=true;
                            next += "cliente_cartera.jsp";
                         }
                 }

            }

        }
        catch (Exception e) {
            System.out.println("Error en GestionCarteraAction: "+e.toString());
            e.printStackTrace();
        }
        finally {
            try {
                if(redirect==false){
                    this.escribirResponse(cadenatabla);
                }
                else{
                    this.dispatchRequest(next);
                }
            } catch (Exception e) {
                System.out.println("Error al redireccionar o escribir respuesta: "+e.toString());
                e.printStackTrace();
            }
        }
    }

    /**
     * Escribe el resultado de la consulta en el response de Ajax
     * @param dato la cadena a escribir
     * @throws Exception cuando hay un error
     */
    protected void escribirResponse(String dato) throws Exception{
        try {
            response.setContentType("text/plain; charset=utf-8");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println(dato);
        }
        catch (Exception e) {
            throw new Exception("Error al escribir el response: "+e.toString());
        }
    }
      
    /**
     * Genera el pdf del estado de cuenta
     * @param user Usuario que lo genera
     * @param estado_cuenta Lista de documentos
     * @param listahead Datos para el encabezado
     * @param vec Otros datos del cliente que se incluyen
     * @throws Exception Cuando hay un error
     */
    private void generarPdf(String user, ArrayList<BeanGeneral> estado_cuenta, ArrayList<String> listahead,String[] vec) throws Exception{
        String directorio = "";
        System.out.println("inicia elaboracion pdf");
        try {
            directorio = cserv.directorioArchivo(user, "estado_cuenta_"+listahead.get(5), "pdf");
            System.out.println("Elaborando directorio");
            Document documento = null;
            Font fuente = new Font(Font.HELVETICA,8);
            Font fuenteG = new Font(Font.HELVETICA,8,Font.BOLD,new java.awt.Color(255,255,255));
            PdfWriter pwriter;
            PdfPCell celda;
            PdfPTable tabla;
            documento = cserv.createDoc();
            PdfWriter.getInstance(documento, new FileOutputStream( directorio ));
            //Rectangle page = documento.getPageSize();
            documento.open();
            documento.newPage();
            PdfPCell celda_vacia = new PdfPCell();
            celda_vacia.setBorderWidthTop(0);
            celda_vacia.setBorderWidthBottom(0);
            celda_vacia.setBorderWidthLeft(0);
            celda_vacia.setBorderWidthRight(0);
            celda_vacia.setColspan(6);
            celda_vacia.setPhrase(new Phrase(" ",fuente));
            celda_vacia.setBackgroundColor(new java.awt.Color(255,255,255));
            //encabezado del documento
            PdfPTable tcontainer = new PdfPTable(2);
            tcontainer.setWidthPercentage(100);
            PdfPTable thead = new PdfPTable(5);
            thead.setWidthPercentage(100);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Datos del Cliente",fuenteG));
            celda.setBackgroundColor(new java.awt.Color(26,126,0));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setColspan(2);
            celda.setPhrase(new Phrase(listahead.get(0),fuente));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Negocio",fuenteG));
            celda.setBackgroundColor(new java.awt.Color(26,126,0));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase(listahead.get(5),fuente));
            thead.addCell(celda);
            
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Nit",fuenteG));
            celda.setBackgroundColor(new java.awt.Color(26,126,0));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setColspan(4);
            celda.setPhrase(new Phrase(vec[0],fuente));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Direccion",fuenteG));
            celda.setBackgroundColor(new java.awt.Color(26,126,0));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setColspan(4);
            celda.setPhrase(new Phrase(vec[1],fuente));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Telefono",fuenteG));
            celda.setBackgroundColor(new java.awt.Color(26,126,0));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setColspan(4);
            celda.setPhrase(new Phrase(listahead.get(1),fuente));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Ciudad",fuenteG));
            celda.setBackgroundColor(new java.awt.Color(26,126,0));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setColspan(4);
            celda.setPhrase(new Phrase(vec[2],fuente));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Afiliado",fuenteG));
            celda.setBackgroundColor(new java.awt.Color(26,126,0));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setColspan(4);
            celda.setPhrase(new Phrase(listahead.get(10),fuente));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Sector",fuenteG));
            celda.setBackgroundColor(new java.awt.Color(26,126,0));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setColspan(4);
            celda.setPhrase(new Phrase(listahead.get(11),fuente));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.addElement(thead);
            celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
            celda.setBorder(0);
            tcontainer.addCell(celda);
            //
            thead = new PdfPTable(2);
            //thead.setWidthPercentage(100);
            GestionCarteraService cserv = new GestionCarteraService(this.user.getBd());
            double carterita = Double.parseDouble(request.getParameter("carterita")!=null ? request.getParameter("carterita"): "0");
            double recaudos = carterita - Double.parseDouble(cserv.saldoNegocio(listahead.get(5)));
            double gastos = cserv.sumatoria_ajustes(listahead.get(5));
            double prejurid = cserv.sumatoria_nc(listahead.get(5));
            double sum_tot = (carterita+gastos)-(recaudos+prejurid);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Total Cartera",fuenteG));
            celda.setBackgroundColor(new java.awt.Color(26,126,0));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase(Util.customFormat(carterita),fuente));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Total Recaudos",fuenteG));
            celda.setBackgroundColor(new java.awt.Color(26,126,0));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase(Util.customFormat((recaudos*-1)),fuente));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Gastos Cobranza",fuenteG));
            celda.setBackgroundColor(new java.awt.Color(26,126,0));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase(Util.customFormat(gastos),fuente));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Reportes Pre-Juridicos",fuenteG));
            celda.setBackgroundColor(new java.awt.Color(26,126,0));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase(Util.customFormat((prejurid*-1)),fuente));
	    celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Saldo Total",fuenteG));
            celda.setBackgroundColor(new java.awt.Color(26,126,0));
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase(Util.customFormat(sum_tot),fuente));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            thead.addCell(celda);
            celda = new PdfPCell();
            celda.addElement(thead);
            celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            celda.setBorder(0);
            tcontainer.addCell(celda);
            documento.add(tcontainer);
            //
            System.out.println("elaboracion cabecera listo");
            //cuerpo del documento
            PdfPTable tbody = new PdfPTable(9);
            tbody.setWidthPercentage(100);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Fecha",fuenteG));
            celda.setBackgroundColor(new java.awt.Color(26,126,0));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tbody.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Documento",fuenteG));
            celda.setBackgroundColor(new java.awt.Color(26,126,0));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
            tbody.addCell(celda);
            celda.setPhrase(new Phrase("Tipo",fuenteG));
            tbody.addCell(celda);
            celda.setPhrase(new Phrase("Valor factura",fuenteG));
            tbody.addCell(celda);
            celda.setPhrase(new Phrase("Valor ingreso",fuenteG));
            tbody.addCell(celda);
            celda.setPhrase(new Phrase("Vr. ajuste ingreso",fuenteG));
            tbody.addCell(celda);
            celda.setPhrase(new Phrase("Vr. aplicado",fuenteG));
            tbody.addCell(celda);
            celda.setPhrase(new Phrase("Cuenta de ajuste",fuenteG));
            tbody.addCell(celda);
            celda.setPhrase(new Phrase("Cuenta cabecera",fuenteG));
            tbody.addCell(celda);
            //inicia la generacion
            BeanGeneral beang = null;
            double suma1=0;
            double suma2=0;
            double suma3=0;
            double suma4=0;
            for(int i=0;i<estado_cuenta.size();i++){
                beang = estado_cuenta.get(i);
                suma1 += Double.parseDouble(beang.getValor_04().replaceAll(",", ""));
                suma2 += Double.parseDouble(beang.getValor_05().replaceAll(",", ""));
                suma3 += Double.parseDouble(beang.getValor_06().replaceAll(",", ""));
                suma4 += Double.parseDouble(beang.getValor_07().replaceAll(",", ""));
                celda = new PdfPCell();
                celda.setPhrase(new Phrase(beang.getValor_01(),fuente));
                celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tbody.addCell(celda);
                celda = new PdfPCell();
                celda.setPhrase(new Phrase(beang.getValor_02(),fuente));
                celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tbody.addCell(celda);
                celda = new PdfPCell();
                celda.setPhrase(new Phrase(beang.getValor_03(),fuente));
                celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tbody.addCell(celda);
                celda = new PdfPCell();
                celda.setPhrase(new Phrase(beang.getValor_04(),fuente));
                celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
                tbody.addCell(celda);
                celda = new PdfPCell();
                celda.setPhrase(new Phrase(beang.getValor_05(),fuente));
                celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
                tbody.addCell(celda);
                celda = new PdfPCell();
                celda.setPhrase(new Phrase(beang.getValor_06(),fuente));
                celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
                tbody.addCell(celda);
                celda = new PdfPCell();
                celda.setPhrase(new Phrase(beang.getValor_07(),fuente));
                celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
                tbody.addCell(celda);
                celda = new PdfPCell();
                celda.setPhrase(new Phrase(beang.getValor_08(),fuente));
                celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tbody.addCell(celda);
                celda = new PdfPCell();
                celda.setPhrase(new Phrase(beang.getValor_09(),fuente));
                celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
                tbody.addCell(celda);
                celda = null;
            }
            //se totaliza
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("Totales",fuente));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            celda.setColspan(3);
            tbody.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase(Util.customFormat(suma1),fuente));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            tbody.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase(Util.customFormat(suma2),fuente));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            tbody.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase(Util.customFormat(suma3),fuente));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            tbody.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase(Util.customFormat(suma4),fuente));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            tbody.addCell(celda);
            celda = new PdfPCell();
            celda.setPhrase(new Phrase("",fuente));
            celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
            celda.setColspan(2);
            tbody.addCell(celda);
            System.out.println("estado listo");
            documento.add(tbody);
            documento.close();
        }
        catch (Exception e) {
            throw new Exception("Error al generar el archivo xls: "+e.toString());
        }
        System.out.println("fin generacion pdf");
    }

    /**
     * Genera el xls del estado de cuenta
     * @param user Usuario que lo genera
     * @param estado_cuenta Lista de documentos
     * @param listahead Datos para el encabezado
     * @param vec Otros datos del cliente que se incluyen
     * @throws Exception Cuando hay un error
     */
    private void generarXls(String user,ArrayList<BeanGeneral> estado_cuenta,ArrayList<String> listahead,String[] vec) throws Exception{
        String directorio = "";
        System.out.println("inicia generacion de xls");
        try {
            directorio = cserv.directorioArchivo(user, "estado_cuenta_"+listahead.get(5), "xls");
            System.out.println("Elaborando directorio");
            ExcelApplication excel = this.instanciar(listahead.get(5));
            excel.setDataCell(0, 0, "Datos del Cliente");
            excel.setCellStyle(0, 0, excel.getStyle("estilo2"));
            excel.setDataCell(0, 1, listahead.get(0));
            excel.setCellStyle(0, 1, excel.getStyle("estilo3"));
            excel.setDataCell(0, 2, "Negocio");
            excel.setCellStyle(0, 2, excel.getStyle("estilo2"));
            excel.setDataCell(0, 3, listahead.get(5));
            excel.setCellStyle(0, 3, excel.getStyle("estilo3"));
            excel.setDataCell(1, 0, "Nit");
            excel.setCellStyle(1, 0, excel.getStyle("estilo2"));
            excel.setDataCell(1, 1, vec[0]);
            excel.setCellStyle(1, 1, excel.getStyle("estilo3"));
            excel.setDataCell(2, 0, "Direccion");
            excel.setCellStyle(2, 0, excel.getStyle("estilo2"));
            excel.setDataCell(2, 1, vec[1]);
            excel.setCellStyle(2, 1, excel.getStyle("estilo3"));
            excel.setDataCell(3, 0, "Telefono");
            excel.setCellStyle(3, 0, excel.getStyle("estilo2"));
            excel.setDataCell(3, 1, listahead.get(1));
            excel.setCellStyle(3, 1, excel.getStyle("estilo3"));
            excel.setDataCell(4, 0, "Ciudad");
            excel.setCellStyle(4, 0, excel.getStyle("estilo2"));
            excel.setDataCell(4, 1, vec[2]);
            excel.setCellStyle(4, 1, excel.getStyle("estilo3"));
            excel.setDataCell(5, 0, "Afiliado");
            excel.setCellStyle(5, 0, excel.getStyle("estilo2"));
            excel.setDataCell(5, 1, listahead.get(10));
            excel.setCellStyle(5, 1, excel.getStyle("estilo3"));
            excel.setDataCell(5, 2, "Sector");
            excel.setCellStyle(5, 2, excel.getStyle("estilo2"));
            excel.setDataCell(5, 3, listahead.get(11));
            excel.setCellStyle(5, 3, excel.getStyle("estilo3"));
            //
            GestionCarteraService cserv = new GestionCarteraService(this.user.getBd());
            double carterita = Double.parseDouble(request.getParameter("carterita")!=null ? request.getParameter("carterita"): "0");
            double recaudos = carterita - Double.parseDouble(cserv.saldoNegocio(listahead.get(5)));
            double gastos = cserv.sumatoria_ajustes(listahead.get(5));
            double prejurid = cserv.sumatoria_nc(listahead.get(5));
            double sum_tot = (carterita+gastos)-(recaudos+prejurid);
            excel.setDataCell(0, 5, "Total Cartera");
            excel.setCellStyle(0, 5, excel.getStyle("estilo2"));
            excel.setDataCell(0, 6, Double.parseDouble(Util.customFormat(carterita).replaceAll(",", "")));
            excel.setCellStyle(0, 6, excel.getStyle("estilo3"));
            excel.setDataCell(1, 5, "Total Recaudos");
            excel.setCellStyle(1, 5, excel.getStyle("estilo2"));
            excel.setDataCell(1, 6, Double.parseDouble(Util.customFormat(recaudos*-1).replaceAll(",", "")));
            excel.setCellStyle(1, 6, excel.getStyle("estilo3"));
            excel.setDataCell(2, 5, "Gastos Cobranza");
            excel.setCellStyle(2, 5, excel.getStyle("estilo2"));
            excel.setDataCell(2, 6,  Double.parseDouble(Util.customFormat(gastos).replaceAll(",", "")));
            excel.setCellStyle(2, 6, excel.getStyle("estilo3"));
            excel.setDataCell(3, 5, "Reportes Pre-Juridicos");
            excel.setCellStyle(3, 5, excel.getStyle("estilo2"));
            excel.setDataCell(3, 6,  Double.parseDouble(Util.customFormat(prejurid*-1).replaceAll(",", "")));
            excel.setCellStyle(3, 6, excel.getStyle("estilo3"));
            excel.setDataCell(4, 5, "Saldo Total");
            excel.setCellStyle(4, 5, excel.getStyle("estilo2"));
            excel.setDataCell(4, 6,  Double.parseDouble(Util.customFormat(sum_tot).replaceAll(",", "")));
            excel.setCellStyle(4, 6, excel.getStyle("estilo3"));
            //
            excel.setDataCell(7, 0, "Fecha");
            excel.setCellStyle(7, 0, excel.getStyle("estilo2"));
            excel.setDataCell(7, 1, "Documento");
            excel.setCellStyle(7, 1, excel.getStyle("estilo2"));
            excel.setDataCell(7, 2, "Tipo");
            excel.setCellStyle(7, 2, excel.getStyle("estilo2"));
            excel.setDataCell(7, 3, "Valor factura");
            excel.setCellStyle(7, 3, excel.getStyle("estilo2"));
            excel.setDataCell(7, 4, "Valor ingreso");
            excel.setCellStyle(7, 4, excel.getStyle("estilo2"));
            excel.setDataCell(7, 5, "Vr ajuste ingreso");
            excel.setCellStyle(7, 5, excel.getStyle("estilo2"));
            excel.setDataCell(7, 6, "Vr aplicado");
            excel.setCellStyle(7, 6, excel.getStyle("estilo2"));
            /*excel.setDataCell(7, 7, "Cuenta de ajuste");
            excel.setCellStyle(7, 7, excel.getStyle("estilo2"));
            excel.setDataCell(7, 8, "Cuenta cabecera");
            excel.setCellStyle(7, 8, excel.getStyle("estilo2"));*/
            System.out.println("cabecera lista");
            int filaAct = 8;
            BeanGeneral beang = null;
            //se escriben los registros
            double suma1=0;
            double suma2=0;
            double suma3=0;
            double suma4=0;
            for(int i=0;i<estado_cuenta.size();i++){
                beang = estado_cuenta.get(i);
                suma1 += Double.parseDouble(beang.getValor_04().replaceAll(",", ""));
                suma2 += Double.parseDouble(beang.getValor_05().replaceAll(",", ""));
                suma3 += Double.parseDouble(beang.getValor_06().replaceAll(",", ""));
                suma4 += Double.parseDouble(beang.getValor_07().replaceAll(",", ""));
                excel.setDataCell(filaAct, 0, beang.getValor_01());
                excel.setCellStyle(filaAct, 0, excel.getStyle("estilo3"));
                excel.setDataCell(filaAct, 1, beang.getValor_02());
                excel.setCellStyle(filaAct, 1, excel.getStyle("estilo3"));
                excel.setDataCell(filaAct, 2, beang.getValor_03());
                excel.setCellStyle(filaAct, 2, excel.getStyle("estilo3"));
                excel.setDataCell(filaAct, 3, Double.parseDouble(beang.getValor_04().replaceAll(",", "")));
                excel.setCellStyle(filaAct, 3, excel.getStyle("estilo3"));
                excel.setDataCell(filaAct, 4, Double.parseDouble(beang.getValor_05().replaceAll(",", "")));
                excel.setCellStyle(filaAct, 4, excel.getStyle("estilo3"));
                excel.setDataCell(filaAct, 5, Double.parseDouble(beang.getValor_06().replaceAll(",", "")));
                excel.setCellStyle(filaAct, 5, excel.getStyle("estilo3"));
                excel.setDataCell(filaAct, 6, Double.parseDouble(beang.getValor_07().replaceAll(",", "")));
                excel.setCellStyle(filaAct, 6, excel.getStyle("estilo3"));
                filaAct += 1;
            }
            excel.setDataCell(filaAct, 2,  "Totales");
            excel.setCellStyle(filaAct, 2, excel.getStyle("estilo2"));
            excel.setDataCell(filaAct, 3,  ""+(suma1));
            excel.setCellStyle(filaAct, 3, excel.getStyle("estilo3"));
            excel.setDataCell(filaAct, 4,  ""+(suma2));
            excel.setCellStyle(filaAct, 4, excel.getStyle("estilo3"));
            excel.setDataCell(filaAct, 5,  ""+(suma3));
            excel.setCellStyle(filaAct, 5, excel.getStyle("estilo3"));
            excel.setDataCell(filaAct, 6,  ""+(suma4));
            excel.setCellStyle(filaAct, 6, excel.getStyle("estilo3"));
            System.out.println("estado listo");
            //y se guarda
            excel.saveToFile(directorio);
            System.out.println("guardado listo");
        }
        catch (Exception e) {
            throw new Exception("Error al generar el archivo xls: "+e.toString());
        }
        System.out.println("fin proceso xls");
    }

   

    /**
     * Crea un objeto tipo ExcelApplication
     * @param descripcion Nombre de la hoja principal del libro
     * @return Objeto ExcelApplication creado
     * @throws Exception Cuando hay error
     */
    private ExcelApplication instanciar(String descripcion) throws Exception{
        ExcelApplication excel = new ExcelApplication();
        try{
            excel.createSheet(descripcion);
            excel.createFont("Titulo", "Arial", (short)1, true, (short)12);
            excel.createFont("Subtitulo", "Verdana", (short)0, true, (short)10);
            excel.createFont("Contenido", "Verdana", (short)0, false, (short)10);

            excel.createColor((short)11, (byte)255, (byte)255, (byte)255);//Blanco
            excel.createColor((short)9, (byte)26, (byte)126, (byte)0);//Verde dark
            excel.createColor((short)10, (byte)37, (byte)69, (byte)255);//Azul oscuro

            excel.createStyle("estilo1", excel.getFont("Titulo"), (short)10, true, "@");
            excel.createStyle("estilo2", excel.getFont("Subtitulo"), (short)9, true, "@");
            excel.createStyle("estilo3", excel.getFont("Contenido"), (short)11, true, "@");

        }
        catch(Exception e){
            throw new Exception("Error al instanciar el objeto: "+e.toString());
        }
        return excel;
    }

    /**
     * Genera el pdf de facturacion
     * @param user Usuario que lo genera
     * @param listafacts Lista de facturas
     * @param consneg Numero del negocio
     * @throws Exception Cuando hay un error
     */
    private void generarPdf2(String user,ArrayList<factura> listafacts,String consneg) throws Exception{
        String directorio = "";
        System.out.println("inicia elaboracion pdf");
        try {
            directorio = cserv.directorioArchivo(user, "facturacion_"+consneg, "pdf");
            System.out.println("Elaborando directorio");
            Document documento = null;
            Font fuente = new Font(Font.HELVETICA,8);
            Font fuenteG = new Font(Font.HELVETICA,8,Font.BOLD,new java.awt.Color(255,255,255));
            PdfPCell celda = null;
            PdfPTable tabla = null;
            factura fact = null;
            documento = cserv.createDoc();
            PdfWriter.getInstance(documento, new FileOutputStream( directorio ));
            //Rectangle page = documento.getPageSize();
            documento.open();
            documento.newPage();
            documento.add(new Paragraph(new Phrase("Facturacion del negocio "+consneg)));
            documento.add(new Paragraph(new Phrase("\n")));
            documento.add(new Paragraph(new Phrase("\n")));
            tabla = new PdfPTable(11);
            tabla.setWidthPercentage(100);
            celda = new PdfPCell();
            celda.setBackgroundColor(new java.awt.Color(3,167,12));
            celda.setPhrase(new Phrase("Factura",fuenteG));
            tabla.addCell(celda);
            celda.setPhrase(new Phrase("Relacionada",fuenteG));
            tabla.addCell(celda);
            celda.setPhrase(new Phrase("No. titulo",fuenteG));
            tabla.addCell(celda);
            celda.setPhrase(new Phrase("Vencimiento",fuenteG));
            tabla.addCell(celda);
            celda.setPhrase(new Phrase("Dias mora",fuenteG));
            tabla.addCell(celda);
            celda.setPhrase(new Phrase("Venc. prejuridico",fuenteG));
            tabla.addCell(celda);
            celda.setPhrase(new Phrase("Estado",fuenteG));
            tabla.addCell(celda);
            celda.setPhrase(new Phrase("Ult. abono",fuenteG));
            tabla.addCell(celda);
            celda.setPhrase(new Phrase("Abonos",fuenteG));
            tabla.addCell(celda);
            celda.setPhrase(new Phrase("Valor",fuenteG));
            tabla.addCell(celda);
            celda.setPhrase(new Phrase("Saldo",fuenteG));
            tabla.addCell(celda);
            for(int i=0;i<listafacts.size();i++){
                fact = listafacts.get(i);
                celda = new PdfPCell();
                celda.setPhrase(new Phrase(fact.getDocumento(),fuente));
                tabla.addCell(celda);
                celda.setPhrase(new Phrase(fact.getFactura(),fuente));
                tabla.addCell(celda);
                celda.setPhrase(new Phrase(fact.getFisico(),fuente));
                tabla.addCell(celda);
                celda.setPhrase(new Phrase(fact.getFecha_vencimiento(),fuente));
                tabla.addCell(celda);
                celda.setPhrase(new Phrase(fact.getDias_mora()+"",fuente));
                tabla.addCell(celda);
                celda.setPhrase(new Phrase(fact.getFecha_ven_prejurid(),fuente));
                tabla.addCell(celda);
                celda.setPhrase(new Phrase(fact.getEstado(),fuente));
                tabla.addCell(celda);
                celda.setPhrase(new Phrase(fact.getFecha_ultimo_pago(),fuente));
                tabla.addCell(celda);
                celda.setHorizontalAlignment(PdfPCell.ALIGN_RIGHT);
                celda.setPhrase(new Phrase(fact.getValor_abono()+"",fuente));
                tabla.addCell(celda);
                celda.setPhrase(new Phrase(fact.getValor_factura()+"",fuente));
                tabla.addCell(celda);
                celda.setPhrase(new Phrase(fact.getValor_saldo()+"",fuente));
                tabla.addCell(celda);
            }
            documento.add(tabla);
            documento.close();
        }
        catch (Exception e) {
            throw new Exception("Error al generar el archivo pdf: "+e.toString());
        }
        System.out.println("fin generacion pdf");
    }

    /**
     * Genera el xls de facturacion
     * @param user Usuario que lo genera
     * @param listafacts Lista de facturas
     * @param consneg Numero del negocio
     * @throws Exception Cuando hay un error
     */
    private void generarXls2(String user,ArrayList<factura> listafacts, String consneg) throws Exception{
        String directorio = "";
        System.out.println("inicia generacion de xls");
        try {
            directorio = cserv.directorioArchivo(user,"facturacion_"+ consneg, "xls");
            System.out.println("Elaborando directorio");
            ExcelApplication excel = this.instanciar(consneg);
            excel.setDataCell(1, 0, "Facturacion del negocio");
            excel.setCellStyle(1, 0, excel.getStyle("estilo2"));
            excel.setDataCell(1, 1, consneg);
            excel.setCellStyle(1, 1, excel.getStyle("estilo3"));
            excel.setDataCell(2, 0, "Factura");
            excel.setCellStyle(2, 0, excel.getStyle("estilo2"));
            excel.setDataCell(2, 1, "Relacionada");
            excel.setCellStyle(2, 1, excel.getStyle("estilo2"));
            excel.setDataCell(2, 2, "No. titulo");
            excel.setCellStyle(2, 2, excel.getStyle("estilo2"));
            excel.setDataCell(2, 3, "Vencimiento");
            excel.setCellStyle(2, 3, excel.getStyle("estilo2"));
            excel.setDataCell(2, 4, "Dias mora");
            excel.setCellStyle(2, 4, excel.getStyle("estilo2"));
            excel.setDataCell(2, 5, "Venc. prejuridico");
            excel.setCellStyle(2, 5, excel.getStyle("estilo2"));
            excel.setDataCell(2, 6, "Estado");
            excel.setCellStyle(2, 6, excel.getStyle("estilo2"));
            excel.setDataCell(2, 7, "Ult. abono");
            excel.setCellStyle(2, 7, excel.getStyle("estilo2"));
            excel.setDataCell(2, 8, "Abonos");
            excel.setCellStyle(2, 8, excel.getStyle("estilo2"));
            excel.setDataCell(2, 9, "Valor");
            excel.setCellStyle(2, 9, excel.getStyle("estilo2"));
            excel.setDataCell(2, 10, "Saldo");
            excel.setCellStyle(2, 10, excel.getStyle("estilo2"));
            factura fact = null;
            int filplus = 3;
            for(int i=0;i<listafacts.size();i++){
                fact = listafacts.get(i);
                excel.setDataCell(filplus, 0, fact.getDocumento());
                excel.setCellStyle(filplus, 0, excel.getStyle("estilo3"));
                excel.setDataCell(filplus, 1, fact.getFactura());
                excel.setCellStyle(filplus, 1, excel.getStyle("estilo3"));
                excel.setDataCell(filplus, 2, fact.getFisico());
                excel.setCellStyle(filplus, 2, excel.getStyle("estilo3"));
                excel.setDataCell(filplus, 3, fact.getFecha_vencimiento());
                excel.setCellStyle(filplus, 3, excel.getStyle("estilo3"));
                excel.setDataCell(filplus, 4, fact.getDias_mora());
                excel.setCellStyle(filplus, 4, excel.getStyle("estilo3"));
                excel.setDataCell(filplus, 5, fact.getFecha_ven_prejurid());
                excel.setCellStyle(filplus, 5, excel.getStyle("estilo3"));
                excel.setDataCell(filplus, 6, fact.getEstado());
                excel.setCellStyle(filplus, 6, excel.getStyle("estilo3"));
                excel.setDataCell(filplus, 7, fact.getFecha_ultimo_pago());
                excel.setCellStyle(filplus, 7, excel.getStyle("estilo3"));
                excel.setDataCell(filplus, 8, fact.getValor_abono());
                excel.setCellStyle(filplus, 8, excel.getStyle("estilo3"));
                excel.setDataCell(filplus, 9, fact.getValor_factura());
                excel.setCellStyle(filplus, 9, excel.getStyle("estilo3"));
                excel.setDataCell(filplus, 10, fact.getValor_saldo());
                excel.setCellStyle(filplus, 10, excel.getStyle("estilo3"));
                filplus++;
                fact = null;
            }
            System.out.println("facturas listo");
            //y se guarda
            excel.saveToFile(directorio);
            System.out.println("guardado listo");
        }
        catch (Exception e) {
            throw new Exception("Error al generar el archivo xls: "+e.toString());
        }
        System.out.println("fin proceso xls");
    }

}