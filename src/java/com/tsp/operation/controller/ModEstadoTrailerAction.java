/***************************************************************************
 * Nombre clase : ............... modEstadoTrailer.java                    *
 * Descripcion :................. Clase que maneja los eventos             *
 *                                relacionados con el programa de          *
 *                                modificacion del estado del trailer      *
 * Autor :.......................  Ing. Enrique De Lavalle                 *
 * Fecha :........................ 7 de Mayo de 2007, 3:03 PM              *
 * Version :...................... 1.0                                     *
 * Copyright :.................... Fintravalores S.A.                 *
 ***************************************************************************/
package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.util.*;
import org.apache.log4j.*;
/**
 *
 * @author  edelavalle
 */
public class ModEstadoTrailerAction {/*extends Action{
    
    
    public ModEstadoTrailerAction() {
    }
    
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
         /*
          *Declaracion de variables
          *//*
        String placa           = (request.getParameter("pTrailer")!=null)?request.getParameter("pTrailer").toUpperCase():"";
        String estado        = (request.getParameter("estadoT")!=null)?request.getParameter("estadoT"):"";
        String opcion        = (request.getParameter("opcion")!=null)?request.getParameter("opcion"):"";
        HttpSession Session    = request.getSession();
        Usuario user           = new Usuario();
        user                   = (Usuario)Session.getAttribute("Usuario");
        String dstrct          = user.getDstrct();
        String next            = "";
        
        try{
            
            
            System.out.println("placa:  "+placa +"  estado:  "+estado);
            
            if(opcion.equals("BUSCAR_TRAILER")){
                next = "/placas/modEstadoTrailer.jsp";
                model.placaService.buscaPlacaTipo(placa, "T");
                
                if(model.placaService.getPlaca()== null){
                   next += "?msg=El trailer Digitado no existe!!!";   
                
                }else{
                
                    model.paisservice.listarpaises();
                    model.placaService.getPlaca().setPaisUbicacion("");
                    if(!model.placaService.getPlaca().getUbicacion().equals("")){
                        Ciudad ciudad_o = model.ciudadService.obtenerCiudad(model.placaService.getPlaca().getUbicacion());
                        model.ciudadservice.listarCiudadesxpais(ciudad_o.getPais());
                        model.placaService.getPlaca().setPaisUbicacion(ciudad_o.getPais());


                        model.ciudadService.setCiudadO(model.ciudadservice.obtenerCiudades());
                    }
                    //request.setAttribute("ciudadO", model.ciudadservice.obtenerCiudades());
                   
                }
            }else if(opcion.equals("BUSCAR_CIUDADES")){
                next = "/placas/modEstadoTrailer.jsp";
                String pais = request.getParameter("pais");
                model.ciudadservice.listarCiudadesxpais(pais);
                model.ciudadService.setCiudadO(model.ciudadservice.obtenerCiudades());
                model.placaService.getPlaca().setPaisUbicacion(pais);
            
                
                
            }else{    
                    String ubicacion = request.getParameter("ubicacion");
                    model.placaService.modEstadoPlacaTrailer(model.placaService.getPlaca().getPlaca(), estado, ubicacion );
                    model.placaService.setPlaca(null);
                    next = "/placas/modEstadoTrailer.jsp?msg=Estado del Trailer Modificado Exitosamente!!!";
                
            }
            
            
            System.out.println("next:  "+next);
            
            
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en modEstadoTrailerAction .....\n"+e.getMessage());
        }
        
        this.dispatchRequest( next );
    }*/
    
}
