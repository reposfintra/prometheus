/*************************************************************************
 * Nombre ......................InfoActxRemesaAction.java                *
 * Descripci�n..................Clase Action para buscar actividad       *
 * Autor........................LREALES                                  *
 * Fecha........................3 de agosto de 2006, 10:02 AM            *
 * Versi�n......................1.0                                      *
 * Coyright.....................Transportes Sanchez Polo S.A.            *
 *************************************************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class InfoActxRemesaAction extends Action {
    
    /** Creates a new instance of InfoActxRemesaAction */
    public InfoActxRemesaAction() {}
    
    public void run() throws ServletException, InformationException {
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        
        //String next = "/"+request.getParameter("carpeta")+"/"+request.getParameter("pagina");
        String next = "";
        String next1 = "/jsp/trafico/actividad/Infoactividad/BuscarClientexRemesa.jsp";
        String next2 = "/jsp/trafico/actividad/Infoactividad/ClientesxRemesa.jsp";
        String next3 = "/jsp/trafico/actividad/Infoactividad/VerDatosActividad.jsp";
        
        String numrem = request.getParameter("numrem");
              
        Vector info = new Vector();
        
        try{
            
            model.clientactSvc.consultarActividades( numrem );
            info = model.clientactSvc.obtVecClientAct();
                        
            if ( numrem.equals("") ) {
                
                //no dejar campos vacios
                next = next1 + "?men=Por favor digite un Numero de Remesa a consultar!";
                
            } else if ( info == null ) {
                
                //la busqueda no arrojo resultados
                next = next1 + "?men=Su busqueda no arrojo resultados!";
                
            } else if ( info != null && info.size() == 1 ) {
                
                ClienteActividad cli_act = (ClienteActividad) info.elementAt(0);
                //mando directamente a la pagina qu emuestra la informacion
                next = "/controller?estado=Infoact&accion=Busact&carpeta=jsp/trafico/actividad/Infoactividad&pagina=VerDatosActividad.jsp&numpla="+cli_act.getPlanilla()+"&numrem="+cli_act.getRemesa()+"&nomcliente="+cli_act.getNomCliente()+"&cliente="+cli_act.getCodCliente()+"&tipo="+cli_act.getTipoViaje()+"&codact="+cli_act.getCodActividad()+"&est=ver";
                
            } else if ( info != null && info.size() > 0 ) {
                
                //muestro todas las actividades que tiene esa remesa, pa q escoja
                next = next2;
                
            } else {
                
                //la busqueda no arrojo resultados
                next = next1 + "?men=Su busqueda no arrojo resultados!";
                
            }           
            
        } catch ( SQLException e ){
            
            e.printStackTrace();
            throw new ServletException( e.getMessage() );
            
        }
        
        // Redireccionar a la p�gina indicada.
        this.dispatchRequest( next );
        
    }
    
}