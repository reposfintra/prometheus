/**
 *
 */
package com.tsp.operation.controller;

import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import java.io.IOException;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import java.util.*;
import java.text.*;



/**
 * Searches the database for puchase orders matching the
 * purchase search argument
 */
public class PlanViajeQryAgAction extends Action {
    static Logger logger = Logger.getLogger(PlanViajeQryAgAction.class);
    
    /**
     * Executes the action
     */
    public void run() throws ServletException, InformationException {
        String next = "";
        String fechaini;
        String fechafin;
        String agencia;
       
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        Calendar fecha = Calendar.getInstance();
        SimpleDateFormat FFormat = new SimpleDateFormat("yyyyMMdd"); 
        fechaini = request.getParameter("fechaini");
        fechafin = request.getParameter("fechafin");
        agencia = request.getParameter("agencia");

        if ((fechaini == null)||fechaini.equals("")){
            fechaini = FFormat.format(fecha.getTime());
        }
        else{
           fechaini = fechaini.replace("-", "");
        }

        if ((fechafin == null)||fechafin.equals("")){
            fechafin = FFormat.format(fecha.getTime());
        }
        else{
            fechafin = fechafin.replace("-", "");
        }

        if ((agencia == null) || agencia.equals("")){
            agencia = "%";
        }

        try{
           model.planViajeService.planViajeQryAg(fechaini, fechafin, agencia, usuario.getDpto());
           next = "/planviaje/PantallaFiltroPlanViaje.jsp";
           logger.info(usuario.getNombre() + " ejecuto consulta planViaje con:Fecha Inicial: "+fechaini+", Fecha Final: "+fechafin+", Agencia: " + agencia);
        }
        catch (SQLException e){
            throw new ServletException(e.getMessage());
        }

        // Redireccionar a la p�gina indicada.
        this.dispatchRequest(next);    
    }
}
