/*
 * BugReportSendAction.java
 *
 * Created on 7 de abril de 2005, 02:44 PM
 */

package com.tsp.operation.controller;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;

/**
 * Acci�n invocada por el controlador cuando se va a enviar un reporte de error
 * desde la p�gina destinada para ese prop�sito (/ErrorPage.jsp).
 * @author  Ivan Herazo
 */
public class BugReportSendAction extends Action
{
  private static Logger logger = Logger.getLogger(BugReportSendAction.class);
  public void run() throws ServletException, InformationException
  {
    boolean bugRptSent = false;
    try {
      HttpSession session = request.getSession();
      Usuario loggedUser = (Usuario) session.getAttribute("Usuario");
      String userEmail  = null;
      String userName   = null;
      String smtpServer = null;
      String senderName = null;
      if( loggedUser != null )
      {
        userEmail = loggedUser.getEmail();
        userName  = loggedUser.getNombre();
        smtpServer = "mail";
        senderName = "\nNOTA: REPORTE DE ERROR ENVIADO POR: " + userName + "\n\n";
      }else{
        userEmail = "kreales@mail.tsp.com";
        userName  = "Ing. Carlos A. Pe�a B.";
        smtpServer = "proxy";
        senderName = "\nNOTA: NO SE PUDO DETERMINAR QU� USUARIO ENVI� " +
                     "EL REPORTE DE ERROR.\n\n";
      }
      Email emailData = new Email();
      emailData.setEmailfrom(userEmail);
      emailData.setSenderName(userName);
      if( smtpServer.equals("mail") )
      {
        emailData.setEmailto("kreales@mail.tsp.com");
        emailData.setEmailcopyto("");
      }else{
        emailData.setEmailto("kreales@mail.tsp.com");
        emailData.setEmailcopyto("kreales@mail.tsp.com");
      }
      emailData.setEmailsubject("TRANSPORTE MASIVO - Informe de Errores.");
      emailData.setEmailbody(
        (senderName != null ? senderName : "") +
        request.getParameter("bug.fullReport")
      );
      Email.send(smtpServer + ".tsp.com", emailData);
      bugRptSent = true;
    }catch (EmailSendingException EmailSendingE){
      logger.error(request.getParameter("bug.fullReport"));
    }finally{
      request.setAttribute(
        "com.tsp.operation.BugReportSendAction.failure", (bugRptSent ? "false" : "true")
      );
    }
    
    this.dispatchRequest("/error/ErrorPage.jsp");
  }
}