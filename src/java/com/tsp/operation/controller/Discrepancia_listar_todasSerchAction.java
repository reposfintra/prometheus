/************************************************************************
 * Nombre Discrepancia_listar_todasSerchAction.java
 * Descripci�n: Accion para buscar las discrepancias dada una planilla.
 * Autor: Jose de la rosa
 * Fecha: 24 de octubre de 2005, 04:38 PM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 **************************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;

public class Discrepancia_listar_todasSerchAction extends Action {
    
    /** Creates a new instance of Discrepancia_listar_todasSerchAction */
    public Discrepancia_listar_todasSerchAction () {
    }
    
    public void run () throws ServletException, com.tsp.exceptions.InformationException {
        String next="";
        HttpSession session = request.getSession ();
        String distrito = (String) session.getAttribute ("Distrito");
        String listar = request.getParameter ("listar");
        String planilla = request.getParameter ("planilla");
        try {
            if(listar.equals ("True")) {
                model.discrepanciaService.listDiscrepanciaTodas (planilla);
                model.discrepanciaService.getDiscrepancias ();
                next="/jsp/cumplidos/discrepancia/DiscrepanciaListar.jsp?marco=no";
            }
            else {
                String sw = (String) request.getParameter ("sw");
                if (sw.equals ("false")) {
                    String numrem = (String) request.getParameter ("c_numrem");
                    String numpla = (String) request.getParameter ("c_numpla");
                    String tipo_doc = (String) request.getParameter ("c_tipo_doc");
                    String documento = (String) request.getParameter ("c_documento");
                    String tipo_doc_rel = (String) request.getParameter ("c_tipo_doc_rel");
                    String documento_rel = (String) request.getParameter ("c_documento_rel");
                    String num_discre = (String) request.getParameter ("c_num_discre");
                    next="/jsp/cumplidos/discrepancia/DiscrepanciaConsulta.jsp?c_num_discre="+num_discre+"&c_documento="+documento+"&c_tipo_doc="+tipo_doc+"&c_numrem="+numrem+"&sw=true&campos=false&c_numpla="+numpla+"&exis=false&marco=no&campos=false&paso=false&c_tipo_doc_rel="+tipo_doc_rel+"&c_documento_rel="+documento_rel;
                }
                else {
                    String numrem = (String) request.getParameter ("c_numrem");
                    String numpla = (String) request.getParameter ("c_numpla");
                    String tipo_doc = (String) request.getParameter ("c_tipo_doc");
                    String documento = (String) request.getParameter ("c_documento");
                    String num_discre = (String) request.getParameter ("c_num_discre");
                    String tipo_doc_rel = (String) request.getParameter ("c_tipo_doc_rel");
                    String documento_rel = (String) request.getParameter ("c_documento_rel");
                    if (model.discrepanciaService.existeDiscrepancia (numpla, numrem, tipo_doc, documento, tipo_doc_rel, documento_rel, distrito ) ) {
                        model.discrepanciaService.searchDiscrepancia (numpla, numrem, tipo_doc, documento, tipo_doc_rel, documento_rel, distrito );
                        Discrepancia discrepa = model.discrepanciaService.getDiscrepancia ();
                        next="/jsp/cumplidos/discrepancia/DiscrepanciaConsulta.jsp?c_numrem="+numrem+"&sw=true&exis=true&campos=true&c_numpla"+numpla+"&c_tipo_doc="+tipo_doc+"&c_documento="+documento+"&paso=false&marco=no&c_tipo_doc_rel="+tipo_doc_rel+"&c_documento_rel="+documento_rel;
                    }
                    else {
                        next="/jsp/cumplidos/discrepancia/DiscrepanciaConsulta.jsp?c_numrem="+numrem+"&sw=true&exis=false&campos=true&c_numpla"+numpla+"&c_tipo_doc="+tipo_doc+"&c_documento="+documento+"&paso=false&marco=no&c_tipo_doc_rel="+tipo_doc_rel+"&c_documento_rel="+documento_rel;
                    }
                }
            }
            //next = Util.LLamarVentana (next, "Listar Discrepancias");
        }catch (SQLException e) {
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
    
}
