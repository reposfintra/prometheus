/*
 * LiquidacionPrestamosAction.java
 *
 * Created on 10 de julio de 2007, 03:54 PM
 */

package com.tsp.operation.controller;


import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.Util;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import java.util.*;
/**
 *
 * @author  equipo
 */
public class LiquidacionPrestamosAction extends Action {
    
    HttpSession session;
    Usuario usuario;
    
    /** Creates a new instance of LiquidacionPrestamosAction */
    public LiquidacionPrestamosAction() {
    }  
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        
        try{
            session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            String next = "/anticipos/PagosPrestamos.jsp";
            String opcion = Util.coalesce( request.getParameter("opcion"), "");
            if (opcion.equalsIgnoreCase("LoadPago")){
                obtenerDatosPagosPrestamos();
            } else if ( opcion.equalsIgnoreCase("Liquidar") ){
                liquidarPrestamos();
            } else if ( opcion.equalsIgnoreCase("ReportePago") ){
                reportePagos();
                next = "/jsp/cxpagar/prestamos/consultas/ConsultaAbonos.jsp";
            }
            
            
            this.dispatchRequest(next);
        } catch (Exception e){
            e.printStackTrace();
            throw new ServletException ( e.getMessage() );
        }
    }
    
    
    
    
    public void obtenerDatosPagosPrestamos () throws Exception{
        try{            
            String beneficiario = Util.coalesce( request.getParameter("beneficiario"), "");
            Vector anticipos = model.AnticiposPagosTercerosSvc.getPlanillasAbonosPrestamosOrdenadas(beneficiario);
            
            String fecha = "now()";
            
            if (anticipos.size()>0){
                AnticiposTerceros a = (AnticiposTerceros)anticipos.lastElement();
                fecha = a.getFecha_anticipo();
            }
            
            Vector prestamos = model.PrestamoSvc.obtenerPrestamosLiquidacionOrderBy (beneficiario, fecha );   
            
           
            
            //System.out.println("FECHA ANTICIPO_"+a.getFecha_anticipo());
            
            model.PrestamoSvc.setPrestamos(prestamos);
            model.PrestamoSvc.setAnticipos(anticipos);
           
            
        }catch (Exception e){
            e.printStackTrace();
            throw new Exception ( e.getMessage() );
        }
    }
    
    public void liquidarPrestamos() throws Exception {
        try{
            String beneficiario = Util.coalesce( request.getParameter("beneficiario"), "");
            
            model.proveedorService.obtenerProveedorPorNit(beneficiario);
            Proveedor propietario = model.proveedorService.getProveedor();
            
            String ids          = "";
            double t_planillas  = getParameterDouble("t_planillas");
            double t_prestamos  = getParameterDouble("t_prestamos");
            double consignacion = getParameterDouble("t_consignacion");
            double saldo_pro    = getParameterDouble("t_saldo_propietario");
            double saldo_pre    = getParameterDouble("t_saldo_prestamo");
            
            
            Vector anticipos = model.PrestamoSvc.getAnticipos();
            Vector prestamos = model.PrestamoSvc.getPrestamos();
            
            AnticiposTerceros anti = (AnticiposTerceros)anticipos.lastElement();
            System.out.println("anti_"+anti.getFecha_anticipo());
            
            if (propietario==null){
                request.setAttribute("msg","No hay propietario definido para la liquidacion.");
                return ;
            }            
            // validaciones simples
            if (consignacion < 0 ){
                request.setAttribute("msg","No se pueden registrar pagos negativos.");
                return ;
            }
            if (saldo_pro < 0 ){
                request.setAttribute("msg","Fondos insuficientes del propietario.");
                return ;
            }
            if (saldo_pre < 0 ){
                request.setAttribute("msg","Se esta cancelando un mayor valor a la deuda de los prestamos.");
                return ;
            }
            
            
            
            
            
            
            // proceso de liqiuidacion de prestamos
            if (anticipos!=null && !anticipos.isEmpty() && prestamos!=null && !prestamos.isEmpty() ){
                Vector consultas = new Vector();
                double consignacion_aux = consignacion;
                
                
                // tranferencias de las planillas de la liquidacion.
                for ( int i = 0 ; i<anticipos.size(); i++){
                    AnticiposTerceros ant = (AnticiposTerceros) anticipos.get(i);
                    consultas.add(model.AnticiposPagosTercerosSvc.updatePlanillaPrestamosSQL( ant, usuario.getLogin() ) );
                    ids += ( ids.equals("") ? "" : "," ) + ant.getId();
                }                
                
                
                // grabacion de las nuevas amortizaciones
                for (int i = 0; i < prestamos.size(); i++ ) {                
                    Prestamo p = (Prestamo) prestamos.get(i);
                            
                    if (consignacion_aux > 0){
                        
                        double valor   = p.getMonto() + p.getIntereses();
                        double abono   = 0;
                        double saldo   = 0;
                        if ( consignacion_aux > valor ){
                            abono = valor;                                
                            saldo = 0;        
                            consignacion_aux -= valor;
                        }
                        else{
                            abono = consignacion_aux;
                            saldo = valor - abono;
                            consignacion_aux = 0;
                        }                            
                        p.setCuotas( p.getCuotas() + 1  );
                        
                        Amortizacion am = new Amortizacion();                        
                        am.setDstrct         ( p.getDistrito()     );
                        am.setBeneficiario   ( p.getBeneficiario() );
                        am.setTercero        ( p.getTercero()      );
                        am.setCuota          ( p.getCuotas()       );
                        am.setMonto          ( valor               );
                        am.setTotalAPagar    ( abono               );
                        am.setCapital        ( p.getMonto()        );
                        am.setInteres        ( p.getIntereses()    );
                        am.setSaldo          ( saldo               );
                        am.setReferencia     ( ids );
                        am.setEstadoDescuento  ( "50" );
                        am.setEstadoPagoTercero( "50" );
                        
                        List lista = new LinkedList();
                        lista.add(am);
                        p.setAmortizacion(lista);
                        
                        
                        // actualizacion de los datos del prestamo
                        consultas.add(model.PrestamoSvc.grabarAmortizacionesII(p, usuario.getLogin(),anti.getFecha_anticipo()));
                        consultas.add(model.PrestamoSvc.updatePrestamoSQL      (p, usuario.getLogin()));
                        
                    } else {
                        break;
                    }
                }
                
                
                // verificacion del saldo del propietario
                if ( saldo_pro > 0 ){
                    AnticiposTerceros ant = (AnticiposTerceros) anticipos.get(0);
                    consultas.add(getSaldoSQL(ant, (float) saldo_pro));
                }
                
                
                // grabacion de los movimientos de la liquidacion.                
                if (!consultas.isEmpty()){
                    model.despachoService.insertar(consultas);
                    request.setAttribute("msg","Liquidacion realizada con exito.");
                    
                    obtenerDatosPagosPrestamos();
                }
                
            } else {
                request.setAttribute("msg","No existen datos para aplicar la liquidacion.");
            }
            
            
        } catch (Exception e){
            e.printStackTrace();
            request.setAttribute("msg", e.getMessage() );
        }
    }
    
    
    
    public double getParameterDouble( String name ) throws Exception {        
        return Double.parseDouble( Util.coalesce( request.getParameter(name) , "0").replaceAll(",",""));
    }
    public String getParameter( String name ) throws Exception {        
        return Util.coalesce( request.getParameter(name) , "");
    }
    
    
     public String getSaldoSQL( AnticiposTerceros ant , float saldo ) throws Exception {
        try {
            ant.setSupplier  ( ""    );
            ant.setPlanilla  ( "SAL ABPRES" );
            ant.setVlrNeto   ( saldo );
            ant.setCurrency  ( "PES" );
            ant.setReanticipo( "N"   );
            return model.AnticiposPagosTercerosSvc.InsertAnticipo( ant , usuario);
            
        } catch (Exception e){
            e.printStackTrace();
            throw new Exception( e.getMessage() );
        }        
    }
     
      
     public void reportePagos() throws Exception {
         //2009-09-02
         try{
             String fecha_inicial = getParameter( "FechaInicial" );
             String fecha_final   = getParameter( "FechaFinal" );
             
             
           //  HReportePagosPrestamos h = new HReportePagosPrestamos();
           //  h.start(model, usuario, fecha_inicial, fecha_final);
             
          
             request.setAttribute("msg","El reporte ha sido iniciado con exito, verifique el Log de Procesos");             
         } catch (Exception ex){ 
             ex.printStackTrace();
             throw new Exception ( ex.getMessage() );
         }        
     }
    
}
