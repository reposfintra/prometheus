package com.tsp.operation.controller;

import com.tsp.exceptions.InformationException;
import com.tsp.finanzas.contab.model.beans.ComprobanteFacturas;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.Banco;
import com.tsp.operation.model.beans.CXPItemDoc;
import com.tsp.operation.model.beans.CXP_Doc;
import com.tsp.operation.model.beans.CajaIngreso;
import com.tsp.operation.model.beans.CajaPago;
import com.tsp.operation.model.beans.Cheque;
import com.tsp.operation.model.beans.Cliente;
import com.tsp.operation.model.beans.FacturasCheques;
import com.tsp.operation.model.beans.Ingreso;
import com.tsp.operation.model.beans.Ingreso_detalle;
import com.tsp.operation.model.beans.TablaGen;
import com.tsp.operation.model.beans.Tasa;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.factura;
import com.tsp.operation.model.beans.factura_detalle;
import com.tsp.operation.model.services.CajasEfectivoService;
import com.tsp.operation.model.services.ChequeXFacturaService;
import com.tsp.util.Util;
import java.sql.SQLException;
import java.util.Map;
import java.util.TreeMap;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.FileItem;
import java.io.*;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Vector;
import org.apache.commons.io.FilenameUtils;

/**
 * Action para el programa de cajas de efectivo
 * 2/02/2012
 * @author darrieta
 */
public class CajasEfectivoAction extends Action {

    HttpSession session;
    Usuario usuario;
    String next = "";
    String nombreArchivo = "";
    CXP_Doc cxpDoc = null;

    @Override
    public void run() throws ServletException, InformationException {
        try {
            session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            String opcion = request.getParameter("opcion") == null ? "" : request.getParameter("opcion");
            boolean redirect = true;
            String strRespuesta = "";
            next = "/jsp/cajasEfectivo/cajasEfectivo.jsp";

            if (opcion.equals("CARGAR_SUCURSALES")) {
                strRespuesta = cargarSucursales();
                redirect = false;
            } else if (opcion.equals("GUARDAR_INGRESO")) {
                guardarIngreso();
            } else if (opcion.equals("CONFIRMAR_INGRESO")) {
                confirmarIngreso();
            } else if (opcion.equals("GUARDAR_PAGO")) {
                guardarPago();
            }

            if (redirect) {
                this.dispatchRequest(next);
            } else {
                response.setContentType("text/plain");
                response.setHeader("Cache-Control", "no-store");
                response.setDateHeader("Expires", 0);
                response.getWriter().print(strRespuesta);
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new ServletException("Error en CajasEfectivoAction: " + e.getMessage());
        }
    }

    /**
     * Carga las sucursales del banco seleccionado
     * @return html con las sucursales a agregar al select
     * @throws SQLException 
     */
    public String cargarSucursales() throws SQLException {
        String banco = request.getParameter("banco");
        model.servicioBanco.loadSusursales(banco);
        TreeMap<String, String> sucursales = model.servicioBanco.getSucursal();
        String options = "";

        for (Map.Entry<String, String> entry : sucursales.entrySet()) {
            options += "<option value='" + entry.getKey() + "'>" + entry.getValue() + "</option>";
        }

        return options;
    }

    /**
     * registra el ingreso de dinero a la caja y realiza el documento contable
     */
    public void guardarIngreso() throws Exception {
        String mensaje = "";
        try {
            TransaccionService tService = new TransaccionService(usuario.getBd());
            CajasEfectivoService cajaService = new CajasEfectivoService(usuario.getBd());
            tService.crearStatement();

            CajaIngreso caja = new CajaIngreso();
            caja.setDstrct(usuario.getDstrct());
            caja.setValor(Double.parseDouble(request.getParameter("valor").replaceAll(",", "")));
            caja.setFecha(request.getParameter("fecha"));
            caja.setAgencia(request.getParameter("agencia"));
            caja.setCreation_user(usuario.getLogin());

            //Variables para el ingreso
            String fecha_actual = Util.getFechaActual_String(4);
            model.tablaGenService.buscarDatos("CE_BANCOS", "BANCO_" + caja.getAgencia());
            TablaGen tbanco = model.tablaGenService.getTblgen();
            model.tablaGenService.buscarDatos("CE_BANCOS", "SUC_" + caja.getAgencia());
            TablaGen tsucursal = model.tablaGenService.getTblgen();
            Banco bancoDetalle = model.servicioBanco.obtenerBanco(usuario.getDstrct(), request.getParameter("banco"), request.getParameter("sucursal"));
            com.tsp.finanzas.contab.model.Model modelcontab = (com.tsp.finanzas.contab.model.Model) session.getAttribute("modelcontab");
            String mon_ingreso = "PES";
            String mon_local = "PES";
            String num_ingreso = "";
            String tipodoc = "ING";
            String nit = "802022016";

            //Generar el ingreso
            Ingreso ingreso = new Ingreso();
            ingreso.setDstrct(usuario.getDstrct());
            ingreso.setReg_status("");
            ingreso.setCodcli("CL01729");//tablagen
            ingreso.setNitcli(nit);//tablagen
            ingreso.setConcepto("FE");//tablagen
            ingreso.setTipo_ingreso("C");
            ingreso.setFecha_consignacion(caja.getFecha());
            ingreso.setFecha_ingreso(fecha_actual);
            ingreso.setBranch_code(tbanco.getReferencia());
            ingreso.setBank_account_no(tsucursal.getReferencia());
            ingreso.setCodmoneda("PES");
            ingreso.setAgencia_ingreso(usuario.getId_agencia());
            ingreso.setPeriodo("000000");
            ingreso.setCant_item(1);
            ingreso.setAbc("");
            ingreso.setBase(usuario.getBase());
            ingreso.setTransaccion(0);
            ingreso.setTransaccion_anulacion(0);
            ingreso.setTipo_documento(tipodoc);
            ingreso.setCreation_user(usuario.getLogin());
            ingreso.setCreation_date(Util.fechaActualTIMESTAMP());
            ingreso.setLast_update(Util.fechaActualTIMESTAMP());
            ingreso.setUser_update(usuario.getLogin());
            ingreso.setDescripcion_ingreso(request.getParameter("descripcion"));
            ingreso.setNro_consignacion("");
            ingreso.setCuenta("");
            ingreso.setTasaDolBol(0);
            ingreso.setCmc("");
            ingreso.setTipos(null);
            ingreso.setAuxiliar("");

            Tasa tasa = model.tasaService.buscarValorTasa(mon_local, mon_ingreso, mon_local, caja.getFecha() != null ? caja.getFecha() : fecha_actual);
            if (tasa != null) {
                ingreso.setVlr_ingreso((!mon_local.equals("DOL")) ? (int) Math.round(tasa.getValor_tasa() * caja.getValor()) : tasa.getValor_tasa() * caja.getValor());
                ingreso.setVlr_ingreso_me(caja.getValor());
                ingreso.setVlr_saldo_ing(caja.getValor());
                ingreso.setVlr_tasa(tasa.getValor_tasa());
                ingreso.setFecha_tasa(tasa.getFecha());

                Banco ban = model.servicioBanco.obtenerBanco(ingreso.getDstrct(), ingreso.getBranch_code(), ingreso.getBank_account_no());
                //valido cuando es ingreso y la moneda del ingreso sea igual a la moneda del banco
                if (ban.getMoneda().equals(ingreso.getCodmoneda())) {
                    num_ingreso = model.ingresoService.buscarSerie(tipodoc + "C");
                    if (!num_ingreso.equals("")) {
                        ingreso.setNum_ingreso(num_ingreso);
                        caja.setNumIngreso(num_ingreso);

                        tService.getSt().addBatch(model.ingresoService.insertarIngreso(ingreso));

                        //detalles del ingreso
                        Ingreso_detalle item = new Ingreso_detalle();
                        item.setCuenta(bancoDetalle.getCodigo_cuenta());
                        item.setValor_abono(caja.getValor());
                        item.setCodigo_retefuente("");
                        item.setCodigo_reteica("");
                        double saldo = 0;
                        item.setDescripcion_factura("Ajuste saldo al ingreso Nro. " + num_ingreso);

                        item.setTipo_aux("");
                        item.setAuxiliar("");

                        item.setValor_retefuente_me(0);
                        item.setValor_reteica_me(0);
                        item.setValor_ingreso_me(caja.getValor());

                        item.setValor_retefuente(0);
                        item.setValor_reteica(0);
                        item.setValor_ingreso(item.getValor_abono());

                        // verificacion de cuentas
                        com.tsp.finanzas.contab.model.beans.PlanDeCuentas plan_cuenta = modelcontab.planDeCuentasService.consultaCuentaModulo(usuario.getDstrct(), item.getCuenta(), 3);
                        if (plan_cuenta != null) {
                            if (plan_cuenta.getSubledger().equalsIgnoreCase("S")) {
                                try {
                                    modelcontab.subledgerService.buscarCuentasTipoSubledger(item.getCuenta());
                                } catch (Exception e) {
                                    e.getMessage();
                                }
                                if (item.getTipo_aux().equals("")) {
                                    mensaje += " requiere de tipo cuenta";
                                    if (item.getAuxiliar().equals("")) {
                                        mensaje += " y requiere de auxiliar";
                                    }
                                } else {
                                    if (item.getAuxiliar().equals("")) {
                                        mensaje += " requiere de auxiliar";
                                    }
                                }
                            } else {
                                modelcontab.subledgerService.setCuentastsubledger(null);
                            }
                        } else {
                            modelcontab.subledgerService.setCuentastsubledger(null);
                            mensaje = " no existe";
                        }
                        if (!mensaje.equals("")) {
                            mensaje = " En el detalle la cuenta" + mensaje + ", ";
                            throw new Exception(mensaje);
                        }
                        item.setDistrito(usuario.getDstrct());
                        item.setNit_cliente(ingreso.getNitcli());
                        item.setTipo_documento(ingreso.getTipo_documento());
                        item.setTipo_doc("");
                        item.setDocumento("");
                        item.setNumero_ingreso(ingreso.getNum_ingreso());
                        item.setItem(1);
                        item.setCreation_user(usuario.getLogin());
                        item.setBase(usuario.getBase());
                        item.setValor_tasa(1);
                        item.setValor_saldo_factura_me(saldo + item.getValor_ingreso_me());
                        item.setFecha_factura(fecha_actual);

                        tService.getSt().addBatch(model.ingreso_detalleService.insertarIngresoDetalle(item));
                        tService.getSt().addBatch(model.ingreso_detalleService.updateSaldoConsignacionIngreso(usuario.getDstrct(), ingreso.getTipo_documento(), ingreso.getNum_ingreso()));

                        tService.getSt().addBatch(cajaService.insertarIngresoCaja(caja));
                        mensaje = "Se ha registrado el ingreso a caja";
                    } else {
                        mensaje = "No hay serie para el ingreso";
                        throw new Exception(mensaje);
                    }
                } else {
                    mensaje = "La moneda " + ingreso.getCodmoneda() + " no pertenece al banco " + ingreso.getBranch_code() + " " + ingreso.getBank_account_no();
                    throw new Exception(mensaje);
                }

            } else {
                mensaje = "No hay tasa";
                throw new Exception(mensaje);
            }
            tService.execute();
        } catch (Exception exception) {
            mensaje = "Ha ocurrido un error. " + exception.getMessage();
        }
        next += "?mensaje=" + mensaje;
    }
    

    public String guardarArchivoEnBd(String documento, String ruta, String agencia) throws Exception {
        String sql = "";
        try {
            List archivos = new LinkedList();
            archivos.add("" + nombreArchivo);
            model.ImagenSvc.setImagenes(archivos);

            Dictionary fields = new Hashtable();
            fields.put("actividad", "PLA");
            fields.put("tipoDocumento", "PLA");
            fields.put("documento", documento);
            fields.put("agencia", agencia);
            fields.put("usuario", usuario.getLogin());

            File file = new File(ruta);


            if (file.exists()) {
                File[] arc = file.listFiles();
                for (int i = 0; i < arc.length; i++) {
                    if (!arc[i].isDirectory()) {
                        String name = arc[i].getName();
                        if (model.ImagenSvc.isLoad(model.ImagenSvc.getImagenes(), name) && model.ImagenSvc.ext(name)) {
                            FileInputStream in = new FileInputStream(file + "/" + name);
                            ByteArrayOutputStream out = new ByteArrayOutputStream();
                            ByteArrayInputStream bfin;
                            int input = in.read();
                            byte[] savedFile = null;
                            while (input != -1) {
                                out.write(input);
                                input = in.read();
                            }
                            out.close();
                            savedFile = out.toByteArray();
                            bfin = new ByteArrayInputStream(savedFile);
                            fields.put("imagen", name);
                            // Insertar imagen
                            sql = this.insertImagen(bfin, savedFile.length, fields);
                        }
                    }
                }
            }

            model.ImagenSvc.reset();

        } catch (Exception e) {
            throw e;
        }
        return sql;
    }

    public String insertImagen(ByteArrayInputStream bfin, int longitud, Dictionary fields) throws Exception {
        String sql = "";
        String fileName = (String) fields.get("imagen");

        try {
            model.ImagenSvc.setEstado(false);
            //sql = model.ImagenSvc.insertImagenBatch(bfin, longitud, fields);
            model.ImagenSvc.insertImagen(bfin, longitud, fields);
            model.ImagenSvc.setEstado(true);
        } catch (Exception e) {
            throw new Exception("No se pudo guardar la imagen " + fileName + "<br>" + e.getMessage());
        }
        return sql;
    }
    
    public void confirmarIngreso() throws Exception{
        String respuesta = "";
        CajasEfectivoService service = new CajasEfectivoService(usuario.getBd());
        CajaIngreso cajaIngreso = new CajaIngreso();
        cajaIngreso.setDstrct(usuario.getDstrct());
        cajaIngreso.setUsuarioRecibido(usuario.getLogin());
        
        try {
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            if (ServletFileUpload.isMultipartContent(request)) {

                ServletFileUpload servletFileUpload = new ServletFileUpload(new DiskFileItemFactory());
                List fileItemsList = servletFileUpload.parseRequest(request);

                //// Itero para obtener todos los FileItem
                Iterator it = fileItemsList.iterator();
                String nombre_archivo;
                while (it.hasNext()) {
                    FileItem fileItem = (FileItem) it.next();

                    //// El FileItem es un campo simple, del tipo nombre-valor
                    if (fileItem.isFormField()) {
                        if (fileItem.getFieldName().equals("ingreso")) {
                            cajaIngreso.setNumIngreso(fileItem.getString());
                        }
                        if (fileItem.getFieldName().equals("agencia")) {
                            cajaIngreso.setAgencia(fileItem.getString());
                        }
                    } else {//// El FileItem contiene un archivo para upload


                        //// Nombre del archivo en el cliente. Algunos navegadores (por ej. IE 6)
                        //// incluyen el path completo, lo que puede implicar separar path
                        //// de nombre.
                        //nombreArchivo = fileItem.getName();
                        String fileName = fileItem.getName();
                        if (fileName != null) {
                            nombreArchivo = FilenameUtils.getName(fileName);
                        }


                        //// Obtengo extensión del archivo de cliente
                        String extension = nombreArchivo.substring(nombreArchivo.indexOf("."));

                        //// Guardo archivo del cliente en servidor

                        //Se obtiene la ruta temporal
                        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
                        String directorioArchivos = rb.getString("ruta") + "/documentos/imagenes/viewPrevias/" + usuario.getLogin();
                        File ruta = new File(directorioArchivos);
                        if (!ruta.exists()) {
                            ruta.mkdirs();
                        }
                        nombre_archivo = directorioArchivos+ "/" + nombreArchivo;
                        File archivo = new File(nombre_archivo);

                        fileItem.write(archivo);
                        if (archivo.exists()) {
                            respuesta = "Se han guardado los cambios";
                            tService.getSt().addBatch(guardarArchivoEnBd(cajaIngreso.getNumIngreso(), directorioArchivos, cajaIngreso.getAgencia()));
                            tService.getSt().addBatch(service.confirmarIngresoCaja(cajaIngreso));
                            tService.getSt().addBatch(service.actualizarSaldoCaja(usuario.getDstrct(), cajaIngreso.getAgencia(), cajaIngreso.getNumIngreso(), usuario.getLogin()));
                        } else {
                            respuesta = "Imagen Rechazada";
                        }

                    }

                }
            }
            tService.execute();
        } catch (Exception e) {
            System.out.println("e" + e.toString() + "__" + e.getMessage());
            respuesta = "Problema al cargar";
        }
        next = "/jsp/cajasEfectivo/confirmarIngresoCaja.jsp?mensaje="+respuesta;
    }
    
    public void guardarPago(){
        String respuesta = "";
        try {
            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            CajasEfectivoService service = new CajasEfectivoService(usuario.getBd());            
            String bancoDetalle=request.getParameter("bancoDetalle");
            String sucursalDetalle=request.getParameter("sucursalDetalle");
            String tipoTrans=request.getParameter("tipoDocumento");
            CajaPago pago = new CajaPago();
            pago.setDstrct(usuario.getDstrct());
            pago.setAgencia(request.getParameter("agencia"));
            pago.setFecha(request.getParameter("fecha"));
            pago.setTransportadora(request.getParameter("codcli"));
            pago.setTipoDocumento(request.getParameter("tipoDocumento"));
            pago.setNumDocumento(request.getParameter("documento"));
            pago.setBeneficiario(request.getParameter("provee"));
            pago.setBanco(request.getParameter("banco"));
            pago.setValor(Double.parseDouble(request.getParameter("valor").replaceAll(",", "")));
            pago.setComision(Double.parseDouble(request.getParameter("valor").replaceAll(",", "")));
            pago.setComision(Double.parseDouble(request.getParameter("comision").replaceAll(",", "")));
            pago.setValorComision(Double.parseDouble(request.getParameter("valorComision").replaceAll(",", "")));
            pago.setValorEntregar(Double.parseDouble(request.getParameter("valorEntregar").replaceAll(",", "")));
            pago.setCreationUser(usuario.getLogin());
            pago.setComisionBancaria(Double.parseDouble(request.getParameter("comisionBancaria").replaceAll(",", "")));
            pago.setTipo(request.getParameter("tipo"));
            
            String numFac = service.consultarSerieCXC("FAC_CAJA");
            pago.setNumCxc(numFac);
            tService.getSt().addBatch(service.insertarCajaPago(pago));
            
            //Se consulta la información de la transportadora
            model.clienteService.BuscarCliente(pago.getTransportadora());
            Cliente clTransportadora = model.clienteService.getClienteUnidad();
            
            //SE CREA LA ORDEN DE CHEQUE
            ArrayList<String> lisQuery = crearComprobante(pago, clTransportadora.getNit());
            for (String sql : lisQuery) {
                    tService.getSt().addBatch(sql);
                }
            //asignamos los bancos dinamicos para todos los documentos 
            if(tipoTrans.equals("T")){
                pago.setBanco(bancoDetalle);
                pago.setSucursal(sucursalDetalle);
            }
            
            //GENERAR LA CXP AL CLIENTE
            ArrayList<String> listQuery = crearCXP(pago);
            for (String sql : listQuery) {
                 tService.getSt().addBatch(sql);
             }
            //GENERAR EL EGRESO PARA CANCELAR LA CXP
            ArrayList<String> listQuery2 = crearEgreso(pago);
            for (String sql : listQuery2) {
                 tService.getSt().addBatch(sql);
             }

            
            //SE GENERA LA CXC
            model.tablaGenService.buscarDatos("CE_CUENTA", "HC");
            TablaGen tHC = model.tablaGenService.getTblgen();
            model.tablaGenService.buscarDatos("CE_CUENTA", "CXC");
            TablaGen tCuenta = model.tablaGenService.getTblgen();
            
            
            
            factura cxc = new factura();
            cxc.setTipo_documento("FAC");
            cxc.setDocumento(numFac);
            cxc.setForma_pago("CREDITO");
            cxc.setAgencia_facturacion(pago.getAgencia());
            cxc.setPlazo("30");
            cxc.setValor_factura(pago.getValor());
            cxc.setValor_facturame(pago.getValor());
            cxc.setFecha_factura(pago.getFecha());
            cxc.setDescripcion("COBRO DE CHEQUE No "+pago.getNumDocumento());
            cxc.setObservacion("");
            cxc.setZona("");
            cxc.setMoneda("PES");
            cxc.setVlr_tasa_remesas(1);
            cxc.setCmc(tHC.getReferencia());
            cxc.setNomcli(clTransportadora.getNomcli());
            cxc.setCodcli(pago.getTransportadora());
            cxc.setNit(clTransportadora.getNit());
            cxc.setCantidad_items(1);
            cxc.setValor_tasa(1d);
            cxc.setAgencia_cobro("");
            cxc.setRif("");
            
            tService.getSt().addBatch(service.ingresarFactura(cxc, usuario.getDstrct(), usuario.getLogin(), "COL"));
            
            factura_detalle cxcDetalle = new factura_detalle();
            
            cxcDetalle.setNumero_remesa("");
            cxcDetalle.setFecrem("");
            cxcDetalle.setDescripcion(cxc.getDescripcion());
            cxcDetalle.setTiposubledger("");
            cxcDetalle.setTiposub(null);
            cxcDetalle.setAuxliliar("");
            cxcDetalle.setAux("");
            cxcDetalle.setCantidad(1d);
            cxcDetalle.setUnidad("");
            cxcDetalle.setValor_unitario(pago.getValor());
            cxcDetalle.setValor_unitariome(pago.getValor());
            cxcDetalle.setCodigo_cuenta_contable(tCuenta.getReferencia());
            cxcDetalle.setValor_tasa(1d);
            cxcDetalle.setValor_item(pago.getValor());
            cxcDetalle.setValor_itemme(pago.getValor());
            cxcDetalle.setMoneda("PES");
            cxcDetalle.setItem(1);
            cxcDetalle.setDocumento(numFac);  
            cxcDetalle.setNit(clTransportadora.getNit());
            
            tService.getSt().addBatch(service.ingresarDetalleFactura(cxcDetalle, usuario.getDstrct(), usuario.getLogin(), "COL"));
            
            tService.execute();
            respuesta = "Se han guardado los cambios";
        } catch (Exception e) {
            e.printStackTrace();
            respuesta = "Ha ocurrido un error. "+e.getMessage();
        }
        next = "/jsp/cajasEfectivo/guardarPagoCaja.jsp?mensaje="+respuesta;
    }
    
    /**
     * Genera el sql para un comprobante
     * @param pago
     * @throws SQLException
     * @throws Exception 
     */
    public ArrayList<String> crearComprobante(CajaPago pago, String nitTransportadora) throws SQLException, Exception{
        ArrayList<String> listSql = new ArrayList<String>();
        try {
            //Se instancia el model de contabilidad
            com.tsp.finanzas.contab.model.Model modelContab = new com.tsp.finanzas.contab.model.Model(usuario.getBd());
            
            String tipodoc = "OCH";            
            
            String[] monedaBase = modelContab.GrabarComprobanteSVC.getMoneda(usuario.getCia()).split("-");
            Hashtable DatosTipoDoc = modelContab.GrabarComprobanteSVC.buscarDatosTipoDoc(usuario.getDstrct(), tipodoc);
            java.sql.Timestamp ffinal = Util.ConvertiraTimestamp(pago.getFecha() + " 00:00:00");
            
            String periodo = Util.formatoTimestamp(ffinal, 4);
            String numdoc = pago.getNumDocumento();
            String concepto = "Contabilizacion Caja Efectivo " + pago.getNumDocumento();
            double valor = pago.getValor();
            
            ComprobanteFacturas comprobante = new ComprobanteFacturas();
            
            comprobante.setDstrct(usuario.getDstrct());
            comprobante.setTipodoc(tipodoc);
            comprobante.setNumdoc(numdoc);
            comprobante.setSucursal(usuario.getId_agencia());
            comprobante.setPeriodo(periodo);
            comprobante.setFechadoc(pago.getFecha());
            comprobante.setDetalle(concepto);
            comprobante.setTercero("");
            comprobante.setTipo("C");
            comprobante.setTotal_debito(valor);
            comprobante.setTotal_credito(valor);
            comprobante.setMoneda(monedaBase[0]);
            comprobante.setAprobador(usuario.getLogin());
            comprobante.setBase(monedaBase[1]);
            comprobante.setDocumento_interno((String) DatosTipoDoc.get("codigo_interno"));
            comprobante.setOrigen("COMPROBANTE");
            comprobante.setTipo_operacion("009");
            
            ArrayList items = new ArrayList();

            //Generar los items del comprobante
            model.tablaGenService.buscarDatos("CE_CUENTA", "OCH_VALOR");
            TablaGen tCuenta = model.tablaGenService.getTblgen();
            ComprobanteFacturas comprodet = this.crearComprodet(nitTransportadora, comprobante, tCuenta.getReferencia(), valor, 0, comprobante.getDetalle());
            items.add(comprodet);
            if (pago.getTipoDocumento().equals("T") && pago.getTipo().equals("A")) {
                // si es trasnferencia y anticipo se pone la cuenta de trasnferencia
                model.tablaGenService.buscarDatos("CE_CUENTA", "OCH_COMISION_T");
            } else if (pago.getTipo().equals("A")) {
                //si es anticipo
                model.tablaGenService.buscarDatos("CE_CUENTA", "OCH_COMISION_" + pago.getAgencia());
            } else if (pago.getTipo().equals("P")) {
                //si es pronto pago
                model.tablaGenService.buscarDatos("CE_CUENTA", "OCH_COMISION_PP");
            }

            tCuenta = model.tablaGenService.getTblgen();
            comprodet = this.crearComprodet(pago.getBeneficiario(), comprobante, tCuenta.getReferencia(), 0, pago.getValorComision(), comprobante.getDetalle());
            items.add(comprodet);
            model.tablaGenService.buscarDatos("CE_CUENTA", "OCH_ENTREGADO");
            tCuenta = model.tablaGenService.getTblgen();
            comprodet = this.crearComprodet(pago.getBeneficiario(), comprobante, tCuenta.getReferencia(), 0, pago.getValorEntregar(), comprobante.getDetalle());
            items.add(comprodet);
            
            //aqui agregamos el item de la de la comision..
            int num_item = 3;
            if (pago.getTipoDocumento().equals("T")) {
                
                if(pago.getTipo().equals("P")){
                 model.tablaGenService.buscarDatos("CE_CUENTA", "OCH_COMISION_TP");
                 comprobante.setDetalle("Comision transferencia pronto pago");
                }
                if(pago.getTipo().equals("A")){
                  comprobante.setDetalle("Comision transferencia anticipo");
                  model.tablaGenService.buscarDatos("CE_CUENTA", "OCH_COMISION_TA");
                
                }
                
                //Agregamos el item numero 4
                tCuenta = model.tablaGenService.getTblgen();
                comprodet = this.crearComprodet(pago.getBeneficiario(), comprobante, tCuenta.getReferencia(), 0, pago.getComisionBancaria(), comprobante.getDetalle());
                items.add(comprodet);
                num_item = 4;
            
            }
            
            
            
            
            comprobante.setItems(items);
            comprobante.setTotal_items(num_item);
            modelContab.GrabarComprobanteSVC.setComprobante(comprobante);

            ArrayList listaContable = new ArrayList();
            listaContable.add(comprobante);
            if (modelContab.GrabarComprobanteSVC.existeDocumento(usuario.getDstrct(), tipodoc, numdoc)) {
                throw new Exception("Este numero de documento generado ya existe dentro de la base de datos");
            } else {
                listSql = modelContab.ContabilizacionFacSvc.generarSQLComprobante(comprobante, usuario.getLogin());
                String msgError = modelContab.ContabilizacionFacSvc.getESTADO();
                
                if (!msgError.equals("")) {
                    throw new Exception(msgError);
                } else {
                    modelContab.GrabarComprobanteSVC.UpdateSerie(usuario.getDstrct(), tipodoc);
                }
            }
        } catch (Exception exception) {
            throw exception;
        }
        return listSql;
    }
    
    public ComprobanteFacturas crearComprodet(String nit, ComprobanteFacturas comprobante, String codContable, double debito, double credito, String detalle) throws SQLException, Exception{
        //Se instancia el model de contabilidad
        com.tsp.finanzas.contab.model.Model modelContab = new com.tsp.finanzas.contab.model.Model(usuario.getBd());
        
        modelContab.GrabarComprobanteSVC.buscarCuenta(usuario.getDstrct(), codContable);
        boolean Existe = modelContab.GrabarComprobanteSVC.isExisteCuenta();
        ComprobanteFacturas comprodet = new ComprobanteFacturas();
        comprodet.setDstrct(usuario.getDstrct());
        comprodet.setTipodoc(comprobante.getTipodoc());
        comprodet.setNumdoc(comprobante.getNumdoc());
        comprodet.setPeriodo(comprobante.getPeriodo());
        comprodet.setCuenta(codContable);
        comprodet.setAuxiliar("");
        comprodet.setDetalle(detalle);
        comprodet.setAbc("");
        comprodet.setTdoc_rel("OCH");
        comprodet.setNumdoc_rel(comprobante.getNumdoc());
        comprodet.setTotal_debito(debito);
        comprodet.setTotal_credito(credito);
        comprodet.setTercero(nit);
        comprodet.setBase(usuario.getBase());
        comprodet.setDocumento_interno(comprobante.getDocumento_interno());
        comprodet.setExisteCuenta(Existe);
        comprodet.setTipo("D");
        comprodet.setOrigen("COMPROBANTE");
        Vector tipSubledger = null;
        if (!Existe) {
            throw new Exception("Los numeros de cuentas parametrizados para el comprobante no existen en la base de datos o no pueden ser usadas dentro de este modulo");
        } else {
            Hashtable cuenta = modelContab.GrabarComprobanteSVC.getCuenta();
            if (cuenta.get("modulo1").toString().equalsIgnoreCase("N")) {
                comprodet.setExisteCuenta(false);
                throw new Exception("Los numeros de cuentas parametrizados para el comprobante no existen en la base de datos o no pueden ser usadas dentro de este modulo");
            }
            modelContab.subledgerService.buscarCodigosCuenta(codContable);
            tipSubledger = modelContab.subledgerService.getCodigos();
        }
        comprodet.setCodSubledger(tipSubledger);
        
        return comprodet;
    }
    
    public ArrayList<String> crearCXP(CajaPago pago) throws Exception{
        //GENERAR LA CXP

        CXP_Doc factura = new CXP_Doc();
        String monedaUsuario = (String)session.getAttribute("Moneda");
        double valor_tasa = 0;
        TablaGen tBanco;
        TablaGen tSucursal;

        //Se consulta la informacion de los bancos
        if(!pago.getTipoDocumento().equals("T")){
        
            model.tablaGenService.buscarDatos("CE_CUENTA", "EGR_BANCO_" + pago.getAgencia());
            tBanco = model.tablaGenService.getTblgen();
            model.tablaGenService.buscarDatos("CE_CUENTA", "EGR_SUCURSAL_" + pago.getAgencia());
            tSucursal = model.tablaGenService.getTblgen();

        }else{
            
            //llenamos el banco en un tabla gen...
            tBanco=new TablaGen();
            tBanco.setTable_code("NEWBANK");
            tBanco.setDescripcion("Este banco ahora es dinamico");
            tBanco.setReferencia(pago.getBanco());
            
            
            //llenamos la sucursal en un tabla gen...
            
            tSucursal=new TablaGen();
            tSucursal.setTable_code("NEWCURS");
            tSucursal.setDescripcion("Esta sucursal ahora es dinamica");
            tSucursal.setReferencia(pago.getSucursal());
        
        
        }
        
        
        //Se consulta el HC
        model.tablaGenService.buscarDatos("CE_CUENTA", "HC");
        TablaGen tHC = model.tablaGenService.getTblgen();
        
        //Se consulta la cuenta del detalle
        model.tablaGenService.buscarDatos("CE_CUENTA", "CXP");
        TablaGen tDetCxp = model.tablaGenService.getTblgen();        
        
        Banco bancoEgreso = model.servicioBanco.obtenerBanco(usuario.getDstrct(), tBanco.getReferencia(), tSucursal.getReferencia());

        //Se consulta la tasa
        Tasa tasa = model.tasaService.buscarValorTasa(monedaUsuario, monedaUsuario, monedaUsuario, pago.getFecha());
        if (tasa != null) {
            valor_tasa = tasa.getValor_tasa();
        }

        factura.setDstrct(usuario.getDstrct());
        factura.setProveedor(pago.getBeneficiario());
        factura.setTipo_documento("FAP");
        factura.setDocumento(pago.getNumDocumento());
        factura.setDescripcion("PAGO EN CAJA EFECTIVO "+pago.getNumDocumento());
        factura.setAgencia(bancoEgreso.getCodigo_Agencia());
        factura.setId_mims("");
        factura.setFecha_documento(pago.getFecha());
        factura.setTipo_documento_rel("OCH");
        factura.setDocumento_relacionado(pago.getNumDocumento());
        factura.setFecha_aprobacion(Util.getFechaActual_String(4));
        factura.setUsuario_aprobacion("JGOMEZ");
        factura.setClase_documento_rel("4");
        factura.setAprobador("JGOMEZ");
        factura.setFecha_vencimiento(Util.getFechaActual_String(4));
        factura.setUltima_fecha_pago("0099-01-01 00:00:00");
        factura.setBanco(tBanco.getReferencia());
        factura.setSucursal(tSucursal.getReferencia());
        factura.setMoneda(monedaUsuario);
        factura.setMoneda_banco(bancoEgreso.getMoneda());
        factura.setHandle_code(tHC.getReferencia());
        factura.setTasa(valor_tasa);
        factura.setUsuario_contabilizo("");
        factura.setFecha_contabilizacion("0099-01-01 00:00:00");
        factura.setUsuario_anulo("");
        factura.setFecha_anulacion("0099-01-01 00:00:00");
        factura.setFecha_contabilizacion_anulacion("0099-01-01 00:00:00");
        factura.setObservacion("");
        factura.setNum_obs_autorizador(0);
        factura.setNum_obs_pagador(0);
        factura.setNum_obs_registra(0);
        factura.setCreation_user(usuario.getLogin());
        factura.setUser_update(usuario.getLogin());
        factura.setBase(usuario.getBase());
        factura.setVlr_neto(pago.getValorEntregar());
        factura.setVlr_total_abonos(0);
        factura.setVlr_saldo(pago.getValorEntregar());
        factura.setVlr_neto_me(pago.getValorEntregar());
        factura.setVlr_total_abonos_me(0);
        factura.setVlr_saldo_me(pago.getValorEntregar());

        Vector vItems = new Vector();
        int numItem = 1;
        CXPItemDoc item = crearDetalleCXP(pago.getBeneficiario(), "FAP", pago.getNumDocumento(), factura.getDescripcion(), pago.getValorEntregar(), tDetCxp.getReferencia(), numItem++);
        vItems.add(item);
        
        ArrayList<String> listQuery = model.cxpDocService.insertarCXPDoc_SQL(factura, vItems, new Vector(), bancoEgreso.getCodigo_Agencia(),"");
       
        cxpDoc = factura;
        return listQuery;
    }
    
    private CXPItemDoc crearDetalleCXP(String proveedor, String tipoDocumento, String documento, String descripcion, double valor, String cuenta, int numItem){
        CXPItemDoc item = new CXPItemDoc();
        item.setDstrct(usuario.getDstrct());
        item.setProveedor(proveedor);
        item.setTipo_documento(tipoDocumento);
        item.setDocumento(documento);
        item.setItem("00"+numItem);
        item.setDescripcion(descripcion);
        item.setConcepto("");
        item.setVlr(valor);
        item.setVlr_me(valor);
        item.setCodigo_cuenta(cuenta);
        item.setCodigo_abc("");
        item.setPlanilla("");
        item.setCodcliarea("");
        item.setTipcliarea("");
        item.setCreation_user(usuario.getLogin());
        item.setUser_update(usuario.getLogin());
        item.setBase(usuario.getBase());
        item.setAuxiliar("");
        item.setTipoSubledger("");
        Vector vImpuestosPorItem = new Vector();
        item.setVItems(vImpuestosPorItem);
        return item;
    }
    
    private FacturasCheques convertirCxpAFacturaCheque(CXP_Doc cxp){
        FacturasCheques    factura = new FacturasCheques();
              
        factura.setDstrct         (usuario.getDstrct() );
        factura.setProveedor      (cxp.getProveedor() );
        factura.setTipo_documento (cxp.getTipo_documento());
        factura.setDocumento      (cxp.getDocumento() );
        factura.setDescripcion    (cxp.getDescripcion() );
        factura.setAgencia        (cxp.getAgencia());
        factura.setFecha_documento(cxp.getFecha_documento() );
        factura.setBanco          (cxp.getBanco());
        factura.setSucursal       (cxp.getSucursal());
        factura.setMoneda         (cxp.getMoneda() );
        factura.setVlr_neto       (cxp.getVlr_neto());
        factura.setVlr_neto_me    (cxp.getVlr_neto_me());
        factura.setVlr_saldo      (cxp.getVlr_saldo());
        factura.setVlr_saldo_me   (cxp.getVlr_saldo_me() );
        factura.setBase           (cxp.getBase());
        factura.setMonedaBanco    (cxp.getMoneda_banco()); 
        factura.setOc             (""); 
        return factura;
    }
    
    private ArrayList<String> crearEgreso(CajaPago pago) throws Exception{

        /*
         * Se genera el precheque y el egreso
         * Tomado de ChequeXFacturaAction.java
         */
        ArrayList<String> sql = new ArrayList<String>();
        ChequeXFacturaService ChequeXFacturaSvc = new ChequeXFacturaService(usuario.getBd());
        ChequeXFacturaSvc.setProveedor( pago.getBeneficiario() );
        ChequeXFacturaSvc.setTipoFactura("FAP");
        ChequeXFacturaSvc.setUsuario(usuario);
        //ChequeXFacturaSvc.searchFacturasProveedor();
        LinkedList facturasProveedor = new LinkedList();
        facturasProveedor.add(convertirCxpAFacturaCheque(cxpDoc));
        ChequeXFacturaSvc.setListFacturasPro(facturasProveedor);
        List lista = ChequeXFacturaSvc.getListFacturasPro();                    
        if(lista.isEmpty())
            throw new Exception("El proveedor no tiene facturas disponibles");
        String[] vecFacturas = new String[1];
        vecFacturas[0] = pago.getNumDocumento();
        ChequeXFacturaSvc.loadFacturasCheque(vecFacturas);
        if(!ChequeXFacturaSvc.getFacturasCheque().isEmpty()){
         
            TablaGen tBanco;
            TablaGen tSucursal;

            //Se consulta la informacion de los bancos
            if (!pago.getTipoDocumento().equals("T")) {

                model.tablaGenService.buscarDatos("CE_CUENTA", "EGR_BANCO_" + pago.getAgencia());
                tBanco = model.tablaGenService.getTblgen();
                model.tablaGenService.buscarDatos("CE_CUENTA", "EGR_SUCURSAL_" + pago.getAgencia());
                tSucursal = model.tablaGenService.getTblgen();

            } else {

                //llenamos el banco en un tabla gen...
                tBanco = new TablaGen();
                tBanco.setTable_code("NEWBANK");
                tBanco.setDescripcion("Este banco ahora es dinamico");
                tBanco.setReferencia(pago.getBanco());


                //llenamos la sucursal en un tabla gen...

                tSucursal = new TablaGen();
                tSucursal.setTable_code("NEWCURS");
                tSucursal.setDescripcion("Esta sucursal ahora es dinamica");
                tSucursal.setReferencia(pago.getSucursal());


            }

            FacturasCheques facCheque = (FacturasCheques)ChequeXFacturaSvc.getFacturasCheque().get(0);
            facCheque.setBanco(tBanco.getReferencia());
            facCheque.setSucursal(tSucursal.getReferencia());
            String msg = "";
            msg = ChequeXFacturaSvc.formarPrecheque();
            if(msg.isEmpty()){
                ChequeXFacturaSvc.loadNextCheque();
                ChequeXFacturaSvc.getCheque().setBanco(tBanco.getReferencia());
                ChequeXFacturaSvc.getCheque().setSucursal(tSucursal.getReferencia());
                //Agencias para impresion
                //model.agenciaService.listarOrdenadas();


                /*
                 * Se genera el precheque
                 */
                double vlrPagar  = pago.getValorEntregar();

                // Redondemos valor a pagar de acuerdo a la moneda:
                String  monedaBanco = ChequeXFacturaSvc.getCheque().getMoneda();
                vlrPagar = (monedaBanco.equals("PES") || monedaBanco.equals("BOL")) ? (double)Math.round(vlrPagar) : Util.roundByDecimal(vlrPagar, 2);

                ChequeXFacturaSvc.getCheque().setVlrPagar( vlrPagar );
                Banco b = model.servicioBanco.obtenerBanco( ChequeXFacturaSvc.getCheque().getDistrito(), ChequeXFacturaSvc.getCheque().getBanco(), ChequeXFacturaSvc.getCheque().getSucursal() ); 
                ChequeXFacturaSvc.setAgencia_impresion(b.getCodigo_Agencia());
                sql.addAll(ChequeXFacturaSvc.guardarPreCheque(vlrPagar));
                Vector items = new Vector();
                items.add(ChequeXFacturaSvc.getPrecheque());
                ChequeXFacturaSvc.getPrecheque().setItems(items);
                ChequeXFacturaSvc.getPrecheque().setFacturas(pago.getNumDocumento()+",");

                boolean hayTasa = model.tasaService.isTasaHoy( usuario.getDstrct() );            
                if( hayTasa ){
                    Vector precheques = new Vector();
                    precheques.add(ChequeXFacturaSvc.getPrecheque());
                    ChequeXFacturaSvc.setPrecheques(precheques);
                    Vector seleccionados = new Vector();
                    seleccionados.add(ChequeXFacturaSvc.getPrecheque().getId());
                    msg = ChequeXFacturaSvc.prechequeToCheque(ChequeXFacturaSvc.getPrecheque(), 0 );
                    ChequeXFacturaSvc.setListFacturasPro(lista);
                    if(msg.isEmpty()){
                        /*
                         * Se genera el egreso
                         */
                        ChequeXFacturaSvc.loadNextCheque();
                        Cheque cheque = ChequeXFacturaSvc.getCheque();
                        cheque.setVlrPagar(vlrPagar);

                        sql.addAll(ChequeXFacturaSvc.updateChequePreCheque(vlrPagar,0));
                        ChequeXFacturaSvc.removePrecheque( cheque.getId_precheque() );
                    }else{
                        throw new Exception(msg);
                    }
                }else{
                    throw new Exception("No hay tasa");
                }
            }else{
                throw new Exception(msg);
            }
        }else{
            throw new Exception("No se encontro la factura o la factura no ha sido aprobada");
        }
        return sql;
    }
}
