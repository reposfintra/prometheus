/*******************************************************************
 * Nombre clase: ClienteBuscarAction.java
 * Descripci�n: Accion para buscar una placa de la bd.
 * Autor: Ing. fily fernandez
 * Fecha: 16 de febrero de 2006, 05:41 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import com.tsp.exceptions.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import java.util.Vector;
import java.lang.*;
import java.sql.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import com.tsp.operation.model.services.*;
/**
 *
 * @author  jdelarosa
 */
public class ReporteEquipoAction extends Action{
    
    /** Creates a new instance of PlacaBuscarAction */
    public ReporteEquipoAction () {
    }
    
    public void run () throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "/jsp/trafico/reportes/ReporteEquipos.jsp?";
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario)session.getAttribute ("Usuario");
        String usu = usuario.getLogin();
        String opcion = request.getParameter ("opcion")!=null?request.getParameter ("opcion"):"";
        try {
              HReporteEquiposTrailers h = new HReporteEquiposTrailers();
              h.start(usu);
              String mensaje = h.getMensaje(); 
              System.out.println("mensaje  "+mensaje);
               next = next + "msg=se ha iniciado el proceso!"; 
               
            
             
            
        } catch ( Exception e ) {
            
            throw new ServletException ( "Error en Reporte de ReporteEquipoAction : " + e.getMessage() );
            
        }        
        
        this.dispatchRequest ( next );
        
    }
    
}
