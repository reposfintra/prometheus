
package com.tsp.operation.controller;

import com.tsp.operation.model.*;
import java.io.*;
import java.util.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;

public class MenuMigracionConProAction extends Action{

     
     public void run() throws javax.servlet.ServletException, InformationException {
      try{
          // LLamamos la pagina JSP
            final String next = "/migracion/MigracionConductorPropietario.jsp";
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
              throw new Exception("No se pudo encontrar "+ next);
            rd.forward(request, response); 
        }
        catch(Exception e){ throw new InformationException(e.getMessage());} 
    }  
   
    
}
