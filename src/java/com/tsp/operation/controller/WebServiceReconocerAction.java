package com.tsp.operation.controller;

import com.tsp.exceptions.InformationException;
import javax.servlet.ServletException;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.services.WSLocalizacionService;
import javax.servlet.http.HttpSession;


/**
 *
 * @author adminfintra
 */
public class WebServiceReconocerAction extends Action {

    WSLocalizacionService wsService = new WSLocalizacionService();    
    HttpSession session;
    Usuario usuario;

    @Override
    public void run() throws ServletException, InformationException {
        try {
            session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            String tipoIdentificacion = request.getParameter("tipoIdentificacion");
            String identificacion = request.getParameter("identificacion");
            String primerApellido = request.getParameter("primerApellido");

            String mensaje = wsService.consultarLocalizacion(tipoIdentificacion, identificacion, primerApellido, usuario);
            
            String next = "/jsp/webServices/localizacion.jsp?mensaje="+mensaje;
            this.dispatchRequest(next);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
