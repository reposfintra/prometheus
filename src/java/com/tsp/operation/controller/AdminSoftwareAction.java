    /***************************************
    * Nombre Clase ............. AdminSoftwareAction.java
    * Descripci�n  .. . . . . .  Maneja los eventos para la adminitracion de software
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  10/01/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...Transportes Sanchez Polo S.A.
    *******************************************/


package com.tsp.operation.controller;



import java.io.*;
import java.util.*;
import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import javax.servlet.*;
import javax.servlet.http.*;



public class AdminSoftwareAction extends Action{
    
   
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
                
        try{
            HttpSession session = request.getSession();
            Usuario     usuario = (Usuario)session.getAttribute("Usuario");
            String      user    = usuario.getLogin();
      
            
            String next          = "/jsp/general/inventario/AdminSoftware.jsp?msj=";
            String msj           = "";
            String opcion        = request.getParameter("evento");
            
            
            if(opcion!=null){
                opcion = opcion.toUpperCase();
                model.AdminSoft.loadRequest(request);
                if(opcion.equals("SAVE")){
                    String[] files = request.getParameterValues("programFile");
                    model.AdminSoft.setFilesProgram(files);
                    msj = model.AdminSoft.save(user);    
                    next += msj;
                }
                if(opcion.equals("LOAD")){
                    model.AdminSoft.searchFile();
                }
                if(opcion.equals("EXTENSION")){
                    String[] files = request.getParameterValues("programFile");
                    if(files!=null && files.length>0){
                       model.AdminSoft.setFilesProgram(files);
                       model.AdminSoft.selectFile();
                    }
                    String ext = request.getParameter("ext");
                    model.AdminSoft.fileExt(ext);
                }
                if(opcion.equals("RESET")){
                    model.AdminSoft.reset();
                }                
                
            }else
                model.AdminSoft.reset();           
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new Exception("No se pudo encontrar "+ next);
            rd.forward(request, response);            
        } catch (Exception e){
             throw new ServletException(e.getMessage());
        }
         
    }
    
}
