/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.exceptions.InformationException;
import com.tsp.operation.model.DAOS.impl.EstadoCuentaImpl;
import com.tsp.operation.model.beans.Usuario;
import javax.servlet.ServletException;
import com.tsp.operation.model.DAOS.EstadoCuentaDAO;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 *
 * @author HaroldC
 */
public class EstadoCuentaAction extends Action {

    private final int CONSULTAR_ESTADO_CUENTA = 1;
    private final int TERCERO_PLACA = 2;

    private JsonObject respta = null;
    private JsonObject respuesta = null;
    private Usuario usuario;
    private String Negocio;
    private String Placa;
    private EstadoCuentaDAO fcDAO;

    @Override
    public void run() throws ServletException, InformationException {
        try {
            
            usuario = (Usuario) request.getSession().getAttribute("Usuario");
            Negocio = request.getParameter("negocio");
            Placa = request.getParameter("placa");
            fcDAO = new EstadoCuentaImpl(usuario.getBd());

            int opcion = (request.getParameter("opcion") != null) ? Integer.parseInt(request.getParameter("opcion")) : -1;
            switch (opcion) {
                
                case CONSULTAR_ESTADO_CUENTA:

                    respuesta = new JsonObject();
                    JsonObject joSForm = request.getParameter("form") != null ? (JsonObject) (new JsonParser()).parse(request.getParameter("form")) : null;

                    respuesta.add("hdc", fcDAO.EstadoCuentaApoteosys(joSForm, Negocio));
                    break;

                case TERCERO_PLACA:

                    respuesta = new JsonObject();
                    JsonObject joS = request.getParameter("form") != null ? (JsonObject) (new JsonParser()).parse(request.getParameter("form")) : null;

                    respuesta.add("placa", fcDAO.TerceroPlaca(joS, Placa));
                    break;


                default:
                    throw new Exception("La opcion de orden " + opcion + " no valida");
            }
            
        } catch (Exception exc) {
            respuesta = new JsonObject();
            respuesta.addProperty("error", exc.getMessage());
            exc.printStackTrace();
        } finally {
            try {
                response.setContentType("application/json; charset=utf-8");
                response.setHeader("Cache-Control", "no-cache");
                response.getWriter().println((new Gson()).toJson(respuesta));
            } catch (Exception e) {
            }
        }
    }
    
}
