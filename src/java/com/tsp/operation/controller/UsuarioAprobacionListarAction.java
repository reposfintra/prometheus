/*******************************************************************
 * Nombre:        UsuarioAprobacionListar.java
 * Descripci�n:   Clase Action para listar usuario aprobacion
 * Autor:         Ing. Diogenes Antonio Bastidas Morales
 * Fecha:         14 de enero de 2006, 11:44 AM
 * Versi�n        1.0
 * Coyright:      Transportes Sanchez Polo S.A.
 *************************************************************************/


package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class UsuarioAprobacionListarAction extends Action {
    
    /** Creates a new instance of UsuarioAprobacionListar */
    public UsuarioAprobacionListarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = (String) session.getAttribute("Distrito");
        String next = "/jsp/hvida/aprobacion/";
        String tabla = request.getParameter("tabla");
        try{
            if (tabla.equals("NIT")){
                model.identidadService.listarIdentidadNOAprobada(usuario.getLogin());
                next=next+"aprobarIdentidades.jsp";
            }
            else if (tabla.equals("PLACA")){
                model.placaService.listarPlacaNoAprobada(usuario.getLogin());
                next=next+"aprobarPlacas.jsp";
                
            }
        }
        catch(Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
    
}
