/***************************************
 * Nombre Clase ............. ChequeCxPAnularAction.java
 * Descripci�n  .. . . . . .  Anula el Cheque para una factura
 * Autor  . . . . . . . . . . Ing. ANDRES MATURANA DE LA CRUZ
 * Fecha . . . . . . . . . .  Created on 20 de enero de 2007, 01:26 PM
 * versi�n . . . . . . . . .  1.0
 * Copyright ...............  Transportes Sanchez Polo S.A.
 *******************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import java.util.regex.*;
import org.apache.log4j.Logger;
import com.tsp.finanzas.contab.model.*;
import com.tsp.finanzas.contab.*;

/**
 *
 * @author  Andres
 */
class AnulacionInvalidaException extends Exception{
    public AnulacionInvalidaException(String msg){
        super(msg);
    }
}
public class ChequeCxPAnularAction extends Action {
        //2009-09-02

    Logger logger = Logger.getLogger(this.getClass());
    
    /** Creates a new instance of ChequeCxPAnularAction */
    public ChequeCxPAnularAction() {
    }
    
    public void run() throws ServletException, InformationException {
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String dstrct = session.getAttribute("Distrito").toString();
        String next = "/jsp/cxpagar/cheques/mostrarChequeImpre.jsp";
        
        String opcion = request.getParameter("opcion");
        String banco = request.getParameter("banco");
        String sucursal = request.getParameter("sucursal");
        String cheque = request.getParameter("cheque");
        
        logger.info("?opcion: " + opcion);
        logger.info("?banco: " + banco);
        logger.info("?sucursal: " + sucursal);
        logger.info("?cheque: " + cheque);
        boolean chqReemplazo = false;
        String chqViejo = "";
        
        try{
            
            int opc = Integer.parseInt(opcion);
            
            switch( opc ){
                case 1:
                    logger.info(".. cargando las sucursales del banco: " + banco);
                    model.servicioBanco.obtenerSucursalesBanco(banco);
                    next = "/jsp/cxpagar/cheques/ChequeCxPAnular.jsp";
                    break;
                case 2:
                    //logger.info("?continuar: " + model.ChequeXFacturaSvc.chequeImpreso(dstrct, banco, sucursal, cheque.toUpperCase()));
                    if ( model.ChequeXFacturaSvc.isChequeCXP(dstrct, banco, sucursal, cheque.toUpperCase())  ) {
                        if( model.ChequeXFacturaSvc.chequeImpreso(dstrct, banco, sucursal, cheque.toUpperCase()) ){
                            /*ChequeCxpAnulacionTh hilo = new ChequeCxpAnulacionTh();
                            hilo.start(model, dstrct, banco, sucursal, cheque, usuario.getLogin());*/
                            model.ChequeXFacturaSvc.getCheque(dstrct, banco, sucursal, cheque.toUpperCase());
                            model.causas_anulacionService.llenarTree();
                            model.trecuperacionaService.llenarTree();
                            //next += "?msg=Se ha iniciado la anulaci�n del cheque.";
                        } else {
                            next = "/jsp/cxpagar/cheques/ChequeCxPAnular.jsp";
                            next += "?msg=El cheque no ha sido impreso o est� anulado.&cheque=" + cheque.toUpperCase();
                        }
                    } else {
                        next = "/jsp/cxpagar/cheques/ChequeCxPAnular.jsp";
                        next += "?msg= El cheque no pertenece a un pago o abono de Facturas de Proveedores.";
                    }
                    break;
                case 3:
                    String trecuperacion = request.getParameter("trecuperacion");
                    String causa = request.getParameter("causas");
                    String observacion = request.getParameter("observacion");
                    
                    logger.info("? trecuperacion: " + trecuperacion);
                    logger.info("? causa: " + causa);
                    logger.info("? observacion: " + observacion);
                    
                    Vector sql = new Vector();
                    String str = "";
                    
                    try{
                        //Anulacion de Cheque x Reeemplazo Henry  09-05-2007
                        Egreso reemplazo =null;// model.ChequeXFacturaSvc.getChequeReemplazo(cheque.toUpperCase());
                        if (reemplazo!=null){
                            chqViejo = cheque.toUpperCase();
                            /*Anulaci�n normal del cheque*/
                            model.ChequeXFacturaSvc.getCheque(dstrct, banco, sucursal, cheque.toUpperCase());
                            Egreso egreso = model.ChequeXFacturaSvc.getEgreso();
                            String base = egreso.getBase();                            
                            //Insert cheque negativo
                            str = null;
                            str = new String();
                            str = model.ChequeXFacturaSvc.anularCheque(dstrct, banco, sucursal, cheque, usuario.getLogin());
                            logger.info("?sql cabecera negativa: " + str);
                            sql.add(str);
                            
                            //Insert en anulacion_egreso
                            str = null;
                            str = new String();
                            str = model.ChequeXFacturaSvc.insertAnulacion_egreso(dstrct, banco, sucursal, cheque, usuario.getLogin(), causa, observacion, trecuperacion, base);
                            logger.info("?sql insertar anulacion_egreso: " + str);
                            sql.add(str);
                            
                            model.despachoService.insertar(sql);
                            
                            chqReemplazo = true;
                            dstrct    = reemplazo.getDstrct();
                            banco     = reemplazo.getBranch_code();
                            sucursal  = reemplazo.getBank_account_no();
                            cheque    = reemplazo.getDocumento();
                        }
                        //inicializando el Vector; 
                        sql = new Vector();
                        model.ChequeXFacturaSvc.getCheque(dstrct, banco, sucursal, cheque.toUpperCase());
                        Egreso egreso = model.ChequeXFacturaSvc.getEgreso();
                        String base = egreso.getBase();
                        
                        if (chqReemplazo==false) {
                            //Insert cheque negativo
                            str = null;
                            str = new String();
                            str = model.ChequeXFacturaSvc.anularCheque(dstrct, banco, sucursal, cheque, usuario.getLogin());
                            logger.info("?sql cabecera negativa: " + str);
                            sql.add(str);
                        }
                        
                        //Delete corrida
                        str = null;
                        str = new String();
                        str = model.ChequeXFacturaSvc.deleteCorridaChequeCxP(dstrct, banco, sucursal, cheque);
                        logger.info("?sql liberar corrida: " + str);
                        sql.add(str);
                        
                        //Update cxp_doc
                        str = null;
                        str = new String();
                        str = model.ChequeXFacturaSvc.updateCxP_DocCheque(dstrct, banco, sucursal, cheque);
                        logger.info("?sql actualizar cxp: " + str);
                        sql.add(str);
                        
                        /* Actualizar los saldos y abonoa a la moneda de la factura - AMATURANA 20.03.2007 */
                        model.ChequeXFacturaSvc.obtenerCxP_DocCheque(dstrct, banco, sucursal, cheque);
                        Vector fras = model.ChequeXFacturaSvc.getFacturas();
                        for( int i=0; i<fras.size(); i++){
                            RepGral obj = (RepGral) fras.elementAt(i);
                            str = null;
                            str = new String();
                            String mda_cia = (String) session.getAttribute("Moneda");
                            Tasa tasa = model.tasaService.buscarValorTasa(mda_cia, obj.getCheq_moneda(), obj.getFra_moneda(), obj.getFecha_fra());
                            logger.info("? tasa " + obj.getCheq_moneda() + " a " + obj.getFra_moneda() + " : " + tasa.getValor_tasa());
                            if( tasa==null ){
                                throw new AnulacionInvalidaException("Las tasa no registra para la conversi�n a la moneda de la factura " + obj.getDocumento() );
                            }
                            str = model.ChequeXFacturaSvc.updateSaldosCxP_DocCheque(
                            obj.getDstrct(),
                            obj.getProveedor(),
                            obj.getTipo_documento(),
                            obj.getDocumento(),
                            obj.getCheq_vlr()*tasa.getValor_tasa(),
                            obj.getCheq_vlr_for()*tasa.getValor_tasa());
                            logger.info("?actualizar saldos: " + str);
                            sql.add(str);
                        }
                        //Verificamos que no anulemos el cheque padre del reemplazo
                        if (chqReemplazo==false) {
                            //Insert en anulacion_egreso
                            str = null;
                            str = new String();
                            str = model.ChequeXFacturaSvc.insertAnulacion_egreso(dstrct, banco, sucursal, cheque, usuario.getLogin(), causa, observacion, trecuperacion, base);
                            logger.info("?sql insertar anulacion_egreso: " + str);
                            sql.add(str);
                        }
                        //Descontabilizar
                        logger.info("?transaccion: " + egreso.getTransaccion());
                        com.tsp.finanzas.contab.model.Model contab = ( com.tsp.finanzas.contab.model.Model ) session.getAttribute("modelcontab");
                        str = null;
                        str = new String();
                        str = contab.comprobanteService.descontabilizar( dstrct, "EGR", cheque, egreso.getTransaccion(), usuario);
                        logger.info("? sql descontabilizar: " + str);
                        sql.add(str);
                        
                        model.despachoService.insertar(sql);
                        if (!chqViejo.equals(""))                            
                            cheque = chqViejo;
                        next = "/jsp/cxpagar/cheques/ChequeCxPAnular.jsp" +
                        "?msg=Se ha anulado el Cheque " + cheque.toUpperCase() + " exitosamente.";
                        
                    } catch ( AnulacionInvalidaException iv ){
                        iv.printStackTrace();
                        next = "/jsp/cxpagar/cheques/ChequeCxPAnular.jsp" +
                        "?msg=Error: " + iv.getMessage();
                    } catch ( Exception exc ){
                        exc.printStackTrace();
                        next = "/jsp/cxpagar/cheques/ChequeCxPAnular.jsp" +
                        "?msg=Error: " + exc.getMessage();
                    }
                    break;
            }
            
            logger.info("?next= " + next);
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new ServletException("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
        } catch ( Exception e){
            e.printStackTrace();
            throw new ServletException("Error en ChequeCxPAnularAction .....\n"+e.getMessage());
        }
    }
    
}
