/*
 * MigracionFacturasClientesAction.java
 *
 * Created on 25 de agosto de 2006, 09:24 AM
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
//import com.tsp.operation.model.threads.ExportarPorcentajes;
import com.tsp.util.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
/**
 *
 * @author  ALVARO
 */
public class MigracionFacturasClientesAction extends Action{
    
    /** Creates a new instance of MigracionFacturasClientesAction */
    public MigracionFacturasClientesAction() {
    }
    public void run(){
        String next="/jsp/cxcobrar/migracionClientes/MigracionFacturasClientes.jsp";
        try{
            HttpSession session = request.getSession();
            String fechaInicio = (request.getParameter("fechaInicio")!=null)?request.getParameter("fechaInicio"):"";
            String fechaFinal = (request.getParameter("fechaFinal")!=null)?request.getParameter("fechaFinal"):"";
            Usuario usuario = (Usuario) session.getAttribute("Usuario");
            
            model.mfcs.setMigracionFacturasClientes(fechaInicio, fechaFinal, model, usuario);
            next="/jsp/cxcobrar/migracionClientes/MigracionFacturasClientes.jsp?Mensaje=Su proceso ha iniciado exitosamente en Rango fechas("+fechaInicio+"/"+fechaFinal+")";
            ////System.out.println(next);
            this.dispatchRequest(next);
        }catch (Exception e){
            e.printStackTrace();
        }
        
        
    }
}
