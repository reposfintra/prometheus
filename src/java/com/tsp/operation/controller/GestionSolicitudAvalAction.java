/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsp.operation.controller;


import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.GestionCondicionesService;
import com.tsp.operation.model.services.GestionConveniosService;
import com.tsp.operation.model.services.GestionSolicitudAvalService;
import com.tsp.operation.model.services.NegocioTrazabilidadService;
import com.tsp.operation.model.services.WSHistCreditoService;
import com.tsp.util.Util;
import java.util.ArrayList;
import com.tsp.operation.model.services.ProcesoIndemnizacionServices;
import com.tsp.finanzas.contab.model.beans.Comprobantes;
import com.tsp.opav.model.DAOS.ApplusDAO;
import com.tsp.operation.model.DAOS.GestionConveniosDAO;
import com.tsp.operation.model.DAOS.NegocioTrazabilidadDAO;
import com.tsp.operation.model.services.AsesoresService;
import java.io.BufferedReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Vector;
import javax.servlet.http.HttpSession;

/**
 *
 * @author rhonalf
 */
public class GestionSolicitudAvalAction extends Action {

    GestionSolicitudAvalService gsaserv;
    AsesoresService asesor;
    WSHistCreditoService wsdatacred;
    NegocioTrazabilidadService trazaService;
    String login_into="";
    ApplusDAO apdao;
    Usuario usuario = null;
    public GestionSolicitudAvalAction() {
    }

    @Override
    public void run() {
        String next = "";
        String opcion = "";
        HttpSession session = request.getSession();
        usuario = (Usuario) session.getAttribute("Usuario");
        boolean redirect = false;
        String cadenaWrite = "";
        try {
            gsaserv = new GestionSolicitudAvalService(usuario.getBd());
            asesor = new AsesoresService(usuario.getBd());
            wsdatacred = new WSHistCreditoService(usuario.getBd());
            trazaService = new NegocioTrazabilidadService(usuario.getBd());
            apdao = new ApplusDAO(usuario.getBd());
            
            opcion = request.getParameter("opcion") != null ? request.getParameter("opcion") : "";
            gsaserv.setDstrct(usuario.getDstrct() != null ? usuario.getDstrct() : "FINV");
            gsaserv.setLoginuser(usuario.getLogin() != null ? usuario.getLogin() : "ADMIN");
            login_into=  (String)session.getAttribute("login_into");

            if (opcion.equals("delete")) {
                int num_solicitud = (request.getParameter("num_solicitud") != null && request.getParameter("num_solicitud").equals("") == false)
                        ? Integer.parseInt(request.getParameter("num_solicitud")) : 0;
                boolean pasa = true;
                redirect = true;
                try {
                    gsaserv.borrarSolicitud(num_solicitud);
                } catch (Exception e) {
                    pasa = false;
                    System.out.println("error al borrar: " + e.toString());
                    e.printStackTrace();
                }
                if (pasa == true) {
                    next = "/jsp/fenalco/avales/solicitud_aval.jsp";
                } else {
                    next = "/jsp/fenalco/avales/solicitud_aval.jsp?num_solicitud=" + num_solicitud;
                }
            } else if (opcion.equals("src_sol")) {
                int num_solicitud = (request.getParameter("dato") != null && request.getParameter("dato").equals("") == false)
                        ? Integer.parseInt(request.getParameter("dato")) : 0;
                String vista = request.getParameter("vista") != null ? request.getParameter("vista") : "0";
                String tipoconv = request.getParameter("tipoconv") != null ? request.getParameter("tipoconv") : "Consumo";
                String trans = request.getParameter("trans") != null ? request.getParameter("trans") : "N";
                SolicitudAval bean = null;
                boolean pasa = true;
                redirect = false;
                try {
                    if (vista.equals("4")||vista.equals("8")||tipoconv.equals("Microcredito")) {
                        bean = gsaserv.buscarSolicitud(num_solicitud);
                        if(tipoconv.equals("Microcredito")&&!bean.getTipoConv().equals("Microcredito")){
                            bean= null;
                        }else{
                            tipoconv=bean.getTipoConv();
                            if(trans.equals("S")&&bean.isCat()){
                                 bean= null;
                            }
                        }
                    } else {

                        bean = gsaserv.buscarSolicitud(num_solicitud, usuario.getLogin());

                    }
                } catch (Exception e) {
                    System.out.println("error al listar datos: " + e.toString());
                    pasa = false;
                    e.printStackTrace();
                }
                if (pasa == true && bean != null) {
                    GestionConveniosService convServ = new GestionConveniosService(usuario.getBd());
                    boolean aval=convServ.isAvalTercero(bean.getIdConvenio());
                    if(vista.equals("8")&&!( aval&&bean.getActividadNegocio().equals("RAD")&&(bean.getEstadoSol().equals("P")||bean.getEstadoSol().equals("V"))
                            ||(!aval&&bean.getActividadNegocio().equals("RAD")&&(bean.getEstadoSol().equals("P"))))){
                        cadenaWrite = "<span class='fila'>No existe el formulario o no se encuentra listo para referenciar</span>";
                    }else{
                        if(vista.equals("4")&&(!tipoconv.equals("Multiservicio"))){
                            cadenaWrite = "<span class='fila'>La actividad de Registro es para negocios de tipo Multiservicio</span>";
                        }else{
                            if (vista.equals("6")) {
                                cadenaWrite = "/jsp/fenalco/avales/solicitud_aval_simple.jsp?num_solicitud=" + num_solicitud + "&vista=1";
                            } else if (vista.equals("7")) {
                                cadenaWrite = "/jsp/fenalco/avales/solicitud_aval_simple.jsp?num_solicitud=" + num_solicitud + "&vista=3";
                            } else {
                                cadenaWrite = "/jsp/fenalco/avales/solicitud_aval.jsp?num_solicitud=" + num_solicitud + "&vista=" + vista+"&tipoconv="+tipoconv+"&trans="+trans;
                            }
                        }
                    }
                } else {
                    cadenaWrite = "<span class='fila'>No se encontraron registros</span>";
                }
            } else if(opcion.equals("pre_solicitud")){
                redirect = false;
                
                JsonObject info = (JsonObject) (new JsonParser()).parse(request.getParameter("datos"));
                System.out.println("Datos: " + info);//imprime la fila seleccionada
                SolicitudAval bean_sol=new SolicitudAval();
                bean_sol.setNumeroSolicitud(info.getAsJsonObject("info").getAsJsonObject().get("numero_solicitud").getAsString());
                bean_sol.setValorProducto(info.getAsJsonObject("info").getAsJsonObject().get("monto_credito").getAsString());
                bean_sol.setRenovacion("");
                bean_sol.setPreAprobadoMicro("");
                
                if(info.getAsJsonObject("info").getAsJsonObject().get("tipo_credito").getAsString().equals("PREAPROBADO")){
                    bean_sol.setPreAprobadoMicro("S");
                } else if (info.getAsJsonObject("info").getAsJsonObject().get("tipo_credito").getAsString().equals("RENOVACION")){
                    bean_sol.setRenovacion("S");
                }
                
                
                bean_sol.setNumeroAprobacion("0000O");
                bean_sol.setEstadoSol(info.getAsJsonObject("info").getAsJsonObject().get("estado_sol").getAsString());
                bean_sol.setIdConvenio(info.getAsJsonObject("info").getAsJsonObject().get("id_convenio").getAsString());
                bean_sol.setFianza("S");
                bean_sol.setAsesor(info.getAsJsonObject("info").getAsJsonObject().get("asesor").getAsString());
                bean_sol.setValorSolicitado(info.getAsJsonObject("info").getAsJsonObject().get("monto_credito").getAsString());
                bean_sol.setValorAprobado(info.getAsJsonObject("info").getAsJsonObject().get("monto_credito").getAsString());
                bean_sol.setTipoPersona("natural");//quitar
                bean_sol.setActividadNegocio("");
                bean_sol.setCuotaMaxima("0");
                bean_sol.setValor_renovacion(info.getAsJsonObject("info").getAsJsonObject().get("monto_renovacion").getAsDouble());
                bean_sol.setPolitica(info.getAsJsonObject("info").getAsJsonObject().get("politica").getAsString());
                bean_sol.setNit_fondo(info.getAsJsonObject("info").getAsJsonObject().get("nit_fondo").getAsString());
                bean_sol.setProducto_fondo(info.getAsJsonObject("info").getAsJsonObject().get("producto_fondo").getAsInt());
                bean_sol.setCobertura_fondo(info.getAsJsonObject("info").getAsJsonObject().get("cobertura_fondo").getAsInt());
                
                SolicitudPersona bean_pers =this.getBeansSolicitudPersona();
                bean_pers.setPrimerApellido(info.getAsJsonObject("info").getAsJsonObject().get("primer_apellido").getAsString());
                bean_pers.setPrimerNombre(info.getAsJsonObject("info").getAsJsonObject().get("primer_nombre").getAsString());
                bean_pers.setTipoId( info.getAsJsonObject("info").getAsJsonObject().get("tipo_identificacion").getAsString());
                bean_pers.setIdentificacion( info.getAsJsonObject("info").getAsJsonObject().get("identificacion").getAsString());
                bean_pers.setFechaExpedicionId( info.getAsJsonObject("info").getAsJsonObject().get("fecha_expedicion").getAsString());
                bean_pers.setFechaNacimiento(info.getAsJsonObject("info").getAsJsonObject().get("fecha_nacimiento").getAsString());
                bean_pers.setEmail( info.getAsJsonObject("info").getAsJsonObject().get("email").getAsString());
                bean_pers.setCelular(info.getAsJsonObject("info").getAsJsonObject().get("celular").getAsString());
                bean_pers.setCiudad(info.getAsJsonObject("info").getAsJsonObject().get("ciudad").getAsString());
                bean_pers.setTipoPersona("N");
                bean_pers.setGenero("");
                String cp =info.getAsJsonObject("info").getAsJsonObject().get("compra_cartera").getAsString();
              
                session.setAttribute("PresolicitudesMicrocreditoBeans",  new PresolicitudesMicrocreditoBeans(bean_sol, bean_pers));
                
                if (info.getAsJsonObject("info").get("entidad").getAsString().equals("MICROCREDITO")) {
                    cadenaWrite = request.getContextPath()+"/jsp/fenalco/avales/solicitud_aval.jsp?num_solicitud=" + bean_sol.getNumeroSolicitud() + "&vista=30&tipoconv=Microcredito&trans=N&compra_cartera="+cp+""
                            + "&valor_renovacion="+bean_sol.getValor_renovacion()+"&politica="+bean_sol.getPolitica();
                } else if(info.getAsJsonObject("info").get("entidad").getAsString().equals("LIBRANZA")) {
                    cadenaWrite = request.getContextPath()+"/jsp/fenalco/avales/formulario_libranza.jsp?num_solicitud=" + bean_sol.getNumeroSolicitud() + "&presolicitud=yes" + "&id_filtro=" 
                            + gsaserv.obtenerIdFiltro(Integer.parseInt(bean_sol.getNumeroSolicitud()))+"&compra_cartera="+cp;
                }
                System.out.println(cadenaWrite);
               
            }else if (opcion.equals("cargarconvenios")) {
                String afiliado = request.getParameter("afiliado") != null ? request.getParameter("afiliado") : "";
                ArrayList lista = null;
                redirect = false;
                try {
                    lista = gsaserv.buscarConveniosAfil(usuario.getLogin(), afiliado);
                } catch (Exception e) {
                    System.out.println("error(action): " + e.toString());
                    e.printStackTrace();
                }
                cadenaWrite = "<select id='convenio' name='convenio' style='width:20em;'>";
                if (lista.isEmpty()) {
                    cadenaWrite += "<option value=''>...</option>";
                } else {
                    String[] dato1 = null;
                    for (int i = 0; i < lista.size(); i++) {
                        dato1 = ((String) lista.get(i)).split(";_;");
                        cadenaWrite += " <option value='" + dato1[0] + "'>" + dato1[1] + "</option>";
                    }
                }

                cadenaWrite += "</select>";
            }else if (opcion.equals("cargarservicios")) {
                String producto = request.getParameter("producto") != null ? request.getParameter("producto") : "";

                ArrayList <Codigo>lista = null;
                redirect = false;
                try {
                    lista = wsdatacred.buscarTablaRef("H", "servicio", producto);
                } catch (Exception e) {
                    System.out.println("error(action): " + e.toString());
                    e.printStackTrace();
                }
                cadenaWrite = "<select id='servicio' name='servicio' style='width:12em;'>";
                cadenaWrite += "<option value=''>...</option>";
                    for (int i = 0; i < lista.size(); i++) {
                        cadenaWrite += " <option value='" + lista.get(i).getCodigo() + "'>" +lista.get(i).getValor()+ "</option>";
                    }
                

                cadenaWrite += "</select>";
            } else if (opcion.equals("cargarmatricula")) {
                String producto = request.getParameter("producto") != null ? request.getParameter("producto") : "";

                ArrayList <Codigo>lista = null;
                redirect = false;
                try {
                    lista = wsdatacred.buscarTablaRef("H", "matricula", producto);
                } catch (Exception e) {
                    System.out.println("error(action): " + e.toString());
                    e.printStackTrace();
                }
                cadenaWrite = "<select id='matricula' name='matricula' style='width:12em;'>";

                cadenaWrite += "<option value=''>...</option>";
                for (int i = 0; i < lista.size(); i++) {
                    cadenaWrite += " <option value='" + lista.get(i).getEquivalencia() + "'>" + lista.get(i).getValor() + "</option>";
                }

                cadenaWrite += "</select>";
            } else if (opcion.equals("cargarciu")) {
                String codpt = request.getParameter("dept") != null ? request.getParameter("dept") : "";
                String idciu = request.getParameter("ciu") != null ? request.getParameter("ciu") : "";
                ArrayList lista = null;
                redirect = false;
                try {
                    lista = gsaserv.listadoCiudades(codpt);
                } catch (Exception e) {
                    System.out.println("error(action): " + e.toString());
                    e.printStackTrace();
                }
                cadenaWrite = "<select id='" + idciu + "' name='" + idciu + "' style='width:20em;' onchange=\"completarDatosNegocio(this.id)\">";
                cadenaWrite += "<option value=''>...</option>";
                String[] dato1 = null;
                for (int i = 0; i < lista.size(); i++) {
                    dato1 = ((String) lista.get(i)).split(";_;");
                    cadenaWrite += " <option value='" + dato1[0] + "'>" + dato1[1] + "</option>";
                }

                cadenaWrite += "</select>";
            } else if (opcion.equals("cargarocu")) {
                String act = request.getParameter("act") != null ? request.getParameter("act") : "";
                String ocu = request.getParameter("ocu") != null ? request.getParameter("ocu") : "";
                ArrayList lista = null;
                redirect = false;
                try {
                    lista = gsaserv.listadoOcupaciones(act);
                } catch (Exception e) {
                    System.out.println("error(action): " + e.toString());
                    e.printStackTrace();
                }
                cadenaWrite = "<select id='" + ocu + "' name='" + ocu + "' style='width:20em;'>";
                cadenaWrite += "<option value=''>...</option>";
                String[] dato1 = null;
                for (int i = 0; i < lista.size(); i++) {
                    dato1 = ((String) lista.get(i)).split(";_;");
                    cadenaWrite += " <option value='" + dato1[0] + "'>" + dato1[1] + "</option>";
                }

                cadenaWrite += "</select>";
            } else if (opcion.equals("src_sol_docs")) {
                String vista = request.getParameter("vista") != null ? request.getParameter("vista") : "2";
                int num_solicitud = (request.getParameter("dato") != null && request.getParameter("dato").equals("") == false)
                        ? Integer.parseInt(request.getParameter("dato")) : 0;
                SolicitudAval bean = null;
                boolean pasa = true;
                redirect = false;
                try {
                    if(vista.equals("5")){
                    bean = gsaserv.buscarSolicitud(num_solicitud);
                    }else{
                        bean = gsaserv.buscarSolicitud(num_solicitud, usuario.getLogin());
                    }
                    if(bean.getTipoConv()!=null&&bean.getTipoConv().equals("Multiservicio")){
                        pasa=false;
                    }
                } catch (Exception e) {
                    System.out.println("error al listar datos: " + e.toString());
                    pasa = false;
                    e.printStackTrace();
                }
                if (pasa == true && bean != null) {
                    cadenaWrite = "/jsp/fenalco/avales/solicitud_aval_documentos.jsp?num_solicitud=" + num_solicitud+"&vista="+vista;
                } else {
                    cadenaWrite = "<span class='fila'>No se encontraron registros</span>";
                }
            } else if (opcion.equals("ins_docs")) {
                String msj="";
                String radicacion= request.getParameter("radicacion")!=null?request.getParameter("radicacion"):"S";
                int num_solicitud = (request.getParameter("num_solicitud") != null && request.getParameter("num_solicitud").equals("") == false)
                        ? Integer.parseInt(request.getParameter("num_solicitud")) : 0;
                redirect = true;
                try {
                    double valor_aprob = (request.getParameter("val_aprobado") != null && request.getParameter("val_aprobado").equals("") == false)
                            ? Double.parseDouble(request.getParameter("val_aprobado")) : 0;
                    String tipo_neg = request.getParameter("tipo_neg") != null ? request.getParameter("tipo_neg") : "PG";
                    String n_tipo_neg = request.getParameter("n_tipo_neg") != null ? request.getParameter("n_tipo_neg") : "0";
                    String banco_docs = request.getParameter("banco_docs") != null ? request.getParameter("banco_docs") : "0";
                    String banco_docs_suc = request.getParameter("banco_docs_suc") != null ? request.getParameter("banco_docs_suc") : "";
                    String banco_docs_ncheq = request.getParameter("banco_docs_ncheq") != null ? request.getParameter("banco_docs_ncheq") : "0";
                    String cod_neg=request.getParameter("cod_neg") != null ? request.getParameter("cod_neg") : "";
                    boolean redesc = request.getParameter("redesc") != null ? Boolean.parseBoolean( request.getParameter("redesc")) : true;
                    String ciudad_cheque = request.getParameter("ciudad_cheque") != null ? request.getParameter("ciudad_cheque") : "";
                    gsaserv.actualizarSolicDoc(num_solicitud, ciudad_cheque, tipo_neg, n_tipo_neg, banco_docs, banco_docs_suc, banco_docs_ncheq);
                    ArrayList<SolicitudDocumentos> lista = new ArrayList<SolicitudDocumentos>();
                    SolicitudDocumentos b = null;
                    int num_docs = (request.getParameter("num_docs") != null && request.getParameter("num_docs").equals("") == false)
                            ? Integer.parseInt(request.getParameter("num_docs")) : 0;
                    String chequegirado="";
                    for (int i = 0; i < num_docs; i++) {
                        if ((request.getParameter("num_titulo" + i) != null) && (request.getParameter("num_titulo" + i).equals("") == false)) {
                            b = null;
                            b = new SolicitudDocumentos();
                            b.setNumeroSolicitud(num_solicitud+"");
                            b.setNumTitulo(request.getParameter("num_titulo" + i));
                            double valor_titulo = (request.getParameter("valor_titulo" + i) != null && request.getParameter("valor_titulo" + i).equals("") == false)
                                    ? Double.parseDouble(request.getParameter("valor_titulo" + i).replaceAll(",", "")) : 0;
                            b.setValor("" + valor_titulo);
                            String fecha_titulo = request.getParameter("fecha_titulo" + i) != null ? request.getParameter("fecha_titulo" + i) : "now()";
                            b.setFecha(fecha_titulo);
                            if(redesc){
                                 b.setEstIndemnizacion("");
                            }else{
                                if(gsaserv.chequeGirado(n_tipo_neg, request.getParameter("num_titulo" + i))){
                                    chequegirado="Cheque "+request.getParameter("num_titulo" + i)+" de la cuenta "+n_tipo_neg+" registrado en la base";
                                    i =num_docs;
                                }
                                 b.setEstIndemnizacion("VIG");
                            }
                            lista.add(b);
                        }
                    }
                   if (chequegirado.equals("")) {
                         Vector batch = new Vector();
                        if (!cod_neg.equals("") && redesc) {
                            gsaserv.updateDocumentos(num_solicitud, lista);
                            //marcar actividad                           
                            NegocioTrazabilidadDAO daotraza = new NegocioTrazabilidadDAO(usuario.getBd());
                            if (radicacion.equals("S")) {
                                batch.add(daotraza.updateActividad(cod_neg, "LIQ"));
                                 apdao.ejecutarSQL(batch);
                                // se redirecciona
                            } else {
                                // se registra la zatraza de radicacion con concepto no aplica
                                batch.add(daotraza.updateActividad(cod_neg, "RAD"));
                                batch.add(trazaService.insertNegocioTrazabilidad(trazaService.trazabilidad_radicacion(cod_neg,String.valueOf(num_solicitud), usuario)));
                            }

                        } else {
                            gsaserv.insertDocumentos(num_solicitud, lista);                             
                         }
                         apdao.ejecutarSQL(batch);
                        msj = "Titulos valores Almacenados Exitosamente.&secuencia=S";
                    } else {
                        model.Negociossvc.denny(chequegirado, cod_neg, "R");
                        msj = "Negocio: Rechazado";
                    }

                } catch (Exception e) {
                    msj = "los titulos valores no se almacenaron: " + e.toString();
                    System.out.println("error al insertar datos: " + e.toString());
                    e.printStackTrace();
                }
                if (radicacion.equals("S")) {
                    SolicitudAval bean_sol = null;
                    GestionConveniosService gcserv = new GestionConveniosService(usuario.getBd());
                      bean_sol = gsaserv.buscarSolicitud(num_solicitud);                  
                    Convenio convenio = gcserv.buscar_convenio(usuario.getBd(), bean_sol.getIdConvenio());                  
                    next = "/jsp/applus/importar.jsp?num_osx="+bean_sol.getCodNegocio()+"&tipito=negocio&form="+bean_sol.getNumeroSolicitud()+"&tipoconv="+convenio.getTipo()+"&vista=6";
                } else {
                    next = "/jsp/fenalco/avales/solicitud_aval_documentos.jsp?num_solicitud=" + num_solicitud + "&msj=" + msj;
                }
                
            } 
            
             else if (opcion.equals("ins_data")||opcion.equals("ins_data2")) {
                redirect = true;
                String botonG = request.getParameter("boton") != null ? request.getParameter("boton") : "A"; 
                String est_sol = request.getParameter("dato") != null ? request.getParameter("dato") : "B";
                String est = request.getParameter("est") != null ? request.getParameter("est") : "N";
                String cod = request.getParameter("cod") != null ? request.getParameter("cod") : "N";
                String cod2 = request.getParameter("cod2") != null ? request.getParameter("cod2") : "N";
                String tipoconv = request.getParameter("tipoconv") != null ? request.getParameter("tipoconv") : "Consumo";
                String trans = request.getParameter("trans") != null ? request.getParameter("trans") : "N";
                String vista = request.getParameter("vista") != null ? request.getParameter("vista") : "";
                String renovacion = request.getParameter("renovacion") != null ? request.getParameter("renovacion") : "N";
                String fecha_primera_cuota = request.getParameter("fecha_priemra_cuota") != null ? request.getParameter("fecha_priemra_cuota") : "0099-01-01";
                String codNegocioRenovado = request.getParameter("codNegocioRenovado") != null ? request.getParameter("codNegocioRenovado") : "";
                String preAprobadoMicro = request.getParameter("preAprobado") != null ? request.getParameter("preAprobado") : "N";
                SolicitudAval solicitud = new SolicitudAval();
                SolicitudCuentas cuenta = new SolicitudCuentas();
                SolicitudPersona persona = new SolicitudPersona();
                SolicitudReferencias referencia = new SolicitudReferencias();
                SolicitudHijos hijo = new SolicitudHijos();
                SolicitudLaboral laboral = new SolicitudLaboral();
                SolicitudBienes bien = new SolicitudBienes();
                SolicitudVehiculo vehiculo = new SolicitudVehiculo();
                SolicitudNegocio snegocio = null;
                ObligacionesCompra ocompra=null;
                SolicitudEstudiante estudiante = null;
                ArrayList<SolicitudPersona> personaList = new ArrayList<SolicitudPersona>();
                ArrayList<SolicitudCuentas> cuentaList = new ArrayList<SolicitudCuentas>();
                ArrayList<SolicitudReferencias> referenciaList = new ArrayList<SolicitudReferencias>();
                ArrayList<SolicitudLaboral> laboralList = new ArrayList<SolicitudLaboral>();
                ArrayList<SolicitudBienes> bienList = new ArrayList<SolicitudBienes>();
                ArrayList<SolicitudVehiculo> vehiculoList = new ArrayList<SolicitudVehiculo>();
                ArrayList<SolicitudHijos> hijoList = new ArrayList<SolicitudHijos>();
                ArrayList<ObligacionesCompra> ocompraList =new ArrayList<>();
                int num_solicitud = 0;
                num_solicitud = (request.getParameter("num_solicitud") != null && request.getParameter("num_solicitud").equals("") == false) ? Integer.parseInt(request.getParameter("num_solicitud")) : 0;//comentado
                 if (est_sol.equals("B")) {
                    next = "/jsp/fenalco/avales/solicitud_aval.jsp?num_solicitud=" + num_solicitud + "&vista=1&est=" + est + "&cod=" + cod +  "&cod2=" + cod2 + "&trans=" + trans;
                } else {
                    if (vista.equals("8")) {
                        next = "/jsp/fenalco/avales/solicitud_aval.jsp?num_solicitud=" + num_solicitud + "&vista=8" + "&trans=" + trans;

                    } 
                    
                    
                    if ( vista.equals("1")||vista.equals("0")||vista.equals("30")){

                        //validar si tiene opcion de liquidar negocio
                        //tipo de liquidador para la solicitud
                       if(!gsaserv.ValidarSolicitudDevuelta(String.valueOf(num_solicitud), "SOL", "REF")) {
                        {
                         TablaGen tOpcion=null;
                          model.tablaGenService.buscarDatos("VAL_OPCION", "LIQ_MICROCREDITO");
                         tOpcion = model.tablaGenService.getTblgen();
                        if ((tipoconv.equals("Microcredito")) && (model.usuarioService.ValidarOpcion(usuario.getLogin(), tOpcion.getReferencia()))) { //si es microcredito 1160-> cupo expres
                            next = "/jsp/fenalco/liquidadores/liquidadorNegMicrocredito.jsp?tipo_liq=STANDAR&num_solicitud=" + num_solicitud+"&fecha_pc="+fecha_primera_cuota+"&vista=1";
                        } else {
                              model.tablaGenService.buscarDatos("VAL_OPCION", "LIQ_NEGOCIO");
                             tOpcion = model.tablaGenService.getTblgen();
                            if ((tipoconv.equals("Consumo")) && (model.usuarioService.ValidarOpcion(usuario.getLogin(), tOpcion.getReferencia()))) { //si es consumo
                                 //next = "/jsp/fenalco/liquidadores/liquidadorNegocios.jsp?tipo_liq=STANDAR&numero_solicitud=" + num_solicitud;
                                 next = "/jsp/fenalco/liquidadores/liquidadorNegocios.jsp?tipo_liq=STANDAR&numero_solicitud=" + num_solicitud+"&act_liq=FORM";
                            } else {
                               next = "/jsp/fenalco/avales/solicitud_aval.jsp?num_solicitud=" + num_solicitud + "&vista=8" + "&trans=" + trans;// salida normal

                            }
                        }
                       }
                     }
                else
                       {
                           next = "/jsp/fenalco/avales/solicitud_aval.jsp?num_solicitud=" + num_solicitud + "&vista=3" + "&trans=" + trans;
                     }
                        
                }
                }

                //primero los datos basicos de la solicitud
                String tipo_p = request.getParameter("tipo_p") != null && !request.getParameter("tipo_p").equals("") ? request.getParameter("tipo_p") : "natural";
                if (tipo_p.equals("juridica")) {
                    solicitud.setTipoPersona("J");
                } else if (tipo_p.equals("natural")) {
                    solicitud.setTipoPersona("N");
                } else {
                    solicitud.setTipoPersona("N");
                }
                String fecha_cons = request.getParameter("fecha_cons") != null && !request.getParameter("fecha_cons").equals("") ? request.getParameter("fecha_cons") : "now()";   
                String valor_solicitado = request.getParameter("valor_solicitado") != null && !request.getParameter("valor_solicitado").trim().equals("") ? request.getParameter("valor_solicitado") : "0";
                String valor_producto = request.getParameter("valor_producto") != null && !request.getParameter("valor_producto").trim().equals("") ? request.getParameter("valor_producto") : "0";
                String agente = request.getParameter("agente") != null ? request.getParameter("agente") : "";
                String afiliado = request.getParameter("afiliado") != null ? request.getParameter("afiliado") : "";
                String convenio = request.getParameter("convenio") != null ? request.getParameter("convenio") : "";
                String producto = request.getParameter("producto") != null ? request.getParameter("producto") : "";
                String servicio = request.getParameter("servicio") != null ? request.getParameter("servicio") : "";
                String matricula = request.getParameter("matricula") != null ? request.getParameter("matricula") : "";
                String actividad = request.getParameter("actividad") != null ? request.getParameter("actividad") : "";
                String negocio = request.getParameter("negocio");
                String codigo = request.getParameter("codigo") != null && !request.getParameter("codigo").equals("") ? request.getParameter("codigo") : "";
                String codigo1 = request.getParameter("codigo1") != null && !request.getParameter("codigo1").equals("") ? request.getParameter("codigo1") : "";
                String codigo2 = request.getParameter("codigo2") != null && !request.getParameter("codigo2").equals("") ? request.getParameter("codigo2") : "";
                String numero_aprobacion = request.getParameter("numero_aprobacion") != null ? request.getParameter("numero_aprobacion") : "";
                String asesor = request.getParameter("asesor") != null ? request.getParameter("asesor") : "";
                String sector= request.getParameter("sectid");
                String subsector = request.getParameter("subsectid");
                String plazo = request.getParameter("plazo") != null ? request.getParameter("plazo") : "1";
                String plazoPrCuota = request.getParameter("forma_pago") != null ? request.getParameter("forma_pago") : "1";
                String titulo_valor=request.getParameter("cmbTituloValor") != null ? request.getParameter("cmbTituloValor") : "";
                String fianza = request.getParameter("fianza") != null ? request.getParameter("fianza") : "N";               
                
                NegocioTrazabilidad negtraza = new NegocioTrazabilidad();
                String conv = request.getParameter("conv") != null ? request.getParameter("conv") : "";
                if(!convenio.equals("")){
                    conv =convenio;
                }                
                GestionConveniosService convServ = new GestionConveniosService(usuario.getBd());
                NegocioTrazabilidadService trazaService = new NegocioTrazabilidadService(usuario.getBd());
                NegocioTrazabilidad sw = trazaService.buscarTraza(Integer.parseInt(num_solicitud+""), "SOL");
                boolean crearneg=false;
                if(est_sol.equals("P") && actividad.equals("")&&(!convServ.tieneRedescuento(conv))){
                    crearneg=true;
                    est_sol="B";
                }
                if (est_sol.equals("P") && (actividad.equals("")||(actividad.equals("SOL")&&sw!=null&&!vista.equals("4")))) {
                    negtraza.setActividad("SOL");
                    negtraza.setNumeroSolicitud(num_solicitud + "");
                    negtraza.setUsuario(usuario.getLogin());
                    negtraza.setCausal("");
                    negtraza.setComentarios("");
                    negtraza.setDstrct(usuario.getDstrct());
                    negtraza.setConcepto("");
                }
                boolean aval=convServ.isAvalTercero(conv);
                sw = trazaService.buscarTraza(Integer.parseInt(num_solicitud+""), "REG");
                if( vista.equals("4")&&(aval&&(est_sol.equals("P") ||est_sol.equals("V"))&& (actividad.equals("LIQ")||(actividad.equals("SOL")&&sw!=null)))||((!aval)&&est_sol.equals("P")&& (actividad.equals("LIQ")||(actividad.equals("SOL")&&sw!=null)))||(!aval&&actividad.equals("SOL")&&est_sol.equals("P")&&tipoconv.equals("Multiservicio"))){
                    negtraza.setActividad("REG");
                    negtraza.setNumeroSolicitud(num_solicitud + "");
                    negtraza.setUsuario(usuario.getLogin());
                    negtraza.setCausal("");
                    negtraza.setComentarios("");
                    negtraza.setCodNeg(negocio);
                    negtraza.setDstrct(usuario.getDstrct());
                    negtraza.setConcepto("");
                }

                solicitud.setNumeroSolicitud("" + num_solicitud);
                solicitud.setFechaConsulta(fecha_cons);
                solicitud.setValorSolicitado(valor_solicitado.replaceAll(",", ""));
                solicitud.setAgente(agente);
                solicitud.setAfiliado(afiliado);
                solicitud.setCodigo(codigo + codigo1 + codigo2);
                solicitud.setNumeroAprobacion(numero_aprobacion);
                solicitud.setEstadoSol(est_sol);
                solicitud.setIdConvenio(convenio);
                solicitud.setActividadNegocio(actividad);
                solicitud.setProducto(producto);
                solicitud.setServicio(servicio);
                solicitud.setCiudadMatricula(matricula);
                solicitud.setValorProducto(valor_producto.replaceAll(",", ""));
                solicitud.setAsesor(asesor);
                solicitud.setPlazo(plazo);
                solicitud.setSector(sector);
                solicitud.setSubsector(subsector);
                solicitud.setTipoNegocio(titulo_valor);
                solicitud.setPlazoPrCuota(plazoPrCuota);
                solicitud.setBanco(request.getParameter("banco1") != null ? request.getParameter("banco1") : "");
                solicitud.setNumTipoNegocio(request.getParameter("cuenta1") != null ? request.getParameter("cuenta1") : "");
                solicitud.setRenovacion(renovacion.equals("S") ? "S" : "N");
                solicitud.setFianza(fianza);
                solicitud.setCapitalDeTrabajo(request.getParameter("id_capital_trabajo") != null);
                solicitud.setActivoFijo(request.getParameter("id_activo_fijo") != null);
                solicitud.setCompraCartera(request.getParameter("compra_cartera") != null);
                solicitud.setCuotaMaxima(request.getParameter("id_cuota_maxima") != null && !request.getParameter("id_cuota_maxima").trim().equals("") ? request.getParameter("id_cuota_maxima").replaceAll(",", "") : "0");
                solicitud.setValor_renovacion( request.getParameter("id_valor_renovacion")!=null ? Double.parseDouble(request.getParameter("id_valor_renovacion")):0);
                solicitud.setPolitica(request.getParameter("id_politica") != null ? request.getParameter("id_politica") : "");
                solicitud.setNit_fondo(request.getParameter("fg_nit_fondo") != null ? request.getParameter("fg_nit_fondo") : "");
                solicitud.setProducto_fondo(request.getParameter("fg_producto") != null ?  Integer.parseInt(request.getParameter("fg_producto")) : 0);
                solicitud.setCobertura_fondo(request.getParameter("fg_cobertura") != null ? Integer.parseInt(request.getParameter("fg_cobertura")) : 0);
                solicitud.setDestinoCredito("");
                
                if (preAprobadoMicro.equals("S")) {  
                    BeanGeneral bgPreaprobado = gsaserv.buscarFechaPreaprobado(request.getParameter("id_nat") != null && !request.getParameter("id_nat").equals("") ? request.getParameter("id_nat") : "", 1);
//                    solicitud.setFecha_primera_cuota(bgPreaprobado.getValor_08());
                    solicitud.setFecha_primera_cuota(Util.getFechaActual_String(4));
                    codNegocioRenovado=bgPreaprobado.getValor_02();
                    solicitud.setRenovacion("S");     
                } else if (renovacion.equals("S")) {                    
//                    BeanGeneral fecha_pct = gsaserv.buscarFechaPreaprobado(request.getParameter("id_nat") != null && !request.getParameter("id_nat").equals("") ? request.getParameter("id_nat") : "", 1);
//                    solicitud.setFecha_primera_cuota(fecha_pct.getValor_08());
                      solicitud.setFecha_primera_cuota(Util.getFechaActual_String(4));
                } else {
                    solicitud.setFecha_primera_cuota("0099-01-01");
                }
                
                solicitud.setCodNegocioRenovado(codNegocioRenovado);
                solicitud.setPreAprobadoMicro(preAprobadoMicro.equals("S") ? "S" : "N");
                // datos negocio

                if (tipoconv.equals("Microcredito")) {
                    snegocio = new SolicitudNegocio();
                    if (tipo_p.equals("natural")) {
                        snegocio.setNombre(request.getParameter("nombre_neg") != null ? request.getParameter("nombre_neg") : "");
                        snegocio.setDireccion(request.getParameter("dir_neg") != null ? request.getParameter("dir_neg") : "");
                        snegocio.setDepartamento(request.getParameter("dep_neg") != null ? request.getParameter("dep_neg") : "");
                        snegocio.setCiudad(request.getParameter("ciu_neg") != null ? request.getParameter("ciu_neg") : "");
                        snegocio.setBarrio(request.getParameter("barrio_neg") != null ? request.getParameter("barrio_neg") : "");
                        snegocio.setTelefono(request.getParameter("telefono_neg") != null ? request.getParameter("telefono_neg") : "");

                            for (int i = 1; i < 3; i++) {
                            if ((request.getParameter("raz_soc_ref" + i) != null) && (request.getParameter("raz_soc_ref" + i).equals("") == false)) {
                                referencia = null;
                                referencia = new SolicitudReferencias();
                                referencia.setNumeroSolicitud("" + num_solicitud);
                                referencia.setTipo("S");
                                referencia.setTipoReferencia("C");
                                referencia.setSecuencia("" + i);
                                referencia.setNombre(request.getParameter("raz_soc_ref" + i) != null ? request.getParameter("raz_soc_ref" + i) : "");
                                referencia.setPrimerApellido("");
                                referencia.setSegundoApellido("");
                                referencia.setPrimerNombre("");
                                referencia.setSegundoNombre("");
                                referencia.setTelefono(request.getParameter("raz_soc_tel1" + i) != null ? request.getParameter("raz_soc_tel1" + i) : "");
                                referencia.setTelefono2(request.getParameter("raz_soc_tel2" + i) != null ? request.getParameter("raz_soc_tel2" + i) : "");
                                referencia.setExtension("");
                                referencia.setCelular("");
                                referencia.setCiudad(request.getParameter("ciu_raz_soc_" + i) != null ? request.getParameter("ciu_raz_soc_" + i) : "");
                                referencia.setDepartamento(request.getParameter("dep_raz_soc_" + i) != null ? request.getParameter("dep_raz_soc_" + i) : "");
                                referencia.setTiempoConocido("0");
                                referencia.setDireccion(request.getParameter("raz_soc_dir" + i) != null ? request.getParameter("raz_soc_dir" + i) : "");
                                referencia.setReferenciaComercial(request.getParameter("id_tipo_refc_de" + i) != null ? request.getParameter("id_tipo_refc_de" + i) : "");
                                referenciaList.add(referencia);
                            }
                        }
                    }
                    snegocio.setNumeroSolicitud("" + num_solicitud);
                    snegocio.setSector(request.getParameter("sector") != null ? request.getParameter("sector") : "");
                    snegocio.setSubsector(request.getParameter("subsector") != null ? request.getParameter("subsector") : "");
                    snegocio.setTiempoLocal(request.getParameter("tiempo_local") != null&& !request.getParameter("tiempo_local").equals("") ? request.getParameter("tiempo_local") : "0");
                    snegocio.setNumExpNeg(request.getParameter("num_exp_negocio") != null&& !request.getParameter("num_exp_negocio").equals("")  ? request.getParameter("num_exp_negocio") : "0");
                    snegocio.setTiempoMicroempresario(request.getParameter("tiempo_microempresario") != null && !request.getParameter("tiempo_microempresario").equals("") ? request.getParameter("tiempo_microempresario") : "0");
                    snegocio.setNumTrabajadores(request.getParameter("num_trabajadores") != null&& !request.getParameter("num_trabajadores").equals("")  ? request.getParameter("num_trabajadores") : "0");
                    snegocio.setTieneSocios(request.getParameter("id_tienesocios") != null ? request.getParameter("id_tienesocios") : "");
                    snegocio.setPorcentajeParticipacion(request.getParameter("id_parcitipacion") != null && !request.getParameter("id_parcitipacion").trim().equals("") ? request.getParameter("id_parcitipacion") : "0");
                    snegocio.setNombreSocio(request.getParameter("id_nomb_socio") != null ? request.getParameter("id_nomb_socio") : "");
                    snegocio.setCedulaSocio(request.getParameter("cedula_socio") != null ? request.getParameter("cedula_socio") : "");
                    snegocio.setDireccionSocio(request.getParameter("dir_neg_socio") != null ? request.getParameter("dir_neg_socio") : "");
                    snegocio.setTelefonoSocio(request.getParameter("id_tel_socio") != null ? request.getParameter("id_tel_socio") : "");
                    snegocio.setTipoActividad(request.getParameter("id_tipo_actividad") != null ? request.getParameter("id_tipo_actividad") : "");
                    snegocio.setLocal(request.getParameter("id_local") != null ? request.getParameter("id_local") : "");
                    snegocio.setTieneCamaraComercio(request.getParameter("camara_comercio") != null ? request.getParameter("camara_comercio") : "");
                    snegocio.setTipoNegocio(request.getParameter("id_tipo_negocio") != null ? request.getParameter("id_tipo_negocio") : "");
                    snegocio.setActivos(request.getParameter("activos") != null && !request.getParameter("activos").trim().equals("") ? request.getParameter("activos").replaceAll(",", "") : "0");
                    snegocio.setPasivos(request.getParameter("pasivos") != null && !request.getParameter("pasivos").trim().equals("") ? request.getParameter("pasivos").replaceAll(",", "") : "0");
                    //Aqui vamos procesar la nueva informacion de micro para compra de cartera...
                    if (request.getParameter("compra_cartera") != null) {
                        if (request.getParameter("compra_cartera").equals("on")) {
                            System.out.println("Es compra de cartera...");
                            int max_obligaciones = request.getParameter("max_obligaciones") != null ? Integer.parseInt(request.getParameter("max_obligaciones")) : 0;
                            for (int i = 1; i <= max_obligaciones; i++) {
                                if(request.getParameter("form.solicitante.obligaciones[" + i + "].valor_recoger") != null && !request.getParameter("form.solicitante.obligaciones[" + i + "].valor_recoger").equals("")){
                                    ocompra = new ObligacionesCompra();
                                    ocompra.setSecuencia(i);
                                    ocompra.setNumero_solicitud("" + num_solicitud);
                                    ocompra.setEntidad(request.getParameter("form.solicitante.obligaciones[" + i + "].entidad") != null ? request.getParameter("form.solicitante.obligaciones[" + i + "].entidad") : "");
                                    ocompra.setNit_proveedor(request.getParameter("form.solicitante.obligaciones[" + i + "].nit_prov") != null ? request.getParameter("form.solicitante.obligaciones[" + i + "].nit_prov") : "");
                                    ocompra.setNumero_cuenta(request.getParameter("form.solicitante.obligaciones[" + i + "].cuenta_prov") != null ? request.getParameter("form.solicitante.obligaciones[" + i + "].cuenta_prov") : "");
                                    ocompra.setTipo_cuenta(request.getParameter("form.solicitante.obligaciones[" + i + "].tcuenta_prov") != null ? request.getParameter("form.solicitante.obligaciones[" + i + "].tcuenta_prov") : "");
                                    ocompra.setValor_comprar(Double.parseDouble(request.getParameter("form.solicitante.obligaciones[" + i + "].valor_recoger") != null ? request.getParameter("form.solicitante.obligaciones[" + i + "].valor_recoger").replaceAll(",", "") : "0"));
                                    if (ocompra.getValor_comprar() > 0 ) {
                                        ocompraList.add(ocompra);
                                    }                                    
                                }
                            }

                        }
                    }
                }
                //datos de cuentas ...

                //primera
                cuenta = null;
                cuenta = new SolicitudCuentas();
                cuenta.setNumeroSolicitud("" + num_solicitud);
                cuenta.setConsecutivo("1");
                cuenta.setTipo(request.getParameter("tipo_cuenta1") != null ? request.getParameter("tipo_cuenta1") : "");
                cuenta.setBanco(request.getParameter("banco1") != null ? request.getParameter("banco1") : "");
                cuenta.setCuenta(request.getParameter("cuenta1") != null ? request.getParameter("cuenta1") : "");
                cuenta.setFechaApertura(request.getParameter("fecha_apertura1") != null ? (request.getParameter("fecha_apertura1").equals("") == true ? "0099-01-01 00:00:00" : request.getParameter("fecha_apertura1")) : "0099-01-01 00:00:00");
                cuenta.setNumeroTarjeta(request.getParameter("num_tarjeta1") != null ? request.getParameter("num_tarjeta1") : "");
                cuentaList.add(cuenta);
                //segunda
                if ((request.getParameter("tipo_cuenta2") != null) && (request.getParameter("tipo_cuenta2").equals("") == false)) {
                    cuenta = null;
                    cuenta = new SolicitudCuentas();
                    cuenta.setNumeroSolicitud("" + num_solicitud);
                    cuenta.setConsecutivo("2");
                    cuenta.setTipo(request.getParameter("tipo_cuenta2") != null ? request.getParameter("tipo_cuenta2") : "");
                    cuenta.setBanco(request.getParameter("banco2") != null ? request.getParameter("banco2") : "");
                    cuenta.setCuenta(request.getParameter("cuenta2") != null ? request.getParameter("cuenta2") : "");
                    cuenta.setFechaApertura(request.getParameter("fecha_apertura2") != null ? (request.getParameter("fecha_apertura2").equals("") == true ? "0099-01-01 00:00:00" : request.getParameter("fecha_apertura2")) : "0099-01-01 00:00:00");
                    cuenta.setNumeroTarjeta(request.getParameter("num_tarjeta2") != null ? request.getParameter("num_tarjeta2") : "");
                    cuentaList.add(cuenta);
                }
                //tercera
                if ((request.getParameter("tipo_cuenta3") != null) && (request.getParameter("tipo_cuenta3").equals("") == false)) {
                    cuenta = null;
                    cuenta = new SolicitudCuentas();
                    cuenta.setNumeroSolicitud("" + num_solicitud);
                    cuenta.setConsecutivo("3");
                    cuenta.setTipo(request.getParameter("tipo_cuenta3") != null ? request.getParameter("tipo_cuenta3") : "");
                    cuenta.setBanco(request.getParameter("banco3") != null ? request.getParameter("banco3") : "");
                    cuenta.setCuenta(request.getParameter("cuenta3") != null ? request.getParameter("cuenta3") : "");
                    cuenta.setFechaApertura(request.getParameter("fecha_apertura3") != null ? (request.getParameter("fecha_apertura3").equals("") == true ? "0099-01-01 00:00:00" : request.getParameter("fecha_apertura3")) : "0099-01-01 00:00:00");
                    cuenta.setNumeroTarjeta(request.getParameter("num_tarjeta3") != null ? request.getParameter("num_tarjeta3") : "");
                    cuentaList.add(cuenta);
                }
                if (tipo_p.equals("juridica")) {
                    persona = null;
                    persona = new SolicitudPersona();
                    persona.setNumeroSolicitud("" + num_solicitud);
                    persona.setTipoPersona("J");
                    persona.setTipo("S");
                    String cod_cli = gsaserv.codigoCliente(request.getParameter("nit") != null ? request.getParameter("nit") : "");
                    persona.setCodcli(cod_cli.split(";_;")[0]);
                    persona.setTipoId("NIT");
                    persona.setFechaExpedicionId("0099-01-01 00:00:00");
                    persona.setFechaNacimiento("0099-01-01 00:00:00");
                    persona.setPersonasACargo("0");
                    persona.setNumHijos("0");
                    persona.setTotalGrupoFamiliar("0");
                    persona.setEstrato("0");
                    persona.setParentescoDeudorSolidario("");
                    persona.setTiempoResidencia("0");
                    persona.setSalarioCony("0");
                    persona.setNombre(request.getParameter("razon_social") != null ? request.getParameter("razon_social") : "");
                    persona.setIdentificacion(request.getParameter("nit") != null ? request.getParameter("nit") : "");
                    persona.setCiiu(request.getParameter("ciiu") != null ? request.getParameter("ciiu") : "");
                    persona.setDireccion(request.getParameter("dir_jur") != null ? request.getParameter("dir_jur") : "");
                    persona.setCiudad(request.getParameter("ciu_jur") != null ? request.getParameter("ciu_jur") : "");
                    persona.setDepartamento(request.getParameter("dep_jur") != null ? request.getParameter("dep_jur") : "");
                    persona.setTelefono(request.getParameter("telefono1_jur") != null ? request.getParameter("telefono1_jur") : "");
                    persona.setTelefono2(request.getParameter("telefono2_jur") != null ? request.getParameter("telefono2_jur") : "");
                    persona.setFax(request.getParameter("fax") != null ? request.getParameter("fax") : "");
                    persona.setTipoEmpresa(request.getParameter("caracter") != null ? request.getParameter("caracter") : "");
                    persona.setFechaConstitucion(request.getParameter("f_const") != null ? (request.getParameter("f_const").equals("") == true ? "0099-01-01 00:00:00" : request.getParameter("f_const")) : "0099-01-01 00:00:00");
                    persona.setRepresentanteLegal(request.getParameter("rep_legal") != null ? request.getParameter("rep_legal") : "");
                    persona.setGeneroRepresentante(request.getParameter("gen_repr") != null ? request.getParameter("gen_repr") : "M");
                    persona.setTipoIdRepresentante(request.getParameter("tipo_id_repr") != null ? request.getParameter("tipo_id_repr") : "CED");
                    persona.setIdRepresentante(request.getParameter("id_repr") != null ? request.getParameter("id_repr") : "");
                    persona.setFirmadorCheques(request.getParameter("autorizado") != null ? request.getParameter("autorizado") : "");
                    persona.setGeneroFirmador(request.getParameter("gen_autor") != null ? request.getParameter("gen_autor") : "M");
                    persona.setTipoIdFirmador(request.getParameter("tipo_id_autor") != null ? request.getParameter("tipo_id_autor") : "CED");
                    persona.setIdFirmador(request.getParameter("id_autor") != null ? request.getParameter("id_autor") : "");
                    persona.setEmail(request.getParameter("mail_jur") != null ? request.getParameter("mail_jur") : "");
                    persona.setCelular(request.getParameter("cel_jur") != null ? request.getParameter("cel_jur") : "");
                    personaList.add(persona);
                    //referencias comerciales

                    for (int i = 1; i < 3; i++) {
                        if ((request.getParameter("raz_soc_ref" + i) != null) && (request.getParameter("raz_soc_ref" + i).equals("") == false)) {
                            referencia = null;
                            referencia = new SolicitudReferencias();
                            referencia.setNumeroSolicitud("" + num_solicitud);
                            referencia.setTipo("S");
                            referencia.setTipoReferencia("C");
                            referencia.setSecuencia("" + i);
                            referencia.setNombre(request.getParameter("raz_soc_ref" + i) != null ? request.getParameter("raz_soc_ref" + i) : "");
                            referencia.setPrimerApellido("");
                            referencia.setSegundoApellido("");
                            referencia.setPrimerNombre("");
                            referencia.setSegundoNombre("");
                            referencia.setTelefono(request.getParameter("raz_soc_tel1" + i) != null ? request.getParameter("raz_soc_tel1" + i) : "");
                            referencia.setTelefono2(request.getParameter("raz_soc_tel2" + i) != null ? request.getParameter("raz_soc_tel2" + i) : "");
                            referencia.setExtension("");
                            referencia.setCelular("");
                            referencia.setCiudad(request.getParameter("ciu_raz_soc_" + i) != null ? request.getParameter("ciu_raz_soc_" + i) : "");
                            referencia.setDepartamento(request.getParameter("dep_raz_soc_" + i) != null ? request.getParameter("dep_raz_soc_" + i) : "");
                            referencia.setTiempoConocido("0");
                            referencia.setDireccion(request.getParameter("raz_soc_dir" + i) != null ? request.getParameter("raz_soc_dir" + i) : "");
                            referencia.setReferenciaComercial(request.getParameter("id_tipo_refc_de" + i) != null ? request.getParameter("id_tipo_refc_de" + i) : "");
                            referenciaList.add(referencia);
                        }
                    }

                } else if (tipo_p.equals("natural")) {
                    //datos basicos
                    persona = null;
                    persona = new SolicitudPersona();
                    persona.setNumeroSolicitud("" + num_solicitud);
                    persona.setTipoPersona("N");
                    persona.setTipo("S");
                    String cod_cli = gsaserv.codigoCliente(request.getParameter("id_nat") != null ? request.getParameter("id_nat") : "");
                    persona.setCodcli(cod_cli.split(";_;")[0]);
                    persona.setPrimerApellido(request.getParameter("pr_apellido_nat") != null ? request.getParameter("pr_apellido_nat") : "");
                    persona.setSegundoApellido(request.getParameter("seg_apellido_nat") != null ? request.getParameter("seg_apellido_nat") : "");
                    persona.setPrimerNombre(request.getParameter("pr_nombre_nat") != null ? request.getParameter("pr_nombre_nat") : "");
                    persona.setSegundoNombre(request.getParameter("seg_nombre_nat") != null ? request.getParameter("seg_nombre_nat") : "");
                    persona.setNombre(persona.getPrimerApellido() + " " + persona.getSegundoApellido() + " " + persona.getPrimerNombre() + " " + persona.getSegundoNombre());//nombre completo
                    persona.setGenero(request.getParameter("genero_nat") != null ? request.getParameter("genero_nat") : "M");
                    persona.setTipoId(request.getParameter("tipo_id_nat") != null ? request.getParameter("tipo_id_nat") : "CED");
                    persona.setIdentificacion(request.getParameter("id_nat") != null && !request.getParameter("id_nat").equals("") ? request.getParameter("id_nat") : "");
                    persona.setDireccion(request.getParameter("dir_nat") != null ? request.getParameter("dir_nat") : "");
                    persona.setTelefono(request.getParameter("tel_nat") != null ? request.getParameter("tel_nat") : "");
                    persona.setFechaExpedicionId(request.getParameter("f_exp_nat") != null ? (request.getParameter("f_exp_nat").equals("") == true ? "0099-01-01 00:00:00" : request.getParameter("f_exp_nat")) : "0099-01-01 00:00:00");
                    persona.setCiudadExpedicionId(request.getParameter("ciu_exp_nat") != null ? request.getParameter("ciu_exp_nat") : "");
                    persona.setDptoExpedicionId(request.getParameter("dep_exp_nat") != null ? request.getParameter("dep_exp_nat") : "");
                    persona.setFechaNacimiento(request.getParameter("f_nac_nat") != null ? (request.getParameter("f_nac_nat").equals("") == true ? "0099-01-01 00:00:00" : request.getParameter("f_nac_nat")) : "0099-01-01 00:00:00");
                    persona.setCiudadNacimiento(request.getParameter("ciu_nac_nat") != null ? request.getParameter("ciu_nac_nat") : "");
                    persona.setDptoNacimiento(request.getParameter("dep_nac_nat") != null ? request.getParameter("dep_nac_nat") : "");
                    persona.setEstadoCivil(request.getParameter("est_civil_nat") != null ? request.getParameter("est_civil_nat") : "");
                    persona.setNivelEstudio(request.getParameter("niv_est_nat") != null ? request.getParameter("niv_est_nat") : "");
                    persona.setProfesion(request.getParameter("prof_nat") != null ? request.getParameter("prof_nat") : "");
                    persona.setPersonasACargo(request.getParameter("pcargo_nat") != null && !request.getParameter("pcargo_nat").equals("") ? request.getParameter("pcargo_nat") : "0");
                    persona.setNumHijos(request.getParameter("nhijos_nat") != null && !request.getParameter("nhijos_nat").equals("") ? request.getParameter("nhijos_nat") : "0");
                    persona.setTotalGrupoFamiliar(request.getParameter("ngrupo_nat") != null && !request.getParameter("ngrupo_nat").equals("") ? request.getParameter("ngrupo_nat") : "0");
                    persona.setCiudad(request.getParameter("ciu_nat") != null ? request.getParameter("ciu_nat") : "");
                    persona.setDepartamento(request.getParameter("dep_nat") != null ? request.getParameter("dep_nat") : "");
                    persona.setBarrio(request.getParameter("barrio_nat") != null ? request.getParameter("barrio_nat") : "");
                    persona.setEstrato(request.getParameter("estr_nat") != null && !request.getParameter("estr_nat").equals("") ? request.getParameter("estr_nat") : "0");
                    persona.setParentescoDeudorSolidario(request.getParameter("id_parentesco_cod_titular") != null ? request.getParameter("id_parentesco_cod_titular") : "");
                    persona.setFechaConstitucion("0099-01-01 00:00:00");
                    persona.setTipoVivienda(request.getParameter("tipo_viv_nat") != null ? request.getParameter("tipo_viv_nat") : "");
                    String tres = request.getParameter("tres_nat") != null && !request.getParameter("tres_nat").equals("") ? request.getParameter("tres_nat") : "0";
                    tres += " A�os ";
                    tres += request.getParameter("tres_nat1") != null && !request.getParameter("tres_nat1").equals("") ? request.getParameter("tres_nat1") : "0";
                    tres += " Meses";
                    persona.setTiempoResidencia(tres);
                    persona.setCelular(request.getParameter("cel_nat") != null ? request.getParameter("cel_nat") : "");
                    persona.setEmail(request.getParameter("mail_nat") != null ? request.getParameter("mail_nat") : "");
                    persona.setEnviarExtractoCorrespondecia(request.getParameter("id_correspondencia") != null);
                    persona.setEnviarExtractoEmail(request.getParameter("id_correo_electronico") != null);
                    
                    for (int i = 0; i < Integer.parseInt(persona.getNumHijos()); i++) {
                        hijo = null;
                        hijo = new SolicitudHijos();
                        hijo.setSecuencia((i + 1) + "");
                        hijo.setNumeroSolicitud("" + num_solicitud);
                        hijo.setTipo("S");
                        hijo.setNombre(request.getParameter("nom_hij_nat" + i) != null ? request.getParameter("nom_hij_nat" + i) : "");
                        hijo.setEdad(request.getParameter("edad_hij_nat" + i) != null && !request.getParameter("edad_hij_nat" + i).equals("") ? request.getParameter("edad_hij_nat" + i) : "0");
                        hijo.setDireccion(request.getParameter("dir_hij_nat" + i) != null ? request.getParameter("dir_hij_nat" + i) : "");
                        hijo.setEmail(request.getParameter("mail_hij_nat" + i) != null ? request.getParameter("mail_hij_nat" + i) : "");
                        hijo.setTelefono(request.getParameter("tel_hij_nat" + i) != null ? request.getParameter("tel_hij_nat" + i) : "");
                        hijoList.add(hijo);

                    }
                    //si tiene conyuge

                    persona.setPrimerApellidoCony(request.getParameter("pr_apellido_con_nat") != null ? request.getParameter("pr_apellido_con_nat") : "");
                    persona.setSegundoApellidoCony(request.getParameter("seg_apellido_con_nat") != null ? request.getParameter("seg_apellido_con_nat") : "");
                    persona.setPrimerNombreCony(request.getParameter("pr_nombre_con_nat") != null ? request.getParameter("pr_nombre_con_nat") : "");
                    persona.setSegundoNombreCony(request.getParameter("seg_nombre_con_nat") != null ? request.getParameter("seg_nombre_con_nat") : "");
                    persona.setTipoIdentificacionCony(request.getParameter("tipo_id_con_nat") != null ? request.getParameter("tipo_id_con_nat") : "CED");
                    persona.setIdentificacionCony(request.getParameter("id_con_nat") != null ? request.getParameter("id_con_nat") : "");
                    persona.setEmpresaCony(request.getParameter("emp_con_nat") != null ? request.getParameter("emp_con_nat") : "");
                    persona.setDireccionEmpresaCony(request.getParameter("emp_dir_con_nat") != null ? request.getParameter("emp_dir_con_nat") : "");
                    persona.setTelefonoCony(request.getParameter("emp_tel_con_nat") != null ? request.getParameter("emp_tel_con_nat") : "");
                    persona.setCargoCony(request.getParameter("emp_car_con_nat") != null ? request.getParameter("emp_car_con_nat") : "");
                    persona.setSalarioCony(request.getParameter("emp_sal_con_nat") != null && !request.getParameter("emp_sal_con_nat").equals("") ? request.getParameter("emp_sal_con_nat").replaceAll(",", "") : "0");
                    persona.setCelularCony(request.getParameter("cel_con_nat") != null ? request.getParameter("cel_con_nat") : "");
                    persona.setEmailCony(request.getParameter("mail_con_nat") != null ? request.getParameter("mail_con_nat") : "");

                    personaList.add(persona);
                    //datos laborales
                    laboral = null;
                    laboral = new SolicitudLaboral();
                    laboral.setNumeroSolicitud("" + num_solicitud);
                    laboral.setTipo("S");
                    laboral.setOcupacion(request.getParameter("ocup_nat") != null ? request.getParameter("ocup_nat") : "");
                    laboral.setActividadEconomica(request.getParameter("act_econ_nat") != null ? request.getParameter("act_econ_nat") : "");
                    laboral.setNombreEmpresa(request.getParameter("nom_emp_nat") != null ? request.getParameter("nom_emp_nat") : "");
                    String nit = request.getParameter("nit_emp_nat") != null ? request.getParameter("nit_emp_nat") : "";
                    nit += request.getParameter("nit_emp_nat2") != null && !request.getParameter("nit_emp_nat2").equals("") ? "-" + request.getParameter("nit_emp_nat2") : "";
                    laboral.setNit(nit);
                    laboral.setDireccion(request.getParameter("dir_emp_nat") != null ? request.getParameter("dir_emp_nat") : "");
                    laboral.setDireccionCobro(request.getParameter("dir_cob_nat") != null ? request.getParameter("dir_cob_nat") : "");
                    laboral.setCiudad(request.getParameter("ciu_emp_nat") != null ? request.getParameter("ciu_emp_nat") : "");
                    laboral.setDepartamento(request.getParameter("dep_emp_nat") != null ? request.getParameter("dep_emp_nat") : "");
                    laboral.setTelefono(request.getParameter("tel_emp_nat") != null ? request.getParameter("tel_emp_nat") : "");
                    laboral.setExtension(request.getParameter("ext_emp_nat") != null ? request.getParameter("ext_emp_nat") : "");
                    laboral.setCargo(request.getParameter("car_emp_nat") != null ? request.getParameter("car_emp_nat") : "");
                    laboral.setFechaIngreso(request.getParameter("f_ing_nat") != null && !request.getParameter("f_ing_nat").equals("") ? request.getParameter("f_ing_nat") : "0099-01-01 00:00:00");
                    laboral.setTipoContrato(request.getParameter("tipo_cont_nat") != null ? request.getParameter("tipo_cont_nat") : "");
                    laboral.setSalario(request.getParameter("sal_nat") != null && !request.getParameter("sal_nat").equals("") ? request.getParameter("sal_nat").replaceAll(",", "") : "0");
                    laboral.setOtrosIngresos(request.getParameter("otros_nat") != null && !request.getParameter("otros_nat").equals("") ? request.getParameter("otros_nat").replaceAll(",", "") : "0");
                    laboral.setConceptoOtrosIng(request.getParameter("conc_otros_nat") != null ? request.getParameter("conc_otros_nat") : "");
                    laboral.setGastosManutencion(request.getParameter("manuten_nat") != null && !request.getParameter("manuten_nat").equals("") ? request.getParameter("manuten_nat").replaceAll(",", "") : "0");
                    laboral.setGastosCreditos(request.getParameter("cred_nat") != null && !request.getParameter("cred_nat").equals("") ? request.getParameter("cred_nat").replaceAll(",", "") : "0");
                    laboral.setGastosArriendo(request.getParameter("arr_nat") != null && !request.getParameter("arr_nat").equals("") ? request.getParameter("arr_nat").replaceAll(",", "") : "0");
                    laboral.setCelular(request.getParameter("cel_emp_nat") != null ? request.getParameter("cel_emp_nat") : "");
                    laboral.setEmail(request.getParameter("mail_empr_nat") != null ? request.getParameter("mail_empr_nat") : "");
                    laboral.setEps(request.getParameter("eps_nat") != null ? request.getParameter("eps_nat") : "");
                    laboral.setTipoAfiliacion(request.getParameter("tip_afil_nat") != null ? request.getParameter("tip_afil_nat") : "");

                    laboralList.add(laboral);
                    //bean.setValor_01(""+num_solicitud);
                    //datos de bienes

                    for (int i = 1; i < 4; i++) {
                        if ((request.getParameter("tipo_bien" + i + "_nat") != null) && (request.getParameter("tipo_bien" + i + "_nat").equals("") == false)) {
                            bien = null;
                            bien = new SolicitudBienes();
                            bien.setNumeroSolicitud("" + num_solicitud);
                            bien.setTipo("S");
                            bien.setSecuencia("" + i);
                            bien.setTipoBien(request.getParameter("tipo_bien" + i + "_nat") != null ? request.getParameter("tipo_bien" + i + "_nat") : "");
                            bien.setHipoteca(request.getParameter("hipoteca" + i + "") != null ? request.getParameter("hipoteca" + i + "") : "");
                            bien.setaFavorDe(request.getParameter("favor_de" + i + "_nat") != null ? request.getParameter("favor_de" + i + "_nat") : "");
                            bien.setValorComercial(request.getParameter("valor_com" + i + "_nat") != null && !request.getParameter("valor_com" + i + "_nat").trim().equals("") ? request.getParameter("valor_com" + i + "_nat").replaceAll(",", "") : "0");
                            bien.setDireccion(request.getParameter("dir_bien" + i + "_nat") != null ? request.getParameter("dir_bien" + i + "_nat") : "");
                            bienList.add(bien);
                        }
                    }
                    //datos de vehiculos
                    for (int i = 1; i < 3; i++) {
                        if ((request.getParameter("placa_veh" + i + "_nat") != null) && (request.getParameter("placa_veh" + i + "_nat").equals("") == false)) {
                            vehiculo = null;
                            vehiculo = new SolicitudVehiculo();
                            vehiculo.setNumeroSolicitud("" + num_solicitud);
                            vehiculo.setTipo("S");
                            vehiculo.setSecuencia("" + i);
                            vehiculo.setMarca(request.getParameter("marca_veh" + i + "_nat") != null ? request.getParameter("marca_veh" + i + "_nat") : "");
                            vehiculo.setTipoVehiculo(request.getParameter("tvehiculo" + i + "_nat") != null ? request.getParameter("tvehiculo" + i + "_nat") : "");
                            vehiculo.setPlaca(request.getParameter("placa_veh" + i + "_nat") != null ? request.getParameter("placa_veh" + i + "_nat") : "");
                            vehiculo.setModelo(request.getParameter("modelo_veh" + i + "_nat") != null ? request.getParameter("modelo_veh" + i + "_nat") : "");
                            vehiculo.setValorComercial(request.getParameter("valor_veh" + i + "_nat") != null ? request.getParameter("valor_veh" + i + "_nat").replaceAll(",", "") : "0");
                            vehiculo.setCuotaMensual(request.getParameter("cuota_veh" + i + "_nat") != null ? request.getParameter("cuota_veh" + i + "_nat").replaceAll(",", "") : "0");
                            vehiculo.setPignoradoAFavorDe(request.getParameter("pign_veh" + i + "_nat") != null ? request.getParameter("pign_veh" + i + "_nat") : "");
                            vehiculoList.add(vehiculo);
                        }
                    }
                    //datos de referencias personales
                    for (int i = 1; i < 3; i++) {
                        if ((request.getParameter("pr_apellido_refp_nat" + i) != null) && (request.getParameter("pr_apellido_refp_nat" + i).equals("") == false)) {
                            referencia = null;
                            referencia = new SolicitudReferencias();
                            referencia.setNumeroSolicitud("" + num_solicitud);
                            referencia.setTipo("S");
                            referencia.setTipoReferencia("P");
                            referencia.setSecuencia("" + i);
                            referencia.setPrimerApellido(request.getParameter("pr_apellido_refp_nat" + i) != null ? request.getParameter("pr_apellido_refp_nat" + i) : "");
                            referencia.setSegundoApellido(request.getParameter("seg_apellido_refp_nat" + i) != null ? request.getParameter("seg_apellido_refp_nat" + i) : "");
                            referencia.setPrimerNombre(request.getParameter("pr_nombre_refp_nat" + i) != null ? request.getParameter("pr_nombre_refp_nat" + i) : "");
                            referencia.setSegundoNombre(request.getParameter("seg_nombre_refp_nat" + i) != null ? request.getParameter("seg_nombre_refp_nat" + i) : "");
                            referencia.setNombre(referencia.getPrimerApellido() + " " + referencia.getSegundoApellido() + " " + referencia.getPrimerNombre() + " " + referencia.getSegundoNombre());
                            referencia.setTelefono(request.getParameter("tel1_refp_nat" + i) != null ? request.getParameter("tel1_refp_nat" + i) : "");
                            referencia.setTelefono2(request.getParameter("tel2_refp_nat" + i) != null ? request.getParameter("tel2_refp_nat" + i) : "");
                            referencia.setExtension(request.getParameter("ext_refp_nat" + i) != null ? request.getParameter("ext_refp_nat" + i) : "");
                            referencia.setCelular(request.getParameter("cel_refp_nat" + i) != null ? request.getParameter("cel_refp_nat" + i) : "");
                            referencia.setCiudad(request.getParameter("ciu_refp_nat" + i) != null ? request.getParameter("ciu_refp_nat" + i) : "");
                            referencia.setDepartamento(request.getParameter("dep_refp_nat" + i) != null ? request.getParameter("dep_refp_nat" + i) : "");
                            referencia.setTiempoConocido(request.getParameter("tconocido_refp_nat" + i) != null ? request.getParameter("tconocido_refp_nat" + i) : "0");
                            referencia.setParentesco("");
                            referencia.setEmail(request.getParameter("mail_refp_nat" + i) != null ? request.getParameter("mail_refp_nat" + i) : "");
                            referencia.setDireccion(request.getParameter("dir_ref_nat" + i) != null ? request.getParameter("dir_ref_nat" + i) : "");
                            referencia.setReferenciaComercial(request.getParameter("id_tipo_refc_de" + i) != null ? request.getParameter("id_tipo_refc_de" + i) : "");
                            referenciaList.add(referencia);
                        }
                    }
                    //datos de referencias familiares
                    for (int i = 1; i < 3; i++) {

                        if ((request.getParameter("pr_apellido_refam_nat" + i) != null) && (request.getParameter("pr_apellido_refam_nat" + i).equals("") == false)) {
                            referencia = null;
                            referencia = new SolicitudReferencias();
                            referencia.setNumeroSolicitud("" + num_solicitud);
                            referencia.setTipo("S");
                            referencia.setTipoReferencia("F");
                            referencia.setSecuencia("" + i);
                            referencia.setPrimerApellido(request.getParameter("pr_apellido_refam_nat" + i) != null ? request.getParameter("pr_apellido_refam_nat" + i) : "");
                            referencia.setSegundoApellido(request.getParameter("seg_apellido_refam_nat" + i) != null ? request.getParameter("seg_apellido_refam_nat" + i) : "");
                            referencia.setPrimerNombre(request.getParameter("pr_nombre_refam_nat" + i) != null ? request.getParameter("pr_nombre_refam_nat" + i) : "");
                            referencia.setSegundoNombre(request.getParameter("seg_nombre_refam_nat" + i) != null ? request.getParameter("seg_nombre_refam_nat" + i) : "");
                            referencia.setNombre(referencia.getPrimerApellido() + " " + referencia.getSegundoApellido() + " " + referencia.getPrimerNombre() + " " + referencia.getSegundoNombre());
                            referencia.setTelefono(request.getParameter("tel1_refam_nat" + i) != null ? request.getParameter("tel1_refam_nat" + i) : "");
                            referencia.setTelefono2(request.getParameter("tel2_refam_nat" + i) != null ? request.getParameter("tel2_refam_nat" + i) : "");
                            referencia.setExtension(request.getParameter("ext_refam_nat" + i) != null ? request.getParameter("ext_refam_nat" + i) : "");
                            referencia.setCelular(request.getParameter("cel_refam_nat" + i) != null ? request.getParameter("cel_refam_nat" + i) : "");
                            referencia.setCiudad(request.getParameter("ciu_refam_nat" + i) != null ? request.getParameter("ciu_refam_nat" + i) : "");
                            referencia.setDepartamento(request.getParameter("dep_refam_nat" + i) != null ? request.getParameter("dep_refam_nat" + i) : "");
                            referencia.setTiempoConocido("");
                            referencia.setParentesco(request.getParameter("parent_refam_nat" + i) != null ? request.getParameter("parent_refam_nat" + i) : "0");
                            referencia.setEmail(request.getParameter("mail_refam_nat" + i) != null ? request.getParameter("mail_refam_nat" + i) : "");
                            referencia.setDireccion(request.getParameter("dir_refam_nat" + i) != null ? request.getParameter("dir_refam_nat" + i) : "");
                            referenciaList.add(referencia);
                        }
                    }
                }
                //codeudor
                if (((request.getParameter("pr_apellido_cod") != null) && (request.getParameter("pr_apellido_cod").equals("") == false))
                 || ((request.getParameter("nombre_cod_jur") != null) && (request.getParameter("nombre_cod_jur").equals("") == false))) {
                
                    if ((request.getParameter("pr_apellido_cod") != null) && (request.getParameter("pr_apellido_cod").equals("") == false)) {
                        cod = "S";
                    }
                    if ((request.getParameter("nombre_cod_jur") != null) && (request.getParameter("nombre_cod_jur").equals("") == false)) {
                        cod2 = "S";
                    }   
                    persona = null;
                    persona = new SolicitudPersona();
                    persona.setNumeroSolicitud("" + num_solicitud);
                    persona.setTipo("C");
                    if(cod.equals("S")) {
                        persona.setTipoPersona("N");
                        String cod_cli = gsaserv.codigoCliente(request.getParameter("id_cod") != null ? request.getParameter("id_cod") : "");
                        persona.setCodcli(cod_cli.split(";_;")[0]);
                        persona.setPrimerApellido(request.getParameter("pr_apellido_cod") != null ? request.getParameter("pr_apellido_cod") : "");
                        persona.setSegundoApellido(request.getParameter("seg_apellido_cod") != null ? request.getParameter("seg_apellido_cod") : "");
                        persona.setPrimerNombre(request.getParameter("pr_nombre_cod") != null ? request.getParameter("pr_nombre_cod") : "");
                        persona.setSegundoNombre(request.getParameter("seg_nombre_cod") != null ? request.getParameter("seg_nombre_cod") : "");
                        persona.setNombre(persona.getPrimerApellido() + " " + persona.getSegundoApellido() + " " + persona.getPrimerNombre() + " " + persona.getSegundoNombre());//nombre completo
                        persona.setGenero(request.getParameter("genero_cod") != null ? request.getParameter("genero_cod") : "M");
                        persona.setTipoId(request.getParameter("tipo_id_cod") != null ? request.getParameter("tipo_id_cod") : "CED");
                        persona.setIdentificacion(request.getParameter("id_cod") != null ? request.getParameter("id_cod") : "");
                        persona.setFechaExpedicionId(request.getParameter("f_exp_cod") != null && !request.getParameter("f_exp_cod").equals("") ? request.getParameter("f_exp_cod") : "0099-01-01 00:00:00");
                        persona.setCiudadExpedicionId(request.getParameter("ciu_exp_cod") != null ? request.getParameter("ciu_exp_cod") : "");
                        persona.setDptoExpedicionId(request.getParameter("dep_exp_cod") != null ? request.getParameter("dep_exp_cod") : "");
                        persona.setFechaNacimiento(request.getParameter("f_nac_cod") != null && !request.getParameter("f_nac_cod").equals("") ? request.getParameter("f_nac_cod") : "0099-01-01 00:00:00");
                        persona.setCiudadNacimiento(request.getParameter("ciu_nac_cod") != null ? request.getParameter("ciu_nac_cod") : "");
                        persona.setDptoNacimiento(request.getParameter("dep_nac_cod") != null ? request.getParameter("dep_nac_cod") : "");
                        persona.setEstadoCivil(request.getParameter("est_civil_cod") != null ? request.getParameter("est_civil_cod") : "");
                        persona.setNivelEstudio(request.getParameter("niv_est_cod") != null ? request.getParameter("niv_est_cod") : "");
                        persona.setProfesion(request.getParameter("prof_cod") != null ? request.getParameter("prof_cod") : "");
                        persona.setPersonasACargo(request.getParameter("pcargo_cod") != null && !request.getParameter("pcargo_cod").equals("") ? request.getParameter("pcargo_cod") : "0");
                        persona.setNumHijos(request.getParameter("nhijos_cod") != null && !request.getParameter("nhijos_cod").equals("") ? request.getParameter("nhijos_cod") : "0");
                        persona.setTotalGrupoFamiliar(request.getParameter("ngrupo_cod") != null && !request.getParameter("ngrupo_cod").equals("") ? request.getParameter("ngrupo_cod") : "0");
                        persona.setDireccion(request.getParameter("dir_cod") != null ? request.getParameter("dir_cod") : "");
                        persona.setCiudad(request.getParameter("ciu_cod") != null ? request.getParameter("ciu_cod") : "");
                        persona.setDepartamento(request.getParameter("dep_cod") != null ? request.getParameter("dep_cod") : "");
                        persona.setBarrio(request.getParameter("barrio_cod") != null ? request.getParameter("barrio_cod") : "");
                        persona.setEstrato(request.getParameter("estr_cod") != null && !request.getParameter("estr_cod").equals("") ? request.getParameter("estr_cod") : "0");
                        persona.setParentescoDeudorSolidario(request.getParameter("id_parentesco_cod_titular") != null ? request.getParameter("id_parentesco_cod_titular") : "");
                        persona.setTelefono(request.getParameter("tel_cod") != null ? request.getParameter("tel_cod") : "");
                        persona.setTipoVivienda(request.getParameter("tipo_viv_cod") != null ? request.getParameter("tipo_viv_cod") : "");
                        persona.setFechaConstitucion("0099-01-01 00:00:00");
                        String tres = request.getParameter("tres_cod") != null && !request.getParameter("tres_cod").equals("") ? request.getParameter("tres_cod") : "0";
                        tres += " A�os ";
                        tres += request.getParameter("tres_cod1") != null && !request.getParameter("tres_cod1").equals("") ? request.getParameter("tres_cod1") : "0";
                        tres += " Meses";
                        persona.setTiempoResidencia(tres);
                        persona.setCelular(request.getParameter("cel_cod") != null ? request.getParameter("cel_cod") : "");
                        persona.setEmail(request.getParameter("mail_cod") != null ? request.getParameter("mail_cod") : "");
                        persona.setSalarioCony("0");
                    } else if (cod2.equals("S")) {
                        persona.setTipoPersona("J");
                        persona.setNombre(request.getParameter("nombre_cod_jur") != null ? request.getParameter("nombre_cod_jur") : "");//nombre completo
                        persona.setTipoId("NIT");
                        persona.setIdentificacion(request.getParameter("id_cod_jur") != null ? request.getParameter("id_cod_jur") : "");
                    
                        persona.setDireccion(request.getParameter("dir_cod_jur") != null ? request.getParameter("dir_cod_jur") : "");
                        persona.setCiudad(request.getParameter("ciu_cod_jur") != null ? request.getParameter("ciu_cod_jur") : "");
                        persona.setDepartamento(request.getParameter("dep_cod_jur") != null ? request.getParameter("dep_cod_jur") : "");
                        persona.setBarrio(request.getParameter("barrio_cod_jur") != null ? request.getParameter("barrio_cod_jur") : "");
                        persona.setTelefono(request.getParameter("tel_cod_jur") != null ? request.getParameter("tel_cod_jur") : "");
                        persona.setCelular(request.getParameter("cel_cod_jur") != null ? request.getParameter("cel_cod_jur") : "");
                        persona.setEmail(request.getParameter("mail_cod_jur") != null ? request.getParameter("mail_cod_jur") : "");
                        
                        persona.setTipoEmpresa(request.getParameter("caracter_cod_jur") != null ? request.getParameter("caracter_cod_jur") : "");
                        persona.setFechaConstitucion(request.getParameter("f_exp_cod_jur") != null ? (request.getParameter("f_exp_cod_jur").equals("") == true ? "0099-01-01 00:00:00" : request.getParameter("f_exp_cod_jur")) : "0099-01-01 00:00:00");
                        persona.setRepresentanteLegal(request.getParameter("rep_legal_cod_jur") != null ? request.getParameter("rep_legal_cod_jur") : "");
                        persona.setGeneroRepresentante(request.getParameter("gen_repr_cod_jur") != null ? request.getParameter("gen_repr_cod_jur") : "M");
                        persona.setTipoIdRepresentante(request.getParameter("tipo_id_repr_cod_jur") != null ? request.getParameter("tipo_id_repr_cod_jur") : "CED");
                        persona.setIdRepresentante(request.getParameter("id_repr_cod_jur") != null ? request.getParameter("id_repr_cod_jur") : "");
                        
                        persona.setFechaNacimiento("0099-01-01 00:00:00");
                        persona.setFechaExpedicionId("0099-01-01 00:00:00");
                        persona.setPersonasACargo("0");
                        persona.setNumHijos("0");
                        persona.setTotalGrupoFamiliar("0");
                        persona.setEstrato("0");
                        persona.setParentescoDeudorSolidario(request.getParameter("id_parentesco_cod_titular") != null ? request.getParameter("id_parentesco_cod_titular") : "");
                        persona.setTiempoResidencia("0");
                        persona.setSalarioCony("0");
                    }
                    personaList.add(persona);

                    for (int i = 0; i < Integer.parseInt(persona.getNumHijos()); i++) {
                        hijo = null;
                        hijo = new SolicitudHijos();
                        hijo.setSecuencia((i + 1) + "");
                        hijo.setNumeroSolicitud("" + num_solicitud);
                        hijo.setTipo("C");
                        hijo.setNombre(request.getParameter("nom_hij_cod" + i) != null ? request.getParameter("nom_hij_cod" + i) : "0");
                        hijo.setEdad(request.getParameter("edad_hij_cod" + i) != null && !request.getParameter("edad_hij_cod" + i).equals("") ? request.getParameter("edad_hij_cod" + i) : "0");
                        hijo.setDireccion(request.getParameter("dir_hij_cod" + i) != null ? request.getParameter("dir_hij_cod" + i) : "0");
                        hijo.setEmail(request.getParameter("mail_hij_cod" + i) != null ? request.getParameter("mail_hij_cod" + i) : "0");
                        hijo.setTelefono(request.getParameter("tel_hij_cod" + i) != null ? request.getParameter("tel_hij_cod" + i) : "0");
                        hijoList.add(hijo);

                    }
                    
                    for (int i = 1; i < 3; i++) {
                            if ((request.getParameter("raz_soc_ref_cod" + i) != null) && (request.getParameter("raz_soc_ref_cod" + i).equals("") == false)) {
                                referencia = null;
                                referencia = new SolicitudReferencias();
                                referencia.setNumeroSolicitud("" + num_solicitud);
                                referencia.setTipo("C");
                                referencia.setTipoReferencia("C");
                                referencia.setSecuencia("" + i);
                                referencia.setNombre(request.getParameter("raz_soc_ref_cod" + i) != null ? request.getParameter("raz_soc_ref_cod" + i) : "");
                                referencia.setPrimerApellido("");
                                referencia.setSegundoApellido("");
                                referencia.setPrimerNombre("");
                                referencia.setSegundoNombre("");
                                referencia.setTelefono(request.getParameter("raz_soc_tel1_cod" + i) != null ? request.getParameter("raz_soc_tel1_cod" + i) : "");
                                referencia.setTelefono2(request.getParameter("raz_soc_tel2_cod" + i) != null ? request.getParameter("raz_soc_tel2_cod" + i) : "");
                                referencia.setExtension("");
                                referencia.setCelular("");
                                referencia.setCiudad(request.getParameter("ciu_raz_soc_cod" + i) != null ? request.getParameter("ciu_raz_soc_cod" + i) : "");
                                referencia.setDepartamento(request.getParameter("dep_raz_soc_cod" + i) != null ? request.getParameter("dep_raz_soc_cod" + i) : "");
                                referencia.setTiempoConocido("0");
                                referencia.setDireccion(request.getParameter("raz_soc_dir_cod" + i) != null ? request.getParameter("raz_soc_dir_cod" + i) : "");
                                referencia.setReferenciaComercial(request.getParameter("id_tipo_refc_de_cod" + i) != null ? request.getParameter("id_tipo_refc_de_cod" + i) : "");
                                referenciaList.add(referencia);
                            }
                      }

                    for (int i = 1; i < 3; i++) {
                            if ((request.getParameter("pr_apellido_refp_cod" + i) != null) && (request.getParameter("pr_apellido_refp_cod" + i).equals("") == false)) {
                    referencia = null;
                    referencia = new SolicitudReferencias();
                    referencia.setNumeroSolicitud("" + num_solicitud);
                    referencia.setTipo("C");
                    referencia.setTipoReferencia("P");
                    referencia.setSecuencia(""+i);
                    referencia.setPrimerApellido(request.getParameter("pr_apellido_refp_cod"+i) != null ? request.getParameter("pr_apellido_refp_cod"+i) : "");
                    referencia.setSegundoApellido(request.getParameter("seg_apellido_refp_cod"+i) != null ? request.getParameter("seg_apellido_refp_cod"+i) : "");
                    referencia.setPrimerNombre(request.getParameter("pr_nombre_refp_cod"+i) != null ? request.getParameter("pr_nombre_refp_cod"+i) : "");
                    referencia.setSegundoNombre(request.getParameter("seg_nombre_refp_cod"+i) != null ? request.getParameter("seg_nombre_refp_cod"+i) : "");
                    referencia.setNombre(referencia.getPrimerApellido() + " " + referencia.getSegundoApellido() + " " + referencia.getPrimerNombre() + " " + referencia.getSegundoNombre());
                    referencia.setTelefono(request.getParameter("tel1_refp_cod"+i) != null ? request.getParameter("tel1_refp_cod"+i) : "");
                    referencia.setTelefono2(request.getParameter("tel2_refp_cod"+i) != null ? request.getParameter("tel2_refp_cod"+i) : "");
                    referencia.setExtension(request.getParameter("ext_refp_cod"+i) != null ? request.getParameter("ext_refp_cod"+i) : "");
                    referencia.setCelular(request.getParameter("cel_refp_cod"+i) != null ? request.getParameter("cel_refp_cod"+i) : "");
                    referencia.setCiudad(request.getParameter("ciu_refp_cod"+i) != null ? request.getParameter("ciu_refp_cod"+i) : "");
                    referencia.setDepartamento(request.getParameter("dep_refp_cod"+i) != null ? request.getParameter("dep_refp_cod"+i) : "");
                    referencia.setTiempoConocido(request.getParameter("tconocido_refp_cod"+i) != null ? request.getParameter("tconocido_refp_cod"+i) : "0");
                    referencia.setEmail(request.getParameter("mail_refp_cod"+i) != null ? request.getParameter("mail_refp_cod"+i) : "");
                    referencia.setDireccion(request.getParameter("dir_ref_cod"+i) != null ? request.getParameter("dir_ref_cod"+i) : "");
                    referencia.setParentesco("");
                    referenciaList.add(referencia);
                            }
                    }
                    //laboral codeudor

                    laboral = null;
                    laboral = new SolicitudLaboral();
                    laboral.setNumeroSolicitud("" + num_solicitud);
                    laboral.setTipo("C");
                    laboral.setOcupacion(request.getParameter("ocup_cod") != null ? request.getParameter("ocup_cod") : "");
                    laboral.setActividadEconomica(request.getParameter("act_econ_cod") != null ? request.getParameter("act_econ_cod") : "");
                    laboral.setNombreEmpresa(request.getParameter("nom_emp_cod") != null ? request.getParameter("nom_emp_cod") : "");
                    String nit = request.getParameter("nit_emp_cod") != null ? request.getParameter("nit_emp_cod") : "";
                    nit += request.getParameter("nit_emp_cod2") != null && !request.getParameter("nit_emp_cod2").equals("") ? "-" + request.getParameter("nit_emp_cod2") : "";
                    laboral.setNit(nit);
                    laboral.setDireccion(request.getParameter("dir_emp_cod") != null ? request.getParameter("dir_emp_cod") : "");
                    laboral.setDireccionCobro(request.getParameter("dir_cob_cod") != null ? request.getParameter("dir_cob_cod") : "");
                    laboral.setCiudad(request.getParameter("ciu_emp_cod") != null ? request.getParameter("ciu_emp_cod") : "");
                    laboral.setDepartamento(request.getParameter("dep_emp_cod") != null ? request.getParameter("dep_emp_cod") : "");
                    laboral.setTelefono(request.getParameter("tel_emp_cod") != null ? request.getParameter("tel_emp_cod") : "");
                    laboral.setExtension(request.getParameter("ext_emp_cod") != null ? request.getParameter("ext_emp_cod") : "");
                    laboral.setCargo(request.getParameter("car_emp_cod") != null ? request.getParameter("car_emp_cod") : "");
                    laboral.setFechaIngreso(request.getParameter("f_ing_cod") != null && !request.getParameter("f_ing_cod").equals("") ? request.getParameter("f_ing_cod") : "0099-01-01 00:00:00");
                    laboral.setTipoContrato(request.getParameter("tipo_cont_cod") != null ? request.getParameter("tipo_cont_cod") : "");
                    laboral.setSalario(request.getParameter("sal_cod") != null && !request.getParameter("sal_cod").trim().equals("") ? request.getParameter("sal_cod").replaceAll(",", "") : "0");
                    laboral.setOtrosIngresos(request.getParameter("otros_cod") != null && !request.getParameter("otros_cod").trim().equals("") ? request.getParameter("otros_cod").replaceAll(",", "") : "0");
                    laboral.setConceptoOtrosIng(request.getParameter("conc_otros_cod") != null ? request.getParameter("conc_otros_cod") : "");
                    laboral.setGastosManutencion(request.getParameter("manuten_cod") != null && !request.getParameter("manuten_cod").trim().equals("") ? request.getParameter("manuten_cod").replaceAll(",", "") : "0");
                    laboral.setGastosCreditos(request.getParameter("cred_cod") != null && !request.getParameter("cred_cod").trim().equals("") ? request.getParameter("cred_cod").replaceAll(",", "") : "0");
                    laboral.setGastosArriendo(request.getParameter("arr_cod") != null && !request.getParameter("arr_cod").trim().equals("") ? request.getParameter("arr_cod").replaceAll(",", "") : "0");
                    laboral.setCelular(request.getParameter("cel_emp_cod") != null ? request.getParameter("cel_emp_cod") : "");
                    laboral.setEmail(request.getParameter("mail_emp_cod") != null ? request.getParameter("mail_emp_cod") : "");
                    laboral.setEps(request.getParameter("eps_cod") != null ? request.getParameter("eps_cod") : "");
                    laboral.setTipoAfiliacion(request.getParameter("tip_afil_cod") != null ? request.getParameter("tip_afil_cod") : "");
                    laboralList.add(laboral);

                    //datos de referencias familiares

                     for (int i = 1; i < 3; i++) {
                            if ((request.getParameter("pr_apellido_refam_cod" + i) != null) && (request.getParameter("pr_apellido_refam_cod" + i).equals("") == false)) {
                    referencia = null;
                    referencia = new SolicitudReferencias();
                    referencia.setNumeroSolicitud("" + num_solicitud);
                    referencia.setTipo("C");
                    referencia.setTipoReferencia("F");
                    referencia.setSecuencia(""+i);
                    referencia.setPrimerApellido(request.getParameter("pr_apellido_refam_cod"+ i) != null ? request.getParameter("pr_apellido_refam_cod"+ i) : "");
                    referencia.setSegundoApellido(request.getParameter("seg_apellido_refam_cod"+ i) != null ? request.getParameter("seg_apellido_refam_cod"+ i) : "");
                    referencia.setPrimerNombre(request.getParameter("pr_nombre_refam_cod"+ i) != null ? request.getParameter("pr_nombre_refam_cod"+ i) : "");
                    referencia.setSegundoNombre(request.getParameter("seg_nombre_refam_cod") != null ? request.getParameter("seg_nombre_refam_cod"+ i) : "");
                    referencia.setNombre(referencia.getPrimerApellido() + " " + referencia.getSegundoApellido() + " " + referencia.getPrimerNombre() + " " + referencia.getSegundoNombre());
                    referencia.setTelefono(request.getParameter("tel1_refam_cod"+ i) != null ? request.getParameter("tel1_refam_cod"+ i) : "");
                    referencia.setTelefono2(request.getParameter("tel2_refam_cod"+ i) != null ? request.getParameter("tel2_refam_cod"+ i) : "");
                    referencia.setExtension(request.getParameter("ext_refam_cod"+ i) != null ? request.getParameter("ext_refam_cod"+ i) : "");
                    referencia.setCelular(request.getParameter("cel_refam_cod"+ i) != null ? request.getParameter("cel_refam_cod"+ i) : "");
                    referencia.setCiudad(request.getParameter("ciu_refam_cod"+ i) != null ? request.getParameter("ciu_refam_cod"+ i) : "");
                    referencia.setDepartamento(request.getParameter("dep_refam_cod"+ i) != null ? request.getParameter("dep_refam_cod"+ i) : "");
                    referencia.setTiempoConocido("");
                    referencia.setParentesco(request.getParameter("parent_refam_cod"+ i) != null ? request.getParameter("parent_refam_cod"+ i) : "");
                    referencia.setEmail(request.getParameter("mail_refam_cod"+ i) != null ? request.getParameter("mail_refam_cod"+ i) : "");
                    referencia.setDireccion(request.getParameter("dir_refam_cod"+i) != null ? request.getParameter("dir_refam_cod"+i) : "");
                    referenciaList.add(referencia);
                            }
                     }
                    
                }
                //estudiante
                if ((request.getParameter("pr_apellido_est") != null) && (request.getParameter("pr_apellido_est").equals("") == false)) {
                    estudiante = new SolicitudEstudiante();
                    persona = null;
                    persona = new SolicitudPersona();
                    persona.setNumeroSolicitud("" + num_solicitud);
                    persona.setTipoPersona("N");
                    persona.setTipo("E");
                    String cod_cli = gsaserv.codigoCliente(request.getParameter("id_est") != null ? request.getParameter("id_est") : "");
                    persona.setCodcli(cod_cli.split(";_;")[0]);
                    persona.setPrimerApellido(request.getParameter("pr_apellido_est") != null ? request.getParameter("pr_apellido_est") : "");
                    persona.setSegundoApellido(request.getParameter("seg_apellido_est") != null ? request.getParameter("seg_apellido_est") : "");
                    persona.setPrimerNombre(request.getParameter("pr_nombre_est") != null ? request.getParameter("pr_nombre_est") : "");
                    persona.setSegundoNombre(request.getParameter("seg_nombre_est") != null ? request.getParameter("seg_nombre_est") : "");
                    persona.setNombre(persona.getPrimerApellido() + " " + persona.getSegundoApellido() + " " + persona.getPrimerNombre() + " " + persona.getSegundoNombre());
                    persona.setGenero(request.getParameter("genero_est") != null ? request.getParameter("genero_est") : "M");
                    persona.setTipoId(request.getParameter("tipo_id_est") != null ? request.getParameter("tipo_id_est") : "CED");
                    persona.setIdentificacion(request.getParameter("id_est") != null ? request.getParameter("id_est") : "");
                    persona.setFechaNacimiento(request.getParameter("f_nac_est") != null && !request.getParameter("f_nac_est").equals("") ? request.getParameter("f_nac_est") : "0099-01-01 00:00:00");
                    persona.setCiudadNacimiento(request.getParameter("ciu_nac_est") != null ? request.getParameter("ciu_nac_est") : "");
                    persona.setDptoNacimiento(request.getParameter("dep_nac_est") != null ? request.getParameter("dep_nac_est") : "");
                    persona.setDireccion(request.getParameter("dir_est") != null ? request.getParameter("dir_est") : "");
                    persona.setCiudad(request.getParameter("ciu_est") != null ? request.getParameter("ciu_est") : "");
                    persona.setDepartamento(request.getParameter("dep_est") != null ? request.getParameter("dep_est") : "");
                    persona.setTelefono(request.getParameter("tel_est") != null ? request.getParameter("tel_est") : "");
                    persona.setCelular(request.getParameter("cel_est") != null ? request.getParameter("cel_est") : "");
                    persona.setEmail(request.getParameter("mail_est") != null ? request.getParameter("mail_est") : "");
                    persona.setFechaExpedicionId(request.getParameter("f_exp_est") != null && !request.getParameter("f_exp_est").equals("") ? request.getParameter("f_exp_est") : "0099-01-01 00:00:00");
                    persona.setCiudadExpedicionId(request.getParameter("ciu_exp_est") != null ? request.getParameter("ciu_exp_est") : "");
                    persona.setDptoExpedicionId(request.getParameter("dep_exp_est") != null ? request.getParameter("dep_exp_est") : "");
                    persona.setEstadoCivil(request.getParameter("est_civil_est") != null ? request.getParameter("est_civil_est") : "");
                    persona.setNivelEstudio(request.getParameter("niv_est_est") != null ? request.getParameter("niv_est_est") : "");
                    persona.setPersonasACargo(request.getParameter("pcargo_est") != null && !request.getParameter("pcargo_est").equals("") ? request.getParameter("pcargo_est") : "0");
                    persona.setNumHijos(request.getParameter("nhijos_est") != null && !request.getParameter("nhijos_est").equals("") ? request.getParameter("nhijos_est") : "0");
                    persona.setTotalGrupoFamiliar(request.getParameter("ngrupo_est") != null && !request.getParameter("ngrupo_est").equals("") ? request.getParameter("ngrupo_est") : "0");
                    persona.setBarrio(request.getParameter("barrio_est") != null ? request.getParameter("barrio_est") : "");
                    persona.setEstrato(request.getParameter("estr_est") != null && !request.getParameter("estr_est").equals("") ? request.getParameter("estr_est") : "0");
                    persona.setTipoVivienda(request.getParameter("tipo_viv_est") != null ? request.getParameter("tipo_viv_est") : "");
                    persona.setFechaConstitucion("0099-01-01 00:00:00");
                    String tres = request.getParameter("tres_est") != null && !request.getParameter("tres_est").equals("") ? request.getParameter("tres_est") : "0";
                    tres += " A�os ";
                    tres += request.getParameter("tres_est1") != null && !request.getParameter("tres_est1").equals("") ? request.getParameter("tres_est1") : "0";
                    tres += " Meses";
                    persona.setTiempoResidencia(tres);
                    persona.setSalarioCony("0");
                    estudiante.setNumeroSolicitud("" + num_solicitud);
                    estudiante.setParentescoGirador(request.getParameter("parentesco") != null ? request.getParameter("parentesco") : "");
                    estudiante.setUniversidad(request.getParameter("nom_uni") != null ? request.getParameter("nom_uni") : "");
                    estudiante.setPrograma(request.getParameter("nom_progr") != null ? request.getParameter("nom_progr") : "");
                    estudiante.setFechaIngresoPrograma(request.getParameter("f_ing_est") != null && !request.getParameter("f_ing_est").equals("") ? request.getParameter("f_ing_est") : "0099-01-01 00:00:00");
                    estudiante.setCodigo(request.getParameter("cod_est") != null ? request.getParameter("cod_est") : "");
                    estudiante.setSemestre(request.getParameter("sem_est") != null && !request.getParameter("sem_est").trim().equals("") ? request.getParameter("sem_est") : "1");
                    estudiante.setValorSemestre(request.getParameter("val_sem_est") != null && !request.getParameter("val_sem_est").trim().equals("") ? request.getParameter("val_sem_est").replaceAll(",", "") : "0");
                    estudiante.setTipoCarrera(request.getParameter("tipo_carrera") != null ? request.getParameter("tipo_carrera") : "");
                    estudiante.setTrabaja(request.getParameter("trabaja") != null ? request.getParameter("trabaja") : "N");
                    estudiante.setNombreEmpresa(request.getParameter("emp_est") != null ? request.getParameter("emp_est") : "");
                    estudiante.setDireccionEmpresa(request.getParameter("dir_emp_est") != null ? request.getParameter("dir_emp_est") : "");
                    laboral.setDireccionCobro(request.getParameter("dir_cob_est") != null ? request.getParameter("dir_cob_est") : "");
                    estudiante.setTelefonoEmpresa(request.getParameter("tel_emp_est") != null ? request.getParameter("tel_emp_est") : "");
                    estudiante.setSalario(request.getParameter("sal_est") != null && !request.getParameter("sal_est").trim().equals("") ? request.getParameter("sal_est").replaceAll(",", "") : "0");
                    personaList.add(persona);

                    for (int i = 0; i < Integer.parseInt(persona.getNumHijos()); i++) {
                        hijo = null;
                        hijo = new SolicitudHijos();
                        hijo.setSecuencia((i + 1) + "");
                        hijo.setNumeroSolicitud("" + num_solicitud);
                        hijo.setTipo("E");
                        hijo.setNombre(request.getParameter("nom_hij_est" + i) != null ? request.getParameter("nom_hij_est" + i) : "");
                        hijo.setEdad(request.getParameter("edad_hij_est" + i) != null && !request.getParameter("edad_hij_est" + i).equals("") ? request.getParameter("edad_hij_est" + i) : "0");
                        hijo.setDireccion(request.getParameter("dir_hij_est" + i) != null ? request.getParameter("dir_hij_est" + i) : "");
                        hijo.setEmail(request.getParameter("mail_hij_est" + i) != null ? request.getParameter("mail_hij_est" + i) : "");
                        hijo.setTelefono(request.getParameter("tel_hij_est" + i) != null ? request.getParameter("tel_hij_est" + i) : "");
                        hijoList.add(hijo);

                    }
                }
                //insertar datos
                if(vista.equals("8"))
                                {
                                    solicitud.setMod_formulario(gsaserv.ValidarCambiosPersonas(personaList,laboralList,bienList,vehiculoList,referenciaList,estudiante,cuentaList,snegocio,ocompraList));
                                }
                boolean ok = gsaserv.insertarDatos(solicitud, referenciaList, cuentaList, laboralList, bienList, vehiculoList, personaList, hijoList, estudiante, negtraza, snegocio, ocompraList);
                if(vista.equals("20"))
                {
                   //aqui vamos a saltar al liquidador si no es el estudiante.
                     next = "/jsp/fenalco/liquidadores/liquidadorNegocios.jsp?tipo_liq=STANDAR&numero_solicitud=" + num_solicitud+"&act_liq=PREA";
                     if(usuario.getPerfil().equals("ESTUDIANTES") ){
                     next = "/jsp/fenalco/liquidadores/prueba.jsp";
                     }
                    
                }
                if (ok) {
                    if (crearneg) {

                        String mensaje = "";
                        Convenio conve = model.gestionConveniosSvc.buscar_convenio("fintra", conv);//Obtener la informacion del convenio
                        est_sol = "P";
                        negtraza.setActividad("SOL");
                        negtraza.setNumeroSolicitud(num_solicitud + "");
                        negtraza.setUsuario(usuario.getLogin());
                        negtraza.setCausal("");
                        negtraza.setComentarios("");
                        negtraza.setDstrct(usuario.getDstrct());
                        negtraza.setConcepto("");
                        Negocios neg = new Negocios();
                        next = "/jsp/fenalco/liquidadores/prueba.jsp";
                        neg.setCod_cli(personaList.get(0).getIdentificacion());
                        neg.setMod_aval("");
                        neg.setMod_cust("");
                        neg.setMod_rem((String) request.getParameter("rem"));
                        neg.setVr_negocio(Double.parseDouble(solicitud.getValorSolicitado()) * Integer.parseInt(solicitud.getPlazo()));
                        neg.setVr_desem(0);
                        neg.setVr_aval(0);
                        neg.setVr_cust(0);
                        neg.setPor_rem(0);
                        neg.setNodocs(Integer.parseInt(solicitud.getPlazo()));
                        neg.setCreation_user(usuario.getLogin());
                        neg.setDist(usuario.getDstrct());
                        neg.setTneg(solicitud.getTipoNegocio());
                        neg.setFpago(solicitud.getPlazoPrCuota());
                        neg.setCod_tabla("0");
                        neg.setEsta("");
                        neg.setFecha_neg(solicitud.getFechaConsulta());
                        neg.setFecha_liquidacion("0099-01-01 00:00:00");
                        neg.setNitp(solicitud.getAfiliado());
                        neg.setTotpagado(0);
                        neg.setCodigoBanco(solicitud.getBanco());
                        neg.setvalor_aval("0");
                        neg.setvalor_remesa("0");
                        neg.setCnd_aval("");
                        neg.setId_convenio(Integer.parseInt(solicitud.getIdConvenio()));
                        neg.setId_remesa(0);
                        neg.setId_sector(solicitud.getSector());
                        neg.setId_subsector(solicitud.getSubsector());
                        neg.setEstado("P");
                        neg.setTasa("0");
                        neg.setPorcentaje_cat(0);
                        neg.setValor_capacitacion(0);
                        neg.setValor_central(0);
                        neg.setValor_seguro(0);
                        neg.setFechatran("0099-01-01 00:00:00");
                        neg.setTipoConv(tipoconv);
                        if (neg.getNodocs() == 1 && neg.getFpago().equals("1")) {
                            neg.setTipoProceso("DSR");
                            //aqui vamos a validar para que el negocio le digiten el titulo valor
                            if (solicitud.getTipoNegocio().equals("01")) {
                                next = "/jsp/fenalco/avales/solicitud_aval_documentos.jsp?num_solicitud=" + num_solicitud + "&vista=" + "2&opcion=1";
                            }
                        } else {
                            neg.setTipoProceso("PSR");
                        }

                        //aqui validamos datacredito si es por fenalco bolivar o atlantico-fintra 
                         boolean consultaDataCredito = false;
                        if (login_into.equals("Fenalco_bol")) {//si es fenalco bolivar 

                            System.out.println("Fenalco Bolivar");
                            consultaDataCredito=gsaserv.aptoCreditoFen_bol(conve, solicitud.getNumeroSolicitud(), usuario,
                                                Integer.parseInt(solicitud.getPlazo()), Double.parseDouble(solicitud.getValorSolicitado()) * Integer.parseInt(solicitud.getPlazo()),
                                                neg.getTipoProceso());

                        } else {//si es fenalco atlantico - fintra

                            System.out.println("Fenalco atlantico");
                            
                           consultaDataCredito= gsaserv.aptoCredito(conve, solicitud.getNumeroSolicitud(), usuario, Integer.parseInt(solicitud.getPlazo()), Double.parseDouble(solicitud.getValorSolicitado()) * Integer.parseInt(solicitud.getPlazo()), neg.getTipoProceso());
                        }
                        //consulta datacredito
                        if (consultaDataCredito) {
                            mensaje = gsaserv.getMensaje();
                            String comentario = gsaserv.getComentario();
                            negtraza.setCausal(gsaserv.getCausal());
                            if (!gsaserv.getEstado_neg().equals("")) {
                                neg.setEstado(gsaserv.getEstado_neg());
                            }
                            if (!neg.getEstado().equals("R")) {

                                neg.setEstado("P");

                                mensaje += "Negocio: Continua Proceso";
                            } else {
                                mensaje += "Negocio: Rechazado";
                            }

                            if (comentario.equals("")) {
                                comentario = mensaje;
                            }
                            negtraza.setComentarios(comentario);
                            negtraza.setConcepto(gsaserv.getResdeudor());
                            try {
                                neg.setCod_negocio(model.Negociossvc.UpCP(conve.getPrefijo_negocio()));//Generar el codigo del negocio
                                model.Negociossvc.listado(neg, null, "", solicitud.getNumeroSolicitud(), conve, negtraza);
                            } catch (Exception e) {
                                 mensaje="error al insertar negocio: " + e.toString();
                                e.printStackTrace();
                            }

                        }

                        if (!mensaje.equals("")) {
                            next += "?mensaje=" + mensaje;
                        }

                    }else{
                      //ojo aqui vamos a redirecionar el negocio caundo sea devuelto a solicitud. 
                      //validar de que instancia ha sido devuelto a solicitud FOR,REF,RAD num_solicitud 
                      
                        NegocioTrazabilidad sw2 = trazaService.buscarDevolverSolTraza(num_solicitud);
                        if(sw2!=null && !botonG.equals("guardar")){
                             next="/jsp/fenalco/liquidadores/prueba.jsp?mensaje=Las modificaciones se han realizado de forma exitosa. ";
                            if (sw2.getConcepto().equals("DEVOLVER SOLICITUD")) {
                                if (sw2.getActividad().equals("RAD")) {
                                    model.Negociossvc.negocioUpdateDS(sw2.getCodNeg(), "LIQ", "P");
                                } else if (sw2.getActividad().equals("REF")) {
                                    model.Negociossvc.negocioUpdateDS(sw2.getCodNeg(), "RAD", "P");
                                } else if (sw2.getActividad().equals("FOR")) {
                                    model.Negociossvc.negocioUpdateDS(sw2.getCodNeg(), "DEC", "V");
                                }
                            }
                        }
                    }
                }

            } else if (opcion.equals("buscarnit")) {
                String nit = request.getParameter("dato") != null ? request.getParameter("dato") : "";
                boolean pasa = true;
                String dato = "";
                SolicitudPersona persona;
                SolicitudLaboral laboral;
                redirect = false;
                try {
                    persona = gsaserv.buscarPersonaId(nit);
                    if (persona != null) {
                        laboral = gsaserv.buscarLaboralId(nit);
                        dato += persona.getPrimerApellido() + ";_;";//0
                        dato += persona.getSegundoApellido() + ";_;";//1
                        dato += persona.getPrimerNombre() + ";_;";//2
                        dato += persona.getSegundoNombre() + ";_;";//3
                        dato += persona.getNombre() + ";_;";//4
                        dato += persona.getGenero() + ";_;";//5
                        dato += persona.getTipoId() + ";_;";//6
                        dato += persona.getIdentificacion() + ";_;";//7
                        dato += persona.getDireccion() + ";_;";//8
                        dato += persona.getTelefono() + ";_;";//9
                        dato += persona.getFechaExpedicionId().substring(0, 10) + ";_;";//10
                        dato += persona.getCiudadExpedicionId() + ";_;";//11
                        dato += persona.getDptoExpedicionId() + ";_;";//12
                        dato += persona.getFechaNacimiento().substring(0, 10) + ";_;";//13
                        dato += persona.getCiudadNacimiento() + ";_;";//14
                        dato += persona.getDptoNacimiento() + ";_;";//15
                        dato += persona.getEstadoCivil() + ";_;";//16
                        dato += persona.getNivelEstudio() + ";_;";//17
                        dato += persona.getProfesion() + ";_;";//18
                        dato += persona.getPersonasACargo() + ";_;";//19
                        dato += persona.getNumHijos() + ";_;";//20
                        dato += persona.getTotalGrupoFamiliar() + ";_;";//21
                        dato += persona.getCiudad() + ";_;";//22
                        dato += persona.getDepartamento() + ";_;";//23
                        dato += persona.getBarrio() + ";_;";//24
                        dato += persona.getEstrato() + ";_;";//52
                        dato += persona.getTipoVivienda() + ";_;";//26
                        String[] tr = new String[2];
                        if (!persona.getTiempoResidencia().equals("")) {
                            tr = persona.getTiempoResidencia().split("A�os");
                            if (tr.length > 1) {
                                tr[1] = tr[1].replaceAll("Meses", "").trim();
                                tr[0] = tr[0].trim();
                            } else {
                               //tr[1] = "";
                            }
                        } else {
                            tr[0] = "";
                            tr[1] = "";
                        }
                        dato += tr[0] + ";_;";//27
                        dato += persona.getCelular() + ";_;";//28
                        dato += persona.getEmail() + ";_;";//29
                        dato += persona.getFax() + ";_;";//30
                        dato += persona.getFechaConstitucion().substring(0, 10) + ";_;";//31
                        dato += persona.getRepresentanteLegal() + ";_;";//32
                        dato += persona.getGeneroRepresentante() + ";_;";//33
                        dato += persona.getTipoIdRepresentante() + ";_;";//34
                        dato += persona.getIdRepresentante() + ";_;";//35
                        dato += persona.getFirmadorCheques() + ";_;";//36
                        dato += persona.getGeneroFirmador() + ";_;";//37
                        dato += persona.getTipoIdFirmador() + ";_;";//38
                        dato += persona.getIdFirmador() + ";_;";//39
                        dato += persona.getCiiu() + ";_;";//40
                        dato += persona.getTipoEmpresa() + ";_;";//41
                        dato += persona.getTelefono2() + ";_;";//42
                        dato += (tr.length > 1 ? tr[1] : "") + ";_;";//43
                        //DATOS DEL CONYUGUE
                        dato += persona.getPrimerApellidoCony() + ";_;";//44
                        dato += persona.getSegundoApellidoCony() + ";_;";//45
                        dato += persona.getPrimerNombreCony() + ";_;";//46
                        dato += persona.getSegundoNombreCony() + ";_;";//47
                        dato += persona.getTipoIdentificacionCony() + ";_;";//48
                        dato += persona.getIdentificacionCony() + ";_;";//49
                        dato += persona.getCelularCony() + ";_;";//50
                        dato += persona.getEmpresaCony() + ";_;";//51
                        dato += persona.getDireccionEmpresaCony() + ";_;";//52
                        dato += persona.getTelefonoCony() + ";_;";//53
                        dato += persona.getCargoCony() + ";_;";//54
                        dato += Util.customFormat(Double.parseDouble(persona.getSalarioCony())) + ";_;";//55
                        dato += persona.getEmailCony() + ";_;";//56
                        //INFOMACION LABORAL Y ECONOMICA
                        if (laboral != null) {
                            String[] rut = new String[2];
                            if (!laboral.getNit().equals("")) {
                                rut = laboral.getNit().split("-");
                                if (!(rut.length > 1)) {
                                    rut[1] = "";
                                }
                            } else {
                                rut[0] = "";
                                rut[1] = "";
                            }
                            dato += laboral.getActividadEconomica() + ";_;";//57
                            dato += laboral.getOcupacion() + ";_;";//58
                            dato += laboral.getNombreEmpresa() + ";_;";//59
                            dato += rut[0] + ";_;";//60
                            dato += rut[1] + ";_;";//61
                            dato += laboral.getDireccion() + ";_;";//62
                            dato += laboral.getDireccionCobro() + ";_;";     //63
                            dato += laboral.getDepartamento() + ";_;";//64
                            dato += laboral.getCiudad() + ";_;";//65
                            dato += laboral.getTelefono() + ";_;";//66
                            dato += laboral.getExtension() + ";_;";//67
                            dato += laboral.getCargo() + ";_;";//68
                            dato += laboral.getFechaIngreso().substring(0, 10) + ";_;";//69
                            dato += laboral.getTipoContrato() + ";_;";//70
                            dato += Util.customFormat(Double.parseDouble(laboral.getSalario())) + ";_;";//71
                            dato += Util.customFormat(Double.parseDouble(laboral.getOtrosIngresos())) + ";_;";//72
                            dato += laboral.getConceptoOtrosIng() + ";_;";//73
                            dato += Util.customFormat(Double.parseDouble(laboral.getGastosManutencion())) + ";_;";//74
                            dato += Util.customFormat(Double.parseDouble(laboral.getGastosCreditos())) + ";_;";//75
                            dato += Util.customFormat(Double.parseDouble(laboral.getGastosArriendo())) + ";_;";//76
                            dato += laboral.getCelular() + ";_;";//77
                            dato += laboral.getEmail() + ";_;";//78
                            dato += laboral.getEps() + ";_;";//79
                            dato += laboral.getTipoAfiliacion() + ";_;";//80
                        }
                    } else {
                        ArrayList<String> info = gsaserv.infoGirador(nit);
                        if (info.isEmpty()) {
                            dato = gsaserv.infoCliente(nit);
                        } else {
                            if (info.get(0).equals("2")) {
                                dato += ";_;";//0
                                dato += ";_;";//1
                                dato += ";_;";//2
                                dato += ";_;";//3
                            } else {
                                dato += this.dividirNombre(info.get(1));
                            }
                            dato += info.get(1) + ";_;";//4
                            if (info.get(2).equals("J")) {
                                dato += ";_;";//5
                            } else {
                                dato += info.get(2) + ";_;";//5
                            }

                            if (info.get(0).equals("1")) {
                                dato += "CED;_;";//6
                            } else if (info.get(0).equals("2")) {
                                dato += "NIT;_;";//6
                            } else if (info.get(0).equals("3")) {
                                dato += "CEX;_;";//6
                            } else {
                                dato += ";_;";//6
                            }

                            dato += info.get(3) + ";_;";//7
                            dato += info.get(4) + ";_;";//8
                            if (!info.get(5).equals("0")) {
                                dato += info.get(5) + ";_;";//9
                            } else {
                                dato += info.get(6) + ";_;";//9
                            }
                            dato += info.get(7) + ";_;";//10
                           
                        }

                    }
                } catch (Exception e) {
                    pasa = false;
                    System.out.println("error en action: " + e.toString());
                    e.printStackTrace();
                }
                   String producto=  request.getParameter("producto")!=null?request.getParameter("producto"):"";
                if (pasa == true && (dato.equals("") == false)) {                                   
                    if (!gsaserv.ValidarSolicitudCliente(nit) && !producto.equals("02")) {
                         cadenaWrite = "El cliente tiene solicitudes en tramite";
                    } else {

                        cadenaWrite = dato;
                    }

                }  else {
                    cadenaWrite = "La informacion digitada no se encuentra registrada";
                }
            } else if (opcion.equals("export_pdf")) {
                boolean pasa = true;
                int num_solicitud = (request.getParameter("num_solicitud") != null && request.getParameter("num_solicitud").equals("") == false)
                        ? Integer.parseInt(request.getParameter("num_solicitud")) : 0;
                try {
                    pasa = gsaserv.exportarPdf(num_solicitud, gsaserv.getLoginuser(),login_into);
                } catch (Exception e) {
                    System.out.println("error en generacion pdf: " + e.toString());
                    e.printStackTrace();
                }
                if (pasa == true) {
                    cadenaWrite = "Archivo generado!\nRevise su directorio.";
                } else {
                    cadenaWrite = "Error en la generacion del archivo";
                }
            } else if (opcion.equals("buscar_cod_afil")) {
                String cod;
                String afil = (request.getParameter("afil") != null) ? request.getParameter("afil") : "";
                try {
                    cod = gsaserv.buscarCodAfils(afil);
                    if (cod.length() == 9) {
                        cadenaWrite = cod.substring(0, 2) + ";_;" + cod.substring(2, 7) + ";_;" + cod.substring(7, 9);
                    } else {
                        cadenaWrite = " ;_; ;_; ";
                    }
                } catch (Exception e) {
                    System.out.println("error en generacion pdf: " + e.toString());
                    e.printStackTrace();
                }

            }
            else if (opcion.equals("cargarmontos")) {
                BeanGeneral montos;
                String convenio = (request.getParameter("convenio") != null) ? request.getParameter("convenio") : "";
                try {
                    montos = model.gestionConveniosSvc.obtenerMontos(convenio);
                    if (montos!=null) {
                        cadenaWrite = montos.getValor_01() + ";_;" + montos.getValor_02() ;
                    } else {
                        cadenaWrite = " ;_; ";
                    }
                } catch (Exception e) {
                    System.out.println("error en generacion pdf: " + e.toString());
                    e.printStackTrace();
                }

            } else if (opcion.equals("busConv")) {
                String nitafil = request.getParameter("nitprov") != null ? request.getParameter("nitprov") : "0";
                ArrayList<BeanGeneral> lista = null;
                BeanGeneral bean = null;
                boolean pasa = true;

                GestionCondicionesService gserv = new GestionCondicionesService(usuario.getBd());
                String cadenatabla = "<div style='overlay: auto;'><table width='100%' style='border-collapse: collapse;'>"
                        + "<thead>"
                        + "<tr class='subtitulo1'>"
                        + "<th>Convenio</th>"
                        + "<th>Sector</th>"
                        + "<th>Subsector</th>"
                        + "</tr>"
                        + "</thead></tbody>";
                try {
                    lista = gserv.conveniosProv(nitafil);
                } catch (Exception e) {
                    pasa = false;
                    System.out.println("Error al buscar datos: " + e.toString());
                }
                if (pasa == true && lista.size() > 0) {
                    for (int i = 0; i < lista.size(); i++) {
                        bean = lista.get(i);
                        cadenatabla += "<tr style='cursor:pointer;' class='filaazul' onclick='colocar(\"" + bean.getValor_02() + "\",\"" + bean.getValor_03() + "\",\"" + bean.getValor_04() + "\",\""
                                + bean.getValor_05() + "\",\"" + bean.getValor_06() + "\",\"" + bean.getValor_07()
                                + "\",\"" + bean.getValor_08() + "\",\"" + bean.getValor_09() + "\",\"" + bean.getValor_10() + "\");cerrarDiv();'>"
                                + "<td> " + bean.getValor_05() + "</td>"
                                + "<td> " + bean.getValor_06() + "</td>"
                                + "<td> " + bean.getValor_07() + "</td>"
                                + "</tr>";
                        bean = null;
                    }
                } else {
                    cadenatabla += "<tr class='filaazul' align='center' style='cursor:pointer;' onclick='cerrarDiv();'>"
                            + "<td colspan='3'>No se encontraron registros</td></tr>";
                }
                cadenatabla += "</tbody></table></div>";
                cadenaWrite = cadenatabla;

            } else if (opcion.equals("busConvUsuario")) {
                String nitafil = request.getParameter("nitprov") != null ? request.getParameter("nitprov") : "0";
                ArrayList<BeanGeneral> lista = null;
                BeanGeneral bean = null;
                boolean pasa = true;

                GestionCondicionesService gserv = new GestionCondicionesService(usuario.getBd());
                String cadenatabla = "<div style='overlay: auto;'><table width='100%' style='border-collapse: collapse;'>"
                        + "<thead>"
                        + "<tr class='subtitulo1'>"
                        + "<th>Convenio</th>"
                        + "<th>Sector</th>"
                        + "<th>Subsector</th>"
                        + "</tr>"
                        + "</thead></tbody>";
                try {
                    lista = gserv.obtenerConveniosProvUsuario(nitafil, usuario.getLogin());
                } catch (Exception e) {
                    pasa = false;
                    System.out.println("Error al buscar datos: " + e.toString());
                }
                if (pasa == true && lista.size() > 0) {
                    for (int i = 0; i < lista.size(); i++) {
                        bean = lista.get(i);
                        cadenatabla += "<tr style='cursor:pointer;' class='filaazul' onclick='colocar(\"" + bean.getValor_02() + "\",\"" + bean.getValor_03() + "\",\"" + bean.getValor_04() + "\",\""
                                + bean.getValor_05() + "\",\"" + bean.getValor_06() + "\",\"" + bean.getValor_07()
                                + "\",\"" + bean.getValor_08() + "\",\"" + bean.getValor_09() + "\",\"" + bean.getValor_10() + "\");cerrarDiv();'>"
                                + "<td> " + bean.getValor_05() + "</td>"
                                + "<td> " + bean.getValor_06() + "</td>"
                                + "<td> " + bean.getValor_07() + "</td>"
                                + "</tr>";
                        bean = null;
                    }
                } else {
                    cadenatabla += "<tr class='filaazul' align='center' style='cursor:pointer;' onclick='cerrarDiv();'>"
                            + "<td colspan='3'>No se encontraron registros</td></tr>";
                }
                cadenatabla += "</tbody></table></div>";
                cadenaWrite = cadenatabla;
            } else if (opcion.equals("buscarTitVlrConv")) {
                String idConvenio = request.getParameter("idConvenio");
                GestionCondicionesService gserv = new GestionCondicionesService(usuario.getBd());
                ArrayList<String> lista = gserv.titulosValorConvenio(idConvenio);
                String strHtml = "<option value='' selected>...</option>";
                if (lista != null && lista.size() > 0) {
                    String row[] = null;
                    for (int i = 0; i < lista.size(); i++) {
                        row = (lista.get(i)).split(";_;");
                        strHtml += "<option value=" + row[0] + ">" + row[1] + "</option>";
                    }
                }
                cadenaWrite = strHtml;

            }else if (opcion.equals("devolver")) {
                String form = request.getParameter("form");
                String comentario = request.getParameter("comentario")!=null?request.getParameter("comentario")+" Titulo(s): ":"Titulo(s): ";
                String negocio=request.getParameter("negocio");
                String[] titulos = request.getParameter("ckestado").split("#");
                int maxdevol=Integer.parseInt(gsaserv.nombreTablagen("INDEMNIZAC", "02"));
                TransaccionService tService = new TransaccionService(usuario.getBd());
                tService.crearStatement();
                boolean sw =true;
                 for (int i = 0; i < titulos.length; i++) {
                     String[] tit = titulos[i].split(";");
                     int devol=Integer.parseInt(tit[1]);
                     if(devol<maxdevol){
                         SolicitudDocumentos doc= new SolicitudDocumentos();
                         doc.setNumeroSolicitud(form);
                         doc.setNumTitulo(tit[0]);
                         doc.setDevuelto(devol+1);
                         doc.setUserUpdate(usuario.getLogin());
                        tService.getSt().addBatch(gsaserv.updateDevueltoDocs(doc));
                        comentario+=tit[0]+", ";
                     }else{
                         sw=false;
                         cadenaWrite += "A el titulo No. "+tit[0]+" no se le pueden realizar mas devoluciones.";
                         i=titulos.length;
                     }

                 }
                if(sw){
                    NegocioTrazabilidad negtraza = new NegocioTrazabilidad();
                    negtraza.setActividad("IND");
                    negtraza.setNumeroSolicitud(form);
                    negtraza.setUsuario(usuario.getLogin());
                    negtraza.setCausal("");
                    negtraza.setComentarios(comentario);
                    negtraza.setDstrct(usuario.getDstrct());
                    negtraza.setConcepto("DEVOLVER");
                    negtraza.setCodNeg(negocio);
                    tService.getSt().addBatch(trazaService.insertNegocioTrazabilidad(negtraza));
                    tService.execute();
                    cadenaWrite  = "/controller?estado=Negocios&accion=Ver&op=5&vista=14";
                }
               
                

            } else if (opcion.equals("indemnizar")) {
                try {
                    ProcesoIndemnizacionServices indemserv = new ProcesoIndemnizacionServices(usuario.getBd());
                    String form = request.getParameter("form");
                    String comentario = request.getParameter("comentario") != null ? request.getParameter("comentario") + " Titulo(s): " : "Titulo(s): ";
                    String negocio = request.getParameter("negocio");
                    String[] titulos = request.getParameter("ckestado").split("#");
                    String fecha = Util.getFechaActual_String(4);
                    String periodo = fecha.substring(0, 7).replaceAll("-", "");
                    TransaccionService tService = new TransaccionService(usuario.getBd());
                    tService.crearStatement();
                    String error = "";
                    boolean sw = true;
                    for (int i = 0; i < titulos.length; i++) {
                        String[] tit = titulos[i].split(";");
                        comentario += tit[0] + ", ";
                        Comprobantes comprobante = (Comprobantes) indemserv.buscarNegocio(negocio, tit[0]);
                        int items = 0;
                        List listitems = new LinkedList();
                        String nit_cliente = comprobante.getTercero();

                        GestionConveniosDAO convenioDao = new GestionConveniosDAO(usuario.getBd());
                        Convenio convenio = convenioDao.buscar_convenio(usuario.getBd(), String.valueOf(comprobante.getId_convenio()));
                        ConvenioCxc tituloValor = convenio.buscarTituloValor(comprobante.getTipoNegocio());

                        comprobante.setFechadoc(fecha);//fecha del comprobante
                        comprobante.setPeriodo(periodo);
                        comprobante.setTipo_operacion("001");
                        comprobante.setUsuario(usuario.getLogin());
                        comprobante.setAprobador(usuario.getLogin());
                        comprobante.setTipo("C");
                        comprobante.setNumdoc(comprobante.getNumdoc() + comprobante.getRef_5());
                        comprobante.setTdoc_rel("NEG");
                        comprobante.setDocrelacionado(comprobante.getNumdoc());

                        comprobante.setTotal_debito(comprobante.getValor());
                        //DEBITO
                        comprobante.setCuenta(tituloValor.getCuenta_prov_cxc());
                        items++;//contar los items
                        listitems.add(indemserv.itemscopy(comprobante, tituloValor.getCuenta_prov_cxp() != null ? tituloValor.getCuenta_prov_cxp() : "", comprobante.getValor(), "D"));//falta agregar a lista de items
                        comprobante.setTercero(comprobante.getRef_1());
                        //CREDITO
                        items++;//contar los items
                        listitems.add(indemserv.itemscopy(comprobante, tituloValor.getCuenta_prov_cxc(), comprobante.getValor(), "C"));//falta agregar a lista de items

                        comprobante.setTotal_credito(comprobante.getValor());

                        comprobante.setTotal_items(items);
                        comprobante.setItems(listitems);
                        error = indemserv.insertar(comprobante, usuario.getLogin());
                        if (!error.substring(0, 5).equals("Error")) {
                            GestionSolicitudAvalService gsaserv = new GestionSolicitudAvalService(usuario.getBd());
                            tService.getSt().addBatch(error);                           
                            String documento = "";
                            BeanGeneral datos = new BeanGeneral();
                            datos.setValor_01(tituloValor.getPrefijo_factura());
                            documento = model.Negociossvc.UpCP(datos.getValor_01());
                            datos.setValor_02(nit_cliente);
                            datos.setValor_03(comprobante.getFecha_creacion());
                            datos.setValor_04(tituloValor.getHc_cxc());
                            datos.setValor_05(usuario.getDstrct());
                            datos.setValor_06(comprobante.getValor() + "");
                            datos.setValor_07(documento + comprobante.getRef_5());
                            datos.setValor_08(usuario.getLogin());
                            String des_cuenta = indemserv.getAccountDES(tituloValor.getCuenta_cxc());
                            datos.setValor_09(tituloValor.getCuenta_cxc());
                            datos.setValor_10(des_cuenta);
                            datos.setValor_11(negocio);
                            datos.setValor_12(usuario.getBase());
                            datos.setValor_13(comprobante.getTipoNegocio());
                            datos.setValor_14(comprobante.getFecha_aplicacion());
                            tService.getSt().addBatch(indemserv.ingresarCXC(datos));

                            tService.getSt().addBatch(indemserv.ingresarDetalleCXC(datos, "1"));
                            datos.setValor_01(convenio.getPrefijo_cxp());
                            documento = model.Negociossvc.UpCP(datos.getValor_01());
                            datos.setValor_02(comprobante.getRef_1());
                            datos.setValor_03(fecha);
                            datos.setValor_04(convenio.getHc_cxp());
                            datos.setValor_07(documento);
                            datos.setValor_09(convenio.getCuenta_cxp());
                            tService.getSt().addBatch(indemserv.ingresarCXP(datos));
                            datos.setValor_11(negocio + comprobante.getRef_5());
                            tService.getSt().addBatch(indemserv.ingresarDetalleCXP(datos));

                            SolicitudDocumentos doc = new SolicitudDocumentos();
                            doc.setNumeroSolicitud(form);
                            doc.setNumTitulo(tit[0]);
                            doc.setEstIndemnizacion("IND");
                            doc.setUserUpdate(usuario.getLogin());
                            tService.getSt().addBatch(gsaserv.updateEstIndDocs(doc));
                            error = "";
                        } else {
                            sw = false;
                            i = titulos.length;
                        }

                    }
                    if (sw) {
                        NegocioTrazabilidad negtraza = new NegocioTrazabilidad();
                        negtraza.setActividad("IND");
                        negtraza.setNumeroSolicitud(form);
                        negtraza.setUsuario(usuario.getLogin());
                        negtraza.setCausal("");
                        negtraza.setComentarios(comentario);
                        negtraza.setDstrct(usuario.getDstrct());
                        negtraza.setConcepto("INDEMNIZAR");
                        negtraza.setCodNeg(negocio);
                        tService.getSt().addBatch(trazaService.insertNegocioTrazabilidad(negtraza));
                        tService.execute();
                        cadenaWrite = "/controller?estado=Negocios&accion=Ver&op=5&vista=14";
                    } else {
                        cadenaWrite = error;
                    }
                    
                } catch (Exception e) {
                    cadenaWrite = e.getMessage();
                }
            }else if (opcion.equals("cargarvias")) {
                try{
                    String codciu = request.getParameter("ciu") != null ? request.getParameter("ciu") : "";
                    String json = gsaserv.cargarVias(codciu);
                    this.printlnResponse(json, "application/json;");
                } catch (Exception e) {
                    cadenaWrite = e.getMessage();
            }

            }else if (opcion.equals("cargarNomenclaturas")) {
                try{
                    String codciu = request.getParameter("ciu") != null ? request.getParameter("ciu") : "";
                    String json = (codciu.equals("")) ? gsaserv.cargarNomenclaturas() : gsaserv.cargarNomenclaturas(codciu);
                    this.printlnResponse(json, "application/json;");
        } catch (Exception e) {
                    cadenaWrite = e.getMessage();
                }               
              
            }else if (opcion.equals("cargarNomenclaturasRel")) {
                try{
                    String codciu = request.getParameter("ciu") != null ? request.getParameter("ciu") : "";
                    String json = gsaserv.cargarNomenclaturasRel(codciu);
                    this.printlnResponse(json, "application/json;");
                } catch (Exception e) {
                    cadenaWrite = e.getMessage();
                }               
              
            }else if (opcion.equals("guardarNomenclatura")) {
                try{
                    String idNomenclatura = request.getParameter("idNomenclatura") != null ? request.getParameter("idNomenclatura") : "";
                    String nombre = request.getParameter("nombre") != null ? request.getParameter("nombre") : "";
                    String descripcion = request.getParameter("descripcion") != null ? request.getParameter("descripcion") : "";
                    String is_default = request.getParameter("is_default") != null ? request.getParameter("is_default") : "";
                    String json = (idNomenclatura.equals("")) ? gsaserv.insertarNomenclatura(nombre,descripcion,is_default,usuario): gsaserv.actualizarNomenclatura(idNomenclatura,nombre,descripcion,is_default,usuario);
                    this.printlnResponse(json, "application/json;");
                } catch (Exception e) {
                    cadenaWrite = e.getMessage();
                }               
              
            }else if (opcion.equals("existeRelNomenclaturaCiudad")) {
                try{
                    String idNomenclatura = request.getParameter("idNomenclatura") != null ? request.getParameter("idNomenclatura") : "";
                    String resp = "{}";
                    if (gsaserv.existeNomenclaturaRelCiudad(Integer.parseInt(idNomenclatura),"")) {
                        resp = "{\"respuesta\":\"SI\"}";
                        this.printlnResponse(resp, "application/json;");
                    } else {
                        resp = "{\"respuesta\":\"NO\"}";
                        this.printlnResponse(resp, "application/json;");
                    }                   
                } catch (Exception e) {
                    cadenaWrite = e.getMessage();
                }              
            }else if (opcion.equals("cambiarEstadoNomenclatura")) {
                try{
                    String idNomenclatura = request.getParameter("idNomenclatura") != null ? request.getParameter("idNomenclatura") : "";
                    String estado_nom = request.getParameter("estadoNom") != null ? request.getParameter("estadoNom") : "";                  
                    String json =  gsaserv.cambiarEstadoNomenclatura(idNomenclatura,estado_nom,usuario);
                    this.printlnResponse(json, "application/json;");
                } catch (Exception e) {
                    cadenaWrite = e.getMessage();
                }               
              
            }else if (opcion.equals("asignarNomenclaturaDefault")) {
                try {
                    String cod_dpto = request.getParameter("dpto") != null ? request.getParameter("dpto") : "";
                    ArrayList listaCiudades = null;
                    ArrayList listadoNomenclaturaDefault = null;
                    listaCiudades = gsaserv.listadoCiudades(cod_dpto);
                    listadoNomenclaturaDefault =  gsaserv.listadoNomenclaturasDefault();
                    String[] dato1 = null;
                    String id_nomenc = "";
                    TransaccionService tService = new TransaccionService(usuario.getBd());
                    tService.crearStatement();
                    for (int i = 0; i < listaCiudades.size(); i++) {
                        dato1 = ((String) listaCiudades.get(i)).split(";_;");                      
                        for (int j = 0; j < listadoNomenclaturaDefault.size(); j++) {
                            id_nomenc = (String) listadoNomenclaturaDefault.get(j);
                            if (!gsaserv.existeNomenclaturaRelCiudad(Integer.parseInt(id_nomenc),dato1[0])) tService.getSt().addBatch(gsaserv.insertarRelNomenclaturaDireccion(id_nomenc,dato1[0],usuario)); 
                        }
                    }
                    tService.execute();
                    String resp = "{\"respuesta\":\"OK\"}";
                    this.printlnResponse(resp, "application/json;");
                } catch (Exception e) {
                    cadenaWrite = e.getMessage();
                }
            }else if (opcion.equals("asociarNomenclatura")) {
                String listado = request.getParameter("listado");
                String codCiudad = request.getParameter("cod_ciu");

                try {
                    JsonParser jsonParser = new JsonParser();
                    JsonObject jo = (JsonObject) jsonParser.parse(listado);
                    JsonArray jsonArr = jo.getAsJsonArray("nomenclaturas");

                    TransaccionService tService = new TransaccionService(usuario.getBd());
                    tService.crearStatement();
                    for (int i = 0; i < jsonArr.size(); i++) {
                        String idNomenclatura = jsonArr.get(i).getAsJsonObject().get("idNomenclatura").getAsJsonPrimitive().getAsString();                                              
                        tService.getSt().addBatch(gsaserv.insertarRelNomenclaturaDireccion(idNomenclatura,codCiudad,usuario));              
                    }
                    tService.execute();
                    String resp = "{\"respuesta\":\"OK\"}";
                    this.printlnResponse(resp, "application/json;");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else if (opcion.equals("desasociarNomenclatura")) {
                try {
                    String codCiudad = request.getParameter("cod_ciu");
                    String listado[] = request.getParameter("listado").split(",");
                    try {
                        TransaccionService tService = new TransaccionService(usuario.getBd());
                        tService.crearStatement();
                        for (int i = 0; i < listado.length; i++) {
                            tService.getSt().addBatch(gsaserv.eliminarRelNomenclaturaDireccion(listado[i]));
                        }
                        tService.execute();
                        String resp = "{\"respuesta\":\"OK\"}";
                        this.printlnResponse(resp, "application/json;");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }else if (opcion.equals("BUSCAR_DATOS_PROVEEDOR")) {
                String json = "{}";
                String entidad = request.getParameter("entidad") != null ? request.getParameter("entidad") : "";
                json = gsaserv.datosProveedorEntidades(entidad, usuario);
                this.printlnResponse(json, "application/json;");
            }else if (opcion.equals("cargarciubarrio")) {
                String codpt = request.getParameter("dept") != null ? request.getParameter("dept") : "";
                String idciu = request.getParameter("ciu") != null ? request.getParameter("ciu") : "";
                String idbarrio = request.getParameter("barrio") != null ? request.getParameter("barrio") : "";
                ArrayList lista = null;
                redirect = false;
                try {
                    lista = gsaserv.listadoCiudades(codpt);
                } catch (Exception e) {
                    System.out.println("error(action): " + e.toString());
                    e.printStackTrace();
                }
                cadenaWrite = "<select id='" + idciu + "' name='" + idciu + "' style='width:20em;' onchange=\"cargarBarrios('" + idciu + "', '" + idbarrio + "'); completarDatosNegocio(this.id)\">";
                cadenaWrite += "<option value=''>...</option>";
                String[] dato1 = null;
                for (int i = 0; i < lista.size(); i++) {
                    dato1 = ((String) lista.get(i)).split(";_;");
                    cadenaWrite += " <option value='" + dato1[0] + "'>" + dato1[1] + "</option>";
                }

                cadenaWrite += "</select>";
            } else if (opcion.equals("cargarbarrio")) {
                String codciu = request.getParameter("ciudad") != null ? request.getParameter("ciudad") : "";
                String idbarrio = request.getParameter("barrio") != null ? request.getParameter("barrio") : "";
                ArrayList lista = null;
                redirect = false;
                try {
                    lista = gsaserv.listadoBarrios(codciu);
                } catch (Exception e) {
                    System.out.println("error(action): " + e.toString());
                    e.printStackTrace();
                }
                cadenaWrite = "<select id='" + idbarrio + "' name='" + idbarrio + "' style='width:20em;'>";
                cadenaWrite += "<option value=''>...</option>";
                String[] dato1 = null;
                for (int i = 0; i < lista.size(); i++) {
                    dato1 = ((String) lista.get(i)).split(";_;");
                    cadenaWrite += " <option value='" + dato1[0] + "'>" + dato1[1] + "</option>";
                }

                cadenaWrite += "</select>";
            }else if (opcion.equals("cargarGridBarrios")) {
                try{
                    String codciu = request.getParameter("ciu") != null ? request.getParameter("ciu") : "";
                    String json = gsaserv.cargarGridBarrios(codciu);
                    this.printlnResponse(json, "application/json;");
                } catch (Exception e) {
                    cadenaWrite = e.getMessage();
                }  
            } else if (opcion.equals("guardarBarrio")) {
                try {
                    String idBarrio = request.getParameter("idBarrio") != null ? request.getParameter("idBarrio") : "";
                    String codciu = request.getParameter("ciu") != null ? request.getParameter("ciu") : "";               
                    String nombre = request.getParameter("nombre") != null ? request.getParameter("nombre").toUpperCase().trim() : "";               
                    String json = (idBarrio.equals("")) ? gsaserv.insertarBarrio(codciu, nombre, usuario) : gsaserv.actualizarBarrio(idBarrio, nombre, usuario);
                    this.printlnResponse(json, "application/json;");
                } catch (Exception e) {
                    cadenaWrite = e.getMessage();
                }

            } else if (opcion.equals("cambiarEstadoBarrio")) {
                try {
                    String idBarrio = request.getParameter("idBarrio") != null ? request.getParameter("idBarrio") : "";
                    String json = gsaserv.cambiarEstadoBarrio(idBarrio, usuario);
                    this.printlnResponse(json, "application/json;");
                } catch (Exception e) {
                    cadenaWrite = e.getMessage();
                }

            } else if (opcion.equals("cargarAserores")) {
                String convenio = request.getParameter("convenio") != null ? request.getParameter("convenio") : "";
                ArrayList lista = null;
                redirect = false;
                try {
                    lista = asesor.cargarAsesoresConvenio(convenio);
                    //this.printlnResponse(json, "application/json;");
                } catch (Exception e) {
                    System.out.println("error(action): " + e.toString());
                    e.printStackTrace();
                    cadenaWrite = e.getMessage();
                }
                cadenaWrite = "<select id='asesor' name='asesor' style='width:20em;'>";
                cadenaWrite += "<option value=''>...</option>";
                String[] dato1 = null;
                for (int i = 0; i < lista.size(); i++) {
                    dato1 = ((String) lista.get(i)).split(";_;");
                    cadenaWrite += " <option value='" + dato1[0] + "'>" + dato1[1] + "</option>";
                }
                cadenaWrite += "</select>";
            }  else if (opcion.equals("buscarPersona")) {
                String id = request.getParameter("documento") != null ? request.getParameter("documento") : "";
                try {
                    String json = gsaserv.buscarInformacionBasicaPersona(id);
                    this.printlnResponse(json, "application/json;");
                } catch (Exception e) {
                    System.err.println("error(action): " + e.toString());
                    cadenaWrite = e.getMessage();
                }
            } else if (opcion.equals("cargarCompraCartera")) {
                String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
                String json = gsaserv.buscarCompraCartera(negocio);
                this.printlnResponse(json, "application/json;");
            } else if (opcion.equals("modificarCompraCartera")) {
                String response, jsonString = "";
                BufferedReader br = request.getReader();
                for (String linea = br.readLine(); linea != null; linea = br.readLine()) {
                    jsonString += linea;
                }
                JsonObject data = (JsonObject) new JsonParser().parse(jsonString);

                if (data.has("negocio") && data.has("secuencia") && data.has("valor") && data.has("nit")) {
                    String negocio = data.get("negocio").getAsString();
                    JsonArray array = data.get("secuencia").getAsJsonArray();
                    int[] secuencia = new int[array.size()];
                    String[] nit = new String[array.size()];
                    String[] cuenta = new String[array.size()];
                    double[] valor = new double[array.size()];

                    for (int i = 0; i < secuencia.length; i++) {
                        secuencia[i] = array.get(i).getAsInt();
                        nit[i] = data.get("nit").getAsJsonArray().get(i).getAsString();
                        cuenta[i] = data.get("cuenta").getAsJsonArray().get(i).getAsString();
                        valor[i] = data.get("valor").getAsJsonArray().get(i).getAsDouble();
                    }
                    response = gsaserv.modificarComprarCartera(negocio, secuencia, nit, cuenta, valor);
                } else {
                    response = "{\"error\": \"Faltan datos por suministrar en la petici�n\"}";
                }

                this.printlnResponse(response, "application/json;");
                
            }else if(opcion.equals("ValidarDireccion")){
                String direccion = request.getParameter("direccion") != null ? request.getParameter("direccion") : "";
                String nit = request.getParameter("nit") != null ? request.getParameter("nit") : "";
               this.printlnResponse(gsaserv.validarDireccionCliente(direccion,nit).toString(), "application/json;");
                
            }
        } catch (Exception e) {
            System.out.println("Error en action gestionsolicitudaval: " + e.toString());            
            e.printStackTrace();
        } finally {
            try {
                if (redirect == false) {
                    this.escribirResponse(cadenaWrite);
                } else {
                    this.dispatchRequest(next);
                }
            } catch (Exception e) {
                System.out.println("Error al redireccionar o escribir respuesta: " + e.toString());
                e.printStackTrace();
            }
        }
    }

    /**
     * Escribe el resultado de la consulta en el response de Ajax
     * @param dato la cadena a escribir
     * @throws Exception cuando hay un error
     */
    protected void escribirResponse(String dato) throws Exception {
        try {
            response.setContentType("text/plain; charset=utf-8");
            response.setHeader("Cache-Control", "no-cache");
            response.getWriter().println(dato);
        } catch (Exception e) {
            throw new Exception("Error al escribir el response: " + e.toString());
        }
    }

    public String dividirNombre(String nombre) throws Exception {
        String[] partes = nombre.split(" ");
        String datos = "";
        if (partes.length > 0) {
            datos += partes[0] + ";_;";//0
            if (partes.length == 2) {
                datos += ";_;";//1
                datos += partes[1] + ";_;";//2
                datos += ";_;";//3
            } else {
                if (partes.length > 2) {
                    datos += partes[1] + ";_;";//1
                    datos += partes[2] + ";_;";//2
                    if (partes.length > 3) {
                        String dato4 = "";
                        for (int i = 3; i < partes.length; i++) {
                            dato4 += partes[i];
                        }
                        datos += dato4 + ";_;";//3
                    } else {
                        datos += ";_;";//3
                    }
                }
            }

        } else {
            datos = nombre;
        }
        return datos;
    }  

    private void printlnResponse(String respuesta, String contentType) throws Exception {

        response.setContentType(contentType + " charset=UTF-8;");
        response.setHeader("Cache-Control", "no-store");
        response.setDateHeader("Expires", 0);
        response.getWriter().println(respuesta);

}
    
    private SolicitudPersona getBeansSolicitudPersona(){
        
        return new  SolicitudPersona("", "", "", "", "", "", "", "", 
                                    "", "", "", "", "", "", "", "", "",
                                    "", "", "", "", "", "", "", "", "",
                                    "", "", "", "", "", "", "", "", "", 
                                    "", "", "", "", "", "", "", "", "", 
                                    "", "", "", "", "", "", "", "", "", 
                                    "", "", "", "", "", "", "", "", "0", 
                                    "", "", "", "");
    
    }    
}
