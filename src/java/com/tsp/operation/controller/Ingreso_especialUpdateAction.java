/*******************************************************************
 * Nombre clase: Ingreso_especialUpdateAction.java
 * Descripci�n: Accion para actualizar un acuerdo especial a la bd.
 * Autor: Ing. Jose de la rosa
 * Fecha: 7 de diciembre de 2005, 08:44 AM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class Ingreso_especialUpdateAction extends Action{
    
    /** Creates a new instance of Ingreso_especialUpdateAction */
    public Ingreso_especialUpdateAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next="/jsp/equipos/ingreso_especial/ingreso_especialModificar.jsp?&reload=ok";
        HttpSession session = request.getSession();
        String dstrct = (String) session.getAttribute ("Distrito");
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String tipo = request.getParameter ("c_tipo_acuerdo").toUpperCase ();
        String codigo = request.getParameter ("c_codigo_concepto").toUpperCase ();
        String clase = request.getParameter ("c_clase_equipo").toUpperCase ();
        float porcentaje = Float.parseFloat (request.getParameter ("c_porcentaje"));
        try{
            Ingreso_especial ingreso = new Ingreso_especial();
            ingreso.setTipo_acuerdo (tipo);
            ingreso.setCodigo_concepto (codigo);
            ingreso.setClase_equipo (clase);
            ingreso.setDistrito (dstrct);
            ingreso.setPorcentaje_ingreso (porcentaje);
            ingreso.setUsuario_modificacion(usuario.getLogin().toUpperCase());
            model.ingreso_especialService.updateIngreso_especial(ingreso);
            request.setAttribute("mensaje","La informaci�n ha sido modificada exitosamente!");
            model.ingreso_especialService.searchIngreso_especial(tipo, codigo, clase, dstrct);
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);          
    }
    
}
