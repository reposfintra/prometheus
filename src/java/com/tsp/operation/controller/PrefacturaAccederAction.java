/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.controller;





import java.util.*;
import java.text.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import com.tsp.util.Util;





/**
 *
 * @author Alvaro
 */
public class PrefacturaAccederAction extends Action {


    public PrefacturaAccederAction() {
    }


    public void run() throws javax.servlet.ServletException {

        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = session.getAttribute("Distrito").toString();

        String evento = ( request.getParameter("evento")!= null) ? request.getParameter("evento") : "";
        String next  = "";
        String msj   = "";
        String aceptarDisable = "";

        try {
            //  EVENTO  :  ACCIONES_SIN_PREFACTURAR de accesoPrefactura.jsp
            //  Arma una lista de las acciones que no se han prefacturado
            if (evento.equalsIgnoreCase("ACCIONES_SIN_PREFACTURAR")) {

                model.applusService.buscaPrefactura(usuario.getNitPropietario());
                List listaPrefactura = model.applusService.getPrefactura();
                if(listaPrefactura.size()!=0) {
                    model.applusService.buscaContratista(usuario.getNitPropietario());

                    model.applusService.buscaImpuestoContrato("IVA","FINV","IVA" );
                    ImpuestoContrato impuesto =  model.applusService.getImpuestoContrato();
                    model.applusService.setImpuesto(impuesto, "IVA");

                    model.applusService.buscaImpuestoContrato("RMAT","FINV","RFTE" );
                    impuesto =  model.applusService.getImpuestoContrato();
                    model.applusService.setImpuesto(impuesto, "RMAT");

                    model.applusService.buscaImpuestoContrato("RMAO","FINV","RFTE" );
                    impuesto =  model.applusService.getImpuestoContrato();
                    model.applusService.setImpuesto(impuesto, "RMAO");

                    model.applusService.buscaImpuestoContrato("ROTR","FINV","RFTE" );
                    impuesto =  model.applusService.getImpuestoContrato();
                    model.applusService.setImpuesto(impuesto, "ROTR");

                    next  = "/jsp/applus/prefactura/prefacturasMarcar.jsp?msj=";
                }
                else {
                    msj   = "El contratista no tiene registros disponibles...";
                    next  = "/jsp/applus/prefactura/accesoPrefactura.jsp?msj=";
                }
            } // Fin de ACCIONES_SIN_PREFACTURAR



            //  EVENTO  :  DETALLE_PREFACTURA  de prefacturasListar.jsp
            //  Arma una lista de detalles de una prefactura
            if (evento.equalsIgnoreCase("DETALLE_PREFACTURA")) { 

                String id_prefactura = ( request.getParameter("id_prefactura")!= null) ? request.getParameter("id_prefactura") : "";

                model.applusService.buscaDetallePrefactura(id_prefactura);
                List listaDetallePrefactura = model.applusService.getDetallePrefactura();
                if(listaDetallePrefactura.size()!=0) {

                    next  = "/jsp/applus/prefactura/prefacturasDetallar.jsp?msj=";
                    msj   = "";
                }
                else {
                    msj   = "La prefactura no contiene items. Favor consulte con soporte de sistemas en FintraValores";
                    next  = "/jsp/applus/prefactura/prefacturasListar.jsp?msj=";
                }
            } // Fin de DETALLE_PREFACTURA



            //  EVENTO  :  EXPORTAR_DETALLE_PREFACTURA de prefacturasDetallar.jsp
            //  Envia una prefactura a exportar a excell
            if (evento.equalsIgnoreCase("EXPORTAR_DETALLE_PREFACTURA")) {

                String prefactura = ( request.getParameter("prefactura")!= null) ? request.getParameter("prefactura") : "";
                HPrefacturaDetalle hilo = new HPrefacturaDetalle();
                hilo.start(model, usuario, prefactura);

                next  = "/jsp/applus/prefactura/prefacturasListar.jsp?msj=";
                msj   = "";

            } // Fin de EXPORTAR_DETALLE_PREFACTURA



            //  EVENTO  :  RESUMEN_PREFACTURA de prefacturasResumir.jsp
            //  Conforma una lista resumida de prefacturas
            if (evento.equalsIgnoreCase("RESUMEN_PREFACTURA")) {

                String fechaInicial   = request.getParameter("fechaInicial");
                String fechaFinal     = request.getParameter("fechaFinal");

                model.applusService.buscaPrefacturaResumen(usuario.getNitPropietario(),fechaInicial,fechaFinal);
                List listaPrefacturaResumen = model.applusService.getPrefacturaResumen();
                model.applusService.buscaImpuestoContrato("RMAT","FINV","RFTE" );

                if(listaPrefacturaResumen.size()!=0) {
                  msj   = "Por favor espere mientras se genera el resumen...";
                  next  = "/jsp/applus/prefactura/prefacturasListar.jsp?msj=";
                }
                else {
                  msj   = "El contratista no tiene prefacturas generadas...";
                  next  = "/jsp/applus/prefactura/prefacturasResumir.jsp?msj=";
                }

            } // Fin de RESUMEN_PREFACTURA



            //  EVENTO  :  CREAR_FACTURA_CONTRATISTA de facturaCrearContratista.jsp
            //  Conforma una lista resumida de prefacturas
            if (evento.equalsIgnoreCase("CREAR_FACTURA_CONTRATISTA")) {

                HFacturaContratista hilo = new HFacturaContratista();
                hilo.start(model, usuario, distrito);

                aceptarDisable = "S";

                next  = "/jsp/applus/prefactura/facturaCrearContratista.jsp?aceptarDisable="+aceptarDisable+"?msj=";
                msj   = "Se ha iniciado el proceso de crear facturas de contratistas. Ver log de proceso para ver su finalizacion";

            } // Fin de CREAR_FACTURA_CONTRATISTA


            //  EVENTO  :  CREAR_FACTURA_APPLUS_RETENCION de facturaCrearRetencion.jsp
            //  Conforma una lista resumida de facturas del contratista para elaborar
            //  facturas a Applus por retencion y bonificacion
            if (evento.equalsIgnoreCase("CREAR_FACTURA_APPLUS_RETENCION")) {


                String fechaInicial   = request.getParameter("fechaInicial");
                String fechaFinal     = request.getParameter("fechaFinal");

                HFacturaApplusRetencion hilo = new HFacturaApplusRetencion();
                hilo.start(model, usuario, distrito,fechaInicial,fechaFinal,"RETENCION");

                aceptarDisable = "S";

                next  = "/jsp/applus/prefactura/facturaCrearRetencion.jsp?aceptarDisable="+aceptarDisable+"?msj=";
                msj   = "Se ha iniciado el proceso de crear facturas de Applus por retencion. Ver log de proceso para ver su finalizacion";

            } // Fin de CREAR_FACTURA_APPLUS_RETENCION




            //  EVENTO  :  CREAR_FACTURA_APPLUS_BONIFICACION de facturaCrearBonificacion.jsp
            //  Conforma una lista resumida de facturas del contratista para elaborar
            //  facturas a Applus por retencion y bonificacion
            if (evento.equalsIgnoreCase("CREAR_FACTURA_APPLUS_BONIFICACION")) {


                String fechaInicial   = request.getParameter("fechaInicial");
                String fechaFinal     = request.getParameter("fechaFinal");

                HFacturaApplusBonificacion hilo = new HFacturaApplusBonificacion();
                hilo.start(model, usuario, distrito,fechaInicial,fechaFinal,"BONIFICACION");

                aceptarDisable = "S";

                next  = "/jsp/applus/prefactura/facturaCrearBonificacion.jsp?aceptarDisable="+aceptarDisable+"?msj=";
                msj   = "Se ha iniciado el proceso de crear facturas de Applus por bonificacion. Ver log de proceso para ver su finalizacion";

            } // Fin de CREAR_FACTURA_APPLUS_BONIFICACION


            //  EVENTO  :  DETALLAR_OFERTA  de ofertaListar.jsp o de ofertaEspecifica.jsp
            //  Arma una lista de detalles de una prefactura
            if (evento.equalsIgnoreCase("DETALLAR_OFERTA")) {

                String opcion = ( request.getParameter("opcion")!= null) ? request.getParameter("opcion") : "";
                String orden = ( request.getParameter("id_orden")!= null) ? request.getParameter("id_orden") : "";

                model.applusService.listaOfertas(model, orden);

                List listaOfertaEcaDetalle = model.applusService.getOfertaEcaDetalle();

                // Calcula el iva de la bonificacion y del iva total_prev1
                if(listaOfertaEcaDetalle.size()!=0) {
                    OfertaEcaDetalle ofertaEcaDetalle = new OfertaEcaDetalle ();
                    double total_prev1 = 0.00;
                    double iva_bonificacion = 0.00;
                    double iva_total_prev1 = 0.00;
                    double iva_utilidad = 0.00;

                    Iterator it = listaOfertaEcaDetalle.iterator();
                    while (it.hasNext()) {
                         ofertaEcaDetalle = (OfertaEcaDetalle)it.next();


                         total_prev1 = ofertaEcaDetalle.getTotal_prev1_mat() + ofertaEcaDetalle.getTotal_prev1_mob() + ofertaEcaDetalle.getTotal_prev1_otr() +
                                       ofertaEcaDetalle.getAdministracion() + ofertaEcaDetalle.getImprevisto() + ofertaEcaDetalle.getUtilidad();

                         // Caso en el que la utilidad es diferente de cero
                         if(ofertaEcaDetalle.getUtilidad() != 0) {

                             // Calculo iva de la bonificacion
                             iva_bonificacion = ( (ofertaEcaDetalle.getUtilidad() * ofertaEcaDetalle.getBonificacion()) / total_prev1 ) * ofertaEcaDetalle.getPorcentaje_iva() ;

                             // Calculo del iva de total_prev1

                             iva_utilidad =  ofertaEcaDetalle.getUtilidad() * ofertaEcaDetalle.getPorcentaje_iva();
                             iva_utilidad =  Util.redondear2(iva_utilidad , 2) ;
                             iva_total_prev1 =iva_utilidad - iva_bonificacion;

                         }
                         // Caso en que la utilidad es cero
                         else {
                             // Calculo iva de la bonificacion
                             iva_bonificacion = ofertaEcaDetalle.getBonificacion() * ofertaEcaDetalle.getPorcentaje_iva()  ;//091229

                             // Calculo del iva de total_prev1
                             iva_total_prev1 =  total_prev1 * ofertaEcaDetalle.getPorcentaje_iva();//091229

                         }
                         iva_bonificacion = Util.redondear2(iva_bonificacion , 2) ;
                         ofertaEcaDetalle.setIvaBonificacion(iva_bonificacion);
                         iva_total_prev1 = Util.redondear2(iva_total_prev1 , 2) ;
                         ofertaEcaDetalle.setIva_total_prev1(iva_total_prev1);



                    }


                }



                if(listaOfertaEcaDetalle.size()!=0) {

                    next  = "/jsp/applus/prefactura/ofertaDetallar.jsp?opcion="+opcion+"&msj=";
                    msj   = "";
                }
                else {
                    msj   = "La orden no contiene acciones. Favor consulte con soporte de sistemas en FintraValores";
                    next  = "/jsp/applus/prefactura/ofertaDetallar.jsp?opcion="+opcion+"&msj=";
                }
            } // Fin de DETALLAR_OFERTA


            //  EVENTO  :  APLICAR_LIQUIDACION  de ofertaDetallar.jsp
            //  Actualiza simbolo variable , observacion , cambia id_estado del negocion para una orden y registra la liquidacion de cada accion
            if (evento.equalsIgnoreCase("APLICAR_LIQUIDACION")) {

               Vector comandos_sql =new Vector();
               java.util.Date fechaActual = new Date();
               String creation_date = fechaActual.toString();

               String comandoSQL = "";


               String opcion = ( request.getParameter("opcion")!= null) ? request.getParameter("opcion") : "";

               // Datos para actualizar el simbolo variable de la orden
               String orden = ( request.getParameter("id_orden")!= null) ? request.getParameter("id_orden") : "";
               String simbolo_variable = ( request.getParameter("simbolo_variable")!= null) ? request.getParameter("simbolo_variable") : "";
               String observacion = ( request.getParameter("observacion")!= null) ? request.getParameter("observacion") : "";
               String fechaFactura = ( request.getParameter("fechaFactura")!= null) ? request.getParameter("fechaFactura") : "";
               String num_os = ( request.getParameter("num_os")!= null) ? request.getParameter("num_os") : "";//090715
               
               int id_orden = Integer.parseInt(orden.trim());

               comandoSQL = model.applusService.setSimboloVariable(simbolo_variable, observacion, 11, id_orden,usuario.getLogin());
               comandos_sql.add(comandoSQL);

               // Datos para actualizar las acciones de la orden

               List listaOfertaEcaDetalle = model.applusService.getOfertaEcaDetalle();
               OfertaEcaDetalle ofertaEcaDetalle = new OfertaEcaDetalle ();

               Iterator it = listaOfertaEcaDetalle.iterator();
               while (it.hasNext()) {
                   ofertaEcaDetalle = (OfertaEcaDetalle)it.next();
                   comandoSQL = model.applusService.setLiquidacionEca(ofertaEcaDetalle);
                   comandos_sql.add(comandoSQL);
               }

               // Actualizando la fecha de la que va a ser la fecha de la factura a eca en tabla app_ofertas

               comandoSQL = model.applusService.setFechaFacturaEca(id_orden, fechaFactura);
               comandos_sql.add(comandoSQL);

               // Grabando todo a la base de datos.
               model.applusService.ejecutarSQL(comandos_sql);
               
               //inicio de 090715
               //inicio de msg
               HSendMail2 hSendMail20=new HSendMail2();
               hSendMail20.start("imorales@fintravalores.com", model.electricaribeVerSvc.getEmailEntidad("APPLUS"), "", "", 
               "en open", 
               "APPLUS:  El multiservicio "+num_os+" ha cambiado su estado a Ingresado en Open"+
                            " . Favor consultar Aplicativo de Consorcio Multiservicios en http://200.30.77.38/consorcio  .",usuario.getLogin()); 

               it = listaOfertaEcaDetalle.iterator();
               while (it.hasNext()) {
                   ofertaEcaDetalle = (OfertaEcaDetalle)it.next();
                   if (!(ofertaEcaDetalle.getId_contratista().equals("CC027"))){
                       hSendMail20=new HSendMail2();
                       hSendMail20.start("imorales@fintravalores.com", model.electricaribeVerSvc.getEmailContratistaApp(ofertaEcaDetalle.getId_contratista()), "", "", 
                       "en open", 
                       ""+model.electricaribeVerSvc.getNombreContratistaApp(ofertaEcaDetalle.getId_contratista())+":  El multiservicio "+num_os+" ha cambiado su estado a Ingresado en Open"+
                                    " . Favor consultar Aplicativo de Consorcio Multiservicios en http://200.30.77.38/consorcio  .",usuario.getLogin());
                   }
               }
               
               //fin de msg
               //fin de 090715

               next  = "/jsp/applus/prefactura/ofertaAcceder.jsp?opcion="+opcion+"&msj=";
               msj   = "";
               
               evento="OFERTAS_POR_FACTURAR";

            } // Fin de APLICAR_LIQUIDACION
            
            
            //  EVENTO  :  OFERTAS_POR_FACTURAR  de ofertaAcceder.jsp
            //  Arma una lista de ofertas
            if (evento.equalsIgnoreCase("OFERTAS_POR_FACTURAR")) {

                model.applusService.buscaOfertaEca();
                List listaOfertaEca = model.applusService.getOfertaEca();

                if(listaOfertaEca.size()!=0) {

                    next  = "/jsp/applus/prefactura/ofertaListar.jsp?msj=";
                    msj   = "";
                }
                else {
                    msj   = "No existen ofertas a listar";
                    next  = "/jsp/applus/prefactura/ofertaAcceder.jsp?msj=";
                }
            } // Fin de OFERTAS_POR_FACTURAR


            //  EVENTO  :  RELIQUIDAR_PORCENTAJE  de ofertaDetallar.jsp
            //  Actualiza liquidacion con los nuevos porcentajes
            if (evento.equalsIgnoreCase("RELIQUIDAR_PORCENTAJE")) {

               String opcion = ( request.getParameter("opcion")!= null) ? request.getParameter("opcion") : "";
               String orden = ( request.getParameter("id_orden")!= null) ? request.getParameter("id_orden") : "";

               String porcentaje_comision_applus = ( request.getParameter("porcentaje_comision_applus")!= null) ? request.getParameter("porcentaje_comision_applus") : "";
               String porcentaje_factoring_fintra = ( request.getParameter("porcentaje_factoring_fintra")!= null) ? request.getParameter("porcentaje_factoring_fintra") : "";
               String porcentaje_comision_provintegral = ( request.getParameter("porcentaje_comision_provintegral")!= null) ? request.getParameter("porcentaje_comision_provintegral") : "";
               String porcentaje_comision_fintra = ( request.getParameter("porcentaje_comision_fintra")!= null) ? request.getParameter("porcentaje_comision_fintra") : "";
               String porcentaje_comision_eca = ( request.getParameter("porcentaje_comision_eca")!= null) ? request.getParameter("porcentaje_comision_eca") : "";
               String porcentaje_iva = ( request.getParameter("porcentaje_iva")!= null) ? request.getParameter("porcentaje_iva") : "";
               String puntos_dtf = ( request.getParameter("puntos_dtf")!= null) ? request.getParameter("puntos_dtf") : "";

               // Recalcular la liquidacion Eca basada en los nuevos porcentajes

               model.applusService.setReliquidarOfertas(model, orden,porcentaje_comision_applus,porcentaje_comision_provintegral,
                                                        porcentaje_comision_fintra,porcentaje_comision_eca,porcentaje_iva,porcentaje_factoring_fintra,
                                                        puntos_dtf);

               // Fin de la recalculacion


               // Pasar parametros para mostrar nuevamente ofertaDetallar.jsp

               next  = "/jsp/applus/prefactura/ofertaDetallar.jsp?opcion=ESPECIFICA&msj=";
               msj   = "";

               // En este caso ofertaDetallar.jsp debe mostrar 3 botones Aplicar (con este boton se debe grabar) , Activar ( con este boton se debe reliquidar), Salir


            } // Fin de RELIQUIDAR_PORCENTAJE



            //  EVENTO  :  APLICAR PORCENTAJE  de ofertaDetallar.jsp
            //  Actualiza simbolo variable , observacion , cambia id_estado del negocion para una orden y registra la liquidacion de cada accion
            if (evento.equalsIgnoreCase("APLICAR PORCENTAJE")) {

               Vector comandos_sql =new Vector();
               java.util.Date fechaActual = new Date();
               String creation_date = fechaActual.toString();

               String comandoSQL = "";


               String opcion = ( request.getParameter("opcion")!= null) ? request.getParameter("opcion") : "";

               // Datos para actualizar el simbolo variable de la orden
               String orden = ( request.getParameter("id_orden")!= null) ? request.getParameter("id_orden") : "";
               String simbolo_variable = ( request.getParameter("simbolo_variable")!= null) ? request.getParameter("simbolo_variable") : "";
               String observacion = ( request.getParameter("observacion")!= null) ? request.getParameter("observacion") : "";

               int id_orden = Integer.parseInt(orden);

               comandoSQL = model.applusService.setSimboloVariable(simbolo_variable, observacion, 11, id_orden,usuario.getLogin());
               comandos_sql.add(comandoSQL);

               // Datos para actualizar las acciones de la orden

               List listaOfertaEcaDetalle = model.applusService.getOfertaEcaDetalle();
               OfertaEcaDetalle ofertaEcaDetalle = new OfertaEcaDetalle ();

               Iterator it = listaOfertaEcaDetalle.iterator();
               while (it.hasNext()) {

                   ofertaEcaDetalle = (OfertaEcaDetalle)it.next();
                   comandoSQL = model.applusService.setLiquidacionEca(ofertaEcaDetalle);
                   comandos_sql.add(comandoSQL);


               }



               // Grabando todo a la base de datos.
               model.applusService.ejecutarSQL(comandos_sql);


               next  = "/jsp/applus/prefactura/ofertaEspecifica.jsp?opcion="+opcion+"&msj=";
               msj   = "";

            } // Fin de APLICAR PORCENTAJE



            //  EVENTO  :  GENERAR_FACTURAR_ECA de facturaCrearEca.jsp
            //  Envia una prefactura a exportar a excell
            if (evento.equalsIgnoreCase("GENERAR_FACTURAR_ECA")) {


                HGenerarFacturaEca hilo =  new HGenerarFacturaEca();
                hilo.start(model, usuario);

                aceptarDisable = "S";
                msj   = "La creacion de las facturas CxC a Eca se ha iniciado...Ver log de proceso para ver su finalizacion";
                next  = "/jsp/applus/prefactura/facturaCrearEca.jsp?aceptarDisable="+aceptarDisable+"?msj=";

            } // Fin de GENERAR_FACTURAR_ECA




            //  EVENTO  :  GENERAR_FACTURA_APP de facturaCrearApp.jsp
            //  Envia una prefactura a exportar a excell
            if (evento.equalsIgnoreCase("GENERAR_FACTURA_APP")) {


                HGenerarFacturaApp hilo =  new HGenerarFacturaApp();
                hilo.start(model, usuario,distrito);

                aceptarDisable = "S";
                msj   = "La creacion de las facturas CxP a Applus se ha iniciado...Ver log de proceso para ver su finalizacion";
                next  = "/jsp/applus/prefactura/facturaCrearApp.jsp?aceptarDisable="+aceptarDisable+"?msj=";

            } // Fin de GENERAR_FACTURA_APP


            //  EVENTO  :  GENERAR_FACTURA_PRO de facturaCrearPro.jsp
            //  Envia una prefactura a exportar a excell
            if (evento.equalsIgnoreCase("GENERAR_FACTURA_PRO")) {


                HGenerarFacturaPro hilo =  new HGenerarFacturaPro();
                hilo.start(model, usuario,distrito);

                aceptarDisable = "S";
                msj   = "La creacion de las facturas CxP a Provintegral se ha iniciado...Ver log de proceso para ver su finalizacion";
                next  = "/jsp/applus/prefactura/facturaCrearPro.jsp?aceptarDisable="+aceptarDisable+"?msj=";

            } // Fin de GENERAR_FACTURA_PRO



            //  EVENTO  :  GENERAR_FACTURA_COMISION_ECA de facturaCrearComisionEca.jsp
            //  Envia una prefactura a exportar a excell
            if (evento.equalsIgnoreCase("GENERAR_FACTURA_COMISION_ECA")) {


                HGenerarFacturaComisionEca hilo =  new HGenerarFacturaComisionEca();
                hilo.start(model, usuario,distrito) ;

                aceptarDisable = "S";
                msj   = "La creacion de las facturas CxP por comision a Eca se ha iniciado...Ver log de proceso para ver su finalizacion";
                next  = "/jsp/applus/prefactura/facturaCrearEca.jsp?aceptarDisable="+aceptarDisable+"?msj=";

            } // Fin de GENERAR_FACTURA_COMISION_ECA





            next += msj ;
            this.dispatchRequest(next);

        }catch (Exception e) {
            e.printStackTrace();
        }

    }












}
