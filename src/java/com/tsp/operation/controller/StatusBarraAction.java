    /***************************************
    * Nombre Clase ............. StatusBarraAction.java
    * Descripci�n  .. . . . . .  Maneja los eventos para la barra de status
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  23/08/2006
    * versi�n . . . . . . . . .  1.0
    * Copyright ...Transportes Sanchez Polo S.A.
    *******************************************/



package com.tsp.operation.controller;



import java.io.*;
import java.util.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import javax.servlet.*;
import javax.servlet.http.*;



public class StatusBarraAction extends Action{
    
   
    
    public void run() throws ServletException, InformationException {
        
        
         try{
                HttpSession session     = request.getSession();
                Usuario     usuario     = (Usuario)session.getAttribute("Usuario");
                String      user        = usuario.getLogin(); 
                String      distrito    = usuario.getDstrct();
                
                String      proveedor   =  model.AnticiposPagosTercerosSvc.getProveedorUser(user);
                
             // Anticipos:
                model.StatusBarraSvc.ANTICIPOS_Aprobar     ( distrito, proveedor );
                model.StatusBarraSvc.ANTICIPOS_Transferir  ( distrito, proveedor );
                
             // Liquidaciones:
                model.StatusBarraSvc.LIQUIDACION_Aprobar   ( );
                model.StatusBarraSvc.LIQUIDACION_Migrar    ( );
                
                
                
                String      next     = "/jsp/finanzas/fintra/Status.jsp";
                RequestDispatcher rd = application.getRequestDispatcher(next);
                if(rd == null)
                   throw new Exception("No se pudo encontrar "+ next);
                rd.forward(request, response); 
                
            
        } catch (Exception e){
             throw new ServletException(e.getMessage());
        }
        
        
    }
    
}
