/******************************************************************
 * Nombre ......................ConsultaUConsultaAction.java
 * Descripci�n..................Clase Action para generar la consulta del reporte de consulta
 * Autor........................David lamadrid
 * Fecha........................21/12/2005
 * Versi�n......................1.0
 * Coyright.....................Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  dlamadrid
 */
public class ConsultaUConsultaAction extends Action {
    
    /** Creates a new instance of ConsultaUConsultaAction */
    public ConsultaUConsultaAction () {
    }
    
    public void run () throws ServletException, InformationException {
        String next="";
        try {
            String opcion = ""+request.getParameter ("opcion");
            Vector campos = new Vector ();
            
            String[] camposS = request.getParameterValues ("cselect");
            
            if(camposS!=null) {
                for( int ii=0 ;ii< camposS.length; ii++) {
                    campos.add (camposS[ii]);
                }
            }
           
            String select=""+request.getParameter ("select");
            //System.out.println("select "+select);
            String from =""+request.getParameter ("from");
            //System.out.println("from "+from);
            String where = ""+request.getParameter ("where");
            //System.out.println("where "+where);
            String otros = ""+request.getParameter ("otros");
            if(where.equals ("null")) {
                where="";
            }
            if(!where.equals ("")){
                if(!(where.indexOf ("where")>=0 ||where.indexOf ("WHERE")>=0)) {
                    where= "where "+where;
                }
            }
            if(otros.equals ("null")) {
                otros="";
            }
            
            String consulta="";
            consulta = select +" " +  from +" "+ where + " " +otros;
            //System.out.println("consulta "+consulta);
            
            Vector vTitulos= new Vector ();
            vTitulos = model.consultaUsuarioService.encabesado (campos);
            model.consultaUsuarioService.setVTitulos (vTitulos);
            
            String ms="";
            boolean valido= model.consultaUsuarioService.existeConsulta (consulta);
            if(valido == true) {
                
                model.consultaUsuarioService.generarConsulta (campos, consulta);
                if(opcion.equals ("2")) {
                    HttpSession session = request.getSession ();
                    Usuario usuario = (Usuario)session.getAttribute ("Usuario");
                    String user = usuario.getLogin ();
                    String agencia = usuario.getDstrct ();
                    Calendar fechaHoy = Calendar.getInstance ();
                    java.util.Date fech = fechaHoy.getTime ();
                    SimpleDateFormat fec = new SimpleDateFormat ("yyyy-MM-dd");
                    String Fecha = fec.format (fech);
                    ReporteConsultasXSL hilo = new ReporteConsultasXSL ();
                    next="/jsp/general/consultas/MensajeReporteCumplido.jsp";
                    next += "?msg=Su petici�n a sido procesada"+hilo.getMensaje ();
                    Vector vConsulta= model.consultaUsuarioService.getVConuslta ();
                    Vector vCampos =model.consultaUsuarioService.getVTitulos ();
                    hilo.start (vConsulta,vCampos,user,Fecha);
                }
                else{
                    next="/jsp/general/consultas/consulta.jsp?accion=1";
                }
            }
            else {
                ms="Consulta invalida";
                next="/jsp/general/consultas/insertarconsultas.jsp?accion=1&ms="+ms;
            }
        }
        catch (Exception e) {
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
    
}
