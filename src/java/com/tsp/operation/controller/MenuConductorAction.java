
package com.tsp.operation.controller;

import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;
import java.io.*;
import java.util.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;

public class MenuConductorAction extends Action{
    
    public void run() throws javax.servlet.ServletException, InformationException {
      try{
         HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
         String login=usuario.getLogin();
         if(!login.equals("") ){
          //--- variables de bloqueos       
           String bloqueoInsert="";
           String bloqueoSearch="";
           String bloqueoUpdate="disabled='disabled'";
           String bloqueoDelete="disabled='disabled'";
           String botones="";
           model.conductorService.reset();
        // LLamamos la pagina JSP
           final String next = "/conductor/Conductor.jsp?estadoInsert="+ bloqueoInsert +"&estadoSearch="+bloqueoSearch+"&estadoDelete="+ bloqueoDelete+"&estadoUpdate="+bloqueoUpdate +"&botones="+botones;
           RequestDispatcher rd = application.getRequestDispatcher(next);
           if(rd == null)
              throw new Exception("No se pudo encontrar "+ next);
           rd.forward(request, response); 
           
        }
        }
        catch(Exception e){ throw new InformationException(e.getMessage());} 
    } 
}
