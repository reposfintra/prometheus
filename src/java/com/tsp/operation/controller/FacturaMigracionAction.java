/*
 * Nombre        FacturaMigracionAction.java
 * Autor         Osvaldo P�rez Ferrer
 * Fecha         25 de noviembre de 2006, 05:47 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import com.tsp.exceptions.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.Util;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.threads.HMigrarFacturaMigracion;


public class FacturaMigracionAction extends Action {
    
    public String campos = "";
    String[] titulos;
    String[] tipos;
    String formato = "";
    String nombre_formato = "";
    
    /**
     * Crea una nueva instancia de  FacturaMigracionAction
     */
    public FacturaMigracionAction(){
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        
        HttpSession session   = request.getSession();
        Usuario     usuario   = (Usuario) session.getAttribute("Usuario");
        
        
        String next           = "/jsp/cxpagar/factura_migracion/";
        String evento         = request.getParameter("evento");
        
        try{
            
            
            if ( evento!=null ){
                
                
                if( evento.equals("LOAD") ){
                    model.formato_tablaService.obtenerFormatosTablas( usuario.getDstrct() );                   
                    next += "filtro_formato_tabla.jsp?opcion=";
                }
                
                if( evento.equals("FILTERS") ){
                    
                    formato = request.getParameter("formato");
                    
                    String[] split = formato.split("-_-");
                    
                    formato = split[0];
                    nombre_formato = split[1];
                    
                    model.formato_tablaService.obtenerCamposTabla( usuario.getDstrct(), formato );
                    
                    next += "filtro_formato_tabla.jsp?opcion=filtros&formato="+formato;
                }
                
                if( evento.equals("SEARCH") ){
                                        
                    String where = this.buildWhere( request );
                    //System.out.println(" Action,SEARCH = "+campos);
                    model.formato_tablaService.obtenerDatosTabla( usuario.getDstrct(), where, campos );
                    model.formato_tablaService.setTitulos(titulos);
                    model.formato_tablaService.setCampos(campos.split(",") );
                    
                    next += "listado.jsp?nombre_formato="+nombre_formato;
                    
                }
                
                if( evento.equals("FACTURAR") ){
                    
                    Vector v = model.formato_tablaService.getDatos();
                    String items = request.getParameter("items");
                                    
                    int tam = v.size();
                    for( int i=0; i< tam; i++ ){
                        Hashtable h = (Hashtable)v.get(i);
                        String id = (String) h.get("id");
                        
                        //Se remueven los NO seleccionados
                        if( items.indexOf( id ) == -1 ){                            
                            v.remove(i);
                            i--;
                            tam--;
                        }
                    }   
                    
                    String moneda = model.ChequeXFacturaSvc.getMonedaLocal( usuario.getDstrct() );
                    //String mens = model.formato_tablaService.insert( v, formato, usuario, moneda );
                    
                    HMigrarFacturaMigracion h = new HMigrarFacturaMigracion();
                    h.start(usuario, formato, moneda, v, model, nombre_formato);
                    
                    next += "listado.jsp?mensaje=Su proceso ha iniciado...";
                    //System.out.println( "FacturaMigracionAction "+mens );
                }
            }
            
            RequestDispatcher rd = application.getRequestDispatcher(next);
            if(rd == null)
                throw new Exception("No se pudo encontrar "+ next);
            rd.forward(request, response);
            
            
        } catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        
    }
    
    public String buildWhere( HttpServletRequest rq ){
        
        String formato    = rq.getParameter("formato");
        Vector vec_campos = model.formato_tablaService.getVector();
        String where      = "";
        String titulos    = ""; 
        String tipos      = "";
        
        where += " formato = '"+formato+"'";
        campos = "";
        for( int i=0; i<vec_campos.size(); i++ ){
            
            Hashtable h = (Hashtable)vec_campos.get(i);
            
            String pk    = h.get("pk")!=null? (String)h.get("pk") : "";
            String tipo  = h.get("tipo")!=null? (String)h.get("tipo") : "";
                        
            String campo = (String)h.get("campo_tabla");
            titulos     += (String)h.get("campo_jsp")+",";
            tipos       += tipo+",";
            
            campos  += campo+", ";
           
            //Si el campo es llave primaria, se agrega como filtro para la consulta
            if( pk.equals("S") ){
                
                String param = rq.getParameter( campo );
                if( tipo.equals("DO") || tipo.equals("INT") ){
                    //Si el parametro viene vacio, no se agrega como filtro
                    if( !param.equals("") ){
                        where +=   " AND "+campo+" = "+ param;
                    }
                }else{
                    if( !param.equals("") ){
                        where += " AND "+campo+" = '"+ param +"'";
                    }
                }
            }
            
        }
        
        this.titulos = titulos.split(",");
        this.tipos   = tipos.split(",");
        //Se remueve la ultima coma
        campos = campos.length()>3? campos.substring(0, (campos.length() - 2)  ) : "";
        
        where += " AND creation_date::date BETWEEN '"+rq.getParameter( "inicial" )+"' AND '"+rq.getParameter( "final" )+"'";
        
        //System.out.println(where);
        return where;
    }
    
}
