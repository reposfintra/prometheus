 /********************************************************************
 *      Nombre Clase.................   ConsultaDemoraCaravanaTramo.java    
 *      Descripci�n..................   Consulta las demoras de un un tramo.
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   31.08.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;
import com.tsp.exceptions.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;


public class ConsultarDemoraTramoAction extends Action{
    
    /** Creates a new instance of ConsultarDemoraTramoAction */
    public ConsultarDemoraTramoAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String next = "/jsp/trafico/demora/consultarDemoraTramoParam.jsp";
        HttpSession session = request.getSession();
        
        /*try{
            Vector planillas = model.demorasSvc.planillasTramo();
            Vector dems = new Vector();
            for(int i=0; i<planillas.size(); i++){
                String numpla = (String) planillas.elementAt(i);
                Demora dem = model.demorasSvc.obtenerDemora(numpla);
                if(dem!=null && dem.getFinalizada().matches("no"))
                    dems.add(dem);
            }
            if ( dems.size()!=0)
                session.setAttribute("Demoras",  dems);
            else
                next = "/jsp/trafico/demora/consultarDemoraCaravana.jsp?msg=" +
                    "No se encuentranb planillas asociadas al numero de la caravana en el archivo de demoras.";
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }*/
        this.dispatchRequest(next);        
    }
    
}
