/*
 * Nombre        TramoIngresarAction.java
 * Autor         Ing Jesus Cuestas
 * Fecha         26 de junio de 2005, 10:12 PM
 * Versi�n       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */


package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class TramoIngresarAction extends Action{
    
    /** Creates a new instance of TramoIngresarAction */
    public TramoIngresarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String Mensaje  = "";
        String next = "/jsp/trafico/tramo/ingresarTramo.jsp";
        
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        String dstrct = (String) session.getAttribute("Distrito");
        
        String origen = request.getParameter("c_origen");
        String destino = request.getParameter("c_destino");
        String consumo = (!request.getParameter("c_comb").equals(""))?request.getParameter("c_comb"):"0";
        String uconsumo = request.getParameter("c_unidadc");
        String distancia = request.getParameter("c_distancia");
        String udistancia =request.getParameter("c_unidadd");
        String duracion = request.getParameter("c_duracion");
        String uduracion = request.getParameter("c_unidaddu");
        String paiso =request.getParameter("c_paiso");
        String paisd =request.getParameter("c_paisd");
        
        Tramo t = new Tramo();
        t.setDstrct(dstrct);
        t.setOrigen(origen);
        t.setDestino(destino);
        t.setConsumo_combustible(Float.parseFloat(consumo));
        t.setUnidad_consumo(uconsumo);
        t.setDistancia(Float.parseFloat(distancia));
        t.setUnidad_distancia(udistancia);
        t.setDuracion(Float.parseFloat(duracion));
        t.setUnidad_duracion(uduracion);
        t.setPaiso(paiso);
        t.setPaisd(paisd);
        t.setUsuario(usuario.getLogin());
        t.setBase(usuario.getBase());
        
        try{
            model.tramoService.setTramo(t);
            model.tramoService.agregarTramo();
            Mensaje = "El Tramo fue ingresado con exito!";
            model.ciudadService.setCiudadO(null);
            model.ciudadService.setCiudadD(null);
        }catch (SQLException e){
            e.printStackTrace();
            try{
                if(model.tramoService.existeTramoAnulado(dstrct, origen, destino)){
                    model.tramoService.modificarTramo();
                    Mensaje = "El Tramo fue ingresado con exito!";
                }
                else{
                    Mensaje = "El Tramo ya existe!&sw=1";
                    model.ciudadservice.listarCiudadesxpais(request.getParameter("c_paiso"));
                    model.ciudadService.setCiudadO(model.ciudadservice.obtenerCiudades());
                    //request.setAttribute("ciudadO", model.ciudadservice.obtenerCiudades());
                    List co = model.ciudadService.obtenerCiudades();            
                    model.ciudadservice.listarCiudadesxpais(request.getParameter("c_paisd"));
                    model.ciudadService.setCiudadD(model.ciudadservice.obtenerCiudades());
                    //request.setAttribute("ciudadD", model.ciudadservice.obtenerCiudades());
                }
            }catch (SQLException a){
                throw new ServletException(e.getMessage());
            }
        }
        next += "?Mensaje="+Mensaje; 
        this.dispatchRequest(next);
    }
}
