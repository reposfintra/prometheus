/************************************************************************
 * Nombre clase: DiscrepanciaCierre_productoAction.java
 * Descripci�n: Accion para realizar listados de discrepancia con cierre o sin cierre.
 * Autor: Jose de la rosa
 * Fecha: 4 de noviembre de 2005, 02:49 PM
 * Versi�n: Java 1.5.0
 * Copyright: Fintravalores S.A. S.A.
 **************************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;

/**
 *
 * @author  Jose
 */
public class Discrepancia_listarCierreAction extends Action {
    
    /** Creates a new instance of DiscrepanciaCierre_productoAction */
    public Discrepancia_listarCierreAction () {
    }
    public void run () throws ServletException, com.tsp.exceptions.InformationException {
        String next="";
        HttpSession session = request.getSession ();
        String distrito = (String) session.getAttribute ("Distrito");
        Usuario usuario = (Usuario) session.getAttribute ("Usuario");
        String numrem = (String) request.getParameter ("c_numrem");
        String cod_producto = (String) request.getParameter ("c_cod_producto");
        String numpla = (String) request.getParameter ("c_numpla");
        String fecha_creacion = (String) request.getParameter ("c_fecha_creacion");
        String tipo_doc = (String) request.getParameter ("c_tipo_doc");
        String documento = (String) request.getParameter ("c_documento");
        String num_discre = (String) request.getParameter ("c_num_discre");
        String tipo_doc_rel = (String) request.getParameter ("c_tipo_doc_rel");
        String documento_rel = (String) request.getParameter ("c_documento_rel");
        try{
            next="/jsp/cumplidos/discrepancia/discrepanciaConsultaCierre.jsp?c_num_discre="+num_discre+"&c_documento="+documento+"&c_tipo_doc="+tipo_doc+"&c_numrem="+numrem+"&sw=true&campos=false&c_numpla="+numpla+"&exis=false&campos=false&paso=false&c_tipo_doc_rel="+tipo_doc_rel+"&c_documento_rel="+documento_rel;
            model.planillaService.obtenerInformacionPlanilla2 (numpla);
            model.planillaService.obtenerNombrePropietario2 (numpla);
            model.planillaService.consultaPlanillaRemesaDiscrepancia(numpla, numrem, tipo_doc, documento, Integer.parseInt (num_discre) );
            model.discrepanciaService.searchDiscrepancia(numpla, numrem, tipo_doc, documento, tipo_doc_rel, documento_rel,distrito );
            model.discrepanciaService.listDiscrepanciaProducto(Integer.parseInt (num_discre) ,numpla,numrem, tipo_doc,documento,tipo_doc_rel,documento_rel,distrito);
            Discrepancia d = model.discrepanciaService.getDiscrepancia ();
            model.ubService.buscarUbicacion (d.getUbicacion (), usuario.getCia ());
        }catch (SQLException e) {
            throw new ServletException (e.getMessage ());
        }
        this.dispatchRequest (next);
    }
}
