/***************************************************************************
 * Nombre clase : ............... RemesaSinDocAction.java                  *
 * Descripcion :................. Clase que maneja los eventos             *
 *                                relacionados con el programa de          *
 *                                Reporte de viajes                        *
 * Autor :....................... Ing. Juan Manuel Escandon Perez          *
 * Fecha :........................ 18 de noviembre de 2005, 01:06 PM      *
 * Version :...................... 1.0                                     *
 * Copyright :.................... Fintravalores S.A.                 *
 ***************************************************************************/
package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.lang.*;
import java.util.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.util.*;
import com.tsp.operation.model.threads.*;
public class RemesaSinDocAction extends Action {
    
    /** Creates a new instance of RemesaSinDocAction */
    public RemesaSinDocAction() {
    }
    
    public void run() throws ServletException, com.tsp.exceptions.InformationException {
        try{
            String next = "";
            
            String opcion                 = request.getParameter("Opcion");
            String agencia                = (request.getParameter("agencia")!=null) ? request.getParameter("agencia") : "";
            String fechai                 = (request.getParameter("finicial")!=null) ? request.getParameter("finicial") : "";
            String fechaf                 = (request.getParameter("ffinal")!=null) ? request.getParameter("ffinal") : "";
            String usuario                = (request.getParameter("usuario")!=null) ? request.getParameter("usuario") : "";
            String numrem                 = (request.getParameter("numrem")!=null) ? request.getParameter("numrem") : "";
            
            HttpSession Session           = request.getSession();
            Usuario user                  = new Usuario();
            user                          = (Usuario)Session.getAttribute("Usuario");
            
            
            String Mensaje = "";
            
            usuario                     =  (usuario.equals("")) ? "%" : usuario;
            
            if(opcion.equals("Generar")){
                model.RemesaSinDocSvc.listRemesasSinDoc(agencia, usuario, fechai, fechaf);
                next = "/jsp/masivo/reportes/RemesaSinDoc.jsp?fechai="+fechai+"&fechaf="+fechaf;
            }
            
            if(opcion.equals("Excel")){
                RemesaSinDocThread hilo = new RemesaSinDocThread();
                hilo.start(model.RemesaSinDocSvc.getListRSD(), fechai, fechaf, user.getLogin());
                Mensaje = "El proceso se ha iniciado exitosamente";
                next = "/jsp/masivo/reportes/RemesaSinDoc.jsp?Mensaje="+Mensaje;
                
            }
            
            
            if(opcion.equals("Destinatarios")){
                model.remesaService.datosRemesa(numrem);
                Remesa rem = model.remesaService.getRemesa();
                String std = rem.getStdJobNo();
                next = "/jsp/masivo/reportes/destinatarios.jsp?sj="+std+"&numrem="+numrem+"&modif=ok";
                RequestDispatcher r = application.getRequestDispatcher(next);
                
                if(r == null)
                    throw new ServletException("No se pudo encontrar "+ next);
                r.forward(request, response);
            }
            
            if( opcion.equals("Generar") || opcion.equals("Excel")){
                RequestDispatcher rd = application.getRequestDispatcher(next);
                if(rd == null)
                    throw new ServletException("No se pudo encontrar "+ next);
                rd.forward(request, response);
            }
            
            
            
            
        }catch(Exception e){
            e.printStackTrace();
            throw new ServletException("Error en RemesaSinDocAction .....\n"+e.getMessage());
        }
    }
}
