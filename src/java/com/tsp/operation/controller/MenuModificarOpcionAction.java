/*
 * Nombre        MenuModificarOpcionAction.java
 * Autor         Ing. Sandra M. Escalante G.
 * Fecha         4 Mayo de 2005, 01:43 PM
 * Versión       1.0
 * Coyright      Transportes Sanchez Polo S.A.
 */


package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  Administrador
 */
public class MenuModificarOpcionAction extends Action {
    
    /** Creates a new instance of MenuModificarOpcionAction */
    public MenuModificarOpcionAction() {
    }
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");        
                
        String next = request.getParameter("carpeta") + "/" + request.getParameter("pagina");
        
        int ido = Integer.parseInt(request.getParameter("opcion"));
        String nom = request.getParameter("nom");
        String desc = request.getParameter("desc");
	int idpadre = Integer.parseInt(request.getParameter("id_padre"));        
        int sub = Integer.parseInt(request.getParameter("sub"));
        int nivel = Integer.parseInt(request.getParameter("nivel"));
        String url = "", carp="", pag="", ot ="";
        
        try{ 
            Menu opm = model.menuService.buscarOpcionMenuxIdopcion(ido);
            int idpr = opm.getIdpadre();            
            int cambioPadre = 0;//1=si
            
            //vericamos si hubo cambio de padre
            if(idpr != idpadre ){                
                cambioPadre = 1;                                
                //obtenemos padre
                Menu m = model.menuService.buscarOpcionMenuxIdopcion(idpadre);                
                nivel = m.getNivel() + 1;                
            }
            
            
            //si es opcion ... solo modififa info de la opcion
            if ( sub == 2 ) {                
                carp = request.getParameter("carp");
                pag = request.getParameter("pag");
                ot = request.getParameter("otro");
                String cabecera = opm.getUrl().substring(0, 48);                
                url = "../controller?estado=Menu&accion=Cargar&carpeta=" + carp + "&pagina=" + pag + ot;                
            } //si es carpeta actualizo niveles de hijos
            else if ( sub == 1){                
                cambio (ido, nivel);   
            }
                        
            Menu menu= new Menu();
            menu.setIdopcion(ido);
            menu.setNivel(nivel);
            menu.setIdpadre(idpadre);
            menu.setDescripcion(desc);                           
            menu.setSubmenu(sub);
            menu.setUrl(url);
            menu.setNombre(nom);            
            menu.setUser_update(usuario.getLogin().toUpperCase());
            menu.setOrden(Integer.parseInt(request.getParameter("orden")));
            
            //////System.out.println("idopcion " + ido + " padre " + idpadre + " desc " + desc + " url " + url + " nom " + nom + " sub " + sub + " nivel " + nivel + " orden " + request.getParameter("orden") );
            
            model.menuService.modificarOpcionMenu(menu);            
            
            if (request.getParameter("pagina").equals("ModificarOpcionMenu2.jsp")){
                next = next+ "?idop="+ ido + "&msg=Modificación Exitosa!";
            }            
            
            //incluir cambios en perfil opcion en caso que la opcion cambie de padre
            //verificar si el nuevo padre esta incluido dentro de las opciones
            ///a las cuales tiene acceso el perfil
            if ( cambioPadre == 1){
                //obtengo los perfilOpcion que continenen esa opcion
                model.perfilService.listarPOxOpcion(ido);
                Vector pos = model.perfilService.getVPerfil();
            
                for (int i = 0; i < pos.size(); i++){
                    //obtengo PerfiOpcion 
                    PerfilOpcion po = (PerfilOpcion) pos.elementAt(i);
                    //obtengo Opcion
                    model.menuService.obtenerOpcion(po.getId_opcion());
                    Menu op = model.menuService.getMenuOpcion();
                    int padre = op.getIdpadre();
                    while (padre != 0){
                        try{//agrego la opcion a PerfilOpcion
                            model.perfilService.agregarPerfilOpcion(po.getId_perfil(), padre, usuario.getLogin().toUpperCase());
                        }catch(SQLException e){//si ya existe verifico si esta anulada para activarla
                            if( model.perfilService.existePerfilOpcionAnulado(po.getId_perfil(), padre)){    
                                model.perfilService.activarPerfilOpcion(usuario.getLogin().toUpperCase(), po.getId_perfil(), padre);
                            }
                        }
                        model.menuService.obtenerOpcion(padre);
                        padre = model.menuService.getMenuOpcion().getIdpadre();
                    }
                }
            }
                     
            Menu m = model.menuService.buscarOpcionMenuxIdopcion(ido);
            model.menuService.obtenerPadreMenuxIDopcion(ido);                   
            model.menuService.obtenerHijos(ido);            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
    public void cambio ( int opcion, int nivel){
        try{    
            //////System.out.println("OPCION CAMBIO " + opcion);
            model.menuService.obtenerHijos(opcion);
            Vector hijos = model.menuService.getVhijos();            
            //////System.out.println("TOTAL HIJOS " + hijos.size());
            for ( int i = 0; i < hijos.size(); i++ ){                
                Menu hijo = (Menu) hijos.elementAt(i);               
                //////System.out.println("OPCION HIJO " + hijo.getIdopcion() + " NIVEL " + hijo.getNivel());
                int nuevoNivel = nivel + 1;            
                //////System.out.println("NUEVOS NIVEL CAMBIO" + nuevoNivel);
                model.menuService.cambiarNivelOpcion(hijo.getIdopcion(), nuevoNivel);                
                cambio ( hijo.getIdopcion(), nuevoNivel);
            }
        }catch( SQLException ex){
           
        }
    }
}
