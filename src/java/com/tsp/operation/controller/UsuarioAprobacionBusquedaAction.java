/*****************************************************************
 * Nombre:        UsuarioAprobacionBusquedaAction.java
 * Descripci�n:   Clase Action para buscar Usuario aprobacion
 * Autor:         Ing. Diogenes Antonio Bastidas Morales
 * Fecha:         18 de enero de 2006, 06:52 PM
 * Versi�n        1.0
 * Coyright:      Transportes Sanchez Polo S.A.
 ********************************************************************/



package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class UsuarioAprobacionBusquedaAction extends Action{
    
    /** Creates a new instance of UsuarioAprobacionBusquedaAction */
    public UsuarioAprobacionBusquedaAction() {
    }
    
    public void run() throws ServletException, InformationException {
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = (String) session.getAttribute("Distrito");
        String next = "/jsp/general/usuarioAprobacion/usuarios.jsp";
        String agencia = request.getParameter("agencia");
        String tabla = request.getParameter("tabla").toUpperCase();
        String usu = request.getParameter("usuario");
        try {
            model.usuaprobacionService.listarUsuariosAprobacion(agencia, tabla, usu );
            
        }
        catch(Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
        
    }
    
}
