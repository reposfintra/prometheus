/*
 * ProyeccionSegurosAction.java
 *
 * Created on 7 de julio de 2005, 10:41 AM
 */

package com.tsp.operation.controller;

import javax.servlet.*;
import javax.servlet.http.*;
import java.text.*;
import com.tsp.operation.model.threads.*;
import com.tsp.operation.model.beans.*;
import java.util.*;
/**
 *
 * @author  henry
 */
public class RptnumeroControlAction extends Action {
    
    /** Creates a new instance of ProyeccionSegurosAction */
    public RptnumeroControlAction() {
    }
    
    public void run() throws javax.servlet.ServletException {
        try{
            HttpSession session =  request.getSession();
            Usuario usuario = (Usuario) session.getAttribute("Usuario");            
            String opc =  (request.getParameter("opc")!=null)?request.getParameter("opc"):""; 
            String dst =  (request.getParameter("distrito")!=null)?request.getParameter("distrito"):"";
            String fecI =  (request.getParameter("fecini")!=null)?request.getParameter("fecini"):""; 
            String fecF =  (request.getParameter("fecfin")!=null)?request.getParameter("fecfin"):"";
            String next = "/jsp/trafico/despachos/rptNumMovimientos.jsp?fecini="+fecI+"&fecfin="+fecF+"&distrito="+dst;
//            model.rptNumeroMovSVC.generarRptNumeroMovimienots(dst, fecI, fecF);            
//            Vector datos = model.rptNumeroMovSVC.getVectorRpts();
//            ////System.out.println("ENTRE AQUI");
//            if (opc.equals("generar")){                
//                ReporteNumeroOCTrafimo RPT = new ReporteNumeroOCTrafimo();
//                RPT.start(datos, fecI, fecF, dst, usuario.getLogin());
//                next = "/jsp/trafico/despachos/rptNumMovimientos.jsp?arc=OK";
//            }
            this.dispatchRequest(next);
        }catch (Exception ex){
            throw new ServletException ("Error en RPTCONTROL : " + ex.getMessage());
        }
        
    }    
}
