/*
 * DespachomInsertarmAction.java
 *
 * Created on 22 de noviembre de 2005, 11:54 AM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  dlamadrid
 */
public class DespachomInsertarmAction extends Action
{    //2009-09-02

    
    /** Creates a new instance of DespachomInsertarmAction */
    public DespachomInsertarmAction ()
    {
        
    }
    
    public void run () throws javax.servlet.ServletException, com.tsp.exceptions.InformationException
    {
        String next="";
        String ms="";
        try
        {
            
            HttpSession session = request.getSession ();
            Usuario usuario = (Usuario) session.getAttribute ("Usuario");
            String userlogin=""+usuario.getLogin ();
            
            String distrito=""+ usuario.getDstrct ();
            String placa=""+request.getParameter ("c_placa");
            String conductor=""+request.getParameter ("c_conductor");
            //System.out.println("conductor::::::::::::::::::::::::::::::::::::::::::"+conductor);
            String ruta = ""+request.getParameter ("c_ruta");
            //System.out.println("rutaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa"+ruta);
            String cliente =""+request.getParameter ("c_cliente");
            String carga =""+request.getParameter ("c_carga");
            String fecha_despacho=""+request.getParameter ("c_fecha");
            String planilla=""+request.getParameter ("c_planilla");
            String nombreConductor=""+request.getParameter ("c_n_conductor");
            
            boolean existeCliente=false;
            boolean existeConductor=false;
            boolean sw=false;
            sw= model.placaService.existePlaca (placa);
            existeCliente= model.clienteService.existeCliente (cliente);
            existeConductor= model.conductorService.existeConductor (conductor);
            
            if(existeConductor==true)
            {
                if(existeCliente==true)
                {
                    if (sw==true)
                    {
//                        model.despachoManualService.actualizarDespacho (distrito,placa, fecha_despacho, conductor, ruta, cliente, carga,planilla,nombreConductor,userlogin);
                        //System.out.println("FECHA    "+fecha_despacho);
                        model.despachoManualService.listaDespacho ();
                        next="/jsp/trafico/despacho_manual/despacho.jsp?accion=1&reload=1";
                        
                    }
                    else
                    {
                        ms="No existe la placa";
                        next="/jsp/trafico/despacho_manual/modificar.jsp?accion=1&ms="+ms;
                    }
                }
                else
                {
                    ms="No existe un Cliente con ese Codigo";
                    next="/jsp/trafico/despacho_manual/modificar.jsp?accion=1&ms="+ms;
                }
            }
            else
            {
                ms="No existe un Conductor con esa Cedula";
                next="/jsp/trafico/despacho_manual/modificar.jsp?accion=1&ms="+ms;
            }
        }//fin del try
        catch (Exception e)
        {
            throw new ServletException (e.getMessage ());
            
        }
        this.dispatchRequest (next);
        
    }
    
}
