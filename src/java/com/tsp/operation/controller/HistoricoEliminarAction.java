/*
 * HistoricoEliminarAction.java
 *
 * Created on 30 de septiembre de 2005, 11:12 AM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.sql.*;
import java.sql.SQLException.*;
import java.util.*;
import java.text.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import org.apache.log4j.Logger;
/**
 *
 * @author  dlamadrid
 */
public class HistoricoEliminarAction extends Action{
    
    /** Creates a new instance of HistoricoEliminarAction */
    public HistoricoEliminarAction() {
    }
    
   
    
    public void run() throws ServletException,InformationException{
      //  String 
       // HttpSession session = request.getSession();
       // Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
       // String usuario = ""+request.getParameter("usuario");
       
        String tablaOrigen = ""+request.getParameter("tablaO");
        String id = ""+request.getParameter("id");
         
        try {
            //System.out.println("tabla origen"+tablaOrigen);
           
            model.adminH.eliminarHistorico(id);
               
            String next = "/adminHistorico/ahistorico.jsp";
                this.dispatchRequest(next);
        }     
       catch(SQLException e){
           //System.out.println(e.getMessage());
          //throw new ServletException("Accion:"+ e.getMessage());
        }    
        
    }    
    
}
