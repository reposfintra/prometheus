/**
 * Nombre        OpcionesConceptosAction.java
 * Descripci�n
 * Autor         Mario Fontalvo Solano
 * Fecha         10 de marzo de 2006, 08:35 AM
 * Version       1.0
 * Coyright      Transportes S�nchez Polo S.A.
 **/

package com.tsp.operation.controller;

import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.TblConceptos;


public class OpcionesConceptosAction extends Action{
    
    TblConceptos concepto  = null;
    Vector       conceptos = null;
    Usuario      usuario   = null;
    
    /** Crea una nueva instancia de  OpcionesConceptosAction */
    public OpcionesConceptosAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        
        try{
            
            HttpSession session = request.getSession();
            usuario             = (Usuario) session.getAttribute("Usuario");
            String opcion       = getParameter("Opcion"      , "");
            String dstrct       = getParameter("dstrct"      , "");
            String concept_code = getParameter("concept_code", "");
            int    index        = Integer.parseInt(getParameter("index", "-1"));
            String next         = "/jsp/masivo/conceptos/";
            
            ////////////////////////////////////////////////////////////////////
            if ( opcion.equalsIgnoreCase("Insert")){
                ////System.out.println("insert");
                this.loadConceptoFromRequest();
                model.ConceptosSvc.setConcepto(null);
                model.ConceptosSvc.buscarConceptos(concepto.getDstrct(), concepto.getConcept_code());
                if ( model.ConceptosSvc.getConcepto()==null){
                    model.ConceptosSvc.insertConcepto(concepto);
                    request.setAttribute( "Mensaje", "Registro Ingresado con exito." );
                } else{
                    request.setAttribute( "Mensaje", "Este concepto ya existe, por favor intente con otro codigo" );
                    request.setAttribute( "concepto", concepto );
                }
                request.setAttribute( "Accion", "Insert" );
                next += "edicion.jsp";
                
            }
            
            ////////////////////////////////////////////////////////////////////
            else if ( opcion.equalsIgnoreCase("Update")){
                
                this.loadConceptoFromRequest();
                model.ConceptosSvc.updateConcepto(concepto);
                request.setAttribute( "Mensaje" , "Registro actualizado con exito." );
                request.setAttribute( "Accion"  , "Update" );
                request.setAttribute( "Accion2" , "reload" );
                request.setAttribute( "concepto", concepto );
                
                next += "edicion.jsp";
                
            }
            
            ////////////////////////////////////////////////////////////////////
            else if ( opcion.equalsIgnoreCase("Anular")){
                
                this.loadConceptosSeleccionados("A");
                model.ConceptosSvc.updateEstado(conceptos);
                model.ConceptosSvc.obtenerTodosLosConceptos();
                request.setAttribute( "Mensaje", "Registros anulados con exito." );
                next += "listado.jsp";
                
                
            }
            
            ////////////////////////////////////////////////////////////////////
            else if ( opcion.equalsIgnoreCase("Activar")){
                
                this.loadConceptosSeleccionados("");
                model.ConceptosSvc.updateEstado(conceptos);
                model.ConceptosSvc.obtenerTodosLosConceptos();
                request.setAttribute( "Mensaje", "Registros activados con exito." );
                next += "listado.jsp";
                
            }
            
            ////////////////////////////////////////////////////////////////////
            else if ( opcion.equalsIgnoreCase("Delete")){
                
                this.loadConceptosSeleccionados("");
                model.ConceptosSvc.deleteConceptos(conceptos);
                model.ConceptosSvc.obtenerTodosLosConceptos();
                request.setAttribute( "Mensaje", "Registros eliminados con exito." );
                next += "listado.jsp";
                
            }
            
            ////////////////////////////////////////////////////////////////////
            else if ( opcion.equalsIgnoreCase("Search")){
                
                model.ConceptosSvc.buscarConceptos(dstrct, concept_code);
                if ( model.ConceptosSvc.getConcepto()!=null )
                    request.setAttribute( "Accion", "Update" );
                else
                    request.setAttribute( "Accion", "Insert" );
                
                request.setAttribute( "concepto", model.ConceptosSvc.getConcepto());
                next += "edicion.jsp";
                
            }
            
            ////////////////////////////////////////////////////////////////////
            else if ( opcion.equalsIgnoreCase("Editar")){
                model.ciaService.listarDistritos();
                model.tablaGenService.buscarRegistros( "TCONCCLASS" );
                model.ConceptosSvc.setCmbClases(
                    model.tablaGenService.obtenerTreeMapLista(
                        model.tablaGenService.obtenerTablas()
                    )
                );
                if (index>=0) {
                    request.setAttribute( "concepto", model.ConceptosSvc.getVector().get(index));
                }
                request.setAttribute( "Accion", "Update" );
                next += "edicion.jsp";
                
            }
            
            
            ////////////////////////////////////////////////////////////////////
            else if ( opcion.equalsIgnoreCase("List")){
                
                model.ConceptosSvc.obtenerTodosLosConceptos();
                next += "listado.jsp";
                
                
            }
            
            
            RequestDispatcher rd = application.getRequestDispatcher( next );
            if (rd==null)
                throw new ServletException("no se pudo encontrar " + next );
            rd.forward(request, response);
            
            
        }catch (Exception ex){
            ex.printStackTrace();
            throw new ServletException( " OpcionesConceptosAction ...\n"+ ex.getMessage()  );
        }
        
    }
    
    
    /* Funcion para leer un parametro del mobjeto request
     * @autor mfontalvo
     * @params str1, parametro a leer
     * @params str2, devuelve este parametro si str1 es null
     * @return el parametro del objeto request
     */
    public String getParameter(String str1, String str2){
        return (request.getParameter(str1)==null?str2:request.getParameter(str1));
    }
    
    
    
    /**
     * Metodo para setear el bean con los datos del request
     * @autor mfontalvo
     */
    private void loadConceptoFromRequest(){
        concepto = new TblConceptos();
        concepto.setDstrct          ( getParameter("dstrct"          ,"") );
        concepto.setConcept_code    ( getParameter("concept_code"    ,"").toUpperCase() );
        concepto.setConcept_desc    ( getParameter("concept_desc"    ,"").toUpperCase() );
        concepto.setInd_application ( getParameter("ind_application" ,"") );
        concepto.setAccount         ( getParameter("account"         ,"").toUpperCase() );
        concepto.setCodigo_migracion( getParameter("codigo_migracion","").toUpperCase() );
        concepto.setVisible         ( getParameter("visible"         ,"") );
        concepto.setModif           ( getParameter("modif"           ,"") );
        concepto.setInd_signo       ( Integer.parseInt(getParameter("ind_signo","1") ));
        concepto.setConcept_class   ( getParameter("concept_class"   ,"") );
        concepto.setCreation_user   ( usuario.getLogin()                  );
        concepto.setUser_update     ( usuario.getLogin()                  );
        concepto.setBase            ( usuario.getBase()                   );
        
        
        concepto.setTipo_cuenta     ( getParameter("tipo_cuenta"           ,"") );
        concepto.setAsocia_cheque   ( getParameter("asocia_cheque"         ,"").toUpperCase() );
        concepto.setTipo            ( getParameter("tipo"                  ,"").toUpperCase() );
        concepto.setVlr             ( Double.parseDouble(getParameter("vlr","0").replaceAll(",","")) );
        concepto.setCurrency        ( getParameter("currency"              ,"").toUpperCase() );
        concepto.setDescuento_fijo  ( getParameter("descuento_fijo"        ,"").toUpperCase() );
        concepto.setIngreso_costo   ( getParameter("ingreso_costo"         ,"").toUpperCase() );

    }
    
    /**
     * Metodo para cargar los conceptos seccionados desde la vista
     * para efectos de atualizacion de estado y eliminacion
     */
    private void loadConceptosSeleccionados(String estado){
        String []LOV = request.getParameterValues("LOV");
        if (LOV!=null){
            conceptos = new Vector();
            for (int i=0; i<LOV.length; i++){
                String []datos = LOV[i].split("~");
                
                TblConceptos con = new TblConceptos() ;
                con.setDstrct      ( datos[0] );
                con.setConcept_code( datos[1] );
                con.setReg_status  ( estado   );
                con.setUser_update ( usuario.getLogin() );
                conceptos.add( con );
            }
        }
    }
    
}
