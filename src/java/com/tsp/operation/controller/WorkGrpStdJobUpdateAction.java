/********************************************************************
 *      Nombre Clase.................   WorkGrpStdJobUpdateAction.java
 *      Descripci�n..................   Actualiza el campo origen de la vista
 *      Autor........................   Ing. Tito Andr�s Maturana
 *      Fecha........................   13.01.2006
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *******************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
import com.tsp.util.*;

public class WorkGrpStdJobUpdateAction extends Action{
    
    public WorkGrpStdJobUpdateAction() {
    }
    
    public void run() throws ServletException, InformationException {        
               
        String cliente = request.getParameter("clienteSelec");
        String origen = request.getParameter("origenSelec");
        String destino = request.getParameter("destinoSelec");      
        
        String next="/jsp/masivo/workgroup_std/WorkGrpStdJInsert.jsp?";
        
        String[] stds = request.getParameterValues("c_stdSelec");
        String[] wgs = request.getParameterValues("c_wgSelec");
        
        /*////System.out.println(".............. Se recibio el cliente: " + cliente);        
        ////System.out.println(".............. Se recibio el origen: " + origen);
        ////System.out.println(".............. Se recibio el destino: " + destino);*/
        
        try{
            
            model.wgroup_stdjobSvc.loadVarCamposJS(wgs);
            model.wgroup_stdjobSvc.loadVarCamposJStd(stds);
            
            if ( destino.length()!=0 ){
                model.stdjobService.setStdJobsCliente(new TreeMap());
                
                model.stdjobService.listarStdJobsCliente(cliente, origen, destino);
                
                request.setAttribute("cliente", cliente);
                request.setAttribute("origen", origen);
                request.setAttribute("destino", destino);
            } else if ( origen.length()!=0 ){
                model.stdjobService.setDestinosStdJobsCliente(new TreeMap());
                model.stdjobService.setStdJobsCliente(new TreeMap());
                
                model.stdjobService.destinosStdJobsCliente(cliente, origen);
                
                request.setAttribute("cliente", cliente);
                request.setAttribute("origen", origen);
            } else if ( cliente.length()!=0 ){
                model.stdjobService.setOrigenesStdJobsCliente(new TreeMap());
                model.stdjobService.setDestinosStdJobsCliente(new TreeMap());
                model.stdjobService.setStdJobsCliente(new TreeMap());
                
                model.stdjobService.origenesStdJobsCliente(cliente);
                
                request.setAttribute("cliente", cliente);
            }
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
}
