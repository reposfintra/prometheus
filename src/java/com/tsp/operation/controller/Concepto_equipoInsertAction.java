/****************************************************************************
* Nombre .................Concepto_equipoInsertAction.java                  *
* Descripción.............Accion para ingresar un concepto equipo a la bd.  *
* Autor...................Ing. Jose de la rosa                              *
* Fecha Creación..........16 de diciembre de 2005, 10:07 PM                 *
* Modificado por..........LREALES                                           *
* Fecha Modificación......5 de junio de 2006, 11:01 AM                      *
* Versión.................1.0                                               *
* Coyright................Transportes Sanchez Polo S.A.                     *
*****************************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;

public class Concepto_equipoInsertAction extends Action{
    
    /** Creates a new instance of Concepto_equipoInsertAction */
    public Concepto_equipoInsertAction () {
    }
    
    public void run () throws ServletException, InformationException {
        
        String next = "/jsp/equipos/concepto_equipo/concepto_equipoInsertar.jsp";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");   
        String dstrct = (String) session.getAttribute("Distrito");
        String descripcion = (request.getParameter("c_descripcion")!= null)?request.getParameter("c_descripcion"):"";
        String codigo = (request.getParameter("c_codigo").toUpperCase()!= null)?request.getParameter("c_codigo").toUpperCase():"";
        int sw=0;
        
        try{
            
            Concepto_equipo concepto = new Concepto_equipo();
            
            concepto.setCodigo ( codigo );
            concepto.setDescripcion ( descripcion );
            concepto.setUsuario_modificacion( usuario.getLogin().toUpperCase() );
            concepto.setUsuario_creacion( usuario.getLogin().toUpperCase() );
            concepto.setDistrito ( dstrct );
            concepto.setBase( usuario.getBase() );
                    
            try{
                
                model.concepto_equipoService.insertConcepto_equipo ( concepto );
                
            } catch( SQLException e ){
                
                sw = 1;
                
            }
            
            if( sw == 1 ){
                
                if(!model.concepto_equipoService.existConcepto_equipo ( codigo )){
                    
                    model.concepto_equipoService.updateConcepto_equipo ( concepto );
                    request.setAttribute( "mensaje", "La información ha sido ingresada exitosamente!" );
                    
                } else{
                    
                    request.setAttribute( "mensaje", "Error..el codigo digitado ya existe!" );
                    
                }
                
            } else{
                
                request.setAttribute( "mensaje", "La información ha sido ingresada exitosamente!" );
                
            }
            
        } catch( SQLException e ){
            
            throw new ServletException( e.getMessage() );
            
        }
        
        this.dispatchRequest( next ); 
        
    }
    
}