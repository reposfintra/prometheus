/*
 * VeridocumenVerificarAction.java
 *
 * Created on 5 de octubre de 2005, 01:21 PM
 */

package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
/**
 *
 * @author  dbastidas
 */
public class VeridocumenVerificarAction extends Action {
    
    /** Creates a new instance of VeridocumenVerificar */
    public VeridocumenVerificarAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/jsp/hvida/veridocumen/documento.jsp?mensaje=Actualizado";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
        String distrito = (String) session.getAttribute("Distrito");
        Date fecha = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String now = format.format(fecha);
        String cedula = (request.getParameter("identificacion")!=null)?request.getParameter("identificacion"):"";
        String placa = (request.getParameter("placa")!=null)?request.getParameter("placa"):"";
        String estado = request.getParameter("est");
        String usuveri = usuario.getLogin();


        try{
            ////System.out.println("Estado Action "+estado );
            VerificacionDoc veridoc = new VerificacionDoc();
            veridoc.setCedula(cedula);
            veridoc.setUsuarioverica(usuveri);
            veridoc.setFechavericacion(now);
            veridoc.setPlaca(placa);
            
            ////System.out.println("");
            ////System.out.println("");            
            ////System.out.println("Verifica");
            
            model.veridocService.Verificar(veridoc,estado);
            next = next+"&men="+estado;
            if (estado.equals("Placa")){
                model.veridocService.cargarDocPlaca(placa);
                ////System.out.println("Carga Placa");
            
            }
            else{
                model.veridocService.cargarDocConductor(cedula);
                ////System.out.println("Carga Conductor");
            }
            ////System.out.println("");
            ////System.out.println("");            
            ////System.out.println("Next "+next);
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
