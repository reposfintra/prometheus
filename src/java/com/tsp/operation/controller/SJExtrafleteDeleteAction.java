/*
 * ProveedoresInsertAction.java
 *
 * Created on 19 de Julio de 2005, 02:00 AM
 */

package com.tsp.operation.controller;


import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
//import com.tsp.exceptions.*;
/**
 *
 * @author  Henry
 */
public class SJExtrafleteDeleteAction extends Action{
    
    /** Creates a new instance of ProveedoresInsertAction */
    public SJExtrafleteDeleteAction() {
    }
    
    public void run() throws ServletException {
        String next ="/extraflete/extrafleteUpdate.jsp?";
        String codcli        = request.getParameter("cliente");
        String std_job       = request.getParameter("stdjob");        
        String codextra      = request.getParameter("extraflete");
        /*HttpSession session = request.getSession();
        Usuario usuario     = (Usuario) session.getAttribute("Usuario");*/            
        SJExtraflete s = new SJExtraflete();
        s.setCod_extraflete(codextra);
        s.setCodcli(codcli);
        s.setStd_job_no(std_job);                
        String resp = "opcion=CL000000ST000000RT000-000EF000&ruta=&men=";
        try{ 
            if(model.sjextrafleteService.existeSJExtraflete(codcli,std_job,codextra)){
                model.sjextrafleteService.eliminarSJExtraflete(s);
                next = next+resp+"OKDEL";
            } else {
                next = next+resp+"ERROR";
            }           
        }catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
}
