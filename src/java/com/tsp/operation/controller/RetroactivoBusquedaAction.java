
/*************************************************************************
 * Nombre:        RetroactivoBusquedaAction.java                         *
 * Descripci�n:   Clase Action para Busqueda retroactivo                 *
 * Autor:         Ing. Diogenes Antonio Bastidas Morales                 *
 * Fecha:         7 de febrero de 2006, 10:04 AM                         * 
 * Versi�n        1.0                                                    * 
 * Coyright:      Transportes Sanchez Polo S.A.                          * 
 *************************************************************************/
package com.tsp.operation.controller;
import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import com.tsp.exceptions.*;
 

public class RetroactivoBusquedaAction extends Action {
    
    /** Creates a new instance of RetroactivoBusquedaAction */
    public RetroactivoBusquedaAction() {
    }
    
    public void run() throws ServletException, InformationException {
        String next = "/jsp/masivo/retroactivo/retroactivos.jsp";
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String distrito = (String) session.getAttribute("Distrito");
        String stdjob = (request.getParameter("stdjob").equals("TR"))?"":request.getParameter("stdjob");
        
         try{
           model.retroactivoService.listarRetroactivo(stdjob, request.getParameter("ruta").toUpperCase());
            
        }catch (SQLException e){
            throw new ServletException(e.getMessage());
        }
        this.dispatchRequest(next);
    }
    
}
