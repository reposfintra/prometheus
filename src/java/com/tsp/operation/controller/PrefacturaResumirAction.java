/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.services.*;
import com.tsp.operation.model.threads.*;
import com.tsp.util.Util;
import com.tsp.operation.model.beans.Usuario;
import javax.servlet.http.*;
import java.sql.SQLException;
import com.tsp.operation.model.*; 
 

/**
 *
 * @author Alvaro
 */
public class PrefacturaResumirAction extends Action {


    public PrefacturaResumirAction() {
    }

    public void run() throws ServletException, InformationException {

        try{

            HttpSession session   = request.getSession();
            Usuario     usuario   = (Usuario) session.getAttribute("Usuario");


            String next           = "/jsp/prefactura/prefacturasListar.jsp?msj=";
            String evento         = request.getParameter("evento");
            String fechaInicial   = request.getParameter("fechaInicial");
            String fechaFinal     = request.getParameter("fechaFinal");

            String msj            = "";

            model.applusService.buscaPrefacturaResumen(usuario.getNitPropietario(),fechaInicial,fechaFinal);
            List listaPrefacturaResumen = model.applusService.getPrefacturaResumen();
            model.applusService.buscaImpuestoContrato("RMAT","FINV","RFTE" );

            if(listaPrefacturaResumen.size()!=0) {
                msj   = "La pagina esta en construccion...";
                next  = "/jsp/applus/prefactura/prefacturasListar.jsp?msj=";
            }
            else {
                msj   = "El contratista no tiene prefacturas generadas...";
                next  = "/jsp/applus/prefactura/prefacturasResumir.jsp?msj=";
            }
            next += msj ;
            this.dispatchRequest(next);






        } catch (Exception e){
            throw new ServletException(e.getMessage());
        }




    }




}
