/*******************************************************************
 * Nombre clase: ClienteBuscarAction.java
 * Descripci�n: Accion para buscar una placa de la bd.
 * Autor: Ing. fily fernandez
 * Fecha: 16 de febrero de 2006, 05:41 PM
 * Versi�n: Java 1.0
 * Copyright: Fintravalores S.A. S.A.
 ********************************************************************/

package com.tsp.operation.controller;

import java.io.*;
import com.tsp.exceptions.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import java.util.Vector;
import java.lang.*;
import java.sql.*;
import com.tsp.exceptions.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.threads.*;
import com.tsp.operation.model.services.*;
/**
 *
 * @author  jdelarosa
 */
public class ReporteViajesAction extends Action{
    
    /** Creates a new instance of PlacaBuscarAction */
    public ReporteViajesAction() {
    }
    
    public void run () throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        String FechaF = request.getParameter ("FechaF")!=null?request.getParameter ("FechaF"):"";
        String FechaI = request.getParameter ("FechaI")!=null?request.getParameter ("FechaI"):"";
        
        String next = "/jsp/trafico/reportes/VerReporteCalidad.jsp?FechaF="+FechaF+"&FechaI="+FechaI;
        HttpSession session = request.getSession ();
        Usuario usuario = (Usuario)session.getAttribute ("Usuario");
        String usu = usuario.getLogin();
        
        String opcion = request.getParameter ("opcion")!=null?request.getParameter ("opcion"):"";
        try {
              Vector vec = model.planllegadacargue.getvectorValores();
              
           if ( opcion.equals("1") ){
                model.planllegadacargue.ReporteViajesDescargues(FechaI,FechaF);
                vec = model.planllegadacargue.getvectorValores();
                if ( vec.size () <= 0 ) {    
                     next = next + "&msg=Su busqueda no arrojo resultados!";
                
                }  
               
           } 
           if ( opcion.equals("2") ){
                 HReporteViajesDescargues cli = new HReporteViajesDescargues();
                cli.start(FechaI, FechaF, usu);
               
           } 
           
            
        } catch ( Exception e ) {
            
            throw new ServletException ( "Error en Reporte de ReporteViajesAction : " + e.getMessage() );
            
        }        
        
        this.dispatchRequest ( next );
        
    }
    
}
