/******************************************************************************
 *      Nombre Clase.................   ReporteFacturasProveedorAction.java
 *      Descripci�n..................   Anula un registro en la tabla tblapl
 *      Autor........................   Ing. Andr�s Maturana
 *      Fecha........................   19.20.2005
 *      Versi�n......................   1.0
 *      Copyright....................   Transportes Sanchez Polo S.A.
 *****************************************************************************/
package com.tsp.operation.controller;

import java.io.*;
import java.util.*;
import java.text.*;
import java.sql.SQLException;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.*;
import com.tsp.operation.model.*;
import org.apache.log4j.Logger;
import com.tsp.exceptions.*;
import com.tsp.util.Util;

import org.apache.log4j.*;

public class ReporteFacturasRecurrentesAction extends Action{
    
    Logger logger = Logger.getLogger(this.getClass());
    
    /** Creates a new instance of DocumentoInsertAction */
    public ReporteFacturasRecurrentesAction() {
    }
    
    public void run() throws javax.servlet.ServletException, com.tsp.exceptions.InformationException {
        //Pr�xima vista
        String next  = "/jsp/cxpagar/reportes/ListadoFacturasRecurrentes.jsp";
        String factura = request.getParameter("factura");
        String tipo_doc = request.getParameter("tipo_doc");
        String nit = request.getParameter("proveedor");
        String numpla = request.getParameter("planilla");
        String agencia = request.getParameter("agencia");
        String banco = request.getParameter("banco");
        String sucursal = request.getParameter("sucursal");
        String fechai = request.getParameter("FechaI");
        String fechaf = request.getParameter("FechaF");
        String pag = request.getParameter("pagada");
        String anuladas = request.getParameter("anuladas");//Osvaldo
        String tipo = request.getParameter("tipo_factura");//Osvaldo
        
        logger.info("FACTURA: " + factura);
        logger.info("TIPO DOC: " + tipo_doc);
        logger.info("PROVEEDOR: " + nit);
        logger.info("PLANILLA: " + numpla);
        logger.info("AGENCIA: " + agencia);
        logger.info("BANCO: " + banco);
        logger.info("SUCURSAL: " + sucursal);
        logger.info("FECHAI: " + fechai);
        logger.info("FECHAF: " + fechaf);
        logger.info("PAGADA: " + pag);
        
        
        //Usuario en sesi�n
        HttpSession session = request.getSession();
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String dstrct = (String) session.getAttribute("Distrito");
        String evento = request.getParameter("evento")!=null? request.getParameter("evento") : "";
        
        try{
            
            List ListaAgencias = model.ciudadService.ListarAgencias();
            TreeMap tm = new TreeMap();
            if(ListaAgencias.size()>0) {
                Iterator It3 = ListaAgencias.iterator();
                while(It3.hasNext()) {
                    Ciudad  datos2 =  (Ciudad) It3.next();
                    tm.put("["+datos2.getCodCiu()+"] "+datos2.getNomCiu(), datos2.getCodCiu());
                }
            }
            request.setAttribute("agencias", ListaAgencias);
            request.setAttribute("agencias_tm", tm);
            
            if( evento.equals("") ){
                
                if(numpla.equals("")){
                    model.factrecurrService.consultarDocumento(dstrct, factura, tipo_doc, agencia, banco, sucursal, nit, fechai, fechaf, pag, anuladas, tipo);
                }else{
                    model.factrecurrService.buscarFacturasPlanillas(dstrct, factura, tipo_doc, agencia, banco, sucursal, nit, fechai, fechaf, pag,numpla);
                }
                
                if( model.factrecurrService.getVecCxp_doc().size()==0 ){
                    next = "/jsp/cxpagar/reportes/ReporteFacturasRecurrentes.jsp?msg=No se encontraron resultados.";
                }
            }else if( evento.equals("anular") ){
                
                String[] facturas = request.getParameterValues("anular");
                int c = model.factrecurrService.anularRecurrentes( facturas, usuario.getLogin() );
                
                if(numpla.equals("")){
                    model.factrecurrService.consultarDocumento(dstrct, factura, tipo_doc, agencia, banco, sucursal, nit, fechai, fechaf, pag, anuladas, tipo);
                }else{
                    model.factrecurrService.buscarFacturasPlanillas(dstrct, factura, tipo_doc, agencia, banco, sucursal, nit, fechai, fechaf, pag,numpla);
                }
                
                if( model.factrecurrService.getVecCxp_doc().size()==0 ){
                    next = "/jsp/cxpagar/reportes/ReporteFacturasRecurrentes.jsp?msg=No se encontraron resultados.<br>"+c+" Factura(s) anulada(s)";
                }else{
                    next += "?msg="+c+" Factura(s) anulada(s)";
                }
            }
            
            // Busca las facturas asociadas a una RXP
            else if( evento.equals("buscar") ){
                next = "/jsp/cxpagar/reportes/ListadoFacturasAsociadas.jsp";
                model.cXP_DocService.consultarFacturasAsociadasRxp( dstrct, nit, tipo_doc, factura );
            }
            
            
        }catch (Exception e){
            throw new ServletException(e.getMessage());
        }
        
        this.dispatchRequest(next);
    }
    
}
