/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.servlets;

/**
 * DenegacionOfertasServlet.java<br />
 * Sevlet para realizar el llamado al hilo que ejecuta el proceso para denviar estado de cuentas a clientes que tiene facturas por vencerse<br />
 * @date 30/08/2011
 * @author ivargas - GEOTECH
 * @version 1.0
 */

import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.threads.HEnviarEstadoCuenta;
import java.io.IOException;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



public class EnviarEstadoCuentaServlet extends HttpServlet implements Servlet
{ 


    public EnviarEstadoCuentaServlet()
    {
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try{

            Usuario usuario = (Usuario) request.getSession().getAttribute("Usuario");
            HEnviarEstadoCuenta envio = new HEnviarEstadoCuenta(usuario.getBd());
            envio.start( );
            envio.join();
            System.out.println(">>> LOS PROCESOS TERMINARON EXITOSAMENTE...");
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("SE PRESENTO UN ERROR EN EL ENVIO DE EXTRACTOS POR CORREOS..."+e.getMessage());
        }

    }

  }





