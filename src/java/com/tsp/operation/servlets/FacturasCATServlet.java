/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.servlets;

/**
 * FacturasCATServlet.java<br />
 * Sevlet para realizar el llamado al hilo que genera las facturas cat por vencimiento de facturas capital<br />
 * @date 16/03/2012
 * @author ivargas - GEOTECH
 * @version 1.0
 */

import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.threads.HFacturasCAT;
import java.io.IOException;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;



public class FacturasCATServlet extends HttpServlet implements Servlet
{ 
    private String ciclo="";
    private String periodo="";
    

    public FacturasCATServlet()
    {
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        ciclo = request.getParameter("ciclo") != null ? request.getParameter("ciclo") : "";
        periodo=request.getParameter("periodo") != null ? request.getParameter("periodo") : "";
        this.doPost(request, response);
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try{
            Usuario usuario = new Usuario();
            usuario.setLogin("ADMIN");
            usuario.setBd("fintra");
            HFacturasCAT hilo = new HFacturasCAT(usuario,ciclo, periodo);
            hilo.start( );
            hilo.join();
            System.out.println(">>> LOS PROCESOS TERMINARON EXITOSAMENTE...");
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("SE PRESENTO UN ERROR EN ELA GENERACION DE CXC CAT..."+e.getMessage());
        }

    }

  }





