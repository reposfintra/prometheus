/*Created on 3 de agosto de 2005, 08:55 AM */


package com.tsp.operation.servlets;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.*;


/**
 * Servlet que realiza el proceso de carga archivos
 * @author fvillacob
 */



public class TblEstadoInsertServlet extends HttpServlet {
    
        private Model model;



        /**
        * Procesa las peticiones enviadas por los m�todos HTTP <code>GET</code>
        * y <code>POST</code>.
        * @param request Peticion al servlet.
        * @param response Respuesta del servlet.
        */
        protected void doUpload(HttpServletRequest request, HttpServletResponse response)
                throws  ServletException, IOException {
      
      
                HttpSession session          = request.getSession(true);
                model                        = new Model();    
                String newLine               = null;
                String filepath              = null;
                String filename              = null;
                String contentType           = null;
                String s                     = null;    
                String comentario = "";
                int xi;
                int pos;    
                String maxFileSize           = request.getParameter("maxFileSize");
                int Max                      = Integer.parseInt(maxFileSize);
                Dictionary fields =  null;
                ServletInputStream in        = request.getInputStream();
                ByteArrayInputStream  bfin   = null;
                ByteArrayOutputStream bfout  = null;    
                byte[] line                  = new byte[256];
                byte[] line2                 = new byte[256];
                byte[] savedFile             = null;

    
                try{ 
                        //--- Controlar el tama�o del archivo.
                        int Length = request.getContentLength();    
                                if( Length > Max ) {
                                        this.redired(request, response, "El tama�o de la imagen supera el tope m�ximo.");
                                        return;      
                                } 
                        else {
        
                        int i = in.readLine(line, 0, 256);

                        if (i > 2) {
                                int boundaryLength = i - 2;
                                String boundary    = new String(line, 0, boundaryLength);
                                fields             = new Hashtable();

                                // RECORREMOS LOS PARAMETROS       
                                
                                while ( i != -1 ) {
                                        newLine = new String(line, 0, i);           
           
                                        if (newLine.startsWith("Content-Disposition: form-data; name=\"")) {              
                                   
                                                //--- EL ARCHIVO
                                                if (newLine.indexOf("filename=\"") != -1) {
                                                        s = new String(line, 0, i-2);   
                                                        
                                                        if ( s != null ) {
                                                                
                                                                String s2 = s;
                                                                filename = this.getName(s);
                                                                filepath = this.getPath(s2);
                                                                
                                                                if ( filename != null && !filename.equals("") ) {

                                                                        //-- validamos que sea un archivo
                                                                        if ( filename.indexOf(".") == -1 ) {
                                                                                this.redired(request, response, "Deber� seleccionar un archivo valido");
                                                                                return;
                                                                        }

                                                                        contentType = this.getContext(in, line);

                                                                        // leemos el archivo en binario

                                                                        i = in.readLine(line, 0, 256);   
                                                                        i = in.readLine(line, 0, 256);

                                                                        newLine = new String(line, 0, i);                                      
                                                                        bfout = new ByteArrayOutputStream();                
                                                                        while ( i != -1 && !newLine.startsWith(boundary) ) {
                                                                                line2 = ( byte[] )line.clone();
                                                                                xi    = i;
                                                                                i     = in.readLine(line, 0, 256);                  
                                                                                if ( new String(line, 0, i).startsWith(boundary) ) 
                                                                                        bfout.write( line2, 0, xi-2 );
                                                                                else                                               
                                                                                        bfout.write( line2, 0, xi );                  
                                                                                newLine = new String(line, 0, i);
                                                                        }                
                                                                        
                                                                        savedFile = bfout.toByteArray();
                                                                        bfout.close();

                                                                } 
                                                        }
                                                }
                                                else{

                                                        //--- OTROS CAMPOS
                                                        pos                     = newLine.indexOf("name=\"");
                                                        String fieldName        = newLine.substring(pos+6, newLine.length()-3);
                                                        i = in.readLine(line, 0, 256);
                                                        newLine                 = new String(line, 0, i);
                                                        StringBuffer fieldValue = new StringBuffer(256);   

                                                        while ( i != -1 && !newLine.startsWith(boundary) ) {
                                                                i = in.readLine(line, 0, 256);
                                                                if ( (i == boundaryLength+2 || i==boundaryLength+4) && (new String(line,0,i).startsWith(boundary))) 
                                                                        fieldValue.append(newLine.substring(0, newLine.length()-2));
                                                                else 
                                                                        fieldValue.append(newLine.substring(0, newLine.length()-2));

                                                                newLine = new String(line, 0, i);
                                                        }

                                                        fields.put(fieldName, fieldValue.toString());
                                                }
                                        }           

                                        i = in.readLine(line, 0, 256);

                                } // end while        
                                
                                bfin = new ByteArrayInputStream( savedFile );        
                                comentario = this.insertImagen(filename, session, bfin, savedFile.length , fields, contentType);
                                
                                if( comentario.matches("1") ){
                                        String tipo = (String)fields.get("c_tipo");  
                                        String codestado = (String)fields.get("c_codestado");  
                                        String color = (String)fields.get("c_color");  
                                        String fondo =  (String)fields.get("c_fondo"); 
                                        String color_letra = (String)fields.get("c_color_letra"); 
                                        String descripcion = (String)fields.get("c_descripcion"); 

                                        Usuario usuario = (Usuario) session.getAttribute("Usuario");
                                        String distrito = (String) session.getAttribute("Distrito");
                                        String base = usuario.getBase();
                                        
                                        TblEstado cod_estado = new TblEstado();
                                        cod_estado.setCodestado(codestado);
                                        cod_estado.setTipo(tipo);
                                        cod_estado.setColor(color);
                                        cod_estado.setFondo(fondo);
                                        cod_estado.setColor_letra(color_letra);
                                        cod_estado.setDescripcion(descripcion);
                                        cod_estado.setPath(filepath);
                                        ////System.out.println("-----------> file en cod_estado=" + cod_estado.getPath());
                                        session.setAttribute("cod_estado", cod_estado);
                                        this.redired(request, response, "El c�digo de estado ya existe.");
                                }

                        }    
                }  
                        
                String pag = "/jsp/masivo/tblestado/EstadoInsert.jsp?comentario=" + comentario;
                String next = com.tsp.util.Util.LLamarVentana(pag, "Ingresar C�digo de Estado");
                RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher(next);
                dispatcher.forward(request, response);
        }catch (Exception e){ 
                throw new ServletException(e.getMessage());
        }
    
  }
  
 public void redired( HttpServletRequest request, HttpServletResponse response, String comentario) throws Exception{
        try{
                String pag = "/jsp/masivo/tblestado/EstadoInsert.jsp?comentario=" + comentario;
                String next = com.tsp.util.Util.LLamarVentana(pag, "Ingresar C�digo de Estado");
                RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher(next);
                if( dispatcher == null )                      
                        throw new Exception("No se pudo encontrar ");
                dispatcher.forward(request, response);
        }catch(Exception e){ throw new Exception(e.getMessage());}
 }
 
 
// Nombre del archivo  
 public String getName(String s){
        String filename = "";
        int pos;
        String filepath;
        String file="";    
        
        if ( s != null ) {
                pos = s.indexOf("filename=\"");                
                if (pos != -1) {
                        filepath = s.substring( pos+10, s.length()-1 );   
                        pos = filepath.lastIndexOf("\\");
                        if (pos != -1) 
                                file = filepath.substring( pos+1 );
                        else           
                                file = filepath;                  
                }
        }
        return file;
 }
 
 // Path
 public String getPath(String s){
        String filename = "";
        int pos;
        String filepath;
        String file="";    
        
        if ( s != null ) {
                pos = s.indexOf("filename=\"");                
                if (pos != -1) {
                        filepath = s.substring( pos+10, s.length()-1 );   
                        file = filepath;                  
                }
        }
        ////System.out.println("---------> file: " + file);
        return file;
 }

 // Context Type  
 public String getContext( ServletInputStream in , byte[] line) throws Exception{
        String contentType="";
        try{      
                int i  = in.readLine(line, 0, 256);                
                String s  = new String (line, 0, i-2);  
                int pos;
                if ( s != null) {
                        pos = s.indexOf(": ");
                        if ( pos != -1 ) {
                                contentType = s.substring(pos+2, s.length());
                        }
                }
        }catch(Exception e){ 
                throw new Exception(e.getMessage());
        }
        return  contentType;
 }

 public String insertImagen(String filename, HttpSession session, ByteArrayInputStream bfin, int longitud,  
        Dictionary fields, String contentType ) throws ServletException{
                
        String estado = "";
        try{ 
                String tipo = (String)fields.get("c_tipo");  
                String codestado = (String)fields.get("c_codestado");  
                
                Usuario usuario = (Usuario) session.getAttribute("Usuario");
                String distrito = (String) session.getAttribute("Distrito");
                String base = usuario.getBase();
                
                boolean existe = model.tbl_estadoSvc.existeEstado(tipo, codestado);                
                
                if( !existe ){
                        String user = usuario.getLogin();
                        model.tbl_estadoSvc.insertEstado(filename, bfin, longitud, fields, contentType, user, distrito, base );
                        estado = "C�digo de estado ingresado exitosamente.";
                }
                else{
                        estado = "1";
                }
                
                

        }catch(Exception e){ 
                //////System.out.println("--------------> error: " + e.getMessage());
                e.printStackTrace();
                estado =  "No se pudo guardar su imagen <br>" + e.getMessage() ;
                throw new ServletException(e.getMessage());
        } 
        return estado;
  }
 
 
  //_______________________________________________________________________________________________________
  
  
  // <editor-fold defaultstate="expanded" desc="Implementation of HTTP GET and POST methods.">
  /** Handles the HTTP <code>GET</code> method.
   * @param request servlet request
   * @param response servlet response
   *///GEN-BEGIN:doGet
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
  throws ServletException, IOException {
    this.doUpload(request, response);
  }//GEN-END:doGet
   
  /** Handles the HTTP <code>POST</code> method.
   * @param request servlet request
   * @param response servlet response
   *///GEN-BEGIN:doPost
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
  throws ServletException, IOException {
    this.doUpload(request, response);
  }//GEN-END:doPost
  // </editor-fold>
}
