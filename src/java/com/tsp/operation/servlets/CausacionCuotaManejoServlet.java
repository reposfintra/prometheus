/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.servlets;

/**
 * CausacionCuotaManejoServlet.java<br />
 * Sevlet para realizar el llamado al hilo que ejecuta el proceso de causacion de cuota de manejo<br />
 * @date 01/07/2016
 * @author mcastillo - FINTRA
 * @version 1.0
 */

import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.threads.HCausacionCuotaManejo;
import java.io.IOException;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



public class CausacionCuotaManejoServlet extends HttpServlet implements Servlet
{

    String ciclo = "";
    String periodo = "";

    public CausacionCuotaManejoServlet()
    {
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {     
        ciclo = request.getParameter("ciclo") != null ? request.getParameter("ciclo") : "";
        periodo=request.getParameter("periodo") != null ? request.getParameter("periodo") : "";
        this.doPost(request, response);
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try{
            Usuario usuario = new Usuario();
            usuario.setLogin("ADMIN");
            usuario.setBd("fintra");
            HCausacionCuotaManejo hilo = new HCausacionCuotaManejo(usuario);
            hilo.start(ciclo, periodo);
            hilo.join();
            System.out.println(">>> LOS PROCESOS TERMINARON EXITOSAMENTE...");
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("SE PRESENTO UN ERROR EN EL PROCESO DE CAUSACION DE CUOTA DE MANEJO..."+e.getMessage());
        }

    }

  }





