/***************************************
    * Nombre Clase ............. ViewFile.java
    * Descripci�n  .. . . . . .  cargamos las imagenes al directorio del usuario
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  10/10/2005
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/




package com.tsp.operation.servlets;




import java.io.*;
import java.lang.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.*;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.util.*;



public class ViewFile extends HttpServlet {
    
   
    
    
    
  /**
 * Recivimos los parametros enviados desde la jsp
 * @autor: ....... Fernel Villacob
 * @param ........ request, response, comentario
 * @throws ....... ServletException, IOException
 * @version ...... 1.0
 */
   protected void doUpload(HttpServletRequest request, HttpServletResponse response)   throws  ServletException, IOException {
       
    HttpSession session          = request.getSession(true);
    Model model                  = (Model) session.getAttribute("model"); 
    String newLine               = null;
    String filepath              = null;
    String filename              = null;
    String s                     = null;    
    String comentario            = "";
    int xi;
    int pos; 
    
    ServletInputStream in        = request.getInputStream();
    int Length                   = request.getContentLength();
    
    ByteArrayInputStream  bfin   = null;
    ByteArrayOutputStream bfout  = null;
    byte[] line                  = new byte[256];
    byte[] line2                 = new byte[256];
    byte[] savedFile             = null;
    String nextJsp               = "/imagen/Manejo.jsp";
    String directory             = "";
    Dictionary fields;    
    
    int Max                      = model.ImagenSvc.getTama�oImages();
     
    Usuario usuario              = (Usuario) session.getAttribute("Usuario");
    String user                  =  usuario.getLogin();
    String agencia               =  usuario.getId_agencia();
    
    model.ImagenSvc.setUser   (  user   );
    model.ImagenSvc.setAgencia( agencia );   
    
    
    String origen                = (String) session.getAttribute("swProcedencia");    
    if ( origen==null)
         origen = "NORMAL";
  
    String ruta                  =  ( origen.equals("DESPACHO") )?  model.ImagenSvc.getCarpetaDespacho() + user +"/" : model.ImagenSvc.getRutaPrevia();
    
    int cantImagenLoad           = 0;
    
   
    try{ 
       
      
        Hashtable  imagenAsociada = new Hashtable();
        List       imgNames       = new LinkedList();
        
        
        int i = in.readLine(line, 0, 256);
    
        if (i > 2) {
            int boundaryLength = i - 2;
            fields             = new Hashtable();            
            String boundary    = new String(line, 0, boundaryLength);
            
            
            if( origen.equals("NORMAL")  )
                model.ImagenSvc.createDir(ruta);
            
            
           // RECORREMOS LOS PARAMETROS       
             while ( i != -1 ) {
                 newLine = new String(line, 0, i);           
           
                if (newLine.startsWith("Content-Disposition: form-data; name=\"")) { 
                    
                         //--- EL ARCHIVO
                     
                        if (  newLine.indexOf("filename=\"")  != -1  ) {
                              s = new String(line, 0, i-2);   
                              if ( s != null ) {

                                  filename = model.ImagenSvc.getName(s);  
                                  if ( filename != null && !filename.equals("") ) {

                                      directory   = model.ImagenSvc.getDirectory( s );                                      

                                      // leemos el archivo en binario

                                      i = in.readLine(line, 0, 256);   
                                      i = in.readLine(line, 0, 256);
                                      i = in.readLine(line, 0, 256);

                                      newLine = new String(line, 0, i);                                      
                                      bfout   = new ByteArrayOutputStream();                
                                      while ( i != -1 && !newLine.startsWith(boundary) ) {
                                          line2 = ( byte[] )line.clone();
                                          xi    = i;
                                          i     = in.readLine(line, 0, 256);                  
                                          if ( new String(line, 0, i).startsWith(boundary) ) bfout.write( line2, 0, xi-2 );
                                          else                                               bfout.write( line2, 0, xi );                  
                                          newLine = new String(line, 0, i);
                                      }                
                                      savedFile = bfout.toByteArray();
                                      bfout.close();
                                      
                                      
                                      
                                     // save el file, Restringimos el tama�o:
                                      if( savedFile.length <= Max ){                                          
                                           if( model.ImagenSvc.ext(filename) ){  
                                               
                                               bfin = new ByteArrayInputStream( savedFile );  
                                               int data;
                                               File             f    = new File( ruta + filename );
                                               FileOutputStream out  = new FileOutputStream(f);
                                               while( (data = bfin.read()) != -1 )
                                                     out.write( data );
                                               bfin.close();
                                               out.close();

                                               imgNames.add( filename );
                                               cantImagenLoad++;
                                           }
                                           else
                                               comentario +=" Archivo :" + filename +" no se pudo cargar, no cumple con la extensi�n establecida <br>" ;
                                      }
                                      else
                                          comentario +=" Imagen :" + filename +" no se pudo cargar, ha excedido el tama�o maximo a subir... <br>" ;
                                      
                                     
                               } 
                           }
                      }
                      else{
                          //--- OTROS CAMPOS
                               pos                     = newLine.indexOf("name=\"");
                               String fieldName        = newLine.substring(pos+6, newLine.length()-3);
                               i = in.readLine(line, 0, 256);
                               newLine                 = new String(line, 0, i);
                               StringBuffer fieldValue = new StringBuffer(256);   

                               while ( i != -1 && !newLine.startsWith(boundary) ) {
                                   i = in.readLine(line, 0, 256);
                                   if ( (i == boundaryLength+2 || i==boundaryLength+4) && (new String(line,0,i).startsWith(boundary))) 
                                       fieldValue.append(newLine.substring(0, newLine.length()-2));
                                   else 
                                       fieldValue.append(newLine.substring(0, newLine.length()-2));

                                   newLine = new String(line, 0, i);
                                }
                                fields.put(fieldName, fieldValue.toString());
                    } 
                         
                         
                }           
                  
                i = in.readLine(line, 0, 256);
            
          } // end while  
            
            
            
      // Seteamos parametros:
           String actividad      =  (String)fields.get("actividad");  
           String tipoDocumento  =  (String)fields.get("tipoDocumento");  
           String documento      =  (String)fields.get("documento"); 
           String procedencia    =  (String)fields.get("procedencia");  
           
           
           
           model.ImagenSvc.setActividad    ( actividad     );
           model.ImagenSvc.setTipoDocumento( tipoDocumento );
           model.ImagenSvc.setDocumento    ( documento     );
           model.ImagenSvc.setProcedencia  ( procedencia   );
           
           
           
           
        // Imagenes asociadas al despacho:    
           if ( origen.equals("DESPACHO") ){               
               imagenAsociada.put("actividad",      actividad      );
               imagenAsociada.put("tipoDocumento",  tipoDocumento  );
               imagenAsociada.put("url",            ruta           );
               imagenAsociada.put("images",         imgNames       );

               model.ImagenSvc.addElement( imagenAsociada  );
               
           }
           
           
           
      //  validamos el documento en la base de datos   
           if( ! model.ImagenSvc.valido(tipoDocumento,documento)){              
               model.ImagenSvc.createDir(ruta);      // Limpiamos el directorio 
               cantImagenLoad = 0;
               comentario     = " El n�mero de documento ingresado no existe en la base de datos <br>";
           }
           
           
           
       // Lista de archivos del directorio           
          model.ImagenSvc.setImagenes( imgNames );
           
           
                      
       // Cantidad archivos cargados...
           if( cantImagenLoad>0 ) {
               model.ImagenSvc.setLoad( true  );
           }else{
               model.ImagenSvc.setLoad( false );
               comentario +=" No se pudieron subir las Imagenes....";
           }           
           nextJsp += "?procedencia="+ procedencia +"&comentario="+comentario;
           
           
        } 
  
        
        
       RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher(nextJsp);
       if( dispatcher == null )                      
           throw new Exception("No se pudo encontrar "+ nextJsp);
       dispatcher.forward(request, response);
    
    
       
       
    }catch (Exception e){ 
       model.ImagenSvc.setLoad( false );
       comentario +=" No se pudieron subir las Imagenes....";
      
       nextJsp += "?procedencia=NORMAL&evento=CLEAR&comentario="+comentario;
       RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher(nextJsp);
       dispatcher.forward(request, response);
       e.printStackTrace();
    }
    
  }
  
  
  
   
   

  
 /**
 * Sirve para direccionar a la jsp
 * @autor: ....... Fernel Villacob
 * @param ........ request, response, comentario
 * @version ...... 1.0
 */
  
 public void redired( HttpServletRequest request, HttpServletResponse response, String comentario) throws Exception{
    try{
      RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/imagen/Manejo.jsp?comentario="+ comentario);
      if( dispatcher == null )  
           throw new Exception("No se pudo encontrar ");
      dispatcher.forward(request, response);
    }catch(Exception e){ throw new Exception(e.getMessage());}
 }
 
 
 
 
 
 
 
  
  //_______________________________________________________________________________________________________
  
  
  // <editor-fold defaultstate="expanded" desc="Implementation of HTTP GET and POST methods.">
  /** Handles the HTTP <code>GET</code> method.
   * @param request servlet request
   * @param response servlet response
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
  throws ServletException, IOException {
    this.doUpload(request, response);
  }

  
  
  
  /** Handles the HTTP <code>POST</code> method.
   * @param request servlet request
   * @param response servlet response
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
  throws ServletException, IOException {
    this.doUpload(request, response);
  }
  // </editor-fold>
    
}
