/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.servlets;

/**
 * DescargaTitulosIndemServlet.java<br />
 * Sevlet para realizar el llamado al hilo que descarga los titulos que no se pueden indemnizar por dias vencidos<br />
 * @date 14/05/2012
 * @author ivargas - GEOTECH
 * @version 1.0
 */

import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.threads.HDescargaContingIndem;
import com.tsp.util.Util;
import java.io.IOException;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



public class DescargaTitulosIndemServlet extends HttpServlet implements Servlet
{ 


    public DescargaTitulosIndemServlet()
    {
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try{
            Usuario usuario = (Usuario) request.getSession().getAttribute("Usuario");
            String fecha_actual = Util.getFechaActual_String(4);
            HDescargaContingIndem hilo = new HDescargaContingIndem();
            hilo.start(usuario, fecha_actual);
            hilo.join();
            System.out.println(">>> LOS PROCESOS TERMINARON EXITOSAMENTE...");
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("SE PRESENTO UN ERROR EN LA DESCARGA DE CONTIGENCIA IDEMNIZACION..."+e.getMessage());
        }

    }

  }





