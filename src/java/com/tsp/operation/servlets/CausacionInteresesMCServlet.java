/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.servlets;

/**
 * CausacionInteresesMCServlet.java<br />
 * Sevlet para realizar el llamado al hilo que ejecuta el proceso de causacion de interes de microcreditos<br />
 * @date 15/03/2012
 * @author ivargas - GEOTECH
 * @version 1.0
 */

import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.threads.HCausacionInteresesMC;
import java.io.IOException;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;



public class CausacionInteresesMCServlet extends HttpServlet implements Servlet
{

    int caso=0;
    String ciclo = "";
    String periodo = "";

    public CausacionInteresesMCServlet()
    {
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        caso=request.getParameter("caso")!=null?Integer.parseInt(request.getParameter("caso")):0;
        ciclo = request.getParameter("ciclo") != null ? request.getParameter("ciclo") : "";
        periodo=request.getParameter("periodo") != null ? request.getParameter("periodo") : "";
        this.doPost(request, response);
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try{
            Usuario usuario = new Usuario();
            usuario.setLogin("ADMIN");
            usuario.setBd("fintra");
            HCausacionInteresesMC hilo = new HCausacionInteresesMC(usuario);
            hilo.start(caso, ciclo, periodo);
            hilo.join();
            System.out.println(">>> LOS PROCESOS TERMINARON EXITOSAMENTE...");
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("SE PRESENTO UN ERROR EN EL PROCESO DE CAUSACION DE INTERESES MICROCREDITO..."+e.getMessage());
        }

    }

  }





