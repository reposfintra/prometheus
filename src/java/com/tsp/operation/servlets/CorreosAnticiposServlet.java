/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.servlets;

/**
 *
 * @author jpinedoff
 */

import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.operation.model.beans.Usuario;

import com.tsp.util.cron.EmailAnticipos;
import javax.servlet.http.*;
import com.tsp.operation.model.threads.*;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.tsp.finanzas.contab.model.threads.HContabilizacion;
import com.tsp.finanzas.contab.model.threads.HContabilizacionCL;
import com.tsp.finanzas.contab.model.threads.HContabilizacionDiferido;
import com.tsp.finanzas.contab.model.threads.HContabilizacionEgresos;
import com.tsp.finanzas.contab.model.threads.HContabilizacionIngresos;
import com.tsp.finanzas.contab.model.threads.HContabilizacionNegocio;
import com.tsp.finanzas.contab.model.threads.HContabilizacionNotasAjuste;
import com.tsp.operation.model.beans.Usuario;
import java.text.SimpleDateFormat;
import java.util.Date;



public class CorreosAnticiposServlet extends HttpServlet implements Servlet
{
    Usuario usuario = new Usuario();
    private  boolean estadoProceso = true;
    private  String planillasConReanticipos = "";


    public CorreosAnticiposServlet()
    {
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try{
            
            HttpSession session = request.getSession();
            usuario = (Usuario) session.getAttribute("Usuario");
            EmailAnticipos Hemail_anticipos = new EmailAnticipos(usuario.getBd());
            Hemail_anticipos.start();
            Hemail_anticipos.join();
            System.out.println(">>> LOS PROCESOS TERMINARON EXITOSAMENTE...");
        }catch(Exception e){
            e.printStackTrace();
            System.out.println("SE PRESENTO UN ERROR EN EL ENVIO DE EXTRACTOS POR CORREOS..."+e.getMessage());
        }

    }

  }





