/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.servlets;

/**
 *
 * @date 01/12/2017
 * @author egonzalez - FINTRA
 * @version 1.0
 */

import com.google.gson.Gson;
import com.tsp.operation.controller.LiquidadorNegociosAction;
import com.tsp.operation.controller.NegociosTransfAction;
import com.tsp.operation.model.DAOS.AdminFintraDAO;
import com.tsp.operation.model.DAOS.InteresesFenalcoDAO;
import com.tsp.operation.model.DAOS.impl.AdminFintraImpl;
import com.tsp.operation.model.DAOS.impl.NegociosFintraImpl;
import com.tsp.operation.model.TransaccionService;
import com.tsp.operation.model.beans.ArchivosAppMicro;
import com.tsp.operation.model.beans.Convenio;
import com.tsp.operation.model.beans.DocumentosNegAceptado;
import com.tsp.operation.model.beans.Negocios;
import com.tsp.operation.model.beans.Persona;
import com.tsp.operation.model.beans.Usuario;
import com.tsp.operation.model.beans.VariablesRefinanciacion;
import com.tsp.operation.model.services.GestionConveniosService;
import com.tsp.operation.model.services.NegociosGenService;
import com.tsp.operation.model.services.WSHistCreditoService;
import com.tsp.operation.model.threads.HHistoriaDeCredito;
import com.tsp.util.Util;
import java.io.IOException;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



public class EndPointCoreServlet extends HttpServlet implements Servlet
{

    int option =0;
    private final int HISTORIA_CEDITO_FINTRA=1;
    private final int OBTENER_ARCHIVOS_MICRO=2;
    private final int GENERAR_CARTERA_MICRO=3;
    private final int ELIMINAR_ARCHIVOS_APP=4;
    private final int GENERAR_PLAN_PAGOS=5;
    private final int ACEPTAR_NEGOCIACION=6;
    private final int GENERAR_CARTERA_MICRO_REFINANCIACION=7;
    private final int GENERAR_CARTERA_EDUCATIVO=8;
    private final int GENERAR_CARTERA_LIBRANZA=9;

    public EndPointCoreServlet()
    {
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {     
       
        this.doPost(request, response);
    }


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try{            
            reponceHeaderAccessControl(response);
            option = request.getParameter("option") != null ? Integer.parseInt(request.getParameter("option")) : 0;

             switch(option){
                 case HISTORIA_CEDITO_FINTRA:                       
                      getUrlPdfhistoriaCredito(request,response);
                      break;
                 case OBTENER_ARCHIVOS_MICRO:
                     getFilesAppMicro(request, response);
                     break;                     
                case GENERAR_CARTERA_MICRO:
                     getCarteraMicro(request, response);
                     break;                     
                case ELIMINAR_ARCHIVOS_APP:
                     deleteFileApp(request, response);
                     break;     
                case GENERAR_PLAN_PAGOS:
                      generarPlanPagos(request, response);
                    break;
                case ACEPTAR_NEGOCIACION:
                    aceptarNegociacionRefinanciacion(request, response);
                    break;
                case GENERAR_CARTERA_MICRO_REFINANCIACION:
                    getCarteraMicroRefinaciacion(request, response);
                    break;
                case GENERAR_CARTERA_EDUCATIVO:
                    generarCarteraEducativo(request, response);
                    break;
                case GENERAR_CARTERA_LIBRANZA:
                    generarCarteraLibranza(request, response);
                    break;
                 default:
                     throw  new Exception("opcion errada");   
             }

        }catch(Exception e){
            e.printStackTrace();
               this.printlnResponseAjax("{\"data\":\"" +  e.getMessage() + "\",\"status\":\"200\"}",
                    "application/json;charset=UTF-8",
                    response);
        }

    }
    
    
        /**
     * Escribe la respuesta de una opcion ejecutada desde AJAX
     *
     * @param respuesta
     * @param contentType recomendado application/json;
     * @param response
     */
    public void printlnResponseAjax(String respuesta, String contentType, HttpServletResponse response) {
        try {

            response.setHeader("Access-Control-Allow-Origin", "*");
            response.setContentType(contentType);
            response.setCharacterEncoding("UTF-8");
            response.getWriter().write(respuesta);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    
    private void reponceHeaderAccessControl(HttpServletResponse response) {
        response.setContentType("text/html;charset=UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers", "x-requested-with");
    }
    
     @Override
    protected void doOptions(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            fixHeaders(response);
        } catch (Exception e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.setContentType("text/plain");
            response.getWriter().println(buildErrorMessage(e));
        } catch (Throwable e) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            response.setContentType("text/plain");
            response.getWriter().println(buildErrorMessage(e));
        }
    }

    private void fixHeaders(HttpServletResponse response) {
        response.addHeader("Access-Control-Allow-Origin", "*");
        response.addHeader("Access-Control-Allow-Methods", "GET, PUT, POST, OPTIONS, DELETE");
        response.addHeader("Access-Control-Allow-Headers", "Content-Type,X-Requested-With, Accept,Cache-Control,token,negocio");
        response.addHeader("Access-Control-Max-Age", "86400");
    }

    private static String buildErrorMessage(Exception e) {
        String msg = e.toString() + "\r\n";

        for (StackTraceElement stackTraceElement : e.getStackTrace()) {
            msg += "\t" + stackTraceElement.toString() + "\r\n";
        }

        return msg;
    }

    private static String buildErrorMessage(Throwable e) {
        String msg = e.toString() + "\r\n";

        for (StackTraceElement stackTraceElement : e.getStackTrace()) {
            msg += "\t" + stackTraceElement.toString() + "\r\n";
        }

        return msg;
    }
    
    
    private void getUrlPdfhistoriaCredito(HttpServletRequest request,HttpServletResponse response) throws Exception{
        
        String identificacion = request.getParameter("identificacion") != null ? request.getParameter("identificacion") : "0";
        String tipoIdentificacion = request.getParameter("tipoIdentificacion") != null ? request.getParameter("tipoIdentificacion") : "0";
        String username = request.getParameter("user") != null ? request.getParameter("user") : "0";
        String vista = "5";
        String resp="";
        
        
        if (identificacion.equals("0")) {
            throw new Exception("Required parameter (identificacion)");
        }
        
        if (tipoIdentificacion.equals("0")) {
             throw new Exception("Required parameter (tipoIdentificacion)");
        }
        
        if (username.equals("0")) {
             throw new Exception("Required parameter (username)");
        }

        //configuracion usuario y bd
        Usuario usuario = this.getConfigUser(username,"fintra");
        
       //Valida que la identificacion exista
        WSHistCreditoService srv = new WSHistCreditoService(usuario.getBd());
        if (srv.existePersona(new Persona(tipoIdentificacion, identificacion))) {
            HHistoriaDeCredito hilo = new HHistoriaDeCredito();
            hilo.start(usuario, tipoIdentificacion, identificacion, vista);
            hilo.join();

            resp = "{\"data\":\"http://prometheus.fintra.co:8094/fintra/exportar/migracion/" + usuario.getLogin() + "/HC_" + identificacion + ".pdf\",\"status\":\"200\"}";
        }else{
            resp = "{\"data\":\"No se encontro la informacion solicitada\",\"status\":\"200\"}";
        }
        
        System.out.println(resp);
        
        printlnResponseAjax(resp, "application/json", response);
    
    }
    
    private void getFilesAppMicro(HttpServletRequest request,HttpServletResponse response) throws Exception{
        String numero_solcitud = request.getParameter("numero_solicitud") != null ? request.getParameter("numero_solicitud") : "0";
        String username = request.getParameter("user") != null ? request.getParameter("user") : "0";
        String und_negocio = request.getParameter("und_negocio") != null ? request.getParameter("und_negocio") : "0";
        ArrayList<ArchivosAppMicro> listFiles =new ArrayList<>();
        String  resp ="{}";
        
        if (numero_solcitud.equals("0")) {
            throw new Exception("Required parameter (numero_solicitud)");
        }
        if (username.equals("0")) {
             throw new Exception("Required parameter (username)");
        }
        
        if (und_negocio.equals("0")) {
             und_negocio="1"; //se asigna 1 que es microcredito para no modificar el app 
        }
        
        //configuracion usuario y bd
        Usuario usuario = this.getConfigUser(username,"fintra");
        
        ResourceBundle rb = ResourceBundle.getBundle("com/tsp/util/connectionpool/db");
        String rutaOrigen = rb.getString("rutaImagenes") + "fintracredit1/";
        String rutaDestino = rb.getString("ruta") + "/images/multiservicios/" + usuario.getLogin() + "/";
        String dominio="http://prometheus.fintra.co:8094/fintra/images/multiservicios/";

        AdminFintraDAO dao = new AdminFintraImpl(usuario.getBd());
       // ArrayList<String> listArchivos=(ArrayList<String>)dao.searchNombresArchivos(rutaOrigen, numero_solcitud);
        
        //Busca los archivos a mostrar
        ArrayList<ArchivosAppMicro> buscarArchivosCargadosApp = dao.buscarArchivosCargadosApp(numero_solcitud,und_negocio);
        
        for (ArchivosAppMicro app : buscarArchivosCargadosApp) {
            if (!app.getNombre_archivo_real().equals("")) {
                if (app.getEstado().equals("") && dao.almacenarArchivoEnCarpetaUsuario(numero_solcitud, ("" + rutaOrigen + numero_solcitud), ("" + rutaDestino), app.getNombre_archivo_real())) {
                    resp = dominio + usuario.getLogin() + "/" + app.getNombre_archivo_real();
                    app.setUrl(resp);
                }
            }
            listFiles.add(app);
        }

        resp= "{\"data\":"+new Gson().toJson(listFiles)+",\"status\":\"200\"}";
        printlnResponseAjax(resp, "application/json", response);
    }
    
    
    private void getCarteraMicro(HttpServletRequest request,HttpServletResponse response) throws Exception{
        
        String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "0";
        String username = request.getParameter("user") != null ? request.getParameter("user") : "0";
        String  resp ="{}";
        
        if (username.equals("0")) {
             throw new Exception("Required parameter (user)");
        }        
            
        if (negocio.equals("0")) {
            throw new Exception("Required parameter (negocio)");
        }
    
        //configuracion usuario y bd);
        Usuario usuario = this.getConfigUser(username,"fintra");
        NegociosGenService genService=new NegociosGenService(usuario.getBd());  
        GestionConveniosService  conveniosService=new GestionConveniosService(usuario.getBd());
       
        Negocios neg = genService.buscarNegocio(negocio);
        Convenio convenio = conveniosService.buscar_convenio("fintra", neg.getId_convenio() + "");
        ArrayList<DocumentosNegAceptado> buscarDetallesNegocio = genService.buscarDetallesNegocio(negocio);
        genService.setLiquidacion(buscarDetallesNegocio);
        String generarCXCMicrocredito = genService.generarCXCMicrocredito(neg, usuario, convenio);
        
        if (generarCXCMicrocredito.equals("")) {
            resp = "{\"data\":\"Se genero la cartera del negocio " + negocio + "\",\"status\":\"200\"}";
        }
        
        printlnResponseAjax(resp, "application/json", response);
        
    }
    
    private void deleteFileApp(HttpServletRequest request,HttpServletResponse response) throws Exception{
    
        String numero_solcitud = request.getParameter("numero_solicitud") != null ? request.getParameter("numero_solicitud") : "0";
        String username = request.getParameter("user") != null ? request.getParameter("user") : "0";
        String id_archivo = request.getParameter("id_archivo") != null ? request.getParameter("id_archivo") : "0";
        String  resp ="{}";
        
        if (numero_solcitud.equals("0")) {
            throw new Exception("Required parameter (numero_solicitud)");
        }
        if (username.equals("0")) {
             throw new Exception("Required parameter (username)");
        }
        if (id_archivo.equals("0")) {
             throw new Exception("Required parameter (id_archivo)");
        }
        
        Usuario usuario = this.getConfigUser(username,"fintra");
        AdminFintraDAO dao = new AdminFintraImpl(usuario.getBd());
        dao.updateFilesAppMIcro(numero_solcitud, id_archivo, usuario);        
        getFilesAppMicro(request, response);
    
    }
    
    private Usuario getConfigUser(String username, String bd){
    
        Usuario usuario = new Usuario();
        usuario.setLogin(username);
        usuario.setBd(bd);
        usuario.setBase("COL");
        usuario.setDstrct("FINV");
      
        return usuario;
    }
    
    private void generarPlanPagos(HttpServletRequest request,HttpServletResponse response) throws Exception{
        
        String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "0";
        String fechapr = request.getParameter("fechapr") != null ? request.getParameter("fechapr") : "0";
        String numsolc = request.getParameter("numsolc") != null ? request.getParameter("numsolc") : "0";        
        String username = request.getParameter("user") != null ? request.getParameter("user") : "0";
        String resp = "{\"success\":false,\"data\":\"Algo salio mal al generar el documento\",\"status\":\"200\"}";
        
        if (username.equals("0")) {
             throw new Exception("Required parameter (user)");
        }                    
        if (negocio.equals("0")) {
            throw new Exception("Required parameter (negocio)");
        }
        if (numsolc.equals("0")) {
            throw new Exception("Required parameter (numsolc)");
        }
        if (fechapr.equals("0")) {
            throw new Exception("Required parameter (fechapr)");
        }
    
        //configuracion usuario y bd
        Usuario usuario = this.getConfigUser(username,"fintra");
        
        String url="http://prometheus.fintra.co:8094/fintra/exportar/migracion/"+ usuario.getLogin().toUpperCase() + "/f_plan_de_pagos" + ".pdf";
        
        LiquidadorNegociosAction lna=new LiquidadorNegociosAction(request,usuario,negocio,fechapr);
        if(lna.exportarPlanPagosFintra(usuario.getLogin()))
              resp = "{\"success\":true,\"data\":\"" + url + "\",\"status\":\"200\"}";
        
        
        printlnResponseAjax(resp, "application/json", response);
    
    
    }
    
    public void aceptarNegociacionRefinanciacion(HttpServletRequest request, HttpServletResponse response) throws Exception {
        
        String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "";
        String tipoRefi = request.getParameter("tipo_refi") != null ? request.getParameter("tipo_refi") : "";
        String fechaPrimeraCuota = request.getParameter("fecha_primera_cuota") != null ? request.getParameter("fecha_primera_cuota") : "";
        int plazo = request.getParameter("cuota") != null ? Integer.parseInt(request.getParameter("cuota")) : 0;
        String fechaProyeccion = request.getParameter("fecha_proyeccion") != null ? request.getParameter("fecha_proyeccion") : "";
        int cuota_inicio = request.getParameter("cuota_inicio") != null ? Integer.parseInt(request.getParameter("cuota_inicio")) : 0;
        String ciudad = request.getParameter("ciudad") != null ? request.getParameter("ciudad") : "";
        String compraCartera = request.getParameter("compra_cartera") != null ? request.getParameter("compra_cartera") : "";
        double porce = request.getParameter("porcentaje") != null ? Double.parseDouble(request.getParameter("porcentaje")) : 0;
        boolean isPago= request.getParameter("ispago") != null ? Boolean.parseBoolean(request.getParameter("ispago")) : true;
        double interes = request.getParameter("interes") != null ? Double.parseDouble(request.getParameter("interes")) : 0;
        double catVencido = request.getParameter("cat_vencido") != null ? Double.parseDouble(request.getParameter("cat_vencido")) : 0;
        double cuotaAdmon = request.getParameter("cuota_admin") != null ? Double.parseDouble(request.getParameter("cuota_admin")) : 0;
        double intMora = request.getParameter("intxmora") != null ? Double.parseDouble(request.getParameter("intxmora")) : 0;
        double gastoCobranza = request.getParameter("gasto_cobranza") != null ? Double.parseDouble(request.getParameter("gasto_cobranza")) : 0;
        double pagoInicial = request.getParameter("pago_inicial") != null ? Double.parseDouble(request.getParameter("pago_inicial")) : 0;
        double capitalRefinanciacion = request.getParameter("capital_refin") != null ? Double.parseDouble(request.getParameter("capital_refin")) : 0;
        double saldoCat = request.getParameter("saldo_cat") != null ? Double.parseDouble(request.getParameter("saldo_cat")) : 0;
        
       
        
        String token = request.getParameter("token") != null ? request.getParameter("token") : "0";
        if (token.equals("0")) {
             throw new Exception("Required parameter (user)");
        }                    
        if (negocio.equals("0")) {
            throw new Exception("Required parameter (negocio)");
        }
        VariablesRefinanciacion refinanciacion = 
                new VariablesRefinanciacion(negocio, tipoRefi, fechaPrimeraCuota, plazo, fechaProyeccion, cuota_inicio,
                        ciudad, compraCartera, porce, isPago, interes, catVencido, cuotaAdmon, intMora,
                        gastoCobranza, pagoInicial, capitalRefinanciacion, saldoCat,0,0,"",0);

        Usuario usuario = this.getConfigUser(token,"fintra");
        NegociosFintraImpl ng =new NegociosFintraImpl(usuario.getBd());
        usuario=ng.validarUserToken(token);
        String rep=ng.proyeccion_refinanciacion_negocios(refinanciacion, usuario);
        printlnResponseAjax(rep, "application/json", response);
    }

    private void getCarteraMicroRefinaciacion(HttpServletRequest request, HttpServletResponse response) throws Exception{
        String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "0";
        String username = request.getParameter("user") != null ? request.getParameter("user") : "0";
        String  resp ="{}";
        
        if (username.equals("0")) {
             throw new Exception("Required parameter (user)");
        }        
            
        if (negocio.equals("0")) {
            throw new Exception("Required parameter (negocio)");
        }
    
        //configuracion usuario y bd);
        Usuario usuario = this.getConfigUser(username,"fintra");
        NegociosGenService genService=new NegociosGenService(usuario.getBd());  
        GestionConveniosService  conveniosService=new GestionConveniosService(usuario.getBd());
       
        Negocios neg = genService.buscarNegocio(negocio);
        neg.setNodocs(neg.getNro_docs_ref());
        neg.setFecha_neg(neg.getSys_date());
        neg.setTipo_liq("REFINANCIACION");
        Convenio convenio = conveniosService.buscar_convenio("fintra", neg.getId_convenio() + "");
        ArrayList<DocumentosNegAceptado> buscarDetallesNegocio = genService.buscarDetallesNegocio(negocio,"REFINANCIACION");
        genService.setLiquidacion(buscarDetallesNegocio);
        neg.setNodocs(buscarDetallesNegocio.size());
        String generarCXCMicrocredito = genService.generarCXCMicrocredito(neg, usuario, convenio);
        
        if (generarCXCMicrocredito.equals("")) {
            resp = "{\"data\":\"Se genero la cartera del negocio " + negocio + "\",\"status\":\"200\"}";
        }
        
        printlnResponseAjax(resp, "application/json", response);
    }

    private void generarCarteraEducativo(HttpServletRequest request, HttpServletResponse response) throws Exception {

        String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "0";
        String username = request.getParameter("user") != null ? request.getParameter("user") : "0";
        String tipo_liq = request.getParameter("tipo_liq") != null ? request.getParameter("tipo_liq") : "0";
        boolean diferido = request.getParameter("diferido") != null ? Boolean.parseBoolean(request.getParameter("diferido")) : false;

        if (negocio.equals("0")) {
            throw new Exception("Required parameter (negocio)");
        }
        if (username.equals("0")) {
            throw new Exception("Required parameter (username)");
        }
        if (tipo_liq.equals("0")) {
            throw new Exception("Required parameter (tipo_liq)");
        }
        if (request.getParameter("diferido") == null) {
            throw new Exception("Required parameter (tipo_liq)");
        }

        String resp = "{}";
        Usuario usuario = this.getConfigUser(username, "fintra");
        NegociosGenService negs = new NegociosGenService(usuario.getBd());
        GestionConveniosService conveniosService = new GestionConveniosService(usuario.getBd());
        NegociosTransfAction transf = new NegociosTransfAction();

        Negocios negocioF = null;
        try {
            negocioF = negs.buscarNegocio(negocio);
            Convenio convenioF = conveniosService.buscar_convenio("fintra", negocioF.getId_convenio() + "");

            ArrayList<String> listQuery = new ArrayList<>();
            negocioF.setTipo_liq(tipo_liq);
            negocioF.setFecha_neg(Util.getFechahoy());

            //Inserta los ingresos diferidos
            if (diferido) {
                InteresesFenalcoDAO intdao = new InteresesFenalcoDAO(usuario.getBd());
                listQuery.addAll(intdao.insertarInteresIFApi(negocioF.getCod_negocio(), negocioF.getCod_cli(), usuario.getLogin(), convenioF.getPrefijo_diferidos(), convenioF.getHc_diferidos(), tipo_liq));
            }

            TransaccionService tService = new TransaccionService(usuario.getBd());
            tService.crearStatement();
            listQuery.addAll(transf.generarDocumentosFenalco(negocioF, convenioF, tService, usuario));
            for (String sql : listQuery) {
                tService.getSt().addBatch(sql);
            }
            tService.execute();

            resp = "{\"data\":\"Se genero la cartera del negocio " + negocio + "\",\"status\":\"200\"}";

        } catch (Exception ex) {
            Logger.getLogger(EndPointCoreServlet.class.getName()).log(Level.SEVERE, null, ex);
            resp = "{\"data\":\"Error al generar la cartera del negocio " + negocio + "\",\"status\":\"500\"}";
        }
        printlnResponseAjax(resp, "application/json", response);
    }
  
    private void generarCarteraLibranza(HttpServletRequest request, HttpServletResponse response) throws Exception {
        String negocio = request.getParameter("negocio") != null ? request.getParameter("negocio") : "0";
        String username = request.getParameter("user") != null ? request.getParameter("user") : "0";
        if (negocio.equals("0")) {
            throw new Exception("Required parameter (negocio)");
        }
        if (username.equals("0")) {
            throw new Exception("Required parameter (username)");
        }

        String resp = "{}";
        try {
            Usuario usuario = this.getConfigUser(username, "fintra");
            NegociosGenService negs = new NegociosGenService(usuario.getBd());
            boolean carteraGenerada = negs.generarCarteraLibranzas(negocio, usuario);
            if (carteraGenerada) {
                resp = "{\"data\":\"Se genero la cartera del negocio " + negocio + "\",\"status\":\"200\"}";
            } else {
                resp = "{\"data\":\"No se pudo generar la cartera del negocio " + negocio + "\",\"status\":\"500\"}";
            }
        } catch (Exception e) {
            Logger.getLogger(EndPointCoreServlet.class.getName()).log(Level.SEVERE, null, e);
            resp = "{\"data\":\"Error al generar la cartera del negocio " + negocio + "\",\"status\":\"500\"}";
        }
        
        printlnResponseAjax(resp, "application/json", response);
    }


    private class BeansFilesApp{
        
        private String name_file;
        private String url;

        public BeansFilesApp() {
        }
       
        public String getNameFile() {
            return name_file;
        }

        public void setNameFile(String nameFile) {
            this.name_file = nameFile;
        }

        public String getUrl() {
            return url;
        }

        public void setUrl(String url) {
            this.url = url;
        }
    }
  }





