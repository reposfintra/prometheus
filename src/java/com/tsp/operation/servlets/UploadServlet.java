/***************************************
    * Nombre Clase ............. UploadServlet.java
    * Descripci�n  .. . . . . .  Subimos imagenes a la base de datos
    * Autor  . . . . . . . . . . FERNEL VILLACOB DIAZ
    * Fecha . . . . . . . . . .  10/10/2005
    * versi�n . . . . . . . . .  1.0
    * Copyright ...............  Transportes Sanchez Polo S.A.
    *******************************************/


package com.tsp.operation.servlets;

import java.io.*;
import java.sql.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;
import com.tsp.util.connectionpool.*;
import com.tsp.operation.model.*;




public class UploadServlet extends HttpServlet {
    
      private        Model  model;
      
      /**
     * Guardamos las imagenes a la base de datos
     * @autor: ....... Fernel Villacob
     * @param ........ HttpServletRequest request, HttpServletResponse response
     * @throws ....... ServletException, IOException
     * @version ...... 1.0
     */
  
      protected void doUpload(HttpServletRequest request, HttpServletResponse response)
      throws  ServletException, IOException {

          
        String comentario   = "";
        String parameter    = "";
        
        try{
            HttpSession session          = request.getSession(true);
            model                        = (Model) session.getAttribute("model");    


        // Obtenemos parametros
               
            String actividad    = model.ImagenSvc.getActividad();
            String tipoDoc      = model.ImagenSvc.getTipoDocumento();
            String documento    = model.ImagenSvc.getDocumento();
            
            String agencia      = model.ImagenSvc.getAgencia();
            String user         = model.ImagenSvc.getUser(); 
            
            
        // Save the parameter
            Dictionary fields   = new Hashtable();            
            fields.put("actividad",     actividad );
            fields.put("tipoDocumento", tipoDoc   );
            fields.put("documento",     documento );
            fields.put("agencia",       agencia   );
            fields.put("usuario",       user      );
                        
            
       // Leemos las Imagenes para Montarlas    
            String ruta               = model.ImagenSvc.getRutaPrevia();
            File  file                = new File( ruta );
            
            
            if(file.exists()){
               File []arc =  file.listFiles();
               for (int i=0;i< arc.length;i++)
                     if( ! arc[i].isDirectory()){
                         
                           String name = arc[i].getName();
                           if(  model.ImagenSvc.isLoad( model.ImagenSvc.getImagenes(), name)   && model.ImagenSvc.ext(name) ){ 
                               
                                  FileInputStream       in  = new FileInputStream(file  +"/"+  name);
                                  ByteArrayOutputStream out = new ByteArrayOutputStream();
                                  ByteArrayInputStream  bfin;
                                  int input                 = in.read();
                                  byte[] savedFile          = null;
                                  while(input!=-1){             
                                      out.write(input);
                                      input  = in.read();
                                  }
                                  out.close();
                                  savedFile  = out.toByteArray();
                                  bfin       = new ByteArrayInputStream( savedFile ); 

                                  fields.put("imagen", name ); 

                              // Insertar imagen  
                                 comentario += this.insertImagen(bfin, savedFile.length ,  fields);
                         
                           } 
                     }
            }
            
            model.ImagenSvc.reset();
            
                
     // parametros de estado de insercion de Imagen      
           parameter+="&statusInsert="+model.ImagenSvc.getEstado();
    
           //********************** Nuevo Diogenes Bastidas 18.11.05 ***************************
           if(model.ImagenSvc.getEstado()){
               actualizarDocumento(actividad, tipoDoc, documento );  
           }
          //***************************************************************************
           
           
    
        this.redired(request, response,  comentario + parameter);      
  
    }catch (Exception e){ 
       throw new ServletException(e.getMessage());
    }
    
  }
  
  
   
 
      
      
      
 
      
      
      
      
 /**
 * Direccionamos a la jsp
 * @autor: .......  Fernel Villacob
 * @param ........  HttpServletRequest request, HttpServletResponse response, String comentario
 * @throws .......  Exception
 * @version ......  1.0
 */
  
 public void redired( HttpServletRequest request, HttpServletResponse response, String comentario) throws Exception{
    try{
      RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher( "/imagen/Manejo.jsp?comentario="+ comentario);
      if( dispatcher == null )                      
          throw new Exception("No se pudo encontrar ");
      dispatcher.forward(request, response);
    }catch(Exception e){ throw new Exception(e.getMessage());}
 }
  
 
 
 
 
  /**
 * Insertamos la imagen a la base de datos
 * @autor: ....... Fernel Villacob
 * @param ........ ByteArrayInputStream bfin, int longitud,  Dictionary fields
 * @throws ....... ServletException
 * @version ...... 1.0
 */
  
  public String insertImagen(ByteArrayInputStream bfin, int longitud,  Dictionary fields) throws ServletException{
     String estado   = "";     
     String fileName = (String)fields.get("imagen"); 
     try{        
         
        model.ImagenSvc.setEstado(false);
        model.ImagenSvc.insertImagen(bfin,longitud,fields); 
        estado = "Imagen "+ fileName +" ha sido guardada <br>";
        model.ImagenSvc.setEstado(true);  
        
     }catch(Exception e){ 
         estado =  "No se pudo guardar la imagen "+  fileName  +"<br>" + e.getMessage() ;
     } 
     return estado;
  }
  
  
  
  
  
  
  

  //********************** < Diogenes > *********************
   /**
     * actualizarDocumento, Actualiza el campo correspondiente al tipo
     * codigo del documento, colocandolo  en 'S' si adjunto la imagen
     *    
     * @autor: ....... Diogenes Bastidas
     * @param ........ codigo actividad, tipo documento y documento 
     * @throws ....... ServletException, IOException
     * @version ...... 1.0
     */     
  public void actualizarDocumento(String codact, String tipodoc, String doc ) throws Exception{
      try{
          //Modificado diogenes 07-03-2006
          if( codact.equals("003") || codact.equals("015") || codact.equals("016") ){          
              model.veridocService.tieneDocumentosConductor(doc,tipodoc);
          }
          else if (codact.equals("005")){
              model.veridocService.tieneDocumentosPlaca(doc,tipodoc);
          }
      }catch (Exception e){ 
           throw new ServletException(e.getMessage());
       }
      
  }
  
  
  
  
  
  //_______________________________________________________________________________________________________
  
  
  // <editor-fold defaultstate="expanded" desc="Implementation of HTTP GET and POST methods.">
  /** Handles the HTTP <code>GET</code> method.
   * @param request servlet request
   * @param response servlet response
   *///GEN-BEGIN:doGet
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
  throws ServletException, IOException {
    this.doUpload(request, response);
  }//GEN-END:doGet

  
  
  
  /** Handles the HTTP <code>POST</code> method.
   * @param request servlet request
   * @param response servlet response
   *///GEN-BEGIN:doPost
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
  throws ServletException, IOException {
    this.doUpload(request, response);
  }//GEN-END:doPost
  // </editor-fold>
}
