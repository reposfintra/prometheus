/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.tsp.operation.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.tsp.finanzas.contab.model.threads.HContabilizacion;
import com.tsp.finanzas.contab.model.threads.HContabilizacionCL;
import com.tsp.finanzas.contab.model.threads.HContabilizacionDiferido;
import com.tsp.finanzas.contab.model.threads.HContabilizacionEgresos;
import com.tsp.finanzas.contab.model.threads.HContabilizacionIngresos;
import com.tsp.finanzas.contab.model.threads.HContabilizacionNegocio;
import com.tsp.finanzas.contab.model.threads.HContabilizacionNotasAjuste;
import com.tsp.operation.model.beans.Usuario;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author maltamiranda
 */
public class ContabilizacionServlet extends HttpServlet implements Servlet  {


    private final String USUARIO = "DPALLARES";
    private PrintWriter out = null;


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/plain; charset=utf-8");
        this.out = response.getWriter();
        this.out.println(">>> INICIALIZANDO CONTABILIZACION AUTOMATICA...");
        this.out.println("----------------------------------------------------------------------");
        Usuario usuario = (Usuario) request.getSession().getAttribute("Usuario");
        if(usuario == null) {
            usuario = new Usuario();
            usuario.setLogin(USUARIO);
            usuario.setDstrct("FINV");
            usuario.setBd(""); //base de datos para contabilizacion de negocios
        }
        com.tsp.finanzas.contab.model.Model model_finanzas = new com.tsp.finanzas.contab.model.Model(usuario.getBd());

        try{
            SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd");
            this.out.println("FECHA:"+df.format(new Date()));
            this.out.println(">>> LOS PROCESOS TERMINARON EXITOSAMENTE...");
            HContabilizacionNegocio hn=new HContabilizacionNegocio();
            hn.start(model_finanzas, usuario,df.format(new Date()));
            hn.join();

            this.out.println(">>> INICIO PROCESO DE CONTABILIZACION DE FACTURAS CXC...");
            HContabilizacion hfc=new HContabilizacion();
            hfc.start(model_finanzas, usuario);
            hfc.join();

            this.out.println(">>> INICIO PROCESO DE CONTABILIZACION DE FACTURAS CXP...");
            HContabilizacionCL hfp=new HContabilizacionCL();
            hfp.start(model_finanzas, usuario);
            hfp.join();

            this.out.println(">>> INICIO PROCESO DE CONTABILIZACION DE INGRESOS...");
            HContabilizacionIngresos hi=new HContabilizacionIngresos();
            hi.start(usuario,model_finanzas.ContabilizacionIngresosSvc);
            hi.join();

            this.out.println(">>> INICIO PROCESO DE CONTABILIZACION DE NOTAS DE AJUSTE...");
            HContabilizacionNotasAjuste hnas=new HContabilizacionNotasAjuste();
            hnas.start(usuario,model_finanzas.ContabilizacionNotasAjusteSvc);
            hnas.join();

            this.out.println(">>> INICIO PROCESO DE CONTABILIZACION DE EGRESOS...");
            String tipoDoc= model_finanzas.comprobanteService.getTipoDocumento("003");
            HContabilizacionEgresos heg=new HContabilizacionEgresos();
            heg.start(usuario,tipoDoc,model_finanzas.comprobanteService.getMoneda(usuario.getDstrct()));
            heg.join();

            HContabilizacionDiferido hd=new HContabilizacionDiferido();

            //hd.start(model_finanzas, usuario,df.format(new Date()));
            java.util.Calendar calendar = java.util.Calendar.getInstance();
            calendar.add(java.util.Calendar.DAY_OF_MONTH, -1);
            Date ayerx = calendar.getTime();
            hd.start(model_finanzas, usuario,df.format(ayerx));

            hd.join();
            this.out.println(">>> LOS PROCESOS TERMINARON EXITOSAMENTE...");
        }catch(Exception e){
            e.printStackTrace();
            this.out.println("SE PRESENTO UN ERROR EN LA CONTABILIZACION AUTOMATICA DE DOCUMENTOS..."+e.getMessage());
        }

    }
}