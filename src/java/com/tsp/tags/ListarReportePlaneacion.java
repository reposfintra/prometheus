/*
 * ListarReportePlaneacion.java
 *
 * Created on July 5, 2005, 7:47 PM
 */

package com.tsp.tags;

import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import com.tsp.operation.model.Model;
import java.util.Vector;
import com.tsp.operation.model.beans.Agencia;
import com.tsp.operation.model.beans.ReportePlaneacion;
import com.tsp.operation.controller.GenerarReporteDiarioAction;
import com.tsp.util.Util;

/**
 *
 * @author  Alejandro
 * @version
 */

public class ListarReportePlaneacion extends SimpleTagSupport {
    
    /**
     * Initialization of agencia property.
     */
    private java.lang.String agencia;
    
    /**
     * Initialization of fechaInicio property.
     */
    private java.lang.String fechaInicio;
    
    /**
     * Initialization of fechaFin property.
     */
    private java.lang.String fechaFin;
    
    /**
     * Called by the container to invoke this tag.
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    public void doTag() throws JspException {
        
        JspWriter out = getJspContext().getOut();
        
        try {
            Model model = new Model();
            //model.ipredoSvc.llenarInforme(fechaInicio,fechaFin,agencia);
            model.ipredoSvc.llenarInforme(fechaInicio,fechaFin);
            Vector datos = model.ipredoSvc.getDatos();
            if ( datos == null || datos.isEmpty() ) {
                out.println( "    <p><strong><span class=\"Estilo11\">No fue encontrado ningun recurso ni requerimiento entre las fechas dadas</span></strong></p>" );
            }
            else {
                
                /*************************** NUEVA VISTA ********************************/
                
                out.println( "<table width=\"1500\" border=\"0\">");
                out.println( "  <tr>");
                out.println( "    <td><div align=\"center\"><strong>TRANSPORTES SANCHEZ POLO </strong></div></td>");
                out.println( "  </tr>");
                out.println( "  <tr>");
                out.println( "    <td><div align=\"center\"><strong>Planeaci&oacute;n y programaci&oacute;n diaria de la operaci&oacute;n </strong></div></td>");
                out.println( "  </tr>");
                out.println( "  <tr>");
                out.println( "    <td><div align=\"center\">Fecha del reporte: "+Util.getFechaActual_String( 6 )+"</div></td>");
                out.println( "  </tr>");
                out.println( "  <tr>");
                out.println( "    <td>&nbsp;</td>");
                out.println( "  </tr>");
                out.println( "  <tr>");
                out.println( "    <td><table width=\"2446\" border=\"1\">");
                out.println( "      <tr>");
                out.println( "        <td colspan=\"6\" bgcolor=\"#FFE084\"><div align=\"center\"><strong>REQUERIMIENTOS</strong></div></td>");
                out.println( "        <td colspan=\"8\" bgcolor=\"#A6C4E1\"><div align=\"center\"><strong>RECURSOS</strong></div></td>");
                out.println( "        </tr>");
                out.println( "      <tr>");
                out.println( "        <td width=\"96\"><div align=\"center\" class=\"Estilo1\">FECHA</div></td>");
                out.println( "        <td width=\"46\" bgcolor=\"#FF9900\"><div align=\"center\"><span class=\"Estilo1 Estilo2\">ITEM</span></div></td>");
                out.println( "        <td width=\"171\" bgcolor=\"#FF9900\"><div align=\"center\" class=\"Estilo11\">FECHA DISPONIBILIDAD </div></td>");
                out.println( "        <td width=\"270\" bgcolor=\"#FF9900\"><div align=\"center\" class=\"Estilo1 Estilo2\">TIPO</div></td>");
                out.println( "        <td width=\"65\" bgcolor=\"#FF9900\"><div align=\"center\" class=\"Estilo10\">ESTANDAR</div></td>");
                out.println( "        <td width=\"373\" bgcolor=\"#FF9900\"><div align=\"center\" class=\"Estilo10\">DESCRIPCION</div></td>");
                out.println( "        <td width=\"57\" bgcolor=\"#4D6185\"><div align=\"CENTER\"><span class=\"Estilo3\">ITEM</span></div></td>");
                out.println( "        <td width=\"62\" bgcolor=\"#4D6185\"><div align=\"center\"><span class=\"Estilo3\">GUARDAR</span></div></td>");
                out.println( "        <td width=\"141\" bgcolor=\"#4D6185\"><div align=\"center\"><span class=\"Estilo12\">FECHA DISPONIBILIDAD </span></div></td>");
                out.println( "        <td width=\"364\" bgcolor=\"#4D6185\"><div align=\"center\" class=\"Estilo3\">DESCRIPCION</div></td>");
                out.println( "        <td width=\"194\" bgcolor=\"#4D6185\"><div align=\"center\" class=\"Estilo3\">ORIGEN</div></td>");
                out.println( "        <td width=\"202\" bgcolor=\"#4D6185\"><div align=\"center\" class=\"Estilo3\">DESTINO</div></td>");
                out.println( "        <td width=\"40\" bgcolor=\"#4D6185\"><div align=\"center\" class=\"Estilo3\">PLACA</div></td>");
                out.println( "        <td width=\"277\" bgcolor=\"#4D6185\"><div align=\"center\" class=\"Estilo3\">TIPO RECURSO</div></td>");
                out.println( "      </tr>");
                boolean tieneRequerimiento = false;
                for (int i = 0,c=0,recursos=1; i < datos.size(); i++) {
                    tieneRequerimiento = false;
                    ReportePlaneacion rep = ( ReportePlaneacion ) datos.elementAt( i );
                    String colorReq = "#FFE084";
                    String colorRec = "#A6C4E1";
                    if ( rep.getStd_job_no().length() > 0 && rep.getPlaca().length() > 0 ){
                        out.println( "      <tr bgcolor=\"#B0E17B\">");
                        colorReq = colorRec = "#B0E17B";
                    }
                    else {
                        out.println( "      <tr>");
                    }
                    out.println( "        <td bgcolor=\"#FFFFFF\"><div align=\"center\" class=\"Estilo16\">"+rep.getFecha_dispxag()+"</div></td>");
                    if ( rep.getStd_job_no().length() > 0 ){
                        c++;
                        // campos ocultos
                        out.println("<input name=\"C"+(c)+"dstrct_code\" type=hidden value=\""+rep.getDstrct_code()+"\">");
                        out.println("<input name=\"C"+(c)+"num_sec\" type=hidden value=\""+rep.getNum_sec()+"\">");
                        out.println("<input name=\"C"+(c)+"std_job_no\" type=hidden value=\""+rep.getStd_job_no()+"\">");
                        out.println("<input name=\"C"+(c)+"fecha_dispo\" type=hidden value=\""+rep.getFechadispC()+"\">");
                        out.println("<input name=\"C"+(c)+"numpla\" type=hidden value=\""+rep.getNumpla()+"\">");
                        out.println("<input name=\"filaC"+c+"\" type=hidden value=\""+c+"\">");
                        
                        out.println( "        <td bgcolor=\"#6DAD27\"><div align=\"center\" class=\"Estilo15 Estilo2 Estilo16\">C"+c+"</div></td>");
                        out.println( "        <td bgcolor=\""+colorReq+"\"><span class=\"Estilo16\">"+rep.getFechadispC()+"</span></td>");
                        out.println( "        <td bgcolor=\""+colorReq+"\"><span class=\"Estilo17\">"+rep.getRecurso_req()+"</span></td>");
                        out.println( "        <td bgcolor=\""+colorReq+"\"><span class=\"Estilo17\">"+rep.getStd_job_no()+"</span></td>");
                        out.println( "        <td bgcolor=\""+colorReq+"\"><span class=\"Estilo17\">"+rep.getStd_job_desc_req()+"</span></td>");
                        tieneRequerimiento = true;
                    }
                    else {
                        out.println( "        <td><span class=\"Estilo16\"></span></td>");
                        out.println( "        <td><span class=\"Estilo16\"></span></td>");
                        out.println( "        <td><span class=\"Estilo16\"></span></td>");
                        out.println( "        <td><span class=\"Estilo16\"></span></td>");
                        out.println( "        <td><span class=\"Estilo16\"></span></td>");
                    }
                    if ( rep.getPlaca().length() > 0 ){
                        // campos ocultos
                        out.println("<input name=\"R"+recursos+"dstrct\" type=hidden value=\""+rep.getDstrct()+"\">");
                        out.println("<input name=\"R"+recursos+"placa\" type=hidden value=\""+rep.getPlaca()+"\">");
                        out.println("<input name=\"R"+recursos+"fecha_disp\" type=hidden value=\""+rep.getFechadispR()+"\">");
                        out.println("<input name=\"filaR"+c+"\" type=hidden value=\""+c+"\">");
                        
                        out.println( "        <td><div align=\"center\" class=\"Estilo16\">");
                        out.println( "          <input name=\"asignado\" value=\""+(tieneRequerimiento?"C"+c:"")+"\" type=\"text\" size=\"9\" alt=\"PARA ASIGNAR ESTE RECURSO A UN REQUERIMIENTO DIGITE EL NUMERO DEL ITEM\">");
                        out.println( "        </div></td>");
                        
                        out.println( "        <td bgcolor=\""+colorRec+"\"><div align=\"center\" class=\"Estilo16\">");
                        out.println( "          <input type=\"submit\" name=\"Submit\" value=\"Guardar\">");
                        out.println( "        </div></td>");
                        
                        out.println( "        <td bgcolor=\""+colorRec+"\"><span class=\"Estilo16\">"+rep.getFechadispR()+"</span></td>");
                        out.println( "        <td bgcolor=\""+colorRec+"\"><span class=\"Estilo17\">"+rep.getStd_job_desc_rec()+"</span></td>");
                        out.println( "        <td bgcolor=\""+colorRec+"\"><span class=\"Estilo17\">"+rep.getOrigenR()+"</span></td>");
                        out.println( "        <td bgcolor=\""+colorRec+"\"><span class=\"Estilo17\">"+rep.getDestinoR()+"</span></td>");
                        out.println( "        <td bgcolor=\""+colorRec+"\"><span class=\"Estilo17\">"+rep.getPlaca()+"</span></td>");
                        out.println( "        <td bgcolor=\""+colorRec+"\"><span class=\"Estilo17\">"+rep.getClase()+"</span></td>");
                        recursos++;
                    }
                    else {
                        out.println( "        <td><span class=\"Estilo16\"></span></td>");
                        out.println( "        <td><span class=\"Estilo16\"></span></td>");
                        out.println( "        <td><span class=\"Estilo16\"></span></td>");
                        out.println( "        <td><span class=\"Estilo16\"></span></td>");
                        out.println( "        <td><span class=\"Estilo16\"></span></td>");
                        out.println( "        <td><span class=\"Estilo16\"></span></td>");
                        out.println( "        <td><span class=\"Estilo16\"></span></td>");
                        out.println( "        <td><span class=\"Estilo16\"></span></td>");
                    }
                    out.println( "      </tr>");
                }
                out.println( "    </table></td>");
                out.println( "  </tr>");
                out.println( "</table>");
                
                out.println( "<br>" );
                out.println( "<div align=\"center\">" );
                out.println( "<input type=\"submit\" name=\"Submit\" value=\"Guardar\">" );
                out.println( "</div>" );
                /*************************** VISTA ANTERIOR **************************
                out.println( "<table width=\"100%\" border=\"1\" align=\"center\" cellpadding=\"3\" cellspacing=\"2\" bordercolor=\"#CCCCCC\" bgcolor=\"#EFE3DE\" class=\"Letras\">" );
                out.println( "<tr bgcolor=\"#FFA928\">" );
                out.println( "  <td colspan=\"2\" nowrap><div align=\"center\" class=\"Estilo2\"><strong><strong>EDITAR REPORTE DIARIO DE LA PLANEACIÓN Y OPERACIÓN</strong></strong></div></td>" );
                out.println( "</tr>" );
                out.println( "<tr>" );
                out.println( "  <td width=\"48%\" bgcolor=\"#FFCC00\" nowrap><div align=\"center\" class=\"Estilo2\"><strong><strong>REQUERIMIENTOS</strong></strong></div></td>" );
                out.println( "  <td width=\"52%\" nowrap><div align=\"center\"><span class=\"Estilo2\"><strong><strong>RECURSOS</strong></strong></span></div></td>" );
                out.println( "</tr>" );
                //boolean tieneRequerimiento = false;
                for (int i = 0,c=0,recursos=0; i < datos.size(); i++) {
                    ReportePlaneacion rep = ( ReportePlaneacion ) datos.elementAt( i );
                    out.println( "<tr>" );
                    if ( rep.getNumpla().length() > 0 && rep.getPlaca().length() > 0 ){
                        out.println( "  <td bgcolor=\"#66CC99\" nowrap>");
                        
                    }
                    else {
                        out.println( "  <td bgcolor=\"#FFCC00\" nowrap>" );
                    }
                    tieneRequerimiento = false;
                    if (rep.getNumpla().length() > 0 ){
                        out.println( "  <table width=\"100%\"  border=\"1\" class=\"Letras\" >" );
                        out.println( "    <tr>" );//dstrct_code, num_sec, std_job_no, fecha_dispo, numpla
                        out.println("<input name=\"C"+(++c)+"dstrct_code\" type=hidden value=\""+rep.getDstrct_code()+"\">");
                        out.println("<input name=\"C"+(c)+"num_sec\" type=hidden value=\""+rep.getNum_sec()+"\">");
                        out.println("<input name=\"C"+(c)+"std_job_no\" type=hidden value=\""+rep.getStd_job_no()+"\">");
                        out.println("<input name=\"C"+(c)+"fecha_dispo\" type=hidden value=\""+rep.getFechadispC()+"\">");
                        out.println("<input name=\"C"+(c)+"numpla\" type=hidden value=\""+rep.getNumpla()+"\">");
                        out.println("<input name=\"filaC"+c+"\" type=hidden value=\""+c+"\">");
                        out.println(
                        "      <div align=\"center\"><span class=\"Estilo1\">C"+(c)+"</span></div>" );
                        out.println( "      <td width=\"90%\">" );
                        out.println(
                        "       <table width=\"100%\"  border=\"1\" bordercolor=\"#CCCCCC\">" );
                        out.println( "        <tr>" );
                        out.println( "          <td width=\"37%\"><strong>Cliente:</strong></td>" );
                        out.println( "          <td width=\"63%\">"+rep.getCliente()+"</td>" );
                        out.println( "        </tr>" );
                        out.println( "        <tr>" );
                        out.println( "          <td><strong>Recurso requerido: </strong></td>" );
                        out.println( "          <td>"+rep.getRecurso_req()+"</td>" );
                        out.println( "        </tr>" );
                        out.println( "        <tr>" );
                        out.println( "          <td><strong>Fecha disponibilidad: </strong></td>" );
                        out.println( "          <td>"+rep.getFechadispC()+"</td>" );
                        out.println( "        </tr>" );
                        out.println( "        <tr>" );
                        out.println( "          <td><strong>Origen:</strong></td>" );
                        out.println( "          <td>"+rep.getOrigenC()+"</td>" );
                        out.println( "        </tr>" );
                        out.println( "        <tr>" );
                        out.println( "          <td><strong>Destino:</strong></td>" );
                        out.println( "          <td>"+rep.getDestinoC()+"</td>" );
                        out.println( "        </tr>" );
                        out.println( "        <tr>" );
                        out.println(
                        "          <td><strong>Fecha asignaci&oacute;n :</strong></td>" );
                        out.println( "          <td>"+(rep.getFechaasig()==null?"(ninguna)":""+rep.getFechaasig())+"</td>" );
                        out.println( "        </tr>" );
                        out.println( "      </table></td>" );
                        out.println( "    </tr>" );
                        out.println( "  </table>" );
                        tieneRequerimiento = true;
                    }
                    out.println( "  </td>" );
                    if ( rep.getNumpla().length() > 0 && rep.getPlaca().length() > 0 ){
                        out.println( "  <td bgcolor=\"#66CC99\" nowrap>");
                    }
                    else {
                        out.println( "  <td nowrap>" );
                    }
                    if ( rep.getPlaca().length() > 0 ){
                        out.println( "   <table width=\"100%\"  border=\"1\">" );
                        out.println( "    <tr>" );//dstrct, placa, fecha_disp
                        out.println("<input name=\"R"+recursos+"dstrct\" type=hidden value=\""+rep.getDstrct()+"\">");
                        out.println("<input name=\"R"+recursos+"placa\" type=hidden value=\""+rep.getPlaca()+"\">");
                        out.println("<input name=\"R"+recursos+"fecha_disp\" type=hidden value=\""+rep.getFechadispR()+"\">");
                        out.println("<input name=\"filaR"+c+"\" type=hidden value=\""+c+"\">");
                        out.println(
                        "      <td width=\"83%\"><table width=\"100%\"  border=\"1\" bordercolor=\"#CCCCCC\">" );
                        out.println( "        <tr>" );
                        out.println( "          <td width=\"40%\"><strong>Placa:</strong></td>" );
                        out.println( "          <td width=\"60%\">"+rep.getPlaca()+"</td>" );
                        out.println( "        </tr>" );
                        out.println( "        <tr>" );
                        out.println( "          <td><strong>Clase: </strong></td>" );
                        out.println( "          <td>"+rep.getClase()+"</td>" );
                        out.println( "        </tr>" );
                        out.println( "        <tr>" );
                        out.println( "          <td><strong>Fecha disponibilidad: </strong></td>" );
                        out.println( "          <td>"+rep.getFechadispR()+"</td>" );
                        out.println( "        </tr>" );
                        out.println( "        <tr>" );
                        out.println( "          <td><strong>Origen:</strong></td>" );
                        out.println( "          <td>"+rep.getOrigenR()+"</td>" );
                        out.println( "        </tr>" );
                        out.println( "        <tr>" );
                        out.println( "          <td><strong>Destino:</strong></td>" );
                        out.println( "          <td>"+rep.getDestinoR()+"</td>" );
                        out.println( "        </tr>" );
                        out.println( "        <tr>" );
                        out.println( "          <td><strong>Cliente:</strong></td>" );
                        out.println( "          <td>"+rep.getClienteR()+"</td>" );
                        out.println( "        </tr>" );
                        out.println( "        <tr>" );
                        out.println(
                        "          <td><strong>Fecha asignaci&oacute;n :</strong></td>" );
                        out.println( "          <td>"+(rep.getFechaasig()==null?"(ninguna)":""+rep.getFechaasig())+"</td>" );
                        out.println( "        </tr>" );
                        out.println( "      </table></td>" );
                        out.println(
                        "      <td width=\"17%\" align=\"center\">Asignar con:<br><br>" );
                        out.println( "" );
                        out.println(
                        "        <input name=\"asignado\" value=\""+(tieneRequerimiento?"C"+c:"")+"\" type=\"text\" size=\"5\"></td>" );
                        out.println( "    </tr>" );
                        out.println( "  </table>" );
                        recursos++;
                    }
                    out.println( " </td>" );
                    out.println( "</tr>" );
                }
                out.println( "</table>" );
                out.println( "<br>" );
                out.println( "<div align=\"center\">" );
                out.println( "<input type=\"submit\" name=\"Submit\" value=\"Guardar\">" );
                out.println( "</div>" );*****************************/
            }
        }
        catch ( Exception ex ) {
            throw new JspException( ex );
        }
        
    }
    
    /**
     * Setter for the fechaInicio attribute.
     */
    public void setFechaInicio( java.lang.String value ) {
        this.fechaInicio = value;
    }
    
    /**
     * Setter for the fechaFin attribute.
     */
    public void setFechaFin( java.lang.String value ) {
        this.fechaFin = value;
    }
    
    /**
     * Setter for the agencia attribute.
     */
    public void setAgencia( java.lang.String value ) {
        this.agencia = value;
    }
}
