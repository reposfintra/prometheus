/*
 * ListarAgencias.java
 *
 * Created on July 5, 2005, 7:47 PM
 */

package com.tsp.tags;

import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import com.tsp.operation.model.Model;
import java.util.Vector;
import com.tsp.operation.model.beans.Agencia;

/**
 *
 * @author  Alejandro
 * @version
 */

public class LlenarComboAgencias extends SimpleTagSupport {

    /**
     * Initialization of distrito property.
     */
    private java.lang.String distrito;
    
    /**
     * Initialization of nombre property.
     */
    private java.lang.String nombre;


    /**Called by the container to invoke this tag.
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    public void doTag() throws JspException {

        JspWriter out = getJspContext().getOut();

        try {
            Model model = new Model();
            Vector agencias = model.agenciaService.obtenerAgencias( distrito );
            if ( agencias.isEmpty() ) {
                out.println( "    <p><strong><span class=\"Estilo11\">El distrito " +
                             distrito + " no tiene agencias</span></strong></p>" );
            }
            else {
                //out.println("<form method=post action=\"<%=CONTROLLER%>?estado=Generar&accion=ReporteDiario\">");
                out.println( "<select name=\""+nombre+"\">" );
                for ( int i = 0; i < agencias.size(); i++ ) {
                    Agencia ag = ( Agencia ) agencias.elementAt( i );
                    out.println( "  <option value=\"" + ag.getId_agencia() + "\">" + ag + "</option>");
                }
                out.println( "</select>" );
            }
        }
        catch ( Exception ex ) {
            throw new JspException( ex.getMessage() );
        }

    }

    /**
     * Setter for the distrito attribute.
     */
    public void setDistrito( java.lang.String value ) {
        this.distrito = value;
    }
    
    /**
     * Setter for the nombre attribute.
     */
    public void setNombre( java.lang.String value ) {
        this.nombre = value;
    }
}
