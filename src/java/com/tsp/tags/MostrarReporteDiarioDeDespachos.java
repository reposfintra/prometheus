/*
 * ListarReportePlaneacion.java
 *
 * Created on July 5, 2005, 7:47 PM
 */

package com.tsp.tags;

import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.*;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import com.tsp.operation.model.Model;
import java.util.Vector;
import java.util.Hashtable;
import com.tsp.operation.model.beans.Agencia;
import com.tsp.operation.model.beans.ReportePlaneacion;
import com.tsp.operation.controller.GenerarReporteDiarioAction;
import com.tsp.util.Util;
import java.util.Enumeration;

/**
 *
 * @author  Alejandro
 * @version
 */

public class MostrarReporteDiarioDeDespachos extends SimpleTagSupport {
    
    /**
     * Initialization of fechaInicio property.
     */
    private java.lang.String fechaInicio;
    
    /**
     * Initialization of fechaFin property.
     */
    private java.lang.String fechaFin;
    
    /**
     * Called by the container to invoke this tag.
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    public void doTag() throws JspException {
        
        JspWriter out = getJspContext().getOut();
        
        try {
            
            Vector datos =(Vector)this.getJspContext().getAttribute("datosReporte",PageContext.SESSION_SCOPE);
            Object fechaInicio = this.getJspContext().getAttribute("fechaInicio",PageContext.SESSION_SCOPE);
            Object fechaFin = this.getJspContext().getAttribute("fechaFin",PageContext.SESSION_SCOPE);
           /* this.getJspContext().removeAttribute("datosReporte",PageContext.SESSION_SCOPE);
            this.getJspContext().removeAttribute("fechaInicio",PageContext.SESSION_SCOPE);
            this.getJspContext().removeAttribute("fechaFin",PageContext.SESSION_SCOPE);*/
            if ( datos == null || datos.isEmpty() ) {
                out.println("    <p><strong><span class=class=\"letra\">No fue encontrado ningun despacho entre las fechas dadas</span></strong></p>" );
            }
            else {
                String[] encabezados = {"OC", "FECHA<br>IMPRESION", "ORIGEN", "DESTINO", "PLACA",
                                   "CONDUCTOR", "CLIENTE", "CANT", "OT","STD JOB","DESCRIPCI&Oacute;N", 
                                   "VLR OT", "VLR OC", "VALOR<br>ANTICIPO", "AM", "%UB"};
                int anchosColumnas [] = {69,96,200,200,68,200,300,51,69,54,500,85,85,38,81,45,45,61};
                out.println("<table width=\"2200\" cellpadding=\"0\" cellspacing=\"0\" class=class=\"letra\">" );
                out.println("  <tr class=\"titulo\">" );
                out.println("    <td >&nbsp;</td>" );
                out.println("  </tr>" );
                out.println("  <tr class=\"titulo\">" );
                out.println("    <td ><div align=\"center\"><strong>TRANSPORTES SANCHEZ POLO S.A</strong></div></td>" );
                out.println("  </tr>" );
                out.println("  <tr class=\"titulo\">" );
                out.println("    <td width=\"471\" height=\"19\"><div align=\"center\"></div></td>" );
                out.println("  </tr>" );
                out.println("  <tr class=\"subtitulos\">" );
                out.println("    <td class=\"subtitulos\"><div align=\"center\"><strong>REPORTE DIARIO DE DESPACHOS </strong></div></td>" );
                out.println("  </tr>" );
                out.println("  <tr class=\"subtitulos\">" );
                out.println("    <td class=\"subtitulos\"><div align=\"center\"><strong>FECHA DE INFORME: ["+fechaInicio+"] - ["+fechaFin+"] </strong></div></td>" );
                out.println("  </tr>" );
                out.println("  <tr class=\"subtitulos\">" );
                out.println("    <td class=\"subtitulos\"><div align=\"center\"><strong>FECHA DE PROCESO: "+Util.getFechaActual_String( 6 )+" </strong></div></td>" );
                out.println("  </tr>" );
                for(int i=0; i<datos.size(); i++ ){
                    Hashtable datosAgencia = (Hashtable)datos.elementAt(i);
                    out.println("  <tr class=\"subtitulos\">" );
                    out.println("    <td>&nbsp;</td>" );
                    out.println("  </tr>" );
                    out.println("  <tr class=\"subtitulos\">" );
                    if ( datosAgencia.size() == 1 ){
                        out.println("    <td  ><div align=\"left\"><strong>LA AGENCIA "+datosAgencia.get("nombre")+" NO TIENE PLANILLAS</strong></div></td>" );
                        continue;
                    }
                    out.println("    <td class=\"subtitulos\"><div align=\"left\"><strong>AGENCIA: "+datosAgencia.get("nombre")+"</strong></div></td>" );
                    out.println("  </tr>" );
                    out.println("  <tr class=\"fila\">" );
                    out.println("    <td>" );
                    out.println("     <table class=\"letra\" cellspacing=\"0\" bordercolor=\"#CCCCCC\" border=\"1\" cellpadding=\"0\">" );
                    out.println("      <tr class=\"letra\" >" );
                    for(int j=0; j<encabezados.length; j++){
                        out.println("        <td width=\""+
                        anchosColumnas[j]+"\" class=\"letra\"><div align=\"center\"><strong>"+
                        encabezados[j]+"</strong></div></td>" );
                    }
                    out.println("      </tr>" );
                    int filas = ((Integer)datosAgencia.get("filas")).intValue();
                    for( int j=0; j<filas; j++ ){
                        Hashtable fila = (Hashtable) datosAgencia.get("fila"+j);
                        out.println("      <tr class=\"letra\">" );
                        for( int k=1; k<=16; k++ ){
                            out.println("        <td class=\"letra\">"+fila.get(new Integer(k))+"</td>" );
                        }
                        out.println("      </tr>" );
                    }
                    out.println("    </table></td>" );
                    out.println("  </tr>" );
                }
                out.println("</table>" );
            }
        }
        catch ( Exception ex ) {
         //   GenerarReporteDiarioAction.imprimirExcepcion(ex,"/error.txt");
            throw new JspException( ex );
        }
        
    }
    
    /**
     * Setter for the fechaInicio attribute.
     */
    public void setFechaInicio( java.lang.String value ) {
        this.fechaInicio = value;
    }
    
    /**
     * Setter for the fechaFin attribute.
     */
    public void setFechaFin( java.lang.String value ) {
        this.fechaFin = value;
    }
}
