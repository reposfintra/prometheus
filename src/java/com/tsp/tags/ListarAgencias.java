/*
 * ListarAgencias.java
 *
 * Created on July 5, 2005, 7:47 PM
 */

package com.tsp.tags;

import javax.servlet.jsp.tagext.*;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.JspException;
import com.tsp.operation.model.Model;
import java.util.Vector;
import com.tsp.operation.model.beans.Agencia;

/**
 *
 * @author  Alejandro
 * @version
 */

public class ListarAgencias extends SimpleTagSupport {

    /**
     * Initialization of distrito property.
     */
    private java.lang.String distrito;


    /**Called by the container to invoke this tag.
     * The implementation of this method is provided by the tag library developer,
     * and handles all tag processing, body iteration, etc.
     */
    public void doTag() throws JspException {

        JspWriter out = getJspContext().getOut();

        try {
            Model model = new Model();
            Vector agencias = model.agenciaService.obtenerAgencias( distrito );
            if ( agencias.isEmpty() ) {
                out.println( "    <p><strong><span class=\"Estilo11\">El distrito " +
                             distrito + " no tiene agencias</span></strong></p>" );
            }
            else {
                //out.println("<form method=post action=\"<%=CONTROLLER%>?estado=Generar&accion=ReporteDiario\">");
                out.println( "<table width=\"80%\"  border=\"1\">" );
                out.println( "  <tr bgcolor=\"#0099FF\">" );
                out.println( "    <th colspan=\"3\" bgcolor=\"#FF9900\" scope=\"col\"><strong><span class=\"Estilo11\">Agencias del distrito " +
                             distrito + " </span></strong></th>" );
                out.println( "  </tr>" );
                out.println( "  <tr bgcolor=\"#0099FF\">" );
                out.println( "    <th scope=\"col\"><span class=\"Estilo7\"></span></th>" );
                out.println( "    <th scope=\"col\"><span class=\"Estilo3\">Nombre</span></th>" );
                out.println( "    <th scope=\"col\"><span class=\"Estilo7\"><input type='checkbox' name='All' onclick='jscript: SelAll();'></span></th>" );
                out.println( "  </tr>" );
                for ( int i = 0; i < agencias.size(); i++ ) {
                    Agencia ag = ( Agencia ) agencias.elementAt( i );
                    out.println( "  <tr>" );
                    out.println(
                        "    <th bgcolor=\"#0099FF\" scope=\"row\">"+(i+1)+"<span class=\"Estilo2\"></span></th>" );
                    out.println( "    <td><span class=\"Estilo9\">" + ag + "</span></td>" );
                    out.println( "    <td><div align=\"center\">" );
                    out.println( "      <input type=\"checkbox\" name=\"agencias\" value=\"" +
                                 ag.getId_agencia() + "\" onclick='jscript: ActAll();'>" );
                    out.println( "    </div></td>" );
                    out.println( "  </tr>" );
                }
                out.println( "<tr>" );
                out.println( "    <td colspan=\"3\"><table width=\"100%\"  border=\"0\">" );
                out.println( "      <tr>" );
                out.println( "        <td width=\"34%\" bgcolor=\"#FF9900\"><strong><span class=\"Estilo11\">Fecha inicial: </span></strong></td>" );
                out.println( "        <td width=\"66%\" bgcolor=\"#0099FF\">");
                out.println( "        <input name='fechai' type='text' id=\"fechai\" style='width:120' readonly>" );
                out.println( "        <a href=\"javascript:void(0)\" onclick=\"jscript: show_calendar('fechai');\" HIDEFOCUS><img name=\"popcal\" align=\"absmiddle\" src=\""+this.getJspContext().getAttribute("BASEURL")+"/js/Calendario/calbtn.gif\" width=\"34\" height=\"22\" border=\"0\" alt=\"\"></a></span>" );
                out.println( "        </td>" );
                out.println( "      </tr>" );
                out.println( "    </table></td>" );
                out.println( "  </tr>" );
                out.println( "  <tr>" );
                out.println( "    <td colspan=\"3\"><table width=\"100%\"  border=\"0\">" );
                out.println( "      <tr>" );
                out.println( "        <td width=\"34%\" bgcolor=\"#FF9900\"><strong><span class=\"Estilo11\">Fecha final: </span></strong></td>" );
                out.println( "        <td width=\"66%\" bgcolor=\"#0099FF\">");
                out.println( "        <input name='fechaf' type='text' id=\"fechaf\" style='width:120' readonly>" );
                out.println( "        <a href=\"javascript:void(0)\" onclick=\"jscript: show_calendar('fechaf');\" HIDEFOCUS><img name=\"popcal\" align=\"absmiddle\" src=\""+this.getJspContext().getAttribute("BASEURL")+"/js/Calendario/calbtn.gif\" width=\"34\" height=\"22\" border=\"0\" alt=\"\"></a></span>" );
                out.println( "        </td>" );
                out.println( "      </tr>" );
                out.println( "    </table></td>" );
                out.println( "  </tr>" );
                out.println( "  <tr>" );
                out.println(
                    "<td colspan=\"3\"><div align=\"center\"><input type=submit value=\"Generar reporte\"></div></td>" );
                out.println( "  </tr>" );
                out.println( "</table>" );
                out.println("<input type=hidden value=\""+distrito+"\" name=\"distrito\"");
                //out.println("</form>");
            }
        }
        catch ( Exception ex ) {
            throw new JspException( ex.getMessage() );
        }

    }

    /**
     * Setter for the distrito attribute.
     */
    public void setDistrito( java.lang.String value ) {
        this.distrito = value;
    }
}
