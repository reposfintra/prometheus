// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, compilación R1)
// Generated source version: 1.1.3

package com.fintra.ws.datacredito;


public class ServicioLocalizacion_consultaRutaDescargas_ResponseStruct2 {
    protected java.lang.String consultaRutaDescargasReturn;
    
    public ServicioLocalizacion_consultaRutaDescargas_ResponseStruct2() {
    }
    
    public ServicioLocalizacion_consultaRutaDescargas_ResponseStruct2(java.lang.String consultaRutaDescargasReturn) {
        this.consultaRutaDescargasReturn = consultaRutaDescargasReturn;
    }
    
    public java.lang.String getConsultaRutaDescargasReturn() {
        return consultaRutaDescargasReturn;
    }
    
    public void setConsultaRutaDescargasReturn(java.lang.String consultaRutaDescargasReturn) {
        this.consultaRutaDescargasReturn = consultaRutaDescargasReturn;
    }
}
