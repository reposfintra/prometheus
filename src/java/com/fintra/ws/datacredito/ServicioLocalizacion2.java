// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, compilación R1)
// Generated source version: 1.1.3

package com.fintra.ws.datacredito;

import javax.xml.rpc.*;

public interface ServicioLocalizacion2 extends javax.xml.rpc.Service {
    public com.fintra.ws.datacredito.ServicioLocalizacion getServicioLocalizacion2() throws ServiceException;
}
