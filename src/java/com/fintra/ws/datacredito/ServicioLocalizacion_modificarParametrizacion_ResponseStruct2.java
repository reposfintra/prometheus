// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, compilación R1)
// Generated source version: 1.1.3

package com.fintra.ws.datacredito;


public class ServicioLocalizacion_modificarParametrizacion_ResponseStruct2 {
    protected java.lang.String modificarParametrizacionReturn;
    
    public ServicioLocalizacion_modificarParametrizacion_ResponseStruct2() {
    }
    
    public ServicioLocalizacion_modificarParametrizacion_ResponseStruct2(java.lang.String modificarParametrizacionReturn) {
        this.modificarParametrizacionReturn = modificarParametrizacionReturn;
    }
    
    public java.lang.String getModificarParametrizacionReturn() {
        return modificarParametrizacionReturn;
    }
    
    public void setModificarParametrizacionReturn(java.lang.String modificarParametrizacionReturn) {
        this.modificarParametrizacionReturn = modificarParametrizacionReturn;
    }
}
