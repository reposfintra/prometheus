// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, compilación R1)
// Generated source version: 1.1.3

package com.fintra.ws.datacredito;

import com.sun.xml.rpc.encoding.*;
import com.sun.xml.rpc.encoding.literal.DetailFragmentDeserializer;
import com.sun.xml.rpc.encoding.simpletype.*;
import com.sun.xml.rpc.encoding.soap.SOAPConstants;
import com.sun.xml.rpc.encoding.soap.SOAP12Constants;
import com.sun.xml.rpc.streaming.*;
import com.sun.xml.rpc.wsdl.document.schema.SchemaConstants;
import javax.xml.namespace.QName;

public class ServicioLocalizacion_consultaArchivosUsuarios_RequestStruct2_SOAPSerializer extends ObjectSerializerBase implements Initializable {
    private static final javax.xml.namespace.QName ns1_tipoId_QNAME = new QName("", "tipoId");
    private static final javax.xml.namespace.QName ns2_string_TYPE_QNAME = SOAPConstants.QNAME_TYPE_STRING;
    private CombinedSerializer ns2_myns2_string__java_lang_String_String_Serializer;
    private static final javax.xml.namespace.QName ns1_identificacion_QNAME = new QName("", "identificacion");
    private static final javax.xml.namespace.QName ns1_nit_QNAME = new QName("", "nit");
    private static final int myTIPOID_INDEX = 0;
    private static final int myIDENTIFICACION_INDEX = 1;
    private static final int myNIT_INDEX = 2;
    
    public ServicioLocalizacion_consultaArchivosUsuarios_RequestStruct2_SOAPSerializer(QName type, boolean encodeType, boolean isNullable, String encodingStyle) {
        super(type, encodeType, isNullable, encodingStyle);
    }
    
    public void initialize(InternalTypeMappingRegistry registry) throws java.lang.Exception {
        ns2_myns2_string__java_lang_String_String_Serializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, java.lang.String.class, ns2_string_TYPE_QNAME);
    }
    
    public java.lang.Object doDeserialize(SOAPDeserializationState state, XMLReader reader,
        SOAPDeserializationContext context) throws java.lang.Exception {
        com.fintra.ws.datacredito.ServicioLocalizacion_consultaArchivosUsuarios_RequestStruct2 instance = new com.fintra.ws.datacredito.ServicioLocalizacion_consultaArchivosUsuarios_RequestStruct2();
        com.fintra.ws.datacredito.ServicioLocalizacion_consultaArchivosUsuarios_RequestStruct2_SOAPBuilder builder = null;
        java.lang.Object member;
        boolean isComplete = true;
        javax.xml.namespace.QName elementName;
        
        reader.nextElementContent();
        for (int i=0; i<3; i++) {
            elementName = reader.getName();
            if (reader.getState() == XMLReader.END) {
                break;
            }
            if (elementName.equals(ns1_tipoId_QNAME)) {
                member = ns2_myns2_string__java_lang_String_String_Serializer.deserialize(ns1_tipoId_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new com.fintra.ws.datacredito.ServicioLocalizacion_consultaArchivosUsuarios_RequestStruct2_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myTIPOID_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setTipoId((java.lang.String)member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_identificacion_QNAME)) {
                member = ns2_myns2_string__java_lang_String_String_Serializer.deserialize(ns1_identificacion_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new com.fintra.ws.datacredito.ServicioLocalizacion_consultaArchivosUsuarios_RequestStruct2_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myIDENTIFICACION_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setIdentificacion((java.lang.String)member);
                }
                reader.nextElementContent();
                continue;
            }
            if (elementName.equals(ns1_nit_QNAME)) {
                member = ns2_myns2_string__java_lang_String_String_Serializer.deserialize(ns1_nit_QNAME, reader, context);
                if (member instanceof SOAPDeserializationState) {
                    if (builder == null) {
                        builder = new com.fintra.ws.datacredito.ServicioLocalizacion_consultaArchivosUsuarios_RequestStruct2_SOAPBuilder();
                    }
                    state = registerWithMemberState(instance, state, member, myNIT_INDEX, builder);
                    isComplete = false;
                } else {
                    instance.setNit((java.lang.String)member);
                }
                reader.nextElementContent();
                continue;
            } else {
                throw new DeserializationException("soap.unexpectedElementName", new Object[] {ns1_nit_QNAME, elementName});
            }
        }
        
        XMLReaderUtil.verifyReaderState(reader, XMLReader.END);
        return (isComplete ? (java.lang.Object)instance : (java.lang.Object)state);
    }
    
    public void doSerializeAttributes(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws java.lang.Exception {
        com.fintra.ws.datacredito.ServicioLocalizacion_consultaArchivosUsuarios_RequestStruct2 instance = (com.fintra.ws.datacredito.ServicioLocalizacion_consultaArchivosUsuarios_RequestStruct2)obj;
        
    }
    
    public void doSerializeInstance(java.lang.Object obj, XMLWriter writer, SOAPSerializationContext context) throws java.lang.Exception {
        com.fintra.ws.datacredito.ServicioLocalizacion_consultaArchivosUsuarios_RequestStruct2 instance = (com.fintra.ws.datacredito.ServicioLocalizacion_consultaArchivosUsuarios_RequestStruct2)obj;
        
        ns2_myns2_string__java_lang_String_String_Serializer.serialize(instance.getTipoId(), ns1_tipoId_QNAME, null, writer, context);
        ns2_myns2_string__java_lang_String_String_Serializer.serialize(instance.getIdentificacion(), ns1_identificacion_QNAME, null, writer, context);
        ns2_myns2_string__java_lang_String_String_Serializer.serialize(instance.getNit(), ns1_nit_QNAME, null, writer, context);
    }
}
