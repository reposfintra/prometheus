// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, compilación R1)
// Generated source version: 1.1.3

package com.fintra.ws.datacredito;


import com.sun.xml.rpc.encoding.*;
import com.sun.xml.rpc.encoding.soap.SOAPConstants;
import com.sun.xml.rpc.encoding.soap.SOAP12Constants;
import com.sun.xml.rpc.soap.message.SOAPFaultInfo;
import com.sun.xml.rpc.streaming.*;
import com.sun.xml.rpc.wsdl.document.schema.SchemaConstants;
import javax.xml.namespace.QName;

public class ServicioLocalizacion_consultarValidacion_Fault_SOAPSerializer extends SOAPFaultInfoSerializer {
    private static final javax.xml.namespace.QName ns1_fault_QNAME = new QName("http://www.datacredito.com.co/services/ServicioLocalizacion2", "fault");
    private static final javax.xml.namespace.QName ns2_LocalizacionException_TYPE_QNAME = new QName("http://exception.ws.loc.dc.com", "LocalizacionException");
    private CombinedSerializer ns2_myLocalizacionException_Exception_SOAPSerializer;
    private CombinedSerializer ns2_myLocalizacionException_Exception_SOAPSerializer_Serializer;
    private static final int COM_FINTRA_WS_DATACREDITO_LOCALIZACIONEXCEPTION_EXCEPTION_INDEX = 0;
    
    public ServicioLocalizacion_consultarValidacion_Fault_SOAPSerializer(boolean encodeType, boolean isNullable) {
        super(encodeType, isNullable);
    }
    
    public void initialize(InternalTypeMappingRegistry registry) throws java.lang.Exception {
        super.initialize(registry);
        ns2_myLocalizacionException_Exception_SOAPSerializer = (CombinedSerializer)registry.getSerializer(SOAPConstants.NS_SOAP_ENCODING, com.fintra.ws.datacredito.LocalizacionException_Exception.class, ns2_LocalizacionException_TYPE_QNAME);
        ns2_myLocalizacionException_Exception_SOAPSerializer_Serializer = ns2_myLocalizacionException_Exception_SOAPSerializer.getInnermostSerializer();
    }
    
    protected java.lang.Object deserializeDetail(SOAPDeserializationState state, XMLReader reader,
        SOAPDeserializationContext context, SOAPFaultInfo instance) throws java.lang.Exception {
        boolean isComplete = true;
        javax.xml.namespace.QName elementName;
        javax.xml.namespace.QName elementType = null;
        SOAPInstanceBuilder builder = null;
        java.lang.Object detail = null;
        java.lang.Object obj = null;
        
        reader.nextElementContent();
        if (reader.getState() == XMLReader.END)
            return deserializeDetail(reader, context);
        XMLReaderUtil.verifyReaderState(reader, XMLReader.START);
        elementName = reader.getName();
        elementType = getType(reader);
        if (elementName.equals(ns1_fault_QNAME)) {
            if (elementType == null || 
                (elementType.equals(ns2_myLocalizacionException_Exception_SOAPSerializer.getXmlType()) ||
                (ns2_myLocalizacionException_Exception_SOAPSerializer_Serializer instanceof ArraySerializerBase &&
                elementType.equals(SOAPConstants.QNAME_ENCODING_ARRAY)) ) ) {
                obj = ns2_myLocalizacionException_Exception_SOAPSerializer.deserialize(ns1_fault_QNAME, reader, context);
                if (obj instanceof SOAPDeserializationState) {
                    builder = new com.fintra.ws.datacredito.ServicioLocalizacion_consultarValidacion_Fault_SOAPBuilder();
                    state = registerWithMemberState(instance, state, obj,
                        COM_FINTRA_WS_DATACREDITO_LOCALIZACIONEXCEPTION_EXCEPTION_INDEX, builder);
                    isComplete = false;
                } else {
                    detail = obj;
                }
                reader.nextElementContent();
                skipRemainingDetailEntries(reader);
                XMLReaderUtil.verifyReaderState(reader, XMLReader.END);
                return (isComplete ? (Object)detail : (Object)state);
            } 
        }
        return deserializeDetail(reader, context);
    }
    
    protected void serializeDetail(java.lang.Object detail, XMLWriter writer, SOAPSerializationContext context)
        throws java.lang.Exception {
        if (detail == null) {
            throw new SerializationException("soap.unexpectedNull");
        }
        writer.startElement(DETAIL_QNAME);
        
        boolean pushedEncodingStyle = false;
        if (encodingStyle != null) {
            context.pushEncodingStyle(encodingStyle, writer);
        }
        if (detail instanceof com.fintra.ws.datacredito.LocalizacionException_Exception) {
            ns2_myLocalizacionException_Exception_SOAPSerializer_Serializer.serialize(detail, ns1_fault_QNAME, null, writer, context);
        }
        writer.endElement();
        if (pushedEncodingStyle) {
            context.popEncodingStyle();
        }
    }
}
