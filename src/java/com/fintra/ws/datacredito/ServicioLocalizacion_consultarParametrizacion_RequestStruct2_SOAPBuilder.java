// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, compilación R1)
// Generated source version: 1.1.3

package com.fintra.ws.datacredito;

import com.sun.xml.rpc.encoding.*;
import com.sun.xml.rpc.util.exception.LocalizableExceptionAdapter;

public class ServicioLocalizacion_consultarParametrizacion_RequestStruct2_SOAPBuilder implements SOAPInstanceBuilder {
    private com.fintra.ws.datacredito.ServicioLocalizacion_consultarParametrizacion_RequestStruct2 _instance;
    private java.lang.String tipoId;
    private java.lang.String id;
    private static final int myTIPOID_INDEX = 0;
    private static final int myID_INDEX = 1;
    
    public ServicioLocalizacion_consultarParametrizacion_RequestStruct2_SOAPBuilder() {
    }
    
    public void setTipoId(java.lang.String tipoId) {
        this.tipoId = tipoId;
    }
    
    public void setId(java.lang.String id) {
        this.id = id;
    }
    
    public int memberGateType(int memberIndex) {
        switch (memberIndex) {
            case myTIPOID_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            case myID_INDEX:
                return GATES_INITIALIZATION | REQUIRES_CREATION;
            default:
                throw new IllegalArgumentException();
        }
    }
    
    public void construct() {
    }
    
    public void setMember(int index, java.lang.Object memberValue) {
        try {
            switch(index) {
                case myTIPOID_INDEX:
                    _instance.setTipoId((java.lang.String)memberValue);
                    break;
                case myID_INDEX:
                    _instance.setId((java.lang.String)memberValue);
                    break;
                default:
                    throw new java.lang.IllegalArgumentException();
            }
        }
        catch (java.lang.RuntimeException e) {
            throw e;
        }
        catch (java.lang.Exception e) {
            throw new DeserializationException(new LocalizableExceptionAdapter(e));
        }
    }
    
    public void initialize() {
    }
    
    public void setInstance(java.lang.Object instance) {
        _instance = (com.fintra.ws.datacredito.ServicioLocalizacion_consultarParametrizacion_RequestStruct2)instance;
    }
    
    public java.lang.Object getInstance() {
        return _instance;
    }
}
