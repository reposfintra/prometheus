/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fintra.ws.apoteosys;

import com.tsp.util.Util;

/**
 *
 * @author egonzalez
 */
public class ConsumoWsdl {
    
    public static void main(String[] args) {
        System.out.println(testConexion("User Test"));
        String centroCostoProy = Util.getCentroCostoProy("",2);
     }

    private static String testConexion(java.lang.String name) {
        com.fintra.ws.apoteosys.Wsapoteosys_Service service = new com.fintra.ws.apoteosys.Wsapoteosys_Service();
        com.fintra.ws.apoteosys.Wsapoteosys port = service.getWsapoteosysPort();
        return port.testConexion(name);
    }

    private static String getCentroCostoProy(java.lang.String nombreProyecto, int tipoProyecto) {
        com.fintra.ws.apoteosys.Wsapoteosys_Service service = new com.fintra.ws.apoteosys.Wsapoteosys_Service();
        com.fintra.ws.apoteosys.Wsapoteosys port = service.getWsapoteosysPort();
        return port.getCentroCostoProy(nombreProyecto, tipoProyecto);
    }

    private static String loadApoteosys(java.lang.String user, java.lang.String password, java.lang.String periodoAno, int periodoMes, java.lang.String operacion) {
        com.fintra.ws.apoteosys.Wsapoteosys_Service service = new com.fintra.ws.apoteosys.Wsapoteosys_Service();
        com.fintra.ws.apoteosys.Wsapoteosys port = service.getWsapoteosysPort();
        return port.loadApoteosys(user, password, periodoAno, periodoMes, operacion);
    }
    
}
