
package com.fintra.ws.apoteosys;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para getCentroCostoProy complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="getCentroCostoProy">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nombre_proyecto" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipo_proyecto" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "getCentroCostoProy", propOrder = {
    "nombreProyecto",
    "tipoProyecto"
})
public class GetCentroCostoProy {

    @XmlElement(name = "nombre_proyecto")
    protected String nombreProyecto;
    @XmlElement(name = "tipo_proyecto")
    protected int tipoProyecto;

    /**
     * Obtiene el valor de la propiedad nombreProyecto.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNombreProyecto() {
        return nombreProyecto;
    }

    /**
     * Define el valor de la propiedad nombreProyecto.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNombreProyecto(String value) {
        this.nombreProyecto = value;
    }

    /**
     * Obtiene el valor de la propiedad tipoProyecto.
     * 
     */
    public int getTipoProyecto() {
        return tipoProyecto;
    }

    /**
     * Define el valor de la propiedad tipoProyecto.
     * 
     */
    public void setTipoProyecto(int value) {
        this.tipoProyecto = value;
    }

}
