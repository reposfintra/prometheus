
package com.fintra.ws.apoteosys;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.2.6-1b01 
 * Generated source version: 2.2
 * 
 */
@WebService(name = "wsapoteosys", targetNamespace = "http://apt.fintra.com/")
@XmlSeeAlso({
    ObjectFactory.class
})
public interface Wsapoteosys {


    /**
     * 
     * @param operacion
     * @param periodoMes
     * @param periodoAno
     * @param password
     * @param user
     * @return
     *     returns java.lang.String
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "loadApoteosys", targetNamespace = "http://apt.fintra.com/", className = "com.fintra.ws.apoteosys.LoadApoteosys")
    @ResponseWrapper(localName = "loadApoteosysResponse", targetNamespace = "http://apt.fintra.com/", className = "com.fintra.ws.apoteosys.LoadApoteosysResponse")
    @Action(input = "http://apt.fintra.com/wsapoteosys/loadApoteosysRequest", output = "http://apt.fintra.com/wsapoteosys/loadApoteosysResponse")
    public String loadApoteosys(
        @WebParam(name = "user", targetNamespace = "")
        String user,
        @WebParam(name = "password", targetNamespace = "")
        String password,
        @WebParam(name = "periodo_ano", targetNamespace = "")
        String periodoAno,
        @WebParam(name = "periodo_mes", targetNamespace = "")
        int periodoMes,
        @WebParam(name = "operacion", targetNamespace = "")
        String operacion);

    /**
     * 
     * @param name
     * @return
     *     returns java.lang.String
     */
    @WebMethod(operationName = "TestConexion")
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "TestConexion", targetNamespace = "http://apt.fintra.com/", className = "com.fintra.ws.apoteosys.TestConexion")
    @ResponseWrapper(localName = "TestConexionResponse", targetNamespace = "http://apt.fintra.com/", className = "com.fintra.ws.apoteosys.TestConexionResponse")
    @Action(input = "http://apt.fintra.com/wsapoteosys/TestConexionRequest", output = "http://apt.fintra.com/wsapoteosys/TestConexionResponse")
    public String testConexion(
        @WebParam(name = "name", targetNamespace = "")
        String name);

    /**
     * 
     * @param tipoProyecto
     * @param nombreProyecto
     * @return
     *     returns java.lang.String
     */
    @WebMethod
    @WebResult(targetNamespace = "")
    @RequestWrapper(localName = "getCentroCostoProy", targetNamespace = "http://apt.fintra.com/", className = "com.fintra.ws.apoteosys.GetCentroCostoProy")
    @ResponseWrapper(localName = "getCentroCostoProyResponse", targetNamespace = "http://apt.fintra.com/", className = "com.fintra.ws.apoteosys.GetCentroCostoProyResponse")
    @Action(input = "http://apt.fintra.com/wsapoteosys/getCentroCostoProyRequest", output = "http://apt.fintra.com/wsapoteosys/getCentroCostoProyResponse")
    public String getCentroCostoProy(
        @WebParam(name = "nombre_proyecto", targetNamespace = "")
        String nombreProyecto,
        @WebParam(name = "tipo_proyecto", targetNamespace = "")
        int tipoProyecto);

}
