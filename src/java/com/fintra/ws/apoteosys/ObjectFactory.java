
package com.fintra.ws.apoteosys;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.fintra.ws.apoteosys package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _TestConexionResponse_QNAME = new QName("http://apt.fintra.com/", "TestConexionResponse");
    private final static QName _GetCentroCostoProyResponse_QNAME = new QName("http://apt.fintra.com/", "getCentroCostoProyResponse");
    private final static QName _LoadApoteosys_QNAME = new QName("http://apt.fintra.com/", "loadApoteosys");
    private final static QName _LoadApoteosysResponse_QNAME = new QName("http://apt.fintra.com/", "loadApoteosysResponse");
    private final static QName _GetCentroCostoProy_QNAME = new QName("http://apt.fintra.com/", "getCentroCostoProy");
    private final static QName _TestConexion_QNAME = new QName("http://apt.fintra.com/", "TestConexion");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.fintra.ws.apoteosys
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link LoadApoteosysResponse }
     * 
     */
    public LoadApoteosysResponse createLoadApoteosysResponse() {
        return new LoadApoteosysResponse();
    }

    /**
     * Create an instance of {@link GetCentroCostoProy }
     * 
     */
    public GetCentroCostoProy createGetCentroCostoProy() {
        return new GetCentroCostoProy();
    }

    /**
     * Create an instance of {@link TestConexion }
     * 
     */
    public TestConexion createTestConexion() {
        return new TestConexion();
    }

    /**
     * Create an instance of {@link GetCentroCostoProyResponse }
     * 
     */
    public GetCentroCostoProyResponse createGetCentroCostoProyResponse() {
        return new GetCentroCostoProyResponse();
    }

    /**
     * Create an instance of {@link TestConexionResponse }
     * 
     */
    public TestConexionResponse createTestConexionResponse() {
        return new TestConexionResponse();
    }

    /**
     * Create an instance of {@link LoadApoteosys }
     * 
     */
    public LoadApoteosys createLoadApoteosys() {
        return new LoadApoteosys();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TestConexionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apt.fintra.com/", name = "TestConexionResponse")
    public JAXBElement<TestConexionResponse> createTestConexionResponse(TestConexionResponse value) {
        return new JAXBElement<TestConexionResponse>(_TestConexionResponse_QNAME, TestConexionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCentroCostoProyResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apt.fintra.com/", name = "getCentroCostoProyResponse")
    public JAXBElement<GetCentroCostoProyResponse> createGetCentroCostoProyResponse(GetCentroCostoProyResponse value) {
        return new JAXBElement<GetCentroCostoProyResponse>(_GetCentroCostoProyResponse_QNAME, GetCentroCostoProyResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadApoteosys }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apt.fintra.com/", name = "loadApoteosys")
    public JAXBElement<LoadApoteosys> createLoadApoteosys(LoadApoteosys value) {
        return new JAXBElement<LoadApoteosys>(_LoadApoteosys_QNAME, LoadApoteosys.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoadApoteosysResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apt.fintra.com/", name = "loadApoteosysResponse")
    public JAXBElement<LoadApoteosysResponse> createLoadApoteosysResponse(LoadApoteosysResponse value) {
        return new JAXBElement<LoadApoteosysResponse>(_LoadApoteosysResponse_QNAME, LoadApoteosysResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetCentroCostoProy }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apt.fintra.com/", name = "getCentroCostoProy")
    public JAXBElement<GetCentroCostoProy> createGetCentroCostoProy(GetCentroCostoProy value) {
        return new JAXBElement<GetCentroCostoProy>(_GetCentroCostoProy_QNAME, GetCentroCostoProy.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TestConexion }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://apt.fintra.com/", name = "TestConexion")
    public JAXBElement<TestConexion> createTestConexion(TestConexion value) {
        return new JAXBElement<TestConexion>(_TestConexion_QNAME, TestConexion.class, null, value);
    }

}
