/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.fintra.ws.transportadoras;

/**
 *
 * @author egonzalez
 */
public class clasePrueba {
    
    public static void main(String[] args) {
        System.out.println(testConnectionParameter("MCASTILLO", "123456"));
    }

    private static String testConnectionParameter(java.lang.String user, java.lang.String password) {
        com.fintra.ws.transportadoras.WsTransportadoras_Service service = new com.fintra.ws.transportadoras.WsTransportadoras_Service();
        com.fintra.ws.transportadoras.WsTransportadoras port = service.getWsTransportadorasPort();
        return port.testConnectionParameter(user, password);
    }
    
}
