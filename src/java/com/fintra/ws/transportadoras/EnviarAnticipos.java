
package com.fintra.ws.transportadoras;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para enviarAnticipos complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="enviarAnticipos">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="tramaJson" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "enviarAnticipos", propOrder = {
    "tramaJson"
})
public class EnviarAnticipos {

    protected String tramaJson;

    /**
     * Obtiene el valor de la propiedad tramaJson.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTramaJson() {
        return tramaJson;
    }

    /**
     * Define el valor de la propiedad tramaJson.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTramaJson(String value) {
        this.tramaJson = value;
    }

}
