
package com.fintra.ws.transportadoras;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.fintra.ws.transportadoras package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _EnviarAnticiposLogin_QNAME = new QName("http://ws.fintra.com/", "enviarAnticiposLogin");
    private final static QName _TestConnectionResponse_QNAME = new QName("http://ws.fintra.com/", "testConnectionResponse");
    private final static QName _TestConnection_QNAME = new QName("http://ws.fintra.com/", "testConnection");
    private final static QName _AnularReanticipoLogin_QNAME = new QName("http://ws.fintra.com/", "anularReanticipoLogin");
    private final static QName _EnviarReanticipo_QNAME = new QName("http://ws.fintra.com/", "enviarReanticipo");
    private final static QName _TestConnectionParameter_QNAME = new QName("http://ws.fintra.com/", "testConnectionParameter");
    private final static QName _AgregarNovedad_QNAME = new QName("http://ws.fintra.com/", "agregarNovedad");
    private final static QName _ListarReanticiposLogin_QNAME = new QName("http://ws.fintra.com/", "listarReanticiposLogin");
    private final static QName _EnviarReanticipoLoginResponse_QNAME = new QName("http://ws.fintra.com/", "enviarReanticipoLoginResponse");
    private final static QName _EnviarAnticipos_QNAME = new QName("http://ws.fintra.com/", "enviarAnticipos");
    private final static QName _ListarReanticiposResponse_QNAME = new QName("http://ws.fintra.com/", "listarReanticiposResponse");
    private final static QName _EnviarAnticiposResponse_QNAME = new QName("http://ws.fintra.com/", "enviarAnticiposResponse");
    private final static QName _EnviarAnticiposLoginResponse_QNAME = new QName("http://ws.fintra.com/", "enviarAnticiposLoginResponse");
    private final static QName _TestConnectionParameterResponse_QNAME = new QName("http://ws.fintra.com/", "testConnectionParameterResponse");
    private final static QName _EnviarReanticipoLogin_QNAME = new QName("http://ws.fintra.com/", "enviarReanticipoLogin");
    private final static QName _ListarReanticipos_QNAME = new QName("http://ws.fintra.com/", "listarReanticipos");
    private final static QName _EnviarReanticipoResponse_QNAME = new QName("http://ws.fintra.com/", "enviarReanticipoResponse");
    private final static QName _ListarReanticiposLoginResponse_QNAME = new QName("http://ws.fintra.com/", "listarReanticiposLoginResponse");
    private final static QName _AnularReanticipoLoginResponse_QNAME = new QName("http://ws.fintra.com/", "anularReanticipoLoginResponse");
    private final static QName _AnularReanticipoResponse_QNAME = new QName("http://ws.fintra.com/", "anularReanticipoResponse");
    private final static QName _AgregarNovedadResponse_QNAME = new QName("http://ws.fintra.com/", "agregarNovedadResponse");
    private final static QName _AnularReanticipo_QNAME = new QName("http://ws.fintra.com/", "anularReanticipo");
    private final static QName _AgregarNovedadLoginResponse_QNAME = new QName("http://ws.fintra.com/", "agregarNovedadLoginResponse");
    private final static QName _AgregarNovedadLogin_QNAME = new QName("http://ws.fintra.com/", "agregarNovedadLogin");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.fintra.ws.transportadoras
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ListarReanticiposLogin }
     * 
     */
    public ListarReanticiposLogin createListarReanticiposLogin() {
        return new ListarReanticiposLogin();
    }

    /**
     * Create an instance of {@link EnviarReanticipoLoginResponse }
     * 
     */
    public EnviarReanticipoLoginResponse createEnviarReanticipoLoginResponse() {
        return new EnviarReanticipoLoginResponse();
    }

    /**
     * Create an instance of {@link EnviarAnticipos }
     * 
     */
    public EnviarAnticipos createEnviarAnticipos() {
        return new EnviarAnticipos();
    }

    /**
     * Create an instance of {@link EnviarReanticipo }
     * 
     */
    public EnviarReanticipo createEnviarReanticipo() {
        return new EnviarReanticipo();
    }

    /**
     * Create an instance of {@link AgregarNovedad }
     * 
     */
    public AgregarNovedad createAgregarNovedad() {
        return new AgregarNovedad();
    }

    /**
     * Create an instance of {@link TestConnectionParameter }
     * 
     */
    public TestConnectionParameter createTestConnectionParameter() {
        return new TestConnectionParameter();
    }

    /**
     * Create an instance of {@link AnularReanticipoLogin }
     * 
     */
    public AnularReanticipoLogin createAnularReanticipoLogin() {
        return new AnularReanticipoLogin();
    }

    /**
     * Create an instance of {@link TestConnection }
     * 
     */
    public TestConnection createTestConnection() {
        return new TestConnection();
    }

    /**
     * Create an instance of {@link EnviarAnticiposLogin }
     * 
     */
    public EnviarAnticiposLogin createEnviarAnticiposLogin() {
        return new EnviarAnticiposLogin();
    }

    /**
     * Create an instance of {@link TestConnectionResponse }
     * 
     */
    public TestConnectionResponse createTestConnectionResponse() {
        return new TestConnectionResponse();
    }

    /**
     * Create an instance of {@link AgregarNovedadResponse }
     * 
     */
    public AgregarNovedadResponse createAgregarNovedadResponse() {
        return new AgregarNovedadResponse();
    }

    /**
     * Create an instance of {@link AnularReanticipo }
     * 
     */
    public AnularReanticipo createAnularReanticipo() {
        return new AnularReanticipo();
    }

    /**
     * Create an instance of {@link AnularReanticipoResponse }
     * 
     */
    public AnularReanticipoResponse createAnularReanticipoResponse() {
        return new AnularReanticipoResponse();
    }

    /**
     * Create an instance of {@link AgregarNovedadLogin }
     * 
     */
    public AgregarNovedadLogin createAgregarNovedadLogin() {
        return new AgregarNovedadLogin();
    }

    /**
     * Create an instance of {@link AgregarNovedadLoginResponse }
     * 
     */
    public AgregarNovedadLoginResponse createAgregarNovedadLoginResponse() {
        return new AgregarNovedadLoginResponse();
    }

    /**
     * Create an instance of {@link ListarReanticipos }
     * 
     */
    public ListarReanticipos createListarReanticipos() {
        return new ListarReanticipos();
    }

    /**
     * Create an instance of {@link EnviarReanticipoLogin }
     * 
     */
    public EnviarReanticipoLogin createEnviarReanticipoLogin() {
        return new EnviarReanticipoLogin();
    }

    /**
     * Create an instance of {@link TestConnectionParameterResponse }
     * 
     */
    public TestConnectionParameterResponse createTestConnectionParameterResponse() {
        return new TestConnectionParameterResponse();
    }

    /**
     * Create an instance of {@link AnularReanticipoLoginResponse }
     * 
     */
    public AnularReanticipoLoginResponse createAnularReanticipoLoginResponse() {
        return new AnularReanticipoLoginResponse();
    }

    /**
     * Create an instance of {@link ListarReanticiposLoginResponse }
     * 
     */
    public ListarReanticiposLoginResponse createListarReanticiposLoginResponse() {
        return new ListarReanticiposLoginResponse();
    }

    /**
     * Create an instance of {@link EnviarReanticipoResponse }
     * 
     */
    public EnviarReanticipoResponse createEnviarReanticipoResponse() {
        return new EnviarReanticipoResponse();
    }

    /**
     * Create an instance of {@link EnviarAnticiposLoginResponse }
     * 
     */
    public EnviarAnticiposLoginResponse createEnviarAnticiposLoginResponse() {
        return new EnviarAnticiposLoginResponse();
    }

    /**
     * Create an instance of {@link EnviarAnticiposResponse }
     * 
     */
    public EnviarAnticiposResponse createEnviarAnticiposResponse() {
        return new EnviarAnticiposResponse();
    }

    /**
     * Create an instance of {@link ListarReanticiposResponse }
     * 
     */
    public ListarReanticiposResponse createListarReanticiposResponse() {
        return new ListarReanticiposResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnviarAnticiposLogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fintra.com/", name = "enviarAnticiposLogin")
    public JAXBElement<EnviarAnticiposLogin> createEnviarAnticiposLogin(EnviarAnticiposLogin value) {
        return new JAXBElement<EnviarAnticiposLogin>(_EnviarAnticiposLogin_QNAME, EnviarAnticiposLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TestConnectionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fintra.com/", name = "testConnectionResponse")
    public JAXBElement<TestConnectionResponse> createTestConnectionResponse(TestConnectionResponse value) {
        return new JAXBElement<TestConnectionResponse>(_TestConnectionResponse_QNAME, TestConnectionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TestConnection }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fintra.com/", name = "testConnection")
    public JAXBElement<TestConnection> createTestConnection(TestConnection value) {
        return new JAXBElement<TestConnection>(_TestConnection_QNAME, TestConnection.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AnularReanticipoLogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fintra.com/", name = "anularReanticipoLogin")
    public JAXBElement<AnularReanticipoLogin> createAnularReanticipoLogin(AnularReanticipoLogin value) {
        return new JAXBElement<AnularReanticipoLogin>(_AnularReanticipoLogin_QNAME, AnularReanticipoLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnviarReanticipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fintra.com/", name = "enviarReanticipo")
    public JAXBElement<EnviarReanticipo> createEnviarReanticipo(EnviarReanticipo value) {
        return new JAXBElement<EnviarReanticipo>(_EnviarReanticipo_QNAME, EnviarReanticipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TestConnectionParameter }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fintra.com/", name = "testConnectionParameter")
    public JAXBElement<TestConnectionParameter> createTestConnectionParameter(TestConnectionParameter value) {
        return new JAXBElement<TestConnectionParameter>(_TestConnectionParameter_QNAME, TestConnectionParameter.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarNovedad }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fintra.com/", name = "agregarNovedad")
    public JAXBElement<AgregarNovedad> createAgregarNovedad(AgregarNovedad value) {
        return new JAXBElement<AgregarNovedad>(_AgregarNovedad_QNAME, AgregarNovedad.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarReanticiposLogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fintra.com/", name = "listarReanticiposLogin")
    public JAXBElement<ListarReanticiposLogin> createListarReanticiposLogin(ListarReanticiposLogin value) {
        return new JAXBElement<ListarReanticiposLogin>(_ListarReanticiposLogin_QNAME, ListarReanticiposLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnviarReanticipoLoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fintra.com/", name = "enviarReanticipoLoginResponse")
    public JAXBElement<EnviarReanticipoLoginResponse> createEnviarReanticipoLoginResponse(EnviarReanticipoLoginResponse value) {
        return new JAXBElement<EnviarReanticipoLoginResponse>(_EnviarReanticipoLoginResponse_QNAME, EnviarReanticipoLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnviarAnticipos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fintra.com/", name = "enviarAnticipos")
    public JAXBElement<EnviarAnticipos> createEnviarAnticipos(EnviarAnticipos value) {
        return new JAXBElement<EnviarAnticipos>(_EnviarAnticipos_QNAME, EnviarAnticipos.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarReanticiposResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fintra.com/", name = "listarReanticiposResponse")
    public JAXBElement<ListarReanticiposResponse> createListarReanticiposResponse(ListarReanticiposResponse value) {
        return new JAXBElement<ListarReanticiposResponse>(_ListarReanticiposResponse_QNAME, ListarReanticiposResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnviarAnticiposResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fintra.com/", name = "enviarAnticiposResponse")
    public JAXBElement<EnviarAnticiposResponse> createEnviarAnticiposResponse(EnviarAnticiposResponse value) {
        return new JAXBElement<EnviarAnticiposResponse>(_EnviarAnticiposResponse_QNAME, EnviarAnticiposResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnviarAnticiposLoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fintra.com/", name = "enviarAnticiposLoginResponse")
    public JAXBElement<EnviarAnticiposLoginResponse> createEnviarAnticiposLoginResponse(EnviarAnticiposLoginResponse value) {
        return new JAXBElement<EnviarAnticiposLoginResponse>(_EnviarAnticiposLoginResponse_QNAME, EnviarAnticiposLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TestConnectionParameterResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fintra.com/", name = "testConnectionParameterResponse")
    public JAXBElement<TestConnectionParameterResponse> createTestConnectionParameterResponse(TestConnectionParameterResponse value) {
        return new JAXBElement<TestConnectionParameterResponse>(_TestConnectionParameterResponse_QNAME, TestConnectionParameterResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnviarReanticipoLogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fintra.com/", name = "enviarReanticipoLogin")
    public JAXBElement<EnviarReanticipoLogin> createEnviarReanticipoLogin(EnviarReanticipoLogin value) {
        return new JAXBElement<EnviarReanticipoLogin>(_EnviarReanticipoLogin_QNAME, EnviarReanticipoLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarReanticipos }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fintra.com/", name = "listarReanticipos")
    public JAXBElement<ListarReanticipos> createListarReanticipos(ListarReanticipos value) {
        return new JAXBElement<ListarReanticipos>(_ListarReanticipos_QNAME, ListarReanticipos.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link EnviarReanticipoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fintra.com/", name = "enviarReanticipoResponse")
    public JAXBElement<EnviarReanticipoResponse> createEnviarReanticipoResponse(EnviarReanticipoResponse value) {
        return new JAXBElement<EnviarReanticipoResponse>(_EnviarReanticipoResponse_QNAME, EnviarReanticipoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ListarReanticiposLoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fintra.com/", name = "listarReanticiposLoginResponse")
    public JAXBElement<ListarReanticiposLoginResponse> createListarReanticiposLoginResponse(ListarReanticiposLoginResponse value) {
        return new JAXBElement<ListarReanticiposLoginResponse>(_ListarReanticiposLoginResponse_QNAME, ListarReanticiposLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AnularReanticipoLoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fintra.com/", name = "anularReanticipoLoginResponse")
    public JAXBElement<AnularReanticipoLoginResponse> createAnularReanticipoLoginResponse(AnularReanticipoLoginResponse value) {
        return new JAXBElement<AnularReanticipoLoginResponse>(_AnularReanticipoLoginResponse_QNAME, AnularReanticipoLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AnularReanticipoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fintra.com/", name = "anularReanticipoResponse")
    public JAXBElement<AnularReanticipoResponse> createAnularReanticipoResponse(AnularReanticipoResponse value) {
        return new JAXBElement<AnularReanticipoResponse>(_AnularReanticipoResponse_QNAME, AnularReanticipoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarNovedadResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fintra.com/", name = "agregarNovedadResponse")
    public JAXBElement<AgregarNovedadResponse> createAgregarNovedadResponse(AgregarNovedadResponse value) {
        return new JAXBElement<AgregarNovedadResponse>(_AgregarNovedadResponse_QNAME, AgregarNovedadResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AnularReanticipo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fintra.com/", name = "anularReanticipo")
    public JAXBElement<AnularReanticipo> createAnularReanticipo(AnularReanticipo value) {
        return new JAXBElement<AnularReanticipo>(_AnularReanticipo_QNAME, AnularReanticipo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarNovedadLoginResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fintra.com/", name = "agregarNovedadLoginResponse")
    public JAXBElement<AgregarNovedadLoginResponse> createAgregarNovedadLoginResponse(AgregarNovedadLoginResponse value) {
        return new JAXBElement<AgregarNovedadLoginResponse>(_AgregarNovedadLoginResponse_QNAME, AgregarNovedadLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AgregarNovedadLogin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.fintra.com/", name = "agregarNovedadLogin")
    public JAXBElement<AgregarNovedadLogin> createAgregarNovedadLogin(AgregarNovedadLogin value) {
        return new JAXBElement<AgregarNovedadLogin>(_AgregarNovedadLogin_QNAME, AgregarNovedadLogin.class, null, value);
    }

}
