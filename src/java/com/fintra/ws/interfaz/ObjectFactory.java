
package com.fintra.ws.interfaz;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.fintra.ws.interfaz package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ConsumoHCPuenteResponse_QNAME = new QName("http://interfaz.fintra.com/", "consumoHCPuenteResponse");
    private final static QName _ConsumoHCPuente_QNAME = new QName("http://interfaz.fintra.com/", "consumoHCPuente");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.fintra.ws.interfaz
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ConsumoHCPuente }
     * 
     */
    public ConsumoHCPuente createConsumoHCPuente() {
        return new ConsumoHCPuente();
    }

    /**
     * Create an instance of {@link ConsumoHCPuenteResponse }
     * 
     */
    public ConsumoHCPuenteResponse createConsumoHCPuenteResponse() {
        return new ConsumoHCPuenteResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsumoHCPuenteResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interfaz.fintra.com/", name = "consumoHCPuenteResponse")
    public JAXBElement<ConsumoHCPuenteResponse> createConsumoHCPuenteResponse(ConsumoHCPuenteResponse value) {
        return new JAXBElement<ConsumoHCPuenteResponse>(_ConsumoHCPuenteResponse_QNAME, ConsumoHCPuenteResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsumoHCPuente }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://interfaz.fintra.com/", name = "consumoHCPuente")
    public JAXBElement<ConsumoHCPuente> createConsumoHCPuente(ConsumoHCPuente value) {
        return new JAXBElement<ConsumoHCPuente>(_ConsumoHCPuente_QNAME, ConsumoHCPuente.class, null, value);
    }

}
