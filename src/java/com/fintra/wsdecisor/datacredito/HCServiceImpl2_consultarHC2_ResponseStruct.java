// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, compilación R1)
// Generated source version: 1.1.3

package com.fintra.wsdecisor.datacredito;


public class HCServiceImpl2_consultarHC2_ResponseStruct {
    protected java.lang.String consultarHC2Return;
    
    public HCServiceImpl2_consultarHC2_ResponseStruct() {
    }
    
    public HCServiceImpl2_consultarHC2_ResponseStruct(java.lang.String consultarHC2Return) {
        this.consultarHC2Return = consultarHC2Return;
    }
    
    public java.lang.String getConsultarHC2Return() {
        return consultarHC2Return;
    }
    
    public void setConsultarHC2Return(java.lang.String consultarHC2Return) {
        this.consultarHC2Return = consultarHC2Return;
    }
}
