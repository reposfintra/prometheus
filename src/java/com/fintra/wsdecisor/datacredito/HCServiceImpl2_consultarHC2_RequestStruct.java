// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, compilación R1)
// Generated source version: 1.1.3

package com.fintra.wsdecisor.datacredito;


public class HCServiceImpl2_consultarHC2_RequestStruct {
    protected java.lang.String xmlConsulta;
    
    public HCServiceImpl2_consultarHC2_RequestStruct() {
    }
    
    public HCServiceImpl2_consultarHC2_RequestStruct(java.lang.String xmlConsulta) {
        this.xmlConsulta = xmlConsulta;
    }
    
    public java.lang.String getXmlConsulta() {
        return xmlConsulta;
    }
    
    public void setXmlConsulta(java.lang.String xmlConsulta) {
        this.xmlConsulta = xmlConsulta;
    }
}
