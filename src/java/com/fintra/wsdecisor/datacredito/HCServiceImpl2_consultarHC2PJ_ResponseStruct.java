// This class was generated by the JAXRPC SI, do not edit.
// Contents subject to change without notice.
// JAX-RPC Standard Implementation (1.1.3, compilación R1)
// Generated source version: 1.1.3

package com.fintra.wsdecisor.datacredito;


public class HCServiceImpl2_consultarHC2PJ_ResponseStruct {
    protected java.lang.String consultarHC2PJReturn;
    
    public HCServiceImpl2_consultarHC2PJ_ResponseStruct() {
    }
    
    public HCServiceImpl2_consultarHC2PJ_ResponseStruct(java.lang.String consultarHC2PJReturn) {
        this.consultarHC2PJReturn = consultarHC2PJReturn;
    }
    
    public java.lang.String getConsultarHC2PJReturn() {
        return consultarHC2PJReturn;
    }
    
    public void setConsultarHC2PJReturn(java.lang.String consultarHC2PJReturn) {
        this.consultarHC2PJReturn = consultarHC2PJReturn;
    }
}
