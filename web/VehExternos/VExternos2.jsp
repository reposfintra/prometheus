<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%

//model.VExternosSvc.LISTALL();
List                ListaGnral  	= model.VExternosSvc.getList();
List				ListaAgencias 	= model.ciudadService.ListarAgencias();
//List                Lista      	= model.SalidaCSvc.getListaElements(); 

Util ut								= new Util();
veh_externos        Datos       	= model.VExternosSvc.getDato();
String              Placa      		= (Datos!=null)?Datos.getPlaca()  :"";
String              Fecha_disp 		= (Datos!=null)?ut.ConvertirToString(Datos.getFecha_disp()).substring(0,10):"";
String              Tiempo_vigencia = (Datos!=null)?String.valueOf(Datos.getTiempo_vigencia()):"";
String              Agencia        	= (Datos!=null)?Datos.getAgencia_disp():"";

String Mensaje       = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
//String NombreBoton   = (Datos==null)?"Guardar":"Modificar";
String NombreBoton1  = (ListaGnral!=null && ListaGnral.size()>0)?"Ocultar Lista":"Listado";
//String BloquearText  = (Datos==null)?"":"ReadOnly";
//String BloquearSelect= (Lista!=null && Lista.size()>0)?"":"Disabled";
//String BloquearBoton = (Datos!=null)?"":"Disabled";
%>

<html>
<head>
  <title>Vehiculos externos en turnos</title>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<script src='<%=BASEURL%>/js/date-picker.js'></script>
	<script src='<%=BASEURL%>/js/validar.js'></script>
    <link href="/css/estilo.css" rel="stylesheet" type="text/css">
	<link href="<%=BASEURL%>/css/estilo.css" rel="stylesheet" type="text/css">   
</head>
<body>
<FORM name='formulario' method='POST' action="<%=CONTROLLER%>?estado=VExternos&accion=Manager" >

    <table width='478' cellpadding='0' cellspacing='0' class='fondotabla' border='1'>
        <tr class='titulo'>
            <td width="474" height='50' colspan='2' align='center'>VEHICULOS EXTERNOS EN TURNO</td>
        </tr>
        <tr class='fila'>
            <TD colspan='2'  height='30' class='fondogris' >&nbsp Placa vehiculo :&nbsp&nbsp
               
                <span class="Letras">
                <input name='placa' type='text' class="textbox" id="placa" style='width:120' value='' >
                </span>
          <input class='boton' onclick="Opcion.value='SeleccionarE';"  type='submit' value='Seleccionar' >          </TD>
      <tr>
             <th colspan='2' class="fila"><br>
                <table  border='0' width='100%' cellspacing='0' cellpadding='0'>                    
                     <tr>
                          <td width='181' class='fila'><div align="left">&nbsp Fecha Disponibilidad: </div></td>
                          <td width="293" nowrap bordercolor="#CCCCCC" bgcolor="ECE0D8" class="Letras"><span class="fila">
                            <input name='fechadisp' type='text' class="textbox" id="fechadisp" style='width:40%' value='' readonly>
                            <a href="javascript:void(0)" onclick="jscript: show_calendar('fechadisp');" HIDEFOCUS><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/Calendario/calbtn.gif" width="34" height="22" border="0" alt=""></a></span></td>
                     </tr>
                     <tr>
                        <td width='181' class='fila'><div align="left">&nbsp Tiempo vigencia :</div></td>
                        <td >   <input name='tiempovigencia' type='text' class='textbox' id="tiempovigencia"style='width:40%;' maxlength='5'></td>
                    </tr>
                     <tr>
                       <td class='fila'>&nbsp Agencia : </td>
                       <td ><span class="fila">
                         <select   name='agencia' class='textbox' id="agencia" style='width:40%'>
                           <%
                     if(ListaAgencias!=null) {
                         Iterator It = ListaAgencias.iterator();
                         while(It.hasNext()) {
                              Ciudad datos = (Ciudad) It.next();
                              out.print("<option value='"+datos.getCodCiu()+"'>["+datos.getCodCiu()+"] "+datos.getNomCiu()+"</option> \n");
                         }
                      }
                    %>
                       </select>
                       </span>                         <INPUT name="Nuevo" type='submit' class='boton' id="Nuevo"              style='width:25%;' onclick="Opcion.value='Nuevo';"               value='Nuevo'></td></tr>                    
                </table>
                <br>        </th>
      </tr>
        <tr>
          <th height='50'  colspan='2' class="fila">                
                <INPUT name="Guardar" type='submit' class='boton'   style='width:120;' onclick="Opcion.value='Guardar';" value="Guardar"  >
                <INPUT name="Modificar" type='submit' class='boton'   style='width:120;' onclick="Opcion.value='Modificar';" value="Modificar"  >
          <INPUT type='submit' class='boton' onclick="Opcion.value='<%=NombreBoton1 %>';"  value='<%=NombreBoton1 %>' style='width:120' >          </th>
        </tr>
  </table>
<br>
<input type='hidden' name='Opcion'/> 
</FORM>
<script>
  formulario.placa.value = '<%= Placa %>';
  formulario.fechadisp.value = '<%= Fecha_disp %>';
  formulario.tiempovigencia.value = '<%= Tiempo_vigencia %>';
  formulario.agencia.value = '<%= Agencia %>';
</script>



<center class='comentario'>
<font color='red'>
<%=Mensaje%>
</font>

<%  if(ListaGnral!=null && ListaGnral.size()>0) { %>
    <form action="<%=CONTROLLER%>?estado=VExternos&accion=Manager" method='post' name='FormularioListado'>
        <br>
        <table  border='1' cellpadding='1' cellspacing='1' width='788'>
        <tr><th class='titulo' height='50' colspan='9'>LISTADO GENERAL DE REGISTROS</th></tr>
        <tr>
          <th class='titulo' height='50' colspan='9'><table width="700" border='0'>
            <tr class='comentario2' style="color:white;">
              <td width='40' height="45">Filtro</td>
              <td width='220'>Agencias                </td>
              <td width='390'><select name='filtroagencias' id="select2" >
                <option value="ALL">Todos</option>
                <%
                       if(ListaAgencias.size()>0) {
                           Iterator It3 = ListaAgencias.iterator();
                           while(It3.hasNext()) {
                               Ciudad  datos2 =  (Ciudad) It3.next();
                               //String Sele = (model.ElementoSvc.getelemento().getClasificacion().equals(datos.getCodigo()))?"selected='selected'":" ";
                               //out.print("<option "+Sele+" value='"+datos2.getCodigo()+"'>"+datos2.getdescripcion_elemento()+"</option> \n");
							   out.print("<option value='"+datos2.getCodCiu()+"'>["+datos2.getCodCiu()+"] "+datos2.getNomCiu()+"</option> \n");
                           }
                       }
                       %>
              </select>
                <INPUT type='submit' class='boton' onclick="Opcion.value='Mostrar';"    value='Mostrar'   style='width:120;'>
              <td width='50'>&nbsp;
              </td>
            </tr>
          </table></th>
        </tr>
        <tr class='subtitulos'>
            <th width='26' >N�</th>
            <th width='26' ><input type='checkbox' name='All' onclick='jscript: SelAll();'></th>
            <th width='65' >PLACA</th>
            <th width='303'>FECHA DISPONIBILIDAD</th>
            <th width='204' >TIEMPO DE VIGENCIA</th>
            <th width='131' >AGENCIA DE UTILIZACION</th>
        </tr>
        <%
           int Cont = 1;
			//ListaGnral = model.SalidaCSvc.paginacion.getListado();
			Iterator it2 = ListaGnral.iterator();
            while(it2.hasNext()){
              	veh_externos dat = (veh_externos) it2.next();
				          String Estilo = (dat.getEstado().equals("A"))?"fondoverde":"fila";
                %>
        <tr class ='<%= Estilo %>' >
            <td class = 'fila' align='center'><span class='fila'><%=Cont++%></span></td>
            <th ><input type='checkbox' name='LOV' value='<%=dat.getPlaca()+"/"+ut.ConvertirToString(dat.getFecha_disp()) %>' onclick='jscript: ActAll();'></th>
            <td align='center'><a href='<%=CONTROLLER%>?estado=VExternos&accion=Manager&Opcion=Seleccionar2&Sel=<%=dat.getPlaca()+"/"+ut.ConvertirToString(dat.getFecha_disp())%>' ><%= dat.getPlaca() %></a></td>
            <td align="center"><%= dat.getFecha_disp()  %></td>
            <td align='center'><%= dat.getTiempo_vigencia()  %></td>
            <td align='center'><%= dat.getAgencia_disp()  %></td>
        </tr>
        <%  }   %>        
        </table>
        <br>
        <input type='hidden' name='Opcion'/> 
        <INPUT type='submit' class='boton' onclick="Opcion.value='Anular';"    value='Anular'   style='width:120;'>
        <INPUT name="Activar" type='submit' class='boton' id="Activar"   style='width:120;' onclick="Opcion.value='Activar';"    value='Activar'>
        <INPUT name="Eliminar" type='submit' class='boton' id="Eliminar"   style='width:120;' onclick="Opcion.value='Eliminar';"    value='Eliminar'>
    </form>
<%  } %>

</center>
</body>
</html>
