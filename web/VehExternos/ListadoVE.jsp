<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%

//model.VExternosSvc.LISTALL();
List                ListaGnral  	= model.VExternosSvc.getList();
List				ListaAgencias 	= model.ciudadService.ListarAgencias();
//List                Lista      	= model.SalidaCSvc.getListaElements(); 

String Mensaje       = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
//String NombreBoton   = (Datos==null)?"Guardar":"Modificar";

//String BloquearText  = (Datos==null)?"":"ReadOnly";
//String BloquearSelect= (Lista!=null && Lista.size()>0)?"":"Disabled";
//String BloquearBoton = (Datos!=null)?"":"Disabled";
%>

<html>
<head>
  <title>Vehiculos externos en turnos</title>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<script src='<%=BASEURL%>/js/validar.js'></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
	 <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
	 
	 	<script>	
	
	function SelAllVE(){
            for(i=0;i<FormularioListado.length;i++)
                    FormularioListado.elements[i].checked=FormularioListado.All.checked;
    }
    function ActAllVE(){
            FormularioListado.All.checked = true;
            for(i=1;i<FormularioListado.length;i++)	
                    if (!FormularioListado.elements[i].checked){
                            FormularioListado.All.checked = false;
                            break;
                    }
    }    	
	
	function verificar_checkboxVE(){
		if( FormularioListado.Opcion.value != 'Mostrar'){
			for(i=1;i<FormularioListado.length;i++)	
                    if (FormularioListado.elements[i].checked)
                            return true;
            alert('Por favor seleccione un registro para poder continuar');
            return false;
		}
	}
	
	</script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Vehiculos Externos"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
		<form action="<%=CONTROLLER%>?estado=VExternos&accion=Manager" method='post' name='FormularioListado' onsubmit='return verificar_checkboxVE();'>
		<input id="Opcion" type="hidden" value="" name="Opcion"/>
		<table border='2' width='800' align="center">
		 <tr>
               <td width="50%" class="subtitulo1">&nbsp;LISTADO GENERAL DE REGISTROS</td>
                <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
		</table>		
		<table  border='2' width='800' align="center">           
            <th class='titulo' colspan='9'>
			<table width="100%" border='0'>
                <tr class="tblTitulo">
                <td width='15%' align="center">&nbsp;Filtro</td>
                <td width='10%'>&nbsp;&nbsp;Agencias</td>
                <td width='30%' align="center"><select name='filtroagencias' id="select2" >
                <option value="ALL">Todos</option>
                <%
                       if(ListaAgencias.size()>0) {
                           Iterator It3 = ListaAgencias.iterator();
                           while(It3.hasNext()) {
                               Ciudad  datos2 =  (Ciudad) It3.next();                               
                               out.print("<option value='"+datos2.getCodCiu()+"'>["+datos2.getCodCiu()+"] "+datos2.getNomCiu()+"</option> \n");
                           }
                       }
                       %>
                </select>
                
                <td width='50'><INPUT name="image" type="image"  onclick="Opcion.value='Mostrar';"    value='Mostrar' src="<%=BASEURL%>/images/botones/buscar.gif" align="middle" height="21" title="Haga click para buscar">
                </td>
                </tr>
				
						               
		</table>
		</th>
            </tr>
			<% if(ListaGnral!=null && ListaGnral.size()>0) { %>
            <tr class="tblTitulo">
                <td width='5%' align="center">N�</th>
                <td width='5%' align="center"><input type='checkbox' name='All' onclick='jscript: SelAllVE();'></th>
                <td width='15%' align="center">PLACA</th>
                <td width='25%' align="center">FECHA DISPONIBILIDAD</th>
                <td width='15%' align="center">VIGENCIA</th>
                <td width='25%' align="center">AGENCIA DE UTILIZACION</th>
            </tr>
			
			<%
           	int Cont = 1;
            Iterator it2 = ListaGnral.iterator();
            while(it2.hasNext()){
              	veh_externos dat = (veh_externos) it2.next();
                String e = (Cont % 2 == 0 )?"filaazul":"filagris";
                String Estilo = (dat.getEstado().equals("A"))?"filaresaltada":e;
                %>
            <tr class ='<%= Estilo %>' >
            <td align='center' class="bordereporte"><%=Cont++%></td>
            <th class="bordereporte"><input type='checkbox' name='LOV' value='<%=dat.getPlaca()+"/"+dat.getFecha_disp() %>' onclick='jscript: ActAllVE();'></th>
            <td align='center' class="bordereporte"><a href='<%=CONTROLLER%>?estado=VExternos&accion=Manager&Opcion=Seleccionar2&Sel=<%=dat.getPlaca()+"/"+dat.getFecha_disp()%>' title="Haga click para modificar"><%= dat.getPlaca() %></a></td>
            <td align="center" class="bordereporte"><%= dat.getFecha_disp()  %></td>
            <td align='center' class="bordereporte"><%= dat.getTiempo_vigencia()  %></td>
            <td align='center' class="bordereporte"><%= model.ciudadService.obtenerNombreCiudad(dat.getAgencia_disp())  %></td>
            </tr>
        <%  }   %> 			
        <%}%>
		</table>   
		<br>
		<% if(ListaGnral!=null && ListaGnral.size()>0) { %>
		<table width="100%">
		<tr align="center">
		  <td>
		<INPUT name="image22" type="image" title="Haga click para Anular" onclick="Opcion.value='Anular';"    value='Anular' src="<%=BASEURL%>/images/botones/anular.gif" align="middle" height="21">
          <INPUT name="image22" type="image"  onclick="Opcion.value='Activar';"  title="Haga click para Activar"  value='Activar' src="<%=BASEURL%>/images/botones/activar.gif" align="middle" height="21">
          <INPUT name="image22" type="image"  onclick="Opcion.value='Eliminar';"  title="Haga click para Eliminar"  value='Eliminar' src="<%=BASEURL%>/images/botones/eliminar.gif" align="middle" height="21">
			</td>
		</tr>
		</table>
		 <%}%>
		<p>&nbsp;</p>
		<p>
		  
          <%if(!Mensaje.equals("")){%>
        </p>
		<p>        
        <table border="2" align="center" width="70%">
          <tr>
            <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                <tr>
                  <td width="500" align="center" class="mensajes"><%=Mensaje%></td>
                  <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                  <td width="58">&nbsp;</td>
                </tr>
            </table></td>
          </tr>
        </table>
		<p>
		<%}%>
		<table width="100%">
		<tr align="center">
			<td>
          <img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" >
        </p>
			</td>
		</tr>
		</table>
        <p></p>
        
		</form>   
</div>  
</body>
</html>
