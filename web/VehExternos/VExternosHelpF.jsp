<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
  <title>Vehiculos Externos Insertar - Ayuda</title>
  <META http-equiv=Content-Type content="text/html; charset=windows-1252">
  <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>
<body>

<% String BASEIMG = BASEURL +"/images/ayuda/VehExternos/"; %>

<table width="100%"  border="2" align="center">
    <tr>
      <td >
 
            <table width='99%' align="center" cellpadding='0' cellspacing='0'>
                 <tr class="subtitulo" ><td height="20"><div align="center">MANUAL DE VEHICULOS EXTERNOS </div></td></tr>
                 <tr class="subtitulo1"> <td>Descripci&oacute;n del funcionamiento </td> </tr>

                 
                 <tr>
                      <td  class="ayudaHtmlTexto" height='40'>
                      El programa permite realizar funciones basicas de mantenimiento sobre la tabla de vehiculos externos, Para el ingreso de un registro se presentar&aacute; una vista como lo indica la Figura 1.</td>
                 </tr>
          
                 <tr><td  align="center" ><img  src="<%= BASEIMG%>Insertar.JPG" >                  <br>
                     <strong>Figura 1</strong></td>
                 </tr>
                 
                 <tr>
                      <td  class="ayudaHtmlTexto" height='90'>
                      En la pantalla de ingreso se solicita informacion  correspondiente a la placa del vehiculo, fecha de disponibilidad, Tiempo de vigencia y una agencia donde se encuentra el vehiculo disponible. </td>
                 </tr>
                 
                 <tr>
                      <td  class="ayudaHtmlTexto" height='50'>
                         En caso dado de que no se registre informacion en un campo obligatorio al momento de dar click en el boton de aceptar, se presenta en pantalla un mensaje de alerta como en la Figura 2. indicando que no se puede continuar si no digita esa informacion.
                         <br>
                         <br>                   </td>
                 </tr>
                 
                 <tr><th  ><img  src="<%= BASEIMG%>Alerta1.JPG">           <br>Figura 2</th></tr>
                 
                 <tr>
                      <td  class="ayudaHtmlTexto" height='90'>
                         En caso de que la fecha de disponibilidad sea menor a la fecha actual, aparecera un mensaje de alerta como en la Figura 3. indicando que la fecha debe ser mayor o igual a la fecha actual. </td>
                 </tr>
                 
                 <tr><th  ><img  src="<%= BASEIMG%>Alerta2.JPG">           <br>Figura 3</th></tr>
                 
                 <tr>
                      <td  class="ayudaHtmlTexto" height='90'>
                         Para registrar un vehiculo en la tabla, deber� hacer click en el bot�n "Aceptar", de lo contrario haga click en el bot�n "Restablecer".
                         para limpiar el formulario, si la operacion fue existosa, aparecera en pantalla un mensaje como en la Figura 4.						 
                      </td>
                 </tr>
                 
                 <tr><th  ><img  src="<%= BASEIMG%>MsgConfirmacion.JPG">      <br>Figura 4</th></tr>
                 
                                                                                 
        </table>
            
      </td>
  </tr>
</table>

</body>
</html>
