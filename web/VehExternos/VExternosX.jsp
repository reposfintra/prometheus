<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%

//model.VExternosSvc.LISTALL();
List                ListaGnral  	= model.VExternosSvc.getList();
List				ListaAgencias 	= model.ciudadService.ListarAgencias();
//List                Lista      	= model.SalidaCSvc.getListaElements(); 

//DATOS PLACA
Placa 				placa			= model.placaService.getPlaca( );
Util ut								= new Util();
String 				Propietario		= (placa!=null)?placa.getPropietario()  :"";
String 				Conductor		= (placa!=null)?placa.getConductor()  :"";
String 				Marca			= (placa!=null)?placa.getMarca()  :"";
String				Clase			= (placa!=null)?placa.getClase()  :"";
String				Capacidad		= (placa!=null)?placa.getCapacidad()  :"";
String				Carroceria		= (placa!=null)?placa.getCarroceria()  :"";
String				Modelo			= (placa!=null)?placa.getModelo()  :"";
String				Nomotor			= (placa!=null)?placa.getNomotor()  :"";

//DATOS VEH_EXTERNOS
veh_externos        Datos       	= model.VExternosSvc.getDato();
String              Placa      		= (Datos!=null)?Datos.getPlaca()  :"";
String              Fecha_disp 		= (Datos!=null)?ut.ConvertirToString(Datos.getFecha_disp()).substring(0,10):"";
String              Tiempo_vigencia = (Datos!=null)?String.valueOf(Datos.getTiempo_vigencia()):"";
String              Agencia        	= (Datos!=null)?Datos.getAgencia_disp():"";

String Mensaje       = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
String NombreBoton   = (Datos==null)?"Guardar":"Modificar";
String NombreBoton1  = (ListaGnral!=null && ListaGnral.size()>0)?"Ocultar Lista":"Listado";
//String BloquearText  = (Datos==null)?"":"ReadOnly";
//String BloquearSelect= (Lista!=null && Lista.size()>0)?"":"Disabled";
//String BloquearBoton = (Datos!=null)?"":"Disabled";
%>

<html>
<head>
  <title>Vehiculos externos en turnos</title>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<script src='<%=BASEURL%>/js/date-picker.js'></script>
	<script src='<%=BASEURL%>/js/validar.js'></script>
    <link href="/css/estilo.css" rel="stylesheet" type="text/css">
	<link href="<%=BASEURL%>/css/estilo.css" rel="stylesheet" type="text/css">   
</head>
<body>
<FORM name='formulario' method='POST' action="<%=CONTROLLER%>?estado=VExternos&accion=Manager" onsubmit='return validarVE(this);' >

    <table width="879" border="0" >
      <tr>
        <td width="478"><table width='478' cellpadding='0' cellspacing='0' class='fondotabla' border='1'>
          <tr class='titulo'>
            <td width="474" height='35' colspan='2' align='center'>VEHICULOS EXTERNOS EN TURNO</td>
          </tr>
          <tr class='fila'>
            <TD colspan='2'  height='37' class='fondogris' >&nbsp Placa vehiculo :&nbsp&nbsp <span class="Letras">
              <input name='placa' type='text' class="textbox" id="placa" style='width:120' value='' >
              </span>
                <input name="submit"  type='submit' class='boton' onclick="Opcion.value='Seleccionar';" value='Seleccionar' >            </TD>
          <tr>
            <th height="134" colspan='2' class="fila"><br>
                <table  border='0' width='100%' cellspacing='0' cellpadding='0'>
                  <tr>
                    <td width='181' class='fila'><div align="left">&nbsp Fecha Disponibilidad: </div></td>
                    <td width="293" nowrap bordercolor="#CCCCCC" bgcolor="ECE0D8" class="Letras">
                      <input name='fechadisp' type='text' class="textbox" id="fechadisp" style='width:40%' value='' readonly>
                      <a href="javascript:void(0)" onclick="jscript: show_calendar('fechadisp');" HIDEFOCUS><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/Calendario/calbtn.gif" width="34" height="22" border="0" alt=""></a></td>
                  </tr>
                  <tr>
                    <td width='181' class='fila'><div align="left">&nbsp Tiempo vigencia :</div></td>
                    <td >
                      <input name='tiempovigencia' type='text' class='textbox' id="tiempovigencia"style='width:40%;' maxlength='5' onKeyPress="soloDigitos(event,'noDec')"></td>
                  </tr>
                  <tr>
                    <td class='fila'>&nbsp Agencia : </td>
                    <td >
                      <select   name='agencia' class='textbox' id="agencia" style='width:40%'>
                        <%
                     if(ListaAgencias!=null) {
                         Iterator It = ListaAgencias.iterator();
                         while(It.hasNext()) {
                              Ciudad datos = (Ciudad) It.next();
                              out.print("<option value='"+datos.getCodCiu()+"'>["+datos.getCodCiu()+"] "+datos.getNomCiu()+"</option> \n");
                         }
                      }
                    %>
                      </select>
                    </td>
                  </tr>
                </table>
                <br>
                <INPUT name="Nuevo" type='submit' class='boton' id="Nuevo3"              style='width:200;' onclick="Opcion.value='Nuevo';"               value='Nuevo vehiculo externo'>            </th>
          </tr>
          <tr>
            <th height='59'  colspan='2' class="fila">
              <INPUT name="submit2" type='submit' class='boton'  style='width:25%;' onclick="Opcion.value='<%=NombreBoton  %>';"  value='<%=NombreBoton %>'>
              <INPUT name="submit" type='submit' class='boton' style='width:120' onclick="Opcion.value='<%=NombreBoton1 %>';"  value='<%=NombreBoton1 %>' >            </th>
          </tr>
        </table></td>
		<% if(placa!=null){ %>
        <td width="391"><table width="100%" height="268"  class='fondotabla' border="1">
          <tr class='titulo'>
            <td height="34" align="center">INFORMACION VEHICULO </td>
          </tr>
          <tr class='fila'>
            <td height="169"><table width="100%"  border="0" class="fila">
              <tr>
                <td width="22%">Propietario </td>
                <td width="29%"><input name='propietario' type='text' class='textbox' id="tiempovigencia3"style='width:100;' readonly="true"></td>
                <td width="22%">Conductor</td>
                <td width="27%"><input name='conductor' type='text' class='textbox' id="propietario7"style='width:100;' readonly="true"></td>
              </tr>
              <tr>
                <td>Marca</td>
                <td><input name='marca' type='text' class='textbox' id="propietario4"style='width:100;' readonly="true"></td>
                <td>Clase</td>
                <td><input name='clase' type='text' class='textbox' id="propietario8"style='width:100;' readonly="true"></td>
              </tr>
              <tr>
                <td>Capacidad</td>
                <td><input name='capacidad' type='text' class='textbox' id="propietario5"style='width:100;' readonly="true"></td>
                <td>Carroceria</td>
                <td><input name='carroceria' type='text' class='textbox' id="propietario9"style='width:100;' readonly="true"></td>
              </tr>
              <tr>
                <td>Modelo</td>
                <td><input name='modelo' type='text' class='textbox' id="propietario6"style='width:100;' readonly="true"></td>
                <td>N&ordm; Motor </td>
                <td><input name='nmotor' type='text' class='textbox' id="propietario10"style='width:100;' readonly="true"></td>
              </tr>
            </table></td>
          </tr>
          <tr class="fila">
            <td><span class="fila">
              <INPUT name="Ocultar" type='submit' class='boton' id="Ocultar"   style='width:120;' onclick="Opcion.value='Ocultar';" value="Ocultar" align="middle">
            </span></td>
          </tr>
        </table></td>
		<%}%>
      </tr>
    </table>
	
  <br>
<input type='hidden' name='Opcion'/> 
</FORM>
<script>
  formulario.placa.value = '<%= Placa %>';
  formulario.fechadisp.value = '<%= Fecha_disp %>';
  formulario.tiempovigencia.value = '<%= Tiempo_vigencia %>';
  formulario.agencia.value = '<%= Agencia %>';
  
  formulario.propietario.value = '<%= Propietario %>';
  formulario.conductor.value = '<%= Conductor %>';
  formulario.marca.value = '<%= Marca %>';
  formulario.clase.value = '<%= Clase %>';
  formulario.carroceria.value = '<%= Carroceria %>';
  formulario.modelo.value = '<%= Modelo %>';
  formulario.capacidad.value = '<%= Capacidad %>';
  formulario.nmotor.value = '<%= Nomotor %>';
</script>



<center class='comentario'>
<font color='red'>
<%=Mensaje%>
</font>


    <form action="<%=CONTROLLER%>?estado=VExternos&accion=Manager" method='post' name='FormularioListado'>
        <br>
        <table  border='1' cellpadding='1' cellspacing='1' width='788'>
        <tr><th class='titulo' height='50' colspan='9'>LISTADO GENERAL DE REGISTROS</th></tr>
        <tr>
          <th class='titulo' height='50' colspan='9'><table width="700" border='0'>
            <tr class='comentario2' style="color:white;">
              <td width='40' height="45">Filtro</td>
              <td width='220'>Agencias                </td>
              <td width='390'><select name='filtroagencias' id="select2" >
                <option value="ALL">Todos</option>
                <%
                       if(ListaAgencias.size()>0) {
                           Iterator It3 = ListaAgencias.iterator();
                           while(It3.hasNext()) {
                               Ciudad  datos2 =  (Ciudad) It3.next();
                               //String Sele = (model.ElementoSvc.getelemento().getClasificacion().equals(datos.getCodigo()))?"selected='selected'":" ";
                               //out.print("<option "+Sele+" value='"+datos2.getCodigo()+"'>"+datos2.getdescripcion_elemento()+"</option> \n");
							   out.print("<option value='"+datos2.getCodCiu()+"'>["+datos2.getCodCiu()+"] "+datos2.getNomCiu()+"</option> \n");
                           }
                       }
                       %>
              </select>
                <INPUT type='submit' class='boton' onclick="Opcion.value='Mostrar';"    value='Mostrar'   style='width:120;'>
              <td width='50'>&nbsp;
              </td>
            </tr>
          </table></th>
          <%  if(ListaGnral!=null && ListaGnral.size()>0) { %>
        </tr>
        <tr class='subtitulos'>
            <th width='26' >N�</th>
            <th width='26' ><input type='checkbox' name='All' onclick='jscript: SelAll();'></th>
            <th width='65' >PLACA</th>
            <th width='303'>FECHA DISPONIBILIDAD</th>
            <th width='204' >TIEMPO DE VIGENCIA</th>
            <th width='131' >AGENCIA DE UTILIZACION</th>
        </tr>
        <%
           int Cont = 1;
			//ListaGnral = model.SalidaCSvc.paginacion.getListado();
			Iterator it2 = ListaGnral.iterator();
            while(it2.hasNext()){
              	veh_externos dat = (veh_externos) it2.next();
				          String Estilo = (dat.getEstado().equals("A"))?"fondoverde":"fila";
                %>
        <tr class ='<%= Estilo %>' >
            <td class = 'fila' align='center'><span class='fila'><%=Cont++%></span></td>
            <th ><input type='checkbox' name='LOV' value='<%=dat.getPlaca()+"/"+ut.ConvertirToString(dat.getFecha_disp()) %>' onclick='jscript: ActAll();'></th>
            <td align='center'><a href='<%=CONTROLLER%>?estado=VExternos&accion=Manager&Opcion=Seleccionar2&Sel=<%=dat.getPlaca()+"/"+ut.ConvertirToString(dat.getFecha_disp())%>' ><%= dat.getPlaca() %></a></td>
            <td align="center"><%= dat.getFecha_disp()  %></td>
            <td align='center'><%= dat.getTiempo_vigencia()  %></td>
            <td align='center'><%= dat.getAgencia_disp()  %></td>
        </tr>
        <%  }   %>        
        </table>
        <br>
        <input type='hidden' name='Opcion'/> 
        <INPUT type='submit' class='boton' onclick="Opcion.value='Anular';"    value='Anular'   style='width:120;'>
        <INPUT name="Activar" type='submit' class='boton' id="Activar"   style='width:120;' onclick="Opcion.value='Activar';"    value='Activar'>
        <INPUT name="Eliminar" type='submit' class='boton' id="Eliminar"   style='width:120;' onclick="Opcion.value='Eliminar';"    value='Eliminar'>
    </form>
<%  } %>

</center>
</body>
</html>
