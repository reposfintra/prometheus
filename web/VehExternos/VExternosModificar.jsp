<!--
- Date  : 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, permite realizar las funciones basicas sobre el programa de tipo de impuesto
--%> 
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%

//model.VExternosSvc.LISTALL();
List                ListaGnral  	= model.VExternosSvc.getList();
List				ListaAgencias 	= model.ciudadService.ListarAgencias();
//List                Lista      	= model.SalidaCSvc.getListaElements(); 

//DATOS PLACA

Placa 				placa			= model.placaService.getPlaca( );
Util ut								= new Util();
 				
String 				id_mims			= (placa!=null)?placa.getPropietario()  :"";	
String 				Conductor		= (placa!=null)?placa.getConductor()  :"";
String				Capacidad		= (placa!=null)?placa.getCapacidad()  :"";
String				Modelo			= (placa!=null)?placa.getModelo()  :"";
String				Marca			= (placa!=null)?placa.getMarca()  :"";

NitSot nitS							= model.propService.buscarPropietarioNIT(id_mims);


String 				Ciudad			= (nitS!=null)?nitS.getCodciu()  :"";
String				Telefono		= (nitS!=null)?nitS.getTelefono() :"";
String				Direccion		= (nitS!=null)?nitS.getDireccion() :"";
String 				Propietario		= (nitS!=null)?nitS.getNombre():"";
//String Propietario = "Juan";
String 				Style			= (placa!=null)?"display:block":"display:none";
String				Ancho			= (placa!=null)?"100%":"50%";

//DATOS VEH_EXTERNOS
veh_externos        Datos       	= model.VExternosSvc.getDato();
String              Placa      		= (Datos!=null)?Datos.getPlaca()  :"";
String              Fecha_disp 		= (Datos!=null)?ut.ConvertirToString(Datos.getFecha_disp()).substring(0,10):"";
String              Tiempo_vigencia = (Datos!=null)?String.valueOf(Datos.getTiempo_vigencia()):"";
String              Agencia        	= (Datos!=null)?Datos.getAgencia_disp():"";

String Mensaje       = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
String Listar       = (request.getParameter("Listar")!=null)?request.getParameter("Listar"):"N";
String filtro       = (request.getParameter("Filtro")!=null)?request.getParameter("Filtro"):"Todos";
String NombreBoton   = (Datos==null)?"Guardar":"Modificar";
String NombreBoton1  = (ListaGnral!=null && ListaGnral.size()>0)?"Ocultar Lista":"Listado";
String TNombreBoton   = (Datos==null)?"aceptar":"modificar";
String TNombreBoton1  = (ListaGnral!=null && ListaGnral.size()>0)?"btnOcultar":"detalles";
//String BloquearText  = (Datos==null)?"":"ReadOnly";
//String BloquearSelect= (Lista!=null && Lista.size()>0)?"":"Disabled";
//String BloquearBoton = (Datos!=null)?"":"Disabled";
%>

<html>
<head>
    <title>Modificar Vehiculos Externos</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <script src='<%=BASEURL%>/js/date-picker.js'></script>
    <script src='<%=BASEURL%>/js/validar.js'></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
	<script>
	function cambio(tipo){
		if ( tipo ==  'ocultar'){
			formulario.Opcion.value='Ocultar';
			info.style.display="none";
			tvex.width = "50%";
		} 
	}
	
	function SelAllVE(){
            for(i=0;i<FormularioListado.length;i++)
                    FormularioListado.elements[i].checked=FormularioListado.All.checked;
    }
    function ActAllVE(){
            FormularioListado.All.checked = true;
            for(i=1;i<FormularioListado.length;i++)	
                    if (!FormularioListado.elements[i].checked){
                            FormularioListado.All.checked = false;
                            break;
                    }
    }      
	</script>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>   
	    <link href="../css/estilostsp.css" rel='stylesheet'>   
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Vehiculos Externos"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
    <FORM name='formulario' method='POST' action="<%=CONTROLLER%>?estado=VExternos&accion=Manager" onsubmit='return validarVE(this);' >
	
	<table width="50%" border="2" align="center">
      <tr>
        <td>
          <table id="tvex" width="100%"  align="center"  class='fondotabla'>
            <tr>
              <td width="301" align="left" class="subtitulo1">&nbsp;Informacion de Vehiculo Externos</td>
              <td width="170" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
            </tr>
            <tr class='fila'>
              <td colspan='2'  height='37' >&nbsp Placa vehiculo :&nbsp&nbsp
			      <input id="vplaca" type="hidden" value="<%= Placa %>" name="vplaca"/>
                  <input name='placa' type='text' class="textbox" id="placa" width='120'>
              <a style="cursor:hand" class="Simulacion_Hiper" onclick="if(formulario.placa.value != ''){NuevaVentana('<%=CONTROLLER%>?estado=Placa&accion=Search&cmd=show&pag=hoja&idplaca='+formulario.placa.value,'PLACA',900,250,100,50);}else{alert('Debe digitar una placa');}" title='Informacion Placa'><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></a>              </td>
            <tr class="fila">
              <th valign="top" height="100" colspan='2'><br>
                  <table  border='0' width='100%' cellspacing='0' cellpadding='0'>
                    <tr align="left" class='fila'>
                      <td >&nbsp Fecha Disponibilidad :</td>
                      <td>
					  <input id="vfechadisp" type="hidden" value="<%= Fecha_disp %>" name="vfechadisp"/>
                        <input name='fechadisp' type='text' class="textbox" id="fechadisp" value='' readonly>
                        <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(fechadisp);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a><a style="cursor:hand" class="Simulacion_Hiper" onclick="if(formulario.placa.value != ''){NuevaVentana('<%=CONTROLLER%>?estado=Placa&accion=Search&cmd=show&pag=hoja&idplaca='+formulario.placa.value,'PLACA',900,250,100,50);}else{alert('Debe digitar una placa');}" title='Informacion Placa'><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></a> </td>
                    </tr>
                    <tr class='fila'>
                      <td>&nbsp Tiempo vigencia ( Horas ) :</td>
                      <td>
                        <input name='tiempovigencia' type='text' class='textbox' id="tiempovigencia" maxlength='5' onKeyPress="soloDigitos(event,'noDec')">
                        <a style="cursor:hand" class="Simulacion_Hiper" onclick="if(formulario.placa.value != ''){NuevaVentana('<%=CONTROLLER%>?estado=Placa&accion=Search&cmd=show&pag=hoja&idplaca='+formulario.placa.value,'PLACA',900,250,100,50);}else{alert('Debe digitar una placa');}" title='Informacion Placa'><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></a></td>
                    </tr>
                    <tr class='fila'>
                      <td>&nbsp Agencia : </td>
                      <td>
                        <select   name='agencia' class='textbox' id="agencia">
                          <%
                     if(ListaAgencias!=null) {
                         Iterator It = ListaAgencias.iterator();
                         while(It.hasNext()) {
                              Ciudad datos = (Ciudad) It.next();
                              out.print("<option value='"+datos.getCodCiu()+"'>["+datos.getCodCiu()+"] "+datos.getNomCiu()+"</option> \n");
                         }
                      }
                    %>
                        </select>
                      <a style="cursor:hand" class="Simulacion_Hiper" onclick="if(formulario.placa.value != ''){NuevaVentana('<%=CONTROLLER%>?estado=Placa&accion=Search&cmd=show&pag=hoja&idplaca='+formulario.placa.value,'PLACA',900,250,100,50);}else{alert('Debe digitar una placa');}" title='Informacion Placa'><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></a>                      </td>
                    </tr>
                  </table>
                  <br>
              </th>
            </tr>
            <tr class='fila'>
              <th height='20'  colspan='2'>&nbsp; </th>
            </tr>
        </table></td>
      </tr>
    </table>
    <br>
<table width="50%" align="center">
	<tr align="center">
		 <td><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgaceptar" onclick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/VehExternos&pagina=ListadoVE.jsp&marco=no'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" title="Haga click para regresar al listado">
   		 <INPUT type="image" src="<%=BASEURL%>/images/botones/<%=TNombreBoton%>.gif"  name="imgsalir2" onClick="Opcion.value='<%=NombreBoton%>';" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" title="Haga click para modificar"></td>		
	</tr>	
</table>
    <br>
    <input id="Opcion" type="hidden" value="" name="Opcion"/>
     
    </FORM>
    <script>
        formulario.placa.value = '<%= Placa %>';
        formulario.fechadisp.value = '<%= Fecha_disp %>';
        formulario.tiempovigencia.value = '<%= Tiempo_vigencia %>';
        formulario.agencia.value = '<%= Agencia %>';                  
    </script>
    <center class='comentario'>
   


 <%if(!Mensaje.equals("")){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
     <%}%>

</center>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>
