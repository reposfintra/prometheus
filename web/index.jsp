<%-- 
    Document   : index1
    Created on : 22/10/2014, 09:42:28 AM
    Author     : egonzalez
--%>
<%@ page session="true"%>
<%@ page errorPage="/error/ErrorPage.jsp"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<% String msj=request.getParameter("msg")!=null?request.getParameter("msg"):"";%>
<!DOCTYPE html>
<html  class="no-js"  >
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Fintra</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
        <link rel="shortcut icon" href="images/bg.jpg"> 

        <link rel="stylesheet" type="text/css" href="css/index/demo.css" />
        <link rel="stylesheet" type="text/css" href="css/index/style.css" />

        <link rel="stylesheet" type="text/css" href="css/index/animate-custom.css" />      
        <link href="css/jquery/jquery-ui/jquery-ui.css" type="text/css" rel="stylesheet" />

        <script type="text/javascript" src="js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>

        <script type='text/javascript' src="js/inicioAlternativo.js"></script>
    </head>
    <body>
        <div class="container">
            <section>		
                <br>
                <div id="container_demo" >
                    <a class="hiddenanchor" id="toregister"></a>
                    <a class="hiddenanchor" id="tologin"></a>

                    <div id="wrapper">
                        <div id="login" class="animate form">
                            <form id="formulario" name="formulario" action="./controller?estado=Usuario&accion=Validar&cmd=show&tipo=login" autocomplete="on" method="post"> 
                                <input type="hidden" name="dstrct" id="dstrct" value="" />
                                <input type="hidden" name="proy" id="proy" value="TRSION" />
                                <input type="hidden" name="estado" id="proy" value="Usuario" />
                                <input type="hidden" name="accion" id="proy" value="Validar" />
                                <input type="hidden" name="login_into" id="login_into" value="Fintra" />
                                <input type="hidden" name="lat_x" id="lat_x" value="" />
                                <input type="hidden" name="lon_y" id="lon_y" value="" />
                                <h1 id="titulo">Fintra S.A</h1> 
                                <p> 
                                    <label for="usuario" class="uname" data-icon="u" >Login</label>
                                    <input id="usuario" name="usuario" required="required" type="text" placeholder="User name" onblur="buscarCia(this.id)"/>
                                </p>
                                <p> 
                                    <label for="clave" class="youpasswd" data-icon="p">Password </label>
                                    <input id="clave" name="clave" required="required" type="password" placeholder="eg. X8df!90EO" /> 
                                </p>
                                <p> 
                                    <label for="cia" class="uname" >Empresa </label><br/>
                                    <select name="cia" id="cia" required="required"  onblur="buscarPerfil('usuario', this.id,'dstrct','proy')"
                                            onchange="document.getElementById('titulo').innerHTML = $('#cia option:selected').text();"></select> 
                                </p>
                                <p> 
                                    <label for="perfil" class="uname" >Perfil </label><br/>
                                    <select name="perfil" id="perfil" required="required" ></select> 
                                </p>
                                <p class="login button"> 
                                    <input id="entrar" name="entrar" type="button" value="Entrar" /> 
                                </p>
                                <p id="msj" style="color:red; font-size: 15px"><%=msj%></p>
                                <p class="change_link">
                                    Cambiar Contraseña <a href="#" id="cambiar" class="to_register"><img src="images/forward.png" ></a>
                                </p>

                            </form>
                        </div>			
                    </div>

                    <div id="dialogo2" class="ventana">

                        <div id="wrapper">
                            <div id="login" class="animate form">
                                <form id="formulario1" name="formulario1" action="./controller?estado=Usuario&accion=Validar&cmd=show&tipo=renovarClave" autocomplete="on" method="post"> 
                                    <input type="hidden" name="dstrct1" id="dstrct1" value="" />
                                    <input type="hidden" name="proy1" id="proy1" value="TRSION" />
                                    <input type="hidden" name="estado" id="proy" value="Usuario" />
                                    <input type="hidden" name="accion" id="proy" value="Validar" />
                                    <input type="hidden" name="login_into" id="login_into" value="Fintra" />    
                                    <input name='cambiandoClave' value='true' type='hidden'>

                                    <h1 id="titulo1">Fintra S.A</h1> 
                                    <p> 
                                        <label for="usuario1" class="uname" data-icon="u" >Login</label>
                                        <input id="usuario1" name="usuario1" required="required" type="text" placeholder="User name" onblur="buscarCia(this.id)" />
                                    </p>
                                    <p> 
                                        <label for="clave1" class="youpasswd" data-icon="p">Password </label>
                                        <input id="clave1" name="clave1" required="required" type="password" placeholder="eg. X8df!90EO" /> 
                                    </p>
                                    <p> 
                                        <label for="nclave" class="youpasswd" data-icon="p">Nuevo-Password </label>
                                        <input id="nclave" name="nclave" required="required" type="password" placeholder="eg. X8df!90EO" /> 
                                    </p>
                                    <p> 
                                        <label for="cnclave" class="youpasswd" data-icon="p">Confirme-Password </label>
                                        <input id="cnclave" name="cnclave" required="required" type="password" placeholder="eg. X8df!90EO" onkeyup="validarpass()"/> 
                                        <span id="msj1" style="color:red; font-size: 10px;text-align: left"></span>
                                    </p>
                                    <p> 
                                        <label for="cia1" class="uname" >Empresa </label><br/>
                                        <select name="cia1" id="cia1" required="required" onblur="buscarPerfil('usuario1', this.id,'dstrct1','proy1')"
                                            onchange="document.getElementById('titulo1').innerHTML = $('#cia1 option:selected').text();"></select> 
                                    </p>
                                    <p> 
                                        <label for="perfil1" class="uname" >Perfil </label><br/>
                                        <select name="perfil1" id="perfil1" required="required" ></select> 
                                    </p>
                                    <p class="login button"> 
                                        <input id="entrar1" name="entrar1" type="submit" value="Entrar"  /> 
                                    </p>
                                    <p id="msj" style="color:red; font-size: 15px"><%=msj%></p>
                                    <p class="change_link">

                                        Regresar Login <a href="#" id="atras" class="to_register">login</a>
                                    </p>

                                </form>
                            </div>			
                        </div>                       

                    </div>
                  

                    <!--div style="text-align: center">
                            <p id="msj" style="color:red; font-size: 15px"><%=msj%></p>
                   </div-->
                
                </div>  
                <footer   style="position: absolute; bottom: 0">
                    <p>Copyright 2014 © Fintra S.A. Todos los derechos reservados.</p>
                    <p>COLOMBIA. Oficina Principal / (57) (5) 367990</p>
                    <p>Carrera 53 No. 79-01 Of 205 - BARRANQUILLA</p>
                    <img src="images/fondo-footer.png" style=" width: 100%">
                </footer> 
            </section>


        </div>   



    </body>
</html>
