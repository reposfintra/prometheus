<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE>Funcionamiento de la ventana de Consulta de Recursos Disponibles</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
</HEAD>
<BODY>  

  <table width="95%"  border="2" align="center">
    <tr>
      <td>
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">CONSULTA DE RECURSOS DISPONIBLES </div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento de la p&aacute;gina para consulta de Recursos Disponibles.</td>
          </tr>
          <tr>
            <td  height="18"  class="ayudaHtmlTexto"><p>&nbsp;</p>
            <p>Esta ventana permite ingresar y/o seleccionar los filtros que se aplicar&aacute;n para la 
               b&uacute;squeda de los Recursos Disponibles.<br>
               Debe aplicarse por lo menos un filtro para la b&uacute;squeda, o una combinaci&oacute;n de todos.</p>
            
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/pags_predo/recursos_disp/img1.JPG"  ></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>Si la b&uacute;squeda gener&oacute; resultados, se listarán los recursos disponibles.
                El usuario tendr&aacute; la opci&oacute;n de Retornar los recursos que tengan Fecha de Asignaci&oacute;n vac&iacute;a
                haciendo click sobre el mismo. <br>
                Se abre la siguiente ventana:</p></td>
          </tr>
          
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/pags_predo/recursos_disp/img2.JPG" ></div></td>
          </tr>   
          
          <tr>
            <td  class="ayudaHtmlTexto"><p>En la cual deber&aacute; ingresar la Fecha y hora de disponibildad, as&iacute; como
                Ciudad y Tiempo de Vigencia.<br>
                Se mostrar&aacute; un mensaje de confirmaci&oacute;n o de error dependiendo del  
                resultado de la operación.</p></td>
          </tr>
          
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
                    
      </table></td>
    </tr>
  </table>
  <table width="416" align="center">
	<tr>
	<td align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	</td>
	</tr>
</table>
</BODY>
</HTML>
