<!--
- Autor : Osvaldo P�rez Ferrer
- Date : 14 de Julio de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, para asignar recurso disponible
--%>

<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.operation.model.*, com.tsp.operation.model.beans.*, com.tsp.util.*,java.util.*, java.text.*"%>

<html>
<head>
    <title>Retornar Recurso</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

    <script language="javascript" src="<%=BASEURL%>/js/validar.js">
    <script src="<%=BASEURL%>/js/utilidades.js"></script>
    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script src='<%= BASEURL %>/js/date-picker.js'></script>

<%   
    String placa = (String)request.getAttribute("placa");
    String fecharec = (String)request.getAttribute("fecha");
    String mensaje = (String)request.getAttribute("mensaje");
    String modified = (String)request.getAttribute("modified");
    String hoy = Utility.getHoy("-");
    if(fecharec == null){ fecharec = (String)request.getAttribute("fecharec");}
%>
</head>

<body>
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=Retornar Recurso"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">        
    <form id="form1" name="form1" method="post" action="<%=CONTROLLER%>?estado=Recursos&accion=Disponibles&opcion=asign">
    <table width="360"  border="2" align="center">
        <tr>
            <td><table width="100%"  border="0" align="center" class="tablaInferior" onclick="document.getElementById('mensaje').style.visibility = 'hidden'">
            <tr>
            <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
            <td height="22" width="50%" class="subtitulo1"><div align="left" class="subtitulo1">
                <strong>Retornar Recurso</strong>                     
            </div></td>
            <td width="212" class="barratitulo">
            <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left">
                    
        </tr>
            </table>
            <table width="99%" border="0" align="center" bordercolor="#999999" bgcolor="#F7F5F4">    
            
                <tr  class="fila" >
                <td height="20" nowrap ><strong>Placa: </strong></td>
                <td ><strong>&nbsp;<%=placa%></strong> </td>                
                </tr>
                
                <input type="hidden" name="placa" value="<%=placa%>">
                <input type="hidden" name="fecharec" value="<%=fecharec%>">
                               
               <tr  class="fila">
                <td nowrap ><strong>Fecha Disponibilidad: </strong></td>
                <td nowrap ><input name="fechadisp" type="text" class="textbox" id="fechadisp" size="17" readonly value="<%=hoy%> 00:00">
                    <a id="cal1" href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fechadisp);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="Click aqui para escoger la fecha"></a>
                </td>
                </tr>
                
                <tr  class="fila">
                <td nowrap ><strong>Ciudad Disponibilidad: </strong></td>
                <td nowrap ><input name="ciudad" type="text" class="textbox" id="ciudad" size="10" maxlength="2">
                <span class="Simulacion_Hiper"><a href="javascript:openVentana('<%=BASEURL%>'); rename('origen');">Buscar Ciudad </a></span>
                </td>
                </tr>
                
                <tr  class="fila">
                <td nowrap ><strong>Tiempo Vigencia: </strong></td>
                <td nowrap ><input name="tiempo" type="text" class="textbox" id="tiempo" size="10" maxlength="3" onKeyPress="soloDigitos(event,'decNo')"> </td>
                </tr>
                                
                                
            </table>  
                      
        </table>  
        </td>
        </tr>
    </table></td>
    </tr>
    </table>
    
    <div id="mensaje">
    <%if(mensaje != null){ %>
        
        <table border="2" align="center">
            <tr>                
                <td height="45"><table width="410" height="41" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                        <td width="282" height="35" align="center" class="mensajes"><%=mensaje%></td>
                        <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="78">&nbsp;</td>
                    </tr>
                </table></td>
            </tr>
        </table>        
               
       <%}%>
       </div>
       
    <br>
    <div align="center">                     
        <img src="<%=BASEURL%>/images/botones/aceptar.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="if(validar()){form1.submit();}" onMouseOut="botonOut(this);" style="cursor:hand">            
        <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">            

    </form>
    <br/>
	

    </div>
    
    <% if( modified != null){%>
        
        <script>
            parent.opener.location.reload();
            document.getElementById("imgsalir").src = "<%=BASEURL%>/images/botones/aceptarDisable.gif";
            document.getElementById("imgsalir").onmouseover = "";
            document.getElementById("imgsalir").onmouseout = "";
            document.getElementById("imgsalir").onclick = "";
        </script>
    <%}%>
     
</div>   
 
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins_24.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    

</body>
</html>

<script>
    function openVentana(BASEURL) {    
        window.open(BASEURL+"/consultas/consultaCiudad.jsp",'','top=20,left=200,width=700,heigth=200,scrollbars=yes,status=yes');    
    }        
    
    function validar(){
        if( form1.ciudad.value == ""){
            alert("Debe ingresar una CIUDAD");
            return false;
        }
        if( form1.tiempo.value == ""){
            alert("Debe ingresar el TIEMPO disponibilidad");
            return false;
        }
        return true;
    }
</script>
