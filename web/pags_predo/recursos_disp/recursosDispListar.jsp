<!--
- Autor : Osvaldo P�rez Ferrer
- Date : 14 de Julio de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve para listar los recursos disponibles
--%>

<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.operation.model.*, com.tsp.operation.model.beans.*, com.tsp.util.*,java.util.*, java.text.*"%>

<html>
<head>
    <title>Recursos</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

    <script src="<%=BASEURL%>/js/utilidades.js"></script>
    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<%
    Vector recursos = (Vector) request.getAttribute("recursos");
    String mensaje = (String) request.getAttribute("mensaje");
    boolean asig = false; 
%>
</head>

<body>
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=B�squeda Recursos Disponibles"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">        
    <form id="form1" method="post">
    
    <% if(recursos != null){%>
    <table width="50%"  border="2" align="center">        
        <tr>                    
            <td><table width="100%"  border="1" align="center" class="tablaInferior">
            <tr>
            <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
            <td height="22" colspan=2 class="subtitulo1"><div align="left" class="subtitulo1">
                <strong>&nbsp;Recursos Disponibles</strong>                     
            </div></td>
            <td width="400" class="barratitulo">
            <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left">
                    
        </tr>
            </table>
            
            
            <table width="950" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">    
                <tr class="tblTitulo" align="center">
                    <td >Placa</td>
                    <td >Planilla</td>                    
                    <td >Recurso</td>
                    <td >Fecha Disponibilidad</td>
                    <td >Origen</td>
                    <td >Destino</td>
                    <td >Fecha Asignaci�n</td>
                    <td >Std Job</td>
                    <td >WorkGroup</td>
                    <td ></td>
                </tr>
                    <%                                    
                    for(int i=0; i<recursos.size(); i++){
                        Recursosdisp r = (Recursosdisp) recursos.get(i);
                        asig = r.getFecha_asig().toString().equals("0099-01-01 00:00:00.0");
                        %>
                <tr class="<%=i%2==0?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' 
                    style="cursor:hand" 
                    <%if(asig){%>
                        onclick="window.open('<%=CONTROLLER%>?estado=Recursos&accion=Disponibles&opcion=update&placa=<%=r.getPlaca()%>&fecha=<%=r.getFecha_disp()%>','myWindow','width=650,height=400,resizable=yes');"
                    <%}%>      
                    title="Click para Modificar" align="center">    
                    
                <td width="5%" class="bordereporte"><%=r.getPlaca()%>&nbsp;</td>    
                <td width="5%" class="bordereporte"><%=r.getNumpla()%>&nbsp;</td>                                
                <td width="18%" class="bordereporte"><%=r.getRecurso()%>&nbsp;</td>
                <td width="17%" class="bordereporte"><%=r.getFecha_disp()%>&nbsp;</td>
                <td width="10%" class="bordereporte"><%=r.getOrigen()%>&nbsp;</td>
                <td width="10%" class="bordereporte"><%=r.getDestino()%>&nbsp;</td>
                <td width="17%" class="bordereporte"><%=(asig)? "" :r.getFecha_asig().toString()%>&nbsp;</td>
                <td width="6%" class="bordereporte"><%=r.getStd_job_no_req()%>&nbsp;</td>
                <td width="12%" class="bordereporte"><%=r.getWork_gorup()%>&nbsp;</td>
                
                </tr>                                          
            </td>
            </tr>            
                <%}%>
                      </table>                        
            </table>               </td>
            </tr>
        </table>
        
        <%}%>
        
    
     <%if(mensaje != null){ %>
        
        <table border="2" align="center">
            <tr>                
                <td height="45"><table width="410" height="41" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                        <td width="282" height="35" align="center" class="mensajes"><%=mensaje%></td>
                        <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="78">&nbsp;</td>
                    </tr>
                </table></td>
            </tr>
        </table>        
        
       <%}%>   
    <br>
    <div align="center">                     
        <img src="<%=BASEURL%>/images/botones/regresar.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="location.replace('<%=CONTROLLER%>?estado=Recursos&accion=Disponibles&opcion=filter');" onMouseOut="botonOut(this);" style="cursor:hand">            
        <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">            
    </div>
    </form>
    
	
   
</body>
</html>
    
