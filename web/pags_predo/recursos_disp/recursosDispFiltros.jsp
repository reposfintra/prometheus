<!--
- Autor : Osvaldo P�rez Ferrer
- Date : 14 de Julio de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve para ingresar el filtro 
                para la b�squeda de recursos disponibles
--%>

<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.operation.model.*, com.tsp.operation.model.beans.*, com.tsp.util.*,java.util.*, java.text.*"%>

<html>

<head>    
    <title>Recursos</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

    <script src="<%=BASEURL%>/js/utilidades.js"></script>
    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<%    
    String campo = (String)request.getAttribute("campo");
    Vector recursos = (Vector) request.getAttribute("recursos");
    List wg = (List) request.getAttribute("wg");
    
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    
%>
</head>

<body>
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=B�squeda Recursos Disponibles"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">        
    <form id="form1" name="form1" method="post" action="<%=CONTROLLER%>?estado=Recursos&accion=Disponibles&opcion=search">
    <table width="360"  border="2" align="center">
        <tr>
            <td><table width="100%"  border="0" align="center" class="tablaInferior">
            <tr>
            <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
            <td height="22" width="50%" class="subtitulo1"><div align="left" class="subtitulo1">
                <strong>&nbsp;Recursos Disponibles</strong>                     
            </div></td>
            <td width="212" class="barratitulo">
            <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left">
             <%=datos[0]%>       
        </tr>
            </table>
            <table width="99%" border="0" align="center" bordercolor="#999999" bgcolor="#F7F5F4">    
            
                <tr  class="fila">
                <td nowrap ><strong>Placa: </strong></td>
                <td nowrap ><input name="placa" type="text" class="textbox" id="placa" size="14" maxlength="12"> </td>
                </tr>
                
                <tr  class="fila">
                <td nowrap ><strong>Recurso: </strong></td>
                <td >
                <select name="recurso"  id="recurso" size="6"> 
                <option value="" selected></option>
                <% for (int i =0; i<recursos.size(); i++){
                    Recursosdisp r = (Recursosdisp)recursos.get(i);%>
                    <option value="<%=r.getRecurso()%>"><%=r.getClase()%></option>
                <%}%>    
                </select>
                </td>
                </tr>
                                
                
                <tr  class="fila">
                <td nowrap ><strong>Origen: </strong></td>
                <td ><input name="origen" type="text" class="textbox" id="origen" size="6" maxlength="2"> 
                <span class="Simulacion_Hiper"><a href="javascript:openVentana('<%=BASEURL%>'); rename('origen');">Buscar Ciudad </a></span></td>
                </tr>
                
                <tr  class="fila">
                <td nowrap ><strong>Destino: </strong></td>
                <td nowrap ><input name="destino" type="text" class="textbox" id="destino" size="6" maxlength="2"> 
                <span class="Simulacion_Hiper"><a href="javascript:openVentana('<%=BASEURL%>'); rename('destino');">Buscar Ciudad </a></span></td>
                </tr>
                
                <tr  class="fila">
                <td nowrap ><strong>WorkGroup: </strong></td>
                <td >
                <select name="work_group" class="textbox" id="work_group"  size="6" maxlength="6">
                    <option value="" selected></option>
                    <% for (int i =0; i<wg.size(); i++){
                        TablaGen t = new TablaGen ();
                        t = (TablaGen) wg.get(i);%>                        
                    <option value="<%=t.getTable_code()%>"><%=t.getDescripcion()%></option>
                    <%}%>
                </select>
                </td>
                </tr>
                <input type="hidden" name ="campo" id ="campo" value="value">              
                                
            </table>  
                      
        </table>  
        </td>
        </tr>
    </table></td>
    </tr>
    </table>
    <br>
    <div align="center">                     
        <img src="<%=BASEURL%>/images/botones/buscar.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="if(validar()){form1.submit();}" onMouseOut="botonOut(this);" style="cursor:hand">            
        <img src="<%=BASEURL%>/images/botones/cancelar.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="form1.reset();" onMouseOut="botonOut(this);" style="cursor:hand">
        <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">                    
    </div>
    </form>
    <br/>
    </div> 
    <%=datos[1]%>
    
</body>
</html>

<script>
    function openVentana(BASEURL) {    
        window.open(BASEURL+"/consultas/consultaCiudad.jsp",'','top=20,left=200,width=700,heigth=200,scrollbars=yes,status=yes');    
    }
    
    function rename(campo){
        var ciu = document.getElementById("ciudad");
                
        if( ciu == null ){                
                if(campo == "destino"){
                document.getElementById(campo).id="ciudad";        
                }
                if(campo == "origen"){
                    document.getElementById(campo).id="ciudad";        
                }
            
        }else{
            if(campo == "destino"){
                document.getElementById("ciudad").id = "origen";
            }else{
                document.getElementById("ciudad").id = "destino";
            }
            rename(campo);
        }
    }
    
    function validar(){        
        if( form1.placa.value == "" && form1.recurso.value == "" && form1.origen.value == "" && form1.destino.value == "" && form1.work_group.value == ""){
            alert("Debe ingresar un dato para buscar");
            return false;
        }      
        return true;          
    }
    
</script>
