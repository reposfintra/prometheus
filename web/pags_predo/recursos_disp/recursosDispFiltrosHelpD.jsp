<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Descripci&oacute;n de campos Consulta de Recursos Disponibles</title>

<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<% String BASEIMG = BASEURL +"/images/botones/"; %> 
<br>
<table width="594"  border="2" align="center">
  <tr>
    <td width="635" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">CONSULTAR RECURSOS DISPONIBLES </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td width="123" class="fila">Placa:</td>
          <td width="551"  class="ayudaHtmlTexto"> Campo para digitar la placa del equipo. </td>
        </tr>
        <tr>
          <td  class="fila"> Recurso:</td>
          <td  class="ayudaHtmlTexto"> Permite buscar recursos disponibles con este recurso. </td>
        </tr>
        <tr>
          <td width="123"  class="fila"> Origen:</td>
          <td width="551"  class="ayudaHtmlTexto">Buscar recursos disponibles con esta ciudad de origen. </td>
        </tr>
        <tr>
          <td width="123"  class="fila"> Destino:</td>
          <td width="551"  class="ayudaHtmlTexto">Buscar recursos disponibles con esta ciudad de destino. </td>
        </tr>
        <tr>
          <td width="123"  class="fila"> WorkGroup:</td>
          <td width="551"  class="ayudaHtmlTexto">Buscar recursos disponibles con este Workgroup. </td>
        </tr>
	<tr>
          <td width="123"  class="fila"> Bot&oacute;n Buscar </td>
          <td width="551"  class="ayudaHtmlTexto">Realiza la b�squeda. </td>
        </tr>
        <tr>
          <td width="123"  class="fila"> Bot&oacute;n Cancelar </td>
          <td width="551"  class="ayudaHtmlTexto">Limpia el formulario. </td>
        </tr>
		<tr>
		  <td  class="fila">Bot&oacute;n Salir </td>
	      <td  class="ayudaHtmlTexto">Cierra la ventana. </td>
		</tr>
      </table>
    </td>
  </tr>
</table>
<br>
<table width="416" align="center">
	<tr>
	<td align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	</td>
	</tr>
</table>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
