<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<title>Detalles por Agencia</title>
<html> 
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <script src="<%=BASEURL%>/js/validar.js"></script>
	<script src='<%=BASEURL%>/js/date-picker.js'></script>
    <link  href='<%=BASEURL%>/css/estilostsp.css' rel='stylesheet'>
	<link  href='../css/estilostsp.css' rel='stylesheet'>
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>

</head>

<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Detalles por Agencia"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<%
String accion = CONTROLLER+"?estado=InformePredox&accion=Agencia";
String nom = request.getParameter("nom");
ReportePlaneacion rp;

	Vector age = model.ipredoSvc.getAgencias();
	System.out.println( "Cantidad " +age.size() );%>

    <table width="98%" border="2" align="center">
      <tr>
        <td height="119"><table width="100%"  border="0">
          <tr>
            <td width="48%" class="subtitulo1">&nbsp;<%=nom%></td>
            <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
        </table>
          <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
          <tr class="tblTitulo" align="center">
            <th width="179">Cliente</th>
            <th width="96">Planilla</th>
            <th width="50">Placa</th>
            <th width="85">Origen</th>
            <th width="86">Destino </th>
            <th width="15">Tipo Recurso </th>
            <th width="70">Recurso</th>
            <th width="83">Fecha Asignaci&oacute;n</th>
            <th width="97">Fecha Disponibilidad</th>
            <th width="77">Equipo Asociado</th>
          </tr>
          <%for (int i = 0;  i < age.size(); i++){
          rp = (ReportePlaneacion) age.elementAt(i);
      %>
          <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
            <td valign='center' class="bordereporte" ><%if(rp.getNomClienreq()!=null){%>
                <%=rp.getNomClienreq()%>
                <%}else{%>
      No tiene informaci&oacute;n
      <%}%></td>
            <td class="bordereporte"><%if(!rp.getNumpla().equals("")){%>
                <%=rp.getNumpla()%>
                <%}else{%>
&nbsp;
      <%}%></td>
            <td class="bordereporte" ><%if(!rp.getPlaca().equals("")){%>
                <%=rp.getPlaca()%>
                <%}else{%>
&nbsp;
      <%}%></td>
            <td class="bordereporte"><%if(rp.getNomorirec()!=null){%>
                <%=rp.getNomorirec()%>
                <%}else{%>
&nbsp;
      <%}%></td>
            <td class="bordereporte"><%if(rp.getNomdesrec()!=null){%>
                <%=rp.getNomdesrec()%>
                <%}else{%>
          &nbsp;
                <%}%></td>
            <td class="bordereporte"><%if(!rp.getTipo_rec_rec().equals(" ")){%>
                <%=rp.getTipo_rec_rec()%>
                <%}else{%>
&nbsp;
      <%}%></td>
            <td width="70" class="bordereporte"><%if(!rp.getRecurso_rec().trim().equals("")){%>
                <%=rp.getRecurso_rec()%>
                <%}else{%>
&nbsp;
      <%}%></td>
            <td width="83" class="bordereporte"><%if(!rp.getFecasigrec().equals("01-01-0099 00:00:00")){%>
                <%=rp.getFecasigrec()%>
                <%}else{%>
&nbsp;
      <%}%></td>
            <td width="97" class="bordereporte"><%if(!rp.getFecdisrec().equals("01-01-0099 00:00:00")){%>
                <%=rp.getFecdisrec()%>
                <%}else{%>
&nbsp;
      <%}%></td>
            <td width="77" class="bordereporte"><%if(!rp.getEqasorec().equals("")){%>
                <%=rp.getEqasorec()%>
                <%}else{%>
&nbsp;
      <%}%></td>
          </tr>
          <%}%>
        </table></td>
      </tr>
    </table>
<br>
    <table width="98%" align="center">
      <tr>
        <td><img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></td>
      </tr>
    </table>
    <p>&nbsp;</p>
</div>
</body>
</html>
