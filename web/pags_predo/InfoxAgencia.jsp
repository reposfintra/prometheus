<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*,java.text.DecimalFormat"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html> 
<head>
    <title>Agencia</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <script src="<%=BASEURL%>/js/validar.js"></script>
	<script src='<%=BASEURL%>/js/date-picker.js'></script>
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
	<link  href='<%=BASEURL%>/css/estilostsp.css' rel='stylesheet'>
	<link  href='../css/estilostsp.css' rel='stylesheet'>
</head>
 
<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Informe Indicadores por Agencia"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
String accion = CONTROLLER+"?estado=InformePredox&accion=Agencia";
String f1 = request.getParameter("fechai");
String f2 = request.getParameter("fechaf");

System.out.println( "FECHA 1 "+f1+" FECHA 2 "+f2 );
int veh=0,vehUtil=0,rem=0;
double flota=0,retorno=0;
ReportePlaneacion rp;
%>
    <form name="form2" method="post" action="<%=accion%>" onSubmit="return validarFecha(this);">
      <table width="500" border="2" align="center">
        <tr>
          <td><table width="100%"  border="0">
            <tr>
              <td width="48%" class="subtitulo1">Generar Indicadores de Gestion </td>
              <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
            </tr>
          </table>
            <table width="100%" class="tablaInferior">
            
            <tr class="fila">
              <td width="177" nowrap>Fecha Inicio </td>
              <td width="287" nowrap><input name='fechai' type='text' class="textbox" id="fechai" style='width:120' value='' readonly>                <img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.form2.fechai);return false;" HIDEFOCUS></td>
            </tr>
            <tr class="fila">
              <td nowrap>Fecha Fin </td>
              <td nowrap>
                <input name='fechaf' type='text' class="textbox" id="fechaf" style='width:120' value='' readonly>                <img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.form2.fechaf);return false;" HIDEFOCUS></td>
            </tr>
          </table></td>
        </tr>
      </table>
      <div align="center"><br>
      <input type="image" name="Submit" src="<%=BASEURL%>/images/botones/aceptar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
&nbsp; <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"> </div>
    </form>

    <%if(f1!=null && f2!=null){
	Vector age = model.ipredoSvc.ListarAgencias(f1, f2);
	System.out.println( "Cantidad " +age.size() );%>
    <table width="820" border="2" align="center">
      <tr>
        <td><table width="100%"  border="0">
          <tr>
            <td width="48%" class="subtitulo1">INDICADORES DE GESTI&Oacute;N</td>
            <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
        </table>
          <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
          <tr class="tblTitulo">
            <th width="212"> Agencia </th>
            <th width="98"> Veh&iacute;culos</th>
            <th width="114">Veh&iacute;culos Utilizados </th>
            <th width="89"> Remesas Despachadas</th>
            <th width="116"> % Utilizaci&oacute;n Flota </th>
            <th width="124">% Utilizaci&oacute;n Retorno </th>
          </tr>
          <%for (int i = 0;  i < age.size(); i++){
          rp = (ReportePlaneacion) age.elementAt(i);
		  veh+=rp.getVehiculos();
		  vehUtil+=rp.getVehiculoutil();
		  rem+=rp.getRemDespachada();
		  flota+=rp.getUtilflota()/((double)age.size());
		  retorno+=rp.getRetorno()/((double)age.size());
		  
      %>
          <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
            <td class="bordereporte" valign='center' style="cursor:hand" 
             onClick="window.open('<%=CONTROLLER%>?estado=InformePredo&accion=Buscar&pagina=ListadoxAgencia.jsp&carpeta=pags_predo&age=<%=rp.getAgasoc()%>&fec1=<%=f1%>&fec2=<%=f2%>&nom=<%=rp.getNomAgasoc()%>' ,'','status=yes,scrollbars=no,width=800 ,height=600,resizable=yes');"><%=rp.getNomAgasoc()%></td>
            <td class="bordereporte" ><%=rp.getVehiculos()%></td>
            <td class="bordereporte" ><%=rp.getVehiculoutil()%></td>
            <td class="bordereporte" ><%=rp.getRemDespachada()%></td>
            <td class="bordereporte" ><%=rp.getUtilflota()%></td>
            <td class="bordereporte" ><%=rp.getRetorno()%></td>
          </tr>
          <%}%>
          <tr class="fila">
            <td valign='center' class="bordereporte" >Total</td>
            <td class="bordereporte"  ><%=veh%></td>
            <td class="bordereporte" ><%=vehUtil%></td>
            <td class="bordereporte" ><%=rem%></td>
            <%DecimalFormat df = new DecimalFormat("0.#");
		   %>
            <td class="bordereporte" ><%=df.format(flota)%></td>
            <td class="bordereporte" ><%=df.format(retorno)%></td>
          </tr>
        </table></td>
      </tr>
    </table>
    <%}%>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    

</body>
</html>
