<%@ page session="true"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
    <title>Requerimientos Cliente</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<script src='<%=BASEURL%>/js/date-picker.js'></script>
    <link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
	<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
	<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validarDocumentos.js"></script>

</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Requerimientos Cliente"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<form name="forma" method="post" action="<%=CONTROLLER%>?estado=ProcesoActualizarPlanilla&accion=Frontera" >
<table width="530" border="2" align="center">
  <tr>
    <td><table width="100%"  border="0">
      <tr>
        <td width="48%" class="subtitulo1">&nbsp; Actualizar Planillas Frontera </td>
        <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
      </tr>
    </table>
    </td>
  </tr>
</table>  
<br>
<table width="100"  border="0" align="center">
  <tr>
    <td><img src="<%=BASEURL%>/images/botones/aceptar.gif" name="aceptar"  height="21" onClick="forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;<img src="<%=BASEURL%>/images/botones/salir.gif" name="aceptar"  height="21" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
  </tr>
</table>
<br>
<%if(request.getParameter("msg") != null){%>
<table border="2" align="center">
  <tr>
    <td><table width="100%" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="262" align="center" class="mensajes"><%=request.getParameter("msg")%> !</td>
        <td width="32" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="44">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table><%}%>

</form>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
</body>
</html>
