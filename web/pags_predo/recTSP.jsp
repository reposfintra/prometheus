<%@ page session="true"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
    <title>Recursos TSP</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<script src='<%=BASEURL%>/js/date-picker.js'></script>
    <link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
	<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
	<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validarDocumentos.js"></script>

</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Recursos TSP"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<%String tipo = "nuevo";
String titulo = "Nuevos";
String ons = "return validarFechas();";
if (request.getParameter("tipo").equals("act")){
	tipo = "act";
	ons = "";
	titulo = "Actualizar";
}%>
<form name="form2" method="post" action="<%=CONTROLLER%>?estado=RecTSP&accion=Proceso&tipo=<%=tipo%>" >
<table width="550" border="2" align="center">
  <tr>
    <td><table width="100%"  border="0">
      <tr>
        <td width="48%" class="subtitulo1">&nbsp;<%=titulo%> Recursos </td>
        <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
      </tr>
    </table>
      <table width="100%" class="tablaInferior" >	
      <%if (tipo.equals("nuevo")){%>
      <tr class="fila">
        <td width="264" nowrap>Fecha Inicial Despacho</td>
        <td width="370" nowrap><input name='fechai' type='text' class="textbox" id="fechai" style='width:120' value='' readonly>          <img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.form2.fechai);return false;" HIDEFOCUS></td>
      </tr>
      <%}else{%>
	  <tr class="fila">
        <td width="100%" align="center">Haga clic en el boton aceptar para iniciar el proceso</td>        
      </tr>
	  <%}%>
        </table></td>
  </tr>
</table>  
<br>
<table align="center">
<tr>
        <td colspan="2" nowrap align="center">
          <%if (tipo.equals("nuevo")){%>
		  <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="aceptar"  height="21" onClick="llenarFecha();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
          <%}
	  else {%>
	       <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod"  height="21"  onclick="form2.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
          <%}%>
		  <img src="<%=BASEURL%>/images/botones/salir.gif" name="mod"  height="21" onclick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
        </td>
      </tr>
</table>
<br>
<%if(!request.getParameter("msg").equals("vacio")){%>
<table border="2" align="center">
  <tr>
    <td><table width="100%" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="262" align="center" class="mensajes"><%=request.getParameter("msg")%> !</td>
        <td width="32" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="44">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%>
</form>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
</body>
</html>