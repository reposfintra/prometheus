<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Ingresar codigos por demora</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="<%=BASEURL%>/js/validar.js"></script>
<script src="<%=BASEURL%>/js/boton.js"></script>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">

</head>
<body onLoad="mostrarMsg('<%=request.getParameter("mensaje")%>');">
<form name="form2" method="post" action="<%=CONTROLLER%>?estado=Compania&accion=Update">
  <table width="630" border="2" align="center">
    <tr>
      <td>
        <table width="99%" align="center" cellpadding="0">
          <tr>
            <td width="392" height="20"  class="subtitulo1"><p align="left"><span class="Estilo2"><strong>BUSCAR COMPA&Ntilde;IA </strong></span></p></td>
            <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
        </table>
        <table width="99%" align="center" cellpadding="0" cellspacing="0" class="Letras">
          <tr bgcolor="#99CCFF" class="fila">
            <td colspan="2" nowrap><strong><U> Escriba el distrito que desea buscar para anular:</U></strong></td>
          </tr>
          <tr class="fila">
            <td width="87" nowrap><strong>Distrito</strong></td>
            <td width="519" nowrap><input name="dstrct" type="text" class="textbox" id="dstrct3" size="20" maxlength="4">
            <img src="<%=BASEURL%>/images/botones/buscar.gif"  name="buscar" width="87" height="21" align="absbottom" style="cursor:hand" onClick="form2.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">            </td>
          </tr>
      </table></td>
    </tr>
  </table>
</form>
<br>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Compania&accion=Update" onSubmit="">
  <%
        String codigo_empresa = "";
        String codigo_reg     = "";
        String project        = "";
        String descripcion    = "";
        String fecha          = "";
        String nit            = "";
        String aseguradora    = "";
        String poliza         = "";
        String resolucion     = "";
        float peso_lleno     = 0;
        float anticipo       = 0;
        String dstrct         = "";
		String hora 		="";
  
  		if(model.ciaService.getCompania()!=null){
			Compania cia = model.ciaService.getCompania();
  			codigo_empresa = cia.getcodigo_empresa();
        	codigo_reg     = cia.getcodigo_regional();
        	project        = cia.getdefault_project();
        	descripcion    = cia.getdescription();
        	fecha          = cia.getfecha_ven_poliza();
        	nit            = cia.getnit();
        	aseguradora    = cia.getnombre_aseguradora();
        	poliza         = cia.getpoliza_aseguradora();
        	resolucion     = cia.getresolucion() ;
        	peso_lleno     = cia.getpeso_lleno_max();
        	anticipo       = cia.getanticipo_max();
        	dstrct         = cia.getdstrct();
			hora 		   = cia.gethora();
  		}
  	
  %>
   <table width="630" border="2" align="center">
  <tr>
    <td>
<table width="99%" align="center" cellpadding="0">
  <tr>
    <td width="50%"  class="subtitulo1"><p align="left">Datos de la empresa </p></td>
    <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
  <table width="99%" align="center" cellpadding="0" cellspacing="0" class="Letras">
    <tr class="fila">
      <td width="146" nowrap><strong>Distrito</strong></td>
      <td nowrap><input name="dstrct" type="text" class="textbox"  readonly id="dstrct" value="<%=dstrct%>" size="9" maxlength="4">
        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
      <td nowrap><strong>Nit</strong></td>
      <td width="217" nowrap><input name="nit" type="text" class="textbox" id="nit2" value="<%=nit%>" size="17" maxlength="12">
        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
      </tr>
    <tr class="fila">
      <td nowrap><strong>Codigo de la Empresa</strong></td>
      <td width="119" nowrap><input name="codigo_empresa" type="text" class="textbox" id="codigo_empresa2" value="<%=codigo_empresa%>" size="15" maxlength="4">
        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
      <td width="128" nowrap><strong>Nombre Compa&ntilde;ia</strong></td>
      <td nowrap><input name="description" type="text" class="textbox" id="description" value="<%=descripcion%>" size="36" maxlength="15">
        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
      </tr>
	<tr class="fila">
      <td nowrap><strong>Codigo Regional </strong></td>
      <td nowrap><input name="codigo_regional" type="text" class="textbox" id="codigo_regional" value="<%=codigo_reg%>" size="10" maxlength="3">
        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
      <td nowrap><strong>Resolucion</strong></td>
      <td nowrap><input name="resolucion" type="text" class="textbox" id="resolucion2" value="<%=resolucion%>" size="20" maxlength="30">
        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
      </tr>
	</table>
	</td>
	</tr>
  </table>
	<table width="630" border="2" align="center">
  <tr>
    <td>
	<table width="99%" align="center" cellpadding="0">
  <tr>
    <td width="50%"   class="subtitulo1"><p align="left">Datos Aseguradora </p></td>
    <td width="50%"   class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
	<table width="99%" align="center">
    <tr class="fila">
      <td width="29%" nowrap><strong>Nombre Aseguradora</strong></td>
      <td colspan="3" nowrap><input name="nombre_aseguradora" type="text" class="textbox" id="nombre_aseguradora" value="<%=aseguradora%>" size="40" maxlength="30">
        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    </tr>
    <tr class="fila">
      <td nowrap><strong>Poliza Aseguradora </strong></td>
      <td width="25%" nowrap><input name="poliza_aseguradora" type="text" class="textbox" id="poliza_aseguradora" value="<%=poliza%>" maxlength="6">
        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
      <td width="22%" nowrap><strong>Fecha vencimiento</strong></td>
      <td width="24%" nowrap><input name="fecha_ven_poliza" type="text" class="textbox" id="fecha_ven_poliza2"  value="<%=fecha%>" size="18" readonly>
        <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fecha_ven_poliza);return false;" HIDEFOCUS> <img src="<%=BASEURL%>/js/Calendario/cal.gif" width="16" height="16"
               border="0" alt="De click aqu&iacute; para escoger la fecha" ></a><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    </tr>
	</table>
	</td>
	</tr>
	</table>
	
	
<table width="630" border="2" align="center">
  <tr>
    <td>
	<table width="99%" align="center" cellpadding="0">
  <tr>
    <td width="50%"  class="subtitulo1"><p align="left">Otra Informaci&oacute;n </p></td>
    <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
	<table width="99%" align="center">
    <tr class="fila">
      <td width="155" nowrap><strong>Proyecto por defecto </strong></td>
      <td colspan="3" nowrap><input name="default_project" type="text" class="textbox" id="default_project" value="<%=project%>" size="20" maxlength="15"></td>
    </tr>
    <tr class="fila" >
      <td nowrap><strong>Peso Maximo Permitido</strong></td>
      <td width="127" nowrap><input name="pmax" type="text" class="textbox" onKeyPress="soloDigitos(event,'decOK')" id="pmax2" value="<%=peso_lleno%>" size="15"></td>
      <td width="130" nowrap><strong>Porcentaje Maximo</strong></td>
      <td width="172" nowrap><input name="pormax" type="text" class="textbox" onKeyPress="soloDigitos(event,'decOK')" id="pormax2" value="<%=anticipo%>" size="6"></td>
    </tr>
    <tr class="fila">
      <td nowrap><strong>Hora Minera</strong></td>
      <td nowrap><input name="hora" type="text" class="textbox" id="hora" onKeyPress="soloDigitos(event,'decOK')" value="<%=hora%>" size="6" maxlength="2">
      </td>
      <td colspan="2" nowrap><span class="msg">* Escriba un numero entre 0 y 24 hrs</span></td>
      </tr>
  </table>
    <table width="530" border="1" align="center" bgcolor="ECE0D8" class="Letras">
  </table>
  </td>
  </tr>
  </table>
<p>
<div align="center"><img src="<%=BASEURL%>/images/botones/modificar.gif" name="mod" width="101" height="21" onClick="form1.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></div>
</form>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>
