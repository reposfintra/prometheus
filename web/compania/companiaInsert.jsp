<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String men = (request.getParameter("men")!=null)?request.getParameter("men"):"";%>
<html>
<head>
<title>Ingresar Compa&ntilde;ia</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="<%=BASEURL%>/js/validar.js"></script>
<script src="<%=BASEURL%>/js/boton.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">

</head>

<body>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Compania&accion=Insert&cmd=show" onSubmit="">
 <table width="630" border="2" align="center">
  <tr>
    <td>
<table width="99%" align="center" cellpadding="0">
  <tr>
    <td width="392"  class="subtitulo1"><p align="left">Datos de la empresa </p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
  <table width="99%" align="center" cellpadding="0" cellspacing="0" class="Letras">
    <tr class="fila">
      <td width="146" nowrap><strong>Distrito</strong></td>
      <td width="116" nowrap><input name="dstrct" type="text" class="textbox" id="dstrct" size="9" maxlength="4">
        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
      <td nowrap><strong>Nit</strong></td>
      <td width="226" nowrap><input name="nit" type="text" class="textbox" id="nit2" size="17" maxlength="12">
        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
      </tr>
    <tr class="fila">
      <td nowrap><strong>Codigo de la Empresa</strong></td>
      <td nowrap><input name="codigo_empresa" type="text" class="textbox" id="codigo_empresa2" size="15" maxlength="4">
        <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
      <td width="122" nowrap><strong>Nombre Compa&ntilde;ia</strong></td>
      <td nowrap><input name="description" type="text" class="textbox" id="description" size="36" maxlength="15">
        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
      </tr>
	<tr class="fila">
      <td nowrap><strong>Codigo Regional </strong></td>
      <td nowrap><input name="codigo_regional" type="text" class="textbox" id="codigo_regional" size="10" maxlength="3">
        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
      <td nowrap><strong>Resolucion</strong></td>
      <td nowrap><input name="resolucion" type="text" class="textbox" id="resolucion2" size="20" maxlength="30">
        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
      </tr>
	</table>
	</td>
	</tr>
  </table>
	<table width="630" border="2" align="center">
  <tr>
    <td>
	<table width="99%" align="center" cellpadding="0">
  <tr>
    <td width="392"  class="subtitulo1"><p align="left">Datos Aseguradora </p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
	<table width="99%" align="center">
    <tr class="fila">
      <td width="29%" nowrap><strong>Nombre Aseguradora</strong></td>
      <td colspan="3" nowrap><input name="nombre_aseguradora" type="text" class="textbox" id="nombre_aseguradora" size="40" maxlength="30">
        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    </tr>
    <tr class="fila">
      <td nowrap><strong>Poliza Aseguradora </strong></td>
      <td width="24%" nowrap><input name="poliza_aseguradora" type="text" class="textbox" id="poliza_aseguradora" maxlength="6">
        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
      <td width="21%" nowrap><strong>Fecha vencimiento</strong></td>
      <td width="26%" nowrap><input name="fecha_ven_poliza" type="text" class="textbox" id="fecha_ven_poliza2"  value="0099-01-01" size="18" readonly>
        <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fecha_ven_poliza);return false;" HIDEFOCUS> <img src="<%=BASEURL%>/js/Calendario/cal.gif" width="16" height="16"
               border="0" alt="De click aqu&iacute; para escoger la fecha" ></a><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    </tr>
	</table>
	</td>
	</tr>
	</table>
	
	
<table width="630" border="2" align="center">
  <tr>
    <td>
	<table width="99%" align="center" cellpadding="0">
  <tr>
    <td width="392"  class="subtitulo1"><p align="left">Otra Informaci&oacute;n </p></td>
    <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
	<table width="99%" align="center">
    <tr class="fila">
      <td width="155" nowrap><strong>Proyecto por defecto </strong></td>
      <td colspan="3" nowrap><input name="default_project" type="text" class="textbox" id="default_project" size="20" maxlength="15"></td>
    </tr>
    <tr class="fila" >
      <td nowrap><strong>Peso Maximo Permitido</strong></td>
      <td width="127" nowrap><input name="pmax" type="text" class="textbox" value="0" id="pmax2" size="15" onKeyPress="soloDigitos(event,'decOK')">
        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
      <td width="130" nowrap><strong>Porcentaje Maximo</strong></td>
      <td width="172" nowrap><input name="pormax" type="text" class="textbox" value="0" id="pormax2" size="6" onKeyPress="soloDigitos(event,'decOK')">
        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
    </tr>
    <tr class="fila">
      <td nowrap><strong>Hora Minera</strong></td>
      <td nowrap><input name="hora" type="text" class="textbox" id="hora" value="0" size="6" maxlength="2" onKeyPress="soloDigitos(event,'decOK')">
        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">      </td>
      <td colspan="2" nowrap><span class="msg">* Escriba un numero entre 0 y 24 hrs</span></td>
      </tr>
  </table>
    <table width="530" border="1" align="center" bgcolor="ECE0D8" class="Letras">
  </table>
  </td>
  </tr>
  </table>
    <p>
<div align="center">
  <p><img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" width="90" height="21" onClick="form1.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
      <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="form1.reset();" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;
      <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></p>
  <p>
    <%if(!men.equals("")){%>
</p>
  <p>
  <table border="2" align="center">
    <tr>
      <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="229" align="center" class="mensajes"><%=men%></td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="58">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p></p>
  <%}%>
</div>
</p>
</div>
    </form>
	<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>
