<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Ingresar codigos por demora</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="js/validar.js"></script>
<link href="css/letras.css" rel="stylesheet" type="text/css">

</head>

<body>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Sjdelay&accion=Insert&cmd=show" onSubmit="return ValidarFormularioACPM(this);">
  <table width="530" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="#EFE3DE" class="Letras">
    <tr bgcolor="#FFA928">
      <td colspan="2" nowrap><div align="center" class="Estilo2"><strong><strong>INGRESAS CODIGOS DE DEMORA POR SJ</strong></strong></div></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td width="168" nowrap bgcolor="#99CCFF"><strong>SJ:</strong></td>
      <td width="346" nowrap bgcolor="ECE0D8"><input name="sj" type="text" id="sj">          </td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap bgcolor="#99CCFF"><strong>Demora: </strong></td>
      <td nowrap bgcolor="ECE0D8">
	  <%model.sdelayService.listDelays("FINV");
	  TreeMap cf = model.sdelayService.getDemoras(); %>
	  <input:select name="cf" options="<%=cf%>" attributesText="style='width:100%;'" />
	  </td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap bgcolor="#99CCFF"><strong>Distrito:</strong></td>
      <td nowrap bgcolor="ECE0D8"><select name="distrito" id="distrito">
        <option value="FINV" selected>FINV</option>
      </select>
	  </td>
    </tr>
  </table>
  <div align="center">
    <input type="submit" name="Submit" value="Registrar">
    <input type="button" name="Submit2" value="Regresar">
  </div>
  <table width="530" border="1" align="center" bgcolor="ECE0D8" class="Letras">
  </table>
</form>
</body>
</html>
