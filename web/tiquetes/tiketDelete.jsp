<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
    <title>Anular  proveedor tiquetes</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>

</head>

<body>

 <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=Anular Proveedor Tiquetes"/>
    </div>
 <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top:100px; overflow: scroll;">  

    <%Usuario usuario = (Usuario) session.getAttribute("Usuario");
	String mensaje =(request.getParameter("mensaje")!=null)?request.getParameter("mensaje"):"";%>
    <form name="form" method="post" action="<%=CONTROLLER%>?estado=Tiquete&accion=Search&num=1">
      <table width="650" border="2" align="center">
        <tr>
          <td><table width="100%" class="tablaInferior">
            <tr>
              <td width="48%" class="subtitulo1">&nbsp;Escoja el Proveedor de Tiquete a 


 Anular</td>
              <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
            </tr>
          </table>   
		    <table width="100%"  border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
            <tr class="tblTitulo" align="center">
              <td colspan="2"  >NIT:</strong></td>
              <td width="135" ><strong>CIUDAD</strong></td>
              <td width="97" >DISTRITO</td>
            </tr>
            <%if(model.proveedortiquetesService.existProveedoresTiquetes(usuario.getDstrct())){
			List list = model.proveedortiquetesService.getProveedoresTIQUETES(usuario.getDstrct());
			int i=0,a=0;
			Iterator it=list.iterator();
				while (it.hasNext()){%>
            <%Proveedor_Tiquetes pa = (Proveedor_Tiquetes) it.next();
					
					String  nit= pa.getNit();
					String desc=pa.getNombre();
					String ciudad=pa.getCiudad();
                    String distrito= pa.getDstrct();
					String sucursal = pa.getCodigo();
					float valor= pa.getValor();
%>
            <tr class="<%=(a % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand" 
			 title="Anular Proveedor de Tiquetes..." onClick="window.location='<%=CONTROLLER%>?estado=Tiquete&accion=Search&num=2&nit=<%=nit%>&sucursal=<%=sucursal%>'" >
              <td width="105" class="bordereporte"><%=nit%></td>
              <td width="313" class="bordereporte"><%=desc%> <%=sucursal%></td>
              <td  class="bordereporte"><%=ciudad%></td>
              <td class="bordereporte"><%=distrito%></td>
            </tr>
			
            <%a++;
			}
	  }%>
          </table></td>
        </tr>
      </table>
    </form>

    <form name="form2" method="post" action="<%=CONTROLLER%>?estado=Tiquetes&accion=Delete&cmd=show">
      <%String nit="";
  	String distrito="", cciudad="", tipo="", moneda="", nombre="", codigo="", nciudad="", sucursal="";
	float valor=0;
	if(request.getAttribute("ptiquetes")!=null){
     	Proveedor_Tiquetes pa= (Proveedor_Tiquetes)request.getAttribute("ptiquetes");
		nit=pa.getNit();
        cciudad=pa.getCity_code();
		nciudad=pa.getCiudad();
        nombre=pa.getNombre();
		valor= pa.getValor();
		distrito=pa.getDstrct();
		sucursal = pa.getCodigo();
					
					
	%>    
      <table width="530" border="2" align="center">
        <tr>
          <td><table width="100%" class="tablaInferior">
            <tr>
              <td width="48%" class="subtitulo1">&nbsp;Anular Proveedor Tiquete</td>
              <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
            </tr>
          </table>
            <table width="100%" class="tablaInferior">
            <tr class="fila">
              <td width="168" rowspan="2" >Nit:</td>
              <td width="346" > <%=nit%>
                  <input name="nit" type="hidden" id="nit" value="<%=nit%>" >
                  <input name="sucursal" type="hidden" id="sucursal" value="<%=sucursal%>"></td>
            </tr>
            <tr class="fila">
              <td><%=nombre%><%=sucursal%></td>
            </tr>
            <tr class="fila">
              <td>Distrito:</td>
              <td><%=distrito%></td>
            </tr>
            <tr class="fila">
              <td >Ciudad:</td>
              <td><%=nciudad%> </td>
            </tr>
          </table></td>
        </tr>
      </table>
    <div align="center"><br>
        <img src="<%=BASEURL%>/images/botones/anular.gif"  name="imganular" onClick="form2.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
        <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">

    </div>
  <%}%>
    <table width="530" border="1" align="center" bgcolor="ECE0D8" class="Letras">
    </table>
    </form>
	  <br>
  <% if( !mensaje.equalsIgnoreCase("")){%>
<table border="2" align="center">
  <tr>
    <td><table width="100%" border="1" align="center" bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes">Anulaci&oacute;n Exitosa</td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>    <% }%>
</div>
</body>
</html>
