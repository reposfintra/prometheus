<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<html>
<head>
<title>Ingresar proveedor tiquetes</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 

</head>

<body>

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=Ingresar Proveedor Tiquetes"/>
    </div>
 <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top:100px; overflow: scroll;">        

<%
   String mensaje =(request.getParameter("mensaje")!=null)?request.getParameter("mensaje"):"";%>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Tiquetes&accion=Insert&cmd=show" >
  <table width="530" border="2" align="center">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td width="48%" class="subtitulo1">&nbsp;Información</td>
          <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
        <table align="center" class="tablaInferior">
        <tr class="fila">
          <td width="168"><strong>Nit:</strong></td>
          <td width="346"><input name="nit" type="text" class="textbox" id="nit" maxlength="15">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">          </td>
        </tr>
        <tr class="fila">
          <td width="168"><strong>Sucursal:</strong></td>
          <td ><input name="sucursal" type="text" class="textbox" id="sucursal">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
        </tr>
        <tr class="fila">
          <td nowrap><strong>Distrito:</strong></td>
          <td nowrap><select name="distrito" class="textbox" id="distrito">
              <option value="FINV" selected>FINV</option>
          </select>
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
        </tr>
        <tr class="fila">
          <td nowrap>Ciudad:</td>
          <td nowrap>
            <%TreeMap ciudades = model.ciudadService.getCiudades(); %>
			 <input:select name="ciudad" options="<%=ciudades%>" attributesText="style='width:70%;' class='textbox'"/> 
			 <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
			 </td>
        </tr>
        <tr class="fila">
          <td nowrap><strong>Codigo de Migracion: </strong></td>
          <td nowrap><input name="codigo_m" type="text" class="textbox" id="codigo_m" size="2" maxlength="1">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
        </tr>
        <tr class="fila">
          <td nowrap>Porcentaje :</td>
          <td nowrap><input name="porcentaje" type="text" class="textbox" id="porcentaje" value="0" size="5" maxlength="5" onKeyPress="soloDigitos(event,'decOK')">
      %</td>
        </tr>
      </table></td>
    </tr>
  </table>
  <br>
  <div align="center">
	  <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="return ValidarFormAnticipo(form1);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
	  <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="form1.reset();" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;
      <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">

  </div>
<br>
<% if( !mensaje.equalsIgnoreCase("")){%>
<table border="2" align="center">
  <tr>
    <td><table width="100%" border="1" align="center" bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=mensaje%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>    <% }%>

</form>
 </div>

</body>
</html>
