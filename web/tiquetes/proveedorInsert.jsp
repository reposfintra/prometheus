<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Ingresar proveedor de anticipo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="<%= BASEURL %>/js/validar.js"></script>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Proveedor de anticipo"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Proveedores&accion=Insert&cmd=show" onSubmit="return ValidarFormAnticipo(this);">
  <table width="530" border="2" align="center">
    <tr>
      <td><table width="100%" border="0" align="center" class="tablaInferior">
        <tr>
           <td width="124" height="24"  class="subtitulo1"><p align="left">Ingresar </p></td>
            <td width="260"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>       
        <tr class="fila">
          <td width="162" nowrap><strong class="fila">Nit:</strong></td>
          <td width="352" nowrap><input name="nit" type="text" id="nit" maxlength="15" class="textbox">
          </td>
        </tr>
        <tr class="fila">
          <td width="162" nowrap><strong>Sucursal:</strong></td>
          <td nowrap><input name="sucursal" type="text" id="sucursal" maxlength="15" class="textbox"></td>
        </tr>
        <tr class="fila">
          <td nowrap><strong>Distrito:</strong></td>
          <td nowrap><select name="distrito" id="distrito" class="textbox">
              <option value="FINV" selected>FINV</option>
          </select></td>
        </tr>
        <tr class="fila">
          <td nowrap><strong>Ciudad:</strong></td>
          <td nowrap>
            <%TreeMap ciudades = model.ciudadService.getCiudades(); %>
            <input:select name="ciudad" options="<%=ciudades%>" attributesText="style='width:100%;' class='textbox';" /></td>
        </tr>
        <tr class="fila">
          <td nowrap><strong>Codigo de migracion: </strong></td>
          <td nowrap><input name="codigo_m" type="text" id="codigo_m" size="2" maxlength="1" class="textbox"></td>
        </tr>
        <tr class="fila">
          <td nowrap><strong>Porcentaje:</strong></td>
          <td nowrap><input name="porcentaje" type="text" id="porcentaje" value="0" size="5" maxlength="5" onKeyPress="soloDigitos(event,'decOK')" class="textbox">
      %</td>
        </tr>
        <tr class="fila">
          <td nowrap><strong>Valor del Servicio </strong></td>
          <td nowrap><input name="valor" type="text" id="valor" onKeyPress="soloDigitos(event,'no')"  class="textbox">
              <select name="moneda" id="moneda" class="textbox">
                <option value="PES">Pesos</option>
                <option value="BOL">Bolivar</option>
                <option value="DOL">Dolares</option>
            </select></td>
        </tr>
      </table></td>
    </tr>
  </table>
  <br>
  <div align="center">
    <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod"  height="21" onClick="return ValidarFormularioACPM(form1);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp; 
    <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="form1.reset();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; 
    <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
  </div>
  
</form>
</div>
</body>
</html>
