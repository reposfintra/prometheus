<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<link href="<%= BASEURL %>/css/estilotsp.css" rel="stylesheet" type="text/css"> 
<link href="../css/estilotsp.css" rel="stylesheet" type="text/css"> 

<html>
<head>
<title>Ingresar proveedor tiquetes</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="js/validar.js"></script>
<link href="css/letras.css" rel="stylesheet" type="text/css">

</head>

<body>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Tiquetes&accion=Insert&cmd=show" onSubmit="return ValidarFormTiquetes(this);">
  <table width="370" border="2" align="center">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td width="48%" class="subtitulo1">&nbsp;Informaci&oacute;n</td>
          <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
        <table width="530" align="center"class="tablaInferior">
        <tr class="fila">
          <td width="167" nowrap><strong>Nit:</strong></td>
          <td width="323" nowrap><input name="nit" type="text" class="textbox" id="nit" value="<%=request.getParameter("nit")%>">
          </td>
        </tr>
        <tr class="fila">
          <td nowrap>Sucursal:</td>
          <td width="323"><input name="sucursal" type="text" class="textbox" id="sucursal" value="<%=request.getParameter("sucursal")%>"></td>
        </tr>
        <tr class="fila">
          <td nowrap><strong>Distrito:</strong></td>
          <td nowrap ><select name="distrito" class="textbox" id="distrito">
              <option value="FINV" selected>FINV</option>
          </select></td>
        </tr>
        <tr class="fila">
          <td nowrap><strong>Ciudad:</strong></td>
          <td nowrap>
            <%TreeMap ciudades = model.ciudadService.getCiudades(); %>
            <input:select name="ciudad" options="<%=ciudades%>" attributesText="style='width:80%;' class='textbox'" default="<%=request.getParameter("ciudad")%>"/></td>
        </tr>
        <tr class="fila">
          <td nowrap><strong>Codigo de Migracion: </strong></td>
          <td nowrap><input name="codigo_m" type="text" class="textbox" id="codigo_m" value="<%=request.getParameter("codigo_m")%>" size="2" maxlength="1"></td>
        </tr>
        <tr class="fila">
          <td nowrap>Porcentaje :</td>
          <td nowrap><input name="porcentaje" type="text" class="textbox" id="porcentaje" value="<%=request.getParameter("porcentaje")%>" size="5" maxlength="3">
      %</td>
        </tr>
      </table></td>
    </tr>
  </table>
  <br>
  <div align="center">
    <input type="submit" name="Submit" value="Registrar">
    <input type="button" name="Submit2" value="Cancelar" onclick="parent.close()">
  </div>
  <table width="530" border="1" align="center" bgcolor="ECE0D8" class="Letras">
  </table>
</form>
</body>
</html>
