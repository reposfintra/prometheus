<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Buscar Anticipo Placa</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="../bancoUsuario/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Estilo1 {font-size: 12px}
.Estilo2 {font-weight: bold}
.Estilo3 {font-size: 12px; font-weight: bold; }
-->
</style>
</head>
<body bgcolor="#ebebeb">
<%
String pl="", ef = "", acpm = "", ta = "", tb = "", tc = "";
String sjrev="";
String msg = "";
Auto_Anticipo aa = null;
int sw = 0;// 0=binactivo 1=bactivo
if (request.getParameter("msg").equals("vacio")){
	msg = "";
}
else if (request.getParameter("msg").equals("errorB")){
	msg = "Su b&uacute;squeda no arroj&oacute; resultados!";
	pl = request.getParameter("pl");
}
else if (request.getParameter("msg").equals("exitoB")){
	sw = 1;
	msg = "Su b&uacute;squeda arroj&oacute; este resultado!";
	aa = (Auto_Anticipo) request.getAttribute("autoant");
	pl = aa.getPlaca();
	ef = "" + aa.getEfectivo();
	acpm = "" + aa.getacpm();
	ta = "" + aa.getTicket_a();
	tb = "" + aa.getTicket_b();
	tc = "" + aa.getTicket_c();
	sjrev = aa.getSj();
}
Usuario usuario = (Usuario) session.getAttribute("Usuario");

%>
<form name="form1" id="form1" method="post" action="<%=CONTROLLER%>?estado=AutoAnticipo&accion=Search">
<table border="2" align="center" width="469">
  <tr>
    <td><span class="Estilo1"></span>
<table width="99%" border="1" align="center"   bordercolor="#F7F5F4" bgcolor="#FFFFFF">
  <tr>
    <td width="48%" height="24"  class="subtitulo1"><p align="left">Buscar Anticipo Placa </p></td>
    <td width="52%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
  <table width="530" align="center" class="Letras">
    <tr class="fila"  >
      <td width="166" nowrap  ><strong>PLACA</strong></td>
      <td width="288" nowrap  >
	    <input name="pl" type="text" class="textbox" id="pl" value="<%=pl%>" maxlength="12"></td>
    </tr>
    <tr class="fila"  >
      <td width="166" nowrap  ><strong>RUTA</strong></td>
      <td nowrap  ><span class="Estilo6"><strong>
        <%
		 if(model.stdjobdetselService.existStandardsProy(usuario.getProject())){
			List list = model.stdjobdetselService.getStandardsProy(usuario.getProject(),usuario.getBase());%>
        <select name="standard" class="listmenu" id="standard">
          <%
		   	Iterator it=list.iterator();
				while (it.hasNext()){
					Stdjobdetsel std = (Stdjobdetsel) it.next();
					String desc = std.getSj_desc();
					String sj=std.getSj();
					if(sjrev.equals(sj)){
					%>
          <option value="<%=sj%>" selected><%=desc%></option>
		  <%}else{%>
		  <option value="<%=sj%>"><%=desc%></option>
		  <%}%>
          <%}%>
        </select>
        <%}else{%>
        <strong>No existe ning&uacute;n StandardJob Registrado.</strong>
        <%}%>
      </strong></span></td>
    </tr>
    <tr class="fila"  >
      <td width="166" nowrap  ><strong>EFECTIVO</strong></td>
      <td nowrap  ><input name="ef" type="text" class="textbox" id="ef" value="<%=ef%>"></td>
    </tr>
    <tr class="fila"  >
      <td nowrap  ><strong>ACPM</strong></td>
      <td nowrap  ><input name="acpm" type="text" class="textbox" id="acpm" value="<%=acpm%>"></td>
    </tr>
	<tr class="fila"  >
      <td nowrap  ><strong>PEAJE</strong></td>
      <td nowrap  >
	    <table width="100%">
          <tr class="fila">
            <td><div align="center">Ticket A </div></td>
            <td><div align="center">Ticket B </div></td>
            <td><div align="center">Ticket C </div></td>
          </tr>
          <tr>
            <td><div align="center">
      		  <input name="ta" type="text" class="textbox" id="ta" value="<%=ta%>" size="15">
    		</div></td>
    		<td><div align="center">
      		  <input name="tb" type="text" class="textbox" id="tb" value="<%=tb%>" size="15">
    		</div></td>
    		<td><div align="center">
      		  <input name="tc" type="text" class="textbox" id="tc" value="<%=tc%>" size="15">
    		</div></td>
          </tr>
	    </table>
	  </td>
    </tr>
  </table> 
  </td>
  </tr>
  </table>
  <br>
<table width="80%"  border="0" align="center">
 <%if (sw == 1 ){%>
	<tr  >
      <td colspan="2" nowrap align="center">
      <img src="<%=BASEURL%>/images/botones/modificar.gif" name="mod"  height="21" onClick="location.href='<%=BASEURL%>/anticipo_placas/anticipopUpdate.jsp?msg=deB&pl=<%=pl%>&ef=<%=ef%>&acpm=<%=acpm%>&ta=<%=ta%>&tb=<%=tb%>&tc=<%=tc%>&sj=<%=sjrev%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></strong>  &nbsp;
	  <img src="<%=BASEURL%>/images/botones/anular.gif" name="mod"  height="21" onClick="location.href='<%=BASEURL%>/anticipo_placas/anticipopNullify.jsp?msg=deB&pl=<%=pl%>&ef=<%=ef%>&acpm=<%=acpm%>&ta=<%=ta%>&tb=<%=tb%>&tc=<%=tc%>&sj=<%=sjrev%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">      </td>
    </tr>
	<%}%>
    <tr>
      <td colspan="2" nowrap align="center">
      <img src="<%=BASEURL%>/images/botones/buscar.gif" name="mod"  height="21" onClick="form1.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">      </td>
    </tr>
</table>
</form>
<br>
  <%if(!msg.equals("")){%>
	<p>
   <table width="420" border="2" align="center">
     <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%>
</body>
</html>