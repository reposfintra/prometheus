<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Registrar Anticipo Placa</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="../bancoUsuario/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script>
function validar0 (){
	if ( (form1.efectivo.value == "") || (form1.acpm.value == "" )){
		alert (" Llene los datos correctamente!");		
	} else 
	form1.submit();
}

var isIE = document.all?true:false;
var isNS = document.layers?true:false;
function soloDigitos(e,decReq) {
	var key = (isIE) ? window.event.keyCode : e.which;
    var obj = (isIE) ? event.srcElement : e.target;
    var isNum = (key > 47 && key < 58) ? true:false;
    var dotOK = (key==46 && decReq=='decOK' && (obj.value.indexOf(".")<0 || obj.value.length==0)) ? true:false;
    window.event.keyCode = (!isNum && !dotOK && isIE) ? 0:key;
    e.which = (!isNum && !dotOK && isNS) ? 0:key;
    return (isNum || dotOK);
}
</script>
<body>
<%String pl = "", ef = "0", acpm = "0", ta = "0", tb = "0", tc = "0";

String msg = "";
Usuario usuario = (Usuario) session.getAttribute("Usuario");
if (request.getParameter("msg")!=null) {
if (request.getParameter("msg").equals("exitoR")){
	msg = "El anticipo de " + request.getParameter("efectivo") + " al vehiculo de placa " + request.getParameter("placa").toUpperCase() + " \nha sido registrado exitosamente!";
}
else if (request.getParameter("msg").equals("error")){
	msg = "Ya existe registrado un anticipo en el sistemas para el vehiculo con placa " + request.getParameter("placa").toUpperCase();
	pl = request.getParameter("placa");
	ef = request.getParameter("efectivo");
	acpm = request.getParameter("acpm");
	ta = request.getParameter("ta");
	tb = request.getParameter("tb");
	tc = request.getParameter("tc");
}
else if (request.getParameter("msg").equals("noBD")){
    msg = "La placa con Nro. " + request.getParameter("placa").toUpperCase() + " no se encuentra registrada en el sistema. Por favor verifique!";
	pl = request.getParameter("placa");
	ef = request.getParameter("efectivo");
	acpm = request.getParameter("acpm");
	ta = request.getParameter("ta");
	tb = request.getParameter("tb");
	tc = request.getParameter("tc");
}
}
%>
<form name="form1" id="form1" method="post" action="<%=CONTROLLER%>?estado=AutoAnticipo&accion=Insert" >
<table border="2" align="center" width="515">
  <tr>
    <td height="182"><span class="Estilo1"></span>
<table width="99%" align="center">
  <tr>
    <td width="48%" height="22"  class="subtitulo1"><p align="left">Registrar Anticipo</p></td>
    <td width="52%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
  <table width="99%" align="center" class="Letras">
    <tr class="fila"  >
      <td width="91" nowrap  ><strong>PLACA</strong></td>
      <td width="383" nowrap  >
	    <input name="placa" type="text" class="textbox" id="placa" value="<%=pl%>" maxlength="12"></td>
    </tr>
    <tr class="fila"  >
      <td width="91" nowrap  ><strong>RUTA</strong></td>
      <td nowrap  ><span class="Estilo6"><strong>
        <%
		 if(model.stdjobdetselService.existStandardsProy(usuario.getProject())){
			List list = model.stdjobdetselService.getStandardsProy(usuario.getProject(),usuario.getBase());%>
        <select name="standard" class="listmenu" id="standard">
          <%
		   	Iterator it = list.iterator();
				while (it.hasNext()){
					Stdjobdetsel std = (Stdjobdetsel) it.next();
					String desc = std.getSj_desc();
					String sj=std.getSj();
					%>
          <option value="<%=sj%>"><%=desc%></option>
          <%}%>
        </select>
        <%}else{%>
        <strong>No existe ning&uacute;n StandardJob Registrado.</strong>
        <%}%>
      </strong></span></td>
    </tr>
    <tr class="fila"  >
      <td width="91" nowrap  ><strong>EFECTIVO</strong></td>
      <td nowrap  ><input name="efectivo" type="text" class="textbox" id="efectivo" onKeyPress="soloDigitos(event,'decOK')" value="<%=ef%>"></td>
    </tr>
    <tr class="fila"  >
      <td nowrap  ><strong>ACPM</strong></td>
      <td nowrap  ><input name="acpm" type="text" class="textbox" id="acpm" onKeyPress="soloDigitos(event,'decOK')" value="<%=acpm%>"></td>
    </tr>
    <tr class="fila"  >
      <td nowrap  ><strong>PEAJE</strong></td>
      <td nowrap  ><table width="100%">
  <tr class="fila">
    <td><div align="center">Ticket A </div></td>
    <td><div align="center">Ticket B </div></td>
    <td><div align="center">Ticket C</div></td>
  </tr>
  <tr>
    <td><div align="center">
      <input name="ta" type="text" class="textbox" id="ta" onKeyPress="soloDigitos(event,'decNO')" value="<%=ta%>" size="15">
    </div></td>
    <td><div align="center">
      <input name="tb" type="text" class="textbox" id="tb" onKeyPress="soloDigitos(event,'decNO')" value="<%=tb%>" size="15">
    </div></td>
    <td><div align="center">
      <input name="tc" type="text" class="textbox" id="tc" onKeyPress="soloDigitos(event,'decNO')" value="<%=tc%>" size="15">
    </div></td>
  </tr>
</table>
</td>
    </tr>
    <tr>
      <td colspan="2" nowrap align="center">
      </td>
    </tr>
  </table>  
  </td>
  </tr>
  </table>
  <br>
  <div align="center">    <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod"  height="21" onClick="validar0();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp; 
	<img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="form1.reset();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; 
	<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></div>
</form>
<br>
<%if(!msg.equals("")) {%>
<table width="465" border="2" align="center">
     <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="321" align="center" class="mensajes"><%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="81">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%>
</body>
</html>