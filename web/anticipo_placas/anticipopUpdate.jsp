<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Modificar Anticipo Placa</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="../bancoUsuario/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script>
function validar0 (){
	if ( (form1.ef.value == "0") || (form1.acpm.value == "0" )){
		alert (" El valor del EFECTIVO y ACPM deben ser diferentes de O (cero)");		
	}
	else 
		form1.submit();
}

var isIE = document.all?true:false;
var isNS = document.layers?true:false;
function soloDigitos(e,decReq) {
	var key = (isIE) ? window.event.keyCode : e.which;
    var obj = (isIE) ? event.srcElement : e.target;
    var isNum = (key > 47 && key < 58) ? true:false;
    var dotOK = (key==46 && decReq=='decOK' && (obj.value.indexOf(".")<0 || obj.value.length==0)) ? true:false;
    window.event.keyCode = (!isNum && !dotOK && isIE) ? 0:key;
    e.which = (!isNum && !dotOK && isNS) ? 0:key;
    return (isNum || dotOK);
}
</script>
<body >
<%
String pl="", ef = "0", acpm = "0", ta = "0", tb = "0", tc = "0";

String msg = "";
String std ="";
if(request.getParameter("msg")!=null){
if (request.getParameter("msg").equals("deB")){
	msg = "";
	pl = request.getParameter("pl");
	ef = request.getParameter("ef");
	acpm = request.getParameter("acpm");
	ta = request.getParameter("ta");
	tb = request.getParameter("tb");
	tc = request.getParameter("tc");
	std = request.getParameter("sj");
}
else if (request.getParameter("msg").equals("exitoM")){
	msg = "La modificaci&oacute;n fue exitosa!";
	Auto_Anticipo aa = (Auto_Anticipo) request.getAttribute("autoant");
	pl = aa.getPlaca();
	ef = "" + aa.getEfectivo();
	acpm = "" + aa.getacpm();
	ta = "" + aa.getTicket_a();
	tb = "" + aa.getTicket_b();
	tc = "" + aa.getTicket_c();
	std = aa.getSj();
	
}
}
Usuario usuario = (Usuario) session.getAttribute("Usuario");

%>

<form name="form1" id="form1" method="post" action="<%=CONTROLLER%>?estado=AutoAnticipo&accion=Update" >
<table border="2" align="center" width="469">
  <tr>
    <td><span class="Estilo1"></span>
<table width="99%" align="center">
  <tr>
    <td width="48%" height="24"  class="subtitulo1"><p align="left">Modificar  Anticipo Placa </p></td>
    <td width="52%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
  <table width="530" align="center" class="Letras">
    <tr class="fila" >
      <td width="161" nowrap ><strong>PLACA</strong></td>
      <td width="345" nowrap  >
	    <input name="pl" type="text" class="textbox" id="pl" value="<%=pl%>" maxlength="12" readonly></td>
    </tr>
    <tr class="fila" >
      <td width="161" nowrap ><strong>RUTA</strong></td>
      <td nowrap  ><span class="Estilo6"><strong>
        <% 
	  Stdjobdetsel sjdet=new Stdjobdetsel();
	  model.stdjobdetselService.searchStdJob(std);
	  
		if(model.stdjobdetselService.getStandardDetSel()!=null){
			sjdet = model.stdjobdetselService.getStandardDetSel();
            }
			else
			out.println("El standard no se encuentra");%>
			<%=sjdet.getSj_desc()%>
        <input name="standard" type="hidden" id="standard" value="<%=std%>">
</strong></span></td>
    </tr>
    <tr class="fila" >
      <td width="161" nowrap ><strong>EFECTIVO</strong></td>
      <td nowrap  ><input name="ef" type="text" class="textbox" id="ef" onKeyPress="soloDigitos(event,'decOK')" value="<%=ef%>"></td>
    </tr>
    <tr class="fila" >
      <td nowrap ><strong>ACPM</strong></td>
      <td nowrap  ><input name="acpm" type="text" class="textbox" id="acpm" onKeyPress="soloDigitos(event,'decOK')" value="<%=acpm%>"></td>
    </tr>
	<tr class="fila" >
      <td nowrap ><strong>PEAJE</strong></td>
      <td nowrap  >
	    <table width="100%">
          <tr class="fila">
            <td><div align="center" ><strong>Ticket A </strong></div></td>
            <td><div align="center" >Ticket B </div></td>
            <td><div align="center" >Ticket C </div></td>
          </tr>
          <tr>
            <td><div align="center">
      		  <input name="ta" type="text" class="textbox" id="ta" onKeyPress="soloDigitos(event,'decNO')" value="<%=ta%>" size="15">
    		</div></td>
    		<td><div align="center">
      		  <input name="tb" type="text" class="textbox" id="tb" onKeyPress="soloDigitos(event,'decNO')" value="<%=tb%>" size="15">
    		</div></td>
    		<td><div align="center">
      		  <input name="tc" type="text" class="textbox" id="tc" onKeyPress="soloDigitos(event,'decNO')" value="<%=tc%>" size="15">
    		</div></td>
          </tr>
	    </table>
	  </td>
    </tr>
  </table>  
  </td>
  </tr>
  </table>
  <br>
<div align="center">  <img src="<%=BASEURL%>/images/botones/modificar.gif" name="mod"  height="21" onClick="validar0();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;	&nbsp; 
	<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
</div>
</form>

<br>
  <%if(!msg.equals("")){%>
	<p>
   <table width="420" border="2" align="center">
     <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%>
</body>
</html>