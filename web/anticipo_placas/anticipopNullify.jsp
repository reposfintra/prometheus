<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Anular Anticipo Placa</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="../bancoUsuario/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Estilo1 {font-size: 12px}
.Estilo2 {font-weight: bold}
.Estilo3 {font-size: 12px; font-weight: bold; }
-->
</style>
</head>
<body bgcolor="#ebebeb">
<%String pl="", ef = "0", acpm = "0", ta = "0", tb = "0", tc = "0";

String msg = "";
int sw = 0;//boton
if (request.getParameter("msg").equals("deB")){
	msg = "";
	pl = request.getParameter("pl");
	ef = request.getParameter("ef");
	acpm = request.getParameter("acpm");
	ta = request.getParameter("ta");
	tb = request.getParameter("tb");
	tc = request.getParameter("tc");
}
else if (request.getParameter("msg").equals("exitoA")){
	msg = "El anticipo correspondiente a la placa " +  request.getParameter("pl");
	sw = 1;
}
%>
<form name="form1" id="form1" method="post" action="<%=CONTROLLER%>?estado=AutoAnticipo&accion=Nullify">
<table border="2" align="center" width="469">
  <tr>
    <td><span class="Estilo1"></span>
<table width="99%" align="center">
  <tr>
    <td width="48%"  class="subtitulo1"><p align="left">Informacion  Anticipo</p></td>
    <td width="52%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
  <table width="99%" align="center" class="Letras">
    <tr   class="fila">
      <td width="120" nowrap  ><strong>PLACA</strong></td>
      <td width="308" nowrap  >
	    <input name="pl" type="text" class="textbox" id="pl" value="<%=pl%>" maxlength="12" readonly></td>
    </tr>
    <tr   class="fila">
      <td width="120" nowrap  ><strong>EFECTIVO</strong></td>
      <td nowrap  ><input name="ef" type="text" class="textbox" id="ef" value="<%=ef%>" readonly></td>
    </tr>
    <tr   class="fila">
      <td nowrap  ><strong>ACPM</strong></td>
      <td nowrap  ><input name="acpm" type="text" class="textbox" id="acpm" value="<%=acpm%>" readonly></td>
    </tr>
	<tr   class="fila">
      <td nowrap  ><strong>PEAJE</strong></td>
      <td nowrap  >
	    <table width="100%">
          <tr class="fila">
            <td><div align="center">Ticket A </div></td>
            <td><div align="center">Ticket B </div></td>
            <td><div align="center">Ticket C </div></td>
          </tr>
          <tr>
            <td><div align="center">
      		  <input name="ta" type="text" class="textbox" id="ta" value="<%=ta%>" size="15" readonly>
    		</div></td>
    		<td><div align="center">
      		  <input name="tb" type="text" class="textbox" id="tb" value="<%=tb%>" size="15" readonly>
    		</div></td>
    		<td><div align="center">
      		  <input name="tc" type="text" class="textbox" id="tc" value="<%=tc%>" size="15" readonly>
    		</div></td>
          </tr>
	    </table>
	  </td>
    </tr>
  </table>  
  </td>
  </tr>
  </table>
  <div align="center">
    <%if ( sw == 0 ){%>
		  <img src="<%=BASEURL%>/images/botones/anular.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="form1.submit();" onMouseOut="botonOut(this);" style="cursor:hand">
		  <%}%>
          &nbsp; <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"><br>
  </div>
</form>
<%if(!msg.equals("")){%>
	<p>
   <table width="420" border="2" align="center">
     <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%>
</body>
</html>