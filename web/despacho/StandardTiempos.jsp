<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Despacho - Despacho Masivo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/validar.js"></script>
<script language="javascript" src="<%=BASEURL%>/js/boton.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">

</head>
 <%String nombres ="";

 	List listTablaN = model.tbltiempoService.getTblTiempos(request.getParameter("standard"));
	Iterator itTblaN=listTablaN.iterator();
	while (itTblaN.hasNext()){
		Tbltiempo tbl = (Tbltiempo) itTblaN.next();
		if(!tbl.getTimeCode().equals("IVIA")){
			nombres=nombres+""+tbl.getTimeCode()+",";
		}
	}
%>
<body onLoad="form1.orden.focus();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 1px;">
 <jsp:include page="/toptsp.jsp?encabezado=STANDARD TIEMPOS"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Campos&accion=Validar&cmd=show" onSubmit="return ValidarFormulario(this,'<%=nombres%>')">
  <div align="center"></div>
  <div align="right">
    <%Usuario usuario = (Usuario) session.getAttribute("Usuario");
	int cant=8;
	String placa="", orden="", cedula="", peso="", trailer="", standar=""; 
	cant=model.tbltiempoService.cantidadTiempos(request.getParameter("standard"));
	placa=request.getParameter("placa");
	orden=request.getParameter("orden");
	cedula=request.getParameter("conductor");
	peso=request.getParameter("peso");
	trailer=request.getParameter("trailer");
	standar=request.getParameter("standard");
	String nombrePag="IGRESO DE VIAJES";
	int anchoTabla= (cant*155)+1424;
	cant=cant+11;
	%>
    <table width="<%=anchoTabla%>" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4" >
      <tr> 
	    
   
	    <td colspan="<%=cant%>" nowrap class="fila" align="center"><%=nombrePag%></td>
      </tr>
      <tr class="tblTitulo" > 
        <td width="27" nowrap><div align="center"><strong>REMISION</strong></div>
        </td>
        <td width="27" nowrap><div align="center" class="letras">
          <div align="center"><strong>PLACA</strong></div>
        </div></td>
        <td width="66"><div align="center" class="letras"> 
            <div align="center"><strong>TRAILER</strong></div>
        </div></td>
        <td width="90" bgcolor="#99CCFF"><div align="center" class="letras"> 
            <div align="center" class="letras"><strong>CONDUCTOR</strong></div>
        </div></td>
        <td width="110" class="letras"><div align="center" class="0  "> 
            <div align="center"><strong>NOMBRE CONDUCTOR </strong></div>
        </div></td>
        <td width="269"><div align="center"> 
            <div align="center"><strong>ESTANDAR</strong></div>
        </div></td>
        <td width="77"><div align="center"> 
            <div align="center"><strong>ORDEN DE CARGA</strong></div>
        </div></td>
        <td width="33"><div align="center" > 
            <div align="center"><strong>PESO LLENO MINA</strong></div>
        </div></td>
	    <td width="34"><div align="center" >
          <strong>PESO VACIO MINA </strong></div></td>
	    <td width="69"><div align="center" class="letras">
          <div align="center"><strong>PESO NETO MINA </strong></div>
        </div></td>
	    <%List listTabla = model.tbltiempoService.getTblTiempos(standar);
			Iterator itTbla=listTabla.iterator();
			while (itTbla.hasNext()){
				Tbltiempo tbl = (Tbltiempo) itTbla.next();
					if(!tbl.getTimeCode().equals("IVIA")){
				%>
				  <td width="155"><div align="center" class="Letras"><strong><%=tbl.getTimeDescription()%></strong></div></td>
			  <%}	
			  }%>
	    <td width="56"><div align="center" class="letras"><strong>ACCION</strong></div></td>
      </tr>
      <tr> 
        <td class="bordereporte"><div align="center">
        <input name="remision" type="text" class="textbox" id="remision" onKeyPress="soloDigitos(event,'decNO')" value="<%=request.getParameter("remision").toUpperCase()%>" size="6" maxlength="6">
</span> </div></td>
        <td class="bordereporte"><div align="center" class="letras">
          <input name="placa" type="text" class="textbox" id="placa" value="<%=request.getParameter("placa").toUpperCase()%>" size="8" >
          <br>
          <span class="letras"><span class="style2"><a href="<%=CONTROLLER%>?estado=Menu&accion=Enviar&numero=18" target="_blank">
          Agregar Placa</a></span></span>        </div></td>
        <td class="bordereporte"> <div align="center" class="letras"> 
            <input name="trailer" type="text" class="textbox" id="trailer" value="<%=request.getParameter("trailer")%>" size="8">
          </div></td>
        <td class="bordereporte"> <div align="center" class="letras"> 
            <input name="conductor" type="text" class="textbox" id="conductor" value="<%=request.getParameter("conductor")%>" size="12">
            <br>
            <a href="<%=CONTROLLER%>?estado=Menu&accion=Conductor" target="_blank">Agregar Conductor</a> </div></td>
        <td width="110" class="bordereporte"></td>
        <td class="bordereporte"> <div align="center" > 
            
		  <%
		 if(model.stdjobdetselService.existStandardsProy(usuario.getProject())){
			List list = model.stdjobdetselService.getStandardsProy(usuario.getProject(),usuario.getBase());%>
		    <select name="standard" class="listmenu" id="standard" onChange="cambiarFormulario2('StandardTiempos.jsp');">
		    <% 	Iterator it=list.iterator();
				while (it.hasNext()){
					Stdjobdetsel std = (Stdjobdetsel) it.next();
					String desc = std.getSj_desc();
					String sj=std.getSj();
						if(sj.equals(request.getParameter("standard"))){
				%>
              <option value="<%=sj%>" selected><%=desc%></option>
              <%
					}else{%>
              <option value="<%=sj%>"><%=desc%></option>
              <%}
					}%>
          </select>
		    <%}else{%>
	        <strong class="letraresaltada">No existe ning&uacute;n StandardJob Registrado.</strong>	      
		     <%}%>
          </div></td>
        <td class="bordereporte"> <div align="right" class="letras"> 
            <div align="center">
              <input name="orden" type="text" class="textbox" id="orden" value="<%=request.getParameter("orden")%>" size="10">
  </div>
          </div></td>
        <td class="bordereporte"><input name="pesolm" type="text" class="textbox" id="pesolm" onChange="buscarPesoN();" onKeyPress="soloDigitos(event,'decOK')" value="<%=request.getParameter("pesolm")%>" size="10"> </td>
		  <td class="bordereporte"><input name="pesovm" type="text" class="textbox" id="pesovm" onChange="buscarPesoN();" onKeyPress="soloDigitos(event,'decOK')" value="<%=request.getParameter("pesovm")%>" size="10"></td>
		  <td class="bordereporte">		    <div align="center" class="letras">
              <div align="center">
                <input name="pesonm" type="text" class="textbox" id="pesonm" value="<%=request.getParameter("pesonm")%>" size="10" readonly="">
              </div>
          </div></td>
		  <%List listTabla2 = model.tbltiempoService.getTblTiempos(standar);
			Iterator itTbla2=listTabla2.iterator();
			        
			while(itTbla2.hasNext()){
				Tbltiempo tbl = (Tbltiempo) itTbla2.next();
				if(!tbl.getTimeCode().equals("IVIA")){%>
	    <td width=155 class="bordereporte">
	    
                <input name="<%=tbl.getTimeCode()%>" type="text" class="textbox" id="<%=tbl.getTimeCode()%>"  onFocus="if(self.gfPop)gfPop.fFocus(document.form1.<%=tbl.getTimeCode()%>,'form1');if(self.gfPop)gfPop.fPopCalendar(document.form1.<%=tbl.getTimeCode()%>);return this.focus();" size="18" >
				
        </td>
		      <%}		
			  }%>
			  
				
        <td width="56" class="bordereporte"><div align="center">
          <input name="Guardar2" type="image"  id="Guardar2" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" src="<%=BASEURL%>/images/botones/validar.gif" align="middle"  style="cursor:hand" height="21">
        </div></td>
      </tr>
    </table>
  </div>
  <div align="center"></div>
  <p align="center">
    <input name="Guardar" type="image"  id="Guardar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" src="<%=BASEURL%>/images/botones/aceptar.gif" align="middle"  style="cursor:hand" height="21">
&nbsp;  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" align="absmiddle" style="cursor:hand" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> </p>  
</form>
<%
	 cant=13;
	 nombrePag="SALIDA DE VIAJES";
	cant=model.tbltiempoService.cantidadTiempos();
	 anchoTabla= (cant*164)+1535;
	cant=cant+16;
	int cantPla=model.planillaService.cantPlasIncompletas(usuario.getLogin());
	listTablaN = model.tbltiempoService.getTblTiemposSalida(usuario.getBase());
	nombres ="";
	itTblaN=listTablaN.iterator();
	while (itTblaN.hasNext()){
		Tbltiempo tbl = (Tbltiempo) itTblaN.next();
		nombres=nombres+""+tbl.getTimeCode()+",";
	}
	List plaAux =  model.planillaService.getPlasIncompletas(usuario.getLogin());
	int cantReg = plaAux.size();
	%>
  <form name="form2" method="post" action="<%=CONTROLLER%>?estado=Salida&accion=Validar&cmd=show" onSubmit="returnreturn ValidarFormularioCompleto(this,'',<%=cantReg%>);">
    <%if(session.getAttribute("PlaIncompletas")!=null){%>    
    <table width="<%=anchoTabla%>" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4" class="Letras">
      <tr class="fila">
        <td colspan="<%=cant-2%>" nowrap><div align="center" class="letras"><strong><strong><%=nombrePag%></strong></strong></div></td>
      </tr>
      <tr class="tblTitulo">
        <td width="83" height="43" nowrap><strong>REMISION</strong></td>
        <td width="79" nowrap><div align="center" class="letras">
          <div align="center"><strong>PLANILLA</strong></div>
        </div></td>
        <td width="52"><div align="center" class="letras">
            <div align="center"><strong>PLACA</strong></div>
        </div></td>
        <td width="72"><div align="center" class="letras">
            <div align="left"><strong>PESO LLENO DESCARGUE </strong></div>
        </div></td>
        <td width="35"><strong>PESO VACIO DESCARGUE</strong></td>
         <td width="35"><strong>PESO NETO DESCARGUE</strong></td>
         <%listTabla = model.tbltiempoService.getTblTiemposSalida(usuario.getBase());
			 itTbla=listTabla.iterator();
			while (itTbla.hasNext()){
				Tbltiempo tbl = (Tbltiempo) itTbla.next();%>
	    <td width="136" class="letras"><div align="center"><strong><%=tbl.getTimeDescription()%></strong></div></td>
		<%}%>
		<td width="87" class="letras"><div align="center" class="letras">
            <div align="center"><strong>VALOR ANTICIPO</strong></div>
        </div></td>
        <td width="104" class="letras"><strong>PROVEEDOR ANTICIPO</strong></td>
        <td width="60"><div align="center" class="letras">
            <div align="center"><strong>GLN ACPM </strong></div>
        </div></td>
        <td width="135"><div align="center" class="letras">
            <p align="center"><strong>PROVEEDOR DE ACPM</strong></p>
        </div></td>
        <td width="73"><div align="center" class="letras Estilo1">
            <div align="center">TIQUETE PEAJE A </div>
        </div></td>
        <td width="73"><div align="center" class="letras">
            <div align="center"><strong>TIQUETE PEAJE B</strong></div>
        </div></td>
        <td width="73" class="letras"><div align="center" class="letras"><strong>TIQUETE PEAJE C </strong></div></td>
        <td width="135" class="letras"><div align="center" class="letras"><strong>PROVEEDOR DE TIQUETES</strong></div></td>
      
        
      </tr>
      <%
	  
	  Iterator pla=plaAux.iterator();
	  int i=0;
	  while (pla.hasNext()){
	  Plaaux planilla = (Plaaux) pla.next();
		String pla_no = planilla.getPlanilla();
		String placa_no= planilla.getPlaca();
		String remision_no = planilla.getRemision();
		String sj = planilla.getSj();
				
	  %>
	  <tr>
        <td rowspan="2" nowrap class="bordereporte">
          <div align="center">
            <input name="rem<%=i%>" type="text" class="textbox" id="rem<%=i%>" value="<%=remision_no.toUpperCase()%>" size="8" readonly>
          </div></td>
        <td rowspan="2" nowrap class="bordereporte"><div align="center"><span class="letras">
        <input name="pla<%=i%>" type="text" class="textbox" value="<%=pla_no.toUpperCase()%>" size="8" readonly>
</span></div></td>
        <td rowspan="2" class="bordereporte">
          <div align="center" class="letras">
            <input name="placa<%=i%>" type="text" class="textbox" value="<%=placa_no.toUpperCase()%>" size="8" readonly>
          </div></td>
        <td rowspan="2" class="bordereporte">
          <div align="center" class="letras">
            <input name="pesoll<%=i%>" type="text" class="textbox" id="pesoll<%=i%>" onChange="buscarPesoSalida('<%=i%>');" onKeyPress="soloDigitos(event,'decOK')" size="12" maxlength="13">
</div></td>
        <td rowspan="2" class="bordereporte"><div align="center">
          <input name="pesov<%=i%>" type="text" class="textbox" onChange="buscarPesoSalida('<%=i%>');" onKeyPress="soloDigitos(event,'decOK')" size="12" maxlength="13">
        </div></td>
        <td rowspan="2" class="bordereporte"><div align="center">
          <input name="peson<%=i%>" type="text" class="textbox" id="peson<%=i%>2" size="12" maxlength="13" readonly>
</div></td>
        <%listTabla2 = model.tbltiempoService.getTblTiemposSalida(usuario.getBase());
			itTbla2=listTabla2.iterator();
			while(itTbla2.hasNext()){
				Tbltiempo tbl = (Tbltiempo) itTbla2.next();
				String id_tabla=tbl.getTimeCode();
				
				%>
		<td width="136" rowspan="2" class="bordereporte"><input name="<%=id_tabla%><%=i%>" type="text" class="textbox" id="<%=id_tabla%><%=i%>" onFocus="if(self.gfPop)gfPop.fFocus(document.form2.<%=id_tabla%><%=i%>,'form2');if(self.gfPop)gfPop.fPopCalendar(document.form2.<%=id_tabla%><%=i%>);return false;" size="18" >
        
	    </td>
		<%}%>
		<td rowspan="2" class="bordereporte"><div align="center">
            <input name="anticipo<%=i%>" type="text" class="textbox" onKeyPress="soloDigitos(event,'decOK')" size="12">
        </div></td>
        <td rowspan="2" class="bordereporte"><%
		 if(model.proveedoranticipoService.existProveedoresAnticipo(usuario.getDstrct())){
			List list = model.proveedoranticipoService.getProveedoresAnticipo(usuario.getDstrct());%>
          <select name="proveedora<%=i%>" class="listmenu" id="proveedora<%=i%>">
            <%
		   	Iterator it=list.iterator();
				while (it.hasNext()){
					
					Proveedor_Anticipo pa = (Proveedor_Anticipo) it.next();
					
					String  nit= pa.getNit();
					String desc=pa.getNombre()+" "+pa.getCodigo();
					
				%>
            <option value="<%=nit%>"><%=desc%> </option>
            <%
				}
			
		%>
          </select>
          <%}else{%>
          <span class="letraresaltada"><strong>No existe ning&uacute;n Proveedor Registrado</strong></span><strong>.</strong>
        <%}%></td>
        <td rowspan="2" class="bordereporte">
          <div align="right" class="letras">
            <div align="center">
              <input name="gacpm<%=i%>" type="text" class="textbox" onKeyPress="soloDigitos(event,'decOK')" size="10">
            </div>
        </div></td>
        <td rowspan="2" class="bordereporte">
          <div align="center" class="letras">
            <div align="center">
              <%
		 if(model.proveedoracpmService.existProveedoresAcpm(usuario.getDstrct())){
			List list = model.proveedoracpmService.getProveedoresACPM(usuario.getDstrct());%>
              <select name="proveedorAcpm<%=i%>" class="listmenu" id="proveedorAcpm<%=i%>">
                <%
		   	Iterator it=list.iterator();
				while (it.hasNext()){
					
					Proveedor_Acpm pa = (Proveedor_Acpm) it.next();
					
					String  nit= pa.getNit()+"/"+pa.getCodigo();
					String desc=pa.getNombre()+" "+pa.getCodigo();
					
				%>
                <option value="<%=nit%>"><%=desc%> </option>
                <%
				}
			
		%>
              </select>
              <%}else{%>
              <strong class="letraresaltada">No existe ning&uacute;n Proveedor Registrado.</strong>
              <%}%>
</div>
        </div></td>
        <td rowspan="2" class="bordereporte"><div align="center">
            <input name="peajea<%=i%>" type="text" class="textbox" onKeyPress="soloDigitos(event,'dec')" size="6"></div></td>
        <td rowspan="2" class="bordereporte"><div align="center">
            <input name="peajeb<%=i%>" type="text" class="textbox" onKeyPress="soloDigitos(event,'dec')" size="6"> 
        </div></td>
        <td rowspan="2" class="bordereporte"><div align="center">
            <input name="peajec<%=i%>" type="text" class="textbox" onKeyPress="soloDigitos(event,'dec')" size="6">
        </div></td>
        <td rowspan="2" class="bordereporte"><div align="center">
          <%
		 if(model.proveedortiquetesService.existProveedoresTiquetes(usuario.getDstrct())){
			List list = model.proveedortiquetesService.getProveedoresTIQUETES(usuario.getDstrct());%>
              <select name="tiquetes<%=i%>" class="textbox" id="tiquetes<%=i%>">
                <%
		   	Iterator it=list.iterator();
				while (it.hasNext()){
					
					Proveedor_Tiquetes pa = (Proveedor_Tiquetes) it.next();
					
					String  nit= pa.getNit();
					String desc=pa.getNombre()+" "+pa.getCodigo();
					
				%>
                <option value="<%=nit%>"><%=desc%> </option>
                <%
				}
			
		%>
            </select>
          <%}else{%>
          <strong class="letraresaltada">No existe ning&uacute;n Proveedor Registrado.</strong>
          <%}%>
        </div></td>
         <%model.anticiposService.vecAnticipos(usuario.getBase(),sj);
		Vector descuentos = model.anticiposService.getAnticipos();
		for(int k = 0 ; k<descuentos.size();k++){
			Anticipos ant = (Anticipos) descuentos.elementAt(k); 
		%>
		<td width="15" class="bordereporte"><b><%=ant.getAnticipo_desc().toUpperCase()%></b></td>
		<td width="15" class="bordereporte"><strong>PROVEEDOR</strong></td>
		<%}%>
        <td rowspan="2" class="bordereporte"><input name="Guardar22" type="image"  id="Guardar22" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" src="<%=BASEURL%>/images/botones/validar.gif" align="middle"  style="cursor:hand" height="21"></td>
	  </tr>
	  <tr>
	    <%model.anticiposService.vecAnticipos(usuario.getBase(),sj);
		descuentos = model.anticiposService.getAnticipos();
		for(int k = 0 ; k<descuentos.size();k++){
			Anticipos ant = (Anticipos) descuentos.elementAt(k); 
			String codigo = ant.getAnticipo_code();
		%>
	    <td><input name="<%=ant.getAnticipo_code()%><%=i%>" type="text" class="textbox" value="<%=request.getParameter(ant.getAnticipo_code()+""+i)%>"></td>
	    <td>
		<%model.proveedoresService.listaAnticipoProvee(codigo);
		Vector provee = model.proveedoresService.getProveedores();%>
		<select name="provee<%=codigo%><%=i%>" class="listmenu">
			<%for(int m =0; m<provee.size(); m++){
				Proveedores prov = (Proveedores)provee.elementAt(m);
			%>
		  <option value="<%=prov.getNit()%>/<%=prov.getSucursal()%>"><%=prov.getNombre()%> <%=prov.getSucursal()%></option>
		  <%}%>
	    </select></td>
		<%}%>
      </tr>
	 <% i++;
	 }%>
    </table>
<p align="center">     <input name="Guardar3" type="image"  id="Guardar3" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" src="<%=BASEURL%>/images/botones/aceptar.gif" align="middle"  style="cursor:hand" height="21">
&nbsp; <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"> </p>
	 <% }
	 %>

  </form>
<iframe width=188 height=166 name="gToday:datetime:agenda.js:gfPop:plugins_24.js" id="gToday:datetime:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng3.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</div>
</body>
</html>

