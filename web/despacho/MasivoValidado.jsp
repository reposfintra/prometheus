<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Despacho - Desapcho Masivo Validado.</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/Style.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/Style.css" rel="stylesheet" type="text/css">

<link href="<%=BASEURL%>/css/letras.css" rel="stylesheet" type="text/css">
<link href="../css/letras.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/StyleM.css" rel="stylesheet" type="text/css">
<link href="../css/StyleM.css" rel="stylesheet" type="text/css">


<script src="<%=BASEURL%>/js/validar.js">
</script>
<style type="text/css">
<!--
.Estilo1 {font-weight: bold}
-->
</style>
</head>
<%String fecpla= request.getParameter("fecdsp");
java.util.Date date = new java.util.Date();
    SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	if(fecpla == null)
		fecpla = s.format(date);   
 %>

 <%String nombres ="";

 	List listTablaN = model.tbltiempoService.getTblTiempos(request.getParameter("standard"));
	Iterator itTblaN=listTablaN.iterator();
	while (itTblaN.hasNext()){
		Tbltiempo tbl = (Tbltiempo) itTblaN.next();
		if(!tbl.getTimeCode().equals("IVIA")){
			nombres=nombres+""+tbl.getTimeCode()+",";
		}
	}
		Usuario usuario = (Usuario) session.getAttribute("Usuario");
  	String remision="";
	String placa="";
	String trailer="";
	String conductor="";
	String orden="";
	String pesolm="";
	String pesovm="";
	String pesonm="";
	String standard="";
	remision=request.getParameter("remision");
	placa=request.getParameter("placa");
	trailer=request.getParameter("trailer");
	conductor=request.getParameter("conductor");
	orden=request.getParameter("orden");
	pesolm=request.getParameter("pesolm");
	pesovm=request.getParameter("pesovm");
	pesonm=request.getParameter("pesonm");
	standard=request.getParameter("standard");
	
%>
<body topmargin="0" leftmargin="0" rightmargin="0">
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Planilla&accion=Insert&cmd=show" onSubmit="return ValidarFormularioInicio(this,'<%=nombres%>')">
    <table width="90%"  border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="ECE0D8" class="LETRAS">
      <tr bgcolor="#FFA928">
        <td colspan="6"><div align="center"><strong>INGRESO DE VIAJE VALIDO </strong></div></td>
      </tr>
      <tr bordercolor="#0066CC" bgcolor="#99CCFF">
        <td colspan="6"><strong>DATOS PRELIMINARES </strong></td>
      </tr>
      <tr>
        <td width="16%"><strong>REMISION</strong></td>
        <td bgcolor="#99cc99"><span class="letras">
          <input name="remision" type="text" id="remision" size="6" maxlength="6" onKeyPress="soloDigitos(event,'decNO')" value="<%=remision%>" readonly>
        </span></td>
        <td colspan="2"><strong>FECHA DEL DESPACHO </strong></td>
        <td colspan="2" bgcolor="#99cc99"><input name="fecdsp" type="text" id="fecdsp" value="<%=fecpla%>" readonly>        </td>
      </tr>
      <tr>
        <td><strong>PLACA</strong></td>
        <td width="15%" bgcolor="#99cc99"><div align="left"><span class="letras">
            <input name="placa" type="text" id="placa" size="8" maxlength="6" value="<%=placa.toUpperCase()%>" readonly>
            </span><br>
            <span class="style1"><a href="<%=CONTROLLER%>?estado=Menu&accion=Enviar&numero=18" target="_blank">Agregar Placa</a></span> </div></td>
        <td width="15%" rowspan="2"><strong>TRAILER</strong></td>
        <td width="12%" rowspan="2" bgcolor="#99cc99"><div align="center"><span class="Estilo6">
            <input name="trailer" type="text" id="trailer" size="8" maxlength="7" value="<%=trailer%>">
        </span></div></td>
        <td width="16%"><strong>CONDUCTOR</strong></td>
        <td bgcolor="#99cc99"><div align="left"><span class="Estilo6">
            <input name="conductor" type="text" id="conductor" size="12" maxlength="12" value="<%=conductor%>" readonly>
            <br>
            </span><span class="Estilo6"><span class="style1"><a href="<%=CONTROLLER%>?estado=Menu&accion=Conductor&conductor=<%=conductor%>" target="_blank">Agregar Conductor</a><br>
            <a class="Simulacion_Hiper" style="cursor:hand "  onClick="abrirConductor('<%=CONTROLLER%>?estado=Menu&accion=Enviar&numero=37&despacho=ok','Conductores')" >Consultar Conductor</a><br>
        </span></span></div></td>
      </tr>
      <tr>
        <td colspan="2" bgcolor="#99cc99"><%=request.getParameter("nombreProp")%>
        <input name="trafico" type="hidden" id="trafico" value="no hay"></td>
        <td colspan="2" bgcolor="#99cc99"><%=request.getParameter("nombre")%></td>
      </tr>
      <tr>
        <td><strong>ESTANDARD</strong></td>
        <td colspan="5" bgcolor="#99cc99"><span class="Estilo6">
          <%
		 if(model.stdjobdetselService.existStandardsProy(usuario.getProject())){
			List list = model.stdjobdetselService.getStandardsProy(usuario.getProject(),usuario.getBase());%>
          <select name="standard" id="standard" onChange="cambiarFormulario2('<%=BASEURL%>/despacho2/InicioDespacho.jsp?reload=ok');">
            <%if(request.getParameter("reload")==null){%>
            <option value="0">Seleccione Alguno</option>
            <%}%>
            <%
		   	Iterator it=list.iterator();
				while (it.hasNext()){
					Stdjobdetsel std = (Stdjobdetsel) it.next();
					String desc = std.getSj_desc();
					String sj=std.getSj();
					if(sj.equals(standard)){%>
            <option value="<%=sj%>" selected><%=desc%></option>
            <%}else{%>
            <option value="<%=sj%>"><%=desc%></option>
            <%}%>
            <%}%>
          </select>
          <%}else{%>
          <strong>No existe ning&uacute;n StandardJob Registrado.</strong>
          <%}%>
        </span></td>
      </tr>
      <tr>
        <td><div align="center"><strong>
          <input name="orden" type="hidden" id="orden2" size="10" maxlength="10" value="<%=orden%>">
        PESO LLENO MINA </strong></div></td>
        <td bgcolor="#99cc99"><input name="pesolm" type="text" id="pesolm2" size="10" onChange="buscarPesoN();" onKeyPress="soloDigitos(event,'decOK');FormatNumber(this,'##.##');" value="<%=pesolm%>"></td>
        <td><div align="center"><span class="letras"><strong>PESO VACIO MINA</strong></span></div></td>
        <td bgcolor="#99cc99"><div align="left"><span class="letras">
          <input name="pesovm" type="text" id="pesovm3" size="10" onChange="buscarPesoN();" onKeyPress="soloDigitos(event,'decOK');FormatNumber(this,'##.##');" value="<%=pesovm%>">
          </span>
        </div></td>
        <td><div align="right"><span class="letras"><strong>PESO NETO MINA</strong>
        </span></div></td>
        <td bgcolor="#99cc99">
          <div align="left">
            <input name="pesonm" type="text" id="pesonm2" size="10" maxlength="13" readonly value="<%=pesonm%>">
            </div>          <div align="center"><span class="letras"></span></div>          <div align="center">
              </div></td>
      </tr>
      <tr bordercolor="#0066CC" bgcolor="#99CCFF">
        <td colspan="6"><div align="center"><strong>FECHAS INICIALES </strong></div></td>
      </tr>
      <tr>
        <td colspan="6">
		<table width="100%"  border="1" cellpadding="0" cellspacing="0" bordercolor="#EBEBEB">
            <%
			List listTabla = model.tbltiempoService.getTblTiempos(standard);
			Iterator itTbla=listTabla.iterator();
			while (itTbla.hasNext()){
				Tbltiempo tbl = (Tbltiempo) itTbla.next();
				String id_tabla=tbl.getTimeCode();
				%>
            <tr>
              <td width="43%" class="Letras Estilo1"><strong><%=tbl.getTimeDescription()%></td>
              <td width="57%" bgcolor="#99cc99"><input name="<%=id_tabla%>" type="text" id="<%=id_tabla%>" onKeyPress="soloDigitos(event,'decNo');FormatNumber(this,'####-##-##');"  value="<%if(request.getParameter(id_tabla)!=null){%><%=request.getParameter(id_tabla)%><%}%>" size="13" maxlength="10" >
                <input name="h<%=id_tabla%>" type="text" id="h<%=id_tabla%>" onClick="" onKeyPress="soloDigitos(event,'decNo');FormatNumber(this,'##:##');" size="6" maxlength="5" value=<%=request.getParameter("h"+id_tabla)!=null?request.getParameter("h"+id_tabla):""%>></td>
            </tr>
            <%
		}%>
        </table></td>
      </tr>
     
     
    </table>
    <div align="center"><br>
        <input type="submit" name="Submit" value="Ingresar Datos" onClick="form1.submit();this.disabled=true;">
        <input type="reset" name="Submit2" value="Borrar Datos">
    </div>
  </div>
</form>
  <form name="form2" method="post" action="">
     <br>
     	<table width="77%"  border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="ECE0D8" class="Letras">
          <tr bgcolor="#FFA928">
            <td colspan="2"><div align="center"><strong>BUSCAR VIAJE EN TRANSITO</strong></div></td>
          </tr>
          <tr>
            <td width="35%"><span class="Estilo1">No. Remision : </span></td>
            <td width="65%"><input name="remision" type="text" id="remision">
            <input name="Submit32" type="button" class="boton_herramienta" value="Buscar por remision..." onClick="window.location='<%=CONTROLLER%>?estado=PlanillaTransito&accion=Search&remision='+form2.remision.value;"></td>
          </tr>
          <tr>
            <td><strong>              No. Placa : </strong></td>
            <td><input name="placa" type="text" id="placa">
            <input name="Submit3" type="button" class="boton_herramienta" value="Buscar por placa..." onClick="window.location='<%=CONTROLLER%>?estado=PlanillaTransito&accion=Search&placa='+form2.placa.value;"></td>
          </tr>
          <tr>
            <td colspan="2"><TABLE width='100%' align='center'>
              <TR>
                <TD ALIGN='center' class="letras"> <b>NOTA : </b> Para ver informaci&oacute;n acerca de <b><a class="Simulacion_Hiper" style="cursor:hand" onclick="window.open('<%=BASEURL%>/despacho/PlasTransito.jsp','','height=300,width=600,scrollbars=yes');">PLANILLAS EN TRANSITO</a></b>, deber&aacute; hacer Click sobre el link. </TD>
              </TR>
            </TABLE></td>
          </tr>
    </table>
</form>
</body>
</html>
