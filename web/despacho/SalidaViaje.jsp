<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Despacho - Salida de Viajes Items validos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="<%=BASEURL%>/js/validar.js"></script>
<script src="<%=BASEURL%>/js/boton.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<%

Usuario usuario = (Usuario) session.getAttribute("Usuario");
String fecpla= "";

java.util.Date date = new java.util.Date();
SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd HH:mm");
fecpla = s.format(date);  

s = new SimpleDateFormat("yyyy-MM-dd");
String fechas = s.format(date);
s = new SimpleDateFormat("HH:mm");
String horas = s.format(date);
//BUSCAMOS LOS DATOS DE LA REMISION, ADEMAS LOS ANTICIPOS QUE SE DEBEN DAR POR DEFAULT A ESTA PLACA.
//DATOS REMISION
String remision = request.getParameter("remision");
String planilla ="";
model.planillaService.bucaPlanilla(remision);
Planilla pla = model.planillaService.getPlanilla();

String placa = "";
String conductor = "";
String estandard="";
String standar="";
String nomcond="";
String nomprop="";
if(pla!=null){
	planilla = pla.getNumpla();
	placa = pla.getPlaveh();
	conductor=pla.getCedcon();
	standar = pla.getSj();
	estandard= pla.getSj_desc();
	nomcond = pla.getNomCond();
	nomprop = pla.getNomprop();
}
//EL ANTICIPO AUTOMATICO
String anticipo="";
String gacpm="";
String peajea="";
String peajeb="";
String peajec="";

model.autoAnticipoService.buscarPSj(placa,standar);
Auto_Anticipo antP= model.autoAnticipoService.getAuto_Anticipo();
if(antP!=null){
	anticipo=""+antP.getEfectivo();
	gacpm=""+antP.getacpm();
	peajea=""+antP.getTicket_a();
	peajeb=""+antP.getTicket_b();
	peajec=""+antP.getTicket_c();
}
%>
<body topmargin="0" leftmargin="0" rightmargin="0" onLoad="form1.pesoll.focus();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 1px;">
 <jsp:include page="/toptsp.jsp?encabezado=SALIDA DE VIAJE"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Salida&accion=Validar&cmd=show" onSubmit="return ValidarFormularioCompleto(this)">
<table border="2" align="center" width="92%">
  <tr>
    <td>
	<table width="100%" align="center">
  <tr>
    <td width="49%"  class="subtitulo1"><p align="left">DATOS PREELIMINARES </p></td>
    <td width="51%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
  <table width="100%" align="center" class="LETRAS">
    <tr class="fila">
      <td width="13%"><strong>REMISION</strong></td>
      <td width="15%"><span class="letras">
        <input name="remision" type="text" class="textbox" id="remision" size="12" maxlength="6" onKeyPress="soloDigitos(event,'decNO');" value="<%=remision%>" readonly>
        <input name="trafico" type="hidden" id="trafico" value="No hay">
        <input name="pla" type="hidden" id="pla" value="<%=planilla%>">
      </span></td>
      <td colspan="2"><strong>FECHA DEL CUMPLIDO </strong></td>
      <td colspan="2"><input name="fecdsp" type="text" class="textbox" id="fecdsp" onKeyPress="soloDigitos(event,'decNo');FormatNumber(this);" value="<%=fecpla%>" maxlength="16" >
      </td>
    </tr>
    <tr class="fila">
      <td><strong>PLACA</strong></td>
      <td><div align="left"><span class="letras">
          <input name="placa" type="text" class="textbox" id="placa" size="8" maxlength="7" value="<%=placa.toUpperCase()%>" readonly>
          </span><span class="Estilo6"><span class="style1"><br>
          </span></span></div>        </td>
      <td><span class="Estilo1"><%=nomprop%></span></td>
      <td><span class="Estilo6"><strong>CONDUCTOR</strong> 
      </span></td>
      <td><span class="Estilo6">
        <input name="conductor" type="text" class="textbox" id="conductor3" size="12" maxlength="12" value="<%=conductor%>" readonly>
        </span></td>
      <td><span class="Estilo6"><span class="Estilo1"><%=nomcond%></span></span></td>
    </tr>
    <tr class="fila">
      <td><strong>RUTA</strong></td>
      <td colspan="5"><span class="Estilo6"><strong>        <%
		 if(model.stdjobdetselService.existStandardsProy(usuario.getProject())){
			List list = model.stdjobdetselService.getStandardsProy(usuario.getProject(),usuario.getBase());%>
        <select name="standard" class="listmenu" id="standard">
          <%
		   	Iterator it=list.iterator();
				while (it.hasNext()){
					Stdjobdetsel std = (Stdjobdetsel) it.next();
					String desc = std.getSj_desc();
					String sj=std.getSj();
					if(sj.equals(standar)){%>
          <option value="<%=sj%>" selected><%=desc%></option>
          <%}else{%>
          <option value="<%=sj%>"><%=desc%></option>
          <%}%>
          <%}%>
        </select>
        <%}else{%>
        <strong>No existe ning&uacute;n StandardJob Registrado.</strong>
        <%}%>
</strong></span></td>
    </tr>
    <tr bordercolor="#0066CC" bgcolor="#99CCFF" class="fila">
      <td colspan="6"><div align="left"><strong>DATOS DE LA SALIDA </strong></div></td>
    </tr>
    <tr class="fila">
      <td><span class="Estilo1">PESO LLENO PUERTO </span>        <div align="center">              </div>                <div align="right"><span class="letras"></span></div>
        <div align="center">              </div>        <div align="center"><span class="letras"></span></div>        <div align="center">                    </div>        </td>
      <td><input name="pesoll" type="text" class="textbox" id="pesolm3" onKeyUp="buscarPeso();" onKeyPress="soloDigitos(event,'decOK');FormatNumber(this,'##.##');" size="10" maxlength="5" ></td>
      <td width="13%"><span class="letras"><strong>PESO VACIO PUERTO </strong></span></td>
      <td width="20%"><input name="pesov" type="text" class="textbox" id="pesovm3" onKeyUp="buscarPeso();" onKeyPress="soloDigitos(event,'decOK');FormatNumber(this,'##.##');" size="10" maxlength="5"></td>
      <td width="19%"><span class="letras"><strong>PESO NETO PUERTO </strong></span></td>
      <td width="20%"><span class="letras">
        <input name="peson" type="text" class="textbox" id="pesonm4" size="10" maxlength="13" readonly>
      </span></td>
    </tr>
    <tr bordercolor="#0066CC" bgcolor="#99CCFF" class="fila">
      <td colspan="6"><div align="left"><strong>FECHAS FINALES </strong></div></td>
    </tr>
    <tr class="fila">
      <td colspan="6">
	  <table width="100%" class="Letras">
          <%
		
		model.tbltiempoService.buscaTiemposSal(usuario.getBase());
		Vector listTabla =  model.tbltiempoService.getTiempos();
			for(int i =0; i<listTabla.size();i++){
				Tbltiempo tbl = (Tbltiempo) listTabla.elementAt(i);
				String id_tabla=tbl.getTimeCode();
				if(!tbl.getTimeCode().equals("IVIA")){
				%>
          <tr>
            <td width="38%"><strong><%=tbl.getTimeDescription()%><strong></strong></strong></td>
            <td width="62%"><input name="<%=id_tabla%>" type="text" class="textbox" id="<%=id_tabla%>"  onKeyPress="soloDigitos(event,'decNo');FormatNumber(this,'####-##-##');" size="13" maxlength="10" value="<%=fechas%>">
              <input name="h<%=id_tabla%>" type="text" class="textbox" id="h<%=id_tabla%>" onClick="" onKeyPress="soloDigitos(event,'decNo');FormatNumber(this,'##:##');" size="6" maxlength="5" <%if(tbl.getTimeCode().equals("SPUE")){%> value="<%=horas%>"<%}%>>
            </td>
          </tr>
          <%}
		   }%>
      </table>
	  </td>
    </tr>
    
   
	<tr bordercolor="#0066CC" bgcolor="#99CCFF" class="fila">
      <td colspan="6"><div align="left"><span class="Estilo1">DESCUENTOS</span></div></td>
    </tr>
	<tr class="fila">
	  <td colspan="2"><strong>EFECTIVO</strong></td>
      <td><input name="anticipo" type="text" class="textbox" size="12" id="anticipo" value="<%=anticipo%>" onKeyPress="soloDigitos(event,'decOK')"></td>
      <td><strong>Proveedor Efectivo</strong></td>
      <td colspan="2"><%
		 if(model.proveedoranticipoService.existProveedoresAnticipo(usuario.getDstrct())){
			List list = model.proveedoranticipoService.getProveedoresAnticipo(usuario.getDstrct());%>
        <select name="proveedora" class="listmenu" id="proveedora">
          <%
		   	Iterator it=list.iterator();
				while (it.hasNext()){
					
					Proveedor_Anticipo pa = (Proveedor_Anticipo) it.next();
					
					String  nit= pa.getNit()+"/"+pa.getCodigo();
					String desc=pa.getNombre()+" "+pa.getCodigo();%>
					<option value="<%=nit%>"><%=desc%> </option>
          <%	}
			
		%>
        </select>
        <%}else{%>
        <strong>No existe ning&uacute;n Proveedor Registrado.</strong>
      <%}%></td>
    </tr>
	<tr class="fila">
	  <td colspan="2"><strong>ACPM</strong></td>
	  <td><input name="gacpm" type="text" class="textbox" size="12" value="<%=gacpm%>" onKeyPress="soloDigitos(event,'decOK')"></td>
	  <td><strong>Proveedor ACPM </strong></td>
	  <td colspan="2"><%
		 if(model.proveedoracpmService.existProveedoresAcpm(usuario.getDstrct())){
			List list = model.proveedoracpmService.getProveedoresACPM(usuario.getDstrct());%>
        <select name="proveedorAcpm" class="listmenu" id="proveedorAcpm" >
          <%
		   	Iterator it=list.iterator();
				while (it.hasNext()){
					
					Proveedor_Acpm pa = (Proveedor_Acpm) it.next();
					
					String  nit= pa.getNit()+"/"+pa.getCodigo();
					String desc=pa.getNombre()+" "+pa.getCodigo();
					if(nit.equals(request.getParameter("proveedorAcpm"))){
					
				%>
          <option value="<%=nit%>" selected><%=desc%> </option>
          <%}else{%>
          <option value="<%=nit%>"><%=desc%> </option>
          <%}
				}
			
		%>
        </select>
        <%}else{%>
        <strong>No existe ning&uacute;n Proveedor Registrado.</strong>
        <%}%></td>
    </tr>
	<tr class="fila">
	  <td colspan="6"><div align="left"><strong>PEAJES</strong></div></td>
    </tr>
	<tr class="fila">
	  <td><strong>TIPO A </strong></td>
      <td><strong>TIPO B </strong></td>
      <td><strong>TIPO C </strong></td>
      <td rowspan="2"><strong>Proveedor Pejaes </strong></td>
      <td colspan="2" rowspan="2"><strong>
        <%
		 if(model.proveedortiquetesService.existProveedoresTiquetes(usuario.getDstrct())){
			List list = model.proveedortiquetesService.getProveedoresTIQUETES(usuario.getDstrct());%>
        <select name="tiquetes" class="listmenu" id="tiquetes">
          <%
		   	Iterator it=list.iterator();
				while (it.hasNext()){
					
					Proveedor_Tiquetes pa = (Proveedor_Tiquetes) it.next();
					
					String  nit= pa.getNit()+"/"+pa.getCodigo();
					String desc=pa.getNombre()+" "+pa.getCodigo();
					%>
          <option value="<%=nit%>"><%=desc%> </option>
          <%}
				%>
        </select>
        <%}else{%>
        <strong>No existe ning&uacute;n Proveedor Registrado.</strong>
        <%}%>
      </strong></td>
    </tr>
	<tr class="fila">
	  <td><span class="letras">
	    <input name="peajea" type="text" class="textbox" id="peajea2" size="6" value="<%=peajea%>" onKeyPress="soloDigitos(event,'n')">
	  </span></td>
	  <td><span class="letras">
	    <input name="peajeb" type="text" class="textbox" id="peajeb2" size="6" value="<%=peajeb%>" onKeyPress="soloDigitos(event,'n')">
	  </span></td>
	  <td><span class="letras">
	    <input name="peajec" type="text" class="textbox" id="peajec2" size="6" value="<%=peajec%>" onKeyPress="soloDigitos(event,'n')">
	  </span></td>
    </tr>
	<%model.anticiposService.vecAnticipos(usuario.getBase(),standar);
		Vector descuentos = model.anticiposService.getAnticipos();
		if(descuentos.size()>0){%>
	<tr bordercolor="#0066CC" bgcolor="#99CCFF" class="fila">
	  <td colspan="6"><div align="center"><strong>OTROS DESCUENTOS </strong></div></td>
    </tr>
	<%
		for(int k = 0 ; k<descuentos.size();k++){
			Anticipos ant = (Anticipos) descuentos.elementAt(k); 
			String codigo = ant.getAnticipo_code();
		%>
	<tr class="fila">
	  <td colspan="2"><b><%=ant.getAnticipo_desc().toUpperCase()%></b></td>
	  <td><input type="text" class="textbox" name="<%=ant.getAnticipo_code()%>"></td>
	  <td><strong>PROVEEDOR</strong></td>
	  <td colspan="2"><%model.proveedoresService.listaAnticipoProvee(codigo);
		Vector provee = model.proveedoresService.getProveedores();%>
        <select name="provee<%=codigo%>" class="listmenu">
          <%for(int m =0; m<provee.size(); m++){
				Proveedores prov = (Proveedores)provee.elementAt(m);
			%>
          <option value="<%=prov.getNit()%>/<%=prov.getSucursal()%>"><%=prov.getNombre()%> <%=prov.getSucursal()%></option>
          <%}%>
        </select></td>
    </tr>
	<%}
	}%>
  </table>
</td>
</tr>
</table>
<br>
  <center>
    <input name="Guardar" type="image"  id="Guardar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" src="<%=BASEURL%>/images/botones/aceptar.gif" align="middle"  style="cursor:hand" height="21">
&nbsp; <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgsalir"  height="21" style="cursor:hand" onClick="form1.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp; <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
  </center>
</form>
</div>
</body>
</html>
