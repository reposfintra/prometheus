<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Despacho - Salida de Viajes Items validos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="<%=BASEURL%>/js/validar.js"></script>
<script src="<%=BASEURL%>/js/boton.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">

</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 1px;">
 <jsp:include page="/toptsp.jsp?encabezado=SALIDA DE VIAJE"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Campos&accion=Validar&cmd=show" onSubmit="return ValidarFormulario(this)">
<table border="2" align="center" width="92%">
  <tr>
    <td>
	<table width="100%" align="center">
  <tr>
    <td width="49%"  class="subtitulo1"><p align="left">Datos Preeliminares </p></td>
    <td width="51%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
  <table width="100%" align="center" class="LETRAS">
    <tr class="fila">
      <td width="13%"><strong>REMISION</strong></td>
      <td><span class="letras">
        <input name="remision" type="text" class="textbox" id="remision" size="6" maxlength="6" onKeyPress="soloDigitos(event,'decNO');" value="<%=remision%>" readonly>
      </span></td>
      <td colspan="2"><strong>FECHA DEL DESPACHO </strong></td>
      <td colspan="2"><input name="fecdsp" type="text" class="textbox" id="fecdsp" value="<%=fecpla%>" readonly>
      </td>
    </tr>
    <tr class="fila">
      <td><strong>PLACA</strong></td>
      <td width="15%"><div align="left"><span class="letras">
          <input name="placa" type="text" class="textbox" id="placa" size="8" maxlength="7" value="<%=placa.toUpperCase()%>" readonly>
          </span><br>
          </div></td>
      <td width="13%"><div align="center"></div>        <strong>CONDUCTOR</strong>        <div align="left"><span class="Estilo6">          <br>
          </span><span class="Estilo6"><span class="style1"><br>
      </span></span></div></td>
      <td colspan="3"><span class="Estilo6">
        <input name="conductor" type="text" class="textbox" id="conductor2" size="12" maxlength="12" value="<%=conductor%>" readonly>
      </span></td>
    </tr>
    <tr class="fila">
      <td colspan="2">&nbsp;</td>
      <td colspan="4">&nbsp;</td>
    </tr>
    <tr class="fila">
      <td><strong>RUTA</strong></td>
      <td colspan="5"><span class="Estilo6">
        <input name="standar" type="hidden" id="standar">
</span></td>
    </tr>
    <tr class="fila">
      <td colspan="6">&nbsp;</td>
    </tr>
    <tr class="fila">
      <td>P<strong>ESO LLENO MINA </strong>        <div align="center">              </div>                <div align="right"><span class="letras"></span></div>
        <div align="center">              </div>        <div align="center"><span class="letras"></span></div>        <div align="center">                    </div>        </td>
      <td><input name="pesolm" type="text" class="textbox" id="pesolm3" size="10" onChange="buscarPesoN();" onKeyPress="soloDigitos(event,'decOK');FormatNumber(this,'##.##');"  value="<%=pesolm%>" ></td>
      <td><span class="letras"><strong>PESO VACIO MINA</strong></span></td>
      <td width="20%"><input name="pesovm" type="text" class="textbox" id="pesovm3" size="10" onChange="buscarPesoN();" onKeyPress="soloDigitos(event,'decOK');FormatNumber(this,'##.##');" value="<%=pesovm%>"></td>
      <td width="19%"><span class="letras"><strong>PESO NETO MINA</strong></span></td>
      <td width="20%"><span class="letras">
        <input name="pesonm" type="text" class="textbox" id="pesonm4" size="10" maxlength="13" readonly value="<%=pesonm%>">
      </span></td>
    </tr>
    <%if(request.getParameter("reload")!=null){%>
    <tr bordercolor="#0066CC" bgcolor="#99CCFF" class="fila">
      <td colspan="6"><div align="center"><strong>FECHAS FINALES </strong></div></td>
    </tr>
    <tr class="fila">
      <td colspan="6"><table width="100%"  border="1" cellpadding="0" cellspacing="0" bordercolor="#EBEBEB">
          <%
		
		model.tbltiempoService.searchTblTiemposEnt(standard,usuario.getBase());
		Vector listTabla =  model.tbltiempoService.getTiempos();
			for(int i =0; i<listTabla.size();i++){
				Tbltiempo tbl = (Tbltiempo) listTabla.elementAt(i);
				String id_tabla=tbl.getTimeCode();
				if(!tbl.getTimeCode().equals("IVIA")){
				%>
          <tr>
            <td width="38%"><strong><%=tbl.getTimeDescription()%><strong></strong></strong></td>
            <td width="62%"><input name="<%=id_tabla%>" type="text" class="textbox" id="<%=id_tabla%>" onClick="" onKeyPress="soloDigitos(event,'decNo');FormatNumber(this);" value="<%if(request.getParameter(id_tabla)!=null){%><%=request.getParameter(id_tabla)%><%}%>" size="18" maxlength="16">
            </td>
          </tr>
          <%}
		}%>
      </table></td>
    </tr>
    
    <%}%>
	<tr bordercolor="#0066CC" bgcolor="#99CCFF" class="fila">
      <td colspan="6"><div align="center"><span class="Estilo1">DESCUENTOS</span></div></td>
    </tr>
	<tr class="fila">
	  <td colspan="2"><strong>EFECTIVO</strong></td>
      <td><input name="anticipo" type="text" class="textbox" size="12" id="anticipo" value="<%=anticipo%>" onKeyPress="soloDigitos(event,'decOK')"></td>
      <td><strong>Proveedor Efectivo</strong></td>
      <td colspan="2"><%
		 if(model.proveedoranticipoService.existProveedoresAnticipo(usuario.getDstrct())){
			List list = model.proveedoranticipoService.getProveedoresAnticipo(usuario.getDstrct());%>
        <select name="proveedora" class="listmenu" id="proveedora">
          <%
		   	Iterator it=list.iterator();
				while (it.hasNext()){
					
					Proveedor_Anticipo pa = (Proveedor_Anticipo) it.next();
					
					String  nit= pa.getNit()+"/"+pa.getCodigo();
					String desc=pa.getNombre()+" "+pa.getCodigo();
					if(nit.equals(proveedora)){
				%>
          <option value="<%=nit%>" selected><%=desc%> </option>
          <%}else{%>
          <option value="<%=nit%>"><%=desc%> </option>
          <%}
				}
			
		%>
        </select>
        <%}else{%>
        <strong>No existe ning&uacute;n Proveedor Registrado.</strong>
        <%}%></td>
    </tr>
	<tr class="fila">
	  <td colspan="2"><strong>ACPM</strong></td>
	  <td><input name="gacpm" type="text" class="textbox" size="12" value="<%=gacpm%>" onKeyPress="soloDigitos(event,'decOK')"></td>
	  <td><strong>Proveedor ACPM </strong></td>
	  <td colspan="2"><%
		 if(model.proveedoranticipoService.existProveedoresAnticipo(usuario.getDstrct())){
			List list = model.proveedoranticipoService.getProveedoresAnticipo(usuario.getDstrct());%>
        <select name="select" class="listmenu" id="select">
          <%
		   	Iterator it=list.iterator();
				while (it.hasNext()){
					
					Proveedor_Anticipo pa = (Proveedor_Anticipo) it.next();
					
					String  nit= pa.getNit()+"/"+pa.getCodigo();
					String desc=pa.getNombre()+" "+pa.getCodigo();
					if(nit.equals(proveedora)){
				%>
          <option value="<%=nit%>" selected><%=desc%> </option>
          <%}else{%>
          <option value="<%=nit%>"><%=desc%> </option>
          <%}
				}
			
		%>
        </select>
        <%}else{%>
        <strong>No existe ning&uacute;n Proveedor Registrado.</strong>
        <%}%></td>
    </tr>
	<tr class="fila">
	  <td colspan="6"><div align="left"><strong>PEAJES</strong></div></td>
    </tr>
	<tr class="fila">
	  <td><strong>TIPO A </strong></td>
      <td><strong>TIPO B </strong></td>
      <td><strong>TIPO C </strong></td>
      <td rowspan="2"><strong>Proveedor Pejaes </strong></td>
      <td colspan="2" rowspan="2"><strong>
        <%
		 if(model.proveedortiquetesService.existProveedoresTiquetes(usuario.getDstrct())){
			List list = model.proveedortiquetesService.getProveedoresTIQUETES(usuario.getDstrct());%>
        <select name="tiquetes" class="listmenu" id="tiquetes">
          <%
		   	Iterator it=list.iterator();
				while (it.hasNext()){
					
					Proveedor_Tiquetes pa = (Proveedor_Tiquetes) it.next();
					
					String  nit= pa.getNit()+"/"+pa.getCodigo();
					String desc=pa.getNombre()+" "+pa.getCodigo();
					
				if(nit.equals(tiquetes)){
				%>
          <option value="<%=nit%>" selected><%=desc%> </option>
          <%}else{%>
          <option value="<%=nit%>"><%=desc%> </option>
          <%}
				}
			
		%>
        </select>
        <%}else{%>
        <strong>No existe ning&uacute;n Proveedor Registrado.</strong>
        <%}%>
      </strong></td>
    </tr>
	<tr class="fila">
	  <td><span class="letras">
	    <input name="peajea" type="text" class="textbox" id="peajea2" size="6" value="<%=peajea%>" onKeyPress="soloDigitos(event,'n')">
	  </span></td>
	  <td><span class="letras">
	    <input name="peajeb" type="text" class="textbox" id="peajeb2" size="6" value="<%=peajeb%>" onKeyPress="soloDigitos(event,'n')">
	  </span></td>
	  <td><span class="letras">
	    <input name="peajec" type="text" class="textbox" id="peajec2" size="6" value="<%=peajec%>" onKeyPress="soloDigitos(event,'n')">
	  </span></td>
    </tr>
	<%model.anticiposService.vecAnticipos(usuario.getBase(),standard);
		Vector descuentos = model.anticiposService.getAnticipos();
		if(descuentos.size()>0){%>
	<tr bordercolor="#0066CC" bgcolor="#99CCFF" class="fila">
	  <td colspan="6"><div align="center"><strong>OTROS DESCUENTOS </strong></div></td>
    </tr>
	<%
		for(int k = 0 ; k<descuentos.size();k++){
			Anticipos ant = (Anticipos) descuentos.elementAt(k); 
			String codigo = ant.getAnticipo_code();
		%>
	<tr class="fila">
	  <td colspan="2"><b><%=ant.getAnticipo_desc().toUpperCase()%></b></td>
	  <td><input type="text" class="textbox" name="<%=ant.getAnticipo_code()%>"></td>
	  <td><strong>PROVEEDOR</strong></td>
	  <td colspan="2"><%model.proveedoresService.listaAnticipoProvee(codigo);
		Vector provee = model.proveedoresService.getProveedores();%>
        <select name="provee<%=codigo%>" class="listmenu">
          <%for(int m =0; m<provee.size(); m++){
				Proveedores prov = (Proveedores)provee.elementAt(m);
			%>
          <option value="<%=prov.getNit()%>/<%=prov.getSucursal()%>"><%=prov.getNombre()%> <%=prov.getSucursal()%></option>
          <%}%>
        </select></td>
    </tr>
	<%}
	}%>
  </table>
</td>
</tr>
</table>
</form>
<p>
  <%
	int cant=12;
	String nombrePag="SALIDA DE VIAJES";
	cant=model.tbltiempoService.cantidadTiempos();
	int anchoTabla= (cant*164)+1535;
	cant=cant+15;
	int cantPla=model.planillaService.cantPlasIncompletas(usuario.getLogin());
	List plaAux =  model.planillaService.getPlasIncompletas(usuario.getLogin());
	int cantReg = plaAux.size();
	
	%>
</p>
<form name="form2" method="post" action="<%=CONTROLLER%>?estado=Salida&accion=Insert&cmd=show" onSubmit="return ValidarFormularioCompleto(this);">
  <%if(session.getAttribute("PlaIncompletas")!=null){%>
<table border="2" align="center" width="<%=anchoTabla%>">
  <tr>
    <td>
	<table width="100%" align="center">
  <tr>
    <td width="50%"  class="subtitulo1"><p align="left">INFORMACI&Oacute;N</p></td>
    <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>

  <table width="<%=anchoTabla%>" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4" >
    <tr >
      <td colspan="<%=cant-2%>" nowrap><%=nombrePag%></td>
    </tr>
    <tr class="tblTitulo">
	 <td width="67" height="43" nowrap><div align="center" class="letras">
          <div align="center"><strong>REMISION</strong></div>
      </div></td>
      <td width="67" height="43" nowrap><div align="center" class="letras">
          <div align="center"><strong>PLANILLA</strong></div>
      </div></td>
      <td width="48"><div align="center" class="letras">
          <div align="center"><strong>PLACA</strong></div>
      </div></td>
      <td width="35"><div align="center"><strong> PESO LLENO DESCARGUE</strong></div></td>
      <td width="35"><div align="center" class="   ">
        <div align="center"><strong>PESO VACIO DESCARGUE </strong></div>
      </div></td>
      <td width="72"><div align="center" class="   ">
        <div align="center"><strong>PESO NETO DESCARGUE </strong></div>
      </div></td>
	   <%List listTabla = model.tbltiempoService.getTblTiemposSalida(usuario.getBase());
			Iterator itTbla=listTabla.iterator();
			while (itTbla.hasNext()){
				Tbltiempo tbl = (Tbltiempo) itTbla.next();%>
      <td width="164" class="letras"><div align="center"><strong><%=tbl.getTimeDescription()%></strong></div></td>
      <%}%>
      <td width="35" class="letras"><div align="center" class="letras Estilo1">
          <div align="center">VALOR ANTICIPO</div>
      </div></td>
      <td width="35" class="letras"><strong>PROVEEDOR ANTICIPO </strong></td>
      <td width="65"><div align="center" class="letras">
          <div align="center"><strong>GLN ACPM </strong></div>
      </div></td>
      <td width="203"><div align="center" class="letras">
          <p align="center"><strong>PROVEEDOR DE ACPM</strong></p>
      </div></td>
      <td width="65"><div align="center" class="letras">
          <div align="center"><strong>TIQUETE PEAJE A </strong></div>
      </div></td>
      <td width="65"><div align="center" class="letras">
          <div align="center"><strong>TIQUETE PEAJE B</strong></div>
      </div></td>
      <td width="64" class="letras"><div align="center" class="letras"><strong>TIQUETE PEAJE C </strong></div></td>
      <td width="203" class="letras"><div align="center" class="letras"><strong>PROVEEDOR DE TIQUETES</strong></div></td>
    </tr>
    <%
	  Iterator pla=plaAux.iterator();
	  int i=0;
	  while (pla.hasNext()){
	  Plaaux planilla = (Plaaux) pla.next();
		String pla_no = planilla.getPlanilla();
		String placa_no= planilla.getPlaca();
		String remision_no = planilla.getRemision();
		String sj = planilla.getSj();		
	  %>
    <tr>
	<td rowspan="2" nowrap bgcolor="<%=request.getAttribute("planilla"+i)%>">
       
          <input name="rem<%=i%>" type="text" class="textbox" size="8" value="<%=remision_no.toUpperCase()%>" readonly>
      </td>
      <td rowspan="2" nowrap bgcolor="<%=request.getAttribute("planilla"+i)%>">
        <div align="center" class="letras">
          <input name="pla<%=i%>" type="text" class="textbox" size="8" value="<%=pla_no.toUpperCase()%>" readonly>
</div></td>
      <td rowspan="2" bgcolor="<%=request.getAttribute("placa"+i)%>">
        <div align="center" class="Estilo6">
          <input name="placa<%=i%>" type="text" class="textbox" size="8" value="<%=placa_no.toUpperCase()%>" readonly>
      </div></td>
     <td rowspan="2" bgcolor="<%=request.getAttribute("pesoLleno"+i)%>">
        <div align="center" class="">
          <input name="pesoll<%=i%>" type="text" class="textbox" id="pesoll<%=i%>" value="<%=request.getParameter("pesoll"+i)%>" size="12" onChange="buscarPesoSalida('<%=i%>');" onKeyPress="soloDigitos(event,'decOK')">
      </div></td>
      <td rowspan="2" bgcolor="<%=request.getAttribute("pesoVacio"+i)%>"><div align="center"><span class="">
      <input name="pesov<%=i%>" type="text" class="textbox" id="pesov<%=i%>" value="<%=request.getParameter("pesov"+i)%>" size="12" onChange="buscarPesoSalida('<%=i%>');" onKeyPress="soloDigitos(event,'decOK')">
</span></div></td>
      <td rowspan="2" bgcolor="<%=request.getAttribute("pesoVacio"+i)%>"><div align="center"><span class="">
          <input name="peson<%=i%>" type="text" class="textbox" id="peson<%=i%>" value="<%=request.getParameter("peson"+i)%>" size="12">
      </span></div></td>
<%List listTabla2 = model.tbltiempoService.getTblTiemposSalida(usuario.getBase());
			Iterator itTbla2=listTabla2.iterator();
			while(itTbla2.hasNext()){
				Tbltiempo tbl = (Tbltiempo) itTbla2.next();
				String id_tabla=tbl.getTimeCode();
				
				%>
      <td width="164" rowspan="2" bgcolor="<%=request.getAttribute("fecha"+i)%>"><input name="<%=id_tabla%><%=i%>" type="text" class="textbox" id="<%=id_tabla%><%=i%>" size="18"  value="<%=request.getParameter(id_tabla+i)%>" onFocus="if(self.gfPop)gfPop.fFocus(document.form1.<%=id_tabla%><%=i%>);if(self.gfPop)gfPop.fPopCalendar(document.form2.<%=id_tabla%><%=i%>);return false;">        <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form2.<%=id_tabla%><%=i%>);return false;" HIDEFOCUS> </a> </td>
      <%}%>
      <td rowspan="2" bgcolor="<%=request.getAttribute("anticipo"+i)%>"><div align="center">
          <input name="anticipo<%=i%>" type="text" class="textbox" size="12" value="<%=request.getParameter("anticipo"+i)%>" onChange="cambiarForm('<%=CONTROLLER%>?estado=Salida&accion=Validar');" onKeyPress="soloDigitos(event,'decOK')">
      </div></td>
      <td rowspan="2" bgcolor="<%=request.getAttribute("proveedora"+i)%>"><%
		 if(model.proveedoranticipoService.existProveedoresAnticipo(usuario.getDstrct())){
			List list = model.proveedoranticipoService.getProveedoresAnticipo(usuario.getDstrct());%>
        <select name="proveedora<%=i%>" class="listmenu" id="proveedora<%=i%>">
          <%
		   	Iterator it=list.iterator();
				while (it.hasNext()){
					
					Proveedor_Anticipo pa = (Proveedor_Anticipo) it.next();
					
					String  nit= pa.getNit()+"/"+pa.getCodigo();
					String desc=pa.getNombre()+" "+pa.getCodigo();
					if(nit.equals(request.getParameter("proveedora"+i))){
				%>
          <option value="<%=nit%>" selected><%=desc%> </option>
          <%}else{%>
          <option value="<%=nit%>"><%=desc%> </option>
          <%}
				}
			
		%>
        </select>
        <%}else{%>
        <strong class="letraresaltada">No existe ning&uacute;n Proveedor Registrado.</strong>
        <%}%></td>
      <td rowspan="2" bgcolor="<%=request.getAttribute("acpm"+i)%>">
        <div align="right" class="Estilo6">
          <div align="center">
            <input name="gacpm<%=i%>" type="text" class="textbox" size="10" value="<%=request.getParameter("gacpm"+i)%>" onChange="cambiarForm('<%=CONTROLLER%>?estado=Salida&accion=Validar');" onKeyPress="soloDigitos(event,'decOK')">
          </div>
      </div></td>
      <td rowspan="2" bgcolor="<%=request.getAttribute("proveedora"+i)%>">
        <div align="center" class="Estilo6">
          <div align="center">
            <%
		 if(model.proveedoracpmService.existProveedoresAcpm(usuario.getDstrct())){
			List list = model.proveedoracpmService.getProveedoresACPM(usuario.getDstrct());%>
            <select name="proveedorAcpm<%=i%>" class="listmenu" id="proveedorAcpm<%=i%>">
              <%
		   	Iterator it=list.iterator();
				while (it.hasNext()){
					
					Proveedor_Acpm pa = (Proveedor_Acpm) it.next();
					
					String  nit= pa.getNit()+"/"+pa.getCodigo();
					String desc=pa.getNombre()+" "+pa.getCodigo();
					if(nit.equals(request.getParameter("proveedorAcpm"+i))){
					
				%>
              <option value="<%=nit%>" selected><%=desc%> </option>
              <%}else{%>
			  <option value="<%=nit%>"><%=desc%> </option>
			  <%}
				}
			
		%>
            </select>
            <%}else{%>
            <strong class="letraresaltada">No existe ning&uacute;n Proveedor Registrado.</strong>
            <%}%>
</div>
      </div></td>
      <td rowspan="2" bgcolor="<%=request.getAttribute("peajea"+i)%>"><div align="center"><span class="letras"><span class="letras">
          <input name="peajea<%=i%>" type="text" class="textbox" size="6" value="<%=request.getParameter("peajea"+i)%>" onChange="cambiarForm('<%=CONTROLLER%>?estado=Salida&accion=Validar');" onKeyPress="soloDigitos(event,'decNO')">
      </span></span></div></td>
      <td rowspan="2" bgcolor="<%=request.getAttribute("peajeb"+i)%>"><div align="center"><span class="letras"><span class="letras">
          <input name="peajeb<%=i%>" type="text" class="textbox" size="6" value="<%=request.getParameter("peajeb"+i)%>" onChange="cambiarForm('<%=CONTROLLER%>?estado=Salida&accion=Validar');" onKeyPress="soloDigitos(event,'decNO')">
      </span></span></div></td>
      <td rowspan="2" bgcolor="<%=request.getAttribute("peajec"+i)%>"><div align="center"><span class="letras"><span class="letras">
          <input name="peajec<%=i%>" type="text" class="textbox" size="6" value="<%=request.getParameter("peajec"+i)%>" onChange="cambiarForm('<%=CONTROLLER%>?estado=Salida&accion=Validar');" onKeyPress="soloDigitos(event,'decNO')">
      </span></span></div></td>
      <td rowspan="2" bgcolor="<%=request.getAttribute("proveedora"+i)%>"><div align="center">
          <%
		 if(model.proveedortiquetesService.existProveedoresTiquetes(usuario.getDstrct())){
			List list = model.proveedortiquetesService.getProveedoresTIQUETES(usuario.getDstrct());%>
          <select name="tiquetes<%=i%>" class="listmenu" id="tiquetes<%=i%>" onChange="cambiarForm('<%=CONTROLLER%>?estado=Salida&accion=Validar&num=<%=i%>');">
            <%
		   	Iterator it=list.iterator();
				while (it.hasNext()){
					
					Proveedor_Tiquetes pa = (Proveedor_Tiquetes) it.next();
					
					String  nit= pa.getNit()+"/"+pa.getCodigo();
					String desc=pa.getNombre()+" "+pa.getCodigo();
					if(nit.equals(request.getParameter("tiquetes"+i))){
					
				%>
              <option value="<%=nit%>" selected><%=desc%> </option>
              <%}else{%>
			  <option value="<%=nit%>"><%=desc%> </option>
			  <%}
				%>
				
           
            <%
				}
			
		%>
          </select>
          <%}else{%>
          <strong class="letraresaltada">No existe ning&uacute;n Proveedor Registrado.</strong>
          <%}%>
      </div></td>
      
     <%model.anticiposService.vecAnticipos(usuario.getBase(),sj);
		Vector descuentos = model.anticiposService.getAnticipos();
		for(int k = 0 ; k<descuentos.size();k++){
			Anticipos ant = (Anticipos) descuentos.elementAt(k); 
		%>
		<td width="15" ><b class="tblTitulo"><%=ant.getAnticipo_desc().toUpperCase()%></b></td>
		<td width="15" ><strong class="tblTitulo">PROVEEDOR</strong></td>
		<%}%>
      <td rowspan="2">        <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="validarSalidaNo(<%=i%>);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
    </tr>
    <tr>
       <%model.anticiposService.vecAnticipos(usuario.getBase(),sj);
		descuentos = model.anticiposService.getAnticipos();
		for(int k = 0 ; k<descuentos.size();k++){
			Anticipos ant = (Anticipos) descuentos.elementAt(k); 
			String codigo = ant.getAnticipo_code();
		%>
	    <td><input type="text" class="textbox" name="<%=ant.getAnticipo_code()%><%=i%>" value="<%=request.getParameter(ant.getAnticipo_code()+""+i)%>"></td>
		<td>
		<%model.proveedoresService.listaAnticipoProvee(codigo);
		Vector provee = model.proveedoresService.getProveedores();%>
		<select name="provee<%=codigo%><%=i%>" class="listmenu">
			<%for(int m =0; m<provee.size(); m++){
				Proveedores prov = (Proveedores)provee.elementAt(m);
				String nit =prov.getNit()+"/"+prov.getSucursal();
				if(nit.equals(request.getParameter("provee"+codigo+""+i))){
			%>
		  <option value="<%=prov.getNit()%>/<%=prov.getSucursal()%>" selected><%=prov.getNombre()%> <%=prov.getSucursal()%></option>
		  <%}else{%>
		  <option value="<%=prov.getNit()%>/<%=prov.getSucursal()%>"><%=prov.getNombre()%> <%=prov.getSucursal()%></option>
		  <%}
		  }%>
	    </select></td>
		<%}%>
    </tr>
    <% i++;
	 }%>
  </table>
</td>
</tr>
</table>

  <p align="center">
  <%if(cantReg>0){%>

     <input type="submit" name="Submit32" value="Registrar todos los items">
<%}%>  
  </p>
  <%}%>
</form>
  <iframe width=188 height=166 name="gToday:datetime:agenda.js:gfPop:plugins_24.js" id="gToday:datetime:agenda.js:gfPop:plugins_24.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
</div>
</body>
</html>
