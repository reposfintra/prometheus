<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Despacho - Salida de Viajes Items validos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="<%=BASEURL%>/js/validar.js"></script>
<script src="<%=BASEURL%>/js/boton.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.Estilo1 {font-weight: bold}
-->
</style>
</head>
<%

Usuario usuario = (Usuario) session.getAttribute("Usuario");
String fecpla= "";

java.util.Date date = new java.util.Date();
SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd HH:mm");
fecpla = s.format(date);  
s = new SimpleDateFormat("yyyy-MM-dd");
String fechas = s.format(date);
s = new SimpleDateFormat("HH:mm");
String horas = s.format(date);
//BUSCAMOS LOS DATOS DE LA REMISION, ADEMAS LOS ANTICIPOS QUE SE DEBEN DAR POR DEFAULT A ESTA PLACA.
//DATOS REMISION
String remision = request.getParameter("remision");
model.planillaService.bucaPlanilla(remision);
Planilla pla = model.planillaService.getPlanilla();

String placa = "";
String conductor = "";
String estandard="";
String standar="";
String nomcond="";
String nomprop="";
String planilla ="";

if(pla!=null){
	placa = pla.getPlaveh();
	conductor=pla.getCedcon();
	standar = pla.getSj();
	estandard= pla.getSj_desc();
	nomcond = pla.getNomCond();
	nomprop = pla.getNomprop();
	planilla = pla.getNumpla();
}
//EL ANTICIPO AUTOMATICO
String anticipo=request.getParameter("anticipo");
String gacpm=request.getParameter("gacpm");
String peajea=request.getParameter("peajea");
String peajeb=request.getParameter("peajeb");
String peajec=request.getParameter("peajec");
String pesoll =request.getParameter("pesoll");
String pesov = request.getParameter("pesov");
String peson = request.getParameter("peson");



%>
<body topmargin="0" leftmargin="0" rightmargin="0" onLoad="form1.pesoll.focus();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 1px;">
 <jsp:include page="/toptsp.jsp?encabezado=Error en la salida"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Salida&accion=Validar&cmd=show" onSubmit="return ValidarFormularioCompleto(this)">
<table border="2" align="center" width="92%">
  <tr>
    <td>
	<table width="100%" align="center">
  <tr>
    <td width="49%"  class="subtitulo1"><p align="left">Datos Preeliminares </p></td>
    <td width="51%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
  <table width="100%" align="center" class="LETRAS">
    <tr class="fila">
      <td width="13%"><strong>REMISION</strong></td>
      <td width="15%"><span class="letras">
        <input name="remision" type="text" class="textbox" id="remision" onKeyPress="soloDigitos(event,'decNO');" value="<%=remision%>" size="12" maxlength="6" readonly>
        <input name="trafico" type="hidden" id="trafico" value="No hay">
        <input name="pla" type="hidden" id="pla" value="<%=planilla%>">
      </span></td>
      <td colspan="2"><strong>FECHA DEL CUMPLIDO </strong></td>
      <td colspan="2"><input name="fecdsp" type="text" class="textbox" id="fecdsp" onKeyPress="soloDigitos(event,'decNo');FormatNumber(this);" value="<%=request.getParameter("fecdsp")%>" maxlength="16" ></td>
    </tr>
    <tr class="fila">
      <td><strong>PLACA</strong></td>
      <td><div align="left"><span class="letras">
          <input name="placa" type="text" class="textbox" id="placa" value="<%=placa.toUpperCase()%>" size="8" maxlength="7" readonly>
          </span><span class="Estilo6"><span class="style1"><br>
      </span></span></div>        </td>
      <td><span class="Estilo1"><%=nomprop%></span></td>
      <td><span class="Estilo6"><strong>CONDUCTOR</strong> 
      </span></td>
      <td><span class="Estilo6">
        <input name="conductor" type="text" class="textbox" id="conductor3" value="<%=conductor%>" size="12" maxlength="12" readonly>
      </span></td>
      <td><span class="Estilo6"><span class="Estilo1"><%=nomcond%></span></span></td>
    </tr>
    <tr class="fila">
      <td><strong>RUTA</strong></td>
      <td colspan="5"><span class="Estilo6"><strong>        
        <%
		 if(model.stdjobdetselService.existStandardsProy(usuario.getProject())){
			List list = model.stdjobdetselService.getStandardsProy(usuario.getProject(),usuario.getBase());%>
        <select name="standard" id="standard" >
          
          <%
		   	Iterator it=list.iterator();
				while (it.hasNext()){
					Stdjobdetsel std = (Stdjobdetsel) it.next();
					String desc = std.getSj_desc();
					String sj=std.getSj();
					if(sj.equals(request.getParameter("standard"))){%>
          <option value="<%=sj%>" selected><%=desc%></option>
          <%}else{%>
          <option value="<%=sj%>"><%=desc%></option>
          <%}%>
          <%}%>
        </select>
        <%}else{%>
        <strong>No existe ning&uacute;n StandardJob Registrado.</strong>
        <%}%>
</strong></span></td>
    </tr>
    <tr bordercolor="#0066CC" class="fila">
      <td colspan="6"><div align="left"><strong>DATOS DE LA SALIDA </strong></div></td>
    </tr>
    <tr class="fila">
      <td><span class="Estilo1">PESO LLENO PUERTO</span>            </td>
      <td><input name="pesoll" type="text" class="textbox" id="pesolm3" onChange="buscarPeso();" onKeyPress="soloDigitos(event,'decOK');FormatNumber(this,'##.##');" value="<%=pesoll%>" size="10" maxlength="5" ></td>
      <td width="13%"><span class="letras"><strong>PESO VACIO PUERTO</strong></span></td>
      <td width="20%"><input name="pesov" type="text" class="textbox" id="pesovm3" onChange="buscarPeso();" onKeyPress="soloDigitos(event,'decOK');FormatNumber(this,'##.##');" value="<%=pesov%>" size="10" maxlength="5"></td>
      <td width="19%"><span class="letras"><strong>PESO NETO PUERTO</strong></span></td>
      <td width="20%"><span class="letras">
        <input name="peson" type="text" class="textbox" id="pesonm4" value="<%=peson%>" size="10" maxlength="13" readonly>
      </span></td>
    </tr>
    <tr bordercolor="#0066CC" class="fila">
      <td colspan="6"><div align="left"><strong>FECHAS FINALES </strong></div></td>
    </tr>
    <tr class="fila">
      <td colspan="6">
	  <table width="100%" class="Letras">
          <%
		
		model.tbltiempoService.buscaTiemposSal(usuario.getBase());
		Vector listTabla =  model.tbltiempoService.getTiempos();
			for(int i =0; i<listTabla.size();i++){
				Tbltiempo tbl = (Tbltiempo) listTabla.elementAt(i);
				String id_tabla=tbl.getTimeCode();
				%>
          <tr>
            <td width="38%"><strong><%=tbl.getTimeDescription()%><strong></strong></strong></td>
            <td width="62%" bgcolor="<%=request.getAttribute(tbl.getTimeCode())%>"><input name="<%=id_tabla%>" type="text" class="textbox" id="<%=id_tabla%>" onClick="" onKeyPress="soloDigitos(event,'decNo');FormatNumber(this,'####-##-##');" value="<%=request.getParameter(id_tabla)%>" size="13" maxlength="10">
              <input name="h<%=id_tabla%>" type="text" class="textbox" id="h<%=id_tabla%>" onClick="" onKeyPress="soloDigitos(event,'decNo');FormatNumber(this,'##:##');" value="<%=request.getParameter("h"+id_tabla)%>" size="6" maxlength="5">
            </td>
          </tr>
          <%
		   }%>
      </table>
	  </td>
    </tr>
    
   
	<tr bordercolor="#0066CC" class="fila">
      <td colspan="6"><div align="left"><span class="Estilo1">DESCUENTOS</span></div></td>
    </tr>
	<tr class="fila">
	  <td colspan="2"><strong>EFECTIVO</strong></td>
      <td><input name="anticipo" type="text" class="textbox" id="anticipo" onKeyPress="soloDigitos(event,'decOK')" value="<%=anticipo%>" size="12"></td>
      <td><strong>Proveedor Efectivo</strong></td>
      <td colspan="2"><%
		 if(model.proveedoranticipoService.existProveedoresAnticipo(usuario.getDstrct())){
			List list = model.proveedoranticipoService.getProveedoresAnticipo(usuario.getDstrct());%>
        <select name="proveedora" class="listmenu" id="select3" >
          <%
		   	Iterator it=list.iterator();
				while (it.hasNext()){
					
					Proveedor_Anticipo pa = (Proveedor_Anticipo) it.next();
					
					String  nit= pa.getNit()+"/"+pa.getCodigo();
					String desc=pa.getNombre()+" "+pa.getCodigo();
					if(nit.equals(request.getParameter("proveedora"))){
				%>
          <option value="<%=nit%>" selected><%=desc%> </option>
          <%}else{%>
          <option value="<%=nit%>"><%=desc%> </option>
          <%}
				}
			
		%>
        </select>
        <%}else{%>
        <strong>No existe ning&uacute;n Proveedor Registrado.</strong>
        <%}%></td>
    </tr>
	<tr class="fila">
	  <td colspan="2"><strong>ACPM</strong></td>
	  <td><input name="gacpm" type="text" class="textbox" onKeyPress="soloDigitos(event,'decOK')" value="<%=gacpm%>" size="12"></td>
	  <td><strong>Proveedor ACPM </strong></td>
	  <td colspan="2"><%
		 if(model.proveedoracpmService.existProveedoresAcpm(usuario.getDstrct())){
			List list = model.proveedoracpmService.getProveedoresACPM(usuario.getDstrct());%>
        <select name="proveedorAcpm" class="listmenu" id="select4">
          <%
		   	Iterator it=list.iterator();
				while (it.hasNext()){
					
					Proveedor_Acpm pa = (Proveedor_Acpm) it.next();
					
					String  nit= pa.getNit()+"/"+pa.getCodigo();
					String desc=pa.getNombre()+" "+pa.getCodigo();
					if(nit.equals(request.getParameter("proveedorAcpm"))){
					
				%>
          <option value="<%=nit%>" selected><%=desc%> </option>
          <%}else{%>
          <option value="<%=nit%>"><%=desc%> </option>
          <%}
				}
			
		%>
        </select>
        <%}else{%>
        <strong>No existe ning&uacute;n Proveedor Registrado.</strong>
        <%}%></td>
    </tr>
	<tr class="fila">
	  <td colspan="6"><div align="left"><strong>PEAJES</strong></div></td>
    </tr>
	<tr class="fila">
	  <td><strong>TIPO A </strong></td>
      <td><strong>TIPO B </strong></td>
      <td><strong>TIPO C </strong></td>
      <td rowspan="2"><strong>Proveedor Pejaes </strong></td>
      <td colspan="2" rowspan="2"><strong>
        <%
		 if(model.proveedortiquetesService.existProveedoresTiquetes(usuario.getDstrct())){
			List list = model.proveedortiquetesService.getProveedoresTIQUETES(usuario.getDstrct());%>
        <select name="tiquetes" class="listmenu" id="select5" >
          <%
		   	Iterator it=list.iterator();
				while (it.hasNext()){
					
					Proveedor_Tiquetes pa = (Proveedor_Tiquetes) it.next();
					
					String  nit= pa.getNit()+"/"+pa.getCodigo();
					String desc=pa.getNombre()+" "+pa.getCodigo();
					if(nit.equals(request.getParameter("tiquetes"))){
					
				%>
          <option value="<%=nit%>" selected><%=desc%> </option>
          <%}else{%>
          <option value="<%=nit%>"><%=desc%> </option>
          <%}
				%>
          <%
				}
			
		%>
        </select>
        <%}else{%>
        <strong>No existe ning&uacute;n Proveedor Registrado.</strong>
        <%}%>
      </strong></td>
    </tr>
	<tr class="fila">
	  <td><span class="letras">
	    <input name="peajea" type="text" class="textbox" id="peajea2" onKeyPress="soloDigitos(event,'n')" value="<%=peajea%>" size="6">
	  </span></td>
	  <td><span class="letras">
	    <input name="peajeb" type="text" class="textbox" id="peajeb2" onKeyPress="soloDigitos(event,'n')" value="<%=peajeb%>" size="6">
	  </span></td>
	  <td><span class="letras">
	    <input name="peajec" type="text" class="textbox" id="peajec2" onKeyPress="soloDigitos(event,'n')" value="<%=peajec%>" size="6">
	  </span></td>
    </tr>
	<%model.anticiposService.vecAnticipos(usuario.getBase(),standar);
		Vector descuentos = model.anticiposService.getAnticipos();
		if(descuentos.size()>0){%>
	<tr bordercolor="#0066CC" class="fila">
	  <td colspan="6"><div align="center"><strong>OTROS DESCUENTOS </strong></div></td>
    </tr>
	<%
		for(int k = 0 ; k<descuentos.size();k++){
			Anticipos ant = (Anticipos) descuentos.elementAt(k); 
			String codigo = ant.getAnticipo_code();
		%>
	<tr class="fila">
	  <td colspan="2"><b><%=ant.getAnticipo_desc().toUpperCase()%></b></td>
	  <td><input name="<%=ant.getAnticipo_code()%>" type="text" class="textbox" value="<%=request.getParameter(ant.getAnticipo_code())%>"></td>
	  <td><strong>PROVEEDOR</strong></td>
	  <td colspan="2"><%model.proveedoresService.listaAnticipoProvee(codigo);
		Vector provee = model.proveedoresService.getProveedores();%>
        <select name="provee<%=codigo%>" class="listmenu">
          <%for(int m =0; m<provee.size(); m++){
				Proveedores prov = (Proveedores)provee.elementAt(m);
			%>
          <option value="<%=prov.getNit()%>/<%=prov.getSucursal()%>"><%=prov.getNombre()%> <%=prov.getSucursal()%></option>
          <%}%>
        </select></td>
    </tr>
	<%}
	}%>
  </table>
</td>
</tr>
</table>
  <div align="center"><br>
    <input name="Guardar" type="image"  id="Guardar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" src="<%=BASEURL%>/images/botones/aceptar.gif" align="middle"  style="cursor:hand" height="21">
&nbsp; <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgsalir"  height="21" style="cursor:hand" onClick="form1.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp; <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"> </div>
</form>
</div>
</body>
</html>
