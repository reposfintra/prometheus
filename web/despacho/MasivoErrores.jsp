<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Despacho - Inicio Despacho Masivo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="<%=BASEURL%>/js/validar.js"></script>
<script src="<%=BASEURL%>/js/boton.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">

</head>
<%String fecpla= request.getParameter("fecdsp");
java.util.Date date = new java.util.Date();
    SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	if(fecpla == null)
		fecpla = s.format(date);   
 %>

 <%String nombres ="";

 	List listTablaN = model.tbltiempoService.getTblTiempos(request.getParameter("standard"));
	Iterator itTblaN=listTablaN.iterator();
	while (itTblaN.hasNext()){
		Tbltiempo tbl = (Tbltiempo) itTblaN.next();
		if(!tbl.getTimeCode().equals("IVIA")){
			nombres=nombres+""+tbl.getTimeCode()+",";
		}
	}
%>
<%
  	Usuario usuario = (Usuario) session.getAttribute("Usuario");
  	String remision="";
	String placa="";
	String trailer="";
	String conductor="";
	String orden="";
	String pesolm="";
	String pesovm="";
	String pesonm="";
	String anticipo="";
	String gacpm="";
	String standard="";
	String pesoll="";
	String pesov="";
	String peson="";
	String peajea="";
	String peajeb="";
	String peajec="";
	String proveedora="";
	String proveedorAcpm="";
	String tiquetes="";
	remision=request.getParameter("remision");
	placa=request.getParameter("placa");
	trailer=request.getParameter("trailer");
	conductor=request.getParameter("conductor");
	orden=request.getParameter("orden");
	pesolm=request.getParameter("pesolm");
	pesovm=request.getParameter("pesovm");
	pesonm=request.getParameter("pesonm");
	standard=request.getParameter("standard");
	
  	
  %>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 1px;">
 <jsp:include page="/toptsp.jsp?encabezado=ERROR INGRESO DE VIAJE"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Campos&accion=Validar&inicio=show" onSubmit="return ValidarFormularioInicio(this,'<%=nombres%>')">
<table border="2" align="center" width="664">
  <tr>
    <td>
	<table width="100%" align="center">
  <tr>
    <td width="49%"  class="subtitulo1"><p align="left">Datos Preeliminares</p></td>
    <td width="51%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
  <table width="100%" align="center" class="LETRAS">
      <tr class="fila">
        <td width="16%"><strong>REMISION</strong></td>
        <td bgcolor="<%=request.getAttribute("remision")%>"><span class="letras">
          <input name="remision" type="text" class="textbox" id="remision" onKeyPress="soloDigitos(event,'decNO')" value="<%=remision%>" size="6" maxlength="6">
        </span></td>
        <td colspan="2"><strong>FECHA DEL DESPACHO </strong></td>
        <td colspan="2" bgcolor="#99cc99"><input name="fecdsp" type="text" class="textbox" id="fecdsp" value="<%=fecpla%>" readonly>        </td>
      </tr>
      <tr class="fila">
        <td><strong>PLACA</strong></td>
        <td width="14%" bgcolor="<%=request.getAttribute("placa")%>"><div align="left"><span class="letras">
            <input name="placa" type="text" class="textbox" id="placa" value="<%=placa.toUpperCase()%>" size="8" maxlength="6" >
            </span><br>
            <span class="style1"><a href="<%=CONTROLLER%>?estado=Menu&accion=Enviar&numero=18&placa=<%=placa.toUpperCase()%>&conductor=<%=conductor%>" target="_blank">Agregar Placa</a></span> </div></td>
        <td width="14%" rowspan="2"><strong>TRAILER</strong></td>
        <td width="12%" rowspan="2" bgcolor="#99cc99"><div align="center"><span class="Estilo6">
            <input name="trailer" type="text" class="textbox" id="trailer" value="<%=trailer%>" size="8" maxlength="7">
        </span></div></td>
        <td width="16%"><strong>CONDUCTOR</strong></td>
        <td width="28%" bgcolor="<%=request.getAttribute("conductor")%>"><div align="left"><span class="Estilo6">
            <input name="conductor" type="text" class="textbox" id="conductor" value="<%=conductor%>" size="12" maxlength="12">
            <br>
            </span><span class="Estilo6"><span class="style1"><a href="<%=CONTROLLER%>?estado=Menu&accion=Conductor&conductor=<%=conductor%>" target="_blank">Agregar Conductor</a><br>
            <a class="Simulacion_Hiper" style="cursor:hand "  onClick="abrirConductor('<%=CONTROLLER%>?estado=Menu&accion=Enviar&numero=37&despacho=ok','Conductores')" >Consultar Conductor</a><br>
        </span></span></div></td>
      </tr>
      <tr class="fila">
        <td colspan="2" bgcolor="<%=request.getAttribute("placa")%>"><%=request.getParameter("nombreProp")%></td>
        <td colspan="2" bgcolor="<%=request.getAttribute("conductor")%>"><%=request.getParameter("nombre")%></td>
      </tr>
      <tr class="fila">
        <td><strong>ESTANDARD</strong></td>
        <td colspan="5" bgcolor="#99cc99"><span class="Estilo6">
          <%
		 if(model.stdjobdetselService.existStandardsProy(usuario.getProject())){
			List list = model.stdjobdetselService.getStandardsProy(usuario.getProject(),usuario.getBase());%>
          <select name="standard" class="listmenu" id="standard" onChange="cambiarFormulario2('<%=BASEURL%>/despacho2/InicioDespacho.jsp?reload=ok');">
            <%if(request.getParameter("reload")==null){%>
            <option value="0">Seleccione Alguno</option>
            <%}%>
            <%
		   	Iterator it=list.iterator();
				while (it.hasNext()){
					Stdjobdetsel std = (Stdjobdetsel) it.next();
					String desc = std.getSj_desc();
					String sj=std.getSj();
					if(sj.equals(standard)){%>
            <option value="<%=sj%>" selected><%=desc%></option>
            <%}else{%>
            <option value="<%=sj%>"><%=desc%></option>
            <%}%>
            <%}%>
          </select>
          <%}else{%>
          <strong>No existe ning&uacute;n StandardJob Registrado.</strong>
          <%}%>
        </span></td>
      </tr>
      <tr class="fila">
        <td >
          <div align="center">
          <input name="orden" type="hidden" id="orden2" size="10" maxlength="10" value="<%=orden%>">
          <span class="Estilo1">PESO LLENO MINA </span>         </div></td>
        <td ><input name="pesolm" type="text" class="textbox" id="pesolm3" onChange="buscarPesoN();" onKeyPress="soloDigitos(event,'decOK');FormatNumber(this,'##.##');" value="<%=pesolm%>" size="10"></td>
        <td ><span class="letras"><strong>PESO VACIO MINA</strong></span></td>
        <td><input name="pesovm" type="text" class="textbox" id="pesovm3" onChange="buscarPesoN();" onKeyPress="soloDigitos(event,'decOK');FormatNumber(this,'##.##');" value="<%=pesovm%>" size="10"></td>
        <td ><span class="letras"><strong>PESO NETO MINA</strong></span></td>
        <td ><input name="pesonm" type="text" class="textbox" id="pesonm6" value="<%=pesonm%>" size="10" maxlength="13" readonly>          
        <span class="letras"></span></td>
      </tr>
      
      <tr bordercolor="#0066CC" bgcolor="#99CCFF" class="fila">
        <td colspan="6"><div align="center"><strong>FECHAS INICIALES </strong></div></td>
      </tr>
      <tr class="fila">
        <td colspan="6"><table width="100%">
            <%List listTabla = model.tbltiempoService.getTblTiempos(standard);
			Iterator itTbla=listTabla.iterator();
			while (itTbla.hasNext()){
				Tbltiempo tbl = (Tbltiempo) itTbla.next();
				String id_tabla=tbl.getTimeCode();
				%>
            <tr>
              <td width="38%" class="Letras"><strong><%=tbl.getTimeDescription()%></strong></td>
              <td width="62%" bgcolor="<%=(String)request.getAttribute("error"+tbl.getTimeCode())%>"><input name="<%=id_tabla%>" type="text" class="textbox" id="<%=id_tabla%>" onKeyPress="soloDigitos(event,'decNo');FormatNumber(this,'####-##-##');"  value="<%if(request.getParameter(id_tabla)!=null){%><%=request.getParameter(id_tabla)%><%}%>" size="13" maxlength="10"  >
                <input name="h<%=id_tabla%>" type="text" class="textbox" id="h<%=id_tabla%>" onClick="" onKeyPress="soloDigitos(event,'decNo');FormatNumber(this,'##:##');" value=<%=request.getParameter("h"+id_tabla)!=null?request.getParameter("h"+id_tabla):""%> size="6" maxlength="5"></td>
            </tr>
            <%
		}%>
        </table></td>
      </tr>
  </table>
</td>
</tr>
</table>
  <center>
	<br>
        <input name="Guardar" type="image"  id="Guardar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" src="<%=BASEURL%>/images/botones/aceptar.gif" align="middle"  style="cursor:hand" height="21">
&nbsp; <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgsalir"  height="21" align="absmiddle" style="cursor:hand" onClick="form1.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp; <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" align="absmiddle" style="cursor:hand" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
    </center>
</form>
    <form name="form2" method="post" action="">
     <br>
<table border="2" align="center" width="488">
  <tr>
    <td width="577">
	<table width="100%" align="center">
  <tr>
    <td width="54%"  class="subtitulo1"><p align="left">BUSCAR VIAJE EN TRANSITO</p></td>
    <td width="46%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
     	<table width="100%" align="center" class="Letras">
          <tr class="fila">
            <td width="35%"><span class="Estilo1">No. Remision : </span></td>
            <td width="65%"><input name="remision" type="text" class="textbox" id="remision"><img src="<%=BASEURL%>/images/botones/buscar.gif"  name="imgsalir"  height="21" align="absmiddle" style="cursor:hand" onClick="window.location='<%=CONTROLLER%>?estado=PlanillaTransito&accion=Search&remision='+form2.remision.value;" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">            </td>
          </tr>
          <tr class="fila">
            <td><strong>              No. Placa : </strong></td>
            <td><input name="placa" type="text" class="textbox" id="placa">
            <img src="<%=BASEURL%>/images/botones/buscar.gif"  name="imgsalir"  height="21" align="absmiddle" style="cursor:hand" onClick="window.location='<%=CONTROLLER%>?estado=PlanillaTransito&accion=Search&placa='+form2.placa.value;" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
          </tr>
          <tr class="fila">
            <td colspan="2"><TABLE width='100%' align='center'>
              <TR>
                <TD ALIGN='center' class="letras"> <b>NOTA : </b> Para ver informaci&oacute;n acerca de <b><a class="Simulacion_Hiper" style="cursor:hand" onclick="window.open('<%=BASEURL%>/despacho/PlasTransito.jsp','','height=300,width=600,scrollbars=yes');">PLANILLAS EN TRANSITO</a></b>, deber&aacute; hacer Click sobre el link. </TD>
              </TR>
            </TABLE></td>
          </tr>
      </table>
</td>
</tr>
</table>
</form>
</div>
</body>
</html>
