<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Despacho - Inicio Despacho Masivo Valido</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="<%=BASEURL%>/js/validar.js"></script>
<script src="<%=BASEURL%>/js/boton.js"></script>
<script src="<%=BASEURL%>/js/reporte.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">

</head>
<%
Usuario usuario = (Usuario) session.getAttribute("Usuario");
Vector pla = model.planillaService.getPlasIncompletas(usuario.getBase());
%>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 1px;">
 <jsp:include page="/toptsp.jsp?encabezado=Viajes en Transito"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="form2" method="post" action="">
     <br>
<table border="2" align="center" width="100%">
  <tr>
    <td>
	<table width="100%" align="center">
  <tr>
    <td width="49%"  class="subtitulo1"><p align="left">Viajes en Transito </p></td>
    <td width="51%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
     	<table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4" class="comentario">
          <tr class="tblTitulo">
            <td width="15%" height="22"><span class="Estilo1">REMISION</span></td>
            <td width="20%"><span class="Estilo1">PLACA</span></td>
            <td width="40%"><span class="Estilo1">CONDUCTOR</span></td>
            <td width="25%"><span class="Estilo1">STANDARD JOB (RUTA)</span></td>
          </tr>
		  <%for(int i=0 ; i<pla.size(); i++){
		  		Plaaux plaaux = (Plaaux) pla.elementAt(i);
			%>
          <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand">
            <td class="bordereporte"><%=plaaux.getRemision()%></td>
            <td class="bordereporte"><%=plaaux.getPlaca()%></td>
            <td class="bordereporte"><%=plaaux.getConductor()%></td>
            <td class="bordereporte"><%=plaaux.getSj()%></td>
          </tr>
		  <%}%>
  </table>
</td>
</tr>
</table>
</form>
</div>
</body>
</html>
