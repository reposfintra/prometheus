<%@ page session="true"%>
<%@ page errorPage="/error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="js/validar.js">
</script>

<link href="<%=BASEURL%>/css/estilo.css" rel="stylesheet" type="text/css">
<link href="../css/estilo.css" rel="stylesheet" type="text/css">

</head>

<body onLoad="<%if(request.getParameter("mensaje")!=null){%>alert('<%=request.getParameter("mensaje")%>')<%}%>">
<form name="form2" method="post" action="<%=CONTROLLER%>?estado=PlanillaAnticipo&accion=Search">
  <table width="530" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="ECE0D8" class="letra">
    <tr>
      <td colspan="2" nowrap><div align="center" class="titulo"><strong><strong>BUSQUE LA PLANILLA </strong></strong></div></td>
    </tr>
    <tr class="fila">
      <td width="194" nowrap><strong class="Estilo6">Numero de la Planilla:</strong></td>
      <td width="320" nowrap ><input name="planilla" type="text" id="planilla" maxlength="10">      
      <input type="submit" name="Submit" value="Buscar.."></td>
    </tr>
  </table>
</form>
<%//Inicializo variables
String estandar="", descripcion="", fecha="", placa="", trailer="",cedula="", nombre="", numpla="", moneda="", reant="";
float saldo=0,valorpla=0,totalant=0,totalajuste=0;
String color="#ECE0D8";
String proveedor="";
String propietario="";
String panti="No se encontro.";
float vlr_max=0;
float antres = 0;

%>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=ReAnticipo&accion=Insert" onSubmit="return validarReAnticipo(this);">
  <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
      <%
      
	if(model.planillaService.getPlanilla()!=null){
        Planilla pla=model.planillaService.getPlanilla();
        numpla= pla.getNumpla();
        fecha =pla.getFecdsp();
        placa= pla.getPlaveh();
        nombre=pla.getNomCond();
        estandar=pla.getSj();
        descripcion=pla.getSj_desc();
        trailer=pla.getPlatlr();
		saldo = pla.getSaldo();
		moneda = pla.getMoneda();
		proveedor=pla.getProveedor();
	    propietario=pla.getNitpro();
    	valorpla=pla.getVlrpla();
		totalant=pla.getTotalanticipo();
		totalajuste=pla.getTotalajustes();
		
		Stdjobdetsel sj=new Stdjobdetsel();
	  	float pa =0;
	  	
	  	float unidad_default = 0;
	  	model.stdjobdetselService.searchStdJob(estandar);
      	if(model.stdjobdetselService.getStandardDetSel()!=null){
			sj = model.stdjobdetselService.getStandardDetSel();
            pa=Float.parseFloat(sj.getPorcentaje_ant().substring(0,sj.getPorcentaje_ant().length()-1));
			panti = sj.getPorcentaje_ant();
			//out.println("Porcentaje maximo anticipo");
		}
		vlr_max= (valorpla  + totalajuste) * (pa/100);
		antres = vlr_max - totalant;
		if(antres<0) antres = 0;
		reant =""+ antres;
		
	}
	 if(request.getAttribute("error")!=null){
		color = (String)request.getAttribute("error");
	}
	if(request.getParameter("reant")!=null){
		reant=request.getParameter("reant");
	} 
	
	 
     
  %>
  <table width="655" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="#ECE0D8" class="letra">
    <tr>
      <th class="titulo">PLANILLA No. 
        <input name="planilla" type="hidden" id="planilla" value="<%=numpla%>">
      <%=numpla%> </th>
    </tr>
    <tr class="fila">
      <td height="248" >
	  <table width="100%"  border="0">
          <tr class="subtitulos">
            <td colspan="2" >DATOS DE REMESA 
            <input name="standard" type="hidden" id="standard3" value="<%=estandar%>"></td>
          </tr>
          <tr class="fila">
            <td width="28%">STANDARD JOB </td>
            <td width="72%">              <%=descripcion%></td>
          </tr>
        </table>
        <br>
        <table width="100%"  border="0">
          <tr class="subtitulos">
            <td colspan="2" >DATOS DE LA PLANILLA </td>
          </tr>
          <tr class="fila">
            <td width="28%"> FECHA DESPACHO </td>
            <td width="72%"><%=fecha%></td>
          </tr>
          <tr class="fila">
            <td><span class="Estilo7">PLACA</span></td>
            <td><%=placa%></td>
          </tr>
          <tr class="fila">
            <td>CONDUCTOR </td>
            <td><%=cedula%><%=nombre%> </td>
          </tr>
          <tr class="fila">
            <td>PROPIETARIO</td>
            <td><%=propietario%></td>
          </tr>
          <tr class="fila">
            <td>CODIGO DE PROVEEDOR Y BANCO </td>
            <td><%=proveedor%></td>
          </tr>
        </table>
        <br>
        <table width="100%"  border="0">
          
          <tr class="subtitulos">
            <td colspan="2" >DETALLES DE VALOR, AJUSTES Y DESCUENTOS REALIZADOS A LA PLANILLA </td>
          </tr>
		 <tr class="fila">
            <td>VALOR DE LA PLANILLA </td>
            <td><%=com.tsp.util.Util.customFormat(valorpla)%></td>
          </tr>
		  <%
		  totalajuste=0;
		  Vector con = model.planillaService.getInformes();
		  if(con!=null && !placa.equals("")){
		  	for (int i = 0; i<con.size();i++){
				Movpla m = (Movpla) con.elementAt(i);
				totalajuste = totalajuste + m.getVlr();
			
		  %>
          <tr class="fila">
            <td width="28%"><%=m.getConcept_code()%></td>
            <td width="72%"><%=com.tsp.util.Util.customFormat(m.getVlr())%></td>
          </tr>
		<%}
		}%>
          <tr class="fila">
            <td>SALDO TOTAL </td>
            <td><%=com.tsp.util.Util.customFormat(valorpla+totalajuste)%>
            <input name="saldo" type="hidden" id="saldo" value="<%=valorpla+totalajuste%>"></td>
          </tr>
		  <tr class="subtitulos">
            <td colspan="2" >DATOS DE ANTICIPO </td>
          </tr>
          <tr class="fila">
            <td>ANTICIPO MAXIMO PERMITIDO </td>
            <td><%=panti%> Valor: <%=com.tsp.util.Util.customFormat(vlr_max)%></td>
          </tr>
          <tr class="fila">
            <td>ANTICIPO RESTANTE</td>
            <td><%=com.tsp.util.Util.customFormat(antres)%>
            <input type="hidden" name="antres" value="<%=antres%>"></td>
          </tr>
      </table>      </td>
    </tr>
  </table>
  <%if(request.getAttribute("planilla")!=null){%>
  <br>
  <table width="655" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="ECE0D8" class="letra">
    <tr class="titulo">
      <td colspan="4"><strong>DATOS DEL RE-ANTICIPO </strong></td>
    </tr>
    <tr class="fila">
      <td width="149"><strong>Valor re-anticipo</strong></td>
      <td colspan="3" bgcolor="<%=color%>"><input name="reant" type="text" id="reant" value="<%=request.getParameter("reant")!=null?request.getParameter("reant"):reant%>">
        <select name="moneda" id="moneda">
          <option value="PES" selected>PES</option>
          <option value="BOL">BOL</option>
          <option value="DOL">DOL</option>
        </select>
        <input name="controller" type="hidden" id="controller" value="<%=CONTROLLER%>"> </td>
    </tr>
    <tr class="fila">
      <td><strong>BANCO</strong></td>
      <td colspan="3" ><%TreeMap bancos= model.buService.getListaBancos(usuario.getLogin()); %>
          <input:select name="banco" options="<%=bancos%>" attributesText="style='width:45%;'" default='<%=request.getParameter("banco")!=null?request.getParameter("banco"):""%>' /> </td>
    </tr>
    <tr class="titulo">
      <td colspan="4" >DATOS QUE SOLO APLICAN AL REANTICIPO </td>
    </tr>
    <tr class="fila">
      <td height="26"><strong>AGENCIA </strong></td>
      <td width="195" >
	  <%TreeMap agencia = model.agenciaService.listar(); 
	  String agen = request.getParameter("agency")!=null?request.getParameter("agency"):"NADA";
	  			 %>
          <input:select name="agency" options="<%=agencia%>"  attributesText="onChange='buscarUsuarios()'" default='<%=agen%>' />
	  </td>
      <td width="85" ><strong>DESPACHADOR</strong></td>
      <td colspan="2" ><% TreeMap usuarios = model.usuarioService.getTusuarios(); %>
          <input:select name="usuario" options="<%=usuarios%>" attributesText="style='width:100%;'" />
        </td>
		
    </tr>
  </table>  
  <p align="center">
    <input type="submit" name="Submit8" value="Aceptar">
    <input type="submit" name="Submit5" value="Regresar">
  </p>
 <%}%>
</form>
<iframe width=188 height=166 name="gToday:datetime:agenda.js:gfPop:plugins_12.js" id="gToday:datetime:agenda.js:gfPop:plugins_12.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</body>
</html>
