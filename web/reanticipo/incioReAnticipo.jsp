<!--
- Autor : Ing. Jose de la rosa
- Date  : 22 de Nobiembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite iniciar un reanticipo
--%>

<%-- Declaracion de librerias--%>
<%@ page session="true"%>
<%@ page errorPage="/error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Reanticipo</title>
<script language="javascript" src="js/validar.js">
</script>
<script>
     function mostrarMoneda(valor){
		  var vec = valor.split("/");
		  	if(vec.length>2){
			
			form1.moneda.value = vec[2];
			}else{
				form1.moneda.value = "";
			}
			
	}
	</script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body onResize="redimensionar();" >
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reanticipo"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<form name="form2" method="post" action="<%=CONTROLLER%>?estado=PlanillaAnticipo&accion=Search">
  <table width="380" border="2" align="center">
    <tr>
      <td><table width="100%" class="tablaInferior">
          <tr>
            <td colspan="2" ><table width="100%"  border="0" cellspacing="1" cellpadding="0">
                <tr>
                  <td width="50%" class="subtitulo1">&nbsp;Busqueda de  planilla</td>
                  <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                </tr>
            </table></td>
          </tr>
          <tr class="letra">
            <td width="50%"><div align="center">N&uacute;mero Planilla:</div></td>
            <td width="50%"><input name="planilla" type="text" class="textbox" id="planilla" size="10" maxlength="10">
				&nbsp;<input type="image" src="<%=BASEURL%>/images/botones/buscar.gif" title="Buscar Remesas" style="cursor:hand" name="Aceptar_" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
			</td>
          </tr>
      </table></td>
    </tr>
  </table>
</form>
<%//Inicializo variables
String estandar="", descripcion="", fecha="", placa="", trailer="",cedula="", nombre="", numpla="", moneda="", reant="";
float saldo=0,valorpla=0,totalant=0,totalajuste=0;
String color="#ECE0D8";
String proveedor="";
String propietario="";
String panti="No se encontro.";
double vlr_max=0;
double antres = 0;


%>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=ReAnticipo&accion=Insert">
  <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
      <%
      
	if(model.planillaService.getPlanilla()!=null){
        Planilla pla=model.planillaService.getPlanilla();
        numpla= pla.getNumpla();
        fecha =pla.getFecdsp();
        placa= pla.getPlaveh();
        nombre=pla.getNomCond();
        estandar=pla.getSj();
        descripcion=pla.getSj_desc();
        trailer=pla.getPlatlr();
		saldo = pla.getSaldo();
		moneda = pla.getMoneda();
		proveedor=pla.getProveedor();
	    propietario=pla.getNitpro();
    	valorpla=pla.getVlrpla();
		totalant=pla.getTotalanticipo();
		totalajuste=pla.getTotalajustes();
    	saldo = pla.getSaldo();
		
		Stdjobdetsel sj=new Stdjobdetsel();
	  	float pa =0;
	  	
	  	float unidad_default = 0;
	  	model.stdjobdetselService.searchStdJob(estandar);
      	if(model.stdjobdetselService.getStandardDetSel()!=null){
			sj = model.stdjobdetselService.getStandardDetSel();
            pa=Float.parseFloat(sj.getPorcentaje_ant().substring(0,sj.getPorcentaje_ant().length()-1));
			panti = sj.getPorcentaje_ant();
			//out.println("Porcentaje maximo anticipo");
		}
		vlr_max= pla.getVlrpla2();
		antres = pla.getMaxReanticipo();
		if(antres<0) antres = 0;
		reant =""+ (long)antres;
		
	
	 if(request.getAttribute("error")!=null){
		color = (String)request.getAttribute("error");
	}
	if(request.getParameter("reant")!=null){
		reant=request.getParameter("reant");
	} 
	
	 
     
  %>
    <table align="center" width="800" border="2">
	<tr>
		<td>
			<table width="100%" align="center">
				<tr>
					<td width="50%" class="subtitulo1">&nbsp; Datos de la Planilla No.
				  <input name="planilla" type="hidden" id="planilla" value="<%=numpla%>"><%=numpla%></td>
					<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
				</tr>
			</table>
	        <table width="100%" class="tablaInferior" >
		  <tr class="letra">
            <td colspan="1" class="filaresaltada"> Estandar Job </td>
            <td colspan="5" class="letra"><%=descripcion%></td>
          </tr>
          <tr >
            <td width="18%" class="filaresaltada"> Fecha Despacho</td>
            <td colspan="3" class="letra"><%=fecha%></td>
            <td width="15%" class="filaresaltada">Placa</td>
            <td width="36%" class="letra"><%=placa%></td>
          </tr>
          <tr >
            <td class="filaresaltada">Conductor</td>
            <td colspan="3" class="letra"><%=cedula%><%=nombre%> </td>
            <td class="filaresaltada">Propietarios</td>
            <td class="letra"><%=propietario%></td>
          </tr>
          <tr>
            <td colspan="6" class="filaresaltada">Codigo de Proveedor: &nbsp;&nbsp;<%=proveedor%></td>
            </tr>
        </table>
        <table width="100%" class="tablaInferior">
          <tr>
            <td colspan="4" class="subtitulo1">Detalles de Valor, Ajustes y Descuentos Realizados a la Planilla</td>
          </tr>
          <tr>
            <td colspan="4">
			<table width="100%"  border="1" bordercolor="#999999">
              <tr class="tblTitulo">
                <td width="42%" height="23"><div align="center">Descripcion del concepto</div></td>
            <td width="14%">Valor</td>
			<td width="20%">Valor Retefuente</td>
			<td width="24%">Valor Reteica</td>
              </tr>
              <%
		  totalajuste=0;
		  Vector con = model.planillaService.getInformes();
		  if(con!=null && !placa.equals("")){
		  	for (int i = 0; i<con.size();i++){
				ItemLiquidacion m = (ItemLiquidacion) con.elementAt(i);
				
			
		  %>
          <tr class="<%=i%2==0?"filagris":"filaazul"%>">
            <td width="42%" class="bordereporte"><div align="center"><%=m.getDescripcion()%></div></td>
            <td width="14%" class="bordereporte"><%=com.tsp.util.Util.customFormat(m.getValor())%></td>
			<td width="20%" class="bordereporte"><%=com.tsp.util.Util.customFormat(m.getReteFuente())%></td>
			<td class="bordereporte"><%=com.tsp.util.Util.customFormat(m.getReteIca())%></td>
            </tr>
		<%}
		}%>
            </table></td>
          </tr>
		 <tr>
            
            </tr>
		 
          <tr class="letra">
            <td class="filaresaltada"><div align="center">Saldo Total</div></td>
            <td colspan="3"><%=com.tsp.util.Util.customFormat(saldo)%>
            <input name="saldo" type="hidden" id="saldo" value="<%=saldo%>"></td>
			</tr>
		  <tr>
            <td colspan="4" class="subtitulo1">Datos de Anticipo</td>
          </tr>
          <tr class="letra">
            <td class="filaresaltada"><div align="center">Valor Total de la Planilla</div></td>
            <td colspan="3"><%=com.tsp.util.Util.customFormat(valorpla)%></td>
          </tr>
          <tr class="letra">
            <td class="filaresaltada"><div align="center">Anticipo Maximo Permitido</div></td>
            <td colspan="3"><%=panti%> Valor: <%=com.tsp.util.Util.customFormat(vlr_max)%></td>
          </tr>
          <tr class="letra">
            <td class="filaresaltada"><div align="center">Anticipo Restante</div></td>
            <td colspan="3"><%=com.tsp.util.Util.customFormat(antres)%>
            <input type="hidden" name="antres" value="<%=antres%>"></td>
          </tr>
      </table>      
	  </td>
    </tr>
  </table>
  <%if(request.getAttribute("planilla")!=null){%>
  <br>
  <table width="90%" align="center" border="2">
	  <tr>
		  <td>
		  	<table width="100%" align="center">
				<tr>
					<td width="50%" class="subtitulo1">&nbsp;Datos del Re-Anticipo</td>
					<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
				</tr>
			</table>
			  <table width="100%" class="tablaInferior">
				<tr class="letra">
				  <td width="108" class="filaresaltada">Valor re-anticipo</td>
				  <td ><input name="anticipo" type="text" onKeyPress="soloDigitos(event,'decNo')" id="anticipo" value="<%=request.getParameter("anticipo")!=null?request.getParameter("anticipo"):reant%>">
					<strong>
					<input name="moneda" type="text" id="moneda" style="font-size:11px;border:0" size="3"  readonly>
					</strong>
					<input name="controller" type="hidden" id="controller" value="<%=CONTROLLER%>"> 
					<strong>
					</strong></td>
				  <td ><div align="center"><a style="cursor:hand " class="Simulacion_Hiper" onClick="window.open('<%=BASEURL%>/colpapel/anticipos.jsp?tipo=0','','height=400,width=800,dependent=yes,resizable=yes,scrollbars=yes,status=yes')"; >Aplicar anticipo a proveedor</a> <strong>
				    <input name="antprov" type="hidden" id="antprov" value="<%=request.getParameter("antprov")%>">
				  </strong></div></td>
				  <td width="56" class="filaresaltada" >Banco</td>
				  <td width="169"><%TreeMap bancos= model.buService.getListBUsuarios();
                    %><input:select name="banco" options="<%=bancos%>" attributesText="style='width:100%;' onChange=mostrarMoneda(this.value);" default='<%=request.getParameter("banco")!=null?request.getParameter("banco"):""%>' /></td>
			      <td><strong>Beneficiario</strong></td>
			      <td><select name="beneficiario" id="beneficiario">
                    <option value="C">Conductor</option>
                    <option value="P">Propietario</option>
					<option value="T">Tercero</option>
                  </select></td>
				</tr>
				<tr>
				  <td colspan="7" class="subtitulo1">Datos para enviar el reanticipo a otras agencias. </td>
				</tr>
				<tr class="letra">
				  <td height="26" class="filaresaltada">Agencia</td>
				  <td width="152" >
				  <%TreeMap agencia = model.agenciaService.listar(); 
				  String agen = request.getParameter("agency")!=null?request.getParameter("agency"):"NADA";
							 %>
					  <input:select name="agency" options="<%=agencia%>"  attributesText="onChange='buscarUsuarios()'" default='<%=agen%>' />
				  </td>
				  <td width="127" class="filaresaltada">Despachador</td>
				  <td colspan="2" ><% TreeMap usuarios = model.usuarioService.getTusuarios(); %> 
				  <input:select name="usuario" options="<%=usuarios%>"   attributesText="onChange='buscarBancosUsuario()'" default='<%=request.getParameter("usuario")!=null?request.getParameter("usuario"):""%>'/></td>
				  <td width="94" class="filaresaltada">Banco</td>
				  <td width="144" ><% TreeMap banco_usuarios =  model.buService.getListBUsuariosOtros(); %>	<input:select name="banco_usuario" options="<%=banco_usuarios%>" attributesText="style='width:100%;' onChange=mostrarMoneda(this.value);"  default='<%=request.getParameter("banco_usuario")!=null?request.getParameter("banco_usuario"):""%>'  />		</td>				</tr>
			  </table> 
		  </td>
	  </tr>
  </table>   
	<p align="center">
		<img  src="<%=BASEURL%>/images/botones/aceptar.gif" title="Asignar Reanticipo" style="cursor:hand" name="Aceptar_" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="if(validarReAnticipo(form1)){form1.submit();this.disabled=true;}">&nbsp;
	<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >&nbsp;	</p>
    <p>
      <%}
 }%>
</p><%if(request.getParameter("mensaje")!=null){%>
    <table border="2" align="center">
      <tr>
        <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
              <td width="229" align="center" class="mensajes"><%=request.getParameter("mensaje")%></td>
              <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
              <td width="58">&nbsp;</td>
            </tr>
        </table></td>
      </tr>
    </table>
	<%}%>
    <p>&nbsp;    </p>
</form>
<iframe width=188 height=166 name="gToday:datetime:agenda.js:gfPop:plugins_12.js" id="gToday:datetime:agenda.js:gfPop:plugins_12.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</div>
</body>
</html>
