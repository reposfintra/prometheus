<%@ page session="true"%>
<%@ page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
    <head>
        <title>Identificacion Usuario</title>
        <script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
        <script src="<%=BASEURL%>/js/inicio.js"></script>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">

        <link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
        
 <style>
 #perfil{ width:100px}
 
 
 </style>
        
        
    </head>
    <%
    if ( "logout".equals(request.getParameter("comando")) ){ %>
        <jsp:forward page="/logout.jsp"/>
 <% }
    %>
<%String ValorBoton = (request.getParameter("tipo") == null || request.getParameter("tipo").equals("login"))?"Ingresar":"Actualizar";
String valorTipo = (request.getParameter("tipo") == null || request.getParameter("tipo").equals("login"))?"login":"renovarClave";
String tam = (request.getParameter("tipo") == null || request.getParameter("tipo").equals("login"))?"332":"318";
%>
    <body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onLoad="validarBuscar( 'listaPerfiles', '<%=CONTROLLER%>?estado=Usuario&accion=Login', '<%=request.getParameter("perfil")%>' );login.usuario.focus();">
        <br>
        <table  width="800" height="500" border="0" align="center" cellpadding="0" cellspacing="0"   background="<%= BASEURL %>/images/login/SDinicio.jpg">
            <tr>
                <td height="31" align="center" valign="top">
                <rd>  </td>
            </tr>
            <tr>
                <td height="405" align="right" valign="bottom">
                <table width="<%= tam %>" border='0'>
                <tr>
                <td width="349" height="280" valign="top">
                <!--  login-->
                <form name="login" method="post" action="<%=CONTROLLER%>?estado=Usuario&accion=Validar&cmd=show&tipo=<%=valorTipo%>" onSubmit="return validarCamposIndex('<%=valorTipo%>');">
                <input type="hidden" name="enviar" id="enviar" value="S" >
                <input type="hidden" name="dstrct" id="dstrct" value="FINV" >
                <input type="hidden" name="proy" id="proy" value="TRSION" >
                <input type="hidden" name="login_into" id="login_into" value="Fintra" >
                <table align="left" width="70%" border="0">
                <tr>
                <td width="100%" height="40" colspan="4" valign="top">
					<%if ( request.getParameter("tipo") == null || request.getParameter("tipo").equals("login")){%>
                <table width="100%" border="0" cellpadding="2" cellspacing="0">
                <tr align="center" >
                        <td width="33%" class="letraresaltada">&nbsp;</td>
                        <td width="50%">&nbsp;</td>
                        </tr>
                                <tr align="center" >
                                <td width="33%" class="msgLogin"> <div align="left" class="letraresaltada">Login </div></td>
                                <td width="50%">
						      <%String us = (request.getParameter("usuario")!=null)?request.getParameter("usuario"):"";%>
                                    <input name="usuario" id="usuario" type="text" class="textbox" style="width:100% " value="<%=us%>" size="19" maxlength="10" onBlur="validarBuscar( 'listaPerfiles', '<%=CONTROLLER%>?estado=Usuario&accion=Login', '<%=request.getParameter("perfil")%>' );">
                                </td>
                                </tr>
                                <tr align="center" >
                                <td width="33%" class="letraresaltada"><div align="left">Password</div></td>
                                <td width="50%"><div align="left"><span class="Estilo3">
                                    <input name="clave" type="password" class="textbox" style="width:100% " size="19" maxlength="10">
                                </td>
                                </tr>
                                <tr align="center" >
                                <td width="33%" class="letraresaltada"> <div align="left">Perfil</div></td>
                                <td width="50%">
                                    <div id="listaPerfiles">
                                        <select name="perfil" class="textbox" id="perfil" style="width:100%">
                                            <option value="">Seleccione</option>
                                        </select>
                                        <input type="hidden" name="userlogin" id="userlogin" value="" >
                                    </div>
                                </td>
                                <td width="17%" rowspan="4" >
                                    <div id="imgworking" align="right" style="visibility:hidden"><img src="<%=BASEURL%>/images/cargando.gif"></div></td>
                                </tr>
                    </table>
					  <%}else if (request.getParameter("tipo").equals("renovarClave")){%>
    			      <input name='cambiandoClave' value='true' type='hidden'>
                            <table width="100%" border="0" cellpadding="2" cellspacing="0">
                                <tr align="center" >
                                <td width="53%" class="letraresaltada"> <div align="left">Login </div></td>
                                <td width="47%">
								  <%String us = (request.getParameter("usuario")!=null)?request.getParameter("usuario"):"";%>
                                <input name="usuario" type="text" class="textbox" style="width:100% " value="<%=request.getParameter("usuario")%>" size="16" maxlength="10" readonly>
                                <input name="perfil" type="hidden" value="<%=request.getParameter("perfil")%>"><input name="dstrct" type="hidden" value="<%=request.getParameter("dstrct")%>"></td>
                                </tr>
                                <tr align="center" >
                                <td width="53%" class="letraresaltada" nowrap><div align="left" class="letraresaltada">Clave de acceso </div></td>
                                <td width="47%"><input name="clave1" type="password" class="textbox" style="width:100% " size="19" maxlength="10">
                                </td>
                                </tr>
                                <tr align="center" >
                                <td width="53%" class="letraresaltada" nowrap> <div align="left">Nueva Clave de aceso </div></td>
                                <td width="47%"><input name="nclave" type="password" class="textbox" style="width:100% " size="16" maxlength="10">
                                </td>
                                </tr>
                                <tr align="center" >
                                <td width="53%" class="letraresaltada" nowrap> <div align="left">Confirme nueva Clave</div></td>
                                <td width="47%"><input name="cnclave" type="password" class="textbox" style="width:100% " size="19" maxlength="10"></td>
                                </tr>
                            </table>
				  <%}%></td>
                  </tr>
                            <tr>
                                <td colspan="2" width="100%" height="26" align="center">
                                    <input type="image" src="<%=BASEURL%>/images/botones/aceptar.gif"  height="21" name="imgaceptar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand; ">
                                    <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">
                                </td>
                            </tr>
                            <tr>
                                <td width="100%" height="30" colspan="2" align="center" valign="top" class="msgLogin">   
                                        <div id="working" class="informacion" >
					<%if ( request.getParameter("msg") != null) {%>
						<%=request.getParameter("msg")%></div>
					<%}%></td>
                            </tr>
                  </table>
                  </form>
                    <!--  ***** -->
                </td>
                </tr>
            </table></td>
            </tr>
    </table>
    </body>
</html>
