<%@page contentType="text/html"%>
<%@page import="java.util.*, com.tsp.operation.model.*, com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
     <title>MODIFICACION DE CARAVANAS</title>
     <link href="css/EstilosFiltros.css" rel="stylesheet" type="text/css">
     <script language="javascript" src='js/PantallaModCaravana.js'></script>
</head>
<body>

<%-- <jsp:useBean id="beanInstanceName" scope="session" class="beanPackage.BeanClassName" /> --%>
<%-- <jsp:getProperty name="beanInstanceName"  property="propertyName" /> --%>
     <table align='center' border='1'>
           <tr class='TableSubHeaderDecoration'>
              <td>
                 <table border='1' align='center'>
                       <tr class='TableHeaderDecoration'> 
                          <form name='frmfndcara' action='controller?estado=Caravana&accion=Update&cmd=find' method='post' onsubmit='return buscarCaravana()'>
                          <td>CARAVANA:</td>
                          <td><input name='caravana' maxlength='6' onkeypress='return soloDigitosKeyPress();'></td>
                          <td><input type='submit' name='btnfind' value='BUSCAR'></td>
                          </form>
                       </tr>
                 </table>
              </td>
           </tr>
           <form name='frmaddcara' action='controller?estado=Caravana&accion=Update&cmd=add' method='post' onsubmit='return addPlanilla()'>
           <tr align='center' class='TableSubHeaderDecoration'>
              <td>
                 <table border='1'>
                      <tr class='TableHeaderDecoration'>
                      <td colspan='5'>ADICIONAR PLANILLA</td>
                      </tr>
                      <tr class='TableSubHeaderDecoration'>
                      <td>DISTRITO:</td>
                          <%
                            Usuario usuario = (Usuario)session.getAttribute("Usuario");
                            TreeMap cia = usuario.getListCia();
                          %>
                      <td><input:select name="cia" options="<%=cia%>"/></td>
                      <td>PLANILLA:</td>
                      <td><input type='text' name='planilla' maxlength='6'></td>
                      <td><input type='submit' name='btnadd' value='ADICIONAR'></td>
                      </tr>
                 </table>
                 <% String mensaje =  (String)request.getAttribute("mensaje");
                   if ( (mensaje != null) && (!mensaje.equals("")) ){ %>
                        <center>
                        <fieldset style='width=450; background=f5f5cd; align=center;' >
                               <p class='MessageDecoration'>
                                  <%=mensaje%>
                               </p>  
                        </fieldset>
                        </center>
                <%}%>
              </td>
              <%
                Vector planilla = model.caravanaService.getPlanilla();
                Caravana caravana = planilla.size() > 0 ? (Caravana)planilla.firstElement():null;
                String carNo = (caravana != null ? caravana.getCodigo() : "");
               %>
               <input type='hidden' name='caravana' value='<%=carNo%>'>
           </tr>
           </form>
           <tr align='center' class='TableSubHeaderDecoration'>
              <td>
                 <table border='1'>
                      <tr class='TableHeaderDecoration'>
                         <th>CARAVANA No:</th>
                         <td colspan='4'><%=carNo%></td>
                      </tr>
                      <tr class='TableSubHeaderDecoration'>
                      <th>FECHA DE CREACION</th>
                      <th>DISTRITO</th>
                      <th>PLANILLA</th>
                      <th>MOTIVO</th>
                      <th>ELIMINAR</th>
                      </tr>
                      <%
                        int i = 0;
                        
                        for( i=0; i<planilla.size(); i++){
                            caravana = (Caravana)planilla.get(i);%>
                            <tr class='TableRowDecoration'>
                            <form name='frmdel<%=i%>' method = 'post' action = 'controller?estado=Caravana&accion=Update&cmd=del'>
                            <input type='hidden' name = 'caravana' value='<%=caravana.getCodigo()%>'>
                            <input type='hidden' name = 'cia' value='<%=caravana.getCia()%>'>
                            <input type='hidden' name = 'planilla' value='<%=caravana.getPlanilla()%>'>
                            <input type='hidden' name = 'creation_date' value='<%=caravana.getCreation_date()%>'>
                            <td><%=caravana.getCreation_date().substring(0,10)%></td><td><%=caravana.getCia()%></td><td><%=caravana.getPlanilla()%></td><td><input type='text' name='motivo' title='MOTIVO POR EL CUAL SE VA A ELIMINAR LA PLANILLA' maxlength='30'></td><td align='center'><img src='images/equis.gif' alt='Eliminar' onClick='vacio(<%=(i+2)%>)' style='cursor:hand'></td>
                            </form>
                            </tr>
                      <%}%>
                 </table>
              </td>
           </tr>
     </table>
</body>
</html>
