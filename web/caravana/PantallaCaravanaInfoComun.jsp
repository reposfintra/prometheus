<%@page contentType="text/html"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@include file="/WEB-INF/InitModel.jsp" %>
<html>
<head><title>JSP Page</title>
<link href="css/EstilosFiltros.css" rel="stylesheet" type="text/css">
<script language="javascript" src="js/Caravana.js"></script>
</head>
<body>

<%-- <jsp:useBean id="beanInstanceName" scope="session" class="beanPackage.BeanClassName" /> --%>
<%-- <jsp:getProperty name="beanInstanceName"  property="propertyName" /> --%>
    <input:form name="frmplanvj" method="post" action="controller?estado=Caravana&accion=GetInfoComun&cmd=show" bean="planVj" attributesText="onsubmit='return validarInfoComun()'">
    <table  border = '2' align = 'center'>
        <tr>
        <td>
        <table>
              <tr class='TableHeaderDecoration' align='center'><td colspan='6'>INFORMACION COMUN</td></tr>
              <tr class='TableHeaderDecoration' align='center'><td colspan='6'>A L I M E N T A C I O N</td></tr>
              <tr class='TableSubHeaderDecoration'>
              <td></td>
              <td>Lugar</td>
              <td colspan='4'>Tel&eacute;fono</td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td>Puesto Nro 1 :</td>
              <td><input:text name="al1" attributesText="size='30' maxlength='30'"/></td>
              <td colspan='4'><input:text name="at1" attributesText="size='20' maxlength='20'"/></td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td>Puesto Nro 2 :</td>
              <td><input:text  name="al2" attributesText="size='30' maxlength='30'"/></td>
              <td colspan='4'><input:text  name="at2" attributesText="size='20' maxlength='20'"/></td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td>Puesto Nro 3 :</td>
              <td><input:text  name="al3" attributesText="size='30' maxlength='30'"/></td>
              <td colspan='4'><input:text  name="at3" attributesText="size='20' maxlength='20'"/></td>
              </tr>
              
              <tr class='TableHeaderDecoration' align='center'><td colspan='6'>P E R N O C T A C I O N</td></tr>
              <tr class='TableSubHeaderDecoration'>
              <td></td>
              <td>Lugar</td>
              <td colspan='4'>Tel&eacute;fono</td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td>Puesto Nro 1 :</td>
              <td><input:text  name="pl1" attributesText="size='30' maxlength='30'"/></td>
              <td colspan='4'><input:text  name="pt1" attributesText="size='20' maxlength='20'"/></td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td>Puesto Nro 2 :</td>
              <td><input:text  name="pl2" attributesText="size='30' maxlength='30'"/></td>
              <td colspan='4'><input:text  name="pt2" attributesText="size='20' maxlength='20'"/></td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td>Puesto Nro 3 :</td>
              <td><input:text  name="pl3" attributesText="size='30' maxlength='30'"/></td>
              <td colspan='4'><input:text  name="pt3" attributesText="size='20' maxlength='20'"/></td>
              </tr>
          
              <tr align='center' class='TableHeaderDecoration'><td colspan='6'>P A R Q U E A D E R O S</td><tr>
              <tr class='TableSubHeaderDecoration'>
              <td></td>
              <td>Lugar</td>
              <td colspan='4'>Tel&eacute;fono</td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td align = 'left'>En Origen :</td>
              <td><input:text  name="qlo" attributesText="size='30' maxlength='30'"/></td>
              <td colspan='4'><input:text  name = "qto" attributesText="size='20' maxlength='20'"/></td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td align = 'left'>En Destino :</td>
              <td><input:text  name="qld" attributesText="size='30' maxlength='30'"/></td>
              <td colspan='4'><input:text  name="qtd" attributesText="size='20' maxlength='20'"/></td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td align = 'left'>Puesto Nro 1 :</td>
              <td><input:text  name="ql1" attributesText="size='30' maxlength='30'"/></td>
              <td colspan='4'><input:text  name="qt1" attributesText="size='20' maxlength='20'"/></td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td align = 'left'>Puesto Nro 2 :</td>
              <td><input:text  name="ql2" attributesText="size='30' maxlength='30'"/></td>
              <td colspan='4'><input:text  name="qt2" attributesText="size='20' maxlength='20'"/></td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td align = 'left'>Puesto Nro 3 :</td>
              <td><input:text  name="ql3" attributesText="size='30' maxlength='30'"/></td>
              <td colspan='4'><input:text  name="qt3" attributesText="size='20' maxlength='20'"/></td>
              </tr>
              
              <tr align='center' class='TableHeaderDecoration'><td colspan='6'>T A N Q U E O</tr></td>
              <tr class='TableSubHeaderDecoration'>
              <td></td>
              <td>Lugar</td>
              <td colspan='4'>Tel&eacute;fono</td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td>Puesto Nro 1 :</td>
              <td><input:text  name="tl1" attributesText="size='30' maxlength='30'"/></td>
              <td colspan='4'><input:text  name="tt1" attributesText="size='20' maxlength='20'"/></td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td>Puesto Nro 2 :</td>
              <td><input:text  name="tl2" attributesText="size='30' maxlength='30'"/></td>
              <td colspan='4'><input:text  name="tt2" attributesText="size='20' maxlength='20'"/></td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td>Puesto Nro 3 :</td>
              <td><input:text  name="tl3" attributesText="size='30' maxlength='30'"/></td>
              <td colspan='4'><input:text  name="tt3" attributesText="size='20' maxlength='20'"/></td>
              </tr>
              
              <tr class='TableHeaderDecoration' align='center'>
                <td colspan='6'>
                    <input type='submit' value='SIGUIENTE' name='btncrear' class='botones'>
                </td>
              </tr>
       </table>
       </td>
       </tr>
    </table>
</input:form>
</body>
</html>
