<%@page contentType="text/html"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%Caravana caravana = (Caravana)session.getAttribute("caravana");
  Usuario usuario = (Usuario)session.getAttribute("Usuario");
%>
<html>
<head><title>JSP Page</title>
<link href="css/EstilosFiltros.css" rel="stylesheet" type="text/css">
<script language="javascript" src="js/Caravana.js"></script>
</head>
<body>

<%-- <jsp:useBean id="beanInstanceName" scope="session" class="beanPackage.BeanClassName" /> --%>
<%-- <jsp:getProperty name="beanInstanceName"  property="propertyName" /> --%>
    <input:form name="frmpla" method="post" action="controller?estado=CaravanaPlanViaje&accion=Search&cmd=show" attributesText="onsubmit='return validarfrmpla()'">
    <table  border = '2' align = 'center'>
        <tr>
        <td>
        <table>
              <tr align='center' class='TableHeaderDecoration'>
                  <td colspan='2'>CARAVANAS</td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
                  <td>CARAVANA NO:</td>
                  <td><%=caravana.getCodigo()%></td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
                 <td>DISTRITO :</td>
                 <td><input:select name="cia" options="<%=usuario.getListCia()%>"/></td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
                 <td>PLANILLA :</td>
                 <td><input:text name="planilla" attributesText="size='10' maxlength='10'"/></td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
                  <td colspan='2'>
                     DIGITE LA PLANILLA A ADICIONAR A LA CARAVANA
                  </td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
                 <td colspan='2'><input type='submit' name='btnbuscar' value='BUSCAR' class='botones'></td>
              </tr>
        </table>
        </td>
        </tr>
    </table> 
    </input:form>
    <center>
    <fieldset style='width=400; background=f5f5cd; align=center;' >
           <p class='MessageDecoration'>
           <% String mensaje =  (String)request.getAttribute("mensaje");
              if ((mensaje != null)&&(!mensaje.equals(""))) out.println(mensaje); %>
           </p>  
    </fieldset>
</center>
</body>
</html>
