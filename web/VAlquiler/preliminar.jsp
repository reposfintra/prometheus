<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/validar.js">
</script>
<link href="<%=BASEURL%>/css/estilo.css" rel="stylesheet" type="text/css">
<link href="../css/estilo.css" rel="stylesheet" type="text/css">

</head>

<body>
  <br>
  <form name="form1" method="post" action="colpapel/InicioDespacho.jsp" onSubmit="return verificarStandard();">
    <table width="75%" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" class="Letras">
      <tr>
        <th colspan="3" class="titulo" scope="col">REGISTRO DE VIAJES PANTALLA PRELIMINAR</th>
      </tr>
      <tr class="Estilo6">
        <th colspan="3" class="subtitulos" scope="row">INFORMACION DEL CLIENTE </th>
      </tr>
      <tr class="fila">
        <th width="164" rowspan="2" scope="row"><div align="left">STANDARD/CLIENTE</div></th>
        <td colspan="2">
          <p>
            <span class="Simulacion_Hiper" style="cursor:hand " onClick="window.open('<%=BASEURL%>/consultas/consultasClientes.jsp','','HEIGHT=200,WIDTH=600,SCROLLBARS=YES,RESIZABLE=YES')">Consultar clientes...</span></p>          </td>
      </tr>
      <tr>
        <td colspan="2" class="fila"><p class="fila">
          
          <input name="cliente" type="text" id="cliente" maxlength="6" onKeyPress="return verificarTecla(event);">
          <input type="button" name="Submit3" value="Buscar..." onClick="buscarClient();">
          <input name="clienteR" type="hidden" id="clienteR" value="<%=request.getParameter("cliente")%>">
          <input name="standard_nom" type="hidden" id="standard_nom" value="<%=(String)request.getAttribute("std")%>">
          <input name="remitentes" type="hidden" id="remitentes" value=" ">
          <input name="docinterno" type="hidden" id="docinterno" value=" ">
          <input name="fechadesp" type="hidden" id="fechadesp" value=" ">
          <input name="placa" type="hidden" id="placa" value=" ">
          <input name="destinatarios" type="hidden" id="destinatarios" value=" ">
          <input name="trailer" type="hidden" id="trailer" value=" ">
          <input name="conductor" type="hidden" id="conductor" value=" ">
          <input name="ruta" type="hidden" id="ruta" value=" ">
          <input name="toneladas" type="hidden" id="toneladas" value=" ">
          <input name="valorpla" type="hidden" id="valorpla3" value=" ">
          <input name="anticipo" type="hidden" id="anticipo" value=" ">
          <input type="hidden" name="hiddenField7">
          <input name="precintos" type="hidden" id="precintos" value=" ">
          <input name="observacion" type="hidden" id="observacion" value=" ">
          <input name="click_buscar" type="hidden" id="click_buscar">
          <br>
          <%if(request.getAttribute("cliente")!=null){%>
          <input name="valorpla" type="hidden" id="valorpla">
        </p>
          <table width="100%" border="0" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" class="fila">
            <tr> 
              <td><strong><%=(String) request.getAttribute("cliente")%></strong></td>
            </tr>
            <tr>
              <td height="27"><strong>Agencia Due&ntilde;a del Cliente : <%=(String) request.getAttribute("agency")%></strong></td>
            </tr>
          </table>
          <%}%> </td>
      </tr>

      <%if(request.getAttribute("std")==null){%>
      <tr class="fila">
        <td rowspan="2" ><strong>RUTA</strong>
        <td width="86"><strong>ORIGEN</strong></td>
        <td width="371">
          <%TreeMap ciudades = model.stdjobdetselService.getCiudadesOri(); 
	  String corigen="";
	  if(request.getParameter("origen")!=null){
	  	corigen = request.getParameter("origen");
	  }
	  String cdest="";
	  if(request.getParameter("destino")!=null){
	  	cdest = request.getParameter("destino");
	  }
	  %>
          <input:select name="ciudadOri" options="<%=ciudades%>" attributesText="style='width:100%;' onChange='buscarDestinos()'" default='<%=corigen%>'/> </td>
      </tr>
      <tr class="Estilo6">
        <td class="fila"><strong>DESTINO</strong></td>
        <td class="fila">
          <%TreeMap ciudadesDest = model.stdjobdetselService.getCiudadesDest(); %>
        <input:select name="ciudadDest" options="<%=ciudadesDest%>" attributesText="style='width:100%;'"  default='<%=cdest%>'/> </td>
      </tr>
      <%if(ciudadesDest.size()>0){%>
      <tr class="fila">
        <td colspan="3" >
          <div align="center">
            <input type="button" name="Submit2" value="Buscar Standard..." onClick="buscarStandard()">
          </div>
      </tr>
      <% }%>
      <%if(request.getAttribute("ok")!=null){%>
      <tr class="fila">
        <td ><strong> ESTANDARD JOB </strong>
        <td colspan="2"><%TreeMap stdjob = model.stdjobdetselService.getStdjobTree(); %>
            <input:select name="standard" options="<%=stdjob%>" attributesText="style='width:100%;'"/> </td>
      </tr>
      <tr class="fila">
        <td colspan="3" >
          <div align="center">
            <input type="submit" name="Submit" value="Realizar Despacho...">
        </div></td>
      </tr>
      <%}
	}else{%>
      <tr class="fila">
        <td ><strong>ESTANDARD JOB </strong></td>
        <td colspan="2" ><%=(String) request.getAttribute("std")%>
            <input name="standard" type="hidden" id="standard" value="<%=(String)request.getAttribute("sj")%>">
        </td>
      </tr>
      <tr class="fila">
        <td colspan="3" >
          <div align="center">
            <input type="submit" name="Submit4" value="Realizar Despacho...">
        </div></td>
      </tr>
      <%}%>
    </table>
	
	 <br>
	 <br>
	 <%if(request.getAttribute("mensaje")!=null){%>
	  <table width="70%"  border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="#ECE0D8" class="letra">
        <tr>
          <td bgcolor="#FFA928"><div align="center"><strong>INFORMACI&Oacute;N</strong></div></td>
        </tr>
        <tr>
          <td><strong>ULTIMA PLANILLA Y REMESA GENERADAS </strong></td>
        </tr>
        <tr>
          <td>Se generaron Planilla <span class="Estilo7"><strong><%=(String)request.getAttribute("numpla")%> </strong></span>y Remesa <span class="Estilo7"><strong><%=(String)request.getAttribute("numrem")%></strong></span></td>
        </tr>
        <tr>
          <td bgcolor="#FFA928"><div align="center"><strong>IMPRESI&Oacute;N</strong></div></td>
        </tr>
        <tr>
          <td class="Letras" style="cursor:hand" title="Imprimir planilla..." onMouseOver="bgColor='#99cc99'" onMouseOut="bgColor='ECE0D8'"  onClick="window.open('<%=CONTROLLER%>?estado=PlanillaImpresion&accion=Opciones&Opcion=Buscar&NumeroPlanilla=<%=(String)request.getAttribute("numpla")%>','','height=400,width=800,dependent=yes,resizable=yes,scrollbars=yes,status=yes');"><span class="letras">Imprimir planilla </span> <b><%=(String)request.getAttribute("numpla")%></b></td>
        </tr>
        <tr>
          <td class="Letras" style="cursor:hand" title="Imprimir remesa..."  onMouseOver="bgColor='#99cc99'" onMouseOut="bgColor='ECE0D8'" onClick="window.open('<%=CONTROLLER%>?estado=Remesa&accion=Buscar&Numrem=<%=(String)request.getAttribute("numrem")%>','','height=400,width=800,dependent=yes,resizable=yes,scrollbars=yes,status=yes')"><span class="letras">Imprimir remesa</span> <b><%=(String)request.getAttribute("numrem")%></b></td>
        </tr>
        <tr>
          <td height="24" class="Letras" style="cursor:hand" title="Lista de planillas por imprimir..." onMouseOver="bgColor='#99cc99'" onMouseOut="bgColor='ECE0D8'"  onClick="window.open('<%=CONTROLLER%>?estado=Menu&accion=PlanillaImpresion','','height=400,width=800,dependent=yes,resizable=yes,scrollbars=yes,status=yes');">Listado de Planillas para imprimir</td>
        </tr>
        <tr>
          <td class="Letras" style="cursor:hand" title="Lista de remesas por imprimir..."  onMouseOver="bgColor='#99cc99'" onMouseOut="bgColor='ECE0D8'" onClick="window.open('<%=CONTROLLER%>?estado=Remesa&accion=Buscar','','height=400,width=800,dependent=yes,resizable=yes,scrollbars=yes,status=yes')">Listado de Remesas  para imprimir</td>
        </tr>
        <tr>
          <td class="Letras" style="cursor:hand" title="Lista de cheques por imprimir..."  onMouseOver="bgColor='#99cc99'" onMouseOut="bgColor='ECE0D8'" onClick="window.open('<%=CONTROLLER%>?estado=Menu&accion=ImpresionCheques&opcion=no&total=0','','height=400,width=800,dependent=yes,resizable=yes,scrollbars=yes,status=yes')">Listado de Cheques  para imprimir</td>
        </tr>
        <tr>
          <td class="Letras">
        <TR>
              <TD ALIGN='center'><FIELDSET>
                <legend><b>NOTA : </b></legend>
                Para ver informaci&oacute;n acerca de <b><a class="Simulacion_Hiper" style="cursor:hand" onclick="window.open('<%=CONTROLLER%>?estado=HReporte&accion=Search');">Hoja de Reporte</a> </b>y <b><a class="Simulacion_Hiper" style="cursor:hand"  onclick="window.open('<%=CONTROLLER%>?estado=PlanViaje&accion=QryAg');">Plan de viaje</a></b>, deber&aacute; hacer Click sobre ellos.
              </FIELDSET></TD>
        </TR>
    </TABLE></td>
        </tr>
    </table>
	  
   
	
	<%}%>
 
<iframe width=188 height=166 name="gToday:datetime:agenda.js:gfPop:plugins_12.js" id="gToday:datetime:agenda.js:gfPop:plugins_12.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</form>
</body>
</html>
