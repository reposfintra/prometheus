
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<% 

//String distrito = "FINV";
model.VAlquilerSvc.LIST();
List Listado = model.VAlquilerSvc.getList();
String Mensaje       = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
if( Listado==null && Listado.size()==0){
	Mensaje = "No hay Registros";
}	
%>

<html>
<head>
<title>Listado de Vehiculos en Alquiler</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">   
   <script language="javascript" src="<%=BASEURL%>/js/validar.js"></script>
   <script language="javascript" src="../js/validar.js"></script>
   <script language="javaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
   <script>
		function SelAll(){
				for(i=0;i<FormularioListado.length;i++)
						FormularioListado.elements[i].checked=FormularioListado.All.checked;
		}
    	function ActAll(){
            FormularioListado.All.checked = true;
            for(i=1;i<FormularioListado.length;i++)	
                    if (!FormularioListado.elements[i].checked){
                            FormularioListado.All.checked = false;
                            break;
                    }
    	}   
</script>   
</head>

<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Vehiculos en Alquiler"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<% if(Listado!=null && Listado.size()>0) { %>
<form action="<%=CONTROLLER%>?estado=VAlquiler&accion=Manager" method='post' name='FormularioListado' id="FormularioListado">
  <div align="center"><br>
  </div>
  <div align="center">
    <table width="600" border="2">
      <tr>
        <td><table width="100%"  border="0" class="tablaInferior">
           <tr>
          		<td width="48%" class="subtitulo1">Vehiculos en Alquiler</td>
            	<td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        	</tr>    
		  </table>
          <table width="100%"  border="0" class="tablaInferior">
		  <tr class="tblTitulo">
            <th width="41">N&ordm;</th>
            <th width="41"><input type='checkbox' name='All' onclick='jscript: SelAll();'>
            </th>
            <th width='400' class="tblTitulo">ESTANDARD JOB</th>
            <th width='108'>PLACA</th>
          </tr>
          <%
           int Cont = 1;
            Iterator it2 = Listado.iterator();
            while(it2.hasNext()){
              	veh_alquiler dat  = (veh_alquiler) it2.next();
				String e = (Cont % 2 == 0 )?"filagris":"filaazul";
				String Estilo = (dat.getReg_status().equals("A"))?"filaresaltada":e;
                %>
          <tr class='<%=Estilo%>'>
            <td align='center' class="bordereporte"><%=Cont++%></td>
            <td align='center' class="bordereporte"><input type='checkbox' name='LOV' value='<%=dat.getPlaca()+"/"+dat.getStd_job_no()%>' onclick='jscript: ActAll();'></td>
            <td class="bordereporte"><%=dat.getStd_job_no() + " - " + dat.getStd_job_desc()  %></td>
            <td align='center' class="bordereporte"><%= dat.getPlaca() %> </td>
          </tr>
          <%  }   %>
        </table></td>
      </tr>
    </table>
    <p><br>
      <input type='hidden' name='Opcion'/>      
	  <input name="anular" src="<%=BASEURL%>/images/botones/anular.gif" type="image" class="boton" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onclick="Opcion.value='Anular';">
      <input name="activar" src="<%=BASEURL%>/images/botones/restablecer.gif" type="image" class="boton" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onclick="Opcion.value='Activar';">	  
	  <img src="<%=BASEURL%>/images/botones/salir.gif" width="90" height="21" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></p>
    <table width="600" border="0">
      <tr align="left">
        <td><a style="cursor:hand" class="Simulacion_Hiper" onClick="window.location='<%=BASEURL%>/VAlquiler/VAlquiler.jsp'">Volver</a></td>
      </tr>
    </table>
    <br>
    </p>
  </div>
</form>

  <center class='comentario'>
  <font color='red'>
  </font>
  <p>
    <%if(!Mensaje.equals("")){%>
  <table border="2" align="center">
    <tr>
      <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="58">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <%}%>
  <font color='red'>  </font>
   <%  } %>
</div>
</body>
</html>
