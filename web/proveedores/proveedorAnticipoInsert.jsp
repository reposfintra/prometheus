<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
    <title>Modificar proveedor ACPM</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <script src="js/validar.js"></script>
    <link href="css/letras.css" rel="stylesheet" type="text/css">

</head>

<body>
 <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
    <table width="677" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="ECE0D8" class="Letras">
        <tr bgcolor="#FFA928">
        <td colspan="4" nowrap><div align="center" class="Estilo2"><strong><strong>ESCOJA  EL PROVEEDOR DE ANTICIPO</strong></strong></div></td>
        </tr>
        <tr bgcolor="#99CCFF">
        <td colspan="2" nowrap bgcolor="#99CCFF"><strong>NIT:</strong></td>
        <td width="164" nowrap bgcolor="#99CCFF"><strong>CIUDAD</strong></td>
        <td width="107" nowrap bgcolor="#99CCFF"><strong>DISTRITO</strong></td>
        </tr>
	 <%
	 if(model.proveedoresService.getProveedores().size()>0){
			Vector list = model.proveedoresService.getProveedores();
			for(int i =0; i<list.size();i++){%>
      
					
					<%Proveedores pa = (Proveedores) list.elementAt(i);
					
					String  nit= pa.getNit();
					String desc=pa.getNombre();
					String ciudad=pa.getCity_code();
                    String distrito= pa.getDstrct();
					String sucursal = pa.getSucursal();
%>
        <tr style="cursor:hand" title="Modificar Proveedores de Anticipo..." onClick="window.location='<%=CONTROLLER%>?estado=Proveedores&accion=Search&num=1&nit=<%=nit%>&sucursal=<%=sucursal%>&anticipo=ok&distrito=<%=distrito%>'" onMouseOver="bgColor='#99cc99'" onMouseOut="bgColor='ECE0D8'">

          <td width="102" nowrap><%=nit%></td>
            <td width="296" nowrap><%=desc%> <%=sucursal%> </td>
            <td nowrap><%=ciudad%></td>
            <td nowrap><%=distrito%></td>
        </tr>
	<%}
	  }%>
    </table>
    <br>
    <br>
    <%if(request.getAttribute("ant")!=null){%>
	<form name="form1" method="post" action="<%=CONTROLLER%>?estado=ProveedorAnticipo&accion=Insert&cmd=show">
	  <input name="nit" type="hidden" id="nit" value="<%=request.getParameter("nit")%>">
	  <input name="sucursal" type="hidden" id="sucursal" value="<%=request.getParameter("sucursal")%>">
      <input name="distrito" type="hidden" id="distrito"  value="<%=request.getParameter("distrito")%>">
      <table width="750" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="ECE0D8" class="Letras">
  <tr bgcolor="#FFA928">
    <td colspan="6" nowrap><div align="center" class="Estilo2"><strong>ANTICIPOS DEL PROVEEDOR </strong></div></td>
  </tr>
  <tr bgcolor="#99CCFF">
    <td colspan="3" nowrap bgcolor="#99CCFF"><strong>ANTICIPO</strong></td>
    <td width="171" nowrap bgcolor="#99CCFF"><strong>TIPO DESCUENTO </strong></td>
    <td width="78" nowrap bgcolor="#99CCFF"><strong>DISTRITO</strong></td>
    <td width="149" nowrap bgcolor="#99CCFF"><strong>STANDARD JOB</strong></td>
  </tr>
  <%
	 if(model.proveedoresService.getAnticipos().size()>0){
			Vector list = model.proveedoresService.getAnticipos();
			int i=0;
			for(i=0; i<list.size(); i++){%>
  			<%Anticipos ant = (Anticipos) list.elementAt(i);
							String codigo = ant.getAnticipo_code();
							String nombre = ant.getAnticipo_desc();
							String tipo_des = ant.getTipo_s();
							String sj = ant.getSj_nombre(); 
							String distrito = ant.getDstrct();
							if(tipo_des.equals("V")){
								tipo_des="Valor";
							}else{
								tipo_des="Porcentaje";
							}
							String nit = request.getParameter("nit");
							String sucursal = request.getParameter("sucursal");
					
%>

	
  <tr style="cursor:hand" title="Modificar Anticipo..."  onMouseOver="bgColor='#99cc99'" onMouseOut="bgColor='ECE0D8'">
    <td width="29" valign="top" nowrap>
	<%if(model.proveedoresService.estaAnticipoProvee(nit, sucursal,codigo)){%>
      <input type="checkbox" name="<%=codigo%>" value="checkbox" checked>
	  <%}else{%>
	  <input type="checkbox" name="<%=codigo%>" value="checkbox">
	  <%}%>   	  </td>
    <td width="71" valign="middle" nowrap><%=codigo%></td>
    <td width="188" valign="middle" nowrap><%=nombre%></td>
    <td valign="middle" nowrap><%=tipo_des%></td>
    <td valign="middle" nowrap><%=distrito%></td>
    <td valign="middle" nowrap><%=sj%>
      <input name="sj" type="hidden" id="sj" value="<%=ant.getSj()%>"></td>
  </tr>
  <%}
	  }%>
</table>
<div align="center"><br>
    <input type="submit" name="Submit" value="Aceptar">
    </div>
	</form>
<div align="center"><br>
  <br>
  <%}%>
</div>
</body>
</html>
