<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
    <title>Anular  proveedor anticipo</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <script src="js/validar.js"></script>
    <link href="css/letras.css" rel="stylesheet" type="text/css">

</head>

<body>
    <form name="form2" method="post" action="<%=CONTROLLER%>?estado=Acpm&accion=Search&num=1">
      <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
      <table width="677" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="ECE0D8" class="Letras">
        <tr bgcolor="#FFA928">
          <td colspan="4" nowrap><div align="center" class="Estilo2"><strong><strong>ESCOJA EL PROVEEDOR DE ANTICIPO A ANULAR </strong></strong></div></td>
        </tr>
        <tr bgcolor="#99CCFF">
          <td colspan="2" nowrap bgcolor="#99CCFF"><strong>NIT:</strong></td>
          <td width="164" nowrap bgcolor="#99CCFF"><strong>CIUDAD</strong></td>
          <td width="107" nowrap bgcolor="#99CCFF"><strong>DISTRITO</strong></td>
        </tr>
        <%
	 if(model.proveedoresService.getProveedores().size()>0){
			Vector list = model.proveedoresService.getProveedores();
			for(int i =0; i<list.size();i++){%>
        <%Proveedores pa = (Proveedores) list.elementAt(i);
					
					String  nit= pa.getNit();
					String desc=pa.getNombre();
					String ciudad=pa.getCity_code();
                    String distrito= pa.getDstrct();
					String sucursal = pa.getSucursal();
%>
        <tr style="cursor:hand" title="Modificar Proveedores de Anticipo..." onClick="window.location='<%=CONTROLLER%>?estado=Proveedores&accion=Search&num=1&nit=<%=nit%>&sucursal=<%=sucursal%>&anular=ok'" onMouseOver="bgColor='#99cc99'" onMouseOut="bgColor='ECE0D8'">
          <td width="102" nowrap><%=nit%></td>
          <td width="296" nowrap><%=desc%> <%=sucursal%> </td>
          <td nowrap><%=ciudad%></td>
          <td nowrap><%=distrito%></td>
        </tr>
        <%}
	  }%>
      </table>
    </form>
<%if(request.getAttribute("provee")!=null){%>
<form name="form2" method="post" action="<%=CONTROLLER%>?estado=Proveedores&accion=Delete&cmd=show">
  <%String nit="";
  	String distrito="", cciudad="", tipo="", moneda="", nombre="", codigo="";
	
     	Proveedores pa= (Proveedores)request.getAttribute("provee");
		nit=pa.getNit();
        cciudad=pa.getCity_code();
        nombre=pa.getNombre();
		codigo = pa.getSucursal();
		String migracion = pa.getmigracion();
		float valor = pa.getTotale();
		distrito = pa.getDstrct();
		float por = pa.getPorcentaje();
		moneda = pa.getMoneda();
					
	%>
  <table width="543" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="#EFE3DE" class="Letras">
    <tr bgcolor="#FFA928">
      <td colspan="2" nowrap><div align="center" class="Estilo2"><strong><strong>ANULAR PROVEEDOR ANTICIPO</strong></strong></div></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td width="166" rowspan="2" nowrap bgcolor="#99CCFF"><strong>Nit:</strong></td>
      <td width="342" nowrap bgcolor="ECE0D8"> <%=nit%>
          <input name="sucursal" type="hidden" id="sucursal" value="<%=codigo%>">
          <input name="nit" type="hidden" id="nit" value="<%=nit%>" ></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap bgcolor="ECE0D8"><%=nombre%> <%=codigo%></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap bgcolor="#99CCFF"><strong>Distrito:</strong></td>
      <td nowrap bgcolor="ECE0D8"><%=distrito%></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap bgcolor="#99CCFF"><strong>Ciudad:</strong></td>
      <td nowrap bgcolor="ECE0D8">
        <%=cciudad%> </td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap bgcolor="#99CCFF"><strong>Codigo de migracion: </strong></td>
      <td nowrap bgcolor="ECE0D8"><%=migracion%></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap bgcolor="#99CCFF"><strong>Porcentaje:</strong></td>
      <td nowrap bgcolor="ECE0D8"><%=por%>%
      </td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap bgcolor="#99CCFF"><strong>Valor del Servicio </strong></td>
      <td nowrap bgcolor="ECE0D8"><%=valor%><%=moneda%>
         </td>
    </tr>
  </table>
  <div align="center"><br>
      <input type="submit" name="Submit" value="Anular">
      <input type="button" name="Submit2" value="Regresar">
  </div>
 
  <table width="530" border="1" align="center" bgcolor="ECE0D8" class="Letras">
  </table>
</form>
 <%}%>  
</body>
</html>
