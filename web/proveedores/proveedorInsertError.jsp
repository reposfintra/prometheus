<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Ingresar proveedor ACPM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="js/validar.js"></script>
<link href="css/letras.css" rel="stylesheet" type="text/css">

</head>

<body>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Proveedores&accion=Insert&cmd=show" onSubmit="return ValidarFormAnticipo(this);">
  <table width="530" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="#EFE3DE" class="Letras">
    <tr bgcolor="#FFA928">
      <td colspan="2" nowrap><div align="center" class="Estilo2"><strong><strong>INGRESAS PROVEEDOR ANTICIPO</strong></strong></div></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td width="165" nowrap bgcolor="#99CCFF"><strong>Nit</strong></td>
      <td width="331" nowrap bgcolor="<%=request.getAttribute("error")%>"><input name="nit" type="text" id="nit" value="<%=request.getParameter("nit")%>" maxlength="15">          </td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap bgcolor="#99CCFF"><strong>Sucursal</strong></td>
      <td width="331" nowrap bgcolor="<%=request.getAttribute("error1")%>"><input name="sucursal" type="text" id="sucursal" value="<%=request.getParameter("sucursal")%>" maxlength="15"></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap bgcolor="#99CCFF"><strong>Distrito</strong></td>
      <td nowrap bgcolor="ECE0D8"><select name="distrito" id="distrito">
        <option value="FINV" selected>FINV</option>
      </select></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap bgcolor="#99CCFF"><strong>Ciudad</strong></td>
      <td nowrap bgcolor="ECE0D8">
	     <%TreeMap ciudades = model.ciudadService.getCiudades(); %>
	  <input:select name="ciudad" options="<%=ciudades%>" attributesText="style='width:100%;'" default="<%=request.getParameter("ciudad")%>"/></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap bgcolor="#99CCFF"><strong>Codigo de migracion</strong></td>
      <td nowrap bgcolor="ECE0D8"><input name="codigo_m" type="text" id="codigo_m" size="2" maxlength="1" value="<%=request.getParameter("codigo_m")%>"></td>
    </tr>
	<tr bgcolor="#99CCFF">
      <td nowrap bgcolor="#99CCFF"><strong>Porcentaje </strong></td>
      <td nowrap bgcolor="ECE0D8"><input name="porcentaje" type="text" id="porcentaje" value="<%=request.getParameter("porcentaje")%>" size="5" maxlength="3" onKeyPress="soloDigitos(event,'decOK')">
        %</td>
    </tr>
	<tr bgcolor="#99CCFF">
	  <td nowrap bgcolor="#99CCFF"><strong>Valor del Servicio </strong></td>
	  <td nowrap bgcolor="ECE0D8"><input name="valor" type="text" id="valor" value="<%=request.getParameter("valor")%>" onKeyPress="soloDigitos(event,'no')">
        <select name="moneda" id="moneda">
          <%if(request.getParameter("moneda").equals("PES")){%>
          <option value="PES" selected>Pesos</option>
          <option value="BOL">Bolivar</option>
          <option value="DOL">Dolares</option>
          <%}else if(request.getParameter("moneda").equals("BOL")){%>
          <option value="PES">Pesos</option>
          <option value="BOL" selected>Bolivar</option>
          <option value="DOL">Dolares</option>
          <%}else if(request.getParameter("moneda").equals("DOL")){%>
          <option value="PES">Pesos</option>
          <option value="BOL">Bolivar</option>
          <option value="DOL" selected>Dolares</option>
          <%}%>
        </select></td>
	</tr>
  </table>
  <br>
  <div align="center">
    <input type="submit" name="Submit" value="Registrar">
    <input type="button" name="Submit2" value="Regresar">
  </div>
  <table width="530" border="1" align="center" bgcolor="ECE0D8" class="Letras">
  </table>
</form>
</body>
</html>
