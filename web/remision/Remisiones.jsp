<%@page session="true"%> 
<%@page import="java.util.*" %>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.Util"%>

<html>
<head>
  <title>Listado de Remisiones</title>
  <link href="<%= BASEURL %>/css/Style.css" rel='stylesheet'>
  <script language="JavaScript" type="text/javascript"  src="<%=BASEURL%>/js/Validaciones.js"></script>
  <SCRIPT language=JavaScript1.2 
    src="<%=BASEURL%>/js/coolmenus3.js">
    alert();
  </SCRIPT>
</head>
<body>
    <BR><BR><BR>
    <center>  
<%
   Remision     remisionPrint   = (Remision)request.getAttribute("remisionPrint"); 
   //Remision     Print   = (Remision)request.getAttribute("Printer"); 
   int total    = Integer.parseInt(request.getParameter("total"));
   int swWindow = Integer.parseInt(request.getParameter("ventana"));
   int  id      = (remisionPrint!=null)?remisionPrint.getId():0; 
   String printed = (String) request.getAttribute("printed");
   
  if(request.getParameter("comentario").equals("")){
     List listaRemision=model.RemisionSvc.getList();
     if(listaRemision!=null){        
         Iterator it = listaRemision.iterator();
         if(it!=null){%>           
         <TABLE  class='fondotabla' width='90%' border='1' cellpadding='0' cellspacing='0'>
              <TR><TH COLSPAN='8'class='titulo1' align='center' height='50'>LISTADO &nbspDE &nbspREMISIONES</TH></TR>
              <TR class='titulo3'>
                      <TH align='center'> No            </TH>
                      <TH align='center'> <img src="<%=BASEURL%>/images/imprimir.gif">  </TH>
                      <TH align='center'> REMISION      </TH>
                      <TH align='center'> PLACA         </TH>
                      <TH align='center'> CONDUCTOR     </TH>
                      <TH align='center'> ANTICIPO      </TH>
                      <TH align='center'> BANCO         </TH>
                      <TH align='center'> FECHA DESPACHO</TH> 
              </TR>
          <%
              int cont=0;
              while(it.hasNext()){
               Remision remision = (Remision)it.next();
               cont++;
                 String op  = "op" +String.valueOf(cont);
                 String op1 = "opa"+String.valueOf(cont);
                 String op2 = "opb"+String.valueOf(cont);
                 String op3 = "opc"+String.valueOf(cont);
                 String op4 = "opd"+String.valueOf(cont);
                 String op5 = "ope"+String.valueOf(cont);
                 String op6 = "opf"+String.valueOf(cont);
               %>
               <TR class='comentario' >
                 <FORM name='formulario_<%=cont%>' id='formulario_<%=cont%>' method='POST'>  
                   <TD align='center' bgcolor='cdcdcd'> <%=cont%></TD>
                   <TD align='center' width='5%' >
                      <font id='<%=op%>' style='width=100%'>                       
                           <input type='checkbox' onclick='javascript:cambiarFilaRemision(<%=op%>,<%=op1%>,<%=op2%>,<%=op3%>,<%=op4%>,<%=op5%>,<%=op6%>,this)'  name='Impresa'>
                      </font>
                   </TD>
                   <TD align='center' width='10%'><font id='<%=op1%>' style='width=100%'><%= remision.getRemision()      %></font></TD>
                   <TD align='center' width='8%' ><font id='<%=op2%>' style='width=100%'><%= remision.getPlaca()         %></font></TD>
                   <TD align='left'   width='25%'><font id='<%=op3%>' style='width=100%'> <%= remision.getConductor()    %></font></TD>
                   <TD align='right'  width='15%'><font id='<%=op4%>' style='width=100%'><%= remision.getAnticipo()      %></font></TD>
                   <TD align='center' width='20%'><font id='<%=op5%>' style='width=100%'><%= remision.getBanco()         %></font></TD>
                   <TD align='center' width='17%'><font id='<%=op6%>' style='width=100%'><%= remision.getFechaEntrada()  %></font></TD>
                   <input type='hidden' name='idRemision' value='<%= remision.getId()%>'>  
                   <input type='hidden' name='id' value='<%= id%>'>  
                 </FORM>
               </TR>
              <% 
              if(remision.getId()==id){%>
                 <script>cambiarFilaRemisionPrint(<%=op%>,<%=op1%>,<%=op2%>,<%=op3%>,<%=op4%>,<%=op5%>,<%=op6%>); </script>
            <% }
              }
            }
          %>           
          </TABLE><br>
          <input type='button'  onclick='javascript:recorrerRemision()' value='Imprimir' style='width=100'>
         <%
     }
  }
 else
 { %>
   <TABLE class='fondotabla'  width='500'>
      <TR><TH CLASS='TITULO1'>INFORMACIÓN</TH></TR>
      <TR>
       <TD align='center' class='comentario2'>
           <br><br><%= request.getParameter("comentario")  %> <br><br><br><br>
       </TD>
     </TR>
   </TABLE>
 <%}%> 
                              
 <%if(printed != null){%>    
    <script>location.replace("<%=BASEURL%>/pdf/dut.pdf");</script>
 <%}%>
</body>
</html>
