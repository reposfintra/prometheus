<%@page session="true"%>
<%@ include file="/WEB-INF/InitModel.jsp" %>
<%@page import="java.util.*" %>
<%@page import="java.util.Date" %>  
<%@page import="java.text.*" %>
<%@page import="com.tsp.operation.model.beans.*" %>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.util.Utility"%>
<HTML XMLNS:IE>
<head>
<title>Directorio de Archivos de Usuario</title>
<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/Validaciones.js"></script>
<script>
	function cambiar(which,element){    
        if( element.checked  && element.id!='All')
            which.style.background='f5f5cd';
        else                            
           which.style.background='white';
   }   
   
   function pintar(element){    
        if( element.checked )  tabla.style.background='f5f5cd';
        else                   tabla.style.background='white';
   }  
   
   function recorrer(theForm){
      var parametros = "";
      var cont       = 0;      
      for (i=0;i< theForm.length ;i++)
         if (theForm.elements[i].type=='checkbox'){
             if( theForm.elements[i].checked  && theForm.elements[i].id!='All' ){
                 parametros+="&id"+cont+"="+  theForm.elements[i].value;
                 cont++;   
             }
         }
      parametros+="&total="+cont;
      if(cont>0)  document.location.href=( theForm.action+parametros);
      else        alert('Deber� seleccionar por lo menos un Archivo');
}

   
   
   
</script>
<SCRIPT>
//<!--
function onDownloadDone(vData) {
	var aData=vData.split("\n");
}

function abrirArchivo(url,titulo){
      option="width=800, height=800 scrollbars=yes, resizable = yes, top=20,left=100, menubar = yes";
      ventana=window.open(url,"ventana",option);
      return ventana;
}
//-->
</SCRIPT>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado= Directorio de Archivos del Usuario"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> <% String msg = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";%>
<% Usuario         User       = (Usuario) session.getAttribute("Usuario");
   ListadoArchivos directorio =  model.DirectorioSvc.getDirectorio();
   List            listado    = directorio.getListado(); %>
  <center>
 
      <TABLE width='700' border="2" align="center">
        <TR>
            <TD colspan='2'>
			<table width="100%" class="tablaInferior" border="0" cellspacing="0" cellpadding="0">
			  <tr class="barratitulo">
			    <%if (  model.publicacionService.esUsuarioConsorcio(User.getLogin()).equals("S")){%>
			    	<td width="35%" class="subtitulo1">&nbsp;CONSORCIO</td>
				<%}else{%>
					<td width="35%" class="subtitulo1">&nbsp;FINTRAVALORES</td>
				<%}%>
			    <!--<td width="35%" class="subtitulo1">FINTRA VALORES  S.A.</td>-->
			    <td width="65%"><img src="<%=BASEURL%>/images/titulo.gif"></td>
			  </tr>
			</table>
  
                
            </TD>
        </TR>
        <TR valign='top'>
            <TD width="8%" align="center" valign="middle" bgcolor="#FFFFFF"> <img src='<%=BASEURL%>/images/file.gif' width="33" height="35" border='0'></TD>
            <TD width="92%" valign="middle">  
               <TABLE width='100%' border="0" cellpadding='1' cellspacing='1'>
                  <TR class="subtitulo1" >
                    <TD colspan='4' >
                      MANEJO DE ARCHIVOS
                    </TD>
                 </TR>
                  <TR class="fila">
                       <TD width='15%'>USUARIO:       </TD><TD width='50%'><%=User.getNombre().toUpperCase()%></TD>
                       <TD width='25%'>TOTAL ARCHIVOS:</TD><TD width='*'  ><%=listado.size()%></TD>
                 </TR>
              </TABLE>
            </TD>
        </TR>
 </TABLE>
  <IE:DOWNLOAD ID="oDownload" STYLE="behavior:url(#default#download)" />

  
  <%
    if(listado!=null && listado.size()>0){
       Iterator it = listado.iterator();%>
       <FORM name="form1" method='post' action="<%= CONTROLLER %>?estado=Ver&accion=Directorio&evento=DELETE&usuario=<%=User.getLogin()%>">
	   <table width="700" border="2"  align="center">
		<tr>
		<td>
		<TABLE  width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" id='tabla' >
         <TR class="tblTitulo">
            <TH width='5%' ><input type='checkbox' id='All' onclick="Sell_all(this.form,this); pintar(this);"></TH>
            <TH width='45%'>ARCHIVO</TH>
            <TH width='25%'>FECHA  </TH>
            <TH width='15%'>TAMA�O </TH>
            <TH width='*'  >Download</TH>
         </TR>
         <% int ele=1;
            while(it.hasNext()){
              Archivo datos = (Archivo) it.next();%>
            <TR id='fila<%=ele%>' class="<%=(ele % 2 == 0 )?"filagris":"filaazul"%>">
              <TD align='center' class="bordereporte"><input type='checkbox' value='<%=datos.get_Id()%>'  id='ele<%=ele%>'  onclick=" Sell_all(this.form,this);  cambiar(fila<%=ele%>,this)"></TD>
              <TD class="bordereporte">&nbsp<%=datos.getName()  %> </TD>
              <TD align='right' class="bordereporte">&nbsp<%=Utility.convertirMilesgdos(datos.getFecha())%> </TD>
              <TD align='right' class="bordereporte">&nbsp<%=String.valueOf(datos.getTama�o()/1024)%>&nbsp KB</TD>
              <TD align='center' class="bordereporte">
					
<P><A  href="#" onClick="abrirArchivo('<%=BASEURL + model.DirectorioSvc.getRaiz()+ User.getLogin() +"/"+datos.getName()%>','<%= datos.getName() %>');" class="Simulacion_Hiper">Download</A>
		  </P>
              </TD>
         </TR>
          <% ele++; }%>
       </TABLE>
	</td>
  </tr>
</table>

       <BR><center>
	   <img src="<%=BASEURL%>/images/botones/eliminar.gif" title="Eliminar..." name="c_imgeliminar"  onClick="recorrer(form1);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	   <img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir..." onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	   </center>
       </FORM>
       <HR size='1' color='cdcdcd'>
    <%}else out.print("<BR><BR><p class='msgLogin'>NO PRESENTAS ARCHIVOS DISPONIBLES.....</p>");%>
</div> 
</body>
</html>
