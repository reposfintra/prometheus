<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ page import="com.tsp.operation.model.beans.*" %>
<html>
<head>
<link href="../css/letras.css" rel="stylesheet" type="text/css">

<title>Redireccionar</title>
</head>
<body>
<%  
   // mfontalvo
   // adaptacion para redireccionar a presupuesto de
   // finanazas desde el menu de slt

   Usuario usuario = (Usuario) session.getAttribute("Usuario");
   String ACCION   = request.getParameter("ACCION");
   String JSP      = request.getParameter("JSP");
   
   
   // parametros basicos de redireccion
   
   response.sendRedirect(
      "/finanzas/PagRedirect.jsp"           + 
	  "?ACCION="  + ACCION                  + 
	  "&JSP="     + JSP                     +  
	  "&AGENCIA=" + usuario.getId_agencia() + 
	  "&USUARIO=" + usuario.getLogin()
   );

   
%>
</body>
</html>



