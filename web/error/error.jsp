<%-- Pagina de Errores --%>
<%@ page import="com.tsp.exceptions.InformationException" %>
<%@ page session="true" %>
<%@ page isErrorPage="true" %>
<%@ include file="../WEB-INF/InitModel.jsp" %>
<%-- Proporcionar la fecha, hora y el nombre del servlet --%><%
String FechaHora          = new java.util.Date().toString();
String bugRptSent         = request.getParameter("bugRptSent");
String remoteAddress      = request.getRemoteAddr();
String servletContext     = request.getContextPath();
String servletContextName = application.getServletContextName();
String bugRptFailure      = (String) request.getAttribute(
                              "com.tsp.sot.BugReportSendAction.failure"
                            );
String errorMessage       = null; //exception.getMessage().replaceAll("\\n", "<br>");
try {
  errorMessage = exception.getMessage().replaceAll("\n", "<br>");
}catch(Exception E){
  errorMessage = exception.getMessage();
}

// Leemos todos los atributos del objeto request.
String item = null;
String requestAttributes = "";
String requestParameters = "";
String sessionAttributes = "";
java.util.Enumeration enumData = null;
if( bugRptSent == null )
{
  enumData = request.getAttributeNames();
  while (enumData.hasMoreElements())
  {
    item = (String) enumData.nextElement();
    requestAttributes += "\n  \t" + item + " = " + request.getAttribute(item);
  }

  // Leemos todos los par�metros del request
  enumData = request.getParameterNames(); 
  while (enumData.hasMoreElements()) {
    item = (String) enumData.nextElement();
    requestParameters += "\n  \t" + item + " = " + request.getParameter(item);
  }

  // Creamos la sesion, si no existe, y leemos sus atributos
  session = request.getSession(true);
  enumData = session.getAttributeNames();
  while (enumData.hasMoreElements()) {
    item = (String) enumData.nextElement();
    sessionAttributes += "\n  \t" + item + " = " + session.getAttribute(item);
  }
}

// Determinar el tipo de excepci�n:
// 1. InformationException: Error informativo.
// 2. XXXException: Error de sistema.
String exceptionImageFile = (exception instanceof InformationException ?
                             "information.jpg" : "error.jpg");
%>
<HTML>
<HEAD>
<TITLE></TITLE>
</HEAD>
<BODY bgcolor='#FFFFE0'>
<DIV align=center><HR>
  <FONT face="MS Serif" size=5>
    <B><FONT color='#FF0000'>Informe de Error</FONT><BR></B>
  </FONT>
</DIV><HR>
<TABLE border="0" cellpadding="1" cellspacing="1" align=center>
  <TR>
    <TD align=center>
      <IMG src='<%=BASEURL%>/images/<%= exceptionImageFile %>' width='100' height='100'>
    </TD>
    <TD align=center>
      <P align=center><%
        if( bugRptSent == null ) { %>
          Se ha producido el siguiente error:<BR><B><%= errorMessage %></B><%
        }else if( bugRptFailure != null && bugRptFailure.equals("true") ){ %>
          <B>No se pudo enviar el reporte de error a DIT.<BR>
          Si embargo, el error qued� registrado en la bit�cora del programa<BR>
          (fecha de entrada: <%= FechaHora %>).<BR>
          Notif�quele por e-mail este dato a DIT para su correspondiente revisi�n.<%
        }else{ %>
          <B>El reporte completo de este error ha sido enviado a DIT.<BR></B><%
        } %>
      </P>
    </TD>
  </TR>
</TABLE>
<FORM name='bugReportFrm' method=post
      action='<%= CONTROLLER %>?estado=BugReport&accion=Send&bugRptSent=true'>
  <TABLE border="1" cellpadding="3" cellspacing="0" align=center>
    <TR>
      <TH align=left>Fecha y hora:</TH>
      <TD>
        <%= FechaHora %>
        <INPUT type="hidden" name="bug.dateTime" value="<%= FechaHora %>">
      </TD>
    </TR>
    <TR>
      <TH align=center>Cliente Web:</TH>
      <TD>
        <%= remoteAddress %>
        <INPUT type="hidden" name="bug.remoteAddress" value="<%= remoteAddress %>">
      </TD>
    </TR>
    <TR>
      <TH align=center>Aplicaci�n:</TH>
      <TD>
        <%= servletContextName %>
        <INPUT type="hidden" name="bug.servletContext" value="<%= servletContext %>">
      </TD>
  </TR>
  </TABLE><BR><%
  if( bugRptSent == null ) { %>
    <CENTER>
      <INPUT type=button value="Haga click aqu�" name=submitBtn
             onclick='document.bugReportFrm.submit(); this.disabled = true;'>
      <P>Para enviar a&nbsp;Desarrollo e Innovaci�n Tecnol�gica (DIT)&nbsp;un e-mail 
      con un informe detallado del error.<BR><BR><HR>
    </CENTER>
    <P><TEXTAREA name='bug.fullReport' rows=1 cols=70 readonly
                 style="visibility: hidden; width: 100%">
    La excepci�n causante del error ha sido:
    <% exception.printStackTrace(new java.io.PrintWriter(out)); %>


    ATRIBUTOS DEL OBJETO REQUEST <%= requestAttributes %>


    PARAMETROS RECIBIDOS <%= requestParameters %>


    DATOS DE SESION
      Sesion activa desde: <%= new java.util.Date(session.getCreationTime()) %>
      Ultimo acceso: <%= new java.util.Date(session.getLastAccessedTime()) %>
      Atributos: <%= sessionAttributes %>
    </TEXTAREA></P><%
  }else if( bugRptFailure != null )
    request.removeAttribute("com.tsp.sot.BugReportSendAction.failure"); %>
</FORM>  
</BODY>
</HTML>

