<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">

<html xmlns="http://www.w3.org/1999/xhtml" hasBrowserHandlers="true">
    <head>
        <title>Problema al cargar la pagina</title>
        <link rel="stylesheet" href="/fintra/css/aboutNetError.css" type="text/css" media="all" />
    </head>

    <body dir="ltr">
        <div id="errorPageContainer">

            <!-- Error Title -->
            <div id="errorTitle">
                <h1 id="errorTitleText">�Vaya, ha ocurrido un error!</h1>
            </div>

            <!-- LONG CONTENT (the section most likely to require scrolling) -->
            <div id="errorLongContent">
                <!-- Long Description (Note: See netError.dtd for used XHTML tags) -->
                <div id="errorLongDesc">
                    <ul>
                        <li>El sitio esta temporalmente no disponible o muy ocupado. Intente de nuevo en unos instantes.</li>
                        <li>Si usted no puede cargar ninguna pagina, revise la conexion de red de su computador.</li>
                    </ul>
                </div>
                <br>

                <%
                    String mensaje = "No puede establecer una conexion con el servidor";
                    if (request.getAttribute("javax.servlet.error.message") != null) {
                        if ((request.getAttribute("javax.servlet.error.message").toString().equalsIgnoreCase("null"))) {
                            mensaje = request.getAttribute("javax.servlet.error.exception").toString();
                        } else {
                            mensaje = request.getAttribute("javax.servlet.error.message").toString();
                        }
                    }
                %>
                <!-- Short Description -->
                <div id="errorShortDesc">
                    <h3 ><%=request.getAttribute("javax.servlet.error.status_code")%></h3>
                    <p id="errorShortDescText"><%=mensaje%></p>
                </div>
                <br>
            </div>

            <button id="errorReload" autocomplete="off" 
                    onclick="document.location.reload(true);" 
                    autofocus="true">
                Recargar
            </button>
            <!-- Retry Button -->
            <button id="errorTryAgain" autocomplete="off" 
                    onclick="window.location.href = '/fintra/';" 
                    autofocus="false">
                Intente iniciar sesion nuevamente
            </button>
        </div>
    </body>
</html>