<%@ page session="true"%>
<%@ page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
    <head>
	
<script language="JavaScript"> 
function mueveReloj(){ 
    momentoActual = new Date() 
    hora = momentoActual.getHours() 
    
    if (hora>13){
		hora=hora-12;
    }

    minuto = momentoActual.getMinutes() 
    segundo = momentoActual.getSeconds() 

    str_segundo = new String (segundo) 
    if (str_segundo.length == 1) 
       segundo = "0" + segundo 

    str_minuto = new String (minuto) 
    if (str_minuto.length == 1) 
       minuto = "0" + minuto 

    str_hora = new String (hora) 
    if (str_hora.length == 1) 
       hora = "0" + hora 

    horaImprimible = hora + ":" + minuto + ":" + segundo 

    //document.form_reloj.reloj.value = horaImprimible 
	document.getElementById("horita").innerHTML = "&nbsp;" + horaImprimible+"&nbsp;&nbsp;";

    setTimeout("mueveReloj()",1000) 
} 
</script> 

	
        <title>Identificacion Usuario</title>
        <script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
        <script src="<%=BASEURL%>/js/inicio.js"></script>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
       
        <link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    <%
    if ( "logout".equals(request.getParameter("comando")) ){ %>
        <jsp:forward page="/logout.jsp"/>
 <% }
    %>
<%String ValorBoton = (request.getParameter("tipo") == null || request.getParameter("tipo").equals("login"))?"Ingresar":"Actualizar";
String valorTipo = (request.getParameter("tipo") == null || request.getParameter("tipo").equals("login"))?"login":"renovarClave";
String tam = (request.getParameter("tipo") == null || request.getParameter("tipo").equals("login"))?"332":"318";
%> 
    <body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" onload="validarBuscar( 'listaPerfiles', '<%=CONTROLLER%>?estado=Usuario&accion=Login', '<%=request.getParameter("perfil")%>' );login.usuario.focus();mueveReloj();">
		<table align="right">
			<tr>
				<td  class="letraresaltada">
					<div id="horita"></div>
				</td>
			</tr>
		</table>
        <br>
        <table  width="770" height="500" border="0" align="center" cellpadding="0" cellspacing="0"   background="<%= BASEURL %>/images/login/SDinicio.jpg">
            <tr>
                <td height="31" align="center" valign="top">
                <rd>  </td>
            </tr>
            <tr>
                <td height="405" align="right" valign="bottom">
                <table width="<%= tam %>" border='0'>
                <tr>
                <td width="349" height="280" valign="top">
                <!--  login-->
                <form name="login" method="post" action="<%=CONTROLLER%>?estado=Usuario&accion=Validar&cmd=show&tipo=<%=valorTipo%>" onSubmit="return validarCamposIndex('<%=valorTipo%>');">
                <input type="hidden" name="enviar" id="enviar" value="S" >
                <table align="left" width="70%" border="0">
                <tr>
                <td width="100%" height="40" colspan="4" valign="top">
					<%if ( request.getParameter("tipo") == null || request.getParameter("tipo").equals("login")){%>
                <table width="100%" border="0" cellpadding="2" cellspacing="0">
                <tr align="center" >
                <td width="45%" align="right" class="letraresaltada"><div align="left" >Compa&ntilde;&iacute;a</div></td>
                <td width="30%">
                    <select name="dstrct" class="textbox" id="dstrct" style="width:160">
                    <option selected value="FINV">FINV</option>
                    </select>
                </td>
            </tr>
                                <tr align="center" >
                                <td width="45%" class="letraresaltada"> <div align="left">Proyecto</div></td>
                                <td width="30%">
                                    <select name="proy" class="textbox" id="proy" style="width:100% ">
                                        <option value="TRSION">TRANSACCION</option>
                                    </select>
                                </td>
                                </tr>
                                <tr align="center" >
                                <td width="45%" class="msgLogin"> <div align="left" class="letraresaltada">Login </div></td>
                                <td width="30%">
								  <%String us = (request.getParameter("usuario")!=null)?request.getParameter("usuario"):"";%>
                                    <input name="usuario" id="usuario" type="text" class="textbox" style="width:100% " value="<%=us%>" size="19" maxlength="10" onblur="validarBuscar( 'listaPerfiles', '<%=CONTROLLER%>?estado=Usuario&accion=Login', '<%=request.getParameter("perfil")%>' );">
                                </td>
                                </tr>
                                <tr align="center" >
                                <td width="45%" class="letraresaltada"><div align="left">Password</div></td>
                                <td width="30%"><div align="left"><span class="Estilo3">
                                    <input name="clave" type="password" class="textbox" style="width:100% " size="19" maxlength="10">
                                </td>
                                </tr>
                                <tr align="center" >
                                <td width="45%" class="letraresaltada"> <div align="left">Perfil</div></td>
                                <td width="30%">
                                    <div id="listaPerfiles">
                                        <select name="perfil" class="textbox" id="perfil" style="width:100%">
                                            <option value="">Seleccione</option>
                                        </select>
                                        <input type="hidden" name="userlogin" id="userlogin" value="" >
                                    </div>
                                </td>
                                <td rowspan="4" >
                                    <div id="imgworking" align="right" style="visibility:hidden"><img src="<%=BASEURL%>/images/cargando.gif"></div></td>
                                </tr>                                
                    </table>
						<%}else if (request.getParameter("tipo").equals("renovarClave")){%>
    			      <input name='cambiandoClave' value='true' type='hidden'>
                            <table width="100%" border="0" cellpadding="2" cellspacing="0">
                                <tr align="center" >
                                <td width="53%" class="letraresaltada"> <div align="left">Login </div></td>
                                <td width="47%">
								  <%String us = (request.getParameter("usuario")!=null)?request.getParameter("usuario"):"";%>
                                <input name="usuario" type="text" class="textbox" style="width:100% " value="<%=request.getParameter("usuario")%>" size="16" maxlength="10" readonly>
                                <input name="perfil" type="hidden" value="<%=request.getParameter("perfil")%>"><input name="dstrct" type="hidden" value="<%=request.getParameter("dstrct")%>"></td>
                                </tr>
                                <tr align="center" >
                                <td width="53%" class="letraresaltada" nowrap><div align="left" class="letraresaltada">Clave de acceso </div></td>
                                <td width="47%"><input name="clave1" type="password" class="textbox" style="width:100% " size="19" maxlength="10">
                                </td>
                                </tr>
                                <tr align="center" >
                                <td width="53%" class="letraresaltada" nowrap> <div align="left">Nueva Clave de aceso </div></td>
                                <td width="47%"><input name="nclave" type="password" class="textbox" style="width:100% " size="16" maxlength="10">
                                </td>
                                </tr>
                                <tr align="center" >
                                <td width="53%" class="letraresaltada" nowrap> <div align="left">Confirme nueva Clave</div></td>
                                <td width="47%"><input name="cnclave" type="password" class="textbox" style="width:100% " size="19" maxlength="10"></td>
                                </tr>
                            </table>
				  <%}%></td>
                  </tr>
                            <tr>
                                <td colspan="2" width="100%" height="30" align="center">
                                    <input type="image" src="<%=BASEURL%>/images/botones/aceptar.gif"  height="21" name="imgaceptar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand; ">
                                    <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" width="100%" height="30" align="center" class="msgLogin">   <br>
                                        <div id="working" class="informacion" ></div>
					<%if ( request.getParameter("msg") != null) {%>
						<%=request.getParameter("msg")%>
					<%}%>
                                </td>
                            </tr>
                  </table>  				
                  </form>
                    <!--  ***** -->
                </td>
                </tr>
            </table></td>
            </tr>
    </table>
    </body>
</html>
