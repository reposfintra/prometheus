<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<html>
<head>
<title>Sincronizacion</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="js/validar.js"></script>
<link href="css/letras.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Estilo1 {color: #231FE5}
-->
</style>
</head>

<body>
<div align="center">
    <p class="Letras"><strong>Consulta de Sincronizacion </strong></p>
  
</div>
<%if(model.logservice.getVector()==null){
out.println("No hay vecto");
}%>
<%if(model.logservice.getVector()!=null){

	String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 17;
    int maxIndexPages = 10;
    Vector prop = model.logservice.getVector();
  	String linea = (String) prop.elementAt(0);
  %>
 <pg:pager
    items="<%=prop.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
  
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="letras">
  <tr>
    <td colspan="4"><strong>log_Sincronizacion_Servidor.txt</strong></td>
  </tr>
  <tr>
    <td height="26" colspan="4"><strong><%=linea%></strong></td>
  </tr>
  <tr>
    <td width="22%" height="9"><strong>Fecha Creacion</strong></td>
    <td width="10%"><div align="center"><strong>Proyecto</strong></div></td>
    <td width="29%"><strong>Descripcion</strong></td>
    <td width="39%"><strong>Valor</strong></td>
  </tr>
  <tr>
    <td height="9" colspan="4"><hr></td>
  </tr>
  
 
 <% for (int i = offset.intValue(),
	         l = Math.min(i + maxPageItems, prop.size());
	     i < l; i++){
 linea = (String) prop.elementAt(i);
 String vec[] = linea.split(",");
 if(vec.length>1){
 %>
 <pg:item>
  <tr>
    <td height="18"><%=vec[0]%></td>
    <td><div align="center"><%=vec[1]%></div></td>
    <td><strong><%=vec[2]%></strong></td>
    <td><%=vec[3]%></td>
  </tr>
  </pg:item>
  <%}%>
  <%}
  %>
</table>
<pg:index>
	<jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
</pg:index>
</pg:pager>
<%}%>
<strong><br>
<br>
<span class="Simulacion_Hiper"><a href="LogExcel.jsp">Exportar a un archivo en Excel...</a></span></strong>
</body>
</html>
