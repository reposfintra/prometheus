<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Sincronizacion</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.Letras {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
	font-style: normal;
	color: #000000;
}-->
</style>


</head>

<body>
<% response.setContentType("text/html");
java.io.PrintWriter oute = response.getWriter();
// lo siguiente genera el nombre del archivo dinamicamente.

boolean InternetExplorer = false;
String accept = request.getHeader("ACCEPT");

if (accept != null) {
	accept = accept.toLowerCase();
	int p = accept.indexOf("application/vnd.ms-excel");
	InternetExplorer = (p != -1);
}
response.setContentType("application/vnd.ms-excel");
if (!InternetExplorer){
	int number = (int) ((241*System.currentTimeMillis()) % 9973);
	String fileName = "0000" + number;
	fileName = fileName.substring(fileName.length() - 4);
	fileName = "exap" + fileName + ".dat";
	String contentDisposition = "attachment" + ";fileName=\"" + fileName + "\"";
	response.setHeader("Content-Disposition",contentDisposition);
}
	oute.println();
%>

<div align="center">
    <p align="left" class="Letras"><strong>FINTRAVALORES S.A<br>
      Archivo: log_Sincronizacion_Servidor.txt<br>
      Creado por Fintravalores S. A.. </strong></p>
</div>
<%if(model.logservice.getVector()==null){
out.println("NO SE ENCONTRO NINGUN ARCHIVO");
}%>
<%if(model.logservice.getVector()!=null){

	String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 17;
    int maxIndexPages = 10;
    Vector prop = model.logservice.getVector();
  	String linea = (String) prop.elementAt(0);
  %>
  
<table width="100%" border="1" align="center" cellpadding="0" cellspacing="0" class="letras">
  <tr>
    <td colspan="4">&nbsp;</td>
  </tr>
  <tr>
    <td height="26" colspan="4"><strong><%=linea%></strong></td>
  </tr>
  <tr>
    <td width="22%" height="34"><strong>Fecha Creacion</strong></td>
    <td width="10%"><div align="center"><strong>Proyecto</strong></div></td>
    <td width="29%"><strong>Descripcion</strong></td>
    <td width="39%"><strong>Valor</strong></td>
  </tr>

  
 <% for (int i = 1;
	     i < prop.size(); i++){
 linea = (String) prop.elementAt(i);
 String vec[] = linea.split(",");
 if(vec.length>1){
 %>
  <tr>
    <td height="18"><%=vec[0]%></td>
    <td><div align="center"><%=vec[1]%></div></td>
    <td><strong><%=vec[2]%></strong></td>
    <td><%=vec[3]%></td>
  </tr>
  <%}%>
  <%}
  %>
</table>
<%}%>

</body>
</html>
