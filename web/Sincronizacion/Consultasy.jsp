<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<html>
<head>
<title>Sincronizacion</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="js/validar.js"></script>
<link href="css/letras.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.Estilo1 {color: #0000FF}
.Estilo2 {color: #0000CC}
-->
</style>
</head>

<body>

  <div align="center">
    <p class="Letras"><strong>Consulta de Sincronizacion </strong></p>
  
  </div>
<div align="center">
    <% 
	Vector lista = (Vector) request.getAttribute("lista");
	Vector journal = (Vector) request.getAttribute("journal");
	%>
	<table width="70%" height="76" border="1" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="#ECE0D8">
      <tr bgcolor="#FFA928">
        <td height="21" colspan="2"><div align="center"><strong class="Letras">LISTA DE ARCHIVOS PROPERTIES </strong></div></td>
      </tr>
      <tr>
        <td width="310" height="19" bgcolor="ECE0D8">&nbsp;</td>
        <td width="207" bgcolor="ECE0D8">&nbsp;</td>
      </tr>
      <%for(int i =0; i<lista.size();i++){%>
	  <tr bgcolor="ECE0D8">
        <td valign="top"><span class="Letras"><strong><%=(String)lista.elementAt(i)%><br>
          <br>
        </strong></span></td>
        <td valign="top" bgcolor="ECE0D8"><span class="letras"><strong><a href="<%=CONTROLLER%>?estado=Leer&accion=Archivo&nombre=<%=(String)lista.elementAt(i)%>&carpeta=">Leer Archivo... </a><br>
        </strong></span></td>
	  </tr>
	  <%}%>
  </table>
    <br>
    <table width="70%" border="1" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="#ECE0D8" class="letras">
      <tr bgcolor="#FFA928">
        <td colspan="2"><div align="center"><strong>ARCHIVO DE LOG DEL SERVIDOR</strong></div></td>
      </tr>
      <tr>
        <td width="60%" bgcolor="#ECE0D8">&nbsp;</td>
        <td width="40%">&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#ECE0D8"><strong>log_Sincronizacion_Servidor.txt</strong></td>
        <td class="Estilo1" ><a style="cursor:hand " onClick="window.open('<%=CONTROLLER%>?estado=Leer&accion=Archivo');"><span class="Estilo2"><u><strong>Leer Log...</strong></u></span></a> </td>
      </tr>
  </table>
    <br>
    <table width="70%" height="76" border="1" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="#ECE0D8">
      <tr bgcolor="#FFA928">
        <td height="21" colspan="3"><div align="center"><strong class="Letras">LISTA DE ARCHIVOS JOURNAL </strong></div></td>
      </tr>
      <tr>
        <td width="383" height="19" bgcolor="ECE0D8">&nbsp;</td>
        <td colspan="2" bgcolor="ECE0D8">&nbsp;</td>
      </tr>
      <%for(int i =0; i<journal.size();i++){%>
      <tr bgcolor="ECE0D8">
        <td valign="top"><span class="Letras"><strong><%=(String)journal.elementAt(i)%><br>
                <br>
        </strong></span></td>
        <td width="160" valign="top" bgcolor="ECE0D8"><span class="letras"><strong><a class="Estilo2" style="cursor:hand; color: #0000FF;" onClick="window.open('<%=CONTROLLER%>?estado=Leer&accion=Archivo&archivo=<%=(String)journal.elementAt(i)%>')"><u>Leer Archivo... </u></a><br>
        </strong></span></td>
        <td width="137" valign="top" bgcolor="ECE0D8"><span class="letras"><strong><a class="Estilo2" style="cursor:hand; color: #0000FF;" onClick="window.open('<%=CONTROLLER%>?estado=Subir&accion=Archivo&archivo=<%=(String)journal.elementAt(i)%>')"><u>Subir Archivo...</u></a></strong></span></td>
      </tr>
      <%}%>
  </table>
    <br>
<span class="letras"></span></div>
  

<br>

<%if(request.getAttribute("archivo")!=null){%>
<table width="81%" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="#ECE0D8" class="letras">
  <tr bgcolor="#FFA928">
    <td colspan="2"><strong><%=request.getParameter("nombre").toUpperCase()%></strong></td>
  </tr>
  <%Vector prop = (Vector) request.getAttribute("archivo");
  for(int i =0; i<prop.size();i++){
  	String linea = (String) prop.elementAt(i);
	String vec[] = linea.split(";");
  %>
  <tr bgcolor="#ECE0D8">
    <td height="18"><strong><u><%=vec[0]%></u></strong></td>
    <td><%=vec[1]%></td>
  </tr>
  <%}%>
</table>
<%}%>
<br>
</body>
</html>
