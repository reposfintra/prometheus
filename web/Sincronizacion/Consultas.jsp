<%@ page session="true"%>

<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<html>
<head>
<title>Sincronizacion</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="js/validar.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Consulta de Sincronización"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<div align="center">
    <table width="75%"  border="2">
      <tr>
        <td><table width="100%">
          <tr>
            <td width="373" class="subtitulo1">Lista de Archivos Properties </td>
            <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
          </tr>
        </table>
          <% 
	Vector lista = (Vector) request.getAttribute("lista");
	Vector journal = (Vector) request.getAttribute("journal");
	%>  
          <table width="100%" height="76" border="0" class="tablaInferior">
          <tr class="fila">
            <td width="310" height="19" >&nbsp;</td>
            <td width="207" >&nbsp;</td>
          </tr>
          <%for(int i =0; i<lista.size();i++){%>
          <tr  class="fila">
            <td valign="top"><span class="Letras"><strong><%=(String)lista.elementAt(i)%><br>
                    <br>
            </strong></span></td>
            <td valign="top" ><span class="letras"><strong><a href="<%=CONTROLLER%>?estado=Leer&accion=Archivo&nombre=<%=(String)lista.elementAt(i)%>&carpeta=" class="Simulacion_Hiper">Leer Archivo... </a><br>
            </strong></span></td>
          </tr>
          <%}%>
        </table>
          <table width="100%">
            <tr>
              <td width="373" class="subtitulo1">Archivo del Log del Servidor </td>
              <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
            </tr>
          </table>          
          <table width="100%" border="0" class="tablaInferior">
            <tr class="fila">
              <td width="60%" >&nbsp;</td>
              <td width="40%">&nbsp;</td>
            </tr>
            <tr class="fila">
              <td ><strong>log_Sincronizacion_Servidor.txt</strong></td>
              <td class="Simulacion_Hiper" ><a style="cursor:hand " onClick="window.open('<%=CONTROLLER%>?estado=Leer&accion=Archivo');"><span class="Estilo2"><u><strong>Leer Log...</strong></u></span></a> </td>
            </tr>
          </table>                  
        <table width="100%">
          <tr>
            <td width="373" class="subtitulo1">Lista de Archivos Journal </td>
            <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
          </tr>
        </table>        
        <table width="100%" height="76" border="0">
          <tr>
            <td width="383" height="19" class="fila" >&nbsp;</td>
            <td colspan="2" class="fila" >&nbsp;</td>
          </tr>
          <%for(int i =0; i<journal.size();i++){%>
          <tr class="fila" >
            <td valign="top"><span class="Letras"><strong><%=(String)journal.elementAt(i)%><br>
                    <br>
            </strong></span></td>
            <td width="160" valign="top" ><span class="letras"><strong><a class="Simulacion_Hiper" style="cursor:hand; color: #0000FF;" onClick="window.open('<%=CONTROLLER%>?estado=Leer&accion=Archivo&archivo=<%=(String)journal.elementAt(i)%>')"><u>Leer Archivo... </u></a><br>
            </strong></span></td>
            <td width="137" valign="top" ><span class="Simulacion_Hiper"><strong><a class="Estilo2" style="cursor:hand; color: #0000FF;" onClick="window.open('<%=CONTROLLER%>?estado=Subir&accion=Archivo&archivo=<%=(String)journal.elementAt(i)%>')"><u>Subir Archivo...</u></a></strong></span></td>
          </tr>
          <%}%>
        </table>        </td>
      </tr>
    </table>
  <p class="Letras">&nbsp;</p>
</div>
<%if(request.getAttribute("archivo")!=null){%>
<table width="81%"  border="2" align="center">
  <tr>
    <td><table width="100%" border="0" align="center" class="letras">
      <tr>
        <td width="47%" class="subtitulo1"><strong><%=request.getParameter("nombre").toUpperCase()%></strong></td>
        <td width="53%" class="barratitulo"><span class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></span></td>
      </tr>
      <%Vector prop = (Vector) request.getAttribute("archivo");
  for(int i =0; i<prop.size();i++){
  	String linea = (String) prop.elementAt(i);
	String vec[] = linea.split(";");
  %>
      <tr class="fila" >
        <td height="18"><strong><u><%=vec[0]%></u></strong></td>
        <td><%=vec[1]%></td>
      </tr>
      <%}%>
    </table></td>
  </tr>
</table>
<%}%>
<br>
</div>
</body>
</html>
