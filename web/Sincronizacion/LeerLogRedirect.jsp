<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<html>
<head>
<title>Sincronizacion</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="js/validar.js"></script>
<link href="css/letras.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.Estilo1 {color: #0000FF}
.Estilo2 {color: #0000CC}
-->
</style>
</head>

<body>
<%
    if(request.getParameter("journal")==null){
		response.sendRedirect("Sincronizacion/LeerLog.jsp");
	}
	else{
		String journal = request.getParameter("journal");
    	response.sendRedirect("Sincronizacion/LeerJournal.jsp?journal="+journal);
	}
%>

</body>
</html>
