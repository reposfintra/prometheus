<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<html>
<head>
<title>Sincronizacion</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="js/validar.js"></script>
<link href="css/letras.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Estilo1 {color: #231FE5}
-->
</style>
</head>

<body>
<div align="center">
    <p class="Letras"><strong>Consulta de Sincronizacion </strong></p>
  
</div>
<%if(model.logservice.getVector()==null){
out.println("No hay vector");
}%>
<%if(model.logservice.getVector()!=null){

	String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 17;
    int maxIndexPages = 10;
    Vector prop = model.logservice.getVector();
   String linea;
  %>
<pg:pager
    items="<%=prop.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
  
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="letras">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td height="26"><strong><%=model.logservice.getNombre()%></strong></td>
  </tr>
  <tr>
    <td height="9"><hr></td>
  </tr>
  
 
 <% for (int i = offset.intValue(),
	         l = Math.min(i + maxPageItems, prop.size());
	     i < l; i++){
 linea = (String) prop.elementAt(i);
%>
 <pg:item>
  <tr>
    <td height="18"><%=linea%>      <div align="center"></div>      </td>
  </tr>
  </pg:item>
 <%}
  %>
</table>
<pg:index>
	<jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/> 
</pg:index>
</pg:pager>
 <p>
     <%}%>
     <strong><br>
     <br>
</strong></p>
 <p>&nbsp;</p>
</body>
</html>
