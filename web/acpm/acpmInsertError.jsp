<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Ingresar proveedor anticipo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="js/validar.js"></script>
<script src="js/boton.js"></script>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Acpm&accion=Insert&cmd=show">
<table width="530" border="2" align="center">
    <tr>
      <td>
	  <table width="99%" align="center">
              <tr>
                <td width="373" class="subtitulo1">Informaci&oacute;n Provvedor ACPM </td>
                <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
        </table>
  <table width="99%" align="center" class="Letras">
    <tr class="fila"  >
      <td width="172" nowrap  ><strong>Nit:</strong></td>
      <td width="317" nowrap bgcolor="<%=request.getAttribute("nit")%>"><input name="nit" type="text" class="textbox" id="nit" value="<%=request.getParameter("nit")%>" maxlength="15">          </td>
    </tr>
    <tr class="fila"  >
      <td nowrap  ><strong>Sucursal: </strong></td>
      <td width="317" nowrap bgcolor="<%=request.getAttribute("sucursal")%>"><input name="sucursal" type="text" class="textbox" id="sucursal" value="<%=request.getParameter("sucursal")%>" maxlength="15"></td>
    </tr>
    <tr class="fila"  >
      <td nowrap  ><strong>Distrito:</strong></td>
      <td nowrap  ><select name="distrito" class="listmenu" id="distrito">
        <option value="FINV" selected>FINV</option>
      </select></td>
    </tr>
    <tr class="fila"  >
      <td nowrap  ><strong>Ciudad:</strong></td>
      <td nowrap  >
	     <%TreeMap ciudades = model.ciudadService.getCiudades(); %>
	  <input:select name="ciudad" options="<%=ciudades%>" attributesText="style='width:100%;'" default="<%=request.getParameter("ciudad")%>"/></td>
    </tr>
    <tr class="fila"  >
      <td nowrap  ><strong>Tipo de Servicio:</strong></td>
      <td nowrap  ><input name="tipoe" type="checkbox" id="tipoe" onClick="llenarInfo();" value="E" checked>
        <strong>Efectivo</strong>        <input name="tipoa" type="checkbox" id="tipoa" onClick="llenarInfo();" value="G" checked>
          <strong>          ACPM</strong></td>
    </tr>
    <tr class="fila"  >
      <td nowrap  ><strong>Valor ACPM:</strong></td>
      <td nowrap  ><input name="acpm" type="text" class="textbox" id="acpm" value="<%=request.getParameter("acpm")%>" maxlength="10"></td>
    </tr>
    <tr class="fila"  >
      <td nowrap  ><strong>Efectivo M&aacute;ximo:</strong></td>
      <td nowrap  ><input name="emax" type="text" class="textbox" id="emax" value="<%=request.getParameter("emax")%>" maxlength="13"></td>
    </tr>
    <tr class="fila"  >
      <td nowrap  ><strong>Moneda:</strong></td>
      <td nowrap  ><select name="moneda" class="listmenu" id="moneda">
        <%if("PES".equals(request.getParameter("moneda"))){%>
		<option value="PES" selected>Pesos</option>
        <%}
		else{
		%>
		<option value="PES" >Pesos</option>
		<%}%>
		 <%if("DOL".equals(request.getParameter("moneda"))){%>
		<option value="DOL" selected>Dolar</option>
		<%}else{%>
		<option value="DOL">Dolar</option>
		<%}%>
		<%if("BOL".equals(request.getParameter("moneda"))){%>
        <option value="BOL" selected>Bolivar</option>
		<%}else{%>
		<option value="BOL">Bolivar</option>
		<%}%>
      </select></td>
    </tr>
    <tr class="fila"  >
      <td nowrap  ><strong>Codigo de Migracion:</strong></td>
      <td nowrap  ><input name="codigo_m" type="text" class="textbox" id="codigo_m" value="<%=request.getParameter("codigo")%>" size="3" maxlength="1"></td>
    </tr>
    <tr class="fila"  >
      <td nowrap  ><strong>Porcentaje :</strong></td>
      <td nowrap  ><input name="porcentaje" type="text" class="textbox" id="porcentaje" value="<%=request.getParameter("porcentaje")%>" size="5" maxlength="3">
        %</td>
    </tr>
  </table>
  </td>
  </tr>
  </table>
  <br>
  <div align="center">  <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod"  height="21" onClick="return ValidarFormularioACPM(form1);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp; <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="form1.reset();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></div>
  <table width="530" border="1" align="center"   class="Letras">
  </table>
</form>
<%String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";%>
<%if(!msg.equals("OK") && !msg.equals("")){%>
	<p>
   <table border="2" align="center">
     <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"> <%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
   <%}%>

</body>
</html>
