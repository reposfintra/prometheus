<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page contentType="text/html"%>
<%@taglib uri="/WEB-INF/tlds/tagsAgencia.tld" prefix="ag"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>



<%String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";%>
<html>
<head>
<title>Ingresar proveedor ACPM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="<%=BASEURL%>/js/validar.js"></script>
<script src="<%=BASEURL%>/js/boton.js"></script>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">

</head>

<body>
 <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=Ingresar Proveedor ACPM"/>
    </div>
 <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top:100px; overflow: scroll;">        
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Acpm&accion=Insert&cmd=show">
<table border="2" align="center" width="468">
  <tr>
    <td><span class="Estilo1"></span>
<table width="99%" align="center">
  <tr>
    <td  class="subtitulo1" width="50%"><p align="left">Proveedor ACPM </p></td>
    <td  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
  <table width="99%" align="center" class="Letras">
    <tr  class="fila">
      <td width="173" nowrap ><strong>Nit:</strong></td>
      <td width="221" nowrap><input name="nit" type="text" class="textbox" id="nit" size="18" maxlength="15">          </td>
    </tr>
    <tr  class="fila">
      <td width="173" nowrap ><strong>Sucursal: </strong></td>
      <td nowrap ><input name="sucursal" type="text" class="textbox" id="sucursal" size="25" maxlength="15"></td>
    </tr>
    <tr  class="fila">
      <td nowrap ><strong>Distrito:</strong></td>
      <td nowrap >
	  <select name="distrito" class="listmenu" id="distrito">
	  <%List list  = model.ciaService.getList();
	  	Iterator it=list.iterator();
		while (it.hasNext()){
			Compania c = (Compania) it.next();
	  %>
        <option value="<%=c.getdstrct()%>"><%=c.getdstrct()%></option>
		<%}%>
      </select></td>
    </tr>
    <tr  class="fila">
      <td nowrap ><strong>Ciudad:</strong></td>
      <td nowrap >
	     <%TreeMap ciudades = model.ciudadService.getCiudades(); %>
	  <input:select name="ciudad" options="<%=ciudades%>" attributesText="style='width:100%;' class='listmenu'" /></td>
    </tr>
    <tr  class="fila">
      <td nowrap ><strong>Tipo de Servicio:</strong></td>
      <td nowrap ><input name="tipoe" type="radio" value="E" checked>        
      <strong>Efectivo</strong>
      <input name="tipoe" type="radio" value="G">        <strong>          ACPM</strong></td>
    </tr>
    <tr  class="fila">
      <td nowrap ><strong>Valor ACPM:</strong></td>
      <td nowrap ><input name="acpm" type="text" class="textbox" id="acpm" size="10" maxlength="6" onKeyPress="soloDigitos(event,'decNO')"></td>
    </tr>
    <tr  class="fila">
      <td nowrap ><strong>Efectivo M&aacute;ximo:</strong></td>
      <td nowrap ><input name="emax" type="text" class="textbox" id="emax" size="12" maxlength="8" onKeyPress="soloDigitos(event,'decNO')"></td>
    </tr>
    <tr  class="fila">
      <td nowrap ><strong>Moneda:</strong></td>
      <td nowrap ><select name="moneda" class="listmenu" id="moneda">
        <option value="PES" selected>Pesos</option>
        <option value="DOL">Dolar</option>
        <option value="BOL">Bolivar</option>
      </select></td>
    </tr>
    <tr  class="fila">
      <td nowrap ><strong>Codigo de Migracion:</strong></td>
      <td nowrap ><input name="codigo_m" type="text" class="textbox" id="codigo_m" size="3" maxlength="1" ></td>
    </tr>
    <tr  class="fila">
      <td nowrap ><strong>Porcentaje sobre anticipo:</strong></td>
      <td nowrap ><input name="porcentaje" type="text" class="textbox" id="porcentaje" value="0" size="3" maxlength="3" onKeyPress="soloDigitos(event,'decOK')">
      </td>
    </tr>
  </table>  
  </td>
  </tr>
  </table>
  <br>
  <div align="center">    <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod"  height="21" onClick="return ValidarFormularioACPM(form1);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp; 
	<img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="form1.reset();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; 
	<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></div>
</form>

<%if(!msg.equals("")){%>
	<p>
   <table border="2" align="center">
     <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"> <%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
   <%}%>
</div>


</body>
</html>
