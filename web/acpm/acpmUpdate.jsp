<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Modificar proveedor ACPM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">

</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=Modificar Proveedor ACPM"/>
    </div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top:100px; overflow: scroll;">        
 <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
 <table border="2" align="center" width="468">
  <tr>
    <td>
<table width="100%" align="center">
  <tr>
    <td width="50%"  class="subtitulo1"><p align="left">Seleccionar Proveedor </p></td>
    <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>

  <table width="530" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4" class="Letras">
    <tr   class="tblTitulo">
      <td width="203" nowrap  ><strong>NIT</strong></td>
      <td width="311" nowrap  ><strong>SUCURSAL</strong></td>
    </tr>
	 <div align="left">
        <%if(model.proveedoracpmService.existProveedoresAcpm(usuario.getDstrct())){
			List list = model.proveedoracpmService.getProveedoresACPM(usuario.getDstrct());
			int i=0;
			Iterator it=list.iterator();
				while (it.hasNext()){%>
        
					
					  <%Proveedor_Acpm pa = (Proveedor_Acpm) it.next();
					
					String  nit= pa.getNit();
					String desc=pa.getNombre();
					String codigo=pa.getCodigo();%>
    </div>
    <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" title="Modificar Proveedor de Acpm..." onClick="window.location='<%=CONTROLLER%>?estado=Acpm&accion=Search&num=1&nit=<%=nit%>&codigo=<%=codigo%>'" >

	  <td nowrap class="bordereporte"><div align="left"><%=desc%></div></td>
	  <td nowrap class="bordereporte"><div align="left"><%=codigo%></div></td></tr>
	 <div align="left">
       <%}
	  }%>
     </div>
  </table>
  </td>
  </tr>
  </table>


<form name="form2" method="post" action="<%=CONTROLLER%>?estado=Acpm&accion=Update&cmd=show">
  <%String nit="";
  	String distrito="", cciudad="", tipo="", moneda="", nombre="", codigo="";
	float  acpm=0, e_max=0;
 	if(request.getAttribute("pacpm")!=null){
     	Proveedor_Acpm pacpm= (Proveedor_Acpm)request.getAttribute("pacpm");
		nit=pacpm.getNit();
        cciudad=pacpm.getCity_code();
        e_max=pacpm.getMax_e();
        moneda=pacpm.getMoneda();
        tipo=pacpm.getTipo();
        acpm=pacpm.getValor();
		nombre=pacpm.getNombre();
		codigo=pacpm.getCodigo();
				
	}%>
  <table border="2" align="center" width="430">
  <tr>
    <td>
<table width="99%" align="center">
  <tr>
    <td width="50%"  class="subtitulo1"><p align="left">Informaci&oacute;n Proveedor ACPM </p></td>
    <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
  <table width="430" align="center">
    <tr class="fila"  >
      <td width="157" rowspan="2" nowrap  ><strong>Nit:</strong></td>
      <td width="249" nowrap>      <%=nit%>
        <input name="codigo" type="hidden" id="codigo" value="<%=codigo%>">
      <input name="nit" type="hidden" id="nit" value="<%=nit%>" ></td>
    </tr>
    <tr  class="fila">
      <td nowra ><%=nombre%></td>
    </tr>
    <tr class="fila"  >
      <td nowrap  ><strong>Distrito:</strong></td>
      <td nowrap class="letrafila"  ><select name="distrito" class="listmenu" id="distrito">
        <option value="FINV" selected>FINV</option>
      </select></td>
    </tr>
    <tr class="fila"  >
      <td nowrap  ><strong>Ciudad:</strong></td>
      <td nowrap class="letrafila"  >
	     <%TreeMap ciudades = model.ciudadService.getCiudades(); %>
	  <input:select name="ciudad" options="<%=ciudades%>" attributesText="style='width:100%;'  class='listmenu'" default="<%=cciudad%>"/></td>
    </tr>
    <tr class="fila"  >
      <td nowrap  ><strong>Tipo de Servicio:</strong></td>
      <td nowrap class="letrafila"  >
	  <%if(tipo.equals("E")){%>
      <strong>Efectivo</strong>
	  <%}else if(tipo.equals("G")){%>        
    <strong>          ACPM</strong>
	<%}%>
	</td>
    </tr>
    <tr class="fila"  >
      <td nowrap  ><strong> Valor ACPM:
      </strong></td>
      <td nowrap class="letrafila"  ><strong>
      </strong>        <input name="acpm" type="text" class="textbox" id="acpm" value="<%=acpm%>" maxlength="10">
      <strong>      </strong>      <strong>      </strong></td>
    </tr>
    <tr class="fila"  >
      <td nowrap  ><strong>Efectivo M&aacute;ximo:</strong></td>
      <td nowrap class="letrafila"  ><input name="emax" type="text" class="textbox" id="emax" value="<%=e_max%>"></td>
    </tr>
    <tr class="fila"  >
      <td height="32" nowrap  ><strong>Moneda:</strong></td>
      <td nowrap class="letrafila"  ><select name="moneda" class="listmenu" id="moneda">
        <%if(moneda.equals("PES")){%>
		<option value="PES" selected>Pesos</option>
        <%}
		else{
		%>
		<option value="PES" >Pesos</option>
		<%}%>
		 <%if(moneda.equals("DOL")){%>
		<option value="DOL" selected>Dolar</option>
		<%}else{%>
		<option value="DOL">Dolar</option>
		<%}%>
		<%if(moneda.equals("BOL")){%>
        <option value="BOL" selected>Bolivar</option>
		<%}else{%>
		<option value="BOL">Bolivar</option>
		<%}%>
      </select></td>
    </tr>
  </table>
  </td>
  </tr>
  </table>
  <%String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";%>
<%if(!msg.equals("OK") && !msg.equals("")){%>
	<p>
   <table width="444" border="2" align="center">
     <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"> <%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
   <%}%>
  <%if(request.getAttribute("pacpm")!=null){%>
  <div align="center"><br>
    <img src="<%=BASEURL%>/images/botones/modificar.gif" name="mod"  height="21" onClick="ValidarACPM();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"> </div>
  <%}%>
  <table width="530" border="1" align="center"   class="Letras">
  </table>
</form>

</div>
</body>
</html>
<script>
//Modificacion del script de validacion 2005-11-08 Henry 
function ValidarACPM() {
	if (isNaN(form2.acpm.value)){
	    alert("el valor del campo acpm no es valido");
	} else 	if (isNaN(form2.emax.value)){
	    alert("el valor del campo acpm no es valido");
	} else { 
	    form2.submit();
	}
}
</script>