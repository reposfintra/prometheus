<%@ page session="true"%>
<%@ page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Eliminar proveedor ACPM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body>

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=Eliminar Proveedor ACPM"/>
    </div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top:100px; overflow: scroll;">
<%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
<table border="2" align="center" width="468">
  <tr>
    <td><span class="Estilo1"></span>
<table width="100%" border="1" align="center">
  <tr>
    <td width="50%"  class="subtitulo1"><p align="left">Seleccionar Proveedor </p></td>
    <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4" class="Letras">
  <tr class="tblTitulo" >
    <td width="162" nowrap ><strong>NIT</strong></td>
    <td width="352" nowrap ><strong>SUCURSAL</strong></td>
  </tr>
  <%if(model.proveedoracpmService.existProveedoresAcpm(usuario.getDstrct())){
			List list = model.proveedoracpmService.getProveedoresACPM(usuario.getDstrct());
			int i=0;
			Iterator it=list.iterator();
				while (it.hasNext()){%>
  <%Proveedor_Acpm pa = (Proveedor_Acpm) it.next();
					
					String  nit= pa.getNit();
					String desc=pa.getNombre();
					String codigo=pa.getCodigo();%>
  <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" title="Anular Proveedor de Acpm..." onClick="window.location='<%=CONTROLLER%>?estado=Acpm&accion=Search&num=2&nit=<%=nit%>&codigo=<%=codigo%>'" >
    <td nowrap class="bordereporte"><%=desc%></td>
    <td nowrap class="bordereporte"><%=codigo%></td>
  </tr>
  <%}
	  }%>
</table>
</td>
</tr>
</table>
<br>

<form name="form2" method="post" action="<%=CONTROLLER%>?estado=Acpm&accion=Delete&cmd=show">
  <%String nit="";
  	String distrito="", cciudad="", tipo="", moneda="", nombre="", codigo="";
	float  acpm=0, e_max=0;
 	if(request.getAttribute("pacpm")!=null){
     	Proveedor_Acpm pacpm= (Proveedor_Acpm)request.getAttribute("pacpm");
		nit=pacpm.getNit();
        cciudad=pacpm.getCity_code();
        e_max=pacpm.getMax_e();
        moneda=pacpm.getMoneda();
        tipo=pacpm.getTipo();
        acpm=pacpm.getValor();
		nombre=pacpm.getNombre();
		codigo=pacpm.getCodigo();
		distrito=pacpm.getDstrct();
				
	}%>
 <table border="2" align="center" width="477">
  <tr>
    <td>
<table width="99%" align="center">
  <tr>
    <td width="50%"  class="subtitulo1"><p align="left">Informaci&oacute;n Proveedor ACPM </p></td>
    <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
  <table width="99%" align="center" >
    <tr class="fila" >
      <td width="234" rowspan="2" nowrap class="letraresaltada" >Nit:</td>
      <td width="210" nowrap class="letrafila" >      <%=nit%>
        <input name="codigo" type="hidden" id="codigo" value="<%=codigo%>">
      <input name="nit" type="hidden" id="nit" value="<%=nit%>" ></td>
    </tr>
    <tr class="fila">
      <td nowrap ><%=nombre%></td>
    </tr>
    <tr class="fila" >
      <td nowrap class="letraresaltada" ><strong>Distrito:</strong></td>
      <td nowrap><%=distrito%></td>
    </tr>
    <tr class="fila" >
      <td nowrap class="letraresaltada" ><strong>Ciudad:</strong></td>
      <td nowrap ><%=cciudad%> </td>
    </tr>
    <tr class="fila" >
      <td nowrap class="letraresaltada" ><strong>Tipo de Servicio:</strong></td>
      <td nowrap class="letrafila" >
	  <%if(tipo.equals("E")){%>
      Efectivo
	  <%}else if(tipo.equals("G")){%>        
        ACPM
	<%}%>
	</td>
    </tr>
    <tr class="fila" >
      <td nowrap class="letraresaltada" ><strong> <%if(tipo.equals("G")){%>Valor ACPM:<%}else if(tipo.equals("E")){%>Efectivo M&aacute;ximo:<%}%></strong></td>
      <td nowrap class="letrafila" >        <%if(tipo.equals("G")){%>        <%=acpm%>        <%}else if(tipo.equals("E")){%>        <%=e_max%>        <%}%>      </td>
    </tr>
    <tr class="fila" >
      <td nowrap class="letraresaltada" ><strong>Moneda:</strong></td>
      <td nowrap class="letrafila" ><%=moneda%></td>
    </tr>
  </table>
  </td>
  </tr>
  </table>
  <%String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";%>
<%if(!msg.equals("OK") && !msg.equals("")){%>
	<p>
   <table width="416" border="2" align="center">
     <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"> <%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
   <%}%>

  <%if(request.getAttribute("pacpm")!=null){%>
  <div align="center"><br>
    <img src="<%=BASEURL%>/images/botones/anular.gif" name="mod"  height="21" onClick="form2.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; 
	<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"> </div>
  <%}%>
  <table width="530" border="1" align="center" bgcolor="ECE0D8" class="Letras">
  </table>
</form>
</div>
</body>
</html>
