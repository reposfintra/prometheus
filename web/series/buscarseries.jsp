<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head><title>Buscar Series</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
     <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validar.js"></script>
     <script language=JavaScript1.2 
         src="<%=BASEURL%>/js/coolmenus3.js">
     </script>
	 <script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
     <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body>
<%
List Lista = model.SeriesSvc.getRegistros();
Iterator It;
String Mensaje = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
String ciudad="";
String documento="";
if(Lista.size()>0){
    
    Series dat = (Series)Lista.get(0); 
    ciudad=dat.getNombre_Ciudad();
    documento=dat.getNombre_Documento();
	
	
}	
String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
String dat[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
	
%>
<script>
function botonaceptar (frm){
     
     if(frm.Documento.value != 'S~'){
        document.formulario.action = "<%= CONTROLLER %>?estado=Series&accion=Opciones&cmd=show"; 
        document.formulario.submit(); 
    }else{
      alert('Debe seleccionar un documento.')
    }    

}
</script>
<br>
<FORM name='formulario' id='formulario' method='POST' >

    <table width="600" border="2" align="center">
      <tr>
        <td><table width="100%"  class="tablaInferior">
            <tr>
              <td colspan="4"><table width="100%" border="0" cellspacing="0"  bordercolor="#F7F5F4" >
                  <tr>
                    <td>
                      <table width="100%"  border="0">
                        <tr>
                          <td width="23%" class="subtitulo1">Buscar Serie</td>
                          <td width="77%" class="barratitulo" align="left"><%=dat[0]%> <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                      </table></td>
                  </tr>
              </table></td>
            </tr>

            <tr class="fila" id="fechas">
              <td width="50" align="left" >Ciudad </td>
              <td width="228" valign="middle" ><select name='Ciudad' style='width:70%;' class="textbox">
                                <%
                                Lista = model.SeriesSvc.getCiudades();
                                It = Lista.iterator();
                                while(It.hasNext()) {
                                    Ciudad datos = (Ciudad) It.next();
                                    out.print("<option value='"+datos.getCodigo()+"'"+(datos.getNombre().toUpperCase().equals(ciudad)?" selected ":"")+">"+datos.getNombre().toUpperCase()+"</option>\n");
                                }
                                %>                                
                            </select></td>
              <td width="130" valign="middle">Tipo de Documento </td>
              <td width="146" valign="middle"><select name='Documento' style='width:95%;' class="textbox">
                                <%
                                Lista = model.SeriesSvc.getDocumento();
                                It = Lista.iterator();
                                while(It.hasNext()) {
                                    TipoDocumento datos = (TipoDocumento) It.next();
									out.print("<option value='"+datos.getTipo()+"'"+(datos.getNombre().toUpperCase().equals(documento)?" selected ":"")+">"+datos.getNombre().toUpperCase()+"</option>\n");
                                }
                                %>                                
                            </select></td>
            </tr>
            <tr class="fila">
              <td colspan="6" align="center">
                <input class='boton' type="hidden" name='Opcion' value='Buscar'>              </td>
          </tr>
        </table></td>
      </tr>
  </table>
 
    <br>
    <div align="center" >
	 <div align="center" style="vertical-align:top " >
	  
	  <img src="<%=BASEURL%>/images/botones/buscar.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="botonaceptar (formulario)" onMouseOut="botonOut(this);" style="cursor:hand">
      
	  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
      </div>
	  <%  String style = "simple";
    String position =  "bottom";
    String index =  "center";
	String tipo = "";
    int maxPageItems = 5;
    int maxIndexPages = 5;
if(model.SeriesSvc.getRegistros().size()>0)
{
%>
    </div>
    <table width="1000" border="2" align="center">
  <tr>
    <td>
      <table width="99%" align="center">
        <tr class="barratitulo">
          <td><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
            <tr>
              <td width="157" class="subtitulo1">&nbsp;Series</td>
              <td width="801" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
            </tr>
          </table>            </td>
          </tr>
      </table>
      <table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
        <tr class="tblTitulo">
          <td width="6%" align="center">Id</td>
          <td width="6%" align="center">Distrito</td>
          
          <td width="11%" align="center">Banco</td>
          <td width="16%" align="center">Agencia Banco </td>
          <td width="12%" align="center">Cuenta</td>
          <td width="6%" align="center">Concepto</td>
          <td width="6%" align="center">Prefijo</td>
          <td width="8%" align="center">N&ordm; Inicial </td>
          <td width="7%" align="center">N&ordm; Final </td>
          <td width="7%" align="center">Ultimo Numero </td>
        </tr>
        <pg:pager
         items="<%=model.SeriesSvc.getRegistros().size()%>"
         index="<%= index %>"
         maxPageItems="<%= maxPageItems %>"
        maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
     export="offset,currentPageNumber=pageNumber"
    scope="request">
	
    <%
    Lista = model.SeriesSvc.getRegistros();
	System.out.println("paginacion:"+Lista.size());
    It = Lista.iterator();
    int i=0;
    for (int j = offset.intValue(),
	  l = Math.min(j + maxPageItems, Lista.size());j < l; j++){
        Series datos = (Series) Lista.get(j); %>
        <pg:item>
        <tr class="<%=(j % 2 == 0 )?"filagris":"filaazul"%>"    style="cursor:hand" onClick="window.location=('<%=CONTROLLER%>?estado=Series&accion=Opciones&cmd=show&Opcion=Seleccionar&Distrito=<%=datos.getDistrito()%>&Ciudad=<%=datos.getCodigo_Ciudad()%>&Documento=*~<%=datos.getCodigo_Documento()%>&Banco=<%=datos.getBanco()%>&Agencia=<%=datos.getAgencia_Banco()%>&Cuenta=<%=datos.getCuenta()%>&Prefijo=<%=datos.getPrefijo()%>&Serie_Inicial=<%=datos.getN_Inicial()%>&Serie_Final=<%=datos.getN_Final()%>&Ultimo_Numero=<%=datos.getL_Numero()%>&Estado=Disabled&concept_code=<%=datos.getConcepto()%>&id=<%= datos.getId()%>');">
          <td class="bordereporte" align="center"><%=datos.getId() %></td>
          <td class="bordereporte" align="center">&nbsp;<%=datos.getDistrito()%></div></td>
          <td width="11%" class="bordereporte" align="center">&nbsp;<%=datos.getBanco()%></td>
          <td width="16%" class="bordereporte" align="center">&nbsp;<%=datos.getAgencia_Banco()%></td>
          <td width="12%" class="bordereporte" align="center">&nbsp;<%=datos.getCuenta()%></td>
          <td width="6%" class="bordereporte" align="center">&nbsp;<%=datos.getConcepto()%></td>
          <td width="6%" class="bordereporte" align="center">&nbsp;<%=datos.getPrefijo()%></td>
          <td width="8%" class="bordereporte" align="center">&nbsp;<%=datos.getN_Inicial()%></td>
          <td width="7%" class="bordereporte" align="center">&nbsp;<%=datos.getN_Final()%></td>
          <td width="7%" class="bordereporte" align="center">&nbsp;<%=datos.getL_Numero()%></td>
        </tr>
        </pg:item>
        <%}%>
        <tr  bordercolor="#E6E6E6">
          <td td height="20" colspan="14" nowrap align="center"> 
		  <pg:index>
            <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
          </pg:index> </td>
        </tr>
        </pg:pager>
    </table></td>
  </tr>
</table>
<br>
<!--<center><input type='submit' name='Opcion' value='Anular' class='boton' style='width:100;'></center>-->
<%
}if(!Mensaje.equals("")){
%>
<table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<%}
Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
<input type='hidden' name='Usuario' value='<%=usuario.getLogin()%>'/> 
</FORM>
<%=dat[1]%>
</body>
</html>
