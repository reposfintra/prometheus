<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head><title>Buscar Series</title>
    <link href="<%= BASEURL %>/css/StyleM.css" rel='stylesheet'>
     <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validar.js"></script>
     <script language=JavaScript1.2 
         src="<%=BASEURL%>/js/coolmenus3.js">
     </script>
</head>
<body>
<%
List     Lista;
Iterator It;
%>
<br>
<FORM name='formulario' id='formulario' method='POST' onsubmit='jscript:return verificar();' action='<%=CONTROLLER%>?estado=Series&accion=Opciones&cmd=show'>

    <table width='600' border='1' align='center' cellpadding="3" cellspacing="2" bordercolor="#CCCCCC">
        <tr class='titulo1'>
            <td colspan='2' align='center' height='50'>BUSCAR SERIES</td>
        </tr>        
        <tr>
            <td colspan='2' class='fondotabla'>                                  
                <table width='100%' border='1' align='center' cellpadding='3' cellspacing='2' bordercolor="#CCCCCC">                    
                    <tr>
                        <td width='10%' class='comentario'>CIUDAD</td>
                        <td width='30%'>
                            <select name='Ciudad' style='width:100%;'>
                                <%
                                Lista = model.SeriesSvc.getCiudades();
                                It = Lista.iterator();
                                while(It.hasNext()) {
                                    Ciudad datos = (Ciudad) It.next();
                                    out.print("<option value='"+datos.getCodigo()+"'>"+datos.getNombre().toUpperCase()+"</option>\n");
                                }
                                %>                                
                            </select>
                        </td>
                        <td width='24%' class='comentario'>&nbsp;TIPO DE DOCUMENTO</td>
                        <td width='26%'>
                            <select name='Documento' style='width:95%;'>
                                <%
                                Lista = model.SeriesSvc.getDocumento();
                                It = Lista.iterator();
                                while(It.hasNext()) {
                                    TipoDocumento datos = (TipoDocumento) It.next();
                                    out.print("<option value='"+datos.getTipo()+"'>"+datos.getNombre().toUpperCase()+"</option>\n");
                                }
                                %>                                
                            </select>
                        </td>
                        <td width='10%'>
                            <input class='boton' type='submit' name='Opcion' value='Buscar' style='width:100%;'>
                        </td>
                    </tr>                    
              </table>                                
            </td>
        </tr>        
  </table>
 <br>
<%
if(model.SeriesSvc.getRegistros().size()>0)
{
%>
<table border='1' align='center' width='950' bordercolor='#CCCCCC' cellpadding='3' cellspacing='2'>
    <tr class='titulo3'>
        <td width='6%'  align='center' class=''>ESTADO</td>
        <td width='7%'  align='center' class=''>DISTRITO</td>
        <td width='10%' align='center' class=''>CIUDAD</td>
        <td width='9%'  align='center' class=''>DOCUMENTO</td>
        <td width='10%' align='center' class=''>BANCO</td>
        <td width='12%' align='center' class=''>AGENCIA BANCO</td>
        <td width='9%'  align='center' class=''>CUENTA</td>
        <td width='7%'  align='center' class=''>PREFIJO</td>
        <td width='7%'  align='center' class=''>N� INICIAL</td>
        <td width='7%'  align='center' class=''>N� FINAL</td>
        <td width='8%'  align='center' class=''>U. NUMERO</td>
        <td width='8%'  align='center' class=''>MODIFICAR</td>
    </tr>
    <%
    Lista = model.SeriesSvc.getRegistros();
    It = Lista.iterator();
    int i=0;
    while(It.hasNext()) {
        Series datos = (Series) It.next();
        out.print("<tr class='fondotabla'>\n");
        out.print("<td class='comentario' align='center'><input type='checkbox' name='Serie_"+(i++)+"' "+datos.getEstado()+"></td>\n");
        out.print("<td class='comentario' align='center'>"+datos.getDistrito()+"</td>\n");
        out.print("<td class='comentario'>"+datos.getNombre_Ciudad()+"</td>\n");
        out.print("<td class='comentario'>"+datos.getNombre_Documento()+"</td>\n");
        out.print("<td class='comentario'>"+datos.getBanco()+"</td>\n");
        out.print("<td class='comentario'>"+datos.getAgencia_Banco()+"</td>\n");
        out.print("<td class='comentario'>"+datos.getCuenta()+"</td>\n");
        out.print("<td class='comentario'>"+datos.getPrefijo()+"</td>\n");
        out.print("<td class='comentario'>"+datos.getN_Inicial()+"</td>\n");
        out.print("<td class='comentario'>"+datos.getN_Final()+"</td>\n");
        out.print("<td class='comentario'>"+datos.getL_Numero()+"</td>\n");
        out.print("<td align='center'><a href='"+CONTROLLER+"?estado=Series&accion=Opciones&cmd=show&Opcion=Seleccionar&Distrito="+datos.getDistrito()+"&Ciudad="+datos.getCodigo_Ciudad()+"&Documento=*~"+datos.getCodigo_Documento()+"&Banco="+datos.getBanco()+"&Agencia="+datos.getAgencia_Banco()+"&Cuenta="+datos.getCuenta()+"&Prefijo="+datos.getPrefijo()+"&Serie_Inicial="+datos.getN_Inicial()+"&Serie_Final="+datos.getN_Final()+"&Ultimo_Numero="+datos.getL_Numero()+"&Estado=Disabled'>Modificar</a></td> \n");
        out.print("</tr>\n");
    }
    %>    
</table>
<br>
<center><input type='submit' name='Opcion' value='Anular' class='boton' style='width:100;'></center>
<%
}
%>
<%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>

<input type='hidden' name='Usuario' value='<%=usuario.getLogin()%>'/> 
</FORM>
</body>
</html>
