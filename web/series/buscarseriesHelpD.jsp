<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Descripcion de campos ingresar despacho</title>


<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<% String BASEIMG = BASEURL +"/images/botones/"; %> 
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center"><span class="subtitulo1">Buscar Serie</span></div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Mantenimiento de tablas </td>
        </tr>
        <tr>
          <td  class="fila">Filtros de busqueda </td>
          <td  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="fila"> Ciudad
            <select class="textbox" name='select2' style='width:45%;'>
            </select></td>
          <td  class="ayudaHtmlTexto">Combo para escojer la ciudad para el cual se va a crear la serie.</td>
        </tr>
        <tr>
          <td  class="fila">Tipo de Documento
            <select class="textbox" name='select' style='width:45%;'>
            </select></td>
          <td  class="ayudaHtmlTexto">Combo para escojer el tipo de documento para el cual se va a crear la serie, entre los tipos de documentos encontramos: Cheques, factura, orden de compra, entre otros. </td>
        </tr>
        <tr>
          <td  class="fila">Boton Buscar<img src="<%=BASEURL%>/images/botones/buscar.gif"> </td>
          <td  class="ayudaHtmlTexto">Boton para iniciar la busqueda por el filtro. </td>
        </tr>
        <tr>
          <td  class="fila">&nbsp;</td>
          <td  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td class="fila"> Boton Aceptar <img src="<%=BASEURL%>/images/botones/aceptar.gif"></td>
          <td  class="ayudaHtmlTexto">Boton para aceptar la serie. </td>
        </tr>
        <tr>
          <td width="556" class="fila">Boton Cancelar <img src="<%= BASEURL %>/images/botones/cancelar.gif" align="absmiddle" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="cancelar();"></td>
          <td width="556"  class="ayudaHtmlTexto">Boton borrar los datos de la vista.</td>
        </tr>
        <tr>
          <td class="fila">Boton Salir<img src="<%=BASEIMG%>salir.gif"></td>
          <td width="556"  class="ayudaHtmlTexto">Boton para salir de la vista. </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
