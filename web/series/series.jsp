<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head><title>Series</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validar.js"></script>
<script language=JavaScript1.2 src="<%=BASEURL%>/js/coolmenus3.js">
<script type='text/javascript' src="<%=BASEURL%>/js/reporte.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/script.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
</script>
<script language="javascript"> 
var SeparadorJS = '~';
<%=model.SeriesSvc.getCiudadesJs()%>
<%=model.SeriesSvc.getDocumentoJs()%>
<%=model.SeriesSvc.getBancoJs()%>
<%=model.SeriesSvc.getAgenciaJs()%>
<%=model.SeriesSvc.getCunetaJs()%>
<%= model.SeriesSvc.getUsuariosJs()%>

<%=model.SeriesSvc.getUsuarioSerieJs()%>
function cancelar(){
    
    document.formulario.action = "<%= CONTROLLER %>?estado=Series&accion=Opciones&cmd=show&cancelar=ok";
    
    document.formulario.submit(); 
    
}
function Cambiar_Distrito() {
    Load_Ciudades();
    Load_Documentos();
    Load_Banco();
    Load_Agencia();    
    Load_Cuenta();
}
function Cambiar_Ciudad() {
    Load_Banco();
    Load_Agencia()
    Load_Cuenta();
}

function Cambiar_Documento() {
    Load_Banco();
    Load_Agencia();
    Load_Cuenta();	
    if ( formulario.Documento.value == 'S~004' ) formulario.concept_code.disabled = false; //AMATURANA 13.04.2007
	else formulario.concept_code.disabled = true;
	formulario.concept_code.value = '';
}

function Cambiar_Banco() {    
    Load_Agencia();
    Load_Cuenta();
}

function Cambiar_Agencia() {        
    Load_Cuenta();
}

function Load_Ciudades(valor){
    var fila;
    for(fila=0 ; fila < ciudades.length ; fila++)        
        if(formulario.Distrito.value==ciudades[fila][0]) break;
    if(fila < ciudades.length) {    
        formulario.Ciudad.length=ciudades[fila].length-1;        
        for(i=0;i<=(ciudades[fila].length-2);i++) {
            var Ciudad = ciudades[fila][i+1].split('~');
            formulario.Ciudad.options[i].value = Ciudad[0];
            formulario.Ciudad.options[i].text  = Ciudad[1];
            if(Ciudad[0]==valor) formulario.Ciudad.options[i].selected=true;
        }        
    }
    else {
        formulario.Ciudad.length    = 1;           
        formulario.Documento.length = 1;
        formulario.Ciudad.options[0].value =" ";
        formulario.Ciudad.options[0].text  =" ";
        formulario.Documento.options[0].value =" ";
        formulario.Documento.options[0].text  =" ";
    }
}

function Load_Documentos(valor) {    
    var fila;
    var aux;
    for(fila=0 ; fila < documentos.length ; fila++)        
        if(formulario.Distrito.value==documentos[fila][0]) break;
    if(fila < documentos.length) {    
        formulario.Documento.length=documentos[fila].length-1;        
        for(i=0;i<=(documentos[fila].length-2);i++) {
            var Documento = documentos[fila][i+1].split('~');
            formulario.Documento.options[i].value = Documento[0]+"~"+Documento[1];
            formulario.Documento.options[i].text  = Documento[2];            
            if(Documento[1]==valor) formulario.Documento.options[i].selected=true;;
        }        
    }    
}

function Load_Banco(valor) {
    if(formulario.Documento.value.split('~')[0]=='S') {
        var fila;
        var columna;
        var sw = false;
        for(fila=0 ; fila < banco.length ; fila++) {        
            if(formulario.Distrito.value==banco[fila][0]) {                                     
                 for(columna=1 ; columna < banco[fila].length ; columna++) {                   
                    if(banco[fila][columna][0]==formulario.Ciudad.value) {                 
                        sw = true;
                        break;
                    }
                }
                if(sw) break;
            }        
        }        
        if(fila < banco.length) {    
            formulario.Banco.length=banco[fila][columna].length-1;        
            for(i=0;i<(banco[fila][columna].length-1);i++) {                
                formulario.Banco.options[i].value = banco[fila][columna][i+1];
                formulario.Banco.options[i].text  = banco[fila][columna][i+1];
                if(banco[fila][columna][i+1]==valor) formulario.Banco.options[i].selected=true;
            }            
        }
    }
    else {
        formulario.Banco.length = 1;
        formulario.Banco.options[0].value =" ";
        formulario.Banco.options[0].text  =" ";
    }
}


function Load_Agencia(valor) {
    if(formulario.Documento.value.split('~')[0]=='S') {
        var fila;
        var columna;      
        
        for(fila=0 ; fila < agencia.length ; fila++) {
            if((formulario.Distrito.value==agencia[fila][0].split('~')[0]) && 
               (formulario.Ciudad.value==agencia[fila][0].split('~')[1])   && 
               (formulario.Banco.value==agencia[fila][0].split('~')[2]))  break;           
        }
                     
        if(fila < agencia.length) {    
            formulario.Agencia.length=agencia[fila].length-1;        
            for(i=0;i<(agencia[fila].length-1);i++) {                                
                formulario.Agencia.options[i].value = agencia[fila][i+1];
                formulario.Agencia.options[i].text  = agencia[fila][i+1];
                if(agencia[fila][i+1]) formulario.Agencia.options[i].selected=true;
            }            
        }
    }
    else {
        formulario.Agencia.length = 1;
        formulario.Agencia.options[0].value =" ";
        formulario.Agencia.options[0].text  =" ";
    }
}

function Load_Cuenta(valor) {
    if(formulario.Documento.value.split('~')[0]=='S') {
        var fila;
        var columna;      
        
        for(fila=0 ; fila < cuenta.length ; fila++) {            
            if((formulario.Distrito.value == cuenta[fila][0].split('~')[0])   && 
               (formulario.Ciudad.value   == cuenta[fila][0].split('~')[1])   && 
               (formulario.Banco.value    == cuenta[fila][0].split('~')[2])   && 
               (formulario.Agencia.value  == cuenta[fila][0].split('~')[3]))  break;           
        }
                     
        if(fila < cuenta.length) {    
            formulario.Cuenta.length=cuenta[fila].length-1;        
            for(i=0;i<(cuenta[fila].length-1);i++) {                                
                formulario.Cuenta.options[i].value = cuenta[fila][i+1];
                formulario.Cuenta.options[i].text  = cuenta[fila][i+1];
                if(cuenta[fila][i+1]==valor) formulario.Cuenta.options[i].selected=true;
            }            
        }
    }
    else {
        formulario.Cuenta.length = 1;
        formulario.Cuenta.options[0].value =" ";
        formulario.Cuenta.options[0].text  =" ";
    }
}

function anular( cont ){
    if( confirm('Desea anular esta serie?') ){        
        document.formulario.action = "<%= CONTROLLER %>?estado=Series&accion=Opciones&Opcion=anular_serie_cheque";
        document.formulario.submit();   
        alert('Se anul� la serie exit�samente');
    }
	}

        function loadCombo(datos, cmb){
            cmb.length = 0;
            for (i=0;i<datos.length;i++){
                var dat = datos[i].split(SeparadorJS);
                addOption(cmb, dat[0], dat[1]);
            }
            order(cmb);
        }
        
        function unloadCombo(datos, cmb){
            for(j=0; j<cmb.length; j++){
                for (i=0;i<datos.length;i++){
                        var dat = datos[i].split(SeparadorJS);
                        if( dat[0] == cmb[j].value ){
                                cmb.remove(j);
                        }
                        //addOption(cmb, dat[0], dat[1]);
                }
            }
            order(cmb);
        }

        function addOption(Comb,valor,texto){
           if(valor!='' && texto!=''){
                var Ele = document.createElement("OPTION");
                Ele.value=valor;
                Ele.text=texto;
                Comb.add(Ele);
          }
        }

        function order(cmb){
           for (i=0; i<cmb.length;i++)
             for (j=i+1; j<cmb.length;j++)
                if (cmb[i].text > cmb[j].text){
                    var temp = document.createElement("OPTION");
                    temp.value = cmb[i].value;
                    temp.text  = cmb[i].text;
                    cmb[i].value = cmb[j].value;
                    cmb[i].text  = cmb[j].text;
                    cmb[j].value = temp.value;
                    cmb[j].text  = temp.text;
                }
        }
        
        function selectAll(cmb){
            for (i=0;i<cmb.length;i++)
              cmb[i].selected = true;
        }

        function deleteRepeat(cmb){
          var ant='';
          i=0;
          while(i<cmb.length){
             if(ant== cmb[i].value){
                ant = cmb[i].value;
                cmb.remove(i);
                i--;
             } else {              
                ant = cmb[i].value;
             }            
             i++;
          }
        }

        function move(cmbO, cmbD){
           for (i=cmbO.length-1; i>=0 ;i--)
            if (cmbO[i].selected){
               addOption(cmbD, cmbO[i].value, cmbO[i].text)
               cmbO.remove(i);
            }
           //order(cmbD);
           deleteRepeat(cmbD);
        }
        
        function move2(cmbO, cmbD){
           for (i=cmbO.length-1; i>=0 ;i--)
            if (cmbO[i].selected){
               addOption(cmbD, cmbO[i].value, cmbO[i].text)
               cmbO.remove(i);
            }
           order(cmbD);
           deleteRepeat(cmbD);
        }

        function moveAll(cmbO, cmbD){
           for (i=cmbO.length-1; i>=0 ;i--){
             if(cmbO[i].value!='' && cmbO[i].text!=''){
               addOption(cmbD, cmbO[i].value, cmbO[i].text)
               cmbO.remove(i);
             }
           }
           order(cmbD);
           deleteRepeat(cmbD);
        }

</script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body>
<%
List     Lista;
Iterator It;
String   aux;
Series serie = model.SeriesSvc.getserie();
//Series nueva = request.getAttribute("nueva")!=null? (Series)request.getAttribute("nueva") : null;
String Mensaje = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);

TreeMap conceptos = model.SeriesSvc.getConceptos();
conceptos.put("","");
/*if( nueva != null ){
    serie = nueva;
}else{*/%>


<FORM name='formulario' id='formulario' method='POST' onsubmit='jscript:return verificar(<%=serie.getL_Numero()%>);' action='<%=CONTROLLER%>?estado=Series&accion=Opciones&cmd=show'>

    <table width="700" border="2" align="center">
      <tr>
        <td>
          <table width="100%" align="center" class="tablaInferior">
            <tr>
              <td colspan="4" align="left">
                <table width="100%"  border="0" cellpadding="0" cellspacing="0">
                  <tr>
                    <td width="13%" class="subtitulo1">Series</td>
                    <td width="87%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" align="left"><%=datos[0]%></td>
                  </tr>
                </table></td>
            </tr>
            <tr class="fila">
              <td align="left" >Distrito</td>
              <td valign="middle"><select <%=serie.getEstado()%> class="textbox" onchange='Cambiar_Distrito()' name='Distrito' style='width:45%;'><option value='FINV'>FINV</option></select>
                        <input type='hidden' name='Distrito2' value='FINV'></td>
              <td valign="middle">Ciudad</td>
              <td valign="middle"><select <%=serie.getEstado()%> class="textbox" onchange='Cambiar_Ciudad()' name='Ciudad' style='width:100%;'></select>
                            <input type='hidden' name='Ciudad2' value='<%=serie.getCodigo_Ciudad()%>'></td>
            </tr>
            <tr class="fila" id="cantidad">
              <td width="151" align="left" >Tipo de Documento</td>
              <td width="177" valign="middle"><select <%=serie.getEstado()%> class="textbox" onchange='Cambiar_Documento()' name='Documento' id='Documento' style='width:100%;'></select>
                            <input type='hidden' name='Documento2' value='<%=serie.getCodigo_Documento()%>'></td>
              <td width="123" valign="middle">Banco</td>
              <td width="217" valign="middle"><select <%=serie.getEstado()%> class="textbox" onchange='Cambiar_Banco()'  name='Banco' style='width:100%;'></select>
                            <input type='hidden' name='Banco2' value='<%=serie.getBanco()%>'></td>
            </tr>
            <tr class="fila">
              <td width="151" align="left" >Concepto</td>
              <td width="177" valign="middle"><input:select name="concept_code" attributesText="class=textbox" options="<%=conceptos %>" default="<%=serie.getConcepto() %>"/>
			  <script>
			  	if ( formulario.Documento.value == '' && '<%=serie.getCodigo_Documento()%>' == '' ) { 
					formulario.concept_code.disabled = true;
				  	formulario.concept_code.value = '';
				} 
			  </script></td>
              <td width="123" valign="middle">Agencia del Banco</td>
              <td width="217" valign="middle"><select <%=serie.getEstado()%> class="textbox" onChange='Cambiar_Agencia()' name='Agencia' style='width:100%;'>
              </select>
                <input type='hidden' name='Agencia2' value='<%=serie.getAgencia_Banco()%>'></td>
            </tr>
            <tr class="fila">
              <td width="151" align="left" >Prefijo Serie</td>
              <td valign="middle"><input value='<%=serie.getPrefijo()%>' class="textbox" maxlength='2' type='text' name='Prefijo' style='width:14%;'></td>
              <td valign="middle">Cuenta de Ahorro</td>
              <td valign="middle"><select <%=serie.getEstado()%>  class="textbox" name='Cuenta' style='width:100%;'>
              </select>
                <input type='hidden' name='Cuenta2' value='<%=serie.getCuenta()%>'></td>
            </tr>
            <tr class="fila">
              <td width="151" align="left" >N&ordm; Serie Inicial</td>
              <td valign="middle"><input <%=serie.getEstado()%> class="textbox"  value='<%=serie.getN_Inicial()%>' onKeyDown='return solo_numeros(this, event)' maxlength='9' type='text' name='Serie_Inicial' style='width:32%;'>
                <input type='hidden' name='Serie_Inicial2' value='<%=serie.getN_Inicial()%>'></td>
              <td valign="middle">&nbsp;</td>
              <td valign="middle">&nbsp;</td>
            </tr>		
            <tr class="fila">
              <td width="151" align="left" ><span class="comentario">N&ordm; Serie Final</span></td>
              <td valign="middle"><input value='<%=serie.getN_Final()%>' class="textbox" onKeyDown='return solo_numeros(this, event)' maxlength='9' type='text' name='Serie_Final' style='width:32%;'></td>
              <td valign="middle">Ultimo Numero</td>
              <td valign="middle"><input value='<%=serie.getL_Numero()%>' class="textbox" onKeyDown='return solo_numeros(this, event)' maxlength='9' type='text' name='Ultimo_Numero' style='width:32%;'></td>
            </tr>
            <tr class="fila">
              <td colspan="4" align="left" ><table width="100%" border="0" align="center">
                      <tr valign="top">
                        <td width="47%" valign="middle" class="subtitulo1"><div align="center">Usuarios</div></td>
                        <td width="6%"><input name="doc_selec" type="hidden" id="doc_selec"></td>
                        <td width="47%" class="subtitulo1"><div align="center">Usuarios<br>
  
seleccionados</div></td>
                      </tr>
                      <tr>
                        <td><select name="c_docs"  size="10" style='width:100%' class="textreadonly" id="c_docs">
                        </select></td>
                        <td align="center"><br> 
                          <p><img src="<%=BASEURL%>/images/botones/envDer.gif" name="c_regresar" id="c_move" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick=" if( formulario.Documento.value == 'S~004' ) { move(c_docs,  c_docSelec ); } else { alert('Estos campos son v�lidos solo para cheques.'); } " style="cursor:hand "><br>
                           <br>
                          <img src="<%=BASEURL%>/images/botones/envIzq.gif" name="c_regresar" id="c_move2" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick=" if( formulario.Documento.value == 'S~004' ) { move2(c_docSelec, c_docs ); } else { alert('Estos campos son v�lidos solo para cheques.'); } " style="cursor:hand "><br>
                           <br>
                          <br>
                        </p></td>
                        <td><select name="c_docSelec" size="10" multiple class="textobligatorio" id="c_docSelec" style='width:100%'>
                        </select></td>
                      </tr>
              </table></td>
            </tr>
        </table></td>
      </tr>
  </table>
    <div align="center"><br>
      <INPUT type="hidden" name='Opcion' value='<%=serie.getNombre_Boton()%>'>
      <!-- input type="image" name="Submit" value="Ingresar" src="<%= BASEURL %>/images/botones/<%= (!serie.getN_Inicial().equals("")?"modificar.gif":"aceptar.gif") %>" align="absmiddle" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="selectAll(formulario.docSelec);" --> 
      <img src="<%= BASEURL %>/images/botones/<%= (!serie.getN_Inicial().equals("")?"modificar.gif":"aceptar.gif") %>"  name="imgaceptar" onClick="selectAll(formulario.c_docSelec); if ( verificar(<%=serie.getL_Numero()%>) ) { formulario.submit(); }" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">&nbsp; 
      <%if( serie.getCodigo_Documento().equals("004") ){%>
        <img src="<%=BASEURL%>/images/botones/anular.gif"  align="absmiddle" name="imganular"  onMouseOver="botonOver(this);" onClick="anular('<%=CONTROLLER%>');" onMouseOut="botonOut(this);" style="cursor:hand">    
      <%}%>
      <img src="<%= BASEURL %>/images/botones/<%= (!serie.getN_Inicial().equals("")?"regresar.gif":"cancelar.gif") %>" align="absmiddle" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="<%= (!serie.getN_Inicial().equals("")?"document.location = ('" + CONTROLLER + "?estado=Menu&accion=Series&carpeta=1&Pagina=series/buscarseries')":"document.location = ('" + CONTROLLER + "?estado=Menu&accion=Series&carpeta=1&Pagina=series/series')") %>"        >
      <img src="<%=BASEURL%>/images/botones/salir.gif"  align="absmiddle" name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">    
      
	  <br>
      <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
  
      <input type='hidden' name='Usuario' value='<%=usuario.getLogin()%>'/> 
  
  </div>
  
   <p>
  <%if(!Mensaje.equals("")){
  %>
</p>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%= Mensaje %></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</FORM>
</p>
 <%}%>
<script language="javascript"> 
Load_Ciudades('<%=serie.getCodigo_Ciudad()%>');
Load_Documentos('<%=serie.getCodigo_Documento()%>');
Load_Banco('<%=serie.getBanco()%>');
Load_Agencia('<%=serie.getAgencia_Banco()%>');
Load_Cuenta('<%=serie.getCuenta()%>');
//AMATURANA 16.04.2007
loadCombo(CamposJSUsers, formulario.c_docs);
if ( '<%=serie.getCodigo_Documento()%>' == '004' ){ 
	unloadCombo(CamposJSUserSerie, formulario.c_docs);
	loadCombo(CamposJSUserSerie, formulario.c_docSelec);
}
</script>
<%=datos[1]%>
</body>
</html>
