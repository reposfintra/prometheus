<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Descripcion de campos ingresar despacho</title>


<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<% String BASEIMG = BASEURL +"/images/botones/"; %> 
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">SERIES</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Mantenimiento de tablas </td>
        </tr>
        <tr>
          <td  class="fila">Distrito
            <select class="textbox" name='Distrito' style='width:45%;'>
              
          </select></td>
          <td  class="ayudaHtmlTexto">Combo para escojer el distrito </td>
        </tr>
        <tr>
          <td  class="fila">Tipo de Documento
            <select class="textbox" name='select' style='width:45%;'>
            </select></td>
          <td  class="ayudaHtmlTexto">Combo para escojer el tipo de documento para el cual se va a crear la serie, entre los tipos de documentos encontramos: Cheques, factura, orden de compra, entre otros. </td>
        </tr>
        <tr>
          <td  class="fila">Prefijo Serie
          <input class="textbox" maxlength='2' type='text' name='Prefijo' style='width:14%;'></td>
          <td  class="ayudaHtmlTexto">Campo de texto para especificar el prefijo que va a llevar la serie por defecto </td>
        </tr>
        <tr>
          <td  class="fila">N&ordm; Serie Inicial
          <input class="textbox" maxlength='2' type='text' name='Prefijo2' style='width:14%;'></td>
          <td  class="ayudaHtmlTexto">Campo de texto para especificar desde donde va a comenzar la serie </td>
        </tr>
        <tr>
          <td  class="fila">N&ordm; Serie Final 
          <input class="textbox" maxlength='2' type='text' name='Prefijo3' style='width:14%;'></td>
          <td  class="ayudaHtmlTexto">Campo de texto para especificar desde donde va a terminar la serie </td>
        </tr>
        <tr>
          <td  class="fila">Ultimo Numero
          <input class="textbox" maxlength='2' type='text' name='Prefijo32' style='width:14%;'>          </td>
          <td  class="ayudaHtmlTexto">Campo de texto para especificar cual es el proximo numero del conteo de la serie </td>
        </tr>
        <tr>
          <td  class="fila"> Ciudad
            <select class="textbox" name='select2' style='width:45%;'>
            </select></td>
          <td  class="ayudaHtmlTexto">Combo para escojer la ciudad para el cual se va a crear la serie.</td>
        </tr>
        <tr>
          <td  class="fila"> Banco
            <select class="textbox" name='select3' style='width:45%;'>
            </select></td>
          <td  class="ayudaHtmlTexto">Combo para escojer el banco para el cual se va a crear la serie, solo se puede escojer si la serie es para el tipo de documento CHEQUE. </td>
        </tr>
        <tr>
          <td  class="fila">Agencia de Banco
            <select class="textbox" name='select5' style='width:45%;'>
            </select></td>
          <td  class="ayudaHtmlTexto">Combo para escojer la la agencia de banco para el cual se va a crear la serie, solo se puede escojer si la serie es para el tipo de documento CHEQUE. </td>
        </tr>
        <tr>
          <td  class="fila">Cuenta de Ahorro
            <select class="textbox" name='select4' style='width:45%;'>
            </select></td>
          <td  class="ayudaHtmlTexto">Combo para escojer la cuenta de ahorro para el cual se va a crear la serie, solo se puede escojer si la serie es para el tipo de documento CHEQUE. </td>
        </tr>
        <tr>
          <td class="fila"> Boton Aceptar <img src="<%=BASEURL%>/images/botones/aceptar.gif"></td>
          <td  class="ayudaHtmlTexto">Boton para aceptar la serie. </td>
        </tr>
        <tr>
          <td width="556" class="fila">Boton Cancelar <img src="<%= BASEURL %>/images/botones/cancelar.gif" align="absmiddle" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="cancelar();"></td>
          <td width="556"  class="ayudaHtmlTexto">Boton borrar los datos de la vista.</td>
        </tr>
        <tr>
          <td class="fila">Boton Salir<img src="<%=BASEIMG%>salir.gif"></td>
          <td width="556"  class="ayudaHtmlTexto">Boton para salir de la vista. </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
