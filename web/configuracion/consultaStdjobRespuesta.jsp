<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<link href="../css/letras.css" rel="stylesheet" type="text/css">

<title>Buscar Standard</title>
<script language="javascript" src="../js/validar.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.Estilo1 {font-weight: bold}
.style3 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
}
.style7 {font-family: Verdana, Arial, Helvetica, sans-serif;  font-size: 12px; }
.style12 {font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 12px; font-weight: bold; }
-->
</style>
</head>
<body>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Configurar&accion=StdJob">

<%
	String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
    Vector stdjob = model.stdjobdetselService.getStdjob();
	if (stdjob.size()<1)
		response.sendRedirect(CONTROLLER+"?estado=Menu&accion=Enviar&numero=42&mensaje=");
%>
<table width="100%" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="ECE0D8" class="Letras">
  <tr bgcolor="#FFA928">
    <td height="25" colspan="7" nowrap><div align="center" class="Estilo1 style3"><strong>LISTA DE STANDARD </strong></div></td>
  </tr>
  <tr bgcolor="#99CCFF" class="style7">
    <td width="91" height="21" nowrap>&nbsp;</td>
    <td width="100" nowrap><strong> STDJOB NO. </strong></td>
    <td width="230" bgcolor="#99CCFF"><div align="center" class="style12">DESCRIPCION</div></td>
    <td width="148" bgcolor="#99CCFF"><span class="style12">CLIENTE</span></td>
    <td width="110" bgcolor="#99CCFF"><span class="style12">ORIGEN</span></td>
    <td width="148" class="Estilo4"><div align="center" class="style12"> DESTINO</div></td>
    <td width="148" class="Estilo4 Estilo1">PESO LLENO MAX PERMITIDO </td>
  </tr>
  <pg:pager
    items="<%=stdjob.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
<%-- keep track of preference --%>

  <%
      for (int i = offset.intValue(),
	         l = Math.min(i + maxPageItems, stdjob.size());
	     i < l; i++)
	{
        Stdjobdetsel std= (Stdjobdetsel) stdjob.elementAt(i);%>
		<pg:item>
 	 <tr style="cursor:hand" title="Elegir estandard..."  onMouseOver="bgColor= '#99cc99'" onMouseOut="bgColor=''">
    <td height="30" nowrap><div align="center">
      <%if(std.getEstaStd()==true){%>
	  <input type="checkbox" name="<%=std.getSj()%>" value="checkbox" checked>
	  <%}
	  else{%>
	  <input type="checkbox" name="<%=std.getSj()%>" value="checkbox">
	  <%}%>
    </div></td>
    <td height="30" nowrap><%=std.getSj()%></td>
    <td><div align="center" class="style7"><%=std.getSj_desc()%> </div></td>
    <td><%=std.getCliente()%></td>
    <td><%=std.getOrigin_name()%></td>
    <td><div align="center" class="style7"><%=std.getDestination_name()%></div></td>
    <td><input name="peso<%=std.getSj()%>" type="text" id="peso" value="<%=std.getPeso()%>" ></td>
 	 </tr>
 	 
  </pg:item>
  <%}
  %>
  <tr bgcolor="#FFFFFF" class="style7">
 	   <td height="30" colspan="7" nowrap>
	<pg:index>
	<jsp:include page="/WEB-INF/jsp/google.jsp" flush="true"/>
	</pg:index>
	</td>
  </tr>
  </pg:pager>
</table>
<div align="center">
  <input type="submit" name="Submit" value="Aceptar">
  <br>  
  </div>
</form>
<a href="<%=CONTROLLER%>?estado=Menu&accion=Enviar&numero=42" class="Letras">Volver a Realizar la consulta...</a>
</body>
</html>



