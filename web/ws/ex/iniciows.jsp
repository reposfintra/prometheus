<%@ page session="true"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
    <title>WS Manual</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
	<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=WS"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<form name="form2" method="post" action="controller?estado=Web&accion=Service">

  <center>
    <table width="500" border="2" align="center">
      <tr>
        <td>
          <table width='100%' align='center' class='tablaInferior'>
            <tr>
              <td class="barratitulo" colspan='2' >
                <table cellpadding='0' cellspacing='0' width='100%'>
                  <tr class="fila">
                    <td width="50%" align="left" class="subtitulo1">&nbsp;WS</td>
                    <td width="*"   align="left"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"  align="left"></td>
                  </tr>				  
              </table></td>
            </tr>
			
			
			<tr class="barratitulo">
              <td  class='informacion' height='60'> &nbsp;&nbsp;Esta opci�n permite pasar la respectiva tabla del servidor al cliente a trav�s de un Web Service. </td>
            </tr>
			<%
			String tabla=request.getParameter("tablax");
			if (tabla==null){tabla="";}
			%>
            <tr class="barratitulo" align="center">
              <td  class='informacion' height='60'> &nbsp;Tabla:&nbsp;&nbsp;&nbsp;<select name="tabla" >
			  			<option value="Nit" <%if (tabla.equals("Nit")){%> selected <%}%>>Nit</option>
					</select> </td>
            </tr>
			
        </table></td>
      </tr>
    </table>
    <br>
	<table align="center">
<tr>
        <td colspan="2" nowrap align="center">
		  <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="aceptar"  height="21" onClick="this.disabled=true;form2.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
		  <img src="<%=BASEURL%>/images/botones/salir.gif" name="mod"  height="21" onclick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
        </td>
      </tr>
</table>
    <br>
    <br>
    <table width="40%"  align="center">
      <tr>
        <td>
          <FIELDSET>
          <legend><span class="letraresaltada">Nota</span></legend>
          <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="informacion">
            <tr>
              <td nowrap align="center">&nbsp; Para iniciar el proceso de haga click en aceptar. </td>
            </tr>
          </table>
        </FIELDSET></td>
      </tr>
    </table>
  </center>
<br>
<%if(request.getParameter("msg")!=null){%>
<table border="2" align="center">
  <tr>
    <td><table width="100%" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="262" align="center" class="mensajes"><%=request.getParameter("msg")%> </td>
          <td width="32" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="44">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<%}%>

</form>
</div>
</body>
</html>