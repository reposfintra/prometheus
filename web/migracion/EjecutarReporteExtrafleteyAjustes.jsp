<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Migracion de Movimientos Contables</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado= Migracion de Movimientos Contables"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<table width="410"  border="2" align="center">
  <tr>
    <td><table width="100%" align="center" class="tablaInferior">
  <tr>
    <td><table width="100%"  border="0" class="barratitulo">
      <tr >
        <td width="69%" class="subtitulo1">MIGRAR MOVIMIENTOS CONTABLE<BR>        </td>
        <td width="31%"><img src="<%=BASEURL%>/images/titulo.gif"></td>
      </tr>
    </table>
	</td>
  </tr>
  <tr>
    <td align="center" class="fila">Generaci&oacute;n del de Movimientos contables, extrafletes y ajustes. <br>
    Presione Aceptar para iniciar el proceso.</td>
  </tr>
</table></td>
</tr>
</table>
<form name="form1" method="post" action="">
  <center>
	<img src="<%=BASEURL%>/images/botones/aceptar.gif" style='cursor:hand' title="Aceptar..." name="c_imgaceptar"  onClick="form1.action='<%=CONTROLLER%>?estado=Generar&accion=ReporteInterfaceContable'; form1.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	<img src="<%=BASEURL%>/images/botones/salir.gif"  style='cursor:hand' name="salir" title="Salir..." onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
  </center>
</form>

<%  String msg = (String) request.getAttribute("mensaje");
    if (msg!=null && !msg.equals("")) { %>
        <table border="2" align="center">
           <tr>
           <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                <tr>
                 <td width="320" align="center" class="mensajes"><%=msg%></td>
                 <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                 <td width="58">&nbsp;</td>
                </tr>
              </table>
           </td>
           </tr>
        </table>  
<% } %> 
</div>
</body>
</html>
