<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head><title>MIGRACION PLANILLAS CARBON</title>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<link  href='<%=BASEURL%>/css/estilostsp.css' rel='stylesheet'>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado= Migracion Planillas Carbon"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>
<form name="formulario" action='' method='post' onsubmit="">
<table width="350" border="2" align="center">
  <tr>
    <td><table width='100%' class="tablaInferior">
      <tr class="barratitulo">
        <th colspan="2"><table width="100%">
  <tr class="barratitulo">
    <td width="60%" class="subtitulo1">MIGRACION DE PLANILLAS </td>
    <td width="40%"><img src="<%=BASEURL%>/images/titulo.gif"></td>
  </tr>
</table>
</th>
      </tr>
      <tr class="fila">
        <td valign="top" class='comentario'>Fecha de Creacion : </td>
        <td valign="top" class='comentario'><input name='fecha_creacion' type='text' class="textbox" style='width:120' value='' readonly>
			<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fecha_creacion);return false;" HIDEFOCUS>
			<img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/Calendario/cal.gif" border="0" alt=""></a></td>
      </tr>
      <tr class="fila">
        <td class='comentario'>Base : </td>
        <td class='comentario'><select name="base" class="textbox" id="base">
            <option value="spo">Sociedad Portuaria SM</option>
            <option value="pco">PUERTO PRODECO</option>
        </select></td>
      </tr>
    </table>
	</td>
  </tr>
</table><br>
<center>
<img src="<%=BASEURL%>/images/botones/aceptar.gif" title="Aceptar..." name="c_imgaceptar"  onClick="formulario.action='<%=CONTROLLER%>?estado=ExportPlanillas&accion=Opciones&colpapel=ok'; jscript: if(fecha_creacion.value==''){alert('Debe indicar la fecha para poder continuar');}else{ formulario.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
<img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir..." onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</form>
<br>

<font class="letraresaltada">
</font>
<%
  String msg = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje") : "";
  if(!msg.equals("")){
%>
<p>
<table width="485" border="2" align="center">
  <tr>
    <td><table width="100%"  align="center"  >
        <tr>
          <td width="229" align="center" class="mensajes"> <%=msg%></td>
          <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="58">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<p></p>
<%}%>
</center>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>
