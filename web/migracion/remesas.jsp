<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
    String Mensaje = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
%>
<html>
<head><title>Busqueda de Remesas</title></head>
<link  href='<%=BASEURL%>/css/Style.css' rel='stylesheet'>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<body>
<center>
<form action='<%= CONTROLLER %>?estado=ExportPlanillas&accion=Opciones' method='post' onsubmit="jscript: if(fecha_creacion.value==''){alert('Debe indicar la fecha para poder continuar'); return false;}">
<table class='fondotabla' width='350' border='1'>
    <tr><th class='titulo1'>BUSQUEDA DE REMESAS</th></tr>
    <tr>
        <td class='comentario'>
            <center>
            <br>            
            &nbsp;&nbsp;Fecha de Creacion  : <input type='text' value='' readonly name='fecha_creacion' style='width:120'>
            <a href="javascript:void(0)" onclick="jscript: show_calendar('fecha_creacion');" HIDEFOCUS><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/Calendario/calbtn.gif" width="34" height="22" border="0" alt=""></a>
            <br><br>&nbsp;            
            <input type='submit' class='boton' style='width:120' name='Opcion' value='Procesar'>
            <br>&nbsp;
    </td></tr>
</table>
</form>
<br>

<font size='4px'>
<%= Mensaje %>
</font>

</center>
</body>
</html>
