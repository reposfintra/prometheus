<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Procesos Saldos Contables</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Proceso de Saldos Contables"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<% String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";%>
<form name="forma" method="post" action="<%=CONTROLLER%>?estado=Cargar&accion=Varios&sw=15">
  <table width="500" border="2" align="center">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr class="barratitulo">
          <td align="left">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="45%" class="subtitulo1">Proceso de Saldos Contables </td>
                <td width="55%"><img src="<%=BASEURL%>/images/titulo.gif"></td>
              </tr>
            </table>            </td>
          </tr>
        <tr class="fila">
          <td align="left" ><div align="center">Haga clic en Aceptar para iniciar el proceso </div></td>
          </tr>
      </table></td>
    </tr>
  </table>
 <br>
 <center>
<img src="<%=BASEURL%>/images/botones/aceptar.gif" title="Aceptar..." name="c_imgaceptar"  onClick="forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
<img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir..." onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</form>
  <% if(!msg.equals("")){%>
  <table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes">
			<%=msg%>
			<%if(request.getParameter("ruta")!=null){%><br>
			&nbsp;&nbsp;<a href="#" class="Simulacion_Hiper" onClick="window.open('<%= CONTROLLER %>?estado=Log&accion=Proceso&Estado=TODO','log','scroll=no, resizable=yes, width=800, height=600')" style="cursor:hand "><%=request.getParameter("ruta")%><%}%></a>
		</td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
 <%}%>
 </div>
</body>
</html>
