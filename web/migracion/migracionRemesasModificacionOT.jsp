<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@page session="true"%> 
<%@page import="java.util.*" %>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head><title>Migración modificacion de remesas</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../css/estilostsp.css" rel="stylesheet">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Migracion Modif620"/>
</div>

 <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Migracion&accion=RemesasModificacionOT">
<table border="2" align="center" width="448">
  <tr>
    <td>
      <table width="99%" border="0" align="center" class="tablaInferior">
        <tr>
          <td width="52%"  class="subtitulo1"><p align="left">Generacion Archivo Modif620</p></td>
          <td width="48%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
      <table width="99%" align="center" class="tablaInferior">
        <tr class="fila">
          <td valign="middle" ><div align="center">Presione Aceptar para iniciar el proceso</div></td>
        </tr>
    </table></td>
  </tr>
</table>
<div align="center"><br>
    <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="this.disabled=true;form1.submit()" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
    <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close()" onMouseOut="botonOut(this);" style="cursor:hand">
</div>

</form>
</div>
</body>
</html>
