<%@page contentType="text/html"%>
<%@page session="true"%> 
<%@page import="java.util.*" %>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<html>
<head><title>Migracion Remesas Anuladas</title>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado= Migracion Remesas Anuladas"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<p>
  <%-- <jsp:useBean id="beanInstanceName" scope="session" class="beanPackage.BeanClassName" /> --%>
  <%-- <jsp:getProperty name="beanInstanceName"  property="propertyName" /> --%>
</p>
<table width="350"  border="2" align="center">
  <tr>
    <td><table width="100%" align="center" class="tablaInferior">
  <tr>
    <td><table width="100%"  border="0" class="barratitulo">
      <tr >
        <td width="62%" class="subtitulo1">Migraci&oacute;n a MIMS</td>
        <td width="38%"><img src="<%=BASEURL%>/images/titulo.gif"></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td align="center" class="fila">Generaci&oacute;n del Archivo Anulacion de Remesas<br>
    Presione Aceptar para iniciar el proceso.</td>
  </tr>
</table></td>
  </tr>
</table>
<form name="form1" method="post" action="">
  <center>
<img src="<%=BASEURL%>/images/botones/aceptar.gif" title="Aceptar..." name="c_imgaceptar"  onClick="form1.action='<%=CONTROLLER%>?estado=Migracion&accion=RemesasAnuladas'; form1.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
<img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir..." onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</form>
</div>
</body>
</html>
