<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head><title>Migración de Placas</title>
    <link href="<%= BASEURL %>/css/Style.css" rel='stylesheet'>
     <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validar.js"></script>
     <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/date-picker.js"></script>
     <script language=JavaScript1.2 
         src="<%=BASEURL%>/js/coolmenus3.js">
     </script>
</head>
<body>
<br>
<%
int Ano_Actual = Integer.parseInt(Util.getFechaActual_String(1));
int Mes_Actual = Integer.parseInt(Util.getFechaActual_String(3));
String Sele;
Usuario usuario = (Usuario)session.getAttribute("Usuario");
%>
<FORM name='x' id='x' method='POST' action="<%=CONTROLLER%>?estado=Placa&accion=Migracion">

    <table border='1' width='320' align='center'>
        <tr class='titulo1'>
            <td colspan='2' align='center' height='50'>MIGRACIÓN DE PLACAS</td>
        </tr>        
        <tr>
            <td colspan='2' class='fondo'>
               
                <br>    
                <table border='0' width='90%' align='center' cellspacing='0'>                    
                    <tr>
                        <td class='comentario2'>FECHA ARCHIVO</td>
                        <td colspan='2'>                     
                            <input style='width:49%;' value='<%=Util.getFechaActual_String(7).replaceAll("/","-")%>' type="text" size='10' readonly='true' name="Fecha" class='comentario'>
                            <input style='width:48%;' type="button" class='comentario' value="Escoger" onclick="javascript:show_calendar('Fecha')">
                        </td>
                    </tr>                                
                </table>
                <br>
                
            </td>
        </tr>
        <tr class='fondo'>
            <td align='center' colspan='2' height='50'>
                <INPUT type='SUBMIT' name='Aceptar' value='Aceptar' style='width:40%;'>
            </td>
        </tr>
    </table>
<br>
<input type='hidden' name='Usuario' value='<%=usuario.getNombre()%>'/> 
</FORM>
</body>
</html>