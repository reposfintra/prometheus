<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@page session="true"%> 
<%@page import="java.util.*" %>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<html>
<head><title>Migración de planillas anuladas con y sin anticipo</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../css/estilostsp.css" rel="stylesheet">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Migracion&accion=PlanillaAnuladaAnticipo">
<table border="2" align="center" width="448">
  <tr>
    <td>
      <table width="100%" align="center" class="tablaInferior">
        <tr>
          <td width="73%"  class="subtitulo1"><p align="left">Migraci&oacute;n Planillas Anuladas con/sin Anticipo </p></td>
          <td width="27%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
      <table width="100%" align="center" class="tablaInferior">
        <tr class="fila">
          <td valign="middle" ><div align="center">Presione Aceptar para iniciar el proceso   </div></td>
        </tr>
    </table></td>
  </tr>
</table>

<div align="center"><br>
    <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="form1.submit();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
    <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close()" onMouseOut="botonOut(this);" style="cursor:hand">
</div>
</form>
</body>
</html>
