<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
    <title>Modificar proveedor tiquetes</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <script src="<%=BASEURL%>/js/validar.js"></script>
    <link href="<%=BASEURL%>/css/letras.css" rel="stylesheet" type="text/css">
	<script src='<%=BASEURL%>/js/date-picker.js'></script>


</head>

<body onLoad="sumarton();">
<%
String accion = CONTROLLER+"?estado=Generar&accion=Corte";
%>
    <form name="form2" method="post" action="<%=accion%>" onSubmit="return validarFechasCorte(this);">
<table width="650" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="ECE0D8" class="Letras">
        <tr bgcolor="#FFA928">
        <td colspan="2" nowrap><div align="center" class="letras"><strong>CORTE</strong></div></td>
    </tr>
        <tr>
        <td width="264" nowrap><strong>FECHA CORTE </strong></td>
        <td width="370" nowrap><span class="comentario">
          <input name='fechai' type='text' id="fechai" style='width:120' value='<%=request.getParameter("fec")%>' readonly>
          <a href="javascript:void(0)" onclick="jscript: show_calendar('fechai');" HIDEFOCUS><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/Calendario/calbtn.gif" width="34" height="22" border="0" alt=""></a></span></td>
        </tr>
    </table>
    <div align="center"><br>
      <br>
      <table width="95%"  border="1" cellpadding="1" cellspacing="1" bordercolor="#CCCCCC" bgcolor="ECE0D8" class="letras">
        <tr>
          <td width="12%" bgcolor="#FFA928"><strong># REMISION</strong></td>
          <td width="10%" bgcolor="#FFA928"><strong>PLACA</strong></td>
          <td width="36%" bgcolor="#FFA928"><strong>CONDUCTOR</strong></td>
          <td width="16%" bgcolor="#FFA928"><div align="center"><strong>TONELAJE</strong></div></td>
          <td width="21%" bgcolor="#FFA928"><strong>FECHA DESPACHO </strong></td>
          <td width="5%">&nbsp;</td>
        </tr>
        <%
						Vector datos = model.planillaService.getInformes();
						%>
        <%   for(int k =0;k<datos.size();  k++){
							Informe dato = (Informe)datos.elementAt(k);
							
							
	 				 %>
        <tr>
          <td><%=dato.getRemision()%></td>
          <td><%=dato.getPlaca()%></td>
          <td><%if(dato.getConductor()!=null)%>
              <%=dato.getConductor()%></td>
          <td>
            <table width="100%"  border="0" cellspacing="0" cellpadding="0" class="letras2">
              <tr>
                <td width="59%"><div align="right"><%=dato.getTonelaje()%></div></td>
                <td width="41%"><div align="center">T </div></td>
              </tr>
          </table></td>
          <td><%=dato.getFechaCump().substring(0,16)%></td>
          <td><input name="check<%=dato.getNumpla()%>" type="checkbox" onChange="sumarton();" value="<%=dato.getTonelaje()%>" checked></td>
        </tr>
		 <%}%>
        <tr>
          <td colspan="2"><div align="right"><strong>TOTAL VIAJES : <%=datos.size()%></strong></div></td>
          <td><div align="right"><strong>TOTAL TONELADAS:  </strong></div></td>
          <td>
            <input name="ton" type="text" id="ton" readonly>          </td>
          <td><strong>VIAJES EN EL CORTE </strong></td>
          <td><input name="cant" type="text" id="cant" size="5" readonly></td>
        </tr>
       
					
	  				
      </table>
      <br>
      <input type="submit" name="Submit" value="Generar Corte">
    </div>
    </form>

    
</body>
</html>
