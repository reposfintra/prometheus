<!--  
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      11/11/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Vista que permite consultar imagenes
 --%>


<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="java.util.*" %>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.util.Util"%>

<html>
<head>
   <title>Busqueda de Imagen</title>
   <link href="<%= BASEURL %>/css/estilotsp.css" rel="stylesheet" type="text/css"> 
   <script type='text/javascript' src="<%= BASEURL %>/js/script.js"></script>
   
   <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
   <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">    
   <script type='text/javascript' src="<%= BASEURL %>/js/Validaciones.js"></script>
  
   
   <!-- Help -->
  <%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
      String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);%>
  
  <% model.ImagenSvc.load(); %>
  
   <script>
   
    <%= model.ImagenSvc.getJsRelacion() %>
    
    function MostrarImagen( ext, url,nombre,largo,hancho,x,y){
      option="  width="+largo+", height="+hancho+",   scrollbars=yes, statusbars=yes, resizable=yes ,top="+x+", left="+y;
      ventana=window.open('',nombre,option);
      ventana.location.href=url;
      ventana.focus();
      if( ext=='TIF' || ext =='TIFF' )
          ventana.close();
    }
   
  
     function load(){
             var fila   = 0;
             var sele   = formulario.actividad.value.split('-')[0];
             var vec    = relacion.split('|');
             for(i=0;i<=(vec.length-1);i++){
                    var vecAct = vec[i].split('-');
                    if( vecAct[0]==sele )
                        fila ++;
             }             
             formulario.tipoDocumento.length = fila;              
             fila=0;
             for(i=0;i<=(vec.length-1);i++){
                    var vecAct = vec[i].split('-');
                    if( vecAct[0]==sele ){
                       formulario.tipoDocumento.options[fila].value = vecAct[1];
                       formulario.tipoDocumento.options[fila].text  = vecAct[2];
                       fila++;
                    }
             }
     }  
     
        
      function validarBusqueda(theForm){       
          var sw   = 0;
          
          var cont = 0;
          for(var i=0;i<theForm.length;i++){
                if(  theForm.elements[i].type=='checkbox'  &&  theForm.elements[i].checked ==true    )
                     cont++;
          }
          
          if( cont==0 ){
               alert('Deber� seleccionar por lo menos un filtro a aplicar');
               sw = 1;
               return false;
          }
          
          if( sw==0){
          
              if(  theForm.byActividad.checked == true  &&  theForm.actividad.value=='' ){
                   alert('Deber� seleccionar el Tipo de actividad del filtro');
                   sw = 1;
                   return false;    
              }

              if(  theForm.bytipoDocumento.checked == true  &&  theForm.tipoDocumento.value=='' ){
                   alert('Deber� seleccionar el Tipo de Documento del filtro');
                   sw = 1;
                   return false;    
              }

              if(  theForm.bydocumento.checked == true  &&  theForm.documento.value=='' ){
                   alert('Deber� digitar el n�mero del documento del filtro');
                   theForm.documento.focus();
                   sw = 1;
                   return false;    
              }

              if(  theForm.byAgencia.checked == true  &&  theForm.agencia.value=='' ){
                   alert('Deber� seleccionar la agencia del  filtro');
                   sw = 1;
                   return false;    
              }

              if(  theForm.byCantidad.checked == true  ){
                   if(  theForm.cantidad.value=='' ){
                        alert('Deber� digitar la cantidad de imagen a visualizar del  filtro');
                        theForm.cantidad.focus();
                        sw = 1;
                        return false; 
                   }
                   else if ( isNaN( theForm.cantidad.value  )  ){                   
                        alert('La cantidad de imagen deber� ser num�rica');
                        theForm.cantidad.focus();
                        sw = 1;
                        return false;
                   }
              }
          }
             
          if(sw==0)
             theForm.submit();
       }
       
   </script>
   
</head>
<body >

<center>



  <% Usuario usuario     = (Usuario) session.getAttribute("Usuario");
     String user         = usuario.getLogin();
     List   actividades  = model.ImagenSvc.getActividades();
     List   imagenes     = model.ImagenSvc.getImagenes()   ;
     List   agencias     = model.ImagenSvc.getAgencias()   ;
     String msj          = request.getParameter("comentario");
     String hoy          = Utility.getHoy("-"); %>
  
  <FORM ACTION='<%=CONTROLLER%>?estado=Buscar&accion=Imagenes' method='post' name='formulario' >
  <table width="470" border="2" align="center">
           
      <tr>
          <td ALIGN='center'>  
              <TABLE  width='100%' class='tablaInferior' >       
                  <tr>
                      <td colspan='2'>
                           <table cellpadding='0' cellspacing='0' width='100%'>
                                <tr class="fila">
                                    <td align="left" width='51%' class="subtitulo1">&nbsp;Filtros de Busqueda</td>
                                    <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                                </tr>
                           </table>
                      </td>
                 </tr>
         
                 <TR class="fila">
                      <TD ><input type='checkbox' name='byActividad'> Tipo Actividad </TD>
                      <TD>
                              <select name='actividad' class="textbox"  onchange='load()' style='width:100%'>
                              <% if( actividades !=null){
                                   Iterator it = actividades.iterator();
                                   while(it.hasNext()){
                                      String actividad = (String) it.next();
                                      String[] valor   = actividad.split("-");%>
                                      <option value='<%= valor[0]%>-<%= valor[1]%>'> <%= valor[1]%> </option>
                                  <% }
                                 }%>
                              </select>
                      </TD>
                 </TR>
                 
                 <TR class="fila">
                     <TD>
                         <input type='checkbox' name='bytipoDocumento'>
                         Tipo Documento 
                     </TD>
                     <TD>
                         <select name='tipoDocumento' class="textbox" style='width:100%'>               
                         </select>
                     </TD>
                 </TR>
         
                 
                 <TR class="fila">
                     <TD>
                        <input type='checkbox' name='bydocumento'>
                        No Documento   
                     </TD>
                     <TD><input type='text' name='documento' class="textbox" style='width:50%'></TD>
                 </TR>
         
                 
                 <TR class="fila">
                     <TD>
                        <input type='checkbox' name='byAgencia'>
                        Agencia  
                     </TD>
                     <TD>
                         <select name='agencia' class="textbox" style='width:100%'>
                          <% if( agencias !=null){
                                Iterator it = agencias.iterator();
                                while(it.hasNext()){
                                   String agencia = (String) it.next();
                                   String[] valor   = agencia.split("-");%>
                                   <option value='<%= valor[0]%>-<%= valor[1]%>'> <%= valor[1]%> </option>
                               <%}
                             }
                           %>
                         </select>
                     </TD>
                 </TR>
         
                 <TR class="fila">
                     <TD>
                        <input type='checkbox' name='byFecha'>
                        Fecha   
                     </TD>
                   <TD>
                            <!-- Fecha Inicial -->
                    <input  name='fechaInicio' size="11" readonly="true" class="textbox" value='<%=hoy%>' style='width:33%'> 
                             <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(fechaInicio);return false;" hidefocus>
                              <img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt="">
                             </a>

                             &nbsp
                             <!-- Fecha Final -->

                            <input  name='fechaFinal' size="11" readonly="true" class="textbox" value='<%=hoy%>' style='width:33%'> 
                           <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(fechaFinal);return false;" hidefocus>
                            <img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt="">
                           </a>


                     </TD>
                 </TR>
         
                  <TR class="fila">
                     <TD>
                        <input type='checkbox' name='byCantidad'>
                        Ultimas Imagenes 
                     </TD>
                     <TD><input type='text' name='cantidad' class="textbox" size='3' maxlength='3'></TD>
                  </TR>
                   
              
         <input type='hidden' name='usuario' value='<%=user%>' >
           
       </TABLE>  
      </td>
    </tr>
  </table>
  
     <br>
     <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar"  onClick=" validarBusqueda(formulario)"   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
     <img src="<%=BASEURL%>/images/botones/salir.gif"   name="imgsalir"     onClick=" parent.close();"               onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
     
          
  </FORM>
  
  
  
  <iframe width=188 height=166 name="gToday:datetime:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" id="gToday:datetime:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" src="<%=BASEURL%>/js/calendartsp/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"></iframe>

  
    
  <!-- Cargamos los Documentos de la Actividad Seleccionada  -->
  <script>load(); </script>
  
  
  
  
  
  
  <!-- LISTADO -->
  
  <% if( msj!=null && imagenes != null ){%>
  
  
   <% float tamanoColumna = 250f;
      float tope          = 20f;      
      int tamano          =  (int)( ( (float)imagenes.size() / tope ) * (float)tamanoColumna  ); 
      if ( tamano< 470)  
           tamano=470;
      if ( tamano>800){
           tamano = 800; 
           tope   =  ( (float)imagenes.size() * (float)tamanoColumna  )  /  tamano ;
      }%>
  
   
    <table width="<%=tamano%>"  border="2" align="center" >           
      <tr>
          <td ALIGN='center'  width="100%">  
               <table width="100%" align="center" bgcolor="#FFFFFF">
                          <tr>
                            <td width="373" class="subtitulo1">&nbsp;Listado de Imagenes...</td>
                            <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"></td>
                          </tr>
               </table>

               <TABLE width="100%" class='tablaInferior' >

               <% if( request.getParameter("byActividad")    !=null || 
                      request.getParameter("bytipoDocumento")!=null || 
                      request.getParameter("bydocumento")    !=null ||
                      request.getParameter("byAgencia")      !=null ||
                      request.getParameter("byFecha")        !=null ||
                      request.getParameter("byCantidad")     !=null) {%> 
                        <TR >
                           <TD>
                            <table cellpadding='0' cellspacing='0' width='100%'>
                                 <tr class="filaresaltada">
                                    <TD width='33%'>
                                                    <%=(request.getParameter("byActividad")    !=null)? "ACTIVIDAD:      "+ request.getParameter("actividad")     +"</BR>":""%>
                                                    <%=(request.getParameter("bytipoDocumento")!=null)? "TIPO DOCUMENTO: "+ request.getParameter("tipoDocumento") +"</BR>":""%>
                                                    <%=(request.getParameter("bydocumento")    !=null)? "DOCUMENTO:      "+ request.getParameter("documento")     +"</BR>":""%> 

                                                    <%=(request.getParameter("byAgencia")      !=null)? "AGENCIA:        "+ request.getParameter("agencia")       +"</BR>":""%> 
                                                    <%=(request.getParameter("byFecha")        !=null)? "PERIODO:        "+ request.getParameter("fechaInicio")   +" &nbsp&nbsp&nbsp "+ request.getParameter("fechaFinal")+"</BR>":""%> 
                                                    <%=(request.getParameter("byCantidad")     !=null)? "CANTIDAD REG.:  "+ request.getParameter("cantidad")      :""%> 

                                    </TD>        
                                </tr>
                            </table>
                           </TD>
                        </TR>
                <%}else{%>
                        <TR class="filaresaltada"><TD>General</TD></TR>
                <%}%>

                <TR>
                   <TD >  
                       <TABLE cellpadding='0' cellspacing='0' WIDTH='100%'>
                           <TR class="fila" valign='top'>
                                   <% int cont = 0;
                                      for(int i=0;i<imagenes.size();i++){
                                            Imagen imagen = (Imagen) imagenes.get(i);
                                            String url    = BASEURL + "/documentos/imagenes/" + user + "/" + imagen.getFileName();
                                            String tdoc   = imagen.getTipoDocumento();
                                            String tact   = imagen.getActividad();
                                            String doc    = imagen.getDocumento();
                                            String[] exts = imagen.getFileName().split("\\.");
                                            String   ext  = "";
                                            if( exts.length >0 ){
                                                ext  = exts[ exts.length - 1 ];
                                                ext  = ext.toUpperCase();
                                            }
                                            
                                            if(cont==0) {%> <TD class='letrafila'> <%}
                                            if(cont>=tope) {
                                               cont=0;%>
                                               </TD>
                                               <TD class='letrafila'>
                                            <%}%>
                                            
                                            
                                            <% if( ext.equals("TIF")  ||   ext.equals("TIFF")  ||  ext.equals("PDF")   ){%>
                                                 <a href='#' onclick=" MostrarImagen('<%= ext%>','<%=url%>','VistaPrevia',400,400,100,300);"><%= i+ 1 %>. <%=imagen.getFileName()%></a><br>
                                            <%}else{%>
                                                 <a href="javascript: viewImagen('<%=url%>','<%=tact%>','<%=tdoc%>','<%=doc%>')"> <%= i+ 1 %>. <%=imagen.getFileName()%></a><br>            
                                            <%}%>
                                            
                                            
                                            <%  cont++; %>
                                            
                                  <%}%>
                             </TR>
                        </TABLE>
                      </TD>
                  </TR>
               </TABLE>

        </td>
    </tr>
  </table>
  
  <%}%>
  
  
  
    
  <%if(msj!=null && !msj.equals(" ")){%> 
      <table border="2" align="center">
          <tr>
              <td>
                  <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                      <tr>
                            <td width="320" align="center" class="mensajes"><%=msj%></td>
                            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                            <td width="58">&nbsp;</td>
                     </tr>
                </table>
             </td>
         </tr>
    </table>
 <%}%>
    
    

<%=datos[1]%>  

    
</body>
</html>
