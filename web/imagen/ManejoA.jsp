 <!--  
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      11/11/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Vista que permite subir imagenes a la base de datos....
 --%>





<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="java.util.*" %>
<%@page import="com.tsp.util.Util"%>

<html>
<head>
   <title>Manejo de Imagen</title>
   <link href="<%= BASEURL %>/css/estilotsp.css" rel="stylesheet" type="text/css"> 
   <script type='text/javascript' src="<%= BASEURL %>/js/Validaciones.js"></script>
   
   
   <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
   <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
   
   
   
   <!-- Parametros de configuracion -->
  <% String  activity = ( request.getParameter("actividad")    !=null)? request.getParameter("actividad")    :"";
     String  tipoDoc  = ( request.getParameter("tipoDocumento")!=null)? request.getParameter("tipoDocumento"):"";
     String  document = ( request.getParameter("documento")    !=null)? request.getParameter("documento")    :"";
	 	 //nuevo veridoc

	 if ( (document!=null) && (tipoDoc!=null)){
	 	model.ImagenSvc.setDocumento_conf(document);
	    model.ImagenSvc.setEstado_conf(tipoDoc);
		model.ImagenSvc.setCodActividad(activity);
	 }
   %>
   
         
   
    <% 
     Usuario usuario       = (Usuario) session.getAttribute("Usuario");
     String user           = usuario.getLogin();
     String agencia        = usuario.getId_agencia();
     String fileLength     = "256000";    // 250 KB   
     String directory      = (request.getParameter("directory")  ==null)?"":request.getParameter("directory");
     List   actividades    = model.ImagenSvc.getActividades() ;
     List   listFile       =  (List) request.getAttribute("listFile");
     String msj            = ( request.getParameter("comentario")==null )?"":request.getParameter("comentario");
     String swExaminar     = request.getParameter("swExaminar");
     String title          = ( swExaminar==null )?"Cargar Imagenes":"Guardar Imagenes";
     String statusInsert   = ( request.getParameter("statusInsert")==null )?"false": request.getParameter("statusInsert") ;
   %>
  
   
   <script>
   
       <%= model.ImagenSvc.getJsRelacion() %>
              
         function loadCombos(){
             var fila   = 0;
             var sele   = formulario.actividad.value.split('-')[0];
             var vec    = relacion.split('|');
             
             
             for(i=0;i<=(vec.length-1);i++){
                var vecAct = vec[i].split('-');
                if( vecAct[0]==sele )
                    fila ++;
             }
             
             formulario.tipoDocumento.length = fila; 
             
             fila=0;
             for(i=0;i<=(vec.length-1);i++){
                var vecAct = vec[i].split('-');
                if( vecAct[0]==sele ){
                   formulario.tipoDocumento.options[fila].value = vecAct[1];
                   formulario.tipoDocumento.options[fila].text  = vecAct[2];
				   if( vecAct[1]=='<%=tipoDoc%>')
                       formulario.tipoDocumento.options[fila].selected='selected';
                   fila++;
                }
             } 
         }
       
         
        function sendImages(theForm, url){ 
            var sw=0;
            if(theForm.documento.value=='') {
                alert('Deber� Ingresar un n�mero  de Documento...');
                theForm.documento.focus();
                sw = 1;
            }
            if(sw==0){
               theForm.action = url;
               theForm.submit();
           }
        }
          
        
        function viewImagen(imagen){
             NuevaVentana22('<%=BASEURL%>/documentos/imagenes/viewPrevias/<%=user%>/'+imagen,'VistaPrevia',400,400,100,300);
        }
    </script>
  
</head>
<body>
  <center>

  
 
  <FORM ACTION='' method='post' enctype="MULTIPART/FORM-DATA"   NAME='formulario'>
  <table width="550" border="2"align="center"  >
      <tr>
          <td ALIGN='center'>  
              <TABLE  width='100%'   class='tablaInferior'  >       
                  <tr>
                      <td colspan='2'>
                           <table cellpadding='0' cellspacing='0' width='100%'>
                                <tr class="fila">
                                    <td align="left" width='55%' class="subtitulo1">&nbsp;<%=title%></td>
                                    <td align="left" width='*'   class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                                </tr>
                           </table>
                      </td>
                 </tr>
       
         <TR class="fila">
             <TD width='25%' >Tipo Actividad </TD>
             <TD width='*'>
                  <select name='actividad' class="textbox" style='width:80%' onchange='loadCombos()'>
                  <% if( actividades !=null){
                         Iterator it = actividades.iterator();
                         while(it.hasNext()){
                             String actividad = (String) it.next();
                             String[] valor   = actividad.split("-");
                             String select    = (valor[0].equals(activity))?"selected='selected'":"";%>
                             <option   value='<%= valor[0]%>'  <%=select%>> <%= valor[1]%> </option>
                      <% }
                     }%>
                  </select>
                  <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
               </TD>
         </TR>
         
         <TR class="fila">
             <TD>Tipo Documento </TD>
             <TD> <select name='tipoDocumento' class="textbox" style='width:80%'> </select>
                  <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
             </TD>
         </TR>
         
         <TR class="fila">
             <TD>No Documento   </TD>
             <TD><input type='text'  name='documento' class="textbox" style='width:80%' value='<%=document%>'>
                 <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></TD>
         </TR>
         
         
         <% if (swExaminar==null){%>
         
                 <TR class="fila">  <TD >Imagen 1 </TD><TD><input type='file'   name='filename' style='width:100%' class="textbox" > </TD></TR>

                 <TR class="fila">  <TD>Imagen 2 </TD><TD><input type='file'   name='filename2' style='width:100%' class="textbox" > </TD></TR>

                 <TR class="fila">  <TD>Imagen 3 </TD><TD><input type='file'   name='filename3' style='width:100%' class="textbox" > </TD></TR>

                 <TR class="fila">  <TD>Imagen 4 </TD><TD><input type='file'   name='filename4' style='width:100%' class="textbox" > </TD></TR>

                 <TR class="fila">  <TD>Imagen 5 </TD><TD><input type='file'   name='filename5' style='width:100%' class="textbox" > </TD></TR>

                 <TR class="fila">  <TD>Imagen 6 </TD><TD><input type='file'   name='filename6' style='width:100%' class="textbox" > </TD></TR>

                 <TR class="fila">  <TD>Imagen 7 </TD><TD><input type='file'   name='filename7' style='width:100%' class="textbox" > </TD></TR>

                 <TR class="fila">  <TD>Imagen 8 </TD><TD><input type='file'   name='filename8' style='width:100%' class="textbox" > </TD></TR>

                 <TR class="fila">  <TD>Imagen 9 </TD><TD><input type='file'   name='filename9' style='width:100%' class="textbox" > </TD></TR>

                 <TR class="fila">  <TD>Imagen 10 </TD><TD><input type='file'  name='filename10' style='width:100%' class="textbox" > </TD></TR>

             </TABLE>  
            </td>
           </tr>
         </table>
         <br>
         
          <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick=" sendImages(formulario,'<%=BASEURL%>/view.do?maxFileSize=<%=fileLength%>&user=<%=user%>');"   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
          <img src="<%=BASEURL%>/images/botones/salir.gif"    name="imgsalir"   onClick=" parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
     
          
         <%}else{%>
         
                 <TR class="fila">                <TD>Directorio </TD><TD  class="letrafila"> <%=directory%>  </TD></TR>
                 <TR class="fila" valign='top'>   <TD>Imagen(es) </TD>             
                      <TD >               
                          <table cellpadding='0' cellspacing='0' width='100%' class="fila">
                            <% if( listFile !=null &&  listFile.size()>0 ){
                                   Iterator it = listFile.iterator();
                                   int cont = 1;
                                   while(it.hasNext()){
                                      String archivo = (String) it.next();%> <tr><td width='10%' > <%=cont%>. </td><td> <A href="javascript:viewImagen('<%=archivo%>')"> <%=archivo%> </a></td></tr>
                            <%     cont++;}}%>                     
                          </table>
                      </TD>
                 </TR>     
        
             
             
            </TABLE> 
          </td>
        </tr>
      </table>
      <br>
      
      <img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgregresar" onClick="javascript: window.location.href('<%=BASEURL%>/imagen/Manejo.jsp?actividad=<%=activity%>&tipoDocumento=<%=tipoDoc%>&documento=<%=document%>') "                                                                                                                              onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
      <img src="<%=BASEURL%>/images/botones/aceptar.gif"   name="imgaceptar"  onClick="sendImages(formulario,'<%=BASEURL%>/upload.do?maxFileSize=<%=fileLength%>&user=<%=user%>&actividad='+ formulario.actividad.value +'&tipoDoc='+ formulario.tipoDocumento.value +'&documento='+ formulario.documento.value +'&agencia=<%=agencia%>&directory=<%=directory%>')" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
      
      <%}%>
   
  </FORM>
  
  
  
  <!-- Cargamos los Documentos de la Actividad Seleccionada  -->
  <script>loadCombos();</script>
  
    
  <!-- habilitamos el focus -->
  <script>formulario.filename.focus();</script>
  
  
  
  
  <%  System.out.println("Estado Almacenamiento "+statusInsert);
      System.out.println("Documento "+model.ImagenSvc.getDocumento_conf());
	  System.out.println("Tipo Doc "+model.ImagenSvc.getEstado_conf());
	  System.out.println("ACTIVIDAD "+model.ImagenSvc.getCodActividad());
    if( statusInsert.equals("true") && model.ImagenSvc.getCodActividad().equals("003") ){
	        if ( !model.ImagenSvc.getDocumento_conf().equals("") && !model.ImagenSvc.getEstado_conf().equals("") ){
      			model.veridocService.tieneDocumentosConductor(model.ImagenSvc.getDocumento_conf(),model.ImagenSvc.getEstado_conf());
				model.veridocService.cargarDocConductor(model.ImagenSvc.getDocumento_conf());
				out.print("<script>parent.opener.location.reload();</script>");	
			}
    }
	if( statusInsert.equals("true") && model.ImagenSvc.getCodActividad().equals("005") ){
	        if ( !model.ImagenSvc.getDocumento_conf().equals("") && !model.ImagenSvc.getEstado_conf().equals("") ){
      			model.veridocService.tieneDocumentosPlaca(model.ImagenSvc.getDocumento_conf(),model.ImagenSvc.getEstado_conf());
				model.veridocService.cargarDocPlaca(model.ImagenSvc.getDocumento_conf());				
   			    out.print("<script>parent.opener.location.reload();</script>");	
			}
    }
	%>
  
	
	
	
	
  <!-- Mensaje -->
  <%if(msj!=null && !msj.equals("")){%> 

      <table border="2" align="center">
      <tr>
        <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="320" align="center" class="mensajes"><%=msj%></td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="58">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table>

    <%}%>

    
  
</body>
</html>
