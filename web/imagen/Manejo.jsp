 <!--
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      11/11/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Vista que permite subir imagenes a la base de datos....
 --%>





<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="java.util.*" %>
<%@page import="com.tsp.util.Util"%>

<html>
<head>
   <title>Manejo de Imagen</title>
   <link href="<%= BASEURL %>/css/estilotsp.css" rel="stylesheet" type="text/css"> 
   <script type='text/javascript' src="<%= BASEURL %>/js/Validaciones.js"></script>
   
   
   <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
   <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
   
   
   <!-- Help -->
  <%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
      String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
  %>


   <% model.ImagenSvc.load(); %>
   
   <!-- Parametros de configuracion -->
  <%   String  activity       = ( request.getParameter("actividad")    !=null)? request.getParameter("actividad")    : model.ImagenSvc.getActividad();
       String  tipoDoc        = ( request.getParameter("tipoDocumento")!=null)? request.getParameter("tipoDocumento"): model.ImagenSvc.getTipoDocumento();
       String  document       = ( request.getParameter("documento")    !=null)? request.getParameter("documento")    : model.ImagenSvc.getDocumento();
       String  procedencia    = request.getParameter("procedencia"); 
       String  estadoNormal   = "NORMAL";
       String  estadoDespacho = "DESPACHO";

       if ( procedencia==null  ){
            model.ImagenSvc.atras();
            procedencia = estadoNormal;
       }
       session.setAttribute("swProcedencia", procedencia );

       if ( (document!=null) && (tipoDoc!=null) ){
	 	model.ImagenSvc.setDocumento_conf(document);
	        model.ImagenSvc.setEstado_conf(tipoDoc);
		model.ImagenSvc.setCodActividad(activity);
       }%>
   
      
  <% Usuario  usuario        = (Usuario) session.getAttribute("Usuario");
     String   user           = usuario.getLogin();
     List     actividades    = model.ImagenSvc.getActividades() ;
     List     listFile       = model.ImagenSvc.getImagenes();     
     String   msj            = ( request.getParameter("comentario")==null )?"":request.getParameter("comentario");
     String   title          = ( ! model.ImagenSvc.isLoad() )?"Datos del registro":"Guardar Imagenes";
     String   statusInsert   = ( request.getParameter("statusInsert")==null )?"false": request.getParameter("statusInsert") ;
     
     String   estadoSelect   = ( !activity.equals("") )? "disabled" : "";
     String   estadoText     = ( !document.equals("") ||  procedencia.equals( estadoDespacho )   )? "readonly" : "";%>
  
    
   <script>
   
       <%= model.ImagenSvc.getJsRelacion() %>
              
         function loadCombos(){
             var fila   = 0;
             var sele   = formulario.actividad.value.split('-')[0];
             var vec    = relacion.split('|');
             for(i=0;i<=(vec.length-1);i++){
                var vecAct = vec[i].split('-');
                if( vecAct[0]==sele )
                    fila ++;
             }
             formulario.tipoDocumento.length = fila; 
             fila=0;
             for(i=0;i<=(vec.length-1);i++){
                var vecAct = vec[i].split('-');
                if( vecAct[0]==sele ){
                   formulario.tipoDocumento.options[fila].value = vecAct[1];
                   formulario.tipoDocumento.options[fila].text  = vecAct[2];
				   if( vecAct[1]=='<%=tipoDoc%>')
                       formulario.tipoDocumento.options[fila].selected='selected';
                   fila++;
                }
             } 
       }
       
       
       function sendImages(theForm, url){ 
            var sw=0;
            activar(theForm);
            if(theForm.actividad.value=='') {
                alert('Deber� seleccionar la actividad');
                theForm.actividad.focus();
                sw = 1;
            }
            
            if(theForm.tipoDocumento.value=='') {
                alert('Deber� Ingresar el tipo de documento');
                theForm.tipoDocumento.focus();
                sw = 1;
            }
            
            <% if ( procedencia.equals( estadoNormal ) ){%>
                if(theForm.documento.value=='') {
                    alert('Deber� Ingresar un n�mero  del documento');
                    theForm.documento.focus();
                    sw = 1;
                }
            <%}%>
            
            if(sw==0){
               var cont=0;
               for(var i=0;i<theForm.length;i++){
                  if( theForm.elements[i].type=='file'  &&  theForm.elements[i].value !='' )
                      cont++;
               }
               if( cont==0){
                  <% if ( procedencia.equals( estadoDespacho ) ){%>
                         desactivar(theForm);
                  <%}%>
                  alert('Deber� seleccionar por lo menos una imagen');
               }
               else{
                 theForm.action = url;
                 theForm.submit();
               }
            }
            else{
                <% if ( procedencia.equals( estadoDespacho ) ){%>
                      desactivar(theForm);
                <%}%>                
            }
                
           
            
        }        
        
        
        
        function loadImagen(theForm, url){
             activar(theForm);
             theForm.action = url;
             theForm.submit();
        }        
        
        
        
        
       function MostrarImagen( ext, url,nombre,largo,hancho,x,y){
          option="  width="+largo+", height="+hancho+",   scrollbars=yes, statusbars=yes, resizable=yes ,top="+x+", left="+y;
          ventana=window.open('',nombre,option);
          ventana.location.href=url;
          ventana.focus();
          if( ext=='TIF' || ext =='TIFF')
              ventana.close();
        }
        
        
        function viewImagen(imagen, ext){
            <% if ( procedencia.equals( estadoDespacho ) ){%>  MostrarImagen(ext,'<%=BASEURL%>/documentos/imagenes/viewPrevias/DESPACHO/<%=user%>/' + imagen,'VistaPrevia',400,400,100,300); <%}%>
            <% if ( procedencia.equals( estadoNormal   ) ){%>  MostrarImagen(ext,'<%=BASEURL%>/documentos/imagenes/viewPrevias/<%=user%>/'          + imagen,'VistaPrevia',400,400,100,300); <%}%>
        }
        
        
        function activar(theForm){
           theForm.actividad.disabled='';
           theForm.documento.readonly='';
        }
        
        function desactivar(theForm){
           theForm.actividad.disabled='disabled';
           theForm.documento.readonly='readonly';
        }
        
       
    </script>
  
</head>
<body>  
<center>

  
 
 
  <FORM ACTION='' method='post' enctype="MULTIPART/FORM-DATA"   NAME='formulario'>
  <table width="550" border="2"align="center"  >
      <tr>
          <td ALIGN='center'>  
              <TABLE  width='100%'   class='tablaInferior'  >       
                  <tr>
                      <td colspan='2'>
                           <table cellpadding='0' cellspacing='0' width='100%'>
                                <tr class="fila">
                                    <td align="left" width='55%' class="subtitulo1">&nbsp;<%=title%></td>
                                    <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                                </tr>
                           </table>
                      </td>
                 </tr>
       
         <TR class="fila">
             <TD width='25%' >TIPO ACTIVIDAD </TD>
             <TD width='*'>
                  <select name='actividad' class="textbox" style='width:80%' onchange='loadCombos()'  <%= estadoSelect %> >
                  <% if( actividades !=null){
                         Iterator it = actividades.iterator();
                         while(it.hasNext()){
                             String actividad = (String) it.next();
                             String[] valor   = actividad.split("-");
                             String select    = (valor[0].equals(activity))?"selected='selected'":"";%>
                             <option   value='<%= valor[0]%>'  <%=select%>> <%= valor[1]%> </option>
                      <% }
                     }%>
                  </select>
                  <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
               </TD>
         </TR>
         
         <TR class="fila">
             <TD>TIPO DOCUMENTO </TD>
             <TD> <select name='tipoDocumento' class="textbox" style='width:80%' > </select>
                  <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
             </TD>
         </TR>
         
         <TR class="fila">
             <TD>No DOCUMENTO   </TD>
             <TD><input type='text'  name='documento' class="textbox" style='width:80%' value='<%=document%>'  <%= estadoText %>>
                 <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></TD>
         </TR>
         
         
         
         <input type='hidden' name='procedencia'  value='<%= procedencia%>'>
         
         
         
         <%if (! model.ImagenSvc.isLoad() ){%>
            
                  <td colspan='2'>
                           <table cellpadding='0' cellspacing='0' width='100%'>
                                <tr class="fila">
                                    <td align="left" width='55%' class="subtitulo1">&nbsp;Imagen(es) Asociada(s)</td>
                                    <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"></td>
                                </tr>
                           </table>
                   </td>
         
                  <%for( int i=1;i<=10;i++){%>         
                        <TR class="fila">  <TD align='center'> <%= i %> </TD><TD><input type='file'   name='filename' style='width:100%' class="textbox" >  </TD></TR>
                  <%}%>

                 </TABLE>  
                </td>
              </tr>

             </table> 
             <br>

             <img src="<%=BASEURL%>/images/botones/aceptar.gif"      name="imgaceptar" onClick=" sendImages(formulario,'<%=BASEURL%>/view.do');"                                                                                             onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
             <img src="<%=BASEURL%>/images/botones/restablecer.gif"  name="imgf5"      onClick=" window.location.href= '<%=CONTROLLER%>?estado=Load&accion=Imagenes&pagina=/imagen/Manejo.jsp&procedencia=<%=procedencia%>&evento=CLEAR';"   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
             <img src="<%=BASEURL%>/images/botones/salir.gif"        name="imgsalir"   onClick=" parent.close();"                                                                                                                            onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;

         <%}else{%>         
                          <TR class="fila" valign='top'>   <TD> IMAGEN(ES) </TD>  
                          <TD >               
                              <table cellpadding='0' cellspacing='0' width='100%' class="fila">
                                <% if( listFile !=null &&  listFile.size()>0 ){
                                       for(int k=0;k<listFile.size();k++){
                                            String archivo = (String) listFile.get(k);
                                            String[] exts  = archivo.split("\\.");
                                            String   ext   = "";
                                            if( exts.length >0 ){
                                                ext  = exts[ exts.length - 1 ];
                                                ext  = ext.toUpperCase();
                                            }%>
                                            
                                            <tr>
                                              <td width='10%' > <%= k +1 %>. </td>
                                              <td> <A href="javascript:viewImagen('<%=archivo%>','<%=ext%>')"> <%=archivo%> </a></td>
                                            </tr>
                                      <%}
                                   }%>                
                              </table>
                          </TD>
                     </TR>  
                </TABLE> 
              </td>
            </tr>
          </table>
          <br>


           <% if ( procedencia!=null && procedencia.trim().equals( estadoNormal ) ){%>
                   <img src="<%=BASEURL%>/images/botones/aceptar.gif"    name="imgaceptar"  onClick="loadImagen(formulario,'<%=BASEURL%>/upload.do')"                                                                                           onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
           <%}%> 

           <img src="<%=BASEURL%>/images/botones/regresar.gif"           name="imgregresar" onClick=" window.location.href('<%=CONTROLLER%>?estado=Load&accion=Imagenes&pagina=/imagen/Manejo.jsp&evento=ATRAS&procedencia=<%=procedencia%>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
           <img src="<%=BASEURL%>/images/botones/salir.gif"              name="imgsalir"    onClick="parent.close();"                                                                                                                           onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; 
           
      <%}%>
   
  </FORM>
  
  
  
  <!-- Cargamos los Documentos de la Actividad Seleccionada  -->
  <script>
          loadCombos();
          <% if ( !model.ImagenSvc.isLoad() ){%>
                 formulario.documento.focus();
          <%}%>
  </script>
  
  
  
  
  <!-- Diogenes -->
  <%  if( statusInsert.equals("true") && model.ImagenSvc.getCodActividad().equals("003") ){
	 model.veridocService.cargarDocConductor(model.ImagenSvc.getDocumento_conf());
      }
      if( statusInsert.equals("true") && model.ImagenSvc.getCodActividad().equals("005") ){
	 model.veridocService.cargarDocPlaca(model.ImagenSvc.getDocumento_conf());
      }%>
  
	
	
	
	
  <!-- Mensaje -->
  <%if(msj!=null && !msj.equals("")){%> 

      <table border="2" align="center">
      <tr>
        <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="500" align="center" class="mensajes"><%=msj%></td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="58">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table>

    <%}%>

    
 
<%=datos[1]%>  

  
</body>
</html>
