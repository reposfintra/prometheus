<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
    <title>Manejo de Imagenes</title>
    <META http-equiv=Content-Type content="text/html; charset=windows-1252">
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body>
<center>

  
<% String BASEIMG = BASEURL +"/images/ayuda/imagen/"; %>
<table width="100%"  border="2" align="center">
    <tr>
      <td >
 
            <table width='99%' align="center" cellpadding='0' cellspacing='0'>
                 <tr class="subtitulo" ><td height="20"><div align="center">MANUAL DE ADICION DE IMAGENES </div></td></tr>
                 <tr class="subtitulo1"> <td>Descripci&oacute;n del funcionamiento </td> </tr>

                 
                 <tr>
                      <td  class="ayudaHtmlTexto" height='40'>
                         El programa permitir� subir imagenes asociadas a un documento. 
                         para lo cual deber� seleccionar:
                         <ul>
                            <li>Tipo de Actividad</li>
                            <li>Tipo de Docuemnto</li>
                            <li>C�digo o N�mero del documento al cual desea asociar la(s) imagen(es)</li>
                            <li>La(s) imagen(es) que desea adjuntar</li>
                         </ul>
                         Tal como lo indica la Figura 1.
                      </td>
                 </tr>
          
                 <tr><td  align="center" ><img  src="<%= BASEIMG%>Adicion.GIF" >  <br> <strong>Figura 1</strong></td></tr>
                 
                  <tr>
                      <td  class="ayudaHtmlTexto" height='200'>
                         Para este caso seleccionamos los datos:
                         <ul>
                              <li>Tipo de Actividad  : Registro Placa</li>
                              <li>Tipo de Documento  : Soat</li>
                              <li>C�digo de Documento: 123</li>
                              <li>En la selecci�n de la imagen a subir, deber� realizar click en cada boton "Examinar" de acuerdo a la cantidad
                                  de imagenes que desee adjuntar al documento.
                                   e aparecer� una ventana como lo indica la Figura 2 en la cual deber� seleccionarla.  </li>
                         </ul>
                      </td>
                 </tr>
                 
                 
                 <tr><td  align="center" ><img  src="<%= BASEIMG%>Seleccion.GIF" >  <br> <strong>Figura 2</strong></td></tr>
                 
                 
                 <tr>
                      <td  class="ayudaHtmlTexto" height='100'>
                        Una vez introducidos los datos y seleccionadas las imagenes a subir, deber� realizar click en el boton "Aceptar"
                        para lo cual el programa subir�  temporalmente la imagenes al servidor y le presentar� una vista de confirmaci�n
                        donde le aparecer� los datos introducidos y las imagenes a relacionar, tal como lo indica la Figura 3.
                      </td>
                 </tr>
                 
                <tr><td  align="center" ><img  src="<%= BASEIMG%>Confirmacion.GIF" >  <br> <strong>Figura 3</strong></td></tr>
                 
                 
                <tr>
                      <td  class="ayudaHtmlTexto" height='100'>
                        Si desea ver una vista previa de las imagenes a subir, deber� realizar click sobre la imagen y el programa le abrir�
                        la imagen en una nueva ventana.
                        Una vez rectificado los datos  e imagenes a subir, deber� realizar click en el bot�n "Aceptar" para que el programa
                        grabe las imagenes a la base de datos.<br>
                        Al grabar las imagenes se le presentar� un mensaje indicando comentarios del proceso de subida a la base de datos.
                      </td>
                 </tr>
                 
                 
           </table>
            
      </td>
  </tr>
</table>
<p>
 <img src="<%=BASEURL%>/images/botones/salir.gif"   name="imgsalir"     onClick=" parent.close();"               onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
</p>
 
</body>
</html>
