<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
    <title>Manejo de Imagenes</title>
    <META http-equiv=Content-Type content="text/html; charset=windows-1252">
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body>
<center>
   
 <% String BASEIMG = BASEURL +"/images/ayuda/imagen/"; %>

 <table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
              <table width="100%" border="0" align="center">
              
                    <tr  class="subtitulo">
                         <td height="24" colspan="2"><div align="center"> ADICION DE IMAGENES </div></td>
                    </tr>
                    <tr class="subtitulo1">
                        <td colspan="2"> Datos del registro </td>
                    </tr>

                    <tr>
                        <td width="149" class="fila"> Tipo de Actividad </td>
                        <td width="525"  class="ayudaHtmlTexto"> Campo para seleccionar el tipo de Actividad al cual pertenece el documento</td>
                    </tr>

                    <tr>
                        <td width="149" class="fila"> Tipo de Documento </td>
                        <td width="525"  class="ayudaHtmlTexto"> Campo para seleccionar el tipo de Documento </td>
                    </tr>

                     <tr>
                        <td width="149" class="fila"> No Documento </td>
                        <td width="525"  class="ayudaHtmlTexto"> 
                               Campo para digitar el n�mero o c�digo del documento al cual desea adjuntar las imagenes, este campo
                               es alfanum�rico.
                        </td>
                    </tr>

                     <tr>
                        <td width="149" class="fila"> Imagen(es) </td>
                        <td width="525"  class="ayudaHtmlTexto"> 
                               Campo en el cual deber� cargar el url de la imagen a subir. El formulario tiene capacidad para subir maximo 10 imagenes
                               relacionadas al documento.
                        </td>
                    </tr>

          </table>
        
    </td>
   </tr>
 </table>
 
 
<p>
 <img src="<%=BASEURL%>/images/botones/salir.gif"   name="imgsalir"     onClick=" parent.close();"               onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
</p>


</body>
</html>
