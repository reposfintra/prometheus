<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
    <title>Busqueda de Imagenes</title>
    <META http-equiv=Content-Type content="text/html; charset=windows-1252">
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
   <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body>
<center>
  
<% String BASEIMG = BASEURL +"/images/ayuda/imagen/"; %>

<table width="100%"  border="2" align="center">
    <tr>
      <td >
 
            <table width='99%' align="center" cellpadding='0' cellspacing='0'>
                 <tr class="subtitulo" ><td height="20"><div align="center">MANUAL DE BUSQUEDA DE IMAGENES </div></td></tr>
                 <tr class="subtitulo1"> <td>Descripci&oacute;n del funcionamiento </td> </tr>

                 
                 <tr>
                      <td  class="ayudaHtmlTexto" height='40'>
                         El programa permitir� consultar imagenes grabadas asociadas a un documento, para lo cual
                         deber� seleccionar los filtros que desee aplicar y sus datos correspondientes para la
                         busqueda.
                         Se presentar�n filtros por:
                         
                         <ul>
                            <li>Tipo de Actividad</li>
                            <li>Tipo de Docuemnto</li>
                            <li>C�digo o N�mero del documento al cual desea asociar la(s) imagen(es)</li>
                            <li>Agencia</li>
                            <li>Fechas de grabaciones</li>
                            <li>Cantidad de las �ltimas imagenes grabadas</li>
                         </ul>
                         
                         Tal como lo indica la Figura 1.<br><br>
                      </td>
                 </tr>
          
                 <tr><td  align="center" ><img  src="<%= BASEIMG%>Busqueda.GIF" >  <br> <strong>Figura 1</strong></td></tr>
                 
                 
                 <tr>
                      <td  class="ayudaHtmlTexto" height='100'>
                        Una vez seleccionados los filtros y establecidos los datos para ellos, deber�
                        realizar click en el bot�n "Aceptar", para lo cual el programa realizar� la busqueda con los parametros
                        dados, y presentar� una lista de las imagenes encontradas, como lo indica la Figura 2.
                      </td>
                 </tr>
                 
                 <tr><td  align="center" ><img  src="<%= BASEIMG%>Resultado.GIF" >  <br> <strong>Figura 2</strong></td></tr>
                 
                 <tr>
                      <td  class="ayudaHtmlTexto" height='100'>
                        Para visualizar las imagenes encontradas, deber� realizar click sobre la imagen deseada y el programa le
                        abrir� la imagen en una nueva ventana.
                      </td>
                 </tr>
                          
           </table>
            
      </td>
  </tr>
</table>
                 
<p>
 <img src="<%=BASEURL%>/images/botones/salir.gif"   name="imgsalir"     onClick=" parent.close();"               onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
</p>
    


</body>
</html>
