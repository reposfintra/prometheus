<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
    <title>Busqueda de Imagenes</title>
    <META http-equiv=Content-Type content="text/html; charset=windows-1252">
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body>
  <center>
  <% String BASEIMG = BASEURL +"/images/ayuda/imagen/"; %>
  
  <table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
    
    
              <table width="100%" border="0" align="center">
                    <tr  class="subtitulo">
                         <td height="24" colspan="2"><div align="center"> BUSQUEDA DE IMAGENES </div></td>
                    </tr>
                    <tr class="subtitulo1">
                        <td colspan="2"> Busqueda de Imagen  </td>
                    </tr>

                    <tr>
                        <td width="149" class="fila"> Tipo de Actividad </td>
                        <td width="525"  class="ayudaHtmlTexto"> Campo para seleccionar el tipo de Actividad al cual pertenece el documento</td>
                    </tr>

                    <tr>
                        <td width="149" class="fila"> Tipo de Documento </td>
                        <td width="525"  class="ayudaHtmlTexto"> Campo para seleccionar el tipo de Documento </td>
                    </tr>

                     <tr>
                        <td width="149" class="fila"> No Documento </td>
                        <td width="525"  class="ayudaHtmlTexto"> 
                               Campo para digitar el n�mero o c�digo del documento al cual desea adjuntar las imagenes, este campo
                               es alfanum�rico.
                        </td>
                    </tr>

                     <tr>
                        <td width="149" class="fila">Agencia</td>
                        <td width="525"  class="ayudaHtmlTexto"> 
                            Campo para seleccionar la agencia del usuario quien grab� la(s) imagen(es)  
                        </td>
                    </tr>

                    <tr>
                        <td width="149" class="fila"> Fecha  </td>
                        <td width="525"  class="ayudaHtmlTexto"> Campo para digitar la fecha de grabaci�n de la(s) imagen(es), representado por n&uacute;meros y en el siguiente formato(AAAA-MM-DD)</td>
                    </tr>

                     <tr>
                        <td width="149" class="fila">Ultimas Imagenes</td>
                        <td width="525"  class="ayudaHtmlTexto"> 
                            Campo para seleccionar la cantidad de imagenes a visualizar ordenadas decendentemente seg�n fecha de grabaci�n.  
                            Este campo es unicamente num�rico.
                        </td>
                    </tr>
            </table>
        
        
        
      </td>
   </tr>
</table>
  
 
 
<p>
 <img src="<%=BASEURL%>/images/botones/salir.gif"   name="imgsalir"     onClick=" parent.close();"               onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
</p>

</body>
</html>
