<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Modificar Despacho Valido</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%=BASEURL%>/css/letras.css" rel="stylesheet" type="text/css">
<script src="<%=BASEURL%>/js/validar.js"></script>

<style type="text/css">
<!--
.Estilo1 {font-weight: bold}
-->
</style></head>
<body>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Modificar&accion=Insert&cmd=show" onSubmit="return ValidarFormModificar(this);">
 <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
  <table width="741" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="#EFE3DE" class="Letras">
    <tr bgcolor="#FFAA29">
      <td colspan="4"><div align="center"><span class="Estilo2"><strong>PLANILLA <%=request.getParameter("planilla")%><strong>
          <input name="planilla" type="hidden" id="planilla" value="<%=request.getParameter("planilla")%>">
      </strong></strong></span></div></td>
    </tr>
    <tr>
      <td width="165" bgcolor="#9CCFFF"><span class="Estilo1">Placa</span></td>
      <td width="174" bgcolor="<%=request.getAttribute("error1")%>"><span class="Estilo1 Estilo5 Estilo1">
        <input name="placa" type="text" id="placa" size="12" value="<%=request.getParameter("placa")%>" onChange="cambiarFormulario('<%=CONTROLLER%>?estado=Modificar&accion=Validar&cmd=show');" >
      </span></td>
      <td width="141" bgcolor="#9CCFFF"><span class="Estilo1">Estandar</span></td>
      <td width="238" bgcolor="#99cc99"><%=request.getParameter("estandar")%>
          <input name="standar" type="hidden" id="standar" value="<%=request.getParameter("standar")%>"></td>
    </tr>
    <tr>
      <td bgcolor="#9CCFFF"><span class="Estilo1">Trailer</span></td>
      <td bgcolor="#99cc99"><span class="Estilo6 Estilo1">
        <input name="trailer" type="text" id="trailer3" size="12" value="<%=request.getParameter("trailer")%>" onChange="cambiarFormulario('<%=CONTROLLER%>?estado=Modificar&accion=Validar&cmd=show');">
      </span></td>
      <td bgcolor="#9CCFFF"><span class="Estilo1 Estilo1">Orden de carga </span></td>
      <td bgcolor="#99cc99"><input name="orden" type="text" id="orden3" size="12" value="<%=request.getParameter("orden")%>" onChange="cambiarFormulario('<%=CONTROLLER%>?estado=Modificar&accion=Validar&cmd=show');" ></td>
    </tr>
    <tr>
      <td bgcolor="#9CCFFF"><span class="Estilo1">Conductor</span></td>
      <td bgcolor="<%=request.getAttribute("error2")%>"><span class="Estilo6 Estilo1">
        <input name="conductor" type="text" id="conductor5" size="12" value="<%=request.getParameter("conductor")%>" onChange="cambiarFormulario('<%=CONTROLLER%>?estado=Modificar&accion=Validar&cmd=show');" >
      </span></td>
      <td bgcolor="#9CCFFF"><span class="Estilo1">Nombre Conductor </span></td>
      <td bgcolor="<%=request.getAttribute("error2")%>"><%=request.getParameter("nombre")%></td>
    </tr>
    <tr>
      <td bgcolor="#9CCFFF"><span class="Estilo1">Peso lleno </span></td>
      <td bgcolor="<%=request.getAttribute("error3")%>"><input name="peso" type="text" id="peso3" size="12" value="<%=request.getParameter("peso")%>" onChange="cambiarFormulario('<%=CONTROLLER%>?estado=Modificar&accion=Validar&cmd=show');" ></td>
      <td bgcolor="#9CCFFF"><strong>Galon Acpm </strong></td>
      <td bgcolor="<%=request.getAttribute("acpm")%>"><span class="Estilo6 Estilo1">
        <input name="acpm" type="text" id="acpm" size="12" value="<%=request.getParameter("acpm")%>" onChange="cambiarFormulario('<%=CONTROLLER%>?estado=Modificar&accion=Validar&cmd=show');" >
      </span></td>
    </tr>
    <tr>
      <td bgcolor="#9CCFFF"><span class="Estilo1">Peso vacio </span></td>
      <td bgcolor="<%=request.getAttribute("pesoVacio")%>"><span class="Estilo6 Estilo1">
        <input name="pesov" type="text" size="12" value="<%=request.getParameter("pesov")%>" onChange="cambiarFormulario('<%=CONTROLLER%>?estado=Modificar&accion=Validar&cmd=show');" >
      </span></td>
      <td bgcolor="#9CCFFF"><span class="Estilo1">Proveedor de acpm </span></td>
      <td bgcolor="#99cc99"><%
		 if(model.proveedoracpmService.existProveedoresAcpm(usuario.getDstrct())){
			List list = model.proveedoracpmService.getProveedoresACPM(usuario.getDstrct());%>
        <select name="proveedorAcpm" id="select8">
          <%
		   	Iterator it=list.iterator();
				while (it.hasNext()){
					
					Proveedor_Acpm pa = (Proveedor_Acpm) it.next();
					
					String  nit= pa.getNit()+"/"+pa.getCodigo();
					String desc=pa.getNombre()+" "+pa.getCodigo();
					
				if(nit.equals(request.getParameter("proveedorAcpm"))){
				%>
          <option value="<%=nit%>" selected><%=desc%> </option>
          <%}else{%>
          <option value="<%=nit%>"><%=desc%> </option>
          <%}
		  }
			
		%>
        </select>
        <%}else{%>
        <strong>No existe ning&uacute;n Proveedor Registrado.</strong>
      <%}%></td>
    </tr>
    <tr>
      <td bgcolor="#9CCFFF"><strong>Valor Anticipo</strong></td>
      <td bgcolor="<%=request.getAttribute("anticipo")%>"><input name="anticipo" type="text" id="anticipo2" size="12" value="<%=request.getParameter("anticipo")%>"></td>
      <td bgcolor="#9CCFFF"><strong>Tiquete Peaje A </strong></td>
      <td bgcolor="<%=request.getAttribute("peajea")%>"><span class="Estilo1"><span class="Estilo5">
        <input name="peajea" type="text" id="peajea" size="12" value="<%=request.getParameter("peajea")%>" onChange="cambiarFormulario('<%=CONTROLLER%>?estado=Modificar&accion=Validar&cmd=show');" >
      </span></span></td>
    </tr>
    <tr>
      <td bgcolor="#9CCFFF"><strong>Proveedor Anticipo </strong></td>
      <td bgcolor="#99cc99"><span class="Estilo6">
      <%
		 if(model.proveedoranticipoService.existProveedoresAnticipo(usuario.getDstrct())){
			List list = model.proveedoranticipoService.getProveedoresAnticipo(usuario.getDstrct());%>
      <select name="proveedorA" id="proveedorA">
        <%
		   	Iterator it=list.iterator();
				while (it.hasNext()){
					
					Proveedor_Anticipo pa = (Proveedor_Anticipo) it.next();
					
					String  nit= pa.getNit()+"/"+pa.getCodigo();
					String desc=pa.getNombre()+" "+pa.getCodigo();
					if(nit.equals(request.getParameter("proveedorA"))){
				%>
        <option value="<%=nit%>" selected><%=desc%> </option>
        <%}else{%>
        <option value="<%=nit%>"><%=desc%> </option>
        <%}
				}
			
		%>
      </select>
      <%}else{%>
      <strong>No existe ning&uacute;n Proveedor Registrado.</strong>
      <%}%>
</span></td>
      <td bgcolor="#9CCFFF"><strong>Tiquete Peaje B </strong></td>
      <td bgcolor="<%=request.getAttribute("peajeb")%>"><span class="Estilo1"><span class="Estilo5">
        <input name="peajeb" type="text" id="peajeb" size="12" value="<%=request.getParameter("peajeb")%>" onChange="cambiarFormulario('<%=CONTROLLER%>?estado=Modificar&accion=Validar&cmd=show');" >
      </span></span></td>
    </tr>
    <tr>
      <td bgcolor="#9CCFFF"><strong><span class="Estilo1">Proveedor de Tiquetes </span> </strong></td>
      <td bgcolor="#99cc99"><span class="Estilo1"><span class="Estilo5">
        <%
		 if(model.proveedortiquetesService.existProveedoresTiquetes(usuario.getDstrct())){
			List list = model.proveedortiquetesService.getProveedoresTIQUETES(usuario.getDstrct());%>
        <select name="tiquetes" id="tiquetes">
          <%
		   	Iterator it=list.iterator();
				while (it.hasNext()){
					
					Proveedor_Tiquetes pa = (Proveedor_Tiquetes) it.next();
					
					String  nit= pa.getNit()+"/"+pa.getCodigo();
					String desc=pa.getNombre()+" "+pa.getCodigo();
					
					if(nit.equals(request.getParameter("tiquetes"))){
				%>
          <option value="<%=nit%>" selected><%=desc%> </option>
          <%}else{%>
          <option value="<%=nit%>"><%=desc%> </option>
          <%}
				}
			
		%>
        </select>
        <%}else{%>
        <strong>No existe ning&uacute;n Proveedor Registrado.</strong>
        <%}%>
      </span></span></td>
      <td bgcolor="#9CCFFF"><strong>Tiquete Peaje C</strong></td>
      <td bgcolor="<%=request.getAttribute("peajec")%>"> <span class="Estilo1"><span class="Estilo5">
        <input name="peajec" type="text" id="peajec" size="12" value="<%=request.getParameter("peajec")%>" onChange="cambiarFormulario('<%=CONTROLLER%>?estado=Modificar&accion=Validar&cmd=show');"  >
      </span></span></td>
    </tr>
  </table>
  <div align="center"><br>
    <input type="submit" name="Submit" value="Modificar">
    <input type="button" name="Submit2" value="Regresar">
  </div>
</form>
<br>
<p>&nbsp;</p>
</body>
</html>
