<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Modificar Despacho</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/letras.css" rel="stylesheet" type="text/css">

<style type="text/css">
<!--
.Estilo1 {font-weight: bold}
-->
</style>
</head>

<body>
  <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>

<table width="530" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="ECE0D8" class="Letras">
  <tr bgcolor="#FFA928">
    <td colspan="5" nowrap><div align="center" class="Estilo2"><strong><strong>DESPACHOS NO IMPRESOS</strong></strong></div></td>
  </tr>
  <tr bgcolor="#99CCFF">
    <td width="82" nowrap><strong>PLANILLA</strong></td>
    <td width="79"><div align="center"><strong>REMESA</strong></div></td>
    <td width="74" bgcolor="#99CCFF"><div align="center"><strong>PLACA</strong></div></td>
    <td width="111" class="Estilo4"><div align="center" class="Estilo10 Estilo2 Estilo3">
    <strong>CONDUCTOR</strong></div></td>
    <td width="150" class="Estilo4"><div align="center"><strong>NOMBRE CONDUCTOR</strong></div></td>
  </tr>
  <%if(model.planillaService.existPlanillas(usuario.getLogin())){
   List planillas = model.planillaService.getPlanillas(usuario.getLogin());
	  Iterator pla=planillas.iterator();
	  int i=0;
	  while (pla.hasNext()){
	  Planilla planilla = (Planilla) pla.next();
		%>
  <tr style="cursor:hand" title="Modificar Despacho..." onClick="window.location='ModificarDespacho/DatosModificar.jsp?planilla=<%=planilla.getNumpla()%>'" onMouseOver="bgColor= '#99cc99'" onMouseOut="bgColor=''">
    <td nowrap>
      <div align="center" class="Estilo1 Estilo5">    <%=planilla.getNumpla()%></div></td>
    <td>
    <div align="center" class="Estilo6"><%=planilla.getNumrem()%> </div></td>
    <td>
      <div align="center" class="Estilo6"><%=planilla.getPlaveh()%>    </div></td>
    <td><div align="center" class="Estilo4"><%=planilla.getCedcon()%></div></td>
    <td><span class="Estilo6"><%=planilla.getNomCond()%></span></td>
  </tr>
  <%}
  }%>
</table>
</body>
</html>
