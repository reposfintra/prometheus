<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Modificar Despacho</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%=BASEURL%>/css/letras.css" rel="stylesheet" type="text/css">
<script src="<%=BASEURL%>/js/validar.js"></script>
<style type="text/css">
<!--
.Estilo1 {font-weight: bold}
-->
</style></head>
<body>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Modificar&accion=Validar&cmd=show" onSubmit="return ValidarFormModificar(this);">
 <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
  <%
  	String placa="", trailer="", conductor="", nomcond="", estandard="", proveedorA="", proveedorAC="", proveedorT="", orden="", standar="",acpmcode="" ;
	float pesolleno=0, pesov=0, ant=0, peajea=0, peajeb=0, peajec=0, galon=0;
 	model.planillaService.buscaPlanilla(request.getParameter("planilla"));
    if(model.planillaService.getPlanilla()!=null){
     	Planilla pla= model.planillaService.getPlanilla();
		placa=pla.getPlaveh();
		trailer=pla.getPlatlr();
		conductor= pla.getCedcon();
		nomcond=pla.getNomCond();
		orden=pla.getOrden_carga();
		estandard=pla.getSj_desc();
		standar= pla.getSj();
    }
	model.planillaService.bucaPlaaux(request.getParameter("planilla"));
	if(model.planillaService.getPlaaux()!=null){
		Plaaux plaaux= model.planillaService.getPlaaux();
		pesolleno = plaaux.getFull_weight();
		pesov=plaaux.getEmpty_weight();
		ant=plaaux.getAcpm_gln();
		peajea=plaaux.getTicket_a();
		peajeb=plaaux.getTicket_b();
		peajec=plaaux.getTicket_c();
		galon= plaaux.getAcpm_gln();
		acpmcode=plaaux.getAcpm_code();
	}
	model.movplaService.buscaMovpla(request.getParameter("planilla"),"01");
	if(model.movplaService.getMovPla()!=null){
		Movpla movpla= model.movplaService.getMovPla();
		ant= movpla.getVlr();
        proveedorA=movpla.getProveedor()+"/"+movpla.getSucursal();
	}
	model.movplaService.buscaMovpla(request.getParameter("planilla"),"02");
	if(model.movplaService.getMovPla()!=null){
		Movpla movpla= model.movplaService.getMovPla();
		proveedorAC=movpla.getProveedor()+"/"+movpla.getSucursal();
		
	}
	model.movplaService.buscaMovpla(request.getParameter("planilla"),"03");
	if(model.movplaService.getMovPla()!=null){
		Movpla movpla= model.movplaService.getMovPla();
		proveedorT=movpla.getProveedor()+"/"+movpla.getSucursal();
		
	}
	
	%>
  		
  <table width="741" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="#EFE3DE" class="Letras">
    <tr bgcolor="#FFAA29">
      <td colspan="4"><div align="center"><span class="Estilo2"><strong>PLANILLA <%=request.getParameter("planilla")%>
        <input name="planilla" type="hidden" id="planilla" value="<%=request.getParameter("planilla")%>">
      </strong></span></div></td>
    </tr>
    <tr>
      <td width="165" bgcolor="#9CCFFF"><span class="Estilo1">Placa</span></td>
      <td width="174"><span class="Estilo1 Estilo5 Estilo1">
        <input name="placa" type="text" id="placa" size="12" value="<%=placa%>" >
      </span></td>
      <td width="141" bgcolor="#9CCFFF"><span class="Estilo1">Estandar</span></td>
      <td width="238"><%=estandard%>
      <input name="standar" type="hidden" id="standar" value="<%=standar%>"></td>
    </tr>
    <tr>
      <td bgcolor="#9CCFFF"><span class="Estilo1">Trailer</span></td>
      <td><span class="Estilo6 Estilo1">
        <input name="trailer" type="text" id="trailer" size="12" value="<%=trailer%>">
      </span></td>
      <td bgcolor="#9CCFFF"><span class="Estilo1 Estilo1">Orden de carga </span></td>
      <td><input name="orden" type="text" id="orden" size="12" value="<%=orden%>"></td>
    </tr>
    <tr>
      <td bgcolor="#9CCFFF"><span class="Estilo1">Conductor</span></td>
      <td><span class="Estilo6 Estilo1">
        <input name="conductor" type="text" id="conductor" size="12" value="<%=conductor%>">
</span></td>
      <td bgcolor="#9CCFFF"><span class="Estilo1">Nombre Conductor </span></td>
      <td><%=nomcond%></td>
    </tr>
    <tr>
      <td bgcolor="#9CCFFF"><span class="Estilo1">Peso lleno </span></td>
      <td><input name="peso" type="text" id="peso" size="12" value="<%=pesolleno%>"></td>
      <td bgcolor="#9CCFFF"><strong>Galon Acpm </strong></td>
      <td><span class="Estilo6 Estilo1">
      <input name="acpm" type="text" id="acpm" size="12" value="<%=galon%>">
</span></td>
    </tr>
    <tr>
      <td bgcolor="#9CCFFF"><span class="Estilo1">Peso vacio </span></td>
      <td><span class="Estilo6 Estilo1">
        <input name="pesov" type="text" size="12" value="<%=pesov%>">
      </span></td>
      <td bgcolor="#9CCFFF"><span class="Estilo1">Proveedor de acpm </span></td>
      <td><%
		 if(model.proveedoracpmService.existProveedoresAcpm(usuario.getDstrct())){
			List list = model.proveedoracpmService.getProveedoresACPM(usuario.getDstrct());%>
        <select name="proveedorAcpm" id="select8">
          <%
		   	Iterator it=list.iterator();
				while (it.hasNext()){
					
					Proveedor_Acpm pa = (Proveedor_Acpm) it.next();
					
					String  nit= pa.getNit()+"/"+pa.getCodigo();
					String desc=pa.getNombre()+" "+pa.getCodigo();
					
				if(nit.equals(proveedorAC)){
				%>
          <option value="<%=nit%>" selected><%=desc%> </option>
          <%}else{%>
		  <option value="<%=nit%>"><%=desc%> </option>
		  <%}
		  }
			
		%>
        </select>
        <%}else{%>
        <strong>No existe ning&uacute;n Proveedor Registrado.</strong>
        <%}%></td>
    </tr>
    <tr>
      <td bgcolor="#9CCFFF"><strong>Valor Anticipo</strong></td>
      <td><input name="anticipo" type="text" id="anticipo2" size="12" value="<%=ant%>"></td>
      <td bgcolor="#9CCFFF"><strong>Tiquete Peaje A </strong></td>
      <td><span class="Estilo1"><span class="Estilo5">
        <input name="peajea" type="text" id="peajea2" size="12" value="<%=peajea%>">
      </span></span></td>
    </tr>
    <tr>
      <td bgcolor="#9CCFFF"><strong>Proveedor Anticipo </strong></td>
      <td><span class="Estilo6">
      <%
		 if(model.proveedoranticipoService.existProveedoresAnticipo(usuario.getDstrct())){
			List list = model.proveedoranticipoService.getProveedoresAnticipo(usuario.getDstrct());%>
      <select name="proveedorA" id="proveedorA">
        <%
		   	Iterator it=list.iterator();
				while (it.hasNext()){
					
					Proveedor_Anticipo pa = (Proveedor_Anticipo) it.next();
					
					String  nit= pa.getNit()+"/"+pa.getCodigo();
					String desc=pa.getNombre()+" "+pa.getCodigo();
					if(nit.equals(proveedorA)){
				%>
        <option value="<%=nit%>" selected><%=desc%> </option>
        <%}else{%>
		<option value="<%=nit%>"><%=desc%> </option>
		<%}
				}
			
		%>
      </select>
      <%}else{%>
      <strong>No existe ning&uacute;n Proveedor Registrado.</strong>
      <%}%>
</span></td>
      <td bgcolor="#9CCFFF"><strong>Tiquete Peaje B </strong></td>
      <td><span class="Estilo1"><span class="Estilo5">
        <input name="peajeb" type="text" id="peajeb" size="12" value="<%=peajeb%>">
      </span></span></td>
    </tr>
    <tr>
      <td bgcolor="#9CCFFF"><strong><span class="Estilo1">Proveedor de Tiquetes </span> </strong></td>
      <td><span class="Estilo1"><span class="Estilo5">
        <%
		 if(model.proveedortiquetesService.existProveedoresTiquetes(usuario.getDstrct())){
			List list = model.proveedortiquetesService.getProveedoresTIQUETES(usuario.getDstrct());%>
        <select name="tiquetes" id="tiquetes">
          <%
		   	Iterator it=list.iterator();
				while (it.hasNext()){
					
					Proveedor_Tiquetes pa = (Proveedor_Tiquetes) it.next();
					
					String  nit= pa.getNit()+"/"+pa.getCodigo();
					String desc=pa.getNombre()+" "+pa.getCodigo();
					
					if(nit.equals(proveedorT)){
				%>
          <option value="<%=nit%>" selected><%=desc%> </option>
          <%}else{%>
		  <option value="<%=nit%>"><%=desc%> </option>
		  <%}
				}
			
		%>
        </select>
        <%}else{%>
        <strong>No existe ning&uacute;n Proveedor Registrado.</strong>
        <%}%>
      </span></span></td>
      <td bgcolor="#9CCFFF"><strong>Tiquete Peaje C</strong></td>
      <td> <span class="Estilo1"><span class="Estilo5">
        <input name="peajec" type="text" id="peajec" size="12" value="<%=peajec%>" >
      </span></span></td>
    </tr>
  </table>
 
  <div align="center"><br>
    <input type="submit" name="Submit" value="Validar">
    <input type="button" name="Submit2" value="Regresar">
  </div>
</form>
<br>
<p>&nbsp;</p>
</body>
</html>
