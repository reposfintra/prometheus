<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>Consultar Standard Job</title>
<script language="javascript" src="js/validar.js"></script>
<script src="<%=BASEURL%>/js/validar.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">

<script>

	var valorEstado = "";

	function setEstado ( valueEs ) {
	
		valorEstado = valueEs;	
		
	}
			
	var valorTipo = "";

	function setTipo ( valueTi ) {
	
		valorTipo = valueTi;	
		
	}
	
	function validar ( form ) {
		
		if ( valorEstado == "" ) {
		
			alert ( 'Defina algun Estado del Standard, para poder continuar..' );
			return false;
			
		}
		
		if ( valorTipo == "" ) {
		
			alert ( 'Defina algun Tipo de Standard, para poder continuar..' );
			return false;
			
		}
		
		if ( form.sj.value != "" ) {
		
			var standard = form.sj.value.toUpperCase();
			
			if ( /([A-Z]|[a-z]|\d)/.test ( standard ) ) {
							
				if ( /([A-Z]|[a-z]|\d){3}G([A-Z]|[a-z]|\d){2}/.test ( standard ) ) {			
					if ( valorTipo == "E" ) {
						alert ( 'El Numero del Standard Digitado no es Especifico..' );
						return false;			
					}	
				}
				
			} else {
				alert ( 'El Numero del Standard Digitado es INVALIDO..' );
				return false;		
			}
			
		}
		
		return true;
		
	}			
			
</script>		

</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Consultar Standard Job"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Consulta&accion=StdJob" onSubmit="return validar(this);">
  <table width="47%"  border="2" align="center">
    <tr>
      <td><table width="100%">
        <tr>
          <td width="373" class="subtitulo1">Busque el Standard</td>
          <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
        </tr>
      </table>
        <table width="100%" align="center" cellpadding="3" class="tablaInferior">
          <tr bgcolor="#99CCFF" class="fila">
            <td nowrap>Activos ? 
              <input type="radio" name="esta" id="esta" value="A" onClick="setEstado(this.value);"></td>
            <td nowrap>Inactivos ? 
              <input type="radio" name="esta" id="esta" value="I" onClick="setEstado(this.value);"></td>
          </tr>
          <tr bgcolor="#99CCFF" class="fila">
            <td width="156" nowrap><strong>N&uacute;mero del Standard</strong></td>
            <td width="188" nowrap><input name="sj" type="text" class="textbox" id="sj" style="text-transform:uppercase" size="6" maxlength="6">
              </td>
          </tr>
          <tr bgcolor="#99CCFF" class="fila">
            <td nowrap>Standard Espec&iacute;fico ? 
              <input type="radio" name="tipo" id="tipo" value="E" onClick="setTipo(this.value);"></td>
            <td nowrap>Standard General ? 
              <input type="radio" name="tipo" id="tipo" value="G" onClick="setTipo(this.value);"></td>
          </tr>
          <tr bgcolor="#99CCFF" class="fila">
            <td nowrap><strong>Nombre del Cliente</strong></td>
            <td nowrap><input name="cliente" type="text" class="textbox" id="cliente" style="text-transform:uppercase"></td>
          </tr>
        </table>      </td>
    </tr>
  </table>
  <div align="center"><br>
    <input src="<%= BASEURL%>/images/botones/buscar.gif"  name="imgaceptar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" type="image">&nbsp; <img src="<%= BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="form1.reset();" onMouseOut="botonOut(this);"  style="cursor:hand ">&nbsp; <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);"  style="cursor:hand "></div>
</form>
<%if(request.getParameter("mensaje")!=null){%>
<table width="512" border="2" align="center">
  <tr>
    <td><table width="100%" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="282" align="center" class="mensajes">No se encontraron resultados en la b&uacute;squeda</td>
          <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="78">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>

	<%}%>

<iframe width=188 height=166 name="gToday:datetime:agenda.js:gfPop:plugins_12.js" id="gToday:datetime:agenda.js:gfPop:plugins_12.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</div>
</body>
</html>