<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Consultar Planilla Carga Varia</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
		<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
			<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Consultar Planilla Carga Varia"/>
		</div>
		<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<% String numpla = (request.getParameter("numpla")!=null)?request.getParameter("numpla"):"";
   List lis = model.ciudadService.getCiudadList();
%>
<form name="form1" id="form1" method="post" action="<%=CONTROLLER%>?estado=Consultar&accion=Planilla&general=ok">  
<table width="650" border="2" align="center">
    <tr>
      <td><table width="100%" align="center"  class="tablaInferior">
        <tr class="fila">
          <td colspan="2" align="left" class="subtitulo1">&nbsp;Consulta De Planillas </td>
          <td colspan="2" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
        <tr class="fila">
          <td align="left" ><strong>Nro. Planilla</strong></td>
          <td colspan="3" align="left" ><input name="numpla" class="textbox" type="text" id="numpla" size="12" maxlength="10" value="<%=numpla%>" style="text-transform:uppercase" onKeyPress="soloAlfa(event)"></td>
          </tr>
<tr class="fila" id="cantidad">
          <td align="left" ><strong>Usuario</strong></td>
          <td align="left" ><input name="despachador" type="text" class="textbox" id="nit2" size="17" maxlength="15" onKeyPress="soloAlfa(event)" style="text-transform:uppercase"></td>
          <td valign="middle">
              <span class="Letras"><strong>Placa </strong></span></td>
          <td valign="middle"><input name="placa" type="text" class="textbox" id="placa" size="12" maxlength="6" style="text-transform:uppercase" onKeyPress="soloAlfa(event)"></td>
</tr>
        <tr class="fila" id="cantidad">
          <td align="left" ><strong>Propietario  </strong></td>
          <td align="left" ><input name="nit" type="text" class="textbox" id="nit2" size="18" maxlength="15" onKeyPress="soloDigitos(event,'decNO')"></td>
          <td valign="middle">
              <span class="Letras"><strong>Conductor</strong></span></td>
          <td valign="middle"><input name="cedcon" type="text" class="textbox" id="cedcon2" size="18" maxlength="15" onKeyPress="soloDigitos(event,'decNO')"></td>
        </tr>
   
<tr class="fila">
          <td width="125">  Origen </td>
          <td width="197"><select name="origen" class="textbox" id="origen" style="width:99%">
            <option value=""></option>
            <%
                                    
                                    for(int i = 0; i<lis.size(); i++){	
                                        Ciudad c1 = (Ciudad) lis.get(i);	%>
            <option value="<%=c1.getCodCiu()%>"><%=c1.getNomCiu()%></option>
            <%}%>
          </select></td>
          <td width="74"> Destino </td>
          <td width="222"><select name="destino" class="textbox" id="destino" style="width:99%">
            <option value=""></option>
            <%
                                    for(int i = 0; i<lis.size(); i++){	
                                        Ciudad c2 = (Ciudad) lis.get(i);	%>
            <option value="<%=c2.getCodCiu()%>"><%=c2.getNomCiu()%></option>
            <%}%>
          </select></td>
        </tr>
<tr class="fila">
          <td width="125" align="left" > Agencia Despacho </td>
          <td colspan="3" valign="middle">
            <select name="agencia" class="textbox" id="c_agencia_despacho">
              <option value=""></option>
              <%//model.agenciaService.cargarAgencias();
                                    Vector vec = model.agenciaService.getAgencias();
                                    for(int i = 0; i<vec.size(); i++){	
                                        Agencia a = (Agencia) vec.elementAt(i);	%>
              <option value="<%=a.getId_agencia()%>"><%=a.getNombre()%></option>
              <%}%>
          </select>          </td>
          </tr>
        <tr class="fila">
          <td colspan="4" align="left" ><div align="center"><strong>Fecha del Despacho</strong></div></td>
          </tr>		    
        <tr class="fila">
		  <td width="125" align="left" ><strong> Desde</strong></td>
          <td width="197" align="left" ><input name="fechaini" type="text" id="fechaini" size="12" class="textbox" readonly>
            <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fechaini);return false;" HIDEFOCUS> <img src="<%=BASEURL%>/js/Calendario/cal.gif" alt="De click aqu&iacute; para escoger la fecha" width="16" height="16"
               border="0" align="absmiddle"></a></td>
          <td width="74" valign="middle"><strong>Hasta            </strong></td>
          <td width="222" valign="middle"><input name="fechafin" type="text" id="fechafin" size="12" class="textbox" readonly>
            <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fechafin);return false;" hidefocus> <img src="<%=BASEURL%>/js/Calendario/cal.gif" alt="De click aqu&iacute; para escoger la fecha" width="16" height="16"
               border="0" align="absmiddle"></a></td>
        </tr>
      </table></td>
    </tr>
  </table>
<p>
<div align="center"><img src="<%=BASEURL%>/images/botones/buscar.gif" style="cursor:hand" title="Buscar Planillas" name="buscar"  onClick="return validarConsulta(form1);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/salir.gif"  name="regresar" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
</p> 
</form>
<%if(request.getParameter("mensaje")!=null){%>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
<%}%>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>