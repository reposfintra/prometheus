<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<link href="../css/estilo.css" rel="stylesheet" type="text/css">

<title>Buscar Planilla</title>
<script language="javascript" src="../js/validar.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<%
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 20;
    int maxIndexPages = 10;
    Vector nits = model.conductorService.getConductores();
	if (nits.size()<1)
		response.sendRedirect(CONTROLLER+"?estado=Menu&accion=Enviar&numero=37&mensaje=");
%>
<table width="80%" border="1" align="center" cellpadding="2" cellspacing="1" bordercolor="#CCCCCC" class="Letras">
  <tr class="titulo">
    <td height="24" colspan="2" nowrap><div align="center" ><strong>LISTA DE CONDUCTORES </strong></div></td>
  </tr>
  <tr class="subtitulos">
    <td width="329" nowrap><span class="style7 Estilo1">CEDULA</span></td>
    <td><div align="center" class="style7 Estilo1">
        <div align="left">NOMBRE</div>
    </div></td>
  </tr>
  <pg:pager
    items="<%=nits.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
  <%-- keep track of preference --%>
  <%
      for (int i = offset.intValue(),
	         l = Math.min(i + maxPageItems, nits.size());
	     i < l; i++)
	{
        Conductor cond = (Conductor) nits.elementAt(i);%>
  <pg:item>
  <tr nowrap class="fila" style="cursor:hand" title="Mostrar Conductor..." onClick="copiarConductor('<%=cond.getCedula()%>');" onMouseOver="bgColor= '#99cc99'" onMouseOut="bgColor=''" height="30">
    <td ><%=cond.getCedula()%></td>
    <td><%=cond.getNombre()%></td>
  </tr>
  </pg:item>
  <%}
  %>
  <tr  class="fila">
    <td height="30" colspan="2" nowrap><pg:index>
      <jsp:include page="/WEB-INF/jsp/google.jsp" flush="true"/>
    </pg:index></td>
  </tr>
  </pg:pager>
</table>
<br>  
  
</body>
</html>



