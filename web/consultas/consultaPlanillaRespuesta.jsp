<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>

<title>Buscar Planilla</title>
<script language="javascript" src="../js/validar.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Planilla"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<%
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
    Vector planillas = model.planillaService.getPlas();
	if (planillas.size()<1)
		response.sendRedirect(CONTROLLER+"?estado=Menu&accion=Enviar&numero=34&mensaje=");
%>
<table width="100%"  border="2" align="center">
  <tr>
    <td><table width="100%">
      <tr>
        <td width="373" class="subtitulo1">Lista  de Planillas </td>
        <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
      </tr>
    </table>      
      <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
      <tr class="tblTitulo"  >
        <td width="81" nowrap>PLANILLA</td>
        <td width="104" ><div align="center" >REMISION</div></td>
        <td width="221" >STANDARD JOB</td>
        <td width="95" >PLACA</td>
        <td width="115"><div align="center" > CONDUCTOR</div></td>
        <td width="188"><div align="center" >NOMBRE CONDUCTOR</div></td>
        <td width="139">FECHA DEL DESPACHO</td>
      </tr>
      <pg:pager
    items="<%=planillas.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
      <%-- keep track of preference --%>
      <%
      for (int i = offset.intValue(),
	         l = Math.min(i + maxPageItems, planillas.size());
	     i < l; i++)
	{
        Planilla planilla = (Planilla) planillas.elementAt(i);%>
      <pg:item>
      <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
        <td height="30" nowrap class="bordereporte" style="cursor:hand" title="Mostrar Planilla..." onMouseOver='cambiarColorMouse(this)'
		onClick="window.open('<%=CONTROLLER%>?estado=ConsultaOC&accion=Buscar&tipo=1&distrito=TSP&numeroOC=<%=planilla.getNumpla()%>&numeroOT=<%=planilla.getNumrem()%>','','status=yes,scrollbars=yes,width=800,height=550,resizable=yes');"><div align="center" ><%=planilla.getNumpla()%> </div></td>
        <td  height="30" nowrap class="bordereporte" style="cursor:hand" title="Mostrar Remesa..." onClick="window.location='<%=CONTROLLER%>?estado=Consultar&accion=Remesa&remesa=<%=planilla.getNumrem()%>'" onMouseOver='cambiarColorMouse(this)'><div align="center" ><%=planilla.getRemision()%> </div></td>
        <td class="bordereporte"><span ><%=planilla.getSj_desc()%></span></td>
        <td class="bordereporte"><span ><%=planilla.getPlaveh()%></span></td>
        <td class="bordereporte"><div align="center" ><span><%=planilla.getCedcon()%></span></div></td>
        <td class="bordereporte"><span ><%=planilla.getNomCond()%></span></td>
        <td class="bordereporte"><span ><%=planilla.getFecdsp().substring(0,10)%></span></td>
      </tr>
      </pg:item>
      <%}
  %>
      <tr >
        <td height="30" colspan="7" nowrap><pg:index>
          <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>     
    </pg:index></td>
      </tr>
      </pg:pager>
    </table></td>
  </tr>
</table>
<br>  
  
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr align="left">
    <td><img src="<%=BASEURL%>/images/botones/regresar.gif" alt="Volver a Realizar la consulta..." name="c_regresar" id="c_regresar" style="cursor:hand " onClick="window.location = '<%=CONTROLLER%>?estado=Menu&accion=Enviar&numero=34';" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
  </tr>
</table>
</div>
</body>
</html>



