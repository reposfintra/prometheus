<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<link href="css/estilo.css" rel="stylesheet" type="text/css">
<link href="../css/estilo.css" rel="stylesheet" type="text/css">

<title>Buscar Planilla</title>
<script language="javascript" src="js/validar.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Consultar&accion=Planilla&normal=ok" onSubmit="return validarConsulta(this);">
  <table width="573" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="ECE0D8" class="letra">
    <tr bgcolor="#FFA928">
      <td colspan="2" nowrap><div align="center" class="titulo"><strong><strong>BUSQUE LA PLANILLAS
              </strong></strong></div></td>
    </tr>
    <tr class="fila">
      <td width="162" nowrap><span class="letra_resaltada">Numero de la Planilla </span></td>
      <td width="395" nowrap><input name="numpla" type="text" id="numpla"></td>
    </tr>
    <tr class="fila">
      <td nowrap><strong>Placa:</strong></td>
      <td width="395" nowrap><input name="placa" type="text" id="placa" maxlength="10"></td>
    </tr>
    <tr class="fila">
      <td nowrap><strong>Propietario</strong></td>
      <td nowrap><input name="nit" type="text" id="nit"></td>
    </tr>
    <tr class="fila">
      <td nowrap><strong>Conductor</strong></td>
      <td nowrap><input name="cedcon" type="text" id="cedcon"></td>
    </tr>
    <tr class="fila">
      <td nowrap><strong>Fecha del despacho: </strong></td>
      <td nowrap>Desde
        <input name="fechaini" type="text" id="fechaini" size="18" readonly>        
        <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fechaini);return false;" HIDEFOCUS> <img src="js/Calendario/cal.gif" width="16" height="16"
               border="0" alt="De click aqu&iacute; para escoger la fecha"></a> Hasta
        <input name="fechafin" type="text" id="fechafin" size="18" readonly>
      <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fechafin);return false;" HIDEFOCUS> <img src="js/Calendario/cal.gif" width="16" height="16"
               border="0" alt="De click aqu&iacute; para escoger la fecha"></a></td>
    </tr>
    <tr bgcolor="#99CCFF" class="fila">
      <td colspan="2" nowrap><div align="center">
        <input type="submit" name="Submit" value="Buscar...">
      </div></td>
    </tr>
  </table>
</form>
<%if(request.getParameter("mensaje")!=null){%>
	<span class="style1">No se encontraron resultados en la busqueda</span>.
<%}%>

<iframe width=188 height=166 name="gToday:datetime:agenda.js:gfPop:plugins.js" id="gToday:datetime:agenda.js:gfPop:plugins.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</body>
</html>

