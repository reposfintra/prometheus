<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>Buscar Planilla</title>
<script language="javascript" src="js/validar.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Planilla"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Consultar&accion=Planilla" onSubmit="return validarConsulta(this);">
  <table width="573"  border="2" align="center">
    <tr>
      <td><table width="100%">
        <tr>
          <td width="373" class="subtitulo1">Datos de la Planilla </td>
          <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
        </tr>
      </table>
        <table width="100%" align="center" cellpadding="3" class="tablaInferior">
          <tr class="fila">
            <td width="162" nowrap>Numero de planilla </td>
            <td width="395" nowrap><input name="numpla" type="text" id="numpla"></td>
          </tr>
          <tr class="fila">
            <td width="162" nowrap><strong>Placa:</strong></td>
            <td nowrap><input name="placa" type="text" class="textbox" id="placa" maxlength="10">
              <input name="numpla" type="hidden" id="numpla"></td>
          </tr>
          <tr class="fila">
            <td nowrap><strong>Propietario</strong></td>
            <td nowrap><input name="nit" type="text" class="textbox" id="nit"></td>
          </tr>
          <tr class="fila">
            <td nowrap><strong>Conductor</strong></td>
            <td nowrap><input name="cedcon" type="text" class="textbox" id="cedcon"></td>
          </tr>
          <tr class="fila">
            <td nowrap><strong>Fecha del despacho: </strong></td>
            <td nowrap>Desde
                <input name="fechaini" type="text" class="textbox" id="fechaini" size="18" readonly>
                <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fechaini);return false;" HIDEFOCUS> <img src="js/Calendario/cal.gif" width="16" height="16"
               border="0" alt="De click aqu&iacute; para escoger la fecha"></a> Hasta
                <input name="fechafin" type="text" class="textbox" id="fechafin" size="18" readonly>
                <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fechafin);return false;" HIDEFOCUS> <img src="js/Calendario/cal.gif" width="16" height="16"
               border="0" alt="De click aqu&iacute; para escoger la fecha"></a></td>
          </tr>
        </table>      </td>
    </tr>
  </table>
  <div align="center"><br>
      <img src="<%= BASEURL%>/images/botones/buscar.gif"  name="imgaceptar" onClick="if ( validarConsulta(form1) ) form1.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">&nbsp; <img src="<%= BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="form1.reset();" onMouseOut="botonOut(this);"  style="cursor:hand ">&nbsp; <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);"  style="cursor:hand ">
  </div>
</form>
<%if(request.getParameter("mensaje")!=null){%>
	<table width="512" border="2" align="center">
      <tr>
        <td><table width="100%" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
              <td width="282" align="center" class="mensajes">No se encontraron resultados en la b&uacute;squeda</td>
              <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
              <td width="78">&nbsp;</td>
            </tr>
        </table></td>
      </tr>
    </table>
	<%}%>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>

