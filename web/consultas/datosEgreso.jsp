<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<link href="css/letras.css" rel="stylesheet" type="text/css">

<title>Datos egreso</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.Estilo1 {font-weight: bold}
.style7 {font-family: Verdana, Arial, Helvetica, sans-serif;  font-size: 12px; }
-->
</style>
</head>
<body>
<% Egreso egreso = model.egresoService.getEgreso();
   List egresosdet = model.egresoService.getEgresosDet();
%>
<table width="70%" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="#EFE3DE" class="Letras">
  <tr bgcolor="#FFAA29">
    <td height="25" colspan="5"><div align="center"><span class="Estilo2"><strong>EGRESO   NUMERO <%=request.getParameter("doc_no")%>
    </strong></span></div></td>
  </tr>
  
  <tr>
    <td width="156" colspan="2" bgcolor="#9CCFFF"><strong>Distrito</strong></td>
    <td width="424" colspan="3"><span class="style7"><%=egreso.getDstrct()%></span></td>
  </tr>
 
  <tr>
    <td colspan="2" bgcolor="#9CCFFF"><strong>Banco</strong></td>
    <td colspan="3"><span class="style7"><%=egreso.getBranch_code()%></span></td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#9CCFFF"><strong>Cuenta</strong></td>
    <td colspan="3"><span class="style7"><%=egreso.getBank_account_no()%></span></td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#9CCFFF"><strong>Nit</strong></td>
    <td colspan="3"><span class="style7"><%=egreso.getNit()%></span></td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#9CCFFF"><strong>Nombre del Beneficiario</strong></td>
    <td colspan="3"><span class="style7"><%=egreso.getPayment_name()%></span></td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#9CCFFF"><strong> Agencia</strong></td>
    <td colspan="3"><span class="style7"><%=egreso.getAgency_id()%></span></td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#9CCFFF"><strong>Copcepto</strong></td>
    <td colspan="3"><span class="style7"><%=egreso.getConcept_code()%></span></td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#9CCFFF"><strong>Valor</strong></td>
    <td colspan="3"><span class="style7"><%=egreso.getVlr()%></span></td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#9CCFFF"><strong>Moneda</strong></td>
    <td colspan="3"><span class="style7"><%=egreso.getCurrency()%></span></td>
  </tr>
  <tr>
    <td colspan="2" bgcolor="#9CCFFF"><strong>Fecha del egreso </strong></td>
    <td colspan="3"><span class="style7"><%=egreso.getCreation_date()%></span></td>
  </tr>
  
  <tr bgcolor="#FFAA29">
    <td colspan="5"><div align="center"><strong>DETALLES</strong></div></td>
  </tr>
  
  <tr bgcolor="#9CCFFF">
    <td><strong>Planilla</strong></td>
    <td><strong>Item</strong></td>
    <td><strong>Valor</strong></td>
    <td><strong>Moneda</strong></td>
    <td><strong>Concepto</strong></td>
  </tr>
 <%Iterator it=egresosdet.iterator();
	while (it.hasNext()){
		Egreso ed= (Egreso) it.next();%> 
  <tr bgcolor="#EFE3DE">
    <td><%=ed.getOc()%></td>
    <td><%=ed.getItem_no()%></td>
    <td><%=ed.getVlr()%></td>
    <td><%=ed.getCurrency()%></td>
    <td><%=ed.getConcept_code()%></td>
  </tr>
  <%}%>
</table>
<br>
<br>
<a href="<%=CONTROLLER%>?estado=Menu&accion=Enviar&numero=36" class="Letras">Volver a Realizar la consulta...</a></body>
</html>



