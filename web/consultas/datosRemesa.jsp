<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<link href="css/letras.css" rel="stylesheet" type="text/css">

<title>Buscar Standard</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.Estilo1 {font-weight: bold}
.style7 {font-family: Verdana, Arial, Helvetica, sans-serif;  font-size: 12px; }
-->
</style>
</head>
<body>
<% Remesa remesa = (Remesa) request.getAttribute("remesa");
   List remitentes = (List) request.getAttribute("Remitentes");
   List destinatarios = (List) request.getAttribute("Destinatarios");
   
%>
<table width="70%" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="#EFE3DE" class="Letras">
  <tr bgcolor="#FFAA29">
    <td height="25" colspan="4"><div align="center"><span class="Estilo2"><strong>REMESA  NUMERO <%=request.getParameter("remesa")%>
    </strong></span></div></td>
  </tr>
  
  <tr>
    <td width="156" bgcolor="#9CCFFF"><strong>Planilla</strong></td>
    <td width="424" colspan="3"><span class="style7"><%=remesa.getOc()%></span></td>
  </tr>
 
  <tr>
    <td bgcolor="#9CCFFF"><strong>Compa&ntilde;ia</strong></td>
    <td colspan="3"><span class="style7"><%=remesa.getCia()%></span></td>
  </tr>
  <tr>
    <td bgcolor="#9CCFFF"><strong>Observacion</strong></td>
    <td colspan="3"><span class="style7"><%=remesa.getObservacion()%></span></td>
  </tr>
  <tr>
    <td bgcolor="#9CCFFF"><strong>Documento Interno</strong></td>
    <td colspan="3"><span class="style7"><%=remesa.getDocInterno()%></span></td>
  </tr>
  <tr>
    <td bgcolor="#9CCFFF"><strong>Fecha de la Remesa</strong></td>
    <td colspan="3"><span class="style7"><%=remesa.getFecRem()%></span></td>
  </tr>
  <tr>
    <td bgcolor="#9CCFFF"><strong> Fecha de Cumplido </strong></td>
    <td colspan="3"><span class="style7"><%=remesa.getFeccum()%></span></td>
  </tr>
  <tr>
    <td bgcolor="#9CCFFF"><strong>Valor de la Remesa</strong></td>
    <td colspan="3"><span class="style7"><%=remesa.getVlrRem()%></span></td>
  </tr>
  <tr>
    <td bgcolor="#9CCFFF"><strong>Peso Real </strong></td>
    <td colspan="3"><span class="style7"><%=remesa.getPesoReal()%></span></td>
  </tr>
  <tr>
    <td bgcolor="#9CCFFF"><strong>Unit Of Work </strong></td>
    <td colspan="3"><span class="style7"><%=remesa.getUnitOfWork()%></span></td>
  </tr>
  <tr>
    <td bgcolor="#9CCFFF"><strong>Numero de Estandard </strong></td>
    <td colspan="3"><span class="style7"><%=remesa.getStdJobNo()%></span></td>
  </tr>
  <tr>
    <td bgcolor="#9CCFFF"><strong>Nombre del Estandard </strong></td>
    <td colspan="3"><span class="style7"><%=remesa.getDescripcion()%></span></td>
  </tr>
  <tr>
    <td bgcolor="#9CCFFF"><strong>Origen</strong></td>
    <td colspan="3"><span class="style7"><%=remesa.getOriRem()%></span></td>
  </tr>
  <tr>
    <td bgcolor="#9CCFFF"><strong>Destino</strong></td>
    <td colspan="3"><span class="style7"><%=remesa.getDesRem()%></span></td>
  </tr>
  <tr>
    <td bgcolor="#9CCFFF"><strong>Cliente</strong></td>
    <td colspan="3"><span class="style7"><%=remesa.getCliente()%></span></td>
  </tr>
  <tr bgcolor="#FFAA29">
    <td colspan="4"><div align="center"><strong>DESTINATARIOS</strong></div></td>
  </tr>
  <%Iterator it=destinatarios.iterator();
	while (it.hasNext()){
		RemesaDest rd= (RemesaDest) it.next();%>
  <tr bgcolor="#EFE3DE">
    <td colspan="4"><%=rd.getNombre()%></td>
  </tr>
  <%}%>
  <tr bgcolor="#FFAA29">
    <td colspan="4"><div align="center"><strong>REMITENTES</strong></div></td>
  </tr>
  <% it=remitentes.iterator();
	while (it.hasNext()){
		RemesaDest rd= (RemesaDest) it.next();%>
  <tr bgcolor="#EFE3DE">
    <td colspan="4"><%=rd.getNombre()%></td>
  </tr>
   <%}%>
</table>
<br>
<br>
<a href="<%=CONTROLLER%>?estado=Menu&accion=Enviar&numero=36" class="Letras">Volver a Realizar la consulta...</a></body>
</html>



