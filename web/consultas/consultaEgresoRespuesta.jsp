<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<link href="../css/letras.css" rel="stylesheet" type="text/css">

<title>Buscar Planilla</title>
<script language="javascript" src="../js/validar.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.Estilo1 {font-weight: bold}
.style3 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
}
.style7 {font-family: Verdana, Arial, Helvetica, sans-serif;  font-size: 12px; }
.style10 {font-size: 11px}
-->
</style>
</head>
<body>
<%
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
    Vector egresos = model.egresoService.getEgresos();
	if (egresos.size()<1)
		response.sendRedirect(CONTROLLER+"?estado=Menu&accion=Enviar&numero=38&mensaje=");
%>
<table width="100%" border="1" align="center" cellpadding="2" cellspacing="3" bordercolor="#CCCCCC" bgcolor="ECE0D8" class="Estilo1">
  <tr bgcolor="#FFA928">
    <td height="25" colspan="5" nowrap><div align="center" class="Estilo1 style3"><strong>LISTA EGRESOS </strong></div></td>
  </tr>
  <tr bgcolor="#99CCFF" class="style7">
    <td width="97" nowrap><span class="style7">DOCUMENTO</span></td>
    <td width="161" bgcolor="#99CCFF"><div align="center" class="style7">BANCO</div></td>
    <td width="130" bgcolor="#99CCFF"><span class="style7">CUENTA</span></td>
    <td width="103" bgcolor="#99CCFF"><span class="style7">VALOR</span></td>
    <td width="195" class="Estilo4"><div align="center" class="style7"> <strong>BENEFICIARIO</strong></div></td>
  </tr>
  <pg:pager
    items="<%=egresos.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
<%-- keep track of preference --%>

  <%
      for (int i = offset.intValue(),
	         l = Math.min(i + maxPageItems, egresos.size());
	     i < l; i++)
	{
        Egreso egreso = (Egreso) egresos.elementAt(i);%>
		<pg:item>
 	 <tr height="30" nowrap style="cursor:hand" title="Mostrar Egreso..." onClick="window.location='<%=CONTROLLER%>?estado=Consultar&accion=Egreso&doc_no=<%=egreso.getDocument_no()%>&solo=0k'" onMouseOver="bgColor= '#99cc99'" onMouseOut="bgColor=''">
    <td><div align="center" class="style7"><%=egreso.getDocument_no()%> </div></td>
    <td  ><div align="center" class="style7"><%=egreso.getBranch_code()%> </div></td>
    <td><span class="style7"><%=egreso.getBank_account_no()%></span></td>
    <td><span class="style7"><%=egreso.getVlr()%></span></td>
    <td><div align="center" class="style7"><span class="Estilo6"><%=egreso.getPayment_name()%></span></div></td>
    </tr>
 	 
  </pg:item>
  <%}
  %>
  <tr bgcolor="#FFFFFF" class="style7">
 	   <td height="30" colspan="5" nowrap><pg:index>
	<jsp:include page="/WEB-INF/jsp/google.jsp" flush="true"/>
	</pg:index></td>
  </tr>
  </pg:pager>
</table>
<br>  
  
<a href="<%=CONTROLLER%>?estado=Menu&accion=Enviar&numero=38" class="Letras">Volver a Realizar la consulta...</a>
</body>
</html>



