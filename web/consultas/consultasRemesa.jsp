<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<script>
function validarConsultaRemesa(){
	var sw = 0;
	if(document.form1.remesa.value=="") {		
       if(document.form1.tipo_doc.value=="") {
           if(document.form1.fechaini.value == ""){
				alert ('El campo fecha de inicio no debe ser vacio!.');
			return false;
			} else if(document.form1.fechafin.value == ""){
				alert ('El campo fecha fin no debe ser vacio!.');
				return false;
			} else if(document.form1.fechafin.value<document.form1.fechaini.value){
				alert ('La fecha fin no puede ser menor que la fecha inicio.');
				return false;
			} 
       } else if(document.form1.tipo_doc.value!="" && document.form1.documento.value=="") {		
            alert ('Debe escribir el documento');    
			return false;            
	   } else {            
            return true;
       }  
	}
	return true;	
}		
</script>
<% 
model.RemDocSvc.LISTTLBDOC();
List LTipoDoc = model.RemDocSvc.getList();
String lista = model.RemDocSvc.LISTCOMBO();
%>
<title>Buscar Remesas</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Consultar Remesas"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="form1" method="post"  onSubmit="return validarConsultaRemesa()" action="<%=CONTROLLER%>?estado=Consultar&accion=Remesa">
<table width="460" border="2" align="center">
    <tr>
        <td width="3350">
			<table width="100%" border="0"align="center" >
            	<tr>
            		<td colspan='3'>
              			<table cellpadding='0' cellspacing='0' width='100%'>
                			<tr class="fila">
                  				<td width="186" align="left"  class="subtitulo1">Buscar Remesa</td>
                  				<td align="left" width="202" class="barratitulo"><img src="<%=BASEURL%>//images/titulo.gif" width="32" height="20">
              				</tr> 
			  			</table>
			  		</td>
				</tr>
  				<tr class="fila">
      				<td colspan="2" nowrap>
              			<input name="cumplida" type="checkbox" id="cumplida" value="20">
							Cumplidas
						<input name="Anulada" type="checkbox" id="Anulada" value="A">
							Anuladas
					</td>
    			</tr>
    			<tr class="fila">
      				<td width="132" nowrap>Numero de la remesa</td>
			      	<td nowrap ><input name="remesa" type="text" id="remesa">
      				</td>
    			</tr>
    			<tr class="fila">
      				<td nowrap>Estandar Job </td>
      				<td nowrap ><input name="sj" type="text" id="sj2"></td>
    			</tr>
    			<tr class="fila">
      				<td nowrap>Nombre del Cliente </td>
      				<td nowrap ><input name="cliente" type="text" id="cliente2"></td>
    			</tr>
<tr class="fila">
      				<td nowrap>Tipo de Documento </td>
      				<td nowrap ><select name="tipo_doc" id="tipodoc1" class="textbox">
      				  <option value="" selected>Seleccione un tipo</option>      				  
            <% Iterator it = LTipoDoc.iterator();
   while (it.hasNext()){ 
   remesa_docto rd = (remesa_docto) it.next();
   %><option value="<%=rd.getDocument_type()%>"><%=rd.getDocument_name()%></option>
            <% } %></select></td>
              </tr>
    			<tr class="fila">
      				<td nowrap>Documento </td>
      				<td nowrap ><input name="documento" type="text" id="nomdest3"></td>
    			</tr>
    			<tr class="fila">
      				<td nowrap>Fecha de la remesa</td>
      				<td nowrap>Desde
        				<input name="fechaini" type="text" id="fechaini" size="18" readonly>        
        					<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fechaini);return false;" HIDEFOCUS> <img src="js/Calendario/cal.gif" width="16" height="16"
               				border="0" alt="De click aqu&iacute; para escoger la fecha"></a> 
					    </input>
						Hasta
        				<input name="fechafin" type="text" id="fechafin" size="18" readonly>
        					<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fechafin);return false;" HIDEFOCUS> <img src="js/Calendario/cal.gif" width="16" height="16"
               				border="0" alt="De click aqu&iacute; para escoger la fecha"></a>
						</input>
					</td>
    			</tr>
    		</table>
		</td>
	</tr>
</table>
<br>
<center>
	<input type="image" src="<%=BASEURL%>/images/botones/buscar.gif" name="buscar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">  
        <img   src="<%=BASEURL%>/images/botones/cancelar.gif" name="c_buscar" onClick="form1.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style='cursor:hand'>    
	<img   src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"    style='cursor:hand'>
</center>

</form>
<p>
<%
String msg = request.getParameter("mensaje");
if(request.getParameter("mensaje")!=null){%>
<table border="2" align="center">
  <tr>
    <td>
	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%= msg %></td>
        <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>

