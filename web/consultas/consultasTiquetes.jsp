<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<script>
function validarConsultaTick(){
	if (document.form1.ciudad.value=="") {
		alert('Digite la ciudad');
		return(false);
	}
	if (document.form1.nombre.value=="") {
		alert('Digite el nombre');
		return(false);
	}
	if (document.form1.nit.value=="") {
		alert('Digite el NIT');
		return(false);
	}
	form1.submit();
}
</script>
<title>Proveedores de Tiquetes</title>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
     <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Proveedores de Tiquetes"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Consultar&accion=ProveedorTiquetes" onSubmit="">
  <table width="350" border="2" align="center">
    <tr>
      <td>
		<table width="100%" border="0"align="center" >
          <tr>
          	<td colspan='3'>
              <table cellpadding='0' cellspacing='0' width='100%'>
                <tr class="fila">
                  <td width="186" align="left"  class="subtitulo1">Consultar Provedor de tiquetes</td>
                  <td align="left" width="202" class="barratitulo"><img src="<%=BASEURL%>//images/titulo.gif" width="32" height="20">
              	</tr> 
			  </table>
			</td>
		  </tr>
     	  <tr class="fila">
      	    <td width="140" nowrap>Ciudad:</td>
      	    <td nowrap><input name="ciudad" type="text" id="ciudad" maxlength="40">
      	    </td>
    	  </tr>
    	  <tr class="fila">
      	    <td nowrap>Nombre:</td>
      	    <td nowrap><input name="nombre" type="text" id="nombre"></td>
    	  </tr>
    	  <tr class="fila">
      	    <td nowrap><strong>Nit.</td>
      	    <td nowrap><input name="nit" type="text" id="nit"></td>
    	  </tr>
    	</table>
	  </td>
    </tr>
  </table>
</form>
<p>
<center>
	<img src="<%=BASEURL%>/images/botones/buscar.gif" name="buscar" onClick="validarConsultaTick();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">  <img src="<%=BASEURL%>/images/botones/salir.gif" name="salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> <img src="<%=BASEURL%>/images/botones/cancelar.gif" name="cancelar" onClick="form1.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">    	 
</center>
<p>
<% String msg = request.getParameter("mensaje");
if(request.getParameter("mensaje")!=null){%>
<table border="2" align="center">
  <tr>
    <td>
	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%= msg %></td>
        <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>

