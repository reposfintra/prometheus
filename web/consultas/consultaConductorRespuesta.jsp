<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">

<title>Lista de Conductores</title>
<script language="javascript" src="../js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

</head>
<body>
<%
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 20;
    int maxIndexPages = 10;
    Vector nits = model.conductorService.getConductores();
	if (nits.size()<1)
		response.sendRedirect(CONTROLLER+"?estado=Menu&accion=Enviar&numero=37&mensaje=");
%>

<table border="2" align="center" width="546">
  <tr>
    <td>
	<table width="99%"  align="center"   >
  <tr>
    <td width="227" height="24"  class="subtitulo1"><p align="left">Lista de Conductores </p></td>
    <td width="229"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4" class="tblTitulo">
  <tr>
    <td width="158" nowrap><span class="style7 Estilo1">CEDULA</span></td>
    <td width="323" ><div align="center" class="style7 Estilo1">
        <div align="left">NOMBRE</div>
    </div></td>
  </tr>
  <pg:pager
    items="<%=nits.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
  <%-- keep track of preference --%>
  <%
      for (int i = offset.intValue(),
	         l = Math.min(i + maxPageItems, nits.size());
	     i < l; i++)
	{
        Conductor cond = (Conductor) nits.elementAt(i);%>
  <pg:item>
  <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" title="Mostrar Conductor..." onClick="window.open('<%=CONTROLLER%>?estado=Conductor&accion=Buscar&identificacion=<%=cond.getCedula()%>','Conductor','status=yes,scrollbars=no,width=700,height=700,resizable=yes');" >
    <td class="bordereporte"><%=cond.getCedula()%></td>
    <td class="bordereporte"><%=cond.getNombre()%></td>
  </tr>
  </pg:item>
  <%}
  %>
  <tr class="filagris">
    <td height="92" colspan="2" nowrap align="center"><pg:index>
      <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
    </pg:index></td>
  </tr>
  </pg:pager>
</table>
</td>
</tr>
</table>
<br>  
  
<a href="<%=CONTROLLER%>?estado=Menu&accion=Enviar&numero=37" class="Letras">Volver a Realizar la consulta...</a>
</body>
</html>



