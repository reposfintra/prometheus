<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Datos del Standard</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Consultar Standard Job"/>
</div> 
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<% Stdjobdetsel std = (Stdjobdetsel) request.getAttribute("standar");
   List lista = (List) request.getAttribute("costos");%>
<table width="70%"  border="2" align="center">
  <tr>
    <td><table width="100%">
      <tr>
        <td width="373" class="subtitulo1">Datos del Standard</td>
        <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
      </tr>
    </table>
      <table width="100%" align="center" class="tablaInferior">
        <tr bgcolor="#FFAA29">
          <td height="25" colspan="4" class="filaresaltada"><div align="center"><span class="Estilo2"><strong>ESTANDARD JOB NUMERO <%=request.getParameter("sj").toUpperCase()%> </strong></span></div></td>
        </tr>
        <tr class="fila">
          <td width="156"><strong>Descripcion</strong></td>
          <td width="424" colspan="3"><%=std.getSj_desc()%></td>
        </tr>
        <tr class="fila">
          <td><strong>Work Group </strong></td>
          <td colspan="3"><%=std.getWork_group()%></td>
        </tr>
        <tr class="fila">
          <td><strong>Work Order Type </strong></td>
          <td colspan="3"><%=std.getWoType()%></td>
        </tr>
        <tr class="fila">
          <td><strong>Originator ID </strong></td>
          <td colspan="3"><%=std.getOriginator_id()%></td>
        </tr>
        <tr class="fila">
          <td><strong>Maint Type </strong></td>
          <td colspan="3"><%=std.getMaint_type()%></td>
        </tr>
        <tr class="fila">
          <td><span class="Estilo1">Tarifa</span></td>
          <td colspan="3"><%=std.getVlr_freight()%><%=std.getCurrency()%></td>
        </tr>
        <tr class="fila">
          <td><strong>Proyecto</strong></td>
          <td colspan="3"><%=std.getProject_no()%></td>
        </tr>
        <tr class="fila">
          <td><strong>Unit of Work </strong></td>
          <td colspan="3"><%=std.getUnit_of_work()%></td>
        </tr>
        <tr class="fila">
          <td><strong>Nombre del Origen </strong></td>
          <td colspan="3"><%=std.getOrigin_name()%></td>
        </tr>
        <tr class="fila">
          <td><strong>Codigo del Origen </strong></td>
          <td colspan="3"><%=std.getOrigin_code()%></td>
        </tr>
        <tr class="fila">
          <td><strong>Nombre del Destino </strong></td>
          <td colspan="3"><%=std.getDestination_name()%></td>
        </tr>
        <tr class="fila">
          <td><strong>Codigo del Destino </strong></td>
          <td colspan="3"><%=std.getDestination_code()%></td>
        </tr>
        <tr class="fila">
          <td><strong>Nombre del Cliente</strong></td>
          <td colspan="3"><%=std.getCliente()%></td>
        </tr>
        <tr class="fila">
          <td><strong>Codigo del Cliente </strong></td>
          <td colspan="3"><%=std.getCodCliente()%></td>
        </tr>
      </table>      </td>
  </tr>
</table>
<br>
<table width="100%"  border="2">
  <tr>
    <td><table width="100%">
      <tr>
        <td width="373" class="subtitulo1">COSTOS</td>
        <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
      </tr>
    </table>
    <table width="100%" border="1" cellpadding="3" cellspacing="2" bordercolor="#999999" bgcolor="#F7F5F4">
      <tr class="tblTitulo">
        <td width="11%"><strong>Cf_code</strong></td>
        <td width="9%"><strong>Ft_code</strong></td>
        <td width="22%"><strong>Ruta</strong></td>
        <td width="8%"><strong>Costo</strong></td>
        <td width="16%"><strong>Costo por Unidad</strong></td>
        <td width="22%"><strong>Unidad de Transporte </strong></td>
        <td width="12%"><strong>Moneda</strong></td>
      </tr>      
      <%int i = 0;
        Iterator it=lista.iterator();
	while (it.hasNext()){
		Stdjobcosto sc= (Stdjobcosto) it.next();%>
      <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
        <td class="bordereporte"><%=sc.getCf_code()%></td>
        <td class="bordereporte"><%=sc.getFt_code()%></td>
        <td class="bordereporte"><%=sc.getRuta()%></td>
        <td class="bordereporte"><%=sc.getCost()%></td>
        <td class="bordereporte"><%=sc.getUnit_cost()%></td>
        <td class="bordereporte"><%=sc.getUnit_transp()%></td>
        <td class="bordereporte"><%=sc.getCurrency()%></td>
      </tr>
      <%i++;
        }%>
    </table></td>
  </tr>
</table>
<br>
<br>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr align="left">
    <td>
	  <div align="center"><img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);"  style="cursor:hand ">
      </div></td>
  </tr>
</table>
</div>
</body>
</html>



