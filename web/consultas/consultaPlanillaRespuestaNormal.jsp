<!--
- Autor : Ing. Jose de la rosa
- Date  : 11 de Nobiembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que mustra la informaci�n sobre las consultas de viajes
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
 
<html>
<head>
<title>Listado Planilla</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
</head>
<%-- Inicio Body --%>
<body onLoad="redimensionar();" onResize="redimensionar();">
		<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
			<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Planilla Carga Varia"/>
		</div>
		<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
		<%
	String style = "simple";
	String position =  "bottom";
	String index =  "center";
	int maxPageItems = 10;
	int maxIndexPages = 10;
	Vector planillas = model.planillaService.getPlas();
	if (planillas.size()<1)
	    response.sendRedirect(BASEURL+"/consultas/consultaPlanillasGeneral.jsp?mensaje=no");
%>
<%-- Datos Consulta Viajes --%>
<table width="1100" border="2" align="center">
	<tr>
		<td>
			<table width="100%" align="center">
				<tr>
					<td width="373" class="subtitulo1">&nbsp;Planillas</td>
					<td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
				</tr>
			</table>
			<table  width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
				<tr  class="tblTitulo" align="center">
                    <td width="7%" align="center">Estado</td>
					<td width="7%" align="center">Planilla</td>
					<td width="7%"  align="center">Remesa</td>
					<td width="30%" align="center">Standar JOB</td>
					<td width="7%" align="center">Placa</td>
					<td width="8%" align="center">Conductor</td>
					<td width="17%" align="center">Nombre Conductor </td>
					<td width="14%" align="center">Fecha del Despacho </td>
					<td width="10%" align="center">Despachador</td>
				    <td width="10%" align="center">Informaci&oacute;n Factura </td>
				</tr>
				<pg:pager
				items="<%=planillas.size()%>"
				index="<%= index %>"
				maxPageItems="<%= maxPageItems %>"
				maxIndexPages="<%= maxIndexPages %>"
				isOffset="<%= true %>"
				export="offset,currentPageNumber=pageNumber"
				scope="request">
				<%-- keep track of preference --%>
				<%
				for (int i = offset.intValue(), l = Math.min(i + maxPageItems, planillas.size()); i < l; i++)
				{
					Planilla planilla = (Planilla) planillas.elementAt(i);
					String accion = "onClick=\"window.open('" + CONTROLLER + "?estado=Consultar&accion=PlanillaRemesa&distrito=" + planilla.getDistrito() + "&numeroOC=" + planilla.getNumpla() + "&numeroOT=" + planilla.getNumrem() + "','','scroll=no, resizable=yes')\"";
					%>
					<pg:item>
					<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand">
						<td width="7%" class="bordereporte" align="center" <%= accion%>><%=planilla.getReg_status()%></td>
                        <td width="7%" class="bordereporte" align="center" <%= accion%>><%=planilla.getNumpla()%></td>
						<td width="7%" class="bordereporte" align="center" <%= accion%>><%=planilla.getNumrem()%></td>  
						<td width="30%" class="bordereporte" align="center" <%= accion%>><%=planilla.getSj_desc()%></td>
						<td width="7%" class="bordereporte" align="center" <%= accion%>><%=planilla.getPlaveh()%></td>
						<td width="8%" class="bordereporte" align="center" <%= accion%>><%=planilla.getCedcon()%></td>
						<td width="17%" class="bordereporte" align="center" <%= accion%>><%=planilla.getNomCond()%></td>
						<td width="14%" class="bordereporte" align="center" <%= accion%>><%=planilla.getFecdsp().substring(0,10)%></td>
						<td width="10%" class="bordereporte" align="center" <%= accion%>><%=planilla.getDespachador()%></td>
				      <td width="10%" class="bordereporte" align="center">
						<% if( !planilla.getFactura().equals("") ){ %>
						<table  width="100%"  border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                          <tr>
                            <td class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>"><div align="center">Factura</div></td>
                            <td class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>"><div align="center">Estado</div></td>
                            <td class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>"><div align="center">Corrida</div></td>
                          </tr>
                          <tr>
                            <td class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>"><div align="center"><a href="JavaScript:void(0);" onClick="window.open('<%= CONTROLLER %>?estado=Factura&accion=Detalle&documento=<%= planilla.getFactura().replaceAll("#","-_-") %>&prov=<%= planilla.getNitpro()%>&tipo_doc=010','DETALLFRA','status=yes,scrollbars=no,width=780,height=650,resizable=yes');" class="Simulacion_Hiper"><%= planilla.getFactura()%></a></div></td>
                            <td class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>"><div align="center"><%= planilla.getStatus_fra() %></div></td>
                            <td class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>"><div align="center"><%= planilla.getCorrida_fra() %></div></td>
                          </tr>
                        </table>
						<div align="center">
						  <% } else {%> 
						  No Presenta
						  <% } %>
						</div></td>
					</tr>
					</pg:item>
				<%}%>
				<tr  class="pie" align="center">
					<td td height="20" colspan="8" nowrap align="center">
						<pg:index>
						<jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
						</pg:index> 
					</td>
				</tr>
				</pg:pager>
			</table>
		</td>
	</tr>
</table>
<br>
<%if(request.getParameter("anular")!=null){%>
	<table border="2" align="center">
		<tr>
			<td>
				<table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
					<tr>
						<td width="282" align="center" class="mensajes">
							<FIELDSET>
							<legend><b>NOTA : </b></legend>
							Se asigno con exito la nueva remesa, a continuaci&oacute;n se muestra la lista de remesas relacionadas a la planilla.
							</FIELDSET>
						</td>
						<td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
						<td width="78">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<br>
<%}%>
<%-- Tabla boton regresar --%>
<table width="1100" border="0" align="center">
	<tr>
		<td>
			<img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Volver" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/consultas&pagina=consultaPlanillasGeneral.jsp&general=ok&marco=no&opcion=24'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
            <img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" title="Volver" name="buscar"  onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> </td>
	</tr>
</table>
</div>
</body>
</html>
