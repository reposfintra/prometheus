<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<link href="css/letras.css" rel="stylesheet" type="text/css">

<title>Datos pacpm</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body ONLOAD="redimensionar();" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Proveedor De ACPM"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<% Proveedor_Acpm pacpm = model.proveedoracpmService.getProveedor();
%>
<table width="60%" border="2" align="center">
	<tr>
        <td>
            <table width="100%" align="center"  >
                <tr class="fila">            
                    <td class="subtitulo1" width="50%">Datos Del Proveedor</td>
                    <td class="barratitulo" width="50%"><img src="<%=BASEURL%>/images/titulo.gif"></img></td>                    
              </tr>     				  				  
				  <tr class="fila">
					<td>Distrito</td>
					<td colspan="2"><%=pacpm.getDstrct()%></td>
			  </tr>
				 
				  <tr class="fila">
					<td>Ciudad</td>
					<td colspan="2"><%=pacpm.getCity_code()%></td>
			  </tr>
				  <tr class="fila">
					<td>Nit</td>
					<td colspan="2"><%=pacpm.getNit()%></td>
			  </tr>
				  <tr class="fila">
					<td>Nombre</td>
					<td colspan="2"><%=pacpm.getNombre()%></td>
			  </tr>
				  <tr class="fila">
					<td>Sucursal</td>
					<td colspan="2"><%=pacpm.getCodigo()%></td>
			  </tr>
				  <tr class="fila">
					<td>Valor Acpm</td>
					<td><%=pacpm.getValor()%><%=pacpm.getMoneda()%></td>
			  </tr>
				  <tr class="fila">
					<td> Max efectivo</td>
					<td colspan="2"><%=pacpm.getMax_e()%></td>
			  </tr>
				  <tr class="fila">
					<td>Tipo de servicio </td>
					<td colspan="2">
						<span class="style7">
							<%if(pacpm.getTipo().equals("E")){%>
								Efectivo
							<%}else{%>
								Gasolina
							<%}%>
						</span>
					</td>
			  </tr>
		  </table>
		</td>
	</tr>
</table>
<br>
<br>
<table align="center" width="60%">
	<tr>
		<td align="center">
			<img title='Volver a realizar la consulta' src="<%= BASEURL %>/images/botones/regresar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick= 'location.href = "<%=CONTROLLER%>?estado=Menu&accion=Enviar&numero=39'></img>			
		</td>
	</tr>
</table>
</div>
</body>
</html>



