<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>Consultar Proveedores Anticipo</title>
<script language="javascript" src="../../slt2%2010.11.2005/js/validar.js"></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="../../slt2%2010.11.2005/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Consultar Proveedores Anticipo"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<%
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
    Vector anticipos =model.proveedoranticipoService.getProveedores();
	if (anticipos.size()<1)
		response.sendRedirect(CONTROLLER+"?estado=Menu&accion=Enviar&numero=41&mensaje=");
%>
<table width="100%"  border="2">
  <tr>
    <td><table width="100%">
      <tr>
        <td width="373" class="subtitulo1">&nbsp;<strong>LISTA DE PROVEEDORES ANTICIPO</strong></td>
        <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
      </tr>
    </table>
      <table width="100%" border="1" align="center" cellpadding="2" cellspacing="3" bordercolor="#999999" bgcolor="#F7F5F4" class="Estilo1">
        <tr class="tblTitulo"  >
          <td width="108" nowrap>DISTRITO</td>
          <td width="87" nowrap>CIUDAD</td>
          <td width="156" ><div align="center" >NIT</div></td>
          <td width="100" ><span >NOMBRE</span></td>
          <td width="100" >SUCURSAL</td>
        </tr>
        <pg:pager
    items="<%=anticipos.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
        <%-- keep track of preference --%>
        <%
      for (int i = offset.intValue(),
	         l = Math.min(i + maxPageItems, anticipos.size());
	     i < l; i++)
	{
        Proveedor_Anticipo pa = (Proveedor_Anticipo) anticipos.elementAt(i);%>
        <pg:item>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
          <td class="bordereporte"><span ><%=pa.getDstrct()%></span></td>
          <td class="bordereporte"><span ><%=pa.getCiudad()%></span></td>
          <td class="bordereporte"  ><div align="center" ><%=pa.getNit()%> </div></td>
          <td class="bordereporte"><span ><%=pa.getNombre()%></span></td>
          <td class="bordereporte"><span ><%=pa.getCodigo()%></span></td>
        </tr>
        </pg:item>
        <%}
  %>
        <tr >
          <td height="30" colspan="5" nowrap> <pg:index>
            <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>         
    </pg:index></td>
        </tr>
        </pg:pager>
      </table>      </td>
  </tr>
</table>
<br>  
  
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr align="left">
    <td><img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="window.location = '<%=CONTROLLER%>?estado=Menu&accion=Enviar&numero=41'"  style="cursor:hand "> 
  </tr>
</table>
</div>
</body>
</html>



