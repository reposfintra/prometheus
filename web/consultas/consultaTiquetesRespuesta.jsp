<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<link href="../css/letras.css" rel="stylesheet" type="text/css">

<title>Buscar Planilla</title>
<script language="javascript" src="../js/validar.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.Estilo1 {font-weight: bold}
.style3 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
}
.style7 {font-family: Verdana, Arial, Helvetica, sans-serif;  font-size: 12px; }
.style10 {font-size: 11px}
-->
</style>
</head>
<body>
<%
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
    Vector tikets = model.proveedortiquetesService.getProveedores();
	if (tikets.size()<1)
		response.sendRedirect(CONTROLLER+"?estado=Menu&accion=Enviar&numero=40&mensaje=La busqueda no arroj� Resultados");
%>
<table width="600"  border="2" align="center">
  <tr>
    <td>
	  <table width="100%" class="tablaInferior">
        <tr>
          <td colspan="2">
		    <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="barratitulo">
              <tr>
                <td width="33%" class="subtitulo1">Lista de Proveedor de Ticketes</td>
                <td width="67%"><img src="<%=BASEURL%>/images/titulo.gif" width="33" height="20"></td>
              </tr>
            </table>          
	      </td>
        </tr>
        <trtr class="subtitulos" align="center">
          <td width="108" nowrap>Distrito</td>
          <td width="87" nowrap>Ciudad</td>
          <td width="156">Nit</td>
          <td width="100">Nombre</td>
          <td width="100">Sucursal</td>
        </tr>
  <pg:pager
    items="<%=tikets.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
<%-- keep track of preference --%>

  <%
      for (int i = offset.intValue(),
	         l = Math.min(i + maxPageItems, tikets.size());
	     i < l; i++)
	{
        Proveedor_Tiquetes pt = (Proveedor_Tiquetes) tikets.elementAt(i);%>
		<pg:item>
 	    <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand">
          <td><%=pt.getDstrct()%></td>
          <td><%=pt.getCiudad()%></td>
          <td><%=pt.getNit()%></td>
          <td><%=pt.getNombre()%></td>
          <td><%=pt.getCodigo()%></td>
 	    </tr>
 	 
  </pg:item>
  <%}
  %>
        <tr>
 	      <td height="20" colspan="9" nowrap align="center"><pg:index>
	<jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
	</pg:index></td>
        </tr>
  </pg:pager>
      </table>
    </td>
  </tr>
</table>
<br>  
 <table width="600" align="center">
  <tr>
     <img src="<%=BASEURL%>/images/botones/regresar.gif" name="regresar" id="regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="history.back();">
  </tr>
</table>  
</body>
</html>



