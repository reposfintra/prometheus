<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">

<title>Lista de Identidades</title>
<script language="javascript" src="../js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script language="javascript" src="<%=BASEURL%>/js/boton.js"></script>

<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Consultas Propietario y/o Conductor"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
</head>
<%
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 20;
    int maxIndexPages = 10;
    Vector nits = model.conductorService.getConductores();
	if (nits.size()<1)
		response.sendRedirect(CONTROLLER+"?estado=Menu&accion=Enviar&numero=37&mensaje=");
%>
<script>
function ir( variable1, variable2 ){
	window.opener.forma.nit_ben.value= variable1; 
	window.opener.forma.nom_ben.value= variable2; 
	window.close();
}
</script>
<table border="2" align="center" width="546">
  <tr>
    <td>
	<table width="99%"  align="center"   >
  <tr>
    <td width="227" height="24"  class="subtitulo1"><p align="left">Lista de Identidades </p></td>
    <td width="229"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4" class="tblTitulo">
  <tr>
    <td width="158" nowrap><span class="style7 Estilo1">CEDULA</span></td>
    <td width="323" ><div align="center" class="style7 Estilo1">
        <div align="left">NOMBRE</div>
    </div></td>
  </tr>
  <pg:pager
    items="<%=nits.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
  <%-- keep track of preference --%>
  <%
      for (int i = offset.intValue(),
	         l = Math.min(i + maxPageItems, nits.size());
	     i < l; i++)
	{
        Conductor cond = (Conductor) nits.elementAt(i);%>
  <pg:item>
  <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" title="Mostrar Conductor..." onClick="ir('<%=cond.getCedula()%>','<%=cond.getNombre()%>');" >
    <td class="bordereporte"><%=cond.getCedula()%></td>
    <td class="bordereporte"><%=cond.getNombre()%></td>
  </tr>
  </pg:item>
  <%}
  %>
  <tr class="filagris">
    <td height="92" colspan="2" nowrap align="center"><pg:index>
      <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
    </pg:index></td>
  </tr>
  </pg:pager>
</table>
</td>
</tr>
</table>
<br>
<table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr align="left">
    <td><img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Enviar&numero=37'" style="cursor:hand "> 
  </tr>
</table>
</div>
</body>
</html>



