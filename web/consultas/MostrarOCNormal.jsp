<%@page session="true"%>
<%@page import="java.util.*" %>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<html>
<head>
<title>Consulta Planilla - Remesa</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
 <script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
 <script type='text/javascript' src="<%=BASEURL%>/js/consultadespacho.js"></script> 
 <script type='text/javascript' src="<%=BASEURL%>/js/general.js"></script> 
</head> 
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Consulta Planilla - Remesa"/>
</div> 

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%
   Planillas  planilla = model.PlanillasSvc.getPlanilla() ;
   Remesa rem = model.remesaService.getRemesa();
   double vlrTasa = 1;
   	if(rem.getVlrRem()!=0){
		vlrTasa=com.tsp.util.Util.redondear(rem.getVlr_pesos()/rem.getVlrRem(),2);
	}
    double vlrTasaPla= 1;
   	if(planilla.getValorMe()!=0){
		vlrTasaPla=com.tsp.util.Util.redondear(planilla.getValorMe()/planilla.getValor(),2);
	}                    
    
   if (request.getParameter("comentario").equals("") && planilla!=null && rem!=null){
   	String docinterno=rem.getDocInterno();
	String vecdoc []= docinterno.split(",");
	docinterno="";
		for(int v =0; v<vecdoc.length; v++){
			String a = vecdoc[v];
			docinterno =  a.replace(":","") +"<br>"+ docinterno;
			
		}
   String c1="",c2="";
   if(planilla.getContenedores()!=null){
   	if(!planilla.getContenedores().equals("")){
   		String v []=planilla.getContenedores().split(",");
		c1 = v[0];
		if(v.length>1){
			c2=v[1];
		}
		
   	}
   }
   
   String pc1="",pc2="";
   String precinto="";
   String precintos[] = planilla.getPrecintos().split(",");
	for(int p=0; p<precintos.length;p++){
			
			if(p==0){
				if(!precintos[p].equals(" "))
					pc1 = precintos[p];
			}
			else if(p==1){
				if(!precintos[p].equals(" "))
					pc2 = precintos[p];
			}
			if(pc1.equals("")&&pc2.equals("") && p > 1 ){
				precinto = precinto +precintos[p]+"<br>";
			}
	}
   %>
<table width="850" border="2" align="center">          
    <tr>
      <td width="100%">
	  	<table width="100%">
          <tr>
            <td width="49%" height="22" align="left" class="subtitulo1">&nbsp;Informaci&oacute;n de la Planilla </td>
            <td width="51%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
        </table>
	  	<table width="100%">
	    	
        	<tr>
          		<td width="15%" class="fila">No.</td>
          		<td width="18%" class="letra"><%= planilla.getPlanilla() %></td>
          		<td width="11%" class="fila">Estado </td>
		        <td width="24%" class="letra"><%=planilla.getEstado()%> <%=planilla.getCumplido()%> <%=planilla.getDiscrepancia()%></td>
       		   	<td width="16%" class="fila">Fecha Creaci&oacute;n</td>
          		<td width="16%" class="letra"><%= planilla.getFechaCreacion()   %> </td>
        	</tr>
			<tr>
        	  <td class="fila">Distrito</td>
        	  <td class="letra"><%= planilla.getDistrito() %></td>
        	  <td class="fila">Tipo</td>
        	  <td class="letra"><%= planilla.getTipo() %></td>
        	  <td class="fila">Fecha Anulaci&oacute;n</td>
        	  <td class="letra"><%=planilla.getFecha_anul()%></td>
      	  </tr>
        	<tr>
        	  <td class="fila">Ruta</td>
        	  <td class="letra"><%= planilla.getOrigen() %> - <%= planilla.getDestino()%></td>
        	  <td class="fila">Remitente</td>
        	  <td class="letra"><%= rem.getNrem() %></td>
       	      <td colspan="2" class="fila" align="center">Precintos</td>
   	      </tr>
        	<tr>
        	  <td height="20" class="fila">Placa</td>
        	  <td class="letra"><%= planilla.getPlaca()       %></td>
        	  <td class="fila">Trailer</td>
        	  <td class="letra"><%=planilla.getTrailer()%></td>
        	  <td colspan="2" rowspan="3" valign="top" class="letra"><div style=" overflow:auto ; WIDTH: 100%;  height:50; "><%=precinto%>
			  </div></td>
       	  </tr>
		  <tr>
        	  <td class="fila">Cedula Conductor</td>
        	  <td class="letra"><%= planilla.getCedulaConductor()%></td>
        	  <td class="fila">Nombre</td>
        	  <td class="letra"><%= planilla.getConductor()%></td>
       	  </tr>
        	<tr>
        	  <td height="20" class="fila">Cedula Propietario</td>
        	  <td class="letra"><%= planilla.getCedulaPropietario() %></td>
        	  <td class="fila">Nombre</td>
        	  <td class="letra"><%=planilla.getPropietario()%></td>
       	  </tr>
		
   	
        	<tr>
        	  <td class="fila">Cantidad despacho</td>
        	  <td class="letra"><%= planilla.getCarga() %> <%= planilla.getUnidad() %> </td>
        	  <td class="fila">Valor Flete</td>
        	  <td class="letra"><%=com.tsp.util.Util.customFormat(planilla.getValorFlete())%> <%= planilla.getMoneda()%> </td>
			  <td class="fila" align="center">Placa Contenedor </td>
        	  <td class="fila" align="center">Precinto Contenedor</td>
			  
       	  </tr>
        	<tr>
        	  <td height="20" class="fila">Valor Planilla</td>
        	  <td class="letra"><%= com.tsp.util.Util.customFormat(planilla.getValor())          %> <%= planilla.getMoneda()%> </td>
        	  <td class="fila">Valor Planilla Moneda Local </td>
        	  <td valign="top" class="letra"><%=com.tsp.util.Util.customFormat(planilla.getValorMe())%></td>
        	  <td class="letra" align="center"><%=c1%></td>
        	  <td class="letra" align="center"><%=pc1%></td>
       	  </tr>
        	<tr>
        	  <td class="fila"><span class="letra">Tasa de Cambio</span></td>
        	  <td class="letra"><%=com.tsp.util.Util.customFormat(vlrTasaPla)%>&nbsp;</td>
        	  <td class="fila">&nbsp;</td>
        	  <td class="fila">&nbsp;</td>
        	  <td class="letra" align="center"><%=c2%></td>
        	  <td class="letra" align="center"><%=pc2%></td>
      	  </tr>
   	  </table>
	  <%if(!planilla.getCumplido().equals("")){%>	  
	  	<table width="100%">
          <tr>
            <td width="15%" class="fila">Cant. Cumplida</td>
            <td width="18%" colspan="2" class="letra"><%= com.tsp.util.Util.customFormat( Double.parseDouble(planilla.getCantCumplida()) ) %> <%= planilla.getUnidadCumplida() %> </td>
            <td width="12%" class="fila">Fecha Cumplida</td>
            <td width="23%" class="letra"><%= planilla.getFechaCumplido() %></td>
            <td width="16%" class="fila">Cumplida por </td>
            <td width="16%" class="letra"><%= planilla.getUsuario_cumplio() %></td>
          </tr>
          <tr>
            <td colspan="2" class="fila">Valor planilla cumplido</td>
            <td colspan="2" class="letra"><%=com.tsp.util.Util.customFormat( Double.parseDouble(planilla.getCantCumplida()) * planilla.getValorFlete())%>&nbsp;</td>
            <td class="fila">Valor planilla cumplido Moneda Local</td>
            <td colspan="2" class="letra"><span class="letra"><%=com.tsp.util.Util.customFormat(Double.parseDouble(planilla.getCantCumplida()) * planilla.getValorFlete() * vlrTasaPla)%></span></td>
          </tr>
        </table>
		<%}%></td>
	</tr>
</table>


  <table width="850" border="2" align="center">
    <tr>
      <td width="100%" height="60">
        <table width="100%">
          <tr>
            <td width="49%" height="22" align="left" class="subtitulo1">&nbsp;Informaci&oacute;n de la Remesa </td>
            <td width="51%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
        </table>
        <table width="100%">
          <tr>
            <td width="15%" class="fila">No.</td>
            <td width="13%" class="letra"><%= rem.getNumrem() %></td>
            <td width="14%" class="fila">Estado </td>
            <td width="26%" class="letra"><%=rem.getEstado()%> <%=rem.getCumplida()%> <%=!rem.getEstado().equalsIgnoreCase("ANULADA")?rem.getNfacturable().equals("S")?"NO FACTURABLE":"FACTURABLE":""%> </td>
            <td width="16%" class="fila">Fecha Creaci&oacute;n </td>
            <td width="16%" class="letra"><%=rem.getFecRem()%> </td>
          </tr>
		  <tr>
            <td class="fila">Stdjob</td>
            <td class="letra" style="cursor:hand " onClick="window.open('<%=CONTROLLER%>?estado=Consulta&accion=StdJob&sj=<%=rem.getStdJobNo()%>&cliente=','','status=yes,scrollbars=no,resizable=yes')"><span class="Simulacion_Hiper"><%= rem.getStdJobNo()     %></span></td>
            <td class="fila">Descripci&oacute;n</td>
            <td class="letra"><%=  rem.getDescripcion() %></td>
            <td class="fila">Fecha Anulaci&oacute;n</td>
            <td class="letra"><%=rem.getFec_anul()%></td>
          </tr>
          <tr>
            <td class="fila">Codigo Cliente</td>
            <td class="letra"><%= rem.getCliente()%></td>
            <td class="fila">Nombre</td>
            <td class="letra"><%= rem.getNombre_cli() %></td>
            <td class="fila">Cross Docking</td>
            <td class="letra"><%=rem.getCrossdocking().equals("N")?"NO":rem.getCrossdocking().equals("Y")?"SI":"NO REGISTRADO"%></td>
          </tr>
          <tr>
            <td class="fila">Remesa Padre </td>
            <td class="letra" style="cursor:hand "   onClick="window.open('<%=CONTROLLER%>?estado=RemesaPadre&accion=Search&padre=0k&numrem=<%=rem.getNumrem()%>&numpadre=<%=rem.getPadre()%>&texto=&operacion=1&opcion=2','','status=yes,scrollbars=no,resizable=yes')" ><span class="Simulacion_Hiper"><%=rem.getPadre()%></span></td>
            <td class="fila">Valor Tarifa </td>
            <td class="letra"><%=com.tsp.util.Util.customFormat(rem.getTarifa())%> <%=rem.getCurrency()%> </td>
            <td class="letra"><span class="fila">Tasa de Cambio</span></td>
            <td class="letra"><%=com.tsp.util.Util.customFormat(vlrTasa)%></td>
          </tr>
      </table>
        <table width="100%">
          <tr>
            <td width="15%" class="fila">Cant. a Facturar</td>
            <td width="13%" class="letra"><%=rem.getPesoReal()%> <%=rem.getUnidad()%> </td>
            <td width="14%" class="fila">Valor Remesa </td>
            <td width="26%" class="letra"><%=com.tsp.util.Util.customFormat(rem.getVlrRem())%> <%=rem.getCurrency()%></td>
            <td width="16%" class="fila">Valor Remesa Moneda Local</td>
            <td width="16%" class="letra"><%=com.tsp.util.Util.customFormat(rem.getVlr_pesos())%></td>
          </tr>
        </table>
        <%if(!rem.getCumplida().equals("")){%>
        <table width="100%">
          <tr>
            <td height="22" align="left" class="subtitulo1">Datos del Cumplido</td>
          </tr>
        </table>
        <table width="100%">
          <tr>
            <td width="15%" class="fila">Fecha Cumplida</td>
            <td width="13%" class="letra"><%= rem.getFeccum() %></td>
            <td width="14%" class="fila">Cumplida por </td>
            <td colspan="3" class="letra"><%= rem.getUsuario_cumple() %></td>
          </tr>
          <tr>
            <td class="fila">Cant. Cumplida</td>
            <td class="letra"><%= com.tsp.util.Util.customFormat( Double.parseDouble(rem.getCant_cumplida()) ) %> <%= rem.getUnidad_cumplida()!=null ? rem.getUnidad_cumplida().substring(0,1) : ""%></td>
            <td class="fila">Valor Remesa </td>
            <td width="26%" class="letra"><%=com.tsp.util.Util.customFormat( Double.parseDouble(rem.getCant_cumplida()) * rem.getTarifa())%> <%=rem.getCurrency()%></td>
            <td width="16%" class="fila">Valor Remesa Moneda Local</td>
            <td width="16%" class="letra"><%=com.tsp.util.Util.customFormat( Double.parseDouble(rem.getCant_cumplida()) * rem.getTarifa() * vlrTasa )%></td>
          </tr>
        </table>        
        <%}%></td>
    </tr>
  </table>
  
  <table width="850" border="2" align="center">
    <tr>
      <td height="26"><table width="100%">
          <tr class="fila" id="cantidad">
            <td width="21%" height="16"  >
              <div align="center"><a onClick="javascript:VerMovimiento('<%=CONTROLLER%>','<%= planilla.getPlanilla() %>','<%= planilla.getDistrito() %>','<%= planilla.getCod_agencia_despacho() %>');" style="cursor:hand; text-decoration:underline" class="Simulacion_Hiper" >Ver Movimientos de Anticipos</a></div></td>
            <td width="29%"  align="center" ><a onClick="window.open('<%=CONTROLLER%>?estado=Consultar&accion=Despacho&planilla=<%= planilla.getPlanilla() %>&opcion=TodosMov','','status=no,scrollbars=no,width=600,height=700,resizable=yes');" style="cursor:hand; text-decoration:underline" class="Simulacion_Hiper" >Ver Todos los Movimientos de la Planilla</a></td>
            <td width="17%"  align="center" >
              <%if( planilla.getDiscrepancia().equals("CON DISCREPANCIA")){%>
              <a onClick="javascript:abrir_discrepancia('<%=CONTROLLER%>','<%= planilla.getPlanilla() %>');" style="cursor:hand; text-decoration:underline" class="Simulacion_Hiper" >Ver Discrepancia </a>
            <%}%></td>
            <td width="13%"  align="center" ><a onClick="javascript:VerExtrafletes('<%=CONTROLLER%>','<%= planilla.getPlanilla() %>','<%= planilla.getDistrito() %>','<%= rem.getStdJobNo() %>');" style="cursor:hand; text-decoration:underline" class="Simulacion_Hiper" >Ver ExtraFletes </a></td>
            <td width="20%"  align="center" ><a onClick="javascript:openWindow('<%=CONTROLLER%>?estado=MovimientoTrafico&accion=BuscarPlanilla&cmd=show&numpla=<%= planilla.getPlanilla() %>');" style="cursor:hand; text-decoration:underline" class="Simulacion_Hiper" >Ver Movimiento Trafico </a></td>
          </tr>
      </table></td>
    </tr>
  </table>
  <%Vector vecDoc = model.RemDocSvc.getDocumentos();
   if(vecDoc.size()>0){%>
  <table width="850" border="2" align="center">
    <tr>
      <td><table width="100%">
          <tr>
            <td width="48%" height="22" align="left" class="subtitulo1">&nbsp;Documentos Remesa </td>
            <td width="52%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
        </table>
          <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
            <tr class="tblTitulo" align="center">
              <td width="15%"  >Tipo Documento</td>
              <td width="14%"  >Documento</td>
            </tr>
            <%
                      if (vecDoc!=null) {
                          for (int i=0; i<vecDoc.size(); i++) {
                             remesa_docto remD = (remesa_docto) vecDoc.elementAt(i);%>
            <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
              <td height="16"  class="bordereporte"><%=remD.getDocument_name()%></td>
              <td height="16"    class="bordereporte"><%=remD.getDocumento()%></td>
            </tr>
            <%    } //cierre for
                      }//cierre if%>
        </table></td>
    </tr>
  </table>
  <%}
     Vector vecDocs = model.RemDocSvc.getDestinatariosdoc();
	 if(vecDocs.size()>0){%>
  <table width="850" border="2" align="center">
    <tr>
      <td><table width="100%">
        <tr>
          <td width="48%" height="22" align="left" class="subtitulo1">&nbsp;Datos Destinatarios y Documentos</td>
          <td width="52%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
	  
      <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
          <tr class="tblTitulo" align="center">
            <td width="12%"  >Ciudad</td>
            <td width="29%"  >Destinatario</td>
            <td width="15%"  >Tipo Documento</td>
            <td width="14%"  >Documento</td>
            <td width="17%"  >Tipo Doc. Relacionado</td>
            <td width="13%"  > Doc. Relacionado</td>
          </tr>
          <%
                      if (vecDocs!=null) {
                          for (int i=0; i<vecDocs.size(); i++) {
                             remesa_docto remD = (remesa_docto) vecDocs.elementAt(i);%>
          <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
            <td height="16"   class="bordereporte"><%=remD.getCiudad_destinatario()%></td>
            <td height="16"   class="bordereporte"><%=remD.getDestinatario()%></td>
            <td height="16"  class="bordereporte"><%=remD.getDocument_name()%></td>
            <td height="16"    class="bordereporte"><%=remD.getDocumento()%></td>
            <td   class="bordereporte"><%=remD.getDocument_rel_name()%></td>
            <td   class="bordereporte"><%=remD.getDocumento_rel()%></td>
          </tr>
          <%    } //cierre for
                      }//cierre if%>
      </table></td>
    </tr>
  </table>
  <%}%>
  <table width="850" border="2" align="center">
    <tr>
      <td width="100%">
        <table width="100%">
          <tr>
            <td width="48%" height="22" align="left" class="subtitulo1">&nbsp;Informaci&oacute;n Adicional </td>
            <td width="52%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
        </table>
        <table width="100%">
          <tr>
            <td width="14%" class="fila">Age. Elabor&oacute;</td>
            <td width="32%" class="letra"><%= planilla.getAgenciaDespacho() %></td>
            <td width="17%" class="fila">Despachador</td>
            <td width="37%" class="letra"><%= planilla.getDespachador() %> </td>
          </tr>
      </table></td>
    </tr>
  </table>  
  <div align="center"><br>
    </tab
  ><img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
  </div>
  <p><%
 }
 else{
  %>
</p>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%= request.getParameter("comentario") %></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%}%>
</div>
</body>
</html>
