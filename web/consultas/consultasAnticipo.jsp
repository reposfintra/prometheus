<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<link href="css/letras.css" rel="stylesheet" type="text/css">

<title>Consultar Proveedores Anticipo</title>
<script language="javascript" src="js/validar.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<script src="<%=BASEURL%>/js/validar.js"></script>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Consultar Proveedores Anticipo"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Consultar&accion=ProveedorAnticipo" onSubmit="return validarConsulta(this);">
  <table width="405"  border="2" align="center">
    <tr>
      <td><table width="100%" class="tablaInferior">
          <tr>
            <td width="50%" class="subtitulo1">Busque Proveedor Anticipo</td>
            <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"> </td>
          </tr>
        </table>
          <table width="100%" align="center" class="tablaInferior">
            <tr class="fila">
              <td width="23%" nowrap><strong>Ciudad:</strong></td>
              <td width="77%" nowrap><input name="ciudad" type="text" class="textbox" id="ciudad" maxlength="40">
              </td>
            </tr>
            <tr class="fila">
              <td nowrap><strong>Nombre:</strong></td>
              <td nowrap><input name="nombre" type="text" class="textbox" id="nombre"></td>
            </tr>
            <tr class="fila">
              <td nowrap><strong>Nit.</strong></td>
              <td nowrap><input name="nit" type="text" class="textbox" id="nit"></td>
            </tr>
          </table>          </td>
    </tr>
  </table>
  <div align="center"><br>
      <input:form name="forma" bean="app" attributesText="id='forma' onSubmit='return validarTCamposLlenos();'" method="post" action="<%=CONTROLLER + "?estado=Aplicacion&accion=Insert&cmd=show"%>" ><img src="<%= BASEURL%>/images/botones/buscar.gif"  name="imgbuscar" id="imgbuscar" style="cursor:hand " onClick="form1.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp; <img src="<%= BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="form1.reset();" onMouseOut="botonOut(this);"  style="cursor:hand ">&nbsp; <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);"  style="cursor:hand "></input:form>  <br>
  </div>
</form>
<%if(request.getParameter("mensaje")!=null){%>
<table border="2" align="center">
  <tr>
    <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="282" align="center" class="mensajes"><span class="style1">No se encontraron resultados en la b&uacute;squeda</span>.</td>
          <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="78">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<%}%>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</body>
</html>

