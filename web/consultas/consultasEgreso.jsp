<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<link href="css/letras.css" rel="stylesheet" type="text/css">

<title>Buscar Planilla</title>
<script language="javascript" src="js/validar.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.Estilo1 {font-weight: bold}
.style1 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-weight: bold;
	font-size: 12px;
	color: #FF0000;
}
-->
</style>
</head>
<body>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Consultar&accion=Egreso" onSubmit="return validarConsulta(this);">
  <table width="573" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="ECE0D8" class="Letras">
    <tr bgcolor="#FFA928">
      <td colspan="2" nowrap><div align="center" class="Estilo2"><strong><strong>BUSQUE EL EGRESO</strong></strong></div></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td width="162" nowrap><strong>Distrito:</strong></td>
      <td width="395" nowrap bgcolor="#CEFFFF"><input name="distrito" type="text" id="distrito" maxlength="10">
      </td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap><strong>Banco:</strong></td>
      <td nowrap bgcolor="#CEFFFF"><input name="banco" type="text" id="banco"></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap><strong>Cuenta:</strong></td>
      <td nowrap bgcolor="#CEFFFF"><input name="cuenta" type="text" id="cuenta"></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap><strong>No del Egreso:</strong></td>
      <td nowrap bgcolor="#CEFFFF"><input name="doc_no" type="text" id="doc_no"></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap><strong>Nit:</strong></td>
      <td nowrap bgcolor="#CEFFFF"><input name="nit" type="text" id="nit"></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap><strong>Nombre Beneficiario : </strong></td>
      <td nowrap bgcolor="#CEFFFF"><input name="nombre" type="text" id="nombre"></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td nowrap><strong>Fecha del despacho: </strong></td>
      <td nowrap bgcolor="#CEFFFF">Desde
        <input name="fechaini" type="text" id="fechaini" size="18" readonly>        
        <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fechaini);return false;" HIDEFOCUS> <img src="js/Calendario/cal.gif" width="16" height="16"
               border="0" alt="De click aqu&iacute; para escoger la fecha"></a> Hasta
        <input name="fechafin" type="text" id="fechafin" size="18" readonly>
        <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fechafin);return false;" HIDEFOCUS> <img src="js/Calendario/cal.gif" width="16" height="16"
               border="0" alt="De click aqu&iacute; para escoger la fecha"></a></td>
    </tr>
    <tr bgcolor="#99CCFF">
      <td colspan="2" nowrap><div align="center">
        <input type="submit" name="Submit" value="Buscar...">
      </div></td>
    </tr>
  </table>
</form>
<%if(request.getParameter("mensaje")!=null){%>
	<span class="style1">No se encontraron resultados en la busqueda</span>.
<%}%>

<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</body>
</html>

