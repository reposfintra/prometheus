<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<link href="../css/letras.css" rel="stylesheet" type="text/css">

<title>Buscar Planilla</title>
<script language="javascript" src="../js/validar.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css">
<!--
.Estilo1 {font-weight: bold}
.style3 {
	font-family: Verdana, Arial, Helvetica, sans-serif;
	font-size: 12px;
}
.style7 {font-family: Verdana, Arial, Helvetica, sans-serif;  font-size: 12px; }
.style10 {font-size: 11px}
-->
</style>
</head>
<body>
<%if(request.getParameter("anular")!=null){%>
<TABLE width='100%' align='center' class='Letras'>
  <TR>
    <TD ALIGN='center'><FIELDSET>
      <legend><b>NOTA : </b></legend>
      Se asigno con exito la nueva remesa, a continuaci&oacute;n se muestra la lista de remesas relacionadas a la planilla. 
    </FIELDSET></TD>
  </TR>
</TABLE>
<%}%>
<br>
<%
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
    Vector planillas = model.planillaService.getPlas();
	if (planillas.size()<1)
		response.sendRedirect(CONTROLLER+"?estado=Menu&accion=Enviar&numero=34&normal=ok&mensaje=");
%>
<table width="100%" border="1" align="center" cellpadding="2" cellspacing="1" bordercolor="#CCCCCC" bgcolor="ECE0D8" class="Letras">
  <tr bgcolor="#FFA928">
    <td height="25" colspan="8" nowrap><div align="center" class="Estilo1 style3"><strong>LISTA PLANILLAS</strong></div></td>
  </tr>
  <tr bgcolor="#99CCFF" class="style7">
    <td width="81" nowrap><span class="style7 Estilo1">PLANILLA</span></td>
    <td width="104" bgcolor="#99CCFF"><div align="center" class="style7 Estilo1">REMISION</div></td>
    <td width="221" bgcolor="#99CCFF"><span class="style7 Estilo1">STANDARD JOB</span></td>
    <td width="95" bgcolor="#99CCFF"><span class="style7 Estilo1">PLACA</span></td>
    <td width="115" class="Estilo4"><div align="center" class="style7 Estilo1"> CONDUCTOR</div></td>
    <td width="188" class="Estilo4"><div align="center" class="style7 Estilo1">NOMBRE CONDUCTOR</div></td>
    <td width="69" class="Estilo4"><span class="style7 Estilo1">FECHA DEL DESPACHO</span></td>
    <td width="69" class="Estilo1">DESPACHADOR</td>
  </tr>
  <pg:pager
    items="<%=planillas.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
<%-- keep track of preference --%>

  <%
      for (int i = offset.intValue(),
	         l = Math.min(i + maxPageItems, planillas.size());
	     i < l; i++)
	{
        Planilla planilla = (Planilla) planillas.elementAt(i);%>
		<pg:item>
 	 <tr >
    <td height="30" nowrap style="cursor:hand" title="Mostrar Planilla..." onClick="window.location='<%=CONTROLLER%>?estado=ConsultaOCNormal&accion=Buscar&tipo=1&distrito=TSP&numeroOC=<%=planilla.getNumpla()%>&numeroOT=<%=planilla.getNumrem()%>'" onMouseOver="bgColor= '#99cc99'" onMouseOut="bgColor=''"><div align="center" class="style7"><%=planilla.getNumpla()%> </div></td>
    <td  height="30" nowrap style="cursor:hand" title="Mostrar Remesa..." onClick="window.location='<%=CONTROLLER%>?estado=ConsultaOCNormal&accion=Buscar&tipo=1&distrito=TSP&numeroOC=<%=planilla.getNumpla()%>&numeroOT=<%=planilla.getNumrem()%>'" onMouseOver="bgColor= '#99cc99'" onMouseOut="bgColor=''"><div align="center" class="style7"><%=planilla.getNumrem()%> </div></td>
    <td><span class="style7"><%=planilla.getSj_desc()%></span></td>
    <td><span class="style7"><%=planilla.getPlaveh()%></span></td>
    <td><div align="center" class="style7"><span class="Estilo6"><%=planilla.getCedcon()%></span></div></td>
    <td><span class="style7"><%=planilla.getNomCond()%></span></td>
    <td><span class="style7"><%=planilla.getFecdsp().substring(0,10)%></span></td>
    <td><span class="style7"><%=planilla.getDespachador()%></span></td>
 	 </tr>
 	 
  </pg:item>
  <%}
  %>
  <tr bgcolor="#FFFFFF" class="style7">
 	   <td height="30" colspan="8" nowrap><pg:index>
	<jsp:include page="../WEB-INF/jsp/google.jsp" flush="true"/>
	</pg:index></td>
  </tr>
  </pg:pager>
</table>
<br>  
  
<a href="<%=CONTROLLER%>?estado=Menu&accion=Enviar&numero=34&normal=ok" class="Letras">Volver a Realizar la consulta...</a>
</body>
</html>



