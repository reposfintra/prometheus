<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<link href="../css/letras.css" rel="stylesheet" type="text/css">

<title>Buscar Planilla</title>
<script language="javascript" src="../js/validar.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

</head>
<body>
<%  
    if(request.getParameter("planilla")!=null){
		if(request.getParameter("normal")!=null){
			String dir = "consultas/consultaPlanillaRespuestaNormal.jsp";
			if(request.getParameter("anular")!=null)
				dir = dir + "?anular=0k";
			response.sendRedirect(dir);
		}
		response.sendRedirect("consultas/consultaPlanillaRespuestaNormal.jsp");
	}
   if(request.getParameter("planilla")!=null){
		if(request.getParameter("general")!=null){
			String dir = "consultas/consultaPlanillaRespuestaNormal.jsp";
			if(request.getParameter("anular")!=null) {
				dir = dir + "?anular=0k";
            }
			response.sendRedirect(dir);
		}
		response.sendRedirect("consultas/consultaPlanillaRespuestaNormal.jsp");
	}
	if(request.getParameter("proveedor")!=null){
			response.sendRedirect("consultas/consultaProveedorReembolsableRespuesta.jsp?proveedor="+request.getParameter("proveedor"));
	}
	else if(request.getParameter("stdjob")!=null){
	    response.sendRedirect("consultas/consultaStdjobRespuesta.jsp");
	}
	else if(request.getParameter("remision")!=null){
	    response.sendRedirect("consultas/consultaRemisionRespuesta.jsp");
	}
	else if(request.getParameter("remesa")!=null){
	    response.sendRedirect("consultas/consultaRemesaRespuesta.jsp");
	}
	else if(request.getParameter("conductor")!=null){
	    response.sendRedirect("consultas/consultaConductorRespuesta.jsp");
	}
	else if(request.getParameter("egreso")!=null){
	    response.sendRedirect("consultas/consultaEgresoRespuesta.jsp");
	}
	else if(request.getParameter("acpm")!=null){
	    response.sendRedirect("consultas/consultaAcpmRespuesta.jsp");
	}
	else if(request.getParameter("tiket")!=null){
	    response.sendRedirect("consultas/consultaTiquetesRespuesta.jsp");
	}
	else if(request.getParameter("anticipo")!=null){
	    response.sendRedirect("consultas/consultaAnticipoRespuesta.jsp");
	}
	else if(request.getParameter("despacho")!=null){
	    response.sendRedirect("consultas/consultaDesapchoConductorRespuesta.jsp");
	}
	else if(request.getParameter("cliente")!=null){
		if(request.getParameter("coma")==null){
		    response.sendRedirect("consultas/consultaClientesRespuesta.jsp");
		}
		else{
			response.sendRedirect("jsp/general/exportar/consultaClientesRespuesta.jsp");
		}
	}
	else if(request.getParameter("propietario")!=null){
		
	    response.sendRedirect("consultas/consultaPropietarioRespuesta.jsp");
	}
    else if(request.getParameter("hvida")!=null){
	    response.sendRedirect("consultas/consultaPropietarioRespuesta.jsp?hvida=ok");
	}
    else if(request.getParameter("registroT")!=null){
	    response.sendRedirect("consultas/consultaConductorTRModificar.jsp?registroT=ok");
	}

	else if(request.getParameter("registroTI")!=null){
	    response.sendRedirect("consultas/consultaConductorTR.jsp?registroTI=ok");
	}
        if(request.getParameter("proveedor2")!=null){
			response.sendRedirect("consultas/consultaProveedorRespuesta.jsp?provee="+request.getParameter("provee"));
	}
	 	if(request.getParameter("var")!=null){
			response.sendRedirect("jsp/fenalco/liquidadores/consultaClientesRespuesta.jsp");
	}

%> 
No redirecciono 
</body>
</html>



