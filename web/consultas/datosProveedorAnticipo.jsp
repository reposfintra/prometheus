<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Datos Proveedor Anticipo</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<script src="<%=BASEURL%>/js/validar.js"></script>
<link href="../../slt2%2010.11.2005/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Datos Proveedor Anticipo"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<% Proveedor_Anticipo pt = model.proveedoranticipoService.getProveedor();
%>
<table width="70%"  border="2" align="center">
  <tr>
    <td><table width="100%" class="tablaInferior">
      <tr>
        <td width="50%" class="subtitulo1">Proveedor Anticipo</td>
        <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"> </td>
      </tr>
    </table>
      <table width="100%" align="center" class="tablaInferior">
        <tr class="fila">
          <td width="156"><strong>Distrito</strong></td>
          <td width="424" colspan="2"><span class="style7"><%=pt.getDstrct()%></span></td>
        </tr>
        <tr class="fila">
          <td><strong>Ciudad</strong></td>
          <td colspan="2"><span class="style7"><%=pt.getCiudad()%></span></td>
        </tr>
        <tr class="fila">
          <td><strong>Nit</strong></td>
          <td colspan="2"><span class="style7"><%=pt.getNit()%></span></td>
        </tr>
        <tr class="fila">
          <td><strong>Nombre</strong></td>
          <td colspan="2"><span class="style7"><%=pt.getNombre()%></span></td>
        </tr>
      </table>      </td>
  </tr>
</table>
<br>
<table width="70%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr align="left">
    <td><img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="window.location = '<%=CONTROLLER%>?estado=Menu&accion=Enviar&numero=40'"  style="cursor:hand "> 
  </tr>
</table>
</div>
</body>
</html>



