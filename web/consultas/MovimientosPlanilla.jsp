<!--
- Autor : Ing. Jose de la rosa
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra los movimientos de anticipo
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
 
<html>
<head>
<title>Movimientos de Anticipos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
</head>
<%-- Inicio Body --%>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Consulta Planilla - Remesa"/>
</div> 

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left:0px; top: 100px; overflow: scroll;"> 

<% 
	Vector listaMov =model.movplaService.getLista();
	if(listaMov!=null || listaMov.size()>0){%>

<br>
<br>
<table width="130%" border="2" align="center">
    <tr>
      <td>
	  <table width="100%" align="center">
    
              <tr>
                <td width="373" class="subtitulo1">Todos los &nbsp;Movimientos de la Planilla</td>
                <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
        </table>
		  <table width="100%" border="1" bordercolor="#999999" class="informacion" bgcolor="#F7F5F4" align="center">
          <tr class="tblTitulo" align="center">
          <td width="5%" height="38" align="center">Cheque</td>
          <td width="12%"  align="center">Banco</td>
          <td width="9%" align="center">Sucursal</td>
          <td width="13%" align="center">Concepto</td>
          <td width="2%" align="center">T</td>
          <td width="2%" align="center">A</td>
          <td width="8%" align="center">Valor</td>
		  <td width="6%" align="center">Valor Neto </td>
          <td width="5%" align="center">Moneda</td>
          <td width="6%" align="center">Valor ML. </td>
          <td width="4%" align="center">Proveedor</td>
          <td width="5%" align="center">Usuario</td>
          <td width="7%" align="center">Fecha Creacion </td>
          <td width="6%" align="center">Fecha Migracion </td>
          <td width="10%" align="center">Fecha Contabiliazacion </td>
          <td width="8%" align="center">Informaci&oacute;n Factura </td>
          </tr>
        <%-- keep track of preference --%>
	  <% 
	       for (int i = 0;
	     i <listaMov.size() ; i++)
	{
		
			   Movpla  mov = (Movpla) listaMov.elementAt(i); %>
				<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
				  <td class="bordereporte informacion" align="center"><%=mov.getDocument()%></td>
				  <td class="bordereporte informacion" align="center"><%=mov.getBranch_code()%></td>  
				  <td  class="bordereporte informacion" align="center"><%=mov.getBank_account_no()%></td>
				  <td  class="bordereporte informacion" align="center"><%=mov.getConcept_code()%></td>
				  <td  align="center" class="bordereporte informacion"><%=mov.getInd_vlr()%></td>
				  <td align="center" class="bordereporte informacion"><%=mov.getApplication_ind()%></td>
				  <td class="bordereporte informacion" align="center"><div align="right"><font color="<%=mov.getColor()%>"><%=com.tsp.util.Util.customFormat(mov.getVlr_for())%></font></div></td>
				  <td  class="bordereporte informacion" align="center"><div align="right"><font color="<%=mov.getColor()%>"><%=com.tsp.util.Util.customFormat(mov.getVlr())%></font></div></td>
				  <td  class="bordereporte informacion" align="center"><%=mov.getCurrency()%></td>
				  <td  align="center" class="bordereporte informacion"><div align="right"><font color="<%=mov.getColor()%>"><%=com.tsp.util.Util.customFormat(mov.getVlr_disc())%></font></div></td>
				  <td  align="center" class="bordereporte informacion"><%=mov.getProveedor()!=null?mov.getProveedor():"&nbsp;"%></td>
				  <td width="5%" align="center" class="bordereporte informacion"><%=mov.getCreation_user()%></td>
				  <td width="7%" class="bordereporte informacion" align="center"><%=mov.getCreation_date()%></td>
				  <td width="6%" align="center" class="bordereporte informacion"><%=mov.getFecha_migracion()%></td>
				  <td width="10%" align="center" class="bordereporte informacion"><%=mov.getFecha_cheque()%></td>
				  <td width="8%" class="bordereporte" align="center">
                                  <% if( !mov.getFactura().equals("") ){ %>
                                  <table  width="100%"  border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                                    <tr>
                                        <td class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>"><div align="center">Factura</div></td>
                                        <td class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>"><div align="center">Estado</div></td>
                                    </tr>
                                    <tr>
                                        <td class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>"><div align="center"><a href="JavaScript:void(0);" onClick="window.open('<%= CONTROLLER %>?estado=Factura&accion=Detalle&documento=<%= mov.getFactura().replaceAll("#","-_-") %>&prov=<%= mov.getProvedor_id()%>&tipo_doc=010','DETALLFRA','status=yes,scrollbars=no,width=780,height=650,resizable=yes');" class="Simulacion_Hiper"><%= mov.getFactura()%></a></div></td>
                                        <td class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>"><div align="center"><%= mov.getEstado_factura() %></div></td>
                                     </tr>
                                   </table>
                                    <div align="center">
                                        <% } else {%> 
                                             No Presenta
                                        <% } %>
                                    </div>
                                 </td>
				</tr>

        <%}%>
      </table></td>
    </tr>
</table>
<%}
 else { %>
  <br>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <%}%>
	<br>
<%--tabla para el boton de salir--%>
<table width="5%" border="0" align="center">
	<tr>
		<td>
			<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
		</td>
	</tr>
</table>
</div>
</body>
</html>
<!--
- Entregado a karen 12 Febrero 2007
-->
