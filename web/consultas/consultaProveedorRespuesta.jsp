<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<title>Buscar Proveedor</title>
<script language="javascript" src="../js/validar.js"></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body>
<%
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 15;
    int maxIndexPages = 10;
    List listProveedores = model.ChequeXFacturaSvc.getProveedores();
    //Vector nits = model.clienteService.getClientesVec();
	
%>

<br><br>
<table width="80%"  border="2" align="center">
  <tr>
    <td><table width="100%" class="tablaInferior">
  <tr>
    <td height="24" colspan="2" nowrap><div align="center" class="titulo">
      <table width="100%"  border="0" class="barratitulo">
        <tr>
          <td width="33%" class="subtitulo1">LISTA DE PROVEEDORES</td>
          <td width="67%"><img src="<%=BASEURL%>/images/titulo.gif"></td>
        </tr>
      </table>
      </div></td>
  </tr>
  <tr class="subtitulos">
  <td  nowrap colspan="6">
	  <table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4">
  		<tr class="tblTitulo">
  			<td width="154" >NIT</td>
	    	<td width="620">NOMBRE</td>
  		</tr>
  <pg:pager
    items="<%=listProveedores.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
  <%-- keep track of preference --%>
  <%
      for (int i = offset.intValue(),
	         l = Math.min(i + maxPageItems, listProveedores.size());
	     i < l; i++)
	{
        Hashtable prov = (Hashtable)listProveedores.get(i);
        %>
  <pg:item>
	  <tr height="30" class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' nowrap style="cursor:hand" title="Seleccionar Proveedor..." onClick="copiarProveedor('<%= prov.get("nit") %>','<%= prov.get("nombre") %>');">
	    <td class="bordereporte" ><%= prov.get("nit") %></td>
    	<td class="bordereporte"><%= prov.get("nombre")%></td>
	  </tr>
  </pg:item>
  <%}
  %>
	  <tr bgcolor="#FFFFFF" class="fila">
    	<td height="30" colspan="2" nowrap><pg:index>
	      <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
    	</pg:index></td>
	  </tr>
  </pg:pager>
 	</table>
</td>
</tr>
</table>
</td>
    
</table>

<br>  
  <table width="80%"  border="0" align="center">
  <tr>
    <td><a href="javascript: window.history.back()" class="Simulacion_Hiper">Volver a Realizar la consulta...</a></td>
  </tr>
</table>


</body>
</html>

<script>
    function copiarProveedor(codigo, nombre){
        window.opener.document.form1.provee.value=codigo;
        window.opener.document.form1.nombre.value=nombre;
        window.opener.document.form1.provee.focus();
        window.close();
    }
</script>




