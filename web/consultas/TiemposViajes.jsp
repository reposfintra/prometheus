<!--
- Autor : Ing. Jose de la rosa
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra los tiempos de viajes
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
 
<html>
<head>
<title>Tiempos De Viaje</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
</head>
<%-- Inicio Body --%>
<body>

<%	String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
	Planillas  planilla = model.PlanillasSvc.getPlanilla() ;
	Remesa rem = model.remesaService.getRemesa();
	List listaTiempo = planilla.getListaTiempo();
	if(listaTiempo!=null || listaTiempo.size()>0){ %>

<table width="900" border="2" align="center">
    <tr>
      <td width="1211">
	  <table width="99%" align="center">
              <tr>
                <td width="373" class="subtitulo1">&nbsp;Tiempos de Viajes</td>
                <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
        </table>
		  <table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
          <tr class="tblTitulo" align="center">
          <td width="7%" align="center">Codigo</td>
          <td width="13%"  align="center">Secuencia</td>
          <td width="10%" align="center">Descripci&oacute;n</td>
          <td width="5%" align="center">Fecha</td>
          <td width="9%" align="center">Codigo 1 </td>
          <td width="11%" align="center">Codigo 2 </td>
          <td width="12%" align="center">Diferencia</td>
          </tr>
        <pg:pager
         items="<%=listaTiempo.size()%>"
         index="<%= index %>"
         maxPageItems="<%= maxPageItems %>"
        maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
        <%-- keep track of preference --%>
	<% 
		   int i = 0;
		   Iterator ittT=listaTiempo.iterator();
		   while(ittT.hasNext()){
		   	  i++;
			  TiempoPlanilla tiempo = (TiempoPlanilla) ittT.next(); %>
		<pg:item>
				<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
				  <td width="7%" class="bordereporte" align="center">&nbsp;<%=tiempo.getCodigo()%></td>
				  <td width="13%" class="bordereporte" align="center">&nbsp;<%=tiempo.getSecuencia()%></td>  
				  <td width="10%" class="bordereporte" align="center">&nbsp;<%=tiempo.getDescripcion()%></td>
				  <td width="5%" class="bordereporte" align="center">&nbsp;<%=tiempo.getFecha()%></td>
				  <td width="9%" class="bordereporte" align="center">&nbsp;<%=tiempo.getCodigo1()%></td>
				  <td width="11%" class="bordereporte" align="center">&nbsp;<%=tiempo.getCodigo2()%></td>
				  <td width="12%" class="bordereporte" align="center">&nbsp;<%=tiempo.getDiferencia()%></td>
				</tr>
		</pg:item>
		<%}	%>
        <tr class="pie">
          <td td height="20" colspan="7" nowrap align="center">
		   <pg:index>
            <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
           </pg:index> 
	      </td>
        </tr>
        </pg:pager>
      </table></td>
    </tr>
</table>
<%}
 else { %>
   <br>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <%}%>
	<br>
	<%--tabla para el boton de salir--%>
	<table width="1000" border="0" align="center">
		<tr>
			<td>
				<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
			</td>
		</tr>
	</table>
</body>
</html>
