<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>Buscar Remesa</title>
<script language="javascript" src="../js/validar.js"></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Buscar Remisi�n"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
    Vector remesas = model.remesaService.getRemesas();
//	if (remesas.size()<1)
		//response.sendRedirect(CONTROLLER+"?estado=Menu&accion=Enviar&numero=36&mensaje=");
%>
<table width="100%"  border="2">
  <tr>
    <td><table width="100%">
      <tr>
        <td width="373" class="subtitulo1">&nbsp;Lista Remesas </td>
        <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
      </tr>
    </table>
      <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4"   >
        <tr class="tblTitulo"  >
          <td width="64" nowrap><span >REMISION</span></td>
          <td width="85" ><div align="center" >PLANILLA</div></td>
          <td width="257" ><span >STANDARD JOB</span></td>
          <td width="177" ><span >CLIENTE</span></td>
          <td width="121" ><div align="center" > ORIGEN</div></td>
          <td width="121" ><div align="center" >DESTINO</div></td>
          <td width="118" ><span >FECHA DE LA REMESA</span></td>
        </tr>
        <pg:pager
    items="<%=remesas.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
        <%-- keep track of preference --%>
        <%
      for (int i = offset.intValue(),
	         l = Math.min(i + maxPageItems, remesas.size());
	     i < l; i++)
	{
        Remesa remesa = (Remesa) remesas.elementAt(i);%>
        <pg:item>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
          <td height="30" nowrap class="bordereporte" style="cursor:hand" title="Mostrar Remesa..." onClick="window.open('<%=CONTROLLER%>?estado=ConsultaOC&accion=Buscar&tipo=1&distrito=TSP&numeroOC=<%=remesa.getOc()%>&numeroOT=<%=remesa.getNumrem()%>','','')"  onMouseOver='cambiarColorMouse(this)'><div align="center" ><%=remesa.getRemision()%> </div></td>
          <td height="30" nowrap class="bordereporte" style="cursor:hand" title="Mostrar Planilla..." onClick="window.open('<%=CONTROLLER%>?estado=ConsultaOC&accion=Buscar&tipo=1&distrito=TSP&numeroOC=<%=remesa.getOc()%>&numeroOT=<%=remesa.getNumrem()%>','','')" onMouseOver='cambiarColorMouse(this)'><div align="center" ><%=remesa.getOc()%> </div></td>
          <td class="bordereporte"><span ><%=remesa.getDescripcion()%></span></td>
          <td class="bordereporte"><span ><%=remesa.getCliente()%></span></td>
          <td class="bordereporte"><div><%=remesa.getOriRem()%></div></td>
          <td class="bordereporte"><span ><%=remesa.getDesRem()%></span></td>
          <td class="bordereporte"><span ><%=remesa.getFecRem()%></span></td>
        </tr>
        </pg:item>
        <%}
  %>
        <tr bgcolor="#FFFFFF" >
          <td height="30" colspan="7" nowrap><pg:index>
            <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>    
    </pg:index></td>
        </tr>
        </pg:pager>
      </table>      </td>
  </tr>
</table>
<br>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr align="left">
    <td><img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="window.history.back();"  style="cursor:hand "> 
  </tr>
</table>
<br>  
</div>  
</body>
</html>



