<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<link href="<=BASEURL>/css/estilostsp.css" rel="stylesheet" type="text/css">

<title>Buscar Planilla</title>
<script language="javascript" src="../js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body>
<%
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
    Vector acpms = model.proveedoracpmService.getProveedores();
	if (acpms.size()<1)
		response.sendRedirect(CONTROLLER+"?estado=Menu&accion=Enviar&numero=39&mensaje=");
%>
<table width="100%"  border="2">
  <tr>
    <td><table width="100%" align="center" class="tablaInferior">
  <tr>
    <td height="25" colspan="7" nowrap>
      <table width="100%"  border="0">
        <tr>
          <td width="35%" class="subtitulo1">LISTA DE PROVEEDORES ACPM</td>
          <td width="65%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
      </td>
  </tr>
  <tr>
    <td>
	  <table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
	    <tr class="tblTitulo">
		    <td width="67" nowrap>DISTRITO</td>
		    <td width="89" nowrap>CIUDAD</td>
		    <td width="127" >NIT</td>
		    <td width="140" >NOMBRE</td>
		    <td width="115" >SUCURSAL</td>
		    <td width="138" class=""> TIPO DE SERVICIO </td>
		    <td width="107" class="">VALOR</td>
	    </tr>
	  <pg:pager
    	items="<%=acpms.size()%>"
	    index="<%= index %>"
	    maxPageItems="<%= maxPageItems %>"
	    maxIndexPages="<%= maxIndexPages %>"
    	isOffset="<%= true %>"
	    export="offset,currentPageNumber=pageNumber"
	    scope="request">
		<%-- keep track of preference --%>

	  <%
    	  for (int i = offset.intValue(),
	    	     l = Math.min(i + maxPageItems, acpms.size());
		     i < l; i++)
		   {
	        Proveedor_Acpm pacpm = (Proveedor_Acpm) acpms.elementAt(i);%>
		<pg:item>
		<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand">
    		<td class="bordereporte"><%=pacpm.getDstrct()%> </td>
		    <td class="bordereporte"><%=pacpm.getCity_code()%></td>
		    <td class="bordereporte"><%=pacpm.getNit()%> </td>
		    <td class="bordereporte"><%=pacpm.getNombre()%></td>
		    <td class="bordereporte"><%=pacpm.getCodigo()%></td>
		    <td class="bordereporte">
			<%if(pacpm.getTipo().equals("E")){%>
				Efectivo
			<%}else{%>
				Gasolina
			<%}%></td>
    		<td class="bordereporte">
   		   	<%if(pacpm.getTipo().equals("E")){%>
      			<%=pacpm.getMax_e()%><%=pacpm.getMoneda()%>
      		<%}else{%>
      			<%=pacpm.getValor()%><%=pacpm.getMoneda()%>
      		<%}%>
     		</td>
    	</tr> 
  		</pg:item>
  		<%}%>
	  	<tr class="filagris">
 	   		<td height="30" colspan="7" nowrap><pg:index>
				<jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
				</pg:index>
			</td>
  		</tr>
  		</pg:pager>
	  </table>
	</td>
  </tr>
</table>
<br>  
  
<a href="<%=CONTROLLER%>?estado=Menu&accion=Enviar&numero=39" class="Letras">Volver a Realizar la consulta...</a>
</body>
</html>



