<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
  <script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Consultar Remesas"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
    Vector remesas = model.remesaService.getRemesas();
	if (remesas.size()<1)
		response.sendRedirect(CONTROLLER+"?estado=Menu&accion=Enviar&numero=36&mensaje= La busqueda no arrojo resultados");
%>

<table width="100%" border="2" align="center">
  <tr>
    <td class="barratitulo">
	  <table width="100%">
        <tr>
          <td width="373" class="subtitulo1">Lista Remesas</td>
          <td width="427" ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
	  <table width="100%" border="1" cellpadding="4" cellspacing="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
          <tr class="tblTitulo" align="center">
    	  <td width="64" nowrap>Remesa</td>
    	  <td width="85" nowrap>Planilla</td>
    	  <td width="257"nowrap>Standart Job</td>
    	  <td width="177"nowrap>Cliente</td>
    	  <td width="121"nowrap>Origen OT</td>
    	  <td width="121"nowrap>Destino OT</td>
    	  <td width="121"nowrap>Origen OC</td>
    	  <td width="121"nowrap>Destino OC</td>    	  
    	  <td width="118"nowrap>Fecha Remesa</td>
      </tr>
  <pg:pager
    items="<%=remesas.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
<%-- keep track of preference --%>

  <%
      for (int i = offset.intValue(),
	         l = Math.min(i + maxPageItems, remesas.size());
	     i < l; i++)
	{
        Remesa remesa = (Remesa) remesas.elementAt(i);%>
		<pg:item>
 	 <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'
		onClick="window.open('<%=CONTROLLER%>?estado=ConsultaOCNormal&accion=Buscar&tipo=1&distrito=TSP&numeroOC=<%=remesa.getOc()%>&numeroOT=<%=remesa.getNumrem()%>&planilla=<%=remesa.getOc()%>' ,'M','status=yes,scrollbars=no,width=780,height=650,resizable=yes');" title="Consultar Planilla Remesa..." style="cursor:hand">
       <td height="30"  class="bordereporte" nowrap  ><%= ( remesa.getNumrem()!=null )?remesa.getNumrem():""%></td>
       <td height="30"  class="bordereporte" nowrap ><%= ( remesa.getOc() != null )?remesa.getOc():""%></td>
       <td class="bordereporte"><%= ( remesa.getDescripcion() != null )?remesa.getDescripcion():""%></td>
       <td class="bordereporte"><%= ( remesa.getCliente() != null )?remesa.getCliente():""%></td>
       <td class="bordereporte" align="center"><%= ( remesa.getOriRem() != null )?remesa.getOriRem():""%></td>
       <td class="bordereporte" align="center"><%= ( remesa.getDesRem() != null )?remesa.getDesRem():""%></td>
       <td class="bordereporte" align="center"><%= ( remesa.getOrigenOC() != null )?remesa.getOrigenOC():""%></td>
       <td class="bordereporte" align="center"><%= ( remesa.getDestinoOC() != null )?remesa.getDestinoOC():""%></td>
       <td class="bordereporte" align="center"><%= ( remesa.getFecRem() != null)?String.valueOf(remesa.getFecRem()):""%></td>
     </tr> 	 
  </pg:item>
  <%}
  %>
  <tr>
 	   <td height="30" colspan="7" nowrap><pg:index>
	<jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
	</pg:index></td>
  </tr>
  </pg:pager>
</table>
</td>
</tr>
</table>
<br>  
<table width="100%" align="center">
  <tr>
     <img src="<%=BASEURL%>/images/botones/regresar.gif" name="regresar" id="regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="history.back();" style='cursor:hand'>
  </tr>
</table>  
</div>
</body>
</html>



