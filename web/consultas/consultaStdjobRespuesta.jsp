<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>Buscar Standard</title>
<script language="javascript" src="../js/validar.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Consultar Standard Job"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%
	String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
    Vector stdjob = model.stdjobdetselService.getStdjob();
	if (stdjob.size()<1)
		response.sendRedirect(CONTROLLER+"?estado=Menu&accion=Enviar&numero=35&mensaje=");
%>
<table width="100%"  border="2">
  <tr>
    <td><table width="100%">
      <tr>
        <td width="373" class="subtitulo1">&nbsp;Listado de Standard </td>
        <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
      </tr>
    </table>
      <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4" class="Letras">
        <tr class="tblTitulo" >
          <td width="81" align="center" nowrap>STD NO.</td>
          <td width="173" align="center" nowrap>DESCRIPCION</td>
          <td width="152" align="center" nowrap>CLIENTE</td>
          <td width="95" align="center" nowrap>ORIGEN</td>
          <td width="115" align="center" nowrap>DESTINO</td>
        </tr>
        <pg:pager
    items="<%=stdjob.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
        <%-- keep track of preference --%>
        <%
      for (int i = offset.intValue(),
	         l = Math.min(i + maxPageItems, stdjob.size());
	     i < l; i++)
	{
        Stdjobdetsel std= (Stdjobdetsel) stdjob.elementAt(i);%>
        <pg:item>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand" 
				title="Mostrar Planilla..." onClick="window.open('<%=CONTROLLER%>?estado=Consulta&accion=StdJob&cliente=<%=std.getCliente()%>&sj=<%=std.getSj()%>','','scroll=no')" >
          <td nowrap class="bordereporte"><%=std.getSj()%></td>
          <td nowrap class="bordereporte"><%=std.getSj_desc()%></td>
          <td nowrap class="bordereporte"><%=std.getCliente()%></td>
          <td nowrap class="bordereporte"><%=std.getOrigin_name()%></td>
          <td nowrap class="bordereporte"><%=std.getDestination_name()%></td>
        </tr>
        </pg:item>
        <%}
  %>
        <tr bgcolor="#FFFFFF" class="style7">
          <td colspan="5" nowrap> <pg:index>
            <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>     
    </pg:index> </td>
        </tr>
        </pg:pager>
      </table>    </td>
  </tr>
</table>
<br>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr align="left">
    <td><img src="<%=BASEURL%>/images/botones/regresar.gif" alt="Volver a Realizar la consulta..." name="c_regresar" id="c_regresar" style="cursor:hand " onClick="window.location = '<%=CONTROLLER%>?estado=Menu&accion=Enviar&numero=35';" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
  </tr>
</table>
</div>
</body>
</html>



