<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<link href="css/letras.css" rel="stylesheet" type="text/css">

<title>Buscar Proveedor De ACPM</title>
<script language="javascript" src="js/validar.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body ONLOAD="redimensionar();" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Proveedor De ACPM"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="forma" method="post" action="<%=CONTROLLER%>?estado=Consultar&accion=ProveedorAcpm">
  <table width="400" border="2" align="center">
    <tr>
        <td>
            <table width="100%" align="center"  >
                <tr>            
                    <td class="subtitulo1" width="50%">Datos Del Proveedor</td>
                    <td class="barratitulo" width="50%"><img src="<%=BASEURL%>/images/titulo.gif"></img></td>                    
                </tr>      
				<tr class="fila">
				  <td nowrap>Ciudad:</td>
				  <td nowrap><input name="ciudad" type="text" class="textbox" id="ciudad" maxlength="40"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img></td>
			  </tr>
				<tr class="fila">
				  <td nowrap>Nombre:</td>
				  <td nowrap><input name="nombre" type="text" class="textbox" id="nombre"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img></td>
			  </tr>
				<tr class="fila">
				  <td nowrap>Nit.</td>
				  <td nowrap><input name="nit" type="text" class="textbox" id="nit"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img></td>
			  </tr>
				<tr class="fila">
				  <td nowrap>Tipo de servicio:</td>
				  <td nowrap>
					  <select name="tipo_servicio" class="textbox" id="tipo_servicio">
						<option value="">Cualquiera</option>
						<option value="E">Efectivo</option>
						<option value="G">Gasolina</option>
					  </select>
					  <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img>
				  </td>
			  </tr>								  
		  </table>
		</td>
		</tr>
  </table>
  <br>
  <table align="center" width="400">
  	<tr>
		<td align="center">
			<img title='Buscar' src="<%= BASEURL %>/images/botones/buscar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="TCamposLlenos();"></img>
			<img title='Cancelar' src="<%= BASEURL %>/images/botones/cancelar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick='forma.reset();'></img>
			<img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="window.close()"></img>
		</td>
		
	</tr>
  </table>
</form>
<%if(request.getParameter("mensaje")!=null){
	out.print("<table width=450 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=350 align=center class=mensajes>No se encontraron resultados</td><td width=100><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");            
}%>

<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</div>
</body>
</html>

