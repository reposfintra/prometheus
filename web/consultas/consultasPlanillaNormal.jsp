<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Buscar Planillas</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
		<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
			<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Consultar Planilla Carga Varia"/>
		</div>
		<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<% String numpla = (request.getParameter("numpla")!=null)?request.getParameter("numpla"):"";%>
<form name="form1" id="form1" method="post" action="<%=CONTROLLER%>?estado=Consultar&accion=Planilla&normal=ok">  
<table width="458" border="2" align="center">
    <tr>
      <td><table width="100%" align="center"  class="tablaInferior">
        <tr class="fila">
          <td colspan="2" align="left" class="subtitulo1">&nbsp;Consulta De Planillas </td>
          <td colspan="2" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
        <tr class="fila">
          <td align="left" ><strong>Nro. Planilla</strong></td>
          <td align="left" ><input name="numpla" class="textbox" type="text" id="numpla" size="12" maxlength="10" value="<%=numpla%>"></td>
          <td valign="middle"><strong>Placa </strong></td>
          <td valign="middle"><input name="placa" type="text" class="textbox" id="placa2" size="12" maxlength="10"></td>
        </tr>
        <tr class="fila" id="cantidad">
          <td align="left" ><strong>Propietario  </strong></td>
          <td align="left" ><input name="nit" type="text" class="textbox" id="nit2" size="12" maxlength="15"></td>
          <td valign="middle">
              <span class="Letras"><strong>Conductor</strong></span></td>
          <td valign="middle"><input name="cedcon" type="text" class="textbox" id="cedcon2" size="12" maxlength="15"></td>
        </tr>		
        <tr class="fila">
          <td colspan="4" align="left" ><div align="center"><strong>Fecha del Despacho</strong></div></td>
          </tr>		    
        <tr class="fila">
		  <td width="85" align="left" ><strong> Desde</strong></td>
          <td width="137" align="left" ><input name="fechaini" type="text" id="fechaini" size="10" class="textbox" readonly>
            <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fechaini);return false;" HIDEFOCUS> <img src="js/Calendario/cal.gif" width="16" height="16"
               border="0" alt="De click aqu&iacute; para escoger la fecha"></a></td>
          <td width="79" valign="middle"><strong>Hasta            </strong></td>
          <td width="121" valign="middle"><input name="fechafin" type="text" id="fechafin" size="10" class="textbox" readonly>
            <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fechafin);return false;" hidefocus> <img src="js/Calendario/cal.gif" width="16" height="16"
               border="0" alt="De click aqu&iacute; para escoger la fecha"></a></td>
        </tr>
      </table></td>
    </tr>
  </table>
<p>
<div align="center"><img src="<%=BASEURL%>/images/botones/buscar.gif" style="cursor:hand" title="Buscar Planillas" name="buscar"  onClick="return validarConsulta(form1);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/salir.gif"  name="regresar" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
</p> 
</form>
<%if(request.getParameter("mensaje")!=null){%>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
<%}%>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>