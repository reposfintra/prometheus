<!--
- Autor : Ing. Jose de la rosa
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra los movimientos de anticipo
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
 
<html>
<head>
<title>Movimientos de Anticipos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
</head>
<%-- Inicio Body --%>
<body>
<% 	String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
	/*Planillas  planilla = model.PlanillasSvc.getPlanilla() ;
	Remesa rem = model.remesaService.getRemesa();*/
	List listaMov = model.RemisionSvc.getMovimiento();
	if(listaMov!=null || listaMov.size()>0){%>
<%-- Tabla del detalle de la planilla --%>	
<table width="1211" border="2" align="center">
    <tr>
      <td>
	  <table width="100%" align="center">
    <%	   Iterator iter=listaMov.iterator();
           String pla = "";
		   if(iter.hasNext()){		   	   
			   AnticipoPlanilla  movi = (AnticipoPlanilla) iter.next(); 
               pla = movi.getPlanilla();
           } 
    %>
              <tr>
                <td width="373" class="subtitulo1">&nbsp;Movimientos de Anticipos de la Planilla <%=pla%></td>
                <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
        </table>
		  <table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
          <tr class="tblTitulo" align="center">
          <td width="5%" align="center">Agencia</td>
          <td width="8%"  align="center">Tipo Documento </td>
          <td width="8%" align="center">Cheque</td>
          <td width="3%" align="center">Item</td>
          <td width="7%" align="center">Concepto</td>
          <td width="6%" align="center">Fecha Anticipo</td>
	  <td width="7%" align="center">Valor Cheque/Anticipo ML</td>
          <td width="8%" align="center">Valor Cheque/Anticipo</td>
          <td width="6%" align="center">Moneda Cheque/Anticipo</td>
          <td width="8%" align="center">Banco</td>
          <td width="8%" align="center">Sucursal</td>
          <td width="20%" align="center">Descripcion</td>
          <td width="6%" align="center">Estado</td>
          </tr>
        <pg:pager
         items="<%=listaMov.size()%>"
         index="<%= index %>"
         maxPageItems="<%= maxPageItems %>"
        maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
        <%-- keep track of preference --%>
	  <% 
	       int i = 0;
		   Iterator itt=listaMov.iterator();
		   while(itt.hasNext()){
		   	   i++;
			   AnticipoPlanilla  mov = (AnticipoPlanilla) itt.next(); %>
				<pg:item>
				<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
				  <td width="6%" class="bordereporte" align="center">&nbsp;<%=mov.getAgencia()%></td>
				  <td width="11%" class="bordereporte" align="center">&nbsp;<%=mov.getTipoDocumento()%></td>  
				  <td width="8%" class="bordereporte" align="center">&nbsp;<%=mov.getDocumento()%></td>
				  <td width="4%" class="bordereporte" align="center">&nbsp;<%=mov.getItem()%></td>
				  <td width="14%" class="bordereporte" align="center">&nbsp;<%=mov.getConcepto()%></td>
				  <td width="3%" class="bordereporte" align="center">&nbsp;<%=mov.getDate_doc()%></td>
				  <td align="center" class="bordereporte">&nbsp;<%=com.tsp.util.Util.customFormat(mov.getValor())%></td>
				  <td width="9%" class="bordereporte" align="center">&nbsp;<%=com.tsp.util.Util.customFormat(mov.getValor_for())%></td>
				  <td width="6%" class="bordereporte" align="center">&nbsp;<%=mov.getMoneda()%></td>
				  <td width="8%" class="bordereporte" align="center">&nbsp;<%=mov.getBanco()%></td>
				  <td width="9%" class="bordereporte" align="center">&nbsp;<%=mov.getCuenta()%></td>
				  <td width="18%" class="bordereporte" align="center">&nbsp;<%=mov.getDescripcion()%></td>
				  <td width="6%" class="bordereporte" align="center">&nbsp;<%=mov.getReg_status ()%></td>
				</tr>
				</pg:item>
        <%}%>
        <tr class="pie">
          <td td height="20" colspan="13" nowrap align="center">
		   <pg:index>
            <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
           </pg:index> 
	      </td>
        </tr>
        </pg:pager>
      </table></td>
    </tr>
</table>
<%}
 else { %>
  <br>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <%}%>
	<br>
<%--tabla para el boton de salir--%>
<table width="1000" border="0" align="center">
	<tr>
		<td>
			<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
		</td>
	</tr>
</table>
</body>
</html>
