<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<link href="css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">

<title>Buscar Ciudades</title>
<script language="javascript" src="<%=BASEURL%>/js/validar.js"></script>
<script language="javascript" src="<%=BASEURL%>/js/boton.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body onLoad="form1.nit.focus();">

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=Buscar Remitente Ciudades"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top:100px; overflow: scroll;">
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Ciudad&accion=Search" >
<table border="2" align="center" width="488">
  <tr>
    <td width="476">
	<table width="99%"  align="center"   >
  <tr>
    <td width="227" height="30"  class="subtitulo1"><p align="left">Busque la Ciudad por codigo o nombre </p></td>
    <td width="229"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
  <table width="473" align="center" class="Letras">
    <tr >
      <td nowrap><table width="100%" border="0" cellpadding="0" cellspacing="0" class="fila">
        <tr>
          <td height="19"><strong>Escriba el codigo o nombre</strong> </td>
          <td><input name="var" type="text" class="textbox" id="nit" size="35" maxlength="30"></td>
        </tr>
      </table>        </td>
    </tr>
  </table>
  </td>
  </tr>
  </table>
  <div align="center"><br>
    <img src="<%=BASEURL%>/images/botones/buscar.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="form1.submit();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
  </div>
</form>

<%if(request.getParameter("msg")!=null){%>
	<table border="2" align="center">
      <tr>
        <td height="45"><table width="410" height="41" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
              <td width="282" height="35" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
              <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
              <td width="78">&nbsp;</td>
            </tr>
        </table></td>
      </tr>
    </table>
	<%}%>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</body>
</html>

