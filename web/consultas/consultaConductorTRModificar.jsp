<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">

<title>Lista de nits</title>
<script language="javascript" src="../js/validar.js"></script>
<script language="javascript" src="../js/reporte.js"></script>
<script language="javascript" src="../js/boton.js"></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Consultas Propietario y/o Conductor"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 20;
    int maxIndexPages = 10;
    Vector nits = model.conductorService.getConductores();
    String hoja = (request.getParameter("registroT")!=null)?request.getParameter("registroT"):""; 
	if (nits.size()<1)
		response.sendRedirect(CONTROLLER+"?estado=Menu&accion=Enviar&numero=37&mensaje=");
%>
<table border="2" align="center" width="474">
  <tr>
    <td>
	<table width="99%"  align="center"   >
  <tr>
    <td width="227" height="24"  class="subtitulo1"><p align="left">Lista de nits </p></td>
    <td width="229"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%" border="1" align="center" cellpadding="2" cellspacing="1" bordercolor="#CCCCCC" class="Letras">
  <tr class="tblTitulo">
    <td width="151" nowrap><span class="style7 Estilo1">CEDULA</span></td>
    <td width="361"><div align="center" class="style7 Estilo1">
        <div align="left">NOMBRE</div>
    </div></td>
  </tr>
  <pg:pager
    items="<%=nits.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
  <%-- keep track of preference --%>
  <%
      for (int i = offset.intValue(),
	         l = Math.min(i + maxPageItems, nits.size());
	     i < l; i++)
	{
        Conductor cond = (Conductor) nits.elementAt(i);%>
  <pg:item>
  <tr nowrap   class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand"  title="Mostrar Conductor..." <%if(hoja.equals("ok")) { out.print("onclick='copiarRegistroT("+cond.getCedula()+")'");  }else {%>onClick="copiarPropietario('<%=cond.getCedula()%>');"<%}%>  >
    <td class="bordereporte"><%=cond.getCedula()%></td>
    <td class="bordereporte"><%=cond.getNombre()%></td>
  </tr>
  </pg:item>
  <%}
  %>
  <tr  class="filagris">
    <td height="30" colspan="2" nowrap align="center"><pg:index>
      <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
    </pg:index></td>
  </tr>
  </pg:pager>
</table>
</tr>
</td>
</table>
<br>  
</div>  
</body>
</html>



