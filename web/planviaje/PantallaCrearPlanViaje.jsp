<%@page contentType="text/html" import="java.util.*, com.tsp.operation.model.beans.*" session = "true"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@include file="/WEB-INF/InitModel.jsp" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>.: Plan de Viaje de Conductores :.</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="css/EstilosFiltros.css" rel="stylesheet" type="text/css">
<script language="javascript" src="js/PlanViaje.js"></script>
<link href="js/jscalendar/calendar-system.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jscalendar/calendar.js"></script>
<script type="text/javascript" src="js/jscalendar/lang/calendar-es.js"></script>
<script type="text/javascript" src="js/jscalendar/calendar-setup.js"></script>
</head>

<body>

<!--FORMULARIO-->
<input:form name="frmplanvj" method="post" action="controller?estado=PlanViaje&accion=Create&cmd=show" bean="planVj" attributesText="onsubmit='return validarPlanviaje()'">
    <table  border = '2' align = 'center'>
        <tr>
        <td>
        <table>
              <tr align='center' class='TableHeaderDecoration'><td colspan='4'>DATOS DEL VIAJE</td></tr>
              <tr class='TableSubHeaderDecoration'>
              <td>Distrito :</td>
              <td colspan='3'><input:text name="cia" attributesText="size='10' readonly"/></td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td>Planilla :</td>
              <td><input:text name="planilla" attributesText="size='10' readonly"/></td>
              <td align='left'>Fecha y hora de Salida :</td>
              <td><input:text name="fecha" attributesText="size='13' readonly"/>
              <img name="popcal" id="popcal" align="absmiddle" src="js/jscalendar/img.gif">
              </td>
              </tr>
              
              <tr class='TableSubHeaderDecoration'>
              <td>Placa :</td>
              <td><input:text name="placa" attributesText="size='10' readonly"/></td>
              <td>Placa Unidad de Carga :</td>
              <td><input:text name="trailer" attributesText="size='10' maxlength='10'"/></td>
              </tr> 

              <tr class='TableSubHeaderDecoration'>
              <td>Producto :</td>
              <td><input:text name="producto" attributesText="size='50' maxlength='50'"/></td>
              <td>Ruta (Via) :</td>
              <td>
                 <%
                   TreeMap vias = model.viaService.getCbxVias();
                 %>
                  <input:select name="ruta" options="<%=vias%>"/>
              </td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td align='left'>Contenedor(es) :</td>
              <td><input:text name="contenedor" attributesText="size='50' maxlength='50'"/></td>
              <td align='left'>Tipo de Carga :</td>
              <td>
                 <input:hidden name="codtipocarga"/> 
                 <input:text name="tipocarga" attributesText="size='25' maxlength='50' readonly"/>
              </td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td align='left'>Destinatario :</td>
              <td><input:text name="destinatario" attributesText="size='40' maxlength='40'"/></td>
              <td align='left'>Compromiso Retorno:</td>
              <td><input:checkbox name="retorno" value="S"/></td>
              </tr>
              
              <tr class='TableHeaderDecoration' align='center'><td colspan='4'>DATOS DEL CONDUCTOR</td></tr>
              <tr class='TableSubHeaderDecoration'>
              <td>C&eacute;dula :</td>
              <td colspan='3'><input:text name="cedcon" attributesText="size='15' readonly"/></td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td>Nombre :</td>
              <td><input:text name="nomcon" attributesText="size='50' maxlength='60'"/></td>
              <td>Tel&eacute;fono :</td>
              <td><input:text name="phonecon" attributesText="size='20' maxlength='20'"/></td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td>Direcci&oacute;n</td>
              <td ><input:text name="dircon" attributesText="size='50' maxlength='60'"/></td>
              <td>Ciudad :</td>
              <td>
              <%TreeMap ciudades = model.ciudadService.getCiudades();%>
              <input:select name="ciucon" options="<%=ciudades%>"/>
              </td>
              </tr>
              
              <tr class='TableHeaderDecoration' align='center'><td colspan='4'>A L I M E N T A C I O N</td></tr>
              <tr class='TableSubHeaderDecoration'>
              <td></td>
              <td>Lugar</td>
              <td colspan='2'>Tel&eacute;fono</td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td>Puesto Nro 1 :</td>
              <td><input:text name="al1" attributesText="size='30' maxlength='30'"/></td>
              <td colspan='2'><input:text name="at1" attributesText="size='20' maxlength='20'"/></td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td>Puesto Nro 2 :</td>
              <td><input:text  name="al2" attributesText="size='30' maxlength='30'"/></td>
              <td colspan='2'><input:text  name="at2" attributesText="size='20' maxlength='20'"/></td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td>Puesto Nro 3 :</td>
              <td><input:text  name="al3" attributesText="size='30' maxlength='30'"/></td>
              <td colspan='2'><input:text  name="at3" attributesText="size='20' maxlength='20'"/></td>
              </tr>
              
              <tr class='TableHeaderDecoration' align='center'><td colspan='4'>P E R N O C T A C I O N</td></tr>
              <tr class='TableSubHeaderDecoration'>
              <td></td>
              <td>Lugar</td>
              <td colspan='2'>Tel&eacute;fono</td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td>Puesto Nro 1 :</td>
              <td><input:text  name="pl1" attributesText="size='30' maxlength='30'"/></td>
              <td colspan='2'><input:text  name="pt1" attributesText="size='20' maxlength='20'"/></td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td>Puesto Nro 2 :</td>
              <td><input:text  name="pl2" attributesText="size='30' maxlength='30'"/></td>
              <td colspan='2'><input:text  name="pt2" attributesText="size='20' maxlength='20'"/></td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td>Puesto Nro 3 :</td>
              <td><input:text  name="pl3" attributesText="size='30' maxlength='30'"/></td>
              <td colspan='2'><input:text  name="pt3" attributesText="size='20' maxlength='20'"/></td>
              </tr>
              
              <tr align='center' class='TableHeaderDecoration'><td colspan='4'>EQUIPOS DE COMUNICACION</td></tr>
              <tr class='TableSubHeaderDecoration'>
              <td>Radio</td><td><input:text  name="radio" attributesText="size='20' maxlength='20'"/></td>
              <td>Celular</td><td><input:text  name="celular" attributesText="size='20' maxlength='20'"/></td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td>Avantel</td><td><input:text  name="avantel" attributesText="size='20' maxlength='20'"/></td>
              <td>Tel&eacute;fono</td><td><input:text  name="telefono" attributesText="size='20' maxlength='20'"/></td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td>Cazador</td><td><input:text  name="cazador" attributesText="size='20' maxlength='20'"/></td>
              <td>Movil</td><td><input:text  name="movil" attributesText="size='20' maxlength='20'"/></td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td align = 'left'>Otro :</td>
              <td colspan='3'><input:text  name="otro" attributesText="size='40' maxlength='40'"/></td>
              </tr>
              
              <tr align='center' class='TableHeaderDecoration'><td colspan='4'>P A R Q U E A D E R O S</td><tr>
              <tr class='TableSubHeaderDecoration'>
              <td></td>
              <td>Lugar</td>
              <td colspan='2'>Tel&eacute;fono</td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td align = 'left'>En Origen :</td>
              <td><input:text  name="qlo" attributesText="size='30' maxlength='30'"/></td>
              <td colspan='2'><input:text  name = "qto" attributesText="size='20' maxlength='20'"/></td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td align = 'left'>En Destino :</td>
              <td><input:text  name="qld" attributesText="size='30' maxlength='30'"/></td>
              <td colspan='2'><input:text  name="qtd" attributesText="size='20' maxlength='20'"/></td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td align = 'left'>Puesto Nro 1 :</td>
              <td><input:text  name="ql1" attributesText="size='30' maxlength='30'"/></td>
              <td colspan='2'><input:text  name="qt1" attributesText="size='20' maxlength='20'"/></td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td align = 'left'>Puesto Nro 2 :</td>
              <td><input:text  name="ql2" attributesText="size='30' maxlength='30'"/></td>
              <td colspan='2'><input:text  name="qt2" attributesText="size='20' maxlength='20'"/></td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td align = 'left'>Puesto Nro 3 :</td>
              <td><input:text  name="ql3" attributesText="size='30' maxlength='30'"/></td>
              <td colspan='2'><input:text  name="qt3" attributesText="size='20' maxlength='20'"/></td>
              </tr>
              
              <tr align='center' class='TableHeaderDecoration'><td colspan='4'>T A N Q U E O</tr></td>
              <tr class='TableSubHeaderDecoration'>
              <td></td>
              <td>Lugar</td>
              <td colspan='2'>Tel&eacute;fono</td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td>Puesto Nro 1 :</td>
              <td><input:text  name="tl1" attributesText="size='30' maxlength='30'"/></td>
              <td colspan='2'><input:text  name="tt1" attributesText="size='20' maxlength='20'"/></td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td>Puesto Nro 2 :</td>
              <td><input:text  name="tl2" attributesText="size='30' maxlength='30'"/></td>
              <td colspan='2'><input:text  name="tt2" attributesText="size='20' maxlength='20'"/></td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td>Puesto Nro 3 :</td>
              <td><input:text  name="tl3" attributesText="size='30' maxlength='30'"/></td>
              <td colspan='2'><input:text  name="tt3" attributesText="size='20' maxlength='20'"/></td>
              </tr>
              
              <tr class='TableHeaderDecoration' align='center'><td colspan='4'>DATOS DE UN FAMILIAR</td></tr>
              <tr class='TableSubHeaderDecoration'>
              <td>Nombre :</td>
              <td colspan='3'><input:text  name="nomfam" attributesText="size='50' maxlength='50'"/></td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td>Tel&eacute;fono :</td>
              <td colspan='3'><input:text  name="phonefam" attributesText="size='20' maxlength='20'"/></td>
              </tr>
              
              <tr align='center' class='TableHeaderDecoration'><td colspan='4'>DATOS GENERALES DEL PROPIETARIO</td></tr>
              <tr class='TableSubHeaderDecoration'>
              <td align = 'left'>C&eacute;dula :</td>
              <td colspan='3'><input:text  name="nitpro" attributesText="size='15' readonly"/></td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td>Nombre :</td>
              <td><input:text  name="nompro" attributesText="size='50' maxlength='60'"/></td>
              <td>Tel&eacute;fono :</td>
              <td><input:text  name="phonepro" attributesText="size='20' maxlength='20'"/></td>
              </tr>
              <tr class='TableSubHeaderDecoration'>
              <td>Direcci&oacute;n</td>
              <td><input:text  name="dirpro" attributesText="size='50' maxlength='60'"/></td>
              <td>Ciudad :</td>
              <td><input:select name="ciupro" options="<%=ciudades%>"/></td>
              </tr>
              
              <tr align='center' class='TableHeaderDecoration'><td colspan='4'>C O M E N T A R I O S</td></tr>
              <tr class='TableSubHeaderDecoration' align='center'>
              <td colspan='4'><input:text  name="comentario1" attributesText="size='100' maxlength='100'"/></td>
              </tr>
              <tr class='TableSubHeaderDecoration' align='center'>
              <td colspan='4'><input:text  name="comentario2" attributesText="size='100' maxlength='100'"/></td>
              </tr>
              <tr class='TableSubHeaderDecoration' align='center'>
              <td colspan='4'><input:text  name="comentario3" attributesText="size='100' maxlength='100'"/></td>
              </tr>
              
              <tr class='TableHeaderDecoration' align='center'>
                <td colspan='4'>
                    <input type='submit' value='CREAR PLAN DE VIAJE' name='btncrear' class='botones'>
                </td>
              </tr>
       </table>
       </td>
       </tr>
    </table>
</input:form>
<script type="text/javascript">
    Calendar.setup(
    {
    inputField : "fecha", // ID of the input field
    ifFormat : "%Y-%m-%d %H:%M", // the date format
    button : "popcal", // ID of the button
    showsTime : true,
    timeFormat : "24"
    }
    );
</script>
  </BODY>
</HTML>
