<%@page contentType="text/html"%>
<%@page import="java.util.*, com.tsp.operation.model.*, com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="css/EstilosFiltros.css" rel="stylesheet" type="text/css">

<script language='JavaScript'>
<!--
function validate ()
{
    if (document.forms.numpla.planilla.value == "") 
    {
       alert("Digite numero de planilla");
       document.forms.numpla.planilla.focus();
       return false;
    }

  return true;
} 
//-->
</script>
<link href="js/jscalendar/calendar-system.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="js/jscalendar/calendar.js"></script>
<script type="text/javascript" src="js/jscalendar/lang/calendar-es.js"></script>
<script type="text/javascript" src="js/jscalendar/calendar-setup.js"></script>
</head>
<body onload = 'document.numpla.planilla.focus()'>
<table class='TableRowDecoration' align='center'>
     <tr>
        <td>
           <a href='controller?estado=Caravana&accion=GetInfoComun'><img src='images/regcaravanas.gif'></a>
        </td>
        <td>
           <a href='controller?estado=Caravana&accion=Update'><img src='images/modcaravanas.gif'></a>
        </td>
     </tr>
</table>

<table border='1' align='center'>
    <tr>
    <td>
    <table>
    <tr class='TableHeaderDecoration'>
    <td align = center colspan='5'>
        Registrar/Consultar por planilla
    </td>
    </tr>
    <tr class='TableSubHeaderDecoration'>
        <input:form name="numpla" method="post" action="controller?estado=PlanViaje&accion=Search&cmd=show" attributesText="onsubmit='return validate()'">
                    <th align = 'left'>Distrito :</th>
                    <td>
                       <%
                         Usuario usuario = (Usuario)session.getAttribute("Usuario");
                         TreeMap cia = usuario.getListCia();
                        %>
                       <input:select name="cia" options="<%=cia%>" default="FINV"/>
                    </td>
                    <th align = 'left'>Planilla Nro :</th>
                    <td><input:text name="planilla" attributesText="size = '10' maxlength='10'"/></td>
                    <td><input type = 'submit' name='btnenviar' value = 'Aceptar' class='botones'></td>
 
        </input:form>
    </tr>
    </table>
    </td>
    </tr>
</table>

<% String mensaje =  (String)request.getAttribute("mensaje");
   if ( (mensaje != null) && (!mensaje.equals("")) ){ %>
        <center>
        <fieldset style='width=400; background=f5f5cd; align=center;' >
               <p class='MessageDecoration'>
                  <%=mensaje%>
               </p>  
        </fieldset>
        </center>
<%}%>
<hr>

<!-- CONSULTA POR AGENCIA Y FECHA DE MODIFICACION -->
<table border='1' cellspacing='5' align='center'>
<tr>
   <td>
       <table>
       <tr class='TableHeaderDecoration'>
       <td colspan='2'>
          Consulta por Agencia y Fecha de Modificaci&oacute;n
       </td>
       <input:form name="frmagencia" method="post" action="controller?estado=PlanViaje&accion=QryAg&cmd=show"> 
       <tr class='TableSubHeaderDecoration'>
          <th align=left>Agencia : </th>
          <td> 
             <%TreeMap agencias = model.agenciaService.searchAgencia();%>
             <input:select name='agencia' options="<%=agencias%>"/> 
          </td>
       </tr>

            <!--Parametro de fecha inicial -->
        <tr class='TableSubHeaderDecoration'>
        <th>Fecha Inicial : </th>
        <td> 
        <input:text name='fechaini' attributesText="id='fechaini' readonly"/>
        <img name="popcal1" id="popcal1" align="absmiddle" src="js/jscalendar/img.gif">
        </td>
        </tr> 
        
        <tr class='TableSubHeaderDecoration'>
        <th>Fecha Final : </th>
        <td> 
        <input:text name='fechafin' attributesText="id='fechafin' readonly"/>
        <img name="popcal2" id="popcal2" align="absmiddle" src="js/jscalendar/img.gif">
        </td>
        </tr>
        
        <tr class='TableSubHeaderDecoration'>
            <td colspan='2'>
            <input type='submit' name='btnenviar' value='Aceptar' class='botones'>
            </td>
        </tr>
       </input:form>
       <script type="text/javascript">
            Calendar.setup(
            {
            inputField : "fechaini", // ID of the input field
            ifFormat : "%Y-%m-%d", // the date format
            button : "popcal1" // ID of the button
            }
            );
        </script>
        <script type="text/javascript">
            Calendar.setup(
            {
            inputField : "fechafin", // ID of the input field
            ifFormat : "%Y-%m-%d", // the date format
            button : "popcal2" // ID of the button
            }
            );
        </script>
       </table>
       </td>

       <td>
       <table>
       <tr class='TableHeaderDecoration'>
       <td colspan='2'>
          Consulta por Zona y Fecha de Modificación
       </td>
       <input:form name="frmzona" method="post" action="controller?estado=PlanViaje&accion=QryZn&cmd=show"> 
       <tr class='TableSubHeaderDecoration'>
          <th align=left>Zonas : </th>
          <td> 
             <%TreeMap zonas = model.agenciaService.searchZona();%>
             <input:select name='zona' options="<%=zonas%>"/> 
          </td>
       </tr>

            <!--Parametro de fecha inicial -->
        <tr class='TableSubHeaderDecoration'>
        <th>Fecha Inicial : </th>
        <td> 
        <input:text name="fechaini" attributesText="id='fechainizn' readonly"/>
        <img name="popcal3" id="popcal3" align="absmiddle" src="js/jscalendar/img.gif">
        </td>
        </tr> 
        
        <tr class='TableSubHeaderDecoration'>
        <th>Fecha Final : </th>
        <td> 
        <input:text name="fechafin" attributesText="id='fechafinzn' readonly"/>
        <img name="popcal4" id="popcal4" align="absmiddle" src="js/jscalendar/img.gif">
        </td>
        </tr>
        
        <tr class='TableSubHeaderDecoration'>
            <td colspan='2'>
            <input type='submit' name='btnenviar' value='Aceptar' class='botones'>
            </td>
        </tr>
       </input:form>
       </table>
       <script type="text/javascript">
            Calendar.setup(
            {
            inputField : "fechainizn", // ID of the input field
            ifFormat : "%Y-%m-%d", // the date format
            button : "popcal3" // ID of the button
            }
            );
        </script>
        <script type="text/javascript">
            Calendar.setup(
            {
            inputField : "fechafinzn", // ID of the input field
            ifFormat : "%Y-%m-%d", // the date format
            button : "popcal4" // ID of the button
            }
            );
        </script>
       </td>
       </tr>
</table>
<!--FIN ADICION ONSULTA POR ZONA-->
<br>

  <table border = '1' align='center' >
  <caption>
     <font color = 'red' face='verdana' size='1'>Para ver plan de viaje debe seleccionar el n&uacute;mero de planilla deseado
     </font>
  </caption>
  <tr bgcolor='blue' class='TableHeaderDecoration'>
  <th colspan='7' align='center'>
      LISTA DE PLANES DE VIAJES REGISTRADOS
  </th>
  </tr>
  <tr bgcolor='blue' class='TableSubHeaderDecoration'>
    <th>Zona</th> 
    <th>Agencia</th>    
    <th>Fecha y Hora Modificaci&oacute;n&nbsp;</th>
    <th>&nbsp;Planilla&nbsp;</th>
    <th>Placa</th>
    <th>Ruta</th>
    <th>Leido</th>
  </tr> 
  
  <%List qryPlanVj = model.planViajeService.getPlanVjQrys();
   Iterator it = qryPlanVj.iterator();
   PlanVjQry planVjQry = null;
   while (it.hasNext()){
       planVjQry = (PlanVjQry)it.next();
	%>
    <tr class="<%=planVjQry.getClase()%>"> 
		<td><%=planVjQry.getDesczona()%></td> 
		<td><%=planVjQry.getNomciu()%></td>
		<td><%=planVjQry.getFecha()%></td>
		<td><a href="#" onClick="window.open('<%=CONTROLLER%>?estado=PlanViaje&accion=Search&cmd=show&planilla=<%=planVjQry.getPlanilla()%>&cia=<%=planVjQry.getCia()%>','','status=no,scrollbars=yes,width=800,height=600,resizable=yes')"> <%=planVjQry.getPlanilla()%> </a></td>
		<td><%=planVjQry.getPlaca()%></td>
		<td><%=planVjQry.getDescripcion()%></td>
		<td><%=planVjQry.getLeido()%></td>		
	</tr> 
   <%}%>
   
  <tr bgcolor = 'white'>
  <td></td><td></td><td></td><td></td><td></td><td></td>
  </tr>  
  </table>
</body>
</html>
