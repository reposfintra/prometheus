f<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%  
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
  String formato ="";
  if(model.tablaGenService.getTblgen()!=null){
		formato =model.tablaGenService.getTblgen().getReferencia()!=null?model.tablaGenService.getTblgen().getReferencia():"";
	}
%>
<html>
<head>
<title>Despacho</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/validar.js">
</script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">

</head>
  <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
<body onLoad="<%if(request.getAttribute("mensajeCli")!=null){%>alert('<%=request.getAttribute("mensajeCli")%>')<%}%>">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Despacho"/>
</div>

 <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
  <br>
  <form name="form1" method="post" action="<%=CONTROLLER%>?estado=BuscarStandard&accion=Colpapel" onSubmit="return verificarStandard();">
    <table width="75%"  border="2" align="center">
	<tr>
	  <td>	     
	   <table width="100%" align="center" class="tablaInferior">
      <tr>
        <th colspan="3"  scope="row"><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="22" colspan=2 class="subtitulo1">Registro de viajes pantalla preliminar </td>
            <td width="368" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
          </tr>
        </table></th>
      </tr>
      <tr class="subtitulo1">
        <th colspan="3"  scope="row"><div align="left">INFORMACION DEL CLIENTE</div></th>
      </tr>
      <tr class="fila">
        <th class="fila" width="143" rowspan="2" scope="row"><div align="left">STANDARD/CLIENTE</div></th>
        <td colspan="2" class="fila">
          <p>
            <span class="Simulacion_Hiper" style="cursor:hand " onClick="window.open('<%=BASEURL%>/consultas/consultasClientes.jsp','','HEIGHT=200,WIDTH=600,SCROLLBARS=YES,RESIZABLE=YES')">Consultar clientes...</span></p>          </td>
      </tr>
      <tr>
        <td colspan="2" class="fila">
          
          <input name="cliente" type="text" class="textbox" id="cliente" onKeyPress="return verificarTecla(event);" maxlength="6">
          <img src="<%=BASEURL%>/images/botones/buscar.gif" width="87" height="21" align="absmiddle" style="cursor:hand" onClick="buscarClient();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
          <input name="clienteR" type="hidden" id="clienteR" value="<%=request.getParameter("cliente")%>">
          <input name="standard_nom" type="hidden" id="standard_nom" value="<%=(String)request.getAttribute("std")%>">
          <input name="remitentes" type="hidden" id="remitentes" value=" ">
          <input name="docinterno" type="hidden" id="docinterno" value=" ">
          <input name="fechadesp" type="hidden" id="fechadesp" value=" ">
          <input name="placa" type="hidden" id="placa" value=" ">
          <input name="destinatarios" type="hidden" id="destinatarios" value=" ">
          <input name="trailer" type="hidden" id="trailer" value=" ">
          <input name="conductor" type="hidden" id="conductor" value=" ">
          <input name="ruta" type="hidden" id="ruta" value=" ">
          <input name="toneladas" type="hidden" id="toneladas" value=" ">
          <input name="valorpla" type="hidden" id="valorpla3" value=" ">
          <input name="anticipo" type="hidden" id="anticipo" value=" ">
          <input type="hidden" name="hiddenField7">
          <input name="precintos" type="hidden" id="precintos" value=" ">
          <input name="observacion" type="hidden" id="observacion" value=" ">
          <input name="click_buscar" type="hidden" id="click_buscar">
          <br>
          <%if(request.getAttribute("cliente")!=null){%>
          <input name="valorpla" type="hidden" id="valorpla">
       
          <table width="100%" border="0" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" class="fila" >
            <tr class="fila"> 
              <td class="fila"><strong><%=(String) request.getAttribute("cliente")%></strong></td>
            </tr>
            <tr class="fila">
              <td height="27" class="fila"><strong>Agencia Due&ntilde;a del Cliente : <%=(String) request.getAttribute("agency")%></strong></td>
            </tr>
          </table>
          <%}%> </td>
      </tr>

      <%if(request.getAttribute("std")==null){%>
      <tr class="fila">
        <td rowspan="2" class="fila" ><strong>RUTA</strong>
        <td width="203" class="fila"><strong>ORIGEN</strong></td>
        <td width="377" class="fila">
          <%TreeMap ciudades = model.stdjobdetselService.getCiudadesOri(); 
	  String corigen="";
	  if(request.getParameter("origen")!=null){
	  	corigen = request.getParameter("origen");
	  }
	  String cdest="";
	  if(request.getParameter("destino")!=null){
	  	cdest = request.getParameter("destino");
	  }
	  %>
        <input:select name="ciudadOri" options="<%=ciudades%>" attributesText="style='width:100%;' onChange='buscarDestinos()'; class='textbox'" default='<%=corigen%>'/> </td>
      </tr>
      <tr class="fila">
        <td class="fila"><strong>DESTINO</strong></td>
        <td class="fila">
          <%TreeMap ciudadesDest = model.stdjobdetselService.getCiudadesDest(); %>
        <input:select name="ciudadDest" options="<%=ciudadesDest%>" attributesText="style='width:100%;' class='textbox'"  default='<%=cdest%>'/> </td>
      </tr>
	   
      <%if(ciudadesDest.size()>0){%>
      <tr class="fila">
        <td colspan="3" class="fila" >          <div align="center">
            <img src="<%=BASEURL%>/images/botones/buscar.gif" width="87" height="21" onClick="buscarStandard()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></div>
      </tr>
     
      <% }%>
      <%if(request.getAttribute("ok")!=null){%>
      <tr class="fila">
        <td class="fila"><strong> ESTANDARD JOB </strong>
        <td colspan="2" class="fila"><%TreeMap stdjob = model.stdjobdetselService.getStdjobTree(); %>
            <input:select name="standard" options="<%=stdjob%>" attributesText="style='width:100%;' class='textbox'"/> </td>
      </tr>
      <tr class="fila">
        <td colspan="3" class="fila">          <div align="center">            <img src="<%=BASEURL%>/images/botones/aceptar.gif" width="90" height="21" onClick="form1.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></div></td>
      </tr>
      <%}
	}else{%>
      <tr class="fila">
        <td class="fila"><strong>ESTANDARD JOB </strong></td>
        <td colspan="2" class="fila"><%=(String) request.getAttribute("std")%>
            <input name="standard" type="hidden" id="standard" value="<%=(String)request.getAttribute("sj")%>" >
        </td>
      </tr>
      <tr class="fila">
        <td colspan="3" class="fila">          <div align="center">            <img src="<%=BASEURL%>/images/botones/aceptar.gif" width="90" height="21" onClick="form1.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></div></td>
      </tr>  <%}%>
      <tr class="subtitulo1">
        <td colspan="3">BUSCAR POR ORDEN DE CARGA</td>
      </tr>
      <tr class="fila">
        <td rowspan="2" class="fila">ORDEN DE CARGA </td>
        <td colspan="2" valign="middle" class="fila"><span class="Simulacion_Hiper" style="cursor:hand " onClick="window.open('<%=BASEURL%>/jsp/masivo/ordencargue/listaOCargue.jsp','','HEIGHT=500,WIDTH=700,SCROLLBARS=YES,RESIZABLE=YES')">Consultar Ordenes de Carga...</span> </td>
        </tr>
      <tr class="fila">
        <td valign="middle" class="fila"><input name="orden" type="text" class="textbox" id="orden" ></td>
        <td valign="middle" class="fila"><img src="<%=BASEURL%>/images/botones/buscar.gif" width="87" height="21" onClick="this.disabled=true;form1.action='<%=CONTROLLER%>?estado=OCarga&accion=Despacho&cmd=ocargue';form1.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
      </tr>
    
    </table>	
	</td>
    </tr>
  </table>
	 <%if(request.getParameter("mensaje")!=null){%>
	 <br>
	 <br>
     <table border="2" align="center">
       <tr>
         <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
             <tr>
               <td width="229" align="center" class="mensajes"><span class="normal"><%=request.getParameter("mensaje")%></span></td>
               <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
               <td width="58">&nbsp;</td>
             </tr>
         </table></td>
       </tr>
     </table>
     <%}%>
     <br>
	 <br>
	 <%if(request.getAttribute("mensaje")!=null){%>
	 <table width="75%"  border="2" align="center" cellpadding="0" cellspacing="0" bgcolor="#EFEBDE">
	<tr>
	  <td>
	  <table width="100%" class="tablaInferior">
        <tr>
          <td height="22" colspan=2 class="subtitulo1">Informaci&oacute;n del Despacho </td>
          <td width="368" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
	  <table width="100%" align="center">
        <tr class="subtitulo1">
          <td width="100%" colspan="2"><strong  >ULTIMA PLANILLA Y REMESA GENERADAS </strong></td>
        </tr>
        <tr class="fila">
          <td colspan="2">Se generaron Planilla <span class="Estilo7"><strong><%=(String)request.getAttribute("numpla")%> </strong></span>y Remesa <span class="Estilo7"><strong><%=(String)request.getAttribute("numrem")%></strong></span></td>
        </tr>
		<%if(!"".equals(formato)){%>
        <tr class="fila">
          <td colspan="2" style="cursor:hand" title="Datos adicionales Cliente..."  onClick="window.open('<%=CONTROLLER%>?estado=FormatoNacional&accion=Datos&evento=LOAD&formato=<%=formato%>&tipodoc=002&doc=<%=(String)request.getAttribute("numrem")%>','','height=400,width=800,dependent=yes,resizable=yes,scrollbars=yes,status=yes');"  ><span class="Simulacion_Hiper">Datos adicionales del cliente.</span></td>
        </tr>
		<%}%>
		<%if(request.getAttribute("LINK")!=null){%>
        <tr class="fila">
          <td colspan="2" style="cursor:hand" title="Relacionar remesa con orden de compra..."  onClick="window.open('<%=CONTROLLER%>?estado=CargueLG&accion=Manager&cmd=Buscar&planilla=<%=(String)request.getAttribute("numpla")%>','','height=400,width=800,dependent=yes,resizable=yes,scrollbars=no,status=yes');"  ><span class="Simulacion_Hiper">Ingresar informacion de cargue.</span></td>
        </tr>
		<%}%>
        <tr class="fila">
          <td colspan="2"></td>
        </tr>
        <tr class="fila">
          <td colspan="2"><div align="center"><strong>IMPRESI&Oacute;N</strong></div></td>
        </tr>
        <tr class="fila">
          <td colspan="2" style="cursor:hand" title="Imprimir planilla..."  onClick="window.open('<%=CONTROLLER%>?estado=Impresion&accion=PlanillasPDF&numpla=<%=(String)request.getAttribute("numpla")%>&opcion=Buscar','','height=400,width=800,dependent=yes,resizable=yes,scrollbars=yes,status=yes');"  onMouseOver='cambiarColorMouse(this)'><span class="letras">Imprimir planilla </span> <b><%=(String)request.getAttribute("numpla")%></b></td>
        </tr>
        <tr class="fila">
          <td colspan="2"  style="cursor:hand" title="Imprimir remesa..." onClick="window.open('<%=CONTROLLER%>?estado=Impresion&accion=RemesasPDF&numrem=<%=(String)request.getAttribute("numrem")%>&opcion=Buscar','','height=400,width=800,dependent=yes,resizable=yes,scrollbars=yes,status=yes')"  onMouseOver='cambiarColorMouse(this)'><span class="letras">Imprimir remesa</span> <b><%=(String)request.getAttribute("numrem")%></b></td>
        </tr>
        <tr class="fila">
          <td colspan="2" height="24" class="Letras" style="cursor:hand" title="Lista de planillas por imprimir..."  onClick="window.open('<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/impresiones_pdf/&pagina=planillasnoimpPDF.jsp&marco=no&opcion=23','','height=400,width=800,dependent=yes,resizable=yes,scrollbars=no,status=yes');" onMouseOver='cambiarColorMouse(this)'>Listado de Planillas para imprimir</td>
        </tr>
        <tr class="fila">
          <td colspan="2"  style="cursor:hand" title="Lista de remesas por imprimir..." onClick="window.open('<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/impresiones_pdf/&pagina=remesanoimpPDF.jsp&marco=no&opcion=22','','height=400,width=800,dependent=yes,resizable=yes,scrollbars=yes,status=yes')"  onMouseOver='cambiarColorMouse(this)'>Listado de Remesas  para imprimir</td>
        </tr>
        <tr class="fila">
          <td colspan="2" style="cursor:hand" title="Lista de cheques por imprimir..." onClick="window.open('<%=CONTROLLER%>?estado=Menu&accion=ImpresionCheques&opcion=no&total=0','','height=400,width=800,dependent=yes,resizable=yes,scrollbars=yes,status=yes')"  onMouseOver='cambiarColorMouse(this)'>Listado de Cheques  para imprimir</td>
        </tr>
        <tr class="fila">
          <td colspan="2" class="Letras">
        <TR class="fila">
              <TD colspan="2" ALIGN='center'><FIELDSET>
                <legend><b>NOTA : </b></legend>
                Para ver informaci&oacute;n acerca de <b><a class="Simulacion_Hiper" style="cursor:hand" onclick="window.open('<%=CONTROLLER%>?estado=HReporte&accion=Search&planilla=<%=(String)request.getAttribute("numpla")%>&cia=<%=usuario.getDstrct()%>&cmd=ok');">Hoja de Reporte</a> </b>y <b><a class="Simulacion_Hiper" style="cursor:hand"  onclick="window.open('<%=CONTROLLER%>?estado=PlanViaje&accion=Buscar&txtDistrito=TSP&txtPlanilla=<%=(String)request.getAttribute("numpla")%>&rdOpcion=SAVE');">Plan de viaje</a></b>, deber&aacute; hacer Click sobre ellos.
              </FIELDSET></TD>
        </TR>
    </TABLE>	  </td>
       </tr>
    </table>
	  
   
	
	<%}%>
 
</form>
 </div>
<iframe width=188 height=166 name="gToday:datetime:agenda.js:gfPop:plugins_12.js" id="gToday:datetime:agenda.js:gfPop:plugins_12.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
<%=datos[1]%>
</body>
</html>
