<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Relacionar una nueva remesa a una planilla</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/validar.js">
</script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">

</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Agregar Remesa"/>
</div>

 <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<form name="form2" method="post" action="<%=CONTROLLER%>?estado=Planilla&accion=Search&colpapel=ok&nremesa=ok">
  <table width="57%"  border="2" align="center" bgcolor="#EFEBDE">
    <tr>
      <td>
        <table width="100%" align="center" class="tablaInferior">
          <tr>
            <td height="22" colspan=2 class="subtitulo1">Agregar Remesa <span class="titulo"><strong><br>
            </strong></span></td>
            <td width="308" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
        </table>
        <table width="100%" border="0" align="center" class="fila">
          <tr class="filaresaltada">
            <td width="162" nowrap><strong>Numero de la Planilla:</strong></td>
            <td width="352" nowrap>               
             <input name="planilla" type="text" id="planilla3" maxlength="10" onkeypress='soloAlfa(event)'>
            <input type='image' src="<%=BASEURL%>/images/botones/buscar.gif" width="87" height="21"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
          </tr>
      </table></td>
    </tr>
  </table>
</form>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=BuscarStandard&accion=Colpapel&remesa=ok" onSubmit="">
      <%Usuario usuario = (Usuario) session.getAttribute("Usuario");
	  java.util.Date date = new java.util.Date();
	  String placa="", trailer="", conductor="", codruta="",nomcond="", estandard="",  standar="", remitente="", destinatario="",docinterno="", fechadesp="", observacion="", rutaP="", precinto="", planilla="" ;
	  float ant=0, peso=0, vpla=0;
      if(model.planillaService.getPlanilla()!=null ){
        Planilla pla=  model.planillaService.getPlanilla();
        placa=pla.getPlaveh();
		trailer=pla.getPlatlr();
		conductor= pla.getCedcon();
		nomcond=pla.getNomCond();
		estandard=pla.getSj_desc();
		standar= pla.getSj();
		fechadesp = pla.getFecdsp();
		peso=pla.getPesoreal();
		rutaP = pla.getRuta_pla();
		codruta = pla.getCodruta();
		precinto = pla.getPrecinto();
		planilla= pla.getNumpla();
		vpla = pla.getVlrpla();
		model.movplaService.buscaMovpla(planilla,"01");
    	if(model.movplaService.getMovPla()!=null){
			Movpla movpla= model.movplaService.getMovPla();
			ant= movpla.getVlr();
    	}
    }else{
		
		model.planillaService.setPlanilla(null);
	}
    
	 
     
  %>

  <table width="98%"  border="2" align="center">
    <tr>
      <td>  <table width="100%" align="center" class="tablaInferior">
        <tr>
          <td height="22" colspan=2 class="subtitulo1">Agregar Remesa <span class="titulo"><strong><br>
          </strong></span></td>
          <td width="308" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>        
        
        <table width="100%" align="center" class="tablaInferior">
          <tr>
            <td height="22" class="subtitulo1">Infromacion de la planilla <span class="titulo"><%=planilla%>
                <input name="planilla" type="hidden" id="planilla2" value="<%=planilla%>">
                <input name="anular" type="HIDDEN" id="anular" value="<%=request.getParameter("anular")!=null?request.getParameter("anular"):""%>">
            </span></td>
          </tr>
        </table>
        <table width="100%" class="tablaInferior">
          <tr >
            <th height="26" scope="row" class="filaresaltada">Placa</th>
            <td width="86" height="26" class="letra" scope="row"><%=placa%></td>
            <td width="94" height="26" class="filaresaltada" scope="row">Trailer</td>
            <td width="91" class="letra"><%=trailer%></td>
            <td width="228" class="filaresaltada">Conductor</td>
            <td colspan="2" class="letra"><%=conductor%> <%=nomcond%></td>
          </tr>
          <tr>
            <th width="130" class="filaresaltada" scope="row">Precintos</th>
            <td height="31" colspan="2" class="letra" scope="row"><%=precinto%></td>
            <td class="filaresaltada">Ruta Planilla</td>
            <td class="letra"><%=rutaP%>
                <input name="rutaP2" type="hidden" id="rutaP6" value="<%=codruta%>">            </td>
            <td width="142" class="filaresaltada">Cantidad Depachada</td>
            <td width="158" class="letra"><%=peso%></td>
          </tr>
          <tr>
            <th height="31" colspan="2"  class="filaresaltada" scope="row">Valor Total de la Planilla</th>
            <td height="31" colspan="2"  class="letra" scope="row"><%=com.tsp.util.Util.customFormat(vpla)%></td>
            <td class="filaresaltada">Valor del Anticipo</td>
            <td colspan="2" class="letra"><%=com.tsp.util.Util.customFormat(ant)%></td>
          </tr>
        </table>
        <table width="100%" border="1" align="center" class="Letras">
        <tr>
          <td colspan="4" nowrap class="subtitulo1"><div align="left"><strong>LISTA REMESAS RELACIONADAS </strong></div></td>
        </tr>
        <tr bordercolor="#999999" class="tblTitulo">
         <td align="center">REMESA</td>
              <td align="center">CLIENTE</td>
              <td  align="center">ESTANDAR</td>
              <td align="left">PORCENTAJE</td>
        </tr>
        <%  
		if(!planilla.equals("")){
	  List remesas= model.planillaService.buscarRemesas(planilla);
	  Iterator rem=remesas.iterator();
	  int i=0;
	  while (rem.hasNext()){
	  	i++;
	  %>
	  
	  <%Remesa remesa = (Remesa) rem.next();
	  if(!remesa.getNumrem().equals(request.getParameter("anular"))){
		%>
        <tr class="<%=i%2==0?"filagris":"filaazul"%>">
          <td  class="bordereporte">
            <div align="center"><%=remesa.getNumrem()%> </div></td>
          <td class="bordereporte">
            <div align="center" class="Estilo6"><%=remesa.getCliente()%> </div></td>
          <td  class="bordereporte"><div align="center"><%=remesa.getDescripcion()%></div></td>
          <td class="bordereporte">
            <%=remesa.getPorcentaje()%>
			<input name="por<%=remesa.getNumrem()%>" type="hidden" id="por<%=remesa.getNumrem()%>" size="5" maxlength="3" value="<%=remesa.getPorcentaje()%>"  onKeyPress="soloDigitos(event,'decNo')" readonly>
			</td>
        </tr>
        <%}
		else{%>
		<input name="por<%=remesa.getNumrem()%>" type="hidden" id="P<%=remesa.getNumrem()%>" size="5" maxlength="3" value="0"  onKeyPress="soloDigitos(event,'decNo')" readonly>
		<%}
		}
		}
  %>
      </table>
	  </td>
    </tr>
  </table>
	   <%if(request.getAttribute("numrem")!=null){%>
       <table width="70%"  border="2" align="center">
         <tr>
           <td>
             <table width="100%" align="center" class="tablaInferior">
               <tr>
                 <td height="22" colspan=2 class="subtitulo1">Informacion de la nueva remesa <span class="titulo"><strong><br>
                 </strong></span></td>
                 <td width="308" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
               </tr>
             </table>
             <table width="100%" align="center" class="tablaInferior">
               <tr class="subtitulo1">
                 <td >ULTIMA REMESA</td>
               </tr>
               <tr>
                 <td class="filaresaltada">Se gener&oacute; Remesa <strong><%=(String)request.getAttribute("numrem")%></strong></td>
               </tr>
               <tr class="subtitulo1">
                 <td ><div align="left"><strong>IMPRESI&Oacute;N</strong></div></td>
               </tr>
               <%if(!planilla.equals("")){%>
               <tr>
                 <td class="filaresaltada" style="cursor:hand" title="Imprimir planilla..." onMouseOver="bgColor='#99cc99'" onMouseOut="bgColor='ECE0D8'"  onClick="window.open('<%=CONTROLLER%>?estado=PlanillaImpresion&accion=Opciones&Opcion=Buscar&NumeroPlanilla=<%=planilla%>','','height=400,width=800,dependent=yes,resizable=yes,scrollbars=yes,status=yes');"><span class="letras">Imprimir planilla </span> <b><%=planilla%></b></td>
               </tr>
               <%}%>
               <tr>
                 <td class="filaresaltada" style="cursor:hand" title="Imprimir remesa..."  onMouseOver="bgColor='#99cc99'" onMouseOut="bgColor='ECE0D8'" onClick="window.open('<%=CONTROLLER%>?estado=Remesa&accion=Buscar&Numrem=<%=(String)request.getAttribute("numrem")%>','','height=400,width=800,dependent=yes,resizable=yes,scrollbars=yes,status=yes')"><span class="letras">Imprimir remesa</span> <b><%=(String)request.getAttribute("numrem")%></b></td>
               </tr>
               <tr>
                 <td class="filaresaltada" style="cursor:hand" title="Lista de remesas por imprimir..."  onMouseOver="bgColor='#99cc99'" onMouseOut="bgColor='ECE0D8'" onClick="window.open('<%=CONTROLLER%>?estado=Remesa&accion=Buscar','','height=400,width=800,dependent=yes,resizable=yes,scrollbars=yes,status=yes')">Listado de Remesas para imprimir</td>
               </tr>
               <tr>
                 <td height="24" class="filaresaltada" style="cursor:hand" title="Lista de planillas por imprimir..." onMouseOver="bgColor='#99cc99'" onMouseOut="bgColor='ECE0D8'"  onClick="window.open('<%=CONTROLLER%>?estado=Menu&accion=PlanillaImpresion','','height=400,width=800,dependent=yes,resizable=yes,scrollbars=yes,status=yes');">Listado de Planillas para imprimir</td>
               </tr>
           </TABLE></td>
         </tr>
       </table>
       <%}%>
	   <%if(model.planillaService.getPlanilla()!=null ){%>
       <table width="98%"  border="2" align="center">
    <tr>
      <td> 
	  <table width="100%">
   
   <tr valign="top">
      <th colspan="2"  scope="row"><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td height="22" colspan=2 class="subtitulo1">Registro de Remesas Pantalla Preliminar</td>
          <td width="368" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
      <table width="100%" align="center" class="tablaInferior">
        <tr class="fila">
          <th width="164" rowspan="2" scope="row"><div align="left">STANDARD/CLIENTE</div></th>
          <td colspan="2" class="fila" > <span class="Simulacion_Hiper" style="cursor:hand " onClick="window.open('<%=BASEURL%>/consultas/consultasClientes.jsp','','HEIGHT=200,WIDTH=600,SCROLLBARS=YES,RESIZABLE=YES')">Consultar clientes...</span></td>
        </tr>
        <tr class="Estilo6">
          <td colspan="2" class="fila">
            <input name="cliente" type="text" id="cliente2"   maxlength="6" onKeyPress="return verificarTecla2(event);">
            <img src="<%=BASEURL%>/images/botones/buscar.gif" width="87" height="21" align="absmiddle" style="cursor:hand" onClick="buscarClient2();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
            <input name="clienteR" type="hidden" id="clienteR" value="<%=request.getParameter("cliente")%>">
            <input name="standard_nom" type="hidden" id="standard_nom" value="<%=(String)request.getAttribute("std")%>">
            <input name="remitentes" type="hidden" id="remitentes" value=" ">
            <input name="docinterno" type="hidden" id="docinterno" value=" ">
            <input name="fechadesp" type="hidden" id="fechadesp" value=" ">
            <input name="placa" type="hidden" id="placa" value=" ">
            <input name="destinatarios" type="hidden" id="destinatarios" value=" ">
            <input name="trailer" type="hidden" id="trailer" value=" ">
            <input name="conductor" type="hidden" id="conductor" value=" ">
            <input name="ruta" type="hidden" id="ruta2" value=" ">
            <input name="toneladas" type="hidden" id="toneladas" value=" ">
            <input name="valorpla" type="hidden" id="valorpla" value=" ">
            <input name="anticipo" type="hidden" id="anticipo" value=" ">
            <input name="precintos" type="hidden" id="precintos" value=" ">
            <input name="observacion" type="hidden" id="observacion" value=" ">
            <br>
            <%if(request.getAttribute("cliente")!=null){%>
            <table width="100%" border="0" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" class="fila">
              <tr>
                <td><strong><%=(String) request.getAttribute("cliente")%></strong></td>
              </tr>
              <tr>
                <td><strong>Agencia Due&ntilde;a del Cliente : <%=(String) request.getAttribute("agency")%></strong></td>
              </tr>
</table>            
            <%}%></td>
        </tr>
        <%if(request.getAttribute("std")==null){%>
        <tr class="fila">
          <td rowspan="2" ><strong>RUTA</strong>
          <td width="86"><strong>ORIGEN</strong></td>
          <td width="371">
            <%TreeMap ciudades = model.stdjobdetselService.getCiudadesOri(); 
	  String corigen="";
	  if(request.getParameter("origen")!=null){
	  	corigen = request.getParameter("origen");
	  }
	  String cdest="";
	  if(request.getParameter("destino")!=null){
	  	cdest = request.getParameter("destino");
	  }
	  %>
            <input:select name="ciudadOri" options="<%=ciudades%>" attributesText="style='width:100%;' onChange='buscarDestinos2()'" default='<%=corigen%>'/> </td>
        </tr>
        <tr class="Estilo6">
          <td class="fila"><strong>DESTINO</strong></td>
          <td class="fila">
            <%TreeMap ciudadesDest = model.stdjobdetselService.getCiudadesDest(); %>
            <input:select name="ciudadDest" options="<%=ciudadesDest%>" attributesText="style='width:100%;'"  default='<%=cdest%>'/> </td>
        </tr>
        <%if(ciudadesDest.size()>0){%>
        <tr class="fila">
          <td colspan="3" >            <div align="center">              <img src="<%=BASEURL%>/images/botones/buscar.gif" width="87" height="21" onClick="buscarStandard2()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></div>
        </tr>
        <% }%>
        <%if(request.getAttribute("ok")!=null){%>
        <tr class="fila">
          <td ><strong> ESTANDARD JOB </strong>
          <td colspan="2"><%TreeMap stdjob = model.stdjobdetselService.getStdjobTree(); %>
              <input:select name="standard" options="<%=stdjob%>" attributesText="style='width:100%;'"/> </td>
        </tr>
        <tr class="fila">
          <td colspan="3" >            <div align="center">
              <input type="image" src="<%=BASEURL%>/images/botones/aceptar.gif" width="90" height="21"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">          </div></td>
        </tr>
        <%}
	}else{%>
        <tr class="fila">
          <td ><strong>ESTANDARD JOB </strong></td>
          <td colspan="2" ><%=(String) request.getAttribute("std")%>
              <input name="standard" type="hidden" id="standard" value="<%=(String)request.getAttribute("sj")%>">          </td>
        </tr>
        <tr class="fila">
          <td colspan="3" >            <div align="center">
              <input type="image" src="<%=BASEURL%>/images/botones/aceptar.gif" width="90" height="21"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></div></td>
        </tr>
        <%}%>
      </table></th>
    </tr>
  </table>  </td>
    </tr>
  </table>
  <%}%>
       <br>
	   
       <%if(request.getParameter("mensaje")!=null){%>
       <table border="2" align="center">
         <tr>
           <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
               <tr>
                 <td width="229" align="center" class="mensajes"><span class="normal"><%=request.getParameter("mensaje")%></span></td>
                 <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                 <td width="58">&nbsp;</td>
               </tr>
           </table></td>
         </tr>
       </table>
       <%}%>
</form>
<iframe width=188 height=166 name="gToday:datetime:agenda.js:gfPop:plugins_12.js" id="gToday:datetime:agenda.js:gfPop:plugins_12.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</div>
</body>
</html>
