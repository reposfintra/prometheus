<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*,com.tsp.util.*,java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Relacionar una nueva remesa a una planilla</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/validar.js">
</script>
<script language="javascript" src="<%=BASEURL%>/js/validarDespacho.js">
</script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<%String isAnular = request.getParameter("anular")!=null?!request.getParameter("anular").equals("")?"SI":"NO":"NO";%>
</head>
<% 
		 String origen ="";
		String pa ="";
	  	String uw="";
	  	String vlr ="";
		double vlr_pes=0;
		float unidad_default = 0;
	  	Stdjobdetsel sj=new Stdjobdetsel();
	  	String standard = request.getParameter("standard");
	  	model.stdjobdetselService.searchStdJob(request.getParameter("standard"));
        if(model.stdjobdetselService.getStandardDetSel()!=null){
			sj = model.stdjobdetselService.getStandardDetSel();
			uw=sj.getUnit_of_work();
            pa=sj.getPorcentaje_ant();
			vlr = ""+sj.getVlr_freight();
			 
            origen = sj.getOrigin_code(); 
			if(sj.getVlr_pes()!=0){
				vlr =  ""+sj.getVlr_pes();
			}           
			unidad_default=sj.getUnidades(); 
			
          }
		
		  %>
<body onLoad="buscarValorOt();buscarpor();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Agregar Remesa"/>
</div>

 <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<form name="form2" method="post" action="<%=CONTROLLER%>?estado=Planilla&accion=Search&colpapel=ok&nremesa=ok">
    <table width="57%"  border="2" align="center">
    <tr>
      <td>
        <table width="100%" align="center" class="tablaInferior">
          <tr>
            <td height="22" colspan=2 class="subtitulo1">Buscar Planilla<span class="titulo"><strong><br>
            </strong></span></td>
            <td width="308" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
        </table>
        <table width="100%" border="0" align="center" class="fila">
          <tr class="filaresaltada">
            <td width="162" nowrap><strong>Numero de la Planilla:</strong></td>
            <td width="352" nowrap><input name="planilla" type="text" id="planilla3" maxlength="10">
                <img src="<%=BASEURL%>/images/botones/buscar.gif" width="87" height="21" onClick="form2.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
          </tr>
      </table></td>
    </tr>
  </table>
</form>
 <%Usuario usuario = (Usuario) session.getAttribute("Usuario");
	  java.util.Date date = new java.util.Date();
	  String placa="", trailer="", conductor="", codruta="",nomcond="", estandard="",  standar="", remitente="", destinatario="",docinterno="", fechadesp="", observacion="", rutaP="", precinto="", planilla="" ;
	  float ant=0, peso=0,vpla=0;
      if(model.planillaService.getPlanilla()!=null){
        Planilla pla= model.planillaService.getPlanilla();
        placa=pla.getPlaveh();
		trailer=pla.getPlatlr();
		conductor= pla.getCedcon();
		nomcond=pla.getNomCond();
		estandard=pla.getSj_desc();
		standar= pla.getSj();
		fechadesp = pla.getFecdsp();
		peso=pla.getPesoreal();
		rutaP = pla.getRuta_pla();
		codruta = pla.getCodruta();
		precinto = pla.getPrecinto();
		planilla= pla.getNumpla();
		vpla = pla.getVlrpla();
    }
   
	String numremP="";
	SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd hh:mma");
	String fecpla = s.format(date); 
     
  %>
   <form name="form1" method="post" action="<%=CONTROLLER%>?estado=Remesa&accion=Validar" >
 <table width="98%"  border="2" align="center">
   <tr>
     <td>
       <table width="100%" align="center" class="tablaInferior">
         <tr>
           <td height="22" colspan=2 class="subtitulo1">Agregar Remesa <span class="titulo"><strong><br>
           </strong></span></td>
           <td width="308" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
         </tr>
       </table>
       <table width="100%" align="center" class="tablaInferior">
         <tr>
           <td height="22" class="subtitulo1">Informacion de la planilla <span class="titulo"><%=planilla%>
                 <input name="planilla" type="hidden" id="planilla" value="<%=planilla%>">
                 <input name="anular" type="hidden" id="anular" value="<%=request.getParameter("anular")!=null?request.getParameter("anular"):""%>">
           </span></td>
         </tr>
       </table>
       <table width="100%" class="tablaInferior">
         <tr >
           <th height="26" scope="row" class="filaresaltada">Placa</th>
           <td width="86" height="26" class="letra" scope="row"><%=placa%></td>
           <td width="94" height="26" class="filaresaltada" scope="row">Trailer</td>
           <td width="91" class="letra"><%=trailer%></td>
           <td width="228" class="filaresaltada">Conductor</td>
           <td colspan="2" class="letra"><%=conductor%> <%=nomcond%></td>
         </tr>
         <tr>
           <th width="130" class="filaresaltada" scope="row">Precintos</th>
           <td height="31" colspan="2" class="letra" scope="row"><%=precinto%></td>
           <td class="filaresaltada">Ruta Planilla</td>
           <td class="letra"><%=rutaP%>
               <input name="rutaP2" type="hidden" id="rutaP6" value="<%=codruta%>">           </td>
           <td width="142" class="filaresaltada">Cantidad Depachada</td>
           <td width="158" class="letra"><%=peso%></td>
         </tr>
         <tr>
           <th height="31" colspan="2"  class="filaresaltada" scope="row">Valor Total de la Planilla</th>
           <td height="31" colspan="2"  class="letra" scope="row"><%=com.tsp.util.Util.customFormat(vpla)%></td>
           <td class="filaresaltada">Valor del Anticipo</td>
           <td colspan="2" class="letra"><%=com.tsp.util.Util.customFormat(ant)%></td>
         </tr>
       </table>
       <table width="100%" border="1" align="center" class="Letras">
         <tr>
           <td colspan="4" nowrap class="subtitulo1"><div align="left"><strong>LISTA REMESAS RELACIONADAS </strong></div></td>
         </tr>
         <tr bordercolor="#999999" class="tblTitulo">
          <td align="center">REMESA</td>
              <td align="center">CLIENTE</td>
              <td  align="center">ESTANDAR</td>
              <td align="left">PORCENTAJE</td>
         </tr>
         <%  
		 int i=0;
		if(!planilla.equals("")){
	  List remesas= model.planillaService.buscarRemesas(planilla);
	  Iterator rem=remesas.iterator();
	  
	  while (rem.hasNext()){
	  	i++;
	  %>
         <%Remesa remesa = (Remesa) rem.next();
	  if(!remesa.getNumrem().equals(request.getParameter("anular"))){
	  	numremP = remesa.getNumrem();

		%>
         <tr class="<%=i%2==0?"filagris":"filaazul"%>">
           <td  class="bordereporte">
             <div align="center"><%=remesa.getNumrem()%> </div></td>
           <td class="bordereporte">
             <div align="center" ><%=remesa.getCliente()%> </div></td>
           <td  class="bordereporte"><div align="center"><%=remesa.getDescripcion()%></div></td>
           <td class="bordereporte">
             <input name="por<%=remesa.getNumrem()%>" type="text" id="por<%=remesa.getNumrem()%>" size="5" maxlength="3" value="<%=remesa.getPorcentaje()%>"  onKeyPress="soloDigitos(event,'decNo')">
			 <input name="vlr<%=remesa.getNumrem()%>" type="hidden" id="vlr<%=remesa.getNumrem()%>" value="<%=remesa.getVlr_pesos()==0?remesa.getVlrRem():remesa.getVlr_pesos()%>">
</td>
         </tr>
         
         <%}
		else{%>
         <input name="por<%=remesa.getNumrem()%>" type="hidden" id="P<%=remesa.getNumrem()%>" size="5" maxlength="3" value="0"  onKeyPress="soloDigitos(event,'decNo')" readonly>
         <%}
		}
		
		}
		i++;
  %>
  	<tr class="<%=i%2==0?"filagris":"filaazul"%>">
           <td colspan="3"  class="bordereporte"><div align="right"><b>PORCENTAJE NUEVA REMESA</b></div></td>
           <td class="bordereporte"><input name="porcent" type="text" id="porcent" onKeyPress="soloDigitos(event,'decNo')" value="<%=request.getParameter("porcent")%>" size="5" maxlength="3"></td>
         </tr>
     </table>
        </td>
   </tr>
 </table>
 <br>
 <br>
 <%numremP="";
 if(model.remesaService.getRemesa()!=null){
     	Remesa reme = model.remesaService.getRemesa();
		numremP = reme.getNumrem();
        remitente = reme.getRemitente()!=null?reme.getRemitente():"";
        destinatario= reme.getDestinatario();
        	
}%>
   <table width="98%"  border="2" align="center">
   <tr>
     <td>
 <table width="100%" align="center" class="tablaInferior">
   <tr>
     <td height="22" colspan=2 class="subtitulo1"><span class="Estilo6 Estilo11">
       
     </span>Informacion de la nueva remesa<span class="titulo"><strong><br>
     </strong></span></td>
     <td width="308" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
   </tr>
 </table>
 <table width="100%" align="center"  >
   <tr class="fila">
     <th colspan="2" scope="row"><div align="left">Estandar Job</div></th>
     <td colspan="3"><span class="Estilo17"><%=sj.getSj()+"-"+sj.getSj_desc()%>
           <input name="standard" type="hidden" id="standard" value="<%=sj.getSj()%>">
     </span></td>
     <td width="18%">Fecha Remesa </td>
     <td><input name="fechadesp2" type="text" id="fechadesp22" size="18" value="<%=fecpla%>"></td>
   </tr>
   <tr class="fila">
     <th colspan="2" scope="row"><div align="left">Informaci&oacute;n al Cliente </div></th>
     <td colspan="5"><span class="Estilo6">
       <input name="remitentes" type="hidden" id="remitentes" value="<%=remitente%>">
       <a class="Simulacion_Hiper" style="cursor:hand " onClick="if(form1.remitentes.value =='' )form1.remitentes.value='a';abrirPagina('<%=BASEURL%>/colpapel/remitentes.jsp?sj='+form1.standard.value+'&origen='+form1.origstd.value,'');">
       <input name="remitentesR" type="hidden" id="remitentesR" value="<%=remitente%>">
       Agregar Remitentes</a>
       <input name="origstd" type="hidden" id="origstd" value="<%=origen%>">
       <img src="<%=remitente.equals("")?BASEURL+"/images/equis2.gif":BASEURL+"/images/ok2.gif"%>" width="17" height="17" id="imre">
       <input name="imagenre" type="hidden" id="imagenre" value="<%=remitente.equals("")?BASEURL+"/images/equis2.gif":BASEURL+"/images/ok2.gif"%>">
       <input name="imagendest" type="hidden" id="imagendest" value="<%=destinatario.equals("")?BASEURL+"/images/equis2.gif":BASEURL+"/images/ok2.gif"%>">
       <input name="destinatarios" type="hidden" id="destinatarios" value="<%=destinatario%>" readonly>
       <a class="Simulacion_Hiper" style="cursor:hand " onClick="if(form1.destinatarios.value == '')form1.destinatarios.value='a';abrirPagina('<%=CONTROLLER%>?estado=Buscar&accion=Destinatarios&numrem=<%=numremP%>&sj='+form1.standard.value,'');">
       <input name="destinatarioR" type="hidden" id="destinatarioR" value="<%=destinatario%>">
       Agregar Destinatarios</a>
       <input name="docudest" type="hidden" id="docudest" value="<%=request.getParameter("docudest")!=null?request.getParameter("docudest"):"a"%>">
       <img src="<%=destinatario.equals("")?BASEURL+"/images/equis2.gif":BASEURL+"/images/ok2.gif"%>" width="17" height="17" id="imdest">
       <input name="docinterno" type="hidden" id="docinterno" size="38" value="<%=request.getParameter("docinterno")%>">
       <input name="fcial2" type="hidden" id="fcial2" size="70" value="<%=request.getParameter("fcial")%>">
       <a class="Simulacion_Hiper" onClick="abrirPagina('<%=CONTROLLER%>?estado=Aplicar&accion=Documentos&generar=ok','')" style="cursor:hand ">Agregar Documentos Relacionados</a><strong> </strong> </span> </td>
   </tr>
   <tr class="fila">
     <th colspan="2" scope="row"><div align="left">Cantidad a Facturar </div></th>
     <td width="24%"><span class="Estilo6"><span class="Estilo8"><span class="Estilo1">
       <input name="cfacturar" type="text" id="cfacturar"  onKeyPress="soloDigitos(event,'decOK')"  onKeyUp="buscarValorOt();buscarpor();" value="<%=unidad_default%>">
       </span>
             <input name="vlrrem" type="HIDDEN" id="vlrrem"  value="<%=request.getParameter("vlrrem")!=null?request.getParameter("vrem"):vlr%>" readonly>
             <input name="vremesa" type="hidden" id="vremesa2" readonly value="<%=request.getParameter("vremesa")!=null?request.getParameter("vremesa"):""%>">
             <input name="vrem2" type="hidden" id="vrem2" value="<%=vlr%>">
     </span> </span></td>
     <td width="17%">Unidad de Facturacion </td>
     <td colspan="2"><strong> <%=uw%>
           <input name="uw" type="hidden" id="uw" value="<%=request.getParameter("uw")!=null?request.getParameter("uw"):uw%>">
     </strong> </td>
     <td rowspan="2">
       <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="filaresaltada">
         <tr>
           <td><input name="facturable"  type="checkbox" id="facturable3" value="N">
No Facturable</td>
         </tr>
         <tr>
           <td><input name="cdock" type="checkbox" id="cdock3" value="N">
Cross Docking </td>
         </tr>
         <tr>
           <td><input name="cadena" type="checkbox" id="cadena" value="N">
      Cadena</td>
         </tr>
       </table>       </td>
   </tr>
   <tr class="fila">
     <th colspan="2" class="Estilo7" scope="row"><div align="left"><span class="Estilo8">Cantidad de Empaque</span></div></th>
     <td><span class="Estilo8"><span class="Estilo1">
       <input name="cantreal" type="text" id="cantreal" size="12" value="<%=request.getParameter("")!=null?request.getParameter(""):""%>">
     </span> </span> </td>
     <td><strong><span class="Estilo8">Unidad de Empaque</span></strong> </td>
     <td colspan="2"><%TreeMap unidades = model.unidadService.getUnidades(); %>
         <input:select name="unidad" options="<%=unidades%>" attributesText="style='width:100%;'" default='<%=request.getParameter("unidad")%>' /></td>
     </tr>
 </table>
 <table width="100%" class="tablaInferior">
   <tr>
     <td height="22" class="subtitulo1">Observaciones</td>
     </tr>
 </table> 
 
 <table width="100%" height="44" border="1" bordercolor="#CCCCCC">
   <tr class="filaresaltada">
     <th height="26" scope="row"><div align="left">
       <textarea name="observacion" cols="60" id="textarea6" ><%=request.getParameter("observacion")!=null?request.getParameter("observacion"):""%></textarea>
</div></th>
   </tr>
 </table> </td>
  </tr>
   </table>
   <br>
   <br>
   <p align="center">
    <img name="imageField"  src="<%=BASEURL%>/images/botones/aceptar.gif" width="90" height="21" border="0" onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);" onClick="if( ValidarColpapelR(form1,'<%=isAnular%>','')){form1.submit();this.disabled=true;}">
    <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand"> </p>
</form>
<iframe width=188 height=166 name="gToday:datetime:agenda.js:gfPop:plugins_12.js" id="gToday:datetime:agenda.js:gfPop:plugins_12.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</DIV>
</body>
</html>
