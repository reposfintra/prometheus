<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Descripcion Asociaci�n OT Padre</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</head>

<body>
<br>


<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Agregar remesas a remesa Padre </div></td>
        </tr>
        <tr >
          <td colspan="2" class="fila"> Asociar a una remesa Padre existente </td>
        </tr>
        
        <tr>
            <td width="525" colspan="2" class="ayudaHtmlTexto">Esta opci�n habilita la parte izquierda del panel, la cual permite buscar una remesa padre, si la remesa existe se presentar�n las remesas asociadas a esta.
            </td>
        </tr>
        
        <tr class="subtitulo1">
          <td colspan="2"> Remesa Padre  </td>
        </tr>
        <tr>
          <td width="149" class="fila"> Numero de la remesa </td>
          <td width="525"  class="ayudaHtmlTexto">Campo para digitar el n�mero de la remesa Padre.</td>
        </tr>
        
        <tr>
          <td width="149" class="fila"> Descripci�n </td>
          <td width="525"  class="ayudaHtmlTexto">Campo para ingresar y/o modificar la descripci�n de la remesa padre.</td>
        </tr>
        
        <tr class="subtitulo1">
          <td colspan="2" height="20" >   </td>
        </tr>
   
        <tr class="subtitulo1">
          <td colspan="2" class="fila"> Agregar a una remesa Padre  </td>
        </tr>
        <tr>
            <td width="525" colspan="2" class="ayudaHtmlTexto">Esta opci�n habilita la parte derecha del panel, la cual permite buscar una remesa y generar una remesa padre nueva.
            </td>
        </tr>
         <tr class="subtitulo1">
          <td colspan="2"> Remesa  </td>
        </tr>
        <tr>
          <td width="149" class="fila"> Numero de la remesa </td>
          <td width="525"  class="ayudaHtmlTexto">Campo para digitar el n�mero de la remesa.</td>
        </tr>
                
      </table>
    </td>
  </tr>
</table>
<br>
<div align="center">
    <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" style="cursor:pointer" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> </p>
</div>
<p>&nbsp;</p>

</body>
</html>
