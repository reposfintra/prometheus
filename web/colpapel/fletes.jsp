<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Modificacion de fletes..</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/validar.js">
</script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">

</head>

<body onLoad="form2.maximo.value=window.opener.form1.valorpla.options[window.opener.form1.valorpla.selectedIndex].label;<%if(request.getParameter("mensaje")!=null){%>alert('<%=request.getParameter("mensaje")%>')<%}%>;">
<form action="" method="post" name="form2" id="form2">
  <%
	Stdjobdetsel sj = model.stdjobdetselService.getStandardDetSel();
	
	%>
  <table width="81%"  border="2" align="center">
	<tr>
	  <td><div align="center">
	    <table width="100%" align="center" class="tablaInferior">
          <tr>
            <td height="22" colspan=2 class="subtitulo1"><span class="Letras"><strong>APLICAR UN NUEVO VALOR DE FLETE</strong> </span><span class="titulo"><strong>
              <input name="sj" type="hidden" id="sj4" value="<%=request.getParameter("sj")%>">
              <input name="ccorrect" type="hidden" id="ccorrect4" value="<%=request.getParameter("ccorrect")!=null?request.getParameter("ccorrect"):"NO"%>">
              <br>
            </strong></span></td>
            <td width="308" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
        </table>
	    <table width="100%" class="filaresaltada">
          <tr class="subtitulo1">
            <td colspan="2" ><%=sj.getSj()+"-"+sj.getSj_desc()%></td>
            </tr>
          <tr>
            <td width="42%"><strong>VALOR DEL FLETE A CAMBIAR </strong> : </td>
            <td width="58%"><input name="maximo" type="text" id="maximo2" readonly></td>
          </tr>
          <tr>
            <td><strong>NUEVO VALOR A APLICAR: </strong></td>
            <td><input name="nuevo" type="text" id="nuevo" onKeyUp="verificarFlete();" value="<%=request.getParameter("nuevo")!=null?request.getParameter("nuevo"):""%>">
              </td>
          </tr>
          <tr>
            <td><strong>JUSTIFICACION DEL CAMBIO</strong></td>
            <td><textarea name="justificacion" cols="40" <%=request.getParameter("justificacion")!=null?"":"disabled"%> id="justificacion" onFocus="if(this.value == 'Escriba aqui su justificación.')this.value =''"><%=request.getParameter("justificacion")!=null?request.getParameter("justificacion"):"No necesita justificación."%>
</textarea></td>
          </tr>
        </table>
      </div></td>
    </tr>
  </table>
  <div align="center"><img src="<%=BASEURL%>/images/botones/aceptar.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="aplicarFlete('<%=sj.isBloquea_despacho()%>','<%=BASEURL%>');" onMouseOut="botonOut(this);" style="cursor:hand"> <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand"> <br>
    <br>
    <%if(request.getParameter("clave")!=null){%>
  </div>
  <table width="36%"  border="2" align="center">
	<tr>
	  <td>
  <table width="100%" align="center" class="tablaInferior">
    <tr>
      <td height="22" colspan=2 class="subtitulo1"><strong>INGRESAR CLAVE </strong><span class="titulo"><strong><br>
      </strong></span></td>
      <td width="136" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
    </tr>
  </table>
  <table width="100%" align="center" bgcolor="#EFEBDE" class="filaresaltada">
    <tr class="titulo">
      <td height="37" ><div align="center"><strong> CLAVE </strong></div></td>
      <td height="37" ><input name="nclave" type="text" id="nclave2"></td>
    </tr>
    <tr>
      <td><div align="center">NUMERO DE SOLICITUD
      </div></td>
      <td><input name="numsol" type="text" id="numsol2" onKeyUp="window.opener.form1.solicitud.value=this.value;"></td>
    </tr>
  </table>
  </td>
    </tr>
  </table>
  <div align="center">
    <input type="button" name="Submit3" value="ACEPTAR" >
    <img src="<%=BASEURL%>/images/botones/aceptar.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.location='<%=CONTROLLER%>?estado=Validar&accion=Clave&standard=<%=sj.getSj()%>&placa='+window.opener.form1.placa.value+'&vflete='+window.opener.form1.valorpla.options[window.opener.form1.valorpla.selectedIndex].label+'&nuevo='+form2.nuevo.value+'&nclave='+form2.nclave.value+'&justificacion='+form2.justificacion.value+'&sj='+form2.sj.value+'&numsol='+form2.numsol.value" onMouseOut="botonOut(this);" style="cursor:hand">    <%}%>
    
</div>
</form>
</body>
</html>
