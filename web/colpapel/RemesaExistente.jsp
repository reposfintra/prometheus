<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Relacionar una remesa a una planilla</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/validar.js">
</script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</script>
<script language="javascript" src="<%=BASEURL%>/js/validarDespacho.js">
</script>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">

</head>
<%
String color="";
if(request.getAttribute("standar")!=null){
	color = (String)request.getAttribute("standar");
}

%>
<body <%if(request.getAttribute("remesa")!=null){%>onLoad="buscarpor();"<%}else {%>onLoad="form2.remesa.focus();"<%}%>>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Relacionar Remesa Existente"/>
</div>

 <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<%String estandar="", trailer="", cliente="", fecha="",standar="", observacion="",docuinterno="", remitentes="", destinatarios="", remesa="", planilla="", cedula="", nombre="", placa="", precinto=""; 
float peso=0, valor=0;
double vlr=0;
String conductor="", estandard="",codruta="",nomcond="",  fechadesp="",  rutaP="";
float ant=0;
List planillas=new LinkedList();
%>
<%Usuario usuario = (Usuario) session.getAttribute("Usuario");
      java.util.Date date = new java.util.Date();
      if(request.getAttribute("remesa")!=null){
	    Remesa rem = (Remesa)request.getAttribute("remesa");
	    request.getSession().setAttribute("remesa",rem);
        estandar = rem.getDescripcion();
        standar= rem.getStdJobNo();
        docuinterno = rem.getDocInterno();
        remitentes = rem.getRemitente();
        destinatarios = rem.getDestinatario();
        remesa = rem.getNumrem();
		observacion = rem.getObservacion();
		cliente = rem.getCliente();
		vlr = rem.getVlr_pesos();
	  }
      if(request.getAttribute("planillas")!=null){
        planillas = (List) request.getAttribute("planillas");
		session.setAttribute("planillas", planillas);
	  }
	  planilla = request.getParameter("planilla");
	  model.planillaService.bucarColpapel(planilla);
      if(model.planillaService.getPlanilla()!=null){
      	Planilla pla = model.planillaService.getPlanilla();
      	String nomruta= model.planillaService.getRutas(pla.getNumpla());
      	pla.setCodruta(pla.getRuta_pla());
      	pla.setRuta_pla(nomruta);
		placa=pla.getPlaveh();
		trailer=pla.getPlatlr();
		conductor= pla.getCedcon();
		nomcond=pla.getNomCond();
		estandard=pla.getSj_desc();
		fechadesp = pla.getFecdsp();
		peso=pla.getPesoreal();
		rutaP = pla.getRuta_pla();
		codruta = pla.getCodruta();
		precinto = pla.getPrecinto();
		planilla= pla.getNumpla();
	  }
	  
  %>
<form name="form2" method="post" action="<%=CONTROLLER%>?estado=RemesaPlanilla&accion=Search">
  <br>
  <table width="34%"  border="2" align="center">
    <tr>
      <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="22" colspan=2 class="subtitulo1">Relacionar</td>
            <td width="165" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
        </table>
          <table width="100%" align="center">
            <tr class="filaresaltada" >
              <td width="193" nowrap><strong>Numero de la Remesa:</strong></td>
              <td width="270" nowrap ><input name="remesa" type="text" id="remesa" maxlength="10" value="<%=request.getParameter("remesa")!=null?request.getParameter("remesa"):""%>">
                <input name="existe" type="hidden" id="existe3" value="true">
                <input name="planilla" type="hidden" id="planilla4" value="<%=planilla%>">
               
                <input name="anular" type="hidden" id="anular" value="<%=request.getParameter("anular")%>">
              </td>
            </tr>
        </table></td>
    </tr>
  </table>
  <div align="center"><br>
      <img src="<%=BASEURL%>/images/botones/buscar.gif" width="87" height="21" onClick="if(form2.remesa.value !='' &&  form2.planilla.value !=''){form2.submit();}else{alert('Escriba el numero de la  Remesa');}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">      <input name="planilla" type="hidden" id="planilla" value="<%=planilla%>">
  </div>
</form>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=RemesaPlanilla&accion=Search&relacionar=ok">
  <table width="67%"  border="2" align="center">
    <tr>
      <td><table width="100%" border="0" align="center" cellpadding="0" cellspacing="0"  class="tablaInferior">
          <tr >
            <td width="463" nowrap  class="subtitulo1">RELACIONAR PLANILLA CON REMESA 
              <input name="anular" type="hidden" id="anular" value="<%=request.getParameter("anular")%>"></td>
            <td width="362" nowrap class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
      </table>
        
        <table width="100%" align="center" cellpadding="3" cellspacing="2"  class="Letras">
          <tr class="subtitulo1">
            <th height="4" colspan="2" scope="row"><div align="left">INFORMACION DE LA PLANILLA <%=planilla%>
                  <input name="planilla" type="hidden" id="planilla" value="<%=planilla%>"> 
                  <input name="existe" type="hidden" id="existe" value="true">
              </div></th>
            </tr>
          <tr >
            <th height="5" class="fila" scope="row"><div align="left">FECHA DESPACHO </div></th>
            <td class="letra"><%=fechadesp%></td>
          </tr>
          <tr >
            <th height="13" scope="row" class="fila"><div align="left"><span class="Estilo7">PLACA</span></div></th>
            <td class="letra"><%=placa%></td>
          </tr>
          <tr >
            <th class="fila" scope="row "><div align="left">TRAILER</div></th>
            <td class="letra">
              <div align="left"><%=trailer%> </div></td>
          </tr>
          <tr >
            <th class="fila" scope="row"><div align="left">CEDULA CONDUCTOR </div></th>
            <td class="letra">
              <div align="left"><%=conductor%> <%=nomcond%> </div></td>
          </tr>
          <tr >
            <th class="fila" scope="row"><div align="left">RUTA PLANILLA </div></th>
            <td class="letra"><%=rutaP%>
              <input name="rutaP" type="hidden" id="rutaP2" value="<%=codruta%>">
              <%=codruta%></td>
          </tr>
          <tr >
            <th class="fila" scope="row"><div align="left">CANTIDAD</div></th>
            <td class="letra"><%=peso%> </td>
          </tr>
          <tr >
            <th class="fila" scope="row"><div align="left">VALOR ANTICIPO </div></th>
            <td class="letra"><%=ant%> </td>
          </tr>
          <tr >
            <th scope="row" class="fila"><div align="left" class="Estilo7">PRECINTOS</div></th>
            <td class="letra"><%=precinto%></td>
          </tr>
		  </table>
                <table width="100%" border="1" align="center" class="Letras">
          <tr>
            <td colspan="4" nowrap class="subtitulo1"><div align="left"><strong>LISTA REMESAS RELACIONADAS </strong></div></td>
          </tr>
          <tr bordercolor="#999999" class="tblTitulo">
            <td align="center">REMESA</td>
            <td align="center">CLIENTE</td>
            <td  align="center">ESTANDAR</td>
            <td align="left">PORCENTAJE</td>
          </tr>
          <%  
		 int i=0;
		if(!planilla.equals("")){
		 	 List remesas= model.planillaService.buscarRemesas(planilla);
			 Iterator rem=remesas.iterator();
	  
			  while (rem.hasNext()){
	  			i++;
	  			Remesa remesa1 = (Remesa) rem.next();
				String numremP = remesa1.getNumrem();
if(!remesa1.getNumrem().equals(request.getParameter("anular"))){
		%>
          <tr class="<%=i%2==0?"filagris":"filaazul"%>">
            <td  class="bordereporte">
              <div align="center"><%=remesa1.getNumrem()%> </div></td>
            <td class="bordereporte">
              <div align="center" ><%=remesa1.getCliente()%> </div></td>
            <td  class="bordereporte"><div align="center"><%=remesa1.getDescripcion()%></div></td>
            <td class="bordereporte">
              <input name="por<%=remesa1.getNumrem()%>" type="text" id="por<%=remesa1.getNumrem()%>" size="5" maxlength="3" value="<%=remesa1.getPorcentaje()%>"  onKeyPress="soloDigitos(event,'decNo')">
              <input name="vlr<%=remesa1.getNumrem()%>" type="hidden" id="vlr<%=remesa1.getNumrem()%>" value="<%=remesa1.getVlr_pesos()==0?remesa1.getVlrRem():remesa1.getVlr_pesos()%>">
            </td>
          </tr>
          <%}
		
		}
		}
		i++;
  %>
          <%if(i>1){%>
          <tr class="<%=i%2==0?"filagris":"filaazul"%>">
            <td colspan="3"  class="bordereporte"><div align="right"><b>PORCENTAJE NUEVA REMESA</b></div></td>
            <td class="bordereporte"><input name="porcent" type="text" id="porcent" onKeyPress="soloDigitos(event,'decNo')" value="<%=request.getParameter("porcent")!=null?request.getParameter("porcent"):"0"%>" size="5" maxlength="3"></td>
          </tr>
          <%}%>
        </table>
        <table width="100%" align="center" cellpadding="3" cellspacing="2"  class="Letras">
		  <tr bgcolor="#99CCFF" class="subtitulo1">
            <th colspan="2"  scope="row"><div align="left">INFORMACION DE LA REMESA <%=remesa%></div></th>
          </tr>
          <tr >
            <th width="164" scope="row" class="fila"><div align="left">ESTANDARD JOB </div></th>
            <td width="475" bgcolor="<%=color%>" class="letra"><%=estandar%>
                <input name="standard" type="hidden" id="standard2" value="<%=standar%>">
                <input name="remesa" type="hidden" id="remesa3" value="<%=remesa%>">
      Codigo estandard. <%=standar%>
      <input name="vlrrem" type="HIDDEN" id="vlrrem"  value="<%=vlr%>" readonly>
      </td>
          </tr>
          <tr >
            <th scope="row" class="fila"><div align="left">CLIENTE</div></th>
            <td class="letra" ><div align="left" class="letra"><%=cliente%> </div></td>
          </tr>
          <tr>
            <th scope="row" class="fila"><div align="left"><span class="Estilo7">REMITENTE</span></div></th>
            <td class="letra"><div align="left"><%=remitentes%>
                    <input name="remitentes" type="hidden" id="remitentes2" value="<%=remitentes%>">
            </div></td>
          </tr>
          <tr >
            <th scope="row" class="fila"><div align="left">DESTINATARIO</div></th>
            <td class="letra"><%=destinatarios%>
                <input name="destinatarios" type="hidden" id="destinatarios2" value="<%=destinatarios%>"></td>
          </tr>
          <tr >
            <th class="fila" scope="row"><div align="left">OBSERVACION</div></th>
            <td class="letra"><%=observacion%></td>
          </tr>
          
        </table></td>
    </tr>
  </table>

  <%if(request.getAttribute("remesa")!=null){%>
  <p align="center">
    <img  src="<%=BASEURL%>/images/botones/aceptar.gif" width="90" height="21" border="0" onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);" onClick="if(validarRelacionar(form1)){form1.submit();this.disabled=true;}" style="cursor:hand">
    <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand"> </p>
 <%}%>
 <br>
 <%String mensaje = request.getParameter("mensaje"); 
    if(mensaje!=null){%>
 <br>
 <table border="2" align="center">
   <tr>
     <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
         <tr>
           <td width="229" align="center" class="mensajes"><%=mensaje%></td>
           <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
           <td width="58">&nbsp;</td>
         </tr>
     </table></td>
   </tr>
 </table>
 <%  } %>
</form>
<iframe width=188 height=166 name="gToday:datetime:agenda.js:gfPop:plugins_12.js" id="gToday:datetime:agenda.js:gfPop:plugins_12.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</div>
</body>
</html>
