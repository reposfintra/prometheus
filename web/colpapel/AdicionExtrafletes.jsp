<!--
- Autor : Ing. FERNEL VILLACOB DIAZ
- Date  : Diciembre  2 / 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite aplicar ajustes a la planilla
--%>



<%-- Declaracion de librerias--%>
<%@page session="true"%> 
<%@page import="java.util.*" %>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*,com.tsp.util.*"%>
<html>
<head>

        <title>Aplicar Ajustes a Planillas</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script>  
	<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script> 
        <script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
        
        
        <script>
            function validarNuevoAjuste(){
                var sw = 0;
                if(  extraflete.value=='' ||  valor.value=='' ||  moneda.value=='' ){                
                     if(  extraflete.value==''){
                          alert('Deber� seleccionar el tipo de ajuste a aplicar a la planilla');
                          extraflete.focus();
                          sw = 1;
                     }
                     if(   valor.value=='' && sw==0 ){
                          alert('Deber� digitar el valor del ajuste ');
                          valor.focus();
                          sw = 1;
                     }                       
                     if(  moneda.value==''  && sw==0 ){
                          alert('Deber� digitar la moneda al valor del ajuste ');
                          moneda.focus();
                          sw = 1;
                     }                     
               } 
               
               if( sw==0 )                     
                  location.href='<%=CONTROLLER%>?estado=AdicionAjustes&accion=Planillas&evento=APLICAR&ajuste=' + extraflete.value +'&valor=' + valor.value +'&moneda='+ moneda.value;
         
            }
            
            
            function soloDigitos(e,decReq) {               
                var key = (isIE) ? window.event.keyCode : e.which;
                var obj = (isIE) ? event.srcElement : e.target;
                var isNum = (key > 47 && key < 58) ? true:false;
                var dotOK =  ( decReq=='decOK' && key ==46 && obj.value.indexOf('.')==-1) ? true:false;
                var menos =  ( key ==45 &&  obj.value.indexOf('-')==-1   &&  obj.value.length==0 ) ? true:false;
                
                window.event.keyCode = (!isNum && !dotOK  && !menos && isIE) ? 0:key;
                e.which = (!isNum && !dotOK && isNS && !menos) ? 0:key;   
                return ( menos || isNum || dotOK );
            }
            
        </script>
        
        
</head>
<body onLoad="redimensionar();" onResize="redimensionar();"   onKeyDown="onKeyDown();" >
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Aplicar Extrafletes"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>

   <% String    msj    =  request.getParameter("msj"); 
      Ajuste    ajuste = model.AdicionAjustesPlanillasSvc.getAjuste(); 
      if(  ajuste != null  ){%>
  
            <table width="90%" border="2" align="center">
               <tr>
                  <td>  
                       <table width='100%' align='center' class='tablaInferior'>

                             <tr class="barratitulo">
                                    <td colspan='2' >
                                       <table cellpadding='0' cellspacing='0' width='100%'>
                                         <tr>
                                          <td align="left" width='50%' class="subtitulo1">&nbsp;DATOS PLANILLA</td>
                                          <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"  height="20" align="left"></td>
                                        </tr>
                                       </table>
                                    </td>
                             </tr>
                             
                             <tr class="barratitulo">
                                    <td colspan='2' >
                                       <table cellpadding='0' cellspacing='0' width='100%'  >
                                       
                                         <tr  class='fila'>
                                              <td width='15%'>PLANILLA  </td> <td width='7%'  style="font size:12"> <%= ajuste.getPlanilla() %> </td>
                                              <td width='10%'>PLACA     </td> <td width='7%'  style="font size:12"> <%= ajuste.getPlaca()    %> </td>
                                              <td width='7%'>RUTA       </td> <td width='20%' style="font size:12"> <%= ajuste.getOrigen() +"  - "+ ajuste.getDestino() %></td>
                                              <td width='7%'>ESTADO     </td> <td width='7%'  style="font size:12"> <%= ajuste.getEstado()  %> </td>
                                              <td width='7%'>FACTURA    </td> <td width='*'   style="font size:12"> 
                                                   <% if( !ajuste.getFactura().equals("") ){%>
                                                         <a href='#' onclick=" window.open('<%=CONTROLLER%>?estado=Factura&accion=Detalle&documento=<%= ajuste.getFactura()%>&prov=<%= ajuste.getPropietario()%>&tipo_doc=010','','');">
                                                               <%= ajuste.getFactura() %> 
                                                         </a>                                                   
                                                   <%}else{%>
                                                        <%= ajuste.getFactura() %> 
                                                   <%}%>    
                                              </td>
                                         </tr>
                                         
                                         <tr class='fila'>
                                              <td >PROPIETARIO </td> <td colspan='5' style="font size:12"> <%= ajuste.getPropietario() +" &nbsp "+  ajuste.getNamePropietario() %> </td>
                                         </tr>
                                         
                                         <tr class='fila'>
                                              <td >CONDUCTOR   </td> <td colspan='5' style="font size:12"> <%= ajuste.getConductor()   +" &nbsp "+ ajuste.getNameConductor() %> </td>
                                         </tr>
                                         
                                         <tr class='fila'>
                                              <td >VALOR  OC      </td> <td style="font size:12" colspan='2'> <%=  Util.customFormat(ajuste.getValor())       %>  &nbsp <%= ajuste.getMoneda() %></td>
                                         </tr>
                                        
                                       </table>
                                       <BR>
                                    </td>
                             </tr>
                             
                             
                             
                             
                             <tr class="barratitulo">
                                    <td colspan='2' >
                                       <table cellpadding='0' cellspacing='0' width='100%'>
                                         <tr>
                                          <td align="left" width='50%' class="subtitulo1">&nbsp;AJUSTE A APLICAR</td>
                                          <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"  height="20" align="left"></td>
                                        </tr>
                                       </table>
                                    </td>
                             </tr>
                             
                             
                              <tr class="barratitulo">
                                    <td colspan='2' >
                                       <table cellpadding='0' cellspacing='0' width='100%' >
                                       
                                         <tr  class='fila'>
                                              <td >AJUSTES   </td> 
                                              <td >       
                                                  <select name='extraflete' id='extraflete'>
                                                       <option value=''> Seleccione
                                                       <% List  listaExtrafletes  =  model.AdicionAjustesPlanillasSvc.getListaExtrafletes();
                                                          for(int i=0;i<listaExtrafletes.size();i++){
                                                             Hashtable  ex =  (Hashtable)listaExtrafletes.get(i);%>                                                         
                                                             <option value='<%= (String)ex.get("codigo") %>'> <%= (String)ex.get("descripcion") %>                                                         
                                                         <%}%>  
                                                   </select>
                                              </td>
                                              <td >         
                                                    VALOR 
                                                    <input type='text' name='valor' id='valor' title='Valor a aplicar' maxlength='15'  onKeyPress="soloDigitos(event,'decOK')">                                              
                                              </td>
                                              
                                              <td >MONEDA   </td>
                                              <td >       
                                                  <select name='moneda' id='moneda'>
                                                       <option value=''> Seleccione
                                                       <% List  listaMonedas  =  model.AdicionAjustesPlanillasSvc.getListaMonedas();
                                                          for(int i=0;i<listaMonedas.size();i++){
                                                             Hashtable  mon =  (Hashtable)listaMonedas.get(i);%>                                                         
                                                             <option value='<%= (String)mon.get("codigo") %>'> <%= (String)mon.get("descripcion") %>                                                         
                                                         <%}%>  
                                                       
                                                   </select>
                                              </td>
                                              
                                              <td >         
                                                   <img src="<%=BASEURL%>/images/botones/aplicar.gif" title='Aplicar Ajuste'        name="acept"   height="21"   onClick=" validarNuevoAjuste();"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">                                                       
                                              </td>
                                              
                                         </tr>
                                       
                                         
                                       </table>
                                       <BR>
                                       
                                    </td>
                             </tr>
                             
                             
                             
                             
                              <tr class="barratitulo">
                                    <td colspan='2' >
                                    
                                        <table cellpadding='0' cellspacing='0' width='100%'>
                                           <tr>
                                               <td align="left" width='50%' class="subtitulo1">&nbsp;MOVIMIENTOS</td>
                                               <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"  height="20" align="left"></td>
                                           </tr>
                                        </table>
                                       
                                       
                                       
                                       <table cellpadding='0' cellspacing='0' width='100%' > 
                                       
                                              <tr   class="letra" valign='top'>
                                                  <td width='50%'>
                                                      <table  width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center" >
                                                         <tr  class="tblTitulo" >
                                                               <th >CONCEPTO       </th>
                                                               <th title='Indicador de Valor'      >T              </th>
                                                               <th title='Aplica a'                >A              </th>                                            
                                                               <th title='Valor Moneda Local'      >VALOR ML       </th>  
                                                               <th title='valor Moneda Extranjera' >VALOR ME       </th> 
                                                               <th >MONEDA         </th>
                                                               <th >PROVEEDOR      </th> 
                                                               <th >FECHA          </th> 
                                                               <th >USUARIO        </th>
                                                               <th >FACTURA        </th>
                                                         </tr>                                                          
                                                         
                                                      <%  List movimientos =  ajuste.getMovimientos();
                                                          double total     =  0;
                                                          for(int i=0;i<movimientos.size();i++){
                                                              MoviOC  mov = (MoviOC )movimientos.get(i);        
                                                              total  +=mov.getVlrReal();%>
                                                              <tr  class='<%= (i%2==0?"filagris":"filaazul") %>'>
                                                                    <td nowrap style="font size:11" > 
                                                                        <table cellpadding='0' cellspacing='0' width='100%' border='1'>
                                                                          <tr class='<%= (i%2==0?"filagris":"filaazul") %>'>
                                                                             <td width='20%'  nowrap style="font size:11" >      <%= mov.getConcepto()    %> </td>
                                                                             <td width='*'    nowrap style="font size:11" >&nbsp <%= mov.getDescripcion() %> </td>
                                                                          </tr>
                                                                        </table>
                                                                    </td>
                                                                    <td nowrap style="font size:11" align='center'> <%= mov.getIndicador_valor() %>   </td>
                                                                    <td nowrap style="font size:11" align='center'> <%= mov.getAplica()          %>   </td>                                            
                                                                    <td nowrap style="font size:11" align='right'   <% if ( mov.getIndicador_valor().equals("P") ){ %> title='<%= mov.getVlrReal()  %>' <%}%> > <%=  Util.customFormat( mov.getValor()  )         %>  </td>  
                                                                    <td nowrap style="font size:11" align='right' > <%= Util.customFormat(mov.getValor_me() )  %>  </td> 
                                                                    <td nowrap style="font size:11" align='center'> <%= mov.getMoneda()          %>   </td>
                                                                    <td nowrap style="font size:11"               > <%= mov.getProveedor() +" &nbsp "+ mov.getNameProveedor() %>    </td> 
                                                                    <td nowrap style="font size:11" align='center'> <%= mov.getFecha()           %>   </td> 
                                                                    <td nowrap style="font size:11" align='center'> <%= mov.getUsuario()         %>   </td>
                                                                    <td nowrap style="font size:11" align='center'> <%= mov.getFactura()         %>   </td>
                                                                    
                                                                    
                                                              </tr> 
                                                        <%}%>  
 
                                                      </table>
                                                  </td>
                                              </tr>
                                                 
                                        </table>        
                                   
                               </td>
                            </tr>                     
                             
                           
                              <tr class="barratitulo">
                                    <td colspan='2' >
                                    
                                            <table cellpadding='0' cellspacing='0' width='100%' >
                                                 <tr class="barratitulo">
                                                     <td align="left" width='50%' class="subtitulo1">&nbsp;VALORES ML [  <%= ajuste.getMonedaLocal() %>  ]</td>
                                                     <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"  height="20" align="left"></td>
                                                 </tr>  
                                             </table>    
                                     </td>
                                </tr>
                                        
                                 
                             <tr  class='fila' >
                                <td nowrap width='20%'  > TOTAL MOV. </td>                                      
                                <td nowrap width='*' align='right' style="font size:12"> <%=  Util.customFormat( total  ) %>  </td>  
                             </tr> 

                             <tr class='fila'>
                                <td nowrap  > VALOR OC </td>                                      
                                <td nowrap align='right' style="font size:12"> <%=  Util.customFormat( ajuste.getVlrOCML()  ) %>  </td>  
                             </tr> 

                             <tr  class='fila' >
                                <td nowrap  title='Valor Planilla + Valores Mov. aplican a valor'   > VALOR REAL OC </td>                                      
                                <td nowrap align='right' style="font size:12"> <%=  Util.customFormat( ajuste.getVlrPlanilla()  ) %>  </td>  
                             </tr> 

                             <tr  class='fila' >
                                <td nowrap               > SALDO      </td>                                      
                                <td nowrap align='right' > <%=  Util.customFormat( ajuste.getNeto() ) %>  </td>  
                            </tr> 
                               
                                        
                       </table>
      
                   </td>
                </tr>
            </table>
                             
        
  
   <%}%>
  
  
  
   
  <!-- mensajes -->
    <% if(msj!=null &&  !msj.equals("") ){%>  
         <BR><BR>
         <table border="2" align="center" width="600">
              <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                  <tr>
                    <td align='center' width="420" class="mensajes"><%=msj%></td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="58">&nbsp;</td>
                  </tr>
                </table></td>
              </tr>
        </table>
    <%  } %>


     <p>
            <img src="<%=BASEURL%>/images/botones/salir.gif"       name="Exit"        height="21"  onClick="parent.close();"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
     </p>
         
</div>

</body>
</html>
