<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Desasociar Planilla-Remesa</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</script>
<script type='text/javascript' src="<%=BASEURL%>/js/validarDespacho.js"></script> 
</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=DESPACHO"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
  

  <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
  <br>
  <form name="form1" method="post" action="<%=CONTROLLER%>?estado=Desasociar&accion=PlaRem">
   <table width="50%"  border="2" align="center">
    <tr>
      <td>

    <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="barratitulo">
      <tr>
        <td width="50%" class="subtitulo1" colspan='3'>Buscar Planilla </td>
        <td width="50%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif"></td>
      </tr>
    </table>
    <table width="100%" align="center">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr class="fila">
              <td width="179" >Numero de la planilla</td>
              <td width="306"><input name="planilla" type="text" id="planilla" ></td>
            </tr>
        </table></td>
      </tr>
    </table>	</td>
      </tr>
    </table>
	<div align="center"><br>
	  <input name="imageField" type="image" style="cursor:hand " src="<%=BASEURL%>/images/botones/buscar.gif" align="middle" width="87" height="21" border="0" onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);">
	</div>
	  <br>
	  <%if(model.planillaService.getPlas()!=null){%>
      <br>
      <br>    
	<table width="100%"  border="2" align="center">
    <tr>
      <td>
	<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="barratitulo">
      <tr>
        <td width="50%" class="subtitulo1" colspan='3'>LISTA DE REMESAS DE LA PLANILLA <%=request.getParameter("planilla")%></td>
        <td width="50%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif"></td>
      </tr>
    </table>
    <table width="100%" border="1" align="center" bordercolor="#999999">
      <tr class="tblTitulo">
        <td colspan="2" nowrap><div align="center"><strong>PLANILLA</strong></div></td>
        <td width="55"><div align="center"><strong>REMESA</strong></div></td>
        <td width="53"><div align="center"><strong>PLACA</strong></div></td>
        <td width="168" ><div align="center"><strong>NOMBRE CONDUCTOR</strong></div></td>
        <td width="272" ><div align="center">STANDAR</div></td>
        <td width="87" ><div align="center">PORCENTAJES</div></td>
        <td width="231" ><div align="center">ACCIONES</div></td>
      </tr>
      <% Vector planillas = model.planillaService.getPlas();
	  
	  	for(int i = 0; i<planillas.size(); i++){
	  		Planilla planilla = (Planilla) planillas.elementAt(i);
		%>
      <tr class="<%=i%2==0?"filagris":"filaazul"%>"  title="Desasociar Planilla y Remesa..." >
        <td width="21" rowspan="2" style="cursor:hand" class="bordereporte"><input type="checkbox" name="<%=planilla.getNumrem()%>" value="checkbox" <%=planilla.getReg_status().equals("A") || planillas.size()==1?"disabled":""%> onClick="validarDesasociar('<%=planilla.getNumrem()%>');buscarPorcentaje('<%=planilla.getNumrem()%>');"> </td>
        <td width="42" rowspan="2" class="bordereporte"><%=planilla.getNumpla()%></td>
        <td rowspan="2" class="bordereporte"> <%=planilla.getNumrem()%> </td>
        <td rowspan="2" class="bordereporte"> <%=planilla.getPlaveh()%> </td>
        <td rowspan="2" class="bordereporte"><%=planilla.getNomCond()%></td>
        <td rowspan="2" class="bordereporte"><%=planilla.getSj_desc()%></td>
		<td rowspan="2" class="Simulacion_Hiper bordereporte" style="cursor:hand"><div align="center">
          <input name="des<%=planilla.getNumrem()%>" type="hidden" id="des<%=planilla.getNumrem()%>" value="0" size="5" >
          <input name="vlr<%=planilla.getNumrem()%>" type="hidden" id="vlr<%=planilla.getNumrem()%>" value="<%=planilla.getVlrpla()%>" size="15">
          <input name="por<%=planilla.getNumrem()%>" type="text" size="5" value="<%=planilla.getPesoreal()%>" onKeyPress="soloDigitos(event,'decNo')">
		</div></td>
        <td class="Simulacion_Hiper bordereporte" style="cursor:hand"><%if(planilla.getReg_status().equals("A")){%><div onClick="window.open('<%=CONTROLLER%>?estado=Remesa&accion=Search&remesa=<%=planilla.getNumrem()%>','','menubar=no,personalbar=no,scrollbars=no,toolbar=no,resizable=yes')">
          <ul>
            <li>Crear una nueva planilla para esta remesa</li>
          </ul>
        </div><%}%>
		<%if(planillas.size()==1){%><div onClick="window.open('<%=CONTROLLER%>?estado=Planilla&accion=Search&colpapel=ok&nremesa=ok&planilla=<%=planilla.getNumpla()%>','','menubar=no,personalbar=no,scrollbars=no,toolbar=no,resizable=yes')">
		  <ul>
		    <li>Crear una nueva remesa para esta planilla</li>
		    </ul>
		</div><%}%>
		</td>
        
      </tr>
      <tr class="<%=i%2==0?"filagris":"filaazul"%>"  style="cursor:hand" title="Desasociar Planilla y Remesa..." >
        <td class="Simulacion_Hiper bordereporte"  style="cursor:hand"><%if(planilla.getReg_status().equals("A")){%><div onClick="window.open('<%=BASEURL%>/colpapel/relacionar.jsp?remesa=<%=planilla.getNumrem()%>','','menubar=no,personalbar=no,scrollbars=no,toolbar=no,resizable=yes')">
          <ul>
            <li>Asignar una planilla existente a esta remesa</li>
          </ul>
        </div><%}%>
			<%if(planillas.size()==1){%><div onClick="window.open('<%=BASEURL%>/colpapel/RemesaExistente.jsp?planilla=<%=planilla.getNumpla()%>','','menubar=no,personalbar=no,scrollbars=no,toolbar=no,resizable=yes')">
			  <ul>
			    <li>Asignar una remesa existente a esta planilla</li>
			    </ul>
			</div><%}%>
			</td>
        </tr>
      <%}
  %>
    </table>
	</td>
      </tr>
    </table>
	<%if(planillas.size()>1){%>
    <div align="center"><br>

      <br>

      <input name="imageField3" type="image" src="<%=BASEURL%>/images/botones/restablecer.gif" width="119" height="21" border="0" onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);" onClick="this.disabled=true;form1.action = '<%=CONTROLLER%>?estado=Desasociar&accion=PlaRem&planilla=<%=request.getParameter("planilla")%>';form1.submit();">
      <input name="imageField2" type="image" src="<%=BASEURL%>/images/botones/aceptar.gif" width="90" height="21" border="0" onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);" onClick="this.disabled=true;form1.action = '<%=CONTROLLER%>?estado=Desasociar&accion=PlaRem&desasociar=ok&planilla=<%=request.getParameter("planilla")%>';form1.submit();" >
      <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand"><br>
      <%}%>
    </div>
	<%}%>
  </form>
 <%if(request.getParameter("mensaje")!=null){%>
	<%--Mensaje de informacion--%>
	<table border="2" align="center">
		<tr>
			<td>
				<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
					<tr>
						<td width="229" align="center" class="mensajes"><%=request.getParameter("mensaje")%></td>
						<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
						<td width="58">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<%}%>
</div>
</body>
</html>
