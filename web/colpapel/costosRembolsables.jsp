<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Extrafletes...</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/validar.js">
</script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">

</head>

<body onLoad="<%if(request.getParameter("cerrar")!=null){%>window.close();<%}%><%if(request.getParameter("mensaje")!=null){%>alert('<%=request.getParameter("mensaje")%>')<%}%>">

<%
String no_sj = request.getParameter("sj");
	String voc = request.getParameter("voc");
	String vunidad = "1";
/*if(request.getParameter("cr").equals("0")){
out.println("RECARGUE EL VALOR");
		model.sjextrafleteService.listaCostos(no_sj);
	}*/%>
<form action="<%=CONTROLLER%>?estado=Extraflete&accion=Aplicar&costos=si" method="post" name="form2" id="form2">
  <table width="95%"  border="2" align="center" cellpadding="0" cellspacing="0" bgcolor="#EFEBDE" class="filaresaltada">
	<tr>
	  <td>
        <table width="100%" align="center" class="tablaInferior">
          <tr>
            <td height="22" colspan=2 class="subtitulo1"><span class="titulo"><strong>
              <input name="sj" type="hidden" id="sj2" value="<%=request.getParameter("sj")%>"> 
              COSTOS REEMBOLSABLES
              <input name="cr" type="hidden" id="cr" value="<%=request.getParameter("cr")%>">
              <br>
            </strong></span></td>
            <td width="308" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
        </table>
        <table width="100%"  border="1" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC">
	<tr class="filaresaltada">
      <td height="32" colspan="9"><strong>VALOR TOTAL </strong> :
        <input name="totalexto" type="text" id="totalexto" value="<%=com.tsp.util.Util.customFormat(Float.parseFloat(request.getParameter("cr")!=null?request.getParameter("cr"):"0"))%>" readonly>  
        <input name="totalext" type="hidden" id="totalext" value="<%=request.getParameter("cr")!=null?request.getParameter("cr"):"0"%>"></td>
    </tr>
	<tr bordercolor="#999999" class="tblTitulo">
	  <td height="0" colspan="5">COSTOS</td>
      <td>INGRESOS</td>
	  <td colspan="2">LIQUIDACION</td>
	  <td>&nbsp;</td>
	</tr>
	<tr bordercolor="#999999" class="tblTitulo">
	  <td height="24" colspan="2"><div align="center"><strong>DESCRIPCION</strong></div></td>
	  <td width="14%"><div align="center"><strong>VALOR COSTO </strong></div></td>
	  <td width="9%" height="24"><div align="center"><strong>PORCENTAJE</strong></div></td>
	  <td width="8%"><div align="center"><strong>CANTIDAD</strong></div></td>
      <td width="14%"><div align="center"><strong>VALOR</strong></div></td>
	  <td width="7%"><div align="center"><strong>TOTAL COSTO </strong></div></td>
	  <td width="7%">TOTAL INGRESO </td>
	  <td width="17%">PROVEEDOR</td>
	</tr>
	<%
	
	
    Vector fletes = model.sjextrafleteService.getC_reembolsables();

	for(int i= 0; i<fletes.size(); i++){
			
			SJExtraflete s =(SJExtraflete) fletes.elementAt(i);
			String costo_fv =s.getVf_costo().equals("F")?"readonly":"";
			String ing_fv=s.getVf_ingreso().equals("F")?"readonly":"";
			String unidad=s.getUnidades().equals("N")?"disabled":"";
			String reembolsable =s.getReembolsable().equals("N")?"disabled":"";
			String tipo_valor ="";
			String tipo_porcent="";
			String discosto = "";
			if(s.getClase_valor().equals("V")){
				tipo_porcent="disabled";
				s.setPorcentaje(0);
			}
			else{
				tipo_valor ="disabled";
				float vdoc = Float.parseFloat(voc);
				float vpoc =(vdoc * s.getPorcentaje())/100;
				s.setValor_costo(com.tsp.util.Util.redondear(vpoc,0));
				unidad = "disabled";
				discosto = "disabled";
				vunidad = "0";
				
			}
		%>
	<tr class="<%=i%2==0?"filagris":"filaazul"%>">
	  <td width="3%"><input name="chek<%=s.getCod_extraflete()%>" type="checkbox" value="checkbox" onClick="calcularExtraflete('<%=s.getCod_extraflete()%>');" <%if(s.isSelec()){%> checked <%}%>></td>
      <td width="21%"><%=s.getDescripcion()%></td>
      <td><input name="cost<%=s.getCod_extraflete()%>" type="text" id="cost<%=s.getCod_extraflete()%>" onKeyUp="calcularValorExIng('<%=s.getCod_extraflete()%>');calcularValorEx('<%=s.getCod_extraflete()%>');" value="<%=com.tsp.util.Util.customFormat(s.getValor_costo())%>" <%=costo_fv%> <%=discosto%> onKeyPress="soloDigitos(event,'decNo')" <%if(s.isSelec()){%> disabled <%}%>>        
        <input type="hidden" name="cost<%=s.getCod_extraflete()%>o" id="cost<%=s.getCod_extraflete()%>o"  value="<%=s.getValor_costo()%>"></td>
      <td><div align="center">
        <input name="p<%=s.getCod_extraflete()%>" type="text" id="p<%=s.getCod_extraflete()%>" value="<%=s.getPorcentaje()%>" size="4" <%=tipo_porcent%> <%=costo_fv%>>
      </div></td>
      <td><div align="center">
        <input name="cant<%=s.getCod_extraflete()%>" type="text" id="cant<%=s.getCod_extraflete()%>" onKeyUp="calcularValorEx('<%=s.getCod_extraflete()%>');" value="<%=s.getCantidad()%>" size="7" <%=unidad%> onKeyPress="soloDigitos(event,'decNo')" <%if(s.isSelec()){%> disabled <%}%>>
        <input name="cant<%=s.getCod_extraflete()%>o" type="hidden" id="cant<%=s.getCod_extraflete()%>o" value="<%=s.getCantidad()%>">
      </div></td>
      <td><div align="center">
        <input name="ingreso<%=s.getCod_extraflete()%>" type="text" id="ingreso<%=s.getCod_extraflete()%>" onKeyUp="calcularValorExIng('<%=s.getCod_extraflete()%>');calcularValorEx('<%=s.getCod_extraflete()%>');" value="<%=com.tsp.util.Util.customFormat(s.getValor_ingreso())%>" <%=ing_fv%> <%=reembolsable%> onKeyPress="soloDigitos(event,'decNo')" <%if(s.isSelec()){%> disabled <%}%>>
        <input name="ingresoO<%=s.getCod_extraflete()%>" type="hidden" id="ingresoO<%=s.getCod_extraflete()%>"  value="<%=s.getValor_ingreso()%>" >
        <input type="hidden" name= "ingreso<%=s.getCod_extraflete()%>o"  id="ingreso<%=s.getCod_extraflete()%>o" value="<%=s.getValor_ingreso()%>">
      </div></td>
	  <td><input name="liq<%=s.getCod_extraflete()%>" type="text" id="liq<%=s.getCod_extraflete()%>" value="<%=com.tsp.util.Util.customFormat(s.getCantidad()*s.getValor_costo())%>" readonly onKeyPress="soloDigitos(event,'decNo')">
	    <input type="hidden" name="liq<%=s.getCod_extraflete()%>o" id="liq<%=s.getCod_extraflete()%>o" value="<%=s.getCantidad()*s.getValor_costo()%>"></td>
	  <td><input name="liqIng<%=s.getCod_extraflete()%>" type="text" id="liqIng<%=s.getCod_extraflete()%>" value="<%=com.tsp.util.Util.customFormat(s.getCantidad()*s.getValor_ingreso())%>" readonly onKeyPress="soloDigitos(event,'decNo')">
        <input type="hidden" name="liqIng<%=s.getCod_extraflete()%>o" id="liqIng<%=s.getCod_extraflete()%>o" value="<%=s.getCantidad()*s.getValor_ingreso()%>"></td>
	  <td><input type="text" name="provee<%=s.getCod_extraflete()%>" id="provee<%=s.getCod_extraflete()%>" value="<%=s.getProveedor()%>">
	    <a style="cursor:hand " onClick="window.open('<%=BASEURL%>/consultas/consultasProveedorReembolsable.jsp?proveedor='+form2.provee<%=s.getCod_extraflete()%>.name)" class="Simulacion_Hiper">Consultar nit</a></td>
	</tr>
	<%}%>
	<tr>
	  <td colspan="9"><div align="center">
	    <img src="<%=BASEURL%>/images/botones/aceptar.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="aplicarReembolsable();" onMouseOut="botonOut(this);" style="cursor:hand">		 <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand"></div></td>
    </tr>
  </table>  </td>
    </tr>
  </table>
</form>
</body>
</html>
