<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Relacionar una nueva remesa a una planilla</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/validar.js">
</script>
<link href="<%=BASEURL%>/css/letras.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/Style.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/StyleM.css" rel="stylesheet" type="text/css">
<link href="../css/letras.css" rel="stylesheet" type="text/css">
<link href="../css/Style.css" rel="stylesheet" type="text/css">
<link href="../css/StyleM.css" rel="stylesheet" type="text/css">
</head>

<body>
<%//Inicializo variables
String estandar="", trailer="", cliente="", fecha="",standar="", observacion="",docuinterno="", remitentes="", destinatarios="", remesa="", planilla="", cedula="", nombre="", placa="", precinto=""; 
float peso=0, valor=0;
List planillas=new LinkedList();
String remesalista=request.getParameter("remesalista"); 
String color="";
if(request.getAttribute("standar")!=null){
	color = (String)request.getAttribute("standar");
}

%>
<form name="form1" method="post" action="colpapel/agregarRemesaLista2.jsp" >
  <div align="center">
    <p>
      <%
	  
	  Usuario usuario = (Usuario) session.getAttribute("Usuario");
	  java.util.Date date = new java.util.Date();
	 
      List plaerror=new LinkedList();
	  model.remesaService.buscarPlanillaError(remesalista);
      if(model.remesaService.getPlasError()!=null){
      	plaerror = model.remesaService.getPlasError();
		session.setAttribute("plaerror", plaerror);
      }
	 
     
  %>
      <input name="remesalista" type="hidden" id="remesalista" value="<%=remesalista%>">
</p>
  </div>
  <table width="655" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="#ECE0D8" class="Letras">
    <tr bgcolor="#FFA928" class="Estilo6">
      <th class="Estilo6" scope="row">LISTA DE PLANILLAS A RELACIONAR</th>
    </tr>
    <tr class="Estilo6">
      <td scope="row"><div align="left">
        <table width="100%"  border="1" cellpadding="2" cellspacing="1" bordercolor="#CCCCCC" class="Letras">
          <tr bgcolor="#99CCFF">
            <td width="12%"><strong>PLANILLA</strong></td>
            <td width="18%"><strong>FECHA DESPACHO </strong></td>
            <td width="14%"><strong>PLACA</strong></td>
            <td width="17%"><strong>CONDUCTOR</strong></td>
            </tr>
          <% Iterator it=plaerror.iterator();
	  while (it.hasNext()){
	     Planilla plani = (Planilla) it.next();
	   %>
          <tr>
            <td height="20"><%=plani.getNumpla()%></td>
            <td><%=plani.getFecdsp().substring(0,10)%></td>
            <td><%=plani.getPlaveh()%></td>
            <td><strong><%=plani.getCedcon()%><br> 
              <%=plani.getNomCond()%></strong></td>
            </tr>
          <%}%>
        </table> 
        </div>        </td>
    </tr>
    <tr bgcolor="#FFA928" class="Estilo6">
      <th scope="row">&nbsp;INFORMACION DE LA NUEVA REMESA</th>
    </tr>
    <tr class="Estilo6">
      <th scope="row"><table width="100%" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="#ECE0D8" class="Letras">
        <tr class="Estilo6">
          <th width="164" rowspan="2" scope="row"><div align="left">STANDARD/CLIENTE</div></th>
          <td colspan="2"><span class="Simulacion_Hiper" style="cursor:hand " onClick="window.open('<%=BASEURL%>/consultas/consultasClientes.jsp','','HEIGHT=200,WIDTH=600,SCROLLBARS=YES,RESIZABLE=YES')">Consultar clientes...</span>
            </td>
        </tr>
        <tr class="Estilo6">
          <td colspan="2"><input name="cliente" type="text" id="cliente2"  onKeyPress="soloDigitos(event,'decNo')" maxlength="6">
            <input type="button" name="Submit3" value="Buscar..." onClick="buscarClient3();">
            <input name="clienteR" type="hidden" id="clienteR" value="<%=request.getParameter("cliente")%>">
            <input name="standard_nom" type="hidden" id="standard_nom" value="<%=(String)request.getAttribute("std")%>">
            <input name="remitentes" type="hidden" id="remitentes" value=" ">
            <input name="docinterno" type="hidden" id="docinterno" value=" ">
            <input name="fechadesp" type="hidden" id="fechadesp" value=" ">
            <input name="placa" type="hidden" id="placa" value=" ">
            <input name="destinatarios" type="hidden" id="destinatarios" value=" ">
            <input name="trailer" type="hidden" id="trailer" value=" ">
            <input name="conductor" type="hidden" id="conductor" value=" ">
            <input name="ruta" type="hidden" id="ruta2" value=" ">
            <input name="toneladas" type="hidden" id="toneladas" value=" ">
            <input name="valorpla" type="hidden" id="valorpla" value=" ">
            <input name="anticipo" type="hidden" id="anticipo" value=" ">
            <input name="precintos" type="hidden" id="precintos" value=" ">
            <input name="observacion" type="hidden" id="observacion" value=" ">
            <br>
            <%if(request.getAttribute("cliente")!=null){%>
            <table width="100%" border="0" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" class="Letras">
              <tr>
                <td><strong><%=(String) request.getAttribute("cliente")%></strong></td>
              </tr>
              <tr>
                <td><strong>Agencia Due&ntilde;a del Cliente : <%=(String) request.getAttribute("agency")%></strong></td>
              </tr>
</table>            
            <%}%></td>
        </tr>
        <%if(request.getAttribute("std")==null){%>
        <tr class="Estilo6">
          <td rowspan="2" ><strong>RUTA</strong>
          <td width="86"><strong>ORIGEN</strong></td>
          <td width="371">
            <%TreeMap ciudades = model.stdjobdetselService.getCiudadesOri(); 
	  String corigen="";
	  if(request.getParameter("origen")!=null){
	  	corigen = request.getParameter("origen");
	  }
	  String cdest="";
	  if(request.getParameter("destino")!=null){
	  	cdest = request.getParameter("destino");
	  }
	  %>
            <input:select name="ciudadOri" options="<%=ciudades%>" attributesText="style='width:100%;' onChange='buscarDestinos3()'" default='<%=corigen%>'/> </td>
        </tr>
        <tr class="Estilo6">
          <td><strong>DESTINO</strong></td>
          <td>
            <%TreeMap ciudadesDest = model.stdjobdetselService.getCiudadesDest(); %>
            <input:select name="ciudadDest" options="<%=ciudadesDest%>" attributesText="style='width:100%;'"  default='<%=cdest%>'/> </td>
        </tr>
        <%if(ciudadesDest.size()>0){%>
        <tr class="Estilo6">
          <td colspan="3" >
            <div align="center">
              <input type="button" name="Submit2" value="Buscar Standard..." onClick="buscarStandard3();">
            </div>
        </tr>
        <% }%>
        <%if(request.getAttribute("ok")!=null){%>
        <tr class="Estilo6">
          <td ><strong> ESTANDARD JOB </strong>
          <td colspan="2"><%TreeMap stdjob = model.stdjobdetselService.getStdjobTree(); %>
              <input:select name="standard" options="<%=stdjob%>" attributesText="style='width:100%;'"/> </td>
        </tr>
        <tr class="Estilo6">
          <td colspan="3" >
            <div align="center">
              <input type="submit" name="Submit2" value="Continuar...">
          </div></td>
        </tr>
        <%}
	}else{%>
        <tr class="Estilo6">
          <td ><strong>ESTANDARD JOB </strong></td>
          <td colspan="2" ><%=(String) request.getAttribute("std")%>
              <input name="standard" type="hidden" id="standard" value="<%=(String)request.getAttribute("sj")%>">
          </td>
        </tr>
        <tr class="Estilo6">
          <td colspan="3" >
            <div align="center">
              <input type="submit" name="Submit4" value="Continuar...">
          </div></td>
        </tr>
        <%}%>
      </table></th>
    </tr>
  </table>
  <p align="center">&nbsp;  </p>
</form>
<iframe width=188 height=166 name="gToday:datetime:agenda.js:gfPop:plugins_12.js" id="gToday:datetime:agenda.js:gfPop:plugins_12.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</body>
</html>
