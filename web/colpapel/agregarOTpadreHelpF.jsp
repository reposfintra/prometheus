<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE>Funcionamiento de la ventana de Asociación de Remesa Padre</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
</HEAD>
<BODY>  

  <table width="95%"  border="2" align="center">
    <tr>
      <td>
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">Asignación de Remesa Padre </div></td>
          </tr>
          <tr><td height="20"></td></tr>
          <tr class="subtitulo1">
            <td align="center" height="30">Agregar a una remesa Padre EXISTENTE</td>
          </tr>
          <tr>
            <td  height="18"  class="ayudaHtmlTexto"><p>&nbsp;</p>
            <p>Esta opcion permite buscar una remesa Padre, para posteriormente asociarle una remesa</p>            
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/colpapel/img1.JPG"  ></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>Se listan las remesas asociadas al remesa padre dada</p></td>
          </tr>
          
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/colpapel/img2.JPG" ></div></td>
          </tr>   
          
          <tr>
            <td  class="ayudaHtmlTexto"><br><p>Para asociar otra(s) remesa(s) a la remesa padre buscada, seleccione la opcion "Agregar a una remesa Padre" del lado derecho, digite la remesa o remesas separadas por COMA "," y finalmente haga click en AGREGAR.<br>
                </p></td>
          </tr>
          
          <tr>
            <td height="18"  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/colpapel/img3.JPG"  ></div></td>
          </tr>
          
          
          <tr>
            <td  class="ayudaHtmlTexto"><br><p>Se muestra un mensaje de confirmación con la remesa padre y las remesas que se asociaron.<br>
                </p></td>
          </tr>
          
          <tr>
            <td height="18"  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/colpapel/img4.JPG"  ></div></td>
          </tr>
          <tr><td height="20"></td></tr>
          <tr class="subtitulo1">
            <td align="center" height="30">Agregar a una remesa Padre NUEVA</td>
          </tr>
                
          
          <tr>
            <td  class="ayudaHtmlTexto"><br><p>Para asociar una remesa a una remesa Padre nueva, se selecciona la opción "Agregar a una remesa Padre" del lado derecho, digite la remesa o remesas separadas por COMA ","<br>
                </p></td>
          </tr>
          
          <tr>
            <td height="18"  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/colpapel/img5.JPG"  ></div></td>
          </tr>
          
          
          <tr>
            <td  class="ayudaHtmlTexto"><br><p>Luego de hacer click en ACEPTAR, se genera una remesa padre nueva, a la cual le puede agregar una descripción.<br>
                Click en GENERAR para asociar las remesas, se muestra mensaje de confirmación.
                </p></td>
          </tr>
          
          <tr>
            <td height="18"  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/colpapel/img6.JPG"  ></div></td>
          </tr>
          
      </table></td>
    </tr>
  </table>
  <table width="416" align="center">
	<tr>
	<td height="30" align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	</td>
	</tr>
</table>
</BODY>
</HTML>
