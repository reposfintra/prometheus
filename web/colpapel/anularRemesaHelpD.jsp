<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Descripción campos anular remesa</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" height="" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Despacho </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2"> Buscar Planilla </td>
        </tr>
        <tr>
          <td width="149" class="fila"> Numero de la Remesa:</td>
          <td width="525"  class="ayudaHtmlTexto">Campo para ingresar el c&oacute;digo de la remesa descrito por numeros y letras. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> Boton Buscar</td>
          <td width="525"  class="ayudaHtmlTexto">Boton que realiza una busqueda segun el codigo de la remesa digitado. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> Boton Anular</td>
          <td width="525"  class="ayudaHtmlTexto">Boton que realiza la anulación de la remesa previamente buscada. </td>
        </tr>
    </table>    </td>
  </tr>
</table>
<p>&nbsp; </p>
</body>
</html>
