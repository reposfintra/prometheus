<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE></TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</HEAD>
<BODY> 
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE DESPACHOS WEB </div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa de Despacho </td>
          </tr>
          <tr>
            <td  height="18"  class="ayudaHtmlTexto">Para entrar a la p&aacute;gina WEB de despachos de FINTRAVALORES se debe acceder a trav&eacute;s de  internet explorer la p&aacute;gina <A 
href="http://localhost:8080/fintravalores/">http://localhost:8080/fintravalores/</A>.            </td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">Aqu&iacute; se ingresa el usuario y el password y se escoge el perfil del funcionario. Los Jefes de Negocio o despachadores deben escoger el perfil JEFE DE NEGOCIO</span></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=410 src="<%=BASEURL%>/images/ayuda/despacho/image002.jpg" width=567 
border=0 v:shapes="_x0000_i1164"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">Aparece el Menu. Se Ingresa a trav&eacute;s de la opci&oacute;n OPERACI&Oacute;N/DESPACHO/CARGA GENERAL/Inicio</span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=411 src="<%=BASEURL%>/images/ayuda/despacho/image004.jpg" width=566 
border=0 v:shapes="_x0000_i1145"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">En la siguiente pantalla se puede colocar el n&uacute;mero del est&aacute;ndar si se conoce, o el c&oacute;digo del cliente, si se conoce o utilizar la opci&oacute;n Consultar clientes.</span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=384 src="<%=BASEURL%>/images/ayuda/despacho/image006.jpg" width=567 
border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">Si se ingresa por el link de Consultar clientes, aparece la siguiente pantalla, en donde se coloca el nombre del cliente. Se puede utilizar el s&iacute;mbolo % como comod&iacute;n. Es decir que donde se coloque puede existir cualquier informaci&oacute;n adicional que lo reemplace.</span></td>
          </tr><tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">Y se presiona BUSCAR</span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=214 src="<%=BASEURL%>/images/ayuda/despacho/image008.jpg" width=567 
border=0 v:shapes="_x0000_i1052"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">En la pantalla aparecen los nombres de los clientes que coincidan con la b&uacute;squeda realizada. Al nombre que se requiera, se le presiona CLICK</span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=214 src="<%=BASEURL%>/images/ayuda/despacho/image010.jpg" width=567 
border=0 v:shapes="_x0000_i1054"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">En la siguiente pantalla se presiona BUSCAR (en el cliente) y se escoge el origen y el destino del viaje y se presiona BUSCAR</span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=321 src="<%=BASEURL%>/images/ayuda/despacho/image012.jpg" width=567 
border=0 v:shapes="_x0000_i1061"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">Aparecen los est&aacute;ndares que cumplen con el origen y el destino relacionado y se presiona ACEPTAR</span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=340 src="<%=BASEURL%>/images/ayuda/despacho/image014.jpg" width=566 
border=0 v:shapes="_x0000_i1083"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">En la siguiente pantalla se presiona el link Agregar Remitentes</span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=428 src="<%=BASEURL%>/images/ayuda/despacho/graficos1.JPG" width=787></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">Se selecciona el remitente correspondiente, d&aacute;ndole clic al campo Seleccionar y luego SALIR.</span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=243 src="<%=BASEURL%>/images/ayuda/despacho/image023.jpg" width=520 
border=0 v:shapes="_x0000_i1091"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">Luego se selecciona el campo Agregar Destinatarios</span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=411 src="<%=BASEURL%>/images/ayuda/despacho/image024.jpg" width=566 
border=0 v:shapes="_x0000_i1093"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">Se selecciona la ciudad destino. </span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=435 src="<%=BASEURL%>/images/ayuda/despacho/image025.jpg" width=524 
border=0 v:shapes="_x0000_i1104"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">Luego se selecciona el destinatario correspondiente y si es necesario ingresar los documentos relacionados a cada destinatario se presiona el link correspondiente</span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=435 src="<%=BASEURL%>/images/ayuda/despacho/image026.jpg" width=524 
border=0 v:shapes="_x0000_i1105"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">En la siguiente pantalla se colocan los documentos relacionados con el destinatario, escogiendo el tipo de documento correspondiente. Si este documento tiene otros relacionados se ingresan. Si se presiona el bot&oacute;n (+) se podr&aacute;n agregar mas documentos relacionados</span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=339 src="<%=BASEURL%>/images/ayuda/despacho/image028.jpg" width=566 
border=0 v:shapes="_x0000_i1107"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=411 src="<%=BASEURL%>/images/ayuda/despacho/image029.jpg" width=566 
border=0 v:shapes="_x0000_i1109"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">En esta pantalla pantalla se selecciona el tipo de documento y se coloca el numero. Si se requieren agregar mas documentos se presiona el bot&oacute;n Agregar</span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=328 src="<%=BASEURL%>/images/ayuda/despacho/image030.jpg" width=508 
border=0 v:shapes="_x0000_i1111"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto">Posteriormente se colocan las cantidades a facturar (que dependen de la unidad de facturaci&oacute;n) si es M3, las cantidades son en M3, si es en viajes se colocar&aacute; 1 (1 viaje), etc).</td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto">Si se requiere se colocan las cantidades de empaque y se escogen las unidades.</td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">Se coloca la placa, el numero del trailer (se define si es de TSP&nbsp; NO), Los contenedores (definiendo si son de TSP o no) y los precintos de esos contenedores.</span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">Se escoge la ruta de la planilla, la v&iacute;a por la cual va a transitar el veh&iacute;culo y se colocan las cantidades teniendo en cuenta la unidad del flete.</span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=420 src="<%=BASEURL%>/images/ayuda/despacho/grafico2.JPG" width=793></div></td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">En caso de que se necesite cambiar el flete se presiona APLICAR OTRO VALOR DE FLETE y aparecer&aacute; la siguiente pantalla. Si el valor del flete nuevo es mayor que el definido en el estandar. Se debe colocar una Justificaci&oacute;n para este aumento. Se presiona ACEPTAR</span></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=212 src="<%=BASEURL%>/images/ayuda/despacho/image044.jpg" width=567 
border=0 v:shapes="_x0000_i1115"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">En el caso que se vayan a agregar extrafletes se presiona la opci&oacute;n correspondiente.</span></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">Se ingresan los precintos en caso que no sean de contenedores. </span></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">Adicionalmente se ingresa fecha de salida, entrada a cargue, salida cargue, entrada descargue, salida descargue.</span></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">Se debe verificar los descuentos relacionados y marcar los requeridos.</span></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">Se presiona VALIDAR.</span></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><P class=ayudaHtmlTexto>El sistema verifica que toda la informaci&oacute;n ingresada est&eacute; correcta, de lo contrario la marca en rojo. Se debe corregir y volver a presionar VALIDAR. Una vez est&eacute; todo correcto, se presiona ACEPTAR</P>    </td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=411 src="<%=BASEURL%>/images/ayuda/despacho/image046.jpg" width=566 
border=0 v:shapes="_x0000_i1117"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">Se verifica la c&eacute;dula del conductor. Que sea la correspondiente al conductor que realizar&aacute; el viaje</span></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=119 src="<%=BASEURL%>/images/ayuda/despacho/image047.jpg" width=234 
border=0 v:shapes="_x0000_i1161"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">El sistema verifica el vencimiento del SOAT y la fecha de vencimiento del pase del conductor.</span></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=105 src="<%=BASEURL%>/images/ayuda/despacho/image049.jpg" width=567 
border=0 v:shapes="_x0000_i1165"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">Se coloca ACEPTAR y el sistema genera la planilla y la remesa.</span></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/despacho/image051.jpg" width="566" height="339"></div></td>
</tr>
      </table></td>
    </tr>
  </table>
</BODY>
</HTML>
