<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Descripcion campos agregar remesa</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" height="" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Despacho </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2"> Buscar Planilla </td>
        </tr>
        <tr>
          <td width="149" class="fila"> Numero de la Planilla:</td>
          <td width="525"  class="ayudaHtmlTexto">Campo para ingresar el c&oacute;digo de planilla descrito por numeros y letras. </td>
        </tr>
        <tr>
          <td width="149" class="fila"> Boton Buscar </td>
          <td width="525"  class="ayudaHtmlTexto">Boton que realiza la busqueda de el codigo digitado en el campo <span class="fila">Numero de la Planilla:</span></td>
        </tr>
    </table>    </td>
  </tr>
</table>
<p>&nbsp;</p>

<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Despacho </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2"> Informacion de la nueva remesa </td>
        </tr>
        <tr>
          <td  class="fila"><strong>Agregar Remitentes</strong></td>
          <td  class="ayudaHtmlTexto">Link que direcciona a una pantalla donde se pueden seleccionar Remitentes. </td>
        </tr>
        <tr>
          <td class="fila">Agregar Destinatarios</td>
          <td  class="ayudaHtmlTexto">Link que direcciona a una pantalla donde se pueden seleccionar Destinatarios.</td>
        </tr>
<tr>
          <td class="fila">Agregar Documentos Relacionados</td>
          <td  class="ayudaHtmlTexto">Link que direcciona a una pantalla donde se relacionan los documentos internos.</td>
        </tr>
		<tr>
          <td width="149" class="fila"> Cantidad a Facturar</td>
          <td width="525"  class="ayudaHtmlTexto">Campo que muestra la cantidad a facturar segun el standar job, esta cantidad puede ser modificada. </td>
        </tr>
        <tr>
          <td width="149" class="fila"> Unidad de Facturacion</td>
          <td width="525"  class="ayudaHtmlTexto">Campo que describe la unidad en la que se encuentran definida la carga. </td>
        </tr>
        <tr>
          <td  class="fila"><strong>Unidad de Empaque </strong></td>
          <td  class="ayudaHtmlTexto">Campo de seleccion donde se encuentra una lista de las diferentes unidades de empaque. </td>
        </tr>
        <tr>
          <td class="fila"> No Facturable </td>
          <td  class="ayudaHtmlTexto">Campo de chequeo para indicar si la remesa es o no facturable.</td>
        </tr>
		<tr>
          <td class="fila">Cross Docking </td>
          <td  class="ayudaHtmlTexto">Campo de chequeo para indicar si el viaje va o no va con Cross Docking.</td>
        </tr>
		<tr>
          <td width="149" class="fila"> OBSERVACIONES </td>
          <td width="525"  class="ayudaHtmlTexto">Campo de texto donde se digitan las observaciones pertinentes a la nueva remesa agregada. </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" height="" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Despacho </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2"> <table width="181">
            <tr>
              <td class="subtitulo1" colspan="2">Nuevo Flete de la Planilla </td>
            </tr>
          </table>            </td>
        </tr>
       <tr>
          <td width="149" class="fila"> Ruta planilla</td>
          <td width="525"  class="ayudaHtmlTexto">Campo de selecci&oacute;n donde se encuentran las distintas rutas relacionadas a la planilla. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> Via</td>
          <td width="525"  class="ayudaHtmlTexto">Campo de selecci&oacute;n donde se encuentran las vias relacionadas a las rutas. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> Cantidad a Despachar </td>
          <td width="525"  class="ayudaHtmlTexto">Campo que recibe el numero de unidades a despachar, por ejemplo: viajes, Toneladas, galones, etc. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> Valor del Flete</td>
          <td width="525"  class="ayudaHtmlTexto">Campo de seleccion donde se escoge el Valor del flete.. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> APLICAR OTRO VALOR DE FLETE (F8)</td>
          <td width="525"  class="ayudaHtmlTexto">link que direcciona a una pantalla con un campo para ingresar nuevo valor en moneda del flete. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> Nuevo Valor Aplicado</td>
          <td width="525"  class="ayudaHtmlTexto">campo donde se muestra si hubo un cambio o no en el valor del flete a aplicar. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> INGRESAR/ MODIFICAR EXTRAFLETES</td>
          <td width="525"  class="ayudaHtmlTexto">link que direcciona a una pantalla donde se ingresan o modifican los valores de los extrafletes. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> INGRESAR/ MODIFICAR COSTOS REEMBOLSABLES</td>
          <td width="525"  class="ayudaHtmlTexto">link que direcciona auna pantalla donde se ingresan o modifican los valores de los costos reembolsables. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> Valor total en extrafletes</td>
          <td width="525"  class="ayudaHtmlTexto">Campo que muestra el valor total de los extrafletes. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> Anticipo maximo permitido</td>
          <td width="525"  class="ayudaHtmlTexto">Campo que muestra el valor maximo permitido de los anticipos. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> Valor del Anticipo</td>
          <td width="525"  class="ayudaHtmlTexto">Campo donde se digite el valor en moneda del anticipo.. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> Banco</td>
          <td width="525"  class="ayudaHtmlTexto">Campo de Seleccion donde se encuentran los diferentes bancos relacionados. </td>
        </tr>
      </table>    </td>
  </tr>
</table>
<p>&nbsp; </p>
</body>
</html>
