<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/validar.js">
</script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">

</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Anular Planilla"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

  <%//Inicializo variables
String estandar="", trailer="", fecha="",standar="", observacion="",docuinterno="", remitentes="", ruta="",destinatarios="", remesa="", planilla="", cedula="", nombre="", placa="", precinto=""; 
float peso=0, valor=0;
%>
 <%List planillas = new LinkedList();
  planillas=(List)request.getAttribute("plaerror");
  request.setAttribute("plaerror",planillas);%>
 <table width="73%"  border="2" align="center">
   <tr>
     <td><table width="100%" align="center" class="tablaInferior">
       <tr>
         <td height="22" colspan=2 class="subtitulo1">Mensaje</td>
         <td width="368" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
       </tr>
     </table>
      <table width="100%"  border="2" align="center" cellpadding="0" cellspacing="0" bgcolor="#EFEBDE" class="filaresaltada">
        <tr>
          <td>
            <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="filaresaltada">
              <tr>
                <td colspan="6" valign="top"><span class="Estilo19">Ocurri&oacute; un error al tratar de Anular la Planilla No. <%=request.getParameter("planilla")%>. </span><br>
                    <span class="Estilo21"><br>
            Raz&oacute;n : una o mas de las remesas solo esta relacionada con esta planilla.</span><br>
            <br>
            <span class="Estilo21">Para corregir este error usted puede:</span><br>
            <br>
            <table width="100%" height="21"  border="0" cellpadding="3" cellspacing="2" class="filaresaltada">
              <tr>
                <td width="9%" rowspan="3">&nbsp;</td>
                <td width="91%" class="Letras"  ><ul>
                    <li class="Estilo24">Asignar una planilla existente a todas las remesas </li>
                </ul></td>
              </tr>
              <tr>
                <td width="91%" class="Letras" ><ul>
                    <li class="Estilo24">Crear una nueva planilla para todas las remesas </li>
                </ul></td>
              </tr >
              <tr >
                <td class="Letras" ><ul>
                    <li class="Estilo24">Anular la planilla y la remesa (Anular Despacho)</li>
                </ul></td>
              </tr>
            </table>
            <span class="Estilo24">Luego de realizar este proceso usted podr&aacute; eliminar la Planilla No <%=request.getParameter("planilla")%>. </span><br></td>
              </tr>
          </table></td>
        </tr>
      </table></td>
   </tr>
 </table>
 <br>
 <form name="form1" method="post" action="<%=CONTROLLER%>?estado=Planilla&accion=Search&colpapel=ok&planilla=<%=request.getParameter("planilla")%>&aplanilla=ok">
  <input name="ventana" type="hidden" id="ventana"> 
  <table width="100%"  border="2" align="center">
   <tr>
     <td> 
  <table width="100%"  border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#999999">
    <tr >
      <td width="13%" height="23" bordercolor="#999999" class="tblTitulo"><strong>REMESAS</strong></td>
      <td width="42%" bordercolor="#999999" class="tblTitulo"><strong>STANDARD</strong></td>
      <td colspan="2" bordercolor="#999999" class="tblTitulo"><div align="center"><strong>ACCION</strong></div></td>
    </tr>
    <%Vector rem = model.remesaService.getRemesas();
		for(int i = 0; i<rem.size(); i++){
	  Remesa r = (Remesa) rem.elementAt(i);%>
    <tr class="<%=i%2==0?"filagris":"filaazul"%>" >
      <td height="20" class="bordereporte"><%=r.getNumrem()%></td>
      <td class="bordereporte"><%=r.getDescripcion()%></td>
      <%if(rem.size()>1){%><td width="18%" title="Anular Remesa..." style="cursor:hand" onClick="openPage('<%=CONTROLLER%>?estado=Remesa&accion=Search&placol=0k&remesa=<%=r.getNumrem()%>','')"  onMouseOver='cambiarColorMouse(this)'>Anular Remesa</td><%}
	 else if(rem.size()==1){%><td width="18%" style="cursor:hand" title="Anular Despacho..."  onClick="window.location='<%=CONTROLLER%>?estado=Planilla&accion=Search&colpapel=ok&planilla=<%=request.getParameter("planilla")%>&remesaRel=<%=r.getNumrem()%>'"  onMouseOver='cambiarColorMouse(this)'>Anular Despacho</td><%}%>
      <td class="bordereporte" width="27%" style="cursor:hand" title="Agregar Remesa..."  onClick="openPage('<%=CONTROLLER%>?estado=AgregarPlanilla&accion=Search&remesa=<%=r.getNumrem()%>&anular=<%=request.getParameter("planilla")%>','')"  onMouseOver='cambiarColorMouse(this)'>Crear una nueva planilla para esta remesa</td>
    </tr>
    <%}%>
  </table>
  </td>
   </tr>
 </table><br>
    <div align="center">
      <input name="imageField" type="image" src="<%=BASEURL%>/images/botones/regresar.gif" width="90" height="21" border="0" onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);">
      <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand"></div>
  </form>
</div>
 
</body>
</html>
