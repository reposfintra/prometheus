<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*,com.tsp.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<%
String estandar="", trailer="", cliente="", fecha="",standard="", observacion="",docuinterno="", remitentes="", destinatarios="", remesa="", planilla="", cedula="", nombre="", placa="", precinto=""; 
float peso=0, valor=0;
List planillas=new LinkedList();

	Usuario usuario = (Usuario) session.getAttribute("Usuario");
	model.ImagenSvc.resetImagesDespacho(usuario.getLogin());
      if(model.remesaService.getRemesa()!=null){
	    Remesa rem = model.remesaService.getRemesa();
	    estandar = rem.getDescripcion();
        standard= rem.getStdJobNo();
        docuinterno = rem.getDocInterno();
        remitentes = rem.getRemitente();
        destinatarios = rem.getDestinatario();
        remesa = rem.getNumrem();
		observacion = rem.getObservacion();
		cliente = rem.getCliente();
      }
      if(request.getAttribute("planillas")!=null){
        planillas = (List) request.getAttribute("planillas");
		session.setAttribute("planillas", planillas);
	  }
	  
	java.util.Date date = new java.util.Date();
    SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd hh:mma");
	String fecpla = s.format(date);   
    Stdjobdetsel sj=new Stdjobdetsel();
	  String pa ="";
	  String uw="";
	  String vlr ="";
	  String origen ="";
	  float unidad_default = 0;
	  model.stdjobdetselService.searchStdJob(standard);
      if(model.stdjobdetselService.getStandardDetSel()!=null){
			sj = model.stdjobdetselService.getStandardDetSel();
            uw=sj.getUnit_of_work();
            pa=sj.getPorcentaje_ant();
			vlr = ""+sj.getVlr_freight();             
			origen = sj.getOrigin_code();
			unidad_default=sj.getUnidades();
			//out.println(origen);
            }%>           

<html>
<head>
<title>Agregar Planilla</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/validar.js">
</script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">

<script language="JavaScript" type="text/JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
function mostrarMoneda(valor){
		  var vec = valor.split("/");
		  	if(vec.length>2){
			
			form1.moneda.value = vec[2];
			}else{
				form1.moneda.value = "";
			}
			
	}

//-->
</script>
</head>

<body <%if(request.getParameter("mensaje")!=null){%>onLoad= "alert('<%=request.getParameter("mensaje")%>')"<%}%>>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Agregar Planilla"/>
</div>

 <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<form name="form2" method="post" action="<%=CONTROLLER%>?estado=AgregarPlanilla&accion=Search">

 <table width="51%"  border="2" align="center">
	<tr>
	  <td>
  <table width="100%" align="center" class="tablaInferior">
    <tr>
      <td height="22" colspan=2 class="subtitulo1"><span class="titulo"><strong>Buscar Remesa <br>
      </strong></span></td>
      <td width="246" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
    </tr>
  </table>
  <table width="100%" align="center" bordercolor="#CCCCCC" class="Letras">
    <tr class="fila">
      <td width="162" nowrap><strong class="Estilo6">Numero de la Remesa:</strong></td>
      <td width="352" nowrap><input name="remesa" type="text" id="remesa" maxlength="10">
        <input type="image" src="<%=BASEURL%>/images/botones/buscar.gif" width="87" height="21" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> </td>
    </tr>
  </table>

</td>
    </tr>
</table>
</form>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Colpapel&accion=Validar&colpapel=show" onSubmit="return ValidarColpapel(this);">


 <table width="100%"  border="2">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td height="22" colspan=2 class="subtitulo1">Agregar Nueva Planilla <input name="remesa" type="hidden" id="remesa" value="<%=remesa%>"></td>
          <td width="368" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>        
        <table width="100%" class="tablaInferior">
          <tr>
            <td height="22" class="subtitulo1">Datos Remesa <%=remesa%>
              <input name="anular" type="hidden" id="anular" value="<%=request.getParameter("anular")!=null?request.getParameter("anular"):""%>"></td>
            </tr>
        </table>                 
	     <table width="100%" align="center" class="tablaInferior"  >
          <tr class="filaresaltada">
            <th width="17%" scope="row"><div align="left">Estandar Job</div>
                <span class="Estilo5 Estilo8"> </span><strong></strong></th>
            <td colspan="3">
              <%=estandar%>
              <input name="standard" type="hidden" id="standard" value="<%=standard%>">
              
              <input name="cfacturar" type="hidden" id="cfacturar" value="15">
			  <input name="cantreal" type="hidden" id="cantreal" value="15"></td>
            <td width="13%">Fecha Despacho </td>
            <td colspan="2"><input name="fechadesp" type="text" id="fechadesp" size="18" value="<%=fecpla.toLowerCase()%>" readonly></td>
            </tr>
          <tr class="filaresaltada">
            <th scope="row"><div align="left">Remitente</div></th>
            <td width="18%"><%=remitentes%>
              <input name="remitentes" type="hidden" id="remitentes" value="<%=remitentes%>"> </td>
            <td width="13%">Destinatarios</td>
            <td colspan="2"><%=destinatarios%>
              <input name="destinatarios" type="hidden" id="destinatarios" value="<%=destinatarios%>"></td>
            <td width="11%">Documentos</td>
            <td width="22%" ><%=docuinterno%></td>
            </tr>
          <tr class="filaresaltada">
            <th scope="row"><div align="left">Observacion</div></th>
            <td colspan="7"><span class="Estilo6"><span class="Estilo8"><%=observacion%>
              </span></span></td>
            </tr>
        
		  </table>
		  <table width="100%" class="tablaInferior">
            <tr>
              <td height="22" class="subtitulo1">Datos Planilla </td>
            </tr>
          </table>		
		  <table width="100%" class="tablaInferior">
          <tr class="filaresaltada">
            <th height="26" colspan="2" scope="row"><div align="left"><span class="Estilo7">Placa</span></div></th>
            <td colspan="7"><input name="placa" type="text" id="placa" size="12"></td>
          </tr>
          <tr class="filaresaltada">
            <th width="85" rowspan="2" class="Estilo7" scope="row"><div align="left">Trailer</div></th>
            <th width="73" rowspan="2" class="Estilo7" scope="row"><div align="left">
              <input name="trailer" type="text" id="trailer" size="12">
            </div></th>
            <td colspan="2"><strong>&iquest;Trailer de TSP? </strong></td>
            <td width="86" rowspan="2"><strong>Contenedores</strong></td>
            <td width="93"><input name="c1" type="text" id="c14" size="15"></td>
            <td width="82" rowspan="2"><strong>
              <input name="tipo_cont" type="radio" value="FINV">
  TSP</strong><br>
  <input name="tipo_cont" type="radio" value="NAV" checked>
  <strong>Naviera</strong></td>
            <td width="116" rowspan="2"><span class="Estilo8"><strong>Precintos de Contedores </strong></span></td>
            <td width="328"><input name="c1precinto" type="text" id="c1precinto3" size="20" onChange="controlPrecintoC('<%=CONTROLLER%>?estado=Colpapel&accion=Validar&cmd=show');"></td>
          </tr>
          <tr class="filaresaltada">
            <td height="31" colspan="2"><strong>
              <input name="tipo_tra" type="radio" value="FINV" checked>
  Si </strong><br>
  <input name="tipo_tra" type="radio" value="NAV">
  <strong>No</strong></td>
            <td width="93"><input name="c2" type="text" id="c23" size="15"></td>
            <td width="328"><input name="c2precinto" type="text" id="c2precinto3" size="20" onChange="controlPrecintoC('<%=CONTROLLER%>?estado=Colpapel&accion=Validar&cmd=show');"></td>
          </tr>
		  </table>		
		  <%
	  String unit_transp="";
	  model.stdjobcostoService.buscarStandardJobCostoFull(request.getParameter("standard"),request.getParameter("valorpla"));
	  if(model.stdjobcostoService.getStandardCosto()!=null){
      	Stdjobcosto stdjobcosto = model.stdjobcostoService.getStandardCosto();
        unit_transp = stdjobcosto.getUnit_transp();
        }
	  %>
		  <table width="100%" class="tablaInferior">
            <tr class="filaresaltada">
              <th width="13%" height="26" rowspan="3" scope="row"><div align="left">Ruta Planilla </div></th>
              <th colspan="2" rowspan="3" scope="row"><div align="left">
                  <% List rutas = model.stdjobcostoService.getRutas(standard)!=null? model.stdjobcostoService.getRutas(standard):new Vector();%>
                  <select name="ruta" class="textbox" id="select" onChange="cambiarFormulario2('<%=BASEURL%>/colpapel/agregarPlanilla2.jsp')">
                    <option value="0">Seleccione Alguna</option>
                    <%
		  	String rutaD = request.getParameter("ciudadOri")+request.getParameter("ciudadDest");
		   	Iterator it=rutas.iterator();
				while (it.hasNext()){
					Stdjobcosto std = (Stdjobcosto) it.next();
					String ruta = std.getRuta();
					String codigo=std.getFt_code();
					%>
                    <option value="<%=codigo%>"><%=ruta%></option>
                    <%}%>
                  </select>
              </div></th>
              <th width="15%" scope="row"> Cantidad a Despachar              </th>
              <th colspan="3" scope="row">                <div align="left">
                  <input name="toneladas" type="text" class="textbox" id="toneladas"  onKeyPress="soloDigitos(event,'decOK')" size="12">
                  <%=unit_transp%><span class="Estilo8"> </span><span class="Estilo8"> </span> <strong><span class="Estilo8"> </span> </strong></div></th>
            </tr>
            <tr class="filaresaltada">
              <th scope="row">Valor del Flete
                  <% List costos = model.stdjobcostoService.getCostos("....",request.getParameter("standard"));%></th>
              <th width="10%" scope="row"><select name="valorpla" class="textbox" id="select2" onChange="">
                  <option value="0" title="">Seleccione Alguna</option>
                  <%
		   	it=costos.iterator();
				while (it.hasNext()){
					Stdjobcosto std = (Stdjobcosto) it.next();
					float costo = std.getUnit_cost();
					String codigo=std.getFt_code();
					String vacio ="";
					if(std.getCf_code().length()>=7){
						if(std.getCf_code().substring(6,7).equals("V"))
							vacio = "-vacio";
					}
					String unit = std.getUnit_transp();
					%>
                  <option value="<%=codigo%>"><%=Util.customFormat(costo)%> <%=std.getCurrency()%>/<%=unit%><%=vacio%></option>
                  <%}%>
              </select></th>
              <th scope="row"><span class="Estilo8"><strong>Nuevo Valor Aplicado </strong></span></th>
              <th width="15%" scope="row"><strong><span class="Estilo8">
                <input name="otro" type="text" class="textbox" id="otro2"  onKeyPress="soloDigitos(event,'decNo')" value="NO" readonly>
              </span></strong></th>
            </tr>
            <tr class="filaresaltada">
              <th colspan="2" scope="row"><strong>Valor Total de la Planilla </strong></th>
              <th height="21" colspan="2" scope="row"><span class="Estilo8"></span><span class="Estilo8"><strong>
                <input name="voc" type="text" class="textbox" id="voc" value="0" size="15" readonly>
                <input name="vocH" type="hidden" id="vocH" value="0">
              </strong> </span></th>
            </tr>
            <tr class="filaresaltada">
              <th colspan="2" scope="row"><strong><a style="cursor:hand " class="Simulacion_Hiper" onClick="window.open('<%=BASEURL%>/colpapel/extrafletes.jsp?sj=<%=request.getParameter("standard")%>&tipo=DESPACHO&voc='+form1.vocH.value+'&extraflete='+form1.extraflete.value,'Extrafletes','height=400,width=800,dependent=yes,resizable=yes,scrollbars=yes,status=yes');" >INGRESAR/ MODIFICAR EXTRAFLETES</a></strong></th>
              <th scope="row"><strong><a style="cursor:hand " class="Simulacion_Hiper" onClick="window.open('<%=BASEURL%>/colpapel/costosRembolsables.jsp?sj=<%=request.getParameter("standard")%>&voc='+form1.vocH.value+'&cr='+form1.cr.value,'Costos','height=400,width=800,dependent=yes,resizable=yes,scrollbars=yes,status=yes');" >INGRESAR/ MODIFICAR COSTOS REEMBOLSABLES</a></strong></th>
              <th scope="row">Valor total en extrafletes </th>
              <th scope="row"><div align="left">
                  <input name="extraflete" type="text" class="textbox" id="extraflete" value="0" size="12" readonly>
                  <input name="cr" type="hidden" id="cr" value="0">
              </div></th>
              <th colspan="2" scope="row">Procentaje maximo de anticipo: <%=pa%>
                  <input name="pa" type="hidden" id="pa" value="<%=pa%>"></th>
            </tr>
            <tr class="filaresaltada">
              <th rowspan="2" scope="row"><span class="Estilo8"><strong>Anticipo maximo permitido </strong></span></th>
              <th width="10%" rowspan="2" scope="row"><div align="left">
                  <input name="antmax" type="text" class="textbox" id="antmax2" size="15" readonly>
              </div></th>
              <th width="15%" rowspan="2" scope="row">Valor del Anticipo </th>
              <th scope="row"><input name="anticipo" type="text" class="textbox" id="anticipo" onKeyPress="soloDigitos(event,'decNO')" size="12" >
                  <strong>
<span class="<%=(String)request.getAttribute("anticipo")%>"><strong><strong><strong><strong>
<input name="moneda" type="text" id="moneda" style="font-size:11px;border:0" size="3"  readonly>
</strong></strong></strong></strong></span><input name="antprov" type="hidden" id="antprov" value="<%=request.getParameter("antprov")%>">
</strong></th>
              <th rowspan="2" scope="row"><span class="Estilo8">Banco</span></th>
              <th rowspan="2" scope="row"><div align="left">
                  <%TreeMap bancos= model.buService.getListaBancos(usuario.getLogin()); %>
                  <input:select name="banco" options="<%=bancos%>" attributesText="style='width:100%'; onChange=mostrarMoneda(this.value); class='textbox'" default=""/> </div></th>
              <th scope="row">Beneficiario</th>
            </tr>
            <tr class="filaresaltada">
              <th scope="row"><a style="cursor:hand " class="Simulacion_Hiper" onClick="window.open('<%=BASEURL%>/colpapel/anticipos.jsp?tipo=2&remesa=<%=remesa%>','','height=400,width=800,dependent=yes,resizable=yes,scrollbars=yes,status=yes')"; >Aplicar anticipo a proveedor</a> </th>
              <th scope="row"><select name="beneficiario" id="beneficiario">
                <option value="C" >Conductor</option>
                <option value="P">Propietario</option>
                <option value="T" >Tercero</option>
              </select></th>
            </tr>
          </table>
		  <table width="100%" class="tablaInferior">
            <tr>
              <td height="22" class="subtitulo1">Otros Precintos </td>
            </tr>
          </table>		
		  <table width="100%" height="44" class="tablaInferior">
            <tr class="filaresaltada">
              <td width="20%">              <div align="center">
                    <input name="precintos" type="text" id="precintos7" size="20" onChange="controlPrecintos('<%=CONTROLLER%>?estado=Colpapel&accion=Validar&cmd=show');">
                  </div>
		      </td><td ><div align="center">
                  <input name="precintos2" type="text" id="precintos23" size="20" onChange="controlPrecintos('<%=CONTROLLER%>?estado=Colpapel&accion=Validar&cmd=show');">
                </div></td>
             <td width="20%">
		     <div align="center">
                <input name="precintos3" type="text" id="precintos33" size="20" onChange="controlPrecintos('<%=CONTROLLER%>?estado=Colpapel&accion=Validar&cmd=show');">
              </div></td>
              <td width="20%"><div align="center">
                <input name="precintos4" type="text" id="precintos43" size="20" onChange="controlPrecintos('<%=CONTROLLER%>?estado=Colpapel&accion=Validar&cmd=show');">
              </div></td>
              <td width="20%"><div align="center">
                <input name="precintos5" type="text" id="precintos53" size="20" onChange="controlPrecintos('<%=CONTROLLER%>?estado=Colpapel&accion=Validar&cmd=show');">
              </div></td>
            </tr>
          </table>		
		  <%	List listTabla2 = model.tbltiempoService.getTblTiemposSalida(usuario.getBase());
			Iterator itTbla2=listTabla2.iterator();
			if(itTbla2.hasNext()){
				%>
		  <table width="100%" class="tablaInferior">
            <tr>
              <td height="22" class="subtitulo1">Fechas YYYY-MM-DD HH:MM 24 Hrs </td>
            </tr>
          </table>
		  <table width="100%" height="44" class="tablaInferior">
		  
          <tr class="filaresaltada">
		    <%		while(itTbla2.hasNext()){
					Tbltiempo tbl = (Tbltiempo) itTbla2.next();
					String id_tabla=tbl.getTimeCode();%>
				  
            <td><div align="left"><%=tbl.getTimeDescription()%></div></td>
			  <td><input name="<%=id_tabla%>" type="text" id="<%=id_tabla%>" onKeyPress="soloDigitos(event,'decNo');FormatNumber(this);" size="13" maxlength="16" onChange="formatoFecha(this);">
                <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.<%=id_tabla%>);return false;" HIDEFOCUS> <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a>&nbsp </td>
			  <%}%>
            </tr>
          </table>
		  <%}%>
           <%model.anticiposService.vecAnticipos("",standard);
		Vector descuentos = model.anticiposService.getAnticipos();
		String tipo="";
		if(descuentos.size()>0){%>
		  
        <table width="100%" class="tablaInferior">
            <tr>
              <td height="22" class="subtitulo1">Descuentos</td>
            </tr>
          </table>		
          <table width="100%" height="44" class="tablaInferior">
            <tr class="filaresaltada">
		     <%
			for(int k = 0 ; k<descuentos.size();k++){
				Anticipos ant = (Anticipos) descuentos.elementAt(k); 
				String codigo = ant.getAnticipo_code();
				valor = ant.getValor();
				model.proveedoresService.listaAnticipoProvee(codigo);
				Vector provee = model.proveedoresService.getProveedores();
				String readonly ="readonly";
				if(ant.getModif().equals("Y")){
					readonly ="";
				}
				if(provee.size()>0)
					readonly ="";
				tipo = ant.getTipo_s();
			%>
			  <td>
			  <%=ant.getAnticipo_desc().toUpperCase()%>
			  </td>
              <td>
			  <input name="<%=ant.getAnticipo_code()%>" type="text" onKeyPress="soloDigitos(event,'decNo')" value="<%=valor%>" size="15" <%=readonly%>  >
              <%=tipo.equals("P")?"%":""%>
              <input name="check<%=ant.getAnticipo_code()%>" type="checkbox" value="checkbox" <%if(ant.isChequeado()){%> checked <%}%>>
			  </td>
            
		  <%if(provee.size()>0){%>
		  	  <td>   <select name="provee<%=codigo%>">
                  <%for(int m =0; m<provee.size(); m++){
				Proveedores prov = (Proveedores)provee.elementAt(m);
			%>
                  <option value="<%=prov.getNit()%>/<%=prov.getSucursal()%>"><%=prov.getNombre()%> <%=prov.getSucursal()%></option>
                  <%}%>
              </select>
			  </td>
			  <%}%>
		  <%}%>
          </tr>
        </table>
	    <%}%> <br>
	    <table width="100%" class="tablaInferior">
          <tr>
            <td height="22" class="subtitulo1">Observaciones</td>
          </tr>
        </table>
	    <table width="100%" height="71" class="tablaInferior">
          <tr class="filaresaltada">
            <th width="54%" height="26" align="center" valign="top" scope="row"><div align="left">
                <textarea name="observacion" style="width:100%" id="textarea"></textarea>
            </div></th>
            <th width="21%" height="16" scope="row"><div align="left">Numero del Sticker </div></th>
            <th width="25%" scope="row"><div align="left">
                <input name="sticker" type="text" id="sticker">
            </div></th>
          </tr>
          <tr class="filaresaltada">
            <th width="54%" height="47" align="center" valign="top" scope="row"><div align="left"><span class="Estilo6"><strong><a class="Simulacion_Hiper" style="cursor:hand " onClick="abrirPagina('<%=BASEURL%>/colpapel/inventarios.jsp?sj='+form1.standard.value,'');">Despacho mercancia en inventario.</a></strong></span> <span class="Estilo6">
              <input name="inventarios" type="hidden" id="inventarios" value="a" size="80">
            </span></div></th>
            <th height="47" scope="row"><div align="left">Valor estimado de la mercancia</div></th>
            <th height="47" scope="row"><div align="left">
                <input name="vlrmercan" type="text" id="vlrmercan">
            </div></th>
          </tr>
        </table></td>
    </tr>
  </table>
	
  <%if(model.remesaService.getRemesa()!=null){%>
  <p align="center">    <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">  </p>
 <%}%>
</form>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins_24.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</body>
</html>
