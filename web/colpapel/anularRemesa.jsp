<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/validar.js">
</script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</script>

<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Anular Remesa"/>
</div>

 <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<%//Inicializo variables
String estandar="", trailer="", cliente="", fecha="",standar="", observacion="",docuinterno="", remitentes="", ruta="",destinatarios="", remesa="", planilla="", cedula="", nombre="", placa="", precinto=""; 
float peso=0, valor=0;
%>
<form name="form2" method="post" action="<%=CONTROLLER%>?estado=Remesa&accion=Search&placol=0k">
<table width="50%"  border="2" align="center">
    <tr>
      <td>
  <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td height="22" colspan=2 class="subtitulo1">Buscar Remesa </td>
      <td width="220" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
    </tr>
  </table>
  <table width="100%" align="center">
    <tr class="fila">
      <td width="162" nowrap><strong >Numero de la Remesa:</strong></td>
      <td width="352" nowrap><input name="remesa" type="text" id="remesa" maxlength="10">
      <img src="<%=BASEURL%>/images/botones/buscar.gif" width="87" height="21" onClick="form2.submit();this.disabled=true" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
    </tr>
  </table>
  </td>
  </tr>
  </table>
</form>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Remesa&accion=Delete">
     <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
      <%java.util.Date date = new java.util.Date();
      if(request.getAttribute("remesa")!=null){
	    Remesa rem = (Remesa)request.getAttribute("remesa");
        estandar = rem.getDescripcion();
        standar= rem.getStdJobNo();
        docuinterno = rem.getDocInterno();
        remitentes = rem.getRemitente();
        destinatarios = rem.getDestinatario();
        remesa = rem.getNumrem();
		observacion = rem.getObservacion();
		cliente = rem.getCliente();
      }
      if(request.getAttribute("planilla")!=null){
        Planilla pla = (Planilla) request.getAttribute("planilla");
		planilla = pla.getNumpla();
		nombre = pla.getNomCond();
		cedula = pla.getCedcon();
        placa = pla.getPlaveh();
        peso = pla.getPesoreal();
		trailer = pla.getPlatlr();
		fecha = pla.getFecdsp();
		ruta = pla.getRuta_pla();
		precinto = pla.getPrecinto();
		
      }
      if(request.getAttribute("movpla")!=null){
       Movpla movpla = (Movpla) request.getAttribute("movpla");
       valor = movpla.getVlr();
      }
	  List planillas = new LinkedList();
	  
	  if(request.getAttribute("planillas")!=null){
	  	planillas = (List) request.getAttribute("planillas");
	  }
  %>
	<table width="77%"  border="2" align="center">
    <tr>
      <td height="100%">

        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="22" colspan=2 class="subtitulo1">Anular Remesa <span class="titulo"><%=remesa%>
                <input name="remesa" type="hidden" id="remesa" value="<%=remesa%>">
            </span></td>
            <td width="220" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tablaInferior">
          <tr>
            <td height="22" class="subtitulo1">Datos Planilla </td>
          </tr>
        </table>
        <table width="100%" class="tablaInferior">
		 <%if(planillas.size()<=1){%>
          <tr class="filaresaltada">
            <th width="11%" height="26" scope="row"><div align="left"><span class="Estilo7">Placa</span></div>                </th>
            <th colspan="2" scope="row"><%=placa%></th>
            <th width="14%" scope="row">Conductor</th>
            <th colspan="2" scope="row"><%=cedula%> <%=nombre%></th>
          </tr>
          <tr class="filaresaltada">
            <th height="31" class="Estilo7" scope="row"><div align="left">Trailer</div>              <div align="left" class="letra"> </div>              <span class="Estilo8"></span></th>
            <th width="10%" height="31" class="Estilo7" scope="row"><span class="letra"><%=trailer%></span></th>
            <th width="13%" class="Estilo7" scope="row"><span class="Estilo8"><strong>Precintos</strong></span></th>
            <th height="31" class="Estilo7" scope="row"><span class="Estilo8"><strong class="letra"><%=precinto%></strong></span></th>
            <th width="18%" height="31" class="Estilo7" scope="row"><span class="Estilo8">Ruta Planilla </span></th>
            <th class="Estilo7" scope="row"> <strong class="letra"> </strong><%=ruta%></th>
            </tr>
          <tr class="filaresaltada">
            <th height="31" colspan="2" class="Estilo7" scope="row"><div align="left">Cantidad Depachada </div>              </th>
            <th height="31" class="Estilo7" scope="row"><%=peso%></th>
            <th height="31" colspan="2" class="Estilo7" scope="row">Valor del Anticipo</th>
            <th height="31" class="Estilo7" scope="row"><%=valor%></th>
          </tr>
		   <%if(request.getAttribute("remesas")!=null){
	  	List remesas= model.planillaService.buscarRemesas(planilla);
	  %>
          <tr class="filaresaltada">
            <th height="31" colspan="6" class="Estilo7" scope="row"><u>Existen <%=remesas.size()%> remesas relacionadas con esta planilla. Haga click aqui para obtener mas informaci&oacute;n.</u></th>
            </tr>
			 <%}else{%>
          <tr class="filaresaltada">
            <th height="31" colspan="6" class="Estilo7" scope="row"><div align="left"><span class="fila">No existen remesa relacionadas con esta planilla.</span></div></th>
          </tr>
		   <%}
	  }else{%>
          <tr class="filaresaltada">
            <th height="31" colspan="6" class="Estilo7" scope="row"><table width="100%"  border="1" bordercolor="#999999">
              <tr class="tblTitulo">
                <td width="12%"><strong>PLANILLA</strong></td>
                <td width="18%"><strong>FECHA DESPACHO </strong></td>
                <td width="14%"><strong>PLACA</strong></td>
                <td width="17%"><strong>CONDUCTOR</strong></td>
                <td width="23%"><strong>RUTA</strong></td>
                <td width="16%"><em class="Estilo18"><strong>Ver remesas relacionadas.</strong></em></td>
              </tr>
              <% Iterator it=planillas.iterator();
	  int i=0;
	  while (it.hasNext()){
	  i++;
	     Planilla plani = (Planilla) it.next();
	     List remesas= model.planillaService.buscarRemesas(plani.getNumpla());%>
              <tr class="<%=i%2==0?"filagris":"filaazul"%>">
                <td class="bordereporte" height="20"><%=plani.getNumpla()%></td>
                <td class="bordereporte"><%=plani.getFecdsp().substring(0,10)%></td>
                <td class="bordereporte"><%=plani.getPlaveh()%></td>
                <td class="bordereporte"><strong><%=plani.getCedcon()%> <%=plani.getNomCond()%></strong></td>
                <td class="bordereporte"><strong><%=plani.getRuta_pla()%></strong></td>
                <td class="bordereporte" style="cursor:hand" title="Ver Remesas..."  onClick="MM_openBrWindow('colpapel/listaRemesas.jsp?nopla=<%=plani.getNumpla()%>','','menubar=yes,scrollbars=yes,resizable=yes')"><em><%=remesas.size()%> relacionadas</em></td>
              </tr>
              <%}%>
            </table></th>
          </tr>
		   <%}%>
        </table>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="22" class="subtitulo1">Datos Remesa </td>
          </tr>
        </table>
        <table width="100%" align="center" class="tablaInferior"  >
          <tr class="fila">
            <th colspan="2" scope="row"><div align="left">Estandar Job</div>
                <span class="Estilo5 Estilo8"> </span><strong></strong></th>
            <td><span class="Estilo5 Estilo8">
              </span><strong><%=estandar%>
              <input name="standard" type="hidden" id="standard3" value="<%=standar%>">
              <input name="estandard" type="hidden" id="estandard2" value="<%=estandar%>">
</strong></td>
            <td>Cliente</td>
            <td><span class="Estilo6"><%=cliente%> </span></td>
            </tr>
          <tr class="fila">
            <th colspan="2" scope="row"><div align="left">Remitente</div></th>
            <td><span class="Estilo6"><%=remitentes%> </span> </td>
            <td>Destinatario</td>
            <td><%=destinatarios%></td>
            </tr>
          <tr class="fila">
            <th colspan="2" scope="row"><div align="left">Documentos</div></th>
            <td colspan="5"><span class="Estilo6"><span class="Estilo8"><%=docuinterno%>
              </span>            </span><strong>            </strong> </td>
            </tr>
          <tr class="fila">
            <th colspan="2" class="Estilo7" scope="row"><div align="left"><span class="Estilo8">Observacion</span></div></th>
            <td colspan="5"><span class="Estilo8"><%=observacion%>
              </span> </td>
            </tr>
        </table>
       	</td>
	</tr>
	</table>
<%	 if(request.getAttribute("remesa")!=null){%>
  <p align="center">
    <img name="imageField" src="<%=BASEURL%>/images/botones/anular.gif" width="90" height="21" border="0" onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);" onClick="form1.submit(); this.disabled=true">
    <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand"> <br>
    
	 
    </p> <%}%>
	<%if( request.getParameter("mensaje")!=null){%>
  <table border="2" align="center">
    <tr>
      <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="229" align="center" class="mensajes"><%= request.getParameter("mensaje")%></td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="58">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <%  } %>
  <p align="center">&nbsp;  </p>
</form>
<iframe width=188 height=166 name="gToday:datetime:agenda.js:gfPop:plugins_12.js" id="gToday:datetime:agenda.js:gfPop:plugins_12.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</div>
</body>
</html>
