<%@page session="true"%>
<%@page import="java.util.*" %>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Agregar Extrafletes.</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
 <script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
 <script type='text/javascript' src="<%=BASEURL%>/js/validarDespacho.js"></script> 
</head> 
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Aplicar Extrafletes"/>
</div> 
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%  String pc1="",pc2="";
	   String precinto="";
	   String c1="",c2="";
   Planillas  planilla = model.PlanillasSvc.getPlanilla() ;
   double vlrTasa = 1;
	String sj = "";                        
   if ( planilla!=null ){
   		sj =planilla.getSJDescripcion();
	   	
	  	if(planilla.getContenedores()!=null){
   			if(!planilla.getContenedores().equals("")){
   				String v []=planilla.getContenedores().split(",");
				c1 = v[0];
				if(v.length>1){
					c2=v[1];
				}
		
   			}
   		}	
   
	  
   	   String precintos[] = planilla.getPrecintos().split(",");
		for(int p=0; p<precintos.length;p++){
			
			if(p==0){
				if(!precintos[p].equals(" "))
					pc1 = precintos[p];
			}
			else if(p==1){
				if(!precintos[p].equals(" "))
					pc2 = precintos[p];
			}
			if(pc1.equals("")&&pc2.equals("") && p > 1 ){
				precinto = precinto +precintos[p]+"<br>";
			}
		}
	}
   %>
<table width="70%" border="2" align="center">          
    <tr>
      <td width="100%">
	  	<table width="100%">
          <tr>
            <td width="49%" height="22" align="left" class="subtitulo1">&nbsp;Informaci&oacute;n de la Planilla            </td>
            <td width="51%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
        </table>
	  	<table width="100%">
	    	
        	<tr>
          		<td width="15%" class="fila">No.</td>
          		<td width="18%" class="letra"><%= planilla.getPlanilla() %></td>
          		<td width="11%" class="fila">Estado </td>
		        <td width="24%" class="letra"><%=planilla.getEstado()%></td>
       		   	<td width="16%" class="fila">Fecha Creaci&oacute;n</td>
          		<td width="16%" class="letra"><%= planilla.getFechaCreacion()   %> </td>
        	</tr>
			<tr>
        	  <td class="fila">Distrito</td>
        	  <td class="letra"><%= planilla.getDistrito() %></td>
        	  <td class="fila">Tipo</td>
        	  <td class="letra"><%= planilla.getTipo() %></td>
        	  <td class="fila">Fecha Anulaci&oacute;n</td>
        	  <td class="letra"><%=planilla.getFecha_anul()%></td>
      	  </tr>
        	<tr>
        	  <td class="fila">Ruta</td>
        	  <td class="letra"><%= planilla.getOrigen() %> - <%= planilla.getDestino()%></td>
        	  <td class="fila">Trailer</td>
        	  <td class="letra"><%=planilla.getTrailer()%></td>
       	      <td colspan="2" class="fila" align="center">Precintos</td>
   	      </tr>
        	<tr>
        	  <td height="20" class="fila">Placa</td>
        	  <td class="letra"><%= planilla.getPlaca()       %></td>
        	  <td class="fila">&nbsp;</td>
        	  <td class="letra">&nbsp;</td>
        	  <td colspan="2" rowspan="3" valign="top" class="letra"><div style=" overflow:auto ; WIDTH: 100%;  height:50; "><%=precinto%>
			  </div></td>
       	  </tr>
		  <tr>
        	  <td class="fila">Cedula Conductor</td>
        	  <td class="letra"><%= planilla.getCedulaConductor()%></td>
        	  <td class="fila">Nombre</td>
        	  <td class="letra"><%= planilla.getConductor()%></td>
       	  </tr>
        	<tr>
        	  <td height="20" class="fila">Cedula Propietario</td>
        	  <td class="letra"><%= planilla.getCedulaPropietario() %></td>
        	  <td class="fila">Nombre</td>
        	  <td class="letra"><%=planilla.getPropietario()%></td>
       	  </tr>
		
   	
        	<tr>
        	  <td class="fila">Cantidad a pagar </td>
        	  <td class="letra"><%= planilla.getCarga() %> <%= planilla.getUnidad() %> </td>
        	  <td class="fila">Valor Flete</td>
        	  <td class="letra"><%=com.tsp.util.Util.customFormat(planilla.getValorFlete())%> <%= planilla.getMoneda()%> </td>
			  <td class="fila" align="center">Placa Contenedor </td>
        	  <td class="fila" align="center">Precinto Contenedor</td>
			  
       	  </tr>
        	<tr>
        	  <td height="20" class="fila">Valor Planilla</td>
        	  <td class="letra"><%= com.tsp.util.Util.customFormat(planilla.getValor())          %> <%= planilla.getMoneda()%> </td>
        	  <td class="fila">Standard Job</td>
        	  <td valign="top" class="letra"><%=sj%></td>
        	  <td class="letra" align="center"><%=c1%></td>
        	  <td class="letra" align="center"><%=pc1%></td>
       	  </tr>
        	<tr>
        	  <td colspan="4" class="fila">&nbsp;</td>
        	  <td class="letra" align="center"><%=c2%></td>
        	  <td class="letra" align="center"><%=pc2%></td>
      	  </tr>
   	  </table>
	  <%if(!planilla.getCumplido().equals("")){%>	  
	  	<table width="100%">
          <tr>
            <td width="15%" class="fila">Cant. Cumplida</td>
            <td width="18%" class="letra"><%= com.tsp.util.Util.customFormat( Double.parseDouble(planilla.getCantCumplida()) ) %> <%= planilla.getUnidadCumplida() %> </td>
            <td width="12%" class="fila">Fecha Cumplida</td>
            <td width="23%" class="letra"><%= planilla.getFechaCumplido() %></td>
            <td width="16%" class="fila">Cumplida por </td>
            <td width="16%" class="letra"><%= planilla.getUsuario_cumplio() %></td>
          </tr>
        </table>
		<%}%></td>
	</tr>
</table>
  <form action="<%=CONTROLLER%>?estado=Planilla&accion=Extraflete&cmd=Aplicar" method="post" name="form2" id="form2">
<%  Vector fletes = model.sjextrafleteService.getFletes();
   if(fletes.size()>0){%>
    <table width="85%"  border="2" align="center" cellpadding="0" cellspacing="0" bgcolor="#EFEBDE" class="filaresaltada">
      <tr>
        <td>
          <table width="100%" align="center" class="tablaInferior">
            <tr>
              <td height="22" colspan=2 class="subtitulo1"><span class="titulo"><strong>
                <input name="sj" type="hidden" id="sj2" value="<%=request.getParameter("sj")%>">
              EXTRAFLETES<br>
              </strong></span></td>
              <td width="308" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
            </tr>
          </table>
          <table width="100%"  border="1" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC">
            <tr class="filaresaltada">
              <td height="32" colspan="9"><strong>VALOR NUEVO A APLICAR </strong> :
                  <input name="totalexto" type="text" id="totalexto" value="<%=com.tsp.util.Util.customFormat(Float.parseFloat(request.getParameter("extraflete")!=null?request.getParameter("extraflete"):"0"))%>" readonly>
                  <input name="totalext" type="hidden" id="totalext" value="0" readonly>
                  <input name="planilla" type="hidden" id="planilla" value="<%=request.getParameter("planilla")%>">
                  <input name="remesa" type="hidden" id="remesa" value="<%=request.getParameter("remesa")%>">
</td>
            </tr>
            <tr bordercolor="#999999" class="tblTitulo">
              <td height="0" colspan="5">COSTOS</td>
              <td>INGRESOS</td>
              <td colspan="2">LIQUIDACION</td>
              <td width="16%" rowspan="2">Aprobado por</td>
            </tr>
            <tr bordercolor="#999999" class="tblTitulo">
              <td height="24" colspan="2"><div align="center"><strong>DESCRIPCION</strong></div></td>
              <td width="13%"><div align="center"><strong>VALOR COSTO </strong></div></td>
              <td width="9%" height="24"><div align="center"><strong>PORCENTAJE</strong></div></td>
              <td width="10%"><div align="center"><strong>CANTIDAD</strong></div></td>
              <td width="16%"><div align="center"><strong>VALOR</strong></div></td>
              <td width="6%"><div align="center"><strong>TOTAL COSTO </strong></div></td>
              <td width="7%">TOTAL INGRESO </td>
            </tr>
            <%
	String no_sj = request.getParameter("sj");
	String voc = request.getParameter("voc");
	String vunidad = "1";
	
		for(int i= 0; i<fletes.size(); i++){
			SJExtraflete s =(SJExtraflete) fletes.elementAt(i);
			String costo_fv =s.getVf_costo().equals("F")?"readonly":"";
			String ing_fv=s.getVf_ingreso().equals("F")?"readonly":"";
			String unidad=s.getUnidades().equals("N")?"disabled":"";
			String reembolsable =s.getReembolsable().equals("N")?"disabled":"";
			String tipo_valor ="";
			String tipo_porcent="";
			String discosto = "";
			if(s.getClase_valor().equals("V")){
				tipo_porcent="disabled";
				s.setPorcentaje(0);
			}
			else{
				tipo_valor ="disabled";
				float vdoc = Float.parseFloat(voc);
				float vpoc =(vdoc * s.getPorcentaje())/100;
				s.setValor_costo(com.tsp.util.Util.redondear(vpoc,0));
				unidad = "disabled";
				discosto = "disabled";
				vunidad = "0";
				
			}
		%>
            <tr class="<%=i%2==0?"filagris":"filaazul"%>">
	  <td width="3%"><input name="chek<%=s.getCod_extraflete()%>" type="checkbox" value="checkbox" onClick="calcularExtraflete('<%=s.getCod_extraflete()%>');" <%if(s.isSelec()){%> checked <%}%>></td>
      <td width="20%"><%=s.getDescripcion()%></td>
      <td><input name="cost<%=s.getCod_extraflete()%>" type="text" id="cost<%=s.getCod_extraflete()%>" onKeyUp="calcularValorExIng('<%=s.getCod_extraflete()%>');calcularValorEx('<%=s.getCod_extraflete()%>');" value="<%=com.tsp.util.Util.customFormat(s.getValor_costo())%>" <%=costo_fv%> <%=discosto%> onKeyPress="soloDigitos(event,'decNo')" <%if(s.isSelec()){%> disabled <%}%>>
        <input type="hidden" name="cost<%=s.getCod_extraflete()%>o" id="cost<%=s.getCod_extraflete()%>o"  value="<%=s.getValor_costo()%>"></td>
      <td><div align="center">
        <input name="p<%=s.getCod_extraflete()%>" type="text" id="p<%=s.getCod_extraflete()%>" onKeyUp="calcularValorEx('<%=s.getCod_extraflete()%>');" value="<%=s.getPorcentaje()%>" size="4" <%=tipo_porcent%> <%=costo_fv%>>
      </div></td>
      <td><div align="center">
        <input name="cant<%=s.getCod_extraflete()%>" type="text" id="cant<%=s.getCod_extraflete()%>" onKeyUp="calcularValorEx('<%=s.getCod_extraflete()%>');" value="<%=s.getCantidad()%>" size="7" <%=unidad%> onKeyPress="soloDigitos(event,'decNo')" <%if(s.isSelec()){%> disabled <%}%>>
        <input name="cant<%=s.getCod_extraflete()%>o" type="hidden" id="cant<%=s.getCod_extraflete()%>o" value="<%=s.getCantidad()%>">
      </div></td>
      <td><div align="center">
        <input name="ingreso<%=s.getCod_extraflete()%>" type="text" id="ingreso<%=s.getCod_extraflete()%>" onKeyUp="calcularValorExIng('<%=s.getCod_extraflete()%>');calcularValorEx('<%=s.getCod_extraflete()%>');" value="<%=com.tsp.util.Util.customFormat(s.getValor_ingreso())%>" <%=ing_fv%> <%=reembolsable%> onKeyPress="soloDigitos(event,'decNo')" <%if(s.isSelec()){%> disabled <%}%>>
        <input name="ingresoO<%=s.getCod_extraflete()%>" type="hidden" id="ingresoO<%=s.getCod_extraflete()%>"  value="<%=s.getValor_ingreso()%>" >
        <input type="hidden" name= "ingreso<%=s.getCod_extraflete()%>o"  id="ingreso<%=s.getCod_extraflete()%>o" value="<%=s.getValor_ingreso()%>">
      </div></td>
	  <td><input name="liq<%=s.getCod_extraflete()%>" type="text" id="liq<%=s.getCod_extraflete()%>" value="<%=com.tsp.util.Util.customFormat(s.getCantidad()*s.getValor_costo())%>" readonly onKeyPress="soloDigitos(event,'decNo')">
	    <input type="hidden" name="liq<%=s.getCod_extraflete()%>o" id="liq<%=s.getCod_extraflete()%>o" value="<%=s.getCantidad()*s.getValor_costo()%>"></td>
	  <td>
	  <input name="liqIng<%=s.getCod_extraflete()%>" type="text" id="liqIng<%=s.getCod_extraflete()%>" value="<%=com.tsp.util.Util.customFormat(s.getCantidad()*s.getValor_ingreso())%>" readonly onKeyPress="soloDigitos(event,'decNo')">
        <input type="hidden" name="liqIng<%=s.getCod_extraflete()%>o" id="liqIng<%=s.getCod_extraflete()%>o" value="<%=s.getCantidad()*s.getValor_ingreso()%>"></td>
	  <td><%TreeMap aprobador=model.tblgensvc.getLista_des();
	  String nombre = "aprobador"+s.getCod_extraflete();
	  		%>
	   
		<input:select name="<%=nombre%>" options="<%=aprobador%>" attributesText="style='width:100%;' class='textbox'" default="<%=s.getAprobador()%>" /></td>
	</tr>
            <%}%>
            <tr>
              <td colspan="9"><div align="center"> </div></td>
            </tr>
        </table></td>
      </tr>
    </table>
	<% }else{%>
    <br>
       <table border="2" align="center">
    <tr>
      <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="229" align="center" class="mensajes"><span class="normal">No existen extrafletes definidos para el standard :<%=sj%></span></td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="58">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  </form>
  <p> </p>

 
  
 
    <%}%>
    <br> <div align="center">
    <img  src="<%=BASEURL%>/images/botones/regresar.gif"  border="0" onMouseOver="botonOver(this);" style="cursor:hand" onMouseOut="botonOut(this);" onClick="window.location='<%=CONTROLLER%>?estado=Planilla&accion=Extraflete&cmd=Buscar&planilla=<%= planilla.getPlanilla() %>&pag=Volver'">
     <%if(fletes.size()>0){%> 
     <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="imgsalir"  width="90" height="21" style="cursor:hand" onClick="aplicarExtraflete('<%=request.getParameter("tipo")%>');"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
     <%}%>
	   <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">  </div>

  <%if(request.getParameter("mensaje")!=null){%>
  <table border="2" align="center">
    <tr>
      <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="229" align="center" class="mensajes"><span class="normal"><%=request.getParameter("mensaje")%></span></td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="58">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <%}%>
</div>
</body>
</html>
