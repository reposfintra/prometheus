<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Inicio Despacho</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/validar.js">
</script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</script>
 <script>
     function mostrarMoneda(valor){
		  var vec = valor.split("/");
		  	if(vec.length>2){
			
			form1.moneda.value = vec[2];
			}else{
				form1.moneda.value = "";
			}
			
	}
	</script>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">

</head>

<body >
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Despacho"/>
</div>

 <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
  
<form name="form1" method="post" action="" onSubmit="return  ValidarColpapel(form1)">
  <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
  <%java.util.Date date = new java.util.Date();
    SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	String fecpla1 = s.format(date);   
	String fecpla=com.tsp.util.Util.fecha_Zonificada(fecpla1, usuario.getLogin());
	
	//Datos de la orden de carga
	String remitente="a";
	String placa="";
	String trailer="";
	String c1="";
	String c2="";
	String pc1="";
	String pc2="";
	String p1="";
	String p2="";
	String p3="";
	String p4="";
	String p5="";
	String tipotra="NAV";
	String tipocon="NAV";
	String imagen ="equis2.gif";
    String imagenD ="equis2.gif";
	String fecha_cargue="";
	String destinatario = "a";
	if(model.imprimirOrdenService.getHojaOrden()!=null){
		HojaOrdenDeCarga oc = model.imprimirOrdenService.getHojaOrden();
		imagen ="ok2.gif";
		remitente=oc.getRemitente();
		placa=oc.getPlaca();
		trailer=oc.getTrailer();
		fecha_cargue=oc.getFecha_cargue();
		if(oc.getContenedores()!=null){
			String vec[] =oc.getContenedores().split(",");
			c1=vec.length>0?vec[0]:"";
			c2=vec.length>1?vec[1]:"";
		}
		pc1=oc.getPrecintoc1();
		pc2=oc.getPrecintoc2();
		p1=oc.getPrecinto1();
		p2=oc.getPrecinto2();
		p3=oc.getPrecinto3();
		p4=oc.getPrecinto4();
		p5=oc.getPrecinto5();
		tipotra=oc.getTipotrailer();
		tipocon=oc.getTipocont();
		destinatario = oc.getDestinatarios();
		if(!destinatario.equals("")){
			imagenD ="ok2.gif";
		}
	}
	
               %>
			   
  <table width="100%"  border="2">
    <tr>
      <td>
	    <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="22" colspan=2 class="subtitulo1">Informacion Despacho </td>
            <td width="368" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
          </tr>
        </table>
	    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td height="22" class="subtitulo1">Datos Remesa </td>
          </tr>
      </table>                  <table width="100%" align="center" class="tablaInferior"  >
        <tr class="fila">
          <th colspan="2" scope="row"><div align="left">Estandar Job</div>
              <span class="Estilo5 Estilo8"> </span><strong></strong></th>
          <td colspan="2"><span class="Estilo5 Estilo8">
            <% 
	  Stdjobdetsel sj=new Stdjobdetsel();
	  String standard = request.getParameter("standard");
	  String pa ="";
	  String uw="";
	  String vlr ="";
	  String origen ="";
	  float unidad_default = 0;
	  
      if(model.stdjobdetselService.getStandardDetSel()!=null){
			sj = model.stdjobdetselService.getStandardDetSel();
            uw=sj.getUnit_of_work();
            pa=sj.getPorcentaje_ant();
			vlr = ""+sj.getVlr_freight();             
			origen = sj.getOrigin_code();
			unidad_default=sj.getUnidades();
			//out.println(origen);
            }%>
            <%=sj.getSj()+"-"+sj.getSj_desc()%><%=sj.getPagador()%>
            <input name="standard" type="hidden" id="standard" value="<%=request.getParameter("standard")%>">
            </span><strong>
            <input name="sj_nombre" type="hidden" id="sj_nombre" value="<%=sj.getSj()+"-"+sj.getSj_desc()%> ">
            <input name="cmd" type="hidden" id="cmd" value="<%=request.getParameter("cmd")%>">
            <input name="orden" type="hidden" id="orden" value="<%=request.getParameter("orden")%>">
            </strong></td>
          <td width="16%">FECHA DESPACHO </td>
          <td width="19%" colspan="2"><input name="fechadesp" type="text" class="textbox" id="fechadesp3" value="<%=fecpla.toLowerCase()%>" size="18"></td>
          </tr>
        <tr class="fila">
          <th colspan="2" scope="row"><div align="left">Informaci&oacute;n al Cliente </div></th>
          <td colspan="5"><span class="Estilo6">
            <input name="remitentes" type="hidden" id="remitentes" value="<%=remitente%>">
            <a class="Simulacion_Hiper" style="cursor:hand "  onClick="abrirPagina('<%=BASEURL%>/colpapel/remitentes.jsp?sj='+form1.standard.value+'&origen='+form1.origstd.value,'');">Agregar Remitentes
            <input name="origstd" type="hidden" id="origstd2" value="<%=origen%>">
            </a><img src="<%=BASEURL%>/images/<%=imagen%>" width="17" height="17" id="imre">
            <input name="imagenre" type="hidden" id="imagenre" value="<%=BASEURL%>/images/<%=imagen%>">
            <input name="imagendest" type="hidden" id="imagendest" value="<%=BASEURL%>/images/<%=imagenD%>">
            <input name="destinatarios" type="hidden" id="destinatarios" value="<%=destinatario%>" >
           <%if(request.getParameter("cmd")!=null){
				if(request.getParameter("cmd").equals("ocargue")){%>
				<a class="Simulacion_Hiper" style="cursor:hand " onClick="abrirPagina('<%=CONTROLLER%>?estado=Buscar&accion=Destinatarios&sj='+form1.standard.value+'&ocargue=<%=request.getParameter("orden").toUpperCase()%>&destinatario=<%=destinatario%>&despacho=ok','');">Agregar Destinatarios</a>
					 
		 		<%}%>
				 
		<% 	}else{%>
		<a class="Simulacion_Hiper" style="cursor:hand " onClick="abrirPagina('<%=CONTROLLER%>?estado=Buscar&accion=Destinatarios&sj='+form1.standard.value,'');">Agregar Destinatarios</a>
<%		 }%>
			<img src="<%=BASEURL%>/images/<%=imagenD%>" width="17" height="17" id="imdest"> 
            <input name="docinterno" cols="38" id="docinterno3" type="hidden">
            <input name="docudest" type="hidden" id="docudest3" value="">
            <input name="escrito" type="hidden" id="escrito" value="NO HAY NADA">
            <input name="fcial" type="HIDDEN" id="fcial" size="70">
            <a class="Simulacion_Hiper" onClick="abrirPagina('<%=CONTROLLER%>?estado=Aplicar&accion=Documentos&generar=ok','')" style="cursor:hand ">Agregar Documentos Relacionados</a><strong> </strong></span></td>
        </tr>
        <tr class="fila">
          <th colspan="2" scope="row"><div align="left">Cantidad a Facturar </div></th>
          <td width="30%"><span class="Estilo6"><span class="Estilo8">
            <input name="cfacturar" type="text" class="textbox" id="cfacturar3"  onKeyPress="soloDigitos(event,'decOK')" value="<%=unidad_default%>" size="12">
          </span>
              <input name="vrem" type="hidden" id="vrem4" readonly value="<%=vlr%>" >
              <input name="vrem2" type="hidden" id="vrem23">
              <input name="vremesa" type="hidden" id="vremesa3" readonly>
            </span></td>
          <td width="17%">Unidad de Facturacion </td>
          <td colspan="2"><strong><%=uw%>
              <input name="uw" type="hidden" id="uw3" value="<%=uw%>">
          </strong>            </td>
          <td rowspan="2"><table width="100%"  border="0" cellpadding="0" cellspacing="0" class="filaresaltada">
            <tr>
              <td><input name="facturable" type="checkbox" id="facturable4" value="N">
No Facturable</td>
            </tr>
            <tr>
              <td><input name="cdock" type="checkbox" id="cdock" value="N">
Cross Docking </td>
            </tr>
            <tr>
              <td><input name="cadena" type="checkbox" id="cadena" value="N">
                Cadena</td>
            </tr>
          </table>            </td>
        </tr>
        <tr class="fila">
          <th colspan="2" class="Estilo7" scope="row"><div align="left"><span class="Estilo8">Cantidad de Empaque</span></div></th>
          <td><span class="Estilo8">
            <input name="cantreal" type="text" class="textbox" id="cantreal3" onKeyPress="soloDigitos(event,'decOK')" size="12">
          </span> </td>
          <td><strong><span class="Estilo8">Unidad de Empaque</span></strong></td>
          <td colspan="2"><%TreeMap unidades = model.unidadService.getUnidades(); 
	 
	  %>
              <input:select name="unidad" options="<%=unidades%>" attributesText="style='width:100%;' class='textbox'" default="" /></td>
          </tr>
		</table>
		      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tablaInferior">
          <tr>
            <td height="22" class="subtitulo1">Datos Planilla </td>
            </tr>
        </table>		
		<table width="100%" class="tablaInferior">
        <tr class="filaresaltada">
          <th height="26" colspan="2" scope="row"><div align="left"><span class="Estilo7">Placa</span></div></th>
          <td colspan="7"><input name="placa" type="text" class="textbox" id="placa2" size="12" value="<%=placa%>"></td>
        </tr>
        <tr class="filaresaltada">
          <th width="85" rowspan="2" class="Estilo7" scope="row"><div align="left">Trailer</div></th>
          <th width="73" rowspan="2" class="Estilo7" scope="row"><div align="left">
            <input name="trailer" type="text" class="textbox" id="trailer" size="12" value="<%=trailer%>">
          </div></th>
          <td colspan="2"><strong>&iquest;Trailer de TSP? </strong></td>
          <td width="86" rowspan="2"><strong>Contenedores</strong></td>
          <td width="93"><input name="c1" type="text" class="textbox" id="c1" size="15" value="<%=c1%>" onKeyPress="return soloNumText(event)"></td>
          <td width="82" rowspan="2"><strong>
            <input name="tipo_cont" type="radio" value="FINV" <%if(tipocon.equals("FINV")){%>checked<%}%> >
TSP</strong><br>
<input name="tipo_cont" type="radio" value="NAV" <%if(tipocon.equals("NAV")){%>checked<%}%> >
<strong>Naviera</strong></td>
          <td width="116" rowspan="2"><span class="Estilo8"><strong>Precintos de Contedores </strong></span></td>
          <td width="328"><input name="c1precinto" type="text" class="textbox" id="c1precinto" value="<%=pc1%>" onChange="controlPrecintoC('<%=CONTROLLER%>?estado=Colpapel&accion=Validar&cmd=show');" size="20"></td>
        </tr>
        <tr class="filaresaltada">
          <td height="31" colspan="2"><strong>
            <input name="tipo_tra" type="radio" value="FINV" <%if(tipotra.equals("FINV")){%>checked<%}%>>
Si </strong><br>
<input name="tipo_tra" type="radio" value="NAV" <%if(tipotra.equals("NAV")){%>checked<%}%>>
<strong>No</strong></td>
          <td width="93"><input name="c2" type="text" class="textbox" id="c23" size="15" value="<%=c2%>" onKeyPress="return soloNumText(event)"></td>
          <td width="328"><input name="c2precinto" type="text" class="textbox" id="c2precinto"  value="<%=pc2%>" onChange="controlPrecintoC('<%=CONTROLLER%>?estado=Colpapel&accion=Validar&cmd=show');" size="20"></td>
        </tr>
		</table>		
		<table width="100%" class="tablaInferior">
          <tr class="filaresaltada">
            <th width="13%" height="26" rowspan="3" scope="row"><div align="left">Ruta Planilla </div></th>
            <th colspan="2" rowspan="3" scope="row"><div align="left">
                  <% List rutas = model.stdjobcostoService.getRutas(request.getParameter("standard"));%>
                  <select name="ruta" class="textbox" id="select5" onChange="cambiarFormulario2('<%=BASEURL%>/colpapel/segundaDespacho.jsp')">
                    <option value="0">Seleccione Alguna</option>
                    <%
		  	String rutaD = request.getParameter("ciudadOri")+request.getParameter("ciudadDest");
		   	Iterator it=rutas.iterator();
				while (it.hasNext()){
					Stdjobcosto std = (Stdjobcosto) it.next();
					String ruta = std.getRuta();
					String codigo=std.getFt_code();
					%>
                    <option value="<%=codigo%>"><%=ruta%></option>
                    <%}%>
                  </select>
            </div></th>
            <th width="15%" scope="row"> Cantidad a Despachar
              <%
	  String unit_transp="";
	  %></th>
            <th colspan="3" scope="row">              <div align="left">
                <input name="toneladas" type="text" class="textbox" id="toneladas4"  onKeyPress="soloDigitos(event,'decOK')" size="12">
                <%=unit_transp%><span class="Estilo8">              </span><span class="Estilo8">
                </span>              <strong><span class="Estilo8">
              </span> </strong></div></th>
            </tr>
          <tr class="filaresaltada">
            <th scope="row">Valor del Flete
              <% List costos = model.stdjobcostoService.getCostos("....",request.getParameter("standard"));%></th>
            <th width="10%" scope="row"><select name="valorpla" class="textbox" id="select7" onChange="">
              <option value="0" title="">Seleccione Alguna</option>
              <%
		   	it=costos.iterator();
				while (it.hasNext()){
					Stdjobcosto std = (Stdjobcosto) it.next();
					float costo = std.getUnit_cost();
					String codigo=std.getFt_code();
					String vacio ="";
					if(std.getCf_code().length()>=7){
						if(std.getCf_code().substring(6,7).equals("V"))
							vacio = "-Vacio";
					}
					String unit = std.getUnit_transp();
					%>
              <option value="<%=codigo%>"><%=costo%> <%=std.getCurrency()%>/<%=unit%><%=vacio%></option>
              <%}%>
            </select></th>
            <th scope="row"><span class="Estilo8"><strong>Nuevo Valor Aplicado </strong></span></th>
            <th width="15%" scope="row"><strong><span class="Estilo8">
              <input name="otro" type="text" class="textbox" id="otro2"  onKeyPress="soloDigitos(event,'decNo')" value="NO" readonly>
            </span></strong></th>
          </tr>
          <tr class="filaresaltada">
            <th colspan="2" scope="row"><strong>Valor Total de la Planilla </strong></th>
            <th height="21" colspan="2" scope="row"><span class="Estilo8"></span><span class="Estilo8"><strong>
              <input name="voc" type="text" class="textbox" id="voc" value="0" size="15" readonly>
              <input name="vocH" type="hidden" id="vocH" value="0">
            </strong>
            </span></th>
            </tr>
          <tr class="filaresaltada">
            <th colspan="2" scope="row"><strong><a style="cursor:hand " class="Simulacion_Hiper" onClick="window.open('<%=BASEURL%>/colpapel/extrafletes.jsp?sj=<%=request.getParameter("standard")%>&voc='+form1.vocH.value+'&extraflete='+form1.extraflete.value,'Extrafletes','height=400,width=800,dependent=yes,resizable=yes,scrollbars=yes,status=yes');" >INGRESAR/ MODIFICAR EXTRAFLETES</a></strong></th>
            <th scope="row"><strong><a style="cursor:hand " class="Simulacion_Hiper" onClick="window.open('<%=BASEURL%>/colpapel/costosRembolsables.jsp?sj=<%=request.getParameter("standard")%>&voc='+form1.vocH.value+'&cr='+form1.cr.value,'Costos','height=400,width=800,dependent=yes,resizable=yes,scrollbars=yes,status=yes');" >INGRESAR/ MODIFICAR COSTOS REEMBOLSABLES</a></strong></th>
            <th scope="row">Valor total en extrafletes </th>
            <th scope="row"><div align="left">
              <input name="extraflete" type="text" class="textbox" id="extraflete" value="0" size="12" readonly>
              <input name="cr" type="hidden" id="cr" value="0">
            </div></th>
            <th colspan="2" scope="row">Procentaje maximo de anticipo: <%=pa%>
              <input name="pa" type="hidden" id="pa6" value="<%=pa%>"></th>
            </tr>
          <tr class="filaresaltada">
            <th rowspan="2" scope="row"><span class="Estilo8"><strong>Anticipo maximo permitido </strong></span></th>
            <th width="10%" rowspan="2" scope="row"><div align="left">
              <input name="antmax" type="text" class="textbox" id="antmax" size="15" readonly>
            </div></th>
            <th width="15%" rowspan="2" scope="row">Valor del Anticipo </th>
            <th scope="row"><input name="anticipo" type="text" class="textbox" id="anticipo" onKeyPress="soloDigitos(event,'decNO')" size="12" >
              <strong>
              <input name="moneda" type="text" id="moneda" style="font-size:11px;border:0" size="3"  readonly>
              <input name="antprov" type="hidden" id="antprov">
              </strong></th>
            <th rowspan="2" scope="row"><span class="Estilo8">Banco</span></th>
            <th rowspan="2" scope="row"><div align="left">
                  <%TreeMap bancos= model.buService.getListaBancos(usuario.getLogin()); %>
				                <input:select name="banco" options="<%=bancos%>" attributesText="style='width:100%;' class='textbox' onChange=mostrarMoneda(this.value);" default="" /> 
            </div></th>
            <th scope="row">Beneficiario</th>
          </tr>
          <tr class="filaresaltada">
            <th scope="row"><a style="cursor:hand " class="Simulacion_Hiper" onClick="window.open('<%=BASEURL%>/colpapel/anticipos.jsp','','height=400,width=800,dependent=yes,resizable=yes,scrollbars=yes,status=yes')"; >Aplicar anticipo a proveedor</a> </th>
            <th scope="row"><select name="beneficiario" id="beneficiario">
              <option value="C">Conductor</option>
              <option value="P">Propietario</option>
			  <option value="T">Tercero</option>
            </select></th>
          </tr>
        </table>		
		        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tablaInferior">
          <tr>
            <td height="22" class="subtitulo1">Otros Precintos </td>
            </tr>
        </table>		
		<table width="100%" height="44" class="tablaInferior">
          <tr class="filaresaltada">
            <td width="20%">              <div align="center">
                  <input name="precintos" type="text" class="textbox" id="precintos7" onChange="controlPrecintos('<%=CONTROLLER%>?estado=Colpapel&accion=Validar&cmd=show');" size="20" value="<%=p1%>">
                </div>
		    </td><td ><div align="center">
                <input name="precintos2" type="text" class="textbox" id="precintos23" onChange="controlPrecintos('<%=CONTROLLER%>?estado=Colpapel&accion=Validar&cmd=show');" size="20" value="<%=p2%>">
              </div></td>
           <td width="20%">
		   <div align="center">
              <input name="precintos3" type="text" class="textbox" id="precintos33" onChange="controlPrecintos('<%=CONTROLLER%>?estado=Colpapel&accion=Validar&cmd=show');" size="20" value="<%=p3%>">
            </div></td>
            <td width="20%"><div align="center">
              <input name="precintos4" type="text" class="textbox" id="precintos43" onChange="controlPrecintos('<%=CONTROLLER%>?estado=Colpapel&accion=Validar&cmd=show');" size="20" value="<%=p4%>">
            </div></td>
            <td width="20%"><div align="center">
              <input name="precintos5" type="text" class="textbox" id="precintos53" onChange="controlPrecintos('<%=CONTROLLER%>?estado=Colpapel&accion=Validar&cmd=show');" size="20" value="<%=p5%>">
            </div></td>
          </tr>
        </table>		
		        <%	List listTabla2 = model.tbltiempoService.getTblTiemposSalida(usuario.getBase());
			Iterator itTbla2=listTabla2.iterator();
			if(itTbla2.hasNext()){
				%>
		<table width="100%" class="tablaInferior">
          <tr>
            <td height="22" class="subtitulo1">Fechas YYYY-MM-DD HH:MM 24 Hrs </td>
            </tr>
        </table>
		<table width="100%" height="44" class="tablaInferior">
		
          <tr class="filaresaltada">
		  <%		while(itTbla2.hasNext()){
					Tbltiempo tbl = (Tbltiempo) itTbla2.next();
					String id_tabla=tbl.getTimeCode();%>
				
            <td><div align="left"><%=tbl.getTimeDescription()%></div></td>
			<td><input name="<%=id_tabla%>" type="text" class="textbox" id="<%=id_tabla%>" onChange="formatoFecha(this);" onKeyPress="soloDigitos(event,'decNo');" readonly size="13" maxlength="16" value="<%=id_tabla.equals("ECAR")?fecha_cargue:""%>">
			<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.<%=id_tabla%>);return false;" HIDEFOCUS>
                        <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></img></a>&nbsp
              </td>
			<%}%>
          </tr>
        </table>
		<%}%>
         <%
		Vector descuentos = model.anticiposService.getAnticipos();
		String tipo="";
		if(descuentos.size()>0){%>
		
        <table width="100%" class="tablaInferior">
          <tr>
            <td height="22" class="subtitulo1">Descuentos</td>
            </tr>
        </table>		
        <table width="100%" height="44">
          <tr class="filaresaltada">
		   <%
			for(int k = 0 ; k<descuentos.size();k++){
				Anticipos ant = (Anticipos) descuentos.elementAt(k); 
				String codigo = ant.getAnticipo_code();
				float valor = ant.getValor();
				/*model.proveedoresService.listaAnticipoProvee(codigo);
				Vector provee = model.proveedoresService.getProveedores();*/
				String readonly ="readonly";
				if(ant.getModif().equals("Y")){
					readonly ="";
				}
				tipo = ant.getTipo_s();
			%>
			<td>
			<%=ant.getAnticipo_desc().toUpperCase()%>
			</td>
            <td>
			<input name="<%=ant.getAnticipo_code()%>" type="text" class="textbox" onKeyPress="soloDigitos(event,'decNo')" value="<%=valor%>" size="15" <%=readonly%>  >
            <%=tipo.equals("P")?"%":""%>
            <input name="check<%=ant.getAnticipo_code()%>" type="checkbox" value="checkbox" <%if(ant.isChequeado()){%> checked <%}%>>
			</td>
        <%}%>
        </tr>
        </table>
		<%}%>
        
        <table width="100%" class="tablaInferior">
          <tr>
            <td height="22" class="subtitulo1">Observaciones</td>
            </tr>
        </table>       
        <table width="100%" class="tablaInferior">
          <tr class="filaresaltada">
            <th width="55%" height="30" align="center" valign="top" scope="row"><div align="left">
              <textarea name="observacion" style="width:100%" class="textbox" id="textarea" onKeyPress="return soloNumText(event)"></textarea>
            </div></th>
            <th width="21%" height="16" scope="row"><div align="left">Numero del Sticker
			</div></th>
            <th width="24%" scope="row"><div align="left">
              <input name="sticker" type="text" id="sticker" onKeyPress="soloDigitos(event,'decOK')">
            </div></th>
          </tr>
          <tr class="filaresaltada">
            <th width="55%" height="20" align="center" valign="top" scope="row"><div align="left"><span class="Estilo6"><strong><a class="Simulacion_Hiper" style="cursor:hand " onClick="abrirPagina('<%=BASEURL%>/colpapel/inventarios.jsp?sj='+form1.standard.value,'');">Despacho mercancia en inventario.</a></strong></span> <span class="Estilo6">
              <input name="inventarios" type="hidden" id="inventarios" value="a" size="80">
            </span></div></th>
            <td height="43" rowspan="2" scope="row"><div align="left">Valor estimado de la mercancia </div></td>
            <td height="43" rowspan="2" scope="row"><input name="vlrmercan" type="text" id="vlrmercan" onKeyPress="soloDigitos(event,'decOK')"></td>
          </tr>
          <tr class="filaresaltada">
            <th height="21" align="center" valign="top" scope="row"><div align="left"><span class="Estilo6"><strong><a class="Simulacion_Hiper" style="cursor:hand " onClick="abrirPagina('<%=CONTROLLER%>?estado=Load&accion=Imagenes&pagina=/imagen/Manejo.jsp&evento=RESET&actividad=004&tipoDocumento=001&documento=&procedencia=DESPACHO','');">Anexar Imagen al despacho.</a></strong></span></div></th>
          </tr>
        </table>	    </td>
	  </tr>
    </table>
        <p align="center">          <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand"> </p>
</form>
</div>

<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins_24.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
<%=datos[1]%>
</body>
</html>
