<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Descripcion Campos anular planilla</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<br>


<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Despacho </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2"> Anular Despacho</td>
        </tr>
        <tr>
          <td width="149" class="fila"> Numero de la planilla</td>
          <td width="525"  class="ayudaHtmlTexto">Campo para digitar el c&oacute;digo de la planilla representado por letras y numeros, para buscar el despacho relacionado a esa. </td>
        </tr>
        
      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Despacho </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2"> Anular Despacho</td>
        </tr>
        <tr>
          <td width="149" class="fila"> CAUSA DE ANULACION </td>
          <td width="525"  class="ayudaHtmlTexto">Campo de seleccion donde se escoge la causa por la cual se va a realizar la anulacion de un despacho.. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> OBSERVACIONES </td>
          <td width="525"  class="ayudaHtmlTexto">Campo de texto donde se digitan las observaciones pertinentes a la anulaci&oacute;n del despacho. </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
