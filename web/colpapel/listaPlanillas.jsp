<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Modificar Despacho</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</script>

</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=DESPACHO"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
  

  <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>

  <table width="70%"  border="2" align="center">
    <tr>
      <td><table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="barratitulo">
        <tr>
          <td width="50%" class="subtitulo1" colspan='3'>Lista de Planillas </td>
          <td width="50%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif"></td>
        </tr>
      </table>        <table width="100%" border="1" align="center" bordercolor="#999999" class="Letras">
        <tr class="tblTitulo">
          <td width="82" nowrap><strong>PLANILLA</strong></td>
          <td width="79"><div align="center"><strong>REMESA</strong></div></td>
          <td width="74"><div align="center"><strong>PLACA</strong></div></td>
          <td width="111" class="Estilo4"><div align="center" class="Estilo10 Estilo2 Estilo3"> <strong>CONDUCTOR</strong></div></td>
          <td width="150" class="Estilo4"><div align="center"><strong>NOMBRE CONDUCTOR</strong></div></td>
        </tr>
        <%  List planillas= model.remesaService.buscarPlanillas(request.getParameter("norem"));
	  Iterator pla=planillas.iterator();
	  int i=0;
	  while (pla.hasNext()){
	  i++;
	  Planilla planilla = (Planilla) pla.next();
		%>
        <tr class="<%=i%2==0?"filagris":"filaazul"%>">
          <td nowrap class="bordereporte">
            <div align="center" class="Estilo1 Estilo5"> <%=planilla.getNumpla()%></div></td>
          <td class="bordereporte">
            <div align="center" class="Estilo6"><%=planilla.getNumrem()%> </div></td>
          <td class="bordereporte">
            <div align="center" class="Estilo6"><%=planilla.getPlaveh()%> </div></td>
          <td class="bordereporte"><div align="center" class="Estilo4"><%=planilla.getCedcon()%></div></td>
          <td class="bordereporte"><span class="Estilo6"><%=planilla.getNomCond()%></span></td>
        </tr>
        <%}
  %>
      </table></td>
    </tr>
  </table>
  </DIV>
</body>
</html>
