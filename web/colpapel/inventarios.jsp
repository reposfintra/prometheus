<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Manejo de inventario</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/validar.js">
</script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">

</head>

<body onLoad="iniciarCheckInventario();">
<form action="<%=CONTROLLER%>?estado=Aplicar&accion=Inventario&cmd=aplicar" method="post" name="form2" id="form2">
  <table width="90%"  border="2" align="center" cellpadding="0" cellspacing="0">
	<tr>
	  <td>
  <table width="100%" class="tablaInferior">
    <tr>
      <td height="22" colspan=2 class="subtitulo1">Inventarios de Productos <span class="Letras"></span></td>
      <td width="232" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
    </tr>
  </table>
  <table width="100%" border="1">
    <tr class="filaresaltada"  >
      <td colspan="2"> Ubicacion      </td>
      <td colspan="2">	  <%
	  String sj1 = request.getParameter("sj");
	  String cliente1=sj1.substring(0,3);
	  TreeMap ciudades = model.cliente_ubicacionService.getUbicacion_prod();
	  %>
        <input name="cliente" type="hidden" id="cliente" value="000<%=cliente1%>">      
        <input name="controller" type="hidden" id="controller" value="<%=CONTROLLER%>">
        <input name="sj" type="hidden" id="sj" value="<%=request.getParameter("sj")%>"> 
        <input name="modif" type="hidden" id="modif" value="<%=request.getParameter("modif")%>"> 
        <input name="numpla" type="hidden" id="numpla" value="<%=request.getParameter("numpla")%>">      
        <input:select name="ciudad" options="<%=ciudades%>" attributesText="onChange='buscarProductos(this.value);'" default='<%=request.getParameter("ubicacion")%>'/>	  </td>
    </tr>
	<tr class="tblTitulo">
	<td colspan="2"  bordercolor="#999999">Producto</td>
	<td  bordercolor="#999999">Cantidad </td>
	<td  bordercolor="#999999">Nota Debito </td>
	</tr>
    <%
	Vector productos = model.productoService.getProductos();
	if(productos == null){
		productos=new Vector();
	}
	if(request.getParameter("ubicacion")==null){
		productos=new Vector();
	}
	for(int i=0; i<productos.size(); i++){
	Producto p = (Producto) productos.elementAt(i);
	String coddest=p.getCodigo()+"/"+p.getDiscrepancia()+"/"+request.getParameter("ubicacion");
    	%>
	<tr class="<%=i%2==0?"filagris":"filaazul"%>">
	    <td width="7%" class="bordereporte"> <input name="check<%=coddest%>" type="checkbox" id="check<%=coddest%>" value="<%=coddest%>" <%=p.isUtilizado()?"selected":""%> onClick="onCheckDInventario(this.name,'<%=BASEURL%>');"></td>
      <td width="39%" class="bordereporte"><span class="Letras"><%=p.getDescripcion()%><strong>
   
    </strong></span></td>
      <td width="27%" class="bordereporte"><%=p.getUnidad()%></td>
	  
	  <td width="27%" class="bordereporte"><%=p.getNota_debito()%></td>
	</tr>
	 <%
	 }%>
	<tr class="filaresaltada">
	  <td colspan="4"><div align="center">
	  </div></td>
    </tr>
  </table>
  </td>
  </tr></table>
  <div align="center"><br>
    <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">
  </div>
</form>
</body>
</html>
