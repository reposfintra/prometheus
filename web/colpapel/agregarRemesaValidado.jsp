<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*,com.tsp.util.*,java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Agregar remesa valido</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/validar.js">
</script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<%String isAnular =  request.getParameter("anular")!=null?!request.getParameter("anular").equals("")?"SI":"NO":"NO";%>
</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Agregar Remesa"/>
</div>

 <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<form name="form2" method="post" action="<%=CONTROLLER%>?estado=Planilla&accion=Search&colpapel=ok&nremesa=ok">
  <table width="57%"  border="2" align="center">
    <tr>
      <td>
        <table width="100%" align="center" class="tablaInferior">
          <tr>
            <td height="22" colspan=2 class="subtitulo1">Buscar Planilla<span class="titulo"><strong><br>
            </strong></span></td>
            <td width="308" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
        </table>
        <table width="100%" border="0" align="center" class="fila">
          <tr class="filaresaltada">
            <td width="162" nowrap><strong>Numero de la Planilla:</strong></td>
            <td width="352" nowrap><input name="planilla" type="text" id="planilla3" maxlength="10">
                <img src="<%=BASEURL%>/images/botones/buscar.gif" width="87" height="21" onClick="form2.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
          </tr>
      </table></td>
    </tr>
  </table>
</form>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Remesa&accion=Insert">
  <div align="center">
      <%Usuario usuario = (Usuario) session.getAttribute("Usuario");
	  java.util.Date date = new java.util.Date();
	  String placa="", trailer="", conductor="", codruta="",nomcond="", estandard="",  standar="", remitente="", destinatario="",docinterno="", fechadesp="", observacion="", rutaP="", precinto="", planilla="" ;
	  float ant=0, peso=0,vpla=0;
      if(request.getAttribute("planilla")!=null){
        Planilla pla= (Planilla) request.getAttribute("planilla");
        placa=pla.getPlaveh();
		trailer=pla.getPlatlr();
		conductor= pla.getCedcon();
		nomcond=pla.getNomCond();
		estandard=pla.getSj_desc();
		standar= pla.getSj();
		fechadesp = pla.getFecdsp();
		peso=pla.getPesoreal();
		rutaP = pla.getRuta_pla();
		codruta = pla.getCodruta();
		precinto = pla.getPrecinto();
		planilla= pla.getNumpla();
		vpla = pla.getVlrpla();
    }
   
	 String numremP="";
      String origen ="";
		String pa ="";
	  	String uw="";
	  	String vlr ="";
	  	Stdjobdetsel sj=new Stdjobdetsel();
	  	String standard = request.getParameter("standard");
	  	model.stdjobdetselService.searchStdJob(request.getParameter("standard"));
        if(model.stdjobdetselService.getStandardDetSel()!=null){
			sj = model.stdjobdetselService.getStandardDetSel();
			uw=sj.getUnit_of_work();
            pa=sj.getPorcentaje_ant();
			vlr = ""+sj.getVlr_freight();   
            origen = sj.getOrigin_code();     
			if(sj.getVlr_pes()!=0){
				vlr =  ""+sj.getVlr_pes();
			}             
          }
		  SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd hh:mma");
	String fecpla = s.format(date); 
		  
  %>
    <table width="98%"  border="2" align="center">
      <tr>
        <td>
          <table width="100%" align="center" class="tablaInferior">
            <tr>
              <td height="22" colspan=2 class="subtitulo1">Agregar Remesa <span class="titulo"><strong><br>
              </strong></span></td>
              <td width="308" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
            </tr>
          </table>
          <table width="100%" align="center" class="tablaInferior">
            <tr>
              <td height="22" class="subtitulo1">Infromacion de la planilla <span class="titulo"><%=planilla%>
                    <input name="planilla" type="hidden" id="planilla2" value="<%=planilla%>">
                    <input name="anular" type="hidden" id="anular" value="<%=request.getParameter("anular")!=null?request.getParameter("anular"):""%>">
              </span></td>
            </tr>
          </table>
          <table width="100%" class="tablaInferior">
            <tr >
              <th height="26" scope="row" class="filaresaltada">Placa</th>
              <td width="86" height="26" class="letra" scope="row"><%=placa%></td>
              <td width="94" height="26" class="filaresaltada" scope="row">Trailer</td>
              <td width="91" class="letra"><%=trailer%></td>
              <td width="228" class="filaresaltada">Conductor</td>
              <td colspan="2" class="letra"><%=conductor%> <%=nomcond%></td>
            </tr>
            <tr>
              <th width="130" class="filaresaltada" scope="row">Precintos</th>
              <td height="31" colspan="2" class="letra" scope="row"><%=precinto%></td>
              <td class="filaresaltada">Ruta Planilla</td>
              <td class="letra"><%=rutaP%>
                  <input name="rutaP2" type="hidden" id="rutaP6" value="<%=codruta%>">              </td>
              <td width="142" class="filaresaltada">Cantidad Depachada</td>
              <td width="158" class="letra"><%=peso%></td>
            </tr>
            <tr>
              <th height="31" colspan="2"  class="filaresaltada" scope="row">Valor Total de la Planilla</th>
              <td height="31" colspan="2"  class="letra" scope="row"><%=com.tsp.util.Util.customFormat(vpla)%></td>
              <td class="filaresaltada">Valor del Anticipo</td>
              <td colspan="2" class="letra"><%=com.tsp.util.Util.customFormat(ant)%></td>
            </tr>
          </table>
          <table width="100%" border="1" align="center" class="Letras">
            <tr>
              <td colspan="4" nowrap class="subtitulo1"><div align="left"><strong>LISTA REMESAS RELACIONADAS </strong></div></td>
            </tr>
            <tr bordercolor="#999999" class="tblTitulo">
              <td align="center">REMESA</td>
              <td align="center">CLIENTE</td>
              <td  align="center">ESTANDAR</td>
              <td align="left">PORCENTAJE</td>
            </tr>
            <%  int i=0;
		if(!planilla.equals("")){
	  List remesas= model.planillaService.buscarRemesas(planilla);
	  Iterator rem=remesas.iterator();
	  
	  while (rem.hasNext()){
	  	i++;
	  %>
            <%Remesa remesa = (Remesa) rem.next();
	  if(!remesa.getNumrem().equals(request.getParameter("anular"))){
	  numremP = remesa.getNumrem();
		%>
            <tr class="<%=i%2==0?"filagris":"filaazul"%>">
              <td  class="bordereporte">
                <div align="center"><%=remesa.getNumrem()%> </div></td>
              <td class="bordereporte">
                <div align="center" ><%=remesa.getCliente()%> </div></td>
              <td  class="bordereporte"><div align="center"><%=remesa.getDescripcion()%></div></td>
              <td class="bordereporte">
                <input name="por<%=remesa.getNumrem()%>" type="text" id="por<%=remesa.getNumrem()%>" size="5" maxlength="3" value="<%=request.getParameter("por"+remesa.getNumrem())%>"  onKeyPress="soloDigitos(event,'decNo')" >
                <input name="vlr<%=remesa.getNumrem()%>" type="HIDDEN" id="vlr<%=remesa.getNumrem()%>" value="<%=remesa.getVlr_pesos()==0?remesa.getVlrRem():remesa.getVlr_pesos()%>"></td>
            </tr>
            <%}
		else{%>
            <input name="por<%=remesa.getNumrem()%>" type="hidden" id="P<%=remesa.getNumrem()%>" size="5" maxlength="3" value="0"  onKeyPress="soloDigitos(event,'decNo')" readonly>
            <%}
		}
		}
  i++;
  %>
  	<tr class="<%=i%2==0?"filagris":"filaazul"%>">
           <td colspan="3"  class="bordereporte"><div align="right"><b>PORCENTAJE NUEVA REMESA</b></div></td>
           <td class="bordereporte"><input name="porcent" type="text" id="porcent2" onKeyPress="soloDigitos(event,'decNo')" value="<%=request.getParameter("porcent")%>" size="5" maxlength="3"></td>
         </tr>
     </table>
	     </td>
   </tr>
      </table>                    </td>
      </tr>
    </table>
    <p>&nbsp;    </p>
    <table width="98%"  border="2" align="center">
      <tr>
        <td>
          <table width="100%" align="center" class="tablaInferior">
            <tr>
              <td height="22" colspan=2 class="subtitulo1"><span class="Estilo6 Estilo11">
         
		
		
<%
numremP="";
if(model.remesaService.getRemesa()!=null){
     	Remesa reme = model.remesaService.getRemesa();
		numremP = reme.getNumrem();
        remitente = reme.getRemitente()!=null?reme.getRemitente():"";
        destinatario= reme.getDestinatario();
        	
}%>

                </span>Informacion de la nueva remesa<span class="titulo"><strong><br>
              </strong></span></td>
              <td width="308" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
            </tr>
          </table>
          <table width="100%" align="center"  >
            <tr class="fila">
              <th colspan="2" scope="row"><div align="left">Estandar Job</div></th>
              <td colspan="2"><span class="Estilo17"><%=sj.getSj()+"-"+sj.getSj_desc()%>
                    <input name="standard" type="hidden" id="standard2" value="<%=request.getParameter("standard")%>">
              </span></td>
              <td width="15%">Fecha Remesa </td>
              <td colspan="2"><input name="fechadesp2" type="text" id="fechadesp2" size="18" value="<%=fecpla%>"></td>
            </tr>
            <tr class="fila">
              <th colspan="2" scope="row"><div align="left">Informaci&oacute;n al Cliente </div></th>
              <td colspan="5"><span class="Estilo6"> <strong>
                <input name="remitentes" type="hidden" id="remitentes" value="<%=request.getParameter("remitentes")%>">
                <a class="Simulacion_Hiper" style="cursor:hand " onClick="abrirPagina('<%=BASEURL%>/colpapel/remitentes.jsp?sj='+form1.standard.value+'&origen='+form1.origstd.value,'');"><strong>
                <input name="remitentesR" type="hidden" id="remitentesR2" value="<%=request.getParameter("remitentesR")%>">
                </strong>Agregar Remitentes</a> <img src=<%=request.getParameter("imagenre")%> width="17" height="17" id="imre">
                <input name="imagenre" type="hidden" id="imagenre2" value="<%=request.getParameter("imagenre")%>">
                <input name="imagendest" type="hidden" id="imagendest2" value="<%=request.getParameter("imagendest")%>">
                <input name="destinatarios" type="hidden" id="destinatarios" value="<%=request.getParameter("destinatarios")%>" readonly>
                <a class="Simulacion_Hiper" style="cursor:hand " onClick="abrirPagina('<%=CONTROLLER%>?estado=Buscar&accion=Destinatarios&numrem=<%=numremP%>&sj='+form1.standard.value,'');"><strong>
                <input name="destinatarioR" type="hidden" id="destinatarioR" value="<%=request.getParameter("destinatarioR")%>">
                </strong>Agregar Destinatarios</a>
                <input name="docudest" type="hidden" id="docudest2" value="<%=request.getParameter("docudest")%>">
                <input name="origstd" type="hidden" id="origstd2" value="<%=request.getParameter("origstd")%>">
                <img src="<%=request.getParameter("imagendest")%>" width="17" height="17" id="imdest">
                <input name="docinterno" type="hidden" id="docinterno" size="38" value="<%=request.getParameter("docinterno")%>">
                <input name="fcial" type="hidden" id="fcial" size="70" value="<%=request.getParameter("fcial")%>">
                <a class="Simulacion_Hiper" onClick="abrirPagina('<%=CONTROLLER%>?estado=Aplicar&accion=Documentos&generar=ok','')" style="cursor:hand ">Agregar Documentos Relacionados</a> </strong> </span> </td>
            </tr>
            <tr class="fila">
              <th colspan="2" scope="row"><div align="left">Cantidad a Facturar </div></th>
              <td width="29%"><span class="Estilo6"><span class="Estilo8"><span class="Estilo1">
                <input name="cfacturar" type="text" id="cfacturar"  onKeyPress="soloDigitos(event,'decOK')" onKeyUp="buscarValorOt();" value="<%=request.getParameter("cfacturar")%>" onChange="cambiarFormulario('<%=CONTROLLER%>?estado=Remesa&accion=Validar')">
                <input name="vlrrem" type="hidden" id="vlrrem" readonly value="<%=request.getParameter("vlrrem")%>">
                <input name="vremesa" type="hidden" id="vremesa" value="<%=request.getParameter("vremesa")%>" readonly>
                <input name="vrem2" type="hidden" id="vrem2">
              </span> </span> </span></td>
              <td width="16%">Unidad de Facturacion </td>
              <td colspan="2"><strong><%=request.getParameter("uw")%>
                    <input name="uw" type="hidden" id="uw" value="<%=request.getParameter("uw")%>">
              </strong> </td><%String fac = "";
	  if(request.getParameter("facturable")!=null){
	  	fac = "checked";
	  }%>
              <td width="13%" rowspan="2">
                                <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="filaresaltada">
                  <tr>
                    <td><input name="facturable" type="checkbox" id="facturable" value="N" <%=fac%> >
      No Facturable</td>
                  </tr>
                  <tr>
                    <td><input name="cdock" type="checkbox" id="cdock" value="N" <%=request.getParameter("cdock")!=null?"checked":""%>>
      Cross Docking </td>
                  </tr>
                  <tr>
                    <td><input type="checkbox" name="cadena" value="N" <%=request.getParameter("cadena")!=null?"checked":""%>>
      Cadena</td>
                  </tr>
                </table></td>
            </tr>
            <tr class="fila">
              <th colspan="2" class="Estilo7" scope="row"><div align="left"><span class="Estilo8">Cantidad de Empaque</span></div></th>
              <td><span class="Estilo8"><span class="Estilo1">
                <input name="cantreal" type="text" id="cantreal" size="12" value="<%=request.getParameter("cantreal")%>">
              </span></span></td>
              <td><strong><span class="Estilo8">Unidad de Empaque</span></strong></td>
              <td colspan="2"><%TreeMap unidades = model.unidadService.getUnidades(); 
	 
	  %>
                  <input:select name="unidad" options="<%=unidades%>" attributesText="style='width:100%;'" default='<%=request.getParameter("unidad")%>' /></td>
              </tr>
          </table>
          <table width="100%" class="tablaInferior">
            <tr>
              <td height="22" class="subtitulo1">Observaciones</td>
            </tr>
          </table>
          <table width="100%" height="44">
            <tr class="filaresaltada">
              <th height="26" scope="row"><div align="left">
                  <textarea name="observacion" cols="60" id="textarea2" ><%=request.getParameter("observacion")%></textarea>
              </div></th>
            </tr>
        </table></td>
      </tr>
    </table>
    <p>&nbsp;    </p>
    <p>
      <img name="imageField"  src="<%=BASEURL%>/images/botones/aceptar.gif" width="90" height="21" border="0" onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);" onClick="if(ValidarColpapelR(form1,'<%=isAnular%>')){form1.submit();this.disabled=true;}">
      <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">  </p>
  </div>
  </form>
<iframe width=188 height=166 name="gToday:datetime:agenda.js:gfPop:plugins_12.js" id="gToday:datetime:agenda.js:gfPop:plugins_12.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</div>
</body>
</html>
