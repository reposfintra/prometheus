<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="js/validar.js">
</script>

<link href="../css/estilo.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilo.css" rel="stylesheet" type="text/css">
<script language="JavaScript" type="text/JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</head>

<body>
<%//Inicializo variables
String estandar="", trailer="", cliente="", fecha="",standar="", observacion="",docuinterno="", remitentes="", destinatarios="", remesa="", planilla="", cedula="", nombre="", placa="", precinto=""; 
float peso=0, valor=0;
List planillas=new LinkedList();
String remesalista=request.getParameter("remesalista"); 
String color="";
if(request.getAttribute("standar")!=null){
	color = (String)request.getAttribute("standar");
}

%>
<form name="form2" method="post" action="<%=CONTROLLER%>?estado=Remesa&accion=Search&lista=ok">
  <table width="530" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="ECE0D8" class="letra">
    <tr>
      <td colspan="2" nowrap><div align="center" class="titulo"><strong><strong>BUSQUE LA REMESA </strong></strong></div></td>
    </tr>
    <tr bgcolor="#99CCFF" class="fila">
      <td width="162" nowrap bgcolor="#99CCFF"><strong class="Estilo6">Numero de la Remesa:</strong></td>
      <td width="352" nowrap><input name="remesa" type="text" id="remesa" maxlength="10">
      <input type="submit" name="Submit" value="Buscar"></td>
    </tr>
  </table>
  <input name="remesalista" type="hidden" id="remesalista" value="<%=remesalista%>">
</form>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Remesa&accion=Validar&varias=show" onSubmit="return  ValidarColpapel(this);">
  <div align="center">
    <p>
      <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
      <%java.util.Date date = new java.util.Date();
      if(request.getAttribute("remesa")!=null){
	    Remesa rem = (Remesa)request.getAttribute("remesa");
	    request.getSession().setAttribute("remesa",rem);
        estandar = rem.getDescripcion();
        standar= rem.getStdJobNo();
        docuinterno = rem.getDocInterno();
        remitentes = rem.getRemitente();
        destinatarios = rem.getDestinatario();
        remesa = rem.getNumrem();
		observacion = rem.getObservacion();
		cliente = rem.getCliente();
      }
      if(request.getAttribute("planillas")!=null){
        planillas = (List) request.getAttribute("planillas");
		session.setAttribute("planillas", planillas);
	  }
	  List plaerror=new LinkedList();
	  model.remesaService.buscarPlanillaError(remesalista);
      if(model.remesaService.getPlasError()!=null){
      	plaerror = model.remesaService.getPlasError();
		session.setAttribute("plaerror", plaerror);
      }
  %>
      <input name="remesalista" type="hidden" id="remesalista" value="<%=remesalista%>">
</p>
  </div>
  <table width="655" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="#ECE0D8" class="letra">
    <tr>
      <th colspan="2" class="titulo" scope="col">LISTA DE PLANILLAS A RELACIONAR</th>
    </tr>
    <tr>
      <th colspan="2" class="Estilo6" scope="col"><table width="100%"  border="1" cellpadding="2" cellspacing="1" bordercolor="#CCCCCC" class="letra">
        <tr class="subtitulos">
          <td width="12%"><strong>PLANILLA</strong></td>
          <td width="18%"><strong>FECHA DESPACHO </strong></td>
          <td width="14%"><strong>PLACA</strong></td>
          <td width="17%"><strong>CONDUCTOR</strong></td>
          <td width="23%"><strong>RUTA</strong></td>
          </tr>
        <% Iterator it=plaerror.iterator();
	  int i=0;
	  while (it.hasNext()){
	     Planilla plani = (Planilla) it.next();
	   %>
        <tr class="fila">
          <td height="20"><%=plani.getNumpla()%></td>
          <td><%=plani.getFecdsp().substring(0,10)%></td>
          <td><%=plani.getPlaveh()%></td>
          <td><strong><%=plani.getCedcon()%> <%=plani.getNomCond()%></strong></td>
          <td><strong><%=plani.getRuta_pla()%></strong></td>
          </tr>
        <%}%>
      </table></th>
    </tr>
    <tr>
      <th colspan="2" class="titulo" scope="col">REMESA No. <%=remesa%></th>
    </tr>
    <tr class="Estilo6">
      <th colspan="2" class="subtitulos" scope="row">INFORMACION DE LA REMESA </th>
    </tr>
    <tr class="fila">
      <th width="164" scope="row"><div align="left">ESTANDARD JOB </div></th>
      <td width="475" bgcolor="<%=color%>"><strong><%=estandar%>
          <input name="standard" type="hidden" id="standard" value="<%=standar%>">
          <input name="remesa" type="hidden" id="remesa" value="<%=remesa%>">
</strong></td>
    </tr>
    <tr class="fila">
      <th scope="row"><div align="left">CLIENTE</div></th>
      <td><div align="left"><%=cliente%> </div></td>
    </tr>
    <tr class="fila">
      <th scope="row"><div align="left"><span class="Estilo7">REMITENTE</span></div></th>
      <td><div align="left"><%=remitentes%> 
        <input name="remitentes" type="hidden" id="remitentes" value="<%=remitentes%>">
      </div></td>
    </tr>
    <tr class="fila">
      <th class="Estilo7" scope="row"><div align="left">DESTINATARIO</div></th>
      <td><%=destinatarios%>
      <input name="destinatarios" type="hidden" id="destinatarios" value="<%=destinatarios%>"></td>
    </tr>
    <tr class="fila">
      <th class="Estilo7" scope="row"><div align="left">DOCUMENTO INTERNO </div></th>
      <td><%=docuinterno%></td>
    </tr>
    <tr class="fila">
      <th class="Estilo7" scope="row"><div align="left">OBSERVACION</div></th>
      <td><%=observacion%></td>
    </tr>
	 <%if(request.getAttribute("planillas")!=null){%>
    <tr style="cursor:hand" title="Ver Planillas..."  onClick="MM_openBrWindow('colpapel/listaPlanillas.jsp?norem=<%=remesa%>','','menubar=yes,scrollbars=yes,resizable=yes')" onMouseOver="bgColor= '#99cc99'" onMouseOut="bgColor=''">
      <th colspan="2" scope="row"><div align="left" class="pie" ><u>Existen planillas relacionadas con esta remesa. Haga click aqui para obtener mas informaci&oacute;n.</u></div></th>
    </tr>
	<%}else{%>
	<tr>
      <th colspan="2" scope="row"><div align="left" class="pie">No existen planillas relacionadas con esta remesa. </div></th>
    </tr>
	<%}%>
	<%List listTabla2 = model.tbltiempoService.getTblTiemposSalida(usuario.getBase());
			Iterator itTbla2=listTabla2.iterator();
			while(itTbla2.hasNext()){
				Tbltiempo tbl = (Tbltiempo) itTbla2.next();
				String id_tabla=tbl.getTimeCode();
				
				%>
	<%}%>
  </table>
  <%if(request.getAttribute("remesa")!=null){%>
  <p align="center">
    <input type="submit" name="Submit8" value="Validar">
    <input type="submit" name="Submit5" value="Regresar">
  </p>
 <%}%>
</form>
<iframe width=188 height=166 name="gToday:datetime:agenda.js:gfPop:plugins_12.js" id="gToday:datetime:agenda.js:gfPop:plugins_12.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</body>
</html>
