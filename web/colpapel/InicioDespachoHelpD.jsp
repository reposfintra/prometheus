<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Descripcion Campos Ingresar Despachos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Despacho </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2"> Informacion de Despacho </td>
        </tr>
		 <tr class="subtitulo1">
          <td colspan="2"> Datos Remesa </td>
        </tr>
        <tr>
          <td width="149" class="fila"> FECHA DESPACHO</td>
          <td width="525"  class="ayudaHtmlTexto">Campo que muestra la fecha de creacion del despacho, generado automaticamente. </td>
        </tr>
        <tr>
          <td  class="fila"><strong>Agregar Remitentes</strong></td>
          <td  class="ayudaHtmlTexto">Link que direcciona a una pantalla donde se pueden seleccionar Remitentes. </td>
        </tr>
        <tr>
          <td class="fila">Agregar Destinatarios</td>
          <td  class="ayudaHtmlTexto">Link que direcciona a una pantalla donde se pueden seleccionar Destinatarios.</td>
        </tr>
<tr>
          <td class="fila">Agregar Documentos Relacionados</td>
          <td  class="ayudaHtmlTexto">Link que direcciona a una pantalla donde se relacionan los documentos internos.</td>
        </tr>
		<tr>
          <td width="149" class="fila"> Cantidad a Facturar</td>
          <td width="525"  class="ayudaHtmlTexto">Campo que muestra la cantidad a facturar segun el standar job, esta cantidad puede ser modificada. </td>
        </tr>
        <tr>
          <td width="149" class="fila"> Unidad de Facturacion</td>
          <td width="525"  class="ayudaHtmlTexto">Campo que describe la unidad en la que se encuentran definida la carga. </td>
        </tr>
        <tr>
          <td  class="fila"><strong>Unidad de Empaque </strong></td>
          <td  class="ayudaHtmlTexto">Campo de seleccion donde se encuentra una lista de las diferentes unidades de empaque. </td>
        </tr>
        <tr>
          <td class="fila"> No Facturable </td>
          <td  class="ayudaHtmlTexto">Campo de chequeo para indicar si la remesa es o no facturable.</td>
        </tr>
		<tr>
          <td class="fila">Cross Docking </td>
          <td  class="ayudaHtmlTexto">Campo de chequeo para indicar si el viaje va o no va con Cross Docking.</td>
        </tr>
		 <tr class="subtitulo1">
          <td colspan="2"> Datos Planilla </td>
        </tr>
		<tr>
          <td width="149" class="fila"> Cantidad a Facturar</td>
          <td width="525"  class="ayudaHtmlTexto">Campo que muestra la cantidad a facturar segun el standar job, esta cantidad puede ser modificada. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> Placa</td>
          <td width="525"  class="ayudaHtmlTexto">Campo para digitar la placa conformada por tres letras y tres numeros. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> ¿Trailer de TSP?</td>
          <td width="525"  class="ayudaHtmlTexto">Campo de chequeo para escoger entre la opci&oacute;n Si y la Opci&oacute;n No. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> Trailer</td>
          <td width="525"  class="ayudaHtmlTexto">Campo para digitar el Codigo de un trailer. El codigo esta conformado por numeros y letras.. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> Contenedor</td>
          <td width="525"  class="ayudaHtmlTexto">Campos para digitar el codigo de contenedores. El codigo puede estar conformado por numeros y letras. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> TSP y Naviera</td>
          <td width="525"  class="ayudaHtmlTexto">Es un campo de chequeo para describir si el contenedor pertenece a TSP o a alguna Naviera.</td>
        </tr>
		<tr>
          <td width="149" class="fila"> Precintos de Contenedores</td>
          <td width="525"  class="ayudaHtmlTexto">Campos para digitar los numeros de los precintos del contenedor. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> Ruta planilla</td>
          <td width="525"  class="ayudaHtmlTexto">Campo de selecci&oacute;n donde se encuentran las distintas rutas relacionadas a la planilla. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> Via</td>
          <td width="525"  class="ayudaHtmlTexto">Campo de selecci&oacute;n donde se encuentran las vias relacionadas a las rutas. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> Cantidad a Despachar </td>
          <td width="525"  class="ayudaHtmlTexto">Campo que recibe el numero de unidades a despachar, por ejemplo: viajes, Toneladas, galones, etc. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> Valor del Flete</td>
          <td width="525"  class="ayudaHtmlTexto">Campo de seleccion donde se escoge el Valor del flete.. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> APLICAR OTRO VALOR DE FLETE (F8)</td>
          <td width="525"  class="ayudaHtmlTexto">link que direcciona a una pantalla con un campo para ingresar nuevo valor en moneda del flete. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> Nuevo Valor Aplicado</td>
          <td width="525"  class="ayudaHtmlTexto">campo donde se muestra si hubo un cambio o no en el valor del flete a aplicar. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> INGRESAR/ MODIFICAR EXTRAFLETES</td>
          <td width="525"  class="ayudaHtmlTexto">link que direcciona a una pantalla donde se ingresan o modifican los valores de los extrafletes. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> INGRESAR/ MODIFICAR COSTOS REEMBOLSABLES</td>
          <td width="525"  class="ayudaHtmlTexto">link que direcciona auna pantalla donde se ingresan o modifican los valores de los costos reembolsables. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> Valor total en extrafletes</td>
          <td width="525"  class="ayudaHtmlTexto">Campo que muestra el valor total de los extrafletes. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> Anticipo maximo permitido</td>
          <td width="525"  class="ayudaHtmlTexto">Campo que muestra el valor maximo permitido de los anticipos. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> Valor del Anticipo</td>
          <td width="525"  class="ayudaHtmlTexto">Campo donde se digite el valor en moneda del anticipo.. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> Banco</td>
          <td width="525"  class="ayudaHtmlTexto">Campo de Seleccion donde se encuentran los diferentes bancos relacionados. </td>
        </tr>
		 <tr class="subtitulo1">
          <td colspan="2"> Otros Precintos </td>
        </tr>
		<tr>
          <td width="149" class="fila"> Otros Precintos</td>
          <td width="525"  class="ayudaHtmlTexto">Se compone de cinco campos para registrar mas numeros de precintos en caso que hagan falta. </td>
        </tr>
		 <tr class="subtitulo1">
          <td colspan="2"> Fechas YYYY-MM-DD HH:MM 24 Hrs </td>
        </tr>
		<tr>
          <td width="149" class="fila"> FECHA SALIDA</td>
          <td width="525"  class="ayudaHtmlTexto">Campo donde se digita la fecha de salida en el formato YYYY-DD-MM HH:MM. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> FECHA ENTRADA CARGUE</td>
          <td width="525"  class="ayudaHtmlTexto">Campo donde se digita la fecha de Entrada a cargar en el formato YYYY-DD-MM HH:MM. </td>
        </tr>
		<tr>
          <td width="149" class="fila">FECHA SALIDA CARGUE</td>
          <td width="525"  class="ayudaHtmlTexto">Campo donde se digita la fecha de Salida de cargue en el formato YYYY-DD-MM HH:MM. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> FECHA ENTRADA DESCARGUE </td>
          <td width="525"  class="ayudaHtmlTexto">Campo donde se digita la fecha de entrada de descargue en el formato YYYY-DD-MM HH:MM. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> FECHA SALIDA DESCARGUE </td>
          <td width="525"  class="ayudaHtmlTexto">Campo donde se digita la fecha de salida de descargue en el formato YYYY-DD-MM HH:MM. </td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2"> Descuentos </td>
        </tr>
		<tr>
          <td width="149" class="fila"> ESTAMPILLA </td>
          <td width="525"  class="ayudaHtmlTexto">Se compone de un campo donde se digita el valor en numeros de descuentos en estampillas y un campo de chequeo que indica si se hace efectivo el descuento o no. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> PAPEL</td>
          <td width="525"  class="ayudaHtmlTexto">Se compone de un campo donde se digita el valor en numeros de descuentos en papel y un campo de chequeo que indica si se hace efectivo el descuento o no. </td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2"> Observaciones </td>
        </tr>
		<tr>
          <td width="149" class="fila"> Observaciones</td>
          <td width="525"  class="ayudaHtmlTexto">Campo de texto donde se ingresan todas las observaciones de despacho que se necesiten registrar.</td>
        </tr>
		<tr>
          <td width="149" class="fila"> Numero del Sticker</td>
          <td width="525"  class="ayudaHtmlTexto">Campo numerico para registrar el numero de Sticker.</td>
        </tr>
		<tr>
          <td width="149" class="fila"> Valor estimado de la mercancia</td>
          <td width="525"  class="ayudaHtmlTexto">Campo donde se registra el un estimado del valor de la mercancia a despachar.</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
