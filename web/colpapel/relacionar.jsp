<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Relacionar Planilla y Remesa</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/validar.js">
</script>
<script language="javascript" src="<%=BASEURL%>/js/validarDespacho.js">
</script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</script>

<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body <%if(request.getAttribute("remesa")!=null){%>onLoad="buscarpor();"<%}else {%>onLoad="form2.remesa.focus();"<%}%>>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Relacionar Planilla y Remesa"/>
</div>

 <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
  
<%//Inicializo variables
String estandard="", estandar="", fechadesp="", rutaP="", codruta="", trailer="", conductor="", nomcond="",cliente="", fecha="",standar="", observacion="",docuinterno="", remitentes="", destinatarios="", remesa1="", planilla="", cedula="", nombre="", placa="", precinto=""; 
float peso=0, valor=0, ant=0;
String color="";
if(request.getAttribute("standar")!=null){
	color = (String)request.getAttribute("standar");
}

%>
<form name="form2" method="post" action="<%=CONTROLLER%>?estado=RemesaPlanilla&accion=Search">
  <table width="34%"  border="2" align="center">
    <tr>
      <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td height="22" colspan=2 class="subtitulo1">Relacionar</td>
          <td width="165" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>        <table width="100%" align="center">
        <tr class="filaresaltada" >
          <td width="193" nowrap><strong>Numero de la Remesa:</strong></td>
          <td width="270" nowrap ><input name="remesa" type="text" id="remesa" maxlength="10" value="<%=request.getParameter("remesa")!=null?request.getParameter("remesa"):""%>">
          </td>
        </tr>
        <tr class="filaresaltada" >
          <td nowrap><strong >Numero de la Planilla:</strong></td>
          <td nowrap><input name="planilla" type="text" id="planilla" maxlength="10" value="<%=request.getParameter("planilla")!=null?request.getParameter("planilla"):""%>">
          </td>
        </tr>
      </table></td>
    </tr>
  </table><br>
  <div align="center">
<img src="<%=BASEURL%>/images/botones/buscar.gif" width="87" height="21" onClick="if(form2.remesa.value !='' &&  form2.planilla.value !=''){form2.submit();}else{alert('Escriba el numero de la Planilla y Remesa');}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
<img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">    
  </div>
</form>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=RemesaPlanilla&accion=Search&relacionar=ok">
  <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
      <%java.util.Date date = new java.util.Date();
	  double vlr = 0;
      if(request.getAttribute("remesa")!=null){
	    Remesa rem = (Remesa)request.getAttribute("remesa");
	    request.getSession().setAttribute("remesa",rem);
        estandar = rem.getDescripcion();
        standar= rem.getStdJobNo();
        docuinterno = rem.getDocInterno();
        remitentes = rem.getRemitente();
        destinatarios = rem.getDestinatario();
        remesa1 = rem.getNumrem();
		observacion = rem.getObservacion();
		cliente = rem.getCliente();
		vlr=rem.getVlr_pesos()==0?rem.getVlrRem():rem.getVlr_pesos();
      }
	   if(request.getAttribute("planilla")!=null){
        Planilla pla= (Planilla) request.getAttribute("planilla");
        placa=pla.getPlaveh();
		trailer=pla.getPlatlr();
		conductor= pla.getCedcon();
		nomcond=pla.getNomCond();
		estandard=pla.getSj_desc();
		standar= pla.getSj();
		fechadesp = pla.getFecdsp();
		peso=pla.getPesoreal();
		rutaP = pla.getRuta_pla();
		codruta = pla.getCodruta();
		precinto = pla.getPrecinto();
		planilla= pla.getNumpla();
    }
	  if(request.getAttribute("movpla")!=null){
       Movpla movpla = (Movpla) request.getAttribute("movpla");
       valor = movpla.getVlr();
      }
	 
     
  %>
      <table width="80%" border="2" align="center">
        <tr>
          <td>
            <table width="100%" border="0" cellpadding="0" cellspacing="0">
              <tr>
                <td height="22" colspan=2 class="subtitulo1">Planilla No. <%=planilla%>
                    <input name="planilla" type="hidden" id="planilla4" value="<%=planilla%>"></td>
                <td width="368" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
            </table>
            <table width="100%" class="tablaInferior">
              <tr >
                <td width="11%" class="filaresaltada" height="26" scope="row">Placa</td>
                <td colspan="2"  class="letra" scope="row"><%=placa%></td>
                <td width="14%" class="filaresaltada" scope="row">Conductor</td>
                <td colspan="2" scope="row" class="letra"><%=conductor%> <%=nomcond%> </td>
              </tr>
              <tr >
                <td class="filaresaltada" scope="row">Trailer</td>
                <td  class="letra" scope="row"><%=trailer%></td>
                <td  class="filaresaltada" scope="row">Precintos</td>
                <td  class="letra" scope="row"><%=precinto%></td>
                <td   class="filaresaltada" scope="row">Ruta Planilla</td>
                <td class="letra" scope="row"><%=rutaP%>
                      <input name="rutaP" type="hidden" id="rutaP3" value="<%=codruta%>">
                </td>
              </tr>
              <tr>
                <td height="31" colspan="2" class="filaresaltada" scope="row">Cantidad Depachada</td>
                <td height="31" class="letra" scope="row"><%=peso%></td>
                <td height="31" colspan="2" class="filaresaltada" scope="row">Valor del Anticipo</td>
                <td height="31" class="letra" scope="row"><%=ant%></td>
              </tr>
          </table>
            
            <table width="100%" border="1" align="center" class="Letras">
              <tr>
                <td colspan="4" nowrap class="subtitulo1"><div align="left"><strong>LISTA REMESAS RELACIONADAS </strong></div></td>
              </tr>
              <tr bordercolor="#999999" class="tblTitulo">
                <td align="center">REMESA</td>
                <td align="center">CLIENTE</td>
                <td  align="center">ESTANDAR</td>
                <td align="left">PORCENTAJE</td>
              </tr>
              <%  
		 int i=0;
		if(!planilla.equals("")){
		 	 List remesas= model.planillaService.buscarRemesas(planilla);
			 Iterator rem=remesas.iterator();
	  
			  while (rem.hasNext()){
	  			i++;
	  			Remesa remesa = (Remesa) rem.next();
				String numremP = remesa.getNumrem();

		%>
              <tr class="<%=i%2==0?"filagris":"filaazul"%>">
                <td  class="bordereporte">
                  <div align="center"><%=remesa.getNumrem()%> </div></td>
                <td class="bordereporte">
                  <div align="center" ><%=remesa.getCliente()%> </div></td>
                <td  class="bordereporte"><div align="center"><%=remesa.getDescripcion()%></div></td>
                <td class="bordereporte">
                  <input name="por<%=remesa.getNumrem()%>" type="text" id="por<%=remesa.getNumrem()%>" size="5" maxlength="3" value="<%=remesa.getPorcentaje()%>"  onKeyPress="soloDigitos(event,'decNo')">
                  <input name="vlr<%=remesa.getNumrem()%>" type="hidden" id="vlr<%=remesa.getNumrem()%>" value="<%=remesa.getVlr_pesos()==0?remesa.getVlrRem():remesa.getVlr_pesos()%>">
                </td>
              </tr>
              <%}
		
		}
		
		i++;
  %><%if(i>1){%>
              <tr class="<%=i%2==0?"filagris":"filaazul"%>">
                <td colspan="3"  class="bordereporte"><div align="right"><b>PORCENTAJE NUEVA REMESA</b></div></td>
                <td class="bordereporte"><input name="porcent" type="text" id="porcent" onKeyPress="soloDigitos(event,'decNo')" value="<%=request.getParameter("porcent")%>" size="5" maxlength="3"></td>
              </tr>
			  <%}%>
            </table>
		  </td>
        </tr>
      </table>
      <table width="80%"  border="2" align="center">
        <tr>
          <td>
		  <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td height="22" colspan=2 class="subtitulo1"><span class="Estilo6">Remesa No. <%=remesa1%>
                  <input name="remesa" type="hidden" id="remesa" value="<%=remesa1%>">
                  <span class="Estilo6"><span class="Estilo8">
                  <input name="vlrrem" type="HIDDEN" id="vlrrem"  value="<%=vlr%>" readonly>
                  
              </span></span> </td>
              <td width="368" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
            </tr>
          </table>
            <table width="100%" align="center" bordercolor="#CCCCCC"  >
            <tr >
              <td colspan="2" scope="row" class="filaresaltada">Estandar Job</td>
              <td colspan="2" class="letra"><%=estandar%>
                  <input name="standard" type="hidden" id="standard4" value="<%=standar%>">
			  </td>
              <td width="16%" class="filaresaltada">FECHA DESPACHO </td>
              <td width="19%" colspan="2" class="letra"><%=fechadesp%></td>
            </tr>
            <tr>
              <td colspan="2" scope="row" class="filaresaltada">Remitente</td>
              <td width="30%" class="letra"><%=remitentes%>
                  <input name="remitentes" type="hidden" id="remitentes" value="<%=remitentes%>">
              </td>
              <td width="17%" class="filaresaltada">Destinatarios</td>
              <td colspan="3" class="letra"><%=destinatarios%>
                <input name="destinatarios" type="hidden" id="destinatarios" value="<%=destinatarios%>"></td>
              </tr>
            <tr >
              <td colspan="2" scope="row" class="filaresaltada">Observacion</td>
              <td colspan="5" class="letra">
			  <%=observacion%></td>
              </tr>
          </table>        
	    </td>
		</tr>
		  
    </table>
		  <%if(request.getAttribute("remesa")!=null){%>
  <p align="center">
    <img name="imageField" type="image" src="<%=BASEURL%>/images/botones/aceptar.gif" width="90" height="21" border="0" onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);" onClick="if(validarRelacionar(form1)){form1.submit();this.disabled=true;}" style="cursor:hand">
    <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand"> </p>
    <%}%>
    <%String mensaje = request.getParameter("mensaje"); 
    if(mensaje!=null){%>
    <br>
  <table border="2" align="center">
    <tr>
      <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="229" align="center" class="mensajes"><%=mensaje%></td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="58">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <%  } %>
  <p>&nbsp;  </p>
</form>
<iframe width=188 height=166 name="gToday:datetime:agenda.js:gfPop:plugins_12.js" id="gToday:datetime:agenda.js:gfPop:plugins_12.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</div>
</body>
</html>
