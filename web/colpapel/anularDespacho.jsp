<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>

<html>
<head>
<title>Anular Despacho</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/validar.js">
</script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">

</head>

<body>
<%//Inicializo variables
String estandar="", trailer="", fecha="",standar="", observacion="",docuinterno="", remitentes="", ruta="",destinatarios="", remesa="", planilla="", cedula="", nombre="", placa="", precinto=""; 
float peso=0, valor=0;
boolean impresa=false;
%>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Anular Despacho"/>
</div>

 <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<form name="form2" method="post" action="<%=CONTROLLER%>?estado=Planilla&accion=Search&colpapel=show">
<table width="57%"  border="2" align="center">
	<tr>
	  <td>

  <table width="100%" align="center" class="tablaInferior">
    <tr>
      <td height="22" colspan=2 class="subtitulo1">Anular Despacho<span class="titulo"><strong><br>
      </strong></span></td>
      <td width="308" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
    </tr>
  </table>
  <table width="100%" border="0" align="center" class="fila">
    <tr class="filaresaltada">
      <td width="162" nowrap><strong>Numero de la Planilla:</strong></td>
      <td width="352" nowrap><input name="planilla" type="text" id="planilla" maxlength="10" onKeyPress="soloAlfa(event)">      <img src="<%=BASEURL%>/images/botones/buscar.gif" width="87" height="21" onClick="if(form2.planilla.value==''){alert('Ingrese el numero de la planilla');}else{form2.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
    </tr>
  </table>
</td>
    </tr>
</table>
</form>
 
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Planilla&accion=Delete&colpapel=show&CGA=OK">
  <div align="center">
    <p>
	
      <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
      <%int sw=0;
      if(model.remesaService.getRemesa()!=null){
	   sw++;
	    Remesa rem = model.remesaService.getRemesa();
		if(rem.getStdJobNo()!=null){
        	estandar = rem.getDescripcion();
	        standar= rem.getStdJobNo();
    	    docuinterno = rem.getDocInterno();
        	remitentes = rem.getRemitente();
	        destinatarios = rem.getDestinatario();
	        remesa = rem.getNumrem();
			observacion = rem.getObservacion();
		}
      }
	  boolean cumplida =false;
      if(model.planillaService.getPlanilla()!=null && sw >0){
	     
        Planilla pla = model.planillaService.getPlanilla();
		if(pla.getNumpla()!=null){
			sw++;	
			planilla = pla.getNumpla();
			nombre = pla.getNomCond();
			cedula = pla.getCedcon();
            placa = pla.getPlaveh();
            peso = pla.getPesoreal();
			trailer = pla.getPlatlr();
			fecha = pla.getFecdsp().substring(0,19);
			ruta = pla.getRuta_pla();
			precinto = pla.getPrecinto();
			cumplida = model.cumplidoService.estaCumplida(planilla);
		}
		
      }
	  model.trecuperacionaService.llenarTree();
	  model.causas_anulacionService.llenarTree();
      
  %>
  
  <input name="remesaRel" type="HIDDEN" id="remesaRel" value="<%=request.getParameter("remesaRel")==null?remesa:request.getParameter("remesaRel")%>">
</p>
  </div>
  <table width="90%"  border="2" align="center" bgcolor="#EFEBDE">
    <tr>
      <td>
        <table width="100%" align="center" class="tablaInferior">
          <tr>
            <td height="22" colspan=2 class="subtitulo1">Anular Despacho<span class="titulo"><strong><span class="Estilo6"> <%=planilla%> <%=cumplida?"CUMPLIDA":""%></span><br>
            </strong></span></td>
            <td width="308" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
        </table>
        <table width="100%" align="center" class="tablaInferior">
          <tr>
            <td height="22" class="subtitulo1">Informacion Remesa <span class="titulo"><strong><span class="Estilo6"></span></strong></span></td>
            </tr>
        </table>        
        <table width="100%" align="center" class="tablaInferior">
      
  <tr class="filaresaltada">
          <th width="137" scope="row"><div align="left">Estandar Job </div></th>
          <td><%=estandar%>
              <input name="planilla" type="hidden" id="planilla" value="<%=planilla%>">
              <input name="standard" type="hidden" id="standard2" value="<%=standar%>">
              <input name="remesa" type="hidden" id="remesa2" value="<%=remesa%>">
              <input name="estandard" type="hidden" id="estandard2" value="<%=estandar%>">
          </td>
          <td>Fecha del despacho</td>
          <td><%=fecha%></td>
  </tr>
        <tr class="filaresaltada">
          <th scope="row"><div align="left"><span class="Estilo7">Remitente</span></div></th>
          <td width="203"><div align="left"> <%=remitentes%> </div></td>
          <td width="195"><span class="Estilo7">Destinatarios</span></td>
          <td width="308"><%=destinatarios%></td>
        </tr>
        <tr class="filaresaltada">
          <th scope="row"><div align="left">Documentos</div></th>
          <td colspan="3"><div align="left"><%=docuinterno%> </div></td>
        </tr>
		</table>
		<table width="100%" align="center" class="tablaInferior">
          <tr>
            <td height="22" class="subtitulo1">Informacion Planilla <span class="titulo"><strong><span class="Estilo6"></span></strong></span></td>
            </tr>
        </table>
		<table width="100%" class="tablaInferior">
        <tr class="fila">
          <th width="11%" height="24" scope="row"><div align="left"><span class="Estilo7">Placa</span></div></th>
          <td width="13%">
            <div align="left"><%=placa%></div></td>
          <td width="9%">Trailer</td>
          <td width="15%"><%=trailer%></td>
          <td width="11%"> Conductor</td>
          <td width="41%"><strong><%=cedula%> <%=nombre%></strong> </td>
        </tr>
        <tr class="fila">
          <th class="Estilo7" scope="row"><div align="left">Ruta</div></th>
          <td colspan="3">
            <div align="left"><strong><%=ruta%></strong></div></td>
          <td>Cantidad</td>
          <td><%=peso%></td>
        </tr>
        <tr class="fila">
          <th class="Estilo7" scope="row"><div align="left">Precintos
              <div align="left" class="Estilo7"></div>
          </div></th>
          <td colspan="3"> <div align="left"><%=precinto%> </div></td>
          <td>Observacion</td>
          <td><%=observacion%></td>
        </tr>
		</table>
		<table width="100%" align="center" class="tablaInferior">
          <tr>
            <td height="22" class="subtitulo1"><span class="Estilo7">Anticipos Impresos </span><span class="titulo"></span></td>
            </tr>
        </table>
		<table width="100%" border="1" bordercolor="#999999">
        <tr bordercolor="#999999" class="tblTitulo">
          <th class="Estilo7" scope="row">NUMERO DEL CHEQUE</th>
          <td width="115"><div align="center"><strong>VALOR</strong></div></td>
          <td width="281"><span class="Estilo7"><strong>TIPO DE RECUPERACION DEL ANTICIPO</strong></span></td>
          <td width="151"><strong>CANTIDAD DEVOLUCION </strong></td>
        </tr>
        <%Vector vec = model.movplaService.getLista();
	if(vec!=null){
	for(int i = 0; i<vec.size(); i++){
		sw++;
		Movpla cheq = (Movpla) vec.elementAt(i);
	%>
        <tr class="<%=i%2==0?"filagris":"filaazul"%>" >
          <th height="23"  class="bordereporte" scope="row"><input name="vdel_<%=cheq.getDocument()%>" type="HIDDEN" id="vdel_<%=cheq.getDocument()%>">
              <%=cheq.getDocument()%></th>
          <td class="bordereporte"><%=com.tsp.util.Util.customFormat(cheq.getVlr_for())%>
              <input name="valor<%=cheq.getDocument()%>" type="hidden" id="valor<%=cheq.getDocument()%>" value="<%=cheq.getVlr_for()%>"></td>
          <td class="bordereporte"><%TreeMap trecuperecion= model.trecuperacionaService.getTreeTRA(); %>
              <input:select name="<%=cheq.getDocument()%>" options="<%=trecuperecion%>" attributesText="<%= "style='width:100%;'  onChange='verificarTRecuperacion(this.value,this.name, form1.cant_devol" + cheq.getDocument() + ".name)'" %>" /></td>
          <td class="bordereporte"><input name="cant_devol<%=cheq.getDocument()%>" type="text" id="cant_devol<%=cheq.getDocument()%>" value="NO APLICA" readonly></td>
        </tr>
        <%}
	}%>
	</table>
		
		<table width="100%" align="center" class="tablaInferior">
          <tr>
            <td height="22" class="subtitulo1">Otros Datos <span class="titulo"></span></td>
            </tr>
        </table>
		<table width="100%" class="tablaInferior">
        <%if(sw>1){%>
        <tr class="filaresaltada">
          <th width="23%" class="Estilo7" scope="row">CAUSA DE LA ANULACION </th>
          <td width="77%" colspan="3"><%TreeMap causas= model.causas_anulacionService.getTcausasa(); 
		  causas.put(" Seleccione", "");
		  %>
              <input:select name="causas" options="<%=causas%>" attributesText="style='width:90%;'" /> </td>
        </tr>
        
        <tr class="filaresaltada">
          <th class="Estilo7" scope="row">OBSERVACIONES </th>
          <td colspan="3"><textarea name="observacion" id="textarea3" style="width:100% "></textarea></td>
        </tr>
        <%}%>
      </table></td>
    </tr>
  </table>
 <%
	  String mensaje = request.getParameter("mensaje")!=null?request.getParameter("mensaje"):"";
	  if(model.planillaService.getPlanilla()!=null && sw >0){%>
      <p align="center">
        <img name="imageField" src="<%=BASEURL%>/images/botones/anular.gif" width="90" height="21" border="0" onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);" style="cursor:hand" onClick="if( form1.causas.value!='' ) { this.disabled=true;form1.submit(); } else { alert('Debe seleccionar la causa de la anulación.'); }">
        <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);"> 
	</p>
	  <%}
	  else if( sw ==0 && request.getParameter("entro")!=null){
	  	mensaje="La planilla  esta anulada o no existe";
	  }
	  
	  if(sw ==0 && (request.getParameter("entro")!=null || request.getParameter("mensaje")!=null)){%>
    <br>
  <table border="2" align="center">
    <tr>
      <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="229" align="center" class="mensajes"><%=mensaje%></td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="58">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <%  } %>
</form>
  <%if(cumplida){%>
          <script>
  	alert('Advertencia: La planilla esta Cumplida');
      </script>
          <%}%>
<iframe width=188 height=166 name="gToday:datetime:agenda.js:gfPop:plugins_12.js" id="gToday:datetime:agenda.js:gfPop:plugins_12.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</DIV>
<%=datos[1]%>
</body>
</html>
