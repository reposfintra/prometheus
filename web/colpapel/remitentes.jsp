<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Agregar Remitentes</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/validar.js">
</script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">



</head>

<body onLoad="iniciarR();">
<form action="" method="post" name="form2" id="form2">
 <table width="81%"  border="2" align="center" cellpadding="0" cellspacing="0">
	<tr>
	  <td>
  <table width="100%" class="tablaInferior">
    <tr>
      <td colspan=2 class="subtitulo1">Remitentes<span class="Letras">
        <input name="numrem" type="hidden" id="numrem" value="<%=request.getParameter("numrem")%>">
        <input name="modif" type="hidden" id="modif" value="<%=request.getParameter("modif")!=null?"1":""%>">
      </span></td>
      <td width="31%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
    </tr>
  </table>
  <table width="100%" cellpadding="3" cellspacing="2">
	<tr class="filaresaltada">
	    <td width="39%"><strong class="Letras">
	      <input name="sj" type="hidden" id="sj" value="<%=request.getParameter("sj")%>">
          <span class="Estilo6">
          <input name="origstd" type="hidden" id="origstd" value="<%=request.getParameter("origen")%>">
      </span>CIUDAD DE ORIGEN </strong> </td>
        <td width="61%"><span class="Letras"><strong><%
	  String sj1 = request.getParameter("sj");
	  String cliente1=sj1.substring(0,3);
	  String origen = request.getParameter("origen");
	  model.remidestService.searchCiudadesr(cliente1,origen);
	  TreeMap ciudades = model.remidestService.getCiudades(); 
	  String corigen = request.getParameter("origen");
	 // out.println( request.getParameter("origen"));
	  %>
        <input:select name="ciudad" options="<%=ciudades%>"  default='<%=corigen%>'/> 
	
   
    </strong></span></td>
    </tr>
  </table>
	<table width="100%" border="1">
	<tr class="tblTitulo">
	  <td width="9%" bordercolor="#999999" ><div align="right">Seleccionar</div></td>
      <td width="91%" bordercolor="#999999" >Remitentes</td>
	</tr>
	 <%
	List lista=model.remidestService.getRemitentes(cliente1,corigen);
  	Iterator it=lista.iterator();
	    int i=0;
  			while (it.hasNext()){
            i++;
            RemiDest rd = (RemiDest) it.next();
            String coddest = rd.getCodigo();
            String nomdest = rd.getNombre();
    %>
	<tr class="<%=i%2==0?"filagris":"filaazul"%>">
	  <td class="bordereporte"><div align="right">
	    <input name="check" type="radio" id="check" value="<%=coddest%>" onClick="onCheckR(this.value,'<%=BASEURL%>');">
      </div></td>
      <td class="bordereporte"><span class="Letras"><strong><%=nomdest%></strong></span></td>
	</tr>
	 <%}
	 %>
  </table>
  </td>
  </tr></table>
 <div align="center"><img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">
 </div>
</form>
</body>
</html>
