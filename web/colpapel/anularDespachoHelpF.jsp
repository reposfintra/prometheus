<!--  
     - Author(s)       :      Karen Reales
     - Date            :      06/05/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   
     - @(#)  
     - Description: Ayuda
--%> 
<%@include file="/WEB-INF/InitModel.jsp"%>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>

<html>
    <head>
        <title>Anular Cheque</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <BR>
        <table width="80%"  border="2" align="center">
            <tr>
            <td width="811" valign="top" >
                <table width="100%" border="0" align="center">
                    <tr  class="subtitulo">
                        <td height="24" align="center">
                        ANULAR DESPACHO</td>
                    </tr>
                    <tr class="subtitulo1">
                        <td> 
                        Anular Despacho </td>
                    </tr>
                    <tr class="ayudaHtmlTexto">
                        <td>                            Para anular un despacho completo (Planilla, Remesa y Cheques) siga los siguientes pasos.<br>
                          <div align="center"><img src="../images/ayuda/anulacionplanilla/imagen1.JPG" width="488" height="168" align="absmiddle">                          <br>
                            </div>
                          <div align="center">                              </div>
                            Aparecen los datos de la planilla a anular para que usted este seguro que es el registro correcto. <br>	
		    
                            <div align="center">
                                <p><img src="../images/ayuda/anulacionplanilla/imagen2.JPG" width="600" height="482"> </p>
                          </div>
                            <p align="left">&nbsp;                      </p>                      </td>
                    </tr>
                </table>
            </td>
            </tr>
    </table>
        <p align="center" class="fuenteAyuda"> Fintravalores S. A. </p>
	<%=datos[1]%>
    </body>

</html>