<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
  String numrem = (request.getParameter("numrem")!=null)?request.getParameter("numrem"):"";
  model.remesaService.buscaRemesa(numrem);
  Remesa rem = model.remesaService.getRemesa();
  String fecha="0000-00-00";
  int mes =0;
  if (rem!=null){
      fecha = rem.getFecRem().toString();
      mes = Integer.parseInt(fecha.substring(5,7));
   }
%>
<html>
<head>
<title>Informacion de Remesa</title>
<script type='text/javascript' src="<%=BASEURL%>/js/date-picker.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Estilo1 {color: #ECE0D8}
-->
</style>
</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Informacion de Remesa"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%if (rem!=null) {%>
<form id="form" name="caravana" method="post" action="<%=CONTROLLER%>?estado=Caravana&accion=Insert">
<table width="780" border="2" align="center">
    <tr>
      <td>
	  <table width="99%" align="center">
              <tr>
                <td width="417" height="22" class="subtitulo1">INFORMACION DE LA REMESA No <%=rem.getNumrem()%></td>
                <td width="411" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
  </table>
<table width="99%" align="center">
	
  <tr class="fila">
    <td colspan="5"><div align="left" class="letra_resaltada">Remesa creada el <%=fecha.substring(8,10)%> de <%=Util.NombreMes(mes)%> de <%=fecha.substring(0,4)%> </div>      <div align="center"></div></td>
    </tr>
	
  <tr class="fila">
    <td width="333" height="15" ><div align="left"><span class="letra_resaltada">CODIGO CLIENTE:</span> <span class="letra"><%=rem.getCodcli()%></span></div></td>
    <td width="328" valign="middle" ><span class="letra_resaltada">CODIGO STANDARD JOB:</span> <span class="letra"><%=rem.getStd_job_no()%></span></td>
    </tr>
	<tr class="fila">
    <td width="333" height="15" ><div align="left" ><span class="letra_resaltada">CLIENTE: </span><span class="letra"><%=rem.getCliente()%></span></div></td>
    <td width="328" valign="middle" ><span class="letra_resaltada">DESCRIPCION:</span> <span class="letra"><%=rem.getDescripcion()%></span></td>
    </tr>
</table>
</td>
</tr>
</table>
<table width="780" border="2" align="center">
    <tr>
      <td>
<table width="99%" align="center">
              <tr>
                <td width="417" height="22" class="subtitulo1">Detalles de la Carga <%=rem.getNumrem()%></td>
                <td width="411" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
  </table>
<table width="99%" align="center">
  <tr class="fila">
    <td width="94"><div align="left" class="letra_resaltada">REMITENTE</div></td>
    <td width="258"><span class="letra_resaltada"></span><span class="letra"><%=rem.getNrem()%></span></td>
    <td width="124"><div align="center"></div>      
      <span class="letras"><span class="letra_resaltada">CANTIDAD A FACTURAR</span></span></td>
    <td width="109" colspan="2"><div align="left"><span class="letras"><span class="letra_resaltada">CANTIDAD DE EMPAQUE 
          </span></span></div></td>
    <td width="113" class="letra_resaltada">UNIDAD DE EMPAQUE </td>
  </tr>
	<tr class="fila">
    <td rowspan="3" class="letra_resaltada">DESTINATARIO</td>
    <td rowspan="3"><span class="letra_resaltada"></span><span class="letra"><%=rem.getNdest()%></span></td>
    <td height="15"><span class="letra_resaltada"><%=rem.getPesoReal()%></span></td>
    <td colspan="2"><span class="letra_resaltada"><%=rem.getCantreal()%></span></td>
    <td><span class="letra_resaltada"></span><%=rem.getUnidad()%> </span><span class="Estilo1">f</span></td>
	</tr>
	<%double val = rem.getVlrRem();
	  double val2 = rem.getTarifa();
	%>
    <tr class="fila">
      <td height="15" colspan="2"><span class="letra_resaltada">VALOR REMESA:</span><span class="letra"> <%=Util.customFormat(val)%></span></td>
      <td height="15" colspan="2"><span class="letra_resaltada">VALOR TARIFA:</span><span class="letra"> <%=val2%></span></td>
    </tr>
    <tr class="fila">
    <td height="15" colspan="2" class="letra_resaltada">MONEDA: <span class="letra"><%=rem.getMoneda()%></span></td>
    <td height="15" colspan="2">&nbsp;</td>
    </tr>
	<tr class="fila">
    <td height="15" colspan="6" ><span class="letra_resaltada">DOCUMENTOS INTERNOS:</span><span  class="letra"> <%=rem.getDocInterno()%></span></td>
    </tr>
	<tr class="fila">
    <td height="15" colspan="6" ><span class="letra_resaltada">FACTURA COMERCIAL :</span> <span class="letra"> <%=rem.getFaccial()%></span></td>
    </tr>
</table>
</td>
<tr>
</table>
</form>
<%}%>
</div>
</body>
</html>
