<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="js/validar.js">
</script>
<link href="../css/estilo.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilo.css" rel="stylesheet" type="text/css">

<script language="JavaScript" type="text/JavaScript">
<!--
function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}
//-->
</script>
</head>

<body>
<%//Inicializo variables
String estandar="", trailer="", cliente="", fecha="",standar="", observacion="",docuinterno="", remitentes="", ruta="",destinatarios="", remesa="", planilla="", cedula="", nombre="", placa="", precinto=""; 
float peso=0, valor=0;
%>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Remesa&accion=Search&placol=0k">
  <div align="center">
    <p>
     <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
      <%java.util.Date date = new java.util.Date();
      if(request.getAttribute("remesa")!=null){
	    Remesa rem = (Remesa)request.getAttribute("remesa");
        estandar = rem.getDescripcion();
        standar= rem.getStdJobNo();
        docuinterno = rem.getDocInterno();
        remitentes = rem.getRemitente();
        destinatarios = rem.getDestinatario();
        remesa = rem.getNumrem();
		observacion = rem.getObservacion();
		cliente = rem.getCliente();
      }
      
	  List planillas = model.remesaService.buscarPlanillas(remesa);
	  
  %>
  <%if(request.getAttribute("remesas")!=null){
	  	List remesas= model.planillaService.buscarRemesas(planilla);
		}%>
      <br>
      <br>
      <span class="letra_resaltada">LISTO!. RELACIONADA LA PLANILLA A LA NUEVA REMESA</span> </p>
    <table width="655" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="#ECE0D8" class="letra">
      <tr bgcolor="#FFA928">
        <th colspan="2" class="titulo" scope="col">DATOS DE LA REMESA No. <%=remesa%> <input name="remesa" type="hidden" id="remesa" value="<%=remesa%>"></th>
      </tr>
     <%if(planillas.size()>0){%>
      
      
      <tr>
        <th colspan="2" scope="row">
		<table width="100%"  border="1" cellpadding="2" cellspacing="1" bordercolor="#CCCCCC" class="letra">
          <tr bgcolor="#99ccff" class="subtitulos">
            <td width="12%"><strong>PLANILLA</strong></td>
            <td width="18%"><strong>FECHA DESPACHO </strong></td>
            <td width="14%"><strong>PLACA</strong></td>
            <td width="17%"><strong>CONDUCTOR</strong></td>
            <td width="16%"><em class="Estilo18"><strong>Ver remesas relacionadas.</strong></em></td>
          </tr>
          <% Iterator it=planillas.iterator();
	  int i=0;
	  while (it.hasNext()){
	     Planilla plani = (Planilla) it.next();
	     List remesas= model.planillaService.buscarRemesas(plani.getNumpla());%>
          <tr class="fila">
            <td height="20"><%=plani.getNumpla()%></td>
            <td><%=plani.getFecdsp().substring(0,10)%></td>
            <td><%=plani.getPlaveh()%></td>
            <td><strong><%=plani.getCedcon()%>  <%=plani.getNomCond()%></strong></td>
            <td style="cursor:hand" title="Ver Remesas..."  onClick="MM_openBrWindow('colpapel/listaRemesas.jsp?nopla=<%=plani.getNumpla()%>','','menubar=yes,scrollbars=yes,resizable=yes')" onMouseOver="bgColor= '#99cc99'" onMouseOut="bgColor=''"><em class="Estilo18"><%=remesas.size()%> relacionadas</em></td>
          </tr>
          <%}%>
        </table></th>
      </tr>
      <%}%>
      <tr bgcolor="#99CCFF" class="Estilo6">
        <th colspan="2" class="subtitulos" scope="row">&nbsp;INFORMACION DE LA  REMESA</th>
      </tr>
      <tr class="fila">
        <th width="171" height="11" scope="row"><div align="left">ESTANDARD JOB</div></th>
        <td width="468">
          <div align="left"><%=estandar%>            <input name="standard" type="hidden" id="standard" value="<%=standar%>">
            <input name="estandard" type="hidden" id="estandard" value="<%=estandar%>">
</div></td>
      </tr>
      <tr class="fila">
        <th class="Estilo7" scope="row"><div align="left">CLIENTE</div></th>
        <td>
          <div align="left"><%=cliente%> </div></td>
      </tr>
      <tr class="fila">
        <th class="Estilo7" scope="row"><div align="left">REMITENTE</div></th>
        <td><%=remitentes%></td>
      </tr>
      <tr class="fila">
        <th class="Estilo7" scope="row"><div align="left">DESTINATARIO</div></th>
        <td>
          <div align="left"><%=destinatarios%> </div></td>
      </tr>
      <tr class="fila">
        <th class="Estilo7" scope="row"><div align="left">DOCUMENTO INTERNO </div></th>
        <td><%=docuinterno%> </td>
      </tr>
      <tr class="fila">
        <th class="Estilo7" scope="row"><div align="left">OBSERVACION</div></th>
        <td><%=observacion%> </td>
      </tr>
    </table>
    <br>
    <input type="submit" name="Submit" value="Ir a anular remesa">
  </div>
</form>
<iframe width=188 height=166 name="gToday:datetime:agenda.js:gfPop:plugins_12.js" id="gToday:datetime:agenda.js:gfPop:plugins_12.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</body>
</html>
