<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Descripcion de campos ingresar despacho</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Despacho </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2"> Registro de viajes pantalla preliminar </td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2"> INFORMACION DEL CLIENTE</td>
        </tr>
        <tr>
          <td width="149" class="fila"> STANDARD/CLIENTE </td>
          <td width="525"  class="ayudaHtmlTexto">Campo para ingresar un c&oacute;digo de seis numeros que representa un cliente o una combinacion de de seis letras y numeros que reprensentan un standard job.</td>
        </tr>
        <tr>
          <td  class="fila"><strong>ORIGEN</strong></td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n donde se encuentra una lista de ciudades. </td>
        </tr>
        <tr>
          <td class="fila"><strong>DESTINO</strong></td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n donde se encuentra una lista de ciudades que representan el destino a donde llegar&aacute; la carga. </td>
        </tr>
		<tr>
          <td class="fila">Consultar clientes...</td>
          <td  class="ayudaHtmlTexto">Link que muestra una pantalla para la busqueda de clientes. </td>
        </tr>
		<tr>
          <td width="149" class="fila"> Boton Buscar </td>
          <td width="525"  class="ayudaHtmlTexto">Boton que valida la busqueda de el codigo digitado en el campo <span class="fila">STANDARD/CLIENTE </span></td>
        </tr>
        <tr>
          <td width="149" class="fila"> STANDARD JOB </td>
          <td width="525"  class="ayudaHtmlTexto">Campo de Selecci&oacute;n donde se encuentra la lista de standard job segun el el origen y el destino seleccionado. </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
