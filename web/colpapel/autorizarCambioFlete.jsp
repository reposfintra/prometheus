<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Autorizacion Cambio de Flete</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="js/validar.js">
</script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</script>

<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">


</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Despacho"/>
</div>

 <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 


<%
	AutorizacionFlete a = model.afleteService.getAflete();
	if(a!=null){
%>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Mostrar&accion=Solicitud&gclave=ok">
<table width="90%" align="center" class="tablaInferior">
  <tr>
    <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td height="22" colspan=2 class="subtitulo1">Autorizar Cambio de Flete
          <input name="planilla" type="hidden" id="planilla" value="<%=request.getParameter("planilla")%>"></td>
        <td width="368" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
      </tr>
    </table>
    <table width="100%" border="0" align="center" class="fila">
      <tr class="subtitulo1">
        <td colspan="6" class="subtitulo1">Datos del Despacho.</td>
        </tr>
      <tr>
        <td width="114"><strong>Despachado por : </strong></td>
        <td colspan="3"><%=a.getDespachador()%></td>
        <td width="130"><strong>Agencia:</strong></td>
        <td width="228"><%=a.getAgency_id()%></td>
      </tr>
      <tr>
        <td valign="top"><strong>Planilla numero: </strong></td>
        <td valign="top">NO TIENE PLANILLA </td>
        <td valign="top"><strong>Ruta y Cliente:</strong></td>
        <td><%=a.getStandard()%><br>          <br></td>
        <td valign="top"><strong>Fecha de la solicitud</strong></td>
        <td valign="top"><%=a.getFecha_req()%></td>
      </tr>
      <tr>
        <td valign="top"><strong>Placa:</strong></td>
        <td width="140" valign="top"><%=a.getPlaca()%></td>
        <td width="92" valign="top"><strong>Propietario:</strong></td>
        <td width="172" valign="middle"><%=a.getPla_owner()%><br>          <br></td>
        <td valign="top"><strong>Conductor:</strong></td>
        <td valign="top"><%=a.getCedcond()%></td>
      </tr>
      <tr>
        <td valign="top"><strong>Valor original flete:</strong></td>
        <td valign="top"><%=com.tsp.util.Util.customFormat(a.getViejoflete())%></td>
        <td valign="top"><strong>Ajuste del flete:</strong></td>
        <td><%=com.tsp.util.Util.customFormat(a.getNuevoflete())%><br>          <br></td>
        <td valign="top"><strong>Unidad:</strong></td>
        <td valign="top"><%=a.getUnidad()%></td>
      </tr>
      <tr>
        <td valign="top"><strong>Justificacion:</strong></td>
        <td colspan="5"><%=a.getJustificacion()%></td>
        </tr>
    </table></td>
    </tr>
	<%if(request.getParameter("clave")!=null){%>
  <tr>
    <td class="mensajes">RECUERDE INFORMAR AL DESPACHADOR SOBRE ESTA CLAVE DE AUTORIZACION: <span class="Estilo1"><%=request.getParameter("clave")%></span></td>
  </tr>
  <%}%>
  <tr>
    <td><div align="center">
      <input type="image" src="../images/botones/aceptar.gif" name="Submit" value="AUTORIZAR CAMBIO" onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);" style="cursor:hand ">
    </div></td>
    </tr>
</table>

</form>
<%}%>
</DIV>
</body>
</html>
