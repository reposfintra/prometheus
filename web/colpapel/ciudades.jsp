<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Ciudades Intermedias</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/validar.js">
</script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">



</head>

<body onLoad="iniciarCheckCiudades()">
<form action="<%=CONTROLLER%>?estado=CargarCiudades&accion=Intermedias&cmd=via" method="post" name="form2" id="form2">
 <table width="54%"  border="2" align="center" cellpadding="0" cellspacing="0">
	<tr>
	  <td>
  <table width="100%" class="tablaInferior">
    <tr>
      <td colspan=2 class="subtitulo1">Ciudades intermedias 
        <input name="eintermed" type="hidden" id="eintermed">
        <input name="ruta" type="hidden" id="ruta" value="<%=request.getParameter("ruta")%>">
        <input name="eintermed1" type="hidden" id="eintermed1" value="<%=request.getParameter("eintermed")%>"></td>
      <td width="57%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
    </tr>
  </table>
  <table width="100%" border="1">
	<tr class="tblTitulo">
	  <td width="9%" bordercolor="#999999" ><div align="right">Seleccionar</div></td>
      <td width="91%" bordercolor="#999999" >Ciudad</td>
	</tr>
	 <%
	Vector lista=model.ciudadService.obtCiudades()!=null?model.ciudadService.obtCiudades():new Vector();
	for(int i  =0; i< lista.size(); i++){
            
            Ciudad rd = (Ciudad) lista.elementAt(i);
            String coddest = rd.getCodCiu();
            String nomdest = rd.getNomCiu();
    %>
	<tr class="<%=i%2==0?"filagris":"filaazul"%>">
	  <td class="bordereporte">
          <div align="center">
            <input type="checkbox" name="<%=rd.getCodCiu()%>" id="<%=rd.getCodCiu()%>" value="<%=rd.getCodCiu()%>" onClick="onCheckCiudad(this.name)">
        </div></td><td class="bordereporte"><span class="Letras"><strong><%=nomdest%></strong></span></td>
	</tr>
	 <%}
	 %>
  </table>
  </td>
  </tr></table>
 <div align="center"><br>
 <%TreeMap vias = model.ciudadService.getVias()!=null?model.ciudadService.getVias():new TreeMap();
	 if(vias.size()>0){%> 
     
   <table width="45%"  border="2" align="center" cellpadding="0" cellspacing="0">
     <tr>
       <td colspan="2">
         <table width="100%" class="tablaInferior">
           <tr>
             <td colspan=2 class="subtitulo1">Vias recomendadas </td>
             <td width="57%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
           </tr>
         </table>
        </td>
     </tr>
	 <tr  class="fila">
       <td width="20%">Vias</td>
       <td width="80%">  <input:select name="via" options="<%=vias%>" attributesText="style='width:100%;'" default='<%=request.getParameter("via")%>' /></td>
     </tr>
   </table>
   <%}%>
   <br>
   <br>
   <img name="imageField"  src="<%=BASEURL%>/images/botones/aceptar.gif" width="90" height="21" border="0" onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);" onClick="buscarVia();" >
  <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand"> </div>
</form>
</body>
</html>
