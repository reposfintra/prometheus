<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>Agregar Destinatarios</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/validar.js">
</script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">

</head>

<body onLoad="iniciarCheck();">
<form action="<%=CONTROLLER%>?estado=Buscar&accion=Destinatarios" method="post" name="form2" id="form2">
  <table width="80%"  border="2" align="center">
    <tr>
      <td>
        <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="barratitulo">
          <tr>
            <td width="50%" class="subtitulo1" colspan='3'>Buscar Destinatarios </td>
            <td width="50%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"> </td>
          </tr>
        </table>
        <TABLE width='100%' border='0' class='tablaInferior'>
          <TR class="letra">
            <TD width="16%" height="35" class="letraresaltada">Destino</TD>
            <TD><%
	 
	  TreeMap ciudades = model.remidestService.getCiudades(); 
	  String corigen="";
	  if(request.getParameter("ciudad")!=null){
	  	corigen = request.getParameter("ciudad");
	  }
	  
	  %>
                <input name="controller" type="hidden" id="controller3" value="<%=CONTROLLER%>">
                <input name="sj" type="hidden" id="sj" value="<%=request.getParameter("sj")%>">
	        <input:select name="ciudad" options="<%=ciudades%>" default='<%=corigen%>'/>	                </TD>
          </TR>
          <TR class="letra">
            <TD height="35" class="letraresaltada">Nombre</TD>
            <TD><input name="nombre" type="text" id="nombre">
            <img name="imageField" type="image" style="cursor:hand " src="<%=BASEURL%>/images/botones/iconos/lupa.gif" align="middle" width="16" height="16" border="0" onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);" onClick="buscarDestinatarios();"></TD>
          </TR>
      </TABLE></td>
    </tr>
  </table>
  <div align="center">  <br>
  </div>
  <table width="99%"  border="2" align="center">
	<tr>
	  <td>
  <table width="100%" class="tablaInferior">
    <tr>
      <td height="22" colspan=2 class="subtitulo1">Lista de Destinatarios<span class="Letras">
        <input name="numrem" type="hidden" id="numrem" value="<%=request.getParameter("numrem")%>">
        <input name="modif" type="hidden" id="modif" value="<%=request.getParameter("modif")!=null?"1":""%>">
      </span></td>
      <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
    </tr>
  </table>
  <table width="100%" border="1" bordercolor="#999999">
	<tr class="tblTitulo">
	<td colspan="3"  bordercolor="#999999">Destinatario</td>
	<td  bordercolor="#999999">Documentos</td>
	</tr>
	
	
 <%
if(model.remidestService.getVectorRemiDest()!=null){%>
	<%String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 5;
    int maxIndexPages = 10;
	String numrem =null;
	String tipo ="002";
	String estado="";
	if(model.remesaService.getRemesa()!=null){
		numrem = model.remesaService.getRemesa().getNumrem();
		tipo = model.remesaService.getRemesa().getTipo_documento();
		estado= model.remesaService.getRemesa().getEstado()==null?"": model.remesaService.getRemesa().getEstado();
		
	}
	%>

	<pg:pager
    items="<%= model.remidestService.getVectorRemiDest().size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
	<%	for (int i = offset.intValue(),
	         l = Math.min(i + maxPageItems, model.remidestService.getVectorRemiDest().size());
	     i < l; i++){
		 //for(int i=0; i<model.remidestService.getVectorRemiDest().size();i++){
  		
            RemiDest rd = (RemiDest) model.remidestService.getVectorRemiDest().elementAt(i);
            String coddest = rd.getCodigo();
            String nomdest = rd.getNombre();
			String direccion  = rd.getDireccion();
    	%> 
	<pg:item>
	<tr class="<%=i%2==0?"filagris":"filaazul"%>">
	    <td width="7%" class="bordereporte"> <input name="check<%=coddest%>" type="checkbox" id="check<%=coddest%>" value="<%=coddest%>" onClick="onCheckD(this.name,'<%=BASEURL%>');"></td>
      <td class="bordereporte"><strong><%=nomdest%></strong><br>
     <%=coddest%></td>
      <td width="26%" class="bordereporte"><%=direccion%></td>
      <td width="39%" class="bordereporte"><input name="docui<%=coddest%>" type="hidden" id="docuint<%=coddest%>" size="50">
	  	<%if(numrem==null){%>
			<a class="Simulacion_Hiper" onClick="window.open('<%=CONTROLLER%>?estado=RemesaDocto&accion=Aplicar&destinatario=<%=coddest%>&nombre=<%=nomdest%>&generar=ok','')" style="cursor:hand ">AGREGAR DOCUMENTOS RELACIONADOS</a> <br>
		<%}else{
			if(tipo.equals("002") || estado.equals("M")){%>
      	<a class="Simulacion_Hiper" onClick="window.open('<%=CONTROLLER%>?estado=Buscar&accion=Destinatarios&CMDdocumentos=ok&destinatario=<%=coddest%>&nombre=<%=nomdest%>&numrem=<%=numrem%>','')" style="cursor:hand ">MODIFICAR  DOCUMENTOS RELACIONADOS</a> 
			<%}else{
				%>
			<a class="Simulacion_Hiper" onClick="window.open('<%=CONTROLLER%>?estado=RemesaDocto&accion=Aplicar&destinatario=<%=coddest%>&nombre=<%=nomdest%>&generar=ok&ocargue=ok&numrem=<%=numrem%>','')" style="cursor:hand ">AGREGAR DOCUMENTOS RELACIONADOS</a><br>
			<%}
			}%>
	  </td>
	</tr>
	</pg:item>
	 <%
	 }%>


	<tr class="filaresaltada">
	  <td colspan="4"><div align="center">
	   <pg:index>
	      <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
    	</pg:index>
	  </div>
	  
	  </td>
    </tr>
	</pg:pager>	 
<%}%>

	
  </table>
  </td>
  </tr></table>
  
  <div align="center"><img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">
  </div>
</form>
</body>
</html>
