<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Relacionar una nueva remesa a una planilla</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="js/validar.js">
</script>
</head>
<link href="../css/letras.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/letras.css" rel="stylesheet" type="text/css">

<link href="../css/Style.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/Style.css" rel="stylesheet" type="text/css">

<link href="../css/StyleM.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/StyleM.css" rel="stylesheet" type="text/css">

<body>
<%//Inicializo variables
String estandar="", trailer="", cliente="", fecha="",standar="", observacion="",docuinterno="", remitentes="", destinatarios="", remesa="", planilla="", cedula="", nombre="", placa="", precinto=""; 
float peso=0, valor=0;
List planillas=new LinkedList();
String remesalista=request.getParameter("remesalista"); 
String color="";
if(request.getAttribute("standar")!=null){
	color = (String)request.getAttribute("standar");
}

%>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Remesa&accion=Insert" onSubmit="return  ValidarColpapelR(this);">
  <div align="center">
    <p>
      <%
	  
	  Usuario usuario = (Usuario) session.getAttribute("Usuario");
	  java.util.Date date = new java.util.Date();
	 
      
    
	  List plaerror=new LinkedList();
	  model.remesaService.buscarPlanillaError(remesalista);
      if(model.remesaService.getPlasError()!=null){
      	plaerror = model.remesaService.getPlasError();
		session.setAttribute("plaerror", plaerror);
      }
	 
     
  %>
      <input name="remesalista" type="hidden" id="remesalista" value="<%=remesalista%>">
</p>
  </div>
  <table width="89%" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="#ECE0D8" class="Letras">
    <tr bgcolor="#FFA928" class="Estilo6">
      <th colspan="4" class="Estilo6" scope="row">LISTA DE PLANILLAS A RELACIONAR</th>
    </tr>
    <tr class="Estilo6">
      <td colspan="4" scope="row"><div align="left">
        <table width="100%"  border="1" cellpadding="2" cellspacing="1" bordercolor="#CCCCCC" class="Letras">
          <tr bgcolor="#99CCFF">
            <td width="12%"><strong>PLANILLA</strong></td>
            <td width="18%"><strong>FECHA DESPACHO </strong></td>
            <td width="14%"><strong>PLACA</strong></td>
            <td width="17%"><strong>CONDUCTOR</strong></td>
            </tr>
          <% Iterator it=plaerror.iterator();
	  while (it.hasNext()){
	     Planilla plani = (Planilla) it.next();
	   %>
          <tr>
            <td height="20"><%=plani.getNumpla()%></td>
            <td><%=plani.getFecdsp().substring(0,10)%></td>
            <td><%=plani.getPlaveh()%></td>
            <td><strong><%=plani.getCedcon()%><br> 
              <%=plani.getNomCond()%></strong></td>
            </tr>
          <%}%>
        </table> 
        </div>        </td>
    </tr>
    <tr bgcolor="#FFA928" class="Estilo6">
      <th colspan="4" scope="row">&nbsp;INFORMACION DE LA NUEVA REMESA</th>
    </tr>
   <tr class="Estilo6">
      <th width="23%" height="11" scope="row"><div align="left">ESTANDARD JOB</div></th>
      <td colspan="3" bgcolor="<%=request.getAttribute("standar")%>">
        <div align="left">
          <% 
	 	String origen ="";
		String pa ="";
	  	String uw="";
	  	String vlr ="";
	  	Stdjobdetsel sj=new Stdjobdetsel();
	  	String standard = request.getParameter("standard");
	  	model.stdjobdetselService.searchStdJob(request.getParameter("standard"));
        if(model.stdjobdetselService.getStandardDetSel()!=null){
			sj = model.stdjobdetselService.getStandardDetSel();
			uw=sj.getUnit_of_work();
            pa=sj.getPorcentaje_ant();
			vlr = ""+sj.getVlr_freight();   
            origen = sj.getOrigin_code();     
                         
            }%>
          <%=sj.getSj()+"-"+sj.getSj_desc()%>
          <input name="standard" type="hidden" id="standard" value="<%=sj.getSj()%>">
</div></td>
    </tr>
    <tr class="Estilo6">
      <th class="Estilo7" scope="row"><div align="left">REMITENTE</div></th>
      <td colspan="3">        <div align="left">
          <input name="remitentes" type="hidden" id="remitentes" value="<%=request.getParameter("remitentes")%>">
          <a class="Simulacion_Hiper" style="cursor:hand " onClick="abrirPagina('remitentes.jsp?sj='+form1.standard.value+'&origen='+form1.origstd.value,'');">Agregar Remitentes</a>
          <input name="remitentesR" type="hidden" id="remitentesR">
          <input name="origstd" type="hidden" id="origstd" value="<%=origen%>">
</div></td>
    </tr>
    <tr class="Estilo6">
      <th class="Estilo7" scope="row"><div align="left">DESTINATARIO</div></th>
      <td colspan="3">         <div align="left">
        <input name="destinatarios" type="hidden" id="destinatarios" value="<%=request.getParameter("destinatarios")%>" readonly>
        <a class="Simulacion_Hiper" style="cursor:hand " onClick="abrirPagina('destinatarios.jsp?sj='+form1.standard.value,'');">Agregar Destinatarios</a>
        <input name="destinatarioR" type="hidden" id="destinatarioR">
        <input name="docudest" type="hidden" id="docudest" value="<%=request.getParameter("docudest")%>">
</div></td>
    </tr>
    <tr class="Estilo6">
      <th class="Estilo7" scope="row"><div align="left">DOCUMENTO INTERNO </div></th>
      <td colspan="3"><input name="docinterno" type="text" id="docinterno" size="38" value="<%=request.getParameter("docinterno")%>"> </td>
    </tr>
    <tr class="Estilo6">
      <th class="Estilo7" scope="row"><div align="left">FACTURA COMERCIAL</div></th>
      <td colspan="3"><input name="fcial" type="text" id="fcial" value="<%=request.getParameter("fcial")%>" size="70"></td>
    </tr>
    <tr class="Estilo6">
      <th class="Estilo7" scope="row"><div align="left"><strong>UNIDAD DE FACTURACION</strong></div></th>
      <td width="24%"><strong><%=uw%>
          <input name="uw" type="hidden" id="uw" value="<%=uw%>">
      </strong></td>
      <td width="23%"><strong>CANTIDAD A FACTURAR</strong></td>
      <td width="30%"><span class="Estilo1">
        <input name="cfacturar" type="text" id="cfacturar"  onKeyPress="soloDigitos(event,'decNo')" value="<%=request.getParameter("cfacturar")%>" >
      </span>
        <input name="vrem" type="hidden" id="vrem" readonly value="<%=vlr%>">
        <input name="vremesa" type="hidden" id="vremesa2" readonly>
        <input name="vrem2" type="hidden" id="vrem22"></td>
    </tr>
    <tr class="Estilo6">
      <th class="Estilo7" scope="row"><div align="left">CANTIDAD DE EMPAQUE</div></th>
      <td><span class="Estilo1">
        <input name="cantreal" type="text" id="cantreal" value="<%=request.getParameter("cantreal")%>" size="12">
      </span></td>
      <td><strong>UNIDAD</strong></td>
      <td><%TreeMap unidades = model.unidadService.getUnidades(); 
	 
	  %>
	  <input:select name="unidad" options="<%=unidades%>" attributesText="style='width:100%;'" default= '<%=request.getParameter("unidad")%>' /></td>
    </tr>
    <tr class="Estilo6">
      <th class="Estilo7" scope="row"><div align="left">OBSERVACION</div></th>
      <td colspan="3"><textarea name="observacion" cols="60" id="observacion"><%=request.getParameter("observacion")%></textarea> </td>
    </tr>
  </table>
  <p align="center">
<input type="submit" name="Submit8" value="Agregar">    
<input type="button" name="Submit5" value="Regresar">
  </p>
</form>
</body>
</html>
