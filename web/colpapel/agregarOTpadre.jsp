<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
    <title>Agregar Remesa Padre</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
    <script type='text/javascript' src="<%=BASEURL%>/jsp/sot/js/PantallaFiltroUbicacionEquipoRuta.js"></script> 
    </script>


</head>

<body onload="tipOperacion('load',0)" onunload="<% if( request.getParameter("numrem") == null ){session.removeAttribute("hijas");}%>">
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado= Agregar Remesa Padre"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<%
    String estandar="", trailer="", cliente="", fecha="",standar="", observacion="",docuinterno="", remitentes="", destinatarios="", remesa="", planilla="", cedula="", nombre="", placa="", precinto="",rem_padre="",descripcion =""; 
    float peso=0, valor=0;
    Vector hijas = new Vector();
    List planillas=new LinkedList();

    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    
    
    String selectedOperacion = ( (request.getParameter("operacion") != null && request.getParameter("operacion").equals("2")) && (request.getParameter("numpadre").equals("")) )? "checked" : "";
%>
        <form id="form1" name="form1" method="post" action="<%=CONTROLLER%>?estado=RemesaPadre&accion=Insert" onsubmit="return  validar();">

        <div align="center">
         <%Usuario usuario = (Usuario) session.getAttribute("Usuario");
           
      if( request.getAttribute("remesa_padre") != null ){
          Remesa rempa = (Remesa) request.getAttribute("remesa_padre");
          descripcion= ( rempa.getDescripcion() != null )? rempa.getDescripcion() :"" ;
          model.remesaService.setPadre( null );
      }
	String padre ="";	
        String generado = ( request.getParameter("generado") != null )? request.getParameter("generado") :"";
		if(request.getParameter("numpadre")!=null){
			padre = request.getParameter("numpadre");
		} 
        if( session.getAttribute( "hijas" ) != null ){
            hijas = (Vector) session.getAttribute( "hijas" );
        }
        
	%>	
	<table width="800" border='2'>
	
            <tr >
                        <td width="49%" height="25"class="subtitulo1" >&nbsp; Agregar Remesas a remesa Padre</td>
                        <td  class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif"><%=datos[0]%></td>
                    </tr>
		
	<tr>
			
	<td colspan='2'>
	   </td>
            </tr>
         </table> 
            <table width="800"  border="2" cellspacing="3" cellpadding="1">                
                <tr  class="filaresaltada">
                <td width="50%">
                    <input checked type="radio" onclick="tipOperacion(this,0)" name="operacion" id="operacion1" value="1"> <strong class="Estilo6">Asociar a una remesa Padre existente </strong>
                </td>
                <td width="50%">
                    <input <%=selectedOperacion%> type="radio" onclick="tipOperacion(this,0)" name="operacion" id="operacion2" value="2"> <strong class="Estilo6">  Agregar a una remesa Padre </strong>
                </td>
                </tr>
                <tr>
                <td><table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="barratitulo">
                    <tr>
                        <td width="50%" class="subtitulo1" colspan='3'>&nbsp; Remesa Padre</td>
                        <td width="50%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif"></td>
                    </tr>
                </table>   
                <table width="100%" align="center" class="Letras">
                    <tr class="filaresaltada">
                    <td  nowrap><strong class="Estilo6">Numero de la Remesa :</strong></td>
                    <td  nowrap ><input  class="textbox" name="numpadre" type="text" id="numpadre2" maxlength="10" value="<%= ( request.getParameter("numpadre")!=null )? request.getParameter("numpadre") : ""%>">
                        <img src="<%=BASEURL%>/images/botones/buscar.gif" onClick="tipOperacion('sub2', 3)" id="btnBuscarPadre" alt="Buscar remesa padre" width="90" height="21"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
                    </td>
                    </tr>
                    <tr bgcolor="#99CCFF" class="filaresaltada">
                    <td nowrap><strong>Descripcion :</strong></td>
                    <td nowrap ><input  class="textbox" name="texto" type="text" id="texto2" value="<%=descripcion%>" size="40"></td>
                    </tr>
                </table></td>
          
                <td>
                    <table width="50%"  border="0" align="center">
                        <tr>
                        <td><table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="barratitulo">
                            <tr>
                                <td width="50%" class="subtitulo1" colspan='3'>&nbsp; Remesa </td>
                                <td width="50%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif"></td>
                            </tr>
                        </table>        <table width="100%" align="center" class="Letras">
                            <tr class="filaresaltada">
                                <td width="162" nowrap>Numero de la Remesa:</td>
                                
                                <td nowrap><input name="numrem" type="text" class="textbox" id="numrem" value="<%= (request.getParameter("numrem") != null)? request.getParameter("numrem"):"" %>">
                                    <img src="<%=BASEURL%>/images/botones/buscar.gif" id="btnBuscarRemesa" style="cursor:hand" width="87" height="21" onClick="tipOperacion('sub2',2)" onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);">
                                </td>
                            </tr>
        
                            <tr class="filaresaltada">
                                <td width="162" nowrap>Generar remesa Padre :</td>
                                <td  nowrap >
                                    <%if( hijas != null && hijas.size() > 0 && request.getParameter("numpadre").equals("") ){%>
                                    <img src="<%=BASEURL%>/images/botones/aceptar.gif" id="btnGenerarPadre" alt="Generar remesa padre" width="90" height="21" onClick="tipOperacion('sub1',1)" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
                                    <%}else{%>
                                    <img src="<%=BASEURL%>/images/botones/aceptarDisable.gif" alt="Generar remesa padre" width="90" height="21" >
                                    <%}%>
                                </td>
                            </tr>
        
                        </table></td>
                        </tr>
                    </table>
                </td>
                </tr>
            </table> 
           
        </div>
      
        <br>
        <br>
        <div align="center">            

        
    <%if(request.getParameter("numpadre")!=null && !request.getParameter("numpadre").equals("") ){%>
            <p align="center">
            <input type="image" src="<%=BASEURL%>/images/botones/agregar.gif" onClick="tipOperacion('sub1',4)" alt="Agregar remesa(s) a remesa Padre" width="90" height="21" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
            <img src="<%=BASEURL%>/images/botones/restablecer.gif" name="c_rest" style="cursor:pointer" onClick="location.replace('<%=BASEURL%>/colpapel/agregarOTpadre.jsp');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
            <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" style="cursor:pointer" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></p>
  <%} else{%>
            <p align="center">
            <img src="<%=BASEURL%>/images/botones/restablecer.gif" name="c_rest" style="cursor:pointer" onClick="location.replace('<%=BASEURL%>/colpapel/agregarOTpadre.jsp');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
            <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" style="cursor:pointer" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> </p>
    <%}%>  
            <div align="center">
    
    <%if(request.getParameter("mensaje")!=null && !request.getParameter("mensaje").equals("null") ){%>
    	
                <table border="2" align="center">
                    <tr>
                        <td>
                        <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                            <tr>
                                <td width="480" align="center" class="mensajes"><%=request.getParameter("mensaje")%></td>
                                <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
                            </tr>
                        </table></td>
                    </tr>
                </table>
 	
    <%}%>
            </div>
    
    
    <%
    if( hijas.size() > 0 ){  
        if( !padre.equals("") && request.getParameter("generado") == null ){
        %>
            <table width="70%"  border="2">
                <tr>
                    <td>
                        <table width="100%" border="0" cellpadding="0" cellspacing="0">
                            <tr align="center">
                                <td height="22" class="subtitulo1">&nbsp; Remesas Asociadas a la Remesa Padre <%=padre.toUpperCase()%></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        <%
        }
        for( int i=0; i<hijas.size(); i++ ){
            Remesa rem = (Remesa) hijas.get(i);
	    //request.getSession().setAttribute("remesa",rem);
            estandar = rem.getDescripcion();
            standar= rem.getStdJobNo();
            docuinterno = rem.getDocInterno();
            remitentes = rem.getRemitente();
            destinatarios = rem.getDestinatario();
            String remesa2 = rem.getNumrem();
	    observacion = rem.getObservacion();
	    cliente = rem.getCliente();
	    rem_padre = rem.getPadre();
            
         
%>
        <input name="standard" type="hidden" id="standard3" value="<%=standar%>">
                                <input name="remesa" type="hidden" id="remesa3" value="<%=remesa%>"> 
				<input name="remitentes" type="hidden" id="remitentes3" value="<%=remitentes%>">
				<input name="destinatarios" type="hidden" id="destinatarios3" value="<%=destinatarios%>"></td>
            <table width="1200"  border="2">
                <tr>
                <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td height="22" class="subtitulo1">&nbsp; Datos Remesa <span class="Estilo6"><%=remesa2%></span></td>
                    </tr>
                </table>          
								
				<table width="100%" align="center" class="tablaInferior"  >
                        <tr>
                        <th width="94" align="left" class="fila">Estandar Job :</th>
                        <td width="299" class="letra"> <%=estandar%>
                            </td>
                        <td width="89" class="fila">Cliente :</td>
                        <td width="308" class="letra"><%=cliente%></td>
						
						<td width="91"  class="fila">Documentos :</td>
                        <td width="279"  class="letra"><%=docuinterno%></td>
						
                       
                        
                        </tr>

                        <tr>
                       
					    <td width="94" class="fila" >Remitente :</td>
                        <td width="299" class="letra"><%=remitentes%></td>
                        <td width="89" class="fila">Destinatario :</td>
                        <td width="308" class="letra"><%=destinatarios%></td>
					    
                        <td  class="fila">Observacion :</td>
                        <td colspan="" class="letra"><%=observacion%></td>
                       
                        </tr>
                        
                        <tr>
                        
                        </tr>
        
                    </table>
                </td>
                </tr>
            </table>
    
    <%
        
        }
    }
    %>
     
        </div>
  
        </form>
        <iframe width=188 height=166 name="gToday:datetime:agenda.js:gfPop:plugins_12.js" id="gToday:datetime:agenda.js:gfPop:plugins_12.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
    </div>
</body>

<%=datos[1]%>

</html>

<script>

    function tipOperacion( opc, accion ){                
                
        var padre = form1.numpadre.value;
        var numrem = form1.numrem.value;

        var chk = (document.getElementById("operacion1").checked)? "1" : "2";

        if( accion == "0" ){
            if( opc.value == "2" || chk == "2"){

                document.getElementById("numpadre2").style.backgroundColor="#CCCCCC";
                document.getElementById("numpadre2").readOnly = true;
                document.getElementById("texto2").style.backgroundColor="#CCCCCC";    
                document.getElementById("numrem").style.backgroundColor="#FFFFFF";
                document.getElementById("numrem").readOnly = false;
                document.getElementById("btnBuscarPadre").src= "<%=BASEURL%>/images/botones/buscarDisable.gif";
                document.getElementById("btnBuscarRemesa").src= "<%=BASEURL%>/images/botones/buscar.gif";

                document.getElementById("btnBuscarPadre").onmouseover="";
                document.getElementById("btnBuscarPadre").onmouseout="";

                document.getElementById("btnBuscarRemesa").onmouseover="botonOver(this);"
                document.getElementById("btnBuscarRemesa").onmouseout="botonOut(this);"

            }else{

                document.getElementById("numpadre2").style.backgroundColor="#FFFFFF";
                document.getElementById("numpadre2").readOnly = false;
                document.getElementById("texto2").style.backgroundColor="#FFFFFF";    
                document.getElementById("numrem").style.backgroundColor="#CCCCCC";
                document.getElementById("numrem").readOnly = true;
                document.getElementById("btnBuscarPadre").src= "<%=BASEURL%>/images/botones/buscar.gif";
                document.getElementById("btnBuscarRemesa").src= "<%=BASEURL%>/images/botones/buscarDisable.gif";

                document.getElementById("btnBuscarRemesa").onmouseover="";
                document.getElementById("btnBuscarRemesa").onmouseout="";

                document.getElementById("btnBuscarPadre").onmouseover="botonOver(this);"
                document.getElementById("btnBuscarPadre").onmouseout="botonOut(this);"


            }
        }else if( accion == "2" && chk == "2" ){
            if ( validar() ){
                padre = "";
                window.location='<%=CONTROLLER%>?estado=RemesaPadre&accion=Search&padre=0k&numrem='+numrem+'&numpadre='+padre+'&texto='+form1.texto.value+'&operacion='+chk+'&opcion=1';
            }
        }else if( accion == "1" && chk == "2" ){
            if ( validar() ){
                window.location='<%=CONTROLLER%>?estado=Generar&accion=Padre&texto='+form1.texto.value+'&remesa='+form1.remesa.value+'&operacion='+chk+'&numrem='+numrem;
            }    
        }else if( accion == "3" && chk == "1" ){
            window.location='<%=CONTROLLER%>?estado=RemesaPadre&accion=Search&padre=0k&numrem='+numrem+'&numpadre='+padre+'&texto='+form1.texto.value+'&operacion='+chk+'&opcion=2';
        }
        /*
        else if( accion == "4" && chk == "2" ){
            if ( validar() ){
                window.location='<%=CONTROLLER%>?estado=RemesaPadre&accion=Insert&padre=0k&numrem='+numrem+'&numpadre='+padre+'&texto='+form1.texto.value+'&operacion='+chk+'&opcion=2';
            }    
        }
        */
		
    }
    
    function validar(){
                           
        var remesaParser = /^\w+$/;        
        var remesasList = trim(form1.numrem.value.toUpperCase());
        
        var remesasArray = remesasList.split(",");
        var idx = 0;
        while( idx < remesasArray.length ){
        
            var remesa = trim(remesasArray[idx]);
            var valido = remesaParser.test(remesa);            
            if( valido ){
                idx++;
            }  
            else{
                alert(
                "Lista de remesas incorrecta." +                
                "\n1.  La remesa DEBE tener solo letras y/o n�meros." +
                "\n1.  La lista DEBE separarse por comas." +                
                "\n1.  El campo no debe estar vac�o." +
                "\nPor favor, rectifique."
              );
              return false;
            }
        } 
        return true;
        
      }
        
 
</script>