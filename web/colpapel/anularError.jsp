<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/validar.js">
</script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</script>

<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Anular Remesa"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

  <%//Inicializo variables
String estandar="", trailer="", fecha="",standar="", observacion="",docuinterno="", remitentes="", ruta="",destinatarios="", remesa="", planilla="", cedula="", nombre="", placa="", precinto=""; 
float peso=0, valor=0;
%>
 <%List planillas = new LinkedList();
  planillas=(List)request.getAttribute("plaerror");
  request.setAttribute("plaerror",planillas);%>
  <br>
  <form name="form1" method="post" action="">

  <table width="73%"  border="2" align="center">
    <tr>
      <td><table width="100%" align="center" class="tablaInferior">
        <tr>
          <td height="22" colspan=2 class="subtitulo1">Mensaje</td>
          <td width="308" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
        <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="fila">
        <tr>
          <td colspan="6" valign="top"><span class="Estilo19">Ocurri&oacute; un error al tratar de Anular la Remesa No. <%=request.getParameter("remesa")%>. </span><br>
              <span class="Estilo21"><br>
      Raz&oacute;n : una de las planillas solo esta relacionada con esta remesa.</span><br>
      <br>
      <span class="Estilo21">Para corregir este error usted puede:</span><br>
      <br>
      <table width="100%" height="21" class="fila">
        <tr>
          <td width="9%" rowspan="3">&nbsp;</td>
          <td width="91%" class="Letras"  ><ul>
              <li class="Estilo24">Asignar una remesa existente a todas las planillas </li>
          </ul></td>
        </tr>
        <tr>
          <td width="91%" class="Letras" ><ul>
              <li class="Estilo24">Crear una nueva remesa para todas las planillas </li>
          </ul></td>
        </tr >
        <%if(planillas.size()<=1){
		  Iterator it=planillas.iterator();
	  int i=0;
	  while (it.hasNext()){
	  Planilla plani = (Planilla) it.next();%>
        <tr >
          <td class="Letras" ><ul>
              <li class="Estilo24">Anular la remesa y la planilla (Anular Despacho)</li>
          </ul></td>
        </tr>
        <%}
		  }%>
      </table>
      <span class="Estilo24">Luego de realizar este proceso usted podr&aacute; eliminar la Remesa No <%=request.getParameter("remesa")%>. </span><br></td>
        </tr>
      </table></td>
    </tr>
  </table>
  <br>

  <input name="ventana" type="hidden" id="ventana"> 
  <table width="100%"  border="2">
    <tr>
      <td><table width="100%" align="center" class="tablaInferior">
        <tr>
          <td height="22" colspan=2 class="subtitulo1"><span class="Estilo21">Lista de Planillas que generan error al intentar anular la Remesa No. <%=request.getParameter("remesa")%></span><span class="titulo"><strong><br>
          </strong></span></td>
          <td width="308" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
        <table width="100%"  border="1" cellpadding="3" cellspacing="2" bordercolor="#999999">
        <tr class="tblTitulo">
          <td width="11%" height="23"><strong>PLANILLA</strong></td>
          <td width="18%"><strong>FECHA DESPACHO</strong></td>
          <td width="13%"><strong>PLACA</strong></td>
          <td width="20%"><strong>CONDUCTOR</strong></td>
          <td colspan="2"><div align="center"><strong>ACCION</strong></div></td>
        </tr>
        <% Iterator it=planillas.iterator();
	  int i=0;
	  while (it.hasNext()){
	  Planilla plani = (Planilla) it.next();%>
        <tr class="<%=i%2==0?"filagris":"filaazul"%>">
          <td height="20" class="bordereporte"><%=plani.getNumpla()%></td>
          <td class="bordereporte"><%=plani.getFecdsp().substring(0,10)%></td>
          <td class="bordereporte"><%=plani.getPlaveh()%></td>
          <td class="bordereporte"><strong><%=plani.getCedcon()%><br>
              <%=plani.getNomCond()%></strong></td>
          <td class="bordereporte" width="19%" style="cursor:hand" title="Agregar Remesa..."  onClick="openPage('colpapel/RemesaExistente.jsp?planilla=<%=plani.getNumpla()%>&anular=<%=request.getParameter("remesa")%>','')" ><span class="Estilo24"><u>Asignar una remesa existente a esta planilla</u></span></td>
          <td  class="bordereporte" width="19%" style="cursor:hand" title="Agregar Remesa..."  onClick="openPage('<%=CONTROLLER%>?estado=Planilla&accion=Search&colpapel=ok&nremesa=ok&planilla=<%=plani.getNumpla()%>&anular=<%=request.getParameter("remesa")%>','')" ><span class="Estilo24"><u>Crear una nueva remesa para esta planilla</u></span></td>
        </tr>
        <%}%>
      </table></td>
    </tr>
  </table> 
  <div align="center"><br>
    <br>
    <br>
    <img  src="<%=BASEURL%>/images/botones/regresar.gif" width="90" height="21" border="0" onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);" onClick="window.location='<%=CONTROLLER%>?estado=Remesa&accion=Search&placol=0k&remesa=<%=request.getParameter("remesa")%>';this.disabled=true">
    <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand"></div>
  </form>
</div>
 
</body>
</html>
