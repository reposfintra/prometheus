<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="../js/validar.js">
</script>
<style type="text/css">
<!--
.Estilo5 {font-family: Arial, Helvetica, sans-serif; font-size: 10pt; }
.Estilo6 {font-family: Arial, Helvetica, sans-serif; font-size: 10pt}
.Estilo7 {font-size: 9pt}
.Estilo8 {font-family: Arial, Helvetica, sans-serif; font-size: 8; }
.Estilo11 {color: #0066FF}
.Estilo14 {font-size: 9pt; font-weight: bold; }
.Estilo15 {font-family: Arial, Helvetica, sans-serif}
-->
</style>
</head>

<body onLoad="limpiarColpapel();">
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Colpapel&accion=Validar&modif=show" onSubmit="return  ValidarColpapel(form1)">
  <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
  <%java.util.Date date = new java.util.Date();
               %>
  <%
  	String nitpro = "";
  	String placa="", trailer="", conductor="", nomcond="", estandard="",  standar="", remitente="", destinatario="",docinterno="", fechadesp="", observacion="", rutaP="", precinto="" ;
	float ant=0, peso=0;
 	model.planillaService.bucarColpapel(request.getParameter("planilla"));
    if(model.planillaService.getPlanilla()!=null){
        Planilla pla= model.planillaService.getPlanilla();
        placa=pla.getPlaveh();
		trailer=pla.getPlatlr();
		conductor= pla.getCedcon();
		nomcond=pla.getNomCond();
		estandard=pla.getSj_desc();
		standar= pla.getSj();
		fechadesp = pla.getFecdsp();
		peso=pla.getPesoreal();
		rutaP = pla.getRuta_pla();
		precinto = pla.getPrecinto();
		nitpro = pla.getNitpro();
    }
    model.movplaService.buscaMovpla(request.getParameter("planilla"),"01");
    if(model.movplaService.getMovPla()!=null){
		Movpla movpla= model.movplaService.getMovPla();
		ant= movpla.getVlr();
    }
    model.remesaService.buscaRemesa(request.getParameter("remesa"));
    if(model.remesaService.getRemesa()!=null){
        Remesa rem = model.remesaService.getRemesa();
        remitente = rem.getRemitente();
        destinatario= rem.getDestinatario();
        docinterno = rem.getDocInterno();
		observacion = rem.getObservacion();
		
    }%>
  <br>
  <table width="655" border="1" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" bgcolor="#ECE0D8">
    <tr bgcolor="#FFA928">
      <th colspan="2" class="Estilo5" scope="col">MODIFICAR PLANILLA No. <%=request.getParameter("planilla")%></th>
    </tr>
    <tr bgcolor="#99CCFF" class="Estilo6">
      <th colspan="2" class="Estilo5" scope="row">INFORMACION DEL CLIENTE </th>
    </tr>
    <tr class="Estilo6">
      <th width="164" scope="row"><div align="left"><span class="Estilo6">&nbsp;ESTANDARD JOB </span></div></th>
      <td width="475"><span class="Estilo5">
       
      </span><span class="Estilo6"></span><span class="Estilo6"><%=estandard%>
      <input name="planilla" type="hidden" id="planilla" value="<%=request.getParameter("planilla")%>">
      <input name="standard" type="hidden" id="standard" value="<%=standar%>">
      <input name="remesa" type="hidden" id="remesa" value="<%=request.getParameter("remesa")%>">
      </span></td>
    </tr>
    <tr class="Estilo5">
      <th scope="row"><div align="left"><span class="Estilo7">&nbsp;REMITENTE</span></div></th>
      <td><div align="left"><span class="Estilo6">
        <input name="remitentes" type="hidden" id="remitentes" value="<%=remitente%>">
        <input type="button" name="Submit" value="Modificar Remitentes...." onClick="abrirPagina('remitentes.jsp?sj=','');">
      </span>
      </div></td>
    </tr>
    <tr class="Estilo5">
      <th scope="row"><div align="left"><span class="Estilo7">&nbsp;DESTINATARIO</span></div></th>
      <td><div align="left"><span class="Estilo6">
        </span>        <span class="Estilo6">
      </span><span class="Estilo6">
      <input name="destinatarios" type="hidden" id="destinatarios" value="<%=destinatario%>" readonly>
         <input type="button" name="Submit2" value="Modificar Destinatarios..." onClick="abrirPagina('destinatarios.jsp?sj=','');">
         <input name="docudest" type="hidden" id="docudest" value="<%=request.getParameter("docudest")%>">
      </span></div></td>
    </tr>
    <tr class="Estilo5">
      <th class="Estilo7" scope="row"><div align="left">&nbsp;DOCUMENTO INTERNO </div></th>
      <td><input name="docinterno" type="text" id="docinterno" size="38" value="<%=docinterno%>"></td>
    </tr>
    <tr class="Estilo5">
      <th class="Estilo7" scope="row"><div align="left">FECHA DESPACHO </div></th>
      <td><input name="fechadesp" type="text" id="fechadesp" size="18" value="<%=fechadesp%>" readonly>
        <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fechadesp);return false;" HIDEFOCUS> <img src="../js/Calendario/cal.gif" width="16" height="16"
               border="0" alt="De click aqu&iacute; para escoger la fecha"></a></td>
    </tr>
    <tr bgcolor="#99CCFF" class="Estilo5">
      <th colspan="2" scope="row">&nbsp;INFORMACION DEL CONDUCTOR </th>
    </tr>
    <tr class="Estilo5">
      <th height="26" scope="row"><div align="left"><span class="Estilo7">&nbsp;PLACA</span></div></th>
      <td><input name="placa" type="text" id="placa" size="12" value="<%=placa%>">        <div align="center">
        </div></td>
    </tr>
    <tr class="Estilo5">
      <th class="Estilo7" scope="row"><div align="left">&nbsp;TRAILER</div></th>
      <td><input name="trailer" type="text" id="trailer" size="12" value="<%=trailer%>">        <div align="center">
        </div></td>
    </tr>
    <tr class="Estilo5">
      <th class="Estilo7" scope="row"><div align="left">&nbsp;CEDULA CONDUCTOR </div></th>
      <td><input name="conductor" type="text" id="conductor" size="12" value="<%=conductor%>">  <%=nomcond%>    
        <div align="center">
      </div></td>
    </tr>
    <tr class="Estilo5">
      <th class="Estilo7" scope="row"><div align="left">&nbsp;RUTA PLANILLA </div></th>
      <td>
	 <% List rutas = model.stdjobcostoService.getRutas(standar);%>
		  <select name="ruta" id="ruta" onChange="cambiarFormulario2('modificarDespachoSegunda.jsp')">

		  <%
		   	Iterator it=rutas.iterator();
				while (it.hasNext()){
					Stdjobcosto std = (Stdjobcosto) it.next();
					String ruta = std.getRuta();
					String codigo=std.getFt_code();
					if(codigo.equals(request.getParameter("ruta"))){
					%>	<option value="<%=codigo%>" selected><%=ruta%></option>
					<%}else{%>
					<option value="<%=codigo%>"><%=ruta%></option>
					<%}}%>
        </select>
	  </td>
    </tr>
    <tr class="Estilo5">
      <th class="Estilo7" scope="row"><div align="left">&nbsp;CANTIDAD</div></th>
      <td><input name="toneladas" type="text" id="toneladas" size="12" value="<%=peso%>"> 
      TON </td>
    </tr>
    <tr class="Estilo5">
      <th class="Estilo7" scope="row"><div align="left">&nbsp;VALOR</div></th>
      <td><% List costos = model.stdjobcostoService.getCostos(request.getParameter("ruta"),standar);%>
		  <select name="valorpla" id="valorpla" onChange="">
		 	<option value="">Seleccione Alguna</option>

		  <%
		   	it=costos.iterator();
				while (it.hasNext()){
					Stdjobcosto std = (Stdjobcosto) it.next();
					float costo = std.getUnit_cost();
					String codigo=std.getFt_code();
					%>	<option value="<%=codigo%>"><%=costo%></option>
					<%}%>
		    
          </select>
		        <input name="anticipo" type="text" id="anticipo" value="<%=ant%>">      <select name="moneda" id="moneda">
        <option selected value="PES">PES</option>
        <option value="BOL">BOL</option>
        <option value="DOL">DOL</option>
        </select></td>
    </tr>
    <tr class="Estilo5">
      <th scope="row"><div align="left" class="Estilo7">&nbsp;PRECINTOS</div></th>
      <td><input name="precintos" type="text" id="precintos" size="48" value="<%=precinto%>"></td>
    </tr>
	
    <tr class="Estilo5">
      <th class="Estilo7" scope="row"><div align="left">&nbsp;OBSERVACION</div></th>
      <td><textarea name="observacion" cols="60" id="observacion"><%=observacion%></textarea></td>
    </tr>
  </table>
  <p align="center">
    <input type="submit" name="Submit8" value="Validar">
    <input type="submit" name="Submit5" value="Regresar">
  </p>
 
</form>
<iframe width=188 height=166 name="gToday:datetime:agenda.js:gfPop:plugins_12.js" id="gToday:datetime:agenda.js:gfPop:plugins_12.js" src="../js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</body>
</html>
