<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Agregar Extrafletes</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</script>

</head>

<body onLoad="form1.planilla.focus();">

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Aplicar Extrafletes"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

  <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
  <br>
  <form name="form1" method="post" action="<%=CONTROLLER%>?estado=Planilla&accion=Extraflete&cmd=Buscar" >
   <table width="50%"  border="2" align="center">
    <tr>
      <td>

    <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="barratitulo">
      <tr>
        <td width="50%" class="subtitulo1" colspan='3'>Buscar Planilla </td>
        <td width="50%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif"></td>
      </tr>
    </table>
    <table width="100%" align="center">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr class="fila">
              <td width="179" >Numero de la planilla</td>
              <td width="306"><input name="planilla" type="text" id="planilla">
                <input name="modificar" type="hidden" id="modificar">               
                <a href='#' onclick="if( planilla.value==''){alert('Deber� digitar la planilla');  planilla.focus(); }else{  var x = window.open('<%=CONTROLLER%>?estado=AdicionAjustes&accion=Planillas&evento=BUSCAR&oc='+ planilla.value ,'AplicarExtra','resizable=yes,top=20,left=200,width=750, height=550, scrollbars=yes,statusbar=yes, status=yes'); }" title='Permite aplicar valores a la planilla sin tener en cuenta el Estandar'>Cargo Directo</a>
              </td>
            </tr>
        </table></td>
      </tr>
    </table>	</td>
      </tr>
    </table>
	<div align="center">
	  <img  style="cursor:hand " src="<%=BASEURL%>/images/botones/buscar.gif" align="middle" width="87" height="21" border="0" onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);" onClick="if(form1.planilla.value ==''){ alert('Escriba la planilla');}else{ form1.submit();this.disabled='true';}">
	</div>
<%if(model.planillaService.getPlas()!=null){
 %>
    
	<table width="70%"  border="2" align="center">
    <tr>
      <td>
	<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="barratitulo">
      <tr>
        <td width="50%" class="subtitulo1" colspan='3'>LISTA DE PLANILLAS </td>
        <td width="50%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif"></td>
      </tr>
    </table>
    <table width="100%" border="1" align="center" bordercolor="#999999">
      <tr class="tblTitulo">
        <td width="82" nowrap><strong>PLANILLA</strong></td>
        <td width="79"><div align="center"><strong>REMESA</strong></div></td>
        <td width="74"><div align="center"><strong>PLACA</strong></div></td>
        <td width="111" ><div align="center"> <strong>CONDUCTOR</strong></div></td>
        <td width="150" ><div align="center"><strong>NOMBRE CONDUCTOR</strong></div></td>
      </tr>
      <% Vector planillas = model.planillaService.getPlas();
	  
	  	for(int i = 0; i<planillas.size(); i++){
	  		Planilla planilla = (Planilla) planillas.elementAt(i);
		%>
      <tr class="<%=i%2==0?"filagris":"filaazul"%>"  style="cursor:hand" title="Aplicar Extrafletes..." onClick="window.location='<%=CONTROLLER%>?estado=Planilla&accion=Extraflete&cmd=Buscar&planilla=<%=planilla.getNumpla()%>&remesa=<%=planilla.getNumrem()%>'"  onMouseOver='cambiarColorMouse(this)'>
        <td class="bordereporte"> <%=planilla.getNumpla()%></td>
        <td class="bordereporte"> <%=planilla.getNumrem()%> </td>
        <td class="bordereporte"> <%=planilla.getPlaveh()%> </td>
        <td class="bordereporte"><%=planilla.getCedcon()%></td>
        <td class="bordereporte"><%=planilla.getNomCond()%></td>
      </tr>
      <%}
  %>
    </table>
	</td>
      </tr>
    </table>
    <%}%>
    <br>
    <%if(request.getParameter("mensaje")!=null){%>
    <table border="2" align="center">
      <tr>
        <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
              <td width="229" align="center" class="mensajes"><span class="normal"><%=request.getParameter("mensaje")%></span></td>
              <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
              <td width="58">&nbsp;</td>
            </tr>
        </table></td>
      </tr>
    </table>
    <%}%>
      </form>
 
</div>
</body>
</html>
