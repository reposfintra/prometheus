<%-- 
    Document   : menuNew
    Created on : 29/01/2014, 02:48:18 PM
    Author     : jpacosta
--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title></title>
        <script type="text/javascript" src="../js/menus/menu.js"></script>
        <link rel="stylesheet" type="text/css" href="../css/menus/menu.css"/>
    </head>
    <body style="background-color:rgba(239, 238, 224, 0.22);">
        <div id="menu">
            <script>
            <% 

            Usuario usuario = (Usuario)session.getAttribute("Usuario");
            String login = (String)session.getAttribute("login_into");
            String dstrct = usuario.getEmpresa();
            if(login.equals("Fenalco")) {
                    login= "FENALCO ATLANTICO";
            } else if (login.equals("Fenalco_bol")) {
                    login = "FENALCO BOLIVAR";
            } else {
                    login = model.menuService.cargarNombreEmpresa(dstrct);
            }
            String json = model.menuService.cargarMenuJson(usuario.getLogin(),login, dstrct);
            %>
                menuJSon(<%=json%>);
            </script>
        </div>
    </body>
</html>
