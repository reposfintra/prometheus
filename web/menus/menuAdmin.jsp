<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title></title>

<script type="text/javascript">
// Framebuster script to relocate browser when MSIE bookmarks this
// page instead of the parent frameset.  Set variable relocateURL to
// the index document of your website (relative URLs are ok):
var relocateURL = "/";

if(parent.frames.length == 0) {
  if(document.images) {
    location.replace(relocateURL);
  } else {
    location = relocateURL;
  }
}
</script>

<script type="text/javascript" src="../js/mtmcode.js">
</script>

<script type="text/javascript">
// Morten's JavaScript Tree Menu
// version 2.3.1, dated 2002-02-02
// http://www.treemenu.com/

// Copyright (c) 2001-2002, Morten Wang & contributors
// All rights reserved.

// This software is released under the BSD License which should accompany
// it in the file "COPYING".  If you do not have this file you can access
// the license through the WWW at http://www.treemenu.com/license.txt

// Nearly all user-configurable options are set to their default values.
// Have a look at the section "Setting options" in the installation guide
// for description of each option and their possible values.

/******************************************************************************
* User-configurable options.                                                  *
******************************************************************************/

// Menu table width, either a pixel-value (number) or a percentage value.
MTMTableWidth = "100%";

// Name of the frame where the menu is to appear.
MTMenuFrame = "code";

// Name of the frame which contains code.html
MTMCodeFrame = "code";

// Variable for determining how a sub-menu gets a plus-sign.
// "Never" means it never gets a plus sign, "Always" means always,
// "Submenu" means when it contains another submenu.
MTMSubsGetPlus = "Always";

// variable that defines whether the menu emulates the behaviour of
// Windows Explorer
MTMEmulateWE = false;


// variable that defines if we should make submenu entries links if the
// menu has no URL attached when we emulate the Windows Explorer. Set to
// true if you want every submenu title to be a link (either to expand the
// menu or to access the URL for the menu. This setting was the default
// up to version 2.3.0). If set to false, submenu titles will not show up
// as links if no URL was specified for them.
MTMAlwaysLinkIfWE = true;

// Directory of menu images/icons
MTMenuImageDirectory = "../images/menu-images/";

// Options for controlling the menu document
MTMDOCTYPE = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">';
MTMcontentType = "text/html; charset=ISO-8859-1";

// Variables for controlling colors in the menu document.
// Regular BODY atttributes as in HTML documents.
MTMBGColor = "#ebebeb";
MTMBackground = "#ebebeb";
MTMTextColor = "black";

// color for all menu items
MTMLinkColor = "black";

// Hover color, when the mouse is over a menu link
MTMAhoverColor = "#3366FF";

// Foreground color for the tracking, clicked submenu and not linked submenu items.
// The latter only applies when MTMEmulateWE is true and MTMAlwaysLinkIfWE is false.


MTMTrackColor = "black";
MTMSubExpandColor = "black";
MTMSubClosedColor = "black";
MTMSubTextColor = "#CCCCCC";

// All options regarding the root text and it's icon
MTMRootIcon = "home.gif";
MTMenuText = "SLT:";
MTMRootColor = "blue";
MTMRootFont = "Arial, Helvetica, sans-serif";
MTMRootCSSize = "84%";
MTMRootFontSize = "-1";

// Font for menu items.
MTMenuFont = "Arial, Helvetica, sans-serif";
MTMenuCSSize = "84%";
MTMenuFontSize = "-1";

// Variables for style sheet usage
// 'true' means use a linked style sheet.
MTMLinkedSS = false;
MTMSSHREF = "edstyle/menu.css";

// Header & footer, these are plain HTML.
// Leave them to be "" if you're not using them

MTMHeader = "";
MTMFooter = "";

// Whether you want an open sub-menu to close automagically
// when another sub-menu is opened.  'true' means auto-close
MTMSubsAutoClose = false;

// This variable controls how long it will take for the menu
// to appear if the tracking code in the content frame has
// failed to display the menu. Number if in tenths of a second
// (1/10) so 10 means "wait 1 second".
MTMTimeOut = 3;

// Message to pop up when a user right-clicks in the menu frame
// in case you prefer that kind of action prevented.

MTMrightClickMessage = "";

// URL for a linked JavaScript file (.js), this URL is relative
// to the directory where code.html is located
MTMLinkedJSURL = "";

MTMLinkedInitFunction = "";

// Cookie usage.  First is use cookies (yes/no, true/false).
// Second is the cookie name to use for storing the menu state.
// Third is how many days we want the cookies to be stored.
// Last is the name of the cookie to store the tracked item in.
// If you do not supply a name for the tracked item cookie feature is turned off,

MTMUseCookies = false;
MTMCookieName = "MTMCookie";
MTMCookieDays = 3;
MTMTrackedCookieName = "";

// Tool tips.  A true/false-value defining whether the support
// for tool tips should exist or not.
MTMUseToolTips = true;


// << Generated by TreeMenu Generator <<

/******************************************************************************
* User-configurable list of icons.                                            *
******************************************************************************/

var MTMIconList = null;
MTMIconList = new IconList();


/******************************************************************************
* User-configurable menu.                                                     *
******************************************************************************/

// Main menu.
var menu = null;
menu = new MTMenu();

menu.addItem("Inicio","../inicio.htm","text","","");
menu.addItem("Despacho","","text","","");
var Var1 = null;
Var1 = new MTMenu();

menu.makeLastSubmenu(Var1,false);
Var1.addItem("Masivo");
var Var2 = null;
Var2 = new MTMenu();

Var1.makeLastSubmenu(Var2,false);
Var2.addItem("Inicio ","../controller?estado=Menu&accion=Enviar&numero=1","text","","");
Var2.addItem("Modificar ","../controller?estado=Menu&accion=Enviar&numero=2","text","","");
Var2.addItem("Anular","../controller?estado=Menu&accion=Enviar&numero=3","text","","");
Var2.addItem("Consultar","../controller?estado=Menu&accion=ConsultaOC&Opcion=PRINCIPAL","text","","");
Var1.addItem("Normal");
var Var3 = null;
Var3 = new MTMenu();

Var1.makeLastSubmenu(Var3,false);
Var3.addItem("Inicio","../controller?estado=Menu&accion=Enviar&numero=20","text","","");
Var3.addItem("Modificar","../controller?estado=Menu&accion=Enviar&numero=21","text","","");
Var3.addItem("Anular","../controller?estado=Menu&accion=Enviar&numero=22","text","","");
Var3.addItem("Consultar","../inicio.htm","text","","");
Var3.addItem("Agregar Planilla","../controller?estado=Menu&accion=Enviar&numero=27","text","","");
Var3.addItem("Anular Planilla","../controller?estado=Menu&accion=Enviar&numero=28","text","","");
Var3.addItem("Agregar Remesa","../controller?estado=Menu&accion=Enviar&numero=29","text","","");
Var3.addItem("Anular Remesa","../controller?estado=Menu&accion=Enviar&numero=30","text","","");
Var3.addItem("Relacionar Remesa con Planilla","../controller?estado=Menu&accion=Enviar&numero=31","text","","");

Var1.addItem("Cumplir Despacho","../controller?estado=Menu&accion=Enviar&numero=32","text","","");
Var1.addItem("Reanticipo","../controller?estado=Menu&accion=Enviar&numero=33","text","","");
Var1.addItem("Registro de Tiempos","../controller?estado=Menu&accion=Enviar&numero=26","text","","");
menu.addItem("Impresion");
var Var4 = null;
Var4 = new MTMenu();

menu.makeLastSubmenu(Var4,false);
Var4.addItem("Planilla"          ,"../controller?estado=Menu&accion=BuscarPlanillaImpresion","text","","");
Var4.addItem("Lista de Planilla" ,"../controller?estado=Menu&accion=PlanillaImpresion","text","","");
Var4.addItem("Remesa","../controller?estado=Menu&accion=ImprimirRemesa","text","","");
Var4.addItem("Lista de Remesas","../controller?estado=Remesa&accion=Buscar","text","","");
Var4.addItem("Remision","../controller?estado=Menu&accion=Remisiones&total=0&opcion=no","text","","");
Var4.addItem("Egreso","../controller?estado=Menu&accion=EsquemaCheque","text","","");
menu.addItem("Consultas");
var Var5 = null;
Var5 = new MTMenu();

menu.makeLastSubmenu(Var5,false);
Var5.addItem("Estandares","../controller?estado=Menu&accion=Enviar&numero=35","text","","");
Var5.addItem("Planillas","../controller?estado=Menu&accion=Enviar&numero=34","text","","");
Var5.addItem("Remesas","../controller?estado=Menu&accion=Enviar&numero=36","text","","");
Var5.addItem("Conductor","../controller?estado=Menu&accion=Enviar&numero=37","text","","");
Var5.addItem("Egresos","../controller?estado=Menu&accion=Enviar&numero=38","text","","");
Var5.addItem("Proveedores de Acpm","../controller?estado=Menu&accion=Enviar&numero=39","text","","");
Var5.addItem("Proveedores de Tiquetes","../controller?estado=Menu&accion=Enviar&numero=40","text","","");
Var5.addItem("Proveedores de Anticipo","../controller?estado=Menu&accion=Enviar&numero=41","text","","");
menu.addItem("Reportes");
var Var6 = null;
Var6 = new MTMenu();

menu.makeLastSubmenu(Var6,false);
Var6.addItem("Reporte registro de Tiempo","../controller?estado=Menu&accion=ReporteRegistroTiempo","text","","");
menu.addItem("Mantenimiento de Tablas","","text","","");
var Var7 = null;
Var7 = new MTMenu();

menu.makeLastSubmenu(Var7,false);
Var7.addItem("Placa");
var Var8 = null;
Var8 = new MTMenu();

Var7.makeLastSubmenu(Var8,false);
Var8.addItem("Ingresar","../controller?estado=Menu&accion=Enviar&numero=18","text","","");
Var8.addItem("Modificar","../controller?estado=Menu&accion=Enviar&numero=19","text","","");
Var7.addItem("Tarifas Diferenciales");
var Var9 = null;
Var9 = new MTMenu();

Var7.makeLastSubmenu(Var9,false);
Var9.addItem("Ingresar","../controller?estado=Menu&accion=Enviar&numero=50","text","","");
Var9.addItem("Modificar","../controller?estado=Menu&accion=Enviar&numero=51","text","","");
Var9.addItem("Anular","../controller?estado=Menu&accion=Enviar&numero=50","text","","");
Var7.addItem("Conductor","../controller?estado=Menu&accion=Conductor","text","","");
Var7.addItem("Peajes");
var Var10 = null;
Var10 = new MTMenu();

Var7.makeLastSubmenu(Var10,false);
Var10.addItem("Ingresar","../controller?estado=Menu&accion=Enviar&numero=15","text","","");
Var10.addItem("Modificar","../controller?estado=Menu&accion=Enviar&numero=16","text","","");
Var10.addItem("Anular","../controller?estado=Menu&accion=Enviar&numero=17","text","","");
Var7.addItem("Proveedores");
var Var11 = null;
Var11 = new MTMenu();

Var7.makeLastSubmenu(Var11,false);
Var11.addItem("Proveedor Acpm");
var Var12 = null;
Var12 = new MTMenu();

Var11.makeLastSubmenu(Var12,false);
Var12.addItem("Ingresar","../controller?estado=Menu&accion=Enviar&numero=4","text","Ingresar","");
Var12.addItem("Modificar","../controller?estado=Menu&accion=Enviar&numero=5","text","Modificar","");
Var12.addItem("Eliminar","../controller?estado=Menu&accion=Enviar&numero=6","text","Eliminar","");
Var11.addItem("Proveedor Tiquetes");
var Var13 = null;
Var13 = new MTMenu();

Var11.makeLastSubmenu(Var13,false);
Var13.addItem("Ingresar","../controller?estado=Menu&accion=Enviar&numero=10","text","Ingresar","");
Var13.addItem("Modificar","../controller?estado=Menu&accion=Enviar&numero=11","text","Modificar","");
Var13.addItem("Eliminar","../controller?estado=Menu&accion=Enviar&numero=12","text","Eliminar","");
Var11.addItem("Proveedor Anticipo");
var Var14 = null;
Var14 = new MTMenu();

Var11.makeLastSubmenu(Var14,false);
Var14.addItem("Ingresar","../controller?estado=Menu&accion=Enviar&numero=7","text","Ingresar","");
Var14.addItem("Modificar","../controller?estado=Menu&accion=Enviar&numero=8","text","Modificar","");
Var14.addItem("Anular","../controller?estado=Menu&accion=Enviar&numero=9","text","Eliminar","");
Var7.addItem("Tiempos");
var Var15 = null;
Var15 = new MTMenu();

Var7.makeLastSubmenu(Var15,false);
Var15.addItem("Definicion de tiempos");
var Var16 = null;
Var16 = new MTMenu();

Var15.makeLastSubmenu(Var16,false);
Var16.addItem("Insertar","","text","","");
Var16.addItem("Modificar","","text","","");
Var16.addItem("Eliminar","","text","","");
Var15.addItem("Diferencias de tiempos");
var Var17 = null;
Var17 = new MTMenu();

Var15.makeLastSubmenu(Var17,false);
Var17.addItem("Insertar","../controller?estado=Menu&accion=Enviar&numero=46","text","","");
Var17.addItem("Modificar","../controller?estado=Menu&accion=Enviar&numero=47","text","","");
Var17.addItem("Eliminar","../controller?estado=Menu&accion=Enviar&numero=48","text","","");
Var15.addItem("Codigos de demora");
var Var18 = null;
Var18 = new MTMenu();

Var15.makeLastSubmenu(Var18,false);
Var18.addItem("Insertar","../controller?estado=Menu&accion=Enviar&numero=13","text","","");
Var18.addItem("Eliminar","../controller?estado=Menu&accion=Enviar&numero=14","text","","");
Var7.addItem("Series");
var Var19 = null;
Var19 = new MTMenu();

Var7.makeLastSubmenu(Var19,false);
Var19.addItem("Insertar","../controller?estado=Menu&accion=Series&Pagina=series/series","text","","");
Var19.addItem("Buscar","../controller?estado=Menu&accion=Series&Pagina=series/buscarseries","text","","");
Var7.addItem("Banco");
var Var20 = null;
Var20 = new MTMenu();

Var7.makeLastSubmenu(Var20,false);
Var20.addItem("Insertar","","text","","");
Var20.addItem("Modificar","","text","","");
Var20.addItem("Eliminar","","text","","");
menu.addItem("Base de Datos","","text","","");
var Var21 = null;
Var21 = new MTMenu();

menu.makeLastSubmenu(Var21,false);
Var21.addItem("Migracion Placas"   ,"../controller?estado=Menu&accion=Placa","text","","");
Var21.addItem("Migracion Planillas","../controller?estado=Menu&accion=PlanillaExport","text","","");
Var21.addItem("Migracion Conductor Propietario","../controller?estado=Menu&accion=MigracionConPro","text","","");
Var21.addItem("Sincronizacion","../controller?estado=Menu&accion=Enviar&numero=52","text","","");
menu.addItem("Configuracion","","text","","");
var Var22 = null;
Var22 = new MTMenu();
menu.makeLastSubmenu(Var22,false);
Var22.addItem("Seleccionar Estandares" ,"../controller?estado=Menu&accion=Enviar&numero=42","text","","");
Var22.addItem("Configurar Tramos" ,"../controller?estado=Menu&accion=Enviar&numero=49","text","","");
Var22.addItem("Compa�ias","","text","","");
var Var23 = null;
Var23= new MTMenu();
Var22.makeLastSubmenu(Var23,false);

Var23.addItem("Ingresa Compa�ia" ,"../controller?estado=Menu&accion=Enviar&numero=43","text","","");
Var23.addItem("Modificar Compa�ia" ,"../controller?estado=Menu&accion=Enviar&numero=44","text","","");
Var23.addItem("Anular Compa�ia" ,"../controller?estado=Menu&accion=Enviar&numero=45","text","","");




</script>

<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"><style type="text/css">
<!--
body {
	background-color: #FFFFFF;
}
-->
</style></head>
<body onload="MTMStartMenu()" text="#ffffcc" link="yellow" vlink="lime" alink="red">
</body>
</html>
