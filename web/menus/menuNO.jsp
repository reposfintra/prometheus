<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title></title>

<script type="text/javascript">
// Framebuster script to relocate browser when MSIE bookmarks this
// page instead of the parent frameset.  Set variable relocateURL to
// the index document of your website (relative URLs are ok):
var relocateURL = "/";

if(parent.frames.length == 0) {
  if(document.images) {
    location.replace(relocateURL);
  } else {
    location = relocateURL;
  }
}
</script>

<script type="text/javascript" src="../js/mtmcode.js">
</script>

<script type="text/javascript">
// Morten's JavaScript Tree Menu
// version 2.3.1, dated 2002-02-02
// http://www.treemenu.com/

// Copyright (c) 2001-2002, Morten Wang & contributors
// All rights reserved.

// This software is released under the BSD License which should accompany
// it in the file "COPYING".  If you do not have this file you can access
// the license through the WWW at http://www.treemenu.com/license.txt

// Nearly all user-configurable options are set to their default values.
// Have a look at the section "Setting options" in the installation guide
// for description of each option and their possible values.

/******************************************************************************
* User-configurable options.                                                  *
******************************************************************************/

// Menu table width, either a pixel-value (number) or a percentage value.
MTMTableWidth = "100%";

// Name of the frame where the menu is to appear.
MTMenuFrame = "code";

// Name of the frame which contains code.html
MTMCodeFrame = "code";

// Variable for determining how a sub-menu gets a plus-sign.
// "Never" means it never gets a plus sign, "Always" means always,
// "Submenu" means when it contains another submenu.
MTMSubsGetPlus = "Always";

// variable that defines whether the menu emulates the behaviour of
// Windows Explorer
MTMEmulateWE = false;


// variable that defines if we should make submenu entries links if the
// menu has no URL attached when we emulate the Windows Explorer. Set to
// true if you want every submenu title to be a link (either to expand the
// menu or to access the URL for the menu. This setting was the default
// up to version 2.3.0). If set to false, submenu titles will not show up
// as links if no URL was specified for them.
MTMAlwaysLinkIfWE = true;

// Directory of menu images/icons
MTMenuImageDirectory = "../images/menu-images/";

// Options for controlling the menu document
MTMDOCTYPE = '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">';
MTMcontentType = "text/html; charset=ISO-8859-1";

// Variables for controlling colors in the menu document.
// Regular BODY atttributes as in HTML documents.
MTMBGColor = "#EEEEEE";
MTMBackground = "#EEEEEE";
MTMTextColor = "#000066";

// color for all menu items
MTMLinkColor = "#000066";

// Hover color, when the mouse is over a menu link
MTMAhoverColor = "#3366FF";

// Foreground color for the tracking, clicked submenu and not linked submenu items.
// The latter only applies when MTMEmulateWE is true and MTMAlwaysLinkIfWE is false.


MTMTrackColor = "#000066";
MTMSubExpandColor = "#000066";
MTMSubClosedColor = "#000066";
MTMSubTextColor = "#CCCCCC";

// All options regarding the root text and it's icon
MTMRootIcon = "home.gif";
MTMenuText = "<strong>FINTRAVALORES</strong>";
MTMRootColor = "#000066";
MTMRootFont = "Verdana, Helvetica, sans-serif";
MTMRootCSSize = "73%";
MTMRootFontSize = "1";

// Font for menu items.
MTMenuFont = "Verdana, Helvetica, sans-serif";
MTMenuCSSize = "70%";
MTMenuFontSize = "-1";

// Variables for style sheet usage
// 'true' means use a linked style sheet.
MTMLinkedSS = false;
MTMSSHREF = "edstyle/menu.css";

// Header & footer, these are plain HTML.
// Leave them to be "" if you're not using them

MTMHeader = "";
MTMFooter = "";

// Whether you want an open sub-menu to close automagically
// when another sub-menu is opened.  'true' means auto-close
MTMSubsAutoClose = false;

// This variable controls how long it will take for the menu
// to appear if the tracking code in the content frame has
// failed to display the menu. Number if in tenths of a second
// (1/10) so 10 means "wait 1 second".
MTMTimeOut = 3;

// Message to pop up when a user right-clicks in the menu frame
// in case you prefer that kind of action prevented.

MTMrightClickMessage = "";

// URL for a linked JavaScript file (.js), this URL is relative
// to the directory where code.html is located
MTMLinkedJSURL = "";

MTMLinkedInitFunction = "";

// Cookie usage.  First is use cookies (yes/no, true/false).
// Second is the cookie name to use for storing the menu state.
// Third is how many days we want the cookies to be stored.
// Last is the name of the cookie to store the tracked item in.
// If you do not supply a name for the tracked item cookie feature is turned off,

MTMUseCookies = false;
MTMCookieName = "MTMCookie";
MTMCookieDays = 3;
MTMTrackedCookieName = "";

// Tool tips.  A true/false-value defining whether the support
// for tool tips should exist or not.
MTMUseToolTips = true;


// << Generated by TreeMenu Generator <<

/******************************************************************************
* User-configurable list of icons.                                            *
******************************************************************************/

var MTMIconList = null;
MTMIconList = new IconList();
</script>
<% 
Usuario usuario = (Usuario)session.getAttribute("Usuario");
Vector menu =  model.menuService.getMenu();
if ( menu != null ){
	out.println("<script type=text/javascript>");
	out.println("var menu = null;");
	out.println("menu = new MTMenu();");
	out.println("menu.addItem('Inicio', '../inicio.htm','text', 'Pagina de Inicio','','');");
	
	for (int i = 0; i < menu.size(); i++){	
		Menu opcion = (Menu) menu.elementAt(i);
		if( (opcion.getSubmenu() == 1) && (opcion.getIdpadre() == 0) ){
			out.println("menu.addItem('"+opcion.getNombre()+"');");
			out.println("var op"+opcion.getIdopcion()+" = null;");
			out.println("op"+opcion.getIdopcion()+" = new MTMenu();");
			out.println("menu.makeLastSubmenu(op"+opcion.getIdopcion()+",false);");
		} else if ( (opcion.getSubmenu() == 1) && (opcion.getIdpadre() != 0) ){
			out.println("var op"+opcion.getIdopcion()+" = null;");
			out.println("op"+opcion.getIdopcion()+" = new MTMenu();");
        	out.println("op"+opcion.getIdpadre()+".addItem('"+opcion.getNombre()+"', '', 'text', '"+
					opcion.getDescripcion()+"','','');");	
			out.println("op"+opcion.getIdpadre()+".makeLastSubmenu(op"+opcion.getIdopcion()+",false);");
		} else if( (opcion.getSubmenu() == 2) && (opcion.getIdpadre() == 0) ){
			out.println("menu.addItem('" + opcion.getNombre() + "', '" + opcion.getUrl() + "','nueva','" 
					+ opcion.getDescripcion() + "', '','');");
		} else if( (opcion.getSubmenu() == 2) && (opcion.getIdpadre() != 0) ){
			out.println("op"+opcion.getIdpadre()+".addItem('"+opcion.getNombre()+"', '"+opcion.getUrl()
					+"', 'nueva', '" + 	opcion.getDescripcion()+"','','');");   
		}
	}	
	
	out.println("menu.addItem('Ayuda', '../ayuda/HTML/index.html','text','Ayuda', 'menu_link_help.gif','menu_link_help.gif');");
	out.println("</script>");
}%>
</head>
<body onload="MTMStartMenu()" scroll="yes"  text="#ffffcc" link="yellow" vlink="lime" alink="red">
</body>
</html>
