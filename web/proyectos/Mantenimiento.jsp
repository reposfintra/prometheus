<%@ page session="true" %>
<%@ page import="java.util.*" %>
<%@ page import="com.tsp.operation.model.beans.*" %>
<%@ include file="/WEB-INF/InitModel.jsp" %>
<html>
<head>
   <title>Asignación Perfil</title>
   <link rel='stylesheet' href='<%= BASEURL %>/css/estilostsp.css'>
   
   <% Usuario usuarioLogin  = (Usuario)session.getAttribute("Usuario");                      
      String usuLogin       = usuarioLogin.getLogin();                     
      List perfiles         = model.ProyectoSvc.searchProyectos();         
      List usuarios         = model.ProyectoSvc.searchUsuarios();          
      List proUser          = model.ProyectoSvc.searchProyectosUsuarios(); 
   %>
   
    <script src='<%= BASEURL %>/js/Validaciones.js'></script>
    <script>
    
        var viewByPerfil = '';
        var viewByUser   = '';
        
        <%= model.ProyectoSvc.getVarJSSeparador()      %>
        <%= model.ProyectoSvc.getVarJSUsuario()        %>
        <%= model.ProyectoSvc.getVarJSProyectos()      %>        
        <%= model.ProyectoSvc.getVarJSProyectoUsuario()%>
      
    
        function _onmousemove(item) { item.className='select';   }
        function _onmouseout (item) { item.className='unselect'; }
        function addOption(Comb,valor,texto){
           if(valor!='' && texto!=''){
                var Ele = document.createElement("OPTION");
                Ele.value=valor;
                Ele.text=texto;
                Comb.add(Ele);
          }
        }    
        
        function loadCombo(datos, cmb){
            cmb.length = 0;
            for (i=0;i<datos.length;i++){
                var dat = datos[i].split(SeparadorJS);
                addOption(cmb, dat[0], dat[1]);
            }
        }
        
        function loadCombo2(datos, datosA ,cmbA, cmbNA){
            cmbA.length = 0;
            cmbNA.length = 0;
            for (i=0;i<datos.length;i++){
                var dat = datos[i].split(SeparadorJS);                
                if (datosA.indexOf(dat[0])==-1)
                    addOption(cmbNA, dat[0], dat[1]);
                else
                    addOption(cmbA , dat[0], dat[1]);
            }
        }   
     
        function move(cmbO, cmbD){
           for (i=cmbO.length-1; i>=0 ;i--)
            if (cmbO[i].selected){
               addOption(cmbD, cmbO[i].value, cmbO[i].text)
               cmbO.remove(i);
            }
           order(cmbD);
           deleteRepeat(cmbD);
        }
        
        function moveAll(cmbO, cmbD){
           for (i=cmbO.length-1; i>=0 ;i--){
             if(cmbO[i].value!='' && cmbO[i].text!=''){
               addOption(cmbD, cmbO[i].value, cmbO[i].text)
               cmbO.remove(i);
             }
           }
           order(cmbD);
           deleteRepeat(cmbD);
        }
        
        
        function order(cmb){
           for (i=0; i<cmb.length;i++)
             for (j=i+1; j<cmb.length;j++)
                if (cmb[i].text > cmb[j].text){
                    var temp = document.createElement("OPTION");
                    temp.value = cmb[i].value;
                    temp.text  = cmb[i].text;
                    cmb[i].value = cmb[j].value;
                    cmb[i].text  = cmb[j].text;
                    cmb[j].value = temp.value;
                    cmb[j].text  = temp.text;
                }
        }
        
        function selectAll(cmb){
            for (i=0;i<cmb.length;i++)
              cmb[i].selected = true;
        }
        
        function deleteRepeat(cmb){
          var ant='';
          for (i=0;i<cmb.length;i++){
             if(ant== cmb[i].value)
                cmb.remove(i);
             ant = cmb[i].value;
          }
        }
        
        
        function validarFormulario (tform){
            if( tform.UsuariosA.length==0  || tform.GruposNA.length==0 ){
               alert('Debera Seleccionar  Proyecto y usuarios...');
               return false;
            }
            selectAll (tform.UsuariosA);
            selectAll (tform.GruposNA );
            return true;
        }
        
        function SelAll(){
            for(i=0;i<FormularioListado.length;i++)
               FormularioListado.elements[i].checked=FormularioListado.All.checked;
        }
        
        function ActAll(){
            FormularioListado.All.checked = true;
            for(i=0;i<FormularioListado.length;i++)	
              if (FormularioListado.elements[i].type=='checkbox' && !FormularioListado.elements[i].checked && !FormularioListado.elements[i].name!='All'){
                  FormularioListado.All.checked = false;
                  break;
              }
        } 
        
        function validarListado(form){
            for(i=0;i<FormularioListado.length;i++)	
                if (FormularioListado.elements[i].type=='checkbox' && FormularioListado.elements[i].checked && !FormularioListado.elements[i].name!='All')
                    return true;
            alert('Por favor seleccione un item para poder continuar');
            return false;
        }   
   
   
          
        function loadUsuarios(tform,perfil){
            var vec = PerfilUsuariosJS.split('|');
            clear(tform.UsuariosA);
            for(var i=0;i<=(vec.length)-1;i++){
               var vec2 = vec[i].split('-');
               var cod = vec2[0];
               if(cod==perfil){
                   var vec3 = vec2[1].split(',');
                   for(var j=0;j<=(vec3.length)-1;j++){
                       var name = '';
                       for(var g=0;g<=(UsuariosJS.length)-1;g++){
                          var ff = UsuariosJS[g];
                          var kk = ff.split(SeparadorJS);
                          if(vec3[j] == kk[0]){
                             name = kk[1];
                             break;
                          }
                       }
                       addOption(tform.UsuariosA,vec3[j],name);                       
                   }
                   //order(tform.UsuariosA);
               }
            }
            order(tform.UsuariosA);
        }
        
        
       function clear(combo){
           for (k=combo.length-1; k>=0 ;k--)
               combo.remove(k);
        }
   
        
        
    </script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onresize="redimensionar()" onload="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Asignacion Usuario - Proyecto"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<FORM METHOD='POST' ACTION="<%= CONTROLLER %>?estado=Menu&accion=Proyecto" NAME='formularioNuevo' onsubmit='javascript: return validarFormulario(this);'><br>
<table width="80%"  border="2" align="center">
  <tr>
    <td><TABLE class='tablaInferior' border='0' width='100%' cellpadding='3' cellspacing='0' align='center'>
          <TR><th class='barratitulo' colspan='2' height='35'><table width="100%"  border="0">
            <tr>
              <td width="42%" class="subtitulo1">ASIGNACION DE PROYECTOS</td>
              <td width="58%"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
            </tr>
          </table>
            </th>
          </TR>
          <TR><TD align='center'>
              <!-- Grupos Asignados   -->
              
              <TABLE width='100%' border="1" bordercolor="#999999" >
              <TR bgcolor="#F7F5F4">
                  <TH width='20%' class='tblTitulo'>Proyectos             </TH>
                  <TH width='8%' class="bordereporte fila">&nbsp;                </TH>
                  <TH width='20%' class='tblTitulo'>Proyecto a Asignar    </TH>
                  <TH width='4%'  class="bordereporte fila">&nbsp;                     </TH>
                  <TH width='20%' class='tblTitulo'>Usuarios Incorporados </TH>
                  <TH width='8%' class="bordereporte fila">&nbsp;                </TH>
                  <TH width='20%' class='tblTitulo'>Usuarios              </TH>              
              </TR>              
              <TR class="bordereporte">
              <TD class="bordereporte fila"><select multiple size='10' class='select' style='width:100%' name='GruposA' onchange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text;} loadUsuarios(this.form,this[this.selectedIndex].value);"></select></TD>
              <TD class="bordereporte fila" align="center">
			  	<img src='<%=BASEURL%>/images/botones/envIzq.gif' style='cursor:hand' name='imgEnvIzq'  onclick="move   (GruposNA, GruposA );  moveAll(UsuariosA,  UsuariosNA) " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
                <img src='<%=BASEURL%>/images/botones/enviarIzq.gif' style='cursor:hand' name='imgTEnvIzq'  onclick="moveAll(GruposNA, GruposA ) ; moveAll(UsuariosA,  UsuariosNA) " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
				<img src='<%=BASEURL%>/images/botones/envDer.gif' style='cursor:hand'  name='imgEnvDer'  onclick="move   (GruposA,  GruposNA ); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
                <img src='<%=BASEURL%>/images/botones/enviarDerecha.gif' style='cursor:hand' name='imgTEnvDer'  onclick="moveAll(GruposA,  GruposNA ); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
              </TD>
             
              <TD class="bordereporte fila"><select multiple size='10' class='select' style='width:100%' name='GruposNA' onchange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text;}"></select></TD>
              <TD class="bordereporte fila">&nbsp;</TD>
              
              <TD class="bordereporte fila"><select multiple size='10' class='select' style='width:100%' name='UsuariosA' onchange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text;}"></select></TD>
            
              <TD class="bordereporte fila" align="center">
			  <img src='<%=BASEURL%>/images/botones/envIzq.gif' style='cursor:hand' name='imgEnvIzq'  onclick="move   (UsuariosNA, UsuariosA ); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
                <img src='<%=BASEURL%>/images/botones/enviarIzq.gif' style='cursor:hand' name='imgTEnvIzq'  onclick="moveAll(UsuariosNA, UsuariosA ); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
				<img src='<%=BASEURL%>/images/botones/envDer.gif' style='cursor:hand'  name='imgEnvDer'  onclick="move   (UsuariosA,  UsuariosNA); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
                <img src='<%=BASEURL%>/images/botones/enviarDerecha.gif' style='cursor:hand' name='imgTEnvDer'  onclick="moveAll(UsuariosA,  UsuariosNA); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
              </TD>
              <TD class="bordereporte fila"><select multiple size='10' class='select' style='width:100%' name='UsuariosNA' onchange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text;}"></select></TD>
              </TR>
              </TABLE>
              <!-- Fin Grupos no Asignados   -->
             <input type='hidden' name='Id' value=''>
             <input type='hidden' name='usuario' value='<%=usuLogin%>'>
             <input type='hidden' value='' name='Opcion' > 
          </TD></TR>
          <TR>
            <TD align='center' class="fila"><input type="image" name="Submit" value="Ingresar" src="<%= BASEURL %>/images/botones/aceptar.gif" align="absmiddle" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></TD>
          </TR>        
      </TABLE></td>
  </tr>
</table>

       
  </FORM>
    <br>
    
    <table align='center' cellpadding='0' cellspacing='0' width='250' >
       <tr>
           <td width='49%' class='comentario' align='center'><a href="javascript:NuevaVentana12('<%=BASEURL%>/proyectos/ViewProyecto.jsp','byProyecto',400,400,100,100);" class="Simulacion_Hiper">Ver por Proyecto</a></td>
           <td width='2%'  align='center'                   ><font color='red'>|</font>              </td>
           <td width='*'  class='comentario' align='center'><a href="javascript:NuevaVentana12('<%=BASEURL%>/proyectos/ViewUsuario.jsp','byUsuario',400,400,100,100);" class="Simulacion_Hiper">Ver por Usuario</a></td>
       </tr>
    </table>
    
    
     <% String comentario = request.getParameter("comentario"); 
      if(comentario!=null){%>
	  <table border="2" align="center">
   		<tr>
        	<td>
            	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                	<tr>
                    	<td width="229" align="center" class="mensajes"><%=comentario%></td>
                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                    </tr>
                </table>
            </td>
		</tr>
	</table>
   <%}%>
    
   <script>
      loadCombo(UsuariosJS, formularioNuevo.UsuariosNA); 
      loadCombo(GruposJS, formularioNuevo.GruposA); 
      formularioNuevo.Opcion.value='Grabar';
   </script>
</div>   
</body>
</html>