<%@ page session="true" %>
<%@ page import="java.util.*" %>
<%@ page import="com.tsp.operation.model.beans.*" %>
<%@ include file="/WEB-INF/InitModel.jsp" %>
<html>
<head>
  <title>Vista por Perfil</title>
  <link rel='stylesheet' href='<%= BASEURL %>/css/estilostsp.css'>
  <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body>
<table width="80%"  border="2" align="center">
  <tr>
    <td><TABLE width='100%' cellpadding='0' cellspacing='0' class="tablaInferior">
 <TR class="barratitulo"><TH colspan='2'><table width="100%"  border="0">
   <tr>
     <td width="32%" class="subtitulo1">VISTA POR USUARIOS</td>
     <td width="68%"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
   </tr>
 </table>
   </TH>
 </TR>
 <%  List perUser  = model.ProyectoSvc.searchUsuariosProyectos();
     if(perUser!=null){
        Iterator itPer = perUser.iterator();
        String   ant   = "";
        while(itPer.hasNext()){
           PerfilUsuario perfil = (PerfilUsuario) itPer.next(); %>
           <TR VALIGN='top'>
              <TD class="fila"><%=(ant.equals(perfil.getUsuarios()))?"":"&nbsp"+perfil.getUserName()%></TD>
              <TD class="letra"><%=perfil.getPerName()%></TD>
           </TR>           
       <% ant = perfil.getUsuarios();
        } 
      }else{%><TR class="fila">
         <TD colspan='2' align='center' height='40' width='100%'>No hay Usuarios asignados ....</TD></TR><%}%>
</TABLE></td>
  </tr>
</table>
</body>
</html>