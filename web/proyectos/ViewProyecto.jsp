<%@ page session="true" %>
<%@ page import="java.util.*" %>
<%@ page import="com.tsp.operation.model.beans.*" %>
<%@ include file="/WEB-INF/InitModel.jsp" %>
<html>
<head>
  <title>Vista por Perfil</title>
  <link rel='stylesheet' href='<%= BASEURL %>/css/estilostsp.css'>
  <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body>
<table width="80%"  border="2" align="center">
  <tr>
    <td><TABLE width='100%' cellpadding='0' cellspacing='0' class="tablaInferior">
 <TR class="barratitulo"><TH class='titulo' colspan='2' height='30'><table width="100%"  border="0">
   <tr>
     <td width="30%" class="subtitulo1">VISTA POR PROYECTOS</td>
     <td width="70%"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
   </tr>
 </table>
   </TH>
 </TR>
 <%  List perUser  = model.ProyectoSvc.searchProyectosUsuarios();
     if(perUser!=null){
        Iterator itPer = perUser.iterator();
        String   ant   = "";
        while(itPer.hasNext()){
           PerfilUsuario perfil = (PerfilUsuario) itPer.next();%>
           <TR VALIGN='top'>
              <TD class="fila"><%=(ant.equals(perfil.getPerfil()))?"":"&nbsp"+perfil.getPerName()%></TD>
              <TD class="letra"><%=perfil.getUserName()%></TD>
           </TR>           
        <% ant = perfil.getPerfil();
        } 
      }else{%><TR class="fila">
          <TD colspan='2' align='center' height='40' width='100%'>No hay Proyectos guardados....</TD></TR><%}%>
  </TABLE></td>
  </tr>
</table>

 
  
<div align="center"></div>
</body>
</html>