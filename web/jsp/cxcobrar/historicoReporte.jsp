<%-- 
    Document   : historicoReporteDatacredito
    Created on : 8/08/2014, 11:02:03 AM
    Author     : desarrollo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Reportados Centrales de Riesgo</title>
        <link href="/fintra/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/default.css" rel="stylesheet" type="text/css">
        <script src="<%=BASEURL%>/js/window.js" type="text/javascript"></script>

        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>        
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>            
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/reciboOficial.js"></script>
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/buttons/botonVerde.css"/>
    
        
        <script>
             $(document).ready(function(){
                $("#divSalida").dialog({
                    autoOpen: false,
                    height: "auto",
                    width: "300px",
                    modal: true,
                    autoResize: true,
                    resizable: false,
                    position: "center",
                    closeOnEscape: false


                });

            });
        </script>    
    </head>
    <body>
        <input name="BASEURL" type="hidden" id="BASEURL" value="<%=BASEURL%>">
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Reportados Centrales de Riesgo"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:83%; z-index:0; left: 0px; top: 100px; overflow:auto;">
            <div id="contenido" style="visibility: hidden; display: none; height: 100px; width: 400px; background-color: #EEEEEE;">
                <table id="tablaDet"></table>
                <div id="pageDet"></div>
            </div>
                </br>
            <div align="center">
                <div id="filtros" style="display:table-cell; " class="ui-jqgrid ui-widget ui-widget-content ui-corner-all">
                    </br>
                    <table>
                        <tr>
                            <td style=" width: 90px"><span> Unidad de negocio </span></td>
                            <td><select id="unidNeg" name="unidNeg"></select></td>
                            <td><span> Periodo </span></td>
                            <td><select id="periodoData" name="periodoData">
                                    <option id="vacio"></option>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td><span> No. Cedula </span></td>
                            <td><input type="text" id="cedula" name="cedula"/></td>
                            <td><span> No. Negocio </span></td>
                            <td><input type="text" id="negocio" name="negocio"/></td>
                        </tr>
                        <tr>

                        </tr> 
                    </table>
                    </br></br>
                    <div aling="center">
                        <table>
                            <tr>
                                <td width="70px"><span  class="form-submit-button form-submit-button-simple_green_apple" onClick="cargarHistoricoReportes();"> Buscar </span></td>
                                <td style=" width: 10px" colspan="2" ></td>        
                                <td width="70px"><span  class="form-submit-button form-submit-button-simple_green_apple" onClick="ExportDataToExcel();"> Exportar </span></td>
                            </tr>
                        </table>
                         </br>
                    </div>
                 </div>             
                    </br>
                    <table id="Historico"></table>
                    <div id="page"></div>
                    <div id="divSalida" title="Exportacion" style=" display: block" >
                        <div id="resp"></div>
                    </div>

                </div>
               </div>             
                </body>
</html>
