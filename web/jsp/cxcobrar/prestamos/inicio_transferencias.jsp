<!--
- Autor : TMolina	
- Date  : 04 septiembre 2008
- Copyrigth Notice : Fintravalores S.A.
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la busqueda de las condicones de los prestamos.
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
	<title>Transferencias Bancarias</title>
	<%	
	  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
	%>
	<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
	<script type='text/javascript' src="<%= BASEURL %>/js/transferencias_pr.js"></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body >
	<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
		<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Transferencias Bancarias De Prestamos"/>
	</div>
	<%String list=(String)request.getParameter("list");
	%>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
	<form name="form2" method="post" action="<%=CONTROLLER%>?estado=Prestamo&accion=Transferencia&evento=3" id="form2" >
		<table width="510" border="2" align="center">
		  <tr>
			<td><table width="100%" align="center"  class="tablaInferior">
				<tr>
					<td width="205" class="subtitulo1">Consultar Prestamos</td>
					<td width="205" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
				</tr>
				<tr class="fila">
				  <td width="205" align="left" >Escoja el banco girador:</td>
				  <td width="205">
				  <%
				  Vector vb=new Vector();
				  model.Negociossvc.listb("todos");
				  vb=model.Negociossvc.getListNeg();
				  %>
					<select name="bco" id="bco" onChange="llamarasincrono('<%=CONTROLLER%>?estado=Prestamo&accion=Transferencia&evento=1&val='+bco.value+'','tabla')">
						<option value="...">Seleccione</option>
						<%for(int j=0;j<vb.size();j++){
							Negocios  neg = (Negocios)vb.get(j);%>
							<option value="<%=neg.getNom_cli()%>,<%=neg.getCod_cli()%>,<%=neg.getCod_tabla()%>"><%=neg.getEstado()%></option>
						<%}%>
				  </select></td>
				</tr>
			</table></td>
		  </tr>
		</table>
		<p>
		<div id="tabla"></div>
		<div id="imgworking" align="center" style="visibility:hidden"><img src="<%=BASEURL%>/images/cargando.gif"></div>
	<div align="center"><img src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand" title="Iniciar Transferencia" name="buscar"  onClick="send(form2)" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
	<img src="<%=BASEURL%>/images/botones/salir.gif"  name="regresar" style="cursor:hand" title="Salir al Menu Principal" onClick="javascript:window.close()" onMouseOver="botonOver(this);"   onMouseOut="botonOut(this);"  ></div>		
	</form>
	</div>
	</body>
</html>
