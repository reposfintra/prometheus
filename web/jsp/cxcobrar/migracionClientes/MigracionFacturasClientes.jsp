<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
 String Mensaje = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
    
 String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
 String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
  <title></title>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>
  <script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
  <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
  <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">

</head>
<script>
function goo(){
     document.form1.action = "<%= CONTROLLER %>?estado=MigracionFacturas&accion=Clientes"; 
     document.form1.submit();   
}
 function formato (fecha){
			return fecha.getYear() + '-' + formato2digitos(fecha.getMonth()+1) + '-' + formato2digitos(fecha.getDate());
		}
function cargar(){
  			var fechaActual  =  new Date();
  			form1.fechaInicio.value = formato(fechaActual);
			form1.fechaFinal.value = formato(fechaActual);
		}
</script>

<body onload="cargar()">
  <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
    <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Facturas Clientes"/>
  </div>

  <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
    <center>
    <form name="form1" method="post" >
      <table width="30%" border="2">
  <tr>
    <td><table width="100%" border="0">
      <tr>
        <td width="62%" class="subtitulo1">Migracion Facturas Clientes </td>
        <td width="38%" class="barratitulo"><%=datos[0]%><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"align="left"></td>
      </tr>
    </table>
    <table width="100%">

        <tr class="subtitulo1">
          <th colspan="3"  scope="row"><div align="left">Filtro </div></th>
        </tr>
        <tr class="fila">
          <td class='letrafila' width='143'> Fecha Inicial:</td>
          <td>
            <input name="fechaInicio" type="text"  size='10' class="textbox" id="fechaInicio" readonly="">
            <span class="Letras"> <img src="<%=BASEURL%>/images/cal.gif" 
              width="16" height="16" align="absmiddle" style="cursor:hand " 
              onClick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fechaInicio);return false;" hidefocus></span> </td>
        </tr>
        <tr class="fila">
          <td width="143" class='letrafila'>Fecha Final :</td>
          <td  >
            <input name="fechaFinal" type="text" class="textbox" size='10' id="fechaFinal" readonly="">
            <span class="Letras"><a href="javascript:void(0)" onClick="jscript: show_calendar('fechaFinal');" hidefocus></a> <img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " 
              onClick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fechaFinal);return false;" hidefocus></span> </td>
        </tr>
        <!-- Campos nuevos -->
 
      </table></td>
  </tr>
</table>

&nbsp;    <img src='<%=BASEURL%>/images/botones/buscar.gif'  style='cursor:hand' 
    onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" 
    onClick="goo();" align="center">
&nbsp;    <img src='<%=BASEURL%>/images/botones/salir.gif'   style='cursor:hand' 
    onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" 
    onclick='window.close();' align="center"> 
       
 <p>
  <%if(!Mensaje.equals("")){
  %>
</p>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%= Mensaje %></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>	   
    </form>
	</p>
 <%}%>
    </center>
  </div>
 
  <iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
<%=datos[1]%>
</body>
</html>
