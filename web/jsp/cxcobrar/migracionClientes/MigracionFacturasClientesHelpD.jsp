<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Descripcion de campos ingresar despacho</title>


<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<% String BASEIMG = BASEURL +"/images/botones/"; %> 
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Control de migraci&oacute;n de Facturas Clientes </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">PROCESOS</td>
        </tr>
        <tr>
          <td  class="fila">Filtro</td>
          <td  class="ayudaHtmlTexto">Es un filtro de fechas para analizar facturas clientes del periodo selecionado. </td>
        </tr>
        <tr>
          <td  class="fila">Fecha 1 </td>
          <td  class="ayudaHtmlTexto">Fecha del Rango inicial </td>
        </tr>
        <tr>
          <td  class="fila">Fecha 2        </td>
          <td  class="ayudaHtmlTexto">Fecha del Rango final </td>
        </tr>
        <tr>
          <td class="fila"> Boton Buscar <img src="<%=BASEURL%>/images/botones/buscar.gif"></td>
          <td  class="ayudaHtmlTexto">Boton para la busqueda</td>
        </tr>
        <tr>
          <td width="556" class="fila">Boton Salir <img src="<%=BASEIMG%>salir.gif"></td>
          <td width="556"  class="ayudaHtmlTexto">Boton para salir de la vista </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
