<%-- 
    Document   : previsualizarReportados
    Created on : 14/08/2014, 05:42:01 PM
    Author     : desarrollo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
        <title>Previsualizacion Reportados Datacredito</title>
        <link href="/fintra/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/default.css" rel="stylesheet" type="text/css">
        <!--script src="<%=BASEURL%>/js/window.js" type="text/javascript"></script-->
        <script src="<%=BASEURL%>/js/boton.js" type="text/javascript"></script>
        <link href="<%= BASEURL%>/css/jquery/jquery-ui/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>        
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>            
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/reciboOficial.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>
        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/jscal2.css" />
        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/border-radius.css" />
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/buttons/botonRose.css"/>
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/buttons/botonVerde.css"/>
        
        
        <script>
             $(document).ready(function(){
                $("#divSalida").dialog({
                    autoOpen: false,
                    height: "auto",
                    width: "300px",
                    modal: true,
                    autoResize: true,
                    resizable: false,
                    position: "center",
                    closeOnEscape: false


                });
                 $("#botonAceptar").hide();
        

            });
            
            
        function validNumericos(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode 
            
            if (((charCode == 8) || (charCode == 46) || (charCode == 109) || (charCode == 173) 
            || (charCode >= 35 && charCode <= 40)  
                || (charCode >= 48 && charCode <= 57)  
                || (charCode >= 96 && charCode <= 105))) {  
                return true;  
            }  
            else {  
                return false;  
            }  
        } 
        
        </script>  
        <style>

            #div_espera {
                background:url('./css/alert/bigrotation.gif') no-repeat center;
                background-color:#EDEEF8;
                position: absolute;
                left: 55%;
                top: 55%;
                width: 100px;
                height: 80px;
                margin-top: -100px;
                margin-left: -150px;
                overflow: auto;
                border:1px solid #D4D4D4;
                text-align:center;
                z-index:100;
            }

        </style>
    </head>
    <body>
        <input name="BASEURL" type="hidden" id="BASEURL" value="<%=BASEURL%>">
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Previsualizacion Reportados Datacredito"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:83%; z-index:0; left: 0px; top: 100px; overflow:auto;">
            <div id="contenido" style="visibility: hidden; display: none; height: 100px; width: 400px; background-color: #EEEEEE;">
                <table id="tablaDet"></table>
                <div id="pageDet"></div>
            </div>
                </br>
            <div align="center">
                <div id="filtros" style="display:table-cell; " class="ui-jqgrid ui-widget ui-widget-content ui-corner-all">
                    </br>
                    <table style=" width: 1000px">
                        <tr>
                            <td style=" width: 150px;  padding-left: 5px"><span> Unidad de negocio </span></td>
                            <td><select id="unidNeg" name="unidNeg"></select></td>
                            <td><span> Periodo Foto</span></td>
                            <td><select id="periodoData" name="periodoData">
                                    <option id="vacio"></option>
                                </select>
                            </td>
                            <td>
                                <label>Fecha de Corte</label> 
                            </td>
                            <td>
                                <input name="fecha" type='text' class="textbox" id="fecha" style='width:80px'  readonly value="">
                                <img src="<%=BASEURL%>/images/cal.gif" id="imgFecha" />
                                <script type="text/javascript">
                                    Calendar.setup({
                                        inputField: "fecha",
                                        trigger: "imgFecha",
                                        align: "top",
                                        onSelect: function() {
                                            this.hide();
                                        }
                                    });
                                </script>                        

                            </td>
                        </tr>
                        <tr>
                            <td style=" width: 60px; padding-left: 5px"><span> Dias Mora Inicial </span></td>
                            <td><input type="text" class="textbox" id="rangoIni" name="rangoIni" onkeydown="return validNumericos(event)" /></td>
                            <td><span> Dias Mora Final </span></td>
                            <td><input type="text" class="textbox" id="rangoFin" name="rangoFin" onkeydown="return validNumericos(event)" /></td>
                        </tr>
                        <tr>

                        </tr>
                    </table>
                     </br></br>               
                     
                        <table style=" width: 750px">        
                            <tr>
                                <td width="70px"><span  class="form-submit-button form-submit-button-simple_green_apple" onClick="cargarPrevisualizacionReportados()"> Buscar </span></td>
                                <td width="70px"><span  class="form-submit-button form-submit-button-simple_green_apple" onClick="exportarExcelPrevisualizacion()"> Exportar </span></td>
                                <td width="95px"><span  class="form-submit-button form-submit-button-simple_green_apple" onClick="guardarReportados()" >Incluir a Reporte </span></td>
                                <td width="90px"><span  class="form-submit-button form-submit-button-simple_green_apple" onClick="limpiarReporteDatacredito()" >Limpiar Reporte </span></td>
                                <td width="95px"><span  class="form-submit-button form-submit-button-simple_green_apple" onClick="generarReporteDatacredito()" >Generar Reporte </span></td>
                            </tr>
                        </table>
                     
                    </br>
                    </div>  
                    </br> 
                    <table id="PrevisualizarReporte"></table>
                    </br>
                    <div id="page"></div>
                    <div id="divSalida" title="Exportacion" style=" display: block" >
                        <center><img id="imgload" style="position:relative;  top: 20px; display: none" src="<%=BASEURL%>/images/cargandoCM.gif"/></center>
                        <div id="resp" style=" display: block"></div>
                    </div>
                    <div id="div_espera" style="display:none;z-index:1000; position:absolute"></div>

                </div>
                </body>
</html>
