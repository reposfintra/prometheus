<%-- 
    Document   : EndosoFactura
    Created on : 29/02/2016, 10:26:51 AM
    Author     : egonzalez
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/EndosoFactura.js"></script> 
        <title>Administracion Endoso</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Administracion Endoso"/>
        </div>
    <center>        
        <!-- tabla de maestro de endoso de facturas-->

        <div style="position: relative;top:150px;">
            <table id="tabla_endosoFactura" ></table>
            <div id="pager"></div>
        </div>

        <!--guardado y edicion -->

        <div id="dialogMsjFacuraendoso"  class="ventana" >
            <div id="tablainterna" style="width: 365px" >
                <table id="tablainterna"  >
                    <tr>
                        <td>
                            <input type="text" id="id" hidden />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Nombre de proceso</label>
                        </td>
                        <td>
                            <input type="text" id="nomproces" onchange="conMayusculas(this)" style="width: 200px"  />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Custodiada por:</label>
                        </td>
                        <td>
                            <select id="custodiadap" style="width: 125px" onchange="Custodiador()" >
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Custodiador</label>
                        </td>
                        <td>
                            <select id="custodiador" style="width: 125px" >
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Cuenta</label>
                        </td>
                        <td>
                            <input type="text" id="cuenta" class="solo-numero" style="width: 120px"  />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>CMC</label>
                        </td>
                        <td>
                            <select id="cmc" style="width: 125px" >
                            </select>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>
    </center>
</body>
<script>
    $(document).ready(function () {
        $('.solo-numero').keyup(function () {
            this.value = (this.value + '').replace(/[^0-9]/g, '');
        });
    });
</script>
</html>
