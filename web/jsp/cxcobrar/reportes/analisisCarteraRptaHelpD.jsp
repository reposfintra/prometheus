<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Reporte de Cartera</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
</head>

<body>
<br>


<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Reporte de Cartera</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Reporte de Cartera </td>
        </tr>
        <tr>
          <td class="fila">Cliente</td>
          <td  class="ayudaHtmlTexto">Campo donde se selecciona el cliente del cual se desea generar el reporte.</td>
        </tr>
        <tr>
          <td class="fila">Agencia de Facturaci&oacute;n</td>
          <td  class="ayudaHtmlTexto">Campo donde se selecciona la agencia de facturaci&oacute;n de la cual se desea generar el reporte.</td>
        </tr>
        <tr>
          <td class="fila">Fecha</td>
          <td  class="ayudaHtmlTexto">Campo donde se selecciona la fecha de referencia para realizar el an&aacute;lisis del reporte de cartera. </td>
        </tr>
        <tr>
          <td width="149" class="fila"><div align="center">
            <input type="image" src = "<%= BASEURL%>/images/botones/aceptar.gif" style="cursor:default ">
          </div></td>
          <td width="525"  class="ayudaHtmlTexto">Bot&oacute;n que inicia la generaci&oacute;n del reporte.</td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
            <input type="image" src = "<%= BASEURL%>/images/botones/cancelar.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto"> Bot&oacute;n que resetea los campos del formulario. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
            <input type="image" src = "<%=BASEURL%>/images/botones/salir.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que cierra el programa. </td>
        </tr>
        
      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
