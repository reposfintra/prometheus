<!--
- Autor : Ing. Fily Fernandez
- Creation Date  : Enero de 2007
- Modified: Ing. Enrique De Lavalle
- Modified Date: 13 de Marzo 2007
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de mostrar el reporte de pagos de un cliente
--%>
<%-- Declaracion de librerias--%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.util.*"%>
<%@page contentType="text/html"%>
<%@page import="java.util.*"%> 
<%@page import="com.tsp.operation.model.*"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
    <head>        
        <title>Reporte Realizados Por El Cliente </title>
		<script src='<%=BASEURL%>/js/date-picker.js'></script>
		<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
		<script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script> 
		<link href="file:///C|/sltequipo%20viejo/slt1/jsp/sot/css/estilostsp.css" rel="stylesheet" type="text/css">
		<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
		 <script>
		 	function Exportar (){
				
			    alert( 
				
					'Puede observar el seguimiento del proceso en la siguiente ruta del menu:' + '\n' + '\n' +
					'SLT -> PROCESOS -> Seguimiento de Procesos' + '\n' + '\n' + '\n' +
					'Y puede observar el archivo excel generado, en la siguiente parte:' + '\n' + '\n' +
					'En la pantalla de bienvenida de la Pagina WEB,' + '\n' +
					'vaya a la parte superior-derecha,' + '\n' +
					'y presione un click en el icono que le aparece.'
					
				);
				
			}
			
		</script>
    </head>
   <body onResize="redimensionar()" onLoad="redimensionar()">
   <%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    
	
	String fecI=(request.getParameter ("fecI")!=null)?request.getParameter ("fecI"):"";
	String fecF =(request.getParameter ("fecF")!=null)?request.getParameter ("fecF"):"";
	String codcli =(request.getParameter ("codcli")!=null)?request.getParameter ("codcli"):"";	
		
		 
	%>
	


	<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
	 <jsp:include page="/toptsp.jsp?encabezado=Reporte de pagos Clientes"/>
	</div>
	<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
	<%String msg = request.getParameter("msg");
		if ( msg!=null){%>	
 <br>
  <table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="300" align="center" class="mensajes">La busqueda no arrojo resultados</td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
	  
    </table></td>
  </tr>
</table>
      <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/cxcobrar/reportes/ReportePClientes.jsp'"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
	  <input name="salir2" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
   
 <%}else{
       UtilFinanzas u = new UtilFinanzas();
        Vector datosC =model.facturaService.getVector();
		String style = "simple";
		String position =  "bottom";
		String index =  "center";
		int maxPageItems = 20;
		int maxIndexPages = 10;
		if( datos != null && datosC.size() > 0 ){
			factura fac = (factura) datosC.elementAt(0);
			String nomcli = fac.getCodcli();
		%>		
   <form name="form1" method="post" action="<%=CONTROLLER%>?estado=ReportePagos&accion=Cliente&op=2">
   
    <input name="FechaI" type="hidden" id="FechaI" value="<%=fecI%>">
	<input name="FechaF" type="hidden" id="FechaF" value="<%=fecF%>">
	<input name='codcli' type='hidden' class="textbox" id="codcli" size="10" maxlength="10" value="<%=codcli%>">
          <p>
                <input name="Guardar2" src="<%=BASEURL%>/images/botones/exportarExcel.gif" type="image" class="boton" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onclick="Exportar();">
                <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/cxcobrar/reportes/ReportePClientes.jsp'"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
 	            <input name="salir" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
          </p>
   </form>
  <table  width="100%">  
           <tr>
				<td class="barratitulo" colspan='2' >
					   <table cellpadding='0' cellspacing='0' width='100%'>
							 <tr class="fila">
							  <td width="45%" align="left" class="subtitulo1">&nbsp;<strong>Consulta de pagos Realizados por <%="         "+nomcli+"   "+codcli%> </strong></td>
								 <td width="13%" class="subtitulo1"  align="left">Fecha: <%=com.tsp.util.Util.getFechaActual_String(4)%></td>
								
							     <td width="42%" class="fila"  align="left"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"  align="left"><%=datos[0]%></td>
						    </tr>
						  <tr  class="letra">
								   <td width='45%' class="fila" ><div align="left"><strong> Desde   <%=fecI%>  hasta   <%= fecF%>  </strong></div></td>
							   <td colspan="2" class="fila" ><div align="left"></div></td>
						  </tr>
						</table>   
			</td>
        </tr>
          <tr>
            <td>
              <table width="100%"  border="1" bordercolor="#999999" bgcolor="#F7F5F4" >
                <tr class="tblTitulo" align="center">
				  <%--1--%><td nowrap width="5%" align="center">ITEM</td>
				  <%--2--%><td nowrap width="15%" align="center">No FACTURA </td>
				  <%--3--%><td nowrap width="15%" align="center">FECHA DE FACTURA </td>
				  <%--4--%><td nowrap width="10%" align="center">FECHA DE VENCIMIENTO </td>
				  <%--9--%><td nowrap width="5%" align="center">DIF DIAS</td>				 
				  <%--5--%><td nowrap width="10%" align="center">VALOR DE FACTURA </td>
                  <%--6--%><td nowrap width="10%" align="center">VALOR CANCELADO</td>				  
                  <%--7--%><td nowrap width="10%" align="center">SALDO PENDIENTE</td>
				  <%--8--%><td nowrap width="10%" align="center">FECHA DE PAGO </td>
				  <%--10--%><td nowrap width="10%" align="center">N� INGRESO</td>
				  <%--11--%><td nowrap width="10%" align="center">VALOR INGRESO</td>
   				  <%--12--%><td nowrap width="10%" align="center">FECHA INGRESO</td>				  
				  <%--13--%><td nowrap width="10%" align="center">NC. APLICADAS </td>				  
				  <%--14--%><td nowrap width="10%" align="center">VALOR NC </td>
				  <%--15--%><td nowrap width="5%" align="center">MONEDA </td>
		   				 
                </tr>	
				<pg:pager
				 items="<%=datosC.size()%>"
				 index="<%= index %>"
				 maxPageItems="<%= maxPageItems %>"
				 maxIndexPages="<%= maxIndexPages %>"
				 isOffset="<%= true %>"
				 export="offset,currentPageNumber=pageNumber"
				 scope="request">					
				 
				 <pg:param name="fecI" value="<%= fecI %>"/>
				 <pg:param name="fecF" value="<%= fecF %>"/>				 
		<%
			int cont = 1;
			for (int i = offset.intValue(), l = Math.min(i + maxPageItems, datosC.size()); i < l; i++){
				factura info = (factura) datosC.elementAt(i);
	  %>  	  
	  			
	  			<pg:item>
                  <tr class="<%=i%2==0?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" title="Click" align="center" > 
				  <%--1--%><td nowrap align="center" class="bordereporte"><%=cont%>&nbsp;</td>
				  <%--2--%><td nowrap align="center" class="bordereporte"><%=(info.getDocumento()!=null)?info.getDocumento():""%>&nbsp;</td>
				  <%--3--%><td nowrap align="center" class="bordereporte"><%=(info.getFecha_factura()!=null)?info.getFecha_factura():""%>&nbsp;</td>
				  <% if (!info.getFecha_vencimiento().equals("099-01-01")){ %>
				  <%--4--%><td nowrap align="center" class="bordereporte"><%=(info.getFecha_vencimiento()!=null)?info.getFecha_vencimiento():""%>&nbsp;</td>          
				  <%}else{%>
				  <%--4--%><td nowrap align="center" class="bordereporte">&nbsp;</td>
				  <%}%>
				  <%--5--%><td nowrap align="center" class="bordereporte"><%=(info.getPlazo()!=null)?info.getPlazo():""%>&nbsp;</td>
				  <% if (!info.getDocumento().equals("")){%>          
				  <%--6--%><td nowrap align="center" class="bordereporte"><%=u.customFormat(info.getValor_factura())%>&nbsp;</td>				  
                  <%--7--%><td nowrap align="center" class="bordereporte"><%=u.customFormat(info.getValor_abonome())%>&nbsp;</td>
				  <%--8--%><td nowrap align="center" class="bordereporte"><%=u.customFormat(info.getValor_saldome())%>&nbsp;</td>
				  <%}else{%>
				  <%--6--%><td nowrap align="center" class="bordereporte">&nbsp;</td>				  
                  <%--7--%><td nowrap align="center" class="bordereporte">&nbsp;</td>
				  <%--8--%><td nowrap align="center" class="bordereporte">&nbsp;</td>
				  <%}%>				  
				  <%--9--%><td nowrap align="center" class="bordereporte"><%=(info.getFecha_ultimo_pago()!=null)?info.getFecha_ultimo_pago():""%>&nbsp;</td>                  
		  		  <%--10--%><td nowrap align="center" class="bordereporte"><%=(info.getNum_ingreso()!=null)?info.getNum_ingreso():""%>&nbsp;</td>
				  <%--11--%><td nowrap align="center" class="bordereporte"><%=u.customFormat(info.getValor_abono())%>&nbsp;</td>
				  <%--12--%><td nowrap align="center" class="bordereporte"><%=(info.getFecha_ingreso()!=null)?info.getFecha_ingreso():""%>&nbsp;</td>				  				  				  
				  <%--13--%><td nowrap align="center" class="bordereporte"><%=(info.getTipo_documento()!=null)?info.getTipo_documento():""%>&nbsp;</td>                  
			  	 <%if(info.getValor_saldo()==0){%>
				 	 <%--14--%><td nowrap align="center" class="bordereporte">&nbsp;</td>					
				 <%}else{%>
				 	<%--14--%><td nowrap align="center" class="bordereporte"><%=u.customFormat(info.getValor_saldo())%>&nbsp;</td>				 	
				 <%}%>	 
				  <%--15--%><td nowrap align="center" class="bordereporte"><%=(info.getMoneda()!=null)?info.getMoneda():""%>&nbsp;</td>                
                  
                </tr>
			  </pg:item>	
		<%cont = cont + 1; 
		}%>
		<tr class="bordereporte">
          <td height="100%" colspan="15" nowrap >
		   <pg:index>
            <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
           </pg:index> 
	      </td>
        </tr>
        </pg:pager>
            </table>
      </table>
			</table>
		   <form name="form2" method="post" action="<%=CONTROLLER%>?estado=ReportePagos&accion=Cliente&op=2">
		        <p>
             <input name="FechaI" type="hidden" id="FechaI" value="<%=fecI%>">
	         <input name="FechaF" type="hidden" id="FechaF" value="<%=fecF%>">
			 <input name='codcli' type='hidden' class="textbox" id="codcli" size="10" maxlength="10" value="<%=codcli%>">
    
	  		<input name="Guardar" src="<%=BASEURL%>/images/botones/exportarExcel.gif" type="image" class="boton" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onclick="Exportar();" style="cursor:hand" onClick="form1.submit();">
      		<img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/cxcobrar/reportes/ReportePClientes.jsp'"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
	  		<input name="salir2" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
	  </p>
    </form>	
	  <%}%>	  
	  
 <%}%>
	  
</body>
</div>
<%=datos[1]%>  
</html>
