<!--
- Autor : Ing. Andr�s Maturana De La Cruz
- Date  : .05.2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la depuraci�n masiva de tramos.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Reporte de Cartera</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src='<%=BASEURL%>/js/tools.js'></script>
<style type="text/css">
    .lista {
        background-color: white;
        width: 80%;
        border: #000 solid 1px;
    }
</style>
<script type='text/javascript' src="<%= BASEURL %>/js/prototype.js"></script>
<script type='text/javascript'>
function validarForma(){
	for (i = 0; i < forma.elements.length; i++){
            if (forma.elements[i].value == "" && forma.elements[i].name!="text" && forma.elements[i].name!="clientes"&& forma.elements[i].name!="proveedor"
					&& forma.elements[i].name!="agencia" && forma.elements[i].name!="agencia_d" && forma.elements[i].name!="agencia_c"
                                        && forma.elements[i].name!="cccli" && forma.elements[i].name!="vence" && forma.elements[i].name!="lim1" && forma.elements[i].name!="lim2" && forma.elements[i].name!="codigocliente" 
                                        && forma.elements[i].name!="unidades" && forma.elements[i].name!="convenios"){
                    forma.elements[i].focus();
                    alert('No se puede procesar la informaci�n. Campo Vac�o.');
                    return false;
            }

			if (forma.lim1.value  > forma.lim2.value)
			{
			   forma.elements[i].focus();
			   alert('El valor inicial no puede ser mayor que el final');
               return false;
			}
			if ((forma.lim1.value==""  && forma.lim2.value!="")||(forma.lim1.value!=""  && forma.lim2.value==""))
			{
			   forma.elements[i].focus();
			   alert('No pueden quedar en blanco');
               return false;
			}

    }
    return true;
}

    var idselect = 'clientes';
    function searchData(valor,selection){
        var url = '<%=CONTROLLER%>?estado=Analisis&accion=VencCXC';
        var p = 'opcion=buscardatos&dato='+valor+'&filtro=';
        if(selection==1){
            idselect = 'clientes';
            p+='cliente';
        }
        else{
            idselect = 'proveedor';
            p+='proveedor';
        }
        new Ajax.Request(
            url,
            {
                method: 'post',
                parameters:p,
                onLoading:loading,
                onComplete: completo
            }
        );
    }

    function loading(){
        //$(idselect).innerHTML = "<option value=''>Procesando</option>";
        $(idselect).innerHTML = '<span class="letra">Procesando </span><img alt="cargando" src="<%= BASEURL%>/images/cargando.gif" name="imgload">';
    }

    function completo(response){
        //$(idselect).innerHTML = response.responseText;
        var texto = response.responseText;
            $(idselect).innerHTML = "<select name='"+idselect+"'>"+texto+"</select>";
    }

    function checkEnter(e,selectx,valor){
        var mykey;
        e = e || window.event; //evento_navegador_bueno||internet_exploiter
        if(e.which) mykey = e.which;// si es mozilla/webkit
        else mykey = e.keyCode;// si es internet exploiter
        if(mykey == 13 ){
            searchData(valor,selectx);
        }
    }
    function cargarLista(tipo) {
        var url = '<%=CONTROLLER%>?estado=Analisis&accion=VencCXC';
        var p = '';
        if (tipo === 'convenios') {
            var filtro = '';
            document.getElementById('convenios').innerHTML = '';
            var select = document.getElementById('unidades');

            for(var x = 0; x < select.options.length;++x) {
                if (select.options[x].selected) {
                    filtro += ','+ select.options[x].value;
                }
            }
            url += '&opcion=convenios&filtro='+filtro.substring(1,filtro.length);
            
        } else {
            url += '&opcion=unidades&filtro=';
        }
        new Ajax.Request(
        url,
        {
            type: 'get',
            datatype: 'json',
            parameters:p,
            onComplete: function (json) {
                var ele; 
                for (var x in json.responseJSON) {
                    ele = document.createElement('option');
                    ele.value = json.responseJSON[x].id;
                    ele.name = json.responseJSON[x].padre;
                    ele.innerHTML = json.responseJSON[x].nombre;
                    document.getElementById(tipo).appendChild(ele);
                }
            }
        });
    }
    
</script>
<body onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:110px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Reporte de Cartera"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 109px; overflow: scroll;">
<%
	TreeMap pr = new TreeMap();//model.clienteService.getTreemap();
	pr.put("      Todos","");
	TreeMap agcs = model.ciudadService.getCiudadTM();
	agcs.put("     Todos","");
	Calendar FechaHoy = Calendar.getInstance();
	Date d = FechaHoy.getTime();
	SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
	TreeMap pro= new TreeMap();//model.Negociossvc.getProv();
	pro.put("     Todos","");

        TreeMap prefijoscxcs=model.facturaService.getPrefijosCxcs();//20100628
	prefijoscxcs.put("     Todos","...");//20100628
        TreeMap hcs= model.facturaService.getHcs();//20100628new TreeMap();//
	hcs.put("     Todos","...");//20100628

%>
<form name="forma" action='<%=CONTROLLER%>?estado=Analisis&accion=VencCXC' id="forma" method="post">
  <table width="596"  border="2" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td colspan="2" ><table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
              <tr>
                <td width="56%" class="subtitulo1">&nbsp;Detalles</td>
                <td width="44%" class="barratitulo">
                    <img alt="" align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%>
                </td>
              </tr>
          </table></td>
        </tr>
        <tr class="fila">
          <td>Cliente</td>
          <td nowrap><table width="100%"  border="0" class="tablaInferior">
              <tr>
                <td>
                    <!-- <input name="text" type="text" class="textbox" id="campo" style="width:200;" onKeyUp="buscar(document.forma.clientes,this)" size="15" > -->
                    <!--<input name="text" type="text" class="textbox" id="campo" style="width:200px;" onkeypress="checkEnter(event,1,this.value);" size="15" >-->
                    <input name="text" type="text" class="textbox" id="campo1" style="width:200px;" onchange="searchData(this.value,1);" onkeypress="checkEnter(event,1,this.value);" size="15" >
                </td>
              </tr>
              <tr>
                <td>
                    <div id="clientes">
                        <select name="clientes">
                            <option value="">Todos</option>
                        </select>
                    </div>
                </td>
              </tr>
          </table></td>
        </tr>
		 <tr class="fila">
          <td>Afiliado</td>
          <td nowrap><table width="100%"  border="0" class="tablaInferior">
              <tr>
                <td>
                    <!-- <input name="text" type="text" class="textbox" id="campo" style="width:200;"  onKeyUp="buscar(document.forma.proveedor,this)" size="15" > -->
                    <!--input name="text" type="text" class="textbox" id="campo" style="width:200px;" onkeypress="checkEnter(event,2,this.value);" size="15" >-->
                    <input name="text" type="text" class="textbox" id="campo2" style="width:200px;" onchange="searchData(this.value,2);" onkeypress="checkEnter(event,2,this.value);" size="15" >
                </td>
              </tr>
              <tr>
                <td>
                    <div id="proveedor">
                        <select name="proveedor">
                            <option value="">Todos</option>
                        </select>
                    </div>
                </td>
              </tr>
          </table></td>
        </tr>
        <tr class="fila">
            <td>
                Unidad de Negocio
            </td>
            <td>
                <select id="unidades" name="unidades" class="lista" multiple=""
                        onblur="cargarLista('convenios')" size="5">
                </select>
            </td>
        </tr>
        <tr class="fila">
            <td>Convenios</td>
            <td>
                <select id="convenios" name="convenios" class="lista" multiple="" size="5"></select>
            </td>
        </tr>
        <script type="text/javascript"> cargarLista('unidades');</script>
        <tr class="fila">
          <td valign="middle">CC Cliente</td>
          <td nowrap><input type="text" name="cccli"></td>
        </tr>

        <tr class="fila">
          <td valign="middle">Cod Cliente</td>
          <td nowrap><input type="text" name="codigocliente"></td>
        </tr>

		<tr class="fila">
          <td valign="middle">Agencia de Facturaci&oacute;n</td>
          <td nowrap><input:select name="agencia" attributesText="class=textbox" options="<%=agcs %>"/></td>
        </tr>

        <tr class="fila">
          <td valign="middle">Agencia Due&ntilde;a </td>
          <td nowrap><input:select name="agencia_d" attributesText="class=textbox" options="<%=agcs %>"/></td>
        </tr>
        <tr class="fila">
          <td valign="middle">Agencia de Cobro </td>
          <td nowrap><input:select name="agencia_c" attributesText="class=textbox" options="<%=agcs %>"/></td>
        </tr>
        <tr class="fila">
          <td width="27%" valign="middle">Fecha</td>
          <td width="73%" nowrap><input name="fecha" type='text' class="textbox" id="fecha" style='width:120px' value='<%= f.format(d) %>' readonly>
            <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma.fecha);return false;">
                <img src="<%=BASEURL%>/js/Calendario/cal.gif" width="16" height="16" border="0" alt="De click aqu&iacute; para escoger la fecha"></a></td>
        </tr>

		<tr class="fila">
          <td width="27%" valign="middle">Vencimiento</td>
          <td width="73%" nowrap>
		  <select name="vence">
		  	<option value="0">Todas</option>
			<option value="1">Vencida</option>
			<option value="2">Vigente</option>
          </select></td>
        </tr>


		<tr class="fila">
          <td width="27%" valign="middle">Prefijo</td>
          <td width="73%" nowrap>
              <input:select name="tfac" attributesText="class=textbox" options="<%=prefijoscxcs %>"/>&nbsp;<!--20100628-->
              <!--
		  <select name="tfac">
		    <option value="...">Todas</option>
		  	<option value="FC">FC</option>
			<option value="FL">FL</option>
			<option value="FF">FF</option>
			<option value="ND">ND</option>
			<option value="R">R</option>
			<option value="G">G</option>
			<option value="PC">PC</option>
			<option value="PL">PL</option>
			<option value="PF">PF</option>
			<option value="NM">NM</option>
			<option value="DF">DF</option>
			<option value="FG">FG</option>
			<option value="PG">PG</option>
			<option value="FM">FM</option>
          </select>--></td>
        </tr>

		<tr class="fila">
          <td width="27%" valign="middle">HC</td>
          <td width="73%" nowrap>
              <input:select name="hc" attributesText="class=textbox" options="<%=hcs %>"/>&nbsp;<!--20100629-->
		  <!--<select name="hc">
		    <option value="...">Todas</option>
		  	<option value="01">Fenalco Gral</option>
			<option value="02">Fenalco</option>
			<option value="PO">ProvIntegral</option>
			<option value="GE">Geotech</option>
			<option value="FO">FVC OutSourcing</option>
			<option value="CP">CrediPago</option>
			<option value="PA">Particulares</option>
			<option value="AI">Anticipos</option>
			<option value="MB">Mail Boxes</option>
			<option value="DF">Derecho Fiduciario</option>
			<option value="CP">Cupo Express Pronto Pago</option>
			<option value="CR">Cupo Express Retroactivo</option>
			<option value="CC">Conpra de Contrato</option>
			<option value="GC">Compra de Cartera</option>
			<option value="PR">PR a Empleados</option>
			<option value="CM">Consorcio Multiservicio</option>
			<option value="AF">Ant a Contratistas</option>
			<option value="04">Fenalco Metrotel</option>
          </select>--></td>
        </tr>
		<tr class="fila">
		  <td valign="middle">Vencimiento</td>
		  <td nowrap> Entre
		    <input name="lim1" type="text" class="textbox" onKeyPress="soloDigitos_signo(event,'decOK');" size="6" maxlength="6">
		    y
		    <input name="lim2" type="text" class="textbox" onKeyPress="soloDigitos_signo(event,'decOK');" size="6" maxlength="6">
		    Dias</td>
		  </tr>

		 <tr class="fila" <%if(session.getAttribute("Perfil").equals("FID_COLPATRI")){%>style="display:none"<%}%>>
		  <td valign="middle">Exportar a Excel</td>
		  <td valign="middle"><input type='checkbox' name='exp' value='ok'></td>
		 </tr>


      </table></td>
    </tr>
  </table>
  <br>

  <br>
  <center>
    <img alt="" src="<%=BASEURL%>/images/botones/aceptar.gif" name="c_aceptar" onClick="if (validarForma()) forma.submit(); " onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:pointer">
    <img alt="" src="<%=BASEURL%>/images/botones/cancelar.gif" name="c_cancelar" onClick="forma.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:pointer">
    <img alt="" src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:pointer">
  </center>
</form>
<% if( request.getParameter("msg") != null ){%>
  <table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="363" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
 <%} %>
</div>
<%=datos[1]%>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>
