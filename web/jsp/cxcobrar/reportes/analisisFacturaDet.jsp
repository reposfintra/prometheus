 <!--
- Autor : TMolina
- Date  : 07-2008
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite escoger las facturas que seran endozadas a Corficolombiana.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
	
	String fecha = (String) request.getAttribute("fecha");
	String afcfact = (String) request.getAttribute("afcfact");
	String codcli = (String) request.getAttribute("codcli");
	String afcdu = (String) request.getAttribute("afcdu");
	String afcob = (String) request.getAttribute("afcob");
	String prov=(String) request.getAttribute("prov");
	String ccli=(String) request.getAttribute("cccli");
	String vto=(String) request.getAttribute("vencem");
	String cfact=(String) request.getAttribute("cfact");
	String hc=(String) request.getAttribute("hc");
	String lim1=(String) request.getAttribute("lim1");
	String lim2=(String) request.getAttribute("lim2");
        String nit_fiducia=(String) request.getAttribute("nit_fiducia");
	%>
<html>
	<head>
		<title>Cargar a Corficolombiana</title> 
 		<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
		<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
		<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
		<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
	</head>
	<body onresize="redimensionar()" onload = 'redimensionar()'>
		<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 			<jsp:include page="/toptsp.jsp?encabezado=Cargar a Corficolombiana"/>
		</div>
		<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
		<% 
   		 Vector vec = (Vector) request.getAttribute("det");
		 String cliente = "";
		 String agc = "";
		 if (vec.size() > 0) {
		%>
		 		<br>
				<table border="2" align="center" width="1250">
				  <tr>
					<td>
						<table width="100%">
						 <tr>
                                                     <td width="15%" class="subtitulo1">Reporte Detallado</td>
                                                     <td width="85%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=model.gestionConveniosSvc.buscarNombreTercero(nit_fiducia)%></td>
                                                 </tr>
                                                </table>
                                                <form id="f1" name="f1" action='<%=CONTROLLER%>?estado=Proceso&accion=Corficolombiana' method="post">
                                                    <table width="1837" border="1" id="t1" cellpadding="4" cellspacing="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                                                        <tr class="tblTitulo" align="center">
                                                            <td width="30" nowrap><input type="hidden" name="nit_fiducia" id="nit_fiducia" value="<%=nit_fiducia%>"></td>
								<td width="120" nowrap><div align="center">NEGOCIO<br>ASOCIADO</div></td>
								<td width="120" nowrap><div align="center">FACTURA</div></td>
								<td width="120" nowrap><div align="center">CONSECUTIVO</div></td>
								<td width="80" nowrap><div align="center">AGENCIA<br>DUE&Ntilde;A</div></td>
								<td width="80" nowrap><div align="center">AGENCIA<br>DE COBRO </div></td>
								<td width="80" nowrap><div align="center">AGENCIA FACTURACION</div></td>
								<td width="80" nowrap><div align="center">CODIGO CLI</div></td>
								<td width="80" nowrap><div align="center">CC CLI</div></td>
								<td width="250" nowrap><div align="center">CLIENTE</div></td>
								<td width="250" nowrap><div align="center">AFILIADO</div></td>
								<td width="80" nowrap><div align="center">FECHA DESEM</div></td>
								<td width="80" nowrap><div align="center">TELEFONO</div></td>
								<td width="100" nowrap><div align="center">TASA FENALCO (%)</div></td>
								<td width="80" nowrap><div align="center">FECHA FACTURA </div></td>
								<td width="80" nowrap><div align="center">FECHA VENCIMIENTO</div></td>
								<td width="80" nowrap><div align="center">VENCIMIENTO FENALCO</div></td>
								<td width="80" nowrap><div align="center">No AVAL</div></td>
								<td width="80" nowrap><div align="center">BANCO</div></td>
								<td width="100" nowrap><div align="center">REFERENCIA CL</div></td>
								<td width="80" nowrap><div align="center">CMC</div></td>
								<td width="80" nowrap><div align="center">MONEDA</div></td>
								<td width="130" nowrap><div align="center">FECHA DE CORTE </div></td>
								<td width="130" nowrap><div align="center">DIAS</div></td>
								<td width="130" nowrap><div align="center">VENCIMIENTO</div></td>
								<td width="130" nowrap><div align="center">VALOR FACTURA </div></td>
								<td width="130" nowrap><div align="center">VALOR SALDO FACTURA </div></td>
								<td width="80" nowrap><div align="center">OBSERVACION</div></td>
						</tr>
					<%
						for (int i = 0; i < vec.size(); i++){
							RepGral obj = (RepGral) vec.elementAt(i);
							if( obj.getCodcli() != null ) {
								//cliente = obj.getCodcli();
								//agc = obj.getCodagcfacturacion();
					%>
						<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
							<td class="bordereporte" nowrap><input type="checkbox" name="facturasC" id="<%= obj.getNegocio() %>" value="<%= obj.getFactura()+";"+obj.getCodcli()+";"+obj.getNitpro()+";"+obj.getVlr_saldo()+";"+obj.getFecha_fra()+";"+
                                                        obj.getDescripcion_rxp()+";"+obj.getNegocio()+";"+ obj.getDocumento()+";"+obj.getTipo_documento()%>" onclick="selectNegocio(this)"></td>
							<td class="bordereporte" nowrap><%= obj.getNegocio() %></td>
							<td class="bordereporte" nowrap><%= obj.getFactura() %></td>
							<td class="bordereporte" nowrap><%= obj.getDocu() %></td>
							<td nowrap class="bordereporte"><div align="center"><%= obj.getAgc_duenia() %></div></td>
							<td nowrap class="bordereporte"><div align="center"><%= obj.getAgc_cobro() %></div></td>
							<td nowrap class="bordereporte"><div align="center"><%= obj.getAgcfacturacion() %></div></td>
							<td class="bordereporte" nowrap><%= obj.getCodcli() %></td>
							<td class="bordereporte" nowrap><%= obj.getNitpro() %></td>
							<td class="bordereporte" nowrap><%= obj.getNomcli() %></td>
							<td class="bordereporte" nowrap><%= obj.getNitter() %></td>
							<td class="bordereporte" nowrap><%= obj.getDespla()%></td>
							<td class="bordereporte" nowrap><%= obj.getNom_costo_2()%></td>
							<td class="bordereporte" nowrap align="center"><%=  obj.getRegistros()  %></td>
							<td class="bordereporte" nowrap><div align="center"><%= obj.getFecha_fra() %></div></td>
							<td class="bordereporte" nowrap><div align="center"><%= obj.getFecha_venc_fra() %></div></td>
							<td class="bordereporte" nowrap><div align="center"><%= obj.getNom_costo_3() %></div></td>
							<td class="bordereporte" nowrap><div align="center"><%= obj.getNumaval()%></div></td>
							<td class="bordereporte" nowrap><div align="center"><%= obj.getCodbco()%></div></td>
							<td class="bordereporte" nowrap><%= obj.getRefcl()%></td>
							<td class="bordereporte" nowrap><div align="center"><%= obj.getCmc()%></div></td>
							<td class="bordereporte" nowrap><div align="center"><%= obj.getMoneda() %></div></td>
							<td class="bordereporte" nowrap><div align="center"><%= obj.getFecha_corte() %></div></td>
							<td class="bordereporte" nowrap><div align="center"><%= obj.getNdias() %></div></td>
							<td class="bordereporte" nowrap><div align="center"><%= obj.getVencimiento() %></div></td>
							<td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(obj.getValor_factura()) %></div></td>
							<td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(obj.getVlr_saldo()) %></div></td>
							<td nowrap class="bordereporte">
								<div align="center">
									<table >
										<tr>
										<%
										if( obj.getNom_costo_4().equals("1") ){%>
											<td width="33%" align="right" nowrap><img src="<%=BASEURL%>/images/verde.JPG" width="11" height="13" title='Tiene Historial de observaciones'></td>
										<%}%>
											<td style="cursor:hand" onclick="window.open('<%=CONTROLLER%>?estado=Factura&accion=Observacion&op=1&factu=<%=obj.getFactura()%>&cod_cliente=<%=obj.getCodcli()%>','',' top=100,left=100, width=700, height=700, scrollbars=yes, status=yes, resizable=yes')"><img src="<%=BASEURL%>/images/nota.gif" width="15" height="12" title='Agregar Observaciones'></td>
										</tr>
									</table>
								</div> 
							</td>
					</tr>
					<%
					  }//Fin Si
					 }//Fin Para
					%>
					  </table>
				   </form>
				</table>
        <%
	     }else{//No se encontraron Resultados
	    %>
			<table width="564" border="2" align="center">
			  <tr>
				<td>
				  <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
					<tr>
					  <td align="center" class="mensajes">No se encontraron resultados. </td>
					  <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
					</tr>
				  </table>
				</td>
			  </tr>
		   </table>
        <%}%>
		<table width="100%" align="center">
		  <tr>
			<td>
				 <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="window.location =  '<%= CONTROLLER %>?estado=Menu&accion=Cargar&carpeta=/jsp/fenalco/corficolombiana&pagina=filtroFacturas.jsp&marco=no&opcion=33&item=15';">
                 <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="c_aceptar" onClick="f1.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
				 <!--Mod Tmolina 2008-08-05 -->
				 <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
				 <!-- Fin Mod-->
			</td>
		  </tr>
		</table>
     </div>
     <%=datos[1]%>
     </body>
</html>
