 <!--
- Autor : Ing. Andr�s Maturana De La Cruz
- Date  : 21 noviembre del 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la generaci�n del reporte
				de cartera.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
	
	String fecha = (String) request.getAttribute("fecha");
	String afcfact = (String) request.getAttribute("afcfact");
	String codcli = (String) request.getAttribute("codcli");
	String afcdu = (String) request.getAttribute("afcdu");
	String afcob = (String) request.getAttribute("afcob");
	String prov=(String) request.getAttribute("prov");
	String ccli=(String) request.getAttribute("cccli");
	String vto=(String) request.getAttribute("vencem");
	String cfact=(String) request.getAttribute("cfact");
	String hc=(String) request.getAttribute("hc");
	String lim1=(String) request.getAttribute("lim1");
	String lim2=(String) request.getAttribute("lim2");
	%>
<html>
<head><title>Reporte de Cartera</title> 
 <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script>
	function exportar(){
				window.open('<%= CONTROLLER %>?estado=Analisis&accion=VencCXC&clientes=<%= codcli%>&agencia=<%= afcfact%>&agencia_d=<%= afcdu%>&agencia_c=<%= afcob %>&fecha=<%= fecha%>&proveedor=<%=prov%>&cccli=<%=ccli%>&vence=<%=vto%>&tfac=<%=cfact%>&hc=<%=hc%>&lim1=<%=lim1%>&lim2=<%=lim2%>&exp=ok','EXP','status=yes,scrollbars=yes,width=400,height=150,resizable=no');
	}
</script>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Reporte de Cartera"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<% 
    Vector vec = (Vector) request.getAttribute("det");
	String cliente = "";
	String agc = "";
	if (vec.size() > 0) {
%>
<br>
<table border="2" align="center" width="1250">
  <tr>
    <td>
	  <table width="100%">
        <tr>
          <td width="50%" class="subtitulo1">Reporte Detallados</td>
          <td width="50%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
        </tr>
      </table>
	  <table width="1737" border="1" cellpadding="4" cellspacing="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
        <tr class="tblTitulo" align="center">
		
          <td width="80" nowrap><div align="center">AGENCIA<br>
            DUE&Ntilde;A</div></td>
          <td width="80" nowrap><div align="center">AGENCIA<br>
      DE COBRO </div></td>
          <td width="80" nowrap><div align="center">AGENCIA FACTURACION</div></td>
          <td width="80" nowrap><div align="center">CODIGO CLI</div></td>
		  <td width="80" nowrap><div align="center">CC CLI</div></td>
          <td width="250" nowrap><div align="center">CLIENTE</div></td>
		  <td width="250" nowrap><div align="center">AFILIADO</div></td>
		  <td width="80" nowrap><div align="center">FECHA DESEM</div></td>
		  <td width="80" nowrap><div align="center">TELEFONO</div></td>
		  <td width="100" nowrap><div align="center">TASA FENALCO (%)</div></td>
          <td width="120" nowrap><div align="center">FACTURA</div></td>
		  <td width="120" nowrap><div align="center">CONSECUTIVO</div></td>
          <td width="80" nowrap><div align="center">FECHA FACTURA </div></td>
          <td width="80" nowrap><div align="center">FECHA VENCIMIENTO</div></td>
		  <td width="80" nowrap><div align="center">VENCIMIENTO FENALCO</div></td>
		  <td width="80" nowrap><div align="center">No AVAL</div></td>
		  <td width="80" nowrap><div align="center">BANCO</div></td>
		   <td width="100" nowrap><div align="center">REFERENCIA CL</div></td>
		   <td width="80" nowrap><div align="center">CMC</div></td>
          <td width="80" nowrap><div align="center">MONEDA</div></td>
          <td width="130" nowrap><div align="center">FECHA DE CORTE </div></td>
          <td width="130" nowrap><div align="center">DIAS</div></td>
          <td width="130" nowrap><div align="center">VENCIMIENTO</div></td>
          <td width="130" nowrap><div align="center">VALOR FACTURA </div></td>
          <td width="130" nowrap><div align="center">VALOR SALDO FACTURA </div></td>
		  <td width="80" nowrap><div align="center">OBSERVACION</div></td>
          </tr>
<%
    for (int i = 0; i < vec.size(); i++){
        RepGral obj = (RepGral) vec.elementAt(i);
		
		if( obj.getCodcli() != null ) {
			cliente = obj.getCodcli();
			agc = obj.getCodagcfacturacion();
%>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
		          
          <td nowrap class="bordereporte"><div align="center"><%= obj.getAgc_duenia() %></div></td>
          <td nowrap class="bordereporte"><div align="center"><%= obj.getAgc_cobro() %></div></td>
          <td nowrap class="bordereporte"><div align="center"><%= obj.getAgcfacturacion() %></div></td>
          <td class="bordereporte" nowrap><%= obj.getCodcli() %></td>
		  <td class="bordereporte" nowrap><%= obj.getNitpro() %></td>
		  <td class="bordereporte" nowrap><%= obj.getNomcli() %></td>
		  <td class="bordereporte" nowrap><%= obj.getNitter() %></td>
          <td class="bordereporte" nowrap><%= obj.getDespla()%></td>
		  <td class="bordereporte" nowrap><%= obj.getNom_costo_2()%></td>
		  <td class="bordereporte" nowrap align="center"><%=  obj.getRegistros()  %></td>
          <td class="bordereporte" nowrap><%= obj.getFactura() %></td>
		  <td class="bordereporte" nowrap><%= obj.getDocu() %></td>
          <td class="bordereporte" nowrap><div align="center"><%= obj.getFecha_fra() %></div></td>
          <td class="bordereporte" nowrap><div align="center"><%= obj.getFecha_venc_fra() %></div></td>
		  <td class="bordereporte" nowrap><div align="center"><%= obj.getNom_costo_3() %></div></td>
		  <td class="bordereporte" nowrap><div align="center"><%= obj.getNumaval()%></div></td>
		  <td class="bordereporte" nowrap><div align="center"><%= obj.getCodbco()%></div></td>
		  <td class="bordereporte" nowrap><%= obj.getRefcl()%></td>
		  <td class="bordereporte" nowrap><div align="center"><%= obj.getCmc()%></div></td>
          <td class="bordereporte" nowrap><div align="center"><%= obj.getMoneda() %></div></td>
          <td class="bordereporte" nowrap><div align="center"><%= obj.getFecha_corte() %></div></td>
          <td class="bordereporte" nowrap><div align="center"><%= obj.getNdias() %></div></td>
          <td class="bordereporte" nowrap><div align="center"><%= obj.getVencimiento() %></div></td>
          <td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(obj.getValor_factura()) %></div></td>
          <td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(obj.getVlr_saldo()) %></div></td>
          <td nowrap class="bordereporte">
		  <div align="center">
		  	<table >
				<tr>
					<%
					if( obj.getNom_costo_4().equals("1") ){%>
					<td width="33%" align="right" nowrap><img src="<%=BASEURL%>/images/verde.JPG" width="11" height="13" title='Tiene Historial de observaciones'></td>
					<%}%>
					<td style="cursor:hand" onClick="window.open('<%=CONTROLLER%>?estado=Factura&accion=Observacion&op=1&factu=<%=obj.getFactura()%>','','  top=100,left=100, width=700, height=700, scrollbars=yes, status=yes, resizable=yes')"><img src="<%=BASEURL%>/images/nota.gif" width="15" height="12" title='Agregar Observaciones'></td>
				</tr>
			</table>
			</div> 
		 </td>
		  </tr>
<%  
		} else {
%>
<%  
		} 
	}
%> 
      </table>
    </table>
<%} else {%>
    <table width="564" border="2" align="center">
      <tr>
        <td>
	      <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
              <td align="center" class="mensajes">No se encontraron resultados. </td>
              <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
            </tr>
          </table>
	    </td>
      </tr>
  </table>
<%}%>
<p>
<table width="100%" align="center">
  <tr>
  	<td>
      <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="window.location =  '<%= CONTROLLER %>?estado=Menu&accion=Cargar&carpeta=/jsp/cxcobrar/reportes&pagina=analisisCartera.jsp&marco=no&opcion=33&item=15';">	  
      <% if (vec.size() > 0) { %>
      <img src="<%=BASEURL%>/images/botones/exportarExcel.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="exportar();">
	  <% } %></td>
  </tr>
</table>
</div>
<%=datos[1]%>
</body>
</html>
