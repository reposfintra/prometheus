<!--
- Autor : Ing. Fily Fernandez
- Creation Date  : Enero de 2007
- Modified: Ing. Enrique De Lavalle
- Modified Date: 13 de Marzo 2007
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la generación del reporte de facturas de clientes.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
</script>
<title>Reporte de los Pagos Clientes</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
<!--librerias calendario-->
<link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/jscal2.css" />
<link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/border-radius.css" />
<script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
<script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>
<script>

function validaFechas(){
   	var fecha1 = document.form1.FechaI.value.replace(/-/g,'').replace('-','');
   	var fecha2 = document.form1.FechaF.value.replace(/-/g,'').replace('-','');
	var cliente = document.form1.codcli.value;
	var fech1 = parseFloat(fecha1);
	var fech2 = parseFloat(fecha2);
	if(form1.FechaI.value == ''){
	  alert('Debe ingresar la fecha inicial para continuar con la busqueda');
	  return (false);
	}
	if(form1.FechaF.value == ''){
	  alert('Debe ingresar la fecha final para continuar con la busqueda');
	  return (false);
	}
	if(fech1>fech2) {     
   		alert('La fecha final debe ser mayor que la fecha inicial');
		return (false);
    }
	if(cliente==''){
		alert('Debe Ingresar un Cliente para generar el reporte');
		return (false);
	}
	return true;
}
</script>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:110px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Reporte de los Pagos Clientes"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 109px; overflow: scroll;">

<form name="form1" action='<%=CONTROLLER%>?estado=ReportePagos&accion=Cliente&op=1' id="forma" method="post">
  <table width="373"  border="2" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td width="367"><table width="101%" class="tablaInferior">
        <tr>
          <td colspan="2" ><table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
              <tr>
                <td width="56%" class="subtitulo1">&nbsp;Reporte de Pagos </td>
                <td width="44%" class="barratitulo"><%=datos[0]%><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
              </tr>
          </table></td>
        </tr>
        <tr class="fila">
         
        </tr>
        <tr class="fila">
          <td>Fecha Inicial: </td>
          <td nowrap>
            <input name="FechaI" type='text' class="textbox" id="FechaI" style='width:120' value='' readonly>
            <img src="<%=BASEURL%>/js/Calendario/cal.gif" id="imgFechaI" />
            <script type="text/javascript">
                Calendar.setup({
                    inputField : "FechaI",
                    trigger    : "imgFechaI",
                    align : "top",
                    onSelect   : function() {
                        this.hide();
                    }
                });
            </script>
        </tr>    
		<tr class="fila">
          <td>Fecha Final:</td>
          <td nowrap>
            <input name="FechaF" type='text' class="textbox" id="FechaF" style='width:120' value='' readonly>
            <img src="<%=BASEURL%>/js/Calendario/cal.gif" id="imgFechaF" />
            <script type="text/javascript">
                Calendar.setup({
                    inputField : "FechaF",
                    trigger    : "imgFechaF",
                    align : "top",
                    onSelect   : function() {
                        this.hide();
                    }
                });
            </script>
        </tr>
		<tr class="fila">
         <td>Codigo Cliente:</td>
			<td nowrap>
			<input name='codcli' type='text' class="textbox" id="codcli" size="10" maxlength="10">
			<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"> <a onClick="window.open('<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=consultasCliente.jsp&marco=no&carpeta=/consultas','','status=yes,scrollbars=no,width=650,height=600,resizable=yes')" style="cursor:hand"><img height="20" width="25" src="<%=BASEURL%>/images/botones/iconos/user.jpg"></a>
			</td>
        </tr>
		
      </table></td>
    </tr>
  </table>
  <br>
  
  <br>
  <center>
<img src="<%=BASEURL%>/images/botones/aceptar.gif" name="c_aceptar" onClick="if (validaFechas()) forma.submit(); " onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">   
 <img src="<%=BASEURL%>/images/botones/cancelar.gif" name="c_cancelar" onClick="forma.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
    <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
  </center>
</form>
<% if( request.getParameter("msg") != null ){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="363" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%} %>
</div>
<%=datos[1]%>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</body>
</html>
