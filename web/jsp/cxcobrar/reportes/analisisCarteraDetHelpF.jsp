<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE>Reporte de Cartera</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</HEAD>
<BODY>  

  <table width="95%"  border="2" align="center">
    <tr>
      <td>
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">Reporte Reporte de Cartera</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa de Reporte de Cartera. </td>
          </tr>
          <tr>
            <td  height="18"  class="ayudaHtmlTexto">Formulario del programa  de Reporte de Cartera. </td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL%>/images/ayuda/cxcobrar/reportes/repcartera/Dibujo1.PNG"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">Si el sistema mostrar&aacute; los resultados obtenidos en la pantalla siguiente: </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL%>/images/ayuda/cxcobrar/reportes/repcartera/Dibujo2.PNG"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">Cuando los resultados obtenidos son de un solo cliente, se obtendra un reporte detallado de cada una de las facturas encontradas. </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL%>/images/ayuda/cxcobrar/reportes/repcartera/Dibujo3.PNG"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">Si el sistema no encuentra resultados, se presentar&aacute; la pantalla siguiente con el mensaje indicando q no se hallaron resultados: </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL%>/images/ayuda/cxcobrar/reportes/repcartera/Dibujo4.PNG"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"></div></td>
          </tr>          
      </table></td>
    </tr>
  </table>
</BODY>
</HTML>
