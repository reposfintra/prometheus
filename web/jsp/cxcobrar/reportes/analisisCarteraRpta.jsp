 <!--
- Autor : Ing. Tito Andr�s Maturana
- Date  : 21 noviembre del 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la generaci�n del reporte
				de cartera.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
	
	String fecha = (String) request.getAttribute("fecha");
	String afcfact = (String) request.getAttribute("afcfact");
%>
<html>
<head><title>Reporte de Cartera</title> 
 <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script>
	function exportar(cliente, agencia){
		window.open('<%= CONTROLLER %>?estado=Analisis&accion=VencCXC&clientes=' + cliente + '&agencia=' + agencia + '&fecha=<%= fecha%>&exp2=ok','EXP','status=yes,scrollbars=yes,width=400,height=150,resizable=no');
	}
</script>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Reporte de Cartera"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<% 
    Vector vec0 = (Vector) request.getAttribute("resum");
	
	Vector vec = vec0.size() == 1 ? (Vector) vec0.elementAt(0) : new Vector();
	String cliente = "";
	String agc = "";
	if (vec.size() > 0) {
%>
<br>
<table border="2" align="center" width="1250">
  <tr>
    <td>
	  <table width="100%">
        <tr>
          <td width="50%" class="subtitulo1">Reporte Detallado </td>
          <td width="50%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
        </tr>
      </table>
	  <table width="2019" border="1" cellpadding="4" cellspacing="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
        <tr class="tblTitulo" align="center">
          <td width="80" nowrap>CODIGO</td>
          <td width="250" nowrap>CLIENTE</td>
          <td width="120" nowrap>FACTURA</td>
          <td width="80" nowrap>AGENCIA<br>
            DUE&Ntilde;A</td>
          <td width="80" nowrap>AGENCIA<br>
            DE COBRO </td>
          <td width="80" nowrap>AGENCIA FACTURACION</td>
          <td width="80" nowrap>FECHA</td>
          <td width="80" nowrap>VENCE</td>
          <td width="80" nowrap>MONEDA</td>
          <td width="130" nowrap>NO VENCIDA</td>
          <td width="130" nowrap>VENCIDA 1 <br>            <%= fecha %></td>
          <td width="130" nowrap>VENCIDA 2 <br>            <%= com.tsp.util.Util.addFecha(fecha, -15, 2) %></td>
          <td width="130" nowrap>VENCIDA 3 <br>            <%= com.tsp.util.Util.addFecha(fecha, -30, 2) %></td>
          <td width="130" nowrap>VENCIDA 4 <br>            <%= com.tsp.util.Util.addFecha(fecha, -45, 2) %></td>
          <td width="130" nowrap>VENCIDA 5 <br>            <%= com.tsp.util.Util.addFecha(fecha, -60, 2) %></td>
          <td width="130" nowrap>VENCIDA 6 <br>            <%= com.tsp.util.Util.addFecha(fecha, -90, 2) %></td>
		</tr>
<%
    for (int i = 0; i < vec.size(); i++){
        RepGral obj = (RepGral) vec.elementAt(i);
		
		if( obj.getCodcli() != null ) {
			cliente = obj.getCodcli();
			agc = obj.getCodagcfacturacion();
%>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
          <td class="bordereporte" nowrap><%= obj.getCodcli() %></td>
          <td class="bordereporte" nowrap><%= obj.getNomcli() %></td>
          <td class="bordereporte" nowrap><%= obj.getFactura() %></td>
          <td class="bordereporte" nowrap><div align="center"><%= obj.getAgc_duenia() %></div></td>
          <td class="bordereporte" nowrap><div align="center"><%= obj.getAgc_cobro() %></div></td>
          <td class="bordereporte" nowrap><div align="center"><%= obj.getAgcfacturacion() %></div></td>
          <td class="bordereporte" nowrap><div align="center"><%= obj.getFecha_fra() %></div></td>
          <td class="bordereporte" nowrap><div align="center"><%= obj.getFecha_venc_fra() %></div></td>
          <td class="bordereporte" nowrap><div align="center"><%= obj.getMoneda() %></div></td>
          <td class="bordereporte" nowrap><div align="right"><%= obj.getNo_vencida() != 0 ? com.tsp.util.UtilFinanzas.customFormat2(obj.getNo_vencida()) : "&nbsp;" %></div></td>
          <td class="bordereporte" nowrap><div align="right"><%= obj.getVencida_1() != 0 ? com.tsp.util.UtilFinanzas.customFormat2(obj.getVencida_1()) : "&nbsp;" %></div></td>
          <td class="bordereporte" nowrap><div align="right"><%= obj.getVencida_2() != 0 ? com.tsp.util.UtilFinanzas.customFormat2(obj.getVencida_2()) : "&nbsp;" %></div></td>
          <td class="bordereporte" nowrap><div align="right"><%= obj.getVencida_3() != 0 ? com.tsp.util.UtilFinanzas.customFormat2(obj.getVencida_3()) : "&nbsp;" %></div></td>
		  <td class="bordereporte" nowrap><div align="right"><%= obj.getVencida_4() != 0 ? com.tsp.util.UtilFinanzas.customFormat2(obj.getVencida_4()) : "&nbsp;" %></div></td>  
          <td class="bordereporte" nowrap><div align="right"><%= obj.getVencida_5() != 0 ? com.tsp.util.UtilFinanzas.customFormat2(obj.getVencida_5()) : "&nbsp;" %></div></td>
		  <td class="bordereporte" nowrap><div align="right"><%= obj.getVencida_6() != 0 ? com.tsp.util.UtilFinanzas.customFormat2(obj.getVencida_6()) : "&nbsp;" %></div></td>         
		</tr>
<%  
		} else {
%>       
		<tr class="subtitulo1" >
          <td colspan="9" class="bordereporte"><div align="right">Totales</div></td>
          <td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(obj.getNo_vencida()) %></div></td>
          <td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(obj.getVencida_1()) %></div></td>
          <td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(obj.getVencida_2()) %></div></td>
          <td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(obj.getVencida_3()) %></div></td>
		  <td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(obj.getVencida_4()) %></div></td>  
          <td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(obj.getVencida_5()) %></div></td>
		  <td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(obj.getVencida_6()) %></div></td>         
		</tr>
<%  
		} 
	}
%> 
      </table>
    </table>
<%} else {%>
    <table width="564" border="2" align="center">
      <tr>
        <td>
	      <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
              <td align="center" class="mensajes">No se encontraron resultados. </td>
              <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
            </tr>
          </table>
	    </td>
      </tr>
  </table>
<%}%>
<p>
<table width="100%" align="center">
  <tr>
  	<td>
<% if ( request.getAttribute("degen") != null  ) { %>
      <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="window.location =  '<%= CONTROLLER %>?estado=Analisis&accion=VencCXC&clientes=&agencia=<%= (String) request.getAttribute("degen") %>&fecha=<%= fecha%>';">
<% } else {%>
      <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="window.location =  '<%= CONTROLLER %>?estado=Menu&accion=Cargar&carpeta=/jsp/cxcobrar/reportes&pagina=analisisCartera.jsp&marco=no&opcion=33&item=15';">	  
<% } %>
      <% if (vec.size() > 0) { %>
      <img src="<%=BASEURL%>/images/botones/exportarExcel.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="exportar('<%= cliente%>','<%= agc %>');">
	  <% } %>
</td>
  </tr>
</table>
</div>
<%=datos[1]%>
</body>
</html>
