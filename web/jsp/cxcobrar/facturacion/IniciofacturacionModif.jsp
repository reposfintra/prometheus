<!--
- Autor : Ing. Juan Manuel Escando Perez
- Date  : 29 Diciembre de 2005 
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite buscar una planilla para la impresion de la hoja de control de viaje
--%>

<%-- Declaracion de librerias--%>
<%@page session="true"%> 
<%@page import="java.util.*" %>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%
    String Mensaje = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
	   
	factura a = model.facturaService.leerArchivo("factura"+usuario.getLogin ()+".txt",usuario.getLogin ());
	Vector b = model.facturaService.leerArchivoItems("factura"+usuario.getLogin ()+".txt",usuario.getLogin ());
	String caracter = "a.reg_status = 'C'";
    if(a!=null && b!= null){
	    model.facturaService.buscarRemesas(a.getCodcli(),caracter,usuario.getDstrct());
	}
	
	String r1   =   (request.getParameter("r1")!=null)?request.getParameter("r1"):"";
	String r2   =   (request.getParameter("r2")!=null)?request.getParameter("r2"):"";
	String che2 =   (request.getParameter("che2")!=null)?request.getParameter("che2"):"";
        
        String che3 =   (request.getParameter("che3")!=null)?request.getParameter("che3"):"";
        
        String limit =   (request.getParameter("limite")!=null)?request.getParameter("limite"):"";
	
%>
<html>
<head><title>Facturacion</title></head>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src='<%=BASEURL%>/js/validacionesImpresionPlanilla.js'></script>
<script>
function maximizar(){
 window.moveTo(0,0);

  if( window.width != screen.availWidth || window.height != screen.availHeight)
   top.window.resizeTo(screen.availWidth,screen.availHeight);
 
}
function abrirFactura(){

    window.location.href="<%=BASEURL%>/jsp/cxcobrar/facturacion/FacturacionAceptadaM.jsp";
   
}
function buscar(){
      var codcli = document.getElementById("codcli"); 

		/*if(codcli.value==''){
		     alert('Favor escribir codigo cliente o numero de factura');  
		      return false;
		}*/
		
		var combo1 =document.getElementById("checkbox1");  
		if(combo1.checked==true){
		       var f1 =document.getElementById("c_fecha"); 
			   var f2 =document.getElementById("c_fecha2");
			   if(f2.value<f1.value){
			        alert('La fecha inicial debe ser igual o menor que la fecha final');  
					return false; 
			   } 
		}
		
		var chk2 = document.getElementById("checkbox2"); 
		var chk3 = document.getElementById("checkbox3");
		if( chk2.checked  && chk3.checked ){
                    alert('Debe seleccionar solo un filtro para el estado de impresion de las facturas');
                    return false;
		}
				
		
      document.form1.action = "<%= CONTROLLER %>?estado=Factura&accion=Modificar&busqueda=modif&cod="+codcli.value; 
      document.form1.submit();     
	  	
}
function modificar(CONTROLLER,factura,cliente){
      var url= CONTROLLER+"?estado=Factura&accion=Modificar&modificar=ok&factura="+factura+"&codcli="+cliente;
	   window.open(url,'factura','status=yes,scrollbars=no,width=820,height=550,resizable=yes');
    
     
}
function eliminar(CONTROLLER,factura,cliente){

	  document.form1.action = "<%= CONTROLLER %>?estado=Factura&accion=Modificar&eliminar=ok&factura="+factura+"&codcli="+cliente; 
      document.form1.submit();     	
}
function cargar(){
 if (window.event.keyCode==13){ 
      var codcli = document.getElementById("codcli");
	  document.form1.action = "<%= CONTROLLER %>?estado=Factura&accion=Modificar&busqueda=modif&cod="+codcli.value; 
      document.form1.submit();  
 }
}
function imprimir(factura,cliente){
	      var url= "<%= CONTROLLER %>?estado=Factura&accion=Insert&imprimir=uno&factura="+factura+"&codcli="+cliente;
	      window.open(url,'fac','status=yes,scrollbars=no,width=600,height=600,resizable=yes');
}	
	
function imprimirChk( aux ){
    var elem    = document.getElementsByName('LOV'); 
    var datos   = '';
    var cont    = 0;
    var limite  = aux;
    for (i=0;i<elem.length; i++){
      if (elem[i].checked){
        datos += (datos==''?'':',') + elem[i].value; 
        cont++;
      }
    }
    
    if (datos==''){
        alert ('Seleccione una factura para poder imprimir');
        return false;
    }
    
    if( cont > limite ){
        alert('El numero de facturas seleccionadas excede en el numero maximo permitido para la impresión. \n' + 'Limite de Factura para imprimir : ' + limite );
        return false;
    }
    
    var url= "<%= CONTROLLER %>?estado=Factura&accion=Insert&imprimir=chk&impchk=chk&LOV="+datos;
    window.open(url,'fac','status=yes,scrollbars=no,width=600,height=600,resizable=yes');
}	

function imprimirtodo(){
	      var url= "<%= CONTROLLER %>?estado=Factura&accion=Insert&imprimir=ok";
	      window.open(url,'fac','status=yes,scrollbars=no,width=600,height=600,resizable=yes');
}	
function formato2digitos( num ){
    		return (num<10? '0':'') + num;
  		}
function formato (fecha){
			return fecha.getYear() + '-' + formato2digitos(fecha.getMonth()+1) + '-' + formato2digitos(fecha.getDate());
	}
function fechas(chec){
	
	    var combo1 =document.getElementById("c_fecha");
		var combo2 =document.getElementById("c_fecha2");
		var f1 =document.getElementById("f1");
		var f2 =document.getElementById("f2");
	    if(chec==true){
		    combo1.style.visibility='visible';
		    combo2.style.visibility='visible';
			f1.style.visibility='visible';
		    f2.style.visibility='visible';
		    var fechaActual  =  new Date();
			combo1.value= formato(fechaActual);
			combo2.value= formato(fechaActual);
		}
		else{
		    combo1.style.visibility='hidden'; 
			combo2.style.visibility='hidden';
			f1.style.visibility='hidden'; 
			f2.style.visibility='hidden';
			
		}	 	   
	}

</script>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modifcacion de Facturas"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
  <form method="post" name="form1" >
		<table align="center" width="397" border="2">
			<tr>
			  <td width="385">
					<TABLE width="100%" align="center"  class="tablaInferior">
				        <tr class="fila">
							<td width="46%" class="subtitulo1">&nbsp;Facturacion</td>
							<td colspan="3" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"align="left"><%=datos[0]%></td>
						</tr>
						<tr class="fila">
							<td align="center">Codigo Cliente o Factura: </td>
							<td colspan="3"><input name='codcli' type='text' class="textbox" id="codcli" size="10" maxlength="10" onKeyPress="cargar()">
						    <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"> <a onClick="window.open('<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=consultasCliente.jsp&marco=no&carpeta=/consultas','','status=yes,scrollbars=no,width=650,height=600,resizable=yes')" style="cursor:hand"><img height="20" width="25" src="<%=BASEURL%>/images/botones/iconos/user.jpg"></a></td>
						</tr>
						<tr class="fila">
						  <td align="center" class="subtitulo1"><div align="left">&nbsp;Filtros</div></td>
						  <td colspan="3"><span class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"align="left"></span></td>
					  </tr>
						<tr class="fila">
						  <td rowspan="2" align="center"><div align="left">Rango de fechas </div></td>
						  <td width="11%" rowspan="2"><input name="checkbox1" type="checkbox" id="checkbox1" onClick="fechas(this.checked)" value="" ></td>
					      <td width="11%">Inicial</td>
					      <td width="32%"><input name="c_fecha" type="text" class="textbox" id="c_fecha" size="14" readonly value="" style="visibility:hidden">
				          <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.c_fecha);return false;" HIDEFOCUS> 
                          <img src="<%=BASEURL%>\js\Calendario\cal.gif"  id="f1" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"  style="visibility:hidden"></td>
					  </tr>
						<tr class="fila">
						  <td>Final</td>
					      <td><input name="c_fecha2" type="text" class="textbox" id="c_fecha2" size="14" readonly value="" style="visibility:hidden">
				          <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.c_fecha2);return false;" HIDEFOCUS> 
                          <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" id="f2" height="16" border="0" alt="De click aqui para escoger la fecha"  style="visibility:hidden"></td>
					  </tr>
						<tr class="fila">
						  <td align="center"><div align="left">Mostrar solo Impresas</div></td>
					      <td colspan="3"><input name="checkbox2" type="checkbox" value="" id="checkbox2" <%if(che2.equals("ok")){%>checked<%}%> ></td>
					  </tr>
					  
					  </tr>
						<tr class="fila">
						  <td align="center"><div align="left">Mostrar no Impresas</div></td>
					      <td colspan="3"><input name="checkbox3" type="checkbox" value="" id="checkbox3" <%if(che3.equals("ok")){%>checked<%}%> ></td>
					  </tr>
					</TABLE>
			  </td>
			</tr>
	</table>
<%if(a!=null && b!= null){%>
        <table border="2" align="center">
          <tr>
            <td width="357">
              <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                <tr>
                  <td width="229" align="center" class="mensajes">Factura Recuperada<%= Mensaje %></td>
                  <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                  <td width="58"><div align="center"><img src="<%=BASEURL%>/images/page.gif" name="imgaceptar" width="22" height="22" style="cursor:hand" onClick="this.disabled=true; abrirFactura();" ></div></td>
                </tr>
            </table></td>
          </tr>
        </table>
        <%}%>
      <br>
		<div align="center"><img src="<%=BASEURL%>/images/botones/buscar.gif" name="imgaceptar"  onMouseOver="botonOver(this);" onClick="buscar();" onMouseOut="botonOut(this);" style="cursor:hand" > <img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ><br>
		  <br>
		    <% Vector vf = model.facturaService.getVFacturas();                      
		if( vf == null ){
		    vf          =   new Vector();                    
                }
		if( vf.size() > 0 ){
               %>
      </div>
		<table width="81%"  border="2" align="center">
          <tr>
            <td>
              <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="barratitulo">
                <tr>
                  <td width="50%" class="subtitulo1" colspan='3'>LISTA DE FACTURAS</td>
                  <td width="50%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif"></td>
                </tr>
              </table>
              <table width="100%" border="1" align="center" bordercolor="#999999">
                <tr class="tblTitulo">
                  <td width="36" nowrap>Modificar</td>
                  <td width="46" nowrap><div align="center">Factura</div></td>
                  <td nowrap align="center">Nombre Cliente </td>
                  <td  nowrap><div align="center">Codigo Cliente</div></td>
                  <td width="46">Fecha Factura </td>
				  <%if(che2.equals("ok")){%>
                  <td width="22">Fecha Impresion </td>
                  <td width="22">Fecha Contabilizacion </td>
				  <%}%>
                  <td width="37"><div align="center"> Items </div></td>
                  <td width="72" ><div align="center">Valor</div></td>
                  <td width="51" ><div align="center">Moneda</div></td>
                  <td width="16" >Anular</td>
                  <td width="45">PDF&nbsp;<img src="<%=BASEURL%>/images/imprimir.gif" name="imgregresar" width="15" height="18" title='Imprimir todas facturas' style="cursor:hand"  onClick="imprimirtodo();"></td>
                </tr>
                <%
	  
	  	for(int i = 0; i<vf.size(); i++){
	  		factura factu = (factura)vf.elementAt(i);
		%>
                <tr class="<%=i%2==0?"filagris":"filaazul"%>"  >
                  <td class="bordereporte"><div align="center"><img src='<%=BASEURL%>/images/botones/iconos/modificar.gif' name='Buscar' align="absmiddle" style='cursor:hand' onclick="<%if(factu.getFecha_impresion().equals("0099-01-01 00:00:00")&&factu.getFecha_contabilizacion().equals("0099-01-01 00:00:00") && usuario.getId_agencia().equals(factu.getAgencia_facturacion())){%>modificar('<%= CONTROLLER %>','<%=factu.getFactura()%>','<%=factu.getCodcli()%>');<%}else{%>alert('La Factura No se puede Modificar por su fecha de impresion, contabilización ó la agencia es diferente a la agencia de facturacion!');<%}%>"> </div></td>
                  <td class="bordereporte"><%=factu.getFactura()%></td>
                  <td class="bordereporte" nowrap><%=factu.getNomcli()%></td>
                  <td class="bordereporte" align="center"><%=factu.getCodcli()%></td>
                  <td class="bordereporte" nowrap> <%=factu.getFecha_factura()%> </td>
                  <%if(che2.equals("ok")){%>
				  <td class="bordereporte"><%=factu.getFecha_impresion()%></td>
                  <td class="bordereporte" nowrap><%=factu.getFecha_contabilizacion()%></td>
				  <%}%>
                  <td class="bordereporte"> <div align="center"><%=factu.getCantidad_items()%> </div></td>
                  <td class="bordereporte" nowrap><div align="right"><%=Util.customFormat(factu.getValor_facturame())%></div></td>
                  <td class="bordereporte"><div align="center"><%=factu.getMoneda()%></div></td>
                  <td class="bordereporte"><div align="center"><img src='<%=BASEURL%>/images/delete.gif' name='Buscar' align="absmiddle" style='cursor:hand' onclick="eliminar('<%= CONTROLLER %>','<%=factu.getFactura()%>','<%=factu.getCodcli()%>');" ></div></td>                                              
                  <td class="bordereporte"><input type='checkbox' name='LOV' value='<%=i%>'>  <img src="<%=BASEURL%>/images/imprimir.gif" name="imgregresar"  onClick="imprimir('<%=factu.getFactura()%>','<%=factu.getCodcli()%>');" style="cursor:hand"></td>                  
                </tr>
                <%}%>
            </table></td>
          </tr>
      </table>
      
      <br>
		<table align="center" width='100%'>
                   <tr align="center">
                       <td>
                            <img src="<%=BASEURL%>/images/botones/imprimir.gif" name="imgimp" title='Imprimir' onMouseOver="botonOver(this);" onClick="imprimirChk('<%=limit%>');" onMouseOut="botonOut(this);" style="cursor:hand">
                       </td>
                   </tr>
                </table>
		<%}%>
		
		
		 
		
  </form>
<%--Mensaje de informacion--%>
	<%if(!Mensaje.equals("")){%>
		<table border="2" align="center">
			<tr>
				<td>
					<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						<tr>
							<td width="229" align="center" class="mensajes"><%= Mensaje %></td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	<%}%>
</div>
<%=datos[1]%>
</body>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</html>
