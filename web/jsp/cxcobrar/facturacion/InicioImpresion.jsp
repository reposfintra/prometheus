<!--
- Autor : Ing. Juan Manuel Escando Perez
- Date  : 29 Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite buscar una planilla para la impresion de la hoja de control de viaje
--%>

<%-- Declaracion de librerias--%>
<%@page session="true"%> 
<%@page import="java.util.*" %>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%
    String Mensaje = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
	   
%>
<html>
<head><title>Facturacion</title></head>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script>
function maximizar(){
 window.moveTo(0,0);

  if( window.width != screen.availWidth || window.height != screen.availHeight)
   top.window.resizeTo(screen.availWidth,screen.availHeight);
 
}
function abrirFactura(){

    window.location.href="<%=BASEURL%>/jsp/cxcobrar/facturacion/FacturacionAceptadaM.jsp";
   
}
function buscar(){
      
	  document.form1.action = "<%= CONTROLLER %>?estado=Factura&accion=Modificar&impresion=modif"; 
      document.form1.submit();     	
}


function cargar(){

 if (window.event.keyCode==13){ 
      var codcli = document.getElementById("codcli");
	  document.form1.action = "<%= CONTROLLER %>?estado=Factura&accion=Modificar&impresion=modif"; 
      document.form1.submit();  
 }
}
function imprimir(){
	      var url= "<%= CONTROLLER %>?estado=Factura&accion=Insert&imprimir=ok";
	      window.open(url,'fac','status=yes,scrollbars=no,width=600,height=600,resizable=yes');
}	
function cargarfechas(){
  			var fechaActual  =  new Date();
  			
			   	form1.c_fecha.value = formato(fechaActual);
			
			
			 
			    form1.c_fecha2.value = formato(fechaActual);
			
}	
function formato (fecha){
			return fecha.getYear() + '-' + formato2digitos(fecha.getMonth()+1) + '-' + formato2digitos(fecha.getDate());
		}
		function formato2digitos( num ){
    		return (num<10? '0':'') + num;
  		}
</script>
<body onLoad="cargarfechas();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Impresion de Facturas"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
  <form method="post" name="form1" >
		<table align="center" width="318" border="2">
			<tr>
				<td>
					<TABLE width="100%" align="center"  class="tablaInferior">
				        <tr class="fila">
							<td width="56%" class="subtitulo1">&nbsp; Impresion Facturas</td>
							<td width="44%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"align="left"><%=datos[0]%></td>
						</tr>
						<tr class="fila">
							<td align="center">Codigo Cliente o Factura: </td>
							<td><input name='codcli' type='text' class="textbox" id="codcli" size="10" maxlength="10" onKeyPress="cargar()">
						    <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"> <a onClick="window.open('<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=consultasCliente.jsp&marco=no&carpeta=/consultas','','status=yes,scrollbars=no,width=550,height=150,resizable=yes')" style="cursor:hand"><img height="20" width="25" src="<%=BASEURL%>/images/botones/iconos/user.jpg"></a></td>
						</tr>
						<tr class="fila">
						  <td rowspan="2" align="center" >Fechas: </td>
					      <td><input name="c_fecha" type="text" class="textbox" id="c_fecha" size="10" readonly >
				          <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.c_fecha);return false;" HIDEFOCUS> 
						  <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></td>
					  </tr>
						<tr class="fila">
						  <td><input name="c_fecha2" type="text" class="textbox" id="c_fecha2" size="10" readonly >
						  <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.c_fecha2);return false;" HIDEFOCUS>
					      <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></td>
					  </tr>
					</TABLE>
				</td>
			</tr>
	</table>
<br>
		<div align="center"><img src="<%=BASEURL%>/images/botones/buscar.gif" name="imgaceptar"  onMouseOver="botonOver(this);" onClick="this.disabled=true; buscar();" onMouseOut="botonOut(this);" style="cursor:hand" > <img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ><br>
		  <br>
		    <%Vector vf = model.facturaService.getVFacturas();
		if(vf==null)
		    vf=new Vector();
		if(vf.size()>0){%>
      </div>
		<table width="54%"  border="2" align="center">
          <tr>
            <td>
              <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="barratitulo">
                <tr>
                  <td width="50%" class="subtitulo1" colspan='3'>LISTA DE FACTURAS </td>
                  <td width="50%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif"></td>
                </tr>
              </table>
              <table width="100%" border="1" align="center" bordercolor="#999999">
                <tr class="tblTitulo">
                  <td nowrap><div align="center">Factura</div></td>
                  <td width="48" nowrap><div align="center">Cliente</div></td>
                  <td width="94">Fecha Factura </td>
                  <td width="37"><div align="center"> Items </div></td>
                  <td width="72" ><div align="center">Valor</div></td>
                  <td ><div align="center">Moneda</div></td>
                </tr>
                <%
	  
	  	for(int i = 0; i<vf.size(); i++){
	  		factura factu = (factura)vf.elementAt(i);
		%>
                <tr class="<%=i%2==0?"filagris":"filaazul"%>" title="Modificar Despacho..." >
                  <td class="bordereporte"><div align="center"> </div>                    <%=factu.getFactura()%></td>
                  <td class="bordereporte"><%=factu.getCodcli()%></td>
                  <td class="bordereporte"> <%=factu.getFecha_factura()%> </td>
                  <td class="bordereporte"> <div align="center"><%=factu.getCantidad_items()%> </div></td>
                  <td class="bordereporte"><div align="right"><%=Util.customFormat(factu.getValor_factura())%></div></td>
                  <td class="bordereporte"><div align="center"><%=factu.getMoneda()%></div>                    <div align="center"></div></td>
                </tr>
                <%}
  %>
            </table></td>
          </tr>
      </table>
		<p align="center"><span class="bordereporte"><img src="<%=BASEURL%>/images/botones/imprimir.gif" name="imgregresar"  onClick="this.disabled=true;imprimir();" style="cursor:hand"></span>     </p>
		<p>
	        <%}%>
                </p>
  </form>
<%--Mensaje de informacion--%>
	<%if(!Mensaje.equals("")){%>
		<table border="2" align="center">
			<tr>
				<td>
					<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						<tr>
							<td width="229" align="center" class="mensajes"><%= Mensaje %></td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	<%}%>
</div>
<%=datos[1]%>
</body>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</html>
