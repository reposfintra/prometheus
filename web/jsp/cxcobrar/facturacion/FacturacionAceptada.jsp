<%@page session="true"%>
<%@page import="java.util.*" %>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Factura</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
 <script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
 <script type='text/javascript' src="<%=BASEURL%>/js/consultadespacho.js"></script> 
 <script type='text/javascript' src="<%=BASEURL%>/js/general.js"></script>
 <script type='text/javascript' src="<%=BASEURL%>/js/posbancaria.js"></script>  
 <style type="text/css">
<!--
.Estilo1 {color: #D0E8E8}
-->
 </style>
</head> 
<% String ag = (String) session.getAttribute("Agencia");
   String Mensaje = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
	factura factu = model.facturaService.getFactu();
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
	//parent.opener.location='"+CONTROLLER+"?estado=Factura&accion=Insert&busqueda=ok&codcli="+factu.getCodcli()+"';
%>
<body onLoad="cargar();maximizar();<%=request.getParameter("reload")!=null?"window.opener.close();":""%><%=(request.getParameter("agregarItems")!=null)?"agregarItems();":"cargarCod();"%><%=(request.getParameter("gas")!=null)?"gastos();":""%>">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Nueva Factura"/>
</div> 

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%
 
 String sinitems = (request.getParameter("sinitems")!=null)?request.getParameter("sinitems"):"";
	String xxx = (request.getParameter("x")!=null)?request.getParameter("x"):"";
	
 double total=0;   
   %>
   <script>
    var maxfila = 0;
    var item_ini = 1;  
	function gastos(){
	    alert('<%=request.getParameter("gas")%>!');     
	}
	function cargarCod(){
	      <%
		  String cargarCodi = (request.getParameter("cargarCodigo")!=null)?request.getParameter("cargarCodigo"):"";
	      String xx = (request.getParameter("x")!=null)?request.getParameter("x"):"";
		  String camp = (request.getParameter("campo")!=null)?request.getParameter("campo"):"";
		  if(cargarCodi.equals("ok")){%>
		      var campu = document.form1.<%=camp%>;
	          campu.value='<%=xx%>';
		  <%}
		  else if(cargarCodi.equals("no")){%>
		      var campu = document.form1.<%=camp%>;
	          campu.value='';
			  alert('codigo cuenta no existe!');
		  <%}%>
	
	}
	
	function procesar (element){
      if (window.event.keyCode==13){ 
        if(element.value !=''){
		    element.value=formatear(element.value,2);
		}
		actualizar();
	  }
    }
	function guardarFactura(CONTROLLER){
	   
	    document.form1.action = "<%= CONTROLLER %>?estado=Factura&accion=Insert&Agregar=ok&guardar=ok&numerofilas="+maxfila; 
        document.form1.submit();   
		
	}
	function aceptarFactura(CONTROLLER){

		var a = form1.textfield9.value.replace(new RegExp(",","g"), "");
        
		
		
                    if(parseFloat(a)>0){
                  document.form1.action = "<%= CONTROLLER %>?estado=Factura&accion=Insert&Agregar=ok&facturacion=ok&numerofilas="+maxfila; 
                  document.form1.submit();
                 }    
                 else
                     alert('El valor de la factura no puede ser igual a cero!');
        
	}
	function aceptarFactura2(causa){
	    document.form1.action = "<%= CONTROLLER %>?estado=Factura&accion=Insert&Agregar=ok&actualizar=ok&numerofilas="+maxfila+"&causa="+causa; 
        document.form1.submit();         
	}
	function agregarItems(){
	   window.open("<%=BASEURL%>/jsp/cxcobrar/facturacion/AgregarItems.jsp",'','status=yes,scrollbars=no,width=1000,height=900,resizable=yes');
	}
	function abrir(CONTROLLER){
	        
		document.form1.action = "<%= CONTROLLER %>?estado=Factura&accion=Insert&Agregar=ok&numerofilas="+maxfila+"&item="+form1.item.value; 
        document.form1.submit();   
	}
	
	
	function BuscarItem(CONTROLLER,item_buscar,item){
	       if(item!=''){
                   document.form1.action = "<%= CONTROLLER %>?estado=Factura&accion=Insert&Agregar=BUSCAR_ITEM&numerofilas="+maxfila+"&item_buscar="+item_buscar+"&item="+item; 
                   document.form1.submit();   
               }else{
                  alert('Debe digitar el numero del item')
               }
	}
	
	function buscarUltimo(CONTROLLER,item_buscar,item){
	      
                   document.form1.action = "<%= CONTROLLER %>?estado=Factura&accion=Insert&opcion=BUSCAR_ULTIMO&Agregar=BUSCAR_ITEM&numerofilas="+maxfila+"&item_buscar="+item_buscar+"&item="+item; 
                   document.form1.submit();   
              
	}
	
	
   function actualizar(){
       
        var moneda= document.getElementById("select2").value;
		var total = document.getElementById("textfield9");
		var suma=0;
		
		for(var i=(item_ini-1);i < maxfila;i++){
		
		    var suma2=0; 
			var valor = document.getElementById("textfield14"+i);
			var valor2 = document.getElementById("textfield15"+i);
			var total2 = document.getElementById("textfield16"+i);
			//alert(valor.value+"    "+ valor2.value+"   "+ total2.value)
			
		    if( valor.value != '' && valor2.value !=''){
		
				if( valor!= '' && valor.value.length>1 && valor!=null)
					var valorlimpio = valor.value.replace(new RegExp(",","g"), "");
				else
					var valorlimpio= 0;
				if( valor2!= '' && valor2.value.length>=1 && valor2!=null)
					var valorlimpio2 = valor2.value.replace(new RegExp(",","g"), "");
				else{
					var valorlimpio2= 0;
					valor2.value=valorlimpio2; 				
				}	
				//if(valorlimpio!=0||valorlimpio2!=0){
					
				suma2 = roundMon(parseFloat(valorlimpio)*parseFloat(valorlimpio2),moneda);
		            //alert("suma2-->"+ suma2)
				if(valorlimpio!='0' && valorlimpio2!='0'){
					if(moneda=='DOL'){
						total2.value=suma2;
						formatoDolar(document.getElementById("textfield16"+i), 2); 
					}else	
						total2.value=formatear(''+suma2,0); 
				}
				else if(valorlimpio!='0' && valorlimpio2=='0'){
					if(moneda=='DOL'){
						total2.value=suma2;
						formatoDolar(document.getElementById("textfield16"+i), 2); 
					}else	
						total2.value=formatear(''+suma2,0);  
				}	
				else if(valorlimpio=='0' && valorlimpio2!='0'){
					if(moneda=='DOL'){
						total2.value=suma2;
						formatoDolar(document.getElementById("textfield16"+i), 2);
					}else	
						total2.value=formatear(''+suma2,0); 
				}	
				suma+=suma2;
		//}
			}
	
		}
		
		if(moneda=='DOL'){
		    total.value =suma;
			formatoDolar(document.getElementById("textfield9"), 2);
		}else
		    total.value =formatear(''+Math.round(suma),0);	
		
	}
	
	function roundMon(numero,moneda){
		if(moneda=='DOL'){
			  numero = (Math.round(numero * 100))/100;
			  return numero;
		}else{
			return Math.round(numero);
		}
	} 
	
	function validaritems(){
       
	   var valor0 = document.getElementById("textfield5");/*agencia*/
	   var valor00 = document.getElementById("c_fecha2");/*plazo*/
	   var valor000 = document.getElementById("textfield");/*tasa*/
	   var valor0000 = document.getElementById("textfield95");/*total*/
	   
	   
	   
	   if(valor0.value == ''){
	       alert('Debe escojer la agencia facturacion!');
		   return false;	
	   }
	   if(valor00.value == ''){
	       alert('Debe tener valor en el plazo!');
		   return false;	
	   }
	   if(valor000.value == ''){
	       alert('Debe tener valor en la tasa !');
		   return false;	
	   }
	   
	   var moneda= document.getElementById("select2").value;
       
	   if(moneda=='DOL'){
               if( valor0000.value.length >19 ){
	       alert('El valor total de la factura excede 14 digitos !');
		   return false;
	       }
           }
           else{
               if( valor0000.value.length >15 ){
	       alert('El valor total de la factura excede 12 digitos !');
		   return false;
	       }
           }
       	
		return true;
		
	}
	
	function sinformato(element){
      element.value.replace(new RegExp(",","g"), "");
    }
		
	function formatear(numero,decimales){
		
	   if (!isNaN (parseInt(numero.replace( new RegExp(",","g"), "")))){
        	   var tmp = parseInt(numero.replace( new RegExp(",","g"), "")) ;
			   var num = '';
			   var pos = 0;
			   while(tmp>0){
					  if (pos%3==0 && pos!=0) num = ',' + num;
					  res  = tmp % 10;
					  tmp  = parseInt(tmp / 10);
					  num  = res + num  ;
					  pos++;
			   }
			   var dec = 1;
			   for (i=0;i<decimales;i++, dec*=10);
			   dnum = parseInt ((parseFloat(numero) - parseInt(numero)) * dec);
					   tmpdec = '';
			   for (i=0;i<decimales - (dnum==0?0:dnum.toString().length ) ;i++, tmpdec +='0');
			   num += (decimales>0?'.' +  tmpdec + (dnum!=0?dnum:''): '');
		       if(num=='')
			       num='0';
        return num;
		}
		else
		    return '';
		
    }  
	
	function revisaritemenblanco(){
	var i= maxfila-1;
	
	        var valor11 = document.getElementById("textfield11"+i);
			var valor12 = document.getElementById("textfield12"+i);
			
			var valor13= document.getElementById("textfield13"+i);
			var valor14 = document.getElementById("textfield14"+i);
			
			var valor15= document.getElementById("textfield15"+i);
			var valor16 = document.getElementById("textfield16"+i);
			
			var valor17 = document.getElementById("textfield17"+i);
			var valor18 = document.getElementById("textfield18"+i);
			
			if(valor11.value==""&&valor12.value==""&&valor13.value==""&&valor14.value==""&&valor15.value==""&&valor16.value==""&&valor17.value==""&&valor18.value=="")
			    return false;
		    else
		        return true;		
	}
	
	
	
	function borrarItem(indice){
          
            document.form1.action = "<%= CONTROLLER %>?estado=Factura&accion=Insert&opcion=ELIMINAR&id="+indice;
            document.form1.submit();
	}
	
	function organizar(){
 	   /* var cont =1;
		for (var i = 0,n = 1;i <= maxfila + 1 ; i++){
			var x = document.getElementById("n_i"+i);
			var y = document.getElementById("filaItem"+i);
			if ( x != null ){
				x.innerText = ""+n;
				n++;
			}
			if( y != null ){
				if( cont % 2 == 0){
				   y.className = "filagris";
				}else{
				   y.className = "fila";
				}
				cont++;
			}
		}*/
	}
	
	function regresar(){
	
	   document.form1.action = "<%= CONTROLLER %>?estado=Factura&accion=Insert&regresar=ok"; 
       document.form1.submit();     
	}
	
	function formato2digitos( num ){
    		return (num<10? '0':'') + num;
  		}
    function formato (fecha){
			return fecha.getYear() + '-' + formato2digitos(fecha.getMonth()+1) + '-' + formato2digitos(fecha.getDate());
		}
		
	    function cargar(){
  			var fechaActual  =  new Date();
  			if(form1.c_fecha.value=='null'){
			   
				form1.c_fecha.value = formato(fechaActual);
                                //alert(fechaActual);
			}
			
		}
		
		
function buscarRemesa( x ) {
 var fact =document.getElementById("textfield11"+x);
 var fec = document.getElementById("textfield12"+x);
 var factura = fact.value;
 if(factura=="") {  

 } else {
       if(confirm('Para facturar REMESA de transporte, presione CANCELAR.\n\nPara facturar GASTO asociado a remesa, presione ACEPTAR. ' )){

		    document.form1.action = "<%= CONTROLLER %>?estado=Factura&accion=Insert&cargar=ok&x="+factura+"&gasto=ok&fec="+fec.value+"&numerofilas="+maxfila; 
            form1.submit();            
		}
		else{
		  
		    document.form1.action = "<%= CONTROLLER %>?estado=Factura&accion=Insert&cargar=ok&x="+factura+"&gasto=no&fec="+fec.value+"&numerofilas="+maxfila; 
            form1.submit(); 
		}
 }
}

function buscarCodigo( x ) {

 var cuenta =document.getElementById("textfield17"+x);
   document.form1.action = "<%= CONTROLLER %>?estado=Factura&accion=Insert&cargarcodigo=ok&x="+cuenta.value+"&campo=textfield17"+x+"&numerofilas="+maxfila; 
   form1.submit();
     
}

function exportar() {

 document.form1.action = "<%= CONTROLLER %>?estado=Factura&accion=Insert&Agregar=ok&opcion=EXPORTAR&numerofilas="+maxfila; 
 document.form1.submit();
     
}



function cargarRemesa( x ){
 if (window.event.keyCode==13){ 
  var remesa =document.getElementById("textfield11"+x);
  if( remesa.value.length != 0 ){
   buscarRemesa( x);
  }
  else{
   alert('Debe LLenar el Campo de la Remesa');
  }
 }
}
function cargarCodigo( x ){
 if (window.event.keyCode==13){ 
    if(validarcuenta(x)!=false){    
	  var remesa =document.getElementById("textfield17"+x);
	  if( remesa.value.length != 0 ){
	   buscarCodigo( x);
	  }
	  else{
	   alert('Debe LLenar el Campo del Codigo');
	  }
	}  
 }
}
// TEngo que verificar si esto esta bien 
/*function cargarfactura(){
 if (window.event.keyCode==13){
	   if(form1.mone.value == "PES" && form1.textfield.value != 1 && form1.textfield.value != ""){
		  document.form1.action = "<%= CONTROLLER %>?estado=Factura&accion=Tasa&Agregar=ok&actualizar=ok&numerofilas="+maxfila+"&causa=portasa"; 
			document.form1.submit(); 
	   }else if(form1.mone.value != "PES"){ 
		 aceptarFactura2('portasa');
	   }
 }
}	*/

function cargarfactura(){
 if (window.event.keyCode==13){ 
     aceptarFactura2('portasa');
 }
}	
function cargarcodigocuenta(a,b,c){
	var url= "<%= CONTROLLER %>?estado=Factura&accion=Insert&codigocuenta=ok&campo="+a+"&cliente="+b+"&nit="+c;
	window.open(url,'factura','status=yes,scrollbars=no,width=550,height=250,resizable=yes');
 }	
 
 function formatoDolar (obj, decimales){
   
     var numero = obj.value.replace( new RegExp(",","g"), "");

     var nums = ( new String (numero) ).split('.');

	 var salida = new String();

	 var TieneDec = numero.indexOf('.');

	 var dato = new String();

	 if( TieneDec !=-1  ){
		var deci = numero.split('.');
		var dec       = (deci[1].length >2)?deci[1].charAt(2):deci[1].substr(0,deci[1].length);
    	if(dec > 5){
        	dato =  (parseInt(deci[1].substr(0,2)) + 1);
     		if(dato>99){
       			nums[0] = new String (parseInt(nums[0])+1);
    			obj.value = nums[0]+'.00';
     		}else{
       			for (var i=nums[0].length-1, j=0; i>=0; salida = nums[0].charAt(i) + (j%3==0 && j!=0? ',':'') + salida , i--, j++);
          			obj.value = salida + (nums.length > 1 && decimales > 0 ? '.' +((nums[1].length >2)?((nums[1].charAt(2)>5)?(parseInt(nums[1].substr(0,2))+1):nums[1].substr(0,2)):(nums[1].length==1)?nums[1].substr(0,1)+'0':nums[1].substr(0,nums[1].length)) : '.00'); 
          	}
     	}else{
        	for (var i=nums[0].length-1, j=0; i>=0; salida = nums[0].charAt(i) + (j%3==0 && j!=0? ',':'') + salida , i--, j++);
        		obj.value = salida + (nums.length > 1 && decimales > 0 ? '.' +((nums[1].length >2)?((nums[1].charAt(2)>5)?(parseInt(nums[1].substr(0,2))+1):nums[1].substr(0,2)):(nums[1].length==1)?nums[1].substr(0,1)+'0':nums[1].substr(0,nums[1].length)) : '.00'); 
    	} 
		
    }else{
      for (var i=nums[0].length-1, j=0; i>=0; salida = nums[0].charAt(i) + (j%3==0 && j!=0? ',':'') + salida , i--, j++);
    	obj.value = salida + (nums.length > 1 && decimales > 0 ? '.' +((nums[1].length >2)?((nums[1].charAt(2)>5)?(parseInt(nums[1].substr(0,2))+1):nums[1].substr(0,2)):(nums[1].length==1)?nums[1].substr(0,1)+'0':nums[1].substr(0,nums[1].length)) : '.00'); 
 	  
	}
	
 }
 function validarcuenta(x){
      var cuenta =document.getElementById("textfield17"+x);
      if(cuenta.value==''){
	  return false;    
      }
	
 }
 function buscarAuxiliar(BASEURL, id, cuenta, tipo){
    var  cuen = document.getElementById("textfield17"+id);
	var tip = document.getElementById("tipo"+id); 
    if(cuen.value == '' || tip.value =='' ){
		alert("Verifique que la cuenta y el tipo tenga datos");
    }
	else{
		var dir =BASEURL+"/jsp/finanzas/contab/subledger/consultasIdSubledger.jsp?idcampo="+id+"&cuenta="+cuenta+"&tipo="+tipo+"&reset=ok";
		window.open(dir,'Auxiliar','width=700,height=520,scrollbars=no,resizable=yes,top=10,left=65,status=yes');
	}
}
function validarAuxiliar(BASEURL, id){
    
	var  cuen = document.getElementById("textfield17"+id);
	var tip = document.getElementById("tipo"+id);
    var aux = document.getElementById("auxiliar"+id);
	
    if(aux.value != ''){ 
    	if(cuen.value == '' || tip.value =='' ){
			alert("Verifique que la cuenta y el tipo tenga datos");
	    }
		else{
			enviar('?estado=IngresoMiscelaneoDetalle&accion=Buscar&evento=Auxiliar&cuenta=' + cuen.value+'&tipo='+tip.value+'&auxiliar='+aux.value+'&campo='+id )
		}
	}
}

function soloTexcoma(e) {
	//alert(window.event.keyCode)
	var key = (isIE) ? window.event.keyCode : e.which;
	var obj = (isIE) ? event.srcElement : e.target;
	var isNum = ((key > 0 && key < 32) || (key >32 && key<35) || (key>35 && key <37) || (key >38 && key <40)  ||  (key > 41 && key < 44) || (key > 47 && key < 48) || (key>59 && key<61)||(key > 61 && key < 65) || (key > 90 &&  key < 97) || (key > 122) || (key == 13) ) ? true:false;
	window.event.keyCode = (isNum && isIE) ? 0:key;
	e.which = (!isNum && isNS) ? 0:key;
	return (isNum);
}

function calcular(){
 var causa ="";
	    document.form1.action = "<%= CONTROLLER %>?estado=Factura&accion=Insert&Agregar=ok&actualizar=ok&numerofilas="+maxfila+"&causa="+causa; 
        document.form1.submit();         
	}
</script>
   <form name="form1" method="post" >
<table width="1000" border="2" align="center">
    <tr>
      <td width="100%" height="60">
     
          
        <table width="100%">
          <tr>
            <td width="49%" height="22" align="left" class="subtitulo1">&nbsp;Informaci&oacute;n de la Factura </td>
            <td width="51%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"align="left"><%=datos[0]%></td>
          </tr>
        </table>
        <table width="100%">
          <tr>
            <td width="141" class="fila">Cliente:</td>
            <td width="141" class="letra">              
              <input name="textfield2" id="textfield2" type="text" size="20" style="border:0"  value="<%= factu.getCodcli()==null? "&nbsp;" : factu.getCodcli()%>" class="filaresaltada" maxlength="10" readonly></td>
            <td colspan="2" class="fila">Agencia Facturacion:</td>
            <td colspan="2" class="letra"> 
			
			<%model.agenciaService.cargarAgencias();
			Vector agencias = model.agenciaService.getVectorAgencias(); 
			 for(int i=0; i< agencias.size();i++){
               Agencia agencia = (Agencia) agencias.get(i);
               
			 if(agencia.getId_agencia().equals(factu.getAgencia_facturacion())){%><%=agencia.getNombre()%><%}%> 
                	
           <%}%>
		    <input type="hidden" id="textfield5" name="textfield5" value="<%=factu.getAgencia_facturacion()%>">
                        
			</td>
            <td width="124" class="fila"><span class="letra">Moneda: </span></td>
			<td width="182" class="letra"><div align="left">
             <%TreeMap mo = model.facturaService.listarMonedas();%>
                	<input:select name="mone"  attributesText="id='select2' style='width:80%;' class='textbox'; onChange=aceptarFactura2('pormoneda');"  options="<%=mo%>" default="<%=factu.getMoneda()%>"/>
			 </div></td>
			 <script>
			 form1.mone.value='<%=factu.getMoneda()%>'
			 </script>
          </tr>
          <tr>
            <td class="fila">Nit:</td>
            <td class="letra">
              <input name="textfield3" id="textfield3" type="text" size="20" style="border:0"  value="<%=factu.getNit()==null? "&nbsp;" : factu.getNit()%>" class="filaresaltada" maxlength="15" readonly ></td>
            <td colspan="2" class="fila">Fecha Factura: </td>
			<td colspan="2" class="letra"><input name="c_fecha" type="text" class="textbox" id="c_fecha" size="14"  value="<%=factu.getFecha_factura()%>" height="">              </td>
            <td class="fila"> <span class="letra">Tasa: </span></td>
           
			<td class="letra"><input name="textfield" id='textfield'type="text " value="<%=UtilFinanzas.customFormat("#,###.#########",factu.getValor_tasa(),9)%>" size="15"  onKeyPress="soloDigitos(event,'decOK');" onKeyUp="cargarfactura()" maxlength='16'>              
			  <div align="left">              </div></td></tr>
		 
          <tr>
            <td class="fila">Forma de Pago: </td>
            <td class="letra">
              <select name="textfield4" id="textfield4" class='textbox'  >
                <%if(factu.getForma_pago().equals("CONTADO")){ %>
                <option value="CONTADO"selected  >CONTADO</option>
                <option value="CREDITO" >CREDITO</option>
                              
                <%}else{ %>
                <option value="CONTADO" >CONTADO</option>
                <option value="CREDITO" selected >CREDITO</option>
                <%}%>
              </select></td>
            <td width="77" class="fila">Plazo: </td>
            <td width="140" class="fila"><input name="c_fecha2" type="text" class="textbox" id="c_fecha2" size="3"  value="<%=factu.getPlazo()==null? "0" :factu.getPlazo()%>" onKeyPress="soloDigitos(event,'decNO')" maxlength="3"></td>
            <td width="57" class="fila">Rif:            </td>
            <td width="90" class="fila"><%=factu.getRif()%></td>
            <td class="fila">Valor Total: <span class="letra">
            <input name="textfield8" id="textfield85" type="hidden" size="10" style="border:0"  value="<%=factu.getPlazo()%>" onKeyPress="soloDigitos(event,'decNO')">
            </span></td>
            <td class="letra"><input name="textfield9" value='<%=(factu.getMoneda().equals("DOL"))?UtilFinanzas.customFormat2(factu.getValor_factura()):UtilFinanzas.customFormat(factu.getValor_factura())%>' class="filaresaltada" id="textfield95" type="text" size="30" style="border:0;"  onKeyPress="soloDigitos(event,'decNO')" readonly>&nbsp;<a onClick="calcular(); " style="cursor:hand;" class="Simulacion_Hiper" HIDEFOCUS>Total</a></td>
          </tr>
          <tr>
            <td class="fila">Formato Impresion: </td>
            <td class="letra">
			                  <select name="zona" >
							  <option value="" >Carga General</option>
							  <option value="FC" <%if(factu.getZona().equals("FC")){%> selected<%}%>>Carbon</option>
			                  </select>
			</td>
           <td class="fila">HC:<% model.tablaGenService.listarHc();TreeMap hcs = model.tablaGenService.getHcs(); %></td>
            <td class="fila"><input:select  name="ForHC" attributesText="style='width:90%;' class='listmenu'" options="<%=hcs%>" /><img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
            <td class="fila">&nbsp;</td>
            <td class="fila">&nbsp;</td>
            <td class="fila">&nbsp;</td>
            <td class="letra">&nbsp;</td>
          </tr>
      </table>
      </td>
    </tr>
  </table>
  
  <table width="1000" border="2" align="center">
    <tr>
      <td height="26"><table width="100%">
          <tr class="fila" id="cantidad">
            <td width="9%"  >
              <div align="center"></div>Descripcion:</td>
            <td  align="center" >            <div align="left">
                <textarea name="textarea" cols="100%" rows="2" onKeyPress="soloTexcoma(event)"><%=factu.getDescripcion()==null? "" : factu.getDescripcion()%></textarea>
            </div></td>
            </tr>
          <tr class="fila" id="cantidad">
            <td height="3"  >Observacion:</td>
            <td  align="center" ><div align="left">
              <textarea name="textarea2" cols="100%" rows="2" onKeyPress="soloTexcoma(event)"><%=factu.getObservacion()==null? "" : factu.getObservacion()%></textarea>
            </div></td>
            </tr>
      </table></td>
    </tr>
  </table>
  

  
  <table width="1000" border="2" align="center">
      <tr>
        <td  style="font-size:10px">  
        <% Vector a = model.facturaService.getRemesasFacturar();
           int x = ( !com.tsp.util.Util.coalesce(request.getParameter("item_buscar"),"").equals(""))? Integer.parseInt(request.getParameter("item_buscar")):1; 
          %>
          <input name='item' value='<%=x%>' type="hidden">
          <table width="100%" align="center">
            <tr>
              <td width="45%" height="22" align="center" class="subtitulo1">&nbsp;Detalle del Documento  </td>
              <td width="5%" align="center" class="barratitulo"><div align="left"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></div></td>
              <td width="15%" align="center" class="fila">Total Item: <%=(a!= null && a.size()!=0)?a.size():1%> </td>
               <td width="19%" align="center" class="barratitulo"><input type="text" name="item_buscar" id="item_buscar" style="font-size:px; " size='7' value='<%=x%>' >&nbsp;<a onClick="BuscarItem('<%=CONTROLLER%>',form1.item_buscar.value,'<%=x%>'); " style="cursor:hand;" class="Simulacion_Hiper" HIDEFOCUS>Buscar Item</a>&nbsp;</td>
              <td width="16%" align="center" class="barratitulo"><a onClick="abrir('<%=CONTROLLER%>'); " style="cursor:hand;" class="Simulacion_Hiper" HIDEFOCUS>Agregar Items</a>&nbsp;</td>
            </tr>
          </table>
          <div align="center">
            <table id="detalle" width="100%" align="center" >
              <tr  id="fila1" class="tblTituloFactura">
                <td align="center">
				<table width="79%" height="37" border="1" align="center"  bordercolor="#999999" bgcolor="#F7F5F4">
				<tr class="tblTitulo">
				 <td width="50" nowrap style="font-size:13px "><div align="center">Item</div></td>
				</tr>
				</table>
				</td>
                <td align="center"> 
				  <table width="100%" border="1"  bordercolor="#999999" bgcolor="#F7F5F4" align="center">
				  <tr class="tblTitulo">
               
                  <td width="56" nowrap style="font-size:11px "><div align="center">Remesa</div></td>
                  <td width="57" nowrap style="font-size:11px "><div align="center">Fecha</div></td>
                  <td width="219" align="center" style="font-size:11px ">Descripcion</td>
                  <td width="93" align="center" style="font-size:11px "><div align="center">Cantidad</div></td>
                  <td width="129" align="center" style="font-size:11px "><div align="center">Valor Unitario </div></td>
                  <td width="126" align="center" style="font-size:11px "><div align="center">Valor</div></td>
                  <td width="190" align="center" style="font-size:9px "> Codigo Cuenta <br>Tipo &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp Auxiliar</td>
				  </tr>
				  </table>
				</td>
              </tr>
              
              
              <% 
                  
				
	   if ( a != null || a.size()>0){
			            
		int itemFin = (a.size()<x+19)?a.size():x+19;
                  %><script>
                       maxfila='<%=itemFin%>';
		       item_ini ='<%=x%>';
	  	    </script>
		         
                      <%
		for( x=x ;x<=itemFin;x++){
		    
		       factura_detalle factu_deta = (factura_detalle)a.get(x-1);
			//   total+=factu_deta.getValor_item();
				%>
				
				<tr class="<%=(x%2!=0)?"fila":"filagris"%>" id="filaItem<%=x-1%>" nowrap  bordercolor="#D1DCEB" >
                <td class="bordereporte">  
				<span> <a id='n_i<%=x-1%>' ><%=x%></a>
                  <a onClick="buscarUltimo('<%=CONTROLLER%>',form1.item_buscar.value,item_ini);" id="insert_item<%=x-1%>" style="cursor:hand"  ><img src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width="12" height="12" ></a>
				   <a onClick="borrarItem('<%=x-1%>');" id="borrarI<%=x-1%>" style="cursor:hand" ><img src='<%=BASEURL%>/images/botones/iconos/menos1.gif'   width="12" height="12"  > </a>
				</span>
				
				</td>  <td>
                <table width='100%' class='tablaInferior'>
			<tr class='<%=(x%2!=0)?"fila":"filagris"%>'  >
			<td width='5%'><div align='center'>
			<%System.out.println("remesa--->"+factu_deta.getNumero_remesa());
			System.out.println("fecha->"+factu_deta.getFecrem());
			System.out.println("desc->"+factu_deta.getDescripcion());
			System.out.println("cantidad->"+factu_deta.getCantidad());
			System.out.println("unidad->"+factu_deta.getUnidad());
			System.out.println("tasa->"+factu_deta.getValor_tasa());
			
			System.out.println("val unitario->"+factu_deta.getValor_unitario());
			
			System.out.println("moneda->"+factu.getMoneda ());
			System.out.println("val item->"+factu_deta.getValor_item());
			%>
			
              <input name='textfield11<%=x-1%>' id='textfield11<%=x-1%>' value="<%=factu_deta.getNumero_remesa()%>" type='text' size='7' onChange="buscarRemesa('<%=x-1%>')" style="font-size:11px " onKeyPress="soloAlfa(event)" maxlength="10" onBlur="if(this.value==''){this.value='<%=factu_deta.getNumero_remesa()%>'}">
</div></td>
             <td width='7%'>
			   <input name='textfield12<%=x-1%>' id='textfield12<%=x-1%>' value="<%=factu_deta.getFecrem()%>" type='text' size='8'  readonly="" style="font-size:11px;border:0 " >
             <td width='23%'>              <div align='left'>
               <textarea name="textfield13<%=x-1%>" cols="40" rows="3" id="textfield13<%=x-1%>"  class="textbox" style="font-size:11px " onKeyPress="soloAlfa(event)"><%=factu_deta.getDescripcion()%></textarea>
             </div></td>
			 <td width="12%">
               <input name='textfield14<%=x-1%>' id='textfield14<%=x-1%>' type='text' size='7' style='text-align:right;font-size:11px ' onBlur="if(this.value !=''){formatoDolar(this,2);}actualizar();"onFocus='sinformato(this);' value="<%=Util.customFormat(factu_deta.getCantidad())%>" onKeyPress="soloDigitos(event,'decOK');procesar(this);" maxlength="16">
               <input name='textfield18<%=x-1%>' id='textfield18<%=x-1%>' type='text' size='1' value="<%=factu_deta.getUnidad()%>" readonly=""style="font-size:11px;border:0 ">
                     <input name="hiddenField<%=x-1%>" type="hidden" id="hiddenField<%=x-1%>" value="<%=factu_deta.getValor_tasa()%>"></td>
              <td width='16%'><input name='textfield15<%=x-1%>' id='textfield15<%=x-1%>' type='text' size='20' style='text-align:right;font-size:11px' onfocus='sinformato(this);' onChange="formatoDolar(this,2);" onKeyPress="soloDigitos(event,'decOK');" onBlur="actualizar();" value='<%=(factu.getMoneda ().equals("DOL"))?UtilFinanzas.customFormat2(factu_deta.getValor_unitario()):UtilFinanzas.customFormat2(factu_deta.getValor_unitario())%>' maxlength='16'>
			    <input name='textfield19<%=x-1%>' id='textfield19<%=x-1%>' type='hidden' value="<%=factu.getMoneda()%>" >
                <input name="hiddenField2<%=x-1%>" type="hidden" id="hiddenField2<%=x-1%>" value="<%=factu_deta.getValor_unitariome()%>"></td>
               <td width='14%'>                 <div align="right">
                 <input name='textfield16<%=x-1%>' id='textfield16<%=x-1%>' value='<%=(factu.getMoneda ().equals("DOL"))?UtilFinanzas.customFormat2(factu_deta.getValor_item()):UtilFinanzas.customFormat(factu_deta.getValor_item())%>' type='text' size='20' style='border:0;text-align:right;font-size:11px' class='filaazul' readonly>
                 
               <input name="hiddenField3<%=x-1%>" type="hidden" id="hiddenField3<%=x-1%>" value="<%=factu_deta.getValor_itemme()%>">
                 </div></td>
               <td width='23%'><div align='center'><span class='letra'>
                <input name='textfield17<%=x-1%>' type='text' id='textfield17<%=x-1%>' size='28' style="font-size:9px " maxlength='28' value ="<%=(factu_deta.getCodigo_cuenta_contable()!=null)?factu_deta.getCodigo_cuenta_contable():""%>" <%=(factu_deta.getCodigo_cuenta_contable().equals("130505030601")||factu_deta.getCodigo_cuenta_contable().equals("130510030601"))?"readonly":""%> onKeyUp="cargarCodigo('<%=x-1%>')"  onBlur="if(this.value!='130510030601'){if(this.value!='130505030601'){if(textfield12<%=x-1%>.value.length < 25){if(validarcuenta('<%=x-1%>')!=false){buscarCodigo('<%=x-1%>')}}}}">
              </span><img src="<%=BASEURL%>/images/botones/iconos/notas.gif" width="15" height="15" style="cursor:hand;<%=(factu_deta.getCodigo_cuenta_contable().equals("130505030601")||factu_deta.getCodigo_cuenta_contable().equals("130510030601")||factu_deta.getFecrem().length()>25)?"visibility:hidden":""%>" onClick="cargarcodigocuenta('textfield17<%=x-1%>','<%=factu.getCodcli()/*20100525.substring(3,6)*/%>','<%=factu.getNit()%>')">
			  
			   <%if(factu_deta.getAux()==null){
			       factu_deta.setAux("N");
			   }
			   
			  // if(factu_deta.getAux().equals("S")){
			     LinkedList tsa = new LinkedList();
				 if(factu_deta.getTiposub()!=null){
			     tsa=factu_deta.getTiposub();}%>
			   <br>
			
			 <select name="tipo<%=x-1%>" id="tipo<%=x-1%>" class='textbox'  style="font-size:9px">
			 <option value=''></option>
              <% for(int i = 0; i<tsa.size(); i++){
                    TablaGen tipo = (TablaGen) tsa.get(i); %>
                    <option value='<%=tipo.getTable_code()%>' <%= (tipo.getTable_code().equals(factu_deta.getTiposubledger()))?"selected":""%> ><%=tipo.getTable_code()%></option>
             <%}%>
			  
              </select>
               <input name='auxiliar<%=x-1%>' type='text' id='auxiliar<%=x-1%>' size='20' style="font-size:9px " maxlength='25'  onBlur="validarAuxiliar('<%=BASEURL%>','<%=x-1%>')" value="<%=factu_deta.getAuxliliar()%>">
               <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name="imagenAux" width="15" height="15"  id="imagenAux" style="cursor:hand"  onClick="buscarAuxiliar('<%=BASEURL%>','<%=x-1%>',form1.textfield17<%=x-1%>.value, form1.tipo<%=x-1%>.value);"  > 
               
</div></td>
			<%//}//fin if(factu_deta.getAux().equals("S"))%>
			<input name="aux<%=x-1%>" type="hidden" id="aux<%=x-1%>" value="<%=factu_deta.getAux()%>">
			</tr></table></td>
			
              </tr>
				
               <%}     
                       }%>
			   
			   
            </table>
        </div></td>
      </tr>
    </table>
	
	<p align="center"><img src="<%=BASEURL%>/images/botones/iconos/guardar.gif" name="imgaceptar" width="22" height="22" style="cursor:hand" onClick="this.disabled=true; guardarFactura();" >
	<img src="<%=BASEURL%>/images/botones/aceptar.gif"  height="21" name="imgaceptar"  onMouseOver="botonOver(this);" onClick="if(validaritems()){aceptarFactura();}" onMouseOut="botonOut(this);" style="cursor:hand" ></tab>
	<img src="<%=BASEURL%>/images/botones/exportarExcel.gif"  height="21" name="imgexportar"  onMouseOver="botonOver(this);" onClick="exportar()" onMouseOut="botonOut(this);" style="cursor:hand" >
        <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
      <img src="<%=BASEURL%>/images/botones/regresar.gif"  height="21" name="imgregresar"  onMouseOver="botonOver(this);" onClick="this.disabled=true;regresar();" onMouseOut="botonOut(this);" style="cursor:hand">  </p>
 
  <p>
  <%if(!Mensaje.equals("")){
  %>
</p>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="400" align="center" class="mensajes"><%= Mensaje %></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</form >
</p>
 <%}%>
</div>
<%=datos[1]%>

</body>
<script>
    function enviar(url){
         var a = "<iframe name='ejecutor' style='visibility:hidden'  src='<%= CONTROLLER %>" + url + "'> ";
         aa.innerHTML = a;
    }

</script>
<font id='aa'></font>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</html>
