<!--
- Autor : Ing. Juan Manuel Escando Perez
- Date  : 29 Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite buscar una planilla para la impresion de la hoja de control de viaje
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
    String Mensaje = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
	factura a = model.facturaService.leerArchivo("factura"+usuario.getLogin ()+".txt",usuario.getLogin ());
	Vector b = model.facturaService.leerArchivoItems("factura"+usuario.getLogin ()+".txt",usuario.getLogin ());
    if(a!=null && b!= null){
	    model.facturaService.buscarRemesas(a.getCodcli(),"a.reg_status = 'C'",usuario.getDstrct());
	}
%>
<html>
<head><title>Facturacion</title></head>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src='<%=BASEURL%>/js/validacionesImpresionPlanilla.js'></script>
<script>

function abrirFactura(){
    window.location.href="<%=BASEURL%>/jsp/cxcobrar/facturacion/FacturacionAceptada.jsp";
   
}
</script>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Facturacion"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
	<form action="<%= CONTROLLER %>?estado=Factura&accion=Insert&busqueda=ok" method="post" name="form1" >
		<table align="center" width="350" border="2">
			<tr>
				<td>
					<TABLE width="100%" align="center"  class="tablaInferior">
				        <tr class="fila">
							<td width="50%" class="subtitulo1">&nbsp;Facturacion</td>
							<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
						</tr>
						<tr class="fila">
							<td align="center">Codigo Cliente:</td>
							<td><input name='codcli' type='text' class="textbox" id="codcli" size="10" maxlength="10">
						    <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"> <a onClick="window.open('<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=consultasCliente.jsp&marco=no&carpeta=/consultas','','status=yes,scrollbars=no,width=650,height=500,resizable=yes')" style="cursor:hand"><img height="20" width="25" src="<%=BASEURL%>/images/botones/iconos/user.jpg"></a></td>
						</tr>
						<tr class="fila">
						  <td align="center">Incluir no cumplidas </td>
						  <td><input name="cumplidas" type="checkbox" id="cumplidas"  value="S" ></td>
					  </tr>
					</TABLE>
				</td>
			</tr>
		</table>
		<%if(a!=null && b!= null){%>
		<table border="2" align="center">
			<tr>
				<td>
					<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						<tr>
							<td width="229" align="center" class="mensajes">Factura Recuperada<%= Mensaje %></td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58"><div align="center"><img src="<%=BASEURL%>/images/page.gif" name="imgaceptar" width="22" height="22" style="cursor:hand" onClick="this.disabled=true; abrirFactura();" ></div></td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	<%}%>
		  <br>
		<p align="center">
			<input type="image" src="<%=BASEURL%>/images/botones/buscar.gif" style="cursor:hand" name="Aceptar_" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" >&nbsp;
			<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >&nbsp;
		</p>
	</form>
	<br>
	<%--Mensaje de informacion--%>
	<%if(!Mensaje.equals("")){%>
		<table border="2" align="center">
			<tr>
				<td>
					<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						<tr>
							<td width="229" align="center" class="mensajes"><%= Mensaje %></td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	<%}%>
</div>
<%=datos[1]%>
</body>
</html>
