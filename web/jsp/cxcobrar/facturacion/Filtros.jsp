<!--
- Autor : Ing.Andres Martinez Grces
- Date  : 03 junio 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite aplicar filtros a una nueva factura para cleintes
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
    String Mensaje = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
    factura factu = model.facturaService.getFactu();
	Vector a = model.facturaService.getRemesas();
	Vector sj = model.facturaService.getStandar();
	
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
	
%>
<html>
<head><title>Facturacion</title></head>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>

<script>
   
    function regresar(){
	
	   document.form1.action = "<%= CONTROLLER %>?estado=Factura&accion=Insert&regresar=ok"; 
       document.form1.submit();     
	}
    function additem(ite,combo){
		var Ele = document.createElement("OPTION");
        Ele.value=ite;
        Ele.text=ite;
		combo.add(Ele);
	}
	function rutas(chec){
	
	     var combo =document.getElementsByName("ruta")[0];
	    if(chec==true){
		    combo.style.visibility='visible';
		    combo.length=0;    
			
			<%Vector b= new Vector();
			for(int i =0; i<a.size();i++){
			       
				   factura_detalle fd = (factura_detalle)a.get(i);
				
			       
				   String ciu1=fd.getOrigen();
				  
				   String ciu2=fd.getDestino();
				   String g=ciu1+"-"+ciu2;
				   fd.setNomorigendestino(g); 
				   int c=0;
				   for(int j=0; j<b.size();j++){
				       String rutilla =(String)b.get(j);
					   if(rutilla.equals(g))
					       c++;
				   }
				   if(c==0){
				       b.add(g);
				   %>  additem('<%=g%>',combo);
	             <%}
			}%>
			
		}
		else	    combo.style.visibility='hidden';  
    }
	function rangos(chec){
	
	    var combo1 =document.getElementById("rango1");
		var combo2 =document.getElementById("rango2");
	    if(chec==true){
		
		    combo1.style.visibility='visible';
		    combo1.length=0; 
			combo2.style.visibility='visible';
		    combo2.length=0; 
			<%Vector c= new Vector();
			for(int i =0; i<sj.size();i++){
			       
				   factura_detalle fd = (factura_detalle)sj.get(i);
				  
				   String g=fd.getStd_job_no();
				   int d=0;
				   for(int j=0; j<c.size();j++){
				       String rutilla =(String)c.get(j);
					   if(rutilla.equals(g))
					       d++;
				   }
				   if(d==0){
				       c.add(g);
				   %>  additem('<%=g%>',combo1);
					   additem('<%=g%>',combo2);
	             <%}
			}%>
		}
		else{
		    combo1.style.visibility='hidden'; 
			combo2.style.visibility='hidden';
		}	 	   
	}
	function formato2digitos( num ){
    		return (num<10? '0':'') + num;
  		}
	function formato (fecha){
			return fecha.getYear() + '-' + formato2digitos(fecha.getMonth()+1) + '-' + formato2digitos(fecha.getDate());
	}
		
    function fechas(chec){
	
	    var combo1 =document.getElementById("c_fecha");
		var combo2 =document.getElementById("c_fecha2");
		var f1 =document.getElementById("f1");
		var f2 =document.getElementById("f2");
	    if(chec==true){
		    combo1.style.visibility='visible';
		    combo2.style.visibility='visible';
			f1.style.visibility='visible';
		    f2.style.visibility='visible';
		    var fechaActual  =  new Date();
			combo1.value= formato(fechaActual);
			combo2.value= formato(fechaActual);
		}
		else{
		    combo1.style.visibility='hidden'; 
			combo2.style.visibility='hidden';
			f1.style.visibility='hidden'; 
			f2.style.visibility='hidden';
			
		}	 	   
	}
	function productos(chec){
	
	    var combo =document.getElementsByName("producto")[0];
	    if(chec==true){
		    combo.style.visibility='visible';
		    combo.length=0;    
			
			<%Vector d= new Vector();
			for(int i =0; i<a.size();i++){
			
				   factura_detalle fd = (factura_detalle)a.get(i);
				   
				   String g=fd.getCodtipocarga();
				   int e=0;
				   for(int j=0; j<d.size();j++){
				       String rutilla =(String)d.get(j);
					   if(rutilla.equals(g))
					       e++;
				   }
				   if(e==0){
				       d.add(g);
				   %>  additem('<%=g%>',combo);
	             <%}
			}%>
			
		}
		else
		    combo.style.visibility='hidden';  
    }
	function documentos(chec,url){
	
	    var combo =document.getElementById("documento");
		var combo1 =document.getElementById("docu1");
	    if(chec==true){
		    combo.style.visibility='visible';
		    combo.length=0;    
			
			<%Vector e= model.facturaService.getVtipodoc();
			for(int i=0; i<e.size();i++){
			    TablaGen tg= (TablaGen)e.get(i);
			    String g1=tg.getTable_code();
                String g2=tg.getDescripcion();%>
				
				var Ele = document.createElement("OPTION");
				Ele.value='<%=g1%>';
				Ele.text='<%=g2%>';
				combo.add(Ele);
			<%}%>
			url+="&tipo="+combo[0].value;
			var a = "<iframe name='ejecutor'  style='visibility:hidden' src='" + url + "'> ";
            aa.innerHTML = a;
		}
		else{
		    combo.style.visibility='hidden';  
		    combo1.style.visibility='hidden'; 
		}	
    }
    function enviar(url){
         var a = "<iframe name='ejecutor'  style='visibility:hidden' src='" + url + "'> ";
         aa.innerHTML = a;
    }
	function validar(){
	    var combo1 =document.getElementById("checkbox5");  
		if(combo1.checked==true){
		       var f1 =document.getElementById("c_fecha"); 
			   var f2 =document.getElementById("c_fecha2");
			   if(f2.value<f1.value){
			        alert('La fecha inicial debe ser igual o menor que la fecha final');  
					return false; 
			   } 
		}
		
	    var combo2 =document.getElementById("checkbox");
		if(combo2.checked==true){
		    var rut =document.getElementsByName("ruta")[0];  
			if(rut.value==''){
			        alert('la ruta no debe ser vacia');  
					return false; 
			} 
		}
		
		var combo3 =document.getElementById("checkbox2");
		if(combo3.checked==true){
		 var r1 =document.getElementById("rango1");  
		 var r2 =document.getElementById("rango2");  
			if(r1.value=='' || r2.value==''){
			        alert('los rangos no pueden ser vacios');  
					return false; 
			} 
		}
		
		var combo4 =document.getElementsByName("checkbox3")[0];
		if(combo4.checked==true){
		   var pro =document.getElementById("select2");  
			if(pro.value=='' ){
			        alert('El producto no puede ser vacio');  
					return false; 
			} 
		}
		
		var combo5 =document.getElementsByName("checkbox4")[0];
		if(combo5.checked==true){
		 var docu =document.getElementById("documento");  
		 var docu1 =document.getElementById("docu1");  
			if(docu.value=='' || docu1.value==''){
			        alert('El tipo documento o documento no puede ser vacio');  
					return false; 
			} 
		}
	    document.form1.action = "<%= CONTROLLER %>?estado=Factura&accion=Insert&filtrar=ok"; 
	    
	    var chk = document.getElementById("chk_stdadd");
            var std = document.getElementById("txtstdadd");
            if(chk.checked==true && std.value == ''){
                alert("Debe ingresar los estandares que desea consultar");
                std.focus();
                return false;
            }
            
        document.form1.submit();   
	}
	
	function validarStdAdd(){
                
	}
	
	function enable_stdadd(chec){
	
	    var std =document.getElementsByName("stdadd")[0];
	    if(chec==true){
		std.style.visibility='visible';
            }else{
                std.style.visibility='hidden';
            }            
        }
    
</script>
<body onLoad="" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;"> 
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Filtros Facturacion"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
	<form name="form1" action="<%= CONTROLLER %>?estado=Factura&accion=Insert&filtrar=ok" method="post">
	    
		<table width="664" border="2" align="center" >
		<tr>
		<td>
         <table width="100%" border="0" >
    
             
                <tr class="fila">
                  <td width="24%" height="22%" class="subtitulo1">&nbsp;Facturacion</td>
                  <td colspan="3" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"align="left"><%=datos[0]%></td>
                </tr>
                <tr class="fila">
                  <td height="21%" align="center"><div align="left">Cliente:</div></td>
                  <td colspan="3" align="center"><div align="left"><%=factu.getNomcli()==null? "&nbsp;" : factu.getNomcli()%></div></td>
                </tr>
                <tr class="fila">
                  <td height="21%" align="center"><div align="left">Nit:</div></td>
                  <td colspan="3" align="center"><div align="left"><%=factu.getNit()==null? "&nbsp;" : factu.getNit()%></div></td>
                </tr>
                <tr class="fila">
                  <td height="22%" class="subtitulo1">&nbsp;Filtros</td>
                  <td colspan="3" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                </tr>
                <tr class="fila">
                  <td height="13%" rowspan="2" align="center"><div align="left">Rango fechas Remesas : </div></td>
                  <td width="5%" height="13%" rowspan="2" align="center"><input name="checkbox5" type="checkbox" id="checkbox5" onClick="fechas(this.checked)" value="" ></td>
                  <td align="center"><div align="left"></div>
                      <div align="left">Inicial:</div></td>
                  <td align="center"><div align="left">
                    <input name="c_fecha" type="text" class="textbox" id="c_fecha" size="14" readonly value="" style="visibility:hidden">
					<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.c_fecha);return false;" HIDEFOCUS> 
                    <img src="<%=BASEURL%>\js\Calendario\cal.gif"  id="f1" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"  style="visibility:hidden"> 
                  </div></td>
                </tr>
                <tr class="fila">
                  <td align="center"><div align="left">Final:</div></td>
                  <td align="center"><div align="left">
                    <input name="c_fecha2" type="text" class="textbox" id="c_fecha2" size="14" readonly value="" style="visibility:hidden">
					<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.c_fecha2);return false;" HIDEFOCUS> 
                    <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" id="f2" height="16" border="0" alt="De click aqui para escoger la fecha"  style="visibility:hidden">
                  </div></td>
                </tr>
                <tr class="fila">
                  <td height="13%" align="center"><div align="left"> Ruta : </div></td>
                  <td width="5%" height="13%" align="center"><input type="checkbox" name="checkbox" id="checkbox" value="" onClick="rutas(this.checked)"></td>
                  <td align="center">Ciudad Origen - Ciudad Destino: </td>
                  <td align="center"><div align="left">
                    <select name="ruta" id="select" class='textbox' onChange="" style="visibility:hidden"  >
                    </select>
                  </div></td>
                </tr>
                <tr class="fila">
                  <td height="31%" rowspan="2" align="center"><div align="left">Rango de Estandares : </div></td>
                  <td rowspan="2" align="center"><input type="checkbox" name="checkbox2" id="checkbox2" value="" onClick="rangos(this.checked)"></td>
                  <td width="32%" height="31%" align="center"><div align="left">Inicial:</div></td>
                  <td width="39%" align="center"><div align="left">
                    <select name="rango1" id="rango1" class='textbox' onChange="" style="visibility:hidden"  >
                    </select>
                  </div></td>
                </tr>
                <tr class="fila">
                  <td height="21%" align="center"><div align="left">Final:</div></td>
                  <td align="center"><div align="left">
                    <select name="rango2" id="rango2" class='textbox' onChange="" style="visibility:hidden"  >
                    </select>
                  </div></td>
                </tr>
                
                 <tr class="fila">
                  <td height="21%" align="center"><div align="left">Estandares Espec�ficos : </div></td>
                  <td align="center"><input type="checkbox" name="chk_stdadd" id="chk_stdadd" title='Estandares separados por coma' value="" onClick="enable_stdadd(this.checked)"></td>
                  <td align="center"><div align="left">N�mero estandard(s):</div></td>
                  <td align="center"><div align="left" id="stdadd" style="visibility:hidden" >
                    <input type="textbox" name="stdadd" id="txtstdadd" class='textbox'  size='35' title='Estandares separados por coma'>
                    
</div></td>
                </tr>
                
                <tr class="fila">
                  <td height="21%" align="center"><div align="left">Producto : </div></td>
                  <td align="center"><input type="checkbox" name="checkbox3" value="" onClick="productos(this.checked)"></td>
                  <td align="center"><div align="left">Codigo:</div></td>
                  <td align="center"><div align="left">
                    <select name="producto" id="select2" class='textbox' onChange="" style="visibility:hidden"  >
                    </select>
</div></td>
                </tr>
                <tr class="fila">
                  <td rowspan="3" align="center"><div align="left">Tipo de Documento : </div>                    
                    <div align="left"></div></td>
                  <td rowspan="3" align="center"><input type="checkbox" name="checkbox4" value="" onClick="documentos(this.checked,'<%= CONTROLLER %>?estado=Factura&accion=Insert&cargarDocumento=ok')"></td>
                  <td align="center"><div align="left">Tipo Documento: </div></td>
                  <td align="center"><div align="left">
                    <select name="documento" id="documento" class='textbox' onChange="enviar('<%= CONTROLLER %>?estado=Factura&accion=Insert&cargarDocumento=ok&tipo='+this.value)" style="visibility:hidden" >
                    </select>
                  </div></td>
                </tr>
                <tr class="fila">
                  <td align="center"><div align="left">Documento</div></td>
                  <td align="center"><div align="left" id="comb">
				     
					
                  </div></td>
                </tr>
               

         </table>
		 </td>
		 </tr>
      </table>
	    <br>
		<p align="center">
			<img src="<%=BASEURL%>/images/botones/aceptar.gif" title="Buscar Remesas" style="cursor:hand" name="Aceptar_" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="validar();">
			&nbsp;<img src="<%=BASEURL%>/images/botones/regresar.gif"  height="21" name="imgregresar"  onMouseOver="botonOver(this);" onClick="this.disabled=true;regresar();" onMouseOut="botonOut(this);" style="cursor:hand">
			<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >&nbsp;
		</p>
	</form>
	<br>
	<%--Mensaje de informacion--%>
	<%if(!Mensaje.equals("")){%>
		<table border="2" align="center">
			<tr>
				<td>
					<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						<tr>
							<td width="229" align="center" class="mensajes"><%= Mensaje %></td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	<%}%>
</div>
<%=datos[1]%>
</body>
 <font id='aa'></font>
 <iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</html>
