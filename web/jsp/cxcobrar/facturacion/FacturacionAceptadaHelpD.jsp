<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Descripcion de campos ingresar despacho</title>


<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<% String BASEIMG = BASEURL +"/images/botones/"; %> 
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">NUEVA FACTURA</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Cuentas por pagar    </td>
        </tr>
        <tr>
          <td  class="fila">Cliente:</td>
          <td  class="ayudaHtmlTexto">Muestra el codigo del cliente </td>
        </tr>
        <tr>
          <td  class="fila">Nit:</td>
          <td  class="ayudaHtmlTexto">Codigo nit del cliente. </td>
        </tr>
        <tr>
          <td  class="fila">Forma de pago :</td>
          <td  class="ayudaHtmlTexto">Campo de texto a&ntilde;adir / modificar forma de pago del cliente. </td>
        </tr>
        <tr>
          <td  class="fila">Agencia Facturacion:</td>
          <td  class="ayudaHtmlTexto">Campo de texto a&ntilde;adir / modificar la agencia de facturacion. </td>
        </tr>
        <tr>
          <td  class="fila">Fecha Factura:<span class="letra"><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></span></td>
          <td  class="ayudaHtmlTexto">Link para que despliega un calendario para cambiar la fecha de la factura. </td>
        </tr>
        <tr>
          <td  class="fila">Fecha Vencimiento: <span class="letra"><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></span></td>
          <td  class="ayudaHtmlTexto">Link para que despliega un calendario para cambiar la fecha de vencimiento. </td>
        </tr>
        <tr>
          <td  class="fila"><span class="letra">Moneda:
            <select name="select" id="select3" class='textbox' onChange="" >
            </select>
          </span></td>
          <td  class="ayudaHtmlTexto">Listas que despliega todos las monedas que maneja la agencia. </td>
        </tr>
        <tr>
          <td  class="fila"><span class="letra">Tasa: </span></td>
          <td  class="ayudaHtmlTexto">Campo de texto a&ntilde;adir / modificar forma la tasa que corresponde a la conversion de la moneda escojido con la del distrito. </td>
        </tr>
        <tr>
          <td  class="fila">Codigo:
            <select name="producto" id="select2" class='textbox' onChange="" >
            </select></td>
          <td  class="ayudaHtmlTexto">Listas que despliega todos los codigos de productos de las remesas del cliente. </td>
        </tr>
        <tr>
          <td  class="fila">Valor Total: <span class="letra"> </span></td>
          <td  class="ayudaHtmlTexto">Muestra el valor total de la factura en la moneda del distrito. </td>
        </tr>
        <tr>
          <td  class="fila">Descripcion:</td>
          <td  class="ayudaHtmlTexto">Campo de texto a&ntilde;adir / modificar una descripcion de la factura. </td>
        </tr>
        <tr>
          <td  class="fila">Observacion:</td>
          <td  class="ayudaHtmlTexto">Campo de texto a&ntilde;adir / modificar una observacion de la factura. </td>
        </tr>
        <tr>
          <td class="fila"> Boton Buscar <img src="<%=BASEURL%>/images/botones/buscar.gif"></td>
          <td  class="ayudaHtmlTexto">Boton para aceptar el cliente.</td>
        </tr>
        <tr>
          <td width="556" class="fila">Boton Salir <img src="<%=BASEIMG%>salir.gif"></td>
          <td width="556"  class="ayudaHtmlTexto">Boton para salir de la vista , regresando a control trafico.</td>
        </tr>
        <tr>
          <td class="subtitulo1"><span class="subtitulo1">Detalle del Documento </span></td>
          <td width="556"  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td class="fila">Item<span class="bordereporte"><span><img src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width="12" height="12" ><img src='<%=BASEURL%>/images/botones/iconos/menos1.gif'   width="12" height="12"  ></td>
          <td width="556"  class="ayudaHtmlTexto">Botones mas y menos, los cuales sirven para agregar nuevo item y eliminar items, respectivamente. </td>
        </tr>
        <tr>
          <td class="fila">Remesa</td>
          <td width="556"  class="ayudaHtmlTexto">Campo de texto a&ntilde;adir / modificar el codigo del item. </td>
        </tr>
        <tr>
          <td class="fila">Fecha</td>
          <td width="556"  class="ayudaHtmlTexto">Campo de texto a&ntilde;adir / modificar la fecha del item. </td>
        </tr>
        <tr>
          <td class="fila">Descripcion</td>
          <td width="556"  class="ayudaHtmlTexto">Campo de texto a&ntilde;adir / modificar una descripcion del item.</td>
        </tr>
        <tr>
          <td class="fila">Cantidad</td>
          <td width="556"  class="ayudaHtmlTexto">Campo de texto a&ntilde;adir / modificar cantidad del item.</td>
        </tr>
        <tr>
          <td class="fila">Valor Unitario</td>
          <td width="556"  class="ayudaHtmlTexto">Campo de texto a&ntilde;adir / modificar valor unitariodel item.</td>
        </tr>
        <tr>
          <td class="fila">Valor</td>
          <td width="556"  class="ayudaHtmlTexto">Campo de texto a&ntilde;adir / modificar valor total del item.</td>
        </tr>
        <tr>
          <td class="fila">Codigo Cuenta</td>
          <td width="556"  class="ayudaHtmlTexto">Campo de texto a&ntilde;adir / modificar codigo cuenta contable del item.</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
