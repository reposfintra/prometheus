<!--
- Autor : Ing. Diogenes Bastidas Morales
- Date  : 28 de noviembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el ingreso de telefonos

--%>
<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">

<title>Codigo cuenta contable</title>
<script language="javascript" src="<%=BASEURL%>/js/validar.js"></script>
<script language="javascript" src="<%=BASEURL%>/js/boton.js"></script>
<script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body onLoad="<%=(request.getParameter("Mensaje")!=null)?"aceptar3();":""%><%=(request.getParameter("close")!=null)?"aceptar2();":""%>">
<%
String campo = (request.getParameter("campo")!=null)?request.getParameter("campo"):"";
String Mensaje = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
String cliente = (request.getParameter("cliente")!=null)?request.getParameter("cliente"):"";
String nit = (request.getParameter("nit")!=null)?request.getParameter("nit"):"";
String cuenta = (request.getParameter("cuenta")!=null)?request.getParameter("cuenta"):" _IT_ ";
String [] tip = cuenta.split("_IT_"); 
System.out.println("campo:"+campo+" cliente:"+cliente+" nit:"+nit);
Vector uni=model.facturaService.getTunidades();
String descripcion = (request.getParameter("descripcion")!=null)?request.getParameter("descripcion"):"";	
Vector ele=model.facturaService.getTelementos();
 %> 
<script>
function aceptar1(forma,nit,cliente){
	if(forma=='forma1'){
	    var c=document.forma.ind; 
		document.forma.action = "<%= CONTROLLER %>?estado=Factura&accion=Insert&Verificarcuenta=ok&forma=1&cuenta=28150502"+c.value+"&nit="+nit+"&campo=<%=campo%>&cliente="+cliente; 
        document.forma.submit();   
	}
	else{
	    var s1=document.forma.select1;
		var s2=document.forma.select2;
		document.forma.action = "<%= CONTROLLER %>?estado=Factura&accion=Insert&Verificarcuenta=ok&forma=2&cuenta=I00"+s1.value+cliente+s2.value+"&nit="+nit+"&campo=<%=campo%>&cliente="+cliente;
        document.forma.submit();
	}
}
function aceptar2(){
    
    <%if(descripcion.equals("")){%>
	    var camp = parent.opener.document.form1.<%=campo%>;
		camp.value='<%=cuenta%>';
		parent.opener.document.form1.<%=campo%>.focus();
		window.close();
	<%}%>
}
function aceptar3(){
	alert('<%=Mensaje%>');
}
function aceptar4(){
	
	var camp = parent.opener.document.form1.<%=campo%>;
	camp.value='<%=tip[0]%>';
	var camp2 = parent.opener.document.form1.tipo<%=campo.substring(11,campo.length())%>;
	var aux   = parent.opener.document.form1.auxiliar<%=campo.substring(11,campo.length())%>;
	aux.value = '<%=tip[1]%>';
	camp2.length=1;
	camp2.options[0].value='IT';
	camp2.options[0].text ='IT';
	parent.opener.document.form1.<%=campo%>.focus();
	
	window.close();
}

</script>
<form name="forma" method="post" action="" >
<table width="550"  border="1" align="center">
  <tr>
    <td><table width="100%"  align="center"   >
  <tr>
    <td width="248" height="22"  class="subtitulo1">Codigo Cuenta Contable</td>
    <td width="216"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
    <table width="100%" align="center" class="tablaInferior">
      <tr class="fila">
        <td width="63" align="left" valign="middle" style='font-size:11px'>Forma1:</td>
        <td width="315" colspan="2" valign="middle"> <table width="67%"  border="0" cellpadding="0" cellspacing="0">
          <tr class="fila">
            <td width="12%">&nbsp; </td>
            <td width="65%"style='font-size:11px'> Plan de cuenta </td>
            <td width="23%"style='font-size:11px'><p>Nit<br>
                        </p></td>
            </tr>
        </table></td>
      </tr>
      <tr class="fila">
        <td width="63" rowspan="2" valign="middle" ><div align="center"></div></td>
        <td valign="middle"> 28150502            
          <input name="ind" type="text" class="textbox" id="ind" value="" size="3" maxlength="3" >
          _
IT_          
          <%=nit%>
          <br>          </td>
        <td valign="middle"><div align="right"><img src="<%=BASEURL%>/images/botones/validar.gif"  name="imgaceptar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick="aceptar1('forma1','<%=nit%>','<%=cliente%>')"></div></td>
      </tr>
      <tr class="fila">
        <td valign="middle"><%=descripcion%> </td>
        <td valign="middle"><div align="right"><img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand;<%=(descripcion.equals(""))?"visibility:hidden":""%>" onClick="aceptar4()">          </div>          <div align="right"></div></td>
      </tr>
    </table>
    <table width="100%" align="center" class="tablaInferior">
      <tr class="fila">
        <td width="63" valign="middle" style='font-size:11px'>Forma2:</td>
        <td width="315" valign="middle"><table width="81%"  border="0" cellpadding="0" cellspacing="0">
            <tr class="fila">
              <td width="8%">&nbsp; </td>
              <td width="31%"style='font-size:11px'>Unidad Negocio </td>
              <td width="22%"style='font-size:11px'><br>
                Cliente</td>
              <td width="39%"style='font-size:11px'><p>Elemento
                del gasto<br>
                <input name="ind2" type="text" class="textbox" id="ind4" value="" size="25" maxlength="25" onKeyUp="buscar(document.forma.select2,this)">
              </p>                </td>
            </tr>
        </table></td>
      </tr>
      <tr class="fila">
        <td width="63" valign="middle" >&nbsp;</td>
        <td valign="middle">I00            
          <select id="select1" name="select1"  class="textbox" >
            <%for(int i=0;i<uni.size();i++){%>
			<option value="<%=(String)uni.get(i)%>" ><%= uni.get(i)%></option>
			<%}%>
          </select>          
           <%=cliente%>
           <select id="select2" name="select2"  class="textbox" >
		  <%for(int i=0;i<ele.size();i++){%>
            <option value="<%=(String)ele.get(i)%>" ><%= ele.get(i)%></option>
			<%}%>
          </select></td>
      </tr>
      <tr class="fila">
        <td colspan="2" valign="middle" ><div align="right"><img src="<%=BASEURL%>/images/botones/validar.gif"  name="imgaceptar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick="aceptar1('forma2','<%=nit%>','<%=cliente%>');"></div></td>
      </tr>
    </table>  </td>
  </tr>
  </table>
  <div align="center">
    <p><br>
    <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></p>
  </div>
</form>


<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</body>
</html>


