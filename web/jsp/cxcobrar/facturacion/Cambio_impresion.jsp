<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@include file="/WEB-INF/InitModel.jsp"%><html>
<head>
<title>Cambiar Agencia y formato de impresion</title>

<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
                    
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>

<script>
function enviarF(CONTROLLER,frm){
      if(frm.Agencia.value!=""){
		  frm.action= CONTROLLER+'?estado=Factura&accion=Insert&opcion=UPDATE_IMPRESION_CAMBIO';
		  frm.submit();
	  }else{
	      alert("La agencia de impresion no puede estar vacia")
		  frm.Agencia.focus();
	  }  
 }
</script>
<body onLoad="redimensionar();doFieldFocus( forma );" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Cambiar Agencia y formato de impresion"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
String reset = (request.getParameter("reset")!= null)?request.getParameter("reset"):"";
if(!reset.equals("")){
  model.facturaService.setFactu(new factura());
}    
    
String msg = (request.getParameter("men")!=null)?request.getParameter("men"):"" ;
factura factu = model.facturaService.getFactu();

%>
<form name='forma' method='POST' action="<%=CONTROLLER%>?estado=Factura&accion=Insert&opcion=BUSCAR_FACTURA_CAMBIO" >
<table width="370" border="2" align="center">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td width="155" class="subtitulo1">&nbsp;Buscar Factura </td>
          <td class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
        <tr class="fila">
          <td>Factura</td>
          <td><input  name='factura'  class='textbox' id="factura" size='15' maxlength='8' >
             <img src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="20" height="20" onClick="forma.submit()" style="cursor:hand "></td>
          </tr>
      </table></td>
    </tr>
  </table>
  
 <%if(factu.getFactura()!= null){
  TreeMap Agencia = model.agenciaService.getAgencia();
  
 %> 
  <table width="700" border="2" align="center">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td colspan="2" class="subtitulo1">&nbsp;Datos De a Factura </td>
          <td colspan="2" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
        <tr class="letra">
          <td width="117" class="fila">Factura:</td>
          <td width="169"><%=factu.getFactura()%></td>
          <td width="133" class="fila">Codigo Cliente:              </td>
          <td width="149"><%=factu.getCodcli()%></td>
        </tr>
        <tr class="letra">
          <td class="fila">Fecha : </td>
          <td ><%=factu.getFecha_factura()%></td>
          <td class="fila">Agencia Facturacion: </td>
          <td><%=factu.getAgencia_facturacion()%></td>
        </tr>
        <tr class="letra">
          <td class="fila">Valor:</td>
          <td><%=factu.getValor_factura()%></td>
          <td class="fila">Moneda:</td>
          <td><%=factu.getMoneda()%></td>
        </tr>
        <tr class="letra">
          <td class="fila">Agencia Impresion: </td>
          <td><%=factu.getAgencia_impresion()%></td>
          <td class="fila">Nueva Agencia Impresion: </td>
          <td> <input:select name="Agencia" attributesText="<%= "id='Agencia'  style='width:100%;' class='textbox'  " %>"  default="" options="<%= Agencia %>" /></td>
        </tr>
        <tr class="letra">
          <td class="fila">Formato Impresion: </td>
          <td><%=(factu.getFormato().equals(""))?"Carga General":"Carbon"%></td>
          <td class="fila">Nuevo Formato Impresion </td>
          <td>    <select name="formato" >
		                      <option value="" >Carga General</option>
							  <option value="FC" <%if(request.getParameter("formato") != null && request.getParameter("formato").equals("FC")){%>selected<%}%>>Carbon</option>
			                  </select>
		  </td>
        </tr>
      </table></td>
    </tr>
  </table>
     
  <%}%>
  
</form>


<div align="center"><%if(factu.getFactura()!= null){%><img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod"  height="21" onClick="javascript:enviarF('<%=CONTROLLER%>',forma);"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;<%}%>   <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"> </div>
 <%if(!msg.equals("")){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%>
</div>
</body>
</html>

