<%@page session="true"%>
<%@page import="java.util.*" %>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Factura</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
 <script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
 <script type='text/javascript' src="<%=BASEURL%>/js/consultadespacho.js"></script> 
 <script type='text/javascript' src="<%=BASEURL%>/js/general.js"></script> 
 <style type="text/css">
<!--
.Estilo1 {color: #D0E8E8}
-->
 </style>
</head> 
<%
   String Mensaje = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
	factura factu = model.facturaService.getFactu();
	//parent.opener.location='"+CONTROLLER+"?estado=Factura&accion=Modificar&busqueda=ok&codcli="+factu.getCodcli()+"';
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
	
%>
<body onLoad="cargar();<%=(request.getParameter("agregarItems")!=null)?"agregarItems();":"cargarCod();"%><%=(request.getParameter("gas")!=null)?"gastos();":""%>" onResize="redimensionar();">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modificacion Factura"/>
</div> 

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%
 
 String sinitems = (request.getParameter("sinitems")!=null)?request.getParameter("sinitems"):"";

 double total=0;   
   %>
<script>
    var maxfila = 0;
	 function gastos(){
	    alert('<%=request.getParameter("gas")%>!');     
	}
	function cargarCod(){
	      <%
		  String cargarCodi = (request.getParameter("cargarCodigo")!=null)?request.getParameter("cargarCodigo"):"";
	      String xx = (request.getParameter("x")!=null)?request.getParameter("x"):"";
		  String camp = (request.getParameter("campo")!=null)?request.getParameter("campo"):"";
		  if(cargarCodi.equals("ok")){%>
		      var campu = document.form1.<%=camp%>;
	          campu.value='<%=xx%>';
		  <%}
		  else if(cargarCodi.equals("no")){%>
		      var campu = document.form1.<%=camp%>;
	          campu.value='';
			  alert('codigo cuenta no existe!');
		  <%}%>
	
	}
	
	function procesar (element){
      if (window.event.keyCode==13){ 
        if(element.value !=''){
		    element.value=formatear(element.value,2);
		}
		actualizar();
	  }
    }
	
	
	function guardarFactura(CONTROLLER){
	    	document.form1.action = "<%= CONTROLLER %>?estado=Factura&accion=Modificar&Agregar=ok&guardar=ok&&numerofilas="+maxfila; 
			document.form1.submit();   
	}
	function modificarFactura(CONTROLLER){
	  
	  var valortotal = document.getElementById("textfield9");
	  var valorabono = document.getElementById("textfield6");
	  
	  var valortot = valortotal.value.replace(new RegExp(",","g"),"");
	  var valorabo = valorabono.value.replace(new RegExp(",","g"),"");
	
	  
	  var saldo=parseFloat(valortot)-parseFloat(valorabo);
	  var a = form1.textfield9.value.replace(new RegExp(",","g"), "");
		if(parseFloat(valortot)<saldo)
		     alert("El valor total de la factura no puede ser menor que el saldo!");
			 
		  else{
	        
	                         if( parseFloat(a)>0 ){
        				 document.form1.action = "<%= CONTROLLER %>?estado=Factura&accion=Modificar&Agregar=ok&facturacionmodif=ok&&numerofilas="+maxfila;
        				 document.form1.submit();  
        			}
                                 else
                                        alert('El valor de la factura no puede ser igual a cero!');
				 
		}
	}
	function aceptarFactura2(causa){
	    document.form1.action = "<%= CONTROLLER %>?estado=Factura&accion=Modificar&Agregar=ok&actualizar=ok&numerofilas="+maxfila+"&causa="+causa; 
        document.form1.submit();     
	}
	function agregarItems(){
	   window.open("<%=BASEURL%>/jsp/cxcobrar/facturacion/AgregarItems.jsp?Modif=ok",'','status=yes,scrollbars=no,width=900,height=550,resizable=yes');
	}
	function abrir(CONTROLLER){
		document.form1.action = "<%= CONTROLLER %>?estado=Factura&accion=Modificar&Agregar=ok&numerofilas="+maxfila; 
        document.form1.submit();     	
	}
	
   function actualizar(){

        var moneda= document.getElementById("select2").value;
	    var total = document.getElementById("textfield9");
		var suma=0;
		
		for(var i=0;i<maxfila;i++){
		    var suma2=0; 
			var valor = document.getElementById("textfield14"+i);
			var valor2 = document.getElementById("textfield15"+i);
			var total2 = document.getElementById("textfield16"+i);
		    if( valor.value != '' && valor2.value !=''){
				if( valor!= '' && valor.value.length>1 && valor!=null)
					var valorlimpio = valor.value.replace(new RegExp(",","g"), "");
				else
					var valorlimpio= 0;
				if( valor2!= '' && valor2.value.length>=1 && valor2!=null)
					var valorlimpio2 = valor2.value.replace(new RegExp(",","g"), "");
				else{
					var valorlimpio2= 0;
					valor2.value=valorlimpio2; 				
				}	
				//if(valorlimpio!=0||valorlimpio2!=0){
				suma2 = roundMon(parseFloat(valorlimpio)*parseFloat(valorlimpio2),moneda);
				if(valorlimpio!='0' && valorlimpio2!='0'){
					if(moneda=='DOL'){
						total2.value=suma2;
						formatoDolar(document.getElementById("textfield16"+i), 2); 
					}else	
						total2.value=formatear(''+suma2,0); 
				}
				else if(valorlimpio!='0' && valorlimpio2=='0'){
					if(moneda=='DOL'){
						total2.value=suma2;
						formatoDolar(document.getElementById("textfield16"+i), 2); 
					}else	
						total2.value=formatear(''+suma2,0);  
				}	
				else if(valorlimpio=='0' && valorlimpio2!='0'){
					if(moneda=='DOL'){
						total2.value=suma2;
						formatoDolar(document.getElementById("textfield16"+i), 2);
					}else	
						total2.value=formatear(''+suma2,0); 
				}	
				suma+=suma2;
		//}
			}	
		}
		if(moneda=='DOL'){
		    total.value =suma;
			formatoDolar(document.getElementById("textfield9"), 2);
		}else
		    total.value =formatear(''+Math.round(suma),0);	
			
	}
	
	function roundMon(numero,moneda){
		if(moneda=='DOL'){
			  numero = (Math.round(numero * 100))/100;
			  return numero;
		}else{
			return Math.round(numero);
		}
	} 
	
	function validaritems(){


	   var valor0 = document.getElementById("textfield5");/*agencia*/
	   var valor00 = document.getElementById("c_fecha2");/*plazo*/
	   var valor000 = document.getElementById("textfield");/*tasa*/
	   var valor0000 = document.getElementById("textfield95");/*total*/
	   
	   
	   if(valor0.value == ''){
	       alert('Debe escojer la agencia facturacion!');
		   return false;	
	   }
	   if(valor00.value == ''){
	       alert('Debe tener valor en el plazo!');
		   return false;	
	   }
	   if(valor000.value == ''){
	       alert('Debe tener valor en la tasa !');
		   return false;	
	   }
	   
           var moneda= document.getElementById("select2").value;

	   if(moneda=='DOL'){
               if( valor0000.value.length >19 ){
	       alert('El valor total de la factura excede 14 digitos !');
		   return false;
	       }
           }
           else{
               if( valor0000.value.length >15 ){
	       alert('El valor total de la factura excede 12 digitos !');
		   return false;
	       }
           }


       for(var i=0;i<maxfila;i++){
		    var mensaje='';
			var valor1 = document.getElementById("textfield11"+i);/*remesa*/
			var valor2 = document.getElementById("textfield12"+i);/*fecha*/
			var valor3 = document.getElementById("textfield13"+i);/*descripcion*/
			var valor4 = document.getElementById("textfield14"+i);/*cantidad*/
			var valor8 = document.getElementById("textfield18"+i);/*unidad*/
			var valor5 = document.getElementById("textfield15"+i);/*valor unitario*/
			var valor6 = document.getElementById("textfield16"+i);/*valor item*/
			var valor7 = document.getElementById("textfield17"+i);/*codigo cuenta*/
			
			if(valor1.value == ''&&valor2.value == ''&&valor3.value == ''&&valor4.value == ''&&valor5.value == ''&&valor6.value == ''&&valor7.value == ''&&valor8.value == ''){
			}
			else{
			   
				if( valor4.value == '' ){
					mensaje = 'La cantidad no puede estar vacia o cero para la fila:'+(i+1);				                
				}	
				if( valor5.value == '' ){
				   mensaje += '\nEl valor unitario no puede estar vacia o cero para la fila:'+(i+1);
				}	
				if( valor6.value == 0){
				   mensaje += '\nEl valor del item '+(i+1)+' no puede ser cero';   
				}
				if( valor7.value == '' ){
				   mensaje += '\nEl codigo de cuenta no puede estar vacia o cero para la fila:'+(i+1);            
				}	
				
				if(mensaje!=''){
					alert(mensaje);
					return false;
				}	
			}			
		}	
		for(var i=0;i<maxfila;i++){
		      var ax = document.getElementById("aux"+i);
			  var auxi = document.getElementById("auxiliar"+i);
			  var tip = document.getElementById("tipo"+i);
			  if(ax.value=='S'){
			      if(tip.value==''){
			          alert('En el item '+(i+1)+', La cuenta '+document.getElementById("textfield17"+i).value+' necesita un tipo de subledger!' ); 
				      return false;  
				  }
				  else if (auxi.value==''){
				      alert('En el item '+(i+1)+', La cuenta '+document.getElementById("textfield17"+i).value+' necesita un auxiliar!' ); 
				      return false; 
				  } 
			  }
		 }
		return true;
		
	}
	
	function sinformato(element){
      element.value.replace(new RegExp(",","g"), "");
    }
		
	function formatear(numero,decimales){
	   
	   if (!isNaN (parseInt(numero.replace( new RegExp(",","g"), "")))){
        	   var tmp = parseInt(numero.replace( new RegExp(",","g"), "")) ;
			   var num = '';
			   var pos = 0;
			   while(tmp>0){
					  if (pos%3==0 && pos!=0) num = ',' + num;
					  res  = tmp % 10;
					  tmp  = parseInt(tmp / 10);
					  num  = res + num  ;
					  pos++;
			   }
			   var dec = 1;
			   for (i=0;i<decimales;i++, dec*=10);
			   dnum = parseInt ((parseFloat(numero) - parseInt(numero)) * dec);
					   tmpdec = '';
			   for (i=0;i<decimales - (dnum==0?0:dnum.toString().length ) ;i++, tmpdec +='0');
			   num += (decimales>0?'.' +  tmpdec + (dnum!=0?dnum:''): '');
		       if(num=='')
			       num='0';
        return num;
		}
		else
		    return '';
		  
    }  
	
	function revisaritemenblanco(){
	var i= maxfila-1;
	
	        var valor11 = document.getElementById("textfield11"+i);
			var valor12 = document.getElementById("textfield12"+i);
			
			var valor13= document.getElementById("textfield13"+i);
			var valor14 = document.getElementById("textfield14"+i);
			
			var valor15= document.getElementById("textfield15"+i);
			var valor16 = document.getElementById("textfield16"+i);
			
			var valor17 = document.getElementById("textfield17"+i);
			var valor18 = document.getElementById("textfield18"+i);
			
			if(valor11.value==""&&valor12.value==""&&valor13.value==""&&valor14.value==""&&valor15.value==""&&valor16.value==""&&valor17.value==""&&valor18.value=="")
			    return false;
		    else
		        return true;		
	}
	
	function insertarItem(BASEURL){
					
			var numfila = detalle.rows.length ;
			var fila = detalle.insertRow(numfila);
			
			if(maxfila%2==0)
			  fila.className="filagris";
			else
			  fila.className="fila";
			  
			fila.id = "filaItem"+maxfila;  
		    var celda = fila.insertCell();   
			var tds ="<span><a id='n_i"+maxfila+"' >"+maxfila+"</a>";
			tds+="      <a onClick=insertarItem('"+BASEURL+"'); id='insert_item"+maxfila+"' style='cursor:hand' ><img src='"+BASEURL+"/images/botones/iconos/mas.gif'   width='12' height='12' ></a>";
			tds+="      <a onClick=borrarItem('filaItem"+maxfila+"');id='borrarI"+maxfila+"' style='cursor:hand' ><img src='"+BASEURL+"/images/botones/iconos/menos1.gif'   width='12' height='12' > </a><span>";
			celda.innerHTML = tds;
			celda = fila.insertCell();
			tds = "";
			tds +="<table width='100%' class='tablaInferior'>";
			tds +=" <tr class='filaazul'  >";
			tds +="<td width='5%'><div align='center'>";
            tds +="<input name='textfield11"+maxfila+"' id='textfield11"+maxfila+"' type='text' size='7' onChange='buscarRemesa("+maxfila+")' style='font-size:11px ' onKeyPress='soloAlfa(event)' maxlength='10'>";
            tds +="                 </div></td>";
            tds +="              <td width='7%'>";
            tds +="     <input name='textfield12"+maxfila+"' id='textfield12"+maxfila+"' type='hidden' size='8' readonly style='font-size:11px '></td>";
            tds +="   <td width='23%'>";
            tds +="    <div align='left'>"; 
            tds +="   <input name='textfield13"+maxfila+"' id='textfield13"+maxfila+"' type='text' size='40' style='font-size:11px' onKeyPress='soloAlfa(event)'>";
			tds +="  </div></td>";
            tds +="    <td width='12%'>";
			tds +="     <input name='textfield14"+maxfila+"' id='textfield14"+maxfila+"' type='text' size='7' style='text-align:right;font-size:11px' onFocus=\"sinformato(this);\" onBlur=\"if(this.value !=''){formatoDolar(this,'2');}actualizar();\" onKeyPress=\"soloDigitos(event,'decOK');procesar(this);\" maxlength='16'>";
            tds +="     <input name='textfield18"+maxfila+"' id='textfield18"+maxfila+"' type='hidden' size='1' readonly style='font-size:11px'> ";
			tds +="     <input name='hiddenField"+maxfila+"' id='hiddenField"+maxfila+"' type='hidden' ></td>";
            tds +="   <td width='16%'><input name='textfield15"+maxfila+"' id='textfield15"+maxfila+"' type='text' size='20' style='text-align:right;font-size:11px' onFocus=\"sinformato(this);\" onBlur=\"if('DOL'=='<%=factu.getMoneda()%>'){actualizar();}else{this.value=formatear(this.value,0);actualizar();}\" onKeyPress=\"if('DOL'=='<%=factu.getMoneda()%>'){soloDigitos(event,'decOK');}else{soloDigitos(event,'decNO');}\" onChange=\"if('DOL'=='<%=factu.getMoneda()%>'){formatoDolar(this,2);}else{onChange=formatear(this.value);}\" maxlength='16'>";																																																																		
			tds +="     <input name='textfield19"+maxfila+"' id='textfield19"+maxfila+"'type='hidden' value='<%=factu.getMoneda()%>' >";
			tds +="     <input name='hiddenField2"+maxfila+"' id='hiddenField2"+maxfila+"' type='hidden' ></td>";
			tds +="   <td width='14%'> <div align='right'>";
			
            tds +="    <input name='textfield16"+maxfila+"' id='textfield16"+maxfila+"' type='text' size='17' style='border:0;text-align:right;font-size:11px'  class='filaazul' onKeyPress=\"soloDigitos(event, 'decNO')\"  readonly>";
			tds +="     <input name='hiddenField3"+maxfila+"' id='hiddenField3"+maxfila+"' type='hidden' ></div></td>";
            tds +="   <td width='23%'><div align='center'><span class='letra'>";
            tds +="     <input name='textfield17"+maxfila+"' type='text' id='textfield17"+maxfila+"' size='28' maxlength='28' style='font-size:9px' onBlur='if(validarcuenta("+maxfila+")!=false){buscarCodigo("+maxfila+")}'>";
            tds +="   </span> <img src='"+BASEURL+"/images/botones/iconos/notas.gif' width='15' height='15' style='cursor:hand;' onClick=\"cargarcodigocuenta('textfield17"+maxfila+"','<%=factu.getCodcli().substring(3,6)%>','<%=factu.getNit()%>')\"> ";
			tds +="   <input name='aux"+maxfila+"' type='hidden' id='aux"+maxfila+"' value='N'></div></td>" ;
			tds +="</tr></table>";
			
			celda.innerHTML = tds;
			maxfila++; 
			organizar();
		
	}
	
	function borrarItem(indice){
          
            var tabla = document.getElementById("detalle");
			
			if ( tabla.rows.length <= 2 ){
				return;
			}
			
			var fila = document.getElementById(indice);
			
		    tabla.deleteRow(fila.rowIndex);				
			organizar();
			aceptarFactura2();
	}
	
	function organizar(){
 	    var cont =1;
		for (var i = 0,n = 1;i <= maxfila + 1 ; i++){
			var x = document.getElementById("n_i"+i);
			var y = document.getElementById("filaItem"+i);
			if ( x != null ){
				x.innerText = ""+n;
				n++;
			}
			if( y != null ){
				if( cont % 2 == 0){
				   y.className = "filagris";
				}else{
				   y.className = "fila";
				}
				cont++;
			}
		}
	}
	
	function regresar(){
	
	   document.form1.action = "<%= CONTROLLER %>?estado=Factura&accion=Modificar&regresar=ok"; 
       document.form1.submit();     
	}
	
	function formato2digitos( num ){
    		return (num<10? '0':'') + num;
  		}
    function formato (fecha){
			return fecha.getYear() + '-' + formato2digitos(fecha.getMonth()+1) + '-' + formato2digitos(fecha.getDate());
		}
		
	    function cargar(){
  			var fechaActual  =  new Date();
  			if(form1.c_fecha.value=='null'){
			   
				form1.c_fecha.value = formato(fechaActual);
			}
			
		}
		
		
function buscarRemesa( x ) {
 var fact =document.getElementById("textfield11"+x);
 var fec = document.getElementById("textfield12"+x);
 var factura = fact.value;
 if(factura=="") {  
  //alert("Digite el Numero de la Remesa");
 } else {
       if(confirm('Para facturar REMESA de transporte, presione CANCELAR.\n\nPara facturar GASTO asociado a remesa, presione ACEPTAR. ')){

		    document.form1.action = "<%= CONTROLLER %>?estado=Factura&accion=Modificar&cargar=ok&x="+factura+"&gasto=ok&fec="+fec.value; 
            form1.submit();            
		}
		else{
		  
		    document.form1.action = "<%= CONTROLLER %>?estado=Factura&accion=Modificar&cargar=ok&x="+factura+"&gasto=no&fec="+fec.value;
            form1.submit(); 
		}
 }
}
function buscarCodigo( x ) {
 var fact =document.getElementById("textfield17"+x);
 var factura = fact.value;
 if(factura=="") {  
   var fact1 =document.getElementById("tipo"+x);
	 var fact2 =document.getElementById("auxiliar"+x);
      fact1.length=0;
	  fact2.value="";
 } else {
 
   document.form1.action = "<%= CONTROLLER %>?estado=Factura&accion=Modificar&cargarcodigo=ok&x="+factura+"&campo=textfield17"+x+"&numerofilas="+maxfila; 
  form1.submit();
 }
}
function cargarRemesa( x ){
 if (window.event.keyCode==13){ 
  var remesa =document.getElementById("textfield11"+x);
  if( remesa.value.length != 0 ){
   buscarRemesa( x);
  }
  else{
   alert('Debe LLenar el Campo de la Remesa');
  }
 }
}
function cargarCodigo( x ){
 if (window.event.keyCode==13){ 
 if(validarcuenta(x)!=false){
  var remesa =document.getElementById("textfield17"+x);
	  if( remesa.value.length != 0 ){
	   buscarCodigo( x);
	  }
	  else{
	   alert('Debe LLenar el Campo del Codigo');
	  }
    }  
 }
}
function cargarfactura(){
 if (window.event.keyCode==13){ 
     aceptarFactura2('portasa');
 }
}		
function cargarcodigocuenta(a,b,c){
    var url= "<%= CONTROLLER %>?estado=Factura&accion=Insert&codigocuenta=ok&campo="+a+"&cliente="+b+"&nit="+c;
	window.open(url,'codigocuenta','status=yes,scrollbars=no,width=550,height=250,resizable=yes');
 }		
 function formatoDolar (obj, decimales){
   
     var numero = obj.value.replace( new RegExp(",","g"), "");

     var nums = ( new String (numero) ).split('.');

	 var salida = new String();

	 var TieneDec = numero.indexOf('.');

	 var dato = new String();

	 if( TieneDec !=-1  ){
		var deci = numero.split('.');
		var dec       = (deci[1].length >2)?deci[1].charAt(2):deci[1].substr(0,deci[1].length);
    	if(dec > 5){
        	dato =  (parseInt(deci[1].substr(0,2)) + 1);
     		if(dato>99){
       			nums[0] = new String (parseInt(nums[0])+1);
    			obj.value = nums[0]+'.00';
     		}else{
       			for (var i=nums[0].length-1, j=0; i>=0; salida = nums[0].charAt(i) + (j%3==0 && j!=0? ',':'') + salida , i--, j++);
          			obj.value = salida + (nums.length > 1 && decimales > 0 ? '.' +((nums[1].length >2)?((nums[1].charAt(2)>5)?(parseInt(nums[1].substr(0,2))+1):nums[1].substr(0,2)):(nums[1].length==1)?nums[1].substr(0,1)+'0':nums[1].substr(0,nums[1].length)) : '.00'); 
          	}
     	}else{
        	for (var i=nums[0].length-1, j=0; i>=0; salida = nums[0].charAt(i) + (j%3==0 && j!=0? ',':'') + salida , i--, j++);
        		obj.value = salida + (nums.length > 1 && decimales > 0 ? '.' +((nums[1].length >2)?((nums[1].charAt(2)>5)?(parseInt(nums[1].substr(0,2))+1):nums[1].substr(0,2)):(nums[1].length==1)?nums[1].substr(0,1)+'0':nums[1].substr(0,nums[1].length)) : '.00'); 
    	} 
		
    }else{
      for (var i=nums[0].length-1, j=0; i>=0; salida = nums[0].charAt(i) + (j%3==0 && j!=0? ',':'') + salida , i--, j++);
    	obj.value = salida + (nums.length > 1 && decimales > 0 ? '.' +((nums[1].length >2)?((nums[1].charAt(2)>5)?(parseInt(nums[1].substr(0,2))+1):nums[1].substr(0,2)):(nums[1].length==1)?nums[1].substr(0,1)+'0':nums[1].substr(0,nums[1].length)) : '.00'); 
 	  
	}
 }
 function validarcuenta(x){
    var cuenta =document.getElementById("textfield17"+x);
	  var aux =document.getElementById("aux"+x);
	  
      if(cuenta.value==''){
	  
	      cuenta.value='';
		  if(aux.value=='S'){
		  var fact1 =document.getElementById("tipo"+x);
	      var fact2 =document.getElementById("auxiliar"+x);
          fact1.length=0;
	      fact2.value="";
		  }
	      return false;    
	  }
	  else if(cuenta.value.length == 13){
	  	
	        if(cuenta.value.substr(0,3)!='I01'){
			    alert('Recuerde los formatos de codigos de cuenta contable:\n1) 28150502(plan de cuenta)IT(nit)\n2) I00(unidad de negocio)(codigo cliente)(elemento del gasto)\nNota: Puede armar codigo cuenta con el link al lado del campo del codigo!');
				cuenta.value='';
				return false    
			}	      
	  }
	  else if (cuenta.value.length > 13){
	 
	        /*if(cuenta.value.substr(0,8)!='28150502'){
			    alert('Recuerde los formatos de codigos de cuenta contable:\n1) 28150502(plan de cuenta)IT(nit)\n2) I00(unidad de negocio)(codigo cliente)(elemento del gasto)\nNota: Puede armar codigo cuenta con el link al lado del campo del codigo!');
				cuenta.value='';
				return false    
			}*/	      
	  }
	  else{
	     
	       alert('Recuerde los formatos de codigos de cuenta contable:\n1) 28150502(plan de cuenta)IT(nit)\n2) I00(unidad de negocio)(codigo cliente)(elemento del gasto)\nNota: Puede armar codigo cuenta con el link al lado del campo del codigo!');
		   cuenta.value='';
		   return false;
	  }   
 }
 function buscarAuxiliar(BASEURL, id, cuenta, tipo){
    var  cuen = document.getElementById("textfield17"+id);
	var tip = document.getElementById("tipo"+id); 
    if(cuen.value == '' || tip.value =='' ){
		alert("Verifique que la cuenta y el tipo tenga datos");
    }
	else{
		var dir =BASEURL+"/jsp/finanzas/contab/subledger/consultasIdSubledger.jsp?idcampo="+id+"&cuenta="+cuenta+"&tipo="+tipo+"&reset=ok";
		window.open(dir,'Auxiliar','width=700,height=520,scrollbars=no,resizable=yes,top=10,left=65,status=yes');
	}
}
function validarAuxiliar(BASEURL, id){
    
	var  cuen = document.getElementById("textfield17"+id);
	var tip = document.getElementById("tipo"+id);
    var aux = document.getElementById("auxiliar"+id);
	
    if(aux.value != ''){ 
    	if(cuen.value == '' || tip.value =='' ){
			alert("Verifique que la cuenta y el tipo tenga datos");
	    }
		else{
			enviar('?estado=IngresoMiscelaneoDetalle&accion=Buscar&evento=Auxiliar&cuenta=' + cuen.value+'&tipo='+tip.value+'&auxiliar='+aux.value+'&campo='+id )
		}
	}
}

function soloTexcoma(e) {
	//alert(window.event.keyCode)
	var key = (isIE) ? window.event.keyCode : e.which;
	var obj = (isIE) ? event.srcElement : e.target;
	var isNum = ((key > 0 && key < 32) || (key >32 && key<35) || (key>35 && key <37) || (key >38 && key <40)  ||  (key > 41 && key < 44) || (key > 47 && key < 48) || (key>59 && key<61)||(key > 61 && key < 65) || (key > 90 &&  key < 97) || (key > 122) || (key == 13) ) ? true:false;
	window.event.keyCode = (isNum && isIE) ? 0:key;
	e.which = (!isNum && isNS) ? 0:key;
	return (isNum);
}

 function eliminar(numrem){
    document.form1.action ="<%= CONTROLLER %>?estado=Factura&accion=Modificar&opcion=ELIMINAR_ITEM&numrem="+numrem;
    document.form1.submit();
 }
</script>
   <form name="form1" method="post" >
<table width="1000" border="2" align="center">
    <tr>
      <td width="100%" height="60">
        <table width="100%">
          <tr>
            <td width="49%" height="22" align="left" class="subtitulo1">&nbsp;Informaci&oacute;n de la Factura <%=factu.getFactura()%></td>
            <td width="51%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"align="left"><%=datos[0]%></td>
          </tr>
        </table>
        <table width="100%">
          <tr>
            <td width="14%" class="fila">Cliente:</td>
            <td width="14%" class="letra">              
              <input name="textfield2" type="text" class="filaresaltada" id="textfield2" style="border:0"  value="<%= factu.getCodcli()==null? "&nbsp;" : factu.getCodcli()%>" size="20" maxlength="10" readonly></td>
            <td colspan="2" class="fila">Agencia Facturacion:</td>
            <td colspan="2" class="letra"> <%model.agenciaService.cargarAgencias();
			Vector agencias = model.agenciaService.getVectorAgencias(); 
			 for(int i=0; i< agencias.size();i++){
               Agencia agencia = (Agencia) agencias.get(i);
               
			 if(agencia.getId_agencia().equals(factu.getAgencia_facturacion())){%><%=agencia.getNombre()%><%}%> 
                	
           <%}%>
		    <input type="hidden" name="textfield5" value="<%=factu.getAgencia_facturacion()%>">
             </td>
            <td width="11%" class="fila"><span class="letra">Moneda: </span></td>
            <td width="19%" class="letra"><div align="left">
              <%TreeMap mo = model.facturaService.listarMonedas();%>
                	<input:select name="mone"  attributesText="id='select2' style='width:80%;' class='textbox'; onChange=aceptarFactura2('pormoneda');"  options="<%=mo%>" default="<%=factu.getMoneda()%>" />
			 
            </div></td>
			 <script>
			 form1.mone.value='<%=factu.getMoneda()%>'
			 </script>
          </tr>
          <tr>
            <td class="fila">Nit:</td>
            <td class="letra">
              <input name="textfield3" type="text" class="filaresaltada" id="textfield3" style="border:0"  value="<%=factu.getNit()==null? "&nbsp;" : factu.getNit()%>" size="20" maxlength="15" readonly></td>
            <td colspan="2" class="fila">Fecha Factura: </td>
            <td colspan="2" class="letra"><input name="c_fecha" type="text" class="textbox" id="c_fecha" size="14" readonly value="<%=factu.getFecha_factura()%>" height="">
               <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.c_fecha);return false;" HIDEFOCUS> 
			  <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></td>
            <td class="fila"> <span class="letra">Tasa: </span></td>
            <td class="letra"><input name="textfield" id="textfield" type="text"  onKeyPress="soloDigitos(event,'decOK');" onKeyUp="cargarfactura()" value="<%=UtilFinanzas.customFormat("#,###.#########",factu.getValor_tasa(),9)%>" size="16" maxlength="15" <%=(factu.getMoneda ().equals(model.facturaService.getMonedaLocal()))?"readonly":""%>> 
            <input name="textfield6" type="hidden" id="textfield6" value="<%=factu.getValor_saldo()%>">             
            <div align="left">              </div></td>
          </tr>
          <tr>
            <td class="fila">Forma de Pago: </td>
            <td class="letra">
              <select name="textfield4" id="textfield4" class='textbox'  >
                <%if(factu.getForma_pago().equals("CONTADO")){ %>
                <option value="CONTADO"selected  >CONTADO</option>
                <option value="CREDITO" >CREDITO</option>
                              
                <%}else{ %>
                <option value="CONTADO" >CONTADO</option>
                <option value="CREDITO" selected >CREDITO</option>
                <%}%>
              </select></td>
            <td width="8%" class="fila">Plazo: </td>
            <td width="8%" class="fila"><input name="c_fecha2" type="text" class="textbox" id="c_fecha22" onKeyPress="soloDigitos(event,'decNO')" value="<%=factu.getPlazo()==null? "0" :factu.getPlazo()%>" size="3" maxlength="3" readonly></td>
            <td width="7%" class="fila">Rif:
              </td>
            <td width="19%" class="letra"><span class="fila"><%=factu.getRif()%></span></td>
            <td class="fila">Valor Total: <span class="letra">
            <input name="textfield8" id="textfield85" type="hidden" size="10" style="border:0"  value="<%=factu.getPlazo()%>" onKeyPress="soloDigitos(event,'decNO')">
            </span></td>
            <td class="letra">              <input name="textfield9" class="filaresaltada" id="textfield95" type="text" size="30" style="border:0;"  onKeyPress="soloDigitos(event,'decNO')" readonly></td>
          </tr>
		  <tr>
            <td class="fila">Formato Impresion: </td>
            <td class="letra">
			                  <select name="zona" >
							  <option value="" >Carga General</option>
							  <option value="FC" <%if(factu.getZona().equals("FC")){%> selected<%}%>>Carbon</option>
			                  </select>
			</td>
           <td class="fila">HC:<% model.tablaGenService.listarHc();TreeMap hcs = model.tablaGenService.getHcs(); %></td>
            <td class="fila"><input:select  name="ForHC" attributesText="style='width:90%;' class='listmenu'" options="<%=hcs%>" /><img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
            <td class="fila">&nbsp;</td>
            <td class="fila">&nbsp;</td>
            <td class="fila">&nbsp;</td>
            <td class="letra">&nbsp;</td>
          </tr>
      </table>
      </td>
    </tr>
  </table>
  
  <table width="1000" border="2" align="center">
    <tr>
      <td height="26"><table width="100%">
          <tr class="fila" id="cantidad">
            <td width="9%"  >
              <div align="center"></div>Descripcion:</td>
            <td  align="center" >            <div align="left">
                <textarea name="textarea" cols="100%" rows="2" onKeyPress="soloTexcoma(event)"><%=factu.getDescripcion()==null? "&nbsp;" : factu.getDescripcion()%></textarea>
            </div></td>
            </tr>
          <tr class="fila" id="cantidad">
            <td height="3"  >Observacion:</td>
            <td  align="center" ><div align="left">
              <textarea name="textarea2" cols="100%" rows="2" onKeyPress="soloTexcoma(event)"><%=factu.getObservacion()==null? "&nbsp;" : factu.getObservacion()%></textarea>
            </div></td>
            </tr>
      </table></td>
    </tr>
  </table>
  

  
  <table width="1000" border="2" align="center">
    <tr>
      <td  font-size:"10px">
        <table width="100%" align="center">
          <tr>
            <td width="45%" height="22" align="center" class="subtitulo1">&nbsp;Detalle del Documento </td>
            <td width="39%" align="center" class="barratitulo"><div align="left"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></div></td>
            <td width="16%" align="center" class="barratitulo"><a onClick="abrir('<%=CONTROLLER%>'); " style="cursor:hand;" class="Simulacion_Hiper" HIDEFOCUS>Agregar Items</a>&nbsp;</td>
          </tr>
        </table>
        <div align="center">
          <table id="detalle" width="100%" align="center" >
            <tr  id="fila1" class="tblTituloFactura">
              <td align="center">
                <table width="79%" height="37" border="1" align="center"  bordercolor="#999999" bgcolor="#F7F5F4">
                  <tr class="tblTitulo">
                    <td width="50" nowrap style="font-size:11px "><div align="center">Item</div></td>
                  </tr>
              </table></td>
              <td align="center">
                <table width="100%" border="1"  bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                  <tr class="tblTitulo">
                    <td nowrap style="font-size:11px "><div align="center">Remesa</div>                      <div align="center"></div></td>
                    <td width="219" align="center" style="font-size:11px ">Descripcion</td>
                    <td width="93" align="center" style="font-size:11px "><div align="center">Cantidad</div></td>
                    <td width="129" align="center" style="font-size:11px "><div align="center">Valor Unitario </div></td>
                    <td width="126" align="center" style="font-size:11px "><div align="center">Valor</div></td>
                    <td width="190" align="center" style="font-size:9px "><div align="center">Codigo Cuenta <br>
                      Tipo &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp Auxiliar</div></td>
                  </tr>
              </table></td>
            </tr>
            <% int x=1;
			      Vector a = model.facturaService.getRemesasFacturar();
				
				if ( a == null || a.size()==0){%>
            <script>insertarItem('<%=BASEURL%>');</script>
            <%
				}
				else{
				 
				for( x=1;x<=a.size();x++){
				
				               factura_detalle factu_deta = (factura_detalle)a.get(x-1);
							  /* 
							  System.out.println("factu_deta.getValor_item():"+factu_deta.getValor_item());
							  System.out.println("factu_deta.getNumero_remesa():"+factu_deta.getNumero_remesa());
							  System.out.println("factu_deta.getFecrem():"+factu_deta.getFecrem());
							  System.out.println("factu_deta.getDescripcion():"+factu_deta.getDescripcion());
							  System.out.println("factu_deta.getUnidad():"+factu_deta.getUnidad());
							  System.out.println("factu_deta.getValor_tasa():"+factu_deta.getValor_tasa());
							  System.out.println("factu_deta.getValor_unitario():"+factu_deta.getValor_unitario());
							  System.out.println("factu.getMoneda ():"+factu.getMoneda ());
							  System.out.println("factu_deta.getValor_unitariome():"+factu_deta.getValor_unitariome());
							  System.out.println("factu_deta.getValor_item():"+factu_deta.getValor_item());
							  System.out.println("factu_deta.getValor_item():"+factu_deta.getValor_itemme());
							  System.out.println("factu_deta.getCodigo_cuenta_contable():"+factu_deta.getCodigo_cuenta_contable());
							  System.out.println("factu_deta.getAux():"+factu_deta.getAux());
							  */
						if(factu_deta.getCodigo_cuenta_contable()==null){
						    factu_deta.setCodigo_cuenta_contable("");
						}
						if(factu_deta.getFecrem()==null){
						    factu_deta.setFecrem("");
						}
					    if(factu_deta.getUnidad()==null){
						    factu_deta.setUnidad("");
						}   
							   total+=factu_deta.getValor_item();
				%>
            <tr class="<%=(x%2!=0)?"fila":"filagris"%>" id="filaItem<%=x-1%>" nowrap  bordercolor="#D1DCEB" >
              <td class="bordereporte"> <span> <a id='n_i<%=x-1%>' ><%=x%></a> <a onClick="insertarItem('<%=BASEURL%>');" id="insert_item<%=x-1%>" style="cursor:hand"  ><img src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width="12" height="12" ></a> <a onClick="<%if(!factu_deta.getNumero_remesa().equals("")){%>eliminar('<%=factu_deta.getNumero_remesa()%>');<%}else{%>borrarItem('filaItem<%=x-1%>');<%}%>  " id="borrarI<%=x-1%>" style="cursor:hand" ><img src='<%=BASEURL%>/images/botones/iconos/menos1.gif'   width="12" height="12"  > </a> </span> </td>
              <td>
                <table width='100%' class='tablaInferior'>
                  <tr class='<%=(x%2!=0)?"fila":"filagris"%>'  >
                    <td width='5%'><div align='center'>
					
                        <input name='textfield11<%=x-1%>' type='text' id='textfield11<%=x-1%>' style="font-size:11px " onChange="buscarRemesa('<%=x-1%>')" onKeyPress="soloAlfa(event)" value="<%=factu_deta.getNumero_remesa()%>" size='7' maxlength="10" onBlur="if(this.value==''){this.value='<%=factu_deta.getNumero_remesa()%>'}">
                    </div></td>
                    <td width='7%'>
                      <input name='textfield12<%=x-1%>' id='textfield12<%=x-1%>' value="<%=factu_deta.getFecrem()%>" type='hidden' size='8'  readonly=""style="font-size:11px "></td>
                    <td width='23%'>
                      <div align='left'>
                        <textarea name="textfield13<%=x-1%>" cols="40" rows="3" id="textfield13<%=x-1%>" style="font-size:11px " onKeyPress="soloTexcoma(event)"><%=factu_deta.getDescripcion()%></textarea>
                    </div></td>
                    <td width="12%">
					
                      <input name='textfield14<%=x-1%>' type='text' id='textfield14<%=x-1%>' style='text-align:right;font-size:11px 'onFocus='sinformato(this);' onBlur="if(this.value !=''){formatoDolar(this,2);}actualizar();" onKeyPress="soloDigitos(event,'decOK');procesar(this);" value="<%=Util.customFormat(factu_deta.getCantidad())%>" size='7' maxlength="16" >
                    
					  <input name='textfield18<%=x-1%>' id='textfield18<%=x-1%>' type='hidden' size='1' value="<%=factu_deta.getUnidad()%>" readonly=""style="font-size:11px ">
                   
					  <input name="hiddenField<%=x-1%>" type="hidden" id="hiddenField<%=x-1%>" value="<%=factu_deta.getValor_tasa()%>"></td>
                  
					<td width='16%'><input name='textfield15<%=x-1%>' type='text' id='textfield15<%=x-1%>' style='text-align:right;font-size:11px' onfocus='sinformato(this);' value='<%=(factu.getMoneda ().equals("DOL"))?UtilFinanzas.customFormat2(factu_deta.getValor_unitario()):UtilFinanzas.customFormat2(factu_deta.getValor_unitario())%>' size='20' maxlength="16" onChange="formatoDolar(this,2);" onKeyPress="soloDigitos(event,'decOK');" onBlur="actualizar();" >
                      
						<input name='textfield19<%=x-1%>' id='textfield19<%=x-1%>' type='hidden' value="<%=factu.getMoneda()%>" >
                      
						<input name="hiddenField2<%=x-1%>" type="hidden" id="hiddenField2<%=x-1%>" value="<%=factu_deta.getValor_unitariome()%>"></td>
                    <td width='14%'>
                      <div align="right">
                    
					    <input name='textfield16<%=x-1%>' id='textfield16<%=x-1%>' value='<%=(factu.getMoneda ().equals("DOL"))?UtilFinanzas.customFormat2(factu_deta.getValor_item()):UtilFinanzas.customFormat(factu_deta.getValor_item())%>' type='text' size='20' style='border:0;text-align:right;font-size:11px' class='filaazul' readonly>
                       
					    <input name="hiddenField3<%=x-1%>" type="hidden" id="hiddenField3<%=x-1%>" value="<%=factu_deta.getValor_itemme()%>">
                  
				    </div></td>
                    <td width='23%'><div align='center'><span class='letra'>
                        <input name='textfield17<%=x-1%>' type='text' id='textfield17<%=x-1%>' size='28' style="font-size:9px " maxlength='28' value ="<%=(factu_deta.getCodigo_cuenta_contable()!=null)?factu_deta.getCodigo_cuenta_contable():""%>" <%=(factu_deta.getCodigo_cuenta_contable().equals("130505030601")||factu_deta.getCodigo_cuenta_contable().equals("130510030601"))?"readonly":""%> onKeyUp="cargarCodigo('<%=x-1%>')"  onBlur="if(this.value!='130510030601'){if(this.value!='130505030601'){if(textfield12<%=x-1%>.value.length < 25){if(validarcuenta('<%=x-1%>')!=false){buscarCodigo('<%=x-1%>')}}}}">
                
				   </span><img src="<%=BASEURL%>/images/botones/iconos/notas.gif" width="15" height="15" style="cursor:hand;<%=(factu_deta.getCodigo_cuenta_contable().equals("130505030601")||factu_deta.getCodigo_cuenta_contable().equals("130510030601")||factu_deta.getFecrem().length()>25)?"visibility:hidden":""%>" onClick="cargarcodigocuenta('textfield17<%=x-1%>','<%=factu.getCodcli().substring(3,6)%>','<%=factu.getNit()%>')">
                    <%if(factu_deta.getAux()==null){
			       factu_deta.setAux("N");
			   }
			   
			   if(factu_deta.getAux().equals("S")){
			     LinkedList tsa = new LinkedList();
				 if(factu_deta.getTiposub()!=null){
			     tsa=factu_deta.getTiposub();}%>
			   <br>

			 <select name="tipo<%=x-1%>" id="tipo<%=x-1%>" class='textbox'  style="font-size:9px">
              <% for(int i = 0; i<tsa.size(); i++){
                    TablaGen tipo = (TablaGen) tsa.get(i); %>
                    <option value='<%=tipo.getTable_code()%>' <%= (tipo.getTable_code().equals(factu_deta.getTiposubledger()))?"selected":""%> ><%=tipo.getTable_code()%></option>
             <%}%>
			  
              </select>
               <input name='auxiliar<%=x-1%>' type='text' id='auxiliar<%=x-1%>' size='20' style="font-size:9px " maxlength='25'  onBlur="validarAuxiliar('<%=BASEURL%>','<%=x-1%>')" value="<%=factu_deta.getAuxliliar()%>">
               <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name="imagenAux" width="15" height="15"  id="imagenAux" style="cursor:hand"  onClick="buscarAuxiliar('<%=BASEURL%>','<%=x-1%>',form1.textfield17<%=x-1%>.value, form1.tipo<%=x-1%>.value);"  > 
               
</div></td>
			<%}%>
			<input name="aux<%=x-1%>" type="hidden" id="aux<%=x-1%>" value="<%=factu_deta.getAux()%>">
			 </tr></table></td>
			 
            </tr>
            <script>maxfila++;organizar();</script>
            <%}%>
            <script>insertarItem('<%=BASEURL%>');</script>
            <%}%>
             <script>
			   <%if(factu.getMoneda ().equals("DOL")){%>
			        form1.textfield9.value = '<%=total%>';
				   formatoDolar(form1.textfield9,2);
			   <%}else{%>
			       form1.textfield9.value = formatear(''+Math.round('<%=total%>'),0);
			   <%}%> 
			   </script>
          </table>
      </div></td>
    </tr>
  </table>
  <p align="center"><img src="<%=BASEURL%>/images/botones/iconos/guardar.gif" name="imgaceptar" width="22" height="22" style="cursor:hand" onClick="this.disabled=true; guardarFactura();" >
	<img src="<%=BASEURL%>/images/botones/modificar.gif" name="imgaceptar"  onMouseOver="botonOver(this);" onClick="if(validaritems()){modificarFactura('<%=CONTROLLER%>');}" onMouseOut="botonOut(this);" style="cursor:hand" ></tab>
	<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></p>
 
    <p>
  <%if(!Mensaje.equals("")){
  %>
</p>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%= Mensaje %></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</form >
</p>
 <%}%>
</div>
<%=datos[1]%>
</body>
<script>
    function enviar(url){
         var a = "<iframe name='ejecutor' style='visibility:hidden'  src='<%= CONTROLLER %>" + url + "'> ";
         aa.innerHTML = a;
    }

</script>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</html>
