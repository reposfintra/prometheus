<!--
- Autor : Ing. Juan Manuel Escando Perez
- Date  : 29 Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite buscar una planilla para la impresion de la hoja de control de viaje
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%@taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<%
    String Mensaje = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
	String Modif = (request.getParameter("Modif")!=null)?request.getParameter("Modif"):"no";
	factura factu = model.facturaService.getFactu();
	Vector a = model.facturaService.getRemesas();
	System.out.println("remesas:"+a.size());
        int maxPageItems  = 40;
	int maxIndexPages = 20;
        String index      = "center";
%>
<html>
<head><title>Facturacion</title></head>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src='<%=BASEURL%>/js/validacionesImpresionPlanilla.js'></script>
<script language="JavaScript1.2">
<!--
   // Maximizar Ventana por Nick Lowe (nicklowe@ukonline.co.uk)

   window.moveTo(0,0);
   if (document.all) {
  	 top.window.resizeTo(screen.availWidth,screen.availHeight);
   }
   else if (document.layers||document.getElementById) {
	   if(top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth){
	   top.window.outerHeight = screen.availHeight;
	   top.window.outerWidth = screen.availWidth;
	   }
   }
//-->
</script>
<script>
	function cambiacolor(element){
	    
		var tr = element.parentNode.parentNode;
		tr.className = (element.checked?'filaseleccion': tr.rowIndex%2==0?'filaazul':'filagris');
	}
	function cambiacolor2(element){
		document.getElementById(element).className = (document.getElementById(element).className=='filagris'?'filaseleccion':'filagris');
    } 
	
	function aceptarCliente(modif){
	var mensa='';
	
	var boton = document.getElementById('imgaceptar');
	
	  boton.disabled=true;	 
	  if(modif=='ok'){
	  		parent.opener.location='<%= CONTROLLER %>?estado=Factura&accion=Modificar&aceptar=ok'+mensa;
	  
	     //document.form2.action = "<%= CONTROLLER %>?estado=Factura&accion=Modificar&aceptar=ok"; 
	  }else{ 
	  		//parent.opener.location='<%= CONTROLLER %>?estado=Factura&accion=Insert&aceptar=ok';
	  	    document.form2.action = "<%= CONTROLLER %>?estado=Factura&accion=Insert&aceptar=ok";
                    
	  }
	  
	  //parent.opener.close();
	  //alert("SI");	  
	  document.form2.submit();
	  
	 // parent.reload();	  	  
	  	 
	  //parent.reload();
	  // parent.opener.close();
	  
           
	}
	
	function facturarTodo(){
	     document.form2.action = "<%= CONTROLLER %>?estado=Factura&accion=Insert&opcion=FACTURAR_TODO";
	       document.form2.submit();
        }
        
	function seleccionartodo(size,check){
	   var chec =  document.getElementsByName("checkbox")
	      
           if(check == true){ 
               for (i=0;i< chec.length ;i++){
	            chec[i].checked = true;
	       }
	   }else{ 
	       for (i=0;i< chec.length ;i++){
	            chec[i].checked = false;
	       }
           }
	
	}
</script>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Agregar Items a Factura"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
	<form name="form2" method="post"  >
		<table width="888" height="288" border="2" align="center">
			<tr>
				<td height="201">
					<TABLE width="100%" align="center"  class="tablaInferior" height="138%">
				        <tr class="fila">
							<td width="25%" height="22" class="subtitulo1">&nbsp;Facturacion</td>
							<td width="75%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
						</tr>
						<tr class="fila">
						  <td height="21" align="center"><div align="left">Cliente:</div></td>
						    <td align="center" colspan='2'><div align="left"><%=factu.getNomcli()==null? "&nbsp;" : factu.getNomcli()%></div></td>
						</tr>
						<tr class="fila">
						  <td height="21" align="center"><div align="left">Nit:</div></td>
					      <td align="center"><div align="left"><%=factu.getNit()==null? "&nbsp;" : factu.getNit()%></div></td><td><a onClick="facturarTodo(); " style="cursor:hand;" class="Simulacion_Hiper" HIDEFOCUS>Facturar Todo</a></td>
					  </tr>
						<tr class="fila">
						  <td colspan="3" align="center"><table width="100%" height="39%" border="1"  bordercolor="#999999">
                            <tr class="tblTitulo">
                              <td width="73" nowrap><div align="center">Remesa </div></td>
                              <td width="71" nowrap><div align="center">Fecha</div></td>
                              <td width="239" align="center">Descripcion</td>
                              <td width="79" align="center">Cantidad</td>
                              <td width="90" align="center">Vlr Unitario </td>
                              <td width="95" align="center">Valor</td>
                              <td width="56" align="center">Moneda</td>
                              <td width="113" align="center">Seleccionar
                                  <input type="checkbox" name="checkboxall=i22" id ="checkboxall=i2" value="checkbox" onClick="seleccionartodo('<%=a.size()%>',this.checked);"></td>
                            </tr>
                           
                              
                              <pg:pager
				 items         ="<%= a.size()%>"
				 index         ="<%= index %>"
				 maxPageItems  ="<%= maxPageItems %>"
				 maxIndexPages ="<%= maxIndexPages %>"
				 isOffset      ="<%= true %>"
				 export        ="offset,currentPageNumber=pageNumber"
				 scope         ="request">
                                  <%
			                
                                  for(int i=0; i<a.size();i++){
                                      factura_detalle factu_deta = (factura_detalle)a.get(i);
                                      if (i<offset.intValue() || i >= Math.min(offset.intValue() + maxPageItems, a.size()) ){
                                      if (factu_deta.isIncluida()){%>
                                <input type="hidden" name="checkbox<%= i %>" value="<%=factu_deta.getNumero_remesa()%>">
                                <%}}
                                  }
                                  
			//for(int i=0; i<a.size();i++){
			 for (int i = offset.intValue(), l = Math.min(i + maxPageItems, a.size()); i < l; i++){
			
			//Vector b= (Vector)a.get(i);
			factura_detalle factu_deta = (factura_detalle)a.get(i);
			
			
	  %>  <pg:item>
                                    <tr title="Modificar Despacho..." class='<%= (factu_deta.isIncluida()?"filaseleccion":"filagris") %>'>
                                      <td width="9%" align="left">
                                        <div align="left"></div>
                                        <div align="left"><%=factu_deta.getNumero_remesa()%>
                                          <input type="hidden" name="remesa<%=i%>" id="remesa<%=i%>" value="<%=factu_deta.getNumero_remesa()%>">
                                      </div></td>
                                      <td width="9%"> <div align="left"><%=factu_deta.getFecrem()%> </div></td><td width="30%">
                                        <div align="left"><%=factu_deta.getDescripcion()%> </div></td>
                                      <td width="10%"><div align="right"><%=Util.customFormat(factu_deta.getCantidad())%>      <%=factu_deta.getUnidad()%></div>                                        <div align="center"></div></td>
                                      <td width="11%"><div align="right"><%=Util.customFormat(factu_deta.getValor_unitario())%></div></td>
                                      <td width="12%"><div align="right"></div>
                                        <div align="right"></div>                                        
                                        <div align="right"><%=Util.customFormat(factu_deta.getValor_item())%></div></td>
                                      <td width="6%"><div align="center"><%=factu_deta.getMoneda()%></div></td>
                                      <td width="13%" align="center">
                                        <input type="checkbox" name="checkbox<%=i%>" id="checkbox"  value="<%=factu_deta.getNumero_remesa()%>" onClick="cambiacolor(this)"  <%= (factu_deta.isIncluida()?"checked":"") %> >
                                      </td>
                                    </tr>
                </pg:item>
                                 
                                  <%
  				     }%>
                             <tr>
				<td colspan="8" align="center" >
					<pg:index>
					<jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
					</pg:index> 
				</td>
			</tr>
			</pg:pager>
                          </table></td>
					  </tr>
			  </TABLE>			  </td>
			</tr>
	  </table>
		<p align="center"><img src="<%=BASEURL%>/images/botones/aceptar.gif"  height="21" name="imgaceptar"  onMouseOver="botonOver(this);" onClick="  aceptarCliente('<%=Modif%>');" onMouseOut="botonOut(this);" style="cursor:hand">  <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close(); this.disabled=true;" onMouseOut="botonOut(this);" style="cursor:hand"> 
		</p>
		
	</form>
    <%--Mensaje de informacion--%>
	<%if(!Mensaje.equals("")){%>
		<table border="2" align="center">
			<tr>
				<td>
					<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						<tr>
							<td width="229" align="center" class="mensajes"><%= Mensaje %></td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	<%}%>
</div>
</body>
</html>
