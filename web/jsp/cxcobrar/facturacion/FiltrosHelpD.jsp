<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Descripcion de campos ingresar despacho</title>


<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<% String BASEIMG = BASEURL +"/images/botones/"; %> 
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">NUEVA FACTURA</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Cuentas por pagar    </td>
        </tr>
        <tr>
          <td  class="fila">Cliente:</td>
          <td  class="ayudaHtmlTexto">Muestra el nombre del cliente </td>
        </tr>
        <tr>
          <td  class="fila">Nit:</td>
          <td  class="ayudaHtmlTexto">Codigo nit del cliente. </td>
        </tr>
        <tr>
          <td  class="subtitulo1">Filtros:</td>
          <td  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="fila">Ruta
          :
          <input type="checkbox" name="checkbox" id="checkbox" value=""></td>
          <td  class="ayudaHtmlTexto">Cuadro de seleccion para activar el filtro por ruta. </td>
        </tr>
        <tr>
          <td  class="fila">Ciudad Origen - Ciudad Destino:
            <select name="ruta" id="ruta" class='textbox' onChange="" >
            </select></td>
          <td  class="ayudaHtmlTexto">Lista que despliega todos las rutas de las remesas del cliente. </td>
        </tr>
        <tr>
          <td  class="fila">Rango de Estandares :
          <input type="checkbox" name="checkbox2" id="checkbox2" value="" ></td>
          <td  class="ayudaHtmlTexto">Cuadro de seleccion para activar el filtro por standares. </td>
        </tr>
        <tr>
          <td  class="fila">Inicial:
            <select name="rango1" id="rango1" class='textbox' onChange=""  >
            </select> 
            final 
            <select name="select" id="select" class='textbox' onChange=""  >
            </select></td>
          <td  class="ayudaHtmlTexto">Listas que despliega todos los standares de las remesas del cliente. </td>
        </tr>
        <tr>
          <td  class="fila">Producto : 
          <input type="checkbox" name="checkbox3" id="checkbox3" value="" ></td>
          <td  class="ayudaHtmlTexto">Cuadro de seleccion para activar el filtro por producto. </td>
        </tr>
        <tr>
          <td  class="fila">Codigo:
            <select name="producto" id="select2" class='textbox' onChange="" >
            </select></td>
          <td  class="ayudaHtmlTexto">Listas que despliega todos los codigos de productos de las remesas del cliente. </td>
        </tr>
        <tr>
          <td  class="fila">Tipo de Documento : 
            <input type="checkbox" name="checkbox4" value="" ></td>
          <td  class="ayudaHtmlTexto">Cuadro de seleccion para activar el filtro por documento. </td>
        </tr>
        <tr>
          <td  class="fila">Tipo Documento: 
            <select name="documento" id="documento" class='textbox' onChange=""  >
            </select></td>
          <td  class="ayudaHtmlTexto">Listas que despliega todos tipos de documento de las remesas del cliente. </td>
        </tr>
        <tr>
          <td class="fila"> Boton Buscar <img src="<%=BASEURL%>/images/botones/buscar.gif"></td>
          <td  class="ayudaHtmlTexto">Boton para aceptar el cliente.</td>
        </tr>
        <tr>
          <td width="556" class="fila">Boton Salir <img src="<%=BASEIMG%>salir.gif"></td>
          <td width="556"  class="ayudaHtmlTexto">Boton para salir de la vista , regresando a control trafico.</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
