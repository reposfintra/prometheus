<%-- 
    Document   : ConsultaCuotasNegocio.jsp
    Created on : 12/09/2011, 03:37:13 PM
    Author     : Iris Vargas
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.util.Util"%>
<%@page session="true"%>
<%@ include file="/WEB-INF/InitModel.jsp" %>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%
            response.addHeader("Cache-Control", "no-cache");
            response.addHeader("Pragma", "No-cache");
            response.addDateHeader("Expires", 0);
            String msj = request.getParameter("msj") != null ? request.getParameter("msj") : "";
            String vista = request.getParameter("vista") != null ? request.getParameter("vista") : "1";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<!-- This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>. -->
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

        <script type='text/javascript' src="<%= BASEURL%>/js/prototype.js"></script>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/causa_anula_docs.js"></script>

        <% if (vista.equals("1")) {%>
        <title>Causacion de Intereses Parcial</title>
        <%} else if (vista.equals("2")) {%>
        <title>Anulación de Intereses</title>
        <%} else if (vista.equals("3")){%>
        <title>Generación de Cat Anticipado</title>
        <%} else if (vista.equals("4")) {%>
        <title>Generación de Cuota de Manejo Anticipada</title>
        <%}%>

        <style type="text/css">
            td{ white-space:nowrap;}

            .filableach{
                color: #000000;
                font-family: Tahoma, Arial;
                background-color:#FFFFFF;
                font-size: 12px;
            }

            .divs {
                font-family: Verdana, Tahoma, Arial, Helvetica, sans-serif;
                font-size: small;
                color: #000000;
                background-color:#9CF49C;
                -moz-border-radius: 0.7em;
                -webkit-border-radius: 0.7em;
            }
        </style>

    </head>
    <body onload="varcontr('<%=BASEURL%>','<%=CONTROLLER%>');"  >
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <% if (vista.equals("1")) {%>
            <jsp:include page="/toptsp.jsp?encabezado=Causacion de Intereses Parcial"/>
            <%} else if (vista.equals("2")) {%>
            <jsp:include page="/toptsp.jsp?encabezado=Anulación de Intereses"/>
            <%} else if (vista.equals("3")) {%>
            <jsp:include page="/toptsp.jsp?encabezado=Generación de Cat Anticipado"/>
            <%} else if (vista.equals("4")) {%>
            <jsp:include page="/toptsp.jsp?encabezado=Generación de Cuota de Manejo Anticipada"/>
            <%}%>
        </div>       
        <div id="capaCentral" style="position:absolute; width:100%; height:83%; z-index:0; left: 0px; top: 100px; overflow:scroll;">
 <form action='' name='formulario' method="post" >
            <table align="center" border="1" width="70%">
                <tbody>
                    <tr>
                        <td style="padding: 0.3em;">
                            <div id="docs" style="width: 100%;">
                                <table style="border-collapse:collapse; width:100%;" border="0">
                                    <tr class="filaazul">
                                        <td width="50%">
                                            Negocio: <br>
                                            <input name="vista" type="hidden" id="vista" value="<%= vista%>">
                                            <input name="negocio" id="negocio" value="" size="15">
                                            <img alt="buscar" src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="15" height="15" style="cursor:pointer;" onclick="cargarCuotas();">
                                             <span id="spangen"></span>
                                        </td>
                                        <td width="50%">
                                               <%if (!vista.equals("2")) {%>
                                            No  Cuotas a Facturar: <br>
                                            <input name="cuotas" id="cuotas"  value="" size="15" >
                                            <%}%>
                                        </td>
                                    </tr>
                                    <tr class="filaazul">
                                        <td colspan="2">
                                            <br>
                                            <br>
                                        </td>
                                    </tr>
                                </table>

                                <table style="border-collapse:collapse; width:100%;" border="0">
                                    <tr class="filableach">
                                        <td width="100%">
                                            <div id="negocios"></div>
                                        </td>

                                    </tr>
                                </table>

                            </div>

                        </td>
                    </tr>
                </tbody>
            </table>
 </form>
            <div align="center">
                <br>

                <img alt="" src = "<%=BASEURL%>/images/botones/aceptar.gif" style = "cursor:pointer" name = "imgaceptar" onclick = "aceptarCambios();" onmouseover="botonOver(this);" onmouseout="botonOut(this);">

                <img alt="" src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:pointer" name = "imgsalir" onclick = "window.close();" onmouseover="botonOver(this);" onmouseout="botonOut(this);">
            </div>

        </div>

        <iframe  width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
    </body>
</html>
