<%-- 
    Document   : verRecaudoAsobancariaInfo
    Created on : 15/04/2015, 05:24:46 PM
    Author     : mcastillo
--%>
<%-- 
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
--%>
<!-- Java Script-->
<script type="text/javascript" src="./js/archivoAsobancaria.js"></script>
<!-- Java Script-->
<div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 16px; ">
    <center>
        <div id="div_qryrecaudo" class="frm_search">  
            <table>
                <tr>
                    <td colspan="2">
                        <label for="fecha">Fecha de Cargue :</label>
                        <input type="text" id="startDatePicker" />
                        <input type="text" id="endDatePicker" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <label for="entidad"> Entidad Recaudo :</label>
                        <select name="entidad_recaudo" id="entidad_recaudo" style="width: 168px">
                        </select>                     
                    </td>
                    <td>
                        <label for="referencia">Referencia :</label>                
                        <input type="text" id="referencia_factura" style="width: 98px"/>
                    </td>
                    <td style="padding-left: 10px">
                        <button id="listarRecaudos" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                role="button" aria-disabled="false">
                            <span class="ui-button-text">Ver información de recaudo</span>
                        </button>
                    </td>
                </tr> 
            </table>
        </div>
        <br><br>
        <table id="tabla_cabecera_recaudo"></table>
        <div id="page_tabla_cabecera_recaudo"></div>
    </center>
   

<!--    <div id="dialogMsj" title="Mensaje" style="display:none; visibility: hidden;">
        <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
    </div>  -->

</div>

