<%-- 
    Document   : duplicadosExtractos
    Created on : 15/07/2015, 04:39:33 PM
    Author     : mariana
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <script type="text/javascript" src="./js/DuplicadosExtractos.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
    </head>
    <body>
        <style>
            label {
                font-family: initial; 
                padding-left: 5px; 
                font-size: 11px;
            }  
            button:hover{
                background: url("images/ui-bg_highlight-soft_50_4eb305_1x100.png") repeat-x scroll 50% 50% #4EB305;
                border: 1px solid #008000;
                color: #FFFFFF;
                font-weight: normal;
            }
            #tablainterna{
                border: 0;
                width: 100%;
                height: 80px;
                top: 120px;
            }
            #tablita{
                border: 1px solid ;
                border-radius: 10px 10px 10px 10px;
                border-color: #070;
                width: 941px;
                position: relative;
                top: 76px;
                background-color: #ffffff;   
            } 
            input{    
                width: 5px;
                color: black;
            }
            
            .ventana{
                display:none;    
                font-family:Arial, Helvetica, sans-serif;
                color:#808080;
                font-size:12px;
                text-align:justify;

            }

        </style>
    <center>
        <div id="tablita" style="top: 0px;width:293px;" >
            <table id="tablainterna" style="margin-top: 19px; margin-left: 8px;" >
                <tr>
                    <td>
                        <label>Cedula</label>
                    </td>
                    <td>
                        <input type="text" id="cedula" style="width: 200px" value="" >
                    </td>
                </tr>
                <tr>
                    <td>
                        <label>Codigo</label>
                    </td>
                    <td>
                        <input type="text" id="codigo" style="width: 200px"  >
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <hr>
                    </td>

                </tr>
            </table>
            <div id ='botones' >
                <button id="generar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                        role="button" aria-disabled="false">
                    <span class="ui-button-text">Pdf</span>
                </button> 

                <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                        role="button" aria-disabled="false" onclick="">
                    <span class="ui-button-text">Buscar Negocios</span>
                </button> 
            </div>
        </div>
        <div id="info"  class="ventana" >
            <p id="notific" >EXITO AL GUARDAR</p>
        </div>

        <div style="position: relative;top: 40px;">
            <table id="tabla_negocios" ></table>
            <div id="pager"></div>
        </div>

        <div id="dialogMsjdetalle"  class="ventana" >
            <table id="tabla_detalle_negocio" ></table>
            <div id="pager1"></div>
            
            <hr>
            <input type="hidden" value="" id="unidad_negocio" />
            <table border="1" style="width:900px;margin:15px auto 0 auto;">
                <tr class="ui-state-default"  >
                    <th   rowspan="2"  style="text-align:center !important" >Descuentos</th>                    
                    <th  style="text-align:center !important">IxM</th>
                    <th style="text-align:center !important">GaC</th>
                    <th  style="text-align:center !important">Valor saldo</th>
                    <th  style="text-align:center !important">Valor Mora</th>
                    <th style="text-align:center !important">Valor GaC</th>
                    <th style="text-align:center !important">total a pagar en pesos</th>
                </tr>                
                <tr>
                  
                    <td style="width: 80px;font-size: 13px;font-weight: bold">
                        <input type="number" id="ixmora"  onkeyup="soloNumeros(this.id);"  onkeypress ="aplicarDescuentosEnterMora(this.id, event)"  min='0' max='100' value='0'  style="width: 80px;text-align: center" />
                    </td>
                    <td style="width: 80px;font-size: 13px;font-weight: bold">
                        <input type="number" id="gacobranza" onkeyup="soloNumeros(this.id);"  onkeypress ="aplicarDescuentosEnterGac(this.id, event)" min='0' max='100' value='0'  style="width: 80px;text-align: center" />
                    </td>
                    <td style="width: 80px;font-size: 13px;font-weight: bold;">
                        <input type="text" id="valorsaldo" readonly="true" onkeyup="soloNumeros(this.id);" value="0" style="width: 150px;text-align: right" />
                    </td>
                    <td style="width: 80px;font-size: 13px;font-weight: bold;">
                        <input type="text" id="valorMora" readonly="true" onkeyup="soloNumeros(this.id);" value="0" style="width: 150px;text-align: right" />
                    </td>
                    <td style="width: 80px;font-size: 13px;font-weight: bold;">
                        <input type="text" id="valorGac" readonly="true" onkeyup="soloNumeros(this.id);" value="0" style="width: 150px;text-align: right" />
                    </td>
                    <td style="width: 80px;font-size: 13px;font-weight: bold;">
                        <input type="text" id="totalExtracto" readonly="true" onkeyup="soloNumeros(this.id);" value="0" style="width: 150px;text-align: right" />
                    </td>
                </tr>                      
            </table>

        </div>
        
        <div id="dialogo2" class="ventana">
            <p id="msj2">texto </p> <br/>
            <center>
                <img src="./images/cargandoCM.gif"/>
            </center>
        </div>

    </center>
</body>
</html>
