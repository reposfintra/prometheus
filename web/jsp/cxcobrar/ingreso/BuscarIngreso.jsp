<%@page session="true"%>
<%@page import="java.util.*" %>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>  
<title>Buscar Ingreso</title> 
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">  
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/ingreso.js"></script>


</head>
<body onResize="redimensionar();" onLoad='redimensionar();' > 

<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path); 

String res = (request.getParameter("msg")!= null)? request.getParameter("msg") : "";%>

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Ingreso"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<FORM ACTION="<%=CONTROLLER%>?estado=Ingreso&accion=Buscar&carpeta=jsp/cxcobrar/ingreso&pagina=ingresoMod.jsp" METHOD='post' id='form1' name='form1'>
  <table width="500" border="2" align="center">
    <tr>
      <td><table width="100%"  border="0">
        <tr>
          <td width="50%" class="subtitulo1">&nbsp;Buscar Ingreso</td>
          <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
          </tr>
      </table>      <table width="100%" class="tablaInferior">
        
        <tr class="fila">
          <td width="121">            Nro. Ingreso </td>
          <td><input  class='textbox' type='text' name='numero' size='15' maxlength='11' onKeyPress="soloAlfa(event,'decNO');"   >
            </td>
          <td width="107">Nro. Factura</td>
          <td width="96"><input  class='textbox' type='text' name='factura' size='15' maxlength='11' onKeyPress="soloAlfa(event,'decNO');"   ></td>
        </tr>
		<tr class="fila">
          <td width="121">            Tipo</td>
          <td colspan="3"><select name="tipodoc" class="textbox" id="select" style="width:90% ">
           <option value="%">Seleccione un items</option>
            <% LinkedList tbltipo = model.tablaGenService.getTipoIngresos();
               for(int i = 0; i<tbltipo.size(); i++){
                       TablaGen reg = (TablaGen) tbltipo.get(i); %>
            <option value="<%=reg.getTable_code()%>"><%=reg.getDescripcion()%></option>
            <%}%>
          </select></td>
        </tr>
        <tr class="fila">
          <td>            Cliente</td>
          <td width="94"><input name="cliente" type="text" class="textbox" id="cliente" onKeyPress="soloAlfa(event,'decNO');" onBlur="completarCodigo();"  size="10" maxlength="7" ></td>
          <td colspan="2"><span class="Simulacion_Hiper" style="cursor:hand " onClick="window.open('<%=BASEURL%>/consultas/consultasClientes.jsp','','HEIGHT=200,WIDTH=600,SCROLLBARS=YES,RESIZABLE=YES,STATUS=NO')">Buscar Clientes</span></td>
          </tr>
		  <tr class="fila">
          <td>            Estado</td>
          <td colspan="3"><select name="reg_status" id="reg_status">
  		               <option value="%">Todos</option>
		               <option value="">Activos</option>
   		               <option value="A">Anulados</option>
          </select></td>
          </tr>
        <tr class="fila">
          <td>            Fecha</td>
          <td><input  name='fechaInicio' id='fechaInicio'  size="11" readonly="true" class="textbox" >
            <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(fechaInicio);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt=""></a></td>
          <td colspan="2"><input  name='fechaFinal' id='fechaFinal' size="11" readonly="true" class="textbox" >
            <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(fechaFinal);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt=""></a></td>
          </tr>
        <input type="hidden" id="evento" name="evento" value="">
      </table></td>
    </tr>
  </table>
 <br>
  <div align="center"><img src="<%=BASEURL%>/images/botones/buscar.gif"  name="imgbuscar" onClick="validarBusqueda();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></div>
  <br>
<%if(!res.equals("")){%>
 <br>
  <table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="300" align="center" class="mensajes">La busqueda no arrojo resultados</td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>

 <%}%>

</form>
</div>
<%=datos[1]%>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>
