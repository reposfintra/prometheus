<%@page session="true"%>
<%@page import="java.util.*" %>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>  
<title>Consultar Ingreso</title> 
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/ingreso.js"></script>
</head>
<body onResize="redimensionar();" onLoad='redimensionar();' > 

<%
	String res = (request.getParameter("msg")!= null)? request.getParameter("msg") : "";
	TreeMap opc_docs = model.tblgensvc.getLista_des();
%>

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Consultar Ingreso"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<FORM ACTION="<%=CONTROLLER%>?estado=Cargar&accion=Varios&carpeta=jsp/cxcobrar/ingreso&pagina=ListadoIngresos.jsp&sw=11" METHOD='post' id='form1' name='form1'>
  <table width="500" border="2" align="center">
    <tr>
      <td><table width="100%"  border="0">
        <tr>
          <td width="50%" class="subtitulo1">&nbsp;Buscar Ingreso</td>
          <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
      </table>      <table width="100%" class="tablaInferior">
        
        <tr class="fila"> 
          <td width="107">Nro. Ingreso </td>
          <td><input name='ingreso' type='text'  class='textbox' id="ingreso" size='15' maxlength='11'   >
            </td>
          <td>Periodo</td>
          <td><input name='periodo' type='text'  class='textbox' id="periodo" size='5' maxlength='5'   ></td>
        </tr>
        <tr class="fila">            
          <td width="107"> Tipo Documento </td>
          <td><input:select name="tipo_documento" options="<%= opc_docs %>" attributesText="class='textbox' id='tipo_documento' style='width:80%'" /></td>
          <td width="81">Documento</td>
          <td width="151"><input name='documento' type='text'  class='textbox' id="documento"  size='15' maxlength='11'   ></td>
        </tr>
        <tr class="fila">
          <td>            Cliente</td>
          <td width="129"><input name="cliente" type="text" class="textbox" id="cliente" onKeyPress="soloDigitos(event,'decNO');" onBlur="if(this.value!=''){completarCodigo();}"  size="10" maxlength="7" ></td>
          <td colspan="2"><span class="Simulacion_Hiper" style="cursor:hand " onClick="window.open('<%=BASEURL%>/consultas/consultasClientes.jsp','','HEIGHT=200,WIDTH=600,SCROLLBARS=YES,RESIZABLE=YES,STATUS=NO')">Buscar Clientes</span></td>
          </tr>
		  <tr class="fila">
		  <td>Tipo Ingreso </td>
		  <td><select name="tipo_ingreso" id="tipo_ingreso" class="textbox">
				<option value=""></option>
				<option value="C">Cliente</option>
				<option value="M">Miscelaneo</option>
		      </select></td>
		  <td>Estado</td>
		  <td><select name="estados" id="estados" class="textbox">
				<option value=""></option>
				<option value="T">Detallado Total</option>
				<option value="P">Detalado Parcial</option>
				<option value="N">Pendiente por Detallar</option>
		      </select></td>
		  </tr>
        <tr class="fila">
          <td>            Fecha Ingreso</td>
          <td><input  name='fechaInicio' size="11" readonly="true" class="textbox" >
            <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(fechaInicio);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt=""></a></td>
          <td colspan="2"><input  name='fechaFinal' size="11" readonly="true" class="textbox" >
            <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(fechaFinal);return false;" hidefocus><img name="popcal2" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt=""></a></td>
          </tr>
      </table></td>
    </tr>
  </table>
 <br>
  <div align="center"><img src="<%=BASEURL%>/images/botones/buscar.gif"  name="imgbuscar" onClick="form1.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></div>
  <br>

</form>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>
