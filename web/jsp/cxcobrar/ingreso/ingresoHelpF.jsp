<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE></TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<style type="text/css">
<!--
.Estilo2 {
	font-size: 16pt;
	color: #FF0000;
}
-->
</style>
</HEAD>
<BODY> 

        <table width="90%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20" align="center">MANUAL DE REGISTRO DE INGRESO DE PAGOS CLIENTE </td>
          </tr>
          <tr class="subtitulo1">
            <td> Registro de Ingreso de cliente </td>
          </tr>
          <tr>
            <td  height="8"  class="ayudaHtmlTexto"><p>Para registrar el ingreso de pagos debe digitar el c&oacute;digo del cliente, autom&aacute;ticamente el cursor sale del campo de texto buscar la informaci&oacute;n correspondiente a ese cliente.</p>            </td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/ingreso/imagen1.jpg" ></div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><p align="left">              El sistema muestra la informaci&oacute;n correspondiente al cliente, si ese cliente tiene un banco asignado, autom&aacute;ticamente selecciona el banco y la sucursal asignada.</p>
            </td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/ingreso/imagen2.jpg">
              </div></td>
          </tr>
<tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><p align="left">Luego selecciona las fechas de consignaci&oacute;n y digita la informaci&oacute;n restante para realizar el ingreso, presione el bot&oacute;n aceptar para almacenar la informaci&oacute;n a la base de datos .</p>
            </td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/ingreso/imagen3.jpg">
              </div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center">&nbsp;</td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"></div></td>
          </tr>
         
      </table>
</BODY>
</HTML>
