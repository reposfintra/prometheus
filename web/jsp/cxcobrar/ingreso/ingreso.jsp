
<%@page session="true"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@page import ="com.tsp.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<% 
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path); 
  Usuario usuario = (Usuario) session.getAttribute("Usuario");
  String ag = usuario.getId_agencia();

  TreeMap bancos = model.servicioBanco.getBanco();
  TreeMap sucursales = model.servicioBanco.getSucursal();
  sucursales.put(" Seleccione Un Item", "");
  bancos.put(" Seleccione Un Item", "");

   Vector VecMon = model.monedaService.getMonedas();
   Moneda moneda;

  String sw = (request.getParameter("sw")!= null)?request.getParameter("sw"):"" ;
  String nom= (request.getParameter("nombre")!= null)?request.getParameter("nombre"):"" ;
  String nit=(request.getParameter("nit")!= null)?request.getParameter("nit"):"" ;
  String cod=(request.getParameter("cliente")!= null)?request.getParameter("cliente").toUpperCase():"" ;
  String fec=(request.getParameter("fecha")!= null)?request.getParameter("fecha"):Util.getFechaActual_String(4);
  String mon = (request.getParameter("moneda")!= null)?request.getParameter("moneda"):"" ;
  String con = (request.getParameter("concepto")!= null)?request.getParameter("concepto"):"" ;
  String res =  (String)session.getAttribute("msg")!= null?(String)session.getAttribute("msg"):"" ;
  String res1 = (String)session.getAttribute("msg2")!= null?(String)session.getAttribute("msg2"):"" ;
  String ban = (request.getParameter("banco")!= null)?request.getParameter("banco"):"" ;
  String suc = (request.getParameter("sucursal")!= null)?request.getParameter("sucursal"):"" ;
  String des = (request.getParameter("descripcion")!= null)?request.getParameter("descripcion"):"" ;
  String val = (request.getParameter("valor")!= null)?request.getParameter("valor").replace(",",""):"";
  String tp = (request.getParameter("tipodoc")!= null)?request.getParameter("tipodoc"):"" ;
  String cuenta = (request.getParameter("cuenta1")!= null)?request.getParameter("cuenta1"):"" ;
  String tipo1 = (request.getParameter("tipo1")!= null)?request.getParameter("tipo1"):"" ;
  String aux = (request.getParameter("auxiliar1")!= null)?request.getParameter("auxiliar1"):"" ;
  String cuen = (request.getParameter("cuentas")!= null)?request.getParameter("cuentas"):"" ;
  String tasa = (request.getParameter("tasa")!= null)?request.getParameter("tasa"):"" ;
  String abc = (request.getParameter("abc")!= null)?request.getParameter("abc"):"" ;
  String nro_extracto = (request.getParameter("nro_extracto")!= null)?request.getParameter("nro_extracto"):"" ;
  if( sw.equals("ok") ){		
        Cliente clin  = model.clienteService.getCliente();
        nom = clin.getNomcli();
        nit = clin.getNit(); 
		String vec[] = suc.split("/");
		if( vec != null && vec.length > 1 ){
			cuen = vec[1];
		}
		else{
			cuen = "";
		}
 }
LinkedList t_aux = modelcontab.subledgerService.getCuentastsubledger();

%>
<html>
<head>
<title>Ingreso de Pagos Cliente</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/ingreso.js"></script>
<script language="javascript" src="<%=BASEURL%>/js/general.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 

<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">

</head>
<body onLoad="tIngreso();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ingreso de Pagos Cliente"/>
</div>

 <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
  <br>
  <form name="form1" method="post" action="<%=CONTROLLER%>?estado=Ingreso&accion=Registrar&carpeta=jsp/cxcobrar/ingreso&pagina=ingreso.jsp" id="form1" >
	<table width="750"  border="2" align="center">
		<tr>
			<td>	     
				<table width="100%">
					<tr>
						<td width="50%" class="subtitulo1">&nbsp;Información del Ingreso</td>
						<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%>
							<input type="hidden" name="numero" value="<%=res%>"></td>
					</tr>
				</table>
				<table width="100%">
					<tr  class="fila">
						<td>Tipo Ingreso</td>
						<td><select name="tipodoc" id="tipodoc" class="textbox" id="select" style="width:90% " onChange="tIngreso();">
							<% LinkedList tbltipo = model.tablaGenService.getTipoIngresos();
							for(int i = 0; i<tbltipo.size(); i++){
								TablaGen reg = (TablaGen) tbltipo.get(i); 
								if( (!ag.equals("OP") && reg.getTable_code().equals("ING") ) || ag.equals("OP") ){%>
									<option value="<%=reg.getTable_code()%>" <%=(reg.getTable_code().equals(tp))?"selected":""%> ><%=reg.getDescripcion()%></option>
								<%}
							}%>
							</select></td>
						<td colspan="4" style="display:none" id="celdahc"> HC: <% model.tablaGenService.listarHc();TreeMap hcs = model.tablaGenService.getHcs(); %><input:select  name="ForHC" attributesText="style='width:40%;' class='listmenu'" options="<%=hcs%>" /></td>
					</tr>
					<tr>
						<td width="14%" class="fila">Cliente </td>
						<td colspan="5" class="letra">
							<table width="100%" border="0" cellpadding="0" cellspacing="0" >
								<tr class="letra">
								<td width="25%">
									<%if(res.equals("error") || res.equals("series") || res.equals("") ){%><input name="cliente" type="text" class="textbox" id="cliente" onBlur="Buscar(event,'decOK','<%=CONTROLLER%>')"  onKeyPress="soloAlfa(event,'decNO');"  size="10" maxlength="8" value="<%=cod%>">
										<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"><%}else{%><%=cod%>
									<%}%>
								</td>
								<td width="55%">&nbsp;<%=nom%>
									<input name="nombre" type="hidden" id="nombre" value="<%=nom%>">
									<input type="hidden" name="nit" value="<%=nit%>">
								</td>
								<td width="20%"><span class="Simulacion_Hiper" style="cursor:hand " onClick="window.open('<%=BASEURL%>/consultas/consultasClientes.jsp','','HEIGHT=200,WIDTH=600,SCROLLBARS=YES,RESIZABLE=YES,STATUS=NO')">Buscar Clientes</span></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr class="fila" id="cuenta" style="display:none">
						<td>Cuenta</td>
						<td><input name='cuenta1' type='text' id='cuenta1' class="textbox" size='20' style="font-size:11px " maxlength='25' onBlur="VerificarCuentaContable(1,3);"   value="<%=cuenta%>" onFocus="this.select()">
						<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
						<td>Tipo</td>
						<td id="combotipo1" nowrap>
							<%if(t_aux==null){%>
								<select name='tipo1'>
									<option value=''> </option>
								</select>
							<%}else{%>
								<select name='tipo1' class='textbox' id='tipo1' onChange="form1.auxiliar1.value='';" >
									<option value=''>  </option> 
									<% for(int j = 0; j<t_aux.size(); j++){
										TablaGen tipo = (TablaGen) t_aux.get(j); %>
										<option value='<%=tipo.getTable_code()%>' <%=(tipo.getTable_code().equals(tipo1) )? "selected" : "" %> ><%=tipo.getTable_code()%></option>";
									<%}%>
								</select>
							<%}%></td>
						<td>Auxiliar</td>
						<td><input name='auxiliar1' class="textbox" type='text' id='auxiliar1' size='20' style="font-size:11px " maxlength='25' value ="<%=aux%>" onBlur="validarAuxiliar('<%=BASEURL%>','1');" onFocus="this.select()">
							<img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name="imagenAux" width="15" height="15"  id="imagenAux" style="cursor:hand"  onClick="buscarAuxiliar('<%=BASEURL%>','1',form1.cuenta1.value, form1.tipo1.value);"  ></td>
					</tr>

					<tr class="fila" id="banco" style="display:none">
						<td>Banco</td>
						<td><input:select name="banco" default="<%=ban%>" attributesText="class=textbox; onChange=\"form1.valor.value = sinformato(form1.valor.value);cargarSelects('controller?estado=Ingreso&accion=Cargar&carpeta=jsp/cxcobrar/ingreso&pagina=ingreso.jsp&evento=sucursal')\"" options="<%= bancos %>"/> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
							<script>form1.banco.value = '<%=ban.replaceAll("\n","")%>';</script>
						<td>Sucursal</td>
						<td colspan="0"><input:select name="sucursal" default="<%=suc%>" attributesText="class=textbox; onChange=colocarCuenta();" options="<%= sucursales %>" /> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
							<input name="cuentas" id="cuentas"  class="filaresaltada" style='width:95%;text-align:left; border:0;' value="<%=cuen%>"></td>
							<script>form1.sucursal.value = '<%=suc.replaceAll("\n","")%>';</script>
                                                        
					</tr>
					
					<tr  class="fila">
						<td>Fecha Consignaci&oacute;n</td>
						<td id="Fecha_ABC"></td>
						<td>Concepto</td>
						<td colspan="3">
							<select name="concepto" class="textbox" id="concepto" style="width:50% ">
								<% LinkedList tblres = model.tablaGenService.obtenerTablas();
								for(int i = 0; i<tblres.size(); i++){
									TablaGen respa = (TablaGen) tblres.get(i); %>
									<option value="<%=respa.getTable_code()%>" <%=(respa.getTable_code().equals(con))?"selected":""%> ><%=respa.getDescripcion()%></option>
								<%}%>
							</select>
							<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
                                        </tr>
					
					<tr  class="fila">
						<td nowrap id="val">Valor Consignación</td>
						<td><input name="valor" type="text" class="textbox" id="valor"  onKeyPress="soloDigitos_signo(event,'decOK');" onFocus="this.select()"
								onChange="
									if(form1.moneda.value == 'DOL'){ 
										formatoDolar(this,2); 
									}else{ 
										this.value = Math.round( parseFloat( sinformato( this.value ) ) );
										valor.value = formato(this.value);
									}" size="20" maxlength="15" 
								value="<%=(mon.equals("DOL"))?UtilFinanzas.customFormat2( (val.equals(""))?"0":val ):UtilFinanzas.customFormat((val.equals(""))?"0":val)%>" style="text-align:right;">
						<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
						<td>Moneda</td>
						<td>
							<select name="moneda"  class="textbox" id="moneda" onChange="cambiar(this.value)">
							<%if ( VecMon.size() > 0 ) {  
								for ( int i=0;  i < VecMon.size(); i++ ){ 
									moneda = (Moneda) VecMon.elementAt(i);%>
									<option value="<%=moneda.getCodMoneda()%>" <%=(moneda.getCodMoneda().equals(mon))?"selected":""%>><%=moneda.getNomMoneda()%></option>
								<%}
							}%>
							</select>
							<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
						<td>Dolar A Bolivar</td>
						<td><input name="tasa" type="text" class="textbox" id="tasa" value="<%=!tasa.equals("")?UtilFinanzas.customFormat("#,###.##########", Double.parseDouble( tasa ),10 ):""%>"  onKeyPress="soloDigitos(event,'decOK')" size="15" maxlength="15" <%=mon.equals("DOL")||mon.equals("BOL")?"":"readonly"%>></td>
					</tr>
                                         <tr class="fila" id="abss">
                                             <td nowrap id="TDabc" ></td>
                                             <td id="Fecha_ABC1"></td>
                                             <td ></td>
                                             <td ></td>
                                             <td ></td>
                                             <td ></td>
                                        </tr>
		  
					<tr  class="fila">
						<td>Descripci&oacute;n</td>
						<td colspan="5"><textarea name="descripcion" cols="90%" rows="3" class="textbox"><%=des%></textarea></td>
					</tr>
                                        <tr  class="fila">
						<td>Numero Extracto</td>
                                                <td colspan="5"><input value="<%=nro_extracto%>" onkeypress="soloNumeros(this.id)" maxlength="10" name="nro_extracto" id="nro_extracto" /></td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br>
	<div align="center">
		<%if(!res.equals("") && !res.equals("error")&& !res.equals("series")){%>
			<input name="opcion" type="hidden" id="opcion" value="imp">
			<img src="<%=BASEURL%>/images/botones/imprimir.gif"  name="imgimp"  onMouseOver="botonOver(this);" onClick="form1.valor.value = sinformato(form1.valor.value);form1.submit();" onMouseOut="botonOut(this);" style="cursor:hand">
			<%if(ag.equals("OP")){%>           
				<img src="<%=BASEURL%>/images/botones/detalles.gif"  name="imgdetalle" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick="window.open('<%=CONTROLLER%>?estado=Cargar&accion=Varios&numero_ingreso=<%=res%>&sw=14&tipo_doc=<%=tp%>&pagina=ingresar','ING','status=yes,scrollbars=no,width=1024,height=680,resizable=yes');">
			<%}
		}else{%>
			<img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="validarIngresoCliente('<%=BASEURL%>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
		<%}%>
		<img src="<%=BASEURL%>/images/botones/restablecer.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=ingreso.jsp&carpeta=/jsp/cxcobrar/ingreso&marco=no&opcion=cxcobrar'" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;
		<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
	</div>
 <%if(!res.equals("")){%>
	<br>
	<table border="2" align="center">
		<tr>
			<td>
				<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
					<tr>
						<td width="300" align="center" class="mensajes">
						<%if(res.equals("error")){%>
							No existen Registro en la Tasas para la conversion
						<%}else if(res.equals("serie")){%>
							No existe serie para el ingreso 
						<%}else{%>
							El ingreso del Cliente <%=res1%><br>Se ha registrado con Nro <%=res%>
							<input name="num" type="hidden" id="num" value='<%=res%>'> 
						<%}%>
						</td>
						<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
						<td width="58">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>

<%}%>
<%if( request.getAttribute("mensaje") != null){%>
	<br>
	<table border="2" align="center">
		<tr>
			<td>
				<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
					<tr>
						<td width="300" align="center" class="mensajes">
							<%=(String)request.getAttribute("mensaje")%></td>
						<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
						<td width="58">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<%}%>
</form>
</div>
<%=datos[1]%>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>
<%if( sw.equals("no") ){%>
	<script>
		alert("No existe registrado el cliente");
		form1.cliente.value = '';     
	</script>
<%}%>
<script>
    function enviar(url){
         var a = "<iframe name='ejecutor' style='visibility:hidden'  src='<%= CONTROLLER %>" + url + "'> ";
         aa.innerHTML = a;
    }
	function colocarCuenta(){
		var vec = form1.sucursal.value.split("/");
		if( form1.sucursal.value != "" ){
			if( vec != null )
				form1.cuentas.value = vec[1];
		}
		else{
			form1.cuentas.value = "";
		}
	}
	
	
	function tIngreso(){
		var tipo = document.getElementById('tipodoc').value;
               // alert(tipo);
		if( tipo=='ICR' || tipo == 'ICA'){
			cuenta.style.display="block";
			banco.style.display="none";
			val.innerHTML = 'Valor';
			TDabc.innerHTML = 'ABC';
			celdahc.style.display="none";
                        abss.style.display="block"
                        Fecha_ABC.innerHTML="<input  type='text' name='fecha' id='fecha' size='12' class='textbox' value='<%=fec%>' readonly> <a id='calen' href='javascript:void(0)' onclick='if(self.gfPop)gfPop.fPopCalendar(document.form1.fecha);return false;' HIDEFOCUS> <img src='<%=BASEURL%>/js/Calendario/cal.gif' width='16' height='16' border='0' alt='De click aqui para escoger la fecha'></a>	<img src='<%=BASEURL%>/images/botones/iconos/obligatorio.gif' width='10' height='10'>";
			Fecha_ABC1.innerHTML="<input name='abc' type='text' class='textbox' id='abc' value='<%=abc%>' size='10' maxlength='10'> <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name='imagen' id='imagen' width='15' height='15' style='cursor:hand' title='Buscar ABC'  onClick='abrirABC()'; >";
			if(tipo=='ICA')
			{
				celdahc.style.display="block";
				banco.style.display="block";
	}
		}else{
			cuenta.style.display="none";
			banco.style.display="block";
			val.innerHTML = 'Valor Consignación';
			//TDabc.innerHTML = 'Fecha Consignación';
			celdahc.style.display="none";
                        abss.style.display="none"
			Fecha_ABC.innerHTML="<input  type='text' name='fecha' id='fecha' size='12' class='textbox' value='<%=fec%>' readonly> <a id='calen' href='javascript:void(0)' onclick='if(self.gfPop)gfPop.fPopCalendar(document.form1.fecha);return false;' HIDEFOCUS> <img src='<%=BASEURL%>/js/Calendario/cal.gif' width='16' height='16' border='0' alt='De click aqui para escoger la fecha'></a>	<img src='<%=BASEURL%>/images/botones/iconos/obligatorio.gif' width='10' height='10'>";

		}
	}
	function abrirABC(){
		window.open('<%=BASEURL%>/jsp/cxcobrar/ingreso/ListaABC.jsp' ,'ABC','status=yes,scrollbars=no,width=600,height=500,resizable=yes');
	}
	
</script>
<font id='aa'></font>
