<%@page session="true"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@page import ="com.tsp.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%  
  Usuario usuario = (Usuario) session.getAttribute("Usuario");
  String ag = usuario.getId_agencia();

  TreeMap bancos = model.servicioBanco.getBanco();
  TreeMap sucursales = model.servicioBanco.getSucursal();
  sucursales.put(" Seleccione Un Item", "");
  bancos.put(" Seleccione Un Item", "");
   LinkedList t_aux = modelcontab.subledgerService.getCuentastsubledger();
   Vector VecMon = model.monedaService.getMonedas();
   Moneda moneda;

  String sw = (request.getParameter("sw")!= null)?request.getParameter("sw"):"" ;
  String nom= (request.getParameter("nombre")!= null)?request.getParameter("nombre"):"" ;
  String nit=(request.getParameter("nit")!= null)?request.getParameter("nit"):"" ;
  String cod=(request.getParameter("cliente")!= null)?request.getParameter("cliente"):"" ;
  String fec=(request.getParameter("fecha")!= null)?request.getParameter("fecha"):Util.getFechaActual_String(4);
  String mon = (request.getParameter("moneda")!= null)?request.getParameter("moneda"):"" ;
  String con = (request.getParameter("concepto")!= null)?request.getParameter("concepto"):"" ;
  String res = (request.getParameter("msg")!= null)?request.getParameter("msg"):"" ;
  String res1 = (request.getParameter("msg2")!= null)?request.getParameter("msg2"):"" ;
  String ban = (request.getParameter("banco")!= null)?request.getParameter("banco"):"" ;
  String suc = (request.getParameter("sucursal")!= null)?request.getParameter("sucursal"):"" ;
  String des = (request.getParameter("descripcion")!= null)?request.getParameter("descripcion"):"" ;
  String val = (request.getParameter("valor")!= null)?request.getParameter("valor").replace(",",""):"";
  String num =  (request.getParameter("num")!= null)?request.getParameter("num"):"";
  int cant =   (request.getParameter("cant")!= null)? Integer.parseInt(request.getParameter("cant")):0;
  String modificar = (request.getParameter("modificar") != null)? request.getParameter("modificar") : "";
  String tipodoc = (request.getParameter("tipodoc") != null)? request.getParameter("tipodoc") : "";
  String feccon = (request.getParameter("feccon") != null)? request.getParameter("feccon") : "";
  String destipo = (request.getParameter("destipo") != null)? request.getParameter("destipo") : "";
  String estado = "";
  int trans =   (request.getParameter("trasn")!= null)? Integer.parseInt(request.getParameter("trasn")):0;
  String cuenta = (request.getParameter("cuenta1")!= null)?request.getParameter("cuenta1"):"" ;
  String tipo1 = (request.getParameter("tipo1")!= null)?request.getParameter("tipo1"):"" ;
  String aux = (request.getParameter("auxiliar1")!= null)?request.getParameter("auxiliar1"):"" ;
  String cuen = (request.getParameter("cuentas")!= null)?request.getParameter("cuentas"):"";
  String tasa = (request.getParameter("tasa")!= null)?!request.getParameter("tasa").equals("")?request.getParameter("tasa"):"0":"0" ;
  String abc = (request.getParameter("abc")!= null)?request.getParameter("abc"):"" ;
  String usua = (request.getParameter("usua")!= null)?request.getParameter("usua"):"" ;
  String subingreso = (request.getParameter("subingreso")!= null)?request.getParameter("subingreso"):"" ;  
  String nro_extracto = (request.getParameter("nro_extracto")!= null)?request.getParameter("nro_extracto"):"" ;

  if( sw.equals("ok") ){		
        Ingreso ing  = model.ingresoService.getIngreso();
        num = ""+ing.getNum_ingreso();
        cod = ing.getCodcli();
        nom = ing.getNomCliente();
        ban = ing.getBranch_code();
		usua = ing.getCreation_user();
		String vec[] = ing.getBank_account_no().split("/");
		if( vec != null && vec.length > 1 ){
			cuen = vec[1];
		}
		suc = ing.getBank_account_no();
        fec = ing.getFecha_consignacion();
        con = ing.getConcepto();
        mon = ing.getCodmoneda();  
        des = ing.getDescripcion_ingreso();
        val = ""+ing.getVlr_ingreso_me();
        cant = ing.getCant_item();
        tipodoc = ing.getTipo_documento();
        feccon = ing.getFecha_contabilizacion();
        destipo = ing.getDestipo();
		estado = ing.getReg_status();
		trans  = ing.getTransaccion();
		cuenta = ing.getCuenta();
		tipo1 = ing.getTipo_aux();
		aux  = ing.getAuxiliar(); 
		abc  = ing.getAbc(); 
		tasa = String.valueOf(ing.getTasaDolBol());   
       nro_extracto=""+ing.getNro_extracto();
 }
%>
<html>
<head>
<title>Ingreso de Pagos Cliente</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/ingreso.js"></script>
<script language="javascript" src="<%=BASEURL%>/js/general.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 

<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<!--  onLoad="redimensionar();"-->
<body onLoad="tIngreso();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ingreso de Pagos Cliente"/>
</div>

 <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
  <br>
  <form name="form1" method="post" action="<%=CONTROLLER%>?estado=Ingreso&accion=Modificar&carpeta=jsp/cxcobrar/ingreso&pagina=ingresoMod.jsp" id="form1" onSubmit="validarIngreso();">
    <table width="750"  border="2" align="center">
	<tr>
	  <td>	     
	    <table width="100%">
          <tr>
            <td width="50%" class="subtitulo1">&nbsp;Información del Ingreso</td>
            <td width="50%" class="barratitulo" align="center"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><div align="right" class="letra"><strong>Elaborado Por:</strong> <%=usua%></div></td>
          </tr>
        </table>
	    <table width="100%">
          
			<tr>
				<td  class="fila"><%=destipo%> Nro</td>
				<td class="letra">&nbsp;<%=num%><input name="num" type="hidden" id="num" value="<%=num%>"><input name="tipodoc" type="hidden" id="tipodoc" value="<%=tipodoc%>"><input type="hidden" name="destipo" value="<%=destipo%>"></td>
				<td class="fila">Cliente</td>
				<td colspan="3" class="letra"><%=cod%>
					<input name="cliente" type="hidden" class="textbox" id="cliente"   size="10" maxlength="6" value="<%=cod%>"> - <%=nom%>
					<input name="nombre" type="hidden" id="nombre" value="<%=nom%>">
					<input name="cant" type="hidden" id="cant" value="<%=cant%>"></td>
			</tr>
		  
			<tr class="fila"  id="cuenta" style="display:none">
				<td>Cuenta</td>
				<td><input name='cuenta1' type='text' id='cuenta1' class="textbox" size='20' style="font-size:11px " maxlength='25' onBlur="VerificarCuentaContable(1,3);"   value="<%=cuenta%>" onFocus="this.select()">
					<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
				<td>Tipo</td>
				<td id="combotipo1">
					<select name='tipo1' class='textbox' id='tipo1' onChange="form1.auxiliar1.value='';" >
						<%if(t_aux!=null){ 
							for(int j = 0; j<t_aux.size(); j++){
								TablaGen tipo = (TablaGen) t_aux.get(j); %>
								<option value='<%=tipo.getTable_code()%>' <%=(tipo.getTable_code().equals(tipo1) )? "selected" : "" %> ><%=tipo.getTable_code()%></option>
							<%}
						}%>
					</select>
					<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
				<td>Auxiliar</td>
				<td><input name='auxiliar1' type='text' id='auxiliar1' class="textbox" size='20' style="font-size:11px " maxlength='25' value ="<%=aux%>" onBlur="validarAuxiliar('<%=BASEURL%>','1');" onFocus="this.select()">
					<img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name="imagenAux" width="15" height="15"  id="imagenAux" style="cursor:hand"  onClick="buscarAuxiliar('<%=BASEURL%>','1',form1.cuenta1.value, form1.tipo1.value);"  ></td>
			</tr>
			
			<tr  class="fila" id="banco" style="display:none">
				<td>Banco</td>
				<td><%if(estado.equals("A")){%>
					<input:select name="banco" default="<%=ban%>"  attributesText="class=textbox; disabled=true; onChange=\"cargarSelects('controller?estado=Ingreso&accion=Cargar&carpeta=jsp/cxcobrar/ingreso&pagina=ingresoMod.jsp&evento=sucursal')\"" options="<%= bancos %>"/> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
					<%}else{%>
					<input:select name="banco" default="<%=ban%>"  attributesText="class=textbox;  onChange=\"cargarSelects('controller?estado=Ingreso&accion=Cargar&carpeta=jsp/cxcobrar/ingreso&pagina=ingresoMod.jsp&evento=sucursal')\"" options="<%= bancos %>"/> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
					<%}%>
					<script>form1.banco.value = '<%=ban.replaceAll("\n","")%>';</script>
				</td>
				<td>Sucursal</td>
				<td colspan="3">
					<%if(estado.equals("A")){%>
						<input:select name="sucursal" default="<%=suc%>" attributesText="class=textbox; disabled=true;" options="<%= sucursales %>" /> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
					<%}else{%>
						<input:select name="sucursal" default="<%=suc%>" attributesText="class=textbox; onChange=colocarCuenta();" options="<%= sucursales %>" /> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
					<%}%>
					<input name="cuentas" id="cuentas"  class="filaresaltada" <%if(estado.equals("A")){%>disabled<%}%> style='width:60%;text-align:left; border:0;' value="<%=cuen%>">
					<script>form1.sucursal.value = '<%=suc.replaceAll("\n","")%>';</script>
				</td>
			</tr>
          
			<tr class="fila">
				<td id="TDabc">Fecha Consignaci&oacute;n</td>
				<td id="Fecha_ABC"></td>
				<td>Concepto</td>
				<td colspan="3"><select name="concepto" class="textbox" id="concepto" style="width:50% ">
					<option value="">Seleccione Un Item</option>
						<% LinkedList tblres = model.tablaGenService.obtenerTablas();
						for(int i = 0; i<tblres.size(); i++){
							TablaGen respa = (TablaGen) tblres.get(i); %>
							<option value="<%=respa.getTable_code()%>" <%=(respa.getTable_code().equals(con))?"selected":""%> ><%=respa.getDescripcion()%></option>
						<%}%>
					</select>
					<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">                  </td>
		    </tr>
			
			<tr class="fila">
				<td id="val">Valor Consignación</td>
				<td><input name="valor" type="text" class="textbox" id="valor"  onFocus="this.select()" 
						onChange="
							if(form1.moneda.value == 'DOL'){ 
								formatoDolar(this,2); 
							}else{ 
								this.value = Math.round( parseFloat( sinformato( this.value ) ) );
								valor.value = formato(this.value);
							}"
							size="15" maxlength="15" value="<%=(mon.equals("DOL"))?UtilFinanzas.customFormat2( (val.equals(""))?"0":val ):UtilFinanzas.customFormat((val.equals(""))?"0":val)%>" style="text-align:right;">              
					<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
					<input type="hidden" name="mostrar" value="<%=request.getParameter("mostrar")%>"></td><td>Moneda</td>
				<td><select name="moneda"  class="textbox" id="moneda" onChange="cambiar(this.value)" >
						<%if ( VecMon.size() > 0 ) {  
							for ( int i=0;  i < VecMon.size(); i++ ){ 
								moneda = (Moneda) VecMon.elementAt(i);%>
								<option value="<%=moneda.getCodMoneda()%>" <%=(moneda.getCodMoneda().equals(mon))?"selected":""%>><%=moneda.getNomMoneda()%></option>
							<%}
						}%>
					</select>
					<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
					<input type="hidden" name="modificar" value="<%=modificar%>">
					<input type="hidden" name="feccon" value="<%=feccon%>"></td>
				<td>Dolar A Bolivar </td>
				<td><input name="tasa" type="text" class="textbox" id="tasa" onFocus="this.select()" value="<%=(int)Double.parseDouble(tasa)==0?"":UtilFinanzas.customFormat("#,###.##########", Double.parseDouble( tasa ),10 )%>" size="15" maxlength="15"  <%=mon.equals("DOL")||mon.equals("BOL")?"":"readonly"%>> </td>
			</tr>
			
			<tr class="fila">
				<td>Descripci&oacute;n</td>
				<td colspan="5"><textarea name="descripcion" cols="80%" rows="3" class="textbox"><%=des%></textarea></td>
			</tr>
                        <tr  class="fila">
                            <td>Numero Extracto</td>
                            <td colspan="5"><input value="<%=nro_extracto%>" onkeypress="soloNumeros(this.id)" maxlength="10" name="nro_extracto" id="nro_extracto" /></td>
                        </tr>
        </table>
	    </td>
	</tr>
  </table>
   <br>
	<div align="center">
      <%if(!estado.equals("A")){
	        if( feccon.equals("0099-01-01 00:00:00") && modificar.equals("true") ){
				if(ag.equals("OP") ){%>
					<img src="<%=BASEURL%>/images/botones/modificar.gif"  name="imgaceptar" onClick="validarIngresoCliente('<%=BASEURL%>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
					<img src="<%=BASEURL%>/images/botones/anular.gif"  name="imganular" onClick="window.location='<%=CONTROLLER%>?estado=Ingreso&accion=Anular&tipodoc=<%=tipodoc%>&num=<%=num%>&fec_con=<%=feccon%>&transacion=<%=trans%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
				<%
				}
			}
			if( !feccon.equals("0099-01-01 00:00:00") && subingreso.equals("false") && ag.equals("OP") ){%>
				<img src="<%=BASEURL%>/images/botones/anular.gif"  name="imganular" onClick="window.location='<%=CONTROLLER%>?estado=Ingreso&accion=Anular&tipodoc=<%=tipodoc%>&num=<%=num%>&fec_con=<%=feccon%>&transacion=<%=trans%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
			<%}if(modificar.equals("true")){%>
  	            <%if(ag.equals("OP")){%>
					<!--<span class="Simulacion_Hiper" style="cursor:hand " onClick="enviarItems('<%=feccon%>', '1');">Anular Contabilizado</span>&nbsp;-->
	                <img src="<%=BASEURL%>/images/botones/detalles.gif"  name="imgdetalle" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick="enviarItems('<%=feccon%>', '1');">
            <%}
            }else{
				if(ag.equals("OP")){%>  
					<img src="<%=BASEURL%>/images/botones/detalles.gif"  name="imgdetalle" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick="enviarItems('<%=feccon%>', '1');">&nbsp;
          		<%}
		  	}
	  }%>
      <input name="opcion" type="hidden" id="opcion" value="">
      <img src="<%=BASEURL%>/images/botones/imprimir.gif"  name="imgimp"  onMouseOver="botonOver(this);" onClick="form1.opcion.value='imp';form1.valor.value = sinformato(form1.valor.value);form1.submit();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
      <img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgregresar"  onMouseOver="botonOver(this);" onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=BuscarIngreso.jsp&carpeta=/jsp/cxcobrar/ingreso&marco=no'" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
	  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
   	  </div>
   	<input name="modificar" value="<%=modificar%>" type="hidden">
	<input name="feccon" value="<%=feccon%>" type="hidden">
	<input name="usua" value="<%=usua%>" type="hidden">
	<input name="subingreso" value="<%=subingreso%>" type="hidden">
	<input name="mostrar" value="<%=request.getParameter("mostrar")%>" type="hidden">
	<%if(!res.equals("")){%>
		<br>
		<table border="2" align="center">
			<tr>
				<td>
					<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						<tr>
							<td width="300" align="center" class="mensajes">
								<%if(!res.equals("error")){%>
								Ingreso modificado exitosamente!
								<%}else{%>
								No existen Registro en la Tasas para la conversi&oacute;n
								<%}%> 
							</td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	<%}%>
	 <%if( request.getAttribute("mensaje") != null){%>
	 <br>
	  <table border="2" align="center">
	  <tr>
		<td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
		  <tr>
			<td width="300" align="center" class="mensajes">
			 <%=(String)request.getAttribute("mensaje")%>
			</td>
			<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
			<td width="58">&nbsp;</td>
		  </tr>
		</table></td>
	  </tr>
	</table>
	 <%}%>
   </form>
 </div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>

<script>
    function enviar(url){
         var a = "<iframe name='ejecutor' style='visibility:hidden'  src='<%= CONTROLLER %>" + url + "'> ";
         aa.innerHTML = a;
    }
	function colocarCuenta(){
		var vec = form1.sucursal.value.split("/");
		if( form1.sucursal.value != "" ){
			if( vec != null )
				form1.cuentas.value = vec[1];
		}
		else{
			form1.cuentas.value = "";
		}
	}
	
	function enviarItems( contabiliza, tipo ){
		if( contabiliza != "0099-01-01 00:00:00" && tipo == '1' ){
			if(  confirm(' El ingreso <%=num%> se encuentra contabilizado, si va a modificar\n'+
						 ' el ingreso se generará otro ingreso igual, pero el numero de ingreso diferente\n\n'+
						 ' ¿Desea modificar el ingreso?') ){
				window.open('<%=CONTROLLER%>?estado=Cargar&accion=Varios&numero_ingreso=<%=num%>&sw=14&tipo_doc=<%=tipodoc%>&pagina=buscar','I','status=yes,scrollbars=no,width=1024,height=680,resizable=yes');
			}
			else{
				return false;
			}
		}
		else
			window.open('<%=CONTROLLER%>?estado=Cargar&accion=Varios&numero_ingreso=<%=num%>&sw=14&tipo_doc=<%=tipodoc%>&pagina=buscar','I','status=yes,scrollbars=no,width=1024,height=680,resizable=yes');

	}	
		
	function tIngreso(){
		var tipo = document.getElementById('tipodoc').value;
		if(tipo=='ICR' || tipo=='ICA'){
			cuenta.style.display="block";
			banco.style.display="none";
			val.innerHTML = 'Valor';
			TDabc.innerHTML = 'ABC';
			Fecha_ABC.innerHTML="<input name='abc' type='text' class='textbox' id='abc' value='<%=abc%>' size='10' maxlength='10'> <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name='imagen' id='imagen' width='15' height='15' style='cursor:hand' title='Buscar ABC'  onClick='abrirABC()'; >";
		}else{
			cuenta.style.display="none";
			banco.style.display="block";
			val.innerHTML = 'Valor Consignación';
			TDabc.innerHTML = 'Fecha Consignación';
			
			Fecha_ABC.innerHTML="<input  type='text' name='fecha' id='fecha' size='12' class='textbox' value='<%=fec%>' readonly> <a id='calen' href='javascript:void(0)' onclick='if(self.gfPop)gfPop.fPopCalendar(document.form1.fecha);return false;' HIDEFOCUS> <img src='<%=BASEURL%>/js/Calendario/cal.gif' width='16' height='16' border='0' alt='De click aqui para escoger la fecha'></a>	<img src='<%=BASEURL%>/images/botones/iconos/obligatorio.gif' width='10' height='10'>";

		}
	}
	function abrirABC(){
		window.open('<%=BASEURL%>/jsp/cxcobrar/ingreso/ListaABC.jsp' ,'ABC','status=yes,scrollbars=no,width=600,height=500,resizable=yes');
	}
</script>
<font id='aa'></font>

