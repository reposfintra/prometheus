<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Descripcion de campos ingresar despacho</title>


<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<br>
<table width="600"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">INGRESO POR PAGOS POR CLIENTE</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2"><p>Regitrar Ingresos</p>
          </td>
        </tr>
        <tr>
          <td width="187"  class="fila">Cliente</td>
          <td width="391"  class="ayudaHtmlTexto">Campo para digitar el c&oacute;digo del cliente</td>
        </tr>
        <tr>
          <td  class="fila">Banco</td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n para escoger el banco del ingreso</td>
        </tr>
        <tr>
          <td  class="fila">Sucursal</td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n para escoger la sucursal del banco </td>
        </tr>
        <tr>
          <td  class="fila">Fecha Consignaci&oacute;n</td>
          <td  class="ayudaHtmlTexto">Campo para escoger las fecha de consignaci&oacute;n</td>
        </tr>
        <tr>
          <td  class="fila">Concepto</td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n para escoger el concepto del ingreso</td>
        </tr>
        <tr>
          <td  class="fila">Valor Consignaci&oacute;n</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el campo el valor de consignado</td>
        </tr>
        <tr>
          <td  class="fila">Moneda</td>
          <td  class="ayudaHtmlTexto">Campos de selecci&oacute;n para escoger la moneda del ingreso</td>
        </tr>
        <tr>
          <td  class="fila">Descripci&oacute;n</td>
          <td  class="ayudaHtmlTexto">Campo para digitar las descripci&oacute;n del ingreso</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
