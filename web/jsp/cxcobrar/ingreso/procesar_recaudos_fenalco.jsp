<!--
- Autor : Ing. Roberto Rocha	
- Date  : 10 de Sept de 2007
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la busqueda de las condicones de los negocios.
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
    <head>
        <title>&nbsp;&nbsp;Procesar Recaudos Fenalco</title>

        <link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/general.js"></script>
        <script src='<%=BASEURL%>/js/date-picker.js'></script>
    </head>
    <body onLoad="redimensionar();doFieldFocus( form2 );" onResize="redimensionar();">
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Procesar Recaudos Fenalco"/>
        </div>

        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <form name="form2" method="post" action="<%=CONTROLLER%>?estado=Procesar&accion=Recaudos">
                <table width="380" border="2" align="center">
                    <tr>
                        <td>
                            <table width="100%" align="center"  class="tablaInferior">
                                <tr>
                                    <td width="173" class="subtitulo1">&nbsp;&nbsp;Procesar Recaudos</td>
                                    <td width="205" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="filaazul">
                            <table width="100%" align="center"  class="tablaInferior">
                                <tr>
                                    <td width="178" class="barratitulo" align="center">
                                        <select name="opcion" id="opcion">
                                            <option value="">...</option>
                                            <option value="1">Procesar Ingresos</option>
                                            <option value="2">Procesar Notas de Ajuste</option>
                                            <option value="3">Procesar Notas Credito   </option>
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" class="filaazul">
                            <table width="100%" align="center"  class="tablaInferior">
                                <tr>
                                    <td width="178" class="barratitulo"><div align="center"><img src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand" title="Aceptar" name="buscar"  onClick="form2.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></div></td>
                                    <td width="178" class="barratitulo"><div align="center"><img src="<%=BASEURL%>/images/botones/salir.gif"  name="regresar" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <%  String Retorno = (String) request.getParameter("msg");
                            if (Retorno != null) {%>
                <table border="2" align="center">
                    <tr>
                        <td>
                            <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                <tr>
                                    <td width="229" align="center" class="mensajes"><%=Retorno%></td>
                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                    <td width="58">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <%
                            }
                %>
            </form>
        </div>
        <iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="yes" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
        </iframe>
    </body>
</html>
