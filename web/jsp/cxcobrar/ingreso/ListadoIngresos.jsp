<!--
- Autor : Ing. Jose de la rosa
- Date  : 3 de agosto del 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que lista los ingresos
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 20;
    int maxIndexPages = 10;
	LinkedList lista = model.ingresoService.getListadoingreso();
%>
<html>
    <head>
        <title>Listado Ingresos</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
		<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
		<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
        
    </head>
    <body >
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listado Ingresos"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <%
            if( lista != null ){%>
                <table width="98%" border="1" align="center">
                <tr>
                    <td>  
						<table width="100%" align="center">
							  <tr>
								<td width="373" class="subtitulo1">&nbsp;Listar Ingresos</td>
								<td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
							  </tr>
						</table>                      
                        <table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4">
                            <tr class="tblTitulo">                            
								<td width="5%" align="center">Num Ingreso </td>
								<td width="22%" align="center">Cliente</td>
								<td width="10%" align="center">Banco</td>
								<td width="10%" align="center">Sucursal</td>
								<td width="15%" align="center">Cuenta</td>
								<td width="8%" align="center">Fecha Consignacion</td>
								<td width="12%" align="center">Valor Consignacion</td>
								<td width="6%" align="center">Periodo</td>
								<td width="12%" align="center">Estado</td>					
                            </tr> 
							<pg:pager
								items="<%=lista.size()%>"
								index="<%= index %>"
								maxPageItems="<%= maxPageItems %>"
								maxIndexPages="<%= maxIndexPages %>"
								isOffset="<%= true %>"
								export="offset,currentPageNumber=pageNumber"
								scope="request">
							<%String pagina = "";
							  for (int i = offset.intValue(), l = Math.min(i + maxPageItems, lista.size()); i < l; i++)
							  {
								Ingreso ing = (Ingreso) lista.get(i);
								pagina = "";
								String vec[] = ing.getBank_account_no().split("/");
								String suc = "", cuen = "";
								if( vec != null && vec.length > 1 ){
									suc = vec[0];
									cuen = vec[1];
								}
								 if( ing.getConcepto() != null && ing.getConcepto().equals("T") && ing.getTipo_ingreso().equals("C") )
								 	pagina = CONTROLLER+"?estado=Cargar&accion=Varios&numero_ingreso="+ing.getNum_ingreso()+"&tipo_doc="+ing.getTipo_documento()+"&carpeta=jsp/cxcobrar/ingreso_detalle&pagina=ListarItemsIngresoDetalle.jsp&sw=12";
								 else if( ing.getConcepto() != null && ing.getConcepto().equals("P") && ing.getTipo_ingreso().equals("C") )
									pagina = CONTROLLER+"?estado=Cargar&accion=Varios&numero_ingreso="+ing.getNum_ingreso()+"&tipo_doc="+ing.getTipo_documento()+"&carpeta=jsp/cxcobrar/ingreso_detalle&pagina=ListarItemsIngresoDetalle.jsp&sw=12";
								 else if( ing.getConcepto() != null && ing.getConcepto().equals("N") && ing.getTipo_ingreso().equals("C") )
									pagina = "";
								 else
								 	pagina = CONTROLLER+"?estado=IngresoMiscelaneo&accion=Buscar&numero="+ing.getNum_ingreso()+"&evento=Cabecera&tipodoc="+ing.getTipo_documento()+"&pagina=consulta";
								 %>
								<pg:item>
									<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand" onClick="	<%if(!ing.getReg_status().equals("A")){
																																									if(!pagina.equals("")){%>
																																										window.open('<%=pagina%>','','status=no,scrollbars=no,width=950,height=600,resizable=yes');
																																									<%}
																																								}%>">
										<td align="center" class="bordereporte"><%=ing.getNum_ingreso()%></td>
										<td align="center" class="bordereporte"><%=ing.getNomCliente()%></td>
										<td align="center" class="bordereporte"><%=!ing.getBranch_code().equals("")?ing.getBranch_code():"&nbsp;"%></td>						
										<td align="center" class="bordereporte"><%=!suc.equals("")?suc:"&nbsp;"%></td>
										<td align="center" class="bordereporte"><%=!cuen.equals("")?cuen:"&nbsp;"%></td>
										<td align="center" class="bordereporte"><%=ing.getFecha_consignacion()%></td>	
										<td align="center" class="bordereporte"><%=(ing.getCodmoneda().equals("DOL"))?UtilFinanzas.customFormat2(ing.getVlr_ingreso_me()):UtilFinanzas.customFormat(ing.getVlr_ingreso_me())%> <%=ing.getCodmoneda()%></td>
										<td align="center" class="bordereporte"><%=ing.getPeriodo()%></td>	
										<td align="center" class="bordereporte"><%if(!ing.getReg_status().equals("A")){
																					if( ing.getConcepto() != null && ing.getConcepto().equals("T") ){%>Detallado Total
																					<%}else if( ing.getConcepto() != null && ing.getConcepto().equals("P") ){%>Detallado Parcial
																					<%}else{%>Pendiente por Detallar
																					<%}
																				}else{%>Anulado<%}%></td>						
									</tr>				
							  </pg:item>
						<%  }%>
							<tr class="pie">
								<td td height="20" colspan="10" nowrap align="center"><pg:index>
										<jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
								</pg:index>
								</td>
							</tr>
							</pg:pager>        
                            </table>
                    </td>
                </tr>
                </table>                
            <%}else{%>    
            <table border="2" align="center">
                <tr>
                    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr> 
                    <td width="350" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="58">&nbsp;</td>
                </tr>
                </table></td>
                </tr>
            </table>                   
            <%}%>
			<br>
			 <table width="98%" border="0" align="center">
				<tr>
				  <td>
				  <img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Volver" name="buscar"  onClick="window.location = '<%=CONTROLLER%>?estado=Menu&accion=Cargar&opcion=38&carpeta=/jsp/cxcobrar/ingreso&pagina=BuscarDatosIngreso.jsp&marco=no';" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
				  </td>
				</tr>
			</table>
        </div>
    </body>
</html>
