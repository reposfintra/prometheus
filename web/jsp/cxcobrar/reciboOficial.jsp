<%-- 
    Document   : reciboOficial
    Created on : 14/04/2014, 10:28:50 AM
    Author     : jpacosta
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Extractos</title>
        <link href="/fintra/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/default.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
        <link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>

        <script src="<%=BASEURL%>/js/prototype.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window.js" type="text/javascript"></script>

        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>        
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>            
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>

    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Extractos"/>
        </div>

        <div id="capaCentral" style="position:absolute; width:100%; height:83%; z-index:0; left: 0px; top: 100px; overflow:auto;">
            <div id="contenido" style="visibility: hidden; display: none; height: 100px; width: 400px; background-color: #EEEEEE;">
                <table id="tablaDet"></table>
                <div id="pageDet"></div>
            </div>
            <div align="center">
                <div id="filtros" style="display:table-cell; " class="ui-jqgrid ui-widget ui-widget-content ui-corner-all">
                    <table>
                        <tr>
                            <td><span> Unidad de negocio </span></td>
                            <td><select id="uNeg" name="uNeg">
                                    <option id="vacio">--todos--</option>
                                </select></td>
                            <td><span> Periodo </span></td>
                            <td><select id="periodo" name="periodo">
                                    <option id="vacio"></option>
                                </select></td>
                        </tr>
                        <tr><td><span> No. Extracto </span></td>
                            <td><input type="text" id="documento" name="documento"/></td>
                            <td><span> No. Cedula </span></td>
                            <td><input type="text" id="cedula" name="cedula"/></td>
                        </tr>
                        <tr>
                            <td><span> No. Negocio </span></td>
                            <td><input type="text" id="negocio" name="negocio"/></td>
                            <td><span> Valor Extracto </span></td>
                            <td><input type="text" id="valor" name="valor"/></td>
                        </tr>
                        <tr>
                            <td>
                            <button id="guardar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                    aria-disabled="false" role="button" onclick="cargarTabla(true);">
                                <span class="ui-button-text">Buscar</span>
                            </button>
                            <button class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                    aria-disabled="false" role="button"  onClick="previo_genArchivo();">
                                <span class="ui-button-text">Generar Archivo</span>
                            </button>  
                            <!--img height="21" align="absmiddle" style="cursor:hand"
                                  name="mod" tittle="Realizar Busqueda" src="<%=BASEURL%>/images/botones/buscar.gif">
                            </img-->
                            </td>
                        </tr>
                    </table>
                </div>
                </br>
                <table id="tabla"></table>
                <div id="page"></div>
            </div>
            <div id="ver_detalle_rop" title="Detalle Rop" class="ventana">                
                <table id="detallerop"></table>     
            </div>  
           
            <div id="act_if" title="Actualizar Fecha IF" class="ventana" style="display: none">  
                <br>
                <table border="0">
                    <tbody>
                        <tr>
                            <td> <label for="cod_neg">Negocio :</label></td>
                            <td><input type="text" id="cod_neg" name="cod_neg" value="" readonly="true" style="text-align: center" size="15"/><br></td>
                        </tr>
                        <tr>
                            <td><label for="fecha_actual">Fecha actual :</label></td>
                            <td><input type="text" id="fecha_actual" name="fecha_actual" readonly="true" value="" style="text-align: center" size="15"/><br></td>
                        </tr>
                        <tr>
                            <td><label for="valor_if">Valor if :</label></td>
                            <td><input type="text" id="valor_if" name="valor_if" value="" readonly="true" style="text-align: center" size="15"/><br></td>
                        </tr>
                        <tr>
                            <td><label for="fechaif">Nueva fecha  if : </label></td>
                            <td><input type="text" id="fechaif" name="fechaif" value="" size="15" readonly="true" style="text-align: center"/></td>
                        </tr>
                    </tbody>
                </table>

           </div>                       
                                
            <div id="gen_arch" title="Generacion de archivos" class="ventana" style="display: none">  
                <br>
                <table border="0">
                    <tbody>
                        <tr>
                            <td> <label for="arh_periodo">Periodo :</label></td>
                            <td><input type="text" id="arh_periodo" name="arh_periodo" value="" style="text-align: center" size="6" maxlength="6"/><br></td>
                        </tr>
                        <tr>
                            <td><label for="arh_ciclo">Ciclo :</label></td>
                            <td><input type="text" id="arh_ciclo" name="arh_ciclo" value="" style="text-align: center" size="6" maxlength="1"/><br></td>
                        </tr>
                    </tbody>
                </table>

           </div>      
        </div>
        <script type="text/javascript" src="<%=BASEURL%>/js/reciboOficial.js"></script>
    </body>
</html>