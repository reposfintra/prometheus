<%-- 
    Document   : tomarFoto
    Created on : 26/01/2015, 11:16:31 AM
    Author     : lcanchila
--%>

<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.operation.model.beans.CmbGeneralScBeans"%>
<%@page import="com.tsp.operation.model.DAOS.impl.PrevisualzarExtractosImpl"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.DAOS.PrevisualzarExtractosDAO"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE html>
<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    PrevisualzarExtractosDAO daoExt = new PrevisualzarExtractosImpl(usuario.getBd());
    ArrayList listaPeridos = daoExt.cargarPeriodos();
    
 %> 
 <html>
     <head>
         <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
         <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/jquery/jquery-ui/jquery-ui.css"/>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/buttons/botonVerde.css"/>
        
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
       
        <script type="text/javascript" src="<%=BASEURL%>/js/extractos.js"></script>

     </head>
     <style>
         .ventana{
             display:none;    
             font-family:Arial, Helvetica, sans-serif;
             color:#808080;
             font-size:12px;
             text-align:justify;

         }
     </style>
     <body>
         <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
             <jsp:include page="/toptsp.jsp?encabezado=Generaci�n de Foto Ciclo"/>
         </div>
         <div id="capaCentral" style="position:absolute; width:100%; height:83%; z-index:0; left: 0px; top: 100px; overflow:auto;">
             </br></br>
             <div align="center">

                 <div id="filtros" style="display:table-cell; " class="ui-jqgrid ui-widget ui-widget-content ui-corner-all">
                     </br>
                     <table align="center" style=" width: 380px ; margin-top: 2%;">
                         <tr>
                             <td style=" width: 80px; padding-left: 8px">
                                 <label style="font-size: 12px;font-weight: bold">Per�odo:</label> 
                             </td>
                             <td style=" width: 180px;"><select name="periodo" class="combo_180px" id="periodo" >
                                     <option value=""></option>
                                     <%
                                         for (int i = 0; i < listaPeridos.size(); i++) {
                                             CmbGeneralScBeans CmbPeriodoFoto = (CmbGeneralScBeans) listaPeridos.get(i);%>
                                     <option value="<%=CmbPeriodoFoto.getIdCmb()%>" ><%=CmbPeriodoFoto.getDescripcionCmb()%></option><%
                                         }%>
                                 </select>
                             </td>
                             <td style=" width: 100px;">
                                 <label style="font-size: 12px;font-weight: bold">N�mero Ciclo:</label> 
                             </td>
                             <td style=" width: 100px !important;"><select name="ciclo" class="combo_180px" id="ciclo" >
                                     <option value=""></option>
                                     <option value="1">1</option>
                                     <option value="2">2</option>
                                     <option value="3">3</option>
                                     <option value="4">4</option>
                                 </select>
                             </td>
                         </tr>
                     </table>                

                    <hr>
                <table id="tb_foto"  style=" width: 470px; height: 50px; " >
                    <tr>
                        <td width="80px">
                            <button id="btCuotaManejo" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="GenerarCuotaManejo();">
                                <span class="ui-button-text">Generar Cuota Admin</span> 
                            </button>
                        </td>
                        <td   width="60px">
                            <button id="btMI" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="GenerarCATyMImes()">
                                <span class="ui-button-text">Generar MI/Cat</span> 
                            </button>
                        </td>
                        <td  width="60px">
                            <button id="btFoto" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="TomarFoto();">
                                <span class="ui-button-text">Tomar Foto Ciclo</span>
                            </button>
                        </td>
                        <td  width="60px">
                            <button id="btFotoGeneral" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="TomarFotoMes();">
                                <span class="ui-button-text">Tomar Foto Mes</span> 
                            </button>
                        </td>
                    </tr>                      
                </table>
                </div>
                <div id="dialogo" class="ventana" title="Mensaje">
                    <p  id="msj" > </p>
                </div>
                <div id="dialogLoading"  class="ventana" >
                    <p  id="msj2">texto </p> <br/>
                    <center>
                        <img src="<%=BASEURL%>/images/cargandoCM.gif"/>
                    </center>
                </div>
                </div>                 
             </div>
         </div>
     </body>
 </html>
