<%-- 
    Document   : generarSanciones
    Created on : 18/02/2015, 05:03:30 PM
    Author     : lcanchila
--%>

<%@page import="com.tsp.operation.model.DAOS.impl.PrevisualzarExtractosImpl"%>
<%@page import="com.tsp.operation.model.DAOS.PrevisualzarExtractosDAO"%>
<%@page import="com.tsp.operation.model.beans.CmbGeneralScBeans"%>
<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE html>
<%    PrevisualzarExtractosDAO daoExt = new PrevisualzarExtractosImpl();
    ArrayList listaPeridos = daoExt.cargarPeriodos();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Generar Sanciones y Condonaciones</title>
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/jquery/jquery-ui/jquery-ui.css"/>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/popup.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/style_azul.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/extractos.js"></script>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Generar Sanciones y Condonaciones"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <br>  </br>  
            <div align="center">
                <table align="center" width="700px" border="0" class="labels" id="tbl_hys">
                    <tr>
                        <td style=" width: 250px; height: 40px" align="center" >
                            <fieldset >
                                <legend>Tipo Acto:</legend>   
                                <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                    <tr>
                                        <td align="left" >
                                            <div id="busquedad" >
                                                <input type="radio" name="sanc" id="sanc" value="1" checked /> <label style=" font-size: 12px">Sanciones</label>
                                                &nbsp;&nbsp;&nbsp;&nbsp;
                                                <input type="radio" name="sanc" id="cond" value="2" /> <label style=" font-size: 12px">Condonaciones</label>
                                            </div>
                                        </td>

                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                        <td style=" width: 250px; height: 100px" rowspan="2" >
                            <fieldset >
                                <legend>Unidades de Negocio:</legend>   
                                <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                    <tr>
                                        <td align="left"  width="250px" >
                                            <div id="unidades" >
                                                <input type="checkbox" name="autofa" id="autofa" value="3" /> <label style=" font-size: 12px">Automotor FA</label> &nbsp;&nbsp;
                                                <input type="checkbox" name="autofb" id="autofb" value="9" /> <label style=" font-size: 12px">Automotor FB</label> 
                                                <input type="checkbox" name="consufa" id="consufa" value="4" /> <label style=" font-size: 12px">Consumo FA</label> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <input type="checkbox" name="consufb" id="consufb" value="10" /> <label style=" font-size: 12px">Consumo FB</label> &nbsp;&nbsp;
                                                <input type="checkbox" name="edufa" id="edufa" value="2" /> <label style=" font-size: 12px">Educativo FA</label> &nbsp;&nbsp;&nbsp;&nbsp;
                                                <input type="checkbox" name="edufb" id="edufb" value="8" /> <label style=" font-size: 12px">Educativo FB</label> &nbsp;
                                                <input type="checkbox" name="micro" id="micro" value="1" /> <label style=" font-size: 12px">Microcredito</label>
                                            </div>
                                        </td>

                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                        <td  width="60px"><button id="btFoto" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="CopiarSancionesMes();">
                                     <span class="ui-button-text">Copiar Sanciones</span> </button></td> 
                        
                    </tr>    
                    <tr>
                        <td  style=" width: 80px; height: 40px"  align="center">
                            <fieldset >
                                <legend>Periodo:</legend>   
                                <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                    <tr>
                                        <td><select  style=" width: 90px; " name="periodo" class="combo_180px" id="periodo" ><%
                                for (int i = 0; i < listaPeridos.size(); i++) {
                                    CmbGeneralScBeans CmbPeriodoFoto = (CmbGeneralScBeans) listaPeridos.get(i);%>
                                                <option value="<%=CmbPeriodoFoto.getIdCmb()%>" selected><%=CmbPeriodoFoto.getDescripcionCmb()%></option><%
                                            }%>
                                            </select>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>
                          <td style=" width: 200px; "><button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="buscarDetalleSanciones();">
                                <span class="ui-button-text">Buscar</span>
                            </button></td>
                    </tr>        
                </table>
                </br> </br></br>
                <div id="tb_detalle" style=" display: none" > <table id="DetalleSanciones"></table> </div>
                 <div id="tb_condonacion" style=" display: none" > <table id="DetalleCondonaciones"></table> </div>
                
                </br>
                <table align="center">
                    <tr>
                        <td><button id="bt_buscar" style=" display: none" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="generarSancionesMes();">
                                <span class="ui-button-text">Generar</span>
                            </button></td>
                    </tr>
                </table>
                <div id="dialogo" class="ventana" title="Mensaje">
                    <p  id="msj" > </p>
                </div>
            </div> 
    </body>
</html>
