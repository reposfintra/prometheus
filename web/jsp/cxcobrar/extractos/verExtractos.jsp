<%-- 
    Document   : verExtractos
    Created on : 19/11/2014, 10:46:40 AM
    Author     : lcanchila
--%>

<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.operation.model.DAOS.impl.PrevisualzarExtractosImpl"%>
<%@page import="com.tsp.operation.model.DAOS.PrevisualzarExtractosDAO"%>
<%@page import="com.tsp.operation.model.beans.CmbGeneralScBeans"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.services.SeguimientoCarteraService"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE html>
<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    SeguimientoCarteraService rqservice= new SeguimientoCarteraService(usuario.getBd());
    PrevisualzarExtractosDAO daoExt = new PrevisualzarExtractosImpl(usuario.getBd());
    ArrayList listaTramos =  rqservice.GetComboGenericoStr("SQL_OBTENER_TRAMOS","id","descripcion","");
    ArrayList listaPeridos = daoExt.cargarPeriodos();
    ArrayList listaCombo =  rqservice.GetComboGenerico("SQL_OBTENER_UNIDAD_NEGOCIO_BASE","id","descripcion","");
    int DiasMes = 0;
 %>   
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Generacion Extractos</title>
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/jquery/jquery-ui/jquery-ui.css"/>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/popup.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/jscal2.css" />
        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/border-radius.css" />
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/buttons/botonVerde.css"/>
        
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
       
        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>
        
        <script type="text/javascript" src="<%=BASEURL%>/js/extractos.js"></script>
        
        
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Generaci�n Extractos"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:83%; z-index:0; left: 0px; top: 100px; overflow:auto;">
            </br></br>
            <div align="center">
                
            <div id="filtros" style="display:table-cell; " class="ui-jqgrid ui-widget ui-widget-content ui-corner-all">
                    </br>
                    <table style=" width: 880px">
                        <tr>
                             <td style=" width: 100px; padding-left: 8px"><span> Periodo:</span></td>
                            <td style=" width: 180px;"><select name="periodo" class="combo_180px" id="periodo" >
                                       <option value=""></option>
                                        <%
                                        for (int i = 0; i < listaPeridos.size(); i++) {
                                            CmbGeneralScBeans CmbPeriodoFoto = (CmbGeneralScBeans) listaPeridos.get(i);%>
                                         <option value="<%=CmbPeriodoFoto.getIdCmb()%>" ><%=CmbPeriodoFoto.getDescripcionCmb()%></option><%
                                                                    }%>
                                </select>
                            </td>
                            <td style=" width: 100px;">
                                <label>N�mero Ciclo:</label> 
                            </td>
                            <td style=" width: 180px;"><select name="ciclo" class="combo_180px" id="ciclo" >
                                       <option value=""></option>
                                       <option value="1">1</option>
                                       <option value="2">2</option>
                                       <option value="3">3</option>
                                       <option value="4">4</option>
                                </select>
                            </td>
                            <td style=" width: 80px;">
                                <label>Fecha Corte:</label> 
                            </td>
                            <td style=" width: 100px;">
                                <input name="fechaHoy" type='text' class="textbox" id="fechaHoy" style='width:80px'  readonly value="">
                                <img src="<%=BASEURL%>/images/cal.gif" id="imgFecha" />
                                <script type="text/javascript">
                                    Calendar.setup({
                                        inputField: "fechaHoy",
                                        trigger: "imgFecha",
                                        align: "top",
                                        onSelect: function() {
                                            this.hide();
                                        }
                                    });
                                </script>                        
                            </td>
                        </tr>
                        <tr>
                            <td style=" width: 100px; padding-left: 8px"><span> Unidad de negocio: </span></td>
                            <td><select id="uNeg" name="uNeg" multiple="select-multiple" style=" width: 150px; height: 70px">
                                    <%
                                    for (int i = 0; i < listaCombo.size(); i++) {
                                        CmbGeneralScBeans CmbContenido = (CmbGeneralScBeans) listaCombo.get(i);%>
                                    <option value="<%=CmbContenido.getIdCmb()%>"><%=CmbContenido.getDescripcionCmb()%></option><%
                                            }%>
                                </select></td>
                            <td style=" width: 100px; ">
                                <label>Vencimiento Mayor:</label> 
                            </td>
                            <td>
                                <select name="vencMayor" class="combo_180px" id="vencMayor" multiple="select-multiple" style=" width: 150px; height: 70px">
                                    <%
                                        for (int i = 0; i < listaTramos.size(); i++) {
                                            CmbGeneralScBeans CmbTramo = (CmbGeneralScBeans) listaTramos.get(i);%>
                                    <option value="<%=CmbTramo.getIdCmbStr()%>"><%=CmbTramo.getDescripcionCmb()%></option><%
                                            DiasMes = CmbTramo.getDiasMesCmb();
                                        }%>
                                </select>
                            </td>
                         </tr>
                    </table>
                     </br>
                     <table style=" width: 780px">        
                         <tr>
                                <td  style=" padding-bottom: 8px;  padding-left: 10px" width="10px"><button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="cargarPrevisualizacionExtracto();">
                                                    <span class="ui-button-text">Buscar</span>
                                                </button></td>
                                <td  style=" padding-bottom: 8px" width="10px"><button id="exportar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="exportarExtracto();">
                                                    <span class="ui-button-text">Exportar</span>
                                                </button></td>                
                                <td  style=" padding-bottom: 8px" width="90px"><button id="incluir" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="confirmarIncluirExtracto();">
                                                    <span class="ui-button-text">Incluir a Extracto</span>
                                                </button></td>
                                <td  style=" padding-bottom: 8px" width="90px"><button id="limpiar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="confirmarlimpiarExtracto();">
                                                    <span class="ui-button-text">Limpiar Extracto</span>
                                                </button></td>                
                                <td  style=" padding-bottom: 8px" width="50px"><button id="generar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="confirmarGeneracionExtracto();">
                                                    <span class="ui-button-text">Generar</span>
                                                </button></td>
                                <td  style=" padding-bottom: 8px; padding-right: 10px" width="70px"><button id="exportar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="generarArchivoExtracto();">
                                                    <span class="ui-button-text">Generar Archivo</span>
                                                </button></td>
                                                
                            </tr>
                    </table>
                </div>     
                    </br> </br></br>
                    <div id="tb_fenalco" style=" display: none"> <table id="PreExtractoFenalco"></table> </div>
                    <div id="pageFen"></div>
                    <div id="tb_micro" style=" display: none"> <table id="PreExtractoMicro"></table> </div>
                    <div id="pageMc"></div>
                <div id="dialogo2" class="ventana" title="Mensaje">
                    <p  id="msj2"> </p>
                    <center><img id="imgload2" style="position:relative;  top: 2px; display: none " src="<%=BASEURL%>/images/cargandoCM.gif"/></center>
                </div>
                <div id="dialogo4" class="ventana" title="Mensaje">
                    <p  id="msj4" > </p>
                </div>
                <div id="dialogo" class="ventana" title="Mensaje">
                    <p  id="msj" > </p>
                </div>
                <div id="divSalida" title="Exportacion" style=" display: block" >
                <p  id="msj3" style=" display:  none"> Espere un momento por favor...</p>
                    <center><img id="imgload1" style="position: relative;  top: 7px; display: none " src="<%=BASEURL%>/images/cargandoCM.gif"/></center>
                    <div id="resp1" style=" display: none"></div>
                </div>
                <div id="divSalidaEx" title="Exportacion" style=" display: block" >
                <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
                    <center><img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="<%=BASEURL%>/images/cargandoCM.gif"/></center>
                    <div id="respEx" style=" display: none"></div>
                </div> 
                    
                 
            </div>            
        </div>
    </body>
</html>
