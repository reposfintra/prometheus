<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE></TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="file:///C|/Tomcat5/webapps/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<style type="text/css">
<!--
.Estilo2 {
	font-size: 16pt;
	color: #FF0000;
}
-->
</style>
</HEAD>
<BODY> 

        <table width="90%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20" align="center">MANUAL DE REGISTRO DE ITEM  MISCELANEOS </td>
          </tr>
          <tr class="subtitulo1">
            <td> Registro de Item Miscelaneos </td>
          </tr>
          <tr>
            <td  height="8"  class="ayudaHtmlTexto"><p>Para registrar los item de un ingreso miscel&aacute;neo, el sistema le presenta la siguiente pantalla.</p>            </td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/ingreso/itemsMis/imagen1.jpg" width="983" height="360"></div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><p align="left">              Si el item que desea registrar tiene numero de auxiliar, debe digitar la cuenta contable, luego el sistema le carga los tipo de que esta posee, permiti&eacute;ndole buscar el numero del auxiliar por el nombre si no lo conoce.</p>
            </td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/ingreso/itemsMis/imagen2.jpg" width="995" height="355">
              </div></td>
          </tr>
<tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><p align="left">Presione aceptar para realizar el ingreso miscel&aacute;neo, el sistema le muestra la siguiente pantalla.</p>
    </td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/ingreso/itemsMis/imagen3.jpg" width="585" height="127">
              </div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="left">El sistema le muestra el resultado de la busqueda</div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/ingreso/itemsMis/imagen4.jpg" width="498" height="160"></div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="left">Una vez seleccionado, autom&aacute;ticamente la ventana se cierra y el auxiliar se agrega a la ventana.</div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><img src="<%=BASEURL%>/images/ayuda/ingreso/itemsMis/imagen5.jpg" width="982" height="345"></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="left">Si presiona la lupa de RFTE el sistema le muestra, la ventana con los c&oacute;digos.</div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><img src="<%=BASEURL%>/images/ayuda/ingreso/itemsMis/listadoRetefuente.JPG" width="452" height="162"></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="left">Si presiona en la del RICA el sistema le muestra, la ventana con los c&oacute;digos.</div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><img src="<%=BASEURL%>/images/ayuda/ingreso/itemsMis/listadoRica.JPG" width="452" height="162"></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="left">Luego de haber seleccionados los impuestos y digitado la informaci&oacute;n detallada presione aceptar para almacenarlo en la base de datos.</div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><img src="<%=BASEURL%>/images/ayuda/ingreso/itemsMis/mensaje.gif" width="209" height="126"></td>
          </tr>
         
      </table>
</BODY>
</HTML>
