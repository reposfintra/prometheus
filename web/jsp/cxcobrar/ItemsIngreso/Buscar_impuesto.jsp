<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de realizar y mostrar busqueda de los tipos de impuestos
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head><title>Tipo de Impuestos</title>
<link href="../../cxpagar/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/ingreso.js"></script> 
 
<script>
function asignarImpuesto( id, codigo, porcentaje, tipo, valor ){
   //campos de informacion
    var mon = parent.opener.document.getElementById("mon_ingreso");
	var vlrneto = parent.opener.document.getElementById("vlrneto"+id);
    var valor = parent.opener.document.getElementById("valor"+id);
	if(tipo=='RICA'){
		//asigno codigo a la reteica
        var valor_rfte = parent.opener.document.getElementById("c_valor_rfte"+id);
		var cod_rica = parent.opener.document.getElementById("c_cod_rica"+id);
		cod_rica.value = codigo;
		//asigno valor a la reteica

		var valor_rica  = parent.opener.document.getElementById("c_valor_rica"+id);
		if(mon.value =='DOL'){
	        valor_rica.value = Fdolar( sinformato(valor.value) * (porcentaje/100), 2 );
		}else{
	        valor_rica.value = formato( sinformato(valor.value) * (porcentaje/100) );
		}
        var val_rica  = parent.opener.document.getElementById("val_rica"+id);
        val_rica.value = porcentaje;
	}
	else if(tipo=='RFTE'){
		var valor_rica    = parent.opener.document.getElementById("c_valor_rica"+id);
        var cod_rfte    = parent.opener.document.getElementById("c_cod_rfte"+id);
		cod_rfte.value = codigo;
		//asigno valor a la retefuente
		var valor_rfte    = parent.opener.document.getElementById("c_valor_rfte"+id);
		if(mon.value =='DOL'){
			valor_rfte.value = Fdolar( sinformato(valor.value) * (porcentaje/100), 2 );		
		}else{
			valor_rfte.value = formato( sinformato(valor.value) * (porcentaje/100) );		
		}
	 	var val_rfte    = parent.opener.document.getElementById("val_rfte"+id);
        val_rfte.value = porcentaje;

	}
      //validación si existen retefuente y reteica
		if( valor_rfte.value != '' && valor_rfte.value.length>1 )
			var vlr_rfte = sinformato(valor_rfte.value);
		else 
			var vlr_rfte = 0;
		if( valor_rica.value != '' && valor_rica.value.length>1 )
			var vlr_rica = sinformato(valor_rica.value);
		else 
			var vlr_rica = 0;
	   if(mon.value =='DOL'){
		  vlrneto.value =  Fdolar( sinformato(valor.value) -  ( parseFloat(vlr_rfte) + parseFloat(vlr_rica) ),2  );	
	   }
	   else{
		 vlrneto.value =  formato( sinformato(valor.value) -  ( parseFloat(vlr_rfte) + parseFloat(vlr_rica) ) );	
	   }

	var total = 0;
	var valor_total = parent.opener.document.getElementById("total");
	var items = parent.opener.document.getElementById("items");

	for (j=1; j<=items.value; j++){
		var vlrneto = parent.opener.document.getElementById("vlrneto"+j);
			if( vlrneto != null ){	
				if( vlrneto.value != '' && vlrneto.value.length>1 )
					var vlr_total = sinformato(vlrneto.value);
				else
					var vlr_total = 0;
				total = parseFloat(total) + parseFloat(vlr_total);
			}
	}
	if(mon.value =='DOL'){
		opener.document.getElementById("vlracum").innerText = Fdolar(total,2);
	}
	else{
		opener.document.getElementById("vlracum").innerText = formato(total);
	}

  	parent.close();
}
</script>
</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Impuestos "/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
    String t_i = ""+request.getParameter("tipo");
    String idI = ""+request.getParameter("id");
	String valor = ""+request.getParameter("valor");
	//String tam = ""+request.getParameter("tam");
	
	//fecha actual
    java.util.Date utilDate = new java.util.Date(); 
    long lnMilisegundos = utilDate.getTime();
    java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(lnMilisegundos);
    Usuario usuario = (Usuario) session.getAttribute("Usuario"); 
	//vector de tipos de impuestos
	Vector vImpuestos= model.TimpuestoSvc.buscarImpuestoPorTipo(t_i,""+sqlTimestamp,usuario.getDstrct(),"");
%>
<form name="forma1" action="" method="post">      
<table width="312" border="2" align="center">
  <tr>
    <td width="300">
<table width="100%" align="center" > 
  <tr>
    <td width="378" height="24"  class="subtitulo1"><p align="left">Impuestos Tipo <%=t_i%></p></td>
    <td width="225"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
  </tr>
</table>
<table width="100%"  align="center" >
 <tr class="tblTitulo" id="titulos" >
    <td width="136" align="center">Codigo </td>
    <td width="146" align="center">Impuesto</td>
 </tr>
<% 
    String fecha =""+sqlTimestamp; 
    for(int i=0; i< vImpuestos.size();i++){
        Tipo_impuesto tipo=(Tipo_impuesto)vImpuestos.elementAt(i);
%>  
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand" title="Tipo de impuestos" onClick="asignarImpuesto('<%=idI%>','<%=tipo.getCodigo_impuesto()%>', '<%=tipo.getPorcentaje1()%>','<%=t_i%>','<%=valor%>');">

        <td align="center"><%=tipo.getCodigo_impuesto()%></td>
        <td align="center"><%=tipo.getPorcentaje1()+ " % "%></td>
        </tr>
<%
    }
%>  
</table>
</td>
</tr>
</table>
</form>
<center><img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir..." onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></center>
</div>
</body>
</html>
