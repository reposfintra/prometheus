
<!--
- Autor : Ing. Diogenes Bastidas
- Date  : 10 de Mayo del 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->   
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja los items de los ingresos
--%>

<%-- Declaracion de librerias--%>
<%@page session="true"%> 
<%@page import="java.util.*" %>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<% String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
   String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path); 
   String mensaje = request.getParameter("mensaje"); 
   String fecha = "";%>
<html>
<head>
<title>Ingreso Detalles</title>
<link href="<%= BASEURL %>/css/estilostspFactura.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostspFactura.css" rel="stylesheet" type="text/css">
 <script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
 <script type='text/javascript' src="<%=BASEURL%>/js/ingreso.js"></script> 
 <script type='text/javascript' src="<%=BASEURL%>/js/general.js"></script> 
</head> 
<%
	Vector vecitems = model.ingreso_detalleService.getItemsMiscelaneo();
	Ingreso ing  = model.ingresoService.getIngreso();%>
<body onLoad="redimensionar();
		<%if(mensaje!=null){%>
			alert('<%=mensaje%>');
		<%}if(vecitems.size()>0){%>
			SumarValores();
		<%} if(request.getParameter("cerrar")!=null){
				if(ing.getPagina().equals("modificar")){%>
					parent.opener.location='<%=CONTROLLER%>?estado=IngresoMiscelaneo&accion=Buscar&evento=1&numero=<%=ing.getNum_ingreso()%>&dstrct=<%=ing.getDstrct()%>&tipodoc=<%=ing.getTipo_documento()%>&mostrar=ok&pagina=<%=ing.getPagina()%>';
				<%}else if( ing.getPagina().equals("consulta") ){%>
					parent.opener.location.reload();
				<%}
		}%>" 
		onResize="redimensionar();">
		
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Detalles de Ingreso Miscelaneo"/>
</div> 

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%String valor = ( request.getParameter("numero") != null  )? request.getParameter("numero") : "1";
String sw = ( request.getParameter("sw") != null  )? request.getParameter("sw") : "";
String maxfila = ( valor.equals("0")   )? "1" : valor;
String dec = (ing.getCodmoneda().equals("DOL"))?"OK":"NO";
%>
<script>
var maxfila = '<%=maxfila%>';
	function nuevaFila(BASEURL, i){
		maxfila++;  
		var numfila = detalle.rows.length ;
		var fila = detalle.insertRow(numfila);
			
		if(maxfila%2==0)
			fila.className="filagrisFac";
		else
			fila.className="filaazul";

		fila.id = "filaItem"+maxfila;  
		var celda = fila.insertCell();
        var tds ="<a id='n_i"+maxfila+"' >"+maxfila+"</a>";
	        tds+="    <a  id='insert_item"+maxfila+" style='cursor:hand'  ><img src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width='12' height='12' onClick=\"insertarItem('<%=BASEURL%>','"+maxfila+"')\"></a> ";
			tds+="    <a onClick=\"borrarItem('filaItem"+maxfila+"')\" id='borrarI"+maxfila+"' style='cursor:hand' ><img src='<%=BASEURL%>/images/botones/iconos/menos1.gif'   width='12' height='12'></a>  ";
			tds+="<input name='nroitem"+maxfila+"' type='hidden' id='nroitem"+maxfila+"' value='"+maxfila+"'> <input name='fec_cont"+maxfila+"' type='hidden' id='fec_cont"+maxfila+"' value='0099-01-01 00:00:00'>";
			celda.innerHTML = tds;
			celda = fila.insertCell();
			tds = "";
		
			tds +="<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
			tds +="<tr align='center' class='bordereporte'><td><table width='100%'><tr>";	        
			tds +="<td width='12%' nowrap><input name='cuenta"+maxfila+"' type='text' id='cuenta"+maxfila+"' size='20' style='font-size:11px' maxlength='25' onBlur='VerificarCuenta("+maxfila+");'></td>";
			tds +="<td width='6%' id='combotipo"+maxfila+"'><select name='tipo"+maxfila+"'><option value=''>  </option></select></td>";	
			tds +="<td width='16%' nowrap><input name='auxiliar"+maxfila+"' type='text' id='auxiliar"+maxfila+"' size='20' style='font-size:11px' maxlength='25' value ='' onBlur=\"validarAuxiliar('<%=BASEURL%>',"+maxfila+");\"  >";
			tds += " <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name='imagenAux"+maxfila+"' width='15' height='15'  id='imagenAux"+maxfila+"' style='cursor:hand'  onClick=\"buscarAuxiliar('<%=BASEURL%>','"+maxfila+"',form1.cuenta"+maxfila+".value, form1.tipo"+maxfila+".value);\"  ></td>";
			tds += "<td width='33%'><input name='c_descripcion"+maxfila+"' type='text'  id='c_descripcion"+maxfila+"'  style='width:99%'></td>";
			tds +="<td width='11%'><select name='tipodoc"+maxfila+"' onChange='VerificarTipoDocumento("+maxfila+");'><option value=''></option><option value='001'>Planilla</option><option value='002'>Remesa</option></select></td>";
			tds +="<td width='11%'><input name='doc"+maxfila+"' type='text' class='textbox' id='c_valor_factura"+maxfila+"' style='text-align:right' size='15' maxlength='15' onBlur='VerificarDocumento("+maxfila+");'></td>";
			tds +="<td width='11%'><input name='valor"+maxfila+"' type='text' class='textbox' id='valor"+maxfila+"' style='text-align:right' onKeyPress=\"Digitos(event,'decOK')\" onfocus='this.value=sinformato(this.value);' size='15' maxlength='15' onBlur=\"AcualizarVlrImpuesto('"+maxfila+"');\" onChange=\" if('<%=ing.getCodmoneda()%>'=='DOL'){formatoDolar(this,2);}else{this.value = Math.round( parseFloat( sinformato( this.value ) ) );formatear(this); } \"  ></td>";
			tds +="</tr></table></td></tr></table>";
			celda.innerHTML = tds;
            form1.items.value = maxfila;
			organizar();
    }

	function insertarItem(BASEURL, i){
			var cuenta = document.getElementById("cuenta"+i);
			var tipo = document.getElementById("tipo"+i);
			var aux = document.getElementById("auxiliar"+i);
			var valor = document.getElementById("valor"+i);
			var tipo_doc = document.getElementById("tipodoc"+i);
	        var doc = document.getElementById("doc"+i);
			var acum = parseFloat(document.getElementById("acumdetalle").value);
		  	var val_ing = parseFloat(document.getElementById("val_ing").value);
           
			var sw = false;
            if(cuenta.value != ''){
				if(tipo.value =='' && aux.value == ''){
            		sw=true;
				}
			    else{
					if(tipo.value !='' && aux.value != ''){
						sw=true;
					}else if (tipo.value ==''){
						alert("El tipo esta vacio");
						tipo.focus();
						sw = false;
					}else if (aux.value ==''){
						alert("El auxiliar esta vacio");
						aux.focus();						
						sw = false;
					}
				}
           	}
			else{
				alert("El nro de la cuenta esta vacio");
				 cuenta.focus();
            }
		  	//Valido los tipos de Documentos

			if (tipo_doc.value != ''){
	            if(doc.value == ''){
					alert("El campo documento esta vacio");
					sw = false;
				}
			}
			//valido que el valor no puede estar vacio
			if(sw){
				if(valor.value != ''){
					if( acum >= val_ing ){
						alert("El valor del acumulado es Mayor o igual que el valor del ingreso");
					}else{
						nuevaFila(BASEURL, i);
					}
				}else{
					alert("El valor esta vacio");
				 	valor.focus();
				}
			}
	}
	

	function borrarItem(indice){
            var tabla = document.getElementById("detalle");
			if ( tabla.rows.length <= 2 ){
				return;
			}
			var fila = document.getElementById(indice);
			//alert(fila.rowIndex);
		    tabla.deleteRow(fila.rowIndex);				
			organizar();
			SumarValores();
	}

	function organizar(){
 	    var cont =1;
		for (var i = 1,n = 1;i <= maxfila + 1 ; i++){
			var x = document.getElementById("n_i"+i);
			var y = document.getElementById("filaItem"+i);
           

			if ( x != null ){
				x.innerText = ""+n;
				n++;
			}
			if( y != null ){
				if( cont % 2 == 0){
				   y.className = "filagrisFac";
				}else{
				   y.className = "filaazul";
				}
				cont++;
			}
		}
	}
</script>
<%
	String distrito = (String) session.getAttribute ("Distrito");	
	String moneda_local = (String) session.getAttribute("Moneda");
	String descripcion = "", fecha_factura = "", facturas = "", moneda_factura = "";
	int x=1;
	String cod_rfte = "", cod_rica = "";
	
	
    String mon = ing.getCodmoneda();
	String accion = (!sw.equals("")) ? "Modificar" :  "Registrar";	


   %>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=IngresoMiscelaneoDetalle&accion=<%=accion%>"  >   
  <br>
<table width="940" border="2" align="center">
    <tr>
      <td width="100%" height="60">
        <table width="100%">
          <tr>
            <td width="50%" height="22" align="left" class="subtitulo1">&nbsp;<%=ing.getDestipo()%> Nro: <%=ing.getNum_ingreso()%>
              <input name="numingreso" type="hidden" id="numingreso" value="<%=ing.getNum_ingreso()%>">
              <input type="hidden" name="tipodoc" value="<%=ing.getTipo_documento()%>"></td>
            <td width="50%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
          </tr>
        </table>
        <table width="100%">
          <tr height="20">
            <td width="13%" class="filaFactura">Identificacion</td>
            <td width="22%" class="letra"><%=ing.getNitcli()%></td>
            <td class="filaFactura">Nro Consignacion </td>
            <td width="14%" class="letra"><%=ing.getNro_consignacion()%></td>
            <td width="19%" class="filaFactura">Valor Consignaci&oacute;n</td>
            <td width="17%" class="letra"><%=(ing.getCodmoneda().equals("DOL"))?UtilFinanzas.customFormat2( ing.getVlr_ingreso_me() ):UtilFinanzas.customFormat(ing.getVlr_ingreso_me())%> <%=ing.getCodmoneda()%>
              <input name="mon_ingreso" type="hidden" id="mon_ingreso" value="<%=ing.getCodmoneda()%>">
              <input name="val_ing" type="hidden" id="val_ing" value="<%=ing.getVlr_ingreso_me()%>"></td>
          </tr>
		  <tr height="20">
            <td class="filaFactura">Nombre</td>
            <td class="letra"><%=ing.getNomCliente()%></td>
            <td width="15%" class="filaFactura">Fecha Consignación</td>
            <td class="letra"><%=ing.getFecha_consignacion()%></td>
            <td class="filaFactura">Acumulado Detalles <input name="acumdetalle" type="hidden" id="acumdetalle"></td>
            <td class="letra" id="vlracum"></td>
		  </tr>
      </table>
      </td>
    </tr>
  </table>
<table width="940" border="2" align="center">
      <tr>
        <td >
          <table width="100%" align="center"  >
            <tr>
              <td align="left" class="subtitulo1">&nbsp;Items del Ingreso</td>
              <td  width="50%"align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20">
              <input type="hidden" name="items" id="items" value="<%=maxfila%>"></td>
            </tr>
          </table>

            <table id="detalle" width="100%" >
              <tr  id="fila1" class="tblTituloFactura">
                <td align="center">Item</td>
                <td width="92%" align="center">
					<table align="center" class="tblTituloFactura" width="100%">
					  <tr class="tblTituloFactura">
						<td width="15%">Nro. Cuenta </td>
						<td width="6%">Tipo</td>
						<td width="17%">Auxiliar</td>
						<td width="32%">Descripción</td>
						<td width="10%">Tipo Doc</td>
						<td width="10%">Documento</td>
				        <td width="10%"> Valor</td>
					  </tr>  
					</table>				
				</td>
              </tr>
             <%
               if(vecitems.size() > 0 ){
			   		for(int i=0; i< vecitems.size(); i++ ){
						Ingreso_detalle ingdetalle = (Ingreso_detalle) vecitems.get(i);
						x=i+1;
			            fecha = ingdetalle.getFecha_contabilizacion();
						%>
                        
			<tr id="filaItem<%=x%>" nowrap   class='<%=(x%2!=0)?"filaAzul":"filagrisFac"%>'  bordercolor="#D1DCEB">
          <td width="8%" nowrap class="bordereporte">			    <a id='n_i<%=x%>' ><%=x%></a>
                   <a  id="insert_item<%=x%>" style="cursor:hand"  ><img src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width="12" height="12" onClick="insertarItem('<%=BASEURL%>','<%=x%>');"></a>
				   <a onClick="borrarItem('filaItem<%=x%>')" id="borrarI<%=x%>" style="cursor:hand" ><img src='<%=BASEURL%>/images/botones/iconos/menos1.gif'   width="12" height="12"  > </a>
				   <input name="nroitem<%=x%>" type="hidden" id="nroitem<%=x%>" value="<%=ingdetalle.getItem()%>"> 
			    </td>
                <td> 
				<table width="100%" border="0" cellpadding="0" cellspacing="0" >
                      <tr align="center" class="bordereporte">
                        <td>
							<table width="100%">
							  <tr>
								<td width="12%" nowrap><input name='cuenta<%=x%>' type='text' id='cuenta<%=x%>' size='20' style="font-size:11px " maxlength='25' <%if(fecha.equals("0099-01-01 00:00:00")){%> onBlur="VerificarCuenta(<%=x%>);"<%}else{%>readonly<%}%>   value="<%=ingdetalle.getCuenta()%>" onFocus="this.select()"></td>
								<td width="6%" id="combotipo<%=x%>">
							 <%if(ingdetalle.getTipos() != null ){
									if(!fecha.equals("0099-01-01 00:00:00")){%>
									<input name='tipo<%=x%>' type='text' id='tipo<%=x%>' size='2' style="font-size:11px " maxlength='3'readonly  value="<%=ingdetalle.getTipo_aux()%>">
								   <%}else{%>   
									 <select name='tipo<%=x%>' class='textbox' id='tipo<%=x%>' onChange="form1.auxiliar<%=x%>.value='';" ><option value=''>  </option> 
                                     <% for(int j = 0; j<ingdetalle.getTipos().size(); j++){
        										TablaGen tipo = (TablaGen) ingdetalle.getTipos().get(j); %>
        								<option value='<%=tipo.getTable_code()%>' <%=(tipo.getTable_code().equals(ingdetalle.getTipo_aux()) )? "selected" : "" %> ><%=tipo.getTable_code()%></option>";
                          			<%}%>
                          			</select>
				                 <%}
							  }else{%>
						        <select name='tipo<%=x%>' >
									 <option value=''>  </option>
                                </select> 
					       <%}%>   </td>
								<td width="16%" nowrap><input name='auxiliar<%=x%>' type='text' id='auxiliar<%=x%>' size='20' style="font-size:11px " maxlength='25' value ="<%=ingdetalle.getAuxiliar()%>" <%if(fecha.equals("0099-01-01 00:00:00")){%>onBlur="validarAuxiliar('<%=BASEURL%>','<%=x%>');"<%}else{%>readonly<%}%>>
							    <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name="imagenAux<%=x%>" width="15" height="15"  id="imagenAux<%=x%>" style="cursor:hand"  onClick="buscarAuxiliar('<%=BASEURL%>','<%=x%>',form1.cuenta<%=x%>.value, form1.tipo<%=x%>.value);"  ></td>
								<td width="33%"><input name="c_descripcion<%=x%>" type="text"  id="c_descripcion<%=x%>"  value="<%=ingdetalle.getDescripcion()%>" style='width:99%' <%=(!fecha.equals("0099-01-01 00:00:00"))? "readonly":"" %>></td>
								<td width="11%"><%if(fecha.equals("0099-01-01 00:00:00")){%>
                                                <select name="tipodoc<%=x%>" onChange="VerificarTipoDocumento(<%=x%>);">
												<option value=""></option>
                                                <option value="001" <%=(ingdetalle.getTipo_doc().equals("001")) ? "selected" : ""%>  >Planilla</option>
                                                <option value="002" <%=(ingdetalle.getTipo_doc().equals("002")) ? "selected" : ""%>  >Remesa</option>
   							    </select><%}else{%>
   							    <input type="hidden" name="tipodoc<%=x%>" id="tipodoc<%=x%>">
   							    <input name="textfield" type="text" value="<%=(ingdetalle.getTipo_doc().equals("001")) ? "Planilla" : (ingdetalle.getTipo_doc().equals("002")) ? "Remesa" : "" %>" size="10" readonly>
   							    <%}%></td>
								<td width="11%"><input name="doc<%=x%>" type="text" class="textbox" id="c_valor_factura<%=x%>" style="text-align:right" size="15" maxlength="15" onBlur="VerificarDocumento(<%=x%>);" onFocus="this.select()" value="<%=ingdetalle.getDocumento()%>" <%=(!fecha.equals("0099-01-01 00:00:00"))? "readonly":"" %>></td>
						        <td width="11%"><input name="valor<%=x%>" type="text" class="textbox" id="valor<%=x%>" onfocus='this.value=sinformato(this.value);' <%=(mon.equals("DOL"))?" onChange='formatoDolar(this,2);'" : " onChange='this.value = Math.round( parseFloat( sinformato( this.value ) ) );formatear(this);'"%> style="text-align:right" onKeyPress="soloDigitos(event,'decOK');"  size="15" maxlength="15" <%if(fecha.equals("0099-01-01 00:00:00")){%>onblur="AcualizarVlrImpuesto('<%=x%>');"<%}else{%>readonly<%}%> value="<%=(mon.equals("DOL"))?UtilFinanzas.customFormat2( ingdetalle.getValor_ingreso_me() ):UtilFinanzas.customFormat( ingdetalle.getValor_ingreso_me() )%>"></td>
							  </tr>  
							</table>						
						</td>
                      </tr>
                  </table></td>
              </tr>
	         <% }
              }else{%>
			  <tr id="filaItem<%=x%>" nowrap   class='<%=(x%2!=0)?"filaAzul":"filagrisFac"%>'  bordercolor="#D1DCEB">
                <td width="8%" nowrap class="bordereporte">			      <a id='n_i<%=x%>' ><%=x%></a>
                   <a  id="insert_item<%=x%>" style="cursor:hand"  ><img src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width="12" height="12" onClick="insertarItem('<%=BASEURL%>','<%=x%>');"></a>
				   <a onClick="borrarItem('filaItem<%=x%>')" id="borrarI<%=x%>" style="cursor:hand" ><img src='<%=BASEURL%>/images/botones/iconos/menos1.gif'   width="12" height="12"  > </a>
				   <input name="nroitem<%=x%>" type="hidden" id="nroitem<%=x%>" value="<%=x%>"> 
			    </td>
                <td> 
				<table width="100%" border="0" cellpadding="0" cellspacing="0" >
                      <tr align="center" class="bordereporte">
                        <td>
							<table width="100%">
							  <tr>
								<td width="12%" nowrap><input name='cuenta<%=x%>' type='text' id='cuenta<%=x%>' size='20' style="font-size:11px " maxlength='25' onBlur="VerificarCuenta(<%=x%>);" onFocus="this.select()"></td>
								<td width="6%" id="combotipo<%=x%>"><select name='tipo<%=x%>'>
                                  <option value=''>  </option>
                                </select></td>
								<td width="16%" nowrap><input name='auxiliar<%=x%>' type='text' id='auxiliar<%=x%>' size='20' style="font-size:11px " maxlength='25' value ="" onBlur="validarAuxiliar('<%=BASEURL%>','<%=x%>');">
							    <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name="imagenAux<%=x%>" width="15" height="15"  id="imagenAux<%=x%>" style="cursor:hand"  onClick="buscarAuxiliar('<%=BASEURL%>','<%=x%>',form1.cuenta<%=x%>.value, form1.tipo<%=x%>.value);"  ></td>
								<td width="33%"><input name="c_descripcion<%=x%>" type="text"  id="c_descripcion<%=x%>"  style='width:99%'></td>
								<td width="11%"><select name="tipodoc<%=x%>" onChange="VerificarTipoDocumento(<%=x%>);">
												<option value=""></option>
                                                <option value="001">Planilla</option>
                                                <option value="002">Remesa</option>
   							    </select></td>
								<td width="11%"><input name="doc<%=x%>" type="text" class="textbox" id="c_valor_factura<%=x%>" onFocus="this.select()" style="text-align:right" size="15" maxlength="15" onBlur="VerificarDocumento(<%=x%>);"></td>
						        <td width="11%"><input name="valor<%=x%>" type="text" class="textbox" id="valor<%=x%>" onfocus='this.value=sinformato(this.value);' style="text-align:right" onKeyPress="soloDigitos(event,'decOK');" size="15" maxlength="15" onBlur="AcualizarVlrImpuesto('<%=x%>');" <%=(ing.getCodmoneda().equals("DOL"))?" onChange='formatoDolar(this,2);'" : " onChange='this.value = Math.round( parseFloat( sinformato( this.value ) ) );formatear(this);'"%> ></td>
							  </tr>  
							</table>						
						</td>
                      </tr>
                  </table></td>
              </tr>
		       <%}%>
            </table>
        </td>
      </tr>
    </table>  
    <input name="opcion" type="hidden" id="opcion" value="">
  </form >
	<br> 
	<div align="center">
	<%if( session.getAttribute("fin") == null && !ing.getPagina().equals("consulta") ){
		if(sw.equals("")){%>
			<img src='<%=BASEURL%>/images/botones/iconos/guardar.gif' name='Buscar' width="25" height="25" align="absmiddle" style='cursor:hand' title='Guardar...'  onclick="escribirArchivo('<%=CONTROLLER%>')" >&nbsp;
			<img src="<%=BASEURL%>/images/botones/restablecer.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick="Restablecer('<%=CONTROLLER%>')" >&nbsp;
		<%}%>
		<img src="<%=BASEURL%>/images/botones/<%=(!sw.equals("") ) ? "modificar.gif" : "aceptar.gif"%>"  height="21" name="imgaceptar"  onMouseOver="botonOver(this);" onClick="validarItems();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
	<%}%>
	<img src="<%=BASEURL%>/images/botones/imprimir.gif"  name="imgimprimir"  height="21" onMouseOver="botonOver(this);" onClick="form1.opcion.value='imp_all';form1.submit();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
	<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
	</div>
</div>
<%=datos[1]%>
</body>

<script>
    function enviar(url){
         var a = "<iframe name='ejecutor' style='visibility:hidden'  src='<%= CONTROLLER %>" + url + "'> ";
         aa.innerHTML = a;
    }

</script>
<font id='aa'></font>


</html>

