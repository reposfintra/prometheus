<%@page session="true"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@page import ="com.tsp.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<% 
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path); 
  Usuario usuario = (Usuario) session.getAttribute("Usuario");
  String ag = usuario.getId_agencia();  


  TreeMap bancos = model.servicioBanco.getBanco();
  TreeMap sucursales = model.servicioBanco.getSucursal();
  sucursales.put(" Seleccione Un Item", "");
  bancos.put(" Seleccione Un Item", "");

   Vector VecMon = model.monedaService.getMonedas();
   Moneda moneda;

  String sw = (request.getParameter("sw")!= null)?request.getParameter("sw"):"" ;
  String nom= (request.getParameter("nombre")!= null)?request.getParameter("nombre"):"" ;
  String nit=(request.getParameter("nit")!= null)?request.getParameter("nit"):"" ;
  String cod=(request.getParameter("identificacion")!= null)?request.getParameter("identificacion"):"" ;
  String fec=(request.getParameter("fecha")!= null)?request.getParameter("fecha"):Util.getFechaActual_String(4);
  String mon = (request.getParameter("moneda")!= null)?request.getParameter("moneda"):"" ;
  String con = (request.getParameter("concepto")!= null)?request.getParameter("concepto"):"" ;
  String res = (request.getParameter("msg")!= null)?request.getParameter("msg"):"" ;
  String res1 = (request.getParameter("msg2")!= null)?request.getParameter("msg2"):"" ;
  String ban = (request.getParameter("banco")!= null)?request.getParameter("banco"):"" ;
  String suc = (request.getParameter("sucursal")!= null)?request.getParameter("sucursal"):"" ;
  String des = (request.getParameter("descripcion")!= null)?request.getParameter("descripcion"):"" ;
  String nro = (request.getParameter("nro_consignacion") !=null)?request.getParameter("nro_consignacion"):"";
  String val = (request.getParameter("valor")!= null)?request.getParameter("valor").replace(",",""):"";
  String tp = (request.getParameter("tipodoc")!= null)?request.getParameter("tipodoc"):"" ;
  String cuen = (request.getParameter("cuentas")!= null)?request.getParameter("cuentas"):"" ;
  if( sw.equals("ok") ){
	  Identidad iden  = model.identidadService.obtIdentidad();
      if(iden!=null){		        
        nom = iden.getNombre();
      }
 }
%>
<html>
<head>
<title>Ingresos Miscelaneos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/ingreso.js"></script>
<script language="javascript" src="<%=BASEURL%>/js/general.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ingresos Miscelaneos"/>
</div>

 <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
  <br>
  <form name="form1" method="post" action="<%=CONTROLLER%>?estado=IngresoMiscelaneo&accion=Registrar&carpeta=jsp/cxcobrar/ingresoMiscelaneo&pagina=ingresoMiscelaneo.jsp&pagina=ingresar" id="form1" >
    <table width="680"  border="2" align="center">
	<tr>
	  <td>	     
	    <table width="100%">
          <tr>
            <td width="50%" class="subtitulo1">&nbsp;Información del Ingreso</td>
            <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
          </tr>
        </table>
	    <table width="100%">
              <tr  class="fila">
            <td>&nbsp;Tipo Ingreso</td>
		    <td><select name="tipodoc" class="textbox" id="select" style="width:90% ">
              <% LinkedList tbltipo = model.tablaGenService.getTipoIngresos();
               for(int i = 0; i<tbltipo.size(); i++){
                       TablaGen reg = (TablaGen) tbltipo.get(i); %>
              <option value="<%=reg.getTable_code()%>" <%=(reg.getTable_code().equals(tp))?"selected":""%> ><%=reg.getDescripcion()%></option>
              <%}%>
            </select> </td>
            <td colspan="2">&nbsp; </td>
            </tr>
          <tr >
            <td width="19%" class="fila">&nbsp;Recibido por</td>
            <td colspan="3" class="letra">
				<%if(res.equals("error") || res.equals("series") || res.equals("") ){%>
					<input name="identificacion" type="text" onBlur="BuscarIden('<%=CONTROLLER%>')" class="textbox" id="identificacion" onKeyPress="BuscarId(event,'decOK','<%=CONTROLLER%>')"  size="20" maxlength="15" value="<%=cod%>">
					<img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name='imagen' id='imagen' width='15' height='15' style='cursor:hand' title='Buscar Usuarios'  onClick="window.open('<%=CONTROLLER%>?estado=Cargar&accion=Varios&sw=19&abrir=ok' ,'USU','status=yes,scrollbars=no,width=700,height=500,resizable=yes');" > 
					<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
				 <%}else{%>
					<%=cod%>
				 <%}%>
                  <input name="nombre" class="filaresaltada" style='width:65%;text-align:left; border:0;' id="nombre" value="<%=nom%>" readonly>
             </td>
          </tr>
          <tr  class="fila">
            <td>&nbsp;Banco</td>
		    <td width="31%"><input:select name="banco" default="<%=ban%>" attributesText="class=textbox; onChange=\"form1.valor.value = sinformato(form1.valor.value);cargarSelects('controller?estado=Ingreso&accion=Cargar&carpeta=jsp/cxcobrar/ingresoMiscelaneo&pagina=ingresoMiscelaneo.jsp&evento=sucursal')\"" options="<%= bancos %>"/> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
             <script>form1.banco.value = '<%=ban.replaceAll("\n","")%>';</script>

            <td width="12%">Sucursal</td>
            <td width="38%"><input:select name="sucursal" default="<%=suc%>" attributesText="class=textbox; onChange=colocarCuenta();" options="<%= sucursales %>" /> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
			<input name="cuentas" id="cuentas"  class="filaresaltada" style='width:95%;text-align:left; border:0;' value="<%=cuen%>"></td>
            <script>form1.sucursal.value = '<%=suc.replaceAll("\n","")%>';</script>
          </tr>
          <tr  class="fila">
            <td>Nro Consignaci&oacute;n </td>
            <td><input name="nro_consignacion" class="textbox" type="text" value="<%=nro%>" size="25" maxlength="25"></td>
            <td>Fecha </td>
            <td><input   name="fecha"  type="text"  class="textbox" value="<%=fec%>" size="12" readonly >
              <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fecha);return false;" hidefocus><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </td>
          </tr>
          <tr class="fila">
            <td>Valor Consignaci&oacute;n</td>
            <td><input name="valor" type="text" class="textbox" id="valor" onKeyPress="soloDigitos(event,'decOK');" onFocus="this.select()"
								onChange="
									if(form1.moneda.value == 'DOL'){ 
										formatoDolar(this,2); 
									}else{ 
										this.value = Math.round( parseFloat( sinformato( this.value ) ) );
										valor.value = formato(this.value);
									}" size="20" maxlength="15" value="<%=(mon.equals("DOL"))?UtilFinanzas.customFormat2( (val.equals(""))?"0":val ):UtilFinanzas.customFormat((val.equals(""))?"0":val)%>" style="text-align:right;">              <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
            <td>Moneda</td>
            <td><select name="moneda"  class="textbox" id="moneda" onChange="cambiar(this.value)">
              <%if ( VecMon.size() > 0 ) {   
                for ( int i=0;  i < VecMon.size(); i++ ){ 
                     moneda = (Moneda) VecMon.elementAt(i);%>
              		<option value="<%=moneda.getCodMoneda()%>" <%=(moneda.getCodMoneda().equals(mon))?"selected":""%>><%=moneda.getNomMoneda()%></option>
               <%}
		      }%>
            </select>
              <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          </tr>
          <tr  class="fila">
            <td>Concepto</td>
            <td><select name="concepto" class="textbox" id="select" style="width:90% ">
              <option value="">Seleccione Un Item</option>
              <% LinkedList tblres = model.tablaGenService.obtenerTablas();
               for(int i = 0; i<tblres.size(); i++){
                       TablaGen respa = (TablaGen) tblres.get(i); %>
              <option value="<%=respa.getTable_code()%>" <%=(respa.getTable_code().equals(con))?"selected":""%> ><%=respa.getDescripcion()%></option>
              <%}%>
            </select>
              <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
            <td>Descripci&oacute;n</td>
            <td><textarea name="descripcion" cols="40" rows="2" class="textbox"><%=des%></textarea></td>
          </tr>
        </table>
	    </td>
	</tr>
  </table>
    <br>
	 <div align="center">
      <%if(!res.equals("") && !res.equals("error")&& !res.equals("series")){%>
		   <input name="opcion" type="hidden" id="opcion" value="imp_all">
		   <img src="<%=BASEURL%>/images/botones/imprimir.gif"  name="imgimp"  onMouseOver="botonOver(this);" onClick="form1.opcion.value='imp_all';form1.valor.value = sinformato(form1.valor.value);form1.submit();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
      <%if(ag.equals("OP")){%>
      <img src="<%=BASEURL%>/images/botones/detalles.gif"  name="imgdetalle" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick="window.open('<%=CONTROLLER%>?estado=IngresoMiscelaneo&accion=Buscar&numero=<%=res%>&tipodoc=<%=tp%>&evento=Cabecera&pagina=ingresar','','height=600,width=900,dependent=yes,resizable=yes,scrollbars=no,status=yes');">&nbsp;
     <%}
       }else{%>
	   <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="validarIngresoMiscelaneo('<%=BASEURL%>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"   >&nbsp;
     <%}%>
	   <img src="<%=BASEURL%>/images/botones/restablecer.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=ingresoMiscelaneo.jsp&carpeta=/jsp/cxcobrar/ingresoMiscelaneo&marco=no&opcion=cxcobrar'" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;
       <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
   </div>
   <input name="tasa" type="hidden" id="tasa" value="">
 <%if(!res.equals("")){%>
 <br>
  <table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="300" align="center" class="mensajes">
       <%if(res.equals("error")){%>
              No existen Registro en la Tasas para la conversion
	   <%}else if(res.equals("serie")){%>
               No existe serie para el ingreso 
        <%}else{%>
              El ingreso de <%=res1%>
              <input name="numingreso" type="hidden" id="numingreso" value="<%=res%>">
           <br>Se ha registrado con Nro <%=res%> 
        <%}%>
        </td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
 <%}%>
<%if( request.getAttribute("mensaje") != null){%>
	<br>
	<table border="2" align="center">
		<tr>
			<td>
				<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
					<tr>
						<td width="300" align="center" class="mensajes">
							<%=(String)request.getAttribute("mensaje")%></td>
						<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
						<td width="58">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<%}%>
   </form>
 </div>
<%=datos[1]%>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>
<%if( sw.equals("no") ){%>
  <script>
      alert("No existe registrado la identificacion");
      form1.identificacion.value = '';     
  </script>
<%}%>
<script>
function colocarCuenta(){
	var vec = form1.sucursal.value.split("/");
	if( form1.sucursal.value != "" ){
		if( vec != null )
			form1.cuentas.value = vec[1];
	}
	else{
		form1.cuentas.value = "";
	}
}
</script>