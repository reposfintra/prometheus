<%@page session="true"%>
<%@page import="java.util.*" %>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>  
<title>Buscar Ingreso</title> 
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">  
<link href="file:///C|/Tomcat5/webapps/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/ingreso.js"></script>


</head>
<body onResize="redimensionar();" onLoad='redimensionar();' > 

<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path); 
String res = (request.getParameter("msg")!= null)? request.getParameter("msg") : "";%>

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Ingreso Miscelaneo"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<FORM ACTION="<%=CONTROLLER%>?estado=IngresoMiscelaneo&accion=Buscar&carpeta=jsp/cxcobrar/ingresoMiscelaneo&pagina=ingresoMiscelaneoMod.jsp" METHOD='post' id='form1' name='form1'>
  <table width="450" border="2" align="center">
    <tr>
      <td><table width="100%"  border="0">
        <tr>
          <td width="50%" class="subtitulo1">&nbsp;Buscar Ingreso</td>
          <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
          </tr>
      </table>      <table width="100%" class="tablaInferior">
        <tr class="fila">
          <td><input name="evento" id="radio" type="radio" value="4" > Tipo
</td>
          <td colspan="2"><select name="tipodoc" class="textbox" id="select" style="width:90% ">
			<option value="%">Seleccione un items</option>
            <% LinkedList tbltipo = model.tablaGenService.getTipoIngresos();
               for(int i = 0; i<tbltipo.size(); i++){
                       TablaGen reg = (TablaGen) tbltipo.get(i); %>
            <option value="<%=reg.getTable_code()%>"  ><%=reg.getDescripcion()%></option>
            <%}%>
          </select></td>
          </tr>
        <tr class="fila">
          <td width="125"><input name="evento" id="evento" type="radio" value="1" >
            Nro. Ingreso </td>
          <td colspan="2"><input  class='textbox' type='text' name='numero' size='15' maxlength='11' onKeyPress="soloAlfa(event,'decNO');"   >
</td>
        </tr>
        <tr class="fila">
          <td><input name="evento" type="radio" value="2">
            Identificaci&oacute;n</td>
          <td width="104"><input name="identificacion" type="text" class="textbox" id="identificacion" onKeyPress="soloDigitos(event,'decNO');" size="20" maxlength="15" ></td>
          <td width="193">&nbsp;</td>
          </tr>
        <tr class="fila">
          <td><input name="evento" type="radio" value="3">
            Fecha</td>
          <td><input  name='fechaInicio' size="11" readonly="true" class="textbox" >
            <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(fechaInicio);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt=""></a></td>
          <td><input  name='fechaFinal' size="11" readonly="true" class="textbox" >
            <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(fechaFinal);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt=""></a></td>
          </tr>
        
      </table></td>
    </tr>
  </table>
 <br>
  <div align="center"><img src="<%=BASEURL%>/images/botones/buscar.gif"  name="imgbuscar" onClick="validarBusqMiscelaneo();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></div>
  <br>
<%if(!res.equals("")){%>
 <br>
  <table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="300" align="center" class="mensajes">La busqueda no arrojo resultados</td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>

 <%}%>

</form>
</div>
<%=datos[1]%>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>
