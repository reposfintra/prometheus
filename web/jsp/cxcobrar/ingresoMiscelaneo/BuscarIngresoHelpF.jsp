<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE></TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="<%=BASEURL%>/../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<style type="text/css">
<!--
.Estilo2 {
	font-size: 16pt;
	color: #FF0000;
}
-->
</style>
</HEAD>
<BODY> 

        <table width="90%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20" align="center">MANUAL DE BUSQUEDA DE INGRESO DE PAGOS MISCELANEOS </td>
          </tr>
          <tr class="subtitulo1">
            <td> Las b&uacute;squedas se pueden realizar de acuerdo a los filtros del formulario.</td>
          </tr>
          <tr>
            <td  height="8"  class="ayudaHtmlTexto"><p>Si desea buscar por un numero de ingreso</p>            </td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/ingreso/miscelaneo/imagen4.jpg"></div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><p align="left">El sistema le muestra la siguiente pantalla</p>
            </td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/ingreso/miscelaneo/imagen5.jpg">
              </div></td>
          </tr>
<tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><p align="left">Si desea realizar la b&uacute;squeda por medio una identifiaci&oacute;n o por un rango de fecha, solo debe digitar la informaci&oacute;n correspondiente a la b&uacute;squeda. Nota: En la b&uacute;squeda por identificaci&oacute;n es obligatorio, seleccionar un rango de fechas</p>
            </td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/ingreso/miscelaneo/imagen6.jpg">
              </div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="left">El sistema le muestra la siguiente pantalla</div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/ingreso/miscelaneo/imagen7.jpg"></div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="left">El sistema le muestra la pantalla con la informaci&oacute;n correspondiente al ingreso, permitiendo modificar, anular el ingreso. Ademas registrar o modificar los detalles del ingreso.<br>
            </div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><img src="<%=BASEURL%>/images/ayuda/ingreso/miscelaneo/imagen5.jpg"></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="left">Si presiona modificar el sistema le muestra el siguiente mensaje</div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/ingreso/ima5.jpg"></div></td>
          </tr>
         
      </table>
</BODY>
</HTML>
