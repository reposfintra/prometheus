<!--
- Autor : Ing. Diogenes Bastidas Morales
- Date  : 28 de noviembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, lista de ingresos

--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%> 
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
 <% Vector lista = model.nitService.getPropietarios(); %>
<html>
<head>
<title>Listado de Usuarios</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>

</head>
<body onLoad="redimensionar();" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listado de Usuarios"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<form name="form" id="form" method="post" action="<%=CONTROLLER%>?estado=Cargar&accion=Varios&sw=19&abrir=">
    <table width="400" border="2" align="center">
      <tr>
        <td>
			<table width="100%" align="center"  class="tablaInferior">
				<tr>
					<td width="50%" class="subtitulo1">&nbsp;Busqueda de usuarios</td>
					<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"></td>
				</tr>
				<tr class="fila">
					<td>Buscar por nombre o por cedula</td>
					<td><input name="nit" type="text" class="textbox" id="nit" size="40"></td>
				</tr>				
        	</table>
		</td>
      </tr>
    </table>
	<br>
	<div align="center">
		<img src="<%=BASEURL%>/images/botones/buscar.gif" style="cursor:hand" title="Buscar Usuarios" name="buscar"  onClick="form.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
		<img src="<%=BASEURL%>/images/botones/salir.gif"  name="regresar" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >
	</div>
</form>
<%  
	if ( lista != null && lista.size() >0 ){  
%>

<table width="500" border="2" align="center">
    <tr>
		<td width="100%">
			<table width="100%" align="center">
				  <tr>
					<td width="373" class="subtitulo1">&nbsp;Datos de Usuarios</td>
					<td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
				  </tr>
			</table>
			<table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
				<tr class="tblTitulo">
					<td width="20%" align="center">Cedula</td>
					<td width="80%" align="center">Nombre</td>
				</tr>
				<%-- keep track of preference --%>
				<%
				for (int i = 0; i < lista.size(); i++)
				{
					Nit n = ( Nit )lista.get(i);%>
					<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand" onClick="parent.opener.document.getElementById('identificacion').value='<%=n.getNit()%>';parent.opener.document.getElementById('nombre').value='<%=n.getName()%>';window.close();">
						<td width="20%" class="bordereporte" align="center"><%=n.getNit()%></td>
						<td width="80%" class="bordereporte" nowrap><%=n.getName()%></td>
					</tr>
				<%}%>
			</table>
		</td>
    </tr>
</table>
  <br>
<table width="500" border="0" align="center">
	<tr>
		<td width="1515"><img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgaceptar" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
	</tr>
</table>
<%}%>
<%if(request.getAttribute("mensaje")!=null){%>
  <br>
	<table border="2" align="center">
		<tr>
			<td>
				<table width="410" height="40" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
					<tr>
						<td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
						<td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
						<td width="78">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
  <%}%>

</div> 
</body>

</html>
