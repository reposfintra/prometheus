<!--
- Autor : Ing. Jose de la rosa
- Date  : 10 de Mayo del 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja los items de los ingresos
--%>       

<%-- Declaracion de librerias--%>
<%@page session="true"%> 
<%@page import="java.util.*" %>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>Ingreso Detalles</title>
<link href="<%= BASEURL %>/css/estilostspFactura.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
 <script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
 <script type='text/javascript' src="<%=BASEURL%>/js/Ingreso_detalle.js"></script>
 <script type='text/javascript' src="<%=BASEURL%>/js/general.js"></script> 
</head> 
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Consulta Items Ingreso"/>
</div> 

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%
	String distrito = (String) session.getAttribute ("Distrito");	
	String moneda_local = model.ingreso_detalleService.monedaLocal(distrito);
	String descripcion = "", fecha_factura = "", facturas = "", moneda_factura = "";
	int x=0;
	double valor_factura = 0, valor_factura_me = 0, impuesto_rfte = 0, impuesto_rica = 0, valor_saldo_me = 0;
	String cod_rfte = "", cod_rica = "", money = "";
	if( model.ingreso_detalleService.getCabecera ()!=null ){
		// objeto de la factura
		Ingreso_detalle factura = model.ingreso_detalleService.getFactura ();
		// objeto de cabecera
		Ingreso_detalle cabecera = model.ingreso_detalleService.getCabecera ();
		cod_rfte = cabecera.getCodigo_retefuente();
		// vector de items
		Vector a = model.ingreso_detalleService.getVecIngreso_detalle ();
		if ( a != null){
			for( x=0; x < a.size(); x++){
				Ingreso_detalle item0 = (Ingreso_detalle)a.elementAt(x);
				if( !item0.getMoneda_factura().equals( cabecera.getMoneda () ) )
					money = item0.getMoneda_factura();
			}
		}
   %>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Ingreso&accion=Registrar" id="form1" >
<table width="1000" border="2" align="center">
    <tr>
      <td width="100%" height="60">
        <table width="100%">
          <tr>
            <td width="50%" height="22" align="left" class="subtitulo1">&nbsp;Información del Ingreso</td>
            <td width="50%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"></td>
          </tr>
        </table>
        <table width="100%">
          <tr height="20">
            <td width="10%" class="filaFactura">Cliente</td>
            <td class="letra"><%=cabecera.getCliente()%> - <%=cabecera.getNom_cliente ()%></td>
            <td class="letra"><span class="filaFactura">Nro Ingreso</span></td>
            <td class="letra"><%=cabecera.getNumero_ingreso ()%></td>
            <td class="filaFactura">Fecha Creaci&oacute;n </td>
            <td class="letra"><%=cabecera.getCreation_date()!=null?cabecera.getCreation_date().substring (0,10):""%></td>
            <input name="num" type="hidden" id="num" value="<%=cabecera.getNumero_ingreso ()%>">
            <input name="tipodoc" type="hidden" id="tipodoc" value="<%=cabecera.getTipo_documento()%>">
            <td width="17%" class="filaFactura">Nit del Cliente </td>
            <td width="12%" class="letra"><%=cabecera.getNit_cliente ()%></td>
          </tr>
		  <tr height="20">
            <td class="filaFactura">Valor Consignación</td>
            <td width="22%" class="letra"><%=Util.customFormat(cabecera.getValor_ingreso_me ())%></td>
            <td width="10%" class="filaFactura">Moneda Ingreso </td>
            <td width="8%" class="letra"><%=cabecera.getMoneda ()%></td>
            <td width="13%" class="filaFactura">Fecha Consignación</td>
            <td width="8%" class="letra"><%=cabecera.getFecha_consignacion ()%></td>
            <td class="filaFactura">Total Acumulado Facturas</td>
            <td class="letra" align="center"><%=cabecera.getValor_total()!=0?Util.customFormat( cabecera.getValor_total() ):""%></td>
          </tr>
      </table>
      </td>
    </tr>
  </table>
<table width="1000" border="2" align="center">
      <tr>
        <td >
          <table width="100%" align="center"  >
            <tr>
              <td width="50%" align="left" class="subtitulo1">&nbsp;Items del Ingreso</td>
              <td colspan="2"align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
            </tr>
			<tr class="letra">
				<td>
				</td>
				<td width="50%" height="20" colspan="2" align="center" class="letra"><strong>Valor Tasa</strong>&nbsp;&nbsp; <%=cabecera.getValor_tasa ()!=0? Util.customFormat( cabecera.getValor_tasa () ):""%> <strong><%=money%></strong></td>
			</tr>
          </table>

            <table id="detalle" width="100%" >
              <tr  id="fila1" class="tblTituloFactura">
                <td align="center">Item</td>
                <td align="center">
					<table align="center" class="tblTituloFactura" width="100%">
					  <tr class="tblTituloFactura">
						<td width="12%">Factura Nro</td>
						<td colspan="3">Descripción</td>
						<td width="10%">Fecha Fac.</td>
						<td width="13%">Vlr Factura MF.</td>
						<td width="13%">Vlr Saldo MF. </td>
					    <td width="13%">Vlr Saldo MI. </td>
					  </tr>	  
					  <tr align="center" class="tblTituloFactura">
						<td>Nro Cuenta </td>
						<td width="13%" rowspan="2">Cod Rfte</td>
						<td width="13%" rowspan="2">Valor Rfte</td>
						<td width="13%" rowspan="2">Cod Rica</td>
						<td rowspan="2">Valor Rica</td>
						<td rowspan="2">&nbsp;</td>
						<td rowspan="2">Vlr Abono</td>
					    <td rowspan="2">Nuevo Saldo</td>
					  </tr>
					  <tr align="center" class="tblTituloFactura">
					    <td>&nbsp;</td>
				      </tr>  
					</table>				
				</td>
              </tr>
              <% 
				if ( a != null){
					for( x=0; x < a.size(); x++){
						Ingreso_detalle item = (Ingreso_detalle)a.elementAt(x);
						descripcion = item.getDescripcion_factura();
						fecha_factura = item.getFecha_factura();
						valor_factura = item.getValor_factura();
						valor_factura_me = item.getValor_factura_me();
						facturas = item.getFactura();
						moneda_factura = item.getMoneda_factura();
						cod_rfte = item.getCodigo_retefuente();
						cod_rica = item.getCodigo_reteica();
						impuesto_rfte = cabecera.getPorcentaje_rfte();
						impuesto_rica = item.getPorcentaje_rica();
						valor_saldo_me = item.getValor_saldo_factura_me();
					%>
					  <tr class="letra" id="filaItem<%=x%>" nowrap  bordercolor="#D1DCEB" >
						<td class="bordereporte" align="center"><strong><%=(x+1)%></strong></td>
						<td> 
						<table width="100%" border="0" cellpadding="1" cellspacing="0" class="filagris">
							  <tr align="center" class="bordereporte">
								<td>
									<table width='100%'>
									  <tr class="letra">
										<td width='12%' height="18"><strong><%=facturas%></strong></td>
										<td colspan='3'><strong><%=descripcion%></strong></td>
										<td width="10%"><strong><%=fecha_factura%></strong></td>
										<td width='13%'><%=(moneda_factura.equals("DOL"))?UtilFinanzas.customFormat2(item.getValor_factura_me()):UtilFinanzas.customFormat(item.getValor_factura_me())%>&nbsp;<strong><%=item.getMoneda_factura()%></strong></td>
										<td width='13%'><%=(moneda_factura.equals("DOL"))?UtilFinanzas.customFormat2(item.getValor_saldo_factura_me()):UtilFinanzas.customFormat(item.getValor_saldo_factura_me())%>&nbsp;<strong><%=item.getMoneda_factura()%></strong></td>
									    <td width='13%'><%=(cabecera.getMoneda ().equals("DOL"))?UtilFinanzas.customFormat2(item.getValor_saldo_factura()):UtilFinanzas.customFormat(item.getValor_saldo_factura())%>&nbsp;<strong><%=cabecera.getMoneda ()%></strong></td>
									  </tr>	  
									  <tr align='center' class="letra">
										<td align='center' height="8"><strong><%=item.getCuenta()%></strong></td>
										<td width='13%' rowspan="2"><strong><%=cod_rfte%></strong></td>
										<td width='13%' rowspan="2"><%=(cabecera.getMoneda ().equals("DOL"))?UtilFinanzas.customFormat2(item.getValor_retefuente()):UtilFinanzas.customFormat(item.getValor_retefuente())%></td>
										<td width='13%' rowspan="2"><strong><%=cod_rica%></strong></td>
										<td rowspan="2"><%=( (cabecera.getMoneda ().equals("DOL"))?UtilFinanzas.customFormat2(item.getValor_reteica()):UtilFinanzas.customFormat(item.getValor_reteica()) )%></td>
										<td rowspan="2">&nbsp;</td>
										<td rowspan="2" ><%=(cabecera.getMoneda ().equals("DOL"))?UtilFinanzas.customFormat2(item.getValor_abono()):UtilFinanzas.customFormat(item.getValor_abono())%></td>
									    <td rowspan="2" ><%=(cabecera.getMoneda ().equals("DOL"))?UtilFinanzas.customFormat2(item.getValor_total_factura()):UtilFinanzas.customFormat(item.getValor_total_factura())%></td>
									  </tr>
									  <tr align='center' class="letra">
									    <td align='center' height="8"><a href="javascript: void(0);" onClick="window.open('<%=CONTROLLER%>?estado=Ingreso_detalle&accion=Update&c_factura=<%=facturas%>&opcion=6','','status=no,scrollbars=no,width=800,height=500,resizable=yes');"  id="Mostrar<%=x%>" style="cursor:hand" >Otros Ingresos</a></td>
								      </tr>  
									</table>						
								</td>
							  </tr>
						  </table></td>
					  </tr>
              <%  }
			  }%>
			  
            </table>
        </td>
      </tr>
    </table>  
	<br> 
	<div align="center">
		<input name="opcion" type="hidden" id="opcion" value="">
        <img src="<%=BASEURL%>/images/botones/imprimir.gif"  name="imgimp"  onMouseOver="botonOver(this);" onClick="form1.opcion.value='impitems';form1.submit();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
		<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
	</div>
  <%}%>
</form>
</div>
</body>
</html>
