<!--
- Autor : Ing. Jose de la rosa
- Date  : 18 Mayo de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite buscar las facturas de los clientes
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.util.*"%>
<%
	LinkedList t_aux = modelcontab.subledgerService.getCuentastsubledger();
    Vector vec = model.ingreso_detalleService.getVecIngreso_detalle ();
	Ingreso_detalle cabecera = model.ingreso_detalleService.getCabecera ();
	String mensaje = (String) request.getAttribute("mensaje");
	double total = 0;
	int tam = 0;
	for (int i = 0; i < vec.size(); i++){
		Ingreso_detalle item = ( Ingreso_detalle )vec.get(i);
		total += item.getValor_total_factura();
		if( item.getValor_total_factura() > 0 )
			tam++;
	}
	%>
<html>
<head><title>Ajustes de Ingreso</title></head>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script language="javascript" src="<%=BASEURL%>/js/general.js"></script>
<script language="javascript" src="<%=BASEURL%>/js/ingreso.js"></script>
<script>
	function cambiacolor(element){
		var tr = element.parentNode.parentNode;
		tr.className = (element.checked?'filaazul':'filagris');
	}
	function sumatoria ( valor, moneda, check ){
		if(check.checked){
			if(form1.total.value != '')
				form1.total.value = parseFloat( sinformato(form1.total.value) ) + parseFloat(valor);
			else
				form1.total.value = valor;
		}
		else{
			if(form1.total.value != '')
				form1.total.value = parseFloat( sinformato(form1.total.value) ) - parseFloat(valor);
			else
				form1.total.value = valor;
		}
		if( parseFloat( form1.total.value) == 0 ){
			form1.total.value = '';
		}
		else{
			if( moneda == 'DOL'){ 
				formatoDolar(form1.total,2); 
			}else{ 
				this.value = Math.round( parseFloat( sinformato( form1.total.value ) ) );
				form1.total.value = formato(form1.total.value);
			}
		}
	}
</script>
<body onLoad="<%if(request.getParameter ("cerrar")!=null){%>parent.opener.location='<%=BASEURL%>/jsp/cxcobrar/ingreso_detalle/UpdateItemsIngresoDetalle.jsp';window.close();<%}%>redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ajustar Ingreso"/>
</div> 

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
	<%if((cabecera.getValor_ingreso_me ()-cabecera.getValor_total()) > 0 ){%>
	<form action="<%=CONTROLLER%>?estado=Ingreso_detalle&accion=Update&opcion=7" method="post" name="form1" id="form1">
	<table width="550" border="2" align="center">
      <tr>
        <td>
			<table width="100%" align="center"  class="tablaInferior">
				<tr>
					<td colspan="3" class="subtitulo1">&nbsp;Informacion de Cuenta</td>
					<td colspan="3" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"></td>
				</tr>
				<tr class="fila" id="cuenta">
						<td width="10%">&nbsp;Cuenta</td>
						<td width="27%"><input name='cuenta1' type='text' id='cuenta1' class="textbox" size='20' style="font-size:11px " maxlength='25' onBlur="VerificarCuentaContable(1,3);"  onFocus="this.select()">
						<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
						<td width="9%">&nbsp;Tipo</td>
						<td width="10%" id="combotipo1"><select name='tipo1'  class='textbox' id='tipo1' onChange="form1.auxiliar1.value='';">
																	<option value=''> </option>
							</select></td>
						<td width="16%">&nbsp;Auxiliar</td>
						<td width="28%"><input name='auxiliar1' class="textbox" type='text' id='auxiliar1' size='20' style="font-size:11px " maxlength='25' onBlur="validarAuxiliar('<%=BASEURL%>','1');" onFocus="this.select()">
							<img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name="imagenAux" width="15" height="15"  id="imagenAux" style="cursor:hand"  onClick="buscarAuxiliar('<%=BASEURL%>','1',form1.cuenta1.value, form1.tipo1.value);"  ></td>
					</tr>
					<tr>
						<td class="fila">&nbsp;</td>
						<td class="fila" align="center" nowrap>Valor Consignaci&oacute;n </td>
						<td colspan="2" class="letra">&nbsp;<%= (cabecera.getMoneda ().equals("DOL"))?UtilFinanzas.customFormat2(cabecera.getValor_ingreso_me ()):UtilFinanzas.customFormat(cabecera.getValor_ingreso_me ())%></td>
					    <td class="fila" >&nbsp;Acumulado Facturas </td>
					    <td class="letra">&nbsp;<%= cabecera.getValor_total() == 0 ? "" : ( (cabecera.getMoneda ().equals("DOL"))?UtilFinanzas.customFormat2(cabecera.getValor_total()):UtilFinanzas.customFormat(cabecera.getValor_total()) )%></td>
					</tr>
					<tr>
					  <td class="fila">&nbsp;</td>
					  <td class="fila" align="center">Valor Ajuste</td>
					  <td colspan="4" class="letra"><input type="text" name="total" id="total" class="textbox" style="text-align:right;" onfocus='this.value=sinformato(this.value);' 
					  								value="<%= cabecera.getValor_total() == 0 ? "" : ( (cabecera.getMoneda ().equals("DOL"))?UtilFinanzas.customFormat2(cabecera.getValor_ingreso_me ()-cabecera.getValor_total()):UtilFinanzas.customFormat(cabecera.getValor_ingreso_me ()-cabecera.getValor_total()) )%>" 
													onBlur="if( parseFloat(this.value) > parseFloat('<%=(cabecera.getValor_ingreso_me ()-cabecera.getValor_total())%>') ){alert('No se puede colocar un valor mayor al valor del saldo del ingreso');this.focus(); }else{<%=(cabecera.getMoneda ().equals("DOL"))?"formatoDolar(this,2);":"formatear(this);"%>}"></td>
			  </tr>
        	</table>
		</td>
      </tr>
    </table>
	<p align="center">
		<img src="<%=BASEURL%>/images/botones/aceptar.gif" id="imgaceptar" height="21" name="imgaceptar"  onMouseOver="botonOver(this);" onClick="enviarDatos();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
		<img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close(); this.disabled=true;" onMouseOut="botonOut(this);" style="cursor:hand">
	</p>
  </form>
	<%--Mensaje de informacion--%>
	<%}else{%>
		<br>
		<table border="2" align="center">
			<tr>
				<td>
					<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						<tr>
							<td width="229" align="center" class="mensajes">No hay saldo en el ingreso para ajustar.</td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<p align="center">
			<img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close(); this.disabled=true;" onMouseOut="botonOut(this);" style="cursor:hand">
		</p>		
	<%}
	if(mensaje!=null){%>
		<script> 
			alert('<%= mensaje %>');
		</script>
	<% } %>
</div>
</body>
</html>
<script>
function enviarDatos(){
	if( form1.cuenta1.value == '' ){
		alert("Debe llenar el campo de la cuenta!");
		form1.cuenta1.focus();
		return false;
	}
	else if( form1.tipo1.value != '' && form1.auxiliar1.value == '' ){
		alert("Debe llenar el campo del auxiliar!");
		form1.auxiliar1.focus();
		return false;
	}
	else if( form1.total.value == ''){
		alert("Debe chequear facturas a ajustar!");
		return false;
	}
	else{
		form1.imgaceptar.disabled=true;
		form1.submit();
	}
}
    function enviar(url){
         var a = "<iframe name='ejecutor' style='visibility:hidden'  src='<%= CONTROLLER %>" + url + "'> ";
         aa.innerHTML = a;
    }
</script>
<font id='aa'></font>