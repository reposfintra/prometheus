<!--  
	 - Author(s)       :      Jose de la Rosa
	 - Description     :      AYUDA FUNCIONAL - Items Ingreso
	 - Date            :      01/08/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>AYUDA FUNCIONAL - Items De Ingreso</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/cxcobrar/items_ingreso/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL ITEMS DE INGRESO </div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa para Modificar Items de Ingreso.</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p>&nbsp;</p>
              </div></td>
          </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">En la siguiente pantalla se permiten ingresar los items de las facturas del ingreso.</p>
              </td>
          </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p><IMG src="<%=BASEIMG%>principal.JPG" border=0 ></p>
              <p><strong>Imagen1</strong></p>
            </div></td>
          </tr>
          
            <tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">Existen dos formas de ingresar facturas: </p>
            <p class="ayudaHtmlTexto">* La primera es de forma manual se digita la factura en le campo de texto de &quot;Factura Nro&quot; como se muestra en la <strong>Imagen1</strong>   luego presiona enter o en el boton <img id="imgini" src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width="12" height="12" >, si no existe la factura aparecer&aacute; el siguiente mensaje de error como lo ilustra la <strong>Imagen2</strong>.</p></td>
            </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p><IMG  src="<%=BASEIMG%>mensajeNoExisteFactura.JPG" border=0 ></p>
              <p><strong>Imagen2</strong></p>
            </div></td>
            </tr>
		
            <tr>
              <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
                <p>Si la facura ya existe en un item anterior, entonces el sistema le mostrar&aacute; el siguiente mensaje como lo ilustra la <strong>Imagen3</strong>. </p></td>
            </tr>
            <tr>
              <td  class="ayudaHtmlTexto"><div align="center">
                <p><IMG src="<%=BASEIMG%>mensajeYaExisteFactura.JPG" border=0 ></p>
                <p><strong>Imagen3</strong></p>
              </div></td>
            </tr>
            <tr>
              <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
                <p>Si no digito la factura y presiono enter o el boton <img id="imgini" src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width="12" height="12" >, entonces el sistema le mostrar&aacute; el siguiente mensaje como se ilustra en la <strong>Imagen4</strong>. </p></td>
            </tr>
            <tr>
              <td  class="ayudaHtmlTexto"><div align="center">
                <p><IMG src="<%=BASEIMG%>mensajeLlenarFactura.JPG" border=0 ></p>
                <p><strong>Imagen4</strong></p>
              </div></td>
            </tr>        
            <tr>
              <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
                <p>* La segunda es de forma automatica presiona click en le link de &quot;Items de Ingreso &quot; como se muestra en la <strong>Imagen1</strong> luego selecciona las facturas que desea agregar en los items como lo ilustra la <strong>Imagen5</strong>.</p></td>
            </tr>
            <tr>
              <td  class="ayudaHtmlTexto"><div align="center">
                <p><IMG src="<%=BASEIMG%>listadoFacturas.JPG" border=0 ></p>
                <p><strong>Imagen5</strong></p>
              </div></td>
            </tr> 
	        <tr>
              <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
                <p>Si selecciona varias facturas con monedas diferentes a la moneda del ingreso, el sistema le mostrar&aacute; el siguiente mensaje como se ilustra en la <strong>Imagen6</strong>. </p></td>
            </tr>
            <tr>
              <td  class="ayudaHtmlTexto"><div align="center">
                <p><IMG src="<%=BASEIMG%>mensajeMonedasDiferente.JPG" border=0 ></p>
                <p><strong>Imagen6</strong></p>
              </div></td>
            </tr> 			
			<tr>
              <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
                <p>Si no existen facturas asociadas al  cliente, entonces el sistema le mostrar&aacute; el siguiente mensaje como se ilustra en la <strong>Imagen7</strong>. </p></td>
            </tr>
            <tr>
              <td  class="ayudaHtmlTexto"><div align="center">
                <p><IMG src="<%=BASEIMG%>mensajeNoHayFactura.JPG" border=0 ></p>
                <p><strong>Imagen7</strong></p>
              </div></td>
            </tr> 			
			<tr>
              <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
                <p>Para aplicar un impuesto de retención a la fuente a el item de la factura debe presionar click en el primer boton <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif' name="imagenRica" width="15" height="15"> y en el listado que aparece en la <strong>Imagen7</strong> presionar click sobre el porcentaje que desa aplicarle a la factura.</p></td>
            </tr>
            <tr>
              <td  class="ayudaHtmlTexto"><div align="center">
                <p><IMG src="<%=BASEIMG%>listadoRetefuente.JPG" border=0 ></p>
                <p><strong>Imagen8</strong></p>
              </div></td>
            </tr> 		
			<tr>
              <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
                <p>Para aplicar un impuesto de reteica a el item de la factura debe presionar click en el segundo boton <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif' name="imagenRica" width="15" height="15"> y en el listado que aparece en la <strong>Imagen9</strong> presionar click sobre el porcentaje que desa aplicarle a la factura.</p></td>
            </tr>
            <tr>
              <td  class="ayudaHtmlTexto"><div align="center">
                <p><IMG src="<%=BASEIMG%>listadoRica.JPG" border=0 ></p>
                <p><strong>Imagen9</strong></p>
              </div></td>
			<tr>
              <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
                <p class="ayudaHtmlTexto">Existen dos formas de eliminar items de facturas: </p>
                <p class="ayudaHtmlTexto">* La primera  debe presionar click en el boton <img id="imgini" src='<%=BASEURL%>/images/botones/iconos/menos1.gif' width="12" height="12"> del item que desea eliminar y aparecerá un mensaje como se ilustra en la <strong>Imagen10</strong>.</p>
                <p class="ayudaHtmlTexto">*  La segunda es de forma automatica presiona click en le link de &quot;Items de Ingreso&quot; como se muestra en la <strong>Imagen1</strong> luego deseleccione las facturas que  desea eliminar de los items como lo ilustra la <strong>Imagen5</strong>.</p>
                <p class="ayudaHtmlTexto">NOTA: si elimina un item que fu&eacute; ingresado el registro del item se anular&aacute; y se reversa el valor de la factura.</p></td>
            </tr>
            <tr>
              <td  class="ayudaHtmlTexto"><div align="center">
                <p><IMG src="<%=BASEIMG%>mensajeEliminado.JPG" border=0 ></p>
                <p><strong>Imagen10</strong></p>
              </div></td>			  
            </tr> 
			<tr>
              <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
                <p>Para grabar los items de ingreso debe precionar click sobre el boton <img src='<%=BASEURL%>/images/botones/aceptar.gif' name='Buscar'> y si el valor total de los items es superior al valor de consignacion, el sistema le mostrar&aacute; el siguiente mensaje como se ilustra en la <strong>Imagen11</strong>.</p></td>
            </tr>
            <tr>
              <td  class="ayudaHtmlTexto"><div align="center">
                <p><IMG src="<%=BASEIMG%>mensajeErrorIngreso.JPG" border=0 ></p>
                <p><strong>Imagen11</strong></p>
              </div></td>
            </tr>
			<tr>
              <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
                <p>Si el valor total de los items es inferior o iqual al valor de consignacion, el sistema le mostrar&aacute; el siguiente mensaje como se ilustra en la <strong>Imagen12</strong>.</p></td>
            </tr>
            <tr>
              <td  class="ayudaHtmlTexto"><div align="center">
                <p><IMG src="<%=BASEIMG%>mensajeRegistroExitoso.JPG" border=0 ></p>
                <p><strong>Imagen12</strong></p>
              </div></td>
            </tr>			
			<tr>
              <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
                <p>Para grabar los items de ingreso temporalmente debe precionar click sobre el boton <img src='<%=BASEURL%>/images/botones/iconos/guardar.gif' name='sss' width="25">, el sistema le mostrar&aacute; el siguiente mensaje como se ilustra en la <strong>Imagen12</strong>.</p></td>
            </tr>
      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
