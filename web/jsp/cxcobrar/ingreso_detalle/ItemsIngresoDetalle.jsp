<!--
- Autor : Ing. Jose de la rosa
- Date  : 10 de Mayo del 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja los items de los ingresos
--%>

<%-- Declaracion de librerias--%>
<%@page session="true"%> 
<%@page import="java.util.*" %>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<% 	// objeto de cabecera
	Ingreso_detalle cabecera = model.ingreso_detalleService.getCabecera ();
	String mensaje = (String) request.getAttribute("mensaje");   
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
 	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<script><%if(request.getParameter ("fin")!=null){
			if( cabecera.getPagina().equals("buscar") ){%>
				parent.opener.location="<%=CONTROLLER%>?estado=Ingreso&accion=Buscar&evento=1&mostrar=no&numero=<%=cabecera.getNumero_ingreso()%>&tipodoc=<%=cabecera.getTipo_documento()%>";
			<%}else{
				if( !cabecera.getPagina().equals("ingresar") ){%>
					parent.opener.location.reload();
				<%}
			}
		}%></script>
<html>
<head>
<title>Ingreso Detalles</title>
<link href="<%= BASEURL %>/css/estilostspFactura.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
 <script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
 <script type='text/javascript' src="<%=BASEURL%>/js/Ingreso_detalle.js"></script> 
 <script type='text/javascript' src="<%=BASEURL%>/js/general.js"></script> 
 <script type='text/javascript' src="<%=BASEURL%>/js/posbancaria.js"></script> 
</head> 
<body onLoad="redimensionar(); maximizar();
				<%if(request.getParameter ("abrir_detalles")!=null){%>
					abrirPaginaItems('<%=BASEURL%>');
				<%}
				if(request.getParameter ("abrir_ajuste")!=null){%>
					abrirPaginaAjuste('<%=BASEURL%>');
				<%}%>
			" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ingreso Detalles Insertar"/>
</div> 

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%
	String distrito = (String) session.getAttribute ("Distrito");	
	String moneda_local = model.ingreso_detalleService.monedaLocal(distrito);
	String descripcion = "", fecha_factura = "", facturas = "", moneda_factura = "";
	int x=0;
	double valor_factura = 0, valor_factura_me = 0, impuesto_rfte = 0, impuesto_rica = 0, valor_saldo_me = 0;
	String cod_rfte = "", cod_rica = "", money = "", cuenta = "";
	if( model.ingreso_detalleService.getCabecera ()!=null ){
		// objeto de la factura
		Ingreso_detalle factura = model.ingreso_detalleService.getFactura ();
		cod_rfte = cabecera.getCodigo_retefuente();
		// vector de items
		Vector a = model.ingreso_detalleService.getVecIngreso_detalle ();
		if ( a != null){
			for( x=0; x < a.size(); x++){
				Ingreso_detalle item0 = (Ingreso_detalle)a.elementAt(x);
				if( !item0.getMoneda_factura().equals( cabecera.getMoneda () ) )
					money = item0.getMoneda_factura();
			}
		}
   %>
<form name="form1" id="form1" method="post" action="<%=CONTROLLER%>?estado=Ingreso_detalle&accion=Search" >   
<table width="68%"  align="center">
	<tr><td>
		<FIELDSET>
		<legend><span class="letraresaltada">Nota</span></legend>
		<table width="100%"  border="0" cellpadding="0" cellspacing="0" class="informacion">
			<tr>
			  <td nowrap>&nbsp;&nbsp;Los valores de saldos e impuestos que se encuentran en la factura estan convertidos en la moneda del Ingreso.</td>
			</tr>
		</table>
	</FIELDSET>
	</td></tr>
</table>
<br>
<table width="1100" border="2" align="center">
    <tr>
      <td width="100%" height="60">
        <table width="100%">
          <tr>
            <td width="50%" height="22" align="left" class="subtitulo1">&nbsp;Información del Ingreso</td>
            <td width="50%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
          </tr>
        </table>
        <table width="100%">
          <tr height="20">
            <td width="12%" class="filaFactura">Cliente</td>
            <td class="letra"><%=cabecera.getCliente()%> - <%=cabecera.getNom_cliente ()%></td>
            <td class="letra"><span class="filaFactura">Nro Ingreso </span></td>
            <td class="letra"><%=cabecera.getNumero_ingreso ()%></td>
            <td class="filaFactura">Fecha Creaci&oacute;n</td>
            <td class="letra"><%=cabecera.getCreation_date()!=null?cabecera.getCreation_date().substring (0,10):""%></td>
            <td width="15%" class="filaFactura">Nit del Cliente </td>
            <td width="14%" class="letra"><%=cabecera.getNit_cliente ()%></td>
          </tr>
          <tr height="20">
            <td class="filaFactura">Valor Consignaci&oacute;n</td>
            <td width="21%" class="letra"><%= (cabecera.getMoneda ().equals("DOL"))?UtilFinanzas.customFormat2(cabecera.getValor_ingreso_me ()):UtilFinanzas.customFormat(cabecera.getValor_ingreso_me ())%></td>
            <td width="10%" class="filaFactura">Moneda Ingreso </td>
            <td width="8%" class="letra"><%=cabecera.getMoneda ()%>
                <input name="mon_ingreso" id="mon_ingreso" type="hidden" value="<%=cabecera.getMoneda ()%>"></td>
            <td width="12%" class="filaFactura">Fecha Consignaci&oacute;n</td>
            <td width="8%" class="letra"><%=cabecera.getFecha_consignacion ()%></td>
            <td class="filaFactura">Total Acumulado Facturas</td>
            <td class="letra"><input name="c_valor_total" type="text"  class="filaresaltada" id="c_valor_total" style='width:95%;text-align:right; border:0;' value='<%= cabecera.getValor_total() == 0 ? "" : ( (cabecera.getMoneda ().equals("DOL"))?UtilFinanzas.customFormat2(cabecera.getValor_total()):UtilFinanzas.customFormat(cabecera.getValor_total()) )%>' size="15" maxlength="15" readonly></td>
          </tr>
        </table></td>
    </tr>
  </table>
<table width="1100" border="2" align="center">
      <tr>
        <td >
          <table width="100%" align="center"  >
            <tr>
              <td width="50%" align="left" class="subtitulo1">&nbsp;Items del Ingreso</td>
              <td colspan="2"align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
            </tr>
			<tr class="filaFactura">
				<td align="center"><span class="Simulacion_Hiper" style="cursor:hand " onClick="ajustarItems('<%=CONTROLLER%>','3','<%= a!=null?String.valueOf(a.size()):"0" %>');">Ajustar Ingreso</span></td>
				<td width="30%" align="center">Valor Tasa <input name="vlr_tasa" class="textboxFactura" type="text" id="vlr_tasa" onblur=" if( this.value !='' ){ obtenerSaldoTasa(<%=a.size()%>); }else{ alert('El campo no puede estar vacio');this.focus(); }" value="<%=cabecera.getValor_tasa ()!=0? UtilFinanzas.customFormat("#,###.##########", cabecera.getValor_tasa (),10 ):""%>" size="15" maxlength="15" <%=cabecera.getValor_tasa ()!=0?"":"disabled"%>>
				<%=money%></td>
			    <td width="20%" align="center"><span class="Simulacion_Hiper" style="cursor:hand " onClick="llenarDatosAlmacenados('<%=CONTROLLER%>','3','<%= a!=null?String.valueOf(a.size()):"0" %>');">Agregar Items</span>
				</td>
			</tr>
          </table>

            <table id="detalle" width="100%" >
              <tr  id="fila1" class="tblTituloFactura">
                <td align="center">Item<br><br>
				<a onClick="eliminarTodos();"  id="borrarIng" style="cursor:hand" ><img id="imgini" src='<%=BASEURL%>/images/botones/iconos/eliminar.gif'  ></a></td>
                <td align="center">
					<div align="center">
					  <table align="center" class="tblTituloFactura" width="100%">
					    <tr class="tblTituloFactura">
						  <td width="9%">Factura Nro</td>
						  <td colspan="5">Descripción</td>
						  <td width="9%">Fecha Fac.</td>
						  <td width="12%">Vlr Factura MF.</td>
						  <td width="12%">Vlr Saldo MF. </td>
					      <td width="12%">Vlr Saldo MI. </td>
					    </tr>	  
					    <tr align="center" class="tblTituloFactura">
						  <td>Otros Ing.</td>
						  <td width="11%">Nro Cuenta </td>
						  <td width="6%">Tipo</td>
						  <td width="11%">Auxiliar</td>
						  <td width="8%">Cod Rfte</td>
						  <td width="10%">Valor Rfte</td>
						  <td>Cod Rica</td>
						  <td>Valor Rica</td>
						  <td>Vlr Abono</td>
					      <td>Nuevo Saldo</td>
					    </tr>
					  </table>
			    </div></td>
              </tr>
              <% 
				if ( a != null){
					for( x=0; x < a.size(); x++){
						Ingreso_detalle item = (Ingreso_detalle)a.elementAt(x);
						descripcion = item.getDescripcion_factura();
						fecha_factura = item.getFecha_factura();
						valor_factura = item.getValor_factura();
						valor_factura_me = item.getValor_factura_me();
						facturas = item.getFactura();
						cuenta = item.getCuenta();
						moneda_factura = item.getMoneda_factura();
						cod_rfte = item.getCodigo_retefuente();
						cod_rica = item.getCodigo_reteica();
						impuesto_rfte = cabecera.getPorcentaje_rfte();
						impuesto_rica = item.getPorcentaje_rica();
						valor_saldo_me = item.getValor_saldo_factura_me();
					%>
					  <tr class="<%=(x%2!=0)?"filaFactura":"filagrisFac"%>" id="filaItem<%=x%>" nowrap  bordercolor="#D1DCEB" >
						<td class="bordereporte"> 
						<a id='n_i"+maxfila+"' ><%=String.valueOf((x+1))%></a>
						<a id="insert_item<%=x%>" style="cursor:hand" ><img src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width="12" height="12" onClick="enviarFacturas('<%=CONTROLLER%>','<%=a.size()%>','<%=a.size()%>');" > </a> 
						<a onClick="eliminarDatos('<%=CONTROLLER%>','3','<%=a.size()%>','<%=x%>');"  id="borrarI<%=x%>" style="cursor:hand" ><img id="imgini" src='<%=BASEURL%>/images/botones/iconos/menos1.gif'   width="12" height="12" ></a></td>
						<td> 
						<table width="100%" border="0" cellpadding="0" cellspacing="0" class="<%=(x%2!=0)?"fila":"filagris"%>">
							  <tr align="center" class="bordereporte">
								<td>
									<table width='100%'>
									  <tr>
										<td width='9%'><div align="center">
										  <input name='c_factura<%=x%>' type='text' class='textboxFactura' id='c_factura<%=x%>' value="<%=facturas%>" size="10" maxlength="10"  readonly>
										  </div></td>
										<td colspan='5' ><input name='c_descripcion<%=x%>' type='text' class='textboxFactura' id='c_descripcion<%=x%>' value='<%=descripcion%>' style='width:470' readonly></td>
										<td width="9%" ><input name='c_fecha_factura<%=x%>' type='text' class='textboxFactura' id='c_fecha_factura<%=x%>' value='<%=fecha_factura%>' size='10' readonly></td>
										<td width='12%' class="<%=(x%2!=0)?"filaFactura":"filagrisFac"%>"><input name='c_valor_factura<%=x%>' type='text' class='textboxFactura' id='c_valor_factura<%=x%>' style="text-align:right" value='<%=(moneda_factura.equals("DOL"))?UtilFinanzas.customFormat2(item.getValor_factura_me()):UtilFinanzas.customFormat(item.getValor_factura_me())%>' size='15' maxlength="15" readonly>&nbsp;<%=item.getMoneda_factura()%></td>
										<td width='12%' class="<%=(x%2!=0)?"filaFactura":"filagrisFac"%>"><input name='c_saldo_factura_ori<%=x%>' type='text' class='textboxFactura' id='c_saldo_factura_ori<%=x%>' style="text-align:right" value='<%=(moneda_factura.equals("DOL"))?UtilFinanzas.customFormat2(item.getValor_saldo_factura_me()):UtilFinanzas.customFormat(item.getValor_saldo_factura_me())%>' size='15' maxlength="15" readonly>&nbsp;<%=item.getMoneda_factura()%></td>
									    <td width='12%' class="<%=(x%2!=0)?"filaFactura":"filagrisFac"%>"><input name='c_valor_saldo<%=x%>' type='text' class='textboxFactura' id='c_valor_saldo<%=x%>' style="text-align:right" value='<%=(cabecera.getMoneda ().equals("DOL"))?UtilFinanzas.customFormat2(item.getValor_saldo_factura()):UtilFinanzas.customFormat(item.getValor_saldo_factura())%>' size='15' maxlength="15" readonly>&nbsp;<%=cabecera.getMoneda ()%></td>
									  </tr>	  
									  <tr align='center'>
										<td align='center' class="<%=(x%2!=0)?"filaFactura":"filagrisFac"%>"><a href="javascript: void(0);" onClick="window.open('<%=CONTROLLER%>?estado=Ingreso_detalle&accion=Update&c_factura=<%=facturas%>&opcion=6','','status=no,scrollbars=no,width=800,height=500,resizable=yes');"  id="Mostrar<%=x%>" style="cursor:hand" >Otros</a></td>
										<td width='11%'><input type="text" class='textboxFactura' id='cuenta<%=x%>' name='cuenta<%=x%>' value="<%=cuenta%>" size='15' maxlength="25" readonly></td>
										<td width='6%' id="combotipo<%=x%>"><select name='tipo<%=x%>' class='textboxFactura' id='tipo<%=x%>' onChange="form1.auxiliar<%=x%>.value='';" >
                                          <option value=''> </option>
                                          <%if(item.getTipos() != null ){
												for(int j = 0; j<item.getTipos().size(); j++){
													TablaGen tipo = (TablaGen) item.getTipos().get(j); %>
                                          <option value='<%=tipo.getTable_code()%>' <%=(tipo.getTable_code().equals(item.getTipo_aux()) )? "selected" : "" %> ><%=tipo.getTable_code()%></option>
                                          <%}
											  }else{%>
                                          <option value=""></option>
                                          <%}%>
                                        </select></td>
										<td width='11%' class="<%=(x%2!=0)?"filaFactura":"filagrisFac"%>"><input name='auxiliar<%=x%>' class="textboxFactura" type='text' id='auxiliar<%=x%>'  size="11" maxlength='25' value ="<%=item.getAuxiliar()%>" onBlur="validarAuxiliar(<%=x%>);">
                                          <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name="imagenAux<%=x%>" width="15" height="15"  id="imagenAux<%=x%>" style="cursor:hand"  onClick="buscarAuxiliar('<%=BASEURL%>','<%=x%>',form1.cuenta<%=x%>.value, form1.tipo<%=x%>.value);"  ></td>
										<td width='8%' class="<%=(x%2!=0)?"filaFactura":"filagrisFac"%>"><input name='c_cod_rfte<%=x%>' type='text' class='textboxFactura' id='c_cod_rfte<%=x%>' value='<%=cod_rfte%>' size='6' maxlength='6' readonly>
                                          <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name="imagenRfte<%=x%>" width="15" height="15"  id="imagenRfte<%=x%>" style="cursor:hand" title="Buscar Rfte"  onClick="buscarTipoImpuesto('RFTECL','<%=BASEURL%>','<%=x%>',form1.c_valor_saldo<%=x%>.value,'<%=a.size()%>','');" ></td>
										<td width='10%'><input name='c_valor_rfte<%=x%>' type='text' class='textboxFactura' id='c_valor_rfte<%=x%>' size='15' maxlength='15' value='<%=item.getValor_retefuente()==0?"":(cabecera.getMoneda ().equals("DOL"))?UtilFinanzas.customFormat2(item.getValor_retefuente()):UtilFinanzas.customFormat(item.getValor_retefuente())%>' 
														<%=cabecera.getMoneda ().equals("DOL")?" onChange='formatoDolar(this,2);'" : " onChange='this.value = Math.round( parseFloat( sinformato( this.value ) ) );formatear(this);'"%> onKeyPress="soloDigitos(event, 'decOK');"  
														onblur=" if( c_cod_rfte<%=x%>.value !='' ){ 
															obtenerSaldoTotal(<%=a.size()%>);<%=(cabecera.getMoneda ().equals("DOL"))?"this.value=formato(this.value, 2);":"onChange=formatear(this)"%>
															if( this.value == '' || this.value == 0){ 
																c_cod_rfte<%=x%>.value = ''; 
																this.value = '';
															} 
														}else{ 
															if( this.value != '' && this.value != 0){ 
																this.value = '';
																alert('Debe seleccionar un codigo de impuesto');this.focus(); 
															}
															else{
																this.value = '';
															} 
														}" onfocus='this.value=sinformato(this.value);' style="text-align:right"></td>
										<td class="<%=(x%2!=0)?"filaFactura":"filagrisFac"%>"><input name='c_cod_rica<%=x%>' type='text' class='textboxFactura' id='c_cod_rica<%=x%>' value='<%=cod_rica%>' size='6' maxlength='6' readonly>
									    <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name="imagenRica<%=x%>" width="15" height="15"  id="imagenRica<%=x%>" style="cursor:hand" title="Buscar Rica"  onClick="buscarTipoImpuesto('RICACL','<%=BASEURL%>','<%=x%>',form1.c_valor_saldo<%=x%>.value,'<%=a.size()%>','<%=item.getAgencia_facturacion ()%>');" ></td>
										<td class="<%=(x%2!=0)?"filaFactura":"filagrisFac"%>"><input name='c_valor_rica<%=x%>' type='text' class='textboxFactura' id='c_valor_rica<%=x%>'  value='<%=item.getValor_reteica()==0?"":( (cabecera.getMoneda ().equals("DOL"))?UtilFinanzas.customFormat2(item.getValor_reteica()):UtilFinanzas.customFormat(item.getValor_reteica()) )%>' size='15' maxlength='15' 
													<%=cabecera.getMoneda ().equals("DOL")?" onChange='formatoDolar(this,2);'" : " onChange='this.value = Math.round( parseFloat( sinformato( this.value ) ) );formatear(this);'"%> onKeyPress="soloDigitos(event, 'decOK');"  
													onblur  =" if( c_cod_rica<%=x%>.value !='' ){ 
														obtenerSaldoTotal(<%=a.size()%>);<%=(cabecera.getMoneda ().equals("DOL"))?"this.value=formato(this.value, 2);":"onChange=formatear(this)"%> 
														if( this.value == '' || this.value == 0){ 
															c_cod_rica<%=x%>.value = '';
															this.value = '';
														}
													}else{ 
														if( this.value != '' && this.value != 0){ 
															this.value = '';
															alert('Debe seleccionar un codigo de impuesto');this.focus(); 
														}
														else{
															this.value = '';
														} 
													}" onfocus='this.value=sinformato(this.value);' style="text-align:right"></td>
										<td ><input name='c_valor_abono<%=x%>' type='text' class='textboxFactura' id='c_valor_abono<%=x%>' style="text-align:right" 
												<%=cabecera.getMoneda ().equals("DOL")?" onChange='formatoDolar(this,2);'" : " onChange='this.value = Math.round( parseFloat( sinformato( this.value ) ) );formatear(this);'"%>
												onKeyPress="soloDigitos(event, 'decOK');" value='<%=(cabecera.getMoneda ().equals("DOL"))?UtilFinanzas.customFormat2(item.getValor_abono()):UtilFinanzas.customFormat(item.getValor_abono())%>' size='15' maxlength="15" onfocus='this.value=sinformato(this.value);' onblur  ='if( parseFloat(this.value) > parseFloat( sinformato(form1.c_valor_saldo<%=x%>.value) ) ){alert("No se puede colocar un valor mayor al valor del saldo de la factura");this.focus(); }else{obtenerSaldoTotal(<%=a.size()%>);<%=(cabecera.getMoneda ().equals("DOL"))?"this.value=formato(sinformato(this.value), 2);":"onChange=formatear(this)"%>}'></td>
									    <td ><input name='c_valor_total_factura<%=x%>' type='text' class='textboxFactura' id='c_valor_total_factura<%=x%>' style="text-align:right"  onKeyPress="soloDigitos(event,'decOK')" value='<%=item.getValor_total_factura()==0?"":(cabecera.getMoneda ().equals("DOL"))?UtilFinanzas.customFormat2(item.getValor_total_factura()):UtilFinanzas.customFormat(item.getValor_total_factura())%>' size='15' maxlength="15" readonly></td>
									  </tr>
									</table>						
								</td>
							  </tr>
						  </table></td>
					  </tr>
						<input name="mon_factura<%=x%>" id="mon_factura<%=x%>" type="hidden" value="<%=moneda_factura%>">
						<input name="vlr_fac<%=x%>" id="vlr_fac<%=x%>" type="hidden" value='<%=item.getValor_factura()==0?"":(cabecera.getMoneda ().equals("DOL"))?UtilFinanzas.customFormat2(item.getValor_factura()):UtilFinanzas.customFormat(item.getValor_factura())%>'>
              <%  }
			  }%>
			  <tr id="filaItem<%=x%>" nowrap   class='<%=(x%2!=0)?"filaFactura":"filagrisFac"%>'  bordercolor="#D1DCEB">
                <td width="6%" nowrap class="bordereporte">
					<a id='n_i"+maxfila+"' ><%=(x+1)%></a>
                  <a  id="insert_item<%=x%>" style="cursor:hand"  ><img src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width="12" height="12" onClick="enviarFacturas('<%=CONTROLLER%>','<%=x%>','<%=a.size()%>');"></a>
				   <a onClick="alert('No se puede eliminar este registro')" id="borrarI<%=x%>" style="cursor:hand" ><img src='<%=BASEURL%>/images/botones/iconos/menos1.gif'   width="12" height="12"  > </a>
			    </td>
                <td> 
				<table width="100%" border="0" cellpadding="0" cellspacing="0" class="<%=(x%2!=0)?"filaFactura":"filagrisFac"%>">
                      <tr align="center" class="bordereporte">
                        <td>
							<table width="100%">
							  <tr>
								<td width="9%"><div align="center">
								  <input name="c_factura<%=x%>" type="text" class="textboxFactura" id="c_factura<%=x%>"  size="10" maxlength="10" onKeyUp="cargarFacturas('<%=CONTROLLER%>','<%=x%>','<%=a.size()%>')">
							    </div></td>
								<td colspan="5"><input name="c_descripcion<%=x%>" type="text" class="textboxFactura" id="c_descripcion<%=x%>"  style='width:470' readonly></td>
								<td width="9%"><input name="c_fecha_factura<%=x%>" type="text" class="textboxFactura" id="c_fecha_factura<%=x%>"  size="10" readonly></td>
								<td width="13%"><input name='c_valor_fac<%=x%>' type='text' class='textboxFactura' id='c_valor_fac<%=x%>' style="text-align:right" size='15' maxlength="15" readonly>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
								<td width="11%"><input name="c_saldo_factura_ori<%=x%>" type="text" class="textboxFactura" id="c_saldo_factura_ori<%=x%>" style="text-align:right" size="15" maxlength="15" readonly>&nbsp;&nbsp;&nbsp;&nbsp;</td>
							    <td width="12%" align="left"><input name='c_valor_fac<%=x%>' type='text' class='textboxFactura' id='c_valor_fac<%=x%>' style="text-align:right" size='15' maxlength="15" readonly>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							  </tr>	  
							  <tr align="center">
								<td align="center"></td>
								<td width="11%" rowspan="2"><input type="text" class='textboxFactura' id='c_cuenta<%=x%>' name='c_cuenta<%=x%>' size='15' maxlength="25"></td>
								<td width="6%" rowspan="2"><select name='tipo<%=x%>' class='textboxFactura' id='tipo<%=x%>'>
															<option value=""></option>
       						    </select></td>
								<td width="11%" rowspan="2"><input name='auxiliar<%=x%>' class="textboxFactura" type='text' id='auxiliar<%=x%>'  size="11" maxlength='25'>
                                  <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name="imagenAux<%=x%>" width="15" height="15"  id="imagenAux<%=x%>" style="cursor:hand"  onClick="alert('Debe primero cargar la factura');"  ></td>
								<td width="8%" rowspan="2"><input name="c_cod_rfte<%=x%>" type="text" class="textboxFactura" id="c_cod_rfte<%=x%>" size="6" maxlength="6" readonly>
                                  <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name="imagenRfte<%=x%>" width="15" height="15"  id="imagenRfte<%=x%>" style="cursor:hand" title="Buscar Rfte"  onClick="alert('Debe primero cargar la factura');" ></td>
								<td width="10%" rowspan="2"><input name="c_valor_rfte<%=x%>" type="text" class="textboxFactura" id="c_valor_rfte<%=x%>" size="15" maxlength="15" onKeyPress="soloDigitos2(event,'decOK')" style="text-align:right"></td>
								<td rowspan="2"><input name="c_cod_rica<%=x%>" type="text" class="textboxFactura" id="c_cod_rica<%=x%>" size="6" maxlength="6" readonly>
							    <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name="imagenRica<%=x%>" width="15" height="15"  id="imagenRica<%=x%>" style="cursor:hand" title="Buscar Rica"  onClick="alert('Debe primero cargar la factura');" ></td>
								<td rowspan="2"><input name="c_valor_rica<%=x%>" type="text" class="textboxFactura" id="c_valor_rica<%=x%>"  size="15" maxlength="15" onKeyPress="soloDigitos(event,'decOK')" style="text-align:right"></td>
								<td rowspan="2" ><input name='c_valor_abono<%=x%>' type='text' class='textboxFactura' id='c_valor_abono<%=x%>' style="text-align:right" size='15' maxlength="15" readonly></td>
							    <td rowspan="2"><input name="c_valor_total_factura<%=x%>" type="text" class="textboxFactura" id="c_valor_total_factura<%=x%>" style="text-align:right" onKeyPress="soloDigitos(event,'decOK')"  size="15" maxlength="15" readonly></td>
							  </tr>
							  <tr align="center">
							    <td align="center">&nbsp;</td>
						      </tr>
							</table>						
						</td>
                      </tr>
                  </table></td>
              </tr>
            </table>
        </td>
      </tr>
    </table>  
  </form >
	<br> 
	<div align="center">
		<%if(session.getAttribute("fin")==null){%>
			<img src='<%=BASEURL%>/images/botones/iconos/guardar.gif' name='Buscar' width="25" height="25" align="absmiddle" style='cursor:hand' title='Guardar...'  onclick="<%if(x>0){%>guardarDatos('<%=CONTROLLER%>','3','<%=a.size()%>');<%}else{%>alert('Debe tener como minimo un item');<%}%>" >&nbsp;
			<img src="<%=BASEURL%>/images/botones/aceptar.gif"  height="21" name="imgaceptar"  onMouseOver="botonOver(this);" onClick="<%if(x>0){%>guardarDatos('<%=CONTROLLER%>','4','<%=a.size()%>');<%}else{%>alert('Debe tener como minimo un item');<%}%>" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
		<%}%>
		<img src="<%=BASEURL%>/images/botones/imprimir.gif"  height="21" name="imgaceptar"  onMouseOver="botonOver(this);" onClick="guardarDatos('<%=CONTROLLER%>','imp','<%=a.size()%>');" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
		<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
	</div>
  <%
  }
  if(mensaje!=null){%>
	<script> 
		alert('<%= mensaje %>');
	</script>
<% } 
if( request.getAttribute("vista") != null ){%>
	<br>
	<table border="2" align="center">
		<tr>
			<td>
				<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
					<tr>
						<td width="300" align="center" class="mensajes"><%=(String)request.getAttribute("vista")%></td>
						<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
						<td width="58">&nbsp;</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
<%}%>
</div>
<%=datos[1]%>
</body>
</html>
<script>
    function enviar(url){
         var a = "<iframe name='ejecutor' style='visibility:hidden'  src='<%= CONTROLLER %>" + url + "'> ";
         aa.innerHTML = a;
    }

</script>
<font id='aa'></font>