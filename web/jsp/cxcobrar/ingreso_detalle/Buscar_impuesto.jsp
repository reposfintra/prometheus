<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de realizar y mostrar busqueda de los tipos de impuestos
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head><title>Tipo de Impuestos</title>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/Ingreso_detalle.js"></script> 
 
<script>
function asignarImpuestoS( id, codigo, porcentaje, tipo, tam ){
	if(tipo=='RICACL'){
		//asigno codigo a la reteica
		var cod_rica = parent.opener.document.getElementById("c_cod_rica"+id);
		cod_rica.value = codigo;
		
		//asigno valor a la reteica
		var valor_saldo = parent.opener.document.getElementById("vlr_fac"+id);
		var valor_rica  = parent.opener.document.getElementById("c_valor_rica"+id);
		
		var valor_rica_ante = 0;
		if( valor_rica.value != "" )
			valor_rica_ante = sinformato(valor_rica.value);
		
		var moneda = parent.opener.document.getElementById("mon_ingreso");
		if(moneda.value == 'DOL'){
			valor_rica.value = formato( sinformato(valor_saldo.value) * (porcentaje/100), 2 );
		}
		else{
			valor_rica.value = parseInt( sinformato(valor_saldo.value) * (porcentaje/100) );
			formatear(valor_rica);
		}
		
		var total = 0;
		var valor_total = parent.opener.document.getElementById("c_valor_total");
		for (j=0; j<tam; j++){
			//asigno valor a la reteica
			var valor_saldo1 = parent.opener.document.getElementById("c_valor_abono"+j);
			var valor_rica1  = parent.opener.document.getElementById("c_valor_rica"+j);
			var valor_rfte1  = parent.opener.document.getElementById("c_valor_rfte"+j);
			var valor_total_factura = parent.opener.document.getElementById("c_valor_total_factura"+j);
			var saldo_smlf = parent.opener.document.getElementById("c_valor_saldo"+j);
			
			//validación si existen retefuente y reteica
			if( valor_rfte1.value != '' && valor_rfte1.value.length>0 )
				var vlr_rfte1 = sinformato(valor_rfte1.value);
			else
				var vlr_rfte1 = 0;
			if( valor_rica1.value != '' && valor_rica1.value.length>0 )
				var vlr_rica1 = sinformato(valor_rica1.value);
			else
				var vlr_rica1 = 0;
				
			if( valor_saldo1.value != '' && valor_saldo1.value.length>0 ){
				var vlr_total = 0;
				if( tam-1 == j ){
					var val1 = parseFloat(sinformato(valor_saldo1.value)) + ( parseFloat(sinformato(valor_rica.value)) - parseFloat(valor_rica_ante) );
					if( val1 > parseFloat( sinformato(saldo_smlf.value)  ) ){
						val1 = parseFloat( sinformato(saldo_smlf.value)  );
					}
					if(moneda.value == 'DOL'){
						valor_saldo1.value = formato( val1, 2);
					}else{
						valor_saldo1.value =  val1;
						formatear(valor_saldo1);
					}
					
					
				}
				vlr_total = sinformato(valor_saldo1.value);
			}
			else
				var vlr_total = 0;
			
			//2007-02-16
			valor_total_factura.value = parseFloat( sinformato(saldo_smlf.value)  ) - parseFloat( vlr_total );
			if( parseFloat( valor_total_factura.value)==0 )
				valor_total_factura.value = "";
			else{
				if(moneda.value == 'DOL')
					valor_total_factura.value = formato(valor_total_factura.value, 2);
				else
					formatear(valor_total_factura);
			}
				
			total = parseFloat(total) + parseFloat(vlr_total) - ( parseFloat(vlr_rfte1) + parseFloat(vlr_rica1) );
		}
		if(moneda.value == 'DOL'){
			valor_total.value = formato( total, 2 );
		}
		else{
			valor_total.value = formato( total, 2 );
			formatear(valor_total);
		}
				
	}
	if(tipo=='RFTECL'){
		//asigno codigo a la retefuente
		var cod_rfte    = parent.opener.document.getElementById("c_cod_rfte"+id);
		cod_rfte.value = codigo;
		//asigno valor a la retefuente
		var valor_saldo = parent.opener.document.getElementById("vlr_fac"+id);
		var valor_rfte  = parent.opener.document.getElementById("c_valor_rfte"+id);
		
		var valor_rfte_ante = 0;
		if( valor_rfte.value != "" )
			valor_rfte_ante = sinformato(valor_rfte.value);
		
		var moneda = parent.opener.document.getElementById("mon_ingreso");
		if(moneda.value == 'DOL'){
			valor_rfte.value = formato( sinformato(valor_saldo.value) * (porcentaje/100), 2 );		
		}else{
			valor_rfte.value =  parseInt( sinformato(valor_saldo.value) * (porcentaje/100) );	
			formatear(valor_rfte);
		}
		
		var total = 0;
		var valor_total = parent.opener.document.getElementById("c_valor_total");
		for (j=0; j<tam; j++){
			//asigno valor a la reteica
			var valor_saldo1 = parent.opener.document.getElementById("c_valor_abono"+j);
			var valor_rica1  = parent.opener.document.getElementById("c_valor_rica"+j);
			var valor_rfte1  = parent.opener.document.getElementById("c_valor_rfte"+j);
			var valor_total_factura = parent.opener.document.getElementById("c_valor_total_factura"+j);
			var saldo_smlf = parent.opener.document.getElementById("c_valor_saldo"+j);
		
			
			//validación si existen retefuente y reteica
			if( valor_rfte1.value != '' && valor_rfte1.value.length>0 )
				var vlr_rfte1 = sinformato(valor_rfte1.value);
			else
				var vlr_rfte1 = 0;
			if( valor_rica1.value != '' && valor_rica1.value.length>0 )
				var vlr_rica1 = sinformato(valor_rica1.value);
			else
				var vlr_rica1 = 0;
			
			if( valor_saldo1.value != '' && valor_saldo1.value.length>0 ){
				var vlr_total = 0;
				if( tam-1 == j ){
					var val1 = parseFloat(sinformato(valor_saldo1.value)) + ( parseFloat(sinformato(valor_rfte.value)) - parseFloat(valor_rfte_ante) );
					if( val1 > parseFloat( sinformato(saldo_smlf.value)  ) ){
						val1 = parseFloat( sinformato(saldo_smlf.value)  );
					}
					if(moneda.value == 'DOL'){
						valor_saldo1.value = formato( val1, 2);
					}else{
						valor_saldo1.value = val1;
						formatear(valor_saldo1);
					}
				}
				vlr_total = sinformato(valor_saldo1.value);
			}
			else
				var vlr_total = 0;
			
			//2007-02-16
			valor_total_factura.value = parseFloat( sinformato(saldo_smlf.value)  ) - parseFloat( vlr_total );
			if( parseFloat( valor_total_factura.value)==0 )
				valor_total_factura.value = "";
			else{
				if(moneda.value == 'DOL')
					valor_total_factura.value = formato(valor_total_factura.value, 2);
				else
					formatear(valor_total_factura);
			}
			
			total = parseFloat(total) + parseFloat(vlr_total) - ( parseFloat(vlr_rfte1) + parseFloat(vlr_rica1) );
		}
		if(moneda.value == 'DOL'){
			valor_total.value = formato( total, 2 );
		}
		else{
			valor_total.value = formato( total, 2 );
			formatear(valor_total);
		}
	}
  	parent.close();
}
</script>
</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Impuestos "/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
    String t_i = ""+request.getParameter("tipo");
    String idI = ""+request.getParameter("id");
    String valor = ""+request.getParameter("valor").replaceAll (",","");
    String tam = ""+request.getParameter("tam");
    String distrito = (String) session.getAttribute ("Distrito");
    String agencia = ""+request.getParameter("agencia");
	//fecha actual
    java.util.Date utilDate = new java.util.Date(); 
    long lnMilisegundos = utilDate.getTime();
    java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(lnMilisegundos);
	
	//vector de tipos de impuestos
	Vector vImpuestos= model.TimpuestoSvc.buscarImpuestoCliente(t_i,""+sqlTimestamp,distrito, agencia);
%>
<form name="forma1" action="" method="post">      
<table width="450" border="2" align="center">
  <tr>
    <td>
<table width="100%" align="center" > 
  <tr>
    <td width="50%" height="24"  class="subtitulo1"><p align="left">Impuestos Tipo <%=t_i%></p></td>
    <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
  </tr>
</table>
<table width="100%"  align="center" >
 <tr class="tblTitulo" id="titulos" >
    <td width="15%" align="center">Codigo </td>
    <td width="20%" align="center">Impuesto</td>
	<td width="65%" align="center">Descripción</td>
 </tr>
<% 
    String fecha =""+sqlTimestamp; 
    for(int i=0; i< vImpuestos.size();i++){
        Tipo_impuesto tipo=(Tipo_impuesto)vImpuestos.elementAt(i);
%>  
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand" title="Tipo de impuestos" onClick="asignarImpuestoS('<%=idI%>','<%=tipo.getCodigo_impuesto()%>', '<%=tipo.getPorcentaje1()%>','<%=t_i%>','<%=tam%>');">

        <td align="center"><%=tipo.getCodigo_impuesto()%></td>
        <td align="center"><%=tipo.getPorcentaje1()+ " % "%></td>
		<td align="center"><%=tipo.getDescripcion()%></td>
        </tr>
<%
    }
%>  
</table>
</td>
</tr>
</table>
</form>
<center><img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir..." onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></center>
</div>
</body>
</html>
