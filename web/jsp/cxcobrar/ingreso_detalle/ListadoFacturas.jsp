<!--
- Autor : Ing. Jose de la rosa
- Date  : 3 de agosto del 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que lista los ingresos
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 2;
	LinkedList lista = model.ingresoService.getListadoingreso();
%>
<html>
    <head>
        <title>Listado Facturas en Ingresos</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
		<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
		<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
        
    </head>
    <body >
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listado Ingresos"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <%
            if( lista != null && lista.size() > 0 ){%>
                <table width="98%" border="1" align="center">
                <tr>
                    <td>  
						<table width="100%" align="center">
							  <tr>
								<td width="373" class="subtitulo1">&nbsp;Listar Facturas en Ingresos</td>
								<td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
							  </tr>
						</table>                      
                        <table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4">
                            <tr class="tblTitulo">                            
								<td width="6%" align="center">Num Ingreso </td>
								<td width="19%" align="center">Cliente</td>
								<td width="15%" align="center">Banco</td>
								<td width="15%" align="center">Sucursal</td>
								<td width="10%" align="center">Fecha Ingreso </td>
								<td width="14%" align="center">Valor Abono </td>
								<td width="6%" align="center">Nro Factura </td>	
                            </tr> 
							<pg:pager
								items="<%=lista.size()%>"
								index="<%= index %>"
								maxPageItems="<%= maxPageItems %>"
								maxIndexPages="<%= maxIndexPages %>"
								isOffset="<%= true %>"
								export="offset,currentPageNumber=pageNumber"
								scope="request">
							<%String pagina = "";
							  for (int i = offset.intValue(), l = Math.min(i + maxPageItems, lista.size()); i < l; i++)
							  {
								 Ingreso ing = (Ingreso) lista.get(i);
								 %>
								<pg:item>
									<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand">
										<td align="center" class="bordereporte"><%=ing.getNum_ingreso()%></td>
										<td align="center" class="bordereporte"><%=ing.getNomCliente()%></td>
										<td align="center" class="bordereporte"><%=ing.getBranch_code()%></td>						
										<td align="center" class="bordereporte"><%=ing.getBank_account_no()%></td>	
										<td align="center" class="bordereporte"><%=ing.getFecha_consignacion()%></td>	
										<td align="center" class="bordereporte"><%=(ing.getCodmoneda().equals("DOL"))?UtilFinanzas.customFormat2(ing.getVlr_ingreso_me()):UtilFinanzas.customFormat(ing.getVlr_ingreso_me())%> <%=ing.getCodmoneda()%></td>
										<td align="center" class="bordereporte"><%=ing.getConcepto()%></td>	
									</tr>				
							  </pg:item>
						<%  }%>
							<tr class="pie">
								<td td height="20" colspan="10" nowrap align="center"><pg:index>
										<jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
								</pg:index>
								</td>
							</tr>
							</pg:pager>        
                            </table>
                    </td>
                </tr>
                </table>                
            <%}else{%> 
			<br>   
            <table border="2" align="center">
                <tr>
                    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr> 
                    <td width="350" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="58">&nbsp;</td>
                </tr>
                </table></td>
                </tr>
            </table>                   
            <%}%>
			<br>
			 <table width="98%" border="0" align="center">
				<tr>
				  <td>
				  <img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" title="Volver" name="buscar"  onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
				  </td>
				</tr>
			</table>
        </div>
    </body>
</html>
