<!--  
	 - Author(s)       :      Jose de la rosa
	 - Description     :      AYUDA DESCRIPTIVA - Items Ingreso Detalle
	 - Date            :      08/06/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>AYUDA DESCRIPTIVA - Ingreso Detalle</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Items Ingreso</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Ingresar Items Ingreso</td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2">INFORMACION DE LOS ITEMS DE INGRESO </td>
        </tr>
        <tr>
          <td  class="fila">Valor Tasa</td>
          <td  class="ayudaHtmlTexto">Campo para modificar el el valor de la tasa. Este campo es de m&aacute;ximo 15 caracteres. </td>
        </tr>		
        <tr>
          <td  class="fila">Nro Factura</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el codigo de la factura a buscar. Este campo es de m&aacute;ximo 10 caracteres. </td>
        </tr>		
        <tr>
          <td  class="fila">Descripción</td>
          <td  class="ayudaHtmlTexto">Campo donde se muestra la descripcion de la factura.</td>
        </tr>
        <tr>
          <td  class="fila">Fecha fac.</td>
          <td  class="ayudaHtmlTexto">Campo donde se muestra la fecha de la factura.</td>
        </tr>			
        <tr>
          <td  class="fila">Vlr Factura MF</td>
          <td  class="ayudaHtmlTexto">Campo donde se muestra el valor de la factura en la moneda de factura.</td>
        </tr>
        <tr>
          <td  class="fila">Vlr Saldo MF</td>
          <td  class="ayudaHtmlTexto">Campo donde se muestra el valor del saldo de la factura en la moneda de factura.</td>
        </tr>
        <tr>
          <td  class="fila">Vlr Saldo MI</td>
          <td  class="ayudaHtmlTexto">Campo donde se muestra el valor del saldo de la factura en la moneda del ingreso.</td>
        </tr>
        <tr>
          <td  class="fila">Cod Rfte</td>
          <td  class="ayudaHtmlTexto">Campo donde se muestra el codigo del porcentaje de retefuente escogido.</td>
        </tr>
        <tr>
          <td  class="fila">Valor Rfte</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el valor del impuesto que desea aplicar al item de la factura. Este campo es de m&aacute;ximo 15 caracteres. </td>
        </tr>
        <tr>
          <td  class="fila">Cod Rica</td>
          <td  class="ayudaHtmlTexto">Campo donde se muestra el codigo del porcentaje de reteica escogido.</td>
        </tr>
		
        <tr>
          <td  class="fila">Valor Rica</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el valor del impuesto que desea aplicar al item de la factura. Este campo es de m&aacute;ximo 15 caracteres. </td>
        </tr>
        <tr>
          <td  class="fila">Valor Abono</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el valor del abono que desea aplicar al item de la factura. Este campo es de m&aacute;ximo 15 caracteres. </td>
        </tr>
        <tr>
          <td  class="fila">Nuevo Saldo</td>
          <td  class="ayudaHtmlTexto">Campo donde se muestra el bnuevo valor saldo de la factura.</td>
        </tr>	
        <tr>
          <td class="fila">Link Items de Ingreso</td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para buscar los impuestos de reteica.</td>
        </tr>								
		<tr>
          <td class="fila">Bot&oacute;n <img id="imgini" src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width="12" height="12" > </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para ingresar item de factura.</td>
        </tr>
        <tr>
          <td class="fila">Bot&oacute;n <img id="imgini" src='<%=BASEURL%>/images/botones/iconos/menos1.gif'   width="12" height="12" > </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para eliminar un item de factura.</td>
        </tr>
        <tr>
          <td class="fila">El primer Bot&oacute;n <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif' name="imagenRfte" width="15" height="15"> </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para buscar los impuestos de retefuente.</td>
        </tr>
        <tr>
          <td class="fila">El segundo Bot&oacute;n <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif' name="imagenRfte" width="15" height="15"> </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para buscar los impuestos de reteica.</td>
        </tr>		
        <tr>
          <td width="149" class="fila">Bot&oacute;n Salir</td>
          <td width="525"  class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista 'Ingresar Items Ingreso' y volver a la vista de los detalles.</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</body>
</html>
