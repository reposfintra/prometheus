<!--
- Autor : Ing. Jose de la rosa
- Date  : 18 Mayo de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite buscar las facturas de los clientes
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.util.*"%>
<%
    Vector vec = model.ingreso_detalleService.getVecFactura ();
	Vector vecdet = model.ingreso_detalleService.getVecIngreso_detalle ();
	Ingreso_detalle cabecera = model.ingreso_detalleService.getCabecera ();
	String mensaje = (String) request.getAttribute("mensaje"); %>
<html>
<head><title>Facturacion de Cliente</title></head>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src='<%=BASEURL%>/js/validacionesImpresionPlanilla.js'></script>
<script>
	function cambiacolor(element){
		var tr = element.parentNode.parentNode;
		tr.className = (element.checked?'filaazul':'filagris');
	}
	function seleccionartodo(size,check){
			if(check == true){
				for (i=0;i<size;i++){
					var checkbox="checkbox"+i;
					var textfield="textfield"+i
					document.getElementById(checkbox).checked=true;
					cambiacolor(document.getElementById(checkbox));
				}    
		    }else{
			    for (i=0;i<size;i++){
					var checkbox="checkbox"+i;
					var textfield="textfield"+i
					document.getElementById(checkbox).checked=false;
					cambiacolor(document.getElementById(checkbox));
				}  
			}
			
	}
</script>
<body onLoad="<%if(request.getParameter ("cerrar")!=null){%>parent.opener.location='<%=BASEURL%>/jsp/cxcobrar/ingreso_detalle/ItemsIngresoDetalle.jsp';window.close();<%}%>redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ingreso Detalles"/>
</div> 

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
	<%if(vec.size()>0){%>
	<form action="<%=CONTROLLER%>?estado=Ingreso_detalle&accion=Search&opcion=5" method="post" name="form1" >
	<table width="900" border="2" align="center">
		<tr>
			<td>
				<TABLE width="100%" align="center"  class="tablaInferior">
					<tr class="fila">
						<td width="50%" class="subtitulo1">&nbsp;Informacion del Cliente </td>
						<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
					</tr>
				</table>
				<table width="100%" height="30">
					<tr class="fila">
					  <td width="7%"><strong>Cliente</strong></td>
						<td width="68%"><%=cabecera.getCliente()%> - <%=cabecera.getNom_cliente ()%></td>
						<td width="4%"><strong>Nit</strong></td>
						<td width="21%"><%=cabecera.getNit_cliente ()%></td>
					</tr>
				</table>
			</td>
		</tr>
  </table>
		<table width="900" border="2" align="center">
			<tr>
				<td>
					<TABLE width="100%" align="center"  class="tablaInferior">
				        <tr class="fila">
							<td width="50%" class="subtitulo1">&nbsp;Facturas del Cliente </td>
							<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
						</tr>
					  
						<tr>
						  <td colspan="2" align="center">
						  <table width="100%" border="1" bordercolor="#999999">
                            <tr class="tblTitulo">
                              <td width="9%" nowrap align="center">Negocio</td>
                              <td width="9%" nowrap align="center">Cuota</td>
                              <td width="9%" nowrap align="center">Facturas</td>
                              <td width="9%" nowrap align="center">Fecha</td>
                              <td width="47%" align="center">Descripcion</td>
                              <td width="12%" align="center">Vlr Factura </td>
                              <td width="12%" align="center">Vlr Saldo </td>
                              <td width="7%" align="center">Moneda</td>
                              <td width="4%" align="center"><input type="checkbox" name="checkboxall" id ="checkboxall" value="checkbox" onClick="seleccionartodo('<%=vec.size()%>',this.checked);"></td>
                            </tr>
                            <tr>
                              
                                  <%
			
			boolean sw = false;
			for(int i=0; i<vec.size();i++){
			Ingreso_detalle ingreso = (Ingreso_detalle)vec.get(i);
				sw = false;
				if(vecdet!=null){
					for(int j = 0; j < vecdet.size(); j++){
						Ingreso_detalle ing =(Ingreso_detalle)vecdet.get(j);
						
						if( ingreso.getFactura().equals( ing.getFactura() ) ){
						   sw = true;
						}	
						
					}
				}
	 			 %>
				  
					<tr class="<%=sw==false?"filagris":"filaazul"%>">
                                          <td width="9%"  class="bordereporte" align="left">&nbsp;<%=ingreso.getNegaso()%></td>
                                          <td width="9%"  class="bordereporte" align="center">&nbsp;<%=ingreso.getNumCuota()%></td> 
					  <td width="9%"  class="bordereporte" align="left">&nbsp;<%=ingreso.getFactura()%></td>
					  <td width="12%"  class="bordereporte" align="center"><%=ingreso.getFecha_factura()%></td>
					  <td width="47%"  class="bordereporte">&nbsp;<%=ingreso.getDescripcion_factura()%></td>
					  <td width="12%"  class="bordereporte" align="right"><%=(ingreso.getMoneda_factura().equals("DOL"))?UtilFinanzas.customFormat2(ingreso.getValor_factura_me()):UtilFinanzas.customFormat(ingreso.getValor_factura_me())%></td>
					  <td width="12%"  class="bordereporte" align="right"><%=(ingreso.getMoneda_factura().equals("DOL"))?UtilFinanzas.customFormat2(ingreso.getValor_saldo_factura_me()):UtilFinanzas.customFormat(ingreso.getValor_saldo_factura_me())%></td>
					  <td width="7%"  class="bordereporte" align="center"><%=ingreso.getMoneda_factura()%></td>
					  <td width="4%"  class="bordereporte" align="center"><input type="checkbox" name="checkbox" id="checkbox<%=i%>" value="<%=i%>" onClick="cambiacolor(this)" <%=sw==true?"checked":""%>></td>
					</tr><%
			}%>
                          </table>
						  </td>
					  </tr>
			  </TABLE>			  
			  </td>
			</tr>
  </table>
		<p align="center">
		<img src="<%=BASEURL%>/images/botones/aceptar.gif"  height="21" name="imgaceptar"  onMouseOver="botonOver(this);" onClick="this.disabled=true; form1.submit();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
		<img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close(); this.disabled=true;" onMouseOut="botonOut(this);" style="cursor:hand">
		</p>
		<input name="vlr_tasa" id="vlr_tasa" value="<%=cabecera.getValor_tasa ()%>" <%=cabecera.getValor_tasa ()!=0?"":"disabled"%> type="hidden">
		</form>
	<%--Mensaje de informacion--%>
	<%}else{%>
		<br>
		<table border="2" align="center">
			<tr>
				<td>
					<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						<tr>
							<td width="229" align="center" class="mensajes">No hay facturas para el cliente <%=cabecera.getNom_cliente ()%></td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<p align="center">
			<img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close(); this.disabled=true;" onMouseOut="botonOut(this);" style="cursor:hand">
		</p>		
	<%}
	if(mensaje!=null){%>
		<script> 
			alert('<%= mensaje %>');
		</script>
	<% } %>
</div>
</body>
</html>
