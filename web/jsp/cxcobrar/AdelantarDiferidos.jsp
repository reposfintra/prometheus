<%-- 
    Document   : adelantarDiferidos
    Created on : 26/04/2018, 11:30:01 AM
    Author     : jbermudez
--%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%
    response.addHeader("Cache-Control", "no-cache");
    response.addHeader("Pragma", "No-cache");
    response.addDateHeader("Expires", 0);
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/adelantarDiferidos.js"></script> 
        <title>Adelantar/Anular Diferidos</title>
        <style>
            html {
                font-size: 16px;
            }
            
            input[type="checkbox"] {
                width: auto;
                margin: 0;
            }

            #negocio, #unegocio {
                padding-left: 0;
                padding-right: 0;
            }

            .button-wrapper {
                display: inline-block;
                margin-top: 10px;
                margin-left: 15px;
            }

            button {
                width: 116px;
            }
        </style>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Adelantar/Anular Diferidos"/>
        </div>
    <center>
        <div id="tablita" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 300px; padding: 8px;">
            <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                <label class="titulotablita"><b>ADELANTAR/ANULAR DIFERIDOS</b></label>
            </div>
            <table id="tablainterna" style="height: 53px;">
                <br>
                <tr>
                    <td>
                        <label>Tipo diferido</label>
                    </td>
                    <td>
                        <select id="tipodif" type="text">
                            <option value="">...</option>
                        </select>
                    </td>
                    <td style="text-align: center;">
                        <label>Negocio</label>
                    </td>
                    <td>
                        <input id="negocio" type="text" style="width: 100%;">
                    </td>                    
                </tr>
                <tr>
                    <td colspan="2">
                        <div class="button-wrapper">
                            <center>
                                <button type="button" id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"> 
                                    <span class="ui-button-text">Buscar</span>
                                </button>  
                            </center>
                        </div>
                    </td>
                    <td colspan="2">
                        <div class="button-wrapper">
                            <center>
                                <button type="button" id="salir" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" > 
                                    <span class="ui-button-text">Salir</span>
                                </button>  
                            </center>
                        </div>
                    </td>
                </tr>
            </table>
        </div>

        <div id="tabla_diferidosWrapper" style="position: relative;top: 140px;">
            <table id="tabla_diferidos" ></table>
            <div id="pager"></div>
        </div>
        <div id="dialogLoading" style="display:none;">
            <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
            <center>
                <img src="./images/cargandoCM.gif"/>
            </center>
        </div>
    </center>
    <div id="dialogMsg" title="Mensaje" style="display:none;">
        <p style="font-size: 12.5px;text-align:justify;" id="msj" >Cargando la información</p>
    </div> 
</body>
<script>

</script>
</html>
