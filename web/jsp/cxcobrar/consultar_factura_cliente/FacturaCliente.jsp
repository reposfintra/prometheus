<!--
- Autor : LREALES
- Date  : 30 de noviembre de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de mostrar el resultado de la busqueda de la factura de un cliente,
                para la actualizacion de la fecha probable de pago.
--%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%-- Declaracion de librerias--%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.util.*"%>
<% Util u = new Util(); %>
<html>
    <head>        
        <title>Factura de un Cliente para Actualizar la Fecha Probable de Pago</title>
		<script src='<%=BASEURL%>/js/date-picker.js'></script>
		<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
		<script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script> 
		<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
		<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
   <body onResize="redimensionar()" onLoad="redimensionar()">
	<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
	 <jsp:include page="/toptsp.jsp?encabezado=Consultar Factura Cliente"/>
	</div>
	<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
	<%String msg = request.getParameter("msg");
		if ( msg!=null && !msg.equals("") ){%>	
								
		  <table border="2" align="center">
			<tr>
				<td>
					<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						<tr>
							<td width="229" align="center" class="mensajes"><%=msg%></td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		  </table>		  
		        <br>
				
				<center>
		        <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/cxcobrar/consultar_factura_cliente/ConsultarFacturaCliente.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
 	  <input name="salir" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
	  </center>
	  
	  <%}
	  
		else {
		
          Vector datos = model.consultarModificarFacturaClienteService.getVectorReporte(); 
					
		if( datos != null && datos.size() > 0 ){
		
			
		%>		
		
		<form name="formulario" method="post" action="<%=CONTROLLER%>?estado=ConsultarModificar&accion=FacturaCliente&hacer=2">
		
		<table width="80%" border="2" align="center" id="tabla1" >
    <tr>
        <td width="" >
           <table width="100%" height="50%" align="center">
              <tr>
			  
			  <%
			  BeanGeneral info = (BeanGeneral) datos.elementAt(0);
			  %>
                <td class="subtitulo1">Informaci&oacute;n de la Factura # <%=(info.getValor_02()!=null)?info.getValor_02():""%> .</td>
				<input name="documento" type="hidden" id="documento" value="<%=info.getValor_02()%>">
                <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
          </table>
         <div class="scroll" style=" overflow:auto ; WIDTH: 100%; HEIGHT:100%; " >
         <table id="tabla3" width="100%">
            <tr id="titulos">
              <td width="18%" align="left"  nowrap class="fila" abbr="">&nbsp;C&oacute;digo del Cliente :&nbsp;</td>
              <td width="17%" align="left"  nowrap class="letra" abbr="">&nbsp;<%=(info.getValor_04()!=null)?info.getValor_04():""%>&nbsp;</td>
              <td width="20%" align="left"  nowrap class="fila" abbr="">&nbsp;Nombre del Cliente:&nbsp;</td>
              <td width="13%" align="left"  nowrap class="letra" abbr="">&nbsp;<%=(info.getValor_05()!=null)?info.getValor_05():""%>&nbsp;</td>
              <td width="19%" align="left"  nowrap class="fila" abbr="">&nbsp;Nit del Cliente:&nbsp;</td>
              <td width="13%" align="left"  nowrap class="letra" abbr="">&nbsp;<%=(info.getValor_03()!=null)?info.getValor_03():""%>&nbsp;</td>
           </tr>
            <tr id="titulos">
              <td width="18%" align="left"  nowrap class="fila" abbr="">&nbsp;Estado de la Factura :&nbsp;</td>
              <td align="left"  nowrap class="letra" abbr="">&nbsp;<%=(info.getValor_01()!=null)?info.getValor_01():""%>&nbsp;</td>
              <td align="left"  nowrap class="fila" abbr="">&nbsp;Agencia de Facturaci&oacute;n:&nbsp;</td>
              <td align="left"  nowrap class="letra" abbr="">&nbsp;<%=(info.getValor_17()!=null)?info.getValor_17():""%>&nbsp;</td>
              <td width="19%" align="left"  nowrap class="fila" abbr="">&nbsp;Agencia de Cobro:&nbsp;</td>
              <td align="left"  nowrap class="letra" abbr="">&nbsp;<%=(info.getValor_18()!=null)?info.getValor_18():""%>&nbsp;</td>
           </tr>
            <tr id="titulos">
              <td align="left"  nowrap class="fila" abbr="">&nbsp;Cantidad de Items:&nbsp;</td>
              <td align="left"  nowrap class="letra" abbr="">&nbsp;<%=(info.getValor_16()!=null)?info.getValor_16():""%>&nbsp;</td>
              <td align="left"  nowrap class="fila" abbr="">&nbsp;Moneda:&nbsp;</td>
              <td align="left"  nowrap class="letra" abbr="">&nbsp;<%=(info.getValor_15()!=null)?info.getValor_15():""%>&nbsp;</td>
              <td align="left"  nowrap class="fila" abbr="">&nbsp;Valor de la Tasa:&nbsp;</td>
              <td align="left"  nowrap class="letra" abbr="">&nbsp;<%=(info.getValor_14()!=null)?info.getValor_14():""%>&nbsp;</td>
            </tr>
            <tr id="titulos">
              <td align="left"  nowrap class="fila" abbr="">&nbsp;Valor de la Factura :&nbsp;</td>
              <td align="left"  nowrap class="letra" abbr="">&nbsp;$&nbsp;<%=(info.getValor_11()!=null)?info.getValor_11():""%>&nbsp;</td>
              <td align="left"  nowrap class="fila" abbr="">&nbsp;Valor del Abono:&nbsp;</td>
              <td align="left"  nowrap class="letra" abbr="">&nbsp;$&nbsp;<%=(info.getValor_12()!=null)?info.getValor_12():""%>&nbsp;</td>
              <td align="left"  nowrap class="fila" abbr="">&nbsp;Valor del Saldo:&nbsp;</td>
              <td align="left"  nowrap class="letra" abbr="">&nbsp;$&nbsp;<%=(info.getValor_13()!=null)?info.getValor_13():""%>&nbsp;</td>
            </tr>
            <tr id="titulos">
              <td align="left"  nowrap class="fila" abbr="">&nbsp;Fecha de Facturaci&oacute;n:&nbsp;</td>
              <td align="left"  nowrap class="letra" abbr="">&nbsp;<%=(info.getValor_06()!=null)?info.getValor_06():""%>&nbsp;</td>
              <td align="left"  nowrap class="fila" abbr="">&nbsp;Fecha de Vencimiento:&nbsp;</td>
              <td align="left"  nowrap class="letra" abbr="">&nbsp;<%=(info.getValor_07()!=null)?info.getValor_07():""%>&nbsp;</td>
              <td align="left"  nowrap class="fila" abbr="">&nbsp;Fecha de Ultimo Pago :&nbsp;</td>
              <td align="left"  nowrap class="letra" abbr="">&nbsp;<%=(info.getValor_08()!=null)?info.getValor_08():""%>&nbsp;</td>
           </tr>
            <tr id="titulos">
              <td align="left"  nowrap class="fila" abbr="">&nbsp;Descripci&oacute;n:&nbsp;</td>
              <td colspan="5" align="left"  nowrap class="letra" abbr="">&nbsp;<%=(info.getValor_09()!=null)?info.getValor_09():""%>&nbsp;</td>
            </tr>
            <tr id="titulos">
              <td align="left"  nowrap class="fila" abbr="">&nbsp;Observaciones:&nbsp;</td>
              <td colspan="5" align="left"  nowrap class="letra" abbr="">&nbsp;<%=(info.getValor_10()!=null)?info.getValor_10():""%>&nbsp;</td>
            </tr>    
			
			<tr id="titulos">
              <td align="left"  nowrap class="fila" abbr="">&nbsp;Remesas:</td>
              <td colspan="5" align="left"  nowrap class="letra" abbr="">&nbsp;<%=(info.getValor_20()!=null)?info.getValor_20():""%></td>
			  </tr>
			<tr id="titulos">
              <td colspan="2" align="left"  nowrap class="fila" abbr="">&nbsp;Fecha Probable de Pago:&nbsp;&nbsp;</td>
              <td colspan="4" align="left"  nowrap class="letra" abbr="">
			  <% if ( info.getValor_01()!=null && info.getValor_01().equals("Pendiente") ) { %>	
			  		
			  		<input name="fecha" type="text" class="textbox" id="fecha" value="<%=info.getValor_19()%>" size="10" readonly>
              		<span class="Letras"> 
						<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" name="obligatorio"> 
						<img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fecha);return false;" HIDEFOCUS>
					</span>
					
			   <% } else { %>
			   
					&nbsp;<%=info.getValor_19()%>&nbsp;
					
			   <% } %>
			   
			  </td>
            </tr>   
			    
          </table> 
		  </div>
        </td>
    </tr>
	
 </table> 
				
				<br>
		    
              <center>
        
        <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/cxcobrar/consultar_factura_cliente/ConsultarFacturaCliente.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
	    <% if ( info.getValor_01()!=null && info.getValor_01().equals("Pendiente") ) { %>	
		&nbsp;
		<input name="modificar2" src="<%=BASEURL%>/images/botones/modificar.gif" type="image" class="boton" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
		&nbsp;
		<% } else { %>
		&nbsp;		
		<% } %>
		 <input name="salir2" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
              </center>
			  
		    </form>	
	  <%}	  
	  }
	  %>	  
	    </div>
		<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
</body>
		
</html>