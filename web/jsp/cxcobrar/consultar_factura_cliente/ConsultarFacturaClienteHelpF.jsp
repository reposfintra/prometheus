<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA FUNCIONAL - Consultar la Factura de un Cliente
	 - Date            :      01/12/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>Funcionalidad para Consultar la Factura de un Cliente</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="/../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/cxcobrar/consultar_factura_cliente/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE REPORTE WEB</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa Consultar la Factura de un Cliente</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">En la siguiente pantalla  se digita el n&uacute;mero de la factura de un cliente, que deseas consultar. </p></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=220 src="<%=BASEIMG%>1.JPG" width=738 border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si en la pantalla le sale el siguiente mensaje de error, es por que usted no ha digitado el n&uacute;mero de la factura a buscar. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=126 src="<%=BASEIMG%>2.JPG" width=359 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si le sale el siguiente mensaje de error, es por que la factura digitada por usted, no existe en nuestra Base de Datos.</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=135 src="<%=BASEIMG%>3.JPG" width=728 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>En la pantalla le mostrara la informacion correspondiente a la factura ingresada.<br>
      Si el programa no le permite modificar la fecha probable de pago, es por que esta factura esta ANULADA. </p>    </td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=278 src="<%=BASEIMG%>4.JPG" width=804 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si le sale la siguiente informacion y  el programa no le permite modificar la fecha probable de pago, es por que esta factura esta PAGADA. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=278 src="<%=BASEIMG%>5.JPG" width=786 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">Despues de haber realizado la busqueda satisfactoriamente, se mostrara la informaci&oacute;n de la factura correspondiente y si esta PENDIENTE, permitir&aacute; que usted pueda modificar la fecha probable de pago. </p></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=287 src="<%=BASEIMG%>6.JPG" width=793 border=0 v:shapes="_x0000_i1052"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">El sistema confirma que la modificaci&oacute;n se realiz&oacute; exitosamente. </p></td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=134 src="<%=BASEIMG%>7.JPG" width=788 border=0 v:shapes="_x0000_i1054"></div></td>
</tr>
        </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>