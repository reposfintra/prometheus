<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA DESCRIPTIVA - Consultar la Factura de un Cliente
	 - Date            :      01/12/2006  
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
    <title>Descripcion de los campos a ingresar al Consultar la Factura de un Cliente</title>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <link href="/../../../css/estilostsp.css" rel="stylesheet" type="text/css">  
    
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
<body> 
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ayuda Descriptiva - Consultar Factura Cliente"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:90%; z-index:0; left: 0px; top: 100px;"> 
   <p>&nbsp;</p>
   <table width="65%" border="2" align="center" id="tabla1" >
        <td>
         <table width="100%" height="50%" align="center">
              <tr>
                <td class="subtitulo1">Factura Cliente </td>
                <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
         </table>
         <table width="100%" borderColor="#999999" bgcolor="#F7F5F4">
           <tr>
             <td colspan="2" class="tblTitulo"><strong>Consultar la Factura de un Cliente- Pantalla Preliminar</strong></td>
           </tr>
           <tr>
             <td colspan="2" class="subtitulo"><strong>INFORMACION</strong></td>
           </tr>
           <tr>
             <td width="40%" class="fila">'N&uacute;mero de la Factura' </td>
             <td width="60%"><span class="ayudaHtmlTexto">Campo donde digito el c&oacute;digo de la factura del cliente que voy a consultar.</span></td>
           </tr>
           <tr>
             <td class="fila">Bot&oacute;n Buscar </td>
             <td><span class="ayudaHtmlTexto">Bot&oacute;n para realizar el procedimiento de b&uacute;squeda de la factura ingresada.</span></td>
           </tr>
           <tr>
             <td class="fila">Bot&oacute;n Salir</td>
             <td><span class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista 'Consultar la Factura de un Cliente' y volver a la vista del men&uacute;.</span></td>
           </tr>
         </table></td>
  </table>
		<p></p>
	<center>
	<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	</center>
</div>  
</body>
</html>