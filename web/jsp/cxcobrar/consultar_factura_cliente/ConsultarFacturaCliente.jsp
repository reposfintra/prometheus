<!--
- Autor : LREALES
- Date  : 30 de noviembre de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de realizar la busqueda de la factura de un cliente,
                para la actualizacion de la fecha probable de pago.
--%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@taglib uri="/WEB-INF/tlds/tagsAgencia.tld" prefix="ag"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%boolean bandera = false; %>
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
    <head>

        <title>Consultar la Factura de un Cliente para Actualizar la Fecha Probable de Pago</title>        
		<script src='<%=BASEURL%>/js/date-picker.js'></script>
		<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
		<script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script> 
        <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
		<script>		
			function validar( form ){
			
				if ( form.documento.value == '' ) {
					alert( 'Debe digitar el numero de la factura para poder continuar..' );
					return false;
				}
				
			}
		</script>
    </head>
   <body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Consultar Factura Cliente"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

        <form name='formulario' method='POST' id="formulario" action="<%=CONTROLLER%>?estado=ConsultarModificar&accion=FacturaCliente&hacer=1" onSubmit="return validar(this);">
            <table width="60%"  border="2" align="center">
              <tr>
                <td><table width="100%"  border="0" class="tablaInferior">
                  
				  <tr>
                   <td class="subtitulo1" width="50%">Buscar la Factura de un Cliente</td>
          			<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                  </tr>
                  
                  <tr class="fila">
                    <td align="left">&nbsp;N&uacute;mero de la Factura </td>
                    <td align="left">
					
                      <input name="documento" id="documento" type="text" class="textbox" size="10" maxlength="10">
                      <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" name="obligatorio">					  					
					  
					</td>
                  </tr>  
				  				                 
                </table></td>
              </tr>
            </table>
			<br>
            <table align="center">
              <tr>
                <td colspan="2" nowrap align="center">
					<input name="buscar" src="<%=BASEURL%>/images/botones/buscar.gif"  style = "cursor:hand" align="middle" type="image" id="buscar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">                  
&nbsp;
                    <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" align="absmiddle" style="cursor:hand" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
              </tr>
            </table>
        </form > 
	</div>   
    <%=datos[1]%>
	</body>
</html>