<%--
    Document   : Modulo de Compras y Servicios.
    Created on : 10/02/2017, 11:00:00 AM
    Author     : hcuello
--%>

<%@page session="true" %>
<%@ page import="com.tsp.operation.model.beans.Usuario"%>
<%@ page import="com.tsp.operation.model.beans.CmbGenericoBeans"%>
<%@ page import="com.tsp.operation.model.services.RequisicionesService"%>
<%@ page import="com.tsp.operation.model.beans.RequisicionesListadoBeans"%>

<%@ page import="com.tsp.operation.model.beans.ComprasProcesoBeans"%>
<%@ page import="com.tsp.operation.model.services.ComprasProcesoService"%>

<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="java.text.SimpleDateFormat" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>


<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String NombreUsuario = usuario.getNombre();
    String NmLogin = usuario.getLogin();

    Date dNow = new Date( );

    SimpleDateFormat nd = new SimpleDateFormat ("yyyy-MM-dd");
    String aniocte = nd.format(dNow);

    String Descripcion = "";

    String id_sol = request.getParameter("id_sol") != "" ? request.getParameter("id_sol") : "";
    String accione = request.getParameter("ACCIONE") != "" ? request.getParameter("ACCIONE") : "";
    String item = request.getParameter("ITEM") != "" ? request.getParameter("ITEM") : "";

    String TipoSolicitud = request.getParameter("estado") != "" ? request.getParameter("estado") : "";
    String Proyecto = request.getParameter("mes") != "" ? request.getParameter("mes") : "";

    ComprasProcesoService LstService = new ComprasProcesoService(usuario.getBd());
    ArrayList listaR =  LstService.ComprasProcesoBeans("SQL_LISTADO_INSUMOS",NmLogin,id_sol,accione,item);
%>

<script language="JavaScript1.2">

$(document).ready(function(){
    cargarBodegas();
    var table = $('#tbl_solicitudes').DataTable( {
        autoWidth: false,
        columnDefs: [
            {
                targets: [ 0 ]
                ,visible: false
                //,searchable: false
            },

            {
                targets: [ 8 ]
                ,searchable: false

            }
        ]

        ,fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            if ( aData[1] == "S" ) {
                //$('td', nRow).css('background-color', '#ff3333');
                $('td', nRow).addClass( "EstiloNuevoInsumo" );
                //background: url('./images/select_ad-x.png');
            }else{
                //$('td', nRow).css('background-color', '#eeffee');
                //background: url('./images/select_ad-x.png');
            }

        }


    });

    if ( $('#accion').val() === 'EDIT' ) {
        CargarInfoRequisicion( $('#item').val() );
    }



     $('#cbx_lista_bodegas').change(function () {
     var optionSelected = $( "#cbx_lista_bodegas option:selected" ).text();
     $('#txt_direccion').val(optionSelected);
    });

    function cargarBodegas() {
        $('#cbx_lista_bodegas').html('');        
       cargarBodegaPrincipal();

        $.ajax({
            type: 'POST',
            url: '/fintra/controlleropav?estado=Orden&accion=Compra',
            dataType: 'json',
            async: false,
            data: {
                id_solicitud: $("#id_solicitud").val(),
                opcion: 0
            },
            success: function (json) {
                if (json.error) {
                    mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var i in json) {
                        $('#cbx_lista_bodegas').append('<option value="'+json[i].id+'">' + json[i].direccion + '</option>');
                    } 
                    $('#txt_direccion').val($( "#cbx_lista_bodegas option:selected" ).text());
                } catch (exception) {
                    //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
                }

            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    };

    function cargarBodegaPrincipal() {
        $.ajax({
            type: 'POST',
            url: '/fintra/controlleropav?estado=Orden&accion=Compra',
            dataType: 'json',
            async: false,
            data: {
                opcion: 3
            },
            success: function (json) {
                if (json.error) {
                    //  mensajesDelSistema(json.error, '250', '180');
                    return;
                }
                try {
                    for (var i in json) {
                        $('#cbx_lista_bodegas').append('<option value="'+json[i].id+'">' + json[i].direccion + '</option>');
                    }
                } catch (exception) {
                    //    mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
                }
            }, error: function (xhr, ajaxOptions, thrownError) {
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
    };
});
</script>

<form name="form_nueva_rq" id="form_nueva_rq" method="post" enctype="multipart/form-data" target="iframeUpload">

<input type="hidden" name="id_solicitud" id="id_solicitud" value="<%=id_sol%>" />
<input type="hidden" name="accion" id="accion" value="<%=accione%>" />
<input type="hidden" name="item" id="item" value="<%=item%>" />

<table with='1600' align="center" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF">

    <tr>
        <td id="drag_nueva_requisicion" style="cursor:pointer">
            <div class="k-header">
                <span class="titulo">Solicitudes Compras y Servicios</span>
                <span class="ui-widget-header ui-corner-all titulo_right" onClick="RecargarRequisiciones()">x</span>
            </div>
        </td>
    </tr>

    <tr><td>&nbsp;</td></tr>

    <tr>
        <td>

            <fieldset id="HeadTable">

            <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels" with='1000'>

                <tr>

                    <td width="750" align="center">
                        <table>

                            <tr>
                                <td align="right">TIPO DE SOLICITUD: &nbsp;</td>
                                <td align="left">
                                    <select name="tiposolicitud"  id="tiposolicitud" class="combo_300px" onchange="ReloadGrillaInsumos()">
                                        <OPTION value='0' selected>-- SELECCIONAR --</OPTION>
                                        <OPTION value='1'>ORDEN DE COMPRA</OPTION>
                                        <OPTION value='2'>ORDEN DE SERVICIO</OPTION>
                                    </select>
                                </td>
                            </tr>


                            <tr>
                                <td align="right">DESCRIPCION: &nbsp;</td>
                                <td><textarea name="descripcion" class="textbox_300x100px" id="descripcion"><%=Descripcion %></textarea></td>
                            </tr>



                        </table>
                    </td> <!--PRIMERA MITAD -->

                    <td width="750" align="center">
                        <table>
                            <tr>
                                <td align="right">FECHA: &nbsp;</td>
                                <td><input name="fecha_actual" type="text" class="input_100px" id="fecha_actual" value="<%=aniocte%>" readonly ></td>
                            </tr>
                            <tr>
                                <td align="right">FECHA ENTREGA: &nbsp;</td>
                                <td><input name="fecha_entrega" type="text" class="inputCalendar" id="fecha_entrega" value="<%=aniocte%>" readonly ></td>
                            </tr>
                            <tr>
                                <td align="right">LUGAR DE ENTREGA: &nbsp;</td>
                                <td align="left">
                                    <select id='cbx_lista_bodegas' disabled=true>
                                        <option value="Seleccion">Selecciona bodega...</option>
                                        <option value="1">BODEGA PRINCIPAL - MARISOL - VIA 40 KM 8</option>
                                    </select>
                                    <input id="txt_direccion" type="text" placeholder="Escriba dirección" class="input_120px">
                                </td>
                            </tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr>
                                <td colspan="2" align="right" >
                                    <!-- <button class="btn" onClick="WhatIdo('<%=NmLogin%>')">Pre-Salvado</button> -->
                                    <ul class="tab">
                                        <li><a href="javascript:void(0)" class="tablinks" onclick="WhatIdo('<%=NmLogin%>')"><img src='./images/floppy-32.png' height="20" width="20"> Pre-Salvar</a></li>
                                        <li><a href="javascript:void(0)" class="tablinks" onclick="CatalogoInsumos('<%=item%>', event)"><img src='./images/buttons_add.png' height="20" width="20"> Adicionar Elemento</a></li>
                                        <li><a href="javascript:void(0)" class="tablinks" onclick="RecargarRequisiciones()"><img src='./images/Descartados.png' height="20" width="20"> Cerrar</a></li>
                                    </ul>
                                </td>
                            </tr>
                        </table>
                    </td><!--SEGUNDA MITAD -->
                </tr>

            </table>

            </fieldset>


        </td>
    </tr>


    <tr>
        <td colspan="3">
            <table border="0" align="right" cellpadding="1" cellspacing="1" with='1000'>
                <tr><td width="1500" align="center">&nbsp;</td></tr>
            </table>

        </td>
    </tr>


    <tr>
        <td>
            <fieldset>
                <div id="CapsuleTable" style="width:1450px;">

                    <table border='0' align='center'>
                        <tr>
                            <td width="1500">

                                <table id="tbl_solicitudes" class="table table-striped table-bordered" width="1400" border='0' align='center' > <!-- width="1250" height="18" border='0' align='left' cellpadding="0" cellspacing="0" -->

                                    <thead>
                                        <tr>
                                            <th width='100' align='center'>Filtro INSUMO</th>
                                            <th width='20' align='center'>ADD</th>
                                            <th width='100' align='center'>TIPO INSUMO</th>
                                            <th width='100' align='center'>CODIGO MATERIAL</th>
                                            <th width='600' align='center'>DESCRIPCION</th>
                                            <th width='60' align='center'>U. MEDIDA</th>
                                            <th width='50' align='center'>PRESUPUESTO</th>
                                            <th width='50' align='center'>SOLICITADOS</th>
                                            <th width='50' align='center'>DISPONIBLES</th>
                                            <th width='60' align='center'>SOLICITAR</th>
                                            <th width='60' align='center'>REREFENCIA</th>
                                            <th width='60' align='center'>OBSERVACION</th>
                                            <th width='60' align='center' style="display:none;">SOLICITAR</th>
                                            <th width='60' align='center' style="display:none;">SOLICITAR</th>
                                            <!-- <th width='60' align='center'>Accion</th> -->
                                        </tr>
                                    </thead><%

                                    String src = "";
                                    String alt = "";
                                    String cierre = "";
                                    String prioridad = "";

                                    String TextField = "";
                                    String TextUser = "";
                                    String TextCodMat = "";
                                    Integer Cuenta = 1;

                                    if ( listaR.size() > 0 ) {%>

                                        <tbody><%

                                        String TextReadOnly;

                                        for (int i = 0; i < listaR.size(); i++) {

                                            TextField = "textfield" + Cuenta;
                                            TextUser = "TextUser" + Cuenta;
                                            TextCodMat = "TextCodMat" + Cuenta;

                                            TextReadOnly = "";

                                            ComprasProcesoBeans ListadoContenido = (ComprasProcesoBeans) listaR.get(i); %>

                                            <tr>
                                                <td align="justify"><%=ListadoContenido.getFiltroInsumo()%></td>
                                                <td align="center"><%=ListadoContenido.getInsumoAdicional()%></td>
                                                <td align="justify"><%=ListadoContenido.getTipoInsumo()%></td>
                                                <td align="left"><%=ListadoContenido.getCodigoMaterial()%></td>
                                                <td align="justify"><%=ListadoContenido.getDescripcionInsumo()%></td>
                                                <td align="center"><%=ListadoContenido.getNombreUnidadInsumo()%></td>
                                                <td align="right"><%=ListadoContenido.getInsumosTotal()%></td>
                                                <td align="right"><%=ListadoContenido.getInsumosSolicitados()%></td>
                                                <td align="right"><%=ListadoContenido.getInsumosDisponibles()%></td><%

                                                if ( accione.equals("VISUALIZAR") ) { %>

                                                    <td align="right"><%=ListadoContenido.getSolicitadoTemporal()%></td> <%

                                                }else{

                                                    if ( ListadoContenido.getInsumosDisponibles().equals("0.0000") ) {
                                                        TextReadOnly = "readonly";
                                                    }%>

                                                    <!-- onchange="CalcularDiferencias(event)" -->
                                                    <td align="center"><input type="text" name="CantidadSolicitada" id="CantidadSolicitada" class="input_80px" <%=TextReadOnly%> value="<%=ListadoContenido.getSolicitadoTemporal()%>" onkeypress="return isNumberKey(event)" ></td>
                                                    <td align="center"><input type="text" name="ReferenciaAdicional" id="ReferenciaAdicional" class="input_120px" <%=TextReadOnly%> value="<%=ListadoContenido.getReferenciaExterna()%>"></td>
                                                    <td align="center"><textarea name="ObservacionTxt" class="textbox_120x35px" id="ObservacionTxt"><%=ListadoContenido.getObservacion_material()%></textarea></td>
                                                    <td align="left" style="display:none;"><input type="text" class="input_80px" name="UserResponsable" id="UserResponsable" value="<%=NmLogin%>" ></td>
                                                    <td align="left" style="display:none;"><input type="text" class="input_80px" name="CodigoMaterial" id="CodigoMaterial" value="<%=ListadoContenido.getCodigoMaterial()%>"></td> <%


                                                }%>

                                            </tr><%

                                            Cuenta++;

                                        }

                                    }else{%>

                                        <tr>
                                            <td colspan="11">NO HAY INSUMOS PARA ESTE PROYECTO</td>
                                        </tr><%

                                    }%>

                                    </tbody>

                                </table>
                                <!--x-->



                            </td>
                        </tr>
                    </table>

                </div>
            </fieldset>

        </td>
    </tr>

</table>

</form>
    
