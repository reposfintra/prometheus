<%--
    Document   : Modulo de Compras y Servicios.
    Created on : 10/02/2017, 11:00:00 AM
    Author     : hcuello
--%>

<%@page session="true" %>
<%@ page import="com.tsp.operation.model.beans.Usuario"%>
<%@ page import="com.tsp.operation.model.beans.CmbGenericoBeans"%>
<%@ page import="com.tsp.operation.model.services.RequisicionesService"%>
<%@ page import="com.tsp.operation.model.beans.RequisicionesListadoBeans"%>

<%@ page import="java.text.DecimalFormat"%>

<%@ page import="com.tsp.operation.model.beans.ComprasProcesoBeans"%>
<%@ page import="com.tsp.operation.model.services.ComprasProcesoService"%>

<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="java.text.SimpleDateFormat" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>


<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String NombreUsuario = usuario.getNombre();
    String NmLogin = usuario.getLogin();

    Date dNow = new Date( );

    SimpleDateFormat nd = new SimpleDateFormat ("yyyy-MM-dd");
    String aniocte = nd.format(dNow);

    DecimalFormat formateador = new DecimalFormat("###,###.##");

    String Descripcion = "";

    String id_sol = request.getParameter("id_sol") != "" ? request.getParameter("id_sol") : "";
    String accione = request.getParameter("ACCIONE") != "" ? request.getParameter("ACCIONE") : "";
    String OrdenComp = request.getParameter("ITEM") != "" ? request.getParameter("ITEM") : "";
    String direccion_entrega = request.getParameter("direccion_entrega") != "" ? request.getParameter("direccion_entrega") : "";
    String recepcion_string  = request.getParameter("recepcion") != "" ? request.getParameter("recepcion") : "";
    boolean recepcion = Boolean.parseBoolean(recepcion_string);

    ComprasProcesoService LstService = new ComprasProcesoService(usuario.getBd());
    ArrayList listaOCS =  LstService.GetOrdenCompra("SQL_OCOMPRA", OrdenComp);
    ArrayList listaR =  LstService.GetInsumosDespacho("SQL_INSUMOS_DESPACHO",NmLogin, OrdenComp);

%>

<script language="JavaScript1.2">

$(document).ready(function(){

    var table = $('#tbl_despacho').DataTable( {

        pageLength: 100
        //,dom: '<"top">rt<"bottom" ip><"clear">'
        ,dom: '<"toolbar">frtip'

        ,columnDefs: [
            {
                targets: [ 1 ]
                ,visible: false
                ,searchable: false
            },
            {
                targets: [ 2 ]
                ,visible: false
                ,searchable: false
            }
        ]

        ,fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {

            //console.log('ppppppppppp');
            //console.log(aData[2]);

            if ( aData[0] == "S" ) {
                //$('td', nRow).css('background-color', '#ff3333');
                $('td', nRow).addClass( "EstiloNuevoInsumo" );
                //background: url('./images/select_ad-x.png');
            }else{
                //$('td', nRow).css('background-color', '#eeffee');
                //background: url('./images/select_ad-x.png');
            }

        }

    });
    /*
    $( "#tipo_solicitud" ).prop( "disabled", false );
    $( "#proveedor" ).prop( "disabled", false );
    $( "#descripcion" ).prop( "disabled", false );
    $( "#orden_ocs" ).prop( "disabled", false );
    $( "#fecha_entrega" ).prop( "disabled", false );
    $( "#direccion_entrega" ).prop( "disabled", false );

    $("#tbl_despacho").find("input,button,textarea,select").attr("disabled", false);

    $('#img_guardar_ocs').show();
    $('#img_eliminar_preocs').show();

    $('#cerrar_orden').hide();
    */
});



$(document).on(

    "change",".input_80pxDespacho",function(){

        var totales = 0;

        $('#tbl_despacho tbody tr').each(

            function () {

               //SE PUEDE CAMBIAR EL COLOR EN EL RECORRIDO
               /*
                var dataHolder = $(".data-holder",$(this));
                console.log( dataHolder );

                var UserResponsable = dataHolder.attr('UserResponsable');
                var CodSolicitud = dataHolder.attr('CodSolicitud');
                var CodigoMaterial = dataHolder.attr('CodigoMaterial');
                var CostoCompra = dataHolder.attr('CostoCompra');
                var CantSolicitar = dataHolder.attr('CantSolicitar');
                var TotalRow = dataHolder.attr('TotalRow');*/

                $(this).find("td")[10].children[0].value = formato($(this).find("td")[9].children[0].value * $(this).find("td")[8].children[0].value).moneda;
                totales = totales + ($(this).find("td")[9].children[0].value * $(this).find("td")[8].children[0].value);
                console.log("totales: "+totales);
            }

        );

        document.getElementById("total_despacho").value = formato(totales).moneda;
    }

);

$(document).on(

    "change",".input_80pxDespachoEsp",function(){

        var totales = 0;

        console.log( $(this).parents('tr') );

        var data = $('#tbl_despacho').DataTable().row(    $(this).parents('tr')    ).data();

        var valor_tabla = parseInt(this.value); console.log("valor_tabla: "+valor_tabla);
        var valor_input = parseInt(data[7]); console.log("valor_input: "+valor_input);

        if ( valor_tabla > valor_input ) {
            smoke.signal("<b>SUPERAS EL VALOR PRESUPUESTADO, FAVOR VALIDAR!</b> <br> ",700);
            this.value = '0.0000';
        }


        $('#tbl_despacho tbody tr').each(

            function () {

               //SE PUEDE CAMBIAR EL COLOR EN EL RECORRIDO
               /*
                var dataHolder = $(".data-holder",$(this));
                console.log( dataHolder );

                var UserResponsable = dataHolder.attr('UserResponsable');
                var CodSolicitud = dataHolder.attr('CodSolicitud');
                var CodigoMaterial = dataHolder.attr('CodigoMaterial');
                var CostoCompra = dataHolder.attr('CostoCompra');
                var CantSolicitar = dataHolder.attr('CantSolicitar');
                var TotalRow = dataHolder.attr('TotalRow');*/

                $(this).find("td")[10].children[0].value = formato($(this).find("td")[9].children[0].value * $(this).find("td")[8].children[0].value).moneda;
                totales = totales + ($(this).find("td")[9].children[0].value * $(this).find("td")[8].children[0].value);
                console.log("totales: "+totales);
            }

        );

        document.getElementById("total_despacho").value = formato(totales).moneda;
    }

);


</script>

<form name="form_nueva_rq" id="form_nueva_rq" method="post" enctype="multipart/form-data" target="iframeUpload">

<input type="hidden" name="id_solicitud" id="id_solicitud" value="<%=id_sol%>" />
<input type="hidden" name="OrdenCompDespacho" id="OrdenCompDespacho" value="<%=OrdenComp%>" />



<table with='1500' align="center" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF">

    <tr>
        <td id="drag_nuevo_despacho" style="cursor:pointer">
            <div class="k-header">
                <span class="titulo">Depachos Compras</span>
                <span class="ui-widget-header ui-corner-all titulo_right" onClick="$('#div_nuevo_despacho').fadeOut('slow')">x</span>
            </div>
        </td>
    </tr>

    <tr><td>&nbsp;</td></tr>

    <tr>
        <td><%
            ComprasProcesoBeans OCompra = (ComprasProcesoBeans) listaOCS.get(0); %>

            <fieldset id="HeadTable">

            <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels" with='1500'>

                <tr>

                    <td width="850" align="center">
                        <table border="0" cellpadding="2" cellspacing="2">

                            <tr>
                                <td align="right">COMPRA / SERVICIO No: &nbsp;</td>
                                <td align="left"><input name="tipo_solicitud" type="text" class="input_450px" id="tipo_solicitud" value="<%=OCompra.getOrdenCompra()%>" readonly ></td>
                                <td align="right">&nbsp;</td>
                            </tr>

                            <tr>
                                <td align="right">PROVEEDOR: &nbsp;</td>
                                <td align="left">
                                    <input name="proveedor" type="text" class="input_200px" id="proveedor" value="<%=OCompra.getCodProveedor()%>" readonly >
                                    <input name="name_proveedor" type="text" class="input_250px" id="name_proveedor" value="<%=OCompra.getNombreProveedor()%>" readonly >
                                </td>
                                <td align="right">&nbsp;</td>
                            </tr>

                            <tr>
                                <td align="right">DESCRIPCION: &nbsp;</td>
                                <td><textarea name="descripcion" class="textbox_450x100px" id="descripcion"><%=Descripcion%></textarea></td>
                                <td align="right">&nbsp;</td>
                            </tr>



                        </table>
                    </td> <!--PRIMERA MITAD -->

                    <td width="850" align="center">
                        <table border="0" cellpadding="2" cellspacing="2">
                            <tr>
                                <td align="right">DESPACHO NO: &nbsp;</td>
                                <td><input type="text" name="orden_ocs" id="orden_ocs" class="input_100px" value="" readonly ></td>
                            </tr>
                            <tr>
                                <td align="right">FECHA DESPACHO: &nbsp;</td>
                                <td><input name="fecha_entrega" type="text" class="inputCalendar" id="fecha_entrega" value="<%=aniocte%>" readonly ></td>
                            </tr>
                            <tr>
                                <td align="right">ENTREGADO EN: &nbsp;</td>
                                <td align="left"><input name="direccion_entrega" type="text" class="input_450px" id="direccion_entrega" value="<%=direccion_entrega%>" readonly></td>
                            </tr>
                            <tr>
                                <!--
                                <td align="right">FORMA DE PAGO: &nbsp;</td>
                                <td>
                                    <select name="f_pago"  id="f_pago" class="combo_150px" onchange="">
                                        <OPTION value='1' selected>CONTADO</OPTION>
                                        <OPTION value='2'>CREDITO</OPTION>
                                    </select>
                                </td> -->
                            </tr>

                            <tr>
                                <td align="right">&nbsp;</td>
                                <td align="left" >
                                    <!--
                                    <img id="img_guardar_ocs" src='./images/Price_list.png' height="35" width="35" onClick="GuardarOCS('<%=NmLogin%>')">
                                    <img id="img_eliminar_preocs" src='./images/shopcartexclude.png' height="40" width="40" onClick="EliminarOCS('<%=NmLogin%>')">
                                    <img id="cerrar_orden" src='./images/stop.png' height="40" width="40" style="display:none;" onClick="$('#div_nueva_ocs').fadeOut('slow')">
                                    -->
                                    <table border="0" cellpadding="2" cellspacing="2">
                                        <tr>
                                        <td width="100%">
                                        <ul class="tab">
                                            <li id="ExtencionCompleta"><a href="javascript:void(0)" class="tablinks" onclick="GuardarDespacho('<%=NmLogin%>', '<%=recepcion%>')"><img src='./images/Dispatch.png' id="img_guardar_dsp" height="25" width="25"> Guardar Despacho</a></li>
                                            <li><a href="javascript:void(0)" class="tablinks" onclick="$('#div_nuevo_despacho').fadeOut('slow')"><img src='./images/stop.png' height="20" width="20"> Cerrar</a></li>
                                        </ul>
                                            </td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                        </table>
                    </td><!--SEGUNDA MITAD -->
                </tr>

            </table>

            </fieldset>


        </td>
    </tr>


    <tr>
        <td colspan="3">
            <table border="0" align="right" cellpadding="1" cellspacing="1" with='1000'>
                <tr><td width="1500" align="center">&nbsp;</td></tr>
            </table>

        </td>
    </tr>


    <tr>
        <td>
            <fieldset>
                <div id="CapsuleDespacho"> <!-- <div id="CapsuleTable"> -->

                    <table border='0' align='center'>
                        <tr>
                            <td width="1500">

                                <table id="tbl_despacho" class="table table-striped table-bordered" width="1400" border='0' align='center' > <!-- width="1250" height="18" border='0' align='left' cellpadding="0" cellspacing="0" -->

                                    <thead>
                                        <tr>
                                            <th width='20' align='center'>ESTADO</th>
                                            <th width='20' align='center'>ID</th>
                                            <th width='20' align='center'>ID_DET</th>
                                            <th width='100' align='center'>COD. MAT</th>
                                            <th width='600' align='center'>DESCRIPCION</th>
                                            <th width='60' align='center'>U. MED</th>
                                            <th width='60' align='center'>REF. EXT</th>
                                            <th width='80' align='center'>CANT DISPONIBLE</th>
                                            <th width='80' align='center'>COSTO UNITARIO</th>
                                            <th width='60' align='center'>TOTAL COSTO</th>
                                            <th width='60' align='center'>COSTO DESPACHADO</th>
                                            <th width='60' align='center'>CANTIDAD DESPACHADA</th>
                                            <th width='60' align='center'>TOTAL DESPACHADO</th>

                                            <th width='60' align='center' style="display:none;">SOLICITAR</th>
                                            <th width='60' align='center' style="display:none;">SOLICITAR</th>
                                            <th width='60' align='center' style="display:none;">SOLICITAR</th>
                                        </tr>
                                    </thead><%

                                    String src = "";
                                    String alt = "";
                                    String cierre = "";
                                    String prioridad = "";

                                    String CostoDespacho = "";
                                    String CantDespacho = "";
                                    String TotalDespacho = "";
                                    Integer Cuenta = 1;

                                    if ( listaR.size() > 0 ) {%>

                                        <tbody><%

                                        String TextReadOnly;

                                        for (int i = 0; i < listaR.size(); i++) {

                                            CostoDespacho = "CostoDespacho" + Cuenta; //CostoCompra
                                            CantDespacho = "CantDespacho" + Cuenta; //CantSolicitar
                                            TotalDespacho = "TotalDespacho" + Cuenta; //TotalRow

                                            TextReadOnly = "";

                                            ComprasProcesoBeans ListadoContenido = (ComprasProcesoBeans) listaR.get(i);%>

                                            <tr>
                                                <td align="center"><%=ListadoContenido.getRegStatus()%></td>
                                                <td align="center"><%=ListadoContenido.getIdOcs()%></td>
                                                <td align="center"><%=ListadoContenido.getIdOcsDetalle()%></td>
                                                <td align="left"><%=ListadoContenido.getCodigoMaterial()%></td>
                                                <td align="justify"><%=ListadoContenido.getDescripcionInsumo()%></td>
                                                <td align="center"><%=ListadoContenido.getNombreUnidadInsumo()%></td>
                                                <td align="center"><%=ListadoContenido.getReferenciaExterna()%></td>
                                                <td align="right"><%=ListadoContenido.getCantidadRecibida()%></td>
                                                <td align="right"><%=formateador.format(Double.parseDouble(ListadoContenido.getCostoUnitarioRecibido()))%></td>
                                                <td align="right"><%=formateador.format(Double.parseDouble(ListadoContenido.getCostoTotalRecibido()))%></td>
                                                <td align="left"><input type="number" name="CostoDespacho" id="CostoDespacho" class="input_80pxDespacho solo-numero" value="<%=ListadoContenido.getCostoUnitarioRecibido()%>"<%=recepcion ? "ReadOnly" : ""%>></td> <!-- input_80pxDespacho -->
                                                <td align="left"><input type="number" name="CantDespacho" id="CantDespacho" class="input_80pxDespachoEsp solo-numero" value="" ></td> <!-- input_80pxDespachoEsp -->
                                                <td align="left"><input type="text" name="TotalDespacho" id="TotalDespacho" class="input_80pxWithOutAction" readonly="readonly" value="0.0000"></td>

                                                <td style="display:none;"><input type="text" name="IdDetalleInsumo" id="IdDetalleInsumo" value="<%=ListadoContenido.getIdOcsDetalle()%>"></td>
                                                <td style="display:none;"><input type="text" name="CodigoMaterial" id="CodigoMaterial" value="<%=ListadoContenido.getCodigoMaterial()%>"></td>
                                                <td style="display:none;"><data-holder  class="data-holder" UserResponsable="<%=NmLogin%>" CodSolicitud="<%=ListadoContenido.getCodSolicitud()%>" CodigoMaterial="<%=ListadoContenido.getCodigoMaterial()%>" CostoDespacho="<%=CostoDespacho%>" CantDespacho="<%=CantDespacho%>" TotalDespacho="<%=TotalDespacho%>" ></data-hodler></td>
                                            </tr><%

                                            Cuenta++;

                                        }%>

                                        </tbody><%

                                    }else{%>

                                        <tr>
                                            <td colspan="10">NO HAY INSUMOS PARA ESTE DESPACHO</td>
                                        </tr><%

                                    }%>
                                </table>
                                <!--x-->



                            </td>
                        </tr>
                    </table>

                </div>
            </fieldset>

        </td>
    </tr>

    <tr>
        <td>

            <fieldset id="HeadTable">

            <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels" with='1500'>

                <tr>

                    <td width="850" align="center"></td> <!--PRIMERA MITAD -->

                    <td width="850" align="right">
                        <table border="0" cellpadding="2" cellspacing="2" align="right">
                            <tr>
                                <td align="right">TOTAL: &nbsp;</td>
                                <td><input type="text" class="input_150px" name="total_despacho" id="total_despacho" value="0" readonly ></td>
                            </tr>
                        </table>
                    </td><!--SEGUNDA MITAD -->
                </tr>

            </table>

            </fieldset>


        </td>
    </tr>


</table>

</form>
