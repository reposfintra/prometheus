<%--
    Document   : Modulo de Compras y Servicios.
    Created on : 10/02/2017, 11:00:00 AM
    Author     : hcuello
--%>

<%@page import="org.omg.PortableInterceptor.SYSTEM_EXCEPTION"%>
<%@page session="true" %>
<%@ page import="com.tsp.operation.model.beans.Usuario"%>
<%@ page import="com.tsp.operation.model.beans.CmbGenericoBeans"%>
<%@ page import="com.tsp.operation.model.services.RequisicionesService"%>
<%@ page import="com.tsp.operation.model.beans.RequisicionesListadoBeans"%>

<%@ page import="com.tsp.operation.model.beans.ComprasProcesoBeans"%>
<%@ page import="com.tsp.operation.model.services.ComprasProcesoService"%>

<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="java.text.SimpleDateFormat" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>


<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String NombreUsuario = usuario.getNombre();
    String NmLogin = usuario.getLogin();
    
    Date dNow = new Date( );
    
    SimpleDateFormat nd = new SimpleDateFormat ("yyyy-MM-dd");
    String aniocte = nd.format(dNow);
    
    String id_sol = request.getParameter("id_sol") != "" ? request.getParameter("id_sol") : "";
    String solicitud = request.getParameter("solicitud") != "" ? request.getParameter("solicitud") : "";
    
    ComprasProcesoService LstService = new ComprasProcesoService(usuario.getBd());
    ArrayList listaR =  LstService.GetInsumosCatalogo("SQL_LISTADO_CATALOGO");

%>

<script language="JavaScript1.2">

$(document).ready(function(){
        
    var table = $('#tbl_catalogo').DataTable();
        
});

</script>

<form name="form_nueva_rq" id="form_nueva_rq" method="post" enctype="multipart/form-data" target="iframeUpload">

<input type="hidden" name="id_solicitud" id="id_solicitud" value="<%=id_sol%>" />
<input type="hidden" name="NmUser" id="NmUser" value="<%=NmLogin%>" />
<input type="hidden" name="CdSol" id="CdSol" value="<%=solicitud%>" />

<table with='1500' align="center" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF">
    
    <tr>
        <td style="cursor:pointer" width="100%">
            <div class="k-header">
                <span class="titulo">Adicionar Elemento</span>
                <span class="ui-widget-header ui-corner-all titulo_right" onClick="ValidarCierreCatalogo(event)">x</span>
            </div>
        </td>
    </tr>   
    
    <tr>
        <td>
            <fieldset>
                <div id="CapsuleTable">
                    
                    <table border='0' align='center'>
                        <tr>
                            <td width="1500">

                                <table id="tbl_catalogo" class="table table-striped table-bordered" width="1400" border='0' align='center' > <!-- width="1250" height="18" border='0' align='left' cellpadding="0" cellspacing="0" -->

                                    <thead>
                                        <tr>
                                            <th width='80' align='center'>TIPO INSUMO</th>
                                            <th width='80' align='center'>CODIGO MATERIAL</th>
                                            <th width='600' align='center'>DESCRIPCION</th>
                                            <th width='150' align='center'>U. MEDIDA</th>
                                            <th width='100' align='center'>CANTIDAD</th>
                                            <th width='60' align='center'>ACCION</th>
                                        </tr>
                                    </thead><%
                                    
                                    if ( listaR.size() > 0 ) {%>

                                        <tbody><%
                                        
                                        for (int i = 0; i < listaR.size(); i++) { 
                                            
                                            ComprasProcesoBeans ListadoContenido = (ComprasProcesoBeans) listaR.get(i); %>

                                            <tr> 
                                                <td align="justify"><%=ListadoContenido.getTipoInsumo()%></td>
                                                <td align="left"><%=ListadoContenido.getCodigoMaterial()%></td>
                                                <td align="justify"><%=ListadoContenido.getDescripcionInsumo()%></td>
                                                <td align="justify">GLOBAL</td>
                                                <td align="center"><input type="text" name="CantidadAsignar" id="CantidadAsignar" class="input_80Normalpx" value="0" onkeypress="return isNumberKey(event)" ></td>
                                                <td align="center"><img src='./images/Returns.png' height="40" width="40" class="RetornoInsumo" ></td> <!-- onclick="RetornarInsumo(this)" -->
                                                
                                            </tr><%

                                        }
                                        
                                    }else{%>

                                        <tr>
                                            <td colspan="11">NO HAY INSUMOS PARA ESTE PROYECTO</td>
                                        </tr><%

                                    }%>
                                    
                                    </tbody>
                                    
                                </table>
                                <!--x-->



                            </td>
                        </tr>
                    </table>
                                
                </div>
            </fieldset>    

        </td>
    </tr>
    
</table>
                
</form>
    