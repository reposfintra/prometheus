<%--
    Document   : Modulo de Compras y Servicios.
    Created on : 10/02/2017, 11:00:00 AM
    Author     : hcuello
--%>

<%@page session="true" %>
<%@ page import="com.tsp.operation.model.beans.Usuario"%>
<%@ page import="com.tsp.operation.model.beans.CmbGenericoBeans"%>
<%@ page import="com.tsp.operation.model.services.RequisicionesService"%>
<%@ page import="com.tsp.operation.model.beans.RequisicionesListadoBeans"%>

<%@ page import="com.tsp.operation.model.beans.ComprasProcesoBeans"%>
<%@ page import="com.tsp.operation.model.services.ComprasProcesoService"%>

<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="java.text.SimpleDateFormat" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>


<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String NombreUsuario = usuario.getNombre();
    String NmLogin = usuario.getLogin();
    
    Date dNow = new Date( );
    
    SimpleDateFormat nd = new SimpleDateFormat ("yyyy-MM-dd");
    String aniocte = nd.format(dNow);
    
    String Descripcion = "";
    
    String id_sol = request.getParameter("id_sol") != "" ? request.getParameter("id_sol") : "";
    String accione = request.getParameter("ACCIONE") != "" ? request.getParameter("ACCIONE") : "";
    String ordeneditar = request.getParameter("ORDENEDITAR") != "" ? request.getParameter("ORDENEDITAR") : "";
    
    ComprasProcesoService LstService = new ComprasProcesoService(usuario.getBd());
    //ArrayList listaR =  LstService.GetInsumosOCS("SQL_INSUMOS_OCS",NmLogin);
    ArrayList listaOCSEditar =  LstService.GetInsumosOCSEdit("SQL_INSUMOS_OCS_EDITAR", NmLogin, ordeneditar);

%>

<script language="JavaScript1.2">

$(document).ready(function(){
        
    var table = $('#tbl_insumos_ocs').DataTable( {
        pageLength: 100
        //,dom: '<"top">rt<"bottom" ip><"clear">'
        ,dom: '<"toolbar">frtip'
        /*
        ,columnDefs: [ 
            {
                targets: [ 6 ]
                ,visible: false
                //,searchable: false
            },
            {
                targets: [ 7 ]
                ,visible: false
                //,searchable: false
            }          
        ]*/
        ,fnRowCallback: function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) {
            
            //console.log('ppppppppppp');
            //console.log(aData[2]);
            
            if ( aData[0] == "S" ) {
                //$('td', nRow).css('background-color', '#ff3333');
                $('td', nRow).addClass( "EstiloNuevoInsumo" );
                //background: url('./images/select_ad-x.png');
            }else{
                //$('td', nRow).css('background-color', '#eeffee');
                //background: url('./images/select_ad-x.png');
            }
            
        }

    });
    
    $( "#tipo_solicitud" ).prop( "disabled", false );
    $( "#proveedor" ).prop( "disabled", false );
    $( "#descripcion" ).prop( "disabled", false );
    $( "#orden_ocs" ).prop( "disabled", false );
    $( "#fecha_entrega" ).prop( "disabled", false );
    $( "#direccion_entrega" ).prop( "disabled", false );
    $( "#dias_pago_credito" ).prop( "disabled", false );

    $("#tbl_insumos_ocs").find("input,button,textarea,select").attr("disabled", false);

    $('#img_guardar_ocs').show();
    $('#img_eliminar_preocs').show();

    $('#cerrar_orden').hide();    
        
});

</script>

<form name="form_nueva_rq" id="form_nueva_rq" method="post" enctype="multipart/form-data" target="iframeUpload">

<input type="hidden" name="id_solicitud" id="id_solicitud" value="<%=id_sol%>" />

<table with='1500' align="center" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF">
    
    <tr>
        <td id="drag_editar_ocs" style="cursor:pointer">
            <div class="k-header">
                <span class="titulo">Editar - Orden Compra y Servicio</span>
                <span class="ui-widget-header ui-corner-all titulo_right" onClick="$('#div_editar_ocs').fadeOut('slow')">x</span>
            </div>
        </td>
    </tr>    
    
    <tr><td>&nbsp;</td></tr>    
    
    <tr>
        <td>
            
            <fieldset id="HeadTable">
                                
            <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels" with='1500'>
                
                <tr>
                    
                    <td width="850" align="center">
                        <table border="0" cellpadding="2" cellspacing="2">

                            <tr>
                                <td align="right">TIPO DE SOLICITUD: &nbsp;</td>
                                <td align="left"><input name="tipo_solicitud" type="text" class="input_450px" id="tipo_solicitud" value="ORDEN DE COMPRA" readonly ></td>
                                <td align="right">&nbsp;</td>
                            </tr>                            

                            <tr>
                                <td align="right">PROVEEDOR: &nbsp;</td>
                                <td align="left">
                                    <input name="proveedor" type="text" class="input_200px" id="proveedor" value="" readonly >
                                    <input name="name_proveedor" type="text" class="input_250px" id="name_proveedor" value="" readonly >
                                </td>
                                <td align="right"><img id="" src='./images/Dj_View.png' height="30" width="30" onClick="CargarProveedor(event)"></td>
                            </tr>
                            
                            <tr>
                                <td align="right">DESCRIPCION: &nbsp;</td>
                                <td><textarea name="descripcion" class="textbox_450x100px" id="descripcion"><%=Descripcion %></textarea></td>
                                <td align="right">&nbsp;</td>
                            </tr>
                            
                            
                            
                        </table>    
                    </td> <!--PRIMERA MITAD -->
                    
                    <td width="850" align="center">
                        <table border="0" cellpadding="2" cellspacing="2">
                            <tr>
                                <td align="right">ORDEN NO: &nbsp;</td>
                                <td><input type="text" name="orden_ocs" id="orden_ocs" class="input_100px" value="" readonly ></td>
                            </tr>
                            <tr>
                                <td align="right">FECHA ENTREGA: &nbsp;</td>
                                <td><input name="fecha_entrega" type="text" class="inputCalendar" id="fecha_entrega" value="<%=aniocte%>" readonly ></td>
                            </tr>
                            <tr>
                                <td align="right">DIRECCION DE ENTREGA: &nbsp;</td>
                                <td align="left"><input name="direccion_entrega" type="text" class="input_450px" id="direccion_entrega" value=""></td>
                            </tr>
                            <tr>
                                <td align="right">FORMA DE PAGO: &nbsp;</td>
                                <td>
                                    <select name="f_pago"  id="f_pago" class="combo_150px" onchange="ValidarFormaPago()">
                                        <OPTION value='1' selected>CONTADO</OPTION>
                                        <OPTION value='2'>CREDITO</OPTION>
                                    </select>  
                                    <input name="dias_pago_credito" type="text" class="input_100px" id="dias_pago_credito" value="" readonly="" onkeypress="return isNumberKey(event)">
                                </td>
                            </tr>                            
                            
                            <tr>
                                <td colspan="2" align="center" >
                                    <!--
                                    <img id="img_guardar_ocs" src='./images/Price_list.png' height="35" width="35" onClick="GuardarOCS('<%=NmLogin%>')">
                                    <img id="img_eliminar_preocs" src='./images/shopcartexclude.png' height="40" width="40" onClick="EliminarOCS('<%=NmLogin%>')">
                                    <img id="cerrar_orden" src='./images/stop.png' height="40" width="40" style="display:none;" onClick="$('#div_nueva_ocs').fadeOut('slow')">
                                    -->
                                    <table border="0" cellpadding="2" cellspacing="2">
                                        <tr>
                                        <td width="100%">    
                                        <ul class="tab">
                                            <li><a href="javascript:void(0)" class="tablinks" onclick="GuardarOCS('<%=NmLogin%>')"><img src='./images/Price_list.png' height="20" width="20"> Guardar OC</a></li>
                                            <li><a href="javascript:void(0)" class="tablinks" onclick="EliminarPreOCS('<%=NmLogin%>')"><img src='./images/shopcartexclude.png' height="20" width="20"> Eliminar OC</a></li>
                                            <li><a href="javascript:void(0)" class="tablinks" onclick="$('#div_editar_ocs').fadeOut('slow')"><img src='./images/stop.png' height="20" width="20"> Cerrar</a></li>
                                        </ul>
                                            </td>    
                                        </tr>    
                                    </table>
                                    
                                </td>
                            </tr>
                        </table>    
                    </td><!--SEGUNDA MITAD -->
                </tr>
                
            </table>
                            
            </fieldset>
                            

        </td>
    </tr>
    
    
    <tr>
        <td colspan="3">
            <table border="0" align="right" cellpadding="1" cellspacing="1" with='1000'>
                <tr><td width="1500" align="center">&nbsp;</td></tr>
            </table>

        </td>
    </tr>
                

    <tr>
        <td>
            <fieldset>
                <div id="CapsuleTable">
                    
                    <table border='0' align='center'>
                        <tr>
                            <td width="1500">

                                <table id="tbl_insumos_ocs" class="table table-striped table-bordered" width="1400" border='0' align='center' > <!-- width="1250" height="18" border='0' align='left' cellpadding="0" cellspacing="0" -->

                                    <thead>
                                        <tr>
                                            <th width='20' align='center'>ADD</th>
                                            <th width='100' align='center'>COD SOLICITUD</th>
                                            <th width='100' align='center'>COD. MAT</th>
                                            <th width='600' align='center'>DESCRIPCION</th>
                                            <th width='60' align='center'>U. MED</th>
                                            <th width='60' align='center'>REF. EXT</th>
                                            <th width='80' align='center'>COSTO PPTO</th>
                                            <th width='80' align='center'>CANTIDAD</th>
                                            <th width='80' align='center'>SOLICITADOS</th>
                                            <th width='60' align='center'>DISPONIBLE</th>
                                            <th width='60' align='center'>COSTO COMPRA</th>
                                            <th width='60' align='center'>SOLICITAR</th>
                                            <th width='60' align='center'>TOTAL</th>
                                            
                                            <th width='60' align='center' style="display:none;">SOLICITAR</th>
                                            <th width='60' align='center' style="display:none;">SOLICITAR</th>
                                        </tr>
                                    </thead><%

                                    String src = "";
                                    String alt = "";
                                    String cierre = "";
                                    String prioridad = "";

                                    String CostoCompra = "";
                                    String CantSolicitar = "";
                                    String TotalRow = "";
                                    Integer Cuenta = 1;

                                    if ( listaOCSEditar.size() > 0 ) {%>

                                        <tbody><%
                                        
                                        String TextReadOnly;
                                        
                                        for (int i = 0; i < listaOCSEditar.size(); i++) { 
                                            
                                            CostoCompra = "CostoCompra" + Cuenta;
                                            CantSolicitar = "CantSolicitar" + Cuenta;
                                            TotalRow = "TotalRow" + Cuenta;
                                            
                                            TextReadOnly = "";
                                            
                                            ComprasProcesoBeans ListadoContenido = (ComprasProcesoBeans) listaOCSEditar.get(i); %>

                                            <tr> 
                                                <td align="center"><%=ListadoContenido.getInsumoAdicional()%></td>
                                                <td align="justify"><%=ListadoContenido.getCodSolicitud()%></td>
                                                <td align="left"><%=ListadoContenido.getCodigoMaterial()%></td>
                                                <td align="justify"><%=ListadoContenido.getDescripcionInsumo()%></td>
                                                <td align="center"><%=ListadoContenido.getNombreUnidadInsumo()%></td>
                                                <td align="center"><%=ListadoContenido.getReferenciaExterna()%></td>
                                                <td align="right"><%=ListadoContenido.getCostoPresupuesto()%></td>
                                                <td align="right"><%=ListadoContenido.getCantidadTotal()%></td>
                                                <td align="right"><%=ListadoContenido.getCantidadSolicitada()%></td>
                                                <td align="right"><%=ListadoContenido.getCantidadDisponible()%></td><!-- 7 ó 8 -->
                                                <td align="left"><input type="text" name="CostoCompra" id="CostoCompra" class="input_80pxValor" value="<%=ListadoContenido.getCostoPresupuesto()%>" ></td>
                                                <td align="left"><input type="text" name="CantSolicitar" id="CantSolicitar" class="input_80pxEspecial" value="<%=ListadoContenido.getSolicitadoTemporal()%>" ></td>
                                                <td align="left"><input type="text" name="TotalRow" id="TotalRow" class="input_80pxWithOutAction" readonly="readonly" value="0.0000"></td>
                                                
                                                <td style="display:none;"><input type="text" name="CodigoMaterial" id="CodigoMaterial" value="<%=ListadoContenido.getCodigoMaterial()%>"></td>
                                                <td style="display:none;"><data-holder  class="data-holder" UserResponsable="<%=NmLogin%>" CodSolicitud="<%=ListadoContenido.getCodSolicitud()%>" CodigoMaterial="<%=ListadoContenido.getCodigoMaterial()%>" CostoCompra="<%=CostoCompra%>" CantSolicitar="<%=CantSolicitar%>" TotalRow="<%=TotalRow%>" ></data-hodler></td>
                                            </tr><%

                                            Cuenta++;

                                        }%>
                                        
                                        </tbody><%    

                                    }else{%>

                                        <tr>
                                            <td colspan="10">NO HAY INSUMOS PARA ESTE PROYECTO</td>
                                        </tr><%

                                    }%>
                                </table>
                                <!--x-->



                            </td>
                        </tr>
                    </table>
                                
                </div>
            </fieldset>    

        </td>
    </tr>
    
    <tr>
        <td>
            
            <fieldset id="HeadTable">
                                
            <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels" with='1500'>
                
                <tr>
                    
                    <td width="850" align="center"></td> <!--PRIMERA MITAD -->
                    
                    <td width="850" align="right">
                        <table border="0" cellpadding="2" cellspacing="2" align="right">
                            <tr>
                                <td align="right">SUBTOTAL: &nbsp;</td>
                                <td><input type="text" class="input_150px" name="subtotal_orden_ocs" id="subtotal_orden_ocs" value="0" readonly ></td>
                            </tr>
                            <tr>
                                <td align="right">DESCUENTOS: &nbsp;</td>
                                <td align="right">
                                    <select name="f_pago"  id="f_pago" class="combo_150px" onchange="">
                                        <OPTION value='0' selected>0%</OPTION>
                                    </select>  
                                </td>
                            </tr>                            
                            <tr>
                                <td align="right">TOTAL: &nbsp;</td>
                                <td><input type="text" class="input_150px" name="total_orden_ocs" id="total_orden_ocs" value="0" readonly ></td>
                            </tr>
                        </table>    
                    </td><!--SEGUNDA MITAD -->
                </tr>
                
            </table>
                            
            </fieldset>
                            

        </td>
    </tr>

    
</table>
                
</form> 
                                    
