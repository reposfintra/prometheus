<%--
    Document   : Modulo de Compras y Servicios.
    Created on : 10/02/2017, 11:00:00 AM
    Author     : hcuello
--%>

<%@page session="true" %>
<%@ page import="com.tsp.operation.model.beans.Usuario"%>
<%@ page import="com.tsp.operation.model.beans.CmbGenericoBeans"%>
<%@ page import="com.tsp.operation.model.services.RequisicionesService"%>
<%@ page import="com.tsp.operation.model.beans.RequisicionesListadoBeans"%>

<%@ page import="com.tsp.operation.model.beans.ComprasProcesoBeans"%>
<%@ page import="com.tsp.operation.model.services.ComprasProcesoService"%>

<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="java.text.SimpleDateFormat" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>


<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String NombreUsuario = usuario.getNombre();
    String NmLogin = usuario.getLogin();
    
    Date dNow = new Date( );
    
    SimpleDateFormat nd = new SimpleDateFormat ("yyyy-MM-dd");
    String aniocte = nd.format(dNow);
    
    String Descripcion = "";
    
    String id_sol = request.getParameter("id_sol") != "" ? request.getParameter("id_sol") : "";
    String accione = request.getParameter("ACCIONE") != "" ? request.getParameter("ACCIONE") : "";
    String CodInsumo = request.getParameter("cod_insumo") != "" ? request.getParameter("cod_insumo") : "";
    String item = request.getParameter("ITEM") != "" ? request.getParameter("ITEM") : "";
    
    String TipoSolicitud = request.getParameter("estado") != "" ? request.getParameter("estado") : "";
    String Proyecto = request.getParameter("mes") != "" ? request.getParameter("mes") : "";
    
    ComprasProcesoService LstService = new ComprasProcesoService(usuario.getBd());
    ArrayList listaR =  LstService.GetInsumosxApu("SQL_INSUMOS_xAPU",NmLogin,id_sol,CodInsumo);

%>

<script language="JavaScript1.2">

$(document).ready(function(){
        
    var table = $('#tbl_insumos_apus').DataTable( {
        /*
        columnDefs: [ 
            {
                targets: [ 0 ]
                ,visible: false
                //,searchable: false
            },
            {
                targets: [ 8 ]
                ,searchable: false
            }                
        ]*/


    });
        
});

</script>

<form name="form_nueva_rq" id="form_nueva_rq" method="post" enctype="multipart/form-data" target="iframeUpload">

<input type="hidden" name="id_solicitud" id="id_solicitud" value="<%=id_sol%>" />

<table with='1500' align="center" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF">
    
    <tr>
        <td id="drag_nueva_requisicion" style="cursor:pointer">
            <div class="k-header">
                <span class="titulo">Insumos Por APU</span>
                <span class="ui-widget-header ui-corner-all titulo_right" onClick="$('#div_InsumosApu').fadeOut('slow')">x</span>
            </div>
        </td>
    </tr>    
    
    <tr><td>&nbsp;</td></tr>    
    
    <tr>
        <td>
            <fieldset>
                <div id="CapsuleTable">
                    
                    <table border='0' align='center'>
                        <tr>
                            <td width="1500">

                                <table id="tbl_insumos_apus" class="table table-striped table-bordered" width="1400" border='0' align='center' > <!-- width="1250" height="18" border='0' align='left' cellpadding="0" cellspacing="0" -->

                                    <thead>
                                        <tr>
                                            <th width='100' align='center'>CODIGO MATERIAL</th>
                                            <th width='600' align='center'>DESCRIPCION</th>
                                            <th width='800' align='center'>APU</th>
                                            <th width='80' align='center'>PRESUPUESTO</th>
                                            <th width='80' align='center'>SOLICITADOS</th>
                                            <th width='80' align='center'>DISPONIBLES</th>
                                            <th width='60' align='center'>SOLICITAR</th>
                                            <th width='60' align='center' style="display:none;">SOLICITAR</th>
                                            <th width='60' align='center' style="display:none;">SOLICITAR</th>
                                        </tr>
                                    </thead><%

                                    String src = "";
                                    String alt = "";
                                    String cierre = "";
                                    String prioridad = "";

                                    String TextField = "";
                                    String TextUser = "";
                                    String TextCodMat = "";
                                    Integer Cuenta = 1;

                                    if ( listaR.size() > 0 ) {%>

                                        <tbody><%
                                        
                                        String TextReadOnly;
                                        
                                        for (int i = 0; i < listaR.size(); i++) { 
                                            
                                            TextField = "textfield" + Cuenta;
                                            TextUser = "TextUser" + Cuenta;
                                            TextCodMat = "TextCodMat" + Cuenta;
                                            
                                            TextReadOnly = "";
                                            
                                            ComprasProcesoBeans ListadoContenido = (ComprasProcesoBeans) listaR.get(i); %>

                                            <tr> 
                                                <td align="left"><%=ListadoContenido.getCodigoMaterial()%></td>
                                                <td align="justify"><%=ListadoContenido.getDescripcionInsumo()%></td>
                                                <td align="center"><%=ListadoContenido.getNombreApu()%></td>
                                                <td align="left"><%=ListadoContenido.getCantidadTotal()%></td>
                                                <td align="left"><%=ListadoContenido.getCantidadSolicitada()%></td>
                                                <td align="left"><%=ListadoContenido.getCantidadDisponible()%></td><%
                                                
                                                if ( accione.equals("VISUALIZAR") ) { %>
                                                
                                                    <td align="left"><%=ListadoContenido.getSolicitadoTemporal()%></td> <%
                                                    
                                                }else{
                                                
                                                    if ( ListadoContenido.getCantidadDisponible().equals("0.0000") ) {
                                                        TextReadOnly = "readonly";
                                                    }%>
                                                    <!-- onchange="CalcularDiferencias(event)" -->
                                                    <td align="left"><input type="text" class="input_80InsumoAPUpx" name="CantidadSolicitada" id="CantidadSolicitada" <%=TextReadOnly%> value="<%=ListadoContenido.getSolicitadoTemporal()%>" ></td><%
                                                    
                                                }%>
                                                
                                                <td align="left" style="display:none;"><input type="text" class="input_80px" name="UserResponsable" id="UserResponsable" value="<%=NmLogin%>" ></td> <!-- <%=TextUser%> -->
                                                <td align="left" style="display:none;"><input type="text" class="input_80px" name="CodigoMaterial" id="CodigoMaterial" value="<%=ListadoContenido.getCodigoMaterial()%>"></td> <!-- <%=TextCodMat%> -->
                                                
                                            </tr><%

                                            Cuenta++;

                                        }%>
                                        </tbody><%    

                                    }else{%>

                                        <tr>
                                            <td colspan="10">NO HAY INSUMOS PARA ESTE PROYECTO</td>
                                        </tr><%

                                    }%>
                                </table>
                                <!--x-->

                            </td>
                        </tr>
                    </table>
                                
                </div>
            </fieldset>    

        </td>
    </tr>
    
</table>
                
</form>
    