<%--
    Document   : Modulo de Compras y Servicios.
    Created on : 10/02/2017, 11:00:00 AM
    Author     : hcuello
--%>

<%@page session="true" %>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@ page import="com.tsp.operation.model.beans.CmbGenericoBeans"%>
<%@ page import="com.tsp.operation.model.services.RequisicionesService"%>


<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="java.text.SimpleDateFormat" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="javax.servlet.*" %>
<%@ page import    ="javax.servlet.http.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>



<%    
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String NombreUsuario = usuario.getNombre();
    String NmLogin = usuario.getLogin();

    Date dNow = new Date();

    SimpleDateFormat nd = new SimpleDateFormat("yyyy");
    String aniocte = nd.format(dNow);

    SimpleDateFormat md = new SimpleDateFormat("M");
    String mescte = md.format(dNow);
    
    int acorriente = Integer.parseInt(aniocte);
    int mescorriente = Integer.parseInt(mescte);
    
    
    String num_solicitud = request.getParameter("num_solicitud") != "" ? request.getParameter("num_solicitud") : "";
%>

<html>
    <head>

        <title>ORDENES DE COMPRA Y SERVICIOS</title>
        
        <!-------------------------------------EstilosBase----------------------------------------->
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/estilostsp.css" >
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/style_azul.css" >
        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/jscal2.css" />
        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/border-radius.css" />
        <link type="text/css" rel="stylesheet" href="./css/main2.css">
        <!-------------------------------------EstilosBase----------------------------------------->        
        

        
        <!-------------------------------------jQeury----------------------------------------->
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery-1.4.2.min.js"></script>
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/jquery-ui-1.8.5.custom.min.js"></script>

        <!-- <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/jquery/jquery-ui/jquery-ui.css"/> OJO -->
        <!--<script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>-->  
        <!--<script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>--> 
        
        <!--<script language="JavaScript" type="text/javascript" src="./js/jquery/jquery-latest-version/1x/jQuery-1.12.4-min.js"></script>
        <script language="JavaScript" type="text/javascript" src="./js/jquery/jquery-latest-version/1x/jQuery-ui-1.12.1-min.js"></script>-->
        <!-------------------------------------jQeury----------------------------------------->
        
        
        <!-------------------------------------Tools----------------------------------------->
        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>
        <script type="text/javascript" src="./js/compras_proceso.js"></script>
        <script type="text/javascript" src="./js/utilidadInformacion.js"></script>
        <!-------------------------------------Tools----------------------------------------->
        
        
        <!-------------------------------------dataTable----------------------------------------->
        <script type="text/javascript" src="./data_table/js/jquery-1.12.4.js"></script>
        <script type="text/javascript" src="./data_table/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="./data_table/js/dataTables.bootstrap.min.js"></script>
        <!-------------------------------------dataTable----------------------------------------->

        
        <!-------------------------------------dataTable-BootsTrap----------------------------------------->
        <link type="text/css" rel="stylesheet" href="./data_table/css/bootstrap.min.css" >
        <link type="text/css" rel="stylesheet" href="./data_table/css/dataTables.bootstrap.min.css" >
        <!-------------------------------------dataTable-BootsTrap----------------------------------------->

        
        <!-------------------------------------Smoke----------------------------------------->
        <script src="./js/smoke/smoke.min_old.js"></script>
        <link href="./js/smoke/smoke.css" rel="stylesheet" type="text/css" />
        <!-------------------------------------Smoke----------------------------------------->

        
        <!-------------------------------------Confirm----------------------------------------->
        <script type="text/javascript" src="./js/jquery/jquery-confirm/dist/jquery-confirm.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./js/jquery/jquery-confirm/dist/jquery-confirm.min.css" />
        <!-------------------------------------Confirm----------------------------------------->

        <div id="div_editar_ocs" style="display:none;z-index:102; position:absolute; border: 1px solid #165FB6; background:#FFFFFF; padding: 3px 3px 3px 3px; width:1500px;">WWWWWWWWWWWWWW</div>
        <div id="div_nuevo_despacho" style="display:none;z-index:102; position:absolute; border: 1px solid #165FB6; background:#FFFFFF; padding: 3px 3px 3px 3px; width:1500px;">YYYYYYYYYYYYY</div>    
        <div id="div_visualizar_ocs" style="display:none;z-index:102; position:absolute; border: 1px solid #165FB6; background:#FFFFFF; padding: 3px 3px 3px 3px; width:1590px;">
            
            <table with='930' align="center" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF">

                <tr>
                    <td id="drag_visualizar_ocs" style="cursor:pointer">
                        <div class="k-header">
                            <span class="titulo">DETALLE ORDEN COMPRA o SERVICIO</span>
                            <span class="ui-widget-header ui-corner-all titulo_right" onClick="$('#div_visualizar_ocs').fadeOut('slow')">x</span>
                        </div>
                    </td>
                </tr>             
            
                <tr>
                    <td id="drag_visualizar_ocs" style="cursor:pointer">
                        <table id="show_detalle_ocs" class="table table-striped table-bordered"></table>
                    </td>
                </tr>             
            </table>    
        </div>
        

        <script language="JavaScript1.2">

            $(document).ready(
                    
                function(){
                    
                    maximizarventana();
                    OrdenesCS();
                    //$j("#div_visualizar_ocs").draggable({ handle: "#drag_visualizar_ocs"});
                    $j("#div_visualizar_ocs").draggable({ handle: "#div_visualizar_ocs"});
                    
                    $j("#div_editar_ocs").draggable({ handle: "#div_editar_ocs"});
                    
                    $j("#div_nuevo_despacho").draggable({ handle: "#div_nuevo_despacho"});

                }
            );

        </script>

</head>

<body>

    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=ORDENES DE COMPRA Y SERVICIOS"/>
    </div>

    <!-- ******************************************************************************************************* -->

    <div id="capaCentral" style="position:absolute; width:80%; height:85%; z-index:0; left: 170px; top: 100px; "> <!-- overflow: scroll; border-style: dotted-->

        <br>

        <center>

            <input type="hidden" id="baseurl" name="baseurl" value="<%=BASEURL%>">
            <input type="hidden" id="controller" name="controller" value ="<%=CONTROLLER%>"/>
            <input type="hidden" name="id_solicitudu" id="id_solicitudu" value="<%=num_solicitud%>" />
            <input type="hidden" name="NmUser" id="NmUser" value="<%=NmLogin%>" />
            <input type="hidden" id="paginaRecepcionMateriales" value="false" />

            <table width="110%" border="0" cellpadding="1" cellspacing="1" class="labels" align="left" >

                <tr>

                    <td width="75%" align="center">
                        <table width="200" border="0">
                            <tr>
                                <td width="100%">&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="100%">&nbsp;</td>
                            </tr>
                        </table>    
                    </td>

                    <td align="right">
                        <fieldset>

                            <legend class="legend">FILTROS</legend>

                            <table width="380" border="0" align="right" cellpadding="1" cellspacing="1" class="labels">
                                <tr>

                                    <td width="102" align="center"><fieldset>
                                            <legend>A&Ntilde;O</legend>
                                            <select name="ano" class="combo_60px" id="ano" onChange="OrdenesCS();"><%
                                                String cad = "";

                                                for (int j = 2015; j <= 2025; j++) {
                                                    int anio = j;
                                                    if (anio == acorriente) {
                                                      cad = "selected";
                                                  } else {
                                                      cad = "";
                                                  }%>
                                                <option value="<%=anio%>" <%=cad%> > <%=anio%> </option><%
                                                                }%>
                                            </select>
                                        </fieldset>
                                    </td>

                                    <td width="102" align="center"><fieldset>
                                            <legend>MES</legend>
                                            <select name="mes" class="combo_60px" id="mes" onChange="OrdenesCS();">

                                                <option value="0">< todos ></option><%

                                                    String meses = "";
                                                    String cadi = "";

                                                    for (int i = 1; i <= 12; i++) {
                                                        int value = i;
                                                        switch (value) {
                                                            case 1:
                                                                meses = "ENE";
                                                                break;
                                                            case 2:
                                                                meses = "FEB";
                                                                break;
                                                            case 3:
                                                                meses = "MAR";
                                                                break;
                                                            case 4:
                                                                meses = "ABR";
                                                                break;
                                                            case 5:
                                                                meses = "MAY";
                                                                break;
                                                            case 6:
                                                                meses = "JUN";
                                                                break;
                                                            case 7:
                                                                meses = "JUL";
                                                                break;
                                                            case 8:
                                                                meses = "AGO";
                                                                break;
                                                            case 9:
                                                                meses = "SEP";
                                                                break;
                                                            case 10:
                                                                meses = "OCT";
                                                                break;
                                                            case 11:
                                                                meses = "NOV";
                                                                break;
                                                            case 12:
                                                                meses = "DIC";
                                                                break;
                                                        }

                                                        if (value == mescorriente) {
                                                                    cadi = "selected";
                                                                } else {
                                                                    cadi = "";
                                                                }%>

                                                <option value="<%=value%>" <%=cadi%> > <%=meses%> </option><%

                                                                }%>
                                            </select>
                                        </fieldset>
                                    </td>


                                    <td width="102" align="center" ><fieldset>
                                            <legend>TIPO DE SOLICITUD</legend>
                                            <table border="0" cellpadding="1" cellspacing="1" class="labels">
                                                <tr>
                                                    <td align="right">
                                                        <select name="tiposolicitudu"  id="tiposolicitudu" class="combo_150px" onChange="OrdenesCS();">
                                                            <OPTION value='1' selected>ORDEN DE COMPRA</OPTION>
                                                            <OPTION value='2'>ORDEN DE SERVICIO</OPTION>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>

                                </tr>
                            </table>
                        </fieldset>
                    </td>


                </tr>  

                <tr>
                    <td width="1600" align="left" colspan="2">
                        <fieldset>
                            <legend class="legend">ORDENES DE COMPRAS Y SERVICIOS</legend> 

                            <div id="fondo4" style="position:relative; width:1800px; height:600px; visibility: visible; overflow:auto; left:0px; top:0px;" align="center">
                                <div id="misreq" align="center" style="position:absolute; z-index:58; left:0px; top:0px; visibility:visible;width:1560px;">

                                    <table id="OrdenesCS" class="table table-striped table-bordered"></table>

                                </div>
                            </div>

                        </fieldset>
                    </td>
                </tr>

                <div id="box" style="top:281px; left:417px; position:absolute; z-index:1000; width: 43px; height: 29px;"></div>

            </table>     


            <div id="contenido">
                <button id="actulizar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="parent.close()">
                    <span class="ui-button-text">Salir</span>
                </button>
            </div>


            <center class='comentario'>
                <div id="comentario" style="visibility: hidden" >
                    <table border="2" align="center">
                        <tr>
                            <td>
                                <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                    <tr>
                                        <td width="229" align="center" class="mensajes"><span class="normal"><div id="mensaje"></div></span></td>
                                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                        <td width="58">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </center> 


        </center>

    </div>

</body>

</html>
