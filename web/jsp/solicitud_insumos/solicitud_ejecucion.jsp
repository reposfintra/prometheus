<%-- 
    Document   : solicitud_ejecucion
    Created on : 4/09/2018, 08:58:55 AM
    Author     : ricardo santamaria porras
--%>

<%@page session="true" %>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@ page import="com.tsp.operation.model.beans.CmbGenericoBeans"%>
<%@ page import="com.tsp.operation.model.services.RequisicionesService"%>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="java.text.SimpleDateFormat" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="javax.servlet.*" %>
<%@ page import    ="javax.servlet.http.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String NmLogin = usuario.getLogin();
%>


<html>

<head>
  <title>Solicitar materiales</title>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <style>
    #draggable { width: 150px; height: 150px; padding: 0.5em; }
  </style>
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/dataTables.semanticui.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/semantic.min.js"></script>
  <script src="https://cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"></script>
  <script type="text/javascript" src="./js/solicitud_ejecucion.js"></script>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.4/semantic.min.css" rel="stylesheet" type="text/css" />
  <script src="https://cdn.rawgit.com/mdehoog/Semantic-UI-Calendar/76959c6f7d33a527b49be76789e984a0a407350b/dist/calendar.min.js"></script>
  <script type="text/javascript" src="./js/Semantic-UI-Alert.js"></script>
  <link href="./css/Semantic-UI-Alert.css" rel="stylesheet" type="text/css" />


  <style>
    td.details-control {
        cursor: pointer;
    }
    tr.shown td.details-control {
    }
    
    table.dataTable tbody tr.selected {
      background-color: #2a88c8b8  !important;
    }
    
    #draggable { width: 150px; height: 150px; padding: 0.5em; }
</style>
  <div class="ui mini  modal">
    <i class="close icon"></i>
    <div class="header">
      Elija la bodega donde están los materiales
    </div>
    <div class="content centered">
      <div class="description centered">
        <div class="ui form">
          <div class="field">
            <div class="ui calendar" id="example2">
              <label>Fecha esperada de entrega</label>
              <div class="ui input left icon">
                <i class="calendar icon"></i>
                <input id="fecha_esperada_entrega" type="text" placeholder="Fecha">
              </div>
              </div>
            </div>
            <div class="field">
              <label>Descripcion de la solicitud</label>
              <textarea id="observaciones" rows="2"></textarea>
            </div>
            <div class="field">
              <label>Seleccione bodega</label>
              <select id="select_bodega" class="ui dropdown">

              </select>
            </div>
          </div>
        </div>
      </div>
      <div class="actions">
        <div class="ui button">Cancelar</div>
        <div class="ui button" onclick="cargarItems(event)">Siguiente</div>
      </div>
    </div>
</head>

<body>
  <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
    <jsp:include page="/toptsp.jsp?encabezado=Solicitud de materiales" />
  </div>
  <!-- ******************************************************************************************************* -->

  <div id="capaCentral" style="position:absolute; width:80%; height:85%; z-index:0; left: 100px; top: 100px; ">
    <br>
    <div class="ui sizer vertical segment">
      <div class="ui huge header">Solicitudes de materiales</div>
      <p>Lista de solicitudes hechas por el usuario</p>
      <div class="ui vertical labeled icon buttons">
        <button class="ui button" onclick="cargarModalBodega(event)">
          <i class="plus icon"></i>
          Crear nueva solicitud
        </button>
      </div>
    </div>


    <table id="tabla_items2" class="display table table-striped table-bordered ui celled" style="width:100%"></table>

  </div>

  <div id="div_items" style="display:none; position:absolute; padding: 3px 3px 3px 3px; width:850px;">
    <table class="ui table">
      <tr>
        <td style="cursor:pointer">
          <div class="k-header ui clearing segment">
            <span class="titulo">Seleccione los materiales que va a solicitar</span>
            <button class="ui red button right floated" onClick="$('#div_items').fadeOut('slow')">
                       Cerrar
                    </button>
            <button class="ui primary button right floated" id="guardarDetalleSolicitud">
                       Guardar
                      </button>
          </div>
        </td>
      </tr>

      <tr>
        <td id="drag_items" style="cursor:pointer">
          <table id="show_items" class="table table-striped table-bordered"></table>
        </td>
      </tr>
    </table>
  </div>


</body>
<script>
  $('.ui.mini.modal').modal();
</script>

</html>

<style>
  #draggable {
    width: 150px;
    height: 150px;
    padding: 0.5em;
  }
    .visible.transition {
      top: 20%;
    }  
</style>

<script>
  $(function() {
    $("#div_items").draggable();
  });

  $('#example2').calendar({
    type: 'date'
  });
</script>