<%--
    Document   : Inventario
    Created on : 30/05/2018, 10:00:00 AM
    Author     : ricardo santamaria porras
--%>

<%@page session="true" %>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@ page import="com.tsp.operation.model.beans.CmbGenericoBeans"%>
<%@ page import="com.tsp.operation.model.services.RequisicionesService"%>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="java.text.SimpleDateFormat" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="javax.servlet.*" %>
<%@ page import    ="javax.servlet.http.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String NmLogin = usuario.getLogin();
%>


<html>

<head>
  <title>INVENTARIO</title>

  <!-------------------------------------EstilosBase----------------------------------------->
  <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/estilostsp.css">
  <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/style_azul.css">
  <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/jscal2.css" />
  <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/border-radius.css" />
  <link type="text/css" rel="stylesheet" href="./css/main2.css">
  <!-------------------------------------EstilosBase----------------------------------------->

  <!-------------------------------------jQeury----------------------------------------->
  <script type="text/javascript" src="<%=BASEURL%>/js/jquery-1.4.2.min.js"></script>
  <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/jquery-ui-1.8.5.custom.min.js"></script>
  <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/jquery-latest.js"></script>
  <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
  <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>
  <!-------------------------------------jQeury----------------------------------------->

  <!-------------------------------------Tools----------------------------------------->
  <script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
  <script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>
  <script type="text/javascript" src="./js/visualizar_inventario.js"></script>
  <!-------------------------------------Tools----------------------------------------->

  <!-------------------------------------dataTable----------------------------------------->
  <script type="text/javascript" src="./data_table/js/jquery-1.12.4.js"></script>
  <script type="text/javascript" src="./data_table/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="./data_table/js/dataTables.bootstrap.min.js"></script>
  <!-------------------------------------dataTable----------------------------------------->

  <!-------------------------------------dataTable-BootsTrap----------------------------------------->
  <link type="text/css" rel="stylesheet" href="./data_table/css/bootstrap.min.css">
  <link type="text/css" rel="stylesheet" href="./data_table/css/dataTables.bootstrap.min.css">
  <!-------------------------------------dataTable-BootsTrap----------------------------------------->


  <!-------------------------------------Smoke----------------------------------------->
  <script src="./js/smoke/smoke.min_old.js"></script>
  <link href="./js/smoke/smoke.css" rel="stylesheet" type="text/css" />
  <!-------------------------------------Smoke----------------------------------------->


  <!-------------------------------------Confirm----------------------------------------->
  <script type="text/javascript" src="./js/jquery/jquery-confirm/dist/jquery-confirm.min.js"></script>
  <link type="text/css" rel="stylesheet" href="./js/jquery/jquery-confirm/dist/jquery-confirm.min.css" />
  <!-------------------------------------Confirm----------------------------------------->

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
<link href="http://cdnjs.cloudflare.com/ajax/libs/semantic-ui/0.19.3/css/semantic.css" rel="stylesheet"/>
<link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.1/components/icon.min.css'>
<script src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.3.3/semantic.min.js"></script>

<link type="text/css" rel="stylesheet" href="./css/toastr.min.css"/>
<script type="text/javascript" src="./js/toastr.min.js"></script>

  </head>
<div class="modal-semantic"/> 
<body>
  <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
    <jsp:include page="/toptsp.jsp?encabezado=INVENTARIO" />
  </div>
  <!-- ******************************************************************************************************* -->
  <div id="capaCentral" style="position:absolute; width:80%; height:85%; z-index:0; left: 100px; top: 100px; ">
    <br>
    <div class="ui sizer vertical segment">
  <div class="ui huge header">Kardex</div>
  <p>Lista de materiales disponibles por bodega</p>
</div>

    <center>

                  
        <table id="tabla_items2" class="display table table-striped table-bordered" style="width:100%">
        </table>        
    </center>
  </div>     
    
</body>

</html>