<fieldset id="HeadTable">
  <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels" with='1500'>
    <tr>
      <td width="850" align="center">
        <table border="0" cellpadding="2" cellspacing="2">
          <tr>
            <td align="right">FECHA DE TRANSACCION: &nbsp;</td>
            <td align="left"><input name="fecha_transaccion" type="datetime-local" class="input_200px" id="fecha_transaccion" value="${param.fecha}"></td>
            <td align="right">&nbsp;</td>
          </tr>

          <tr>
            <td align="right">USUARIO: &nbsp;</td>
            <td align="left"><input name="usuario" type="text" class="input_200px" id="usuario" value="${param.usuario}"readonly="true"></td>
            <td align="right">&nbsp;</td>
          </tr>

          <tr>
            <td align="right">TIPO DE MOVIMIENTO: &nbsp;</td>
            <td align="left">
              <select id="cbx_tipo_movimiento" class="input_200px" />              
            </td>
          </tr>
        </table>
      </td>
      <td width="850" align="center">
        <table border="0" cellpadding="4" cellspacing="4">
          <tr>
          </tr>
          <tr>
            <td align="right">RESPONSABLE: &nbsp;</td>
            <td><input name="responsable" type="text" class="input_200px" id="responsable" placeholder="Escriba el responsable"></td>
          </tr>
          <tr>
            <td align="right">DESCRIPCION: &nbsp;</td>
            <td><textarea name="descripcion" class="input_200px" id="descripcion"></textarea></td>
            <td align="right">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="2" align="center">
              <table border="0" cellpadding="2" cellspacing="2">
                <tr>
                  <td width="100%">
                    <ul class="tab">
                    </ul>
                  </td>
                </tr>
              </table>
            </td>
          </tr>
        </table>
      </td>
    </tr>
    <tr id="include_e_s_t">
        
    </tr>
  </table>
</fieldset>