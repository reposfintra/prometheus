<%--
    Document   : Modulo de Compras y Servicios.
    Created on : 10/02/2017, 11:00:00 AM
    Author     : hcuello
--%>

<%@page session="true" %>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@ page import="com.tsp.operation.model.beans.CmbGenericoBeans"%>
<%@ page import="com.tsp.operation.model.services.RequisicionesService"%>


<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="java.text.SimpleDateFormat" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="javax.servlet.*" %>
<%@ page import    ="javax.servlet.http.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>



<%    
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String NombreUsuario = usuario.getNombre();
    String NmLogin = usuario.getLogin();

    RequisicionesService rqservice = new RequisicionesService(usuario.getBd());
    ArrayList listaCombo = rqservice.GetComboGenerico("SQL_OBTENER_COMBO_PROCESOS_SGC", "id", "descripcion", "");

    Date dNow = new Date();

    SimpleDateFormat nd = new SimpleDateFormat("yyyy");
    String aniocte = nd.format(dNow);

    SimpleDateFormat md = new SimpleDateFormat("M");
    String mescte = md.format(dNow);

    int acorriente = Integer.parseInt(aniocte);
    int mescorriente = Integer.parseInt(mescte);
    
    String num_solicitud = request.getParameter("num_solicitud") != "" ? request.getParameter("num_solicitud") : "";

%>



<html>
    
    <head>

        <title>SOLICITUD DE COMPRAS Y SERVICIOS</title>

        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/estilostsp.css" >
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/style_azul.css" >
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/jquery/jquery-ui/jquery-ui.css"/>
        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/jscal2.css" />
        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/border-radius.css" />        

        <script type="text/javascript" src="<%=BASEURL%>/js/jquery-1.4.2.min.js"></script>
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/jquery-latest.js"></script>
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>  
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>

        <!---->
        <script type="text/javascript" src="./js/compras_proceso.js"></script>
        <script type="text/javascript" src="./js/utilidadInformacion.js"></script>
        <!---->
        
        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>


        <script type="text/javascript" src="./data_table/js/jquery-1.12.4.js"></script>
        <script type="text/javascript" src="./data_table/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="./data_table/js/dataTables.bootstrap.min.js"></script>


        <link type="text/css" rel="stylesheet" href="./data_table/css/bootstrap.min.css" >
        <link type="text/css" rel="stylesheet" href="./data_table/css/dataTables.bootstrap.min.css" >
        
        <script src="./js/smoke/smoke.min_old.js"></script>
        <link href="./js/smoke/smoke.css" rel="stylesheet" type="text/css" />

        <link type="text/css" rel="stylesheet" href="./css/main2.css">

        <div id="div_espera" style="display:none;z-index:1000; position:absolute"></div>
        <div id="div_nueva_solicitud" style="display:none;z-index:102; position:absolute; border: 1px solid #165FB6; background:#FFFFFF; padding: 3px 3px 3px 3px; width:1550px;">WWWWWWWWWWWWWW</div>
        <div id="div_visualizar_solicitud" style="display:none;z-index:103; position:absolute; border: 2px solid #165FB6; background:#FFFFFF; padding: 3px 3px 3px 3px; width:1480px;">
            <table with='1080' align="center" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF">

                <tr>
                    <td id="drag_nueva_requisicion" style="cursor:pointer" width="100%">
                        <div class="k-header">
                            <span class="titulo">Visualizar Elementos Solicitados</span>
                            <span class="ui-widget-header ui-corner-all titulo_right" onClick="$('#div_visualizar_solicitud').fadeOut('slow')">x</span>
                        </div>
                    </td>
                </tr>             
            
                <tr>
                    <td id="drag_nueva_requisicion" style="cursor:pointer" width="100%">
                        <table id="show_solicitud" class="table table-striped table-bordered"></table>
                    </td>
                </tr>             
            </table>   
        </div>
        
        <div id="div_InsumosApu" style="display:none;z-index:105; position:absolute; border: 1px solid #165FB6; background:#FFFFFF; padding: 3px 3px 3px 3px; width:1500px;">WWWWWWWWWWWWWW</div>
        
        <div id="div_CatalogoInsumos" style="display:none;z-index:105; position:absolute; border: 1px solid #165FB6; background:#FFFFFF; padding: 3px 3px 3px 3px; width:1500px;">
            <table with='1080' align="center" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF">

                <tr>
                    <td style="cursor:pointer" width="100%">
                        <div class="k-header">
                            <span class="titulo">Adicionar Elemento</span>
                            <span class="ui-widget-header ui-corner-all titulo_right" onClick="$('#div_CatalogoInsumos').fadeOut('slow')">x</span>
                        </div>
                    </td>
                </tr>             
            
                <tr>
                    <td width="100%">
                        <table id="show_solicitud" class="table table-striped table-bordered">
                            <tr>
                                <td width="100%"></td>
                                <td width="100%"></td>
                                <td width="100%"></td>
                                <td width="100%"></td>
                                <td width="100%"></td>
                            </tr>    
                        </table>
                    </td>
                </tr>             
            </table>            
        </div>

        <script language="JavaScript1.2">
            $(document).ready(

                function(){

                    maximizarventana();
                    EstadoSolicitudes();

                    $j("#div_nueva_solicitud").draggable({ handle: "#drag_detalle_requisicion"});
                    $j("#div_asignar_responsable").draggable({ handle: "#drag_asignar_responsable"});

                    $j("#div_visualizar_solicitud").draggable({ handle: "#drag_visualizar_solicitud"});
                    
                    $j("#div_InsumosApu").draggable({ handle: "#drag_InsumosApu"});
                    
                    $j("#div_CatalogoInsumos").draggable({ handle: "#drag_CatalogoInsumos"});
                    

                    cargando_toggle();

                }
            );
        </script>    
    
    </head>

<body>

    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=SOLICITUDES DE COMPRAS Y SERVICIOS"/>
    </div>

    <!-- ******************************************************************************************************* -->

    <div id="capaCentral" style="position:absolute; width:80%; height:85%; z-index:0; left: 170px; top: 100px; "> <!-- overflow: scroll; border-style: dotted-->

        <br>

        <center>

            <input type="hidden" id="baseurl" name="baseurl" value="<%=BASEURL%>">
            <input type="hidden" id="controller" name="controller" value ="<%=CONTROLLER%>"/>
            <input type="hidden" name="id_solicitudu" id="id_solicitudu" value="<%=num_solicitud%>" />
            <input type="hidden" name="NmUser" id="NmUser" value="<%=NmLogin%>" />

            <table width="100%" border="0" cellpadding="1" cellspacing="1" class="labels" align="center" >

                <tr>

                    <td width="100%" align="center">
                        <table width="600" border="0">
                            <tr>
                                <td width="100%">&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="100%">
                                    <table>
                                        <tr width="100%">
                                            <td width="100%">
                                                <ul class="tab">
                                                    <li><a href="javascript:void(0)" class="tablinks" onclick="ClearTable('example',0)"><img src='./images/All.png' height="20" width="20">  Todos</a></li>
                                                    <li><a href="javascript:void(0)" class="tablinks" onclick="ClearTable('example',1)"><img src='./images/Terminados.png' height="20" width="20">  Enviados</a></li>
                                                    <li><a href="javascript:void(0)" class="tablinks" onclick="ClearTable('example',3)"><img src='./images/Confirmados.png' height="20" width="20">  Terminados</a></li>
                                                    <li><a href="javascript:void(0)" class="tablinks" onclick="ClearTable('example',2)"><img src='./images/Descartados.png' height="20" width="20">  Rechazados</a></li>
                                                </ul>
                                            </td>
                                        </tr>
                                    </table>    
                                </td>
                            </tr>
                        </table>    
                    </td>

                    <td align="right">
                        <fieldset>

                            <legend class="legend">FILTROS</legend>

                            <table width="850" border="0" align="right" cellpadding="1" cellspacing="1" class="labels">
                                <tr>

                                    <td width="50"></td>

                                    <td width="102"><fieldset>
                                            <legend>A&Ntilde;O</legend>
                                            <select name="ano" class="combo_60px" id="ano" onChange="ClearTable('example',0);"><%
                                                String cad = "";

                                                for (int j = 2015; j <= 2025; j++) {
                                                    int anio = j;
                                                    if (anio == acorriente) {
                                                      cad = "selected";
                                                  } else {
                                                      cad = "";
                                                  }%>
                                                <option value="<%=anio%>" <%=cad%> > <%=anio%> </option><%
                                                                }%>
                                            </select>
                                        </fieldset>
                                    </td>

                                    <td width="102"><fieldset>
                                            <legend>MES</legend>
                                            <select name="mes" class="combo_60px" id="mes" onChange="ClearTable('example',0);">

                                                <option value="0">< todos ></option><%

                                                    String meses = "";
                                                    String cadi = "";

                                                    for (int i = 1; i <= 12; i++) {
                                                        int value = i;
                                                        switch (value) {
                                                            case 1:
                                                                meses = "ENE";
                                                                break;
                                                            case 2:
                                                                meses = "FEB";
                                                                break;
                                                            case 3:
                                                                meses = "MAR";
                                                                break;
                                                            case 4:
                                                                meses = "ABR";
                                                                break;
                                                            case 5:
                                                                meses = "MAY";
                                                                break;
                                                            case 6:
                                                                meses = "JUN";
                                                                break;
                                                            case 7:
                                                                meses = "JUL";
                                                                break;
                                                            case 8:
                                                                meses = "AGO";
                                                                break;
                                                            case 9:
                                                                meses = "SEP";
                                                                break;
                                                            case 10:
                                                                meses = "OCT";
                                                                break;
                                                            case 11:
                                                                meses = "NOV";
                                                                break;
                                                            case 12:
                                                                meses = "DIC";
                                                                break;
                                                        }

                                                        if (value == mescorriente) {
                                                                    cadi = "selected";
                                                                } else {
                                                                    cadi = "";
                                                                }%>

                                                <option value="<%=value%>" <%=cadi%> > <%=meses%> </option><%

                                                                }%>
                                            </select>
                                        </fieldset>
                                    </td>


                                    <td width="102"  ><fieldset>
                                            <legend>TIPO DE SOLICITUD</legend>
                                            <table border="0" align="right" cellpadding="1" cellspacing="1" class="labels">
                                                <tr>
                                                    <td align="right">
                                                        <select name="tiposolicitudu"  id="tiposolicitudu" class="combo_150px" onChange="ClearTable('example',0);">
                                                            <OPTION value='0' selected>< todos ></OPTION>
                                                            <OPTION value='1'>ORDEN DE COMPRA</OPTION>
                                                            <OPTION value='2'>ORDEN DE SERVICIO</OPTION>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>


                                    <td width="250"  ><fieldset>
                                            <legend>PROYECTO</legend>
                                            <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                                <tr>
                                                    <td align="center"><input type="text" id="project_name" name="project_name" value="" class="input_270x29px" readonly="">
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>



                                    <td align="right">
                                        <table>
                                            <tr>
                                                <td>
                                                    <button class="btn" id="NewSolicitude" onClick="CrearEditarSolicitud('NUEVO', '', event)">Nueva Solicitud</button>
                                                </td>
                                            </tr>    
                                        </table>
                                    </td>                                     

                                </tr>
                            </table>
                        </fieldset>
                    </td>


                </tr>  

                <tr>
                    <td width="1600" align="center" colspan="2">
                        <fieldset>
                            <legend class="legend">SOLICITUDES DE COMPRAS Y SERVICIOS</legend> 

                            <div id="fondo4" style="position:relative; width:1610px; height:600px; visibility: visible; overflow:auto; left:0px; top:0px;" align="center">
                                <div id="misreq" align="center" style="position:absolute; z-index:58; left:0px; top:0px; visibility:visible;width:1540px;">

                                    <table id="example" class="table table-striped table-bordered"></table>

                                </div>
                            </div>

                        </fieldset>
                    </td>

                </tr>

                <div id="box" style="top:281px; left:417px; position:absolute; z-index:1000; width: 43px; height: 29px;"></div>

            </table>     


            <div id="contenido">
                <button id="actulizar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="parent.close()">
                    <span class="ui-button-text">Salir</span>
                </button>
            </div>


            <center class='comentario'>
                <div id="comentario" style="visibility: hidden" >
                    <table border="2" align="center">
                        <tr>
                            <td>
                                <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                    <tr>
                                        <td width="229" align="center" class="mensajes"><span class="normal"><div id="mensaje"></div></span></td>
                                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                        <td width="58">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </center> 

        </center>

    </div>

</body>
<div id="loader-wrapper">
    <div id="loader"></div>
    <img id="logowast" src="/fintra/images/Selectrik.png" style="z-index: 10002 !important">
    <div class="loader-section section-left">	
    </div>
    <div class="loader-section section-right"></div>
</div>
</html>
