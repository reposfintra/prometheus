<%-- 
    Document   : formulario_movimiento_inventario.jsp
    Created on : Apr 30, 2018, 11:00:02 AM
    Author     : rsantamaria        
--%>

<%@ page session="true" %>
<%@ page import="com.tsp.operation.model.beans.Usuario"%>
<%@ page import="com.tsp.operation.model.beans.CmbGenericoBeans"%>
<%@ page import="com.tsp.operation.model.beans.RequisicionesListadoBeans"%>
<%@ page import="com.tsp.operation.model.beans.ComprasProcesoBeans"%>
<%@ page import="com.tsp.operation.model.services.RequisicionesService"%>
<%@ page import="com.tsp.operation.model.services.ComprasProcesoService"%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="java.text.SimpleDateFormat" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<% String fecha = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm").format(new Date()); %>
<% String usuario = ((Usuario) session.getAttribute("Usuario")).getNombre(); %>

<script type="text/javascript" src="<%=BASEURL%>/js/jquery-1.4.2.min.js"></script>
<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/jquery-latest.js"></script>
<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/jquery-ui-1.8.5.custom.min.js"></script>
<script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>  
<script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>

<!-------------------------------------Notificaciones----------------------------------------->
<link type="text/css" rel="stylesheet" href="./css/toastr.min.css"/>
<script type="text/javascript" src="./js/toastr.min.js"></script>
<!-------------------------------------/Notificaciones----------------------------------------->

<!---->
<script type="text/javascript" src="./js/compras_proceso.js"></script>
<script type="text/javascript" src="./js/utilidadInformacion.js"></script>
<script type="text/javascript" src="./js/formulario_movimiento_inventario.js"></script>
<!---->

<script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
<script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>


<script type="text/javascript" src="./data_table/js/jquery-1.12.4.js"></script>
<script type="text/javascript" src="./data_table/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="./data_table/js/dataTables.bootstrap.min.js"></script>


<div id="div_espera" style="display:none;z-index:1000; position:absolute"></div>
<form name="form_nueva_rq" id="form_nueva_rq" method="post" enctype="multipart/form-data" target="iframeUpload">
  <table with='1500' align="center" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF">
    <tr>
      <td id="drag_nueva_ocs" style="cursor:pointer">
        <div class="k-header">
          <span class="titulo">REGISTRAR TRASPASO</span>
          <span class="ui-widget-header ui-corner-all titulo_right" onClick="$('#div_nueva_ocs').fadeOut('slow'); location.reload();">x</span>
        </div>
      </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>
        <h1 id="titulo_tipo_movimiento" align="center" style="color: black;"></h1>
        <jsp:include page="cabecera_movimiento.jsp">            
            <jsp:param name="fecha" value="<%=fecha%>" />
            <jsp:param name="usuario" value="<%=usuario%>" />
        </jsp:include>
      </td>
    </tr>
    <tr>
      <td>
          <jsp:include page="detalle_movimiento.jsp" />       
      </td>
    </tr>
  </table>      
  <button type="button" id="btn_guardar_movimiento" class="btn btn-primary btn-lg boton-centrado">GUARDAR</button>
</form>
      
      
<div id="loader-wrapper">
    <div id="loader"></div>
    <img id="logowast" src="/fintra/images/Selectrik.png" style="z-index: 10002 !important">
    <div class="loader-section section-left">	
    </div>
    <div class="loader-section section-right"></div>
</div>                    
