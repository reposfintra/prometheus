<%-- 
    Document   : maestro_polizas
    Created on : 23/08/2018, 10:00:07 AM
    Author     : jbermudez
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>MAESTRO POLIZAS</title>
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link href="./css/maestro_polizas.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>
    </head>
    <body>
        <div id="capaSuperior" style="height:100px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=MAESTRO POLIZAS"/>
        </div>
        <div class="wrappers" id="buttons-wrapper">
            <button id="btn_aseguradora" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                <span class="ui-button-text">Aseguradoras</span>
            </button>            
            <button id="btn_tipo_cobro" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                <span class="ui-button-text">Tipo cobro</span>
            </button>
            <button id="btn_valor_poliza" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                <span class="ui-button-text">Valor p�liza</span>
            </button>
            <button id="btn_polizas" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                <span class="ui-button-text">P�lizas</span>
            </button>
        </div>
        <div id="tables-wrapper">
            <div class="table-wrapper" id="aseguradoras-wrapper">
                <table id="tabla_aseguradoras" ></table>
                <div id="pager_aseguradoras"></div>
            </div>
            <div class="table-wrapper" id="polizas-wrapper">
                <table id="tabla_Polizas" ></table>
                <div id="pager_Polizas"></div>
            </div>
            <div class="table-wrapper" id="tipo-cobro-wrapper">
                <table id="tabla_tipo_cobro" ></table>
                <div id="pager_tipo_cobro"></div>
            </div>
            <div class="table-wrapper" id="valor-poliza-wrapper">
                <table id="tabla_valor_poliza" ></table>
                <div id="pager_valor_poliza"></div>
            </div>
        </div>        
        <div id="dialogPolizas"  class="ventana">
            <div class="tabla-interna-wrapper">
                <table>
                    <tr>
                    <input type="text" id="id_poliza" hidden>
                    <td>
                        <label>Nombre poliza</label>
                    </td>
                    <td>
                        <input type="text" id="descripcion_poliza"  maxlength="50">
                    </td>
                    </tr>                    
                </table>
            </div>                               
        </div>
        <div id="dialogAseguradoras"  class="ventana">
            <div class="tabla-interna-wrapper">
                <table>                   
                    <tr>
                        <td>
                            <input type="hidden" id="id_aseguradora">
                            <label for="descripcion_aseguradora">Nombre</label>
                        </td> 
                        <td>
                            <input type="text" id="descripcion_aseguradora" maxlength="50">
                        </td>                         
                    </tr>
                    <tr>
                        <td>
                            <label for="nit_aseguradora">Nit</label>
                        </td>
                        <td>
                            <input type="text" id="nit_aseguradora"  maxlength="9">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="retorno_aseguradora">Retorno</label>
                        </td> 
                        <td>
                            <input type="text" id="retorno_aseguradora" maxlength="3" placeholder="Porcentaje de retorno"><span style="color: red"> %</span>
                        </td> 
                    </tr>
                    <tr>
                        <td>
                            <label for="plazo_pago_aseguradora">Plazo pago</label>
                        </td> 
                        <td>
                            <input type="text" id="plazo_pago_aseguradora" placeholder="Numero de d�as">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="prr_aseguradora">Plazo recuperacion retorno</label>
                        </td>
                        <td>
                            <input type="text" id="prr_aseguradora" placeholder="Numero de d�as">
                        </td>
                    </tr>
                </table>
            </div>            
        </div>       
        <div id="dialogTipoCobro"  class="ventana">
            <div class="tabla-interna-wrapper">
                <table>
                    <input type="text" id="id_tipo_cobro" hidden="true">
                    <tr>
                        <td>
                            <label>Descripcion</label>
                        </td> 
                        <td>
                            <input type="text" id="descripcion_tipo_cobro"  maxlength="50">
                        </td> 
                    <tr>
                        <td>
                            <label for="tipo_tipo_cobro">Tipo</label>
                        </td> 
                        <td>
                            <select id="tipo_tipo_cobro">
                                <option value="A">Cobro anticipado</option>
                                <option value="C">Cobro a cuota</option>
                            </select>
                        </td>                         
                    </tr>
                    <tr>
                        <td>
                            <label for="financiacion_tipo_cobro">Financiacion</label>
                        </td>
                        <td>
                            <select id="financiacion_tipo_cobro" disabled>
                                <option value="N" selected>No</option>
                                <option value="S">Si</option>
                            </select>
                        </td>                         
                    </tr>
                </table>
            </div>            
        </div>
        <div id="dialogValorPoliza"  class="ventana">
            <div class="tabla-interna-wrapper">
                <table>
                    <tr>
                        <td>
                            <input type="text" id="id_valor_poliza" style="width: 137px;color: black;" hidden>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="descripcion_valor_poliza">Descripcion</label>
                        </td> 
                        <td>
                            <input type="text" id="descripcion_valor_poliza" maxlength="50">
                        </td> 
                    </tr>
                    <tr>
                        <td>
                            <label for="tipo_valor_poliza">Tipo</label>
                        </td>
                        <td>
                            <select id="tipo_valor_poliza">
                                <option value="P">Porcentaje</option>
                                <option value="A">Valor Absoluto</option>
                            </select>
                        </td>
                    </tr>
                    <tr id="fila-valor-absoluto">
                        <td>
                            <label for="valor_absoluto_valor_poliza">Valor(ej.1000) <span style="color: red">$</span></label>
                        </td> 
                        <td>                            
                            <input id="valor_absoluto_valor_poliza" type="text">                                
                        </td>
                    </tr>
                    <tr id="fila-valores">
                        <td>                            
                            <input id="valor_solicitado_valor_poliza" type="radio" name="valores" value="K">Valor solicitado
                        </td> 
                        <td>                            
                            <input id="valor_cartera_valor_poliza" type="radio" name="valores" value="KI">Valor cartera
                        </td>
                    </tr>
                    <tr id="fila-porcentaje">
                        <td>
                            <label for="valor_porcentaje_valor_poliza">Valor (ej. 2%)</label>
                        </td>
                        <td>                            
                            <input id="valor_porcentaje_valor_poliza" type="text" maxlength="5"><span style="color: red"> %</span>
                        </td>                         
                    </tr>
                    <tr>
                        <td>
                            <label for="iva_valor_poliza">Incluye IVA</label>
                        </td>
                        <td>
                            <input class="auto-width no-margin" type="checkbox" id="iva_valor_poliza">
                        </td>
                    </tr>
                </table>
            </div>            
        </div>
        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>
        <script type="text/javascript" src="./js/maestro_polizas.js"></script> 
    </body>
</html>