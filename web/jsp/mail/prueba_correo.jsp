<!--- Autor      : Ing. Ivan Morales- Date       : 2009
--Descripcion : Vista que permite probar correo
-->
<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
        <title>Mailing</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
		<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script> 			
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">

<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    %>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Mail"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>
   <%      
      String  msj       = request.getParameter("msj");%>      

   <form action="<%=CONTROLLER%>?estado=Envio&accion=Correo&opcion=enviarmsg" method='post' name='formulario' > 
   <table width="450" border="2" align="center">
       <tr>
          <td>  
               <table width='100%' align='center' class='tablaInferior'>

                  <tr class="barratitulo">
                    <td colspan='2' >
                       <table cellpadding='0' cellspacing='0' width='100%'>
                         <tr>
                          <td align="left" width='65%' class="subtitulo1">&nbsp;Mail</td>
                          <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"  height="20" align="left"><%=datos[0]%></td>
                        </tr>
                       </table>
                    </td>
                 </tr>
                 

                 <tr  class="fila">
                       <td colspan='2' >
                       
                          <table class='tablaInferior' width='100%'>
                               <tr  class="fila">                                  
                                   <td             > to                                      </td>
                                   <td             > <input type='text' name='to' title='to'  size='50' value="imorales@fintravalores.com"> </td>
                               </tr>
							   <tr  class="fila">                                  
                                   <td             > copyto</td>
                                   <td             > <input type='text' name='copyto' title='copyto'  size='50' value=""> </td>
                               </tr>
							   <tr  class="fila">                                  
                                   <td             > hiddencopyto</td>
                                   <td             > <input type='text' name='hiddencopyto' title='hiddencopyto'  size='50' value=""> </td>
                               </tr>
							   <tr  class="fila">                                  
                                   <td             > from</td>
                                   <td             > <input type='text' name='from' title='from'  size='50' value="imorales@fintravalores.com"> </td>
                               </tr>
							   <tr  class="fila">                                  
                                   <td             > asunto       </td>
                                   <td             > <input type='text' name='asunto' title='asunto'  size='50' value="prueba"> </td>
                               </tr>
							   <tr  class="fila">                                  
                                   <td             > mensaje       </td>
                                   <td             > <!--<input type='text' name='mensajex' title='mensajex'  size='50' value="mensaje cualquiera"> -->
								   	<textarea  name="mensajex" rows="5">mensaje cualquiera</textarea>
								   </td>
                               </tr>
							 
							   
                          </table>
                       
                       </td>
                 </tr>
                 
                 

           </table>
         </td>
      </tr>
   </table> 
  
   <br>                 
   <img src="<%=BASEURL%>/images/botones/aceptar.gif"    height="21"  title='Enviar' onclick='this.style.visibility="hidden";formulario.submit();'   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
   <img src="<%=BASEURL%>/images/botones/salir.gif"      height="21"  title='Salir'      onClick="window.close();"     onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
   
   <!-- MENSAJE -->  
   <% if(msj!=null  &&  !msj.equals("") ){%>
        <br><br>
        <table border="2" align="center">
              <tr>
                <td>
                    <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                          <tr>
                                <td width="450" align="center" class="mensajes"><%= msj %></td>
                                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="58">&nbsp; </td>
                          </tr>
                     </table>
                </td>
              </tr>
        </table>  
                
    <%}%>

   
   
   
   
</div>
<!-- Necesario para los calendarios-->
<iframe width="188" height="166" name="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" id="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" src="<%=BASEURL%>/js/calendartsp/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"></iframe>


<%=datos[1]%>  


</body>
</html>
