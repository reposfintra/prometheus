<%-- 
    Document   : FormalizarLibranza
    Created on : 13/04/2016, 03:18:15 PM
    Author     : egonzalez
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/MaestroLibranza.js"></script> 
        <script type="text/javascript" href="http://www.ddginc-usa.com/spanish/editcheck.js"></script>
        <title>FORMALIZAR LIBRANZA</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=FORMALIZAR LIBRANZA"/>
        </div>

    <center>
        <div style="position: relative;top: 231px;">
            <table id="tabla_FormalizarLib" ></table>
            <div id="pager"></div>
        </div>
        <div id="dialogMsConcepto"  class="ventana" >
            <div id="tablainterna" style="width: 450px" >
                <table id="tablainterna" >
                    <tr>
                        <td colspan="2">
                            <input type="text" id="negocio"  style="height: 20px;width: 137px;color: #070708;font-size: 13px;"readonly hidden >
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label style="font-family: Tahoma,Arial;">Usuario</label>
                        </td>
                        <td>
                            <input type="text" id="usuario"  style="height: 20px;width: 137px;color: #070708;font-size: 13px;"readonly >
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label style="font-family: Tahoma,Arial;">Fecha</label>
                        </td>
                        <td>
                            <input type="text" id="fecha"  style="height: 20px;width: 137px;color: #070708;font-size: 13px;"readonly >
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label style="font-family: Tahoma,Arial;">Decision</label>
                        </td>
                        <td>
                            <input type="text" id="decision"  style="height: 20px;width: 137px;color: #070708;font-size: 13px;" readonly>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label style="font-family: Tahoma,Arial;">Comentarios</label>
                        </td>
                        <td>
                            <textarea id="comentarios" style="width: 350px;height: 79px;" readonly></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div id="encabezado2tablita">
                                <label class="titulo2tablita"><b>CONCEPTO Y COMENTARIOS</b></label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label style="font-family: Tahoma,Arial;">Concepto</label>
                        </td>
                        <td>
                            <select id="concepto"style="width: 151px;color: #050505;height: 20px;font-size: 12px;"></select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label style="font-family: Tahoma,Arial;">Comentarios</label>
                        </td>
                        <td>
                            <textarea id="coment" style="width: 350px;height: 79px;"></textarea>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>
        
        <div id="dialogMsFormalizarLib">
            <table id="tabla_Valores" ></table>
            <div id="pager1"></div>
        </div>
    </center>
</body>
<script>
    $(document).ready(function () {
        inicio7();
    });

</script>
</html>
