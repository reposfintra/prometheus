<%-- 
    Document   : SalarioMinimo
    Created on : 30/03/2016, 11:34:20 AM
    Author     : egonzalez
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/MaestroLibranza.js"></script> 

        <title>DEDUCCIONES LIBRANZAS</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=DEDUCCIONES LIBRANZAS"/>
        </div>
        <style>
            .onoffswitch-inner:before {
                content: "A";
                padding-left: 14px;
                background-color: #34A7C1; color: #FFFFFF;
            }
            .onoffswitch-inner:after {
                content: "I";
                padding-right: 11px;
                background-color: #C20F0F; color: #F5F0F0;
                text-align: right;
            }
        </style>
    <center>
        <div style="position: relative;top: 231px;">
            <table id="tabla_DeduccionesLib" ></table>
            <div id="pager"></div>
        </div>
        <div id="dialogDeduccionesLib"  class="ventana" >
            <div id="tablainterna" style="width: 299px;" >
                <table id="tablainterna"  >
                    <tr>
                        <td>
                            <input type="text" id="id" style="width: 137px;color: black;" hidden>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Ocuapcion laboral</label>
                        </td>
                        <td>
                            <select id="ocupacion_laboral" style="width: 144px"></select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Operacion bancaria</label>
                        </td>
                        <td>
                            <select id="operacion_bancaria" style="width: 144px"></select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Descripcion</label>
                        </td>
                        <td>
                            <input type="text" id="descripcion" style="width: 137px;color: black;"onchange="conMayusculas(this)">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Desembolso inicial</label>
                        </td>
                        <td>
                            <input type="text" id="desembolso_inicial" style="width: 137px;color: black;"onkeypress="return onKeyDecimal(event, this)">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Desembolso final</label>
                        </td>
                        <td>
                            <input type="text" id="desembolso_final" style="width: 137px;color: black;" onkeypress="return onKeyDecimal(event, this)">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Valor cobra</label>
                        </td>
                        <td>
                            <input type="text" id="valor_cobrar" style="width: 137px;color: black;" onkeypress="return onKeyDecimal(event, this)" onkeyup="changeStatusCobraValues(event,this.id,this.value)" >
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>% cobrar</label>
                        </td>
                        <td>
                            <input type="text" id="perc_cobrar" style="width: 137px;color: black;" onkeypress="return onKeyDecimal(event, this)" onkeyup="changeStatusCobraValues(event,this.id,this.value)">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>n_xmil</label>
                        </td>
                        <td>
                            <input type="text" id="n_xmil" style="width: 137px;color: black;" onkeypress="return onKeyDecimal(event, this)" onkeyup="changeStatusCobraValues(event,this.id,this.value)">
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>
    </center>
</body>
<script>
    inicio6();
    $(document).ready(function () {
        $('.solo-numero').keyup(function () {
            this.value = (this.value + '').replace(/[^0-9]/g, '');
        });

        $('.solo-numeric').live('blur', function (event) {
            this.value = numberConComas(this.value);
        });

//        $('#valor_cobrar, #perc_cobrar, #n_xmil').keyup(function () {
//            changeStatusCobraValues(this.id, this.value);
//        });

        function numberConComas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
    });
</script>
</html>
