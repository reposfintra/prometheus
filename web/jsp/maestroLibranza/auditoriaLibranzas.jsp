<%-- 
    Document   : auditoriaLibranzas
    Created on : 29/06/2016, 02:39:15 PM
    Author     : dvalencia
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>AUDITORIA LIBRANZAS</title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>   
        <script type="text/javascript" src="./js/auditoriaLibranzas.js"></script> 
    </head>
    <body>
       <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=AUDITORIA LIBRANZAS"/>
        </div>
        <style>
            .onoffswitch-inner:before {
                content: "A";
                padding-left: 14px;
                background-color: #34A7C1; color: #FFFFFF;
            }
            .onoffswitch-inner:after {
                content: "I";
                padding-right: 11px;
                background-color: #C20F0F; color: #F5F0F0;
                text-align: right;
            }
        </style>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 120px; ">
        <center>
        <div id="div_filtro" class="frm_search" style="width:500px">     
         <table width="400" border="0" cellpadding="0" cellspacing="1" class="labels" id="tbl_hys"> 
       
            <tr>
                        <td colspan="2">
                            <fieldset style="width:420px;border:1px solid #00a0f0">
                                
                                
                                
                                <table width="400" border="0" align="left" cellpadding="1" cellspacing="1" class="labels">
                                  <tr>
                                      
                                    <td width="400">
                                        
                                            <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                                 <tr>
                                                    <td colspan="2">
                                                         <label for="fecha">Fecha Negocio:&nbsp&nbsp&nbsp</label>
                                                         <input type="text" id="fecha_ini" readonly style="width:82px"/>
                                                        <input type="text" id="fecha_fin" readonly style="width:82px"/>
                                                    </td>
                                                 </tr>
                                            </table>
                                      
                                    </td>
                                  
                                       
                                    <td style="padding-left: 10px">
                                    <button id="listarAuditoria" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"  
                                        role="button" aria-disabled="false">
                                    <span class="ui-button-text">Buscar</span>
                                    </button>
                                    </td>
                                    
                                    
                             </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr> 
                    </table>
           
            
            </div>
            <div style="position: relative;top: 10px;">
            <table id="tabla_Auditoria" ></table>
            <div id="pager"></div>
            </div>
        
        <div id="dialogAuditoria"  class="ventana">     </div>
        </center>
        </div>
    <script>
  
        auditoriaLibranzas();
    </script>
    </body>
</html>
