<%-- 
    Document   : EntidadesCompraCartera
    Created on : 12/05/2016, 05:10:31 PM
    Author     : MMEDINA
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/MaestroLibranza.js"></script> 

        <title>ENTIDADES COMPRA CARTERA</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=ENTIDADES COMPRA CARTERA"/>
        </div>
    <center>
        <div style="position: relative;top: 231px;">
            <table id="tabla_entidadesCC" ></table>
            <div id="pager"></div>
        </div>
        <div id="dialogEntidades"  class="ventana" >
            <div id="tablainterna" style="width: 716px;" >
                <table id="tablainterna"  >
                    <tr>
                        <td>
                            <label>Nombre entidad</label>
                        </td>
                        <td>
                            <input type="text" id="entidad" style="width: 269px;color: black;" >
                        </td>
                        <td>
                            <label>Nit</label>
                        </td>
                        <td>
                            <input type="text" id="nit" style="width: 137px;color: black;" readonly>
                        </td>
                        <td>
                            <label>Digito verificacion</label>
                        </td>
                        <td>
                            <input type="text" id="digitov" style="width: 19px;color: black;" readonly>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>
    </center>
</body>
<script>
    inicio10();
</script>
</html>
