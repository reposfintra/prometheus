<%-- 
    Document   : PerfeccionamientoCompraCartera
    Created on : 27/04/2016, 04:24:37 PM
    Author     : egonzalez
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/MaestroLibranza.js"></script> 
        <script type="text/javascript" href="http://www.ddginc-usa.com/spanish/editcheck.js"></script>
        <title>PERFECCIONAMIENTO COMPRA CARTERA</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=PERFECCIONAMIENTO COMPRA CARTERA"/>
        </div>
    <center>
        <div style="position: relative;top: 150px;">
            <table id="tabla_PerfeCC" ></table>
            <div id="pager"></div>
        </div>
        <div id="dialogMsConceptoPer"  class="ventana" >
            <div id="tablainterna" style="width: 450px" >
                <table id="tablainterna" >
                    <tr>
                        <td colspan="2">
                            <input type="text" id="negocio"  style="height: 20px;width: 137px;color: #070708;font-size: 13px;"readonly hidden >
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label style="font-family: Tahoma,Arial;">Usuario</label>
                        </td>
                        <td>
                            <input type="text" id="usuario"  style="height: 20px;width: 137px;color: #070708;font-size: 13px;"readonly >
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label style="font-family: Tahoma,Arial;">Fecha</label>
                        </td>
                        <td>
                            <input type="text" id="fecha"  style="height: 20px;width: 137px;color: #070708;font-size: 13px;"readonly >
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label style="font-family: Tahoma,Arial;">Decision</label>
                        </td>
                        <td>
                            <input type="text" id="decision"  style="height: 20px;width: 137px;color: #070708;font-size: 13px;" readonly>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label style="font-family: Tahoma,Arial;">Comentarios</label>
                        </td>
                        <td>
                            <textarea id="comentarios" style="width: 350px;height: 79px;" readonly></textarea>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div id="encabezado2tablita">
                                <label class="titulo2tablita"><b>COMENTARIOS</b></label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label style="font-family: Tahoma,Arial;">Comentarios</label>
                        </td>
                        <td>
                            <textarea id="coment" style="width: 350px;height: 79px;"></textarea>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        
        <div id="cargararchivo" style="display: none" >
            <table style="width: 100%">
            <tr>                          
                            <td class="letra_resaltada" align="center">&nbsp; Seleccione el archivo </td>
                            <td align="left">
                                <form id="formulario" name="formulario"> 
                                      <input type="hidden" name="documento" id="documento" >
                                    <input type="file" id="archivo"  name="archivo" style="width: 270px">                                  
                                </form>
                            </td>       
                            
                            <td>
                                <button id="cargarArchivo" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                        role="button" aria-disabled="false" onclick="cargarPazySalvo()">
                                    <span class="ui-button-text">Subir Archivo</span>
                                </button>
                            </td>   
                            <tr
                        </tr> 
            </table>
        </div>
        <div id="verarchivo" style="display: none" >
            <table style="width: 100%">
                <td style="width:50%" colspan="3">
                     <fieldset >
                         <legend>Archivos Cargados</legend>    
                         <div style="height:80px;overflow:auto;">
                            <table id="tbl_archivos_cargados" aling="center" style=" width: 100%" >             

                            </table>
                         </div>
                     </fieldset>
                </td>

            </table>
        </div>
        
        <div id="info"  class="ventana" style="width: 325px;" >
            <p id="notific">mensaje</p>
        </div>
        <div id="dialogLoading" style="display:none;">
            <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
            <center>
                <img src="./images/cargandoCM.gif"/>
            </center>
        </div>
        <!-- Dialogo de los jsp> -->
        <!--<div id="dialogMsj" style="display:none;">
            <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
        </div> -->
        
    </center>
</body>
<script>
    $(document).ready(function () {
        inicio8();
        
    });

</script>
</html>

