<%-- 
    Document   : configuracionLibranza
    Created on : 31/03/2016, 12:23:11 PM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Configuraci�n Libranza</title>
        
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link href="./css/popup.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script> 
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script src="./js/configLibranza.js" type="text/javascript"></script>

        <!--css logica de negocio-->
         <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />

        <!--jqgrid--> 
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <style type="text/css">
            input{
                color:black !important;
            }           
        </style>
    </head>
    <body>
        <div id="capaSuperior"  style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Configuraci�n Libranza"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">
            <br>
            <center>
                 <table id="tabla_config_libranza"></table>
                 <div id="page_tabla_config_libranza"></div>    
            </center>
            <div id="dialogLoading" style="display:none;">
                <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                <center>
                    <img src="./images/cargandoCM.gif"/>
                </center>
            </div>
            <div id="dialogMsj" title="Mensaje" style="display:none;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div>      
        </div>
        <div id="div_config_libranza"  style="width: 530px;display:none;" > 
                <table aling="center" style=" width: 100%" >                   
                    <tr>
                        <td>
                            <input type="text" id="id_config"  style="width: 50px" hidden >
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" >
                            <label> Convenio Pagaduria<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="3">
                            <input type="text" id="nom_convenio_pag" onchange="conMayusculas(this)" style="width: 293px"  />
                        </td>
                        <td>
                            <label> Plazo M�nimo<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="3">
                            <input type="text"  id="plazo_minimo" class="solo-numero"  />
                        </td>
                    </tr>  
                    <tr>
                        <td colspan="2">
                            <label>Convenio<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="3">
                            <select id="convenio" class="combo_180px" style="width: 300px"></select>                           
                        </td>  
                        <td>
                            <label> Plazo M�ximo<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="3">
                            <input id="plazo_maximo" class="solo-numero"  />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>Pagadur�a<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="3">
                            <select id="pagaduria" class="combo_180px" style="width: 300px"></select>                           
                        </td>
                        <td>
                            <label> Colchon<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="3">
                            <input id="colchon" class="solo-numero"  />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label>Ocupaci�n<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="3">
                            <select id="ocupacion" class="combo_180px" style="width: 300px"></select>                           
                        </td>
                        <td>
                            <label> % Descuento<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="3">
                            <input id="porc_descuento" class="solo-numeric" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label> Tasa Mensual<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="1">
                            <input id="tasa_mensual" style="width: 100px" class="solo-numeric"  />
                        </td>
                        <td colspan="1">
                            <label> Tasa Anual<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="1">
                            <input id="tasa_anual" style="width: 100px" class="solo-numeric" readonly />
                        </td>
                        <td>
                            <label> Dia Entrega Nov.<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="3">
                            <input type="text" id="dia_ent_novedades" readonly/>                      
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label> Tasa Renovaci�n<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="3">
                            <input id="tasa_renov" style="width: 295px" class="solo-numeric"  />
                        </td>
                        <td>
                            <label> D�a de Pago<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="3">
                            <input type="text" id="dia_pago" readonly/>                      
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label> Monto M�nimo<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="3">
                            <input type="tel"  id="monto_minimo" style="width: 295px" class="solo-numeric"  />
                        </td>
                        <td>
                            <label> Periodo Gracia<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="3">
                            <input id="periodo_gracia"  class="solo-numero" maxlength="1"  />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label> Monto M�ximo<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="3">
                            <input id="monto_maximo" style="width: 295px" class="solo-numeric"  />
                        </td>
                        <td>
                            <label> Requiere Anexo<span style="color:red;">*</span></label> 
                        </td>
                        <td>
                            <input type="radio" name="anexo" value="S"  style="width: 20px">Si
                            <input type="radio" name="anexo" value="N" style="width: 20px" checked> No
                        </td>
                    </tr>
                </table> 
        </div>              
        <div id="div_firmas_registradas"  style="width: 530px;display:none;" >  
            <table aling="center" style=" width: 100%" >                   
                <tr>
                    <td>
                        <input type="text" id="id"  style="width: 50px" hidden >
                        <input type="text" id="id_conf_libranza"  style="width: 50px" hidden >
                    </td>
                </tr>
                <tr>
                    <td colspan="2" >
                        <label> Nombre<span style="color:red;">*</span></label> 
                    </td>
                    <td colspan="3">
                        <input type="text" id="nombre" onchange="conMayusculas(this)" style="width: 298px"  />
                    </td>
                </tr>  
                <tr>
                    <td colspan="2">
                        <label>Numero Documento<span style="color:red;">*</span></label> 
                    </td>
                    <td colspan="3">
                        <input type="text" class="solo-numero" maxlength="12" id="documento" style="width: 298px"  />                       
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <label> Telefono<span style="color:red;">*</span></label> 
                    </td>
                    <td colspan="3">
                        <input type="tel"  id="telefono" style="width: 298px" class="solo-numero" maxlength="10"   />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <label> Correo<span style="color:red;">*</span></label> 
                    </td>
                    <td colspan="3">
                        <input type="email" placeholder="Formato: algo@dir.dom" title="correo incorrecto ejemplo : ejemplo@correo.co" id="correo" onchange="conMayusculas(this)" style="width: 298px" />
                    </td>
                </tr>                
            </table> 
        </div>
    </body>
</html>
