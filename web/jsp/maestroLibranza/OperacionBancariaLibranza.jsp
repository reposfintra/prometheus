<%-- 
    Document   : SalarioMinimo
    Created on : 30/03/2016, 11:34:20 AM
    Author     : egonzalez
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/MaestroLibranza.js"></script> 

        <title>OPERACION LIBRANZA</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=OPERACION  LIBRANZA"/>
        </div>
        <style>
            .onoffswitch-inner:before {
                content: "A";
                padding-left: 14px;
                background-color: #34A7C1; color: #FFFFFF;
            }
            .onoffswitch-inner:after {
                content: "I";
                padding-right: 11px;
                background-color: #C20F0F; color: #F5F0F0;
                text-align: right;
            }
        </style>
    <center>
        <div style="position: relative;top: 231px;">
            <table id="tabla_OpBancariaLib" ></table>
            <div id="pager"></div>
        </div>
        <div id="dialogOpBancariaLib"  class="ventana" >
            <div id="tablainterna" style="width: 301px;" >
                <table id="tablainterna"  >
                    <tr>
                        <td>
                            <input type="text" id="id" style="width: 137px;color: black;" hidden>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Descripcion</label>
                        </td>
                        <td>
                            <input type="text" id="descripcion" style="width: 137px;color: black;"onchange="conMayusculas(this)">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Tipo operacion</label>
                        </td>
                        <td>
                            <select id="tipoperacion" style="width: 145px"></select>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Tipo documento</label>
                        </td>
                        <td>
                            <select id="tipo_documento" style="width: 145px"onchange="cargarHC()"></select>
                        </td>
                    </tr>
                    <tr>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div id="config4">
                                <input type="checkbox" id="hc_cab" style="margin-right: -37px;margin-left: -29px;"onchange="valor(this.id)">
                                <label>Hc cabecera</label>
                                <input type="checkbox" id="cuenta_cab" style="margin-right: -37px;margin-left: 10px;"onchange="valor(this.id)">
                                <label>Cuenta cabecera</label>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <select id="cmc" style="width: 125px"onchange="cargarCuentaHC()"></select>
                        </td>
                        <td>
                            <input type="text" id="cuenta_cxp" style="width: 137px;color: black;">
                        </td>
                    </tr>
                    <tr>

                        <td colspan="2">
                            <input type="text" id="cuenta" style="width: 117px;color: black;"readonly="">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Cuenta detalle</label>
                        </td>
                        <td>
                            <input type="text" id="cuenta_detalle" style="width: 137px;color: black;"onchange="conMayusculas(this)">
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>
    </center>
</body>
<script>
    inicio5();
</script>
</html>
