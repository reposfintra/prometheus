<%-- 
    Document   : pagadurias
    Created on : 30/03/2016, 12:24:11 PM
    Author     : user
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
 <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Pagadur�as</title>
        
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
<!--        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>-->
        <link href="./css/popup.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script> 
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script src="./js/pagadurias.js" type="text/javascript"></script>

        <!--css logica de negocio-->
         <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />

        <!--jqgrid--> 
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <style type="text/css">
            input{
                color:black !important;
            }           
        </style>
    </head>
    <body>
        <div id="capaSuperior"  style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Maestro pagadur�as"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">
            <br>
            <center>
                 <table id="tabla_pagadurias"></table>
                 <div id="page_tabla_pagadurias"></div>    
            </center>
            <div id="dialogLoading" style="display:none;">
                <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                <center>
                    <img src="./images/cargandoCM.gif"/>
                </center>
            </div>
            <div id="dialogMsj" title="Mensaje" style="display:none;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div>      
        </div>
        <div id="div_pagaduria"  style="display: none; width: 580px" >  
                  <div id="direccion_dialogo" style="display:none;left: 0; position: absolute; top: 0;padding: 1em 1.2em; margin:0px auto; margin-top:10px; padding:10px; width:370px; min-height:140px; border-radius:4px; background-color:#FFFFFF; box-shadow: 0 2px 5px #666666;"> 
                        <div>
                            <table style="width: 100%;">

                                <tr>
                                    <td class="titulo_ventana" id="drag_direcciones" colspan="3">
                                        <div style="float:center">FORMATO DIRECCIONES<input type="hidden" name="id_usuario" id="id_usuario" value="coco"/></div> 
                                    </td>
                                </tr>                    
                                <tr>  
                                    <td><span>Departamento:</span></td>                     
                                    <td colspan="2"> <select id="dep_dir" name="dep_dir" style="width:20em;"></select>
                                    </td>
                                </tr>
                                <tr>
                                    <td><span>Ciudad:</span></td>     
                                    <td colspan="2">                            
                                        <div id="d_ciu_dir">
                                            <select id="ciu_dir" name="ciu_dir" style="width:20em;"></select>
                                        </div>
                                    </td>
                                </tr>  
                                <tr>
                                    <td>Via Principal</td>
                                    <td>
                                        <select id="via_princip_dir" onchange="setDireccion(2)">                                
                                        </select>
                                    </td>
                                    <td><input type="text" id="nom_princip_dir" style="width: 115px;" onchange="setDireccion(1)"/></td>
                                </tr>
                                <tr>
                                    <!-- <td>Via Generadora</td> -->
                                    <td>Numero</td>
                                    <td>
                                        <select id="via_genera_dir" onchange="setDireccion(1)">                               
                                        </select>
                                    </td>

                                    <td>
                                        <table width="100%" border="0">
                                            <tr>
                                                <td align="center" width="49%">
                                                    <input type="text" id="nom_genera_dir" style="width:40px;" onchange="setDireccion(1)"/>
                                                </td>
                                                <td width="2%" align="center"> - </td>
                                                <td align="center" width="49%">
                                                    <input type="text" id="placa_dir" style="width: 40px;" onkeypress="setDireccion(1)" onchange="setDireccion(1)"/>
                                                </td>    
                                            </tr>
                                        </table>                        
                                    </td>
                                </tr>
                                <tr>
                                    <td>Complemento</td>
                                    <td colspan="2"><input type="text" id="cmpl_dir" style="width: 85%;" onkeypress="setDireccion(1)" onchange="setDireccion(1)"/></td>
                                </tr>
                                <tr>
                                    <td colspan="3"><input type="text" id="dir_resul" name="" style="width: 89%;" readonly/></td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <button onclick="setDireccion(3);">Aceptar</button>
                                        <button onclick="setDireccion(0);">Salir</button>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </br>
                <div id="div_pagaduria"  style="width: 530px" >  
                     <table aling="center" style=" width: 100%" >                   
                         <tr>
                             <td>
                                 <input type="text" id="id"  style="width: 50px"hidden >
                             </td>
                         </tr>
                         <tr>
                             <td colspan="2" >
                                 <label> Razon social<span style="color:red;">*</span></label> 
                             </td>
                             <td colspan="3">
                                 <input type="text" id="razonsocial" onchange="conMayusculas(this)" style="width: 298px"  />
                             </td>
                         </tr>  
                         <tr>
                             <td colspan="2">
                                 <label>Numero Documento<span style="color:red;">*</span></label> 
                             </td>
                             <td colspan="3">
                                 <input type="text" class="solo-numero" maxlength="12" id="documento" style="width: 220px"  onchange="verificarNit()"/>
                                 <span>DV&nbsp;</span><input name="digito_verificacion" type="text" id="digito_verificacion" style="width:40PX;" readonly value ="">
                             </td>
                             <td>
                                 <input type="text"  maxlength="12" id="verificacion" style="width: 100px" hidden />
                             </td>
                         </tr>
                         <tr>
                             <td colspan="2">
                                 <label>Pa�s<span style="color:red;">*</span></label> 
                             </td>
                             <td colspan="3">
                                   <select id="pais" style="width: 315px"></select>
                             </td>
                         </tr>
                         <tr>
                             <td colspan="2">
                                 <label>Departamento<span style="color:red;">*</span></label> 
                             </td>
                            <td colspan="3">
                                   <select id="departamento" style="width: 315px"></select>
                             </td>
                         </tr>
                         <tr>
                             <td colspan="2">
                                 <label>Ciudad<span style="color:red;">*</span></label> 
                             </td>
                             <td colspan="3">
                                   <select id="ciudad" style="width: 315px"></select>
                             </td>
                         </tr>
                         <tr>
                             <td colspan="2" >
                                 <label> Direccion<span style="color:red;">*</span></label> 
                             </td>
                             <td colspan="3">
                                 <input type="text" id="direccion" onchange="conMayusculas(this)" style="width: 298px"  readonly/>
                                  &nbsp;
                                  <img src="/fintra/images/Direcciones.png" width="26" height="23" border="0" align="ABSMIDDLE" onclick="genDireccion('direccion',event);" alt="Direcciones"  title="Direcciones" />
                             </td>
                         </tr>
                         <tr>
                             <td colspan="2">
                                 <label> Correo<span style="color:red;">*</span></label> 
                             </td>
                             <td colspan="3">
                                 <input type="email" placeholder="Formato: algo@dir.dom" title="correo incorrecto ejemplo : ejemplo@correo.co" id="correo" onchange="conMayusculas(this)" style="width: 298px" />
                             </td>
                         </tr>
                         <tr>
                             <td colspan="2">
                                 <label> Telefono<span style="color:red;">*</span></label> 
                             </td>
                             <td colspan="3">
                                 <input type="tel"  id="telefono" style="width: 298px" class="solo-numero" maxlength="10"   />
                             </td>
                         </tr>
                     </table> 
                   </div>
                </br> 
         </div>
        
        <div id="div_empresa_asociada"  style="width: 530px;display:none;">
            <div id="direccion_dialogo_emp" style="display:none;left: 0; position: absolute; top: 0;padding: 1em 1.2em; margin:0px auto; margin-top:10px; padding:10px; width:370px; min-height:140px; border-radius:4px; background-color:#FFFFFF; box-shadow: 0 2px 5px #666666;"> 
                <div>
                    <table style="width: 100%;">

                        <tr>
                            <td class="titulo_ventana" id="drag_direcciones_emp" colspan="3">
                                <div style="float:center">FORMATO DIRECCIONES</div> 
                            </td>
                        </tr>                    
                        <tr>  
                            <td><span>Departamento:</span></td>                     
                            <td colspan="2"> <select id="dep_dir_emp" name="dep_dir" style="width:20em;"></select>
                            </td>
                        </tr>
                        <tr>
                            <td><span>Ciudad:</span></td>     
                            <td colspan="2">                            
                                <div id="d_ciu_dir_emp">
                                    <select id="ciu_dir_emp" name="ciu_dir" style="width:20em;"></select>
                                </div>
                            </td>
                        </tr>  
                        <tr>
                            <td>Via Principal</td>
                            <td>
                                <select id="via_princip_dir_emp" onchange="setDireccion2(2)">                                
                                </select>
                            </td>
                            <td><input type="text" id="nom_princip_dir_emp" style="width: 115px;" onchange="setDireccion2(1)"/></td>
                        </tr>
                        <tr>
                            <!-- <td>Via Generadora</td> -->
                            <td>Numero</td>
                            <td>
                                <select id="via_genera_dir_emp" onchange="setDireccion2(1)">                               
                                </select>
                            </td>

                            <td>
                                <table width="100%" border="0">
                                    <tr>
                                        <td align="center" width="49%">
                                            <input type="text" id="nom_genera_dir_emp" style="width:40px;" onchange="setDireccion2(1)"/>
                                        </td>
                                        <td width="2%" align="center"> - </td>
                                        <td align="center" width="49%">
                                            <input type="text" id="placa_dir_emp" style="width: 40px;" onkeypress="setDireccion2(1)" onchange="setDireccion2(1)"/>
                                        </td>    
                                    </tr>
                                </table>                        
                            </td>
                        </tr>
                        <tr>
                            <td>Complemento</td>
                            <td colspan="2"><input type="text" id="cmpl_dir_emp" style="width: 85%;" onkeypress="setDireccion2(1)" onchange="setDireccion2(1)"/></td>
                        </tr>
                        <tr>
                            <td colspan="3"><input type="text" id="dir_resul_emp" name="" style="width: 89%;" readonly/></td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <button onclick="setDireccion2(3);">Aceptar</button>
                                <button onclick="setDireccion2(0);">Salir</button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div id="div_empresa_asociada"  style="width: 490px" >  
                <br>
                <table aling="center" style=" width: 100%" >                   
                    <tr>
                        <td>
                            <input type="text" id="id_empresa"  style="width: 50px" hidden >
                            <input type="text" id="id_pagaduria"  style="width: 50px" hidden >
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" >
                            <label> Razon Social<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="3">
                            <input type="text" id="razon_social" onchange="conMayusculas(this)" style="width: 298px"  />
                        </td>
                    </tr>  
                    <tr>
                        <td colspan="2">
                            <label>Nit<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="3">
                            <input type="text" class="solo-numero" maxlength="12" id="nit_empresa" style="width: 220px" />
                            <span>DV&nbsp;</span><input name="digito_verificacion_emp" type="text" id="digito_verificacion_emp" style="width:40PX;" readonly value ="">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label> Telefono<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="3">
                            <input type="tel"  id="telefono_empresa" style="width: 298px" class="solo-numero" maxlength="10"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <label> Direcci�n<span style="color:red;">*</span></label> 
                        </td>
                        <td colspan="3">
                            <input type="text" id="direccion_empresa" onchange="conMayusculas(this)" style="width: 298px" readonly/>
                            &nbsp;
                            <img src="/fintra/images/Direcciones.png" width="26" height="23" border="0" align="ABSMIDDLE" onclick="genDireccion2('direccion_empresa',event);" alt="Direcciones"  title="Direcciones" />
                        </td>
                    </tr>                
                </table> 
            </div> 
        </div>
    </body>
</html>
