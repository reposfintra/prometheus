<%-- 
    Document   : actualizarPagaduria
    Created on : 26/04/2018, 11:30:01 AM
    Author     : dvalencia
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/actualizarPagaduria.js"></script> 
        <script type="text/javascript" src="./js/utilidadInformacion.js"></script> 
        <title>Actualizar Pagadur�a - Libranza</title>
        <style>
            html {
                font-size: 16px;
            }

            #tablainterna tr:first-child td:nth-child(2), #tablainterna tr:first-child td:nth-child(3) {
                width: 80px !important;
            }

            #pagaduria, #cedula, #cliente, #comentario {
                padding-left: 0;
                padding-right: 0;
            }

            .button-wrapper {
                display: inline-block;
                margin-top: 10px;
                margin-left: 15px;
            }

            button {
                width: 116px;
            }
        </style>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Pagadurias"/>
        </div>
    <center>
        <div id="tablita" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 600px; padding: 8px;">
            <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                <!--<label class="titulotablita"><b>FILTRO DE BUSQUEDA</b></label>-->
                <label class="titulotablita"><b>ACTUALIZAR PAGADUR�A</b></label>
            </div>
            <table id="tablainterna" style="height: 53px; width: 550px">
                <br>
                <tr>
                    <td>
                        <label>Negocio</label>
                    </td>
                    <td>
                        <input id="negocio" type="text" style="width: 110px;">
                    </td>
                    <td>
                        <label>Pagadur�a</label>
                    </td>
                    <td>
                        <input id="pagaduria" type="text" disabled style="width: 100%;">
                    </td>                    
                </tr>
                <tr>
                    <td>
                        <label>C�dula</label>
                    </td>
                    <td>
                        <input id="cedula" type="text" disabled style="width: 100%;">
                    </td>
                    <td>
                        <label>Cliente</label>
                    </td>
                    <td>
                        <input id="cliente" type="text" disabled style="width: 100%;">
                    </td>                    
                </tr>
<!--                <tr>
                    <td>
                        <label>Comentario</label>
                    </td>                    
                    <td colspan="3">
                        <input id="comentario" type="text" style="width: 100%;">
                    </td>
                </tr>-->
                <tr>
                    <td>
                        <label>Nueva pagadur�a</label>
                    </td>                    
                    <td colspan="3">
                        <select id="listaPagadurias" style="width: 100%;">
                            <option value="">Seleccione una opci�n</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <div class="button-wrapper">
                            <center>
                                <button type="button" id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"> 
                                    <span class="ui-button-text">Buscar</span>
                                </button>  
                            </center>
                        </div>
                        <div class="button-wrapper">
                            <center>
                                <button type="button" id="historico" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"> 
                                    <span class="ui-button-text">Ver Hist�rico</span>
                                </button>  
                            </center>
                        </div>
                        <div class="button-wrapper">
                            <center>
                                <button type="button" id="actualizar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" > 
                                    <span class="ui-button-text">Actualizar</span>
                                </button>  
                            </center>
                        </div>
                        <div class="button-wrapper">
                            <center>
                                <button type="button" id="salir" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" > 
                                    <span class="ui-button-text">Salir</span>
                                </button>  
                            </center>
                        </div>
                    </td>
                </tr>
            </table>
        </div>

        <div id="tabla_NegocioLibranzaWrapper" style="position: relative;top: 140px;">
            <table id="tabla_NegocioLibranza" ></table>
            <div id="pager"></div>
        </div>
        <div id="dialogLoading" style="display:none;">
                <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                <center>
                    <img src="./images/cargandoCM.gif"/>
                </center>
            </div>
        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>
    </center>
    <div id="dialogMsg" title="Mensaje" style="display:none;">
        <p style="font-size: 12.5px;text-align:justify;" id="msj" >TEXTO</p>
    </div> 
</body>
<script>

</script>
</html>
