<%-- 
    Document   : reporte_clientes_libranza
    Created on : 14/10/2016, 08:00:27 AM
    Author     : mcastillo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <title>Reporte de Clientes</title>


        <link href="./css/simple-sidebar.css" rel="stylesheet" type="text/css"/>
        <link href="./css/OportunidadNegocio.css" rel="stylesheet" type="text/css"/>


        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>     
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>      

        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <link type="text/css" rel="stylesheet" href="./css/recibos_caja.css " />
             
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>
        <script type="text/javascript" src="./js/reporte_clientes_libranza.js"></script> 
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
             <jsp:include page="/toptsp.jsp?encabezado=Reporte de Cartera"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">
            <center>  
                <div id="div_filtro_clientes" class="frm_search" style="width:330px;">  
                    <table>
                        <tr>
                            <td colspan="2">
                                <label for="fecha">Fecha Ingreso:&nbsp&nbsp&nbsp<b id="req_fecha" style="color:red">*</b></label>
                                <input type="text" id="fecha_ini" readonly style="width:82px"/>
                                <input type="text" id="fecha_fin" readonly style="width:82px"/>
                            </td>
                        </tr> 
                        <tr>
                            <td>
                                <label for="convenio"> Convenio:&nbsp&nbsp</label>
                                <select name="convenio" id="convenio" class="combo_180px" style="width:200px;">
                                </select>                     
                            </td> 
                        </tr>
                    </table>
                    <br>
                    <div style="text-align: center;">
                        <button id="listarClientes" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                role="button" aria-disabled="false">
                            <span class="ui-button-text">Buscar</span>
                        </button>
                        <button id="clearClientes" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                role="button" aria-disabled="false">
                            <span class="ui-button-text">Limpiar</span>
                        </button>
                    </div>
                </div>
                <br><br>
                <table id="tabla_reporte_clientes"></table>
                <div id="page_tabla_reporte_clientes"></div>
            </center>

            <div id="divSalidaEx" title="Exportacion" style=" display: block" >
                <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
                <center>
                    <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
                </center>
                <div id="respEx" style=" display: none"></div>
            </div>       
        </div>
        <div id="dialogLoading" style="display:none;">
                <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                <center>
                    <img src="./images/cargandoCM.gif"/>
                </center>
            </div>
            <div id="dialogMsj" title="Mensaje" style="display:none;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div>       
    </body>
</html>
