<%-- 
    Document   : generacion_carta_cobro
    Created on : 28/11/2019, 08:17:59 AM
    Author     : mcamargo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>  
        <script src="./js/generacion_carta_cobro.js" type="text/javascript"></script>
        <title>Aprobar Asignacion Cartera</title>
        <style>
            .filtro{
                width: 740px;
                margin: 10px auto; 

            }

            .header_filtro{
                text-align: center;   
                border: 1px solid #234684;
                background: #2A88C8;
                color: #ffffff;
                font-weight: bold;
                border-radius: 8px 8px 0px 0px;
            }

            .body_filtro{                
                background: white; 
                border:1px solid #2A88C8;
                height: 80px;
                border-radius: 0px 0px 8px 8px;
            }

            .contenido{
                height: 83px; 
                width: auto;
                margin-top: 5px;
            }

            label{
                display: block;
                text-align: center;
                font-family: 'Lucida Grande','Lucida Sans',Arial,sans-serif;
                font-size: 15px;
            }

            .caja{
                float:left;
                margin-left:10px;
                margin-right:10px;
                margin-top: 8px;
            }
            input[type=text],input[type=number],select{
                padding: 5px 5px;
                margin: 8px auto;
                border: 1px solid #ccc;
                border-radius: 4px;
                font-size: 15px;
                color: black;
            }
            .select {
                font-size: 12px;
                background: #2A88C8;
                color: white;
                font-weight: bold;
            }
            .button {
                padding: 8px 8px;
                font-size: 12px;
                cursor: pointer;
                text-align: center;
                color: #fff;
                background-color: #2a88c8;
                border: none;
                border-radius:8px;
                margin: 22px auto;
                font-weight: bold;
                width: 80px;
            }
            
        </style>
    </head>
    <body>
        <div>
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Generar Cartas de cobro"/>
        </div>
        
        <div class="filtro">
            <div class="header_filtro">
                <label>FILTRO DE BUSQUEDA</label>
            </div>
            <div class="body_filtro">
                <div class="caja">
                       <label> Unidad Negocio:</label>
                       <select id="id_unidad_negocio" name="id_unidad_negocio" class="select">
                       </select>
                </div>  
                 <div class="caja">
                       <label> Agencia:</label>
                       <select id="id_agencia" name="id_agencia" class="select">
                           <option value="">...</option>
                           <option value="ATL">ATLANTICO</option>
                           <option value="COR">CORDOBA</option>
                           <option value="SUC">SUCRE</option>
                       </select>
                </div>
                
                <div class="caja">
                       <label> Altura mora:</label>
                       <select id="id_altura_mora" name="id_altura_mora" class="select">
                       </select>
                </div> 
                <div class="caja">
                    <label>Imprimir Dia:</label>
                    <input type="number" maxlength="3" id="id_dia" style="width: 100px;">
                </div>
                <div class="caja">
                    <button class="button" onclick="generarCartasCobro();">Generar</button>
                </div>
              
                
            </div>
        </div>
        
    <center>
        <div id="dialogMsj" title="Mensaje" style="display:none; .hide-close .ui-dialog-titlebar-close { display: none }">
            <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
        </div>  
        <div id="info"  class="ventana" >
            <p id="notific"></p>
        </div>

        <div id="dialogLoading" style="display:none;">
            <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
            <center>
                <img src="./images/cargandoCM.gif"/>
            </center>
        </div>
    </center>
    </body>
</html>
