<%-- 
    Document   : auto_scoring
    Created on : 6/04/2016, 03:24:38 PM
    Author     : user
--%>

<%@page import="com.tsp.operation.model.beans.Compania"%>
<%@page import="com.tsp.operation.model.CompaniaService"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.operation.model.beans.CmbGeneralScBeans"%>
<%@page import="java.util.ArrayList"%>
<%@page import="com.tsp.operation.model.DAOS.impl.Auto_ScoringImpl"%>
<%@page import="com.tsp.operation.model.DAOS.Auto_ScoringDAO"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE html>
<%    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String nomEmpresa = "";
    CompaniaService ciaserv = new CompaniaService();
    ciaserv.buscarCia(usuario.getDstrct());
    Compania cia = ciaserv.getCompania();
    nomEmpresa = cia.getdescription();
%>   
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Auto Scoring</title>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>        
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>            
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <link type="text/css" rel="stylesheet" href="./css/popup.css"/>
        <link href="./css/style_azul.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./js/Auto_Scoring.js"></script> 

    </head>
    <body>

        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Auto Scoring"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:83%; z-index:0; left: 0px; top: 100px; overflow:auto;">       


            <!-------------------------------------->


            <center>
                <br>

                <div>
                    <table id="tabla_productos"></table>
                    <div id="page_productos"></div>
                </div>
            </center>


            <!-------------------------------------->


            </br>
            <table border="0"  align="center">
                <tr>
                    <td>

                        <div id="searchCategoria"></div>
                        <table id="Categorias" ></table>
                        <div id="page_tabla_categorias"></div>
                    </td>
                </tr>
            </table>
        </div>


        <div id="div_categoria"  style="display: none; width: 800px" >                       
            <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 110px;">
                </br>
                <table aling="center" style=" width: 100%" >
                    <tr>
                    <input type="hidden" id="idProceso" name="idMeta">     
                    <input type="hidden" id="idempresa" name="idempresa" value="<%=usuario.getDstrct()%>" >      
                    <td style="width: 10%"><span>Nombre</span></td>                          
                    <td style="width: 50%"><input type="text" id="nommeta" name="nommeta" style=" width: 248px" maxlength="100" onblur="igualarcampo('descmeta', 'nommeta');">
                        <!--img src='<%=BASEURL%>/images/botones/iconos/lupa.gif' id="buscarnumos" width="15" height="15"  style="cursor:hand"title="Buscar">
                        <input type="text" id="idcategoria" name="idcategoria"/-->
                    </td>  
                    </tr> 
                    <tr>
                        <td style="width: 10%"><span>Descripcion:</span></td>   
                        <td style="width: 90%" colspan="4"><textarea id ="descmeta" name="descmeta" style="width:535px;resize:none" rows="2" maxlength="300"></textarea></td>                        
                    </tr>
                </table>
            </div>  
            </br> 
        </div>

        <div id="div_editar_categoria"  style="display: none; width: 800px" >
            <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 55px;padding: 0px 10px 5px 10px">
                </br>
                <table aling="center" style=" width: 100%" >
                    <tr>
                    <input type="hidden" id="idMetaEdit" name="idMetaEdit">    
                    <input type="hidden" id="idempresaEdit" name="idempresaEdit" value="<%=usuario.getDstrct()%>" >      
                    <td style="width: 10%"><span>Nombre</span></td>                          
                    <td style="width: 100%"><input type="text" id="nommetaEdit" name="nommetaEdit" style=" width: 500px" maxlength="100" disabled="true"></td>
                    </tr>

                </table>
            </div>  
            </br>  
            <table border="0"  align="center">
                <tr>
                    <td>
                        <table id="subcategoria" ></table>
                        <div id="page_tabla_subcategorias"></div>
                    </td>
                </tr>
            </table>
        </div> 

        <div id="div_Subcategoria" title="CREAR TIPO VARIABLE MERCADO" style="display: none; width: 800px" >                

            <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 51px;padding: 0px 10px 5px 10px">
                </br>
                <table aling="center" style=" width: 100%" >
                    <tr>
                        <td style="display: none"><span>Categoria:</span></td>
                        <td style="display: none"> <select name="idProMeta" disabled = "true" hidden="true" class="combo_180px" style="font-size: 14px;width:230px" id="idProMeta">
                            </select></td>
                        <td style="width: 10%"><span>Nombre</span></td>
                        <td style="width: 30%"><input type="text" id="nomProceso" name="nomProceso" style=" width: 222px" maxlength="100" onblur="igualarcampo('descProceso', 'nomProceso')"/> </td>
                    </tr>
                </table>
            </div>   
            </br> 
        </div>                    


        <!----------------------------pantalla editar subcategoria ----------------------------->
        <div id="div_editar_especificacion"  style="display: none; width: 800px; z-index:1100" >
            <!----------------------------cabezera----------------------------->

            <div id="filtros"  class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 80px;z-index:1100;padding: 0px 10px 5px 10px; ">

                </br>
                <table aling="center" style=" width: 100%" >
                    <tr>
                    <input type="hidden" id="idProinterno" name="idProinterno">
                    <input type="hidden" id="idSubcategoria" name="idSubcategoria">
                    <td style="display: none"><span>Categoria:</span></td>
                    <td style="display: none"> <select name="idProMetaEdit" class="combo_180px" style="font-size: 14px;width:280px" id="idProMetaEdit">
                        </select> </td> 
                    <td style="width: 10%"><span>Nombre SubCategoria:</span></td>
                    <td style="width: 100%"><input type="text" id="nomProinterno" style=" width: 600px" name="nomProinterno" maxlength="100"/></td>                                             
                    <!--                         <td style="width: 20%"><span>Meta Proceso:</span></td>
                                             <td style="width: 30%"><input type="text" id="metaProceso" name="metaProceso" readOnly style=" width: 200px" </td>-->
                    </tr>
                    <td style="width: 100%"><div id="boton_guardar" style="position:absolute; left: 86%; top: 66%; "> 
                            <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="actualizarProInterno();"> Actualizar </span>
                        </div></td>
                </table>
            </div> 
            </br>        

            <div id="bt_asociar_especificacion" title="Asignar especificificacion a subcategoria" style="position:absolute; left: 47%; top: 54%; width: 10%; display:  none "> 
                <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="asociarUndProinterno();"> >> </span>
            </div>
            <div id="bt_desasociar_especificacion" title="DesAsignar especificificacion a subcategoria" style="position:absolute; left: 47%; top: 62%; width: 10%; display:  none "> 
                <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="desasociarUndProinterno();"> << </span>
            </div>
            <div id="div_und_prointerno" style="display: none; position: absolute; left: 55%; top: 17%; width: 40%;z-index:1200 ">    
                <table border="0"  align="center">
                    <tr>
                        <td>
                            <table id="UndNegocioPro" ></table>
                        </td>
                    </tr>
                </table>   
            </div>
            <div id="div_undNegocios" style="position:absolute; left: 5%; top: 17%; width: 40% ">    
                <table border="0"  align="center">
                    <tr>
                        <td>
                            <table id="UndadesNegocios" >
                                <div id="searchEspecificaciones"></div>
                            </table>
                            <div id="page_tabla_especificacion"></div>
                        </td>
                    </tr>
                </table>   
            </div>
        </div>

        <!---------------------------------------------modificar el div acoplar a la foto de harold------------------------------------------------------------------->
        <div id="div_especificacion"  style="display: none; width: 800px; z-index:1100" >
            <!----------------------------cabezera----------------------------->

            <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 88px;padding: 0px 10px 5px 10px;width: auto">
                </br>
                <table aling="center" style=" width: 100%" >
                    <tr>
                    <input type="hidden" id="idEsp" name="idEsp">  
                    <input type="hidden" id="tipo_dato" name="tipo_dato">
                    <input type="hidden" id="id_und" name="id_und">
                    <td style="width: 10%"><span>Nombre</span></td>
                    <td style="width: 30%"><input type="text" id="nomEspecificacion" name="nomEspecificacion" style=" width: 350px" maxlength="100"/> </td>

                    <input type="hidden" id="idRelCatSub" name="idRelCatSub">
                    </tr>

                    <tr>
                        <td>Tipo Entrada</td>
                        <td>

                            <select id="t_entrada" name="t_entrada" style="width: 264px;">
                                <option value="0" onclick="(document.getElementById('idEsp')!=='')? listarvaloresvariablemercado(): listarvalorespredeterminados();">POR RANGO</option>
                                <option value="1" onclick="(document.getElementById('idEsp')!=='')? listarvaloresvariablemercado2(): listarvalorespredeterminados2();">UNICO</option>
                            </select>
                        </td>
                    </tr>

                    <tr id='rtipodato' style="display: none;" >
                        <td style="width: 10%"><span id=>Tipo de Dato</span></td>  
                        <td> 
                            <div id="tipodato" style=" width: 40%">
                                <select id="tipodatos" name="producto" style="width: 264px;">
                                    <option value="2">ENTERO</option>
                                    <option value="3">DECIMAL</option>
                                    <option value="4">TEXTO</option>
                                </select>
                            </div>
                            <input type="text" id="idtiposeleccion" hidden="true" name="idcategoria">
                        </td>
                    </tr>
                </table>

            </div>  
            </br>        


            <div id="div_valorespredeterminados" style="position:absolute; left: 19%; top: 25%; width: 40% ">    
                <table border="0"  align="center">
                    <tr>
                        <td>
                            <table id="valorespredeterminados" >

                                    <div id="searchvalorespredeterminados"></div>
                                </table>
                                <div id="page_tabla_valorespredeterminados"></div>

                        </td>
                    </tr>
                </table>   
            </div>
            
            <div id="div_valorespredeterminados2" style="position:absolute; left: 19%; top: 25%; width: 40% ">    
                <table border="0"  align="center">
                    <tr>
                        <td>
                            <table id="valorespredeterminados2" >

                                    <div id="searchvalorespredeterminados2"></div>
                                </table>
                                <div id="page_tabla_valorespredeterminados2"></div>

                        </td>
                    </tr>
                </table>   
            </div>

        </div>

        <div id="div_asoc_user_prointerno"  style="display: none; width: 800px">
            <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height:110px; z-index:1100; padding: 0px 10px 0px 10px;">

                </br>
                <table aling="center" style=" width: 100%" >
                    <tr>
                    <input type="hidden" id="idProinternoAsoc" name="idProinternoAsoc"/>
                    <td style="width: 10%"><span>Nombre:</span></td>
                    <td style="width: 32%"><input type="text" id="nomProinternoAsoc" name="nomProinternoAsoc" readonly style=" width: 245px" </td>
                    <td style="width: 12%"><span>Categoria:</span></td>
                    <td style="width: 40%"><input type="text" id="nommetaproceso" name="nommetaproceso" readonly style=" width: 278px" </td>
                    </select> </td>  
                    </tr>div_procesos_internos_anul
                    <tr>
                        <td style="width: 10%"><span>Descripcion:</span></td>   
                        <td style="width: 90%" colspan="3"><textarea id ="descProinternoAsoc" name="descProinternoAsoc" style="width:340px;resize:none"  rows="2" readonly></textarea></td>                        
                    </tr>
                </table>
            </div> 
            </br>
            <div id="bt_asociar_userPro" title="Asignar usuarios al proceso interno" style="position:absolute; left: 47%; top: 54%; width: 10%; display:  none "> 
                <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="asociarUsuariosProinterno();"> >> </span>
            </div>  
            <div id="bt_desasociar_userPro" title="Desasignar usuarios del proceso interno" style="position:absolute; left: 47%; top: 62%; width: 10%; display:  none "> 
                <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" onClick="desasocia
                        rUsuariosProinterno()
                                ;"> << </span>
            </div>  
            <div id="div_user_prointerno" style="display: none; position: absolute; left: 55%; top: 22%; width: 40% ">    
                <table border="0"  align="center">
                    <tr>
                        <td>
                            <table id="userProInterno" ></table>
                        </td>
                    </tr>
                </table>   
            </div>
            <div id="div_usuarios" style="position:absolute; left: 3%; top: 21%; width: 40% ">    
                <table border="0"  align="center">
                    <tr>
                        <td>
                            <table id="listUsuarios" ></table>
                        </td>
                    </tr>
                </table> 
            </div>
        </div> 

        <div id="div_procesos_internos_anul"  style="display: none; width: 800px" >
            <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 110px;padding: 0px 10px 5px 10px">
                </br>
                <table aling="center" style=" width: 100%" >
                    <tr>          
                    <input type="hidden" id="idMetaAnul" name="idMetaAnul">    
                    <td style="width: 10%"><span>Nombre</span></td>                          
                    <td style="width: 50%"><input type="text" id="nommetaprocesoAnul" name="nommetaproceso" style=" width: 275px" readonly></td>
                    </tr>
                    <tr>
                        <td style="width: 10%"><span>Descripcion:</span></td>   
                        <td style="width: 90%" colspan="4"><textarea id ="descmetaprocesoAnul" name="descmetaproceso" style="width:565px;resize:none"  rows="2" readonly></textarea></td>                        
                    </tr>                         
                </table>
            </div>  
            </br>  
            <table border="0"  align="center">
                <tr>
                    <td>
                        <table id="procesos_internos_anul" ></table>
                        <div id="page_tabla_procesos_internos_anul"></div>
                    </td>
                </tr>
            </table>
        </div>                             

        <div id="dialogMsgMeta" title="Mensaje" style="display:none; visibility: hidden">
            <p style="font-size: 12.5px;text-align:justify;" id="msj" > Texto </p>
        </div>  
        <!--SOLO PRUEBA-->            
        <div id="dialogMsjmultiservicio" class="ui-widget" style="display:none;top: 14px;" >
            <table id="tablainterna"  >
                <tr>
                    <td>
                        <label style="font-family: Tahoma,Arial; padding-left: 5px;font-size: 14px; ">Nombre Categoria</label>
                        <input type="text" id="multiser"style="width: 119px;font-family: Tahoma,Arial;color: black;font-size: 13px;" > 
                    </td>
                </tr>
            </table>

            <table id="tabla_multiservicio" >

            </table>
        </div>

        <!------------------------------->



    </body>
</html>