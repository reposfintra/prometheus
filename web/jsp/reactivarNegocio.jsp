<%-- 
    Document   : reactivarNegocio
    Created on : 4/08/2016, 11:51:35 AM
    Author     : dvalencia
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/reactivarNegocio.js"></script> 
        <title>REACTIVACION DE NEGOCIO</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=REACTIVACION DE NEGOCIO"/>
        </div>
    <center>
        <div id="tablita" style="top: 150px;width: 780px;height: 95px;" >
            <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                    &nbsp;
                    <label class="titulotablita" ><b>FILTRO DE BUSQUEDA</b></label>
                </span>
            </div>
            <div class="row" style="padding: 5px">
                <div class="col-md-12 ">
                    <table id="tablainterna" style="height: 53px; width: 750px"  >
                        <tr>
                            <td>
                                <label>Identificacion</label>
                            </td>
                            <td>
                                <input type="text" id="identificacion" name="identificacion" style="height: 15px;width: 88px;color: #070708;" >
                            </td>
                            <td>
                                <label>Numero Solicitud</label>
                            </td>
                            <td>
                                <input type="text" id="numero_solicitud" name="numero_solicitud" style="height: 15px;width: 67px;color: #070708;" onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')" >
                            </td>
                            <td>
                                <label for="fecha">Fecha Credito:</label>
                            </td>
                            <td>
                                <input type="text" id="fecha_ini" readonly style="width:75px"/>
                            </td>
                            <td>
                                <input type="text" id="fecha_fin" readonly style="width:75px"/>
                            </td>
                            <td >  
                                <div style="padding-top: 10px">
                                    <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 10px;top: -7px;" > 
                                        <span class="ui-button-text">Buscar</span>
                                    </button>  
                                </div>
                            </td>
                            <td>
                                <div style="padding-top: 10px">
                                    <button id="limpiar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: -2px;top: -7px;" > 
                                        <span class="ui-button-text">Limpiar</span>
                                    </button>  
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div style="position: relative;top: 200px;">
            <table id="tabla_reactivarNegocio" ></table>
            <div id="pager"></div>
        </div>
        <div id="reactivar" style="display: none" >
            <table style="width: 100%">
            <tr>                          
                <td>
                    <label>Numero Solicitud</label>
                </td>
                <td>
                    <input type="text" id="num_solicitud" style="height: 15px;width: 67px;color: #070708;" readonly >
                </td>      
            </tr>
            <tr>                          
                <td>
                    <label>Causal</label>
                </td>
                <td>
                    <select id="comentario" name="estado_solicitud" style="height: 23px;width: 220px;color: #070708;">
                        <option value="">--Seleccione una opcion--</option>
                        <option value="BUEN HABITO DE PAGO INTERNO">BUEN HABITO DE PAGO INTERNO</option>
                        <option value="PILOTO SCORE CUENTAS MICRO">PILOTO SCORE CUENTAS MICRO</option>
                        <option value="PREAPROBADO">PREAPROBADO</option>
                        <option value="RENOVACION">RENOVACION</option>
                        <option value="OTRO">OTRO</option>
                    </select>
                </td>
                
            </tr> 
            </table>
        </div>
        <div id="info"  class="ventana"  >
            <p id="notific">EXITO AL REACTIVAR</p>
        </div>
    </center>
</body>
</html>
