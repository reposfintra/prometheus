<%-- 
    Document   : reporteAsignacionCartera
    Created on : 17/09/2019, 07:47:21 AM
    Author     : mcamargo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/filtros_Style.css" rel="stylesheet" type="text/css"/>
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script> 
        <script src="./js/reporteAsignacionCartera.js" type="text/javascript"></script>
        <title>Reporte Asignacion Cartera</title>
    </head>
    <body>
        <div>
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte Cartera"/>
        </div>

        <div class="filtro">
            <div class="header_filtro">
                <label>FILTRO DE BUSQUEDA</label>
            </div>
            <div class="body_filtro">
                <div class="caja">
                       <label> Unidad Negocio:</label>
                       <select id="id_unidad_negocio" name="id_unidad_negocio" class="select">
                       </select>
                </div> 
                <div class="caja">
                       <label> Entidad:</label>
                       <select id="id_recaudador_tercero" name="id_recaudador_tercero" class="select">
                       </select>
                </div>
                <div class="caja">
                    <button class="button" onclick="cargarReporteNegocios();">Buscar</button>
                </div>
            </div>

        </div>
        <center>
        <div  style="margin-top: 20px; " >
                <table id="tabla_negocios" ></table>
                <div id="pager"></div>
        </div>
        <div id="dialogMsj" title="Mensaje" style="display:none; .hide-close .ui-dialog-titlebar-close { display: none }">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
        </div>  
        <div id="info"  class="ventana" >
                <p id="notific"></p>
            </div>
            
            <div id="dialogLoading" style="display:none;">
                <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                <center>
                    <img src="./images/cargandoCM.gif"/>
                </center>
            </div>            
            </center>
    </body>
</html>
