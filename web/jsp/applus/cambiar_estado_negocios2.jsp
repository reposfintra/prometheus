<%--    Created on : 02/02/2009    Author     : iamorales--%>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.operation.model.*"%>
<%@ page import    ="com.tsp.operation.model.beans.*"%>
<%@ page import    ="com.tsp.operation.model.services.*"%>
<%@ page import    ="com.tsp.util.*"%>
<%@ page import    ="com.tsp.util.Util.*"%>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<%
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);

	Usuario usuario = (Usuario) session.getAttribute("Usuario");
	String loginx=usuario.getLogin();

	ClientesVerService clvsrv = new ClientesVerService(usuario.getBd());
        String perfil = clvsrv.getPerfil(usuario.getLogin());

	String estadi=request.getParameter("estadito");
	if (estadi==null){estadi="0";}

	String contrati=request.getParameter("contratistica");
	if (contrati==null){contrati="";}

	String fact_conformada_consultar=request.getParameter("fact_conformada_consultar");
	if (fact_conformada_consultar==null){fact_conformada_consultar="";}

	String id_solici=request.getParameter("id_solici");//090922
	if (id_solici==null){id_solici="";}//090922

	String id_cliente=(request.getParameter("id_cliente")==null)?"":request.getParameter("id_cliente");//090922
	if (id_cliente==null){id_cliente="";}//090922

	String nicc=request.getParameter("nicc");//090922
	if (nicc==null){nicc="";}//090922

	String nomclie=request.getParameter("nomclie");//090922
	if (nomclie==null){nomclie="";}//090922

	String num_osxi=request.getParameter("num_osx");
	if (num_osxi==null){num_osxi="";}

	ArrayList negocios ;

	try {
            negocios = model.negociosApplusService.getNegociosApplus2(estadi,contrati,num_osxi,fact_conformada_consultar,loginx,id_solici,nicc ,"",id_cliente);
	}
        catch(Exception e ){
            System.out.println("errorcillo en model.NegociosApplusService.getNegociosApplus2()"+e.toString());
            negocios=null;
	}
%>
<html>
<%try{%>
<head>
    <title>Gestion de Negocios</title>

    <link href="<%=BASEURL%>/css/estilostsp.css"    rel="stylesheet" type="text/css">

    <script src="<%=BASEURL%>/js/boton.js"          type="text/javascript"></script>
    <script src="<%=BASEURL%>/js/Validacion.js"     type='text/javascript'></script>
    <script src="<%=BASEURL%>/js/tools.js"          type="text/javascript"></script>
    <script src="<%=BASEURL%>/js/date-picker.js"    type="text/javascript"></script>
    <script src="<%=BASEURL%>/js/transferencias.js" type="text/javascript"></script>
    <script src="<%=BASEURL%>/js/prototype.js"      type="text/javascript"></script>

    <!-- Las siguientes librerias CSS y JS son para el manejo de
        DIVS dinamicos.
    -->

    <link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css">
    <link href="<%=BASEURL%>/css/mac_os_x.css"      rel="stylesheet" type="text/css">

    <script src="<%=BASEURL%>/js/effects.js"        type="text/javascript"></script>
    <script src="<%=BASEURL%>/js/window.js"         type="text/javascript"></script>
    <script src="<%=BASEURL%>/js/window_effects.js" type="text/javascript"></script>

    <!-- Las siguientes librerias CSS y JS son para el manejo del
        calendario(jsCalendar).
    -->

    <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/jscal2.css" />
    <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/border-radius.css" />

    <script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
    <script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>

    <%
    if (request.getParameter("nadita")!=null && request.getParameter("nadita").equals("si")){%>
        <script>alert("No se hizo el cambio por restricci�n de seguridad.");</script>
    <%}
    %>
    <script>
        var temp=';;;;;;;;;;';

        var generar=0;

        function openWin(i){
            var cad = $('select_opt'+i).value;
            cad = cad.split(';');

            if(cad[0] == '1'){
                window.open('<%=BASEURL%>/jsp/delectricaribe/generar_oferta.jsp?id_solicitud='+cad[1],'','width=1000,height=800');
            }
            if(cad[0] == '2'){
                window.open('<%=BASEURL%>/jsp/delectricaribe/aceptacion_ot.jsp?id_padre='+cad[1]+'&id_sol='+cad[2]+'&estado='+cad[3],'','width=1000,height=800');
            }
            if(cad[0] == '3'){
                window.open('<%=BASEURL%>/jsp/delectricaribe/generar_OT.jsp?id_padre='+cad[1]+'&id_sol='+cad[2]+'&cons='+cad[3],'','width=1000,height=800');
            }
            if(cad[0] == '4'){
                showWin(cad[1]);
            }
            if(cad[0] == '5'){
                showWinRecepcionObra(cad[1]);
            }
        }

        function showWinRecepcionObra(idxx){
            var content = '';

            var win = new Window({/*className: "mac_os_x",*/
                            id: "recepobr",
                            title: "RECEPCION_OBRA",
                            width:1150,
                            height:600,
                            showEffectOptions: {duration:0},
                            hideEffectOptions: {duration:0},
                            destroyOnClose: true,
                            //url:'<%=BASEURL%>/jsp/delectricaribe/recepcion_obra.jsp',
                            url:'<%=CONTROLLER%>?estado=Negocios&accion=Applus&opcion=recibirObra&solicitud='+idxx,
                            recenterAuto: false});

            win.showCenter();

        }

        function showWin(id){
            var content = '';

            var win = new Window({/*className: "mac_os_x",*/
                            title: "FINANCIACION",
                            width:300,
                            height:150,
                            showEffectOptions: {duration:0},
                            hideEffectOptions: {duration:0},
                            destroyOnClose: true,
                            recenterAuto: false});

            content = '<div align="center">';
            content += '<br>';
            content += '<br>';
            content += '<a style="font-size:12px;">Fecha de financiacion:</a>';

            content += '<br>';
            content += '<input id="calendar-field" readonly>';
            content += '<button id="calendar-trigger">...</button>';

            content += '<br>';
            content += '<img src="<%=BASEURL%>/images/botones/aceptar.gif" height="21" title="Aceptar" onclick="ajaxFinanciacion('+id+');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">';
            content += '</div>';

            content += '<br>';
            content += '<div align="center" name="loading_div" id="loading_div" style="visibility: hidden">';
            content += 'Cargando...';
            content += '</div>';

            win.getContent().update( content );
            win.showCenter();

            Calendar.setup({
                inputField : "calendar-field",
                trigger    : "calendar-trigger",
                onSelect   : function() { this.hide() }
            });
        }

        function ajaxFinanciacion(id){
            if(generar==0){
                var p = 'opcion=4&idsol='+id+'&fecha='+$('calendar-field').value;

                new Ajax.Request(
                '<%=CONTROLLER%>?estado=Electricaribe&accion=Ot',
                {
                    method: 'get',
                    parameters: p,
                    onLoading: whenLoading,
                    onComplete: ajaxFinanFinish
                });
            }
            else{
                alert('Solicitud en proceso');
            }
        }

        function switchDiv(){
            if($('loading_div').style.visibility == 'visible'){
                $('loading_div').style.visibility = 'hidden';
            }
            else{
                $('loading_div').style.visibility = 'visible';
            }
        }

        function whenLoading(){
            switchDiv();
            generar = 1;
        }

        function ajaxFinanFinish(response){
            alert(response.responseText);
            switchDiv();
            generar = 0;
        }

        function seleccionarOrdenDeAccion(theForm,orden,che){
            if (che){
                for (i=0;i<theForm.length;i++){
                    if (theForm.elements[i].type=='checkbox' && theForm.elements[i].title=="s"+orden){
                        theForm.elements[i].checked=che;
                    }
                }
            }
        }

	function asignarEstado(theForm){
            if (theForm.estado_asignable.value=="0"){
                alert("Debe seleccionar 1 estado a asignar.");
                return;
            }

            var elementos   = document.getElementsByName("idAcc");
            var sw          = false;

            for (var i=0;i<elementos.length; i++){
                    if (elementos[i].checked){
                            sw = true;
                    }
            }
            if(sw==true){
                    formulario.action="<%=CONTROLLER%>?estado=Negocios&accion=Applus&opcion=asignarEstado";
                    theForm.submit();
            }else{
                    alert("Debe seleccionar por lo menos 1 'id_accion'.");
            }
        }

        function checkEnter(e){ //e is event object passed from function invocation//090720
                var characterCode //literal character code will be stored in this variable

                if(e && e.which){ //if which property of event object is supported (NN4)
                        e = e
                        characterCode = e.which //character code is contained in NN4's which property
                }else{
                        e = event
                        characterCode = e.keyCode //character code is contained in IE's keyCode property
                }

                if(characterCode == 13){ //if generated character code is equal to ascii 13 (if enter key)
                        //document.forms[0].submit() //submit the form
                        //alert("enter");
                        //return false
                        consultarEstado();
                }else{
                        //alert("e"+e+"char"+characterCode);
                        return true
                }
        }

        function formatx(input){//090831
                var num=""+input;
                num = num.replace(/\./g,'');
                if(!isNaN(num)){
                        num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
                        num = num.split('').reverse().join('').replace(/^[\.]/,'');
                        input = num;
                }else{
                        alert('Solo se permiten n�meros');
                        //input.value = input.value.replace(/[^\d\.]*/g,'');
                }
                return num;
        }

        function sumarOferta(solicitt){
                var vals = document.getElementsByName(solicitt);
                var respuest=0;
                var temx=0;
                for (var i=0;i<vals.length; i++){
                        temx=vals[i].value.replace(",","");
                        temx=temx.replace(",","");
                        temx=temx.replace(",","");//091216
                        //subtotal=parseInt(subtotal)+parseInt(tem);
                        respuest=parseInt(respuest)+parseInt(temx);
                        //respuest=respuest+vals[i].value;
                }
                alert("oferta: "+formatx(respuest));
        }

        function validar(theForm){
                var elementos = document.getElementsByName("idNegocio");
                var sw = false;

                for (var i=0;i<elementos.length; i++){
                    if (elementos[i].checked){
                        sw = true;
                    }
                }

                if(sw==true){
                    //alert ("cas");
                    formulario.action="<%=CONTROLLER%>?estado=Negocios&accion=Applus&opcion=cambiarEstado";
                    theForm.submit();
                }
                else{
                    alert("Verifique que haya al menos un negocio seleccionado.");
                }
        }

        function Sell_all_col(theForm,nombre){

            for (i=0;i<theForm.length;i++)
                if (theForm.elements[i].type=='checkbox' && theForm.elements[i].name==nombre)
                    theForm.elements[i].checked=theForm.All.checked;
        }

        function consultarCliente(){
                if (formulario.nicc.value=="" && formulario.idclie.value==""){
                        alert("Debe tener nic o nombre de cliente.");
                }else{
                        formulario.target="_blank";
                        formulario.action="<%=CONTROLLER%>?estado=Clientes&accion=Ver&opcion=buscarcliente";
                        formulario.submit();
                }
        }

        function seleccionarOrden(theForm,orden,che){
            for (i=0;i<theForm.length;i++){
                if (theForm.elements[i].type=='checkbox' && theForm.elements[i].value==orden){
                    theForm.elements[i].checked=che;
                }
            }
        }

        function consultarEstado(){
                formulario.target ="";
                formulario.action="<%=CONTROLLER%>?estado=Negocios&accion=Applus&opcion=consultarEstado2";
                formulario.submit();

        }

        function asignar(theForm){

            var elementos = document.getElementsByName("idNegocio");
            var sw = false;

            for (var i=0;i<elementos.length; i++){
                    if (elementos[i].checked){
                            sw = true;
                    }
            }
            if(sw==true){
                    formulario.action="<%=CONTROLLER%>?estado=Negocios&accion=Applus&opcion=asignar";
                    theForm.submit();
            }else{
                    alert("Verifique que haya al menos una solicitud seleccionada.");
            }
        }

        function sendAction(url,filtro,nomselect,def){
            if(temp!=filtro){
                $('imgworking').style.visibility="visible";
                temp=filtro;
                var p = "filtro=" + filtro + "&nomselect=" + nomselect  + "&opcion=clselect" + "&idclie="+def;

                new Ajax.Request(
                url,
                {
                    method: 'get',
                    parameters: p,
                    onSuccess: deploySelect
                });

            }
        }

        function deploySelect(response){
            respuesta=response.responseText.split(';;;;;;;;;;');
            if(respuesta[0]==$('nomclie').value){
                document.getElementById("cliselect").innerHTML=respuesta[1];
                $('imgworking').style.visibility="hidden";
            }
        }

        /* Esta funcion init() se inicializa
         * en el onLoad del body.
         */
        function init(){
            redimensionar();
            formulario.id_solici.focus();
            sendAction('<%=CONTROLLER%>?estado=Negocios&accion=Applus',document.getElementById('nomclie').value,'idclie','<%=(request.getParameter("id_cliente")!=null)?request.getParameter("id_cliente"):""%>');
        }
    </script>
</head>
<body onLoad="init();" onResize="redimensionar();">

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Negocios"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: -1px; top: 100px; overflow: scroll;">
<%
   ArrayList listaEstados       = model.negociosApplusService.getEstadosApplus2();
   ArrayList listaEstadosUser   = model.negociosApplusService.getEstadosApplusUser(loginx);
   ArrayList contratistas       = model.negociosApplusService.getContratistas();

   String nombre_estado="";
   String nombre_contratista="";

   String[] estadixx;

   String[] contratixx;

   for (int i=0;i<listaEstados.size();i++){
		estadixx=(String[]) listaEstados.get(i);
		if(estadixx[0].equals(estadi)){ nombre_estado=estadixx[1];}
   }
   for (int i=0;i<contratistas.size();i++){
		contratixx=(String[]) contratistas.get(i);
		if(contratixx[0].equals(contrati)){ nombre_contratista=contratixx[1];}
   }

   if (loginx!=null ){
        if ( usuario.getNitPropietario().equals("CC002") ){contrati="CC002";}
        if ( usuario.getNitPropietario().equals("CC004") ){contrati="CC004";}
        if ( usuario.getNitPropietario().equals("CC008") ){contrati="CC008";}
        if ( usuario.getNitPropietario().equals("CC009") ){contrati="CC009";}
        if ( usuario.getNitPropietario().equals("CC010") ){contrati="CC010";}
        if ( usuario.getNitPropietario().equals("CC011") ){contrati="CC011";}
        if ( usuario.getNitPropietario().equals("CC012") ){contrati="CC012";}
        if ( usuario.getNitPropietario().equals("CC014") ){contrati="CC014";}
        if ( usuario.getNitPropietario().equals("CC016") ){contrati="CC016";}
        if ( usuario.getNitPropietario().equals("CC017") ){contrati="CC017";}
        if ( usuario.getNitPropietario().equals("CC019") ){contrati="CC019";}
        if ( usuario.getNitPropietario().equals("CC026") ){contrati="CC026";}
        if ( usuario.getNitPropietario().equals("CC027") ){contrati="CC027";}
        if ( usuario.getNitPropietario().equals("CC028") ){contrati="CC028";}
        if ( usuario.getNitPropietario().equals("CC029") ){contrati="CC029";}
        if ( usuario.getNitPropietario().equals("CC030") ){contrati="CC030";}
        if ( usuario.getNitPropietario().equals("CC031") ){contrati="CC031";}
        if ( usuario.getNitPropietario().equals("CC036") ){contrati="CC036";}
        if ( usuario.getNitPropietario().equals("CC038") ){contrati="CC038";}
        if ( usuario.getNitPropietario().equals("CC051") ){contrati="CC051";}
        if ( usuario.getNitPropietario().equals("CC052") ){contrati="CC052";}
        if ( usuario.getNitPropietario().equals("CC053") ){contrati="CC053";}
        if ( usuario.getNitPropietario().equals("CC054") ){contrati="CC054";}
        if ( usuario.getNitPropietario().equals("CC081") ){contrati="CC081";}
        if ( usuario.getNitPropietario().equals("CC082") ){contrati="CC082";}
        if ( usuario.getNitPropietario().equals("CC099") ){contrati="CC099";}
        if ( usuario.getNitPropietario().equals("CC100") ){contrati="CC100";}
        if ( usuario.getNitPropietario().equals("CC666") ){contrati="CC666";}
        if ( usuario.getNitPropietario().equals("CC667") ){contrati="CC667";}
        if ( usuario.getNitPropietario().equals("CC668") ){contrati="CC668";}
   }

   if (negocios!=null  ){
%>
    <form  method="post" name="formulario"  >
    <input type="hidden" name="estadito" value="<%=estadi%>">
    <%
    String respuesta=request.getParameter("respuesta");
	//String valor_neto="";//request.getParameter("valor_neto");
    %>
    <br><br>
	<table align="center">
		<tr class="fila">
			<td>
				&nbsp;&nbsp;Estado actual:&nbsp;&nbsp;
			</td><td>
				<select name="estado_consultar" onChange="consultarEstado();">

					<option value="<%=estadi%>"><%=nombre_estado%></option>
					<%
					for (int i=0;i<listaEstados.size();i++){
						String[] estadinho=(String[])listaEstados.get(i);
						%>
						<option value="<%=estadinho[0]%>"><%=estadinho[1]%></option>
						<%
					}
					%>

				</select>
				&nbsp;&nbsp;
			</td>
		</tr>
		<tr class="fila">
                    <td>
                        &nbsp;&nbsp;Contratista:&nbsp;&nbsp;
                    </td>
                    <td>
                        <%if ( usuario.getNitPropietario().equals("CC002") ||
                               usuario.getNitPropietario().equals("CC004") ||
                               usuario.getNitPropietario().equals("CC008") ||
                               usuario.getNitPropietario().equals("CC009") ||
                               usuario.getNitPropietario().equals("CC010") ||
                               usuario.getNitPropietario().equals("CC011") ||
                               usuario.getNitPropietario().equals("CC012") ||
                               usuario.getNitPropietario().equals("CC014") ||
                               usuario.getNitPropietario().equals("CC016") ||
                               usuario.getNitPropietario().equals("CC017") ||
                               usuario.getNitPropietario().equals("CC019") ||
                               usuario.getNitPropietario().equals("CC026") ||
                               usuario.getNitPropietario().equals("CC027") ||
                               usuario.getNitPropietario().equals("CC028") ||
                               usuario.getNitPropietario().equals("CC029") ||
                               usuario.getNitPropietario().equals("CC030") ||
                               usuario.getNitPropietario().equals("CC031") ||
                               usuario.getNitPropietario().equals("CC036") ||
                               usuario.getNitPropietario().equals("CC038") ||
                               usuario.getNitPropietario().equals("CC051") ||
                               usuario.getNitPropietario().equals("CC052") ||
                               usuario.getNitPropietario().equals("CC053") ||
                               usuario.getNitPropietario().equals("CC054") ||
                               usuario.getNitPropietario().equals("CC081") ||
                               usuario.getNitPropietario().equals("CC082") ||
                               usuario.getNitPropietario().equals("CC099") ||
                               usuario.getNitPropietario().equals("CC100") ||
                               usuario.getNitPropietario().equals("CC666") ||
                               usuario.getNitPropietario().equals("CC668") ||
                               usuario.getNitPropietario().equals("CC667") )
                        {%>
                        <%//if (loginx!=null && (  loginx.equals("SENTEL") )) {%>
                                <input type="text" name="contratista_consultar"  value="<%=contrati%>" readonly  >
                        <%}else{%>
                                <select name="contratista_consultar"  >
                                        <option value="<%=contrati%>"><%=nombre_contratista%></option>
                                        <option value=""> </option>
                                        <%
                                        for (int i=0;i<contratistas.size();i++){
                                                String[] contratistanho=(String[])contratistas.get(i);
                                                %>
                                                <option value="<%=contratistanho[0]%>"><%=contratistanho[1]%></option>
                                                <%
                                        }
                                        %>
                                </select>
                        <%}%>
                        &nbsp;&nbsp;
                    </td>
		</tr>

		<tr class="fila">
		<td>
                    &nbsp;&nbsp;Multiservicio&nbsp;:&nbsp;
		</td>
                <td>
                    <input type="text" name="num_osx"  value="<%=num_osxi%>"  onKeyPress="checkEnter(event)" >
		</td>
	</tr>

	<input type="hidden" name="fact_conformada_consultar" value="">

	<tr class="fila">
		<td>
			&nbsp;&nbsp;Id Solicitud&nbsp;:&nbsp;
		</td><td>
			<input type="text" name="id_solici"  value="<%=id_solici%>"  onKeyPress="checkEnter(event)">
			<img src='<%=BASEURL%>/images/botones/iconos/buscar.gif' width="15" height="15" onClick="consultarEstado();" title="Buscar Solicitud" style="cursor:hand" >
		</td>
	</tr>
	<tr class="fila">
		<td>
			&nbsp;&nbsp;Nic&nbsp;:&nbsp;
		</td><td>
			<input type="text" name="nicc"  value="<%=nicc%>"  onKeyPress="checkEnter(event)" >
		</td>
	</tr>
	<tr class="fila">
		<td>
			&nbsp;&nbsp;Nombre Cliente&nbsp;:&nbsp;
		</td><td>
			<!--<input type="text" name="nomclie"  value="<%//=nomclie%>"  onKeyPress="checkEnter(event)" >-->
                        <input name="nomclie" id="nomclie" type="text" value="<%=nomclie%>"  onKeyPress="checkEnter(event)" class="textbox" id="campo" style="width:200;"  onChange="sendAction('<%=CONTROLLER%>?estado=Negocios&accion=Applus',document.getElementById('nomclie').value,'idclie','');" size="15" >

		</td>
	</tr>
	<tr class="fila">
		<td>&nbsp;

		</td>
                <td>
                    <div id="cliselect"></div>
                    <div id="imgworking" align="right"  style="visibility:hidden"><img src="<%=BASEURL%>/images/cargando.gif"></div>

                    <%if ( (clvsrv.ispermitted(perfil, "5")||clvsrv.ispermitted(perfil, "6")) ){%>
                        <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif' width="15" height="15" onClick="consultarCliente();" title="Buscar Cliente" style="cursor:hand" >&nbsp;&nbsp;
                    <%}%>
                    <%if ( (clvsrv.ispermitted(perfil, "5")||clvsrv.ispermitted(perfil, "6")) ){%>
                        <a target="_blank" href="<%=CONTROLLER%>?estado=Clientes&accion=Ver&opcion=N">crear cliente</a>
                    <%}%>
                </td>
        </tr>
        </table>
        <br>
	<table width="689"  border="2" align="center">
		<tr>
		    <td >
			<table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
                            <tr class="fila">
                                <th nowrap style="font-size:11; font-weight: bold"> <input type='checkbox'  id='All' onClick="Sell_all_col(this.form,'idNegocio');">  </TH>

                                <th> &nbsp;ID SOLICITUD&nbsp;  </th>
                                <th> &nbsp;CONSECUTIVO&nbsp;  </th>
                                <th> &nbsp;No. ORDEN DE TRABAJO&nbsp;  </th>
                                <th> &nbsp;ACCION&nbsp;  </th>
                                <th> &nbsp;ESTADO&nbsp;</th>
                                <th> &nbsp;ENTREGA OFERTA&nbsp;</th>
                                <th> &nbsp;ID CLIENTE&nbsp;</th>
                                <th> &nbsp;NIC&nbsp;</th>
                                <th> &nbsp;NIT&nbsp;</th>
                                <th> &nbsp;NOMBRE CLIENTE&nbsp;</th>
                                <th> &nbsp;TIPO CLIENTE&nbsp;</th>

                                <th> &nbsp;NOMBRE CONTRATISTA&nbsp;</th>

                                <%if (clvsrv.ispermitted(perfil, "15")){%>
                                    <th> &nbsp;VALOR MATERIAL&nbsp;</th>
                                    <th> &nbsp;VALOR MANO DE OBRA&nbsp;</th>
                                    <th> &nbsp;VALOR OTROS&nbsp;</th>
                                    <th> &nbsp;ADMINISTRACION&nbsp;</th>
                                    <th> &nbsp;IMPREVISTOS&nbsp;</th>
                                    <th> &nbsp;UTILIDAD&nbsp;</th>
                                    <th> &nbsp;PORCENTAJE ADMINISTACION&nbsp;</th>
                                    <th> &nbsp;PORCENTAJE IMPREVISTO&nbsp;</th>
                                    <th> &nbsp;PORCENTAJE UTILIDAD&nbsp;</th>
                                <%}%>

                                <th> &nbsp;COSTO CONTRATISTA&nbsp;</th>

                                <%if (!perfil.equals("contratista")){%>
                                    <th> &nbsp;PRECIO VENTA&nbsp;</th>
                                <%}%>

                                <th> &nbsp;ACCIONES&nbsp;</th>
                            </tr>
                            <%
                            NegocioApplus negocioApplus;
                            String idsolneg ="";
                            String idaccion ="";
                            String cons ="";

                            int estado      = 0;//20100214

                            for (int i=0; i<negocios.size(); i++) {
                                negocioApplus   = (NegocioApplus)negocios.get(i);
                                idsolneg        = negocioApplus.getIdSolicitud();
                                cons            = negocioApplus.getConsecutivo_oferta();
                                idaccion        = negocioApplus.getIdAccion();

                                estado          = Integer.parseInt(negocioApplus.getIdEstado());//20100214

                                String valor_negocio=negocioApplus.getVlr();
                                if (valor_negocio==null || valor_negocio.equals("")){
                                    valor_negocio="0";
                                }

                                String value = negocioApplus.getIdSolicitud();
                            %>
                            <tr class='<%= (i%2==0?"filagris":"filaazul") %>' id='fila<%=i%>' style="font-size:12px;">
                                <td class="bordereporte" align="center" style="font-size:11px;" nowrap>
                                    <input type='checkbox' id="id<%=i%>" name='idNegocio' value='<%=value%>' onClick="cambiarColorMouse(fila<%=i%>); seleccionarOrden(formulario,this.value,this.checked);$('id_solicitud').value = <%=value%>"">
                                </td>

                                <td class="bordereporte" align="center" style="font-size:11px;" nowrap>
                                    <a style="font-size: 12px;" href="<%=CONTROLLER%>?estado=Clientes&accion=Ver&opcion=buscarsolicitud&idsolicitud=<%=negocioApplus.getIdSolicitud()%>" target="_blank">
                                        <%=negocioApplus.getIdSolicitud()%>
                                    </a>
                                    &nbsp;
                                    <select id="select_opt<%=i%>" onchange="openWin(<%=i%>);" style="font-size:11px;">
                                        <option>...</option>
                                        <%if (clvsrv.ispermitted(perfil, "25") && model.ElectricaribeOfertaSvc.isOfferReady(negocioApplus.getIdSolicitud())){%><!--si se tiene el PERMISO PARA GENERAR OFERTA y NO EXISTE UNA ACCION  PARA ESA SOLICITUD QUE TENGA ESTADO MENOR QUE PENDIENTE POR GENERAR OFERTA O MAYOR QUE EN EJECUCION-->
                                        <option value="1;<%=idsolneg%>">GENERAR OFERTA</option>
                                        <%}%>
                                        <%if ((clvsrv.ispermitted(perfil, "26") && model.ElectricaribeOtSvc.isPaymentReady(idsolneg) && estado>=60)){%><!--si se tiene el PERMISO PARA ACEPTAR PAGOS y  NO EXISTE UNA ACCION CON ESTADO MENOR QUE "PENDIENTE POR ACEPTACION DEL CLIENTE" O MAYOR QUE "EN EJECUCION" O (ES MAYOR QUE EN EJECUCION Y MENOR QUE "INGRESADO EN OPEN" Y SE TIENE PERMISO ESPECIAL)-->
                                        <option value="2;<%=negocioApplus.getIdCliente()%>;<%=idsolneg%>;<%=estado%>">ACEPTAR PAGOS</option>
                                        <%}%>
                                        <%if (clvsrv.ispermitted(perfil, "27") && model.ElectricaribeOtSvc.isOtReady(idsolneg)){%><!--si tiene PERMISO PARA GENERAR OT Y NO EXISTE UNA ACCION PARA ESA SOLICITUD ANTES DE "PENDIENTE POR EJECUCION" NI DESPUES DE EN EJECUCION-->
                                        <option value="3;<%=negocioApplus.getIdCliente()%>;<%=idsolneg%>;<%=cons%>">GENERAR OT</option>
                                        <%}%>
                                        <%if ((clvsrv.ispermitted(perfil, "9") && estado<=80 && estado>60) || ( (estado>80) && (estado<110) && (clvsrv.ispermitted(perfil, "20")) )){%><!--SI TIENE PERMISO PARA PONER FECHA DE FINANCIACION Y ESTADO MENOR O IGUAL QUE EN EJECUCION O (EL ESTADO ES MAYOR QUE EN EJECUCION Y MENOR QUE INGRESADO EN OPEN Y TIENE PERMISO ESPECIAL-->
                                        <option value="4;<%=idsolneg%>">FINANCIACION</option>
                                        <%}%>
                                        <%if (clvsrv.ispermitted(perfil, "19") && estado>=80) {%><!--SI TIENE PERMISO PARA RECIBIR OBRA Y ESTADO ES EJECUCION o mayor -->
                                        <option value="5;<%=idsolneg%>">RECEPCION OBRA</option>
                                        <%}%>
                                </select>

                                </td>

                                <td class="bordereporte" style="font-size:11px;" nowrap align="center"> <%= negocioApplus.getConsecutivo_oferta()%></td>
                                <td class="bordereporte" style="font-size:11px;" nowrap align="center"> <%= negocioApplus.getNumOs()%></td>

                                <td class="bordereporte" nowrap align="center">
                                    <input type='checkbox' id="idacc<%=i%>" name='idAcc' title="s<%=negocioApplus.getIdSolicitud()%>" value='<%=negocioApplus.getIdAccion()%>' onClick=" seleccionarOrdenDeAccion(formulario,<%=negocioApplus.getIdSolicitud()%>,this.checked);">
                                    <%= negocioApplus.getIdAccion()%>
                                </td>

                                <td class="bordereporte" style="font-size:11px;" nowrap align="center"> <%= negocioApplus.getEstado()%></td>
                                <td class="bordereporte" style="font-size:11px;" nowrap align="center" title="<%= negocioApplus.getUsuarioEntregaOferta()+"_"+negocioApplus.getCreacionFechaEntregaOferta()%>"> <%=  negocioApplus.getFechaEntregaOferta()%></td>
                                <td class="bordereporte" style="font-size:11px;" nowrap align="center"> <%= negocioApplus.getIdCliente()%></td>
                                <td class="bordereporte" style="font-size:11px;" nowrap align="center"> <%= negocioApplus.getNicClient()%></td>
                                <td class="bordereporte" style="font-size:11px;" nowrap align="center"> <%= negocioApplus.getNitClient()%></td>
                                <td class="bordereporte" style="font-size:11px;" nowrap align="center">   <%= negocioApplus.getNombreCliente()%></td>
                                <td class="bordereporte" style="font-size:11px;" nowrap align="center"> <%= negocioApplus.getTipoCliente()%></td>
                                <td class="bordereporte" style="font-size:11px;" nowrap align="center">   <%= negocioApplus.getNombreContratista()%></td>

                                <%if (clvsrv.ispermitted(perfil, "15")){%>
                                    <td class="bordereporte" style="font-size:11px;" align="center"> <%=Util.customFormat(Double.parseDouble(negocioApplus.getMaterial()))%> </td>
                                    <td class="bordereporte" style="font-size:11px;" align="center"> <%=Util.customFormat(Double.parseDouble(negocioApplus.getMano_obra()))%> </td>
                                    <td class="bordereporte" style="font-size:11px;" align="center"> <%=Util.customFormat(Double.parseDouble(negocioApplus.getOtros()))%> </td>
                                    <td class="bordereporte" style="font-size:11px;" align="center"> <%=Util.customFormat(Double.parseDouble(negocioApplus.getAdministracion()))%> </td>
                                    <td class="bordereporte" style="font-size:11px;" align="center"> <%=Util.customFormat(Double.parseDouble(negocioApplus.getImprevisto()))%> </td>
                                    <td class="bordereporte" style="font-size:11px;" align="center"> <%=Util.customFormat(Double.parseDouble(negocioApplus.getUtilidad()))%> </td>
                                    <td class="bordereporte" style="font-size:11px;" align="center"> <%=negocioApplus.getPorc_a()%> </td>
                                    <td class="bordereporte" style="font-size:11px;" align="center"> <%=negocioApplus.getPorc_i()%> </td>
                                    <td class="bordereporte" style="font-size:11px;" align="center"> <%=negocioApplus.getPorc_u()%> </td>
                                <%}%>

                                <td class="bordereporte" style="font-size:11px;" nowrap align="center"> <%= Util.customFormat(Double.parseDouble(negocioApplus.getVlr()))%></td>

                                <%if (!perfil.equals("contratista")){%>
                                    <td class="bordereporte" style="font-size:11px;" nowrap align="center" >
                                        <a onClick="sumarOferta(<%=value%>);" style="cursor:hand" title="sumar">
                                            <%= Util.customFormat(Double.parseDouble(negocioApplus.getEcaOferta()))%>
                                        </a>
                                    </td>
                                <%}%>

                                <input type="hidden" name="<%=value%>" value="<%=Util.customFormat(Double.parseDouble(negocioApplus.getEcaOferta()))%>">

                                <td class="bordereporte" style="font-size:11px;" nowrap align="left">
                                    <%if (negocioApplus.getAcciones()!=null && negocioApplus.getAcciones().length()>50){%>
                                        <a target="_blank" href="<%=CONTROLLER%>?estado=Negocios&accion=Applus&opcion=consultarAlcances&id_solici=<%=  negocioApplus.getIdSolicitud()%>&id_accion=<%=negocioApplus.getIdAccion()%>"><%=  negocioApplus.getAcciones().substring(0,30)%>...</a>
                                    <%}else{%>
                                        <a target="_blank" href="<%=CONTROLLER%>?estado=Negocios&accion=Applus&opcion=consultarAlcances&id_solici=<%=  negocioApplus.getIdSolicitud()%>&id_accion=<%=negocioApplus.getIdAccion()%>"><%=  negocioApplus.getAcciones()%></a>
                                    <%}%>
                                </td>

                            </tr>

			<% } %>

			</table>
		    </td>
		</tr>
	</table>
	<br>
	<center>
	<br>
	<table>
	<%
	boolean sw_modificador=false;
	%>
	</div>
	</table>
	<%
	String hoy = Utility.getHoy("-");
	boolean poner_fecha_entrega_oferta=false;
	if (negocios!=null && negocios.size()>0 && estadi.equals("050")){
		poner_fecha_entrega_oferta=true;
	}
	if (poner_fecha_entrega_oferta){
	%>

	 <table align="center">
		<tr  class="fila">
			<td>&nbsp;Fecha de entrega de oferta :&nbsp;</td>
			<td >
				<input name="fecof" type="text" class="textbox" id="fecof" size="10" value="<%=hoy%>" >
				<span class="Letras"><img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onClick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fecof);return false;" HIDEFOCUS></span>

			</td>
                        <td>
                            &nbsp;&nbsp;<img src="<%=BASEURL%>/images/botones/aceptar.gif" height="21" title='Aceptar' onclick='asignar(formulario)' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
                        </td>
		</tr>
	 </table>
	<%}%>
        <%if (negocios!=null && negocios.size()>0 && estadi.equals("090") && clvsrv.ispermitted(perfil, "30")){//20100305%>
            <table>
            <tr class="fila">
                <td>
                    &nbsp;&nbsp;Estado asignable :&nbsp;&nbsp;
                </td>
                <td>
                    <select name="estado_asignable" >
                    <%
                        for (int i=0;i<listaEstados.size();i++){
                            String[] estadinho=(String[])listaEstados.get(i);
                            if (estadinho[0].equals("100")){
                                %>
                                <option value="<%=estadinho[0]%>"><%=estadinho[1]%></option>
                                <%
                            }
                        }
                    %>
                    </select>
                    &nbsp;&nbsp;
                </td>
                <td align="center" colspan="2">
                    &nbsp;&nbsp;<img src="<%=BASEURL%>/images/botones/aceptar.gif" height="21" title='Asignar Estado' onclick='asignarEstado(formulario)' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
                <td>
            </tr>
            </table>
        <%}//20100305%>
	<br>

        <%if (clvsrv.ispermitted(perfil, "11")){%>
        <table>
            <tr class="fila">
                <td>
                    &nbsp;&nbsp;Estado asignable :&nbsp;&nbsp;
                </td>
                <td>
                    <select name="estado_asignable" >
                    <%
                        for (int i=0;i<listaEstados.size();i++){
                            String[] estadinho=(String[])listaEstados.get(i);
                            %>
                            <option value="<%=estadinho[0]%>"><%=estadinho[1]%></option>
                            <%
                        }
                    %>
                    </select>
                    &nbsp;&nbsp;
                </td>
                <td align="center" colspan="2">
                    &nbsp;&nbsp;<img src="<%=BASEURL%>/images/botones/aceptar.gif" height="21" title='Asignar Estado' onclick='asignarEstado(formulario)' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
                <td>
            </tr>
        </table>

        <br>
        <%}%>

	<img src='<%=BASEURL%>/images/botones/salir.gif'      style='cursor:hand'    title='Salir...'      name='i_salir'       onclick='parent.close();'           onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
	<br><br><br>


	</center>
	<br>

    	<%
	if (respuesta!=null){//request.getParameter("resultado")!=null && request.getParameter("resultado").equals("ok") ){
		%>
		<br><br>
		<p><table border="2" align="center">
		  <tr>
			<td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
			  <tr>
				<td width="170" align="center" class="mensajes"><%=""+respuesta%></td>
				<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
				<td width="40">&nbsp;</td>
			  </tr>
			</table></td>
		  </tr>
		</table>
		</p>

	<% }%>
 </form>
 <br>
 <% }else{
 	%>lista de negocios vacío. raro...<%
 }	%>
</div>
<%=datos[1]%>
	<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
<%}catch(Exception eeee){
	System.out.println("error:::"+eeee.toString());
}
%>

</html>