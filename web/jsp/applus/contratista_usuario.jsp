<%--        Author     : iamorales--%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@page import="com.tsp.opav.model.*"%>
<%@page import="com.tsp.opav.model.beans.*"%>
<%@page import="com.tsp.opav.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path); 
  String respuesta=request.getParameter("contratist"); 
  Usuario usuario = (Usuario) session.getAttribute("Usuario");
  String loginx=usuario.getLogin(); 
  if (respuesta!=null){
  		usuario.setNitPropietario(respuesta);
		session.setAttribute( "Usuario", usuario);
  }
  ArrayList contratistas       = modelopav.NegociosApplusService.getContratistas();
%>
<html >
    <head  >
   
    <title>Contratista de Usuario</title>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
    <link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
    <link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    <script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
    <script src='<%= BASEURL %>/js/date-picker.js'></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>

    <body onresize="redimensionar()" onload = 'redimensionar();forma.cxc_app.focus();'  >

        <div id="capaSuperior" style="position:absolute; width:100%; height:110px; z-index:0; left: 0px; top: 0px;">
         <jsp:include page="/toptsp.jsp?encabezado=CONTRATISTA DE USUARIO"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 109px; overflow: scroll;">

        
    <form name="forma" action='jsp/applus/contratista_usuario.jsp' id="forma" method="post">
  <p>&nbsp;</p>
  <table width="500"  border="2" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td colspan="2" ><table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
              <tr>
                <td width="67%" class="subtitulo1">&nbsp;Datos de Contratista</td>
                <td width="33%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
              </tr>
          </table></td>
        </tr>
        <tr class="fila">
          <td align="center">Contratista de Usuario</td>
          <td nowrap>
			    <select name="contratist" >	
                                <%
                                String[] contratixx;
                                for (int i=0;i<contratistas.size();i++){
                                    contratixx=(String[]) contratistas.get(i);
                                    //if(contratixx[0].equals(contrati)){ nombre_contratista=contratixx[1];
                                %>
                                    <option value="<%=contratixx[0]%>" <%if (respuesta!=null && respuesta.equals(contratixx[0])){out.print(" selected");}%>><%=contratixx[1]%>__<%=contratixx[0]%></option>
				    						
				    
                                <%
                                }
                                    
                                %>
                                    <option value="802022016" <%if (respuesta!=null && respuesta.equals("802022016")){out.print(" selected");}%>>FINV__802022016</option>
				</select>		  		
		  </td>
        </tr>
        
      </table></td>
    </tr>
  </table>
  <br>
  <br>
  <center>
  <%if (respuesta==null){%>
    <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="c_aceptar" onClick="if (TCamposLlenos()){ forma.submit(); }" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
  <%}%>	
	
    <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
  </center>
  <%
        if (respuesta!=null ){
            %>
            <br><br>
            <p><table border="2" align="center">
              <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                  <tr>
                    <td width="150" align="center" class="mensajes"><%="Proceso ejecutado."%></td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="40">&nbsp;</td>
                  </tr>
                </table></td>
              </tr>
            </table>
            </p>
            <%
        }        
  %>        
    </form>					
</div>
<%=datos[1]%>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</body>
</html>
