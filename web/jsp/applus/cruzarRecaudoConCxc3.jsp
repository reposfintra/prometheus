<%--
    Document   : facturaCrearEca
    Created on : 5/03/2009, 11:06:55 PM
    Author     : Alvaro
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>


<html>
<head>
        <title>Seleccion de recaudos y facturas a cruzar  </title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
	<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>

	<script>

           function sendpms(theForm){
	   	      theForm.action='<%=CONTROLLER%>?estado=Recaudoeca&accion=Cruzar&evento=CRUZAR_RECAUDO_CXC_ECA3_PMS';
	              theForm.submit();
	   }

	</script>

</head>

<body onLoad="redimensionar();" onResize="redimensionar();">


<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    %>

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modulo de Cruces de Recaudos de Eca con Cxcs"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; ">


   <% String  msj           = request.getParameter("msj");
      String aceptarDisable = request.getParameter("aceptarDisable");
   %>

 <center>

   <form action="<%=CONTROLLER%>?estado=Recaudoeca&accion=Cruzar&evento=CRUZAR_RECAUDO_CXC_ECA3_PMS" method='post' name='formulario' >


   <table width="450" border="2" align="center">
       <tr>
          <td>
               <table width='100%' align='center' class='tablaInferior'>

                  <tr class="barratitulo">
                    <td colspan='2' >
                       <table cellpadding='0' cellspacing='0' width='100%'>
                         <tr>
                          <td align="left" width='65%' class="subtitulo1">&nbsp;CRUCES DE RECUADOS DE ECA Y CXCS DE CLIENTES</td>
                          <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"  height="20" align="left"><%=datos[0]%></td>
                        </tr>
                       </table>
                    </td>
                 </tr>


                 <tr  class="fila">
                       <td colspan='2' >
                          <table class='tablaInferior' width='100%'>
                               <tr  class="fila">
                                   <td align="center" > Presione ACEPTAR para generar los cruces de los Recaudos de Eca con las CxCs de Clientes con prefijo PM </td>
                               </tr>
                          </table>
                       </td>
                 </tr>

           </table>
         </td>
      </tr>
   </table>
   
   <br>
   <% if ( (aceptarDisable == null) || (aceptarDisable.equals(""))  ) { %>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="<%=BASEURL%>/images/botones/aceptar.gif" height="21"  title='cruces con pms'  name="aceptx" onclick='this.style.visibility="hidden";sendpms(formulario);'   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
   <% }
      else { %>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<img src="<%=BASEURL%>/images/botones/aceptarDisable.gif" title="cruces con pms"    height="21"  style="cursor:hand">&nbsp;con pm
   <% } %>



   <!-- MENSAJE -->
   <% if(msj!=null  &&  !msj.equals("") ){%>
   <script>  formulario.aceptx.style.visibility="hidden"; </script>
        <br><br>
        <table border="2" align="center">
              <tr>
                <td>
                    <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                          <tr>
                                <td width="450" align="center" class="mensajes"><%= msj %></td>
                                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="58">&nbsp; </td>
                          </tr>
                     </table>
                </td>
              </tr>
        </table>

    <%}%>



      </form >

</center>

</div>


<%=datos[1]%>


</body>
</html>