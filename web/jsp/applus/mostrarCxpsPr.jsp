<%--    Created on : 07/2009    Author     : iamorales--%>
<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>
<%
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);	
	Usuario usuario = (Usuario) session.getAttribute("Usuario");
	String loginx=usuario.getLogin();
	String cxc_pr=request.getParameter("cxc_pr");
	
	double total_abono=0;//090727
	double total_vlr=0;//090727
	double total_saldo=0;//090727	
	double total_iva=0,total_base=0,total_base_cxp=0;
	
%>
<html>
<head>
	<title>Gesti�n de Cxps</title>
    <link href="<%= BASEURL %>/css/estilostsp.css"     rel="stylesheet" type="text/css"> 
    <link href="/css/estilostsp.css" rel="stylesheet"  type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script>		
	
	function validarSeleccion(){
	  var abonillo=document.getElementById("abonoPrefactura").value;//090729
	  abonillo=parseFloat(abonillo);//090729
	  var valorcillo=document.getElementById("totalPrefactura").value;//090729
	  valorcillo=parseFloat(valorcillo);//090729
	  var descrilla=document.getElementById("descripcionPrefactura").value;
      var fechi=	  document.getElementById("fecfaccli").value;
	  if  (descrilla==""){alert("Por favor coloque 1 descripci�n.");return false;}
	  if  (fechi==""){alert("Por favor coloque 1 fecha.");return false;}	  
	  if (abonillo>0 && valorcillo>0 ){//090729
		   var validacion=false;
		   var elementos = document.getElementsByName("ckestado");
		   var i=0;		   
		   while (i<elementos.length && validacion==false){ 			  
		      if (elementos[i].checked){
			  		validacion= true;			
              }  
			  i=i+1;
		   }
		   if (validacion){		   
			    formulario.action="<%=CONTROLLER%>?estado=Cxps&accion=Applus&opcion=bajarcxpspr";
				//alert("casi");
				formulario.submit();
		   }else{
		   		alert ('Seleccione por lo menos una cxp para iniciar el proceso de aceptaci�n.');
		   }
	  }else{	   		   
	  	alert("Por favor revise los datos.");
	  }
	}
	
	function seleccionTotal()      {
          var estado = document.getElementById("ckTotal").checked;
          var item = 0;
          var valorTotal = 0;
          finItem = false;
          if (estado) {
             while (finItem == false){
                 item++;
                 if (document.getElementById("ckEstado"+item) != null){
                     document.getElementById("ckEstado"+item).checked = true;
                     var elemento  = document.getElementById("valorItem"+item);
                     elemento      = elemento.firstChild.nodeValue;
                     var valorItem = parseFloat(elemento.replace(/\,/g,""));
                     valorTotal = valorTotal + valorItem;
                 }
                 else
                     finItem = true;
             }
          }
          else {
             while (finItem == false){
                 item++;
                 if (document.getElementById("ckEstado"+item) != null){
                     document.getElementById("ckEstado"+item).checked = false;
                     document.getElementById("totalPrefactura").value = 0.00;
                     document.getElementById("abonoPrefactura").value = 0;					 
                 }
                 else
                     finItem = true;
             }
          }

          document.getElementById("totalPrefactura").value = Formatear(valorTotal,2,true);
		  document.getElementById("abonoPrefactura").value = valorTotal;

      }
	  		
		function Sell_all_col(theForm,nombre){
			 for (i=0;i<theForm.length;i++)
					  if (theForm.elements[i].type=='checkbox' && theForm.elements[i].name==nombre)
						 theForm.elements[i].checked=theForm.All.checked;
	    }	
		/*function sumar(theForm,nombre){
			 for (i=0;i<theForm.length;i++)
					  if (theForm.elements[i].type=='checkbox' && theForm.elements[i].name==nombre)
						 theForm.elements[i].checked=theForm.All.checked;
	    }*/
		
		function valorPrefactura(item)
      {
          var elemento  = document.getElementById("valorItem"+item);
          elemento      = elemento.firstChild.nodeValue;
          var valorItem = parseFloat(elemento.replace(/\,/g,""));
		  //alert("valorItem"+valorItem);
          elemento      = document.getElementById("totalPrefactura");
          elemento      = parseFloat(elemento.value.replace(/\,/g,""));
          var valorTotal= elemento;//parseFloat(elemento.replace(/\,/g,""));
		  //alert("valorTotal"+valorTotal);
          var estado = document.getElementById("ckEstado"+item).checked;
		  //alert("estado"+estado );
          if (estado)
             valorTotal = valorTotal + valorItem;
          else
             valorTotal = valorTotal - valorItem;
			 
   		  //alert("valorTotal"+valorTotal);
          document.getElementById("totalPrefactura").value = Formatear(valorTotal,2,true);
          document.getElementById("abonoPrefactura").value = valorTotal;		  
      }
	  
	  
	  function Formatear(numero,decimales,miles)
      {
        	var numero = new oNumero(numero);
	        return numero.formato(decimales, miles);
      }
	  
	 
      //Objeto oNumero
      function oNumero(numero)
      {
		  //Propiedades
		  this.valor = numero || 0
		  this.dec = -1;
		  //M�todos
		  this.formato = numFormat;
		  this.ponValor = ponValor;
		  //Definici�n de los m�todos
		  function ponValor(cad)
		  {
		  if (cad =='-' || cad=='+') return
		  if (cad.length ==0) return
		  if (cad.indexOf('.') >=0)
			  this.valor = parseFloat(cad);
		  else
			  this.valor = parseInt(cad);
		  }
		  function numFormat(dec, miles)
		  {
		  var num = this.valor, signo=3, expr;
		  var cad = ""+this.valor;
		  var ceros = "", pos, pdec, i;
		  for (i=0; i < dec; i++)
		  ceros += '0';
		  pos = cad.indexOf('.')
		  if (pos < 0)
			  cad = cad+"."+ceros;
		  else
			  {
			  pdec = cad.length - pos -1;
			  if (pdec <= dec)
				  {
				  for (i=0; i< (dec-pdec); i++)
					  cad += '0';
				  }
			  else
				  {
				  num = num*Math.pow(10, dec);
				  num = Math.round(num);
				  num = num/Math.pow(10, dec);
				  cad = new String(num);
				  }
			  }
		  pos = cad.indexOf('.')
		  if (pos < 0) pos = cad.lentgh
		  if (cad.substr(0,1)=='-' || cad.substr(0,1) == '+')
				 signo = 4;
		  if (miles && pos > signo)
			  do{
				  expr = /([+-]?\d)(\d{3}[\.\,]\d*)/
				  cad.match(expr)
				  cad=cad.replace(expr, RegExp.$1+','+RegExp.$2)
				  }
		  while (cad.indexOf(',') > signo)
			  if (dec<0) cad = cad.replace(/\./,'')
				  return cad;
		  }
		  }
									
    </script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Cxps"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: -1px; top: 100px; overflow: scroll;">      
<%
try{
  
   ArrayList cxps=model.cxpsApplusService.getCxpDeCxcPr();       
 
   if (cxps!=null  ){
%>   
    <form  method="post" name="formulario">
    <%
    String respuesta=request.getParameter("respuesta");	
    %>
    <br><br>
	<table align="center">		
		<tr class="fila">
		 <td>			 	   
			  &nbsp;&nbsp;Doc Provintegral&nbsp;:&nbsp;			
		 </td>
		 <td>
			 <input type="text" name="cxc_pr"  value="<%=cxc_pr%>"  readonly >
		 </td>
	    </tr>	
		<tr class="fila">
		 <td>			 	   
			  &nbsp;&nbsp;Total&nbsp;:&nbsp;			
		 </td>
		 <td>
			 <input type="text" name="totalPrefactura"  value="0"  id="totalPrefactura" readonly >
		 </td>
	    </tr>		
	    <tr class="fila">
		 <td>			 	   
			  &nbsp;&nbsp;Abono&nbsp;:&nbsp;			
		 </td>
		 <td>
			 <input type="text" name="abonoPrefactura"  value="0"  id="abonoPrefactura"  >
		 </td>
	    </tr>		
		<tr class="fila">
		 <td>			 	   
			  &nbsp;&nbsp;Descripci�n&nbsp;:&nbsp;			
		 </td>
		 <td>
			 <input type="text" name="descripcionPrefactura"  value=""  id="descripcionPrefactura"  size="50" >
		 </td>
	    </tr>
		<tr class="fila">
		 <td>			 	   
			  &nbsp;&nbsp;Fecha Doc&nbsp;:&nbsp;			
		 </td>
		 <td>
			<input name="fecfaccli" type="text" class="textbox" id="fecfaccli" size="10" >
			<span class="Letras"><img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fecfaccli);return false;" HIDEFOCUS></span>	
		 </td>
	    </tr>	
	</table>
	<br>
    <table width="689"  border="2" align="center">
	<tr>
    	<td >                				 	
			<table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
				<tr class="fila">
					<TH   nowrap style="font size:11; font weight: bold" > <input name="checkbox" type='checkbox'  id='ckTotal' onClick="seleccionTotal();"></TH><!--Sell_all_col(this.form,'idNegocio');--> 								
					<th > &nbsp;ID ORDEN&nbsp;  </th>
					<th > &nbsp;NUM OS&nbsp;  </th>
					<th > &nbsp;ESQUEMA&nbsp;  </th>
    				<th > &nbsp;BASE&nbsp;  </th>
					<th > &nbsp;CXP PR&nbsp;</th>
					<th > &nbsp;VLR&nbsp;</th>
					<th > &nbsp;ABONO&nbsp;</th>
					<th > &nbsp;SALDO&nbsp;</th>
					<th > &nbsp;FEC OPEN&nbsp;</th>
					<th > &nbsp;PROV&nbsp;</th>															
					<th > &nbsp;IVA&nbsp;</th>					
					<th > &nbsp;DOC PR&nbsp;</th>					
			    </tr>								<%                                                                         
				CxpPr cxpPr;
				int i=1;
				for (i=1; i<=cxps.size(); i++) {
					cxpPr=(CxpPr)cxps.get(i-1);					
				%>
			    <tr class='<%= (i%2==0?"filagris":"filaazul") %>' id='fila<%=i%>'   style=" font size:12"   >
					<td class="bordereporte" align='center' nowrap style="font size:11"><%						
					%>
					<input  name="ckestado" type="checkbox" id="ckEstado<%= i %>" value='<%= cxpPr.getIdOrden() %>' onclick=" cambiarColorMouse(fila<%=i%>); valorPrefactura(<%= i %>)">
						
					<!--<input type='checkbox' id="id<%//=i%>" name='idNegocio' value='' onclick=" cambiarColorMouse(fila<%//=i%>);valorPrefactura(<%//= i %>) ">-->
					</td>

				    <td class="bordereporte" nowrap align="center"> <%=cxpPr.getIdOrden()%>&nbsp;</td>
					<td class="bordereporte" nowrap align="center">&nbsp;<%=cxpPr.getNumOs()%>&nbsp;</td>
					<td class="bordereporte" nowrap align="center">&nbsp;<%=cxpPr.getEsquemaComision()%>&nbsp;</td>					
					<td class="bordereporte" nowrap align="center">&nbsp;<%=Util.FormatoMiles(Double.parseDouble(cxpPr.getSumTotPrev1()))%>&nbsp;</td>					
					<%total_base=total_base+Double.parseDouble(cxpPr.getSumTotPrev1());//090731%>
					<td class="bordereporte" nowrap align="center" title="<%=cxpPr.getDocRelacionado()%>"><%=(cxpPr.getFacturaPr())%></td>
					<td class="bordereporte" nowrap align="center"><%=Util.FormatoMiles(Double.parseDouble(cxpPr.getVlrNeto()))%></td>
					<%total_vlr=total_vlr+Double.parseDouble(cxpPr.getVlrNeto());%>
					<td class="bordereporte" nowrap align="center"><%=Util.FormatoMiles(Double.parseDouble(cxpPr.getAbono()))%></td>
					<%total_abono=total_abono+Double.parseDouble(cxpPr.getAbono());%>
					<td class="bordereporte" nowrap align="center" id="valorItem<%= i %>" > <%= Util.FormatoMiles(Double.parseDouble(cxpPr.getSaldo())) %>      </td>
					<%total_saldo=total_saldo+Double.parseDouble(cxpPr.getSaldo());%>
					<!--<td class="bordereporte" nowrap align="center"><%//=(cxpApplus.getSaldo())%></td>-->
					<td class="bordereporte" nowrap align="center"><%=(cxpPr.getFecOpen())%></td>
					
					<td class="bordereporte" nowrap align="center"><%=Util.FormatoMiles(Double.parseDouble(cxpPr.getBaseCxpPr()))%></td>
					<%total_base_cxp=total_base_cxp+Double.parseDouble(cxpPr.getBaseCxpPr());%>
					<td class="bordereporte" nowrap align="center"><%=Util.FormatoMiles(Double.parseDouble(cxpPr.getIvaCxpPr()))%></td>
					<%total_iva=total_iva+Double.parseDouble(cxpPr.getIvaCxpPr());%>
					<td class="bordereporte" nowrap align="center"><%=cxc_pr%></td>

				</tr>	                                                  
			<% } %>
						
			<tr class='<%= (i%2==0?"filagris":"filaazul") %>' id='fila<%=i%>'   style=" font size:12"   >
					<td class="bordereporte" align='center' nowrap style="font size:11">&nbsp;</td>

				    <td class="bordereporte" nowrap align="center">&nbsp; </td>
					<td class="bordereporte" nowrap align="center">&nbsp;</td>
					<td class="bordereporte" nowrap align="center">&nbsp;</td>					
					<td class="bordereporte" nowrap align="center"><%=Util.FormatoMiles(total_base)%></td>	<!--090731-->	
					<td class="bordereporte" nowrap align="center">&nbsp;</td>
					<td class="bordereporte" nowrap align="center"><%=Util.FormatoMiles(total_vlr)%></td>
					
					<td class="bordereporte" nowrap align="center"><%=Util.FormatoMiles(total_abono)%></td>
					
					<td class="bordereporte" nowrap align="center" id="valorItem<%= i %>" > <%= Util.FormatoMiles(total_saldo) %>      </td>
					
					<!--<td class="bordereporte" nowrap align="center"><%//=(cxpApplus.getSaldo())%></td>-->
					<td class="bordereporte" nowrap align="center">&nbsp;</td>
					
					<td class="bordereporte" nowrap align="center" id="valorItem<%= i %>" > <%= Util.FormatoMiles(total_base_cxp) %>      </td>
					<td class="bordereporte" nowrap align="center" id="valorItem<%= i %>" > <%= Util.FormatoMiles(total_iva) %>      </td>

					<td class="bordereporte" nowrap align="center">&nbsp;</td>

				</tr>	                                                  			
						                                                  
			</table>		
		</td>
	</tr>
    </table>
	<br>
	<center>	
	<br>	<br><br>		
	<img src="<%=BASEURL%>/images/botones/aceptar.gif"       height="21"  title='Aceptar'    onclick='validarSeleccion()'                                                                                         onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">        
	
	<img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" style="cursor:hand " onClick="window.location.href = '<%= CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/applus&pagina=cxp_provintegral_pr.jsp&marco=no';" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	
	<img src='<%=BASEURL%>/images/botones/salir.gif'      style='cursor:hand'    title='Salir...'      name='i_salir'       onclick='parent.close();'           onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
	<br><br><br>	
	</center>
	<br>
    <%  
	if (respuesta!=null){
		%>
		<br><br>
		<p><table border="2" align="center">
		  <tr>
			<td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
			  <tr>
				<td width="170" align="center" class="mensajes"><%=""+respuesta%></td>
				<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
				<td width="40">&nbsp;</td>
			  </tr>
			</table></td>
		  </tr>
		</table>
		</p>		
	<% }%>     
 </form>
 <br> 
 <% }else{
 	%>&nbsp;&nbsp;lista de cxps vac�a. raro...<%    
 }	%>
</div>
<%=datos[1]%>
<%}catch(Exception ee){
	System.out.println("error en jsp...:"+ee.toString()+"__"+ee.getMessage());
}
%>
	<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</body>
</html>