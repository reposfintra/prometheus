<%--
    Document   : prefactura
    Created on : 26/01/2009, 08:56:34 AM
    Author     : Alvaro
--%>

<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<%@page import="com.tsp.operation.model.beans.*"%>

<%
            String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
            String datos[] = model.menuService.getContenidoMenu(BASEURL, request.getRequestURI(), path);
            List listaPrefactura = model.applusService.getPrefactura();
            Contratista contratista = model.applusService.getContratista();
            int secuencia_prefactura = contratista.getSecuencia_prefactura() + 1;
            String proxima_prefactura = contratista.getId_contratista() + '-' + Util.llenarConCerosALaIzquierda( secuencia_prefactura, 6 );

            double porcentajeIva = model.applusService.getPorcentaje("IVA");




%>
<html>
  <head>

    <title>Consultar items para prefacturar</title>

    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
    <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>




    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">

  </head>
  <body  >
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
      <jsp:include page="/toptsp.jsp?encabezado=Prefacturar Ordenes"/>
    </div>


    <div id="capaSiguiente" style="position:absolute; width:1600; height:87%; z-index:0; left: 0px; top: 100px; ">
        <table width="580"  border="1"  align="center">
          <tr>
            <td class= "leyenda"   width="130">ID CONTRATISTA</td>
            <td class= "filagris1" width="250"><%= contratista.getId_contratista() %> </td>
            <td class= "leyenda"   width="130">NUMERO PREFACTURA</td>
            <td class= "filagris1" width="70"><%= proxima_prefactura %></td
          ></tr>
          <tr>
            <td class= "leyenda"   width="130">NOMBRE CONTRATISTA</td>
            <td class= "filagris1" width="250"><%= contratista.getNombre_contratista() %></td>
            <td class= "leyenda"   width="130">VALOR PREFACTURA</td>
            <td class= "filagris1" width="70" name="totalPrefactura" id="totalPrefactura">0.00</td
          ></tr>
        </table>



      <form name='formulario' method='post' id="formulario" action="<%=CONTROLLER%>?estado=Prefactura&accion=Registrar&evento=MARCAR_PREFACTURADO" >


        <table width="1540"  border="1"  align="center">

              <TR class="tblTitulo1" align="center">
                      <TH  width="10" align="center">
                         <input name="ckTotal" type="checkbox" id="ckTotal" onclick="seleccionTotal() ">
                      </TH>
                      <TH  width="30" align="center"> ORDEN         </TH>
                      <TH  width="35"> ACCION        </TH>
                      <TH  width="80"> NUM OS</TH>
					  <TH  width="60"> FACTURA CONF.</TH>
                      <TH  width="60" align="center"> MATERIAL  </TH>
                      <TH  width="60"> MANO OBRA </TH>
                      <TH  width="40"> OTROS     </TH>
                      <TH  width="60"> ADMINISTRACION </TH>
                      <TH  width="60"> IMPREVISTO </TH>
                      <TH  width="60"> UTILIDAD </TH>

                      <TH  width="60"> SUBTOTAL </TH>

                      <TH  width="60"> BONIFICACION </TH>
                      <TH  width="80"> SUBTOTAL </TH>
                      <TH  width="60"> BASE IVA </TH>

                      <TH  width="80"> IVA </TH>
                      <TH  width="80"> TOTAL </TH>

                      <TH  width="200"> NOMBRE DEL CLIENTE  </TH>
                      <TH  width="170"> DIRECCION           </TH>
                      <TH  width="50"> CIUDAD              </TH>
                      <TH  width="205"> ACCIONES            </TH>
                      <TH  width="60"> CONSECUTIVO   </TH>

              </TR>




          <%
            int i=0;
            Iterator it = listaPrefactura.iterator();
            while (it.hasNext()) {

              i++;
              Prefactura prefactura = (Prefactura)it.next();
              String id_orden = prefactura.getId_orden();
              String id_accion = prefactura.getId_accion();
              String consecutivo = prefactura.getConsecutivo();
              String id_cliente = prefactura.getId_cliente();
              String nombre_cliente = prefactura.getNombre_cliente();
              String direccion = prefactura.getDireccion();
              String ciudad = prefactura.getCiudad();
              String acciones = prefactura.getAcciones();
			  String num_os= prefactura.getNumOs();

			  String fact_conf=prefactura.getFacturaConformada();

              double total_prev1_mat = prefactura.getTotal_prev1_mat();
              double total_prev1_mob = prefactura.getTotal_prev1_mob();
              double total_prev1_otr = prefactura.getTotal_prev1_otr();
              double administracion  = prefactura.getAdministracion();
              double imprevisto      = prefactura.getImprevisto();
              double utilidad        = prefactura.getUtilidad();
              double base_iva        = prefactura.getBase_iva();


              double total_prev1 = total_prev1_mat + total_prev1_mob + total_prev1_otr + administracion + imprevisto + utilidad;
              double comision_total = -prefactura.getComision_total();
              double subtotal = total_prev1 + comision_total;
              double iva = Util.redondear2(base_iva*porcentajeIva/100, 2);
              double valorItem = subtotal + iva;




          %>
              <tr class='<%= (i%2==0?"filagris1":"filaazul1") %>' >
                <td width="10" align="center" >&nbsp;
                <input vspace="10" name="ckestado" type="checkbox" id="ckEstado<%= i %>" value='<%= id_accion    %>' onclick="valorPrefactura(<%= i %>)"> </td>

                <td width="30"  align="center"> <%= id_orden %>   </td>
                <td width="35"  align="center"> <%= id_accion %>  </td>
                <td width="80"  align="center"><%=num_os%> </td>
				<td width="60"  align="center"><%=fact_conf%> </td>
                <td width="60"  align="right"  id="valorPrev<%= i %>" > <%= Util.customFormat(total_prev1_mat) %> </td>
                <td width="60"  align="right"  id="valorManoObra<%= i %>" > <%= Util.customFormat(total_prev1_mob ) %></td>
                <td width="40"  align="right"  id="valorOtro<%= i %>" > <%= Util.customFormat(total_prev1_otr ) %></td>
                <td width="60"  align="right"  id="administracion<%= i %>" > <%= Util.customFormat(administracion ) %></td>
                <td width="60"  align="right"  id="imprevisto<%= i %>" > <%= Util.customFormat(imprevisto ) %></td>
                <td width="60"  align="right"  id="utilidad<%= i %>" > <%= Util.customFormat(utilidad ) %></td>
                <td width="60"  align="right"  id="valorPrev<%= i %>" > <%= Util.customFormat(total_prev1 ) %>    </td>
                <td width="60"  align="right"  id="valorBonificacion<%= i %>" style="color:red"> <%= Util.customFormat(comision_total) %>  </td>
                <td width="80"  align="right"  id="valorSubtotal<%= i %>" > <%= Util.customFormat(subtotal) %>        </td>
                <td width="60"  align="right"  id="base_iva<%= i %>" > <%= Util.customFormat(base_iva ) %></td>
                <td width="80"  align="right"  id="valorIva         <%= i %>" > <%= Util.FormatoMiles(iva)%>        </td>
                <td width="80"  align="right"  id="valorItem<%= i %>" > <%= Util.FormatoMiles(valorItem) %>      </td>

                <td width="200" align="left"> <%= nombre_cliente %></td>
                <td width="170" align="left"> <%= direccion %>     </td>
                <td width="50"  align="center"> <%= ciudad %>        </td>
                <td width="205" align="left">   <%= acciones %>      </td>
                <td width="60"  align="center"> <%= consecutivo %></td>
              </tr>
           <%
                }
           %>

        </table>

        <p>&nbsp;</p>

        <table align="center">
          <tr>
            <td colspan="2" nowrap align="center">
            <img src="<%=BASEURL%>/images/botones/aceptar.gif"    height="21"  title='Consultar'  onClick="ValidarSeleccion()"      onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
            <img src="<%=BASEURL%>/images/botones/salir.gif"      height="21"  title='Salir'      onClick="window.close();"     onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
          </tr>
        </table>
      </form >
    </div>



    <script >


      function ValidarSeleccion()
      {
          var item = 0;
          finItem = false;
          marcado = false;

          while (finItem == false){
             item++;
             if (document.getElementById("ckEstado"+item) != null){
                 if (document.getElementById("ckEstado"+item).checked == true){
                     marcado = true;
                     finItem = true;

                 }
             }
             else
                 finItem = true;
          }

          if (marcado) {
              formulario.submit();
          }
          else
              alert("Para prefacturar se requiere marcar alguno de los items");

      }


      function valorPrefactura(item)
      {
          var elemento  = document.getElementById("valorItem"+item);
          elemento      = elemento.firstChild.nodeValue;
          var valorItem = parseFloat(elemento.replace(/\,/g,""));

          elemento      = document.getElementById("totalPrefactura");
          elemento      = elemento.firstChild.nodeValue;
          var valorTotal= parseFloat(elemento.replace(/\,/g,""));

          var estado = document.getElementById("ckEstado"+item).checked;
          if (estado)
             valorTotal = valorTotal + valorItem;
          else
             valorTotal = valorTotal - valorItem;

          document.getElementById("totalPrefactura").innerHTML = Formatear(valorTotal,2,true);;
      }


      function seleccionTotal()
      {
          var estado = document.getElementById("ckTotal").checked;

          var item = 0;
          var valorTotal = 0;
          finItem = false;
          if (estado) {
             while (finItem == false){
                 item++;
                 if (document.getElementById("ckEstado"+item) != null){
                     document.getElementById("ckEstado"+item).checked = true;
                     var elemento  = document.getElementById("valorItem"+item);
                     elemento      = elemento.firstChild.nodeValue;
                     var valorItem = parseFloat(elemento.replace(/\,/g,""));
                     valorTotal = valorTotal + valorItem;
                 }
                 else
                     finItem = true;
             }
          }
          else {
             while (finItem == false){
                 item++;
                 if (document.getElementById("ckEstado"+item) != null){
                     document.getElementById("ckEstado"+item).checked = false;
                     document.getElementById("totalPrefactura").innerHTML = 0.00;
                 }
                 else
                     finItem = true;
             }
          }

          document.getElementById("totalPrefactura").innerHTML = Formatear(valorTotal,2,true);

      }


      function Formatear(numero,decimales,miles)
      {
        var numero = new oNumero(numero);
        return numero.formato(decimales, miles);
      }

      //Objeto oNumero
      function oNumero(numero)
      {
      //Propiedades
      this.valor = numero || 0
      this.dec = -1;
      //M�todos
      this.formato = numFormat;
      this.ponValor = ponValor;
      //Definici�n de los m�todos
      function ponValor(cad)
      {
      if (cad =='-' || cad=='+') return
      if (cad.length ==0) return
      if (cad.indexOf('.') >=0)
          this.valor = parseFloat(cad);
      else
          this.valor = parseInt(cad);
      }
      function numFormat(dec, miles)
      {
      var num = this.valor, signo=3, expr;
      var cad = ""+this.valor;
      var ceros = "", pos, pdec, i;
      for (i=0; i < dec; i++)
      ceros += '0';
      pos = cad.indexOf('.')
      if (pos < 0)
          cad = cad+"."+ceros;
      else
          {
          pdec = cad.length - pos -1;
          if (pdec <= dec)
              {
              for (i=0; i< (dec-pdec); i++)
                  cad += '0';
              }
          else
              {
              num = num*Math.pow(10, dec);
              num = Math.round(num);
              num = num/Math.pow(10, dec);
              cad = new String(num);
              }
          }
      pos = cad.indexOf('.')
      if (pos < 0) pos = cad.lentgh
      if (cad.substr(0,1)=='-' || cad.substr(0,1) == '+')
             signo = 4;
      if (miles && pos > signo)
          do{
              expr = /([+-]?\d)(\d{3}[\.\,]\d*)/
              cad.match(expr)
              cad=cad.replace(expr, RegExp.$1+','+RegExp.$2)
              }
      while (cad.indexOf(',') > signo)
          if (dec<0) cad = cad.replace(/\./,'')
              return cad;
      }
      }//Fin del objeto oNumero:


    </script>




  </body>
</html>