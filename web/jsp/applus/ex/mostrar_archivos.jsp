<!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      25/01/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   
     - @(#)  
     - Description: Importacion Directa de un archivo
--%> 

<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import   ="com.tsp.operation.model.beans.Usuario;"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%  
    String Mensaje = (request.getParameter("Mensaje")!=null?request.getParameter("Mensaje"):""); 
    Usuario user = (Usuario) session.getAttribute("Usuario");
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
	String numos=request.getParameter("num_osx");
	if (numos==null){numos="";}
	String archivolisto=request.getParameter("archivolisto");
	String secuenciax=request.getParameter("secuenciax");
	
	String cerrar=request.getParameter("cerrar");
	if (cerrar!=null && (cerrar.equals("si"))){
	%>
		<script>
		//alert("se va a cerra");
			window.close();
		</script>
	<%	
	}
%>    
%>
<html>
<head>
    <title>Consultar Archivos</title>
    <link   href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet">
    <script src ="<%= BASEURL %>/js/boton.js"></script>	
    <script>

      function validarExtension ( archivo ){
         var ext = /.EXE$/gi;
         if (ext.test(archivo.toUpperCase()) ){
             alert ('Extension no valida, para el archivo de importacion');
             return false;
         } return true;
      }  
	  
	  function consultarCodigo(secuenci){
		  if (secuenci!=null){
			//alert("cambio de consulta");
			//formulario.action="<%=CONTROLLER%>?estado=Negocios&accion=Applus&opcion=consultarCodigo";
			fimp.secuenciax.value=secuenci;
			fimp.submit();			
		  }
		}	
		function consultarNombres(){
			location.href = '<%=CONTROLLER%>?estado=Negocios&accion=Applus&opcion=nombres&numerit_os='+fimp.numerit_osx.value;
			
		}	 
		function borrar()	{
			window.close();
			window.open('<%= CONTROLLER %>?estado=Negocios&accion=Applus&opcion=borrar','na','status=yes,scrollbars=yes,width=400,height=150,resizable=no');			
		} 
    </script>
    
</head>
<body onresize="redimensionar()" onLoad="window.resizeTo(700, 600);" >
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Importacion de Archivos "/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">


        <center>
	<br>
	<form action="<%= CONTROLLER %>?estado=Negocios&accion=Applus&opcion=consultarCodigo&num_osx=<%=numos%>" method="post" name="fimp" enctype='multipart/form-data' >
	<input type="hidden"  name="secuenciax" value="0">
	<table width="650" border="2" align="center">
	<tr>
		<td align="center">
		
		<!-- cabecera de la seccion -->
		<table width="100%" >
			<tr>
			<td class="subtitulo1"  width="50%">Consultar Archivos</td>
			<td class="barratitulo" width="50%">
			  <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%>
			</td>
			</tr>
		</table>
		<!-- fin cabecera -->

		<table  width="100%" >
			<tr class="fila">
			    <td width='40%'>&nbsp;Multiservicio&nbsp;</td>
                            <td width='60%'>
                                <input type="text" value="<%=numos%>" name="numerit_osx"  maxlength="50"   >
								<img   width='14'    title='Buscar' style="cursor:hand"   src='<%=BASEURL%>/images/botones/iconos/modificar.gif'								
								onclick="consultarNombres();">
			    </td>
			</tr>		
		
			<tr class="fila">
			    <td >&nbsp;Seleccione su archivo</td>
				<td >&nbsp;</td>
			</tr>
			<%			
     		//String[] archivosx=model.negociosApplusService.getArchivo(numos,user.getLogin());		
			
			java.util.List nombresArchivos =model.negociosApplusService.getNombresArchivos(numos,user.getLogin());		
						
		    //System.out.println("archivosx.length"+archivosx.length);
			for (int w=0;w<nombresArchivos.size();w++){
			%>
			<tr class="fila">
			    <td >&nbsp;</td>
				<%				
				String[] nombre_archiv=(String[])nombresArchivos.get(w);
				//System.out.print("parterutaarchivo"+archivosx[w]+"BASEURL"+BASEURL);
				//namearchi=archivosx[w].split("__");
				
				%>

			    <td ><a target="_blank"  onClick="consultarCodigo(<%=nombre_archiv[0]%>);" style="cursor:hand" ><strong><%=nombre_archiv[0]%>__<%=nombre_archiv[1]%></strong>				
				</a>
				<%
				if (archivolisto!=null && nombre_archiv[0].equals(secuenciax)){
				%>
		
				
					&nbsp;&nbsp;&nbsp;<a href="<%=BASEURL%>/images/multiservicios/<%=user.getLogin()%>/<%=archivolisto%>">ver</a>
				</script>
		
  		        <%	}%>
				</td>
				
			</tr>
			<%}%>
			
		 </table>
        </td>         
      </tr>
      </table>
      <br>
      
      <!--<img name="imgProcesar" src="<%//=BASEURL%>/images/botones/aceptar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" if (fimp.archivo.value!='') { if ( validarExtension(fimp.archivo.value) ) {fimp.submit();} } else { alert ('Indique el archivo, para poder continuar'); } ">-->
      &nbsp;
      <img name="imgSalir" src="<%=BASEURL%>/images/botones/salir.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick="borrar(); ">
      </form>
        


<% if (!Mensaje.equals("")) { %>
    
        <table border="2" align="center">
           <tr>
           <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                <tr>
                 <td width="320" align="center" class="mensajes"><%=Mensaje%></td>
                 <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                 <td width="58">&nbsp;</td>
                </tr>
              </table>
           </td>
           </tr>
        </table>  
        
        <br><br>
        
        
        
        
        
<% } %>      
      
</center>
</div>
<%=datos[1]%>
</body>
</html>
