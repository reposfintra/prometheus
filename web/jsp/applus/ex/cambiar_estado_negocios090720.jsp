<%--    
    Created on : 02/02/2009
    Author     : iamorales
--%>
<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>
<%
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
	
	Usuario usuario = (Usuario) session.getAttribute("Usuario");
	String loginx=usuario.getLogin();
%>
<html>
<head>
	<title>Gesti�n de Negocios</title>
    <link href="<%= BASEURL %>/css/estilostsp.css"     rel="stylesheet" type="text/css"> 
    <link href="/css/estilostsp.css" rel="stylesheet"  type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script>
		function validar(theForm){
		   if (formulario.estado_asignar.value=="nada"){alert("Debe escoger un estado a asignar.");return;}
		   var elementos = document.getElementsByName("idNegocio");		   
		   var sw = false;           
			for (var i=0;i<elementos.length; i++){
				if (elementos[i].checked){
					sw = true;			
				}  
			}		
			if(sw==true){
			//alert ("cas");
				formulario.action="<%=CONTROLLER%>?estado=Negocios&accion=Applus&opcion=cambiarEstado";
				theForm.submit();
			}else{
				alert("Verifique que haya al menos un negocio seleccionado.");
			}
		}
		
		function Sell_all_col(theForm,nombre){
			 for (i=0;i<theForm.length;i++)
					  if (theForm.elements[i].type=='checkbox' && theForm.elements[i].name==nombre)
						 theForm.elements[i].checked=theForm.All.checked;
	    }
		
		function seleccionarOrden(theForm,orden,che){	
			//if (!	formulario.esfactconformada.checked){
				for (i=0;i<theForm.length;i++){
						  if (theForm.elements[i].type=='checkbox' && theForm.elements[i].value==orden){
							 theForm.elements[i].checked=che;							 
						  }
				}
			//}
		}
		
		function consultarEstado(){
			//alert("cambio de consulta");
			formulario.action="<%=CONTROLLER%>?estado=Negocios&accion=Applus&opcion=consultarEstado";
			formulario.submit();
			
		}						
    </script>
</head>
<body onLoad="redimensionar();formulario.contratista_consultar.focus();" onResize="redimensionar();"><!--redimensionar-->

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Negocios"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: -1px; top: 100px; overflow: scroll;">   
    
<%
   
   String estadi=request.getParameter("estadito");
   if (estadi==null){estadi="2";}
   
   String contrati=request.getParameter("contratistica");
   if (contrati==null){contrati="";}
   
   String fact_conformada_consultar=request.getParameter("fact_conformada_consultar");
   if (fact_conformada_consultar==null){fact_conformada_consultar="";}
   
   String num_osxi=request.getParameter("num_osx");
   if (num_osxi==null){num_osxi="";}
      
   ArrayList listaEstados=model.negociosApplusService.getEstadosApplus();  
   
   ArrayList contratistas=model.negociosApplusService.getContratistas();  
   
   String nombre_estado="nada";
   String nombre_contratista="";
      
   String[] estadixx;
   
   String[] contratixx;
   
   for (int i=0;i<listaEstados.size();i++){
		estadixx=(String[]) listaEstados.get(i);
		if(estadixx[0].equals(estadi)){ nombre_estado=estadixx[1];} 
   }
   for (int i=0;i<contratistas.size();i++){
		contratixx=(String[]) contratistas.get(i);
		if(contratixx[0].equals(contrati)){ nombre_contratista=contratixx[1];} 
   }
   /*if (estadi.equals("2")){ nombre_estado="En estudio";}
   if (estadi.equals("3")){ nombre_estado="Inconsistencia";}
   if (estadi.equals("5")){ nombre_estado="Aprobado de Contado";}
   if (estadi.equals("4")){ nombre_estado="Aprobado Financiado";}*/
           
   ArrayList negocios ;
   try {    
	   negocios = model.negociosApplusService.getNegociosApplus(estadi,contrati,num_osxi,fact_conformada_consultar);
   }catch(Exception e ){
   		System.out.println("errorcillo en model.NegociosApplusService.getNegociosApplus()");
		negocios=null;
   }
   if (negocios!=null  ){
%>   
    <form  method="post" name="formulario">
    <input type="hidden" name="estadito" value="<%=estadi%>">
    <%
    String respuesta=request.getParameter("respuesta");
	//String valor_neto="";//request.getParameter("valor_neto");	
    %>
    <br><br>
	<table align="center">
		<tr class="fila">
			<td>
				&nbsp;&nbsp;Estado actual:&nbsp;&nbsp; 
				<select name="estado_consultar" onChange="consultarEstado();">
									
					<option value="<%=estadi%>"><%=nombre_estado%></option>					
					<%
					for (int i=0;i<listaEstados.size();i++){
						String[] estadinho=(String[])listaEstados.get(i);
						%>
						<option value="<%=estadinho[0]%>"><%=estadinho[1]%></option>
						<%
					}
					%>
					
				</select>				
				&nbsp;&nbsp;
			</td>
		</tr>
		<tr class="fila">
			<td>
				&nbsp;&nbsp;Contratista:&nbsp;&nbsp; 
				<select name="contratista_consultar">
					<option value="<%=contrati%>"><%=nombre_contratista%></option>
					<option value=""> </option>
					<%
					for (int i=0;i<contratistas.size();i++){
						String[] contratistanho=(String[])contratistas.get(i);
						%>
						<option value="<%=contratistanho[0]%>"><%=contratistanho[1]%></option>
						<%
					}
					%>
				</select>				
				&nbsp;&nbsp;
			</td>
		</tr>
		
		<tr class="fila">
		<td>		
	 	    <!--&nbsp;<input type="checkbox" name="esfactconformada">-->
			&nbsp;&nbsp;Multiservicio&nbsp;:&nbsp;	
		
		
			<input type="text" name="num_osx"  value="<%=num_osxi%>" >
		</td>
	</tr>
		<tr class="fila">
		<td>		
	 	    <!--&nbsp;<input type="checkbox" name="esfactconformada">-->
			&nbsp;&nbsp;Factura Conformada&nbsp;:&nbsp;			
		
			<input type="text" name="fact_conformada_consultar"  value="<%=fact_conformada_consultar%>" >
		</td>
	</tr>
	</table>
	<br>
    <table width="689"  border="2" align="center">
	<tr>
    	<td >                				 	
			<table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
				<tr class="fila">
					<TH   nowrap style="font size:11; font weight: bold" > <input type='checkbox'  id='All' onclick="Sell_all_col(this.form,'idNegocio');">  </TH> 			
					
					<th > &nbsp;ID ORDEN&nbsp;  </th>
					<th > &nbsp;NUM OS&nbsp;  </th>
					<th > &nbsp;ID ACCION&nbsp;</th>
					
					<th > &nbsp;ID CLIENTE&nbsp;</th>
					<th > &nbsp;NIC&nbsp;</th>
					<th > &nbsp;NIT&nbsp;</th>
					<th > &nbsp;NOMBRE CLIENTE&nbsp;</th>					
					<th > &nbsp;TELEFONO&nbsp;  </th>
					<th > &nbsp;CONTACTO&nbsp;  </th>
					<th >&nbsp;ID CONTRATISTA&nbsp;</th>
					<th >&nbsp;NOMBRE CONTRATISTA&nbsp;</th>
					<th > &nbsp;CUOTAS&nbsp;  </th>
					<!--<th > &nbsp;VALOR CUOTA&nbsp;  </th>-->
					<th > &nbsp;ESTUDIO ECONOMICO&nbsp;  </th>					
					<th > &nbsp;FACTURA CONFORMADA&nbsp;  </th>
					<th > &nbsp;FECHA OPEN&nbsp;  </th>									
					<th > &nbsp;VALOR_PREV1&nbsp;</th>	
																						
					<th >&nbsp;OFERTA AP+&nbsp;</th>					
					<th >&nbsp;VALOR VENTA AP+&nbsp;</th>					
					<th >&nbsp;ECA_OFERTA AP+&nbsp;</th>
					<th >&nbsp;VALOR ECA VENTA AP+&nbsp;</th>				

					<th >&nbsp;OFERTA &nbsp;</th>					
					<th >&nbsp;VALOR VENTA &nbsp;</th>					
					<th >&nbsp;ECA_OFERTA &nbsp;</th>
					<th >&nbsp;VALOR ECA VENTA &nbsp;</th>				
					
					<th >&nbsp;DIF ECA_VENTA CONSORCIO NUEVO&nbsp;</th>		
					<th >&nbsp;DIF ECA_VENTA CONSORCIO ANTIGUO&nbsp;</th>		
					<th >&nbsp;DIF VENTA APPLUS NUEVO&nbsp;</th>					
					<th >&nbsp;DIF VENTA APPLUS ANTIGUO&nbsp;</th>		
					
					<th >&nbsp;% OFERTA APPLUS&nbsp;</th>		
					<th >&nbsp;% ECA_OFERTA CONSORCIO&nbsp;</th>		
					
					<th >&nbsp;PREFACTURA&nbsp;</th>
					<th >&nbsp;FACTURA_ECA&nbsp;</th>
					<th >&nbsp;FACTURA_CONTRATISTA&nbsp;</th>
					<th >&nbsp;FACTURA_RETENCION&nbsp;</th>
					<th >&nbsp;FACTURA_BONIFICACION&nbsp;</th>	
					
					<th >&nbsp;CXP APPLUS&nbsp;</th>
					<th >&nbsp;CXP ECA&nbsp;</th>
					<th >&nbsp;CXP PROVINTEGRAL&nbsp;</th>																		
					<th >&nbsp;ESQUEMA FINANCIACION&nbsp;</th>
					
					<th >&nbsp;OBSERVACI�N&nbsp;</th>
					<th >&nbsp;OBSERVACI�N OPEN&nbsp;</th>
					<th > &nbsp;SIMBOLO VARIABLE&nbsp;  </th>
					<th > &nbsp;ESQUEMA COMISION&nbsp;  </th>
					<th > &nbsp;ESTADO&nbsp;  </th>
					
					

			    </tr>								<%                                                                         
				NegocioApplus negocioApplus;
				for (int i=0; i<negocios.size(); i++) { 
					negocioApplus=(NegocioApplus)negocios.get(i);
					
					String valor_negocio=negocioApplus.getVlr();
					if (valor_negocio==null || valor_negocio.equals("")){
						valor_negocio="0";
					}
				%>									
			    <tr class='<%= (i%2==0?"filagris":"filaazul") %>' id='fila<%=i%>'   style=" font size:12"   >                                                                                
					<td class="bordereporte" align='center' nowrap style="font size:11"><%																					
						String value = negocioApplus.getId();
					%>
						<input type='checkbox' id="id<%=i%>" name='idNegocio' value='<%= value%>' onclick=" cambiarColorMouse(fila<%=i%>); seleccionarOrden(formulario,this.value,this.checked);">						
					</td>														 

					<td class="bordereporte" nowrap align="center"><a target="_blank" href="<%=BASEURL%>/jsp/applus/importar.jsp?num_osx=<%=  negocioApplus.getNumOs()%>&tipito=num_os"> <%=negocioApplus.getId()%> </a></td>                                                                             
				    <td class="bordereporte" nowrap align="center"><a target="_blank" href="<%=BASEURL%>/jsp/applus/mostrar_archivos.jsp?num_osx=<%=  negocioApplus.getNumOs()%>&tipito=num_os"> <%=negocioApplus.getNumOs()%> </a></td>                                                                             					
					<td class="bordereporte" nowrap align="center"><a target="_blank" href="<%=BASEURL%>/jsp/applus/importar.jsp?num_osx=<%=  negocioApplus.getIdAccion()%>&tipito=id_accion">_</a><%= (negocioApplus.getIdAccion())    %><a target="_blank" href="<%=BASEURL%>/jsp/applus/mostrar_archivos.jsp?num_osx=<%=  negocioApplus.getIdAccion()%>&tipito=id_accion">_</a></td>									

					<td class="bordereporte" nowrap align="center"> <%=  negocioApplus.getIdCliente()%></td>
					<td class="bordereporte" nowrap align="center"> <%=  negocioApplus.getNicClient()%></td>
					<td class="bordereporte" nowrap align="center"> <%=  negocioApplus.getNitClient()%></td>
					<td class="bordereporte" nowrap align="left"> <%=  negocioApplus.getNombreCliente()%></td>
					<td class="bordereporte" nowrap align="center"> <%=  negocioApplus.getTelCli()%></td>
					<td class="bordereporte" nowrap align="left"> <%=  negocioApplus.getContacto()%></td>
					<td class="bordereporte" nowrap align="center"> <%=  negocioApplus.getIdContratista()%></td>
					<td class="bordereporte" nowrap align="left"> <%=  negocioApplus.getNombreContratista()%></td>
					<td class="bordereporte" nowrap align="center"> <%=  negocioApplus.getCuotas()%></td>
					<%
					if (negocioApplus.getValCuotas()==null || negocioApplus.getValCuotas().equals("null") || negocioApplus.getValCuotas().equals("")){
						negocioApplus.setValCuotas("0");						
					}
					%>
					<!--<td class="bordereporte" nowrap align="center"><%//= Util.customFormat(Double.parseDouble(negocioApplus.getValCuotas()) )%> </td>-->
					<td class="bordereporte" nowrap align="center"> <%=  negocioApplus.getEstudio()%></td>

					<td class="bordereporte" nowrap align="center"> <%=  negocioApplus.getFacturaConformada()%></td>
					<td class="bordereporte" nowrap align="center"> &nbsp;<%if(negocioApplus.getFecha()!=null){out.print(negocioApplus.getFecha());}%></td>

   					<td class="bordereporte" nowrap align="center"> <%= Util.customFormat(Double.parseDouble(negocioApplus.getVlr())      )%></td>
					
					
					
					<td class="bordereporte" nowrap align="center"> <%= Util.customFormat(Double.parseDouble(negocioApplus.getOferta())      )%></td>
					<td class="bordereporte" nowrap align="center"> <%= Util.customFormat(Double.parseDouble(negocioApplus.getOferta())*1.16      )%></td>
										
					<td class="bordereporte" nowrap align="center"> <%= Util.customFormat(Double.parseDouble(negocioApplus.getEcaOferta())      )%></td>
					<td class="bordereporte" nowrap align="center"> <%= Util.customFormat(Double.parseDouble(negocioApplus.getEcaOferta()) *1.16     )%></td>	
					
					
					
					<%
					double ofertaxx=0;
					double eca_ofertaxx=0;
					double ofertaxx16=0;
					double eca_ofertaxx16=0;
					if (negocioApplus.getEsquemaComision().equals("MODELO_NUEVO")){
						ofertaxx=Double.parseDouble(negocioApplus.getVlr()) * 1.13;
					}else{
						ofertaxx=Double.parseDouble(negocioApplus.getVlr()) * 1.11;
					}
					
					if (negocioApplus.getEsquemaComision().equals("MODELO_NUEVO")){
						eca_ofertaxx=Double.parseDouble(negocioApplus.getVlr()) * 1.13*1.10;
					}else{
						eca_ofertaxx=Double.parseDouble(negocioApplus.getVlr()) * 1.11*1.12;
					}
					ofertaxx16=ofertaxx*1.16;
					eca_ofertaxx16=eca_ofertaxx*1.16;
					%>
					<td class="bordereporte" nowrap align="center"> <%=  Util.customFormat(ofertaxx)%></td>
					<td class="bordereporte" nowrap align="center"> <%=  Util.customFormat(ofertaxx16)%></td>
					<td class="bordereporte" nowrap align="center"> <%=  Util.customFormat(eca_ofertaxx)%></td>
					<td class="bordereporte" nowrap align="center"> <%=  Util.customFormat(eca_ofertaxx16)%></td>									
									
									
					
					<td class="bordereporte" nowrap align="center"> <%if ( negocioApplus.getEstudio().equals("Consorcio ECA-Applus-Fintravalores") && negocioApplus.getEsquemaComision().equals("MODELO_NUEVO")){ out.print(Util.customFormat(Double.parseDouble(negocioApplus.getDifEcaOfertaConsorcioNuevo()) ));}else{out.print(".");}%></td>		
					<td class="bordereporte" nowrap align="center"> <%if ( negocioApplus.getEstudio().equals("Consorcio ECA-Applus-Fintravalores") && negocioApplus.getEsquemaComision().equals("MODELO_ANTERIOR")){ out.print(Util.customFormat(Double.parseDouble(negocioApplus.getDifEcaOfertaConsorcioAntiguo()) ));}else{out.print(".");}%></td>		
					<td class="bordereporte" nowrap align="center"> <%if ( negocioApplus.getEstudio().equals("Applus Norcontrol") && negocioApplus.getEsquemaComision().equals("MODELO_NUEVO")){ out.print(Util.customFormat(Double.parseDouble(negocioApplus.getDifOfertaApplusNuevo()) ));}else{out.print(".");}%></td>										
					<td class="bordereporte" nowrap align="center"> <%if ( negocioApplus.getEstudio().equals("Applus Norcontrol") && negocioApplus.getEsquemaComision().equals("MODELO_ANTERIOR")){ out.print(Util.customFormat(Double.parseDouble(negocioApplus.getDifOfertaApplusAntiguo()) ));}else{out.print(".");}%></td>										
					
					<td class="bordereporte" nowrap align="center"> <%/*if ( negocioApplus.getEstudio().equals("Applus Norcontrol") ){*/ out.print(Util.FormatoMiles(   100.0*(Double.parseDouble(negocioApplus.getOferta())-Double.parseDouble(negocioApplus.getVlr()))/Double.parseDouble(negocioApplus.getVlr())         )+" %");/*}else{out.print(".");}*/%>&nbsp;</td>	
					<td class="bordereporte" nowrap align="center"> <%if ( Double.parseDouble(negocioApplus.getEcaOferta())!=0){ out.print(Util.FormatoMiles(   100.0*(Double.parseDouble(negocioApplus.getEcaOferta())-Double.parseDouble(negocioApplus.getOferta()))/Double.parseDouble(negocioApplus.getOferta())         )+" %");}else{out.print(".");}%>&nbsp;</td>
					
					<td class="bordereporte" nowrap align="center"> <%=  negocioApplus.getPrefactura()%></td>
					<td class="bordereporte" nowrap align="center"> <%=  negocioApplus.getFacturaEca()%></td>
					<td class="bordereporte" nowrap align="center"> <%=  negocioApplus.getFacturaContratista()%></td>
					<td class="bordereporte" nowrap align="center"> <%=  negocioApplus.getFacturaRetencion()%></td>
					<td class="bordereporte" nowrap align="center"> <%=  negocioApplus.getFacturaBoni()%></td>

					<td class="bordereporte" nowrap align="center"> <%=  negocioApplus.getFacturaApp()%></td>
					<td class="bordereporte" nowrap align="center"> <%=  negocioApplus.getFacturaPro()%></td>
					<td class="bordereporte" nowrap align="center"> <%=  negocioApplus.getFacturaComiEca()%></td>
					<td class="bordereporte" nowrap align="center"> <%=  negocioApplus.getEsquemaFinanciacion()%></td>
										
					<td class="bordereporte" nowrap align="center"> <%=  negocioApplus.getObservacion()%></td>
					<td class="bordereporte" nowrap align="center"> <%=  negocioApplus.getObservacionOpen()%></td>
					<td class="bordereporte" nowrap align="center"> <%=  negocioApplus.getSimbolo()%></td>
					<td class="bordereporte" nowrap align="center"> <%=  negocioApplus.getEsquemaComision()%></td>		
					
					<td class="bordereporte" nowrap align="center"> <%=  negocioApplus.getEstado()%></td>	
																								
				</tr>								
                                                                          
			<% } %>
								
			</table>		
		</td>
	</tr>
    </table>            
	<br>
	<center>	
	<br>
	<%if (loginx!=null && (loginx.equals("NAVI") || loginx.equals("JGOMEZ") || loginx.equals("SROBINSON") || true)) {%>
	<table>
	<tr class="fila">
		<td>
			&nbsp;&nbsp;Estado a asignar:&nbsp;&nbsp;
		</td>
		<td>
			<select name="estado_asignar" >
				<option value="nada">Seleccione</option>
				<!--
				<%/*if (estadi.equals("2")){%><option value="4">APROBADO FINANCIADO</option><%}%>	
				<%if (estadi.equals("2")){%><option value="5">APROBADO DE CONTADO</option><%}%>					
				<%if (estadi.equals("2")){%><option value="3">INCONSISTENCIA</option><%}%>
				<%if (estadi.equals("9") || estadi.equals("10")){%><option value="11">INGRESADO EN OPEN</option><%}*/%>	-->
				<%
					for (int i=0;i<listaEstados.size();i++){
						String[] estadinho=(String[])listaEstados.get(i);
						%>
						<option value="<%=estadinho[0]%>"><%=estadinho[1]%></option>
						<%
					}
					%>									
			</select>
			&nbsp;&nbsp;
		</td>
	</tr>
	<br>
	<div id="obser">
	<tr class="fila">
		<td>		
			Observaci�n&nbsp;:&nbsp;	
		</td>
		<td>
			<textarea name="observacion" rows="5"></textarea>
		</td>
	</tr>
	<tr class="fila">
		<td>
			&nbsp;&nbsp;Esquema de comisi�n:&nbsp;&nbsp;
		</td>
		<td>
			<select name="esquema_comision" >
				<option value=""></option>
						<option value="MODELO_NUEVO">MODELO_NUEVO</option>
						<option value="MODELO_ANTERIOR">MODELO_ANTERIOR</option>						
			</select>
			&nbsp;&nbsp;
		</td>
	</tr>
	<tr class="fila">
		<td>
			&nbsp;&nbsp;Esquema de financiaci�n:&nbsp;&nbsp;
		</td>
		<td>
			<select name="esquema_financiacion" >
				<option value=""></option>
						<option value="NUEVO">NUEVO</option>
						<option value="VIEJO">VIEJO</option>						
			</select>
			&nbsp;&nbsp;
		</td>
	</tr>
	<tr class="fila">
		<td>		
			&nbsp;&nbsp;Simbolo variable&nbsp;:&nbsp;	
		</td>
		<td>
			<input type="text" name="svx"  >
		</td>
	</tr>
	<tr class="fila">
		<td>		
			&nbsp;&nbsp;Cuotas reales&nbsp;:&nbsp;	
		</td>
		<td>
			<input type="text" name="cuoticas"  >
		</td>
	</tr>
	<%if (!(contrati.equals(""))){%>
	
	<tr class="fila">
		<td>		
	 	    <!--&nbsp;<input type="checkbox" name="esfactconformada">-->
			&nbsp;&nbsp;Factura Conformada&nbsp;:&nbsp;	
		</td>
		<td>
			<input type="text" name="fact_conformed"  >
		</td>
	</tr>
	<tr>
	
	<!--<tr class="fila">
		<td colspan="2">		
	 	    &nbsp;<input type="checkbox" name="esfactconformada">
			&nbsp;&nbsp;Pago Parcial&nbsp;&nbsp;	
		</td>		
	</tr>-->
	<%}%>
	<td  class="fila">&nbsp;F_facturado_cliente</td>
		<td >					
			<input name="fecfaccli" type="text" class="textbox" id="fecfaccli" size="10" >
			<span class="Letras"><img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fecfaccli);return false;" HIDEFOCUS></span>
			
		</td>
	</tr>
	</div>
	</table>	
		
	<br><br>
	<img src="<%=BASEURL%>/images/botones/aceptar.gif"       height="21"  title='Aceptar'    onclick='validar(formulario)'                                                                                         onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">        
	<%}%>
	<img src='<%=BASEURL%>/images/botones/salir.gif'      style='cursor:hand'    title='Salir...'      name='i_salir'       onclick='parent.close();'           onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
		
	</center>
	<br>
    <%  
	if (respuesta!=null){//request.getParameter("resultado")!=null && request.getParameter("resultado").equals("ok") ){
		%>
		<br><br>
		<p><table border="2" align="center">
		  <tr>
			<td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
			  <tr>
				<td width="170" align="center" class="mensajes"><%=""+respuesta%></td>
				<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
				<td width="40">&nbsp;</td>
			  </tr>
			</table></td>
		  </tr>
		</table>
		</p>
		
	<% }%>
     
 </form>
 <br> 
 <% }else{
 	%>lista de negocios vac�o. raro...<%    
 }	%>
</div>
<%=datos[1]%>
	<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    	
</body>
</html>
