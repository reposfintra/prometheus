<%--    Created on : 02/02/2009    Author     : iamorales--%>
<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.operation.model.*"%>
<%@ page import    ="com.tsp.operation.model.beans.*"%>
<%@ page import    ="com.tsp.operation.model.services.*"%>
<%@ page import    ="com.tsp.util.*"%>
<%@ page import    ="com.tsp.util.Util.*"%>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<%
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);

	Usuario usuario = (Usuario) session.getAttribute("Usuario");
	String loginx=usuario.getLogin();

	ClientesVerService clvsrv = new ClientesVerService();
        String perfil = clvsrv.getPerfil(usuario.getLogin());

	String estadi=request.getParameter("estadito");
	if (estadi==null){estadi="0";}

	String contrati=request.getParameter("contratistica");
	if (contrati==null){contrati="";}

	String fact_conformada_consultar=request.getParameter("fact_conformada_consultar");
	if (fact_conformada_consultar==null){fact_conformada_consultar="";}

	String id_solici=request.getParameter("id_solici");//090922
	if (id_solici==null){id_solici="";}//090922

	String id_cliente=request.getParameter("id_cliente");//090922
	if (id_cliente==null){id_cliente="";}//090922

	String nicc=request.getParameter("nicc");//090922
	if (nicc==null){nicc="";}//090922

	String nomclie=request.getParameter("nomclie");//090922
	if (nomclie==null){nomclie="";}//090922

	String num_osxi=request.getParameter("num_osx");
	if (num_osxi==null){num_osxi="";}

	ArrayList negocios ;

	try {    
		negocios = model.negociosApplusService.getNegociosApplus2(estadi,contrati,num_osxi,fact_conformada_consultar,loginx,id_solici,nicc ,"",id_cliente);
	}catch(Exception e ){
		System.out.println("errorcillo en model.NegociosApplusService.getNegociosApplus2()"+e.toString());
		negocios=null;
	}
%>
<html>
<%try{%>
<head>
	<title>GestiÃ³n de Negocios</title>
    <link href="<%= BASEURL %>/css/estilostsp.css"     rel="stylesheet" type="text/css"> 
    <link href="/css/estilostsp.css" rel="stylesheet"  type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
	
	
	<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
	<!--<script type='text/javascript' src="<%//= BASEURL %>/js/general.js"></script>-->
	<script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
	<script src='<%=BASEURL%>/js/date-picker.js'></script>
	<script src="<%= BASEURL %>/js/transferencias.js"></script>
	<script type='text/javascript' src="<%=BASEURL%>/js/prototype.js"></script>
	
	<%
	if (request.getParameter("nadita")!=null && request.getParameter("nadita").equals("si")){
		%><script>alert("No se hizo el cambio por restricciÃ³n de seguridad.");</script>
	<%}
	%>
    <script>
		function checkEnter(e){ //e is event object passed from function invocation//090720
			var characterCode //literal character code will be stored in this variable

			if(e && e.which){ //if which property of event object is supported (NN4)
				e = e
				characterCode = e.which //character code is contained in NN4's which property
			}else{
				e = event
				characterCode = e.keyCode //character code is contained in IE's keyCode property
			}

			if(characterCode == 13){ //if generated character code is equal to ascii 13 (if enter key)
				//document.forms[0].submit() //submit the form
				//alert("enter");
				//return false 
				consultarEstado();
			}else{
				//alert("e"+e+"char"+characterCode);
				return true 
			}
		}
		
		function formatx(input){//090831
		  	var num=""+input;
			num = num.replace(/\./g,'');
			if(!isNaN(num)){
				num = num.toString().split('').reverse().join('').replace(/(?=\d*\.?)(\d{3})/g,'$1.');
				num = num.split('').reverse().join('').replace(/^[\.]/,'');
				input = num;
			}else{ 
				alert('Solo se permiten números');
				//input.value = input.value.replace(/[^\d\.]*/g,'');
			}			
			return num;
		}
		
		function sumarOferta(solicitt){
			var vals = document.getElementsByName(solicitt);
			var respuest=0;
			var temx=0;
			for (var i=0;i<vals.length; i++){
				temx=vals[i].value.replace(",","");
				temx=temx.replace(",","");			
				//subtotal=parseInt(subtotal)+parseInt(tem);
				respuest=parseInt(respuest)+parseInt(temx);			    
			 	//respuest=respuest+vals[i].value;			    
			}
			alert("oferta: "+formatx(respuest));			
		}
		
		function validar(theForm){
			//if (formulario.estado_asignar.value=="nada"){alert("Debe escoger un estado a asignar.");return;}
			var elementos = document.getElementsByName("idNegocio");
			var sw = false;

			for (var i=0;i<elementos.length; i++){
			if (elementos[i].checked){
			    sw = true;
			}
			}
			if(sw==true){
			//alert ("cas");
			formulario.action="<%=CONTROLLER%>?estado=Negocios&accion=Applus&opcion=cambiarEstado";
			theForm.submit();
			}else{
			alert("Verifique que haya al menos un negocio seleccionado.");
			}
		}
		
		function Sell_all_col(theForm,nombre){

                    for (i=0;i<theForm.length;i++)
                        if (theForm.elements[i].type=='checkbox' && theForm.elements[i].name==nombre)
                            theForm.elements[i].checked=theForm.All.checked;
                }
		
		function consultarCliente(){
			if (formulario.nicc.value=="" && formulario.idclie.value==""){
				alert("Debe tener nic o nombre de cliente.");				
			}else{
				formulario.target="_blank";
				formulario.action="<%=CONTROLLER%>?estado=Clientes&accion=Ver&opcion=buscarcliente";
				formulario.submit();			
			}
		}

                /*function consultarSolicitudX(idsolicit){
				formulario.target="_blank";
				formulario.action="<%=CONTROLLER%>?estado=Clientes&accion=Ver&opcion=buscarsolicitud&idsolicitud="+idsolicit;
				formulario.submit();

		}*/

		function seleccionarOrden(theForm,orden,che){				
				for (i=0;i<theForm.length;i++){
						  if (theForm.elements[i].type=='checkbox' && theForm.elements[i].value==orden){
							 theForm.elements[i].checked=che;							 
						  }
				}			
		}
		
		function consultarEstado(){
			//alert("cambio de consulta");
                        formulario.target ="";
			formulario.action="<%=CONTROLLER%>?estado=Negocios&accion=Applus&opcion=consultarEstado2";
			formulario.submit();

		}
		
		function asignar(theForm){
			//alert("va a asignar");			  
		    var elementos = document.getElementsByName("idNegocio");
		   	var sw = false;           
			for (var i=0;i<elementos.length; i++){
				if (elementos[i].checked){
					sw = true;			
				}  
			}		
			if(sw==true){			
				formulario.action="<%=CONTROLLER%>?estado=Negocios&accion=Applus&opcion=asignar";
				theForm.submit();
			}else{
				alert("Verifique que haya al menos una solicitud seleccionada.");
			}
	    	}

		//funciones hechas por PBASSIL para la parte de
                //AJAX de la generacion de OT's
                function sendAction(url){

                    new Ajax.Request(
                        url,
                        {
                            method: 'get',
                            onComplete: sendMessage
                        });
                }

                function sendMessage(response){
                    alert(response.responseText);
                }
								
    </script>
</head>
<body onLoad="redimensionar();formulario.id_solici.focus();" onResize="redimensionar();"><!--redimensionar-->

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Negocios"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: -1px; top: 100px; overflow: scroll;">       
<%   
      
   ArrayList listaEstados=model.negociosApplusService.getEstadosApplus2();  
   
   ArrayList listaEstadosUser=model.negociosApplusService.getEstadosApplusUser(loginx);  
   
   ArrayList contratistas=model.negociosApplusService.getContratistas();
   
   String nombre_estado="";
   String nombre_contratista="";
      
   String[] estadixx;
   
   String[] contratixx;

   for (int i=0;i<listaEstados.size();i++){
		estadixx=(String[]) listaEstados.get(i);
		if(estadixx[0].equals(estadi)){ nombre_estado=estadixx[1];} 
   }
   for (int i=0;i<contratistas.size();i++){
		contratixx=(String[]) contratistas.get(i);
		if(contratixx[0].equals(contrati)){ nombre_contratista=contratixx[1];} 
   }
   
   if (loginx!=null ){
        if (  usuario.getNitPropietario().equals("CC002") ) {		contrati="CC002";}
        if (  usuario.getNitPropietario().equals("CC008") ) {		contrati="CC008";}
        if (  usuario.getNitPropietario().equals("CC016") ) {		contrati="CC016";}//090730
        if (  usuario.getNitPropietario().equals("CC017") ) {		contrati="CC017";}//090730
        if (  usuario.getNitPropietario().equals("CC027") ) {		contrati="CC027";}//090730
        if (  usuario.getNitPropietario().equals("CC036") ) {		contrati="CC036";}
        if (  usuario.getNitPropietario().equals("CC038") ) {		contrati="CC038";}
        if (  usuario.getNitPropietario().equals("CC082") ) {		contrati="CC082";}
        if (  usuario.getNitPropietario().equals("CC081") ) {		contrati="CC081";}

   		//if (  loginx.equals("SENTEL") ) {		contrati="CC002";}
   }

   if (negocios!=null  ){
%>   
    <form  method="post" name="formulario"  >
    <input type="hidden" name="estadito" value="<%=estadi%>">
    <%
    String respuesta=request.getParameter("respuesta");
	//String valor_neto="";//request.getParameter("valor_neto");	
    %>
    <br><br>
	<table align="center">
		<tr class="fila">
			<td>
				&nbsp;&nbsp;Estado actual:&nbsp;&nbsp; 
			</td><td>				
				<select name="estado_consultar" onChange="consultarEstado();">
									
					<option value="<%=estadi%>"><%=nombre_estado%></option>
					<%
					for (int i=0;i<listaEstados.size();i++){
						String[] estadinho=(String[])listaEstados.get(i);
						%>
						<option value="<%=estadinho[0]%>"><%=estadinho[1]%></option>
						<%
					}
					%>
					
				</select>				
				&nbsp;&nbsp;
			</td>
		</tr>
		<tr class="fila">
			<td>
				&nbsp;&nbsp;Contratista:&nbsp;&nbsp; 
			</td><td>
				<%if (usuario.getNitPropietario().equals("CC002") || usuario.getNitPropietario().equals("CC008") || usuario.getNitPropietario().equals("CC036") || usuario.getNitPropietario().equals("CC038") || usuario.getNitPropietario().equals("CC027") || usuario.getNitPropietario().equals("CC016") || usuario.getNitPropietario().equals("CC017") || usuario.getNitPropietario().equals("CC082") || usuario.getNitPropietario().equals("CC081")){//090730%>
				<%//if (loginx!=null && (  loginx.equals("SENTEL")  )) {%>
					<input type="text" name="contratista_consultar"  value="<%=contrati%>" readonly  >				
				<%}else{%>
					<select name="contratista_consultar"  >
						<option value="<%=contrati%>"><%=nombre_contratista%></option>
						<option value=""> </option>
						<%
						for (int i=0;i<contratistas.size();i++){
							String[] contratistanho=(String[])contratistas.get(i);
							%>
							<option value="<%=contratistanho[0]%>"><%=contratistanho[1]%></option>
							<%
						}
						%>
					</select>		
				<%}%>		
				&nbsp;&nbsp;
			</td>
		</tr>
		
		<tr class="fila">
		<td>		
	 	    <!--&nbsp;<input type="checkbox" name="esfactconformada">-->
			&nbsp;&nbsp;Multiservicio&nbsp;:&nbsp;
		</td><td>		
			<input type="text" name="num_osx"  value="<%=num_osxi%>"  onKeyPress="checkEnter(event)" >			
		</td>
	</tr>
	<!--<tr class="fila">
		<td>			 	   
			&nbsp;&nbsp;Factura Conformada&nbsp;:&nbsp;	
		</td><td>					
			<input type="text" name="fact_conformada_consultar"  value="<%//=fact_conformada_consultar%>" >
		</td>
	</tr>-->
	<input type="hidden" name="fact_conformada_consultar" value="">
	<tr class="fila">
		<td>			 	   
			&nbsp;&nbsp;Id Solicitud&nbsp;:&nbsp;					
		</td><td>	
			<input type="text" name="id_solici"  value="<%=id_solici%>"  onKeyPress="checkEnter(event)">
			<img src='<%=BASEURL%>/images/botones/iconos/buscar.gif' width="15" height="15" onClick="consultarEstado();" title="Buscar Solicitud" style="cursor:hand" >
		</td>
	</tr>
	<tr class="fila">
		<td>			 	   
			&nbsp;&nbsp;Nic&nbsp;:&nbsp;		
		</td><td>				
			<input type="text" name="nicc"  value="<%=nicc%>"  onKeyPress="checkEnter(event)" >
		</td>
	</tr>
	<tr class="fila">
		<td>			 	   
			&nbsp;&nbsp;Nombre Cliente&nbsp;:&nbsp;		
		</td><td>				
			<!--<input type="text" name="nomclie"  value="<%//=nomclie%>"  onKeyPress="checkEnter(event)" >-->						
                        <input name="nomclie" id="nomclie" type="text" value="<%=nomclie%>"  onKeyPress="checkEnter(event)" class="textbox" id="campo" style="width:200;"  onChange="sendAction('<%=CONTROLLER%>?estado=Negocios&accion=Applus',document.getElementById('nomclie').value,'idclie','');" size="15" >

		</td>		
	</tr>
	<tr class="fila">
		<td>&nbsp;			 	   
			
		</td>
                <td>
                    <div id="cliselect"></div>
                    <div id="imgworking" align="right"  style="visibility:hidden"><img src="<%=BASEURL%>/images/cargando.gif"></div>


                    <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif' width="15" height="15" onClick="consultarCliente();" title="Buscar Cliente" style="cursor:hand" >&nbsp;&nbsp;
                    <%if ( (clvsrv.ispermitted(perfil, "5")||clvsrv.ispermitted(perfil, "6")) ){%>
                        <a target="_blank" href="<%=CONTROLLER%>?estado=Clientes&accion=Ver&opcion=N">crear cliente</a>
                    <%}%>
                </td>
        </tr>
        </table>
        <br>
	<table width="689"  border="2" align="center">
		<tr>
		    <td >
			<table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
				<tr class="fila">
				        <TH   nowrap style="font size:11; font weight: bold" > <input type='checkbox'  id='All' onClick="Sell_all_col(this.form,'idNegocio');">  </TH>

				        <th > &nbsp;ID SOLICITUD&nbsp;  </th>
				        <th > &nbsp;ID CLIENTE&nbsp;</th>
				        <th > &nbsp;NIC&nbsp;</th>
				        <th > &nbsp;NIT&nbsp;</th>
				        <th > &nbsp;NOMBRE CLIENTE&nbsp;</th>
				        <th > &nbsp;TIPO CLIENTE&nbsp;</th>

				        <th > &nbsp;NOMBRE CONTRATISTA&nbsp;</th>
				        <th > &nbsp;COSTO CONTRATISTA&nbsp;</th>
				        <%if (!perfil.equals("contratista")){%>
                        	<th > &nbsp;PRECIO VENTA&nbsp;</th>
                        <%}%>
				        <th > &nbsp;ACCIONES...&nbsp;</th>
				        <th > &nbsp;ALCANCES...&nbsp;</th>
				        <th > &nbsp;ID ORDEN&nbsp;  </th>
                                        <th > &nbsp;CONSECUTIVO&nbsp;  </th>
				        <th > &nbsp;No. ORDEN DE TRABAJO&nbsp;  </th>
				        <th > &nbsp;ACCION&nbsp;  </th>
				        <th > &nbsp;ESTADO&nbsp;</th>
				        <th > &nbsp;ENTREGA OFERTA&nbsp;</th>
			    	</tr>								<%
				NegocioApplus negocioApplus;
				for (int i=0; i<negocios.size(); i++) {
				        negocioApplus=(NegocioApplus)negocios.get(i);

				        String valor_negocio=negocioApplus.getVlr();
				        if (valor_negocio==null || valor_negocio.equals("")){
				                valor_negocio="0";
				        }
				%>
				<tr class='<%= (i%2==0?"filagris":"filaazul") %>' id='fila<%=i%>' style=" font size:12">
				    <td class="bordereporte" align='center' nowrap style="font size:11">
					<%
					String value = negocioApplus.getIdSolicitud();
					%>
					<input type='checkbox' id="id<%=i%>" name='idNegocio' value='<%=value%>' onClick=" cambiarColorMouse(fila<%=i%>); seleccionarOrden(formulario,this.value,this.checked);$('id_solicitud').value = <%=value%>"">
				    </td>
				    <td class="bordereporte" nowrap align="center">
					<a  href="<%=CONTROLLER%>?estado=Clientes&accion=Ver&opcion=buscarsolicitud&idsolicitud=<%=negocioApplus.getIdSolicitud()%>" target="_blank">
					<%=  negocioApplus.getIdSolicitud()%>
					</a>
					<%if ( (clvsrv.ispermitted(perfil, "1")||clvsrv.ispermitted(perfil, "6")) ){%>
                                            <img src='<%=BASEURL%>/images/botones/iconos/clasificar.gif' width="14" height="14" onclick="window.open('<%=BASEURL%>/jsp/delectricaribe/generar_oferta.jsp?id_solicitud=<%=negocioApplus.getIdSolicitud()%>','','width=1000,height=800')" title="Generar Oferta" style="cursor:hand">
                                        <%}%>
				    </td>
					<td class="bordereporte" nowrap align="center"> <%= negocioApplus.getIdCliente()%></td>
					<td class="bordereporte" nowrap align="center"> <%= negocioApplus.getNicClient()%></td>
					<td class="bordereporte" nowrap align="center"> <%= negocioApplus.getNitClient()%></td>
					<td class="bordereporte" nowrap align="left">   <%= negocioApplus.getNombreCliente()%></td>
					<td class="bordereporte" nowrap align="center"> <%= negocioApplus.getTipoCliente()%></td>
					<td class="bordereporte" nowrap align="left">   <%= negocioApplus.getNombreContratista()%></td>
					<td class="bordereporte" nowrap align="center"> <%= Util.customFormat(Double.parseDouble(negocioApplus.getVlr()))%></td>
					
					<%if (!perfil.equals("contratista")){%>						
						<td class="bordereporte" nowrap align="center" > 
  					 		<a onClick="sumarOferta(<%=value%>);"  style="cursor:hand" title="sumar">
								<%= Util.customFormat(Double.parseDouble(negocioApplus.getEcaOferta()))%>						
							</a>
						</td>				
                	<%}%>
					
					
					
					
					<input type="hidden" name="<%=value%>" value="<%=Util.customFormat(Double.parseDouble(negocioApplus.getEcaOferta()))%>">
					
					<td class="bordereporte" nowrap align="left">
					<%if (negocioApplus.getAcciones()!=null && negocioApplus.getAcciones().length()>50){%>
						        <a target="_blank" href="<%=CONTROLLER%>?estado=Negocios&accion=Applus&opcion=consultarAlcances&id_solici=<%=  negocioApplus.getIdSolicitud()%>&id_accion=<%=negocioApplus.getIdAccion()%>"><%=  negocioApplus.getAcciones().substring(0,49)%></a>

					<%}else{%>
						        <a target="_blank" href="<%=CONTROLLER%>?estado=Negocios&accion=Applus&opcion=consultarAlcances&id_solici=<%=  negocioApplus.getIdSolicitud()%>&id_accion=<%=negocioApplus.getIdAccion()%>"><%=  negocioApplus.getAcciones()%></a>
					<%}%>
					</td>

					<td class="bordereporte" nowrap align="left">
					<%if (negocioApplus.getAlcances()!=null && negocioApplus.getAlcances().length()>50){%>
					<!--<a target="_blank" href="<%//=CONTROLLER%>?estado=Negocios&accion=Applus&opcion=consultarAlcances&id_solici=<%//=  negocioApplus.getIdSolicitud()%>&id_accion=<%//=negocioApplus.getIdAccion()%>">--><%=  negocioApplus.getAlcances().substring(0,49)%><!--</a>-->

					<%}else{%>
						        <!--<a target="_blank" href="<%//=CONTROLLER%>?estado=Negocios&accion=Applus&opcion=consultarAlcances&id_solici=<%//=  negocioApplus.getIdSolicitud()%>&id_accion=<%//=negocioApplus.getIdAccion()%>">--><%=  negocioApplus.getAlcances()%><!--</a>-->
					<%}%>
					</td>

					<td class="bordereporte" nowrap align="center"> <%= negocioApplus.getId()%></td>
                                        <td class="bordereporte" nowrap align="center"> <%= negocioApplus.getConsecutivo_oferta()%></td>
					<td class="bordereporte" nowrap align="center"> <%= negocioApplus.getNumOs()%></td>
					<td class="bordereporte" nowrap align="center"> <%= negocioApplus.getIdAccion()%></td>
					<td class="bordereporte" nowrap align="center"> <%= negocioApplus.getEstado()%></td>
					<td class="bordereporte" nowrap align="center" title="<%=negocioApplus.getUsuarioEntregaOferta()+"_"+negocioApplus.getCreacionFechaEntregaOferta()%>"> <%=  negocioApplus.getFechaEntregaOferta()%></td>
				</tr>

			<% } %>

			</table>
		    </td>
		</tr>
	</table>
	<br>
	<center>	
	<br>
	<table>
	<%
	boolean sw_modificador=false;
	%>	
	</div>
	</table>	
	<%	
	String hoy = Utility.getHoy("-");
	boolean poner_fecha_entrega_oferta=false;	
	if (negocios!=null && negocios.size()>0 && estadi.equals("050")){
		poner_fecha_entrega_oferta=true;
	}
	if (poner_fecha_entrega_oferta){
	%>
		
	 <table align="center">
		<tr  class="fila">
			<td>&nbsp;Fecha de entrega de oferta :&nbsp;</td>
			<td >					
				<input name="fecof" type="text" class="textbox" id="fecof" size="10" value="<%=hoy%>" >
				<span class="Letras"><img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onClick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fecof);return false;" HIDEFOCUS></span>
				
			</td>
		</tr>
	 </table>
	<%}%>
	<br><br>
	<%	if (poner_fecha_entrega_oferta){%>
		<img src="<%=BASEURL%>/images/botones/aceptar.gif" height="21" title='Aceptar' onclick='asignar(formulario)' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
	<%}%>

	
	<%
	boolean poner_generar_ot=false;
	if (/*negocios!=null && negocios.size()>0 && */estadi.equals("060")){
            poner_generar_ot=true;
	}
	if (poner_generar_ot){
	%>

            <br><br>
            <table width="414">
                <tr class="fila">
                    <td colspan="2">
                        <div align="center"><strong>GENERACION OT</strong></div>
                    </td>
                </tr>
                <tr class="fila">
                    <td width="159"><div align="center">Id Solicitud : </div></td>
                    <td width="243" align="center">
                        <input type="text" value="" id="id_solicitud"/>
                    </td>
                </tr>
                
                <tr class="fila">
                    <td width="159"><div align="center">Cuotas : </div></td>
                    <td width="243" align="center">
                        <input type="text" value="" id="cuotas"/>
                    </td>
                </tr>
                <tr class="fila">
                    <td width="159"><div align="center">% Cuota inicial : </div></td>
                    <td width="243" align="center">
                        <select>
                            <%
                            for(int i=0; i<=100; i++){%>

                            <option><%=i%></option>

                            <%}
                            %>
                        </select>
                    </td>
                </tr>
                <tr class="fila">
                    <td width="159"><div align="center">NIC a facturar : </div></td>
                    <td width="243" align="center">
                        <input type="text" value="" id="nic_fac"/>
                    </td>
                </tr>
                
                <tr class="fila">
                    <td width="159" height="81"><div align="center">Observaciones : </div></td>
                    <td width="243" align="center">
                        <textarea style="height:8em;width:15em;" id="observaciones"></textarea>
                    </td>
                </tr>
            </table>
            <!--<br>
            <input id="consecutivo" name="consecutivo" type="checkbox" value="true">Si esta OT tiene un consecutivo, no borrarlo
            -->
            <br>
            <img src="<%=BASEURL%>/images/botones/generar.gif"  name="imgaceptar" onClick="sendAction('<%=CONTROLLER%>?estado=Electricaribe&accion=Ot&opcion=1&id_solicitud=' + $('id_solicitud').value + '&observaciones=' + $('observaciones').value+ '&nic_fac=' + $('nic_fac').value + '&cuotas=' + $('cuotas').value);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">

        <%
        }%>


	<img src='<%=BASEURL%>/images/botones/salir.gif'      style='cursor:hand'    title='Salir...'      name='i_salir'       onclick='parent.close();'           onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
	<br><br><br>
	
	</center>
	<br>

    	<%
	if (respuesta!=null){//request.getParameter("resultado")!=null && request.getParameter("resultado").equals("ok") ){
		%>
		<br><br>
		<p><table border="2" align="center">
		  <tr>
			<td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
			  <tr>
				<td width="170" align="center" class="mensajes"><%=""+respuesta%></td>
				<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
				<td width="40">&nbsp;</td>
			  </tr>
			</table></td>
		  </tr>
		</table>
		</p>
		
	<% }%>
<script>
    var temp=';;;;;;;;;;';
    /**
 * Ajax.Request.abort
 * extend the prototype.js Ajax.Request object so that it supports an abort method
 */

    function sendAction(url,filtro,nomselect,def){
        if(temp!=filtro)
        {   $('imgworking').style.visibility="visible";
            temp=filtro;
            var p = "filtro=" + filtro + "&nomselect=" + nomselect  + "&opcion=clselect" + "&idclie="+def;

            new Ajax.Request(
            url,
            {
                method: 'get',
                parameters: p,
                onSuccess: deploySelect
            });

        }
    }

    function deploySelect(response){
        respuesta=response.responseText.split(';;;;;;;;;;');
        if(respuesta[0]==$('nomclie').value)
        {   document.getElementById("cliselect").innerHTML=respuesta[1];
            $('imgworking').style.visibility="hidden";
        }
    }

    sendAction('<%=CONTROLLER%>?estado=Negocios&accion=Applus',document.getElementById('nomclie').value,'idclie','<%=(request.getParameter("id_cliente")!=null)?request.getParameter("id_cliente"):""%>');


</script>

 </form>
 <br> 
 <% }else{
 	%>lista de negocios vacÃ­o. raro...<%    
 }	%>
</div>
<%=datos[1]%>
	<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
<%}catch(Exception eeee){
	System.out.println("errror:::"+eeee.toString());
//	eeee.
}
%>

</html>


