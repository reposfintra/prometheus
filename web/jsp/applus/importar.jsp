<!DOCTYPE html>
<%@page import="com.tsp.operation.model.services.NegociosApplusService"%>
<!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      25/01/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   
     - @(#)  
     - Description: Importacion Directa de un archivo
--%> 
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import   ="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.operation.model.beans.BeanGeneral"%>
<%@page import="com.tsp.opav.model.services.GestionSolicitudAiresAAEService"%>
<%@page import="com.tsp.operation.model.services.GestionSolicitudAvalService"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<html>
    <head>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
    String Mensaje = (request.getParameter("Mensaje") != null ? request.getParameter("Mensaje") : "");
    Usuario user = (Usuario) session.getAttribute("Usuario");
    String loginx = user.getLogin();
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL, request.getRequestURI(), path);
    String form = request.getParameter("form") != null ? request.getParameter("form") : "";
    session.setAttribute("numSolicitud", form);
    String tipoconv = request.getParameter("tipoconv") != null ? request.getParameter("tipoconv") : "";
    String vista = request.getParameter("vista") != null ? request.getParameter("vista") : "0";
    session.setAttribute("myvista", vista);
    String numos = request.getParameter("num_osx");
    GestionSolicitudAiresAAEService sasrv = new GestionSolicitudAiresAAEService();
    GestionSolicitudAvalService gsaserv = new GestionSolicitudAvalService(user.getBd());
    
    ArrayList datos_seguro = new ArrayList ();
    String[] dato1 = null;
    ArrayList <BeanGeneral> garantias = gsaserv.ListaTablaGen("GARANT_SEG");

    if (numos == null) {
        numos = "";
    }
    String tipito = request.getParameter("tipito");
    if (tipito == null) {
        tipito = "";
    }
    if(tipito.equals("negocio")&&(tipoconv.equals("Multiservicio"))&&vista.equals("6")){
        datos_seguro = sasrv.buscarGarantiaEquipos(form);
    }

    String multixx = request.getParameter("multixx");//090715
    if (multixx == null) {
        multixx = "";
    }//090715
    String contratixx = request.getParameter("contratixx");//090715
    if (contratixx == null) {
        contratixx = "";
    }//090715

    String cerrar = request.getParameter("cerrar");
    if (cerrar != null && (cerrar.equals("si"))) {
%>
<script>
    //alert("se va a cerra");
    window.close();
</script>
<%            }

%>    
        <meta charset="UTF-8">
        <title>Importar Archivos</title>
        <link   href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet">
        <script src ="<%= BASEURL%>/js/boton.js"></script>	
        <link href="<%=BASEURL%>/css/mac_os_x.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/default.css" rel="stylesheet" type="text/css">
        <script type='text/javascript' src="<%= BASEURL%>/js/prototype.js"></script>
        <script src ="<%= BASEURL%>/js/negocioTrazabilidad.js"></script>
        <script src="<%=BASEURL%>/js/effects.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/window.js" type="text/javascript"></script>
        <script>
            function validarExtension ( archivo ){	//090715   	
                var ext = /.EXE$/gi;
                if (ext.test(archivo.toUpperCase()) ){
                    alert ('Extension no valida, para el archivo de importacion');
                    return false;
                } 
                //inicio de 090715		 
            <%
                if (tipito.equals("id_accion") || tipito.equals("num_os")) {
            %>		    
                if (archivo.toUpperCase().indexOf("OFERTA")!=-1) {	
                    fimp.finarchivo.value="OFERTA";				
                    fimp.swmsgx.value="si";
                }			

                if (archivo.toUpperCase().indexOf("FINAL")!=-1) {	
                    fimp.finarchivo.value="FINAL";				
                    fimp.swmsgx.value="si";
                }		

                if (archivo.toUpperCase().indexOf("CREDITO")!=-1) {					
                    fimp.finarchivo.value="CREDITO";				
                    fimp.swmsgx.value="si";
                }
           <%
                }
            %>
                return true;
            }
                         
            function validarNombreArchivo (archivo){    
                if (!archivo.match(/^[0-9a-zA-Z\_\-\(\)]*\.{1}[0-9a-zA-Z]+$/)) {                                
                    alert('Nombre de archivo no válido. Caracteres permitidos( números, letras, (, ), _, - ). Por favor, Verifique.');                                
                    return false;
                }
                return true;                          
            }

            function validarArchivo (archivo){    
                var estado = false; 
                var startIndex = (archivo.indexOf('\\') >= 0 ? archivo.lastIndexOf('\\') : archivo.lastIndexOf('/'));
                var filename = archivo.substring(startIndex);
                if (filename.indexOf('\\') === 0 || filename.indexOf('/') === 0) {
                        filename = filename.substring(1);
                }
                if (validarNombreArchivo(filename)){
                   estado = validarExtension(filename);
                }
                if (estado==false) fimp.archivo.value = '';
                return estado;
            }

            function borrar()	{
                window.close(    );
                window.open('<%= CONTROLLER%>?estado=Negocios&accion=Applus&opcion=borrar2','na','status=yes,scrollbars=yes,width=400,height=150,resizable=no');			
            } 

            function regresar()            {
                 location.href ="<%= CONTROLLER%>?estado=Negocios&accion=Ver&op=5&vista=6";	
            }
        </script>
    </head>
    <body onresize="redimensionar()"   onLoad="<%=tipito.equals("negocio") ? "" : "window.resizeTo(700, 600);"%>"  >
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Importacion de Archivos "/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <center>
                <br>
                <form action="<%= CONTROLLER%>?estado=Negocios&accion=Applus&opcion=subirArchivo" method="post" name="fimp" enctype='multipart/form-data' charset="UTF-8">
                    <input type="hidden" name="multixx" value="<%=multixx%>"><!--090715-->
                    <input type="hidden" name="contratixx" value="<%=contratixx%>"><!--090715-->	
                    <input type="hidden" name="finarchivo" value=""><!--090715-->
                    <input type="hidden" name="swmsgx" value="no"><!--090715-->	
                    <table width="500" border="2" align="center">
                        <tr>
                            <td align="center">

                                <!-- cabecera de la seccion -->
                                <table width="100%" >
                                    <tr>
                                        <td class="subtitulo1"  width="50%">Importar Archivo</td>
                                        <td class="barratitulo" width="50%">
                                            <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%>
                                        </td>
                                    </tr>
                                </table>
                                <!-- fin cabecera -->

                                <table  width="100%" >
                                    <tr class="fila">
                                        <td width='40%'>&nbsp;Código&nbsp;</td>
                                        <td width='60%'>
                                            <input type="text" value="<%=numos%>" name="numerit_osx"  maxlength="50" <%if (!(numos.equals(""))) {
                                        out.print("readonly");
                                    }%>   >
                                        </td>
                                    </tr>	

                                    <tr class="fila">
                                        <td width='40%'>&nbsp;Tipo&nbsp;</td>
                                        <td width='60%'>
                                               <input type="text" value="<%=tipito%>" name="tipito"  maxlength="20"  <%if (!(tipito.equals(""))) {
                                        out.print("readonly");
                                    }%> >
                                            <% if (tipito.equals("negocio")) {%>
                                            <input type="hidden" name="form" value="<%=form%>">
                                            <input type="hidden" id="tipoconv" name="tipoconv" value="<%=tipoconv%>">
                                            <input type="hidden" name="vista" value="<%=vista%>">
                                            <%}%>
                                        </td>
                                    </tr>	

                                    <%if (1 == 2) {//(loginx.equals("NAVI") || loginx.equals("JGOMEZ")){%>
                                    <tr class="fila">
                                        <td width='40%'>&nbsp;Id&nbsp;</td>
                                        <td width='60%'>
                                            <input type="text" name="idxx" maxlength="8" >
                                        </td>
                                    </tr>
                                    <%}%>
                                    <tr class="fila">
                                        <td >&nbsp;Categoría archivo</td>
                                        <td>
                                            <select id="categoria_archivo" name="categoria_archivo" required>
                                                <option selected value="">Seleccione una opción</option>
                                                <optgroup label="Documentos titular">
                                                <%
                                                    NegociosApplusService nas = new NegociosApplusService(usuario_sesion.getBd());
                                                    String[][] categoriaArchivos = nas.obtenerNombresArchivos(numos);
                                                    
                                                    if (categoriaArchivos != null) {
                                                        int i = 0; 
                                                        do {
                                                %>
                                                <option value="<%=categoriaArchivos[i][0]%>"><%=categoriaArchivos[i][1]%></option>
                                                <%
                                                            i++;
                                                        } while (i < categoriaArchivos.length && categoriaArchivos[i][2].equals("A"));
                                                %>
                                                </optgroup>
                                                <%      if (i < categoriaArchivos.length) {
                                                %>                                                    
                                                <optgroup label="Documentos codeudor">                                                    
                                                <%
                                                            while (i < categoriaArchivos.length && categoriaArchivos[i][2].equals("B")) {
                                                %>
                                                <option value="<%=categoriaArchivos[i][0]%>"><%=categoriaArchivos[i][1]%></option>
                                                <%
                                                                i++;
                                                            }
                                                %>
                                                </optgroup>
                                                <%
                                                        }
                                                    }
                                                %>
                                            </select>
                                        </td>
                                    </tr>
                                    <tr class="fila">
                                        <td >&nbsp;Seleccione su archivo</td>
                                        <td ><input type='file' name='archivo' style='width:100%'></td>
                                    </tr>
                                </table>
                            </td>         
                        </tr>
                    </table>
                    <br>
                   <table>
                        <tr class="fila">
                            <td colspan="2" align="center">
                                <div id="busqueda"></div>
                            </td>
                        </tr>
                    </table>
                    <%if (tipito.equals("negocio") && (tipoconv.equals("Multiservicio")) && vista.equals("6")) {%>
                    <table width="90%" border="2" align="center">
                        <tr>
                            <td align="center">

                                <!-- cabecera de la seccion -->
                                <table width="100%" >
                                    <tr>
                                        <td class="subtitulo1"  width="50%">Seguros Chartis</td>
                                        <td class="barratitulo" width="50%">
                                            <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%>
                                        </td>
                                    </tr>
                                </table>
                                <!-- fin cabecera -->

                                <table  width="100%" >
                                    <tr class="subtitulo1" style=" background-color:#006C3A;">
                                        <th style="vertical-align: top; width: 10%;">Item
                                        </th>
                                        <th style="vertical-align: top; width: 30%;">Equipos
                                        </th>
                                        <th style="vertical-align: top; width: 30%;">Formulario
                                        </th>
                                        <th style="vertical-align: top; width: 30%;">Garantia
                                        </th>
                                    </tr>
                                    <% int tot = 0;
                                        String id_solicitud="";

                                        for (int i = 0; i < datos_seguro.size(); i++) {


                                            String[] dato = ((String) datos_seguro.get(i)).split(";_;");
                                             if (i == 0) {
                                                 id_solicitud = dato[5];
                                             }
                                            if (dato[2].equals("null")) {
                                                int cant = (int) Double.parseDouble(dato[4]);
                                                for (int j = 0; j < cant; j++) {
                                    %>
                                    <tr align="center" class="fila">
                                         <td><%=tot+1%></td>
                                        <td><input type='hidden' id="equipo<%=tot%>" name='equipo<%=tot%>' value="<%=dato[0]%>"><%=dato[1]%></td>
                                        <td><input type='text' id="formulario<%=tot%>" name='formulario<%=tot%>' value="" ></td>
                                        <td> <select id="garantia<%=tot%>" name="gatantia<%=tot%>">
                                                <option value="">...</option>
                                                <% for (int k = 0; k < garantias.size(); k++) {%>
                                                <option value="<%= garantias.get(k).getValor_01()%>" ><%=garantias.get(k).getValor_02()%></option>
                                                <%  }%>
                                            </select> </td>
                                    </tr>
                                    <% tot++;
                                                                                    }
                                                                                } else {
                                                                                    %>
                                    <tr class="fila">
                                          <td><%=tot+1%></td>
                                        <td><input type='hidden' id="equipo<%=tot%>" name='equipo<%=tot%>' value="<%=dato[0]%>"><%=dato[1]%></td>
                                        <td><input type='text' id="formulario<%=tot%>" name='formulario<%=tot%>' value="<%=dato[2]%>" ></td>
                                        <td> <select id="garantia<%=tot%>" name="gatantia<%=tot%>">
                                                <option value="">...</option>
                                                <%  for (int k = 0; k < garantias.size(); k++) {%>
                                                 <option value="<%= garantias.get(k).getValor_01()%>" <%= garantias.get(k).getValor_01().equals(dato[3])?"selected":""%>  ><%=garantias.get(k).getValor_02()%></option>
                                                <%  }%>
                                            </select>
                                        </td>
                                    </tr>
                                    <%tot++;
                                                                                }
                                        }%>
                                </table>
                                <input type='hidden' id="tot" name='tot' value="<%=tot%>">
                                <input type='hidden' id="id_solicitud" name='id_solicitud' value="<%=id_solicitud%>">
                            </td>
                        </tr>
                    </table>
                    <%}%>
                    <br>
                    <%if(tipito.equals("negocio")&&(tipoconv.equals("Microcredito")||tipoconv.equals("Multiservicio"))&&vista.equals("6")||vista.equals("11")){%>
                    <img name="imgradicar" src="<%=BASEURL%>/images/botones/radicar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick="radicar('<%=BASEURL %>','<%= CONTROLLER %>', '<%=numos%>', '<%=form%>'); ">
                    <%}%>
                    <%if (tipito.equals("negocio") && tipoconv.equals("Consumo") && (vista.equals("6")||vista.equals("18")||vista.equals("11"))) {%>
                    <img alt="" src = "<%=BASEURL%>/images/botones/concepto.gif" style="cursor:pointer;" onclick="concepto('<%=BASEURL%>','<%=numos%>', '<%=form%>','RAD','0', '<%=tipoconv%>');" name= "imgconcepto" id="imgconcepto"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                    
                    <%}%>
                    <img name="imgProcesar" src="<%=BASEURL%>/images/botones/aceptar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" if (fimp.archivo.value!='') { if ( validarArchivo(fimp.archivo.value) ) { if (fimp.categoria_archivo.value!='') fimp.submit(); else alert('Indique una categoría') } } else { alert ('Indique el archivo, para poder continuar'); } ">
                    &nbsp;
                    <img name="imgSalir" src="<%=BASEURL%>/images/botones/salir.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick="<%=tipito.equals("negocio") ? "regresar();" : "borrar();"%>">
                </form>      
                <% if (!Mensaje.equals("")) {%>

                <table border="2" align="center">
                    <tr>
                        <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                <tr>
                                    <td width="320" align="center" class="mensajes"><%=Mensaje%></td>
                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                    <td width="58">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>  
                <br><br>
                <% }%>      
            </center>
        </div>
        <%=datos[1]%>
    </body>
</html>