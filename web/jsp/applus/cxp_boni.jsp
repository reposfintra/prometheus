<%--        Author     : iamorales--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path); 
  String respuesta=request.getParameter("respuesta");  
%>
<html >
    <head  >
   
    <title>Cxps Bonificación</title>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
    <link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
    <link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    <script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
    <script src='<%= BASEURL %>/js/date-picker.js'></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>

    <body onresize="redimensionar()" onload = 'redimensionar();forma.cxc_boni.focus();'  >

        <div id="capaSuperior" style="position:absolute; width:100%; height:110px; z-index:0; left: 0px; top: 0px;">
         <jsp:include page="/toptsp.jsp?encabezado=CXPS BONIFICACION"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 109px; overflow: scroll;">

        
    <form name="forma" action='<%=CONTROLLER%>?estado=Cxps&accion=Applus&opcion=mostrarCxpsBoni' id="forma" method="post">
  <p>&nbsp;</p>
  <table width="500"  border="2" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td colspan="2" ><table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
              <tr>
                <td width="67%" class="subtitulo1">&nbsp;Datos de documento</td>
                <td width="33%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
              </tr>
          </table></td>
        </tr>
        <tr class="fila">
          <td align="center">Doc de Bonificacion</td>
          <td nowrap><input name="cxc_boni" type="text" class="textbox" id="cxc_boni"  value="<%= request.getParameter("cxc_boni")!=null ? request.getParameter("cxc_boni") : "" %>" maxlength="20"></td>
        </tr>
        
      </table></td>
    </tr>
  </table>
  <br>

  <br>

  <center>
    <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="c_aceptar" onClick="if (TCamposLlenos()){ forma.submit(); }" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
    <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
  </center>

  <%
        if (respuesta!=null && respuesta.equals("no")){

            %>
            <br><br>
            <p><table border="2" align="center">
              <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                  <tr>
                    <td width="150" align="center" class="mensajes"><%="Datos inválidos. Por favor revise los datos."%></td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="40">&nbsp;</td>
                  </tr>
                </table></td>
              </tr>
            </table>
            </p>
            <%
        }        
  %>
        
    </form>					
</div>
<%=datos[1]%>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</body>
</html>
