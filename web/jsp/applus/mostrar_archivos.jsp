<!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      25/01/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   
     - @(#)  
     - Description: Importacion Directa de un archivo
--%> 

<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import   ="com.tsp.operation.model.beans.Usuario;"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%  
    String Mensaje = (request.getParameter("Mensaje")!=null?request.getParameter("Mensaje"):""); 
    Usuario user = (Usuario) session.getAttribute("Usuario");
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
	String numos=request.getParameter("num_osx")!=null?request.getParameter("num_osx"):"0";
        String numero_solicitud=request.getParameter("numero_solicitud")!=null?request.getParameter("numero_solicitud"):"0";
        String tipoConv=request.getParameter("tipoConv")!=null?request.getParameter("tipoConv"):"0";
	//if (numos==null){numos="";}
   
        numos=numos+"-"+numero_solicitud+"-"+tipoConv;
 
	String archivolisto=request.getParameter("archivolisto");
	String secuenciax=request.getParameter("secuenciax");
        String nomarchivo ="";
        String categoria = request.getParameter("categoria") != null ? request.getParameter("categoria") : "";
	String tipito=request.getParameter("tipito");	
	if (tipito==null){tipito="";}
		
	String cerrar=request.getParameter("cerrar");
	if (cerrar!=null && (cerrar.equals("si"))){
	%>
		<script>
			window.close();
		</script>
	<%	
	}
%>    
%>
<html>
<head>
    <title>Consultar Archivos</title>
    <link   href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet">
    <script src ="<%= BASEURL %>/js/boton.js"></script>	
    <script>

      function validarExtension ( archivo ){
         var ext = /.EXE$/gi;
         if (ext.test(archivo.toUpperCase()) ){
             alert ('Extension no valida, para el archivo de importacion');
             return false;
         } return true;
      }  
	  
	  function consultarCodigo(secuenci){
		  if (secuenci!=null){
			//alert("cambio de consulta");
			//formulario.action="<%=CONTROLLER%>?estado=Negocios&accion=Applus&opcion=consultarCodigo";
			fimp.secuenciax.value=secuenci;
			fimp.submit();			
		  }
		}	
               function consultarNomarchivo(nomarchivo){
                    if (nomarchivo!=null){
	                fimp.nomarchivo.value=nomarchivo;
			fimp.submit();	
                    }
		}   
                
		function consultarNombres(){
			location.href = '<%=CONTROLLER%>?estado=Negocios&accion=Applus&opcion=nombres&numerit_os='+fimp.numerit_osx.value+'&tipito='+fimp.tipito.value;
			
		}
		function borrar()	{
			window.close();
			//window.open('<%= CONTROLLER %>?estado=Negocios&accion=Applus&opcion=borrar','na','status=yes,scrollbars=yes,width=400,height=150,resizable=no');			
		} 
        
        function consultarArchivos(element, idCategoria, tipoPersona) {            
            let table = document.getElementById("table-" + tipoPersona + "-" + idCategoria);
            
            if (table.innerHTML.trim() !== "") {
                element.innerHTML = "&plus;";
                table.innerHTML = "";
            } else {
                element.innerHTML = "&minus;";
                
                let xhr = new XMLHttpRequest();
                xhr.onreadystatechange = function () {
                    if (xhr.readyState === 4) {
                        if (xhr.status === 200) {
                            jsonResponse = JSON.parse(xhr.responseText);                                               

                            if (jsonResponse.length === 0 || jsonResponse === undefined) {
                                table.innerHTML = "<tr class='fila'><td class='enlace'>No existen archivos.</td></tr>";
                            } else {                            
                                for (let o of jsonResponse) {
                                    let tr = document.createElement("tr");
                                    let td = document.createElement("td");

                                    td.addEventListener("click", function () {
                                        let fd = new FormData();
                                        fd.append("nomarchivo", o.nombre);
                                        fd.append("numerit_osx", "<%=numos%>");
                                        fd.append("tipito", "<%=tipito%>");

                                        let xhr = new XMLHttpRequest();
                                        xhr.onreadystatechange = function () {
                                            if (xhr.readyState === 4) {
                                                if (xhr.status === 200) {
                                                    window.open("<%=BASEURL%>/images/multiservicios/<%=user.getLogin()%>/" + o.nombre);
                                                } else {
                                                    alert("No se pudo abrir el archivo");
                                                    console.log("estado: " + xhr.status + " - no se pudo abrir el archivo");
                                                }
                                            }
                                        };
                                        xhr.open("POST", "<%=CONTROLLER%>?estado=Negocios&accion=Applus&opcion=consultarArchivo", true);
                                        xhr.send(fd);
                                    });
                                    td.className = "enlace";
                                    td.innerHTML = o.nombre;

                                    tr.className = "fila";
                                    tr.appendChild(td);
                                    table.appendChild(tr);
                                }
                            }
                        } else {
                            alert("No se pudo cargar las categor�as");
                            console.log("estado: " + xhr.status + " - no se pudo cargar las categor�as");
                        }
                    }
                };
                xhr.open("POST", "<%=CONTROLLER%>?estado=Negocios&accion=Applus&opcion=consultarArchivos&numerit_os=" + fimp.numerit_osx.value + "&tipito=" + fimp.tipito.value
                        + "&categoria=" + idCategoria + "&tipoPersona=" + tipoPersona, true);
                xhr.send();
            }
        }
    </script>
    <style>
        .enlace {
            cursor: pointer;
            font-weight: 300;
            text-decoration: none;
        }
        .enlace:hover {
            color: red;
        }
        
        .fila td:first-child {
            position: relative;
        }
        
        .content-file {
            height: 15px;
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
        }
    </style>
</head>
<body onresize="redimensionar()" onLoad="window.resizeTo(700, 600);" >
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Importacion de Archivos "/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">


        <center>
	<br>
	<form action="<%= CONTROLLER %>?estado=Negocios&accion=Applus&opcion=consultarArchivo&num_osx=<%=numos%>&tipito=<%=tipito%>&nomarchivo=<%=nomarchivo%>" method="post" name="fimp" enctype='multipart/form-data' >
	<input type="hidden"  name="secuenciax" value="0">
        <input type="hidden"  name="nomarchivo" value="">
	<table width="650" border="2" align="center">
	<tr>
		<td align="center">
		
		<!-- cabecera de la seccion -->
		<table width="100%" >
			<tr>
			<td class="subtitulo1"  width="50%">Consultar Archivos</td>
			<td class="barratitulo" width="50%">
			  <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%>
			</td>
			</tr>
		</table>
		<!-- fin cabecera -->

		<table  width="100%" >
			<tr class="fila">
			    <td width='40%'>&nbsp;C�digo&nbsp;</td>
                            <td width='60%' colpsan="2">
                                <input type="text" value="<%=numos%>" name="numerit_osx"  maxlength="50"   <%if (!(numos.equals(""))){out.print("readonly");}%>  >
						   	<img   width='14'    title='Buscar'  src='<%=BASEURL%>/images/botones/iconos/modificar.gif'								
								onclick="consultarNombres();">
			    </td>
			</tr>	
			
			<tr class="fila">
			    <td width='40%'>&nbsp;Tipo&nbsp;</td>
                            <td width='60%' colpsan="2">
                                <input type="text" value="<%=tipito%>" name="tipito"  maxlength="20"  <%if (!(tipito.equals(""))){out.print("readonly");}%> >
						    </td>
			</tr>		
				
		
			<tr class="fila">
			    <td >&nbsp;Documentos titular</td>
				<td >&nbsp;</td>
			</tr>
			<%			
                        // validar si es un negocio con el listado anterior de archivos (Empieza la empanada)
                        if (model.negociosApplusService.esListadoViejo(numos.split("-")[0], tipito)) {
                            java.util.List nombresArchivos = model.negociosApplusService.getNombresArchivos(numos,user.getLogin(),tipito);
                            
                            for (int w=0;w<nombresArchivos.size();w++){
			%>
			<tr class="fila">
			    <td >&nbsp;</td>
                            <td ><a target="_blank"  onClick="consultarNomarchivo('<%=nombresArchivos.get(w)%>');" style="cursor:hand" ><strong><%=nombresArchivos.get(w)%></strong></a>
			<%
                                    if (archivolisto!=null && nombresArchivos.get(w).equals(archivolisto)){
			%>		
					&nbsp;&nbsp;&nbsp;<a  target="_blank" href="<%=BASEURL%>/images/multiservicios/<%=user.getLogin()%>/<%=archivolisto%>">ver</a>		
                        <%
                                    }
                        %>
				</td>
			</tr>
			<% 
                            }
                        } else {
                            java.util.List<java.util.Map<String,String>> categoriaArchivos = model.negociosApplusService.getCategoriaArchivos(numos.split("-")[0]);
                        
                            if (categoriaArchivos != null) {
                                    int w=0;
                                    while (w<categoriaArchivos.size() && categoriaArchivos.get(w).get("tipo_persona").equals("A")){
			%>
                        <tr class="fila">
			    <td>
                                <img class="content-file" alt="icono" src="<%=Integer.parseInt(categoriaArchivos.get(w).get("cantidad")) > 0 ? "/fintra/images/content.png" : "/fintra/images/empty.png"%>">
                            </td>
                            <td>
                                <strong><%=categoriaArchivos.get(w).get("nombre")%></strong>
                            </td>
                            <td>
                                    <a onclick="consultarArchivos(this, <%=categoriaArchivos.get(w).get("id")%>, '<%=categoriaArchivos.get(w).get("tipo_persona")%>');" class="enlace">&plus;</a>
                             </td>
                        </tr>
                        <tr class="fila">
                            <td></td>
                            <td>
                                    <table id="table-<%=categoriaArchivos.get(w).get("tipo_persona")%>-<%=categoriaArchivos.get(w).get("id")%>">                                    
                                </table>
                            </td>
                        </tr>
                        <%          w++;
                                }
                        %>
                        <tr class="fila">
			    <td >&nbsp;Documentos codeudor</td>
				<td >&nbsp;</td>
			</tr>
                        <%
                                    while (w<categoriaArchivos.size() && categoriaArchivos.get(w).get("tipo_persona").equals("B")){
                        %>
                                    <tr class="fila">
                                        <td>
                                            <img class="content-file" alt="icono" src="<%=Integer.parseInt(categoriaArchivos.get(w).get("cantidad")) > 0 ? "/fintra/images/content.png" : "/fintra/images/empty.png"%>">
                                        </td>
                                        <td>
                                            <strong><%=categoriaArchivos.get(w).get("nombre")%></strong>
                                        </td>
                                        <td>
                                            <a onclick="consultarArchivos(this, <%=categoriaArchivos.get(w).get("id")%>, '<%=categoriaArchivos.get(w).get("tipo_persona")%>');" class="enlace">&plus;</a>
                                         </td>
                                    </tr>
                                    <tr class="fila">
                                        <td></td>
                                        <td>
                                            <table id="table-<%=categoriaArchivos.get(w).get("tipo_persona")%>-<%=categoriaArchivos.get(w).get("id")%>">                                    
                                            </table>
                                        </td>
                                    </tr>
                                <%          w++;
                                }
                            }
                        }
                        %>
		 </table>
        </td>         
      </tr>
      </table>
      <br>
      
      <!--<img name="imgProcesar" src="<%//=BASEURL%>/images/botones/aceptar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" if (fimp.archivo.value!='') { if ( validarExtension(fimp.archivo.value) ) {fimp.submit();} } else { alert ('Indique el archivo, para poder continuar'); } ">-->
      &nbsp;
      <img name="imgSalir" src="<%=BASEURL%>/images/botones/salir.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:pointer;" onClick="borrar(); ">
      </form>
        


<% if (!Mensaje.equals("")) { %>
    
        <table border="2" align="center">
           <tr>
           <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                <tr>
                 <td width="320" align="center" class="mensajes"><%=Mensaje%></td>
                 <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                 <td width="58">&nbsp;</td>
                </tr>
              </table>
           </td>
           </tr>
        </table>  
        
        <br><br>
        
        
        
        
        
<% } %>      
      
</center>
</div>
<%=datos[1]%>
<script>
    
</script>
</body>
</html>
