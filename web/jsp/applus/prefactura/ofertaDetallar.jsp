<%-- 
    Document   : ofertaDetallar
    Created on : 26/02/2009, 08:01:09 PM
    Author     : Alvaro
--%>

<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<%@page import="com.tsp.operation.model.beans.*"%>

<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
  String hoy = Utility.getHoy("-");
  String  opcion       = request.getParameter("opcion");
  String  pagina       = "ofertaEspecifica.jsp";


  //System.out.println("inicio");
  
  if (opcion.equalsIgnoreCase("LISTADO") ) {
      pagina = "ofertaListar.jsp";
  }

  List listaOfertaEcaDetalle = model.applusService.getOfertaEcaDetalle();


  //System.out.println("inicio para obtener el objeto 0 de la lista");

  OfertaEcaDetalle  ofertaEcaDetalle = (OfertaEcaDetalle) listaOfertaEcaDetalle.get(0);

   //System.out.println("leido objeto 0");

  int nic = ofertaEcaDetalle.getNic();
  String nombre_cliente = ofertaEcaDetalle.getNombre_cliente();
  String tipo_identificacion=ofertaEcaDetalle.getTipo_identificacion();
  String num_os = ofertaEcaDetalle.getNum_os();
  int id_orden = ofertaEcaDetalle.getId_orden();
  int id_estado_negocio = ofertaEcaDetalle.getId_estado_negocio();

  String prefijo =   ofertaEcaDetalle.getNum_os().substring(0,2);
  if ((ofertaEcaDetalle.getNum_os()).indexOf("VA")!=-1){                  prefijo="VA";              }


  //System.out.println("leido item 0");

  double tot_total_prev1 = 0;
  double tot_total_prev1_sin_eca=0;//090716pm
  double tot_total_prev1_eca=0;//090716pm


  double tot_utilidad = 0;
  double tot_utilidad_sin_eca = 0;
  double tot_utilidad_eca = 0;



  double tot_bonificacion = 0;
  double tot_bonificacion_sin_eca = 0;
  double tot_bonificacion_eca = 0;


  double tot_iva_bonificacion_eca = 0;
  double tot_iva_bonificacion_sin_eca = 0;


  double tot_iva_total_prev1 = 0;
  double tot_iva_total_prev1_sin_eca = 0;//090716pm
  double tot_iva_total_prev1_eca = 0;//090716pm
  
  double tot_comision_applus = 0;
  double tot_comision_applus_sin_eca = 0;//090716pm
  double tot_comision_applus_eca = 0;//090716pm
  
  double tot_comision_factoring_fintra = 0;
  double tot_comision_factoring_fintra_sin_eca = 0;//090716pm
  double tot_comision_factoring_fintra_eca = 0;//090716pm
  
  double tot_iva_comision_factoring_fintra = 0;
  double tot_iva_comision_factoring_fintra_sin_eca = 0;//090716pm
  double tot_iva_comision_factoring_fintra_eca = 0;//090716pm
  
  double tot_iva_comision_applus = 0;  
  double tot_iva_comision_applus_sin_eca = 0;//090716pm
  double tot_iva_comision_applus_eca = 0;//090716pm
  
  double tot_comision_provintegral = 0;
  double tot_comision_provintegral_sin_eca = 0;//090716pm
  double tot_comision_provintegral_eca = 0;//090716pm

  double tot_iva_comision_provintegral = 0;
  double tot_iva_comision_provintegral_sin_eca = 0;//090716pm
  double tot_iva_comision_provintegral_eca = 0;//090716pm
  
  double tot_comision_eca = 0;
  double tot_comision_eca_sin_eca = 0;//090716pm
  double tot_comision_eca_eca = 0;//090716pm
  
  double tot_iva_comision_eca = 0;
  double tot_iva_comision_eca_sin_eca = 0;//090716pm
  double tot_iva_comision_eca_eca = 0;//090716pm
  
  double tot_comision_fintra = 0;
  double tot_comision_fintra_sin_eca = 0;//090716pm
  double tot_comision_fintra_eca = 0;//090716pm
      
  double tot_iva_comision_fintra = 0;
  double tot_iva_comision_fintra_sin_eca = 0;//090716pm
  double tot_iva_comision_fintra_eca = 0;//090716pm    

  double tot_financiacion_fintra = 0;
  double tot_financiacion_fintra_sin_eca = 0;//090716pm
  double tot_financiacion_fintra_eca = 0;//090716pm
      
  double tot_eca_oferta_calculada = 0;
  double tot_eca_oferta_calculada_sin_eca = 0;//090716pm
  double tot_eca_oferta_calculada_eca = 0;//090716pm
      
  double tot_eca_oferta = 0;
  double tot_eca_oferta_sin_eca = 0;//090716pm
  double tot_eca_oferta_eca = 0;//090716pm
  
  double tot_oferta = 0;
  double tot_oferta_sin_eca = 0;//090716pm
  double tot_oferta_eca = 0;//090716pm
  
  double tot_cuota_pago = 0;
  double tot_cuota_pago_sin_eca = 0;//090716pm
  double tot_cuota_pago_eca = 0;//090716pm
  
  double tot_financiacion = 0;
  double tot_financiacion_sin_eca = 0;//090716pm
  double tot_financiacion_eca = 0;//090716pm
  
  double tot_intereses = 0;
  double tot_intereses_sin_eca = 0;//090716pm
  double tot_intereses_eca = 0;//090716pm

  double tot_iva_oferta = 0;
  double tot_iva_oferta_sin_eca = 0;//090716pm
  double tot_iva_oferta_eca = 0;//090716pm
  
  double tot_iva_eca_oferta = 0;
  double tot_iva_eca_oferta_sin_eca = 0;//090716pm
  double tot_iva_eca_oferta_eca = 0;//090716pm
  
  double tot_oferta_mas_iva = 0;
  double tot_oferta_mas_iva_sin_eca = 0;//090716pm
  double tot_oferta_mas_iva_eca = 0;//090716pm
  
  double tot_eca_oferta_mas_iva = 0;
  double tot_eca_oferta_mas_iva_sin_eca = 0;//090716pm
  double tot_eca_oferta_mas_iva_eca = 0;//090716pm

  double tot_ec_financiacion_fintra = 0;
  double tot_ec_financiacion_fintra_sin_eca = 0;//090716pm
  double tot_ec_financiacion_fintra_eca = 0;//090716pm
  
  double tot_ec_cuota_pago = 0;
  double tot_ec_cuota_pago_sin_eca = 0;//090716pm
  double tot_ec_cuota_pago_eca = 0;//090716pm
  
  double tot_ec_total_financiacion = 0;
  double tot_ec_total_financiacion_sin_eca = 0;//090716pm
  double tot_ec_total_financiacion_eca = 0;//090716pm
  
  double tot_ec_intereses = 0;
  double tot_ec_intereses_sin_eca = 0;//090716pm
  double tot_ec_intereses_eca = 0;//090716pm

  double porcentaje_comision_applus = ofertaEcaDetalle.getPorcentaje_comision_applus()*100;
  double porcentaje_comision_provintegral = ofertaEcaDetalle.getPorcentaje_comision_provintegral()*100;
  double porcentaje_comision_fintra = ofertaEcaDetalle.getPorcentaje_comision_fintra()*100;
  double porcentaje_comision_eca = ofertaEcaDetalle.getPorcentaje_comision_eca()*100;
  double porcentaje_iva = ofertaEcaDetalle.getPorcentaje_iva()*100;
  double porcentaje_factoring_fintra = ofertaEcaDetalle.getPorcentaje_factoring_fintra()*100;
  double puntos_dtf = ofertaEcaDetalle.getPuntos_dtf();


%>


<% //System.out.println("HTML"); %>


<html >
    <head  >

        <title>Liquidacion de oferta</title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'>
        <link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    <script type='text/javascript' src="<%= BASEURL%>/js/validar.js"></script>
    <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>

	<script>
	   function send(theForm){
           theForm.submit();
	   }

	   function regresar(){
           window.close();
           window.open('<%= BASEURL%>/jsp/applus/prefactura/<%=pagina %>?msj=""  %>');
       }

	   function aplicarLiquidacion(){

           var elemento  = document.getElementById("id_orden");
           elemento      = elemento.firstChild.nodeValue;
           var id_orden  = elemento;
           var simbolo_variable     = document.getElementById("simbolo_variable").value;
           var observacion      = document.getElementById("observacion").value;
           var fechaFactura     = document.getElementById("fechaFactura").value;

           var parametro = "&opcion=ESPECIFICA";  <%-- Para permitir regresar a la pagina ofertaEspecifica.jsp --%>
           parametro = parametro + "&id_orden=" + id_orden + "&simbolo_variable=" + simbolo_variable + "&observacion=" + observacion + "&fechaFactura=" + fechaFactura+"&num_os=<%=num_os%>";//090715
           
           window.close();

           window.open('<%= CONTROLLER %>?estado=Prefactura&accion=Acceder&evento=APLICAR_LIQUIDACION'+parametro);
		   
	   }

	   function validarReliquidacion(){

           var elemento  = document.getElementById("id_orden");
           elemento      = elemento.firstChild.nodeValue;
           var id_orden  = elemento;

           var porcentaje_comision_applus     = document.getElementById("porcentaje_comision_applus").value;
           var porcentaje_factoring_fintra    = document.getElementById("porcentaje_factoring_fintra").value;
           var porcentaje_comision_provintegral     = document.getElementById("porcentaje_comision_provintegral").value;
           var porcentaje_comision_fintra     = document.getElementById("porcentaje_comision_fintra").value;
           var porcentaje_comision_eca     = document.getElementById("porcentaje_comision_eca").value;
           var porcentaje_iva     = document.getElementById("porcentaje_iva").value;
           var puntos_dtf     = document.getElementById("puntos_dtf").value;


           var parametro = "&opcion=ESPECIFICA";
           parametro = parametro + "&id_orden=" + id_orden ;
           
           parametro = parametro +  "&porcentaje_comision_applus=" + porcentaje_comision_applus +
                                    "&porcentaje_factoring_fintra=" + porcentaje_factoring_fintra +
                                    "&porcentaje_comision_provintegral=" + porcentaje_comision_provintegral +
                                    "&porcentaje_comision_fintra=" + porcentaje_comision_fintra +
                                    "&porcentaje_comision_eca=" + porcentaje_comision_eca +
                                    "&porcentaje_iva=" + porcentaje_iva +
                                    "&puntos_dtf=" + puntos_dtf ;
           window.close();

           window.open('<%= CONTROLLER %>?estado=Prefactura&accion=Acceder&evento=RELIQUIDAR_PORCENTAJE'+parametro);
	   }


	   function aplicarPorcentajeSimbolo(){

           var elemento  = document.getElementById("id_orden");
           elemento      = elemento.firstChild.nodeValue;
           var id_orden  = elemento;
           var simbolo_variable     = document.getElementById("simbolo_variable").value;
           var observacion      = document.getElementById("observacion").value;
           var fechaFactura     = document.getElementById("fechaFactura").value;

           var parametro = "&opcion=ESPECIFICA";  <%-- Para permitir regresar a la pagina ofertaEspecifica.jsp --%>
           parametro = parametro + "&id_orden=" + id_orden + "&simbolo_variable=" + simbolo_variable + "&observacion=" + observacion + "&fechaFactura=" + fechaFactura;

           window.close();

           window.open('<%= CONTROLLER %>?estado=Prefactura&accion=Acceder&evento=APLICAR_PORCENTAJE'+parametro);

	   }


       function validarNumero(evento){
         if((evento.keyCode<48)||((evento.keyCode>57))){
          evento.returnValue=false;
          return false;
         }
         else{
          return true;
         }
       }

       function validarPunto(evento){
         if((evento.keyCode==46)){
          return true;
         }
         else{
          evento.returnValue=false;
          return false;
         }
       }


       function validarNumeroPunto(evento){
         if(  ((evento.keyCode<48)||(evento.keyCode>57))  &&  (evento.keyCode!=46)  ) {
          evento.returnValue=false;
          return false;
         }else{
          return true;
         }
       }


      function validar(campo,entero,decimal){
       evento=event;
       var vector=campo.value.split('.');
       if (vector.length==1){
           if(vector[0].length<entero){
              return (validarNumeroPunto(evento));
           }
           else{
               if(vector[0].length==entero){
                  return (validarPunto(evento));
               }else{
                  evento.returnValue=false;
                  return false;
               }
           }
       }
       else{
           if(vector.length<3){
               if(vector[1].length<decimal){
                   return (validarNumero(evento));
               }
               else{
                   evento.returnValue=false;
                   return false;
               }
           }
       }
       return true;
      }


	</script>
    





    <body onresize="redimensionar()" onload = 'redimensionar()'  >

        <div id="capaSuperior" style="position:absolute; width:100%; height:110px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado= DETALLE DE OFERTA"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:488px; z-index:0; left: 0px; top: 109px; overflow: scroll;">



      <form name='formulario' method='post' id="formulario" action="" >
        <p>&nbsp;</p>

        <table width="480"  border="1"  align="center">
              <tr class='fila'>
              <td width="80"  align="left"> ID OFERTA </td>
                <td width="80"  align="right" name="id_orden" id="id_orden" >  <%= id_orden %> </td>
                <td width="80"  align="left"> NUM OS  </td>
                <td width="80"  align="right" > <%= num_os %> </td>
                <td width="80"  align="left"> NIC </td>
                <td width="80"  align="right" > <%= nic %> </td>
              </tr>
              <tr class='fila'>
                <td   align="left"> CLIENTE </td>
                <td COLSPAN="5" align="left" > <%= nombre_cliente %>&nbsp;(&nbsp;<%=tipo_identificacion%> &nbsp;) </td>

              </tr>
        </table>


        

        <table width="2620"  border="1"  align="center">

              <TR class="tblTitulo1"  align="center">
                      <TH  width="80" align="center"> ID ACCION</TH>
                      <TH  width="60" align="center"> TOTAL PREV1</TH>
                      <TH  width="60" align="center"> UTILIDAD</TH>
                      <TH  width="60" align="center"> BONIFICACION</TH>
                      <TH  width="60" align="center"> IVA BONIFICACION</TH>
                      <TH  width="80" align="center"> IVA TOTAL PREV1 </TH>
                      <TH  width="90" align="center"> COMISION APPLUS</TH>
                      <TH  width="60" align="center"> COMISION FACTORING GD</TH>
                      <TH  width="80" align="center"> COMISION PROVINTEGRAL </TH>
                      <TH  width="80" align="center"> COMISION FINTRA </TH>
                      <TH  width="80" align="center"> IVA COMISION APPLUS </TH>
                      <TH  width="80" align="center"> IVA COMISION FACTORING FINTRA </TH>
                      <TH  width="80" align="center"> IVA COMISION PROVINTEGRAL </TH>
                      <TH  width="80" align="center"> IVA COMISION FINTRA </TH>
                      <TH  width="80" align="center"> COMISION ECA</TH>
                      <TH  width="80" align="center"> IVA COMISION ECA </TH>
					  
		      <TH  width="80" align="center"> OFERTA </TH>
		      <TH  width="80" align="center"> IVA OFERTA </TH>
		      <TH  width="80" align="center"> VALOR VENTA</TH>
					  
                      <TH  width="80" align="center"> ECA OFERTA </TH>
                      <TH  width="80" align="center"> IVA ECA OFERTA </TH>
                      <TH  width="80" align="center"> ECA VALOR VENTA </TH>


                      <TH  width="80" align="center"> OFERTA AP+</TH>
                      <TH  width="80" align="center"> IVA OFERTA AP+       </TH>
                      <TH  width="80" align="center"> VALOR VENTA AP+ </TH>

                      <TH  width="80" align="center"> ECA OFERTA AP+ </TH>
                      <TH  width="80" align="center"> IVA ECA OFERTA AP+ </TH>
                      <TH  width="80" align="center"> ECA VALOR VENTA AP+</TH>

                      <TH  width="80" align="center"> CUOTAS REALES </TH>
                      <TH  width="80" align="center"> ESQUEMA </TH>
                      <TH  width="80" align="center"> DTF SEMANA </TH>
                      <TH  width="80" align="center"> PUNTOS DTF</TH>

                      <TH  width="80" align="center"> FECHA FINANCIACION </TH>


                      <TH  width="80" align="center"> CUOTA PAGO </TH>
                      <TH  width="80" align="center"> TOTAL A FINANCIAR </TH>
                      <TH  width="80" align="center"> INTERESES </TH>

                      <TH  width="80" align="center"> VALOR A FINANCIAR AP+ </TH>
                      <TH  width="80" align="center"> CUOTA PAGO AP+ </TH>
                      <TH  width="80" align="center"> TOTAL FINANCIADO AP+ </TH>
                      <TH  width="80" align="center"> INTERESES AP+ </TH>

					  <TH  width="80" align="center"> CONTRATISTA </TH><!--090716pm-->


              </TR>


          <%
            int i=0;
            Iterator it = listaOfertaEcaDetalle.iterator();
            while (it.hasNext()) {

              i++;

              ofertaEcaDetalle = (OfertaEcaDetalle)it.next();

              id_orden = ofertaEcaDetalle.getId_orden();
              String id_accion = ofertaEcaDetalle.getId_accion();
              double total_prev1_mat = ofertaEcaDetalle.getTotal_prev1_mat();
              double total_prev1_mob = ofertaEcaDetalle.getTotal_prev1_mob();
              double total_prev1_otr = ofertaEcaDetalle.getTotal_prev1_otr();
              double administracion  = ofertaEcaDetalle.getAdministracion();
              double imprevisto      = ofertaEcaDetalle.getImprevisto();
              double utilidad        = ofertaEcaDetalle.getUtilidad();
              double bonificacion    = ofertaEcaDetalle.getBonificacion();
              double iva_bonificacion= ofertaEcaDetalle.getIvaBonificacion();



              int    cuotas_reales   = ofertaEcaDetalle.getCuotas_reales();
              String tipo_dtf        = ofertaEcaDetalle.getTipo_dtf();
              double dtf_semana  = ofertaEcaDetalle.getDtf_semana();
              double porcentaje_factoring  = ofertaEcaDetalle.getPorcentaje_factoring();
              String fecha_financiacion    = ofertaEcaDetalle.getFecha_financiacion();
              double cuota_pago            = ofertaEcaDetalle.getCuota_pago();
              puntos_dtf            = ofertaEcaDetalle.getPuntos_dtf();

              double total_prev1           = ofertaEcaDetalle.getTotal_prev1();
              double iva_total_prev1       = ofertaEcaDetalle.getIva_total_prev1();
              double comision_applus       = ofertaEcaDetalle.getComision_applus();
              double comision_factoring_fintra = ofertaEcaDetalle.getComision_factoring_fintra();
              double iva_comision_factoring_fintra = ofertaEcaDetalle.getIva_comision_factoring_fintra();

              double comision_provintegral = ofertaEcaDetalle.getComision_provintegral();
              double iva_comision_applus   = ofertaEcaDetalle.getIva_comision_applus();
              double iva_comision_provintegral = ofertaEcaDetalle.getIva_comision_provintegral();
              double comision_eca          = ofertaEcaDetalle.getComision_eca();
              double iva_comision_eca      = ofertaEcaDetalle.getIva_comision_eca();

              double comision_fintra       = ofertaEcaDetalle.getComision_fintra();
              double iva_comision_fintra   = ofertaEcaDetalle.getIva_comision_fintra();

              double financiacion_fintra   = ofertaEcaDetalle.getFinanciacion_fintra();


              double eca_oferta_calculada  = ofertaEcaDetalle.getEca_oferta_calculada();
              double eca_oferta  = ofertaEcaDetalle.getEca_oferta();
              double oferta      = ofertaEcaDetalle.getOferta();



              double financiacion = ofertaEcaDetalle.getTotal_financiacion();


              double intereses    = financiacion - financiacion_fintra;



              double iva_oferta =  ofertaEcaDetalle.getIva_oferta();
              double iva_eca_oferta =  ofertaEcaDetalle.getIva_eca_oferta();
              double oferta_mas_iva =  ofertaEcaDetalle.getOferta_mas_iva();
              double eca_oferta_mas_iva =  ofertaEcaDetalle.getEca_oferta_mas_iva();

              double ec_financiacion_fintra = ofertaEcaDetalle.getEc_financiacion_fintra();
              double ec_cuota_pago = ofertaEcaDetalle.getEc_cuota_pago();
              double ec_total_financiacion = ofertaEcaDetalle.getEc_total_financiacion();
              double ec_intereses = ec_total_financiacion - ec_financiacion_fintra;

   			  if (ofertaEcaDetalle.getId_contratista().equals("CC027")){//090716pm
				  tot_total_prev1_eca = tot_total_prev1_eca + total_prev1;

                                  tot_utilidad_eca = tot_utilidad_eca + utilidad;
                                  tot_bonificacion_eca = tot_bonificacion_eca + bonificacion;

                                  tot_iva_bonificacion_eca = tot_iva_bonificacion_eca + iva_bonificacion;



				  tot_iva_total_prev1_eca = tot_iva_total_prev1_eca + iva_total_prev1;
				  tot_comision_applus_eca = tot_comision_applus_eca + comision_applus;
				  tot_iva_comision_applus_eca = tot_iva_comision_applus_eca + iva_comision_applus;
	
				  tot_comision_factoring_fintra_eca = tot_comision_factoring_fintra_eca + comision_factoring_fintra;
				  tot_iva_comision_factoring_fintra_eca = tot_iva_comision_factoring_fintra_eca + iva_comision_factoring_fintra;
	
				  tot_comision_provintegral_eca     = tot_comision_provintegral_eca + comision_provintegral;
				  tot_iva_comision_provintegral_eca = tot_iva_comision_provintegral_eca + iva_comision_provintegral;
				  tot_comision_eca_eca              = tot_comision_eca_eca + comision_eca;
				  tot_iva_comision_eca_eca          = tot_iva_comision_eca_eca + iva_comision_eca;
				  tot_comision_fintra_eca           = tot_comision_fintra_eca +  comision_fintra;
				  tot_iva_comision_fintra_eca       = tot_iva_comision_fintra_eca + iva_comision_fintra;
				  tot_financiacion_fintra_eca       = tot_financiacion_fintra_eca + financiacion_fintra;
				  tot_eca_oferta_calculada_eca      = tot_eca_oferta_calculada_eca + eca_oferta_calculada;
				  tot_eca_oferta_eca                = tot_eca_oferta_eca + eca_oferta;
				  tot_oferta_eca                    = tot_oferta_eca + oferta;
				  tot_cuota_pago_eca                = tot_cuota_pago_eca + cuota_pago;
				  tot_financiacion_eca              = tot_financiacion_eca + financiacion;
				  tot_intereses_eca                 = tot_financiacion_eca - tot_financiacion_fintra_eca;
	
				  tot_iva_oferta_eca                = tot_iva_oferta_eca + iva_oferta;
				  tot_iva_eca_oferta_eca            = tot_iva_eca_oferta_eca + iva_eca_oferta;
				  tot_oferta_mas_iva_eca            = tot_oferta_mas_iva_eca + oferta_mas_iva;
				  tot_eca_oferta_mas_iva_eca        = tot_eca_oferta_mas_iva_eca + eca_oferta_mas_iva;
	
				  tot_ec_financiacion_fintra_eca = tot_ec_financiacion_fintra_eca + ec_financiacion_fintra;
				  tot_ec_cuota_pago_eca = tot_ec_cuota_pago_eca + ec_cuota_pago;
				  tot_ec_total_financiacion_eca = tot_ec_total_financiacion_eca + ec_total_financiacion;
				  tot_ec_intereses_eca = tot_ec_intereses_eca + ec_intereses;

			  }else{
			  	  tot_total_prev1_sin_eca = tot_total_prev1_sin_eca + total_prev1;
                                  tot_utilidad_sin_eca = tot_utilidad_sin_eca + utilidad;
                                  tot_bonificacion_sin_eca = tot_bonificacion_sin_eca + bonificacion;
                                  tot_iva_bonificacion_sin_eca = tot_iva_bonificacion_sin_eca + iva_bonificacion;


				  tot_iva_total_prev1_sin_eca = tot_iva_total_prev1_sin_eca + iva_total_prev1;
				  tot_comision_applus_sin_eca = tot_comision_applus_sin_eca + comision_applus;
				  tot_iva_comision_applus_sin_eca = tot_iva_comision_applus_sin_eca + iva_comision_applus;
	
				  tot_comision_factoring_fintra_sin_eca = tot_comision_factoring_fintra_sin_eca + comision_factoring_fintra;
				  tot_iva_comision_factoring_fintra_sin_eca = tot_iva_comision_factoring_fintra_sin_eca + iva_comision_factoring_fintra;
	
				  tot_comision_provintegral_sin_eca     = tot_comision_provintegral_sin_eca + comision_provintegral;
				  tot_iva_comision_provintegral_sin_eca = tot_iva_comision_provintegral_sin_eca + iva_comision_provintegral;
				  tot_comision_eca_sin_eca              = tot_comision_eca_sin_eca + comision_eca;
				  tot_iva_comision_eca_sin_eca          = tot_iva_comision_eca_sin_eca + iva_comision_eca;
				  tot_comision_fintra_sin_eca           = tot_comision_fintra_sin_eca +  comision_fintra;
				  tot_iva_comision_fintra_sin_eca       = tot_iva_comision_fintra_sin_eca + iva_comision_fintra;
				  tot_financiacion_fintra_sin_eca       = tot_financiacion_fintra_sin_eca + financiacion_fintra;
				  tot_eca_oferta_calculada_sin_eca      = tot_eca_oferta_calculada_sin_eca + eca_oferta_calculada;
				  tot_eca_oferta_sin_eca                = tot_eca_oferta_sin_eca + eca_oferta;
				  tot_oferta_sin_eca                    = tot_oferta_sin_eca + oferta;
				  tot_cuota_pago_sin_eca                = tot_cuota_pago_sin_eca + cuota_pago;
				  tot_financiacion_sin_eca              = tot_financiacion_sin_eca + financiacion;
				  tot_intereses_sin_eca                 = tot_financiacion_sin_eca - tot_financiacion_fintra_sin_eca;
	
				  tot_iva_oferta_sin_eca                = tot_iva_oferta_sin_eca + iva_oferta;
				  tot_iva_eca_oferta_sin_eca            = tot_iva_eca_oferta_sin_eca + iva_eca_oferta;
				  tot_oferta_mas_iva_sin_eca            = tot_oferta_mas_iva_sin_eca + oferta_mas_iva;
				  tot_eca_oferta_mas_iva_sin_eca        = tot_eca_oferta_mas_iva_sin_eca + eca_oferta_mas_iva;
	
				  tot_ec_financiacion_fintra_sin_eca = tot_ec_financiacion_fintra_sin_eca + ec_financiacion_fintra;
				  tot_ec_cuota_pago_sin_eca = tot_ec_cuota_pago_sin_eca + ec_cuota_pago;
				  tot_ec_total_financiacion_sin_eca = tot_ec_total_financiacion_sin_eca + ec_total_financiacion;
				  tot_ec_intereses_sin_eca = tot_ec_intereses_sin_eca + ec_intereses;

			  }	
			
              tot_total_prev1 = tot_total_prev1 + total_prev1;

              tot_utilidad = tot_utilidad + utilidad;
              tot_bonificacion = tot_bonificacion + bonificacion;

              tot_iva_total_prev1 = tot_iva_total_prev1 + iva_total_prev1;
              tot_comision_applus = tot_comision_applus + comision_applus;
              tot_iva_comision_applus = tot_iva_comision_applus + iva_comision_applus;

              tot_comision_factoring_fintra = tot_comision_factoring_fintra + comision_factoring_fintra;
              tot_iva_comision_factoring_fintra = tot_iva_comision_factoring_fintra + iva_comision_factoring_fintra;

              tot_comision_provintegral     = tot_comision_provintegral + comision_provintegral;
              tot_iva_comision_provintegral = tot_iva_comision_provintegral + iva_comision_provintegral;
              tot_comision_eca              = tot_comision_eca + comision_eca;
              tot_iva_comision_eca          = tot_iva_comision_eca + iva_comision_eca;
              tot_comision_fintra           = tot_comision_fintra +  comision_fintra;
              tot_iva_comision_fintra       = tot_iva_comision_fintra + iva_comision_fintra;
              tot_financiacion_fintra       = tot_financiacion_fintra + financiacion_fintra;
              tot_eca_oferta_calculada      = tot_eca_oferta_calculada + eca_oferta_calculada;
              tot_eca_oferta                = tot_eca_oferta + eca_oferta;
              tot_oferta                    = tot_oferta + oferta;
              tot_cuota_pago                = tot_cuota_pago + cuota_pago;
              tot_financiacion              = tot_financiacion + financiacion;
              tot_intereses                 = tot_financiacion - tot_financiacion_fintra;

              tot_iva_oferta                = tot_iva_oferta + iva_oferta;
              tot_iva_eca_oferta            = tot_iva_eca_oferta + iva_eca_oferta;
              tot_oferta_mas_iva            = tot_oferta_mas_iva + oferta_mas_iva;
              tot_eca_oferta_mas_iva        = tot_eca_oferta_mas_iva + eca_oferta_mas_iva;

              tot_ec_financiacion_fintra = tot_ec_financiacion_fintra + ec_financiacion_fintra;
              tot_ec_cuota_pago = tot_ec_cuota_pago + ec_cuota_pago;
              tot_ec_total_financiacion = tot_ec_total_financiacion + ec_total_financiacion;
              tot_ec_intereses = tot_ec_intereses + ec_intereses;




          %>
              <tr <%if ( ofertaEcaDetalle.getId_contratista().equals("CC027"))
                      {%>class='<%= "filaamarilla1"%>'<%}
                    else{%>class='<%= (i%2==0?"filagris1":"filaazul1") %>'<%}%>><!--090716pm-->

                <td width="80"  align="center"> <%= id_accion %>  </td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(total_prev1) %> </td>

                <td width="80"  align="right" > <%= Util.FormatoMiles(utilidad) %> </td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(bonificacion) %> </td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(iva_bonificacion) %> </td>

                <td width="80"  align="right" > <%= Util.FormatoMiles(iva_total_prev1) %> </td>
                <td width="90"  align="right" > <%= Util.FormatoMiles(comision_applus) %> </td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(comision_factoring_fintra) %> </td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(comision_provintegral ) %></td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(comision_fintra ) %></td>
                <td width="60"  align="right" > <%= Util.FormatoMiles(iva_comision_applus) %></td>
                <td width="60"  align="right" > <%= Util.FormatoMiles(iva_comision_factoring_fintra) %></td>

                <td width="60"  align="right" > <%= Util.FormatoMiles(iva_comision_provintegral) %></td>
                <td width="60"  align="right" > <%= Util.FormatoMiles(iva_comision_fintra) %></td>
                <td width="60"  align="right" > <%= Util.FormatoMiles(comision_eca) %></td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(iva_comision_eca ) %></td>

				<%
				double porcxx=0;
				if (ofertaEcaDetalle.getEsquema_comision().equals("MODELO_ANTERIOR")){
					porcxx=1.11;
				}else{
					porcxx=1.13;
				}
				%>
								
				<td width="80"  align="right" > <%= Util.FormatoMiles(total_prev1*porcxx) %></td>
				<td width="80"  align="right" > <%= Util.FormatoMiles(total_prev1*porcxx*0.16) %></td>
				<td width="80"  align="right" > <%= Util.FormatoMiles(total_prev1*porcxx*1.16) %></td>

                <td width="80"  align="right" > <%= Util.FormatoMiles(eca_oferta_calculada) %></td>
				
                <td width="80"  align="right" > <%= Util.FormatoMiles(eca_oferta_calculada*0.16) %></td>
	
			
                <td width="80"  align="right" > <%= Util.FormatoMiles(eca_oferta_calculada*1.16) %></td>
				
                <td width="80"  align="right" > <%= Util.FormatoMiles(oferta) %></td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(iva_oferta) %></td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(oferta_mas_iva) %></td>
				
                <td width="80"  align="right" > <%= Util.FormatoMiles(eca_oferta) %></td>

                <td width="80"  align="right" > <%= Util.FormatoMiles(iva_eca_oferta) %></td>

	        <td width="80"  align="right" > <%= Util.FormatoMiles(eca_oferta_mas_iva) %></td>			


                <td width="80"  align="center" ><%= cuotas_reales %> </td>
                <td width="80"  align="center" ><%= tipo_dtf %> </td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(dtf_semana) %> </td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(puntos_dtf ) %></td>
                <td width="80"  align="right" > <%= fecha_financiacion %> </td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(cuota_pago) %> </td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(financiacion) %> </td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(intereses) %> </td>

                <td width="80"  align="right" > <%= Util.FormatoMiles(ec_financiacion_fintra) %> </td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(ec_cuota_pago) %> </td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(ec_total_financiacion) %> </td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(ec_intereses) %> </td>

		<td width="80"  align="left" > &nbsp;<%= ofertaEcaDetalle.getNombre_contratista()%></td><!--090716pm-->
				
              </tr>

           <%
                }
                i++;
           %>
                <!--
				
                <tr class='filaTotal'>
                <td width="80"  align="center">  TOTALES </td>
                <td width="80"  align="right" > <%//= Util.FormatoMiles(tot_total_prev1) %> </td>
                <td width="80"  align="right" > <%//= Util.FormatoMiles(tot_iva_total_prev1) %> </td>
                <td width="80"  align="right" > <%//= Util.FormatoMiles(tot_comision_applus) %> </td>
                <td width="80"  align="right" > <%//= Util.FormatoMiles(tot_comision_factoring_fintra) %> </td>

                <td width="80"  align="right" > <%//= Util.FormatoMiles(tot_comision_provintegral ) %></td>
                <td width="80"  align="right" > <%//= Util.FormatoMiles(tot_comision_fintra ) %></td>
                <td width="60"  align="right" > <%//= Util.FormatoMiles(tot_iva_comision_applus) %></td>
                <td width="60"  align="right" > <%//= Util.FormatoMiles(tot_iva_comision_factoring_fintra) %></td>
                <td width="60"  align="right" > <%//= Util.FormatoMiles(tot_iva_comision_provintegral) %></td>
                <td width="60"  align="right" > <%//= Util.FormatoMiles(tot_iva_comision_fintra) %></td>
                <td width="60"  align="right" > <%//= Util.FormatoMiles(tot_comision_eca) %></td>
                <td width="80"  align="right" > <%//= Util.FormatoMiles(tot_iva_comision_eca ) %></td>
				<td width="80"  align="right" > </td>
				<td width="80"  align="right" > </td>
				<td width="80"  align="right" > </td>
                <td width="80"  align="right" > <%//= Util.FormatoMiles(tot_eca_oferta_calculada) %></td>
				<td width="80"  align="right" > </td>
				
                <td width="80"  align="right" > <%//= Util.FormatoMiles(tot_financiacion_fintra ) %></td>
                <td width="80"  align="right" > <%//= Util.FormatoMiles(tot_oferta) %></td>
                <td width="80"  align="right" > <%//= Util.FormatoMiles(tot_iva_oferta) %></td>
                <td width="80"  align="right" > <%//= Util.FormatoMiles(tot_oferta_mas_iva) %></td>
				
				
				<td width="80"  align="right" > <%//= Util.FormatoMiles(tot_eca_oferta) %></td>

                <td width="80"  align="right" > <%//= Util.FormatoMiles(tot_iva_eca_oferta) %></td>
                <td width="80"  align="right" > <%//= Util.FormatoMiles(tot_eca_oferta_mas_iva) %></td>

				
				
                <td width="80"  align="right" >   </td>
                <td width="80"  align="center" >  </td>
                <td width="80"  align="center" >  </td>
                <td width="80"  align="right" >   </td>
                <td width="80"  align="right" > </td>

                <td width="80"  align="right" > <%//= Util.FormatoMiles(tot_cuota_pago) %> </td>
                <td width="80"  align="right" > <%//= Util.FormatoMiles(Util.redondear2((tot_financiacion/10),0)*10) %></td><!-//090516ojo->
                <td width="80"  align="right" > <%//= Util.FormatoMiles(tot_intereses) %></td>

                <td width="80"  align="right" > <%//= Util.FormatoMiles(tot_ec_financiacion_fintra) %></td>
                <td width="80"  align="right" > <%//= Util.FormatoMiles(tot_ec_cuota_pago) %> </td>
                <td width="80"  align="right" > <%//= Util.FormatoMiles(Util.redondear2((tot_ec_total_financiacion/10),0)*10) %></td><!-//090516ojo->
                <td width="80"  align="right" > <%//= Util.FormatoMiles(tot_ec_intereses) %></td>
				



                </tr>
				-->
		<tr class='filaTotalAmarilla'>
		
                <td width="80"  align="center">  TOT_ECA </td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_total_prev1_eca) %> </td>

                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_utilidad_eca) %> </td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_bonificacion_eca) %> </td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_iva_bonificacion_eca) %> </td>



                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_iva_total_prev1_eca) %> </td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_comision_applus_eca) %> </td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_comision_factoring_fintra_eca) %> </td>

                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_comision_provintegral_eca ) %></td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_comision_fintra_eca ) %></td>
                <td width="60"  align="right" > <%= Util.FormatoMiles(tot_iva_comision_applus_eca) %></td>
                <td width="60"  align="right" > <%= Util.FormatoMiles(tot_iva_comision_factoring_fintra_eca) %></td>
                <td width="60"  align="right" > <%= Util.FormatoMiles(tot_iva_comision_provintegral_eca) %></td>
                <td width="60"  align="right" > <%= Util.FormatoMiles(tot_iva_comision_fintra_eca) %></td>
                <td width="60"  align="right" > <%= Util.FormatoMiles(tot_comision_eca_eca) %></td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_iva_comision_eca_eca ) %></td>
				<td width="80"  align="right" > </td>
				<td width="80"  align="right" > </td>
				<td width="80"  align="right" > </td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_eca_oferta_calculada_eca) %></td>
				<td width="80"  align="right" > </td>
				
                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_financiacion_fintra_eca ) %></td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_oferta_eca) %></td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_iva_oferta_eca) %></td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_oferta_mas_iva_eca) %></td>
				
				
				<td width="80"  align="right" > <%= Util.FormatoMiles(tot_eca_oferta_eca) %></td>

                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_iva_eca_oferta_eca) %></td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_eca_oferta_mas_iva_eca) %></td>

				
				
                <td width="80"  align="right" >   </td>
                <td width="80"  align="center" >  </td>
                <td width="80"  align="center" >  </td>
                <td width="80"  align="right" >   </td>
                <td width="80"  align="right" > </td>

                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_cuota_pago_eca) %> </td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(Util.redondear2((tot_financiacion_eca/10),0)*10) %></td><!--//090516ojo-->
                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_intereses_eca) %></td>

                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_ec_financiacion_fintra_eca) %></td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_ec_cuota_pago_eca) %> </td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(Util.redondear2((tot_ec_total_financiacion_eca/10),0)*10) %></td><!--//090516ojo-->
                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_ec_intereses_eca) %></td>
			

				<TH  width="80" align="center"> &nbsp; </TH><!--090716pm-->

                </tr>

		<tr class='filaTotal'><!--090716pm-->
                <td width="80"  align="center">  TOT_SIN_ECA </td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_total_prev1_sin_eca) %> </td>

                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_utilidad_eca) %> </td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_bonificacion_eca) %> </td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_iva_bonificacion_sin_eca) %> </td>




                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_iva_total_prev1_sin_eca) %> </td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_comision_applus_sin_eca) %> </td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_comision_factoring_fintra_sin_eca) %> </td>

                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_comision_provintegral_sin_eca ) %></td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_comision_fintra_sin_eca ) %></td>
                <td width="60"  align="right" > <%= Util.FormatoMiles(tot_iva_comision_applus_sin_eca) %></td>
                <td width="60"  align="right" > <%= Util.FormatoMiles(tot_iva_comision_factoring_fintra_sin_eca) %></td>
                <td width="60"  align="right" > <%= Util.FormatoMiles(tot_iva_comision_provintegral_sin_eca) %></td>
                <td width="60"  align="right" > <%= Util.FormatoMiles(tot_iva_comision_fintra_sin_eca) %></td>
                <td width="60"  align="right" > <%= Util.FormatoMiles(tot_comision_eca_sin_eca) %></td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_iva_comision_eca_sin_eca ) %></td>
				<td width="80"  align="right" > </td>
				<td width="80"  align="right" > </td>
				<td width="80"  align="right" > </td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_eca_oferta_calculada_sin_eca) %></td>
				<td width="80"  align="right" > </td>
				
                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_financiacion_fintra_sin_eca ) %></td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_oferta_sin_eca) %></td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_iva_oferta_sin_eca) %></td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_oferta_mas_iva_sin_eca) %></td>
				
				
				<td width="80"  align="right" > <%= Util.FormatoMiles(tot_eca_oferta_sin_eca) %></td>

                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_iva_eca_oferta_sin_eca) %></td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_eca_oferta_mas_iva_sin_eca) %></td>

				
				
                <td width="80"  align="right" >   </td>
                <td width="80"  align="center" >  </td>
                <td width="80"  align="center" >  </td>
                <td width="80"  align="right" >   </td>
                <td width="80"  align="right" > </td>

                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_cuota_pago_sin_eca) %> </td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(Util.redondear2((tot_financiacion_sin_eca/10),0)*10) %></td><!--//090516ojo-->
                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_intereses_sin_eca) %></td>

                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_ec_financiacion_fintra_sin_eca) %></td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_ec_cuota_pago_sin_eca) %> </td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(Util.redondear2((tot_ec_total_financiacion_sin_eca/10),0)*10) %></td><!--//090516ojo-->
                <td width="80"  align="right" > <%= Util.FormatoMiles(tot_ec_intereses_sin_eca) %></td>
			
				<TH  width="80" align="center"> &nbsp; </TH><!--090716pm-->


                </tr>
        </table>

        <p>&nbsp;</p>


<% if ( ( (id_estado_negocio == 8)||(id_estado_negocio == 9) )   && ( !prefijo.equalsIgnoreCase("VA")) ) {  %>
  <table width="750"  border="2" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="100%" class="tablaInferior">
          
        <tr>
            <td colspan="2" >
              <table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
                  <tr>
                    <td width="67%" class="subtitulo1">&nbsp;INGRESO DE SIMBOLO VARIABLE</td>
                    <td width="33%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
                  </tr>
             </table>
           </td>
        </tr>
		<%
  	    String[] ex_prefacturaeca=model.negociosApplusService.getExPrefacturaEca(""+id_orden);		
		String posible_sv=""+nic,posible_observacion="",posible_fechaFactura=hoy;
		if (ex_prefacturaeca[0].equals("S")){
			posible_sv=ex_prefacturaeca[1];
			posible_observacion=ex_prefacturaeca[2];
			posible_fechaFactura=ex_prefacturaeca[3].substring(0,10);
		}
		%>
        <tr class="fila">
          <td align="center">Simbolo variable</td>
          <td nowrap><input name="simbolo_variable" type="text" class="textbox" id="simbolo_variable"   value="<%= posible_sv %>"  </td>
        </tr>
        <tr class="fila">
          <td align="center">Observacion</td>
          <td><textarea name="observacion" rows="4" id="observacion"  ><%=posible_observacion%></textarea>  
			  
		  </td>
        </tr>
        <tr class="fila">
          <td align="center"> Fecha pago oportuno</td>
          <td>
              <!-- Fecha -->
              <input  name='fechaFactura' id='fechaFactura' size="20" readonly="true" class="textbox" value='<%=posible_fechaFactura%>' style='width:50%'>
              <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(fechaFactura);return false;" hidefocus>
                  <img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt="">
              </a>
          </td>
        </tr>

      </table></td>
    </tr>
  </table>

  <% } %>





<% if ( (id_estado_negocio == 8) && ( !prefijo.equalsIgnoreCase("VA")) ) {  %>

  <table width="750"  border="2" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td colspan="4" >
             <table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
              <tr>
                <td width="67%" class="subtitulo1">&nbsp;PORCENTAJES PARA LIQUIDAR COMISIONES</td>
                <td width="33%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
              </tr>
            </table></td>
        </tr>

        <tr class="fila">
          <td align="left">Porcentaje comision applus</td>
          <td nowrap><input name="porcentaje_comision_applus" type="text" class="textbox" id="porcentaje_comision_applus"  onkeypress="return validar(porcentaje_comision_applus,2,1)"  maxlength="10"  value="<%=porcentaje_comision_applus%>"  </td>
          <td align="left">Porcentaje comision factoring fintra</td>
          <td nowrap><input name="porcentaje_factoring_fintra" type="text" class="textbox" id="porcentaje_factoring_fintra"  onkeypress="return validar(porcentaje_factoring_fintra,2,1)"  maxlength="10"  value="<%=porcentaje_factoring_fintra%>"  </td>
        </tr>

        <tr class="fila">
          <td align="left">Porcentaje comision provintegral</td>
          <td nowrap><input name="porcentaje_comision_provintegral" type="text" class="textbox" id="porcentaje_comision_provintegral"  onkeypress="return validar(porcentaje_comision_provintegral,2,1)"maxlength="10"  value="<%=porcentaje_comision_provintegral%>"  </td>
          <td align="left">Porcentaje comision fintra</td>
          <td nowrap><input name="porcentaje_comision_fintra" type="text" class="textbox" id="porcentaje_comision_fintra"  onkeypress="return validar(porcentaje_comision_fintra,2,1)" maxlength="10"  value="<%=porcentaje_comision_fintra%>"  </td>
        </tr>
        <tr class="fila">
          <td align="left">Porcentaje comision ECA</td>
          <td nowrap><input name="porcentaje_comision_eca" type="text" class="textbox" id="porcentaje_comision_eca"  onkeypress="return validar(porcentaje_comision_eca,2,1)" maxlength="10"  value="<%=porcentaje_comision_eca%>"  </td>
          <td align="left">Puntos adicionales DTF</td>
          <td nowrap><input name="puntos_dtf" type="text" class="textbox" id="puntos_dtf"  onkeypress="return validar(puntos_dtf,3,2)" maxlength="10"  value="<%=puntos_dtf%>"  </td>
        </tr>
        <tr class="fila">
          <td align="left">Porcentaje iva</td>
          <td nowrap><input name="porcentaje_iva" type="text" class="textbox" id="porcentaje_iva" onkeypress="return validar(porcentaje_iva,2,1)" maxlength="10"  value="<%=porcentaje_iva%>" readonly </td>
          <td align="left"></td>
          <td align="left"></td>

        </tr>

      </table></td>
    </tr>
  </table>

  <% } %>

        <table align="center">
          <tr>

            <% if ( (id_estado_negocio == 9) && ( !prefijo.equalsIgnoreCase("VA")) ) {  %>
            <td nowrap align="center">
                  <img src="<%=BASEURL%>/images/botones/aplicar.gif"      height="21"  title='REGISTRAR liquidacion y simbolo'      onClick="aplicarLiquidacion();"     onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
            </td>
            <% } %>

            <% if ( (id_estado_negocio == 8) && ( !prefijo.equalsIgnoreCase("VA")) ) {  %>
            <td nowrap align="center">
                  <img src="<%=BASEURL%>/images/botones/validar.gif"      height="21"  title='VALIDAR Reliquidacion con los nuevos porcentajes'      onClick="validarReliquidacion();"     onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
            </td>
            <td nowrap align="center">
                  <img src="<%=BASEURL%>/images/botones/aplicar.gif"      height="21"  title='REGISTRAR liquidacion y simbolo'      onClick="aplicarLiquidacion();"     onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
            </td>
            <% } %>


            <td nowrap align="center">
                  <img src="<%=BASEURL%>/images/botones/salir.gif"      height="21"  title='Salir'      onClick="regresar();"     onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
            </td>

          </tr>
        </table>
      </form >
    </div>


        <!-- Necesario para los calendarios-->
        <iframe width="188" height="166" name="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" id="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" src="<%=BASEURL%>/js/calendartsp/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"></iframe>


        <%=datos[1]%>



    </body>
</html>
