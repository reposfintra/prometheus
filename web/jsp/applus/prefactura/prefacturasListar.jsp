<%--
    Document   : prefacturasListar
    Created on : 11/02/2009, 05:53:17 AM
    Author     : Alvaro
--%>

<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<%@page import="com.tsp.operation.model.beans.*"%>



<%

            String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
            String datos[] = model.menuService.getContenidoMenu(BASEURL, request.getRequestURI(), path);

            List listaPrefacturaResumen = model.applusService.getPrefacturaResumen();

%>




<html>
  <head>

    <title>Consultar items para prefacturar</title>

    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
    <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>




    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">

  </head>
  <body  >
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
      <jsp:include page="/toptsp.jsp?encabezado=Prefacturar Ordenes"/>
    </div>


    <div id="capaSiguiente" style="position:absolute; width:1600; height:87%; z-index:0; left: 0px; top: 100px; ">




      <form name='formulario' method='post' id="formulario" action="" >
        <p>&nbsp;</p>

        <table width="1580"  border="1"  align="center">

              <TR class="tblTitulo1"  align="center">
                      <TH  width="80" align="center"> PREFACTURA</TH>
                      <TH  width="80" align="center"> FACTURA CONF.</TH>
                      <TH  width="80" align="center"> FECHA</TH>
                      <TH  width="80" align="center"> MATERIAL</TH>
                      <TH  width="80" align="center"> MANO DE OBRA</TH>
                      <TH  width="80" align="center"> OTROS</TH>
                      <TH  width="80" align="center"> SUBTOTAL</TH>
                      <TH  width="80" align="center"> ADMINISTRACION</TH>
                      <TH  width="80" align="center"> IMPREVISTO</TH>
                      <TH  width="80" align="center"> UTILIDAD</TH>
                      <TH  width="80" align="center"> BONIFICACION</TH>
                      <TH  width="60" align="center"> SUBTOTAL</TH>
                      <TH  width="60" align="center"> BASE IVA</TH>
                      <TH  width="80" align="center"> IVA </TH>
                      <TH  width="80" align="center"> VALOR PREFACTURA </TH>
                      <TH  width="80" align="center"> RET MATERIAL </TH>
                      <TH  width="80" align="center"> RET MANO OBRA </TH>
                      <TH  width="80" align="center"> RET OTROS</TH>
                      <TH  width="80" align="center"> TOTAL RETENCION </TH>
                      <TH  width="80" align="center"> FACTORING </TH>
                      <TH  width="80" align="center"> FORMULA </TH>
                      <TH  width="80" align="center"> FORMULA PROVINTEGRAL</TH>					  
                      <TH  width="80" align="center"> VALOR A PAGAR </TH>
              </TR>




          <%
            int i=0;
            Iterator it = listaPrefacturaResumen.iterator();
            while (it.hasNext()) {

              i++;
              PrefacturaResumen  prefacturaResumen = (PrefacturaResumen)it.next();
              String id_prefactura = prefacturaResumen.getPrefactura();
              String fecha_prefactura = prefacturaResumen.getFecha_prefactura();
			  String factura_conformada = prefacturaResumen.getFacturaConformada();

              double vlr_mat = prefacturaResumen.getVlr_mat();
              double vlr_mob = prefacturaResumen.getVlr_mob();
              double vlr_otr = prefacturaResumen.getVlr_otr();
              double vlr_factoring = prefacturaResumen.getVlr_factoring();
              double vlr_formula   = prefacturaResumen.getVlr_formula();
			  double vlr_formula_provintegral=prefacturaResumen.getVlr_formula_provintegral();
              double totalPrev1 = prefacturaResumen.getTotal_prev1();
              double vlr_administracion = prefacturaResumen.getVlr_administracion();
              double vlr_imprevisto     = prefacturaResumen.getVlr_imprevisto();
              double vlr_utilidad       = prefacturaResumen.getVlr_utilidad();              
              double bonificacion = prefacturaResumen.getBonificacion();
              double subtotal1    = totalPrev1 + vlr_administracion + vlr_imprevisto + 
                                    vlr_utilidad + bonificacion;

              double vlr_base_iva = prefacturaResumen.getVlr_base_iva();
              double vlr_iva      = prefacturaResumen.getVlr_iva();
              double subtotal2    = subtotal1 + vlr_iva;
              double vlr_rmat = prefacturaResumen.getVlr_rmat();
              double vlr_rmob = prefacturaResumen.getVlr_rmob();
              double vlr_rotr = prefacturaResumen.getVlr_rotr();
              double vlr_retencion      = prefacturaResumen.getVlr_retencion();
              double vlr_pago  = subtotal1 + vlr_iva + vlr_retencion + vlr_factoring + vlr_formula+vlr_formula_provintegral;
              String selectURL = CONTROLLER + "?estado=Prefactura&accion=Acceder&evento=DETALLE_PREFACTURA&id_prefactura="+id_prefactura;

          %>
              <tr class='<%= (i%2==0?"filagris1":"filaazul1") %>'>

                <td width="80"  align="center"><A HREF="<%= selectURL %>" > <%= id_prefactura %>  </A> </td>
                <td width="80"  align="center"> <%= factura_conformada %>  </td>
                <td width="80"  align="center"> <%= fecha_prefactura %>  </td>
                <td width="80"  align="right" > <%= Util.customFormat(vlr_mat) %> </td>
                <td width="80"  align="right" > <%= Util.customFormat(vlr_mob) %> </td>
                <td width="80"  align="right" > <%= Util.customFormat(vlr_otr) %> </td>
                <td width="80"  align="right" > <%= Util.customFormat(totalPrev1) %> </td>
                <td width="80"  align="right" > <%= Util.customFormat(vlr_administracion) %> </td>
                <td width="80"  align="right" > <%= Util.customFormat(vlr_imprevisto) %> </td>
                <td width="80"  align="right" > <%= Util.customFormat(vlr_utilidad) %> </td>
                <td width="80"  align="right" style="color:red"> <%= Util.customFormat(bonificacion ) %></td>
                <td width="60"  align="right" > <%= Util.customFormat(subtotal1) %></td>
                <td width="60"  align="right" > <%= Util.customFormat(vlr_base_iva) %></td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(vlr_iva ) %></td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(subtotal2 ) %></td>
                <td width="80"  align="right" style="color:red"> <%= Util.FormatoMiles(vlr_rmat) %> </td>
                <td width="80"  align="right" style="color:red"> <%= Util.FormatoMiles(vlr_rmob) %> </td>
                <td width="80"  align="right" style="color:red"> <%= Util.FormatoMiles(vlr_rotr) %> </td>
                <td width="80"  align="right" style="color:red"> <%= Util.FormatoMiles(vlr_retencion ) %></td>
                <td width="80"  align="right" style="color:red"> <%= Util.FormatoMiles(vlr_factoring) %> </td>
                <td width="80"  align="right" style="color:red"> <%= Util.FormatoMiles(vlr_formula) %> </td>
                <td width="80"  align="right" style="color:red"> <%= Util.FormatoMiles(vlr_formula_provintegral) %> </td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(vlr_pago ) %></td>

              </tr>
           <%
                }
           %>

        </table>

        <p>&nbsp;</p>

        <table align="center">
          <tr>
            <td nowrap align="center">
                  <img src="<%=BASEURL%>/images/botones/salir.gif"      height="21"  title='Salir'      onClick="window.close();"     onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
          </tr>
        </table>
      </form >
    </div>



    <script >

      function Formatear(numero,decimales,miles)
      {
        var numero = new oNumero(numero);
        return numero.formato(decimales, miles);
      }

      //Objeto oNumero
      function oNumero(numero)
      {
      //Propiedades
      this.valor = numero || 0
      this.dec = -1;
      //M�todos
      this.formato = numFormat;
      this.ponValor = ponValor;
      //Definici�n de los m�todos
      function ponValor(cad)
      {
      if (cad =='-' || cad=='+') return
      if (cad.length ==0) return
      if (cad.indexOf('.') >=0)
          this.valor = parseFloat(cad);
      else
          this.valor = parseInt(cad);
      }
      function numFormat(dec, miles)
      {
      var num = this.valor, signo=3, expr;
      var cad = ""+this.valor;
      var ceros = "", pos, pdec, i;
      for (i=0; i < dec; i++)
      ceros += '0';
      pos = cad.indexOf('.')
      if (pos < 0)
          cad = cad+"."+ceros;
      else
          {
          pdec = cad.length - pos -1;
          if (pdec <= dec)
              {
              for (i=0; i< (dec-pdec); i++)
                  cad += '0';
              }
          else
              {
              num = num*Math.pow(10, dec);
              num = Math.round(num);
              num = num/Math.pow(10, dec);
              cad = new String(num);
              }
          }
      pos = cad.indexOf('.')
      if (pos < 0) pos = cad.lentgh
      if (cad.substr(0,1)=='-' || cad.substr(0,1) == '+')
             signo = 4;
      if (miles && pos > signo)
          do{
              expr = /([+-]?\d)(\d{3}[\.\,]\d*)/
              cad.match(expr)
              cad=cad.replace(expr, RegExp.$1+','+RegExp.$2)
              }
      while (cad.indexOf(',') > signo)
          if (dec<0) cad = cad.replace(/\./,'')
              return cad;
      }
      }//Fin del objeto oNumero:


    </script>




  </body>
</html>