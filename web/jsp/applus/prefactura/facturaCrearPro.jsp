<%-- 
    Document   : facturaCrearPro
    Created on : 8/03/2009, 06:24:55 AM
    Author     : Alvaro
--%>


<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>


<html>
<head>
        <title>Creacion de facturas CxP a PROVINTEGRAL  </title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
	<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>

	<script>
	   function send(theForm){
	              theForm.submit();
	   }
	</script>

</head>

<body onLoad="redimensionar();" onResize="redimensionar();">


<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    %>

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modulo de Prefacturacion"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; ">


   <% String  msj           = request.getParameter("msj");
      String aceptarDisable = request.getParameter("aceptarDisable");
   %>

 <center>

   <form action="<%=CONTROLLER%>?estado=Prefactura&accion=Acceder&evento=GENERAR_FACTURA_PRO" method='post' name='formulario' >


   <table width="450" border="2" align="center">
       <tr>
          <td>
               <table width='100%' align='center' class='tablaInferior'>

                  <tr class="barratitulo">
                    <td colspan='2' >
                       <table cellpadding='0' cellspacing='0' width='100%'>
                         <tr>
                          <td align="left" width='65%' class="subtitulo1">&nbsp;GENERACION DE FACTURAS CXP A PROVINTEGRAL </td>
                          <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"  height="20" align="left"><%=datos[0]%></td>
                        </tr>
                       </table>
                    </td>
                 </tr>


                 <tr  class="fila">
                       <td colspan='2' >
                          <table class='tablaInferior' width='100%'>
                               <tr  class="fila">
                                   <td align="center" > Presione ACEPTAR para generar facturas de CxP a PROVINTEGRAL </td>
                               </tr>
                          </table>
                       </td>
                 </tr>

           </table>
         </td>
      </tr>
   </table>

   <br>



   <% if ( (aceptarDisable == null) || (aceptarDisable.equals(""))  ) { %>
        <img src="<%=BASEURL%>/images/botones/aceptar.gif"    height="21"  title='Genera las facturas de CxP a PROVINTEGRAL'  onclick='send(formulario);'   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
   <% }
      else { %>
        <img src="<%=BASEURL%>/images/botones/aceptarDisable.gif"    height="21"  style="cursor:hand">
   <% } %>

   <img src="<%=BASEURL%>/images/botones/salir.gif"      height="21"  title='Salir'      onClick="window.close();"     onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">


   <!-- MENSAJE -->
   <% if(msj!=null  &&  !msj.equals("") ){%>
        <br><br>
        <table border="2" align="center">
              <tr>
                <td>
                    <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                          <tr>
                                <td width="450" align="center" class="mensajes"><%= msj %></td>
                                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="58">&nbsp; </td>
                          </tr>
                     </table>
                </td>
              </tr>
        </table>

    <%}%>



      </form >

</center>

</div>


<%=datos[1]%>


</body>
</html>
