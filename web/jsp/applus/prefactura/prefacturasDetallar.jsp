<%--
    Document   : prefacturasDetallar
    Created on : 14/02/2009, 06:57:26 AM
    Author     : Alvaro
--%>

<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<%@page import="com.tsp.operation.model.beans.*"%>



<%
            String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
            String datos[] = model.menuService.getContenidoMenu(BASEURL, request.getRequestURI(), path);
            List listaDetallePrefactura = model.applusService.getDetallePrefactura();
            DetallePrefactura  primerItemPrefactura  = (DetallePrefactura) listaDetallePrefactura.get(0);
            String prefacturaExportar = primerItemPrefactura.getPrefactura();
%>



<html>
  <head>

    <title>Consultar items para prefacturar</title>

    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
    <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>

	<script>
	   function send(theForm){
           theForm.submit();
	   }
	   function exportar(){
           window.close();
		   window.open('<%= CONTROLLER %>?estado=Prefactura&accion=Acceder&evento=EXPORTAR_DETALLE_PREFACTURA&prefactura=<%= prefacturaExportar %>');
	   }
	</script>



    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">

  </head>
  <body  >
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
      <jsp:include page="/toptsp.jsp?encabezado=Prefacturar Ordenes"/>
    </div>


    <div id="capaSiguiente" style="position:absolute; width:2700; height:87%; z-index:0; left: 0px; top: 100px; ">




      <form name='formulario' method='post' id="formulario" action="/fintravalores/jsp/applus/prefactura/prefacturasListar.jsp" >
        <p>&nbsp;</p>

        <table width="2630" border="1"  align="center">

              <TR class="tblTitulo1" align="center">
                      <TH  width="80" align="center"> PREFACTURA</TH>
                      <TH  width="80" align="center"> FEC_OPEN</TH>					  
                      <TH  width="80" align="center"> FECHA</TH>
                      <TH  width="60" align="center"> ORDEN</TH>
                      <TH  width="60" align="center"> NUM OS</TH>
                      <TH  width="60" align="center"> ACCION</TH>
                      <TH  width="60" align="center"> CONSECUTIVO</TH>
                      <TH  width="80" align="center"> FACTURA CONFORMADA</TH>
                      <TH  width="80" align="center"> SIMBOLO VARIABLE</TH>
                      <TH  width="120" align="center"> NOMBRE CLIENTE</TH>
                      <TH  width="80" align="center"> TIPO CLIENTE</TH>					  
                      <TH  width="80" align="center"> MATERIAL</TH>
                      <TH  width="80" align="center"> MANO DE OBRA</TH>
                      <TH  width="80" align="center"> OTROS</TH>
                      <TH  width="80" align="center"> SUBTOTAL</TH>

                      <TH  width="80" align="center"> ADMINISTRACION</TH>
                      <TH  width="80" align="center"> IMPREVISTO</TH>
                      <TH  width="80" align="center"> UTILIDAD</TH>


					  <TH  width="80" align="center"> SUBTOTAL (totalprev1)</TH>

                      <TH  width="80" align="center"> BONIFICACION</TH>
                      <TH  width="60" align="center"> SUBTOTAL</TH>

                      <TH  width="60" align="center"> BASE IVA</TH>

                      <TH  width="80" align="center"> IVA </TH>
                      <TH  width="80" align="center"> VALOR PREFACTURA </TH>
                      <TH  width="50" align="center"> %RET MAT </TH>
                      <TH  width="80" align="center"> RET MATERIAL </TH>
                      <TH  width="50" align="center"> %RET MOB </TH>
                      <TH  width="80" align="center"> RET MANO OBRA </TH>
                      <TH  width="50" align="center"> %RET OTROS </TH>
                      <TH  width="80" align="center"> RET OTROS</TH>
                      <TH  width="80" align="center"> TOTAL RETENCION </TH>
                      <TH  width="50" align="center"> TIPO DTF </TH>
                      <TH  width="50" align="center"> CUOTAS </TH>
                      <TH  width="50" align="center"> % FACTORING </TH>
                      <TH  width="80" align="center"> FACTORING </TH>
                      <TH  width="50" align="center"> % FORMULA </TH>
                      <TH  width="80" align="center"> FORMULA </TH>
                      <TH  width="80" align="center"> FORMULA PROVINTEGRAL </TH>
                      <TH  width="80" align="center"> VALOR A PAGAR </TH>

              </TR>




          <%
            int i=0;
            Iterator it = listaDetallePrefactura.iterator();
			

			double suma_material=0;
			double suma_mo=0;
			double suma_otros=0;
			double suma_subtotalprev1=0;

            double suma_vlr_administracion = 0;
            double suma_vlr_imprevisto = 0;
            double suma_vlr_utilidad = 0;


			double suma_boni=0;
			double suma_subtotal1=0;
			double suma_iva=0;
			double suma_valprefactura=0;
			double suma_retmat=0;
			double suma_retmo=0;
			double suma_retotros=0;
			double suma_totret=0;
			double suma_factoring=0;
			double suma_formula=0;
			double suma_tot=0;

            while (it.hasNext()) {

              i++;
              DetallePrefactura  detallePrefactura = (DetallePrefactura)it.next();
              String id_prefactura = detallePrefactura.getPrefactura();
              String fecha_prefactura = detallePrefactura.getFecha_prefactura();

              String orden = detallePrefactura.getId_orden();
              String accion = detallePrefactura.getId_accion();
              String consecutivo = detallePrefactura.getConsecutivo();
              String factura_conformada = detallePrefactura.getFactura_conformada();
              String nombre_cliente = detallePrefactura.getNombre_cliente();
              String simbolo_variable = detallePrefactura.getSimbolo_variable();

              double porcentaje_rmat = detallePrefactura.getPorcentaje_rmat();
              double porcentaje_rmob = detallePrefactura.getPorcentaje_rmob();
              double porcentaje_rotr = detallePrefactura.getPorcentaje_rotr();

              int cuotas_reales = detallePrefactura.getCuotas_reales();
              String tipo_dtf = detallePrefactura.getTipo_dtf();

              double porcentaje_factoring = detallePrefactura.getPorcentaje_factoring();
			  

              double porcentaje_formula = detallePrefactura.getPorcentaje_formula();
              

			   String numos=detallePrefactura.getNumOs();

              double vlr_mat = detallePrefactura.getVlr_mat();
              double vlr_mob = detallePrefactura.getVlr_mob();
              double vlr_otr = detallePrefactura.getVlr_otr();

              double vlr_rmat = detallePrefactura.getVlr_rmat();
              double vlr_rmob = detallePrefactura.getVlr_rmob();
              double vlr_rotr = detallePrefactura.getVlr_rotr();
              double vlr_factoring = detallePrefactura.getValor_factoring();
              double vlr_formula   = detallePrefactura.getValor_formula();

			  double vlr_formula_provintegral=detallePrefactura.getValor_formula_provintegral();

              double totalPrev1 = detallePrefactura.getTotal_prev1();

              double vlr_administracion = detallePrefactura.getVlr_admnistracion();
              double vlr_imprevisto     = detallePrefactura.getVlr_imprevisto();
              double vlr_utilidad       = detallePrefactura.getVlr_utilidad();

		      String f_facturado_cliente                       = detallePrefactura.getF_facturado_cliente();

              double bonificacion = detallePrefactura.getBonificacion();

			  String tipo_identificacion=detallePrefactura.getTipoIdentificacion();

              double subtotal1    = totalPrev1 + vlr_administracion + vlr_imprevisto +
                                    vlr_utilidad + bonificacion;

              double vlr_base_iva = detallePrefactura.getVlr_base_iva();



              double vlr_iva = detallePrefactura.getVlr_iva();
              double subtotal2  = subtotal1 + vlr_iva;
              double vlr_retencion = detallePrefactura.getVlr_retencion();
              double vlr_pago  = subtotal1 + vlr_iva + vlr_retencion + vlr_factoring + vlr_formula+vlr_formula_provintegral;

			  suma_material=suma_material+vlr_mat;
  			  suma_mo=suma_mo+vlr_mob;
			  suma_otros=suma_otros+vlr_otr;
			  suma_subtotalprev1=suma_subtotalprev1+totalPrev1;

              suma_vlr_administracion = suma_vlr_administracion + vlr_administracion;
              suma_vlr_imprevisto = suma_vlr_imprevisto + vlr_imprevisto;
              suma_vlr_utilidad = suma_vlr_utilidad + vlr_utilidad;

			  suma_boni=suma_boni+bonificacion;
			  suma_subtotal1=suma_subtotal1+subtotal1;
			  suma_iva=suma_iva+vlr_iva;
			  suma_valprefactura=suma_valprefactura+subtotal2;
			  suma_retmat=suma_retmat+vlr_rmat;
			  suma_retmo=suma_retmo+vlr_rmob;
			  suma_retotros=suma_retotros+vlr_rotr;
			  suma_totret=suma_totret+vlr_retencion;
			  suma_factoring=suma_factoring+vlr_factoring;
			  suma_formula=suma_formula+vlr_formula;
			  suma_tot=suma_tot+vlr_pago;

          %>
              <tr class='<%= (i%2==0?"filagris1":"filaazul1") %>'>

                <td width="80"  align="center"> <%= id_prefactura %>   </td>
                <td width="80"  align="center"> <%= f_facturado_cliente %>   </td>				
				
                <td width="80"  align="center"> <%= fecha_prefactura %>  </td>
                <td width="60"  align="center"> <%= orden %>  </td>
                <td width="60"  align="center"> <%= numos %>  </td>
                <td width="60"  align="center"> <%= accion %>  </td>
                <td width="60"  align="center"> <%= consecutivo %>  </td>
                <td width="80"  align="center"> <%= factura_conformada %>  </td>
                <td width="80"  align="center"> <%= simbolo_variable %>  </td>
                <td width="120"  align="center"> <%= nombre_cliente %>  </td>
				
				<td width="80"  align="center"> <%= tipo_identificacion %>  </td>
				
				
                <td width="80"  align="right" > <%= Util.customFormat(vlr_mat) %> </td>
                <td width="80"  align="right" > <%= Util.customFormat(vlr_mob) %> </td>
                <td width="80"  align="right" > <%= Util.customFormat(vlr_otr) %> </td>
                <td width="80"  align="right" > <%= Util.customFormat(totalPrev1) %> </td>
                <td width="80"  align="right" > <%= Util.customFormat(vlr_administracion) %> </td>
                <td width="80"  align="right" > <%= Util.customFormat(vlr_imprevisto) %> </td>
                <td width="80"  align="right" > <%= Util.customFormat(vlr_utilidad) %> </td>
				
                <td width="80"  align="right" > <%= Util.customFormat(vlr_utilidad+vlr_imprevisto+vlr_administracion+vlr_otr+vlr_mob+vlr_mat) %> </td>
				
                <td width="80"  align="right" style="color:red"> <%= Util.customFormat(bonificacion ) %></td>
                <td width="60"  align="right" > <%= Util.customFormat(subtotal1) %></td>
                <td width="60"  align="right" > <%= Util.customFormat(vlr_base_iva) %></td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(vlr_iva ) %></td>
                <td width="80"  align="right" > <%= Util.FormatoMiles(subtotal2 ) %></td>
                <td width="50"  align="right" > <%= Util.FormatoMiles(porcentaje_rmat ) %></td>
                <td width="80"  align="right" style="color:red"> <%= Util.FormatoMiles(vlr_rmat) %> </td>
                <td width="50"  align="right" > <%= Util.FormatoMiles(porcentaje_rmob ) %></td>
                <td width="80"  align="right" style="color:red"> <%= Util.FormatoMiles(vlr_rmob) %> </td>
                <td width="50"  align="right" > <%= Util.FormatoMiles(porcentaje_rotr ) %></td>
                <td width="80"  align="right" style="color:red"> <%= Util.FormatoMiles(vlr_rotr) %> </td>
                <td width="80"  align="right" style="color:red"> <%= Util.FormatoMiles(vlr_retencion ) %></td>
                <td width="50"  align="center" > <%= tipo_dtf %> </td>
                <td width="50"  align="center" > <%= cuotas_reales %> </td>
                <td width="50"  align="right" > <%= Util.FormatoMiles(porcentaje_factoring ) %></td>
                <td width="80"  align="right" style="color:red"> <%= Util.FormatoMiles(vlr_factoring) %> </td>
                <td width="50"  align="right" > <%= Util.FormatoMiles(porcentaje_formula ) %></td>
                <td width="80"  align="right" style="color:red"> <%= Util.FormatoMiles(vlr_formula) %> </td>

                <td width="80"  align="right" style="color:red"> <%= Util.FormatoMiles(vlr_formula_provintegral) %> </td>
 
                <td width="80"  align="right" > <%= Util.FormatoMiles(vlr_pago ) %></td>

              </tr>


           <%
                }
           %>
			<tr class='<%= ((i+1)%2==0?"filagris1":"filaazul1") %>' height="25">

                <td width="80"  align="center">    </td>
				<td width="80"  align="center">    </td>
                <td width="80"  align="center">  </td>
                <td width="60"  align="center">   </td>
                <td width="60"  align="center">   </td>
                <td width="60"  align="center">   </td>
                <td width="60"  align="center">   </td>
                <td width="80"  align="center">   </td>
                <td width="80"  align="center">   </td>
                <td width="120"  align="center">   </td>
				<td width="80"  align="center">   </td>
                <td width="80"  align="right" ><strong><u>  <%= Util.customFormat(suma_material) %></u></strong> </td>
                <td width="80"  align="right" > <strong><u>  <%= Util.customFormat(suma_mo) %> </u></strong></td>
                <td width="80"  align="right" > <strong><u>  <%= Util.customFormat(suma_otros) %> </u></strong></td>
                <td width="80"  align="right" > <strong><u>  <%= Util.customFormat(suma_subtotalprev1) %></u></strong> </td>
                <td width="80"  align="right" > <strong><u>  <%= Util.customFormat(suma_vlr_administracion) %></u></strong> </td>
                <td width="80"  align="right" > <strong><u>  <%= Util.customFormat(suma_vlr_imprevisto) %></u></strong> </td>
                <td width="80"  align="right" > <strong><u>  <%= Util.customFormat(suma_vlr_utilidad) %></u></strong> </td>
				
				<td width="80"  align="right" > <strong><u>  <%= Util.customFormat(suma_vlr_utilidad+suma_vlr_imprevisto+suma_vlr_administracion+suma_material+suma_mo+suma_otros) %></u></strong> </td>
				
                <td width="80"  align="right" style="color:red"> <strong><u>  <%= Util.customFormat(suma_boni) %></u></strong></td>
                <td width="60"  align="right" > <strong><u>  <%= Util.customFormat(suma_subtotal1) %></u></strong></td>
                <td width="60"  align="center" >  </td>
                <td width="80"  align="right" ><strong><u>  <%= Util.customFormat(suma_iva) %> </u></strong></td>
                <td width="80"  align="right" > <strong><u>  <%= Util.customFormat(suma_valprefactura) %></u></strong></td>
                <td width="50"  align="right" > </td>
                <td width="80"  align="right" style="color:red"><strong><u> <%= Util.FormatoMiles(suma_retmat) %></u></strong>  </td>
                <td width="50"  align="right" > </td>
                <td width="80"  align="right" style="color:red"><strong><u>  <%= Util.FormatoMiles(suma_retmo) %></u></strong> </td>
                <td width="50"  align="right" > </td>
                <td width="80"  align="right" style="color:red"> <strong><u> <%= Util.FormatoMiles(suma_retotros) %></u></strong> </td>
                <td width="80"  align="right" style="color:red"><strong><u> <%= Util.FormatoMiles(suma_totret) %></u></strong>  </td>
                <td width="50"  align="center" > </td>
                <td width="50"  align="center" >  </td>
                <td width="50"  align="center" >  </td>
                <td width="80"  align="right" style="color:red"><strong><u> <%= Util.FormatoMiles(suma_factoring) %></u></strong>  </td>
                <td width="50"  align="right" > </td>
                <td width="80"  align="right" style="color:red"><strong><u> <%= Util.FormatoMiles(suma_formula) %></u></strong>  </td>
				<td width="80"  align="center">   </td>
                <td width="80"  align="right" ><strong><u> <%= Util.FormatoMiles(suma_tot) %></u></strong> </td>

              </tr>

        </table>

        <p>&nbsp;</p>

        <table align="center">
          <tr>
            <td  nowrap align="center">
               <img src="<%=BASEURL%>/images/botones/exportarExcel.gif" name="c_regresar" id="c_regresar" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="exportar();">
               <img src="<%=BASEURL%>/images/botones/salir.gif"      height="21"  title='Salir'      onClick="send(formulario);"     onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
          </tr>
        </table>
      </form >
    </div>



    <script >

      function Formatear(numero,decimales,miles)
      {
        var numero = new oNumero(numero);
        return numero.formato(decimales, miles);
      }

      //Objeto oNumero
      function oNumero(numero)
      {
      //Propiedades
      this.valor = numero || 0
      this.dec = -1;
      //M�todos
      this.formato = numFormat;
      this.ponValor = ponValor;
      //Definici�n de los m�todos
      function ponValor(cad)
      {
      if (cad =='-' || cad=='+') return
      if (cad.length ==0) return
      if (cad.indexOf('.') >=0)
          this.valor = parseFloat(cad);
      else
          this.valor = parseInt(cad);
      }
      function numFormat(dec, miles)
      {
      var num = this.valor, signo=3, expr;
      var cad = ""+this.valor;
      var ceros = "", pos, pdec, i;
      for (i=0; i < dec; i++)
      ceros += '0';
      pos = cad.indexOf('.')
      if (pos < 0)
          cad = cad+"."+ceros;
      else
          {
          pdec = cad.length - pos -1;
          if (pdec <= dec)
              {
              for (i=0; i< (dec-pdec); i++)
                  cad += '0';
              }
          else
              {
              num = num*Math.pow(10, dec);
              num = Math.round(num);
              num = num/Math.pow(10, dec);
              cad = new String(num);
              }
          }
      pos = cad.indexOf('.')
      if (pos < 0) pos = cad.lentgh
      if (cad.substr(0,1)=='-' || cad.substr(0,1) == '+')
             signo = 4;
      if (miles && pos > signo)
          do{
              expr = /([+-]?\d)(\d{3}[\.\,]\d*)/
              cad.match(expr)
              cad=cad.replace(expr, RegExp.$1+','+RegExp.$2)
              }
      while (cad.indexOf(',') > signo)
          if (dec<0) cad = cad.replace(/\./,'')
              return cad;
      }
      }//Fin del objeto oNumero:


    </script>




  </body>
</html>