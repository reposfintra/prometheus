<%-- 
    Document   : ofertaListar
    Created on : 25/02/2009, 08:37:55 PM
    Author     : Alvaro
--%>


<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<%@page import="com.tsp.operation.model.beans.*"%>



<%

            String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
            String datos[] = model.menuService.getContenidoMenu(BASEURL, request.getRequestURI(), path);

            List listaOfertaEca = model.applusService.getOfertaEca();

%>




<html>
  <head>

    <title>Consultar para listar ofertas</title>

    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
    <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>




    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">

  </head>
  <body  >
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
      <jsp:include page="/toptsp.jsp?encabezado=Lista de Ofertas"/>
    </div>


    <div id="capaSiguiente" style="position:absolute; width:1100; height:87%; z-index:0; left: 0px; top: 100px; ">




      <form name='formulario' method='post' id="formulario" action="" >
        <p>&nbsp;</p>

        <table width="1020"  border="1"  align="center">


              <TR class="tblTitulo1"  align="center">
					  <TH  width="50" align="center"> ex</TH><!--090505-->
                      <TH  width="50" align="center"> NUM_OS</TH>
                      <TH  width="50" align="center"> ORDEN</TH>
                      
                      <TH  width="50" align="center"> CLIENTE</TH>
                      <TH  width="50" align="center"> NIC</TH>
					  <TH  width="50" align="center"> TIPO </TH>
                      <TH  width="80" align="center"> NIT</TH>
                      <TH  width="300" align="center"> NOMBRE CLIENTE</TH>

                      <TH  width="240" align="center"> ESTUDIO ECONOMICO</TH>
                      <TH  width="50" align="center"> TIPO DTF</TH>
                      <TH  width="70" align="center"> CUOTAS REALES</TH>
					  <TH  width="80" align="center"> FECHA OFERTA</TH>
              </TR>




          <%
            int i=0;
            Iterator it = listaOfertaEca.iterator();
            while (it.hasNext()) {

              i++;
              OfertaEca  ofertaEca = (OfertaEca)it.next();

              int id_orden = ofertaEca.getId_orden();

			  String[] ex_prefacturaeca=model.negociosApplusService.getExPrefacturaEca(""+id_orden);

              String fecha_oferta = ofertaEca.getFecha_oferta().substring(0, 11);
			  int id_cliente = ofertaEca.getId_cliente();

              int nic  = ofertaEca.getNic();
			  String tipo_identificacion = ofertaEca.getTipoIdentificacion();
              String nit  = ofertaEca.getNit();
              String nombre_cliente = ofertaEca.getNombre_cliente();
              String num_os = ofertaEca.getNum_os();
              String estudio_economico = ofertaEca.getEstudio_economico();
              String tipo_dtf = ofertaEca.getTipo_dtf();
              int cuotas_reales = ofertaEca.getCuotas_reales();

              String selectURL = CONTROLLER + "?estado=Prefactura&accion=Acceder&evento=DETALLAR_OFERTA&opcion=LISTADO&id_orden="+id_orden;

          %>
              <tr class='<%= (i%2==0?"filagris1":"filaazul1") %>'>
			  
  			    <td width="50"  align="center" > <%= ex_prefacturaeca[0] %> </td><!--090505-->
				<td width="50"  align="center" > <%= num_os %> </td>
                <td width="50"  align="center"><A HREF="<%= selectURL %>" > <%= id_orden %>  </A> </td>

                <td width="50"  align="center"> <%= id_cliente %>  </td>
                <td width="50"  align="center" > <%= nic %> </td>
				
				<td width="50"  align="center" > <%= tipo_identificacion %> </td>
				
                <td width="80"  align="right" > <%= nit %> </td>
                <td width="300"  align="left" > <%= nombre_cliente %> </td>
                
                <td width="240"  align="center" > <%= estudio_economico %> </td>
                <td width="50"  align="center" > <%= tipo_dtf %> </td>
                <td width="70"  align="center" > <%= cuotas_reales %> </td>
				
                <td width="80"  align="center"> <%if (fecha_oferta!=null && fecha_oferta.equals("0099-01-01 ")){out.print("-");}else{out.print(fecha_oferta);} %>  </td>
              </tr>
           <%
                }
           %>

        </table>

        <p>&nbsp;</p>

        <table align="center">
          <tr>
            <td nowrap align="center">
                  <img src="<%=BASEURL%>/images/botones/salir.gif"      height="21"  title='Salir'      onClick="window.close();"     onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
          </tr>
        </table>
      </form >
    </div>



    <script >

      function Formatear(numero,decimales,miles)
      {
        var numero = new oNumero(numero);
        return numero.formato(decimales, miles);
      }

      //Objeto oNumero
      function oNumero(numero)
      {
      //Propiedades
      this.valor = numero || 0
      this.dec = -1;
      //M�todos
      this.formato = numFormat;
      this.ponValor = ponValor;
      //Definici�n de los m�todos
      function ponValor(cad)
      {
      if (cad =='-' || cad=='+') return
      if (cad.length ==0) return
      if (cad.indexOf('.') >=0)
          this.valor = parseFloat(cad);
      else
          this.valor = parseInt(cad);
      }
      function numFormat(dec, miles)
      {
      var num = this.valor, signo=3, expr;
      var cad = ""+this.valor;
      var ceros = "", pos, pdec, i;
      for (i=0; i < dec; i++)
      ceros += '0';
      pos = cad.indexOf('.')
      if (pos < 0)
          cad = cad+"."+ceros;
      else
          {
          pdec = cad.length - pos -1;
          if (pdec <= dec)
              {
              for (i=0; i< (dec-pdec); i++)
                  cad += '0';
              }
          else
              {
              num = num*Math.pow(10, dec);
              num = Math.round(num);
              num = num/Math.pow(10, dec);
              cad = new String(num);
              }
          }
      pos = cad.indexOf('.')
      if (pos < 0) pos = cad.lentgh
      if (cad.substr(0,1)=='-' || cad.substr(0,1) == '+')
             signo = 4;
      if (miles && pos > signo)
          do{
              expr = /([+-]?\d)(\d{3}[\.\,]\d*)/
              cad.match(expr)
              cad=cad.replace(expr, RegExp.$1+','+RegExp.$2)
              }
      while (cad.indexOf(',') > signo)
          if (dec<0) cad = cad.replace(/\./,'')
              return cad;
      }
      }//Fin del objeto oNumero:


    </script>




  </body>
</html>