<%-- 
    Document   : refinanciar_negocios
    Created on : 15/05/2019, 04:04:54 PM
    Author     : mcamargo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script> 
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script src="./js/refinanciar_negocios.js" type="text/javascript"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <title>Refinanciar negocios</title>
        <style type="text/css">

            .filtro{
                width: 63%;
                margin: 10px auto; 

            }

            .header_filtro{
                text-align: center;   
                border: 1px solid #234684;
                background: #2A88C8;
                color: #ffffff;
                font-weight: bold;
                border-radius: 8px 8px 0px 0px;
            }
            
            .body_filtro{                
                background: white; 
                border:1px solid #2A88C8;
                height: 115px;
                border-radius: 0px 0px 8px 8px;
            }

            .contenido{
                height: 83px; 
                width: auto;
                margin-top: 5px;
            }

            label{
                text-align: center;
                font-family: 'Lucida Grande','Lucida Sans',Arial,sans-serif;
                font-size: 12px;                
            }

            .label_valor{
                height:17px;
                width:100px;
                color:#070708;
                font-weight:bold;
                font-size:12px;
                display: block;
            }

            .caja{
                float:left;
                margin-left:10px;
                margin-right:10px;
                margin-top: 8px;
            }
            input[type=text],select,input[type=number]{
                padding: 5px 5px;                
                border: 1px solid #ccc;
                border-radius: 4px;
                font-size: 14px;
                color: black;
                text-align: right;
             
            }
            .select {
                font-size: 15px;
                background: #2A88C8 ;
                color: white;
                font-weight: bold;
            }
            .button {
                padding: 8px 8px;
                font-size: 12px;
                cursor: pointer;
                text-align: center;
                color: #fff;
                background-color: #2a88c8;
                border: none;
                border-radius:8px;
                margin: 22px auto;
                font-weight: bold;
            }
            .radio{
                height:15px; 
                width:15px;
                font-size: 15px;
            }
            #tablainterna_tb td{
                width: 600px;
                text-align: left;
            }            
        </style>
    </head>
    <body>
        <div id="capaSuperior"  style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Refinanciar negocios"/>
        </div>

        <div id="capaCentral" style="position:absolute; width:100%; height:80%; z-index:0; left: 0px; top: 100px; ">
            <center>
                <div id="tablita" style="width: 740px;height: 95px;  margin-top: 5px;" >

                    <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                        <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                            &nbsp;
                            <label  ><b>FILTRO DE BUSQUEDA</b></label>
                        </span>
                    </div>

                    <div  style="padding: 5px; background: white; border:1px solid #2A88C8">
                        <div class="col-md-12 ">
                            <table id="tablainterna" style="height: 53px; width: 100%"  >

                                <tr style = "text-align: middle;vertical-align: middle" style="width: 572px;" >

                                    <td style = "text-align: middle;vertical-align: middle" >
                                        <div style = "float: left;valign:middle">
                                            <select id="tipo_busqueda" name="tipo_busqueda" class="select">
                                                <option value="negocio">Negocio</option>
                                                <option value="ide">Identificacion</option>
                                            </select>
                                        </div>  
                                        <div style = "float: left;valign:middle; margin-left: 5px;">
                                            <input type="text" id="documento" name="documento" maxlength="12" autofocus style="height: 12px;width: 110px;color: #070708;"  >
                                            <select id="estrategia_cartera" name="estrategia_cartera" class="select">
                                            </select> 
                                        </div>
                                    </td> 
                                    <td style = "text-align: middle;vertical-align: middle">
                                        <div style = "float: left">
                                            <hr style="width: 2px;height: 39px;margin-left: 13px;color: rgba(128, 128, 128, 0.39);" /> 
                                        </div>
                                    </td> 

                                    <td style = "text-align: middle;vertical-align: middle">
                                        <div style = "float: left;" >
                                            <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;" > 
                                                <span class="ui-button-text">Buscar</span>
                                            </button>  
                                        </div>
                                    </td>

                                </tr>
                            </table>
                        </div>
                    </div>
                </div>

                <table id="tabla_saldo_financiar"></table>
                <div id="page_tabla_saldo_financiar"></div> 
                <br>
                <div id="div_detalle_cartera"  style="display:none;">             
                    <table id="tabla_detalle_cartera"></table> <br>
                    <button style="font-weight: bold; font-size: 18px;" id="refinanciar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" onclick="ver_detalle_refinanciacion_cartera();"> 
                        <span class="ui-button-text">Ver Negociación</span>
                    </button> 
                    <br>
                </div>
            </center>   

            <div id="div_detalle_refinanciacion_tb" style="display:none;">
                <center>    
                    <div id="tablita_tb" style="width: 820px;" >
                        <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                            <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                                &nbsp;
                                <label  ><b>Negociación</b></label>
                            </span>
                        </div>
                        <div style="background: white; border:1px solid #2A88C8">
                            <table id="tablainterna_tb" cellspacing="8px" style="margin-left: 2%"> 
                                <tr>
                                    <td style="display: none;"> 
                                        <input type="text" id="cod_negocio" name="cod_negocio" readonly="true" autofocus style="height: 17px;width: 100px;color: #070708;">
                                    </td>                                 
                                    <td style="display: none;"> 
                                        <input type="text" id="dias_vencidos" name="dias_vencidos" readonly="true" autofocus style="height: 17px;width: 100px;color: #070708;">
                                    </td>                                       

                                    <td> 
                                        <label>Interes Corriente</label><br>
                                        <label id="id_interes_tb" class="label_valor"></label>                                      
                                    </td> 
                                    <td id="id_cat_vencido"> 
                                        <label>Cat Vencido:</label><br>
                                        <label id="id_cat_vencido_tb" class="label_valor"></label>                                       
                                    </td> 
                                    <td> 
                                        <label >Otros Conceptos</label><br>
                                        <label id="id_cuota_admon_tb" class="label_valor"></label>                                        
                                    </td> 
                                    <td> 
                                        <label >Interes de Mora</label><br>
                                        <label id="id_ixm_tb" class="label_valor"></label>                                       
                                    </td> 

                                    <td> 
                                        <label>Gastos Cobranza</label><br>
                                        <label id="id_gac_tb" class="label_valor"></label>                                      
                                    </td>
                                    <td > 
                                        <label style="color:red;"># Pago mensual: <span id="peridos"></span> </label><br>
                                        <label id="total_pagar" class="label_valor" style="color: #2A88C8"></label>                                       
                                    </td>   
                                </tr>
                                <tr>
                                    <td style="display: none;"> 
                                        <label>Capital Aval:</label>
                                        <label id="id_capital_aval_tb" class="label_valor"></label>        
                                    </td> 
                                    <td style="display: none;"> 
                                        <label>Interes Aval:</label>
                                        <label id="id_int_aval_tb" class="label_valor"></label>                                         
                                    </td>  
                                    <td style="width: 0px;"> 
                                        <label>Capital Refinan.</label><br>
                                        <label id="id_capital_tb" class="label_valor"></label>                                        
                                    </td>
                                    <td style="display: none;"> 
                                        <input type="text" id="tipo_refi" name="tipo_refi" readonly="true" style="height: 17px;width: 100px;color: #070708;">
                                    </td>
                                    <td> 
                                        <label><span>Ley Mypime</span></label><br>
                                        <label id="id_cat_tb" class="label_valor"></label>                                       
                                    </td> 
                                    <td> 
                                        <label><span>Valor Cuota Actual</span></label><br>
                                        <label id="id_vlr_cuota_actual" class="label_valor"></label>                                       
                                    </td> 
                                    <td id="tdfecha"> 
                                        <label>Fecha pago</label>
                                        <select name="id_fecha" id="id_fecha" class="select" style="margin-left: 15px;" onchange=""></select>
                                    </td>                                     
                                    <td> 
                                        <label style="display: flex;">Plazo:</label>
                                        <select name="cuota" id="cuota" onchange="ver_cartera_refinanciar();" class="select" style="width: 80px; margin-left: 15px;" >  
                                        </select>
                                    </td> 
                                </tr>
                            </table>
                        </div>
                    </div>
                </center>
                <br><br>
                <div class="col-md-12 ">
                    <center>
                        <table id="tabla_detalle_refinanciacion"></table> 
                    </center>
                </div>
                <div id="div_obsevaciones" style="display:none;">  
                    <span>Confirmación de celular: </span>
                    <input class="input-number" type="text" id="celular" name="celular" maxlength="10" autofocus style="margin: 8px; height: 20px;width: 140px;color: #070708;font-size: 17px"  >
                    <textarea id="id_observaciones" style="margin-bottom:5px; height: 140px; width: 360px;">
                    </textarea>
                </div>
            </div>


            <div id="div_fecha" style="display:none;">
                <center>
                    <label style="padding: 10px;">Seleccione la fecha de la proyeccion del pago:</label><br><br>
                    <input type="date" id="select_fecha" style="height: 25px;width: 160px;color: #070708;font-weight:  bold;font-size: 16px;">
                </center>
            </div>  

        </div>

    </center>




    <div id="dialogLoading" style="display:none;">
        <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
        <center>
            <img src="./images/cargandoCM.gif"/>
        </center>
    </div>
    <div id="dialogMsj" title="Mensaje del sistema" style="display:none;">
        <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
    </div>  

</div>
</body>
</html>
