<!--  
	 - Author(s)       :      Jose de la rosa
	 - Description     :      AYUDA DESCRIPTIVA - Tipo de Proveedor
	 - Date            :      17/08/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>AYUDA DESCRIPTIVA - TIPO DE PROVEEDORES</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">TIPO DE PROVEEDOR</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Ingresar Tipo de Proveedor </td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2">INFORMACION DEL TIPO DE PROVEEDOR </td>
        </tr>
        <tr>
          <td  class="fila">Codigo</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el codigo del cliente a buscar. Este campo es de m&aacute;ximo 4 caracteres. </td>
        </tr>		
        <tr>
          <td  class="fila">Descripcion</td>
          <td  class="ayudaHtmlTexto">Campo para digitar la descripcion del tipo de proveedor. Este campo es de m&aacute;ximo 30 caracteres. </td>
        </tr>
        <td class="fila">Bot&oacute;n Reestablecer </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para colocar los datos en blanco.</td>
        </tr>
        <tr>
          <td class="fila">Bot&oacute;n Aceptar </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para guardar los datos.</td>
        </tr>
        <tr>
          <td class="fila">Bot&oacute;n Ocultar </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para ocultar el cuadro de modificacion de datos.</td>
        </tr>
        <tr>
          <td width="149" class="fila">Bot&oacute;n Salir</td>
          <td width="525"  class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista 'Tipo Proveedor' y volver a la vista del listado.</td>
        </tr>
        <tr>
          <td class="fila">Bot&oacute;n Anular </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para anular el registro seleccionado.</td>
        </tr>
        <tr>
          <td class="fila">Bot&oacute;n Activar </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para activar el registro seleccionado.</td>
        </tr>
        <tr>
          <td class="fila">Bot&oacute;n Eliminar </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para eliminar de la BD el registro seleccionado.</td>
        </tr>						
      </table>
    </td>
  </tr>
</table>
<p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</body>
</html>
