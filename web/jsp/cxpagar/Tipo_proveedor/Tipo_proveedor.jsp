<!--
- Date  : 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, permite realizar las funciones basicas sobre el programa de tipo de proveedor
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<%
List                ListaGnral  = model.TproveedorSvc.getList();
Tipo_proveedor      Datos       = model.TproveedorSvc.getDato();
String              Codigo      = (Datos!=null)?Datos.getCodigo_proveedor() :"";
String              Descripcion = (Datos!=null)?Datos.getDescripcion():"";
String Mensaje       = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
String NombreBoton   = (Datos==null)?"Guardar":"Modificar";
String TNombreBoton   = (Datos==null)?"aceptar":"modificar";
String NombreBoton1  = (ListaGnral!=null && ListaGnral.size()>0)?"Ocultar Lista":"Listado";
String TNombreBoton1  = (ListaGnral!=null && ListaGnral.size()>0)?"btnOcultar":"detalles";
String BloquearText  = (Datos==null)?"":"ReadOnly";
String BloquearSelect= "";
String BloquearBoton = (Datos!=null)?"":"Disabled";
%>

<html>
<head>
  <title>Tipo de proveedores</title>

  <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>  
  <link href="../css/estilostsp.css" rel='stylesheet'> 
  <script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
  <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
  <script>
      function validar(form){
            if (form.Opcion.value=='Guardar' || form.Opcion.value=='Modificar'){
                if (form.codigo.value ==''){
                    alert('Defina el codigo para poder continuar...')
                    return false;
                }
                if (form.descripcion.value ==''){
                    alert('Defina el descripcion para poder continuar...')
                    return false;
                }
            }
            return true;
      }
    function SelAll(){
            for(i=0;i<FormularioListado.length;i++)
                    FormularioListado.elements[i].checked=FormularioListado.All.checked;
    }
    function ActAll(){
            FormularioListado.All.checked = true;
            for(i=1;i<FormularioListado.length;i++)	
                    if (!FormularioListado.elements[i].checked){
                            FormularioListado.All.checked = false;
                            break;
                    }
    }      
    
    function validarlistado(form){
            for(i=1;i<FormularioListado.length;i++)	
                    if (FormularioListado.elements[i].checked)
                            return true;
            alert('Por favor seleccione un registro para poder continuar');
            return false;
    }      
  </script>
</head>
<body onResize="redimensionar()" onLoad="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Tipo de Proveedor"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<FORM action="<%=CONTROLLER%>?estado=Tipo_proveedor&accion=Manager" method='POST'  name='formulario'>

    <table width='415' align='center'  border='2'>
	  <tr>
	    <td width="50%" align="left" class="subtitulo1">&nbsp;Informacion de proveedor </td>	 
	    <td width="50%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
      </tr>
	  <tr>	  
       <td colspan="2">
          <table  border='0' width='100%'>                    
               <tr class="fila">
                    <td width='29%'>&nbsp Codigo :</td>
                 <td><input type='text' name='codigo'  maxlength='4' SIZE='6' class="textbox" style='text-align:center;'>
                 <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"> </td>                        
               </tr>
               <tr class="fila">
                  <td width='29%'>&nbsp Descripcion :
                  <input type='hidden' name='original' value="<%=Codigo+","+Descripcion%>"/></td>
                 <td >   <input type='text' name='descripcion' maxlength='30' class="textbox"style='width:95%;'>
                 <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"> </td>
              </tr>               
          </table>
      </td>
      </tr>
        <tr>           
        </tr>
  </table>
  <br>
    <table align="center">
  		<td colspan="2" align="center"> 
		<img src="<%=BASEURL%>/images/botones/restablecer.gif"  name="imgrestablecer" onClick="Opcion.value='Nuevo';formulario.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
		<img src="<%=BASEURL%>/images/botones/<%=TNombreBoton%>.gif"  name="imgsalir" onClick="Opcion.value='<%=NombreBoton%>';if(validar(formulario)){formulario.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 		
		<img src="<%=BASEURL%>/images/botones/<%=TNombreBoton1%>.gif"  name="imgdetalles" onClick="Opcion.value='<%=NombreBoton1%>';if(validar(formulario)){formulario.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
		<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
		</td>
    </table>
    <br>
<input type='hidden' name='Opcion'/> 
</FORM>
<script> 
formulario.codigo.value = '<%= Codigo %>';
formulario.descripcion.value = '<%= Descripcion %>';
</script>



<center class='comentario'>


 <%if(!Mensaje.equals("")){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%}%>


<%  if(ListaGnral!=null && ListaGnral.size()>0) { %>
    <form action="<%=CONTROLLER%>?estado=Tipo_proveedor&accion=Manager" method='post' name='FormularioListado'>
        <br>

        <tr><th height='28' colspan='7'>
		<table width="50%" border="2">
          <tr>
            <td><table width="100%"  cellspacing="1" cellpadding="0">
              <tr>
                <td width="60%" class="subtitulo1">LISTADO GENERAL DE REGISTROS</td>
                <td width="40%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
            </table>
			<table bgcolor="#F7F5F4" bordercolor="#999999" border="1" width="100%">
		<tr class="tblTitulo" align="center">
            <td width='10%' align="center">N�</td>
            <td width='6%' align="center"><input type='checkbox' name='All' onclick='jscript: SelAll();'></td>
            <td width='24%' align="center">CODIGO</td>
            <td width='60%' align="center">DESCRIPCION</td>
 
        </tr>
        <%
            int Cont = 1;
            Iterator it2 = ListaGnral.iterator();
            while(it2.hasNext()){
                Tipo_proveedor dat = (Tipo_proveedor) it2.next();
				String e = (Cont % 2 == 0 )?"filaazul":"filagris";
                String Estilo = (dat.getReg_status().equals("A"))?"filaresaltada":e;
                %>
        <tr class='<%= Estilo %>'>
            <td  align='center' class="bordereporte"><span class='comentario'><%=Cont++%></span></td>
            <td align="center" class="bordereporte"><input type='checkbox' name='LOV' value='<%= dat.getCodigo_proveedor()+","+dat.getDescripcion() %>' onclick='jscript: ActAll();'></td>
            <td align='center' class="bordereporte"><a href='<%=CONTROLLER%>?estado=Tipo_proveedor&accion=Manager&Opcion=Seleccionar&original2=<%= dat.getCodigo_proveedor()+","+dat.getDescripcion() %>'><%= dat.getCodigo_proveedor()  %></a></td>
            <td class="bordereporte"><%= dat.getDescripcion()%></td>
 
        </tr>
        <%  }   %>        
        </table></td>
          </tr>
        </table></th>
        </tr>

        <p><br>
            <input type='hidden' name='Opcion'/>             
</p>
        <table align="center">
  		<td colspan="2" align="center"> 
  			<img src="<%=BASEURL%>/images/botones/anular.gif"  name="imganular" onClick="Opcion.value='Anular';if(validarlistado(FormularioListado)){FormularioListado.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
  			<img src="<%=BASEURL%>/images/botones/activar.gif"  name="imgsalir" onClick="Opcion.value='Activar';if(validarlistado(FormularioListado)){FormularioListado.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
			<img src="<%=BASEURL%>/images/botones/eliminar.gif"  name="imgeliminar" onClick="Opcion.value='Eliminar';if(validarlistado(FormularioListado)){FormularioListado.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
			</td>
        </table>
        <p>&nbsp;        </p>
    </form>
<%  } %>

</center>
</div>
<%=datos[1]%>
</body>
</html>
