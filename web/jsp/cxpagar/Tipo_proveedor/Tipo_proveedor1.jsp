<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%
List                ListaGnral  = model.TproveedorSvc.getList();
Tipo_proveedor      Datos       = model.TproveedorSvc.getDato();
String              Codigo      = (Datos!=null)?Datos.getCodigo_proveedor() :"";
String              Descripcion = (Datos!=null)?Datos.getDescripcion():"";
String Mensaje       = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
String NombreBoton   = (Datos==null)?"Guardar":"Modificar";
String NombreBoton1  = (ListaGnral!=null && ListaGnral.size()>0)?"Ocultar Lista":"Listado";
String BloquearText  = (Datos==null)?"":"ReadOnly";
String BloquearSelect= "";
String BloquearBoton = (Datos!=null)?"":"Disabled";
%>

<html>
<head>
  <title>Tipo de proveedores</title>
  <link href="<%= BASEURL %>/css/estilo.css" rel='stylesheet'>  
  <link href="<%= BASEURL %>/css/estilotsp.css" rel='stylesheet'>  
  <link href="../css/estilotsp.css" rel='stylesheet'> 
  <script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
  <script>
      function validar(form){
            if (form.Opcion.value=='Guardar' || form.Opcion.value=='Modificar'){
                if (form.codigo.value ==''){
                    alert('Defina el codigo para poder continuar...')
                    return false;
                }
                if (form.descripcion.value ==''){
                    alert('Defina el descripcion para poder continuar...')
                    return false;
                }
            }
            return true;
      }
    function SelAll(){
            for(i=0;i<FormularioListado.length;i++)
                    FormularioListado.elements[i].checked=FormularioListado.All.checked;
    }
    function ActAll(){
            FormularioListado.All.checked = true;
            for(i=1;i<FormularioListado.length;i++)	
                    if (!FormularioListado.elements[i].checked){
                            FormularioListado.All.checked = false;
                            break;
                    }
    }      
    
    function validarlistado(form){
            for(i=1;i<FormularioListado.length;i++)	
                    if (FormularioListado.elements[i].checked)
                            return tue;
            alert('Por favor seleccione un registro para poder continuar');
            return false;
    }      
  </script>
</head>
<body>
<FORM action="<%=CONTROLLER%>?estado=Tipo_proveedor&accion=Manager" method='POST'  name='formulario' onsubmit='return validar(this);'>

    <table width='400' align='center' cellpadding='0' cellspacing='0' bgcolor="#FFFFFF" border='2'>
	  <tr>
	    <td class="subtitulo1" align="left">&nbsp;Informacion de proveedor </td>	 
	    <td class="barratitulo" align="left"><img src="<%=BASEURL%>/images/cuadrosverde.JPG" width="32" height="20"></td>
      </tr>
	  <tr>	  
       <th colspan="2"><br>
          <table  border='1' width='100%'  bordercolor="#F7F5F4" bgcolor="#FFFFFF">                    
               <tr>
                    <td width='29%' class="fila">&nbsp Codigo :</td>
                 <td><input type='text' name='codigo'  maxlength='4' SIZE='6' class="textbox" style='text-align:center;'>                    </td>                        
               </tr>
               <tr class="fila">
                  <td width='29%'>&nbsp Descripcion :
                  <input type='hidden' name='original' value="<%=Codigo+","+Descripcion%>"/></td>
                  <td >   <input type='text' name='descripcion' maxlength='30' class="textbox"style='width:95%;'></td>
              </tr>                    
          </table>
          <br>
      </th>
      </tr>
        <tr>
            <th height='50' colspan="2">                
                <INPUT type='submit' class="boton"onclick="Opcion.value='Nuevo';"               value='Nuevo'              style='width:25%;'>
                <INPUT type='submit' class="boton"onclick="Opcion.value='<%=NombreBoton  %>';"  value='<%=NombreBoton %>'  style='width:25%;'>
                <INPUT type='submit' class="boton"onclick="Opcion.value='<%=NombreBoton1 %>';"  value='<%=NombreBoton1 %>' style='width:25%;' >                
            </th>
        </tr>
  </table>
<br>
<input type='hidden' name='Opcion'/> 
</FORM>
<script> 
formulario.codigo.value = '<%= Codigo %>';
formulario.descripcion.value = '<%= Descripcion %>';
</script>



<center class='comentario'>


 <%if(!Mensaje.equals("")){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%}%>


<%  if(ListaGnral!=null && ListaGnral.size()>0) { %>
    <form action="<%=CONTROLLER%>?estado=Tipo_proveedor&accion=Manager" method='post' name='FormularioListado' onsubmit='jscript: return validarlistado(this);'>
        <br>

        <tr><th height='28' colspan='7'>
		<table width="50%" border="2">
          <tr>
            <td><table width="100%"  cellspacing="1" cellpadding="0">
              <tr>
                <td width="50%" class="subtitulo1">LISTADO GENERAL DE REGISTROS</td>
                <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/cuadrosverde.JPG" width="32" height="20"></td>
              </tr>
            </table>
			<table bgcolor="#F7F5F4" bordercolor="#999999" border="1" width="100%">
		<tr class="filaresaltada"align="center">
            <th width='19' align="center">N�</th>
            <th width='21' align="center"><input type='checkbox' name='All' onclick='jscript: SelAll();'></th>
            <th width='58' align="center">CODIGO</th>
            <th width='195' align="center">DESCRIPCION</th>
 
        </tr>
        <%
            int Cont = 1;
            Iterator it2 = ListaGnral.iterator();
            while(it2.hasNext()){
                Tipo_proveedor dat = (Tipo_proveedor) it2.next();
                String Estilo = (dat.getReg_status().equals("A"))?"fondoverde2":"letrafila";
                %>
        <tr class='<%= Estilo %>' bgcolor="#F7F5F4" bordercolor="#999999">
            <td  align='center'><span class='comentario'><%=Cont++%></span></td>
            <th align="center"><input type='checkbox' name='LOV' value='<%= dat.getCodigo_proveedor()+","+dat.getDescripcion() %>' onclick='jscript: ActAll();'></th>
            <td align='center' class="letraresaltada"><a href='<%=CONTROLLER%>?estado=Tipo_proveedor&accion=Manager&Opcion=Seleccionar&original2=<%= dat.getCodigo_proveedor()+","+dat.getDescripcion() %>'><%= dat.getCodigo_proveedor()  %></a></td>
            <td ><%= dat.getDescripcion()%></td>
 
        </tr>
        <%  }   %>        
        </table></td>
          </tr>
        </table></th>
        </tr>

        <br>
        <input type='hidden' name='Opcion'/> 
        <INPUT type='submit' class='boton' onclick="Opcion.value='Anular';"    value='Anular'   style='width:120;'>
        <INPUT type='submit' class='boton' onclick="Opcion.value='Activar';"   value='Activar'  style='width:120;'>
        <INPUT type='submit' class='boton' onclick="Opcion.value='Eliminar';"  value='Eliminar' style='width:120;'>
  </form>
<%  } %>

</center>
</body>
</html>
