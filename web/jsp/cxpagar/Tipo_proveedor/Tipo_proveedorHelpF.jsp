<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
  <title>Tipo de Proveedores</title>
  <META http-equiv=Content-Type content="text/html; charset=windows-1252">
  <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>
<body>
<% String BASEIMG = BASEURL +"/images/ayuda/cxpagar/tipo_proveedor/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE TIPO DE PROVEEDORES</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa para Ingresar tipo de proveedores.</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p>&nbsp;</p>
              </div></td>
          </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">En la siguiente pantalla se ingresan los datos del tipo de proveedor.</p>
            </td>
          </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>principal.JPG" border=0 ></div></td>
          </tr>
          
            <tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">Cuando se va a registrar un tipo de proveedor, el sistema verifica si el codigo del tipo de proveedor esta lleno, si no lo estan le saldr&aacute; en la pantalla el siguiente mensaje. </p>
            </td>
            </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG  src="<%=BASEIMG%>MensajeContinuarCodigo.JPG" border=0 ></div></td>
            </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">El sistema verifica si la descripción del tipo de proveedor esta lleno, si no lo estan le saldr&aacute; en la pantalla el siguiente mensaje. </p>
            </td>
            </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG  src="<%=BASEIMG%>MensajeContinuarDescripcion.JPG" border=0 ></div></td>
            </tr>	
            <tr>
              <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
                <p>Cuando se graba existosamente aparecer&aacute; el siguiente mensaje.</p></td>
            </tr>
            <tr>
              <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>MensajeAgregado.JPG" border=0 ></div></td>
            </tr>				
            <tr>
              <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
                <p>Cuando presiona el boton detalles como se muestra en la pagina principal aparecer&aacute; el listado de los tipos de proveedores y en esta  pantalla se anulan o se eliminan los datos del tipo de proveedor.. </p></td>
            </tr>
            <tr>
              <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>modificar.JPG" border=0 ></div></td>
            </tr>   
           	
            <tr>
              <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
                <p>Cuando presiona sobre un link de los tipo de proveedores aparecer&aacute; en la parte superior de la pantalla el dato para modificar el dato del tipo de proveedor seleccionado.</p></td>
            </tr>
            <tr>
              <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>modificar2.JPG" border=0 ></div></td>
            </tr>				 
 			     
			 <tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">Cuando se va a modificar un tipo de proveedor, el sistema verifica si la descripción del tipo de proveedor esta lleno, si no lo estan le saldr&aacute; en la pantalla el siguiente mensaje. </p>
            </td>
            </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG  src="<%=BASEIMG%>MensajeContinuarCodigo.JPG" border=0 ></div></td>
            </tr>		
            <tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">El sistema verifica si la descripción del tipo de proveedor esta lleno, si no lo estan le saldr&aacute; en la pantalla el siguiente mensaje. </p>
            </td>
            </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG  src="<%=BASEIMG%>MensajeContinuarDescripcion.JPG" border=0 ></div></td>
            </tr>
			   
      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
