<%--
/******************************************************************************
 * Nombre clase :                   Mensaje.jsp                               *
 * Descripcion :                    Pagina para Mostrar los Mensajes de la    *
 *                                  Anulacion de las Facturas.                *
 * Autor :                          Leonardo Parody                           *
 * Fecha Creado :                   26 de diciembre de 2005                   *
 * Modificado por:                  LREALES                                   *
 * Fecha Modificado:                17 de mayo de 2006, 04:51 PM              *
 * Version :                        1.0                                       *
 * Copyright :                      Fintravalores S.A.                   *
 *****************************************************************************/
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>
<title>Mensaje en Anular Factura</title>
   <link href="../../../css/estilostsp.css" rel='stylesheet' type="text/css">
   <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<%
String Mensaje = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";
%>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 1px;">
<jsp:include page="/toptsp.jsp?encabezado=Facturas por Pagar"/></div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: -5px; top: 101px; overflow: scroll;">
<table border="2" align="center">
  <tr>
    <td>
		<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      		<tr>
        		<td width="229" align="center" class="mensajes"><%=Mensaje%></td>
        		<td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
      		</tr>
    	</table>
	</td>
  </tr>
</table>
<br>
</p>
<center>
    <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" style="cursor:hand" onClick="window.location='<%=BASEURL%>/jsp/cxpagar/facturasrec/anularfactura.jsp?flag='" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
</center>
</div>
</body>
</html>
