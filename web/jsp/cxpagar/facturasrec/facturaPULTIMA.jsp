<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar el formulario para la 
                            insercion de documentos por pagar
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head><title>Documentos por pagar</title>
<link href="../../../css/estilostspFactura.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostspFactura.css" rel="stylesheet" type="text/css"> 
 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/CXP/cxp.js"></script>
<style type="text/css">
<!--
.Estilo1 {color: #D0E8E8}
-->
</style>
</head>

<%
   String iva =""; 
   String Modificar          =  (request.getParameter ("Modificar")==null?"":request.getParameter ("Modificar"));
     
   String maxfila            =  (request.getParameter ("maxfila")==null?"1":request.getParameter ("maxfila")); 
   int    MaxFi              =  Integer.parseInt(maxfila); 
   
   String id_agencia_usuario =  (String)session.getAttribute("id_agencia" );  
   String validar_agencia    =  (request.getParameter ("ag")==null?"":request.getParameter ("ag"));  
   
   String cod_agencia_cont   =  (String)session.getAttribute("agContable");
   String unidad_contable    =  (String)session.getAttribute("unidadC");
     
   String codigocliente      =  (request.getParameter ("cliente")==null?"":request.getParameter ("cliente")); 
   String opcion 		     =  (request.getParameter ("op")==null?"NO":request.getParameter ("op")); 
   String indice		     =  (request.getParameter ("indice")==null?"":request.getParameter ("indice")); 
   String cod_cuenta		 =  (request.getParameter ("cuenta")==null?"":request.getParameter ("cuenta")); 
%>
<body  <%if (opcion.equals("cargar")){%> onLoad="LoadCodigo(<%=indice%>, '<%=codigocliente%>', '<%=unidad_contable%>', '<%=cod_cuenta%>');cargar('<%=validar_agencia%>');" <%}else{%>onResize="redimensionar()" <%if(request.getParameter("reload")!=null){%>
        onLoad = "redimensionar();parent.opener.location.href='<%=BASEURL%>/jsp/cxpagar/facturasxpagar/facturaP.jsp?';parent.close();" 
  <%} else {%>
       onLoad = "redimensionar();cargar('<%=validar_agencia%>');";
  <%}
  }%>
>

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Documentos por Pagar"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%

	java.util.Date utilDate = new java.util.Date(); //fecha actual
    long lnMilisegundos = utilDate.getTime();
    java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(lnMilisegundos);
	
    CXP_Doc doc;
    doc = model.cxpDocService.getFactura();
						
    String subtotal=(request.getParameter ("total")==null?"":request.getParameter ("total"));
    String tipo_documento="";
    String documento= ""; 
    String proveedor="";
    String tipo_documento_rel ="";
    String documento_relacionado="";
    String fecha_documento=(""+sqlTimestamp).substring(0,10);
    String c_banco="BANCOLOMBIA";
    String b_sucursal="";
    String descripcion="";
    String observacion="";
    String o_moneda="PES";
    double vlr_neto=0;
	double vlr_total=0;
    String usuario_aprobacion="";
    int plazo =0;
	String CabIva  = "";
	String CabRiva = "";
	String CabRica = "";
	String CabRfte = ""; 
	
	Usuario usuarioLogin  = (Usuario) session.getAttribute("Usuario");
    String us  = usuarioLogin.getLogin();
    String agencia = usuarioLogin.getCia();
    String handle_code="";
    String id_mims="";
    String fecha_aprobacion="";

    String op= (request.getParameter("op")!=null )?request.getParameter("op"):"";
    String para1 = "/jsp/cxpagar/facturasxpagar/buscarnit.jsp?fechadoc="+fecha_documento+"&plazo="+plazo;
    para1=Util.LLamarVentana(para1, "Buscar Proveedor");

    String nombre_proveedor="";
	String agenteRet = "N";
	String rica      = "N";
	String rfte      = "N";
	String riva      = "N";
	
	
    if(doc!=null){
		
        tipo_documento=""+doc.getTipo_documento();
        documento= ( doc.getDocumento() != null )?doc.getDocumento():"";
        proveedor = ""+doc.getProveedor();
	    Proveedor prov  = model.proveedorService.obtenerProveedorPorNit(proveedor);
        if( prov!=null ){
		    agenteRet = prov.getC_agente_retenedor();
			rica      = prov.getC_autoretenedor_ica();
            rfte      = prov.getC_autoretenedor_rfte();
            riva      = prov.getC_autoretenedor_iva(); 
	    }
		
	    tipo_documento_rel =""+doc.getTipo_documento_rel();
        documento_relacionado=""+doc.getDocumento_relacionado();
        fecha_documento = (doc.getFecha_documento() != null )?doc.getFecha_documento() :"";
        c_banco = doc.getBanco();
		model.servicioBanco.loadSusursales(c_banco);
        b_sucursal  = doc.getSucursal();
        descripcion =  ( doc.getDescripcion() != null )?doc.getDescripcion():"";
        observacion =   ( doc.getObservacion() != null)?doc.getObservacion() :"";
        o_moneda    =  ( doc.getMoneda() != null )?doc.getMoneda():"PES";
        vlr_neto = doc.getVlr_total();
		vlr_total = doc.getVlr_neto();
		//out.println("<br><br><br><br><br><br> "+vlr_neto);
        plazo=doc.getPlazo();
        usuario_aprobacion=doc.getUsuario_aprobacion();
		CabIva  = (doc.getIva()!= null )?doc.getIva():"";
	    CabRiva = (doc.getRiva()!= null )?doc.getRiva():"";
	    CabRica = (doc.getRica()!= null )?doc.getRica():"";
	    CabRfte = (doc.getRfte()!= null )?doc.getRfte():"";
		
    }
 
   if (proveedor.equals("null")||proveedor.equals("")){
        proveedor ="";
    }
    else{
        Proveedor o_proveedor = model.proveedorService.obtenerProveedorPorNit(proveedor);
        if( o_proveedor!=null ){
	    agenteRet = o_proveedor.getC_agente_retenedor();
	    rica      = o_proveedor.getC_autoretenedor_ica();
        rfte      = o_proveedor.getC_autoretenedor_rfte();
        riva      = o_proveedor.getC_autoretenedor_iva();
       
            id_mims=""+o_proveedor.getC_idMims();

            if(!op.equals("cargarB")){
			    c_banco=""+o_proveedor.getC_branch_code();
                b_sucursal=""+o_proveedor.getC_bank_account();
							
            }
            nombre_proveedor = o_proveedor.getC_payment_name(); 
        }
    }
    model.cxpDocService.setFactura(null);
  
    
    
    TreeMap codCliAre = model.cxpDocService.llenarCodCliAre ();
    TreeMap b = model.servicioBanco.obtenerNombresBancosPorAgencia("BQ");
    TreeMap sbancos = model.servicioBanco.getSucursal();
    TreeMap t_moneda = model.monedaService.obtenerMonedas();
    TreeMap documentos = model.documentoSvc.obtenerDocumentosT();
   // TreeMap u = model.usuarioService.getUsuariosAprobacionT();
    TreeMap u = model.tblgensvc.getUsuarios_aut();
    Vector vTiposImpuestos=model.TimpuestoSvc.vTiposImpuestos();
	
	TreeMap agencias = model.agenciaService.getAgencia();
	
    int num_items=1;
   // num_items=Integer.parseInt(""+request.getParameter("num_items"));
    Vector vItems = model.cxpItemDocService.getVecCxpItemsDoc();
    String concepto ="";
	String descripcion_i="";
    String codigo_cuenta="";
    String codigo_abc="";
    String planilla="";
	String tipcliarea="";
	String codcliarea="";
	String descliarea="";
    Vector vImpuestosI;
	Vector Copia;
    double valor   = 0;
	double valor_t = 0;
    if (vItems == null){  	     
        System.out.println("el Vector de items es nulo ");
    }
    else{
        System.out.println("el Vector de items no es nullo "+ vItems.size()+" Max Fila "+MaxFi);
        for(int i=0;i < MaxFi ;i++){
            CXPItemDoc item = (CXPItemDoc)vItems.elementAt(i);
			System.out.println("ITEM - "+ item.getConcepto());
			if(item.getConcepto() != null ){
				concepto        = ""+item.getConcepto();
				descripcion_i   = ""+ item.getDescripcion();
				codigo_cuenta   = ""+ item.getCodigo_cuenta();
				codigo_abc      = ""+ item.getCodigo_abc();
				planilla        = ""+ item.getPlanilla();
				valor	        = item.getVlr_me();			
				valor_t         = item.getVlr_total();
				tipcliarea      = ""+item.getTipcliarea ();
				codcliarea      = ""+item.getCodcliarea ();
				descliarea      = ""+item.getDescliarea ();
				vImpuestosI     = item.getVItems();
				iva             = item.getIva();
				if (vImpuestosI == null){
					out.print("<br> el vector de Impuestos es null ");
				}
				
				for(int ii=0;ii<vImpuestosI.size();ii++){
					CXPImpItem impuestoItem = (CXPImpItem)vImpuestosI.elementAt(ii);
					String impuesto = impuestoItem.getCod_impuesto();
				}
				
			}
			
        }
		   
    }
	System.out.println("vector facturas");
	Vector NumeroFacturas =  model.cxpDocService.getVFacturas ();
	String estilo ="";
	String clase = "textboxFactura";
	String readonly="";
	String eventoProveedor ="buscarNit('"+validar_agencia +"',maxfila,'ENTER')";
	String eventoLupaProveedor = "buscarProveedores('"+CONTROLLER+"', '"+validar_agencia+"',maxfila);";
	System.out.println("If de modificar");
 if(Modificar.equals("si")){
     estilo ="style='border:0'";
     clase="filaresaltada";
     readonly ="readonly" ;
	 eventoProveedor ="";
	 eventoLupaProveedor="";
 }
System.out.println("Fin Jsp");
%>
<script language="javascript" src="<%= BASEURL %>/js/validarDocPorPagar.js"></script>
<script>
var controlador ="<%=CONTROLLER%>";
var maxfila = <%=maxfila%>

    function insertarItem(validar,BASEURL,Cod_Unid,tipcliarea,codCliAre,Modificar){
		
		var ii=0;
		var suma=0;
         
		var sw=false;
		
		//alert('FILAS ' + maxfila )
        for (var i=1;i<= maxfila;i++){
             //   alert("I - "+ i)
				var subtotal= document.getElementById("valorNeto"+i);
				
				if(subtotal!= null){
					
					//alert(subtotal.value)
					if(subtotal.value!=""){
							sw=true;
					}else{
					alert("Debe digitar un Valor para el Item");
					return;
					}
					
					st=document.getElementById("total");
					suma=suma+parseFloat(obtenerNumero(subtotal.value));
					st.value=formato(suma);
				}
        }
		//alert("ITEM" + ii);

		
		if(sw==true){
		  //  alert(sw);
		    maxfila++;
		    ii = maxfila;
            //forma1.imgini.style.display="block";
			var numfila = detalle.rows.length ;
			var fila = detalle.insertRow(numfila);
			//alert("FILA" + ii)
			
			  
			fila.id = "filaItem"+ii;  
		    var celda = fila.insertCell();   
			
			//fila.id='fila'+ii;
			//fila.className="fila";
			
		    var tds ="<span><a id='n_i"+ii+"' >"+ii+"</a>";
			tds+="      <input name='cod_item"+ii+"' id='cod_item"+ii+"' value='"+ii+"' type='hidden' size='4' maxlength='5' border='0'>";			        
			tds+="      <a onClick=\"insertarItem('"+validar+"','"+BASEURL+"','"+Cod_Unid+"','"+tipcliarea+"','"+codCliAre+"','"+Modificar+"')\" id='insert_item"+ii+"' style='cursor:hand' ><img src='"+BASEURL+"/images/botones/iconos/mas.gif'   width='12' height='12' ></a>";
			tds+="      <a onClick=\"borrarItem('filaItem"+ii+"')\"  value='1' id='borrarI"+ii+"' style='cursor:hand' ><img src='"+BASEURL+"/images/botones/iconos/menos1.gif'   width='12' height='12' > </a></span>";
			celda.innerHTML = tds;
			//celda.innerHTML = "<p>Alejo</p>";
			
			//celda.colSpan ='7';
			celda = fila.insertCell();
            var iva ="";
			var ItemAnterior =""; 
			for(var i=1;i <= ii; i++){
			   var aux = document.getElementById("iva"+(i)); 
			   if(aux != null){
			      iva = document.getElementById("iva"+(i));
				  ItemAnterior = i;
			   }
			}
			
			
			tds = "";
			tds +="<span><table width='100%' border='0' cellpadding='0' cellspacing='0'>";
			tds +="     <tr align='center'>";
			tds +="        <td width='333'><span><textarea name='desc"+ii+"' id='desc"+ii+"' cols='55' rows='1' class='textareaFactura' ></textarea></span></td>";
			tds +="        <td width='201'><input name='descripcion_i"+ii+"' id='descripcion_i"+ii+"'  class='textareaFactura' type='text' size='30'  maxlength='40' onKeyPress=\"BuscarConcepto('"+ii+"',event, 'decNO')\" >  <img src='"+BASEURL+"/images/botones/iconos/lupa.gif'  name='arbol"+ii+"' width='15' height='15'  id='arbol"+ii+"' style='cursor:hand' title='Buscar'  onClick=\"servicios('"+ii+"','"+controlador+"')\" ></td>";
			tds +="        <td width='108'><input name='codigo_cuenta"+ii+"' id='codigo_cuenta"+ii+"'   class='textareaFactura' type='text' size='13' maxlength='20' >";      
			tds +="           <input name='cod_cuenta"+ii+"' id='cod_cuenta"+ii+"' type='hidden' size='4' maxlength='5' border='0' value='"+Cod_Unid+"'></td>";
			tds +="        <td width='43'><input name='codigo_abc"+ii+"' id='codigo_abc"+ii+"' type='text'  class='textareaFactura' size='4' maxlength='2'></td>";
			tds +="        <td width='54'><input name='planilla"+ii+"' id='planilla"+ii+"' type='text'  class='textareaFactura' size='8' maxlength='8' onKeyUp=\"disabledPlanilla('"+ii+"','"+validar+"','"+Modificar+"')\"></td>";
			tds +="        <td width='54'><select name='cod_oc"+ii+"' id='cod_oc"+ii+"' style='width:80%;'   onClick=\" buscarCodigoOC('"+ii+"','"+BASEURL+"')\" ><option value='C' selected>C</option></select></td>    ";
			tds +="        <td width='47'><input name='oc"+ii+"' type='text' id='oc"+ii+"' size='6' maxlength='8'  class='textareaFactura'> <input name='doc"+ii+"' id='doc"+ii+"' type='hidden'></td>";
			tds +="     </tr> ";
			tds +="   </table>";
			tds +="<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
			tds +="   <tr align='center'>";
			tds +="     <td width='125'>&nbsp;</td>";
			tds +="     <td width='239'><input name='valor1"+ii+"' id='valor1"+ii+"'  class='textareaFactura' type='text' size='16' style='text-align:right;' onfocus='this.select()' onBlur='asignarImpuesto("+ii+");' onChange='formatear(this);valorTotal("+ii+");' onKeyPress=\"soloDigitos(event, 'decOK')\"  maxlength='11'  align='right'><input type='hidden' name='iva"+ii+"' id='iva"+ii+"' value="+iva.value+"></td>";
			
			
			<%  
		    for (int l=0;l<vTiposImpuestos.size();l++){
		    %>
			var impuesto = document.getElementById("impuesto<%=l%>"+(ItemAnterior)); 
			
			tds +="    <td width='97' nowrap><input name='impuesto<%=l%>"+ii+"' id ='impuesto<%=l%>"+ii+"'  class='textareaFactura'  value='"+impuesto.value+"' type='text' size='6' maxlength='10' onKeyUp=\"enter('<%=vTiposImpuestos.elementAt(l)%>','<%=l%>"+ii+"','"+BASEURL+"')\" ";
		    <%if(!agenteRet.equals("S")&& l!= 0){  
				if((vTiposImpuestos.elementAt(l).equals("RICA")) && (!rica.equals("S")) ){%>
						tds+=" readonly";
					<%}else if((vTiposImpuestos.elementAt(l).equals("RFTE")) && (!rfte.equals("S")) ){%>
						 tds+=" readonly";
					<%}else if((vTiposImpuestos.elementAt(l).equals("RIVA")) && (!riva.equals("S")) ){%>
						 tds+=" readonly";
					 <%}%>		
				<%}%>   
				   
		    tds +="	> ";
			tds +="      <img src='"+BASEURL+"/images/botones/iconos/lupa.gif'  name='<%=l%>"+ii+"' width='15' height='15'  id='imageB_<%=l%>_"+ii+"' style='cursor:hand' title='Buscar' ";  
			<%if((!agenteRet.equals("S"))||(l==0)){
					if((vTiposImpuestos.elementAt(l).equals("RICA")) && (rica.equals("S")) ){%>
						tds+=" onClick=\"funI('<%= vTiposImpuestos.elementAt(l)%>',this.name,'"+BASEURL+"');\" ";
				  <%}else if((vTiposImpuestos.elementAt(l).equals("RFTE")) && (rfte.equals("S")) ){%>
						tds+=" onClick=\"funI('<%= vTiposImpuestos.elementAt(l)%>',this.name,'"+BASEURL+"');\" ";
				  <%}else if((vTiposImpuestos.elementAt(l).equals("RIVA")) && (riva.equals("S")) ){%>
						 tds+=" onClick=\"funI('<%= vTiposImpuestos.elementAt(l)%>',this.name,'"+BASEURL+"');\" ";
				  <%}else if(vTiposImpuestos.elementAt(l).equals("IVA")){%>
						 tds+=" onClick=\"funI('<%= vTiposImpuestos.elementAt(l)%>',this.name,'"+BASEURL+"');\" ";
				  <%}		
              }%>
			
			tds +="     ><input name='tipo_impuesto<%=l%>"+ii+"' id ='tipo_impuesto<%=l%>"+ii+"' type='hidden' size='10' value='<%=vTiposImpuestos.elementAt(l)%>'></td>";
				  
				  
			<%}%>                       
			tds +="     <td width='143'><input name='valorNeto"+ii+"' id='valorNeto"+ii+"'  class='textareaFactura' type='text' size='16'  style='text-align:right;' onChange='formatear(this);' onKeyPress=\"soloDigitos(event, 'decNO')\"  maxlength='11' align='right' readonly></td>";
			tds +="</tr></table>";       
			
			
			 
			
//			alert (tds);
			celda.innerHTML = tds;
			organizar();
			
			}
			else{
			  alert("Debe digitar un Valor para el Item");
			}		
	}
	
	
	
	function borrarItem(indice){
	       // alert("filas = "+detalle.rows.length+", indice = "+indice);	
//			indice = detalle.rows.length;
          
            var tabla = document.getElementById("detalle");
			//alert("indice= "+indice);
			if ( tabla.rows.length <= 2 ){
				return;
			}
			
			var fila = document.getElementById(indice);
			//alert(fila);
		    tabla.deleteRow(fila.rowIndex);				
				
			
			var suma = 0;
			st = document.getElementById("total");
			//alert(detalle.rows.length);	
			for (var i = 1;i <= maxfila + 1 ; i++){

				var subtotal = document.getElementById("valorNeto"+i);		
				if ( subtotal != null ){
					//alert(valor + " " + i );
					var valor = subtotal.value.replace( new RegExp(",","g"), "");
					
					if (!isNaN ( parseFloat(valor)) ){
						suma += parseFloat(valor);
					}					  
				}
			}
			st.value = formato(suma);			
			organizar();

			
			/*
			var vlr_total = 0;
		    var idItem = indice;
            with (forma1){
              for (i=0; i<elements.length; i++){                  
				  if (elements[i].name.indexOf('valor1')==0){
                     var valor = elements[i].value.replace( new RegExp(",","g"), "");
                     if (!isNaN ( parseFloat(valor)) ){
                        vlr_total += parseFloat(valor);						
                     }					 
                  }
              }
          }		
		forma1.vlr_neto.value =  formato(vlr_total);*/
	}
	
	function organizar(){
 	    var cont =1;
		for (var i = 1,n = 1;i <= maxfila + 1 ; i++){
			var x = document.getElementById("n_i"+i);
			var y = document.getElementById("filaItem"+i);
			if ( x != null ){
				x.innerText = ""+n;
				n++;
			}
			if( y != null ){
				if( cont % 2 == 0){
				   y.className = "filagrisFac";
				}else{
				   y.className = "filaFactura";
				}
				cont++;
			}
		}
	}
	
	 
	
	/* function formato(numero){
          
           var tmp = parseFloat(numero) ;
		   var factor = (tmp < 0 ? - 1 : 1);
           tmp *= factor;
          
           var num = '';
           var pos = 0;
           while(tmp>0){
                  if (pos%3==0 && pos!=0) num = ',' + num;
                  res  = tmp % 10;
                  tmp  = parseInt(tmp / 10);
                  num  = res + num  ;
                  pos++;
           }
           return (factor==-1 ? '-' : '' ) + num ;
        }*/
	function formato(num){
	   var decimales =2;
	    var nums = ( new String (num) ).split('.');
		var salida = new String();
		for (var i=nums[0].length-1, j=0; i>=0; salida = nums[0].charAt(i) + (j%3==0 && j!=0? ',':'') + salida , i--, j++);
		return rellenar(salida + (nums.length > 1 && decimales > 0 ? '.' + (nums[1].substr(0, (nums[1].length>decimales?decimales:nums[1].length))) : ''));


	}  
	
	   
        
        function sinformato(element){
           return element.replace( new RegExp(",","g") ,'');
        }
		
		function disabledPlanilla(x, validar,Modificar ){
		  var comboPlanilla = document.getElementById('cod_oc'+x);
		  var planilla      = document.getElementById('planilla'+x);
		  var descripcion   = document.getElementById('descripcion_i'+x);
		  if( planilla.value.length != 0 )
		  	comboPlanilla.disabled = true;		
		  else  
		    comboPlanilla.disabled = false;		
			if (window.event.keyCode==13){ 
			    if( descripcion.value.length != 0 ){
					BuscarAccountCode( planilla.value, x, validar,Modificar );
				}
				else{
					alert('Debe seleccionar un concepto de pago');
				}
			}
		}
		
		function BuscarAccountCode( numpla, x , validar, Modificar ){
			var sele   =   document.getElementById("codigo_cuenta"+x);	
		    var hidden =   document.getElementById("cod_cuenta"+x);							
			var cuenta =   sele.value;
			document.forma1.action = controlador+"?estado=FacturaSearch&accion=CodigoCuenta&numpla="+numpla+"&indice="+x+"&hidden="+hidden.value+"&cuenta="+cuenta+"&validar="+validar+"&maxfila="+maxfila+"&Modificar="+Modificar; 
			document.forma1.submit(); 
		} 
	
	 function buscarNit (validar,maxfila,OP){
	    
		if (window.event.keyCode==13){ 
			var ii=0;
			for (var i=0;i< detalle.rows.length;i++){
				ii=ii+1;
			}
			var num_items = document.getElementById("num_items");
			num_items.value=""+(ii-1);
			var url="";
			url = controlador+"?estado=Factura&accion=AProveedores&validar="+validar+"&maxfila="+maxfila+"&OP="+OP;
			for (var x=0;x<document.forma1.length;x++){
				url =  url +"&"+ document.forma1.elements[x].name + "=" + document.forma1.elements[x].value;
			}
			
			document.forma1.action = url; 
			document.forma1.submit();
		}
    }
	
	
	var isIE = document.all?true:false;
    var isNS = document.layers?true:false;
	function BuscarConcepto(x,value,e,decReq){
		var concepto = document.getElementById("descripcion_i"+x);
		if (window.event.keyCode==13){ 
			if(concepto.value.length == 4){
			  
		      var url = controlador+"?estado=Factura&accion=Servicios&concepto="+concepto.value+"&OP=ENTER&Id="+x;
		      window.open(url,'Trafico','width=400,height=520,scrollbars=no,resizable=yes,top=10,left=65,status=yes');
			 
			}
			
		}
		soloDigitos(e,decReq);
		
		
	}
	
  function asignarImpuesto(i){
      var campo    = document.getElementById("impuesto0"+i);//este es el campo iva
	  var ValorIva = document.getElementById("iva"+i); 
	  if(campo.value!="" && ValorIva.value!="" ){
		  var ValorIva = document.getElementById("iva"+i); 
		  //valor del item
		  var valor    = document.getElementById("valor1"+i);
		  //valor neto del item
		  var vlrN     = document.getElementById("valorNeto"+i);
		  var vlr      = parseFloat(ValorIva.value.replace( new RegExp(",","g"), ""));
		  
		  //valor total item 
		  var vlrTotal = parseFloat(valor.value.replace( new RegExp(",","g"), ""));
		  var vlrNeto  = parseFloat(vlrN.value.replace( new RegExp(",","g"), ""));
		 
		  vlrN.value   = formatoDecimal( vlrTotal + (( vlrTotal * vlr )/100) );
		  
		
		  
		  
		  var form = document.getElementById("forma1");
		  var vlr_total = 0;
		  
		  for (j=0; j<form.elements.length; j++){                  
			if (form.elements[j].name.indexOf('valorNeto')==0){
				var valor = form.elements[j].value.replace( new RegExp(",","g"), "");
					if (!isNaN ( parseFloat(valor)) ){
						vlr_total += parseFloat(valor);
					 }					 
			 }
		  }
		  form.total.value =  formatoDecimal(vlr_total);
	  }else{
	      valorTotal(i);
	  }

  }
	

</script>
<form name="forma1" id="forma1" action="" method="post"> 
<%
  String mensaje=""+request.getParameter("ms");
    if (! (mensaje.equals("")|| mensaje.equals("null"))){
%>
<input type="hidden" name="Modificar"  id="Modificar" value="<%=Modificar%>">
<table border="2" align="center">
  <tr>
    <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="282" align="center" class="mensajes"><%=mensaje%></td>
          <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="78">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<br>
<%
    }   
%>
     


<table width="960" align="center">
<tr><td>
<table border="2"  >
  <tr >
      <td> 
        <table width="100%" align="center"  >
          <tr >
            <td width="50%"  class="subtitulo1"><p align="left">Documento</p></td>
            <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" class="Estilo1" height="20" align="left"><%=datos[0]%></td>
          </tr>
        </table>
        <table width="100%" align="center" cols="7">
          <tr class="filaFactura">
            <td width="103"  >Documento:</td>
            <td width="117" >
                <input:select name="tipo_documento"      attributesText=" id='documentos' style='width:80%;' onChange='docRel(this.value);'"  default="010" options="<%=documentos%>" />
            </td>
            <td width="103" ><input name="documento" type="text" class="<%=clase%>" onfocus='this.select()' onKeyPress="soloAlfa(this)"  id="documento" <%=readonly%> size="20" maxlength="30" value="<%=documento%>" <%=estilo%>>            </td>
            <td width="72" >Proveedor:</td>
            <td width="169" ><input name="proveedor"  type="text" class="<%=clase%>" value="<%=proveedor%>" onfocus='this.select()' id="proveedor" size="18" maxlength="15"  <%=readonly%> onKeyUp="<%=eventoProveedor%>" <%=estilo%> >
			              &nbsp;&nbsp;
						  <% if(!Modificar.equals("si")){%>
						  <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  width="15" height="15"  onClick="<%=eventoLupaProveedor%>" title="Buscar" style="cursor:hand" >
						  <%}%>
						  <input type="hidden" name="ValidarProv" value="<%=proveedor%>"></td>
            <td colspan="3" nowrap>
            <input type="hidden" name="nombre_proveedor" id="nombre_proveedor" size="50" class="textbox" value='<%=nombre_proveedor%>'> 
            <div id="nombreP">&nbsp;&nbsp;<%=nombre_proveedor%></div></td>
            </tr>
		  <tr class="filaFactura">
		  	<td >Documento Rel:</td>
			<td >
           		<input:select name="tipo_documento_rel"      attributesText=" id='tipo_documento_rel' style='width:80%;'  disabled"  default="010" options="<%=documentos%>" />
			</td>
			<td >
			 
			  <select name="documento_relacionado" id="documento_relacionado"   disabled>
			  <%   
			    if (NumeroFacturas!= null){
			     for(int i=0;i<NumeroFacturas.size();i++){%>
			       <option value ="<%=NumeroFacturas.elementAt(i)%>><%=NumeroFacturas.elementAt(i)%>"</option>
			  <% }
			  }%> 
			  </select>
			
			</td>
			<td >Fecha :</td>
			<td colspan="2"  ><input name="fecha_documento" type="text" id="fecha_documento2" size="11" value="<%=fecha_documento%>">
              <span class="comentario"></span> <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.forma1.fecha_documento);return false;" hidefocus> <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a>			</td>				
			<td width="121" colspan="-1" >Plazo(d&iacute;as):</td>
			<td width="266" colspan="-1" ><input name="plazo" type="text"  value="<%=plazo%>"   id="plazo2" size="4" maxlength="3"></td>
			<script>
			forma1.plazo.value = '<%=plazo%>';
			</script>
		  </tr>
		  <tr class="filaFactura">
		  	<td><p>Valor:</p></td>
			<td ><input name="vlr_neto" type="text"  class="textboxFactura" onKeyPress="soloDigitos(event, 'decOK')"  onfocus='this.select()' onChange="formatear(this)"  id="vlr_neto3"  value='<%=UtilFinanzas.customFormat2(vlr_neto)%>' maxlength="10"  align="right" style="text-align:right;"></td>
			<td ><input name="total" type="text" id="total4" value='<%= UtilFinanzas.customFormat2(vlr_total) %>'   readonly align="right"  class="filaresaltada" style="border:0; text-align:right; width:120;"></td>
			<td >Moneda:</td>
			<td colspan="2" ><input:select name="moneda" attributesText=" id='moneda' style='width:80%;'  "  default="<%=o_moneda%>" options="<%=t_moneda%>" /></td>		                     
			<td colspan="-1" >Banco:</td>	
			<td colspan="-1" ><input:select name="c_banco"      attributesText="<%=" id='c_banco' style='width:150px;'   onChange=\\" bancos('"+CONTROLLER+"','"+validar_agencia+"',maxfila,'"+Modificar+"'); \\" "%>"  default="<%=c_banco%>" options="<%=b%>" /></td>		
			<script>
			 forma1.moneda.value ='<%=o_moneda%>';
			 forma1.c_banco.value = '<%=c_banco%>';
			</script>
  		</tr>
		  <tr class="filaFactura">
		  	  <td >Descripci&oacute;n:</td>
			  <td colspan="5"><textarea name="descripcion" cols="70" rows="1" class="textareaFactura"><%=descripcion%></textarea></td>
				<td colspan="-1" class="filaFactura">Sucursal:</td>
			    <td colspan="-1" ><input:select name="c_sucursal"    attributesText=" id='c_sucursal' style='width:80%;' "  options="<%=sbancos%>" /></td>		
				<script>forma1.c_sucursal.value = '<%=b_sucursal%>';</script>
	      </tr>
  		
		    <tr>
		  	  <td rowspan="2" class="filaFactura">Observaci&oacute;n: </td>
			  <td colspan="5" rowspan="2" class="fila"><table width="100%">
                <tr class="filaFactura">
                  <td width="58%" rowspan="2"><textarea name="observacion" cols="70" rows="3" class="textboxFactura"><%=observacion%></textarea></td>
                  <td width="9%">IVA</td>
                  <td width="13%" nowrap><input name="CabIva" id ="CabIva" type="text" size="6" maxlength="10"  onKeyUp="enterImpALL('IVA','CabIva','<%=BASEURL%>',maxfila);" value="<%=CabIva%>">
&nbsp;<img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name="" width="15" height="15"  id="imageB" style="cursor:hand" title="Buscar"  onClick="funImpALL('IVA','CabIva','<%=BASEURL%>','',maxfila);"> </td>
                  <td width="6%">RIVA</td>
                  <td width="14%" nowrap><input name="CabRiva" id ="CabRiva" type="text" size="6" maxlength="10"  value="<%=CabRiva%>" onKeyUp="enterImpALL('RIVA','CabRiva','<%=BASEURL%>',maxfila);" <%if(!agenteRet.equals("S")&&(!riva.equals("S")) ){%>readonly<%}%>>
                    &nbsp;<img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name="" width="15" height="15"  id="imageB" style="cursor:hand" title="Buscar"  <%if((!agenteRet.equals("S"))&&(riva.equals("S"))){ %>onClick="funImpALL('RIVA','CabRiva','<%=BASEURL%>','',maxfila);" <%}%>></td>
                </tr>
                <tr class="filaFactura">
                  <td>RICA</td>
                  <td nowrap><input name="CabRica" id ="CabRica" type="text" size="6" maxlength="10"   value="<%=CabRica%>" onKeyUp="enterImpALL('RICA','CabRica','<%=BASEURL%>',maxfila);" <%if(!agenteRet.equals("S")&&(!rica.equals("S")) ){%>readonly<%}%>>
&nbsp;<img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name="" width="15" height="15"  id="imageB" style="cursor:hand" title="Buscar" <%if((!agenteRet.equals("S"))&&(rica.equals("S"))){ %>onClick="funImpALL('RICA','CabRica','<%=BASEURL%>','',maxfila);" <%}%>></td>
                  <td>RFTE </td>
                  <td nowrap><input name="CabRfte" id ="CabRfte" type="text" size="6" maxlength="10"   value="<%=CabRfte%>" onKeyUp="enterImpALL('RFTE','CabRfte','<%=BASEURL%>',maxfila);" <%if(!agenteRet.equals("S")&&(!rfte.equals("S")) ){%>readonly<%}%>>
                    &nbsp;<img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name="" width="15" height="15"  id="imageB" style="cursor:hand" title="Buscar" <%if((!agenteRet.equals("S"))&&(rfte.equals("S"))){ %>onClick="funImpALL('RFTE','CabRfte','<%=BASEURL%>','',maxfila);" <%}%>></td>
                </tr>
              </table></td>
			  <td colspan="-1" class="filaFactura">Autorizador: </td>
			  <td colspan="-1" class="filaFactura" ><input:select name="usuario_aprobacion" attributesText="style='width:80%;' " default="<%=usuario_aprobacion%>" options="<%=u%>" /></td>
		    </tr>
		    <tr class="filaFactura">
		      <td height="26" colspan="-1" id="ag">Agencia :</td>
		      <td colspan="-1"><input:select name="agencia" attributesText="<%=" id='agencia' style='width:80%;'  onChange=\\" BuscarAgencias('"+CONTROLLER+"',maxfila,'"+Modificar+"'); \\" "%>"  options="<%=agencias%>" /></td>
			  <script>forma1.agencia.value = '<%=id_agencia_usuario%>';</script>
		    </tr>
	  </table></td>
</tr>
</table>
	<table width="100%" border="2" align="left">
		<tr>
		<td >
			<table width="100%" align="center"  >
          <tr >
            <td width="50%"  class="subtitulo1"><p align="left">Detalle del Documento </p></td>
            <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" class="Estilo1">
              <input name="num_items" id="num_items4" type="hidden" size="16" value='1' maxlength="16"></td>
          </tr>
        </table>
			<div align="left">
			<table id="detalle" width="100%" >
				<tr  id="fila1" class="tblTituloFactura">
					<td align="center">Item</td>
					<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr align="center"  class="tblTituloFactura">
                        <td width="332">Descripcion</td>
                        <td width="197">Concepto</td>
                        <td width="108">Cuenta</td>
                        <td width="47">ABC</td>
                        <td width="58">Planilla</td>
                        <td width="47">Tipo</td>
                        <td width="51">Centro</td>
                      </tr>
                    </table>
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr align="center" class="tblTituloFactura">
                          <td width="125">&nbsp;</td>
                          <td width="239">Valor</td>
                          <%for(int i=0;i< vTiposImpuestos.size();i++){ %>
					             <td width="97"  nowrap><div align="center"><%=vTiposImpuestos.elementAt(i)%> </div></td>
				              <%}%>
    					  <td width="143">Valor Neto</td>
                        </tr>
                      </table></td>
					</tr>
				
				<% int x=1;
				if ( vItems == null){%>
				 <tr class="fila" id="filaItem<%=x%>" nowrap  bordercolor="#D1DCEB" >
				    <td nowrap><a id="n_i<%=x%>" ><%=x%></a>
				        <input name="cod_item<%=x%>" id="cod_item<%=x%>" value="<%=x%>" type="hidden" size="4" maxlength="5" border="0">
			          <a onClick="insertarItem('<%=validar_agencia%>', '<%=BASEURL%>','<%=cod_agencia_cont + "," + unidad_contable%>','<%=tipcliarea%>','<%=codCliAre%>','<%=Modificar%>');"  id="insert_item<%=x%>" style="cursor:hand" ><img src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width="12" height="12"  ></a>
					  <a onClick="borrarItem('filaItem<%=x%>');"  value="1" id="borrarI<%=x%>" style="cursor:hand" ><img id="imgini" name="imgini" src='<%=BASEURL%>/images/botones/iconos/menos1.gif'   width="12" height="12"  > </a></span></td>
					     
				    <td>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
                      <tr align="center">
                        <td width="333"><span>
						<textarea name="desc<%=x%>" id="desc<%=x%>" cols="55" rows="1" class="textboxFactura" ></textarea>
						</span></td>
                        <td width="201"><input name="descripcion_i<%=x%>" id="descripcion_i<%=x%>" type="text" size="30" class="textboxFactura"  maxlength="4"  onKeyPress="BuscarConcepto('<%=x%>',event, 'decNO');">                          <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name="arbol<%=x%>" width="15" height="15"  id="arbol<%=x%>" style="cursor:hand" title="Buscar"  onClick="servicios('<%=x%>','<%=CONTROLLER%>');" ></td>
                        <td width="108"><input name="codigo_cuenta<%=x%>" id="codigo_cuenta<%=x%>" class="textboxFactura" type="text" size="13" maxlength="20" >                            
                          <input name="cod_cuenta<%=x%>" id="cod_cuenta<%=x%>"       type="hidden" size="4" maxlength="5" border="0" value="<%=cod_agencia_cont + "," + unidad_contable%>"></td><td width="43"><input name="codigo_abc<%=x%>" id="codigo_abc<%=x%>" type="text" size="4" maxlength="2"></td>
                        <td width="54"><input name="planilla<%=x%>" id="planilla<%=x%>" type="text" size="8"  class="textboxFactura"  maxlength="8" onKeyUp="disabledPlanilla('<%=x%>', '<%=validar_agencia%>', '<%=Modificar%>')"></td>
                        <td width="54"><select name="cod_oc<%=x%>" id="cod_oc<%=x%>" style='width:80%;'   onClick=" buscarCodigoOC('<%=x%>','<%=BASEURL%>') " ><option value="C" selected>C</option></select></td>
                        <td width="47"><input name="oc<%=x%>" type="text" id="oc<%=x%>" size="6" maxlength="8" class="textboxFactura" >
                          <input name="doc<%=x%>" id="doc<%=x%>" type="hidden"></td>
                      </tr>
                    </table>
				      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                         <tr align="center">
                            <td width="125">&nbsp;</td>
                            <td width="239"><input name="valor1<%=x%>"  onFocus='this.select()' class="textboxFactura"   id="valor1<%=x%>" type="text" size="16"  style="text-align:right;" onBlur="asignarImpuesto(<%=x%>)" onChange="formatear(this);valorTotal(<%=x%>);" onKeyPress="soloDigitos(event, 'decOK')"  maxlength="11"  align="right"></td>
                            <input type="hidden" name="iva<%=x%>" id="iva<%=x%>">
							<%for(int l = 0;l < vTiposImpuestos.size();l++){ %>
                            <td width="97" nowrap><input name="impuesto<%=l%><%=x%>" id ="impuesto<%=l%><%=x%>" type="text" size="6" maxlength="10"  onKeyUp="enter('<%=vTiposImpuestos.elementAt(l)%>','<%=l%><%=x%>','<%=BASEURL%>');" 
							<%if(!agenteRet.equals("S")&& l!= 0){  
                                if((vTiposImpuestos.elementAt(l).equals("RICA")) && (!rica.equals("S")) ){%>
                                         readonly
                                    <%}else if((vTiposImpuestos.elementAt(l).equals("RFTE")) && (!rfte.equals("S")) ){%>
                                         readonly
                                    <%}else if((vTiposImpuestos.elementAt(l).equals("RIVA")) && (!riva.equals("S")) ){%>
                                          readonly
                                     <%}%>		
                                <%}%>
							 >
							 
                            <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name="<%=l%><%=x%>" width="15" height="15"  id="imageB_<%=l%>_<%=x%>" style="cursor:hand" title="Buscar"  
                            <%if((!agenteRet.equals("S"))||(l==0)){ 
                                if((vTiposImpuestos.elementAt(l).equals("RICA")) && (rica.equals("S")) ){%>
                                           onClick="funI('<%=vTiposImpuestos.elementAt(l)%>',this.name,'<%=BASEURL%>');"
                                    <%}else if((vTiposImpuestos.elementAt(l).equals("RFTE")) && (rfte.equals("S")) ){%>
                                         onClick="funI('<%=vTiposImpuestos.elementAt(l)%>',this.name,'<%=BASEURL%>');"
                                    <%}else if((vTiposImpuestos.elementAt(l).equals("RIVA")) && (riva.equals("S")) ){%>
                                          onClick="funI('<%=vTiposImpuestos.elementAt(l)%>',this.name,'<%=BASEURL%>');"	
                                    <%}else if(vTiposImpuestos.elementAt(l).equals("IVA")){%>
                                          onClick="funI('<%=vTiposImpuestos.elementAt(l)%>',this.name,'<%=BASEURL%>');"
                                     <%}%>		
                                <%}%> ><input name="tipo_impuesto<%=l%><%=x%>" id ="tipo_impuesto<%=l%><%=x%>" type="hidden" size="10" value="<%=vTiposImpuestos.elementAt(l)%>"></td>
                              <%}%> 
                             <td width="143"><input name="valorNeto<%=x%>" id="valorNeto<%=x%>" class="textboxFactura"  style="text-align:right;" type="text" size="16"  onChange="formatear(this);" onKeyPress="soloDigitos(event, 'decNO')"  maxlength="11" align="right" readonly></td>
						</tr>
                      </table></td>
				    </tr>   
				<% }
				else{
				 System.out.println("Los Items en la pagina no son null y tienen tama�o "+ vItems.size());
				
				for( x=1;x<=MaxFi;x++){
				                CXPItemDoc item = (CXPItemDoc)vItems.elementAt(x-1);
				  if(item.getConcepto() != null ){
                      
                                    concepto     =""+ item.getConcepto(); 
									descripcion_i=""+ item.getDescripcion();
                                    codigo_cuenta=""+ item.getCodigo_cuenta();
                                    codigo_abc=""+ item.getCodigo_abc();
                                    planilla=""+ item.getPlanilla();
									tipcliarea=""+item.getTipcliarea ();
									codcliarea=""+item.getCodcliarea ();
									descliarea=""+item.getDescliarea ();
                                    valor	=	item.getVlr_me();			
									valor_t = 	item.getVlr_total();
									iva     =   item.getIva();
				%>
                <tr class="<%=(x%2!=0)?"fila":"filagris"%>" id="filaItem<%=x%>" nowrap  bordercolor="#D1DCEB" >
                  <td class="bordereporte"> <a id="n_i<%=x%>" ><%=x%></a>
                      <input name="cod_item<%=x%>" id="cod_item<%=x%>" value="<%=x%>" type="hidden" size="4" maxlength="5" border="0">
                      <%//if( x == 1 ){ %>
                      <a onClick="insertarItem('<%=validar_agencia%>','<%=BASEURL%>','<%=cod_agencia_cont + "," + unidad_contable%>','<%=tipcliarea%>','<%=codCliAre%>','<%=Modificar%>');"  id="insert_item<%=x%>" style="cursor:hand" ><img src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width="12" height="12"  >
					  </a> <a onClick="borrarItem('filaItem<%=x%>');valorTotal('<%=x%>');"  value="1" id="borrarI<%=x%>" style="cursor:hand" ><img id="imgini" src='<%=BASEURL%>/images/botones/iconos/menos1.gif'   width="12" height="12"  ></a>
                      <%//}%>
                  </td>
                  <td>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                      <tr align="center">
                        <td width="333"><span><span class="bordereporte">
                          <textarea name="desc<%=x%>" id="desc<%=x%>" cols="55" rows="1" class="textareaFactura"  ><%=descripcion_i%></textarea>
                        </span>
                            </span></td>
                        <td width="201"><input name="descripcion_i<%=x%>" id="descripcion_i<%=x%>" type="text" size="30" class="textboxFactura"  value='<%=concepto%>' maxlength="40" onKeyPress="BuscarConcepto('<%=x%>',event, 'decNO');">
                          <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name="arbol<%=x%>" width="15" height="15"  id="arbol<%=x%>" style="cursor:hand" title="Buscar"  onClick="servicios('<%=x%>','<%=CONTROLLER%>');" > </td>
                        <td width="108"><input name="codigo_cuenta<%=x%>" id="codigo_cuenta<%=x%>"  class="textboxFactura" type="text" size="13" value='<%=codigo_cuenta%>' maxlength="20" >
                          <input name="cod_cuenta<%=x%>" id="cod_cuenta<%=x%>" value="<%=cod_agencia_cont+","+unidad_contable%>" type="hidden" size="4" maxlength="5" border="0"></td>
                        <td width="43"><input name="codigo_abc<%=x%>" id="codigo_abc<%=x%>" type="text" size="4" class="textboxFactura"  value="<%=codigo_abc%>" maxlength="2"></td>
                        <td width="54"><input name="planilla<%=x%>" id="planilla<%=x%>" type="text" size="8"class="textboxFactura"   value="<%=planilla%>" maxlength="8" onKeyUp="disabledPlanilla('<%=x%>', '<%=validar_agencia%>','<%=Modificar%>')"></td>
                        <td width="54"><select name="cod_oc<%=x%>" id="cod_oc<%=x%>" style='width:80%;'   onClick=" buscarCodigoOC('<%=x%>','<%=BASEURL%>') " >
                            <option value="C" selected>C</option>
                        </select></td>
                        <td width="47"><input name="oc<%=x%>" type="text" id="oc<%=x%>" size="6" maxlength="8"  class="textboxFactura"  value="<%=codcliarea%>"  title="<%=descliarea%>"readonly>                            
                          <input name="doc<%=x%>" type="hidden" id="doc<%=x%>" value="<%=descliarea%>"></td></tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr align="center">
                        <td width="125">&nbsp;</td>
                        <td width="239"><input name="valor1<%=x%>" id="valor1<%=x%>" onFocus='this.select()'  class="textboxFactura" style="text-align:right;"  onBlur="asignarImpuesto(<%=x%>)" onKeyPress="soloDigitos(event, 'decNO')"  type="text"  onChange="formatear(this);valorTotal(<%=x%>);" size="16" value="<%=UtilFinanzas.customFormat2(valor)%>" maxlength="11" align="right"></td>
                        <input type="hidden" name="iva<%=x%>" id="iva<%=x%>" value="<%=iva%>">
                        <%
                            Copia = item.getVCopia();
                            for(int i=0;i< Copia.size();i++){ 
                                CXPImpItem impuestoCopia = (CXPImpItem)Copia.elementAt(i);
                                String impuesto = impuestoCopia.getCod_impuesto();
                                String tipo 	= impuestoCopia.getTipo_impuesto();
                        %>
                        <td width="97" nowrap>
                             <input name="impuesto<%=i%><%=x%>" id ="impuesto<%=i%><%=x%>" type="text" size="6" value="<%=impuesto%>" maxlength="10" onKeyUp="enter('<%=vTiposImpuestos.elementAt(i)%>','<%=i%><%=x%>','<%=BASEURL%>');" 
							 <%if(!agenteRet.equals("S")&& i!= 0){ 
                                if((vTiposImpuestos.elementAt(i).equals("RICA")) && (!rica.equals("S")) ){%>
                                         readonly
                                    <%}else if((vTiposImpuestos.elementAt(i).equals("RFTE")) && (!rfte.equals("S")) ){%>
                                         readonly
                                    <%}else if((vTiposImpuestos.elementAt(i).equals("RIVA")) && (!riva.equals("S")) ){%> 
                                          readonly
                                     <%}%>		
                                <%}%>
							 >
                             <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'    name="<%=i%><%=x%>" width="15" height="15"   id="imageB_<%=i%>_<%=x%>" style="cursor:hand" title="Buscar" 
                            <%if((!agenteRet.equals("S"))||(i==0)){
                                  if((vTiposImpuestos.elementAt(i).equals("RICA")) && (rica.equals("S")) ){%>
                                      onClick="funI('<%=vTiposImpuestos.elementAt(i)%>',this.name,'<%=BASEURL%>');"
                                <%}else if((vTiposImpuestos.elementAt(i).equals("RFTE")) && (rfte.equals("S")) ){%>
                                       onClick="funI('<%=vTiposImpuestos.elementAt(i)%>',this.name,'<%=BASEURL%>');"
                                <%}else if((vTiposImpuestos.elementAt(i).equals("RIVA")) && (riva.equals("S")) ){%>
                                       onClick="funI('<%=vTiposImpuestos.elementAt(i)%>',this.name,'<%=BASEURL%>');"	
                                <%}else if(vTiposImpuestos.elementAt(i).equals("IVA")){%>
                                       onClick="funI('<%=vTiposImpuestos.elementAt(i)%>',this.name,'<%=BASEURL%>');"
                                <%}%>	
                            <%}%>  ><input name="tipo_impuesto<%=i%><%=x%>" id ="tipo_impuesto<%=i%><%=x%>3" type="hidden"size="10" value="<%=vTiposImpuestos.elementAt(i)%>">
                            </td>
                        <%}%>
                        <td width="143"><input  name="valorNeto<%=x%>" id="valorNeto<%=x%>" class="textboxFactura"   style="text-align:right;" type="text" size="16"  onChange="formatear(this);" onKeyPress="soloDigitos(event, 'decNO')"  maxlength="11" align="right" value="<%=UtilFinanzas.customFormat2(valor_t)%>" readonly></td>
                      </tr>
                  </table></td>
                </tr>  
            <%  }
			  }%>
			   <script>
			    organizar();
			   </script>
            <%}%>
			</table>
		  </div>		  </td>
		</tr>
	</table>
</td>
</tr>
</table>
  <tr>

    <div align="center">
	   <%if(Modificar.equals("si")){%>
	      <img src='<%=BASEURL%>/images/botones/agregar.gif' name='Buscar' align="absmiddle" style='cursor:hand'   onClick="insertarItem('<%=validar_agencia%>','<%=BASEURL%>','<%=cod_agencia_cont + "," + unidad_contable%>','<%=tipcliarea%>','<%=codCliAre%>','<%=Modificar%>');" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
	      &nbsp;<img src='<%=BASEURL%>/images/botones/modificar.gif' name='Buscar' align="absmiddle" style='cursor:hand'   onclick="validarDocumento('<%=CONTROLLER%>', '<%=validar_agencia%>',maxfila,'<%=Modificar%>');" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
	      &nbsp;<img src='<%=BASEURL%>/images/botones/salir.gif' name='Buscar' align="absmiddle" style='cursor:hand'   onClick="window.close();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
		  
	   <%}else{%>
		<img src='<%=BASEURL%>/images/botones/iconos/guardar.gif' name='Buscar' width="25" height="25" align="absmiddle" style='cursor:hand' title='Guardar...'  onclick="escribirArchivo('<%=CONTROLLER%>',maxfila);" >
		&nbsp;<img src='<%=BASEURL%>/images/botones/agregar.gif' name='Buscar' align="absmiddle" style='cursor:hand'   onClick="insertarItem('<%=validar_agencia%>','<%=BASEURL%>','<%=cod_agencia_cont + "," + unidad_contable%>','<%=tipcliarea%>','<%=codCliAre%>','<%=Modificar%>');" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
		 &nbsp;<img src='<%=BASEURL%>/images/botones/aceptar.gif' name='Buscar' align="absmiddle" style='cursor:hand'   onclick="validarDocumento('<%=CONTROLLER%>', '<%=validar_agencia%>',maxfila,'<%=Modificar%>');" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
        &nbsp;<img src='<%=BASEURL%>/images/botones/restablecer.gif' name='restablecer' align="absmiddle" style='cursor:hand'   onClick="Restablecer('<%=CONTROLLER%>');"  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
		&nbsp;<img src='<%=BASEURL%>/images/botones/salir.gif' name='Buscar' align="absmiddle" style='cursor:hand'   onClick="window.close();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
		
		<%}%>
    </div>
  <tr>
  </form>
</div>
</body>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
<%=datos[1]%>
</html>

<script>
    document.forma1.documento.focus();
	window.resizeTo(1024,745);
	window.moveTo(0,0);
</script>
    