<!--
- Autor : Ing. Ivan Gomez
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar el formulario para la 
                            grabacion del comprobante contable
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
    <head><title>Documentos por pagar</title>
        
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        
        <style type="text/css">
            <!--
            .Estilo1 {color: #D0E8E8}
            -->
        </style>
    </head>

<%
     
   String maxfila            =  (request.getParameter ("maxfila")==null?"1":request.getParameter ("maxfila")); 
   int    MaxFi              =  Integer.parseInt(maxfila); 
   
  %>
    <body onresize="redimensionar()" >

        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Grabación del comprobante Contable"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

            <script language="javascript" src="<%= BASEURL %>/js/validarDocPorPagar.js"></script>
            <script>
                var controlador ="<%=CONTROLLER%>";
                var maxfila = <%=maxfila%>

                function insertarItem(validar,BASEURL,Cod_Unid,tipcliarea,codCliAre,Modificar){
		
                var ii=0;
                var suma=0;
         
                var sw=false;
		
                //alert('FILAS ' + maxfila )
                for (var i=1;i<= maxfila;i++){
                //   alert("I - "+ i)
                var subtotal= document.getElementById("valorNeto"+i);
				
                if(subtotal!= null){
					
                //alert(subtotal.value)
                if(subtotal.value!=""){
                sw=true;
                }else{
                alert("Debe digitar un Valor para el Item");
                return;
                }
					
                st=document.getElementById("total");
                suma=suma+parseFloat(obtenerNumero(subtotal.value));
                st.value=formatearNumero(suma+"");
                }
                }
                //alert("ITEM" + ii);

		
                if(sw==true){
                //  alert(sw);
                maxfila++;
                ii = maxfila;
                //forma1.imgini.style.display="block";
                var numfila = detalle.rows.length ;
                var fila = detalle.insertRow(numfila);
                //alert("FILA" + ii)
			
			  
                fila.id = "filaItem"+ii;  
                var celda = fila.insertCell();   
			
                //fila.id='fila'+ii;
                //fila.className="fila";
			
                var tds ="<span><a id='n_i"+ii+"' >"+ii+"</a>";
                tds+="      <input name='cod_item"+ii+"' id='cod_item"+ii+"' value='"+ii+"' type='hidden' size='4' maxlength='5' border='0'>";			        
                tds+="      <a onClick=\"insertarItem('"+validar+"','"+BASEURL+"','"+Cod_Unid+"','"+tipcliarea+"','"+codCliAre+"','"+Modificar+"')\" id='insert_item"+ii+"' style='cursor:hand' ><img src='"+BASEURL+"/images/botones/iconos/mas.gif'   width='12' height='12' ></a>";
                tds+="      <a onClick=\"borrarItem('filaItem"+ii+"')\"  value='1' id='borrarI"+ii+"' style='cursor:hand' ><img src='"+BASEURL+"/images/botones/iconos/menos1.gif'   width='12' height='12' > </a></span>";
                celda.innerHTML = tds;
                //celda.innerHTML = "<p>Alejo</p>";
			
                //celda.colSpan ='7';
                celda = fila.insertCell();
                var iva ="";
                var ItemAnterior =""; 
                for(var i=1;i <= ii; i++){
                var aux = document.getElementById("iva"+(i)); 
                if(aux != null){
                iva = document.getElementById("iva"+(i));
                ItemAnterior = i;
                }
                }
			
			
                tds = "";
                tds +="<span><table width='100%' border='0' cellpadding='0' cellspacing='0'>";
                tds +="     <tr align='center'>";
                tds +="        <td width='333'><span><textarea name='desc"+ii+"' id='desc"+ii+"' cols='55' rows='1' class='textareaFactura' ></textarea></span></td>";
                tds +="        <td width='201'><input name='descripcion_i"+ii+"' id='descripcion_i"+ii+"'  class='textareaFactura' type='text' size='30'  maxlength='40' onKeyPress=\"BuscarConcepto('"+ii+"',event, 'decNO')\" >  <img src='"+BASEURL+"/images/botones/iconos/lupa.gif'  name='arbol"+ii+"' width='15' height='15'  id='arbol"+ii+"' style='cursor:hand' title='Buscar'  onClick=\"servicios('"+ii+"','"+controlador+"')\" ></td>";
                tds +="        <td width='108'><input name='codigo_cuenta"+ii+"' id='codigo_cuenta"+ii+"'   class='textareaFactura' type='text' size='13' maxlength='20' >";      
                tds +="           <input name='cod_cuenta"+ii+"' id='cod_cuenta"+ii+"' type='hidden' size='4' maxlength='5' border='0' value='"+Cod_Unid+"'></td>";
                tds +="        <td width='43'><input name='codigo_abc"+ii+"' id='codigo_abc"+ii+"' type='text'  class='textareaFactura' size='4' maxlength='2'></td>";
                tds +="        <td width='54'><input name='planilla"+ii+"' id='planilla"+ii+"' type='text'  class='textareaFactura' size='8' maxlength='8' onKeyUp=\"disabledPlanilla('"+ii+"','"+validar+"','"+Modificar+"')\"></td>";
                tds +="        <td width='54'><select name='cod_oc"+ii+"' id='cod_oc"+ii+"' style='width:80%;'   onClick=\" buscarCodigoOC('"+ii+"','"+BASEURL+"')\" ><option value='C' selected>C</option></select></td>    ";
                tds +="        <td width='47'><input name='oc"+ii+"' type='text' id='oc"+ii+"' size='6' maxlength='8'  class='textareaFactura'> <input name='doc"+ii+"' id='doc"+ii+"' type='hidden'></td>";
                tds +="     </tr> ";
                tds +="   </table>";
                tds +="<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
                tds +="   <tr align='center'>";
                tds +="     <td width='125'>&nbsp;</td>";
                tds +="     <td width='239'><input name='valor1"+ii+"' id='valor1"+ii+"'  class='textareaFactura' type='text' size='16' style='text-align:right;' onfocus='this.select()' onBlur='asignarImpuesto("+ii+");' onChange='formatear(this);valorTotal("+ii+");' onKeyPress=\"soloDigitos(event, 'decNO')\"  maxlength='11'  align='right'><input type='hidden' name='iva"+ii+"' id='iva"+ii+"' value="+iva.value+"></td>";
			
		
			 
			
                //			alert (tds);
                celda.innerHTML = tds;
                organizar();
			
                }
                else{
                alert("Debe digitar un Valor para el Item");
                }		
                }
	
	
	
                function borrarItem(indice){
                // alert("filas = "+detalle.rows.length+", indice = "+indice);	
                //			indice = detalle.rows.length;
          
                var tabla = document.getElementById("detalle");
                //alert("indice= "+indice);
                if ( tabla.rows.length <= 2 ){
                return;
                }
			
                var fila = document.getElementById(indice);
                //alert(fila);
                tabla.deleteRow(fila.rowIndex);				
				
			
                var suma = 0;
                st = document.getElementById("total");
                //alert(detalle.rows.length);	
                for (var i = 1;i <= maxfila + 1 ; i++){

                var subtotal = document.getElementById("valorNeto"+i);		
                if ( subtotal != null ){
                //alert(valor + " " + i );
                var valor = subtotal.value.replace( new RegExp(",","g"), "");
					
                if (!isNaN ( parseFloat(valor)) ){
                suma += parseFloat(valor);
                }					  
                }
                }
                st.value = formato(suma);			
                organizar();

			
                /*
                var vlr_total = 0;
                var idItem = indice;
                with (forma1){
                for (i=0; i<elements.length; i++){                  
                if (elements[i].name.indexOf('valor1')==0){
                var valor = elements[i].value.replace( new RegExp(",","g"), "");
                if (!isNaN ( parseFloat(valor)) ){
                vlr_total += parseFloat(valor);						
                }					 
                }
                }
                }		
                forma1.vlr_neto.value =  formato(vlr_total);*/
                }
	
                function organizar(){
                var cont =1;
                for (var i = 1,n = 1;i <= maxfila + 1 ; i++){
                var x = document.getElementById("n_i"+i);
                var y = document.getElementById("filaItem"+i);
                if ( x != null ){
                x.innerText = ""+n;
                n++;
                }
                if( y != null ){
                if( cont % 2 == 0){
                y.className = "filagrisFac";
                }else{
                y.className = "filaFactura";
                }
                cont++;
                }
                }
                }
	
		
	
                function formato(numero){
           
                var tmp = parseInt(numero) ;
                var factor = (tmp < 0 ? - 1 : 1);
                tmp *= factor;
          
                var num = '';
                var pos = 0;
                while(tmp>0){
                if (pos%3==0 && pos!=0) num = ',' + num;
                res  = tmp % 10;
                tmp  = parseInt(tmp / 10);
                num  = res + num  ;
                pos++;
                }
                return (factor==-1 ? '-' : '' ) + num ;
                }      
        
                function sinformato(element){
                return element.replace( new RegExp(",","g") ,'');
                }
		
                function disabledPlanilla(x, validar,Modificar ){
                var comboPlanilla = document.getElementById('cod_oc'+x);
                var planilla      = document.getElementById('planilla'+x);
                var descripcion   = document.getElementById('descripcion_i'+x);
                if( planilla.value.length != 0 )
                comboPlanilla.disabled = true;		
                else  
                comboPlanilla.disabled = false;		
                if (window.event.keyCode==13){ 
                if( descripcion.value.length != 0 ){
                BuscarAccountCode( planilla.value, x, validar,Modificar );
                }
                else{
                alert('Debe seleccionar un concepto de pago');
                }
                }
                }
		
                function BuscarAccountCode( numpla, x , validar, Modificar ){
                var sele   =   document.getElementById("codigo_cuenta"+x);	
                var hidden =   document.getElementById("cod_cuenta"+x);							
                var cuenta =   sele.value;
                document.forma1.action = controlador+"?estado=FacturaSearch&accion=CodigoCuenta&numpla="+numpla+"&indice="+x+"&hidden="+hidden.value+"&cuenta="+cuenta+"&validar="+validar+"&maxfila="+maxfila+"&Modificar="+Modificar; 
                document.forma1.submit(); 
                } 
	
                function buscarNit (validar,maxfila,OP){
	    
                if (window.event.keyCode==13){ 
                var ii=0;
                for (var i=0;i< detalle.rows.length;i++){
                ii=ii+1;
                }
                var num_items = document.getElementById("num_items");
                num_items.value=""+(ii-1);
                var url="";
                url = controlador+"?estado=Factura&accion=AProveedores&validar="+validar+"&maxfila="+maxfila+"&OP="+OP;
                for (var x=0;x<document.forma1.length;x++){
                url =  url +"&"+ document.forma1.elements[x].name + "=" + document.forma1.elements[x].value;
                }
			
                document.forma1.action = url; 
                document.forma1.submit();
                }
                }
	
	
                var isIE = document.all?true:false;
                var isNS = document.layers?true:false;
                function BuscarConcepto(x,value,e,decReq){
                var concepto = document.getElementById("descripcion_i"+x);
                if (window.event.keyCode==13){ 
                if(concepto.value.length == 4){
			  
                var url = controlador+"?estado=Factura&accion=Servicios&concepto="+concepto.value+"&OP=ENTER&Id="+x;
                window.open(url,'Trafico','width=400,height=520,scrollbars=no,resizable=yes,top=10,left=65,status=yes');
			 
                }
			
                }
                soloDigitos(e,decReq);
		
		
                }
	
                function asignarImpuesto(i){
                var campo    = document.getElementById("impuesto0"+i);//este es el campo iva
                var ValorIva = document.getElementById("iva"+i); 
                if(campo.value!="" && ValorIva.value!="" ){
                var ValorIva = document.getElementById("iva"+i); 
                //valor del item
                var valor    = document.getElementById("valor1"+i);
                //valor neto del item
                var vlrN     = document.getElementById("valorNeto"+i);
                var vlr      = parseFloat(ValorIva.value.replace( new RegExp(",","g"), ""));
		  
                //valor total item 
                var vlrTotal = parseFloat(valor.value.replace( new RegExp(",","g"), ""));
                var vlrNeto  = parseFloat(vlrN.value.replace( new RegExp(",","g"), ""));
		 
                vlrN.value   = formato( vlrTotal + (( vlrTotal * vlr )/100) );
		  
		
		  
		  
                var form = document.getElementById("forma1");
                var vlr_total = 0;
		  
                for (j=0; j<form.elements.length; j++){                  
                if (form.elements[j].name.indexOf('valorNeto')==0){
                var valor = form.elements[j].value.replace( new RegExp(",","g"), "");
                if (!isNaN ( parseFloat(valor)) ){
                vlr_total += parseFloat(valor);
                }					 
                }
                }
                form.total.value =  formato(vlr_total);
                }else{
                valorTotal(i);
                }

                }
	

            </script>
                <form name="forma1" id="forma1" action="" method="post"> 
<%
  String mensaje=""+request.getParameter("ms");
    if (! (mensaje.equals("")|| mensaje.equals("null"))){
%>
                
                <table border="2" align="center">
                    <tr>
                        <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                            <tr>
                                <td width="282" align="center" class="mensajes"><%=mensaje%></td>
                                <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="78">&nbsp;</td>
                            </tr>
                        </table></td>
                    </tr>
                </table>
                <br>
<%
    }   
%>
     


                <table width="960" align="center">
                    <tr><td>
                    <table border="2" width='100%'  >
                        <tr>
                            <td width="50%"  class="subtitulo1"><p align="left">Cabecera </p></td>
                                <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" class="Estilo1"></td>
                        </tr>
                    </table>
                    <table width="100%" border="2" align="left">
                        <tr>
                        <td >
                        <table width="100%" align="center"  >
                            <tr >
                                <td width="50%"  class="subtitulo1"><p align="left">Detalle del Documento </p></td>
                                <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" class="Estilo1"></td>
                            </tr>
                        </table>
                        
                        <table id="detalle" width="100%" >
                        <tr  id="fila1" class="tblTitulo">
                            <td align="center" nowrap>Item  </td>
                            <td align="center">Cod Contable </td>
                            <td align="center">Descripcion  </td>
                            <td align="center">Tercero      </td>
                            <td align="center">Auxiliar    </td>
                            <td align="center">Vlr Debito   </td>
                            <td align="center">Vlr Credito  </td>
                        </tr>
				
				<%Vector vItems =null;
                                 int x=1;
				if ( vItems == null){%>
                        <tr class="fila" id="filaItem<%=x%>" nowrap  bordercolor="#D1DCEB" >
                            <td nowrap><a id="n_i<%=x%>" ><%=x%></a>
                            <input name="cod_item<%=x%>" id="cod_item<%=x%>" value="<%=x%>" type="hidden" size="4" maxlength="5" border="0">
                            <a onClick="insertarItem();"  id="insert_item<%=x%>" style="cursor:hand" ><img src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width="12" height="12"  ></a>
                            <a onClick="borrarItem('filaItem<%=x%>');"  value="1" id="borrarI<%=x%>" style="cursor:hand" ><img id="imgini" name="imgini" src='<%=BASEURL%>/images/botones/iconos/menos1.gif'   width="12" height="12"  > </a></span></td>
					     
                            <td align="center"><input type="text" maxlength='25' style='width:182' name='CodContable<%=x%>' id='CodContable<%=x%>'> </td>
                            <td align="center"><textarea name="descripcion<%=x%>" id="descripcion<%=x%>" cols="35" rows="1" class="textbox" ></textarea> </td>
                            <td align="center"><input type="text" name='tercero<%=x%>' id='tercero<%=x%>' maxlength='15' style='width:128' onKeyPress="soloDigitos(event, 'decNO')" >      </td>
                            <td align="center"><select><option value='CC'>Cedula </option>
                                                       <option value='NT'>Nit    </option>
                                                       <option value='PK'>Placa  </option>
                                               </select><input type="text" maxlength='25' style='width:182' name='idSubledger' id='idSubledger<%=x%>'>
                            </td>
                            <td align="center"><input name="valorDebito<%=x%>" id="valorDebito<%=x%>" onFocus='this.select()'  class="textboxFactura" style="text-align:right;"  onKeyPress="soloDigitos(event, 'decNO')"  type="text"  onChange="formatear(this);" size="16"  maxlength="11" align="right"></td>
                            <td align="center"><input name="valorCredito<%=x%>" id="valorCredito<%=x%>" onFocus='this.select()'  class="textboxFactura" style="text-align:right;"  onKeyPress="soloDigitos(event, 'decNO')"  type="text"  onChange="formatear(this);" size="16"  maxlength="11" align="right"></td>
                        </tr>   
				
                        
                               <%}%>
                    </table>
                     </td>
                    </tr>
                </table>
                </td>
                </tr>
            </table>
            
                <center>
                <img src='<%=BASEURL%>/images/botones/iconos/guardar.gif' name='Buscar' width="25" height="25" align="absmiddle" style='cursor:hand' title='Guardar...'  onclick="escribirArchivo('<%=CONTROLLER%>',maxfila);" >
                &nbsp;<img src='<%=BASEURL%>/images/botones/agregar.gif' name='Buscar' align="absmiddle" style='cursor:hand'   onClick="insertarItem();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
                &nbsp;<img src='<%=BASEURL%>/images/botones/aceptar.gif' name='Buscar' align="absmiddle" style='cursor:hand'   onclick="validarDocumento();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
                &nbsp;<img src='<%=BASEURL%>/images/botones/restablecer.gif' name='restablecer' align="absmiddle" style='cursor:hand'   onClick="Restablecer('<%=CONTROLLER%>');"  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
                &nbsp;<img src='<%=BASEURL%>/images/botones/salir.gif' name='Buscar' align="absmiddle" style='cursor:hand'   onClick="window.close();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
		</center>
	
            </div>
            <tr>
            </form>
        </div>
    </body>
    <iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
<%=datos[1]%>
</html>


    