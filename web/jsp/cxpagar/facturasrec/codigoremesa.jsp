<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de realizar y mostrar busqueda de los nits de proveedores
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script language="javascript">
	function asignarCodigoRemesa(x,codigo,nombre){	
		var campo = parent.opener.document.getElementById("oc"+x);
		campo.value=codigo;	
		campo.title=nombre;	
		var texto=parent.opener.document.getElementById("doc"+x);
		texto.value=nombre;
		
		var sele = window.opener.document.getElementById("descripcion_i"+x);
		//alert(sele.value.substring(0,1));
		var temp1=sele.value.substring(0,6);
		var temp2=sele.value.substring((sele.value.indexOf(":")-4),sele.length);
		if(sele.value.substring(0,1)!="G"){
			var cadena = codigo.substring(codigo.length-3,codigo.length);
		    sele.value=temp1 + cadena+temp2;
		}
		
		parent.close();	
    }
</script>
<title>JSP Page</title>
</head>

<body onLoad="" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Buscar Clientes Remesas"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
  String x=(request.getParameter("x")==null?"":request.getParameter("x"));
 // out.println("X "+x);
%>
<form name="forma1" action="" method="post">      
  <%
      Vector vClientes = model.remesaService.getVClientes ();
%>
  <table width="566" border="2" align="center">
    <tr>
    <td width="585">
<table width="555"  >
          <tr>
    <td width="346"  class="subtitulo1"><p align="left">Clientes </p></td>
    <td width="193"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
  </tr>
</table>
        <table width="556" >
          <tr class="tblTitulo" id="titulos">
    <td width="56" height="22">Codigo</td>
    <td width="334" >Clientes</td>
    </tr>
<%
      for(int i=0; i< vClientes.size();i++){
        Vector fila= (Vector)vClientes.elementAt(i);
%>  
  <tr class="fila">
    <td width="56" height="22"><%=fila.elementAt(0)%></td>
    <td width="334" ><a  onClick="asignarCodigoRemesa('<%=x%>','<%=fila.elementAt(0)%>','<%=fila.elementAt(1)%>')" style="cursor:hand" ><%=fila.elementAt(1)%></a></td>
    </tr>
<%
      }
%>  
</table>
</td>
</tr>
</table>
	<p align="center"><img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir..." onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></p>
	<p>
    </p>
</form>
</div>
</body>
</html>
    