<!--
- Autor : Ing. Sandra Escalante
- Modificado: Ing Ivan Dario Gomez
- Date  : 02 Febrero 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Vista para mostrar la informacion de las ayudas
--%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE></TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">  
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</HEAD>
<BODY>
<% String BASEIMG = BASEURL +"/images/ayuda/cxpagar/facturasrec/"; %> 
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="1" >      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="20"><div align="center">MANUAL PARA LA CREACION DE FACTURAS </div></td>
        </tr>
        <tr class="subtitulo1">
          <td>Descripci&oacute;n del funcionamiento del programa para la creacion de facturas </td>
        </tr>
        <tr>
          <td  height="18"  class="ayudaHtmlTexto">El programa para la creacion de facturas contiene multiples opciones, para un mayor entendimiento lo divideremos en dos partes: Cabecera o Documento y Item de la factura o Detalle del Documento. </td>
        </tr>
        <tr>
          <td height="18"  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEIMG%>Dibujo1.PNG"> </div></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><p>En la parte para agregar el proveedor, la que se muestra en la figura 2, tiene dos opciones de busqueda:</p>
          </td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">- Busqueda por Nombre </td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">- Busqueda por Nit </td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><div align="center">
            <p><img src="<%= BASEIMG%>Dibujo2.PNG"> </p>
            </div></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><strong>Busqueda por nombre</strong></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><div align="left">Para la busqueda por nombre debe dar click en la lupa y se desplegara una pantalla como lo muestra en la fugura 3.</div></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><div align="left">En esta pantalla puede digitar parte del nombre y luego presionar el boton buscar para que el programa le muestre un listado de todos los nombres que contenga lo que escribio. ejemplo: en la figura 3 el nombre digitado es &quot;KAREN&quot;, al dar click en buscar se listan todos los nombres de proveedores que contengan &quot;KAREN&quot; como se muestra en la figura 4 . </div></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEIMG%>Dibujo3.PNG"></div></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">una vez listado los nombres, para obtener el nombre y el codigo en la vista que se muestra en la figura 1, debera dar click sobre el nombre.</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEIMG%>Dibujo4.PNG"></div></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><strong>Busqueda por Nit </strong></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><div align="left">Para la busqueda por nit debera digitar el nit en el campo proveedor como se muestra en la figura 1, y luego presionar la tecla Enter, si el proveedor existe se le cargara el nombre, plazo, banco, sucursal del proveedor en los campos correspondientes. </div></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEIMG%>Dibujo5.PNG"></div></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">Los campos que se muestran en la figura 5 son para aplicar los impuestos a todos los item creados en la factura.  tenemos dos formas de realizar la busqueda de los impuestos:</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">- Si no tiene el codigo del impuesto debe dar click en la lupa de busqueda del tipo de impuesto que desea aplicar, luego se le mostrara una lista de impuesto como se muestra en la figura 6, debera dar click sobre el impuesto que desea aplicar, la ventana se cerrara aplicando el impuesto a todos los item que esten el la factura.</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><div align="center"><img src="../../../images/ayuda/cxpagar/facturas/figura6.JPG" width="603" height="437"></div></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">- Si tiene el codigo del impuesto debera digitarlo y presionar la tecla Enter para aplicar el impuesto a todos los item que esten en la factura, si el codigo digitado no existe le aparecera una ventana de error informandole que el codigo no existe Figura 7, y se le listaran lis codigos del tipo de impuesto que esta digitando, como se muestra en la Figura 6. </td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><div align="center"><img src="../../../images/ayuda/cxpagar/facturas/figura7.JPG" width="324" height="176"></div></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><strong>Detalle del Documento </strong></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEIMG%>Dibujo6.PNG"></div></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><table cellspacing="0" cellpadding="0">
              <tr>
                <td width="960" class="ayudaHtmlTexto">&nbsp;</td>
              </tr>
          </table>
            <div align="center"></div></td>
        </tr>
      </table></td>
    </tr>
  </table>
</BODY>
</HTML>
