<!--
- Autor : Ing. Ivan Dario Gomez Vanegas
- Date  : Noviembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra los datos de entrada para 
--              la creacion de la boleta recoge como parametro la OC
--%>

<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>



<html>
<head>

    <title>Modificacion de Facturas</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script src='<%= BASEURL %>/js/validar.js'></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
	
</head>
<% String mensaje = (request.getParameter("mensaje")!= null)?request.getParameter("mensaje"):"";
%>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modificacion de Factura Recurrente"/>
</div>

             <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
    <form name="form1" method="post" action="<%= CONTROLLER %>?estado=FactRecurr&accion=Buscar" >
        <table width="60%" border="2" align="center">
			<tr>
                            <td>
                            <table width="99%" height="34" border="0" align="center" class="tablaInferior">
                            <tr>
                               <td width="190" height="24"  class="subtitulo1"><p align="left">Buscar Factura</p></td>
                                <td width="404"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                                
			    </tr>     						
                                                                    
                                    <tr class="fila">
                                    <td class="letra_resaltada" align="left">&nbsp;&nbsp;Factura</td>
                                    <td align="left">&nbsp;&nbsp;<input type="text"  name="documento" class="textbox" maxlength="30"  ></td>            
                                    </tr>
									 <tr class="fila">
                                    <td class="letra_resaltada" align="left">&nbsp;&nbsp;Proveedor</td>
                                    <td align="left">&nbsp;&nbsp;<input type="text"  name="prov" class="textbox" maxlength="30"  onKeyUp="enviar(form1)" ></td>            
                                    </tr>
                              </table>
			</td>        
		      </tr>
        	
		</table>
        <table align="center">
          <tr align="center">
            <td colspan="2"> <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="Validar(form1);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> </td>
          </tr>
        </table>
        
    </form>
	<%if(!mensaje.equals("")){%> 
   <table border="2" align="center">
  <tr>
    <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="282" align="center" class="mensajes"><%=mensaje%></td>
          <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="78">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<%}%>
	</div> 
  
</body>
<script>
function Validar(form1){
    if(form1.documento.value == ""){
	    alert("Ingrese el numero de la factura");
	}else if(form1.prov.value == ""){
	     alert("Ingrese el codigo del proveedor");  
	}else{
	form1.submit();
	}
   
   
}

function enviar (form1){
    if (window.event.keyCode==13){ 

		if (form1.documento.value == "" ){ 
			alert("Digite el numero de la factura");
		}else if(form1.prov.value == "" ){
		    alert("Digite el codigo del proveedor");
		}else{
		    form1.submit();
		}
	}
}

</script>
</html>
