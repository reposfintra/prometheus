<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de realizar y mostrar busqueda de los tipos de impuestos
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head><title>JSP Page</title>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script>
function asignarImpuesto(id,codigo,vl){
  var iva      = parent.opener.document.getElementById("iva"+id.substr(1,id.length)); 
  var campo    = parent.opener.document.getElementById("impuesto"+id);
  var i   	   = id.substr(1,id.length);  
  //valor del item
  var valor    = parent.opener.document.getElementById("valor1"+i);
  //valor neto del item
  var vlrN     = parent.opener.document.getElementById("valorNeto"+i);
  var vlr      = parseFloat(vl.replace( new RegExp(",","g"), ""));
  campo.value  = codigo;
  //valor total item 
  var vlrTotal = parseFloat(valor.value.replace( new RegExp(",","g"), ""));
  var vlrNeto  = parseFloat(vlrN.value.replace( new RegExp(",","g"), ""));
// alert("vlrNeto ="+ vlrNeto+"  vlrTotal  =" +vlrTotal + "   vlr="+vlr)
  
  if( id.substr(0,1) == 0 ){//el numero 0 es el que indica si es iva
  	vlrN.value   = formato( vlrTotal + (( vlrTotal * vlr )/100) );
	iva.value    = vl;	
  }
  /*
  if( id.substr(0,1) != 1 ){
     vlrN.value   = formato( vlrNeto - (( vlrNeto * vlr )/100) );	 
  }
  else{
	  vlrN.value   = formato( vlrNeto + (( vlrTotal * vlr )/100) );	
  }*/

  
  
  var form = parent.opener.document.getElementById("forma1");
  var vlr_total = 0;
  
  for (j=0; j<form.elements.length; j++){                  
	if (form.elements[j].name.indexOf('valorNeto')==0){
    	var valor = form.elements[j].value.replace( new RegExp(",","g"), "");
			if (!isNaN ( parseFloat(valor)) ){
				vlr_total += parseFloat(valor);
             }					 
     }
  }
  form.total.value =  formato(vlr_total);
 
  parent.close();
}

function formato(num){
	    var decimales =2;
	    var nums = ( new String (num) ).split('.');
		var salida = new String();
		for (var i=nums[0].length-1, j=0; i>=0; salida = nums[0].charAt(i) + (j%3==0 && j!=0? ',':'') + salida , i--, j++);
		return rellenar(salida + (nums.length > 1 && decimales > 0 ? '.' + (nums[1].substr(0, (nums[1].length>decimales?decimales:nums[1].length))) : ''));


	}


 function rellenar(val){
           var longitud = 2;
           var cantDEcimal = val.indexOf('.');
           if( cantDEcimal >-1  ){
               var decimal = val.substr(cantDEcimal+1, val.length );
               var cntDec  =   decimal.length;
               if ( cntDec < longitud){
                    for( var i= 1;i< longitud;i++)
                        val +='0'
               }
           }

           return val;
        }

/*funcion para asignar los impuestos a todos los item*/
function asignarImpuestoCab(id,codigo,vl,maxfila,tipoImp){
  
  var campo =  parent.opener.document.getElementById(id);
  var imp   =(tipoImp =='IVA')?'impuesto0':(tipoImp =='RIVA')?'impuesto1':(tipoImp =='RICA')?'impuesto2':(tipoImp =='RFTE')?'impuesto3':'';
 
  for (var i=1;i<= maxfila;i++){
      var iva      = parent.opener.document.getElementById("iva"+i); 
      if(iva != null){
		 
		  
		  var impItem =  parent.opener.document.getElementById(imp+""+i); 
		  //valor del item
		  var valor    = parent.opener.document.getElementById("valor1"+i);
		  //valor neto del item
		  var vlrN     = parent.opener.document.getElementById("valorNeto"+i);
		  var vlr      = parseFloat(vl.replace( new RegExp(",","g"), ""));
		  campo.value  = codigo;
		  impItem.value = codigo;
		  //valor total item 
		  var vlrTotal = parseFloat(valor.value.replace( new RegExp(",","g"), ""));
		  var vlrNeto  = parseFloat(vlrN.value.replace( new RegExp(",","g"), ""));
		// alert("vlrNeto ="+ vlrNeto+"  vlrTotal  =" +vlrTotal + "   vlr="+vlr)
		  
		  if( tipoImp == 'IVA' ){
			vlrN.value   = formato( vlrTotal + (( vlrTotal * vlr )/100) );
			iva.value    = vl;	
		  }
		  /*
		  if( id.substr(0,1) != 1 ){
			 vlrN.value   = formato( vlrNeto - (( vlrNeto * vlr )/100) );	 
		  }
		  else{
			  vlrN.value   = formato( vlrNeto + (( vlrTotal * vlr )/100) );	
		  }*/
		
		  
		  
		  var form = parent.opener.document.getElementById("forma1");
		  var vlr_total = 0;
		  
		  for (j=0; j<form.elements.length; j++){                  
			if (form.elements[j].name.indexOf('valorNeto')==0){
				var valor = form.elements[j].value.replace( new RegExp(",","g"), "");
					if (!isNaN ( parseFloat(valor)) ){
						vlr_total += parseFloat(valor);
					 }					 
			 }
		  }
		  form.total.value =  formato(vlr_total);
		 
		  parent.close();
	  }
  }
}

/*****************************************************/
</script>
</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Impuestos "/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
    String t_i = ""+request.getParameter("tipo");
    String idI = ""+request.getParameter("id");
    String op  = ""+request.getParameter("OP");
	String codigo =""+request.getParameter("codigo");
	String maxfila =  (request.getParameter ("maxfila")==null?"":request.getParameter ("maxfila")); 
    java.util.Date utilDate = new java.util.Date(); //fecha actual
    long lnMilisegundos = utilDate.getTime();
    java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(lnMilisegundos);
	Vector vImpuestos= model.TimpuestoSvc.buscarImpuestoPorTipo(t_i,""+sqlTimestamp);
	Tipo_impuesto datos = new  Tipo_impuesto();
	if(op.equals("ENTER")){
	    datos = model.TimpuestoSvc.buscarImpuestoPorCodigos(t_i,codigo,""+sqlTimestamp);
		if(datos ==null){
		   op = "";
		   %><script>
		        alert("El codigo de impuesto digitado no existe en la bd")
		     </script>
		   <%
		   
		}
	}
	
%>
<form name="forma1" action="" method="post">      
<table width="312" border="2" align="center">
  <tr>
    <td width="300">
<table width="100%" align="center" > 
  <tr>
    <td width="378" height="24"  class="subtitulo1"><p align="left">Impuestos Tipo <%=t_i%></p></td>
    <td width="225"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
  </tr>
</table>
<table width="100%"  align="center" >
 <tr class="tblTitulo" id="titulos">
    <td width="136" align="center">Codigo </td>
    <td width="146" align="center">Impuesto</td>
 </tr>
<% 
    String fecha =""+sqlTimestamp; 
    for(int i=0; i< vImpuestos.size();i++){
        Tipo_impuesto tipo=(Tipo_impuesto)vImpuestos.elementAt(i);
%>  
        <tr class="fila">
		
		 <%if(maxfila.equals("")){%>
			<td align="center"><a  onClick="asignarImpuesto('<%=idI%>','<%=tipo.getCodigo_impuesto()%>', '<%=tipo.getPorcentaje1()%>');" style="cursor:hand" ><%=tipo.getCodigo_impuesto()%></a></td>
			<td align="center"><a  onClick="asignarImpuesto('<%=idI%>','<%=tipo.getCodigo_impuesto()%>', '<%=tipo.getPorcentaje1()%>');" style="cursor:hand" ><%=tipo.getPorcentaje1()+ " % "%></a></td>
        <%}else{%>
		    <td align="center"><a  onClick="asignarImpuestoCab('<%=idI%>','<%=tipo.getCodigo_impuesto()%>', '<%=tipo.getPorcentaje1()%>','<%=maxfila%>','<%=t_i%>');" style="cursor:hand" ><%=tipo.getCodigo_impuesto()%></a></td>
			<td align="center"><a  onClick="asignarImpuestoCab('<%=idI%>','<%=tipo.getCodigo_impuesto()%>', '<%=tipo.getPorcentaje1()%>','<%=maxfila%>','<%=t_i%>');" style="cursor:hand" ><%=tipo.getPorcentaje1()+ " % "%></a></td> 
		<%}%>
	    </tr>
<%
    }
%>  
</table>
</td>
</tr>
</table>
</form>
<div>
<iframe width=188 height=166 name="gToday:datetime:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins_24.js" id="gToday:datetime:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins_24.js" src="<%=BASEURL%>/js/calendartsp/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%if(op.equals("ENTER")){%>
<center><img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir..." onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></center></body>
<%}%>
</html>
<%if(op.equals("ENTER")){
     if(maxfila.equals("")){%>
		<script>
		 asignarImpuesto('<%=idI%>','<%=datos.getCodigo_impuesto()%>', '<%=datos.getPorcentaje1()%>');
		</script> 
   <%}else{%>
      <script>
	     asignarImpuestoCab('<%=idI%>','<%=datos.getCodigo_impuesto()%>', '<%=datos.getPorcentaje1()%>','<%=maxfila%>','<%=t_i%>');
	  </script>  
  <% }
}%>