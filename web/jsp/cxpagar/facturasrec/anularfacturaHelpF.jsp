<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA FUNCIONAL - Anular Factura
	 - Date            :      13/06/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>AYUDA FUNCIONAL - Anular Factura</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY>  
<% String BASEIMG = BASEURL +"/images/ayuda/cxpagar/anularFactura/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE FACTURAS RECURREMTES</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa para anular una facturarecurrente.</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">En la siguiente pantalla se digita la informaci&oacute;n que corresponda al documento que desea buscar.</p>            </td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=195 src="<%=BASEIMG%>image_001.JPG" width=779 border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Usted puede escoger el tipo de documento que desea buscar para anular. Este tipo de documento puede ser una factura, una nota debito o una nota cr&eacute;dito como se muestra en la siguiente pantalla. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=144 src="<%=BASEIMG%>image_002.JPG" width=291 border=0 v:shapes="_x0000_i1143"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>El sistema verifica que no hayan campos vacios.</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=126 src="<%=BASEIMG%>image_error001.JPG" width=320 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>El sistema verifica que  la informaci&oacute;n ingresada est&eacute; correcta y s&iacute; exista en el sistema, de lo contrario nos muestra el siguiente mensaje, y se debe corregir y volver a presionar BUSCAR.</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=103 src="<%=BASEIMG%>image_error002.JPG" width=297 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si el documento buscado, posee un cheque asociado, la acci&oacute;n de anulaci&oacute;n no puede ser realizada. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=114 src="<%=BASEIMG%>image_error003.JPG" width=300 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si el sistema le muestra el siguiente mensaje en su pantalla, es por que el documento se encuentra en una corrida.</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=112 src="<%=BASEIMG%>image_error004.JPG" width=297 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si el saldo del documento es igual a cero, es por que este documento esta cancelado, y en la pantalla nos sadr&aacute; el siguiente mensaje. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=113 src="<%=BASEIMG%>image_error005.JPG" width=300 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si el documendo esta como abonada, es decir que el saldo del documento es menor que el valor neto de este mismo, entonces el sistema nos arrojar&aacute; la siguiente informaci&oacute;n.</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=114 src="<%=BASEIMG%>image_error006.JPG" width=301 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">Al llenar la informaci&oacute;n debidamente y presionar BUSCAR,  en la pantalla aparecen los datos que coinciden con la b&uacute;squeda realizada.</p></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=382 src="<%=BASEIMG%>image003.JPG" width=739 border=0 v:shapes="_x0000_i1052"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">Al verificar que la informaci&oacute;n mostrada es la deseada para anular, entonces se procede a dar Click en el Boton Anular, y el sistema nos mostrar&aacute; el siguiente mensaje en la pantalla.</p></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=114 src="<%=BASEIMG%>image004.JPG" width=302 border=0 v:shapes="_x0000_i1054"></div></td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
