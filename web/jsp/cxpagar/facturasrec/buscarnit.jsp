<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de realizar y mostrar busqueda de los nits de proveedores
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Buscar Nit</title>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body onLoad="redimensionar();" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Buscar Nit"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
	 String accion = (request.getParameter("accion")==null?"":request.getParameter("accion"));
	 String subtotal=(request.getParameter("subtotal")==null?"":request.getParameter("subtotal"));
	 String validar =(request.getParameter("validar")==null?"":request.getParameter("validar"));
	 String maxfila = request.getParameter("maxfila");
%>
<form name="forma1" action="<%=CONTROLLER%>?estado=FactRecurr&accion=Proveedoresnit&subtotal=<%=subtotal%>&validar=<%=validar%>&maxfila=<%=maxfila%> " method="post" onsubmit='return ValidarForm();'>      
  <table border="2" align="center" width="566">
    <tr>
    <td width="610" >
        <table width="100%" align="center"  >
          <tr>
            <td width="295" height="24"  class="subtitulo1"><p align="left">Documento</p></td>
            <td width="265"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
          </tr>
        </table>        
        <table width="100%" align="center" >
          <tr class="fila">
            <td width="107" >Id del Proveedor: </td>
            <td width="424" > 
              <input name="proveedor" type="text" class="textbox" id="proveedor" size="18" maxlength="15">
      
      &nbsp;

<input type="image" src='<%=BASEURL%>/images/botones/buscar.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Aceptar...'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'></td>
          </tr>
        </table></td>
</tr>
</table>




<%
    if (accion.equals("1")){
        String nit = "" +request.getParameter("nit");
        Vector vProveedores=  model.proveedorService.getProveedores ();
%>
  <table width="566" border="2" align="center">
    <tr>
    <td width="585">
<table width="555"  >
          <tr>
    <td width="346"  class="subtitulo1"><p align="left">Proveedor</p></td>
    <td width="193"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
  </tr>
</table>
        <table width="556" >
          <tr class="tblTitulo" id="titulos">
    <td width="56" height="22">Nit</td>
    <td width="334" >nombre</td>
    </tr>
<%
        for(int i=0; i< vProveedores.size();i++){
            Proveedor o_proveedor=(Proveedor)vProveedores.elementAt(i);
%>  
  <tr class="fila">
    <td width="56" height="22"><%=o_proveedor.getC_nit()%></td>
    <td width="334" ><a  onClick="asignarCodigoP('<%=o_proveedor.getC_nit()%>','<%=o_proveedor.getC_payment_name()%>','<%=o_proveedor.getC_branch_code()%>','<%=o_proveedor.getC_bank_account()%>','<%=o_proveedor.getPlazo()%>','<%=subtotal%>','<%=CONTROLLER%>', '<%=validar%>','<%=maxfila%>','<%=o_proveedor.getCurrency()%>','<%=o_proveedor.getC_agency_id()%>','<%=o_proveedor.getNom_beneficiario()%>','<%=o_proveedor.getC_hc()%>')" style="cursor:hand" ><%=o_proveedor.getC_payment_name()%></a></td>
    </tr>
<%
        }
%>  
</table>
</td>
</tr>
</table>
<%
    }
%>
</form>
<div align="center"><img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir..." onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</div>
</div>
</body>
</html>

<script>
	function asignarCodigoP(nit,nombre,banco,sucursal,plazo,subtotal,CONTROLLER, validar,maxfila,moneda_banco,agenciaBanco,beneficiario,hc){
		parent.opener.location.href = CONTROLLER+"?estado=FactRecurr&accion=CargarP&subtotal="+subtotal+"&proveedor="+nit+"&banco="+banco+"&sucursal="+sucursal+"&plazo="+plazo+"&ag="+validar+"&maxfila="+maxfila+'&moneda_banco='+moneda_banco+'&agenciaBanco='+agenciaBanco+'&beneficiario='+beneficiario+'&hc='+hc; 
		parent.close();
	}
	
	function procesarProveedores (element,subtotal,CONTROLLER, validar){
		if (window.event.keyCode==13) 
		listaP(subtotal, validar);
	}
	
	/*function buscar (subtotal,CONTROLLER, estado,maxfila){
		if (window.event.keyCode==13) 
		listaP(subtotal,CONTROLLER,estado, maxfila);
	}*/
	
	function listaP(subtotal,CONTROLLER, estado,maxfila){
	         if(forma1.proveedor.value !=''){
		   document.forma1.action = CONTROLLER+"?estado=FactRecurr&accion=Proveedoresnit&subtotal="+subtotal+"&validar="+estado+"&maxfila="+maxfila; 
		   document.forma1.submit();
                }else{
                   alert('Digite el Id del proveedor')
                }
	} 
	
	function ValidarForm(){	
	         if(forma1.proveedor.value !=''){
		      return true;
                }else{
                   alert('Digite el Id del proveedor')
                   return false;
                }
	}
	
</script>
    