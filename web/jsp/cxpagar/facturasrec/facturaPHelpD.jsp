<!--
- Autor : Ing. Sandra Escalante
- Modificado: Ing Ivan Dario Gomez
- Date  : 02 Febrero 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Vista para mostrar la informacion de las ayudas 
--%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Descripcion de campos para la Creacion de Facturas</title>

<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">CUENTAS POR PAGAR </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2"> CREACION DE FACTURAS </td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2"> CABECERA </td>
        </tr>
        <tr>
          <td width="131" class="fila"> Documento </td>
          <td width="543"  class="ayudaHtmlTexto">Campo donde se especifica el tipo de documento y el numero del documento </td>
        </tr>
        <tr>
          <td  class="fila"> Documento Rel.</td>
          <td  class="ayudaHtmlTexto">Campo donde se especifica si tiene documento relacionado </td>
        </tr>
        <tr>
          <td  class="fila">Valor</td>
          <td  class="ayudaHtmlTexto"> Campo deonde se especifica el Valor  de la factura </td>
        </tr>
        <tr>
          <td  class="fila">Moneda</td>
          <td  class="ayudaHtmlTexto">Tipo de moneda en que se va a generar la factura </td>
        </tr>
        <tr>
          <td  class="fila">Fecha</td>
          <td  class="ayudaHtmlTexto">Fecha de generacion de la Factura </td>
        </tr>
        <tr>
          <td  class="fila">Proveedor</td>
          <td  class="ayudaHtmlTexto">Campo donde se colaca la cedula del proveedor al cual se le esta generando la factura </td>
        </tr>
        <tr>
          <td  class="fila">Plazo</td>
          <td  class="ayudaHtmlTexto">Campo donde se especifica el plazo al proveedor de cancelar la factura </td>
        </tr>
        <tr>
          <td  class="fila">Cuotas</td>
          <td  class="ayudaHtmlTexto">Campos donde se digitan el n&uacute;mero de cuotas de la factura</td>
        </tr>
        <tr>
          <td  class="fila">Fecha Inicio </td>
          <td  class="ayudaHtmlTexto">Campo donde se especifica la fecha de inicio de la primera transferencia </td>
        </tr>
        <tr>
          <td  class="fila">Frecuencia</td>
          <td  class="ayudaHtmlTexto"><div align="left">Campo donde se digita frecuencia para la transferencia (Diario/Semanal/Quincenal/Mensual/Semestral) </div></td>
        </tr>
        <tr>
          <td  class="fila">Banco</td>
          <td  class="ayudaHtmlTexto">Banco donde se va a cancelar la factura </td>
        </tr>
        <tr>
          <td  class="fila">Sucursal</td>
          <td  class="ayudaHtmlTexto">Sucursal del banco en donde se cancelara la factura </td>
        </tr>
        <tr>
          <td  class="fila">Autorizador</td>
          <td  class="ayudaHtmlTexto">Campo donde se especifica el usuario que autoriza el pago de la factura </td>
        </tr>
        <tr>
          <td  class="fila">Descripcion</td>
          <td  class="ayudaHtmlTexto"> Descripcion de la cabecera de la factura </td>
        </tr>
        <tr>
          <td  class="fila">Observacion</td>
          <td  class="ayudaHtmlTexto">Observacion de la factura </td>
        </tr>
        <tr>
          <td  class="fila">IVA</td>
          <td  class="ayudaHtmlTexto">Campo donde se aplica el impuesto IVA a todos los item </td>
        </tr>
        <tr>
          <td  class="fila">RIVA</td>
          <td  class="ayudaHtmlTexto">Campo donde se aplica el impuesto RIVA a todos los item </td>
        </tr>
        <tr>
          <td  class="fila">RICA</td>
          <td  class="ayudaHtmlTexto">Campo donde se aplica el impuesto RICA a todos los item </td>
        </tr>
        <tr>
          <td  class="fila">RFTE</td>
          <td  class="ayudaHtmlTexto">Campo donde se aplica el impuesto RFTE a todos los item </td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2"> ITEM </td>
        </tr>
        <tr>
          <td  class="fila"><img src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width="12" height="12"  ></td>
          <td  class="ayudaHtmlTexto">Boton para agregar item a la factura </td>
        </tr>
        <tr>
          <td  class="fila"><img id="imgini" name="imgini" src='<%=BASEURL%>/images/botones/iconos/menos1.gif'   width="12" height="12"  ></td>
          <td  class="ayudaHtmlTexto">Boton para eliminar item </td>
        </tr>
        <tr>
          <td  class="fila">Descripcion </td>
          <td  class="ayudaHtmlTexto">Campo donde se especifica la descripcion del item </td>
        </tr>
        <tr>
          <td  class="fila">Concepto</td>
          <td  class="ayudaHtmlTexto">Campo donde se especifica la descripcion del elemento del gasto </td>
        </tr>
        <tr>
          <td  class="fila">Cuenta</td>
          <td  class="ayudaHtmlTexto">Campo donde se arma el codigo de cuenta contable </td>
        </tr>
        <tr>
          <td  class="fila">ABC</td>
          <td  class="ayudaHtmlTexto">Campo donde se guarda el codigo ABC </td>
        </tr>
        <tr>
          <td  class="fila">Planilla</td>
          <td  class="ayudaHtmlTexto">Campo para buscar una planilla para armar el codigo de cuenta contable </td>
        </tr>
        <tr>
          <td  class="fila">Tipo</td>
          <td  class="ayudaHtmlTexto">Campo para buscar el cliente </td>
        </tr>
        <tr>
          <td  class="fila">Centro</td>
          <td  class="ayudaHtmlTexto">Campo dende se guarda el codigo del cliente </td>
        </tr>
        <tr>
          <td  class="fila">Valor</td>
          <td  class="ayudaHtmlTexto">Campo donde se guarda el valor del item </td>
        </tr>
        <tr>
          <td  class="fila">IVA</td>
          <td  class="ayudaHtmlTexto">Campo donde se le aplica el impuesto IVA a el item </td>
        </tr>
        <tr>
          <td  class="fila">RIVA</td>
          <td  class="ayudaHtmlTexto">Campo donde se le aplica el impuesto RIVA a el item </td>
        </tr>
        <tr>
          <td  class="fila">RICA</td>
          <td  class="ayudaHtmlTexto">Campo donde se le aplica el impuesto RICA a el item </td>
        </tr>
        <tr>
          <td  class="fila">RFTE</td>
          <td  class="ayudaHtmlTexto">Campo donde se le aplica el impuesto RFTE a el item </td>
        </tr>
        <tr>
          <td  class="fila">Valor Neto </td>
          <td  class="ayudaHtmlTexto">Campo donde se guarda el valor neto del item </td>
        </tr>
        <tr>
          <td  class="fila"><img src='<%=BASEURL%>/images/botones/iconos/guardar.gif' name='Buscar' width="25" height="25" align="absmiddle" ></td>
          <td  class="ayudaHtmlTexto">boton para guardar lo los datos almacenados en los campos para la creacion de factura </td>
        </tr>
        <tr>
          <td  class="fila"><img src='<%=BASEURL%>/images/botones/agregar.gif' name='Buscar' align="absmiddle"    ></td>
          <td  class="ayudaHtmlTexto">Boton para agregar item a la factura</td>
        </tr>
        <tr>
          <td  class="fila"><img src='<%=BASEURL%>/images/botones/aceptar.gif' name='Buscar' align="absmiddle"   onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'></td>
          <td  class="ayudaHtmlTexto">Boton para grabar la factura </td>
        </tr>
        <tr>
          <td  class="fila"><img src='<%=BASEURL%>/images/botones/restablecer.gif' name='restablecer' align="absmiddle"    ></td>
          <td  class="ayudaHtmlTexto">Boton para limpiar los datos digitados en la pantalla </td>
        </tr>
        <tr>
          <td  class="fila"><img src='<%=BASEURL%>/images/botones/salir.gif' name='Buscar' align="absmiddle" ></td>
          <td  class="ayudaHtmlTexto"> Boton para cerrar la ventana </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

</body>
</html>
