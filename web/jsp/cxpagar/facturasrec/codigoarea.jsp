<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de realizar y mostrar busqueda de los nits de proveedores
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script language="javascript">
	function asignarCodigoArea(x,codigo,nombre, c){	
		var campo = parent.opener.document.getElementById("oc"+x);
		campo.value=codigo;	
		campo.title=nombre;
		var texto=parent.opener.document.getElementById("doc"+x);
		texto.value=nombre;		
		var sele = window.opener.document.getElementById("codigo_cuenta"+x);	
		var hidden = window.opener.document.getElementById("cod_cuenta"+x);
		
		var cod1   =   window.opener.document.getElementById("cod1"+x);	
		var cod2   =   window.opener.document.getElementById("cod2"+x);	
		var cod3   =   window.opener.document.getElementById("cod3"+x);	
		var cod4   =   window.opener.document.getElementById("cod4"+x);	
		var cod5   =   window.opener.document.getElementById("cod5"+x);	

		var cuenta = cod1.value + cod2.value + cod3.value + cod4.value + cod5.value;
		var cod_cuenta = "";
	    alert( cuenta )
   		alert("H " + hidden.value );
		cod2.value = hidden.value.split(',')[0];
		cod3.value = hidden.value.split(',')[1];
		cod4.value = codigo;
	    cod_cuenta  = cod1.value + cod2.value + cod3.value + cod4.value + cod5.value;
			
		
		sele.value = cod_cuenta;
		parent.close();	
    }
</script>
<title>JSP Page</title>
</head>

<body onLoad="" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Buscar Areas"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
  String x=(request.getParameter("x")==null?"":request.getParameter("x"));
  String cuenta = (request.getParameter("cuenta")==null?"":request.getParameter("cuenta"));
  model.tblgensvc.searchDatosTablaArea();  
  TreeMap tm = model.tblgensvc.getClaseArea ();         
  tm.remove("Seleccione un Item");
%>
<form name="forma1" action="" method="post">      
 
  <table width="566" border="2" align="center">
    <tr>
    <td width="585">
<table width="555"  >
          <tr>
    <td width="346" class="subtitulo1"><p align="left">Areas</p></td>
    <td width="193"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
  </tr>
</table>
        <table width="556" >
          <tr class="tblTitulo" id="titulos">
    <td width="56" height="22">Codigo</td>
    <td width="334" >Clientes</td>
    </tr>
<%
        Iterator it = tm.keySet ().iterator ();
            while (it.hasNext ()){
			 String codigo = (String) it.next ();
                String valor  = (String) tm.get (codigo);
%>  
  <tr class="fila">
    <td width="56" height="22"><%=valor%></td>
    <td width="334" ><a  onClick="asignarCodigoArea('<%=x%>','<%=valor%>','<%=codigo%>', '<%=cuenta%>')" style="cursor:hand" ><%=codigo%></a></td>
    </tr>
<%
      }
%>  
</table>
</td>
</tr>
</table><br>
<center><img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir..." onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"><p></center>
	<p>
    </p>
</form>
</div>
</body>
</html>
    