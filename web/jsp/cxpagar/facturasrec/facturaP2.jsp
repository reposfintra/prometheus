<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar el formulario para la 
                            insercion de documentos por pagar
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head><title>Documentos por pagar</title>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/CXP/cxp.js"></script>
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Estilo1 {color: #D0E8E8}
-->
</style>
</head>

<%
   
   String id_agencia_usuario =  (String)session.getAttribute("id_agencia" );  
   String validar_agencia    =  (request.getParameter ("ag")==null?"":request.getParameter ("ag"));  
   
   String cod_agencia_cont   =  (String)session.getAttribute("agContable");
   String unidad_contable    =  (String)session.getAttribute("unidadC");
     
   String codigocliente      =  (request.getParameter ("cliente")==null?"":request.getParameter ("cliente")); 
   String opcion 		     =  (request.getParameter ("op")==null?"NO":request.getParameter ("op")); 
   String indice		     =  (request.getParameter ("indice")==null?"":request.getParameter ("indice")); 
   String cod_cuenta		 =  (request.getParameter ("cuenta")==null?"":request.getParameter ("cuenta")); 
%>
<body  <%if (opcion.equals("cargar")){%> onLoad="LoadCodigo(<%=indice%>, '<%=codigocliente%>', '<%=unidad_contable%>', '<%=cod_cuenta%>');cargar('<%=validar_agencia%>');" <%}else{%>onResize="redimensionar()" <%if(request.getParameter("reload")!=null){%>
        onLoad = "redimensionar();parent.opener.location.href='<%=BASEURL%>/jsp/cxpagar/facturasxpagar/facturaP.jsp?';parent.close();" 
  <%} else {%>
       onLoad = "redimensionar();cargar('<%=validar_agencia%>');";
  <%}
  }%>
>

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Documentos por Pagar"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%

	java.util.Date utilDate = new java.util.Date(); //fecha actual
    long lnMilisegundos = utilDate.getTime();
    java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(lnMilisegundos);
	
    CXP_Doc doc;
    doc = model.cxpDocService.getFactura();
						
    String subtotal=(request.getParameter ("total")==null?"":request.getParameter ("total"));
    String tipo_documento="";
    String documento= ""; 
    String proveedor="";
    String tipo_documento_rel ="";
    String documento_relacionado="";
    String fecha_documento=(""+sqlTimestamp).substring(0,10);
    String c_banco="BANCOLOMBIA";
    String b_sucursal="";
    String descripcion="";
    String observacion="";
    String o_moneda="PES";
    double vlr_neto=0;
	double vlr_total=0;
    String usuario_aprobacion="";
    int plazo =0;
    if(doc!=null){
		
        tipo_documento=""+doc.getTipo_documento();
        documento= ( doc.getDocumento() != null )?doc.getDocumento():"";
        proveedor = ""+doc.getProveedor();
        tipo_documento_rel =""+doc.getTipo_documento_rel();
        documento_relacionado=""+doc.getDocumento_relacionado();
        fecha_documento = (doc.getFecha_documento() != null )?doc.getFecha_documento() :"";
        c_banco = doc.getBanco();
		model.servicioBanco.loadSusursales(c_banco);
        b_sucursal  = doc.getSucursal();
        descripcion =  ( doc.getDescripcion() != null )?doc.getDescripcion():"";
        observacion =   ( doc.getObservacion() != null)?doc.getObservacion() :"";
        o_moneda    =  ( doc.getMoneda() != null )?doc.getMoneda():"PES";
        vlr_neto = doc.getVlr_total();
		vlr_total = doc.getVlr_neto();
		//out.println("<br><br><br><br><br><br> "+vlr_neto);
        plazo=doc.getPlazo();
        usuario_aprobacion=doc.getUsuario_aprobacion();
    }
    model.cxpDocService.setFactura(null);
  
    Usuario usuarioLogin  = (Usuario) session.getAttribute("Usuario");
    String us  = usuarioLogin.getLogin();
    String agencia = usuarioLogin.getCia();
    String handle_code="";
    String id_mims="";
    String fecha_aprobacion="";

    String op= (request.getParameter("op")!=null )?request.getParameter("op"):"";
    String para1 = "/jsp/cxpagar/facturasxpagar/buscarnit.jsp?fechadoc="+fecha_documento+"&plazo="+plazo;
    para1=Util.LLamarVentana(para1, "Buscar Proveedor");

    String nombre_proveedor="";
    if (proveedor.equals("null")||proveedor.equals("")){
        proveedor ="";
    }
    else{
        Proveedor o_proveedor = model.proveedorService.obtenerProveedorPorNit(proveedor);
        if( o_proveedor!=null ){
            id_mims=""+o_proveedor.getC_idMims();

            if(!op.equals("cargarB")){
                c_banco=""+o_proveedor.getC_branch_code();
                b_sucursal=""+o_proveedor.getC_bank_account();				
            }
            nombre_proveedor = o_proveedor.getC_payment_name();
        }
    }
    TreeMap codCliAre = model.cxpDocService.llenarCodCliAre ();
    TreeMap b = model.servicioBanco.obtenerNombresBancosPorAgencia("BQ");
    TreeMap sbancos = model.servicioBanco.getSucursal();
    TreeMap t_moneda = model.monedaService.obtenerMonedas();
    TreeMap documentos = model.documentoSvc.obtenerDocumentosT();
   // TreeMap u = model.usuarioService.getUsuariosAprobacionT();
    TreeMap u = model.tblgensvc.getUsuarios_aut();
    Vector vTiposImpuestos=model.TimpuestoSvc.vTiposImpuestos();
	
	TreeMap agencias = model.agenciaService.getAgencia();
	
    int num_items=1;
   // num_items=Integer.parseInt(""+request.getParameter("num_items"));
    Vector vItems = model.cxpItemDocService.getVecCxpItemsDoc();
    String descripcion_i="";
    String codigo_cuenta="";
    String codigo_abc="";
    String planilla="";
	String tipcliarea="";
	String codcliarea="";
	String descliarea="";
    Vector vImpuestosI;
	Vector Copia;
    double valor   = 0;
	double valor_t = 0;
    if (vItems == null){  	     
        System.out.println("el Vector de items es nulo ");
    }
    else{
        System.out.println("el Vector de items no es nullo ");
        for(int i=0;i<vItems.size();i++){
            CXPItemDoc item = (CXPItemDoc)vItems.elementAt(i);
            descripcion_i=""+ item.getDescripcion();
            codigo_cuenta=""+ item.getCodigo_cuenta();
            codigo_abc=""+ item.getCodigo_abc();
            planilla=""+ item.getPlanilla();
            valor	=	item.getVlr();			
			valor_t = 	item.getVlr_total();
			tipcliarea=""+item.getTipcliarea ();
			codcliarea=""+item.getCodcliarea ();
			descliarea=""+item.getDescliarea ();
            vImpuestosI= item.getVItems();
            if (vImpuestosI == null){
                out.print("<br> el vector de Impuestos es null ");
            }
            for(int ii=0;ii<vImpuestosI.size();ii++){
                CXPImpItem impuestoItem = (CXPImpItem)vImpuestosI.elementAt(ii);
                String impuesto = impuestoItem.getCod_impuesto();
            }
        }    
    }
	Vector NumeroFacturas =  model.cxpDocService.getVFacturas ();
%>
<script language="javascript" src="<%= BASEURL %>/js/validarDocPorPagar.js"></script>
<script>

    function insertarItem(validar){
        var ii=0;
		var suma=0;
		var sw=false;
        for (var i=0;i< detalle.rows.length;i++){
                ii=ii+1;
				
				if(ii<detalle.rows.length){
					var subtotal=document.getElementById("valorNeto"+ii);
					if(ii==detalle.rows.length-1){
						if(subtotal.value!=""){
								sw=true;
						}
					}
					st=document.getElementById("total");
					suma=suma+parseFloat(obtenerNumero(subtotal.value));
					st.value=formatearNumero(suma+"");
				}
        }
		//alert("ITEM" + ii);
		if(sw==true){
			var fila = detalle.insertRow(ii);
			fila.className="fila";
	
			var celda1 = fila.insertCell();
			var celda2 = fila.insertCell();
			var celda3 = fila.insertCell();
			var celda4 = fila.insertCell();
			var celda5 = fila.insertCell();
			var celda51 = fila.insertCell();
			var celda52 = fila.insertCell();
			//var celda53 = fila.insertCell();
			var celda6 = fila.insertCell();
			var celda7 = fila.insertCell();
			celda1.className="bordereporte";
			celda2.className="bordereporte";
			celda3.className="bordereporte";
			celda4.className="bordereporte";
			celda5.className="bordereporte";
			celda51.className="bordereporte";
			celda52.className="bordereporte";
			celda6.className="bordereporte";
			celda7.className="bordereporte";
			
			
			ii=ii-1;
			fila.id="fila"+(ii+1);
			celda1.id="celda1"+ii;
			celda2.id="celda2"+ii;
			//alert(""+ii);
			var text1 = document.createTextNode(" ");
			
			/*var toClone1= document.getElementById("insert_item"+ii);
			var cloneNode1 = toClone1.cloneNode(true);
			cloneNode1.id="insert_item"+(ii+1);
			cloneNode1.name="insert_item"+(ii+1);*/
			
			
			var toClone111= document.getElementById("n_i"+ii);
			var cloneNode111 = toClone111.cloneNode(true);
			cloneNode111.id="n_i"+(ii+1);			
			cloneNode111.innerHTML= (ii+1)+"";
			
			celda1.align = "left";
			
			/*var eliminar  = document.getElementById("borrarI"+ii);
			var eliminar1 = eliminar.cloneNode(true);
			eliminar1.value=""+(ii+1);
			eliminar1.id="borrarI"+(ii+1);
			eliminar1.name="borrarI"+(ii+1);
			eliminar.onclick = new Function("borrarItem('"+(ii+1)+"')");*/
			
			var cod1 =document.getElementById("cod_item"+ii);
			var cod11=cod1.cloneNode(true);
			cod11.value = (ii+1);
			cod11.id="cod_item"+(ii+1);
			cod11.name="cod_item"+(ii+1);
			
			celda1.appendChild(cloneNode111);
			celda1.appendChild(cod11);			
			//celda1.appendChild(cloneNode1); 
			//celda1.innerHTML=celda1.innerHTML+"&nbsp;";
			
			celda1.appendChild(text1);
			//celda1.appendChild(eliminar1);
			
			
			var text2 = document.createTextNode(" ");
			 
			var toClone2  = document.getElementById("descripcion_i"+ii);
			var cloneNode2 = toClone2.cloneNode();
			cloneNode2.id="descripcion_i"+(ii+1);
			cloneNode2.value="";
			cloneNode2.name="descripcion_i"+(ii+1);
			celda2.align = "center";
			celda2.appendChild(cloneNode2);
			
			
			
			var toClone21= document.getElementById("arbol"+ii);
			var cloneNode21 = toClone21.cloneNode();
			cloneNode21.id="arbol"+(ii+1);
			cloneNode21.name="arbol"+(ii+1);
			cloneNode21.onclick = new Function("servicios('"+(ii+1)+"','"+controlador+"')") 
			//celda2.innerHTML=celda2.innerHTML+"&nbsp;";
			celda2.appendChild(text2);
			celda2.appendChild(cloneNode21);
			
			var toClone3= document.getElementById("codigo_cuenta"+ii);
			var cloneNode3 = toClone3.cloneNode();
			cloneNode3.id="codigo_cuenta"+(ii+1);
			cloneNode3.name="codigo_cuenta"+(ii+1);
			cloneNode3.value="";
			celda3.align = "center";
			celda3.appendChild(cloneNode3);
			
			var toClone3= document.getElementById("cod_cuenta"+ii);
			var cloneNode3 = toClone3.cloneNode();
			cloneNode3.id="cod_cuenta"+(ii+1);
			cloneNode3.name="cod_cuenta"+(ii+1);
			celda3.align = "center";
			celda3.appendChild(cloneNode3);
			
			
			var toClone4= document.getElementById("codigo_abc"+ii);
			var cloneNode4 = toClone4.cloneNode();
			cloneNode4.id="codigo_abc"+(ii+1);
			cloneNode4.value="";
			cloneNode4.name="codigo_abc"+(ii+1);
			celda4.align = "center";
			celda4.appendChild(cloneNode4);
			
			var toClone5= document.getElementById("planilla"+ii);
			var cloneNode5 = toClone5.cloneNode();
			cloneNode5.id="planilla"+(ii+1);
			cloneNode5.value="";
			cloneNode5.name="planilla"+(ii+1);
			celda5.align = "center";
			cloneNode5.onkeyup=  new Function("disabledPlanilla('"+(ii+1)+"', '"+validar+"')");
			celda5.appendChild(cloneNode5);
			
			var toClone51= document.getElementById("cod_oc"+ii);
			var cloneNode51 = toClone51.cloneNode(true);
			cloneNode51.id="cod_oc"+(ii+1);
			cloneNode51.name="cod_oc"+(ii+1);
			cloneNode51[0].text="C";
			cloneNode51[0].value="C";	
			var indice = (ii+1);
			//cloneNode51.onchange= new Function("buscarCodigoOC('2')") ;
			cloneNode51.onclick = new Function("buscarCodigoOC('"+indice+"')") ;
			celda51.align = "center";
			celda51.appendChild(cloneNode51);
			
			var toClone52= document.getElementById("oc"+ii);
			var cloneNode52 = toClone52.cloneNode();
			cloneNode52.id="oc"+(ii+1);
			cloneNode52.name="oc"+(ii+1);
			cloneNode52.value="";
			celda52.align = "center";
			celda52.appendChild(cloneNode52);
			
			var toClone53= document.getElementById("doc"+ii);
			var cloneNode53 = toClone53.cloneNode();
			cloneNode53.id="doc"+(ii+1);
			cloneNode53.name="doc"+(ii+1);
			cloneNode53.value="";
			celda52.align = "center";
			celda52.appendChild(cloneNode53);
			
			var toClone6= document.getElementById("valor1"+ii);
			var cloneNode6 = toClone6.cloneNode();
			//alert("valor1"+(ii+1))
			cloneNode6.id="valor1"+(ii+1);
			cloneNode6.value="";
			cloneNode6.name="valor1"+(ii+1);
			celda6.align = "center";
			cloneNode6.onkeyup = new Function("valorTotal("+(ii+1)+");");
			celda6.appendChild(cloneNode6);
			
			var toClone7= document.getElementById("valorNeto"+ii);
			var cloneNode7 = toClone7.cloneNode();
			cloneNode7.id = "valorNeto"+(ii+1);
			cloneNode7.value="";
			cloneNode7.name="valorNeto"+(ii+1);			
			celda7.align = "center";
			celda7.appendChild(cloneNode7);
			
			
			<%  
				int c =6;
				for (int i=0;i<vTiposImpuestos.size();i++){
					c=c+1;
			 %>
					var celda<%=c%> = fila.insertCell();
					var toClone<%=c%>= document.getElementById("impuesto<%=i%>"+ii);
					var cloneNode<%=c%> = toClone<%=c%>.cloneNode();
					cloneNode<%=c%>.id="impuesto<%=i%>"+(ii+1);
					cloneNode<%=c%>.value="";
					cloneNode<%=c%>.name="impuesto<%=i%>"+(ii+1);
					celda<%=c%>.align = "center";
					celda<%=c%>.className="bordereporte";
					var toClone0<%=c%>= document.getElementById("tipo_impuesto<%=i%>"+ii);
					var cloneNode0<%=c%> = toClone0<%=c%>.cloneNode();
					cloneNode0<%=c%>.id="tipo_impuesto<%=i%>"+(ii+1);
					cloneNode0<%=c%>.name="tipo_impuesto<%=i%>"+(ii+1);
						
					var image<%=c%>= document.getElementById("imageB_<%=i%>_"+ii);
					var imageC<%=c%> = image<%=c%>.cloneNode();
					imageC<%=c%>.id="imageB_<%=i%>_"+(ii+1);
					imageC<%=c%>.name="<%=i%>"+(ii+1);
	
					celda<%=c%>.appendChild(cloneNode<%=c%>); 
					celda<%=c%>.appendChild(cloneNode0<%=c%>); 
					celda<%=c%>.appendChild(imageC<%=c%>);
				<%}%>
			}
			else{
			  alert("Debe digitar un Valor para el Item");
			}		
	}
	
	function borrarItem(){
			indice = detalle.rows.length - 1;
			var tabla = document.getElementById("detalle");
			if(indice != 1){
				tabla.deleteRow(indice);				
			}
			else{
				var descripcion = document.getElementById("descripcion_i"+(indice));
			    descripcion.value="";
						
				var codigo_cuenta= document.getElementById("codigo_cuenta"+(indice));
				codigo_cuenta.value="";
						
				var codigo_abc=document.getElementById("codigo_abc"+(indice));
				codigo_abc.value="";
						
				var planilla=document.getElementById("planilla"+(indice));
				planilla.value="";
						
				var valor=document.getElementById("valor1"+(indice));
				valor.value="";
				
				var valorN =document.getElementById("valorNeto"+(indice));
				valorN.value="";
						
				<%  c =6;
					for (int l=0;l<vTiposImpuestos.size();l++){
						c = c + 1;
				%>
				var impuesto<%=c%>= document.getElementById("impuesto<%=l%>"+(indice));   
			    impuesto<%=c%>.value="";
							
				var tipo_impuesto<%=c%>= document.getElementById("tipo_impuesto<%=l%>"+(indice));   
	            tipo_impuesto<%=c%>.value="";
		                                          
			        <%}%>	
						
			}
		
			var suma = 0;
			for (var i = 1;i < detalle.rows.length; i++){
				var subtotal = document.getElementById("valorNeto"+i);						
				st = document.getElementById("total");								
				var valor = subtotal.value.replace( new RegExp(",","g"), "");
                if (!isNaN ( parseFloat(valor)) ){
					suma += parseFloat(valor);
				}					  
			}
			st.value = formato(suma);
			
			var vlr_total = 0;
		    var idItem = indice;
            with (forma1){
              for (i=0; i<elements.length; i++){                  
				  if (elements[i].name.indexOf('valor1')==0){
                     var valor = elements[i].value.replace( new RegExp(",","g"), "");
                     if (!isNaN ( parseFloat(valor)) ){
                        vlr_total += parseFloat(valor);						
                     }					 
                  }
              }
          }		
		forma1.vlr_neto.value =  formato(vlr_total);
	}
	
	function valorTotal(id){        		 
		  var vlr_total = 0;
		  var vNeto  = 0;
		  var idItem = id;
          with (forma1){
              for (i=0; i<elements.length; i++){                  
				  if (elements[i].name.indexOf('valor1')==0){
                     var valor = elements[i].value.replace( new RegExp(",","g"), "");
					 if (!isNaN ( parseFloat(valor)) ){
						vlr_total += parseFloat(valor);
						if( elements[i].name == ('valor1'+ idItem) ){
							var vlr = document.getElementById("valorNeto"+idItem) ;
							vlr.value = formato(valor);
						}
                     }					 
                  }
				  
				  if (elements[i].name.indexOf('valorNeto')==0){
                     var vN = elements[i].value.replace( new RegExp(",","g"), "");
					 if (!isNaN ( parseFloat(vN)) ){
						vNeto += parseFloat(vN);						
                     }					 
                  }
				  
              }
          }		
		forma1.vlr_neto.value =  formato(vlr_total);
		forma1.total.value    =  formato(vNeto);
	}	
	
	 function formato(numero){
           
           var tmp = parseInt(numero) ;
           var factor = (tmp < 0 ? - 1 : 1);
           tmp *= factor;
          
           var num = '';
           var pos = 0;
           while(tmp>0){
                  if (pos%3==0 && pos!=0) num = ',' + num;
                  res  = tmp % 10;
                  tmp  = parseInt(tmp / 10);
                  num  = res + num  ;
                  pos++;
           }
           return (factor==-1 ? '-' : '' ) + num ;
        }      
        
        function sinformato(element){
           return element.replace( new RegExp(",","g") ,'');
        }
		
		function disabledPlanilla(x, validar ){
		  var comboPlanilla = document.getElementById('cod_oc'+x);
		  var planilla      = document.getElementById('planilla'+x);
		  var descripcion   = document.getElementById('descripcion_i'+x);
		  if( planilla.value.length != 0 )
		  	comboPlanilla.disabled = true;		
		  else  
		    comboPlanilla.disabled = false;		
			if (window.event.keyCode==13){ 
			    if( descripcion.value.length != 0 ){
					BuscarAccountCode( planilla.value, x, validar );
				}
				else{
					alert('Debe seleccionar un concepto de pago');
				}
			}
		}
		
		function BuscarAccountCode( numpla, x , validar ){
			var sele   =   document.getElementById("codigo_cuenta"+x);	
		    var hidden =   document.getElementById("cod_cuenta"+x);							
			var cuenta =   sele.value;
			document.forma1.action = controlador+"?estado=FacturaSearch&accion=CodigoCuenta&numpla="+numpla+"&indice="+x+"&hidden="+hidden.value+"&cuenta="+cuenta+"&validar="+validar; 
			document.forma1.submit(); 
		} 
	
	
</script>
<form name="forma1" id="forma1" action="" method="post"> 
<%
  String mensaje=""+request.getParameter("ms");
    if (! (mensaje.equals("")|| mensaje.equals("null"))){
%>

<table border="2" align="center">
  <tr>
    <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="282" align="center" class="mensajes"><%=mensaje%></td>
          <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="78">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<br>
<%
    }   
%>
     


<table>
<tr><td>
<table border="2"  width="1000">
  <tr >
      <td> 
        <table width="100%" align="center"  >
          <tr >
            <td width="374"  class="subtitulo1"><p align="left">Documento</p></td>
            <td width="585"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" class="Estilo1"></td>
          </tr>
        </table>
        <table width="100%" align="center" cols="7">
          <tr class="fila">
            <td  >Documento:</td>
            <td >
                <input:select name="tipo_documento"      attributesText=" id='documentos' style='width:100%;' class='listmenu'  onChange='docRel(this.value);'"  default="010" options="<%=documentos%>" />
            </td>
            <td width="125" ><input name="documento" type="text" class="texto_obligatorio" id="documento" size="20" maxlength="30" value="<%=documento%>">            </td>
            <td width="139" > Proveedor: </td>
            <td width="202">
              <%/*
                            String para2 = "/jsp/cxpagar/facturasxpagar/buscarnit1.jsp?num_items="+num_items;
                            para2=Util.LLamarVentana(para2, "Buscar proveedores");*/
			%>
              <input name="proveedor"  type="text" class="texto_obligatorio" value="<%=proveedor%>" id="proveedor" size="18" maxlength="15" >
			              &nbsp;&nbsp;<img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  width="15" height="15"  onClick="buscarProveedores('<%=CONTROLLER%>', '<%=validar_agencia%>');" title="Buscar" style="cursor:hand" ></td>
            <td colspan="2">
            <input type="hidden" name="nombre_proveedor" id="nombre_proveedor" size="50" class="textbox" value='<%=nombre_proveedor%>'> 
            <div id="nombreP"><%=nombre_proveedor%></div></td></tr>
		  <tr class="fila">
		  	<td >Documento Rel:</td>
			<td >
           		<input:select name="tipo_documento_rel"      attributesText=" id='tipo_documento_rel' style='width:100%;' class='listmenu'  disabled"  default="010" options="<%=documentos%>" />
			</td>
			<td >
			 
			  <select name="documento_relacionado" id="documento_relacionado"  disabled>
			  <%   
			    if (NumeroFacturas!= null){
			     for(int i=0;i<NumeroFacturas.size();i++){%>
			  <option value ="<%=NumeroFacturas.elementAt(i)%>><%=NumeroFacturas.elementAt(i)%>"</option>
			  <% }}%> 
			  </select>
			
			</td>
			<td  >Fecha  :</td>				
			<td ><input name="fecha_documento" type="text" class="textbox" id="fecha_documento" size="20" value="<%=fecha_documento%>">      <span class="comentario"></span></span> 
			 <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma1.fecha_documento);return false;" HIDEFOCUS>
			  <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha">
			</td>
			<td >Plazo(d&iacute;as):</td>
			<td ><input name="plazo" type="text" class="textbox" value="<%=plazo%>"   id="plazo2" size="4" maxlength="3"></td>
		  </tr>
		  <tr class="fila">
		  	<td><p>Valor:</p></td>
			<td ><input name="vlr_neto" type="text" onKeyPress="soloDigitos(event, 'decNO')"   onChange="formatear(this)" class="textbox" id="vlr_neto" size="10" value='<%=model.cxpDocService.formatearNumero ((vlr_neto+"").substring(0,(vlr_neto+"").indexOf(".")))%>' maxlength="10" readonly="true" align="right">
			</td>
			<td ><input name="total" type="text" id="total" value='<%=model.cxpDocService.formatearNumero ((vlr_total+"").substring(0,(vlr_total+"").indexOf(".")))%>'   readonly align="right"></td>
			<td >Moneda:</td>		
			<td  ><input:select name="moneda" attributesText=" id='moneda' style='width:80%;' class='listmenu' "  default="<%=o_moneda%>" options="<%=t_moneda%>" /></td>
			<td >Banco:</td>	
			<td ><input:select name="c_banco"      attributesText="<%=" id='c_banco' style='width:150px;' class='listmenu'  onChange=\\" bancos('"+CONTROLLER+"','"+validar_agencia+"'); \\" "%>"  default="<%=c_banco%>" options="<%=b%>" /></td>		
  		</tr>
		  <tr class="fila">
		  	  <td >Descripci&oacute;n:</td>
			  <td colspan="4"> 
                  <div align="left">
                    <textarea name="descripcion" cols="105" rows="1" class="textbox"><%=descripcion%></textarea>
                  </div></td>
				<td>Sucursal:</td>
			    <td ><input:select name="c_sucursal"    attributesText=" id='c_sucursal' style='width:80%;' class='listmenu'"  options="<%=sbancos%>" /></td>		
				<script>forma1.c_sucursal.value = '<%=b_sucursal%>';</script>
	      </tr>
  		
		    <tr class="fila">
		  	  <td rowspan="2">Observaci&oacute;n: </td>
			  <td  colspan="4" rowspan="2">
			      <div align="left">
			        <textarea name="observacion" cols="105" rows="2" class="textbox"><%=observacion%></textarea>
                  </div></td>
			  <td>Autorizador: </td>
			  <td ><input:select name="usuario_aprobacion" attributesText="style='width:80%;' class='listmenu'" default="<%=usuario_aprobacion%>" options="<%=u%>" /></td>
		    </tr>
		    <tr class="fila">
		      <td id="ag">Agencia :</td>
		      <td><input:select name="agencia" attributesText="<%=" id='agencia' style='width:80%;' class='listmenu' onChange=\\" BuscarAgencias('"+CONTROLLER+"'); \\" "%>"  options="<%=agencias%>" /></td>
			  <script>forma1.agencia.value = '<%=id_agencia_usuario%>';</script>
		    </tr>
	  </table></td>
</tr>
</table>
	<table width="1000" border="2" align="left">
		<tr>
		<td>
			<table width="100%" align="center"  >
          <tr >
            <td width="374"  class="subtitulo1"><p align="left">Detalle del Documento </p></td>
            <td width="585"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" class="Estilo1"></td>
          </tr>
        </table>
			<div align="left">
			<table id="detalle" width="76%" border="1" align="left" bordercolor="#999999" bgcolor="#F7F5F4">
				<tr class="tblTitulo" id="fila1">
					<td nowrap><div align="center">Item</div></td>
					<td ><div align="center">Descripcion</div></td>
					<td  ><div align="center">Cuenta </div></td>
					<td ><div align="center">ABC </div></td>
					<td  ><div align="center">Planilla</div></td>
					<td ><div align="center">Tipo</div></td>
					<td ><div align="center">Centro</div></td>
					<td ><div align="center">Valor</div></td>
					<td ><div align="center">Valor Neto</div></td>
					<%for(int i=0;i< vTiposImpuestos.size();i++){ %>
					<td width="100"  nowrap><div align="center"><%=vTiposImpuestos.elementAt(i)%> </div></td>
					<%}%>
				</tr>
				<% int x=1;
				if ( vItems == null){%>
				<tr class="fila" nowrap>
					<td class="bordereporte" nowrap>
						<a id="n_i<%=x%>" ><%=x%></a><input name="cod_item<%=x%>" id="cod_item<%=x%>" value="<%=x%>" type="hidden" size="4" maxlength="5" border="0"><a onClick="insertarItem('<%=validar_agencia%>');"  id="insert_item<%=x%>" style="cursor:hand" ><img src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width="12" height="12"  ></a> <a onClick="borrarItem(this.value);"  value="1" id="borrarI<%=x%>" style="cursor:hand" ><img src='<%=BASEURL%>/images/botones/iconos/menos1.gif'   width="12" height="12"  > </a>
					</td>
					<td width="200" height="30" class="bordereporte" nowrap>						<div align="center">
						<input name="descripcion_i<%=x%>" id="descripcion_i<%=x%>" type="text" size="30"  maxlength="40" readonly>
						<img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name="arbol<%=x%>" width="15" height="15"  id="arbol<%=x%>" style="cursor:hand" title="Buscar"  onClick="servicios('<%=x%>','<%=CONTROLLER%>');" >                        
					</div>
					</td>
					<td width="74" class="bordereporte">						<div align="center">
						<input name="codigo_cuenta<%=x%>" id="codigo_cuenta<%=x%>" type="text" size="13" maxlength="20" readonly>
						<input name="cod_cuenta<%=x%>" id="cod_cuenta<%=x%>"       type="hidden" size="4" maxlength="5" border="0" value="<%=cod_agencia_cont + "," + unidad_contable%>">
					</div>
					</td>
					<td width="43" class="bordereporte">
                      <div align="center">
                        <input name="codigo_abc<%=x%>" id="codigo_abc<%=x%>" type="text" size="4" maxlength="2">
                    </div></td>
					<td width="71" class="bordereporte">
						<div align="center" >
						<input name="planilla<%=x%>" id="planilla<%=x%>" type="text" size="8" maxlength="8" onKeyUp="disabledPlanilla('<%=x%>', '<%=validar_agencia%>')">
						</div>
					</td>
					<td width="42" nowrap class="bordereporte">
                      <div align="center">
					  <input:select name="<%= ("cod_oc" + x) %>" attributesText="<%= "id='cod_oc" + x +"' style='width:80%;' class='listmenu'  onClick=\\" buscarCodigoOC('"+x+"') \\"   "  %>"  default="<%=tipcliarea%>" options="<%=codCliAre%>" />
                    </div></td>
					<td width="61" nowrap class="bordereporte">                      <div align="center">                        <input name="oc<%=x%>" type="text" id="oc<%=x%>" size="6" maxlength="8">
                        <input name="doc<%=x%>" id="doc<%=x%>" type="hidden">
                      </div></td>
					<td width="101" class="bordereporte">
						<div align="center">
						<input name="valor1<%=x%>" id="valor1<%=x%>" type="text" size="16"  onChange="formatear(this);" onKeyPress="soloDigitos(event, 'decNO')"  maxlength="11" onKeyUp="valorTotal(<%=x%>);" align="right">
						</div>
					</td>  
					<td width="101" class="bordereporte">
						<div align="center">
						<input name="valorNeto<%=x%>" id="valorNeto<%=x%>" type="text" size="16"  onChange="formatear(this);" onKeyPress="soloDigitos(event, 'decNO')"  maxlength="11" align="right">
						</div>
					</td>
					<%for(int l = 0;l < vTiposImpuestos.size();l++){ %>
					<td width="256" class="bordereporte">			    <div align="center">
						<input name="impuesto<%=l%><%=x%>" id ="impuesto<%=l%><%=x%>" type="text" size="6" maxlength="10" onKeyUp="procesar(this,<%=x%>);" readonly>
						<input name="tipo_impuesto<%=l%><%=x%>" id ="tipo_impuesto<%=l%><%=x%>" type="hidden" size="10" value="<%=vTiposImpuestos.elementAt(l)%>"> 
						<img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name="<%=l%><%=x%>" width="15" height="15"  id="imageB_<%=l%>_<%=x%>" style="cursor:hand" title="Buscar"  onClick="funI('<%=vTiposImpuestos.elementAt(l)%>',this.name);" >				  </div>
					</td>  
					<%}%>      
					
				</tr>   
				<% }
				else{
				// System.out.println("Los Items en la pagina no son null y tienen tama�o "+ vItems.size());
				for( x=1;x<=vItems.size();x++){
                                    CXPItemDoc item = (CXPItemDoc)vItems.elementAt(x-1);
                                    descripcion_i=""+ item.getDescripcion();
                                    codigo_cuenta=""+ item.getCodigo_cuenta();
                                    codigo_abc=""+ item.getCodigo_abc();
                                    planilla=""+ item.getPlanilla();
									tipcliarea=""+item.getTipcliarea ();
									codcliarea=""+item.getCodcliarea ();
									descliarea=""+item.getDescliarea ();
                                    valor	=	item.getVlr();			
									valor_t = 	item.getVlr_total();
				%>
                <tr class="fila" nowrap>
                	<td class="bordereporte" nowrap>
						<a id="n_i<%=x%>" ><%=x%></a>
						<input name="cod_item<%=x%>" id="cod_item<%=x%>" value="<%=x%>" type="hidden" size="4" maxlength="5" border="0"> 
					<% if( x == 1 ){ %>	
						<a onClick="insertarItem('<%=validar_agencia%>');"  id="insert_item<%=x%>" style="cursor:hand" ><img src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width="12" height="12"  ></a>  
						<a onClick="borrarItem(this.value);valorTotal('<%=x%>');"  value="1" id="borrarI<%=x%>" style="cursor:hand" ><img src='<%=BASEURL%>/images/botones/iconos/menos1.gif'   width="12" height="12"  ></a>
					<%}%>
					</td>					
					<td width="200" height="30" class="bordereporte" nowrap>						<div align="center">
				   <input name="descripcion_i<%=x%>" id="descripcion_i<%=x%>" type="text" size="30" value='<%=descripcion_i%>' maxlength="40" readonly>
						<img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name="arbol<%=x%>" width="15" height="15"  id="arbol<%=x%>" style="cursor:hand" title="Buscar"  onClick="servicios('<%=x%>','<%=CONTROLLER%>');" >
						</div>
					</td>
              		<td width="74" class="bordereporte">						<div align="center">
						<input name="codigo_cuenta<%=x%>" id="codigo_cuenta<%=x%>" type="text" size="13" value='<%=codigo_cuenta%>' maxlength="20" readonly>
						<input name="cod_cuenta<%=x%>" id="cod_cuenta<%=x%>" value="<%=cod_agencia_cont+","+unidad_contable%>" type="hidden" size="4" maxlength="5" border="0">
						</div>
					</td>
              		<td width="43" class="bordereporte">
                      <div align="center">
                        <input name="codigo_abc<%=x%>" id="codigo_abc<%=x%>" type="text" size="4" value="<%=codigo_abc%>" maxlength="2">
                    </div></td>
              		<td width="71" class="bordereporte">
						<div align="center">
						<input name="planilla<%=x%>" id="planilla<%=x%>" type="text" size="8" value="<%=planilla%>" maxlength="8" onKeyUp="disabledPlanilla('<%=x%>', '<%=validar_agencia%>')">
						</div>
					</td>
              		<td width="42" class="bordereporte"><div align="center">
              		   <input:select name="<%= ("cod_oc" + x) %>" attributesText="<%= "id='cod_oc" + x +"' style='width:80%;' class='listmenu'  onClick=\\" buscarCodigoOC('"+x+"') \\"  " %>"  default="<%=tipcliarea%>" options="<%=codCliAre%>" />
		</div></td>
              		<td width="61" class="bordereporte"><div align="center">
              		  <input name="oc<%=x%>" type="text" id="oc<%=x%>" size="6" maxlength="8"  value="<%=codcliarea%>"  title="<%=descliarea%>"readonly>
                      <input name="doc<%=x%>" type="hidden" id="doc<%=x%>" value="<%=descliarea%>">
              		</div></td>
              		<td width="101" class="bordereporte">
						<div align="center">
						<input name="valor1<%=x%>" id="valor1<%=x%>"  onKeyPress="soloDigitos(event, 'decNO')"  type="text"  onChange="formatear(this);" size="16" value="<%=model.cxpDocService.formatearNumero ((""+valor).substring(0,(""+valor).indexOf(".")))%>" maxlength="11" onKeyUp="valorTotal(<%=x%>);" align="right">
						</div>
					</td> 
					<td width="101" class="bordereporte">
						<div align="center">
						<input name="valorNeto<%=x%>" id="valorNeto<%=x%>" type="text" size="16"  onChange="formatear(this);" onKeyPress="soloDigitos(event, 'decNO')"  maxlength="11"  readonly="true" align="right" value="<%=model.cxpDocService.formatearNumero ((""+valor_t).substring(0,(""+valor_t).indexOf(".")))%>">
						</div>
					</td> 					    
					<%
						Copia = item.getVCopia();
						System.out.println(Copia.size());
						for(int i=0;i< Copia.size();i++){ 
							CXPImpItem impuestoCopia = (CXPImpItem)Copia.elementAt(i);
							String impuesto = impuestoCopia.getCod_impuesto();
							String tipo 	= impuestoCopia.getTipo_impuesto();
					%>  					
              		<td width="256" class="bordereporte">
						<div align="center">
						<input name="impuesto<%=i%><%=x%>" id ="impuesto<%=i%><%=x%>" type="text" size="6" value="<%=impuesto%>" maxlength="10" onKeyUp="procesar(this,'<%=x%>');"  readonly>
						<img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'    name="<%=i%><%=x%>" width="15" height="15"   id="imageB_<%=i%>_<%=x%>" style="cursor:hand" title="Buscar"  onClick="funI('<%=vTiposImpuestos.elementAt(i)%>',this.name);" >				
						<input name="tipo_impuesto<%=i%><%=x%>" id ="tipo_impuesto<%=i%><%=x%>3" type="hidden"size="10" value="<%=vTiposImpuestos.elementAt(i)%>">
						</div>
					</td>
				<%}%>
				</tr>   
            <%}
            }%>
			</table>
		  </div>		  </td>
		</tr>
	</table>
<input name="num_items" id="num_items" type="hidden" size="16" value='1' maxlength="16">

</td></tr>
</table>
  <tr>

    <div align="center">
		<img src='<%=BASEURL%>/images/botones/iconos/guardar.gif' name='Buscar' width="25" height="25" align="absmiddle" style='cursor:hand' title='Guardar...'  onclick="escribirArchivo('<%=CONTROLLER%>');" >        &nbsp;<img src='<%=BASEURL%>/images/botones/aceptar.gif' name='Buscar' align="absmiddle" style='cursor:hand'   onclick="validarDocumento('<%=CONTROLLER%>', '<%=validar_agencia%>');" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
        <img src='<%=BASEURL%>/images/botones/restablecer.gif' name='restablecer' align="absmiddle" style='cursor:hand'   onClick="Restablecer('<%=CONTROLLER%>');"  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
		<img src='<%=BASEURL%>/images/botones/salir.gif' name='Buscar' align="absmiddle" style='cursor:hand'   onClick="window.close();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
    </div>
  <tr>
  </form>
</div>
</body>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</html>
<script>
    document.forma1.documento.focus();
	window.resizeTo(1024,745);
	window.moveTo(0,0);
</script>
    