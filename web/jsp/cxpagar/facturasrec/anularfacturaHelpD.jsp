<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA DESCRIPTIVA - Anular Factura
	 - Date            :      13/06/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>AYUDA DESCRIPTIVA - Anular Factura</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center"> FACTURAS RECURREMTES</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Anular Factura  - Pantalla Preliminar</td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2">INFORMACION DE LA FACTURA </td>
        </tr>
        <tr>
          <td  class="fila">N&deg;</td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n donde escojo el tipo del documento; y al lado se ingresa el c&oacute;digo del documento que desea anular. Este campo es de m&aacute;ximo 30 caracteres. </td>
        </tr>
        <tr>
          <td class="fila">Nit del Proveedor </td>
          <td  class="ayudaHtmlTexto">Campo para ingresar el nit del proveedor. Este campo es de m&aacute;ximo 15 caracteres. </td>
        </tr>
        <tr>
          <td class="fila">Bot&oacute;n Buscar</td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que realiza la busqueda de la informaci&oacute;n de la factura digitada en el campo.</td>
        </tr>
		<tr>
          <td class="fila">Bot&oacute;n Cancelar</td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que cancela todas los operaciones realizadas, y resetea la pagina llevandola a su estado inicial.</td>
        </tr>
		<tr>
          <td width="149" class="fila">Bot&oacute;n Salir</td>
          <td width="525"  class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista 'Anular Factura' y volver a la vista del men&uacute;.</td>
        </tr>
        <tr>
          <td width="149" class="fila">Bot&oacute;n Anular </td>
          <td width="525"  class="ayudaHtmlTexto">Bot&oacute;n para realizar la anulaci&oacute;n de la factura deseada.</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</body>
</html>
