<%-- 
    Document   : reversarPlanillasTsp
    Created on : 14/10/2014, 09:10:25 AM
    Author     : lcanchila
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Reversar Planillas TSP</title>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/reestructuracion.css" type="text/css" rel="stylesheet" />
        <link href="<%=BASEURL%>/css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/jquery/jquery-ui/jquery-ui.css"/>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/reversionAnticipos.js"></script>

    </head>
    <body>
        <input name="fechaTransf" type="hidden" id="fechaTransf">
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=REVERSAR ANTICIPOS"/>
        </div>


        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow:auto">
            <center>
                <table>
                    <tr>
                        <td>
                            <div class="k-block" style=" width: 600px">
                                <div class="k-header" style=" padding: 2px"><span class="titulo">BUSCAR PLANILLAS TSP</span> </div>
                                <div id="contenido">
                                    <table border="0" class="table"  >
                                        <tr>
                                            <td class="td"><label for="fecha"> Fecha </label></td>
                                            <td class="td"><input type="text" size="15px"  style=" font-size: 12px" required="true" readonly="true" name="fecha" id="fecha"/></td>
                                            <td class="td"><label for="user"> Usuario </label></td>
                                            <td class="td"> <select name="user" id="user"  style=" font-size: 12px" ></select></td>
                                            <td class="td">
                                                <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="buscarPlanillasAnticipo();">
                                                    <span class="ui-button-text">Buscar</span>
                                                </button>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
                </br> </br></br></br>
                <table id="grid_planillasAnticipo"></table>
            </center> 
        </div>
        <div  id="div_detalle_anticipo" class="ventana" title="Mensaje" > 
            <br>
            <table id="detalle_anticipos" ></table>
        </div>  
        <div id="dialogo" class="ventana" title="Mensaje">
            <p  id="msj" >texto </p>
        </div>
        <div id="dialogo2" class="ventana">
            <p  id="msj2">texto </p> <br/>
            <center>
                <img src="<%=BASEURL%>/images/cargandoCM.gif"/>
            </center>
        </div>

    </body>
</html>
