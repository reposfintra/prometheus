<!--
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      19/02/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Vista de listado de cuotas de Prestamos
 --%>

 
<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>

<html>
<head>
       <title>Listado de Cuotas</title>  
       <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
       <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>  
       <script type='text/javascript' src="<%= BASEURL %>/js/Validaciones.js"></script>

       <script>
       
             
              function sendCuotas(url,opcion ){  
                    url = url +'&evento='+ opcion;
                    if(opcion=='UPDATE_FECHA'  ||  opcion=='UPDATE_ALL' )
                         document.location.href=(url);

                    if(opcion=='MIGRAR' || opcion=='ADD'){                
                           var parametros= '';
                           var cont      = 0;
                           for(i=0;i<document.forms.length;i++) {
                                if(document.forms[i].name=='formCuotas'){
                                     if(document.forms[i].cuota.checked) { 
                                         cont++;  
                                         parametros+="&parametro"+cont+"="+document.forms[i].idPrestamo.value; 
                                     }
                                }
                           }
                           parametros+="&total="+cont;

                           if(cont>0){
                           
                               if(opcion=='MIGRAR'){
                                   if(confirm('Desea migrar a CXP DOC las cuotas seleccionadas'))
                                      document.location.href=(url + parametros); 
                               }
                               if(opcion=='ADD'){
                                   if(confirm('Desea seleccionar estas cuotas para ser migradas a  CXP DOC ')){
                                      document.location.href=(url + parametros); 
                                      parent.close(); 
                                   }
                               }
                           }
                           else
                               alert('Deber� seleccionar las cuotas que desea migrar a CXP DOC');
                    }
              }
              
              
              
              function selectedAll(  element ){                   
                   for(i=0;i<document.forms.length;i++) {
                        if(document.forms[i].name=='formCuotas'){
                            document.forms[i].cuota.checked = element.checked;
                        }                   
                   }
              }
             
              
      </script>

</head>
<body onresize="redimensionar()" onload = "redimensionar(); ">
     <center>
     
     
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listado de Cuotas"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
 

<%    Usuario  User         = (Usuario) session.getAttribute("Usuario");
      String   hoy          = Util.getFechaActual_String(4);
      List     listCuotas   = model.CuotasSvc.getListCuotas();
      String   evento       = model.CuotasSvc.getEventoBtn();
      String   title        = (evento.equals("ADD"))?"SELECCIONAR CUOTAS":"MIGRACI�N";
      String   urlAccion    = CONTROLLER + "?estado=Cuotas&accion=Manager";
      String   msj          = request.getParameter("msj");%>
    
  
      
      
       <!-- Listado de Cuotas -->
  <% if ( listCuotas.size()>0 ){%>
  
         <br>
         <table border="2" align="center" width='800' id='tablaListado'>
         <tr>
          <td>      
                    <table  width='100%' class='tablaInferior' >     

                           <tr>
                              <td colspan='7'>
                                  <table width='100%'  class="barratitulo" cellpadding='0' cellspacing='0'>
                                     <tr class="fila">
                                            <td align="left" width='60%' class="subtitulo1" nowrap> &nbsp LISTADO DE CUOTAS PARA TRANSFERIR A CXP DOC :<br>
                                       &nbsp <%= title %></td>
                                            <td align="left" width='*' ><img src="<%=BASEURL%>/images/titulo.gif"></td>
                                     </tr>
                                  </table>
                              </td>
                           </tr>  
                           
                           <tr class="fila">  
                                <td colspan='7' align='right'  style='font-size:10px' >
                                          APLICAR FECHA DE PAGO &nbsp&nbsp
                                          <input type='text' class="textbox" id='fpagoCuotas'    name='fpagoCuotas'   value='<%= hoy %>' size='12' readonly     title='Fecha de pago a Aplicar para cuotas'>                                                        
                                          <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(fpagoCuotas);return false;" HIDEFOCUS>
                                                 <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha">
                                          </a>
                                          <img src='<%=BASEURL%>/images/botones/iconos/modificar.gif'   style=" cursor:hand'"  title='Modificar fecha de pago'   name='i_update'  onclick="sendCuotas('<%= urlAccion %>&newfecha='+fpagoCuotas.value,'UPDATE_ALL')"    >

                                </td>
                           </tr>      
                           

                           <tr class="tblTitulo" style="font size:11">
                                  <TH width='4%' > <input type='checkbox' name='ALL' onclick='selectedAll(this)'></TH>
                                  <TH width='5%' >No               </TH>
                                  <TH width='13%'>PRESTAMO No      </TH>
                                  <TH width='9%' >CUOTA No         </TH> 
                                  <TH width='35%'>BENEFICIARIO     </TH>               
                                  <TH width='13%'>VALOR CUOTA      </TH> 
                                  <TH width='*'  >FECHA PAGO       </TH>               
                           </tr>

                             <% double vlrTotal = 0;
                                for(int i=0;i<listCuotas.size();i++){
                                     Amortizacion  amort =  (Amortizacion)  listCuotas.get(i);  
                                     vlrTotal           +=  amort.getTotalAPagar();
                                     String select       =  (amort.isSeleccionada())?"checked='checked'":"";%>     
                        
                                     <form  method='post' name='formCuotas' > 
                                          <tr class='<%= (i%2==0?"filagris":"filaazul") %>' id='row<%= amort.getIdObjeto() %>'    style="font size:11">
                                                  
                                                  <td class='bordereporte' align='center'>
                                                                  <input type='checkbox' name='cuota'  <%=select%>  >
                                                                  <input type='hidden'   name='idPrestamo' value='<%= amort.getIdObjeto() %>'>
                                                  </td>
                                                  <td class='bordereporte' align='center'><%= i+1%></td>
                                                  <td class='bordereporte' align='center'><%= amort.getPrestamo()         %>  </td>
                                                  <td class='bordereporte' align='center'><%= amort.getItem()             %>  </td>
                                                  <td class='bordereporte'               ><%= amort.getNameBeneficiario() %>  </td>               
                                                  <td class='bordereporte' align='right' ><%= UtilFinanzas.customFormat2(amort.getTotalAPagar()) %></td>              
                                                  <td class='bordereporte' align='right'  nowrap>

                                                       <%= amort.getFechaPago()        %> 
                                                       <input type='text' class="textbox" id='fpago<%= amort.getIdObjeto() %>'    name='fpago<%= amort.getIdObjeto() %>'   value='<%= amort.getFechaPago() %>' size='12' readonly     title='Fecha de pago'>                                                        
                                                       <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(fpago<%= amort.getIdObjeto() %>);return false;" HIDEFOCUS>
                                                                <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha">
                                                       </a>
                                                       
                                                       <img src='<%=BASEURL%>/images/botones/iconos/modificar.gif'   style=" cursor:hand'"  title='Modificar fecha de pago'   name='i_update'  onclick="sendCuotas('<%= urlAccion %>&idPrestamo=<%= amort.getIdObjeto() %>&newFecha='+ fpago<%= amort.getIdObjeto() %>.value,'UPDATE_FECHA')"    >
                                   
                                                  </td>
                                                  

                                          </tr>
                                       </form>                                      
                                      
                            <%}%> 
                  
                            <tr class="tblTitulo" style="font size:11">
                                  <td>&nbsp</td>
                                  <td colspan='4'><b> VALOR TOTAL <%= title %></td>
                                  <td  align='right'><b><%= UtilFinanzas.customFormat2(vlrTotal) %></td> 
                                  <td>&nbsp</td>
                           </tr>

                     </table>

              </td>
            </tr>
          </table>
                    
          <p>
           
              <img src='<%=BASEURL%>/images/botones/aceptar.gif'    style="cursor:hand"   title='Crear Prestamo....'     name='i_crear'   onclick="sendCuotas('<%= urlAccion %>','<%=evento%>')"    onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
              <img src='<%=BASEURL%>/images/botones/salir.gif'      style="cursor:hand"    title='Salir...'               name='i_salir'   onclick='parent.close();'                                 onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
          </p>
          
       
   <%}%>
   
   
   
   
   
   
   <!--MENSAJE-->
   
   <% if( ! msj.equals("") ) { %>          
          <table border="2" align="center">
               <tr>
                   <td>
                       <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                          <tr>
                            <td width="500" align="center" class="mensajes"> <%= msj %> </td>                            
                            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                            <td width="58">&nbsp; </td>
                          </tr>
                      </table>
                  </td>
                </tr>
            </table>   
            <% if ( listCuotas.size()==0 ){%>
                    <p>
                      <img src='<%=BASEURL%>/images/botones/salir.gif'      style='cursor:hand'   title='Salir...'               name='i_salir'   onclick='parent.close();'                            onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
                    </p>
            <% } %>
   
   <% } %>
   
   
    <iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
  
  
</div>



</body>
</html>
