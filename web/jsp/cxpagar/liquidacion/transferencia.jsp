<%--
/******************************************************************************
 * Nombre clase :                   transferencia.jsp                         *
 * Descripcion :                    transferencia para pronto pago            *
 * Autor :                          Mario Fontalvo                            *
 * Fecha Creado :                   13 de agosto de 2006                      *
 * Version :                        1.0                                       *
 * Copyright :                      Fintravalores S.A.                   *
 *****************************************************************************/
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*" %>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*, com.tsp.operation.model.beans.*,com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
  double valor     = Double.parseDouble(String.valueOf(request.getParameter("valor")!=null?request.getParameter("valor"):"0"));
  String nit       = request.getParameter("cpro");
  String nombre    = request.getParameter("npro");
  String voajp	   = request.getParameter("voajp");
  System.out.println("transf voajp =("+voajp+")");
  
  Usuario     usuario     = (Usuario)session.getAttribute("Usuario");
  String      user        = usuario.getLogin(); 
  String      proveedor   =  model.AnticiposPagosTercerosSvc.getProveedorUser(user);
  model.AnticiposPagosTercerosSvc.searchListaCuentasTercero(proveedor);
%>

<html>
<head>
<title>Transferencias Pronto Pago</title>
<link  href="../../../css/estilostsp.css" rel="stylesheet">
<link  href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet">
<script src="<%= BASEURL %>/js/transferencias.js"></script>
<script src="<%= BASEURL %>/js/boton.js"></script>
<script src="<%= BASEURL %>/js/reporte.js"></script>
<script > BASEURL = '<%= BASEURL %>';</script>
</head>
<body onresize="try{ redimensionar() } catch(e){}" onload = 'try{ redimensionar() } catch(e){}'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Transferencias Pronto Pago"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: -1px; top: 100px; overflow: scroll;">
<center>
<br>


<form action="<%= CONTROLLER %>?estado=Transferencias&accion=ProntoPago&Opcion=Transferir" method="post" name="forma" target="Transferencia">
<table width="503" border="2">
  <tr>
    <td width="491">
	
		<!-- cabecera de la seccion -->
		<table width="100%" align="center" class="tablaInferior" >
			<tr >
			<td class="subtitulo1"  width="50%">&nbsp;TRANSFERENCIAS </td>
			<td class="barratitulo" width="50%"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
			</tr>
		</table>		
		<!-- fin cabecera -->	
		
		
		<!-- seccion del filtro -->
		<table width="100%" class="tablaInferior">
			<tr >
				<td width="37%" class="fila">&nbsp;PROVEEDOR</td>
				<td width="63%" class="letra">&nbsp;<%="["+nit+"] "+nombre%><input type="hidden" name="nit" value="<%= nit      %>"></td>
			</tr>			
			<tr style="display:none " >
				<td width="37%" class="fila">&nbsp;TRANSFERIR A </td>
				<td width="63%" class="letra">&nbsp;<input type="hidden" name="btercero" value="" onKeyUp="  buscarDatosProveedor(event,'<%= CONTROLLER %>',this.value);" >&nbsp;(NIT)</td>
			</tr>
			<tr style="display:none " id="dter">
				<td width="37%" class="fila">&nbsp;TERCERO</td>
				<td width="63%" class="letra">&nbsp;<input type="text" name="tercero" value="" readonly="" style=" width:97%; border:0"></td>
			</tr>
			<tr style="display:none " id="dnom">
				<td width="37%" class="fila">&nbsp;NOMBRE</td>
				<td width="63%" class="letra">&nbsp;<input type="text" name="nombre" value="" readonly="" style=" width:97%; border:0"> </td>
			</tr>			
			<tr style="display:none " id="dban">
				<td width="37%" class="fila">&nbsp;BANCO</td>
				<td width="63%" class="letra">&nbsp;<input type="text" name="banco" value="" readonly="" style="width:97%; border:0"></td>
			</tr>
			<tr style="display:none " id="dsuc">
				<td width="37%" class="fila">&nbsp;SUCURSAL</td>
				<td width="63%" class="letra">&nbsp;<input type="text" name="sucursal" value="" readonly="" style="width:97%; border:0"></td>
			</tr>			
			<tr class="subtitulo1">
				<td  colspan="2">&nbsp;VALORES DE LA LIQUIDACIÓN</td>
			</tr>				
			<tr class="filaazul">
				<td width="37%" class="bordereporte">&nbsp;VALOR LIQUIDACIÓN</td>
				<td width="63%" align="right" class="bordereporte"><input type="text" name="vlrLiquidacion" value="<%= Utility.customFormat(valor) %>" readonly style="text-align:right; width:100%; border:0; background-color:#EEEEF2"></td>
			</tr>			
			<tr class="filagris">
				<td width="37%" class="bordereporte">&nbsp;VALOR A TRANSFERIR</td>
				<td width="63%" class="bordereporte" align="right"><input type="text" name="vlrReanticipo" value="<%= Utility.customFormat(valor) %>" style="text-align:right; width:100%; border:0" onKeyPress="soloDigitos(event,'decNO')" onBlur="this.value=formatear(this.value,2); actualizarSaldo();" onFocus="this.value=sinformato(this); this.select();"></td>
			</tr>	
			<tr class="filaazul">
				<td width="37%" class="bordereporte">&nbsp;SALDO</td>
				<td width="63%" class="bordereporte" align="right"><input type="text" name="saldo" value="<%= 0 %>" readonly style="text-align:right; width:100%; border:0;  background-color:#EEEEF2 "></td>
			</tr>						
			
			
			<tr class="subtitulo1">
				<td  colspan="2">&nbsp;BANCO TRANSACCION&nbsp;
				<img width='15' title='Asignar banco Transaccion' style="cursor:hand" src='/fintravalores/images/botones/iconos/lupa.gif' onclick="javascript: var x= window.open( '<%= CONTROLLER %>?estado=Anticipos&accion=PagosTerceros&evento=LISTABANCOS_TRANSACCION&nit=<%= nit %>&sendPP=ok&global=&anticipo=','Asignar','top=100,left=100, width=700, height=400, scrollbars=yes, status=yes, resizable=yes  ');  x.focus();">
				</td>
			</tr>		
			<tr >
				<td width="37%" class="fila">&nbsp;BANCO</td>
					<td width="63%" class="letra">&nbsp;<input type="text"   name="banco_T2"  value="" readonly="" style=" width:97%; border:0">
														<input type="hidden" name="banco_T"  value="">
                                    					<input type="hidden" name="B_trans"  value="">
					                                    <input type="hidden" name="C_trans"  value="">
														<input type="hidden" name="Tc_trans" value="">
				</td>
			</tr>			
			
			
			<!-- INFORMACION SOBRE A QUE PERSONA SE LO TRANSFERIRA EL DINERO -->
			<tr class="subtitulo1">
				<td  colspan="2">&nbsp;PAGO&nbsp;
				<img width='15' title='Asignar bancos' style="cursor:hand" src='/fintravalores/images/botones/iconos/lupa.gif' onclick="javascript: var x= window.open( '<%= CONTROLLER %>?estado=Anticipos&accion=PagosTerceros&evento=LISTABANCOS&nit=<%= nit %>&sendPP=ok&global=&anticipo=','Asignar','top=100,left=100, width=700, height=400, scrollbars=yes, status=yes, resizable=yes  ');  x.focus();">
				</td>
			</tr>		
			<tr >
				<td width="37%" class="fila">&nbsp;A NOMBRE DE</td>
				<td width="63%" class="letra">&nbsp;<input type="text"   name="t_nit_name"   value="" readonly="" style=" width:97%; border:0">
                                    <input type="hidden" name="t_nit"    value="">
                                    <input type="hidden" name="t_nombre" value="">
				</td>
			</tr>			
			<tr  >
				<td width="37%" class="fila">&nbsp;TIPO PAGO</td>
				<td width="63%" class="letra">&nbsp;<input type="text" name="t_tipo_name" value="" readonly="" style=" width:97%; border:0">
                                    <input type="hidden" name="t_tipo" value="" readonly="" style=" width:97%; border:0">
                                </td>
		</td>
				
</tr>
			<tr  >
				<td width="37%" class="fila">&nbsp;BANCO</td>
				<td width="63%" class="letra">&nbsp;<input type="text" name="t_banco" value="" readonly="" style=" width:97%; border:0"></td>
			</tr>
			<tr  >
				<td width="37%" class="fila">&nbsp;SUCURSAL</td>
				<td width="63%" class="letra">&nbsp;<input type="text" name="t_sucursal" value="" readonly="" style=" width:97%; border:0"> </td>
			</tr>			
			<tr  >
				<td width="37%" class="fila">&nbsp;CUENTA</td>
				<td width="63%" class="letra">&nbsp;<input type="text" name="t_cuenta" value="" readonly="" style="width:97%; border:0"></td>
			</tr>
	
		</table>		
		<!-- fin filtro -->
		<input type="hidden" name="voajp" id="voajp" value="<%=voajp%>">

				
	</td>
  </tr>
</table>

<br>
<img name="imgActualizar" src="<%=BASEURL%>/images/botones/aceptar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" validarTransferencia('<%=BASEURL%>');">
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
<br>
</form>

<% Proveedor p = model.proveedorService.getProveedor();
   if (p!=null){ %>
        <script>
                inicializarTercero('<%= p.getC_nit()  %>', '<%= p.getC_payment_name() %>', '<%= p.getC_branch_code() %>', '<%= p.getC_bank_account() %>');
        </script>
<% } %>

</center>
</div>
<%=datos[1]%>
</body>
</html>
