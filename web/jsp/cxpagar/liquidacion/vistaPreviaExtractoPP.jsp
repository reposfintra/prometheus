<!--
     - Author(s)       :      JULIO BARROS RUEDA
     - Date            :      25/11/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: listado de Extractos Pronto pago
 --%>

<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>
<%
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
	<title>Vista Previa de Pronto Pagos realizados </title>
    <link href="<%= BASEURL %>/css/estilostsp.css"     rel="stylesheet" type="text/css"> 
    <link href="/css/estilostsp.css" rel="stylesheet"  type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script>
		function validarSeleccion(){
		   var elementos = document.getElementsByName("idListOc");
		   for (var i=0;i < elementos.length; i++){
		      if (elementos[i].checked){
			    return true;			
                      }  
		   }		   
		   alert ('Seleccione por lo menos una planilla para poder iniciar el proceso de liquidacion.');
		   return false;
		}
    </script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listado de Pronto Pagos"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
   
<% Vector datox = model.ExtractoPPSvc.getExtractoPP();
	int op= Integer.parseInt(""+request.getParameter("op"));

   if (datox!=null && !datox.isEmpty() ){%>   
   
    <form method="post" name="formulario">
    
    <table width="990"  border="2" align="center">
     <tr>
         <td >           
    
                <table width="100%"  align="center">
                  <tr >
                     <td>
                          <table width='100%'  class="barratitulo">
                                <tr class="fila">
                                        <td align="left" width='46%' class="subtitulo1" nowrap> Pronto Pagos </td>
                                        <td width='54%' ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"align="left"><%=datos[0]%></td>
                                </tr>
                          </table>
                     </td>
                  </tr>
                  <tr class='fila'>
                     <td>
					 	
							<table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
								<tr class="tblTitulo">
										
									<th width="14%" > NIT      </th>				
									<th width="14%" > LIQUIDACION </th>		
									<th width="14%" > NOMBRE      </th>
									<th width="55%" nowrap > VALOR ABONADO  </th>
									<%if(op==1){ %>
										<th width="55%" nowrap > VALOR ABONADO A OC  </th>
									<%}%>
									<th width="12%" > FECHA </th>
									<th width="20%" > ESTADO   </th>
									<th width="20%" > BANCO  </th>
									<th width="40%" > FECHA APROBACION  </th>
								</tr>
								<% for (int i=0; i<datox.size(); i++) { 
								   	Extracto p = (Extracto) datox.get(i);
								%>
									<tr class="<%= (i%2==0?"filagris":"filaazul")%>">
										
										<td class="bordereporte" nowrap align="center"> <%= p.getNit()    %></td>
										<td class="bordereporte" nowrap align="center"> 
										<img width='14' title='Ver Liquidación Completa' style="cursor:hand" src='<%=BASEURL%>/images/botones/iconos/buscar.gif' 
												onclick="javascript: var x= window.open( '<%= CONTROLLER%>?estado=Extracto&accion=PP&opcion=ExtractoSecuencia&secuencia=<%= p.getSecuencia() %>',
												'Asignar','top=100,left=100, width=700, height=400, scrollbars=yes, status=yes, resizable=yes');  x.focus();">&nbsp  
												<%= p.getSecuencia()    %></td>
										<td class="bordereporte" nowrap align="center"> 
									        <img width='14' title='Consultar Pronto pago' style="cursor:hand" src='<%=BASEURL%>/images/botones/iconos/buscar.gif' 
												onclick="javascript: var x= window.open( '<%= CONTROLLER%>?estado=Extracto&accion=PP&opcion=ExtractoLiquidacion&nit=<%= p.getNit()%>&fecha=<%=p.getCreation_date()%>',
												'Asignar','top=100,left=100, width=700, height=400, scrollbars=yes, status=yes, resizable=yes');  x.focus();">&nbsp  
												<%= p.getNombre_trans()    %></td>
										<td class="bordereporte" nowrap align="center"> <%= Utility.customFormat(      model.ExtractoPPSvc.retornar_vlr_pp_e(p.getFecha())     ) %></td>
										
										
                						
										
										
										<%if(op==1){ 
											String documento=request.getParameter("OC");%>
											<td class="bordereporte" nowrap align="center"> <%= Utility.customFormat(   model.ExtractoPPSvc.retornar_vlr_pp_ed(documento,p.getFecha() )  ) %></td>
										<%}%>
										<%  
										String[] fech=  p.getFecha().split(" "); 
										String F1=fech[0];
										//System.out.println(" La fecha 1  "+F1);
										%>
										<td class="bordereporte" nowrap align="center"> <%= F1 %></td>
										<%if (p.getReg_status().equals("P") ){%>
												<td class="bordereporte" nowrap align="center">  Aprobado </td>
										  <%}else if (p.getReg_status().equals("") ){%>
										  		<td class="bordereporte" nowrap align="center">  Sin Aprobar </td>
										  <%}%>
										<td class="bordereporte" nowrap align="center"> <%= p.getBanco()   %></td>
										<%if( (""+p.getFecha_aprobacion() ).equals("0099-01-01 00:00:00") ){ %>
											<td class="bordereporte" nowrap align="center"> No definida  </td>
										<%}else {
											
										%>
											<td class="bordereporte" nowrap align="center"> <%= p.getFecha_aprobacion() %></td>
										<%}%>
									</tr>								
								<% } %>
								
							</table>						
					 
					 </td>
                  </tr>				  
		  
                </table>
                
        </td>
      </tr>
      </table>  
      <br>
        <center>
	
        <img src='<%=BASEURL%>/images/botones/regresar.gif'   style=" cursor:hand"  title='Aceptar...'     name='i_crear'       onclick="document.location = '<%= BASEURL %>/jsp/cxpagar/liquidacion/ConsultaExtractos.jsp' "     onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
        <img src='<%=BASEURL%>/images/botones/salir.gif'      style='cursor:hand'    title='Salir...'      name='i_salir'       onClick='parent.close();'           onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>        </center>
	     <br>
		 <%if (request.getParameter("msg") != null ){%>      
            <table border="2" align="center">
              <tr>
                <td>
					<table width="100%"  border="0" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                  		<tr>
                    		<td width="500" align="center" class="mensajes"><%= request.getParameter("msg") %></td>
                    		<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    		<td width="58">&nbsp; </td>
                  		</tr>
                	</table>
				</td>
              </tr>
           </table>               
              <%}//fin de los mensajes%>   
	 <input type="hidden" name="Opcion" value="ConfirmacionOcs"></form>
<% } //fin if de revisar datos%>	

	
</div>
<%=datos[1]%>
</body>
</html>
