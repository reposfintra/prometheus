<!--
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      07/10/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: formulario donde se muestra el resultado de la liquidacion de una planilla especifica
 --%>

<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>
<%@ page import    ="com.tsp.operation.model.services.*"%>
<%@ page import    ="com.tsp.operation.model.Model"%>


<%String base = (String) request.getParameter("base"); %>
<html>
    <head>

        <title>.: Liquidacion OC</title>
  
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
		<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/transferencias.js"></script> 
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    

    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Liquidacion de OCs"/>
        </div>

        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">  
            <center>
  <script>
		   function cambiarLeyenda (tipo,neto){
			  switch (parseInt(tipo)){
				 case 0: 
				 	valor.value = neto - 6000;
					
					break;
				 case 1: 
				 	//if(tipo3.checked ){
				 		valor.value = neto - 2000;
					//}else{valor.value = neto - 3500;}
					break;
			  }
		   }				
		   
		   function formatear(objeto){
			   var n     =   replaceALL(objeto.value,',','');
			   objeto.value=formatearNumero(n);
		   }
  </script>
  <% 
     
     Usuario usuario            = (Usuario) session.getAttribute("Usuario");
     Vector  liquidaciones      = model.LiqOCSvc.getLiquidaciones();
     Vector  ajustesPropietario = model.LiqOCSvc.getDescuentosPropietario();
     Vector  ajustesPlaca       = model.LiqOCSvc.getDescuentosPlaca();
     String  msjEstado          =  "";
	 double totalPlanillas      = 0;
	 double totalDescuentoPro   = 0;
	 double totalDescuentoProS  = 0;
	 double totalImpuestosPro   = 0;
	 double totalDescuentoPla   = 0;
	 double totalImpuestosPla   = 0;
     int    planillasConEerrores = 0;
	 String ProveedorP ="";
	 double TemporalOPneg = 0;
	 
	 
	 if (liquidaciones!=null && !liquidaciones.isEmpty()){ 
	   OP opg = null;
	   for (int contLiquidaciones = 0; contLiquidaciones<liquidaciones.size(); contLiquidaciones++){
	      Liquidacion liquidacion = (Liquidacion) liquidaciones.get(contLiquidaciones);
              String      oc          =  model.LiqOCSvc.getOC();
              List        facturas    =  liquidacion.getFacturas();
              Planilla    pla         =  liquidacion.getPlanilla();  
			  
              opg = (OP) facturas.get(0);
			  ProveedorP = opg.getProveedor();
              if ( (facturas != null ) && (facturas.size() != 0 )){ 
                 if (contLiquidaciones==0) {%>
		    <table border="1" width="630">
			<tr><td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<table width='100%'  class="barratitulo">
							<tr class="fila">
								<td align="left" width='46%' class="subtitulo1" nowrap> DATOS DEL PROVEEDOR </td>
								<td width='54%' ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"align="left"></td>
							</tr>
					  </table>							
					</td>
				</tr>	
				<tr>
				    <td>
					<table width="100%"  border="0" class="tablaInferior">
						<tr class="fila">
							<td width="120">PROPIETARIO</td>
							<td width="139"><%= opg.getProveedor()%></td>
							<td width="105">NOMBRE</td>
							<td width="266"><%= opg.getNameProveedor() %></td>
						</tr>
						<tr class="fila">
							<td>BANCO</td>
							<td><%=opg.getBanco()      %></td>
							<td>SUCURSAL</td>
							<td><%=opg.getSucursal()   %></td>
						</tr>
						<tr class="letra">
						<td colspan="4">&nbsp;</td>
					</tr>		
					</table>		
				</td></tr>
				</table>
				</td>
			  </tr>
			  </table>
				<br>				
			<%}%>
			

			<table width="630" border="2" cellspacing="0" cellpadding="0">
				<tr>
					<td>
					
							
							
			<% for(int i=0;i<facturas.size();i++){
				OP    op  = (OP) facturas.get(i); 
				 
  			        msjEstado = op.getComentario(); 
			%>
							<table width="100%"  border="0" class="tablaInferior">
								<tr>
								<td colspan="4">
									<table width="100%"  border="0" cellpadding="1" cellspacing="0" class="tablaInferior">
                                                                        
									        
										<tr class="letra">
										<td width="16%" class="letraresaltada">OC</td>
										<td width="23%" ><%= pla.getNumpla()%></td>
										<td width="13%" class="letraresaltada">PLACA</td>
										<td width="20%" ><%= pla.getPlaveh()  %></td>
										<td width="10%" align="right" class="letraresaltada"><%= ( !op.getBase().equals (" ") )?op.getTipocant():"" %></td>
										<td width="5%">&nbsp;</td>
										<%   if(! op.isTieneOp() ){   %>
											<td width="13%" ><%= ( !op.getBase().equals (" ") )?op.getCantiCump():"" %></td>
										<%} else{%>
											<td width="13%" ></td>
										<%}%>
										</tr> 
										<tr class="letra">
										<td class="letraresaltada">CLIENTE</td>
										<td colspan="6" ><%= pla.getClientes()  %></td>
										</tr>
										<tr class="letra">
										<td class="letraresaltada">RUTA</td>
										<td colspan="4" ><%= pla.getNomori() %> - <%= pla.getNomdest()%></td>
										<td colspan='2'><%=(!pla.getReg_status().equals("C")&&!pla.getReg_status().equals("A"))?"NO CUMPLIDA":(pla.getReg_status().equals("C"))?"CUMPLIDA":"" %></td>
										</tr >
										<tr class="letra" >
										<td class="letraresaltada">F. DESCPACHO </td>
										<td colspan="6" ><%= pla.getFecdsp()%></td>
										</tr>
										<tr class="letra">
										<td class="letraresaltada">REMESA</td>
										<td colspan="6" ><%= pla.getNumrem()  %></td>
										</tr>
										<tr class="letra">
										<td colspan="7"><HR></td>
										</tr>
										
										<% 
										   if ( msjEstado.equals("") ){
												totalPlanillas += op.getVlrNeto();%>
												<tr class="letra">
												<td class="letraresaltada">FACTURA</td>												
												<td colspan="6" ><%=op.getDocumento()  %></td>
												</tr>
												<tr>
												<td colspan="7">
													<table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
														<tr align="center" class="tblTitulo">
															<td >ITEM</td>
															<td >DESCRIPCION</td>
															<td >VALOR</td>
															<td >RETEFUENTE</td>
															<td >RETEICA</td>
														</tr>
																  <%
													  			  
																  double tv = 0;
																  double trfte = 0;
																  double trica = 0;
																  List listItem = op.getItem();
																  if ( listItem!=null ) {
																		for(int j=0;j<listItem.size();j++){
																				OPItems  item = (OPItems)listItem.get(j);
																																								
																				if(item.isVisible() || op.isTieneOp() ){//Ivan Gomez 2006-03-24 %>
																				<tr class="<%=(j % 2 == 0 )?"filagris":"filaazul"%>">
																					<td class="bordereporte"><%= item.getItem() %></td>
																					<td class="bordereporte"><%= item.getDescconcepto() %></td>
																					<td align="right" class="bordereporte"><%= Utility.customFormat(item.getVlr() ) %></td>
																					<td align="right" class="bordereporte"><%= Utility.customFormat(item.getVlrReteFuente() ) %></td>
																					<td align="right" class="bordereporte"><%= Utility.customFormat(item.getVlrReteIca() ) %></td>
																					</tr>
																  <%				tv += item.getVlr();
																					trfte += item.getVlrReteFuente();
																					trica += item.getVlrReteIca();
																		  	}//fin IF(item.getVisible()) ivan Gomez 20006-03-24      
																		}//fin for
																   }//fin if%>
														<tr class="fila">
														<td colspan="2" class="bordereporte">TOTAL FACTURAS </td>
														<td align="right" class="filaamarilla bordereporte"><%= Utility.customFormat(tv) %></td>
														<td align="right" class="filaroja bordereporte"><%= Utility.customFormat(trfte) %></td>
														<td align="right" class="filaroja bordereporte"><%= Utility.customFormat(trica) %></td>
														</tr>
														<tr class="subtitulo">
															<td colspan="2" class="bordereporte">TOTAL A PAGAR PLANILLA</td>
															<%
															if(op.getVlrNeto()< 0){
															TemporalOPneg+=op.getVlrNeto();
															}
															%>
															<td colspan="3" align="right" class="bordereporte">$ <%= Utility.customFormat(op.getVlrNeto() ) %></td>
														</tr>
													</table>
												</td>
												</tr>
										  <%}else{planillasConEerrores++; %>
										  
												<tr class="letra">
												   <td class="letraresaltada"><font color='red'>ERROR</td>
												   <td colspan="6" ><%= msjEstado %></td>
												</tr>
										  
										  <%}%>                                
										
									</table>
								</td>
								</tr>
							</table>
				  </td>
			  </tr>
			  </table>
			  <a href='<%= CONTROLLER %>?estado=Liquidar&accion=OC&Opcion=RetirarOc&parametro=<%= op.getOc() %>' class='Simulacion_Hiper'>Retirar la planilla <%= op.getOc() %> de la liquidación</a><br>

		
		
				 <br>
				 <br>
				 <%} 
		   } else{%>      
		
					<table border="2" align="center">
						<tr>
							<td>
							<table width="100%"  border="0" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
							<tr>
							<td width="500" align="center" class="mensajes">No se encontraron facturas  para la planilla : <%= oc %></td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58">&nbsp; </td>
						</tr>
							</table>
						</td>
						</tr>
					</table> 
					<br>
					  
		  <%}%>   
		  
		<% }// end for de liquidaciones 
		   if (ajustesPlaca!=null && !ajustesPlaca.isEmpty()){%>
		   <BR><BR>
 			<table border="1" width="630">
				<tr >
					<td>
                          <table width='100%'  class="barratitulo">
                                <tr class="fila">
                                        <td align="left" width='46%' class="subtitulo1" nowrap> FACTURAS PLACAS </td>
                                        <td width='54%' ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"align="left"></td>
                                </tr>
                          </table>							
						<table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
						<tr class="tblTitulo">
							<th>PLACA</th>
							<th>FACTURA</th>
							<th>ITEM</th>							
							<th>DESCRIPCION</th>
							<th>VALOR</th>
							<th>IMPUESTOS</th>
						</tr>
						<%  for (int contDescuentos = 0; contDescuentos<ajustesPlaca.size(); contDescuentos++){
								  DescuentoMIMS descuento = (DescuentoMIMS) ajustesPlaca.get(contDescuentos);		
								  totalDescuentoPla += descuento.getValor_item();
								  totalImpuestosPla += descuento.getIva()  -descuento.getRiva() -descuento.getRica() - descuento.getRetefuente();
					    %>						
						<tr class="<%= (contDescuentos%2==0?"filaazul":"filagris") %>">
							<td class="bordereporte" align="center" nowrap><%= descuento.getPlaca() %></td>						
							<td class="bordereporte" align="center" nowrap><%= descuento.getFactura_ext() %></td>
							<td class="bordereporte" align="center" nowrap><%= descuento.getItem()        %></td>
							<td class="bordereporte" align="left"   nowrap><%= descuento.getDescripcion() %></td>
							<td class="bordereporte" align="right"  nowrap><%= Utility.customFormat(descuento.getValor_item())  %></td>
							<td class="bordereporte" align="right"  nowrap><%= Utility.customFormat(descuento.getIva()  -descuento.getRiva() -descuento.getRica() - descuento.getRetefuente())  %></td>
						</tr>							
						<%    } // end for de descuentos %>					
						<tr  class="subtitulo">
							<td class="bordereporte" colspan="4" nowrap>TOTALES FACTURAS PLACA </td>
							<td class="bordereporte" align="right" nowrap ><%= Utility.customFormat(totalDescuentoPla)  %></td>
							<td class="bordereporte" align="right" nowrap ><%= Utility.customFormat(totalImpuestosPla )  %></td>
						</tr>							
						</table>				
					</td>
				</tr>
			</table>				     
  		 <%  } // end if de descuentos propietario	
		   if (ajustesPropietario!=null && !ajustesPropietario.isEmpty()){%>
		   <BR><BR>
 			<table border="1" width="630">
				<tr >
					<td>
 						<table width='100%'  class="barratitulo">
                                <tr class="fila">
                                        <td align="left" width='46%' class="subtitulo1" nowrap> FACTURAS DE PROPIETARIO </td>
                                        <td width='54%' ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"align="left"></td>
                                </tr>
                          </table>						
						<table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
						<tr class="tblTitulo">
							<th>Factura</th>
							<th>Item</th>							
							<th>Descripcion</th>
							<th>Valor</th>
							<th>Impuestos</th>
							<th>Saldo</th>
						</tr>
						<%  for (int contDescuentos = 0; contDescuentos<ajustesPropietario.size(); contDescuentos++){
								  DescuentoMIMS descuento = (DescuentoMIMS) ajustesPropietario.get(contDescuentos);		
								  totalDescuentoPro += descuento.getValor_item();
								  totalImpuestosPro += descuento.getIva()  -descuento.getRiva() -descuento.getRica() - descuento.getRetefuente();
								  totalDescuentoProS  += descuento.getValor() ;
					    %>						
						<tr class="<%= (contDescuentos%2==0?"filaazul":"filagris") %>">
							<%-- <td class="bordereporte" align="center" nowrap><= descuento.getFactura_ext() %></td> --%>
							<td class="bordereporte" align="center" nowrap><%= descuento.getFactura() %></td>
							<td class="bordereporte" align="center" nowrap><%= descuento.getItem()        %></td>
							<td class="bordereporte" align="left"   nowrap><%= descuento.getDescripcion() %></td>
							<td class="bordereporte" align="right"  nowrap><%= Utility.customFormat(descuento.getValor_item())  %></td>
							<td class="bordereporte" align="right"  nowrap><%= Utility.customFormat(descuento.getIva()  + descuento.getRiva() + descuento.getRica() + descuento.getRetefuente())  %></td>
							<td class="bordereporte" align="right"  nowrap><%= Utility.customFormat( descuento.getValor() )  %></td>
						</tr>							
						<%    } // end for de descuentos %>					
						<tr  class="subtitulo">
							<td class="bordereporte" colspan="3" nowrap>TOTAL FACTURAS PROPIETARIO</td>
							<td class="bordereporte" align="right" nowrap ><%= Utility.customFormat(totalDescuentoPro)  %></td>
							<td class="bordereporte" align="right" nowrap ><%= Utility.customFormat(totalImpuestosPro )  %></td>
							<td class="bordereporte" align="right" nowrap ><%= Utility.customFormat(totalDescuentoProS  )  %></td>  
						</tr>							
						</table>				
					</td>
				</tr>
			</table>				     
		     <BR>
  		 <%  } // end if de descuentos %>	
  		 <br>
  		 <br>	 
		 
		
 		 <% if (ajustesPlaca!=null && !ajustesPlaca.isEmpty()){ %>
 			<table border="1" width="400">
				<tr>
					<td>
						 <table width='100%'  class="barratitulo" >
                                <tr class="fila">
                                        <td align="left" width='46%' class="subtitulo1" nowrap> RESUMEN DE PLACAS </td>
                                        <td width='54%' ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                                </tr>
                          </table>							
						 <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
						 <tr class="tblTitulo">
							<th>PLACA</th>
							<th>TOTAL</th>
						 </tr>
						 <%   String ant = null;
							  double total_a_mostrar = 0;
							  int  i = 0;
							  for (int contDescuentos = 0; contDescuentos<ajustesPlaca.size(); contDescuentos++){
								  DescuentoMIMS descuento = (DescuentoMIMS) ajustesPlaca.get(contDescuentos);					  				  
								  double subtotal = descuento.getValor_item() + descuento.getIva()- descuento.getRiva() -descuento.getRica() - descuento.getRetefuente();		 				  
								  total_a_mostrar +=  subtotal;
							  
								  if( (ant!=null && !ant.equals(descuento.getPlaca()))) {	  
									  i++;
								  %>
									<tr class="<%= (i%2==0? "filaazul":"filagris") %>" >
										<td align="center" class="bordereporte"><%= ant %></td>
										<td align="right" class="bordereporte"><%= Utility.customFormat(total_a_mostrar) %></td>
									</tr>								  
								  <%
								  	total_a_mostrar = 0;
								  }				 
								  ant = descuento.getPlaca();
							  }
							  %>
								<tr class="<%= ((i+1)%2==0? "filaazul":"filagris") %>" >
									<td align="center" class="bordereporte"><%= ant %></td>
									<td align="right" class="bordereporte"><%= Utility.customFormat(total_a_mostrar) %></td>
								</tr>								  						  
							  <%
							  
							  
						 %>
						 </table>
					</td>
				</tr>
			</table>
		 
		 
		 <% } %>		
		
		
		
		 
		  <br>
                 <br>
 			<table border="1" width="500">
			  	<tr>
					<td>			
						<table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
						<tr>
							<td colspan="2" class="bordereporte">
								<table width='100%'  class="barratitulo">
									<tr class="fila">
										<td align="left" width='46%' class="subtitulo1" nowrap> DATOS DE GENERALES DE LIQUIDACION </td>
										<td width='54%' ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"align="left"></td>
									</tr>
							  </table>								
							</td>
						</tr>
						<tr class="filaazul">
							<td class="bordereporte"> TOTAL PLANILLAS </td>
							<td class="bordereporte" align="right"><%= Utility.customFormat(totalPlanillas) %></td>
						</tr>
						<tr class="filagris">
							<td class="bordereporte"> &nbsp;&nbsp;&nbsp;&nbsp;FACTURAS PLACAS </td>
							<td class="bordereporte" align="right"><%= Utility.customFormat(totalDescuentoPla + totalImpuestosPla) %></td>
						</tr>		
						<tr class="filagris">
							<td class="bordereporte"> &nbsp;&nbsp;&nbsp;&nbsp;FACTURAS PROPIETARIO </td>
							<td class="bordereporte" align="right"><%= Utility.customFormat( totalDescuentoProS ) %></td>
						</tr>		
						<tr class="filaazul">
							<td class="bordereporte"> TOTAL FACTURAS </td>
							<td class="bordereporte" align="right"><%= Utility.customFormat(totalDescuentoPla + totalImpuestosPla + totalDescuentoProS ) %></td>
						</tr>						
						<tr class="subtitulo">
							<td class="bordereporte"> TOTAL LIQUIDACION </td>
							<td class="bordereporte" align="right"><%= Utility.customFormat(totalPlanillas+ totalDescuentoPla + totalImpuestosPla + totalDescuentoProS ) %></td>
						</tr>		
						</table>
					</td>
				</tr>
			</table>
			
			
			 <br>
                 <br>
				 <% double neto = 0;
                                 if (base==null) {%>
 			<table border="1" width="500">
			  	<tr>
					<td>
						<table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
						<tr>
							<td colspan="2" class="bordereporte">
								<table width='100%'  class="barratitulo">
									<tr class="fila">
										<td align="left" width='46%' class="subtitulo1" nowrap> VALOR NETO DE CONSIGNACION </td>
										<td width='54%' ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"align="left"></td>
									</tr>
							  </table>
							</td>
						</tr>

						<% 	    double porcentaje = model.AnticiposPagosTercerosSvc.descuento( ProveedorP );
								double ElValor = (totalPlanillas+ totalDescuentoPla + totalImpuestosPla + totalDescuentoProS);
								double descuento       = ( ElValor * porcentaje )/100;
                                neto            =  ElValor - descuento ;
						%>
						<tr class='fila'>
							 <td colspan='2' > 
								<span style="width:25%;">
									  &nbsp;<input type='radio' value='0' name='tipo'  onClick="cambiarLeyenda(this.value,'<%=neto%>');if(valor.value!='')formatear(valor); tipo2.checked=false;" checked  >
									  Efectivo 						
								</span>
								<span style="width:25%;">
 									  &nbsp;<input type='radio' value='1' name='tipo2' onClick="cambiarLeyenda(this.value,'<%=neto%>');if(valor.value!='')formatear(valor); tipo.checked=false;" >
									 Cuenta						
								</span>	
								<!-- <span style="width:25%;" >
 									  &nbsp;<input type= 'checkbox' name='tipo3'  onClick="cambiarLeyenda(tipo.checked == true ? '0' : '1' ,'<=neto%>');if(valor.value!='')formatear(valor);" >
									 Mismo Banco						
								</span>	 --> 					
							</td>					 
						  </tr>
						<tr class="subtitulo">
							<td class="bordereporte"> TOTAL CONSIGNACION </td>
							<td class="bordereporte" align="right"> <input name="valor" type="text" class="subtitulo" style="border:0;text-align:right" readonly > </td>
						</tr>		
						</table>
					</td>
				</tr>
			</table>
			<%}%>
			            <script>
						   cambiarLeyenda('0','<%=neto%>');
						   if(valor.value!='')formatear(valor);
						</script>
			<br>
                    <% if (base==null) {%>	
                            <% if ((totalPlanillas+ totalDescuentoPla + totalImpuestosPla + totalDescuentoProS )>0) { %>
                                 <a href="javascript: void(0);" class="Simulacion_Hiper" onClick= " transferir('<%= CONTROLLER %>','<%= totalPlanillas+ totalDescuentoPla + totalImpuestosPla + totalDescuentoProS  %>','<%= opg.getProveedor() %>','<%= opg.getNameProveedor() %>','<%= totalDescuentoProS %>');javascript:window.close(); "  title="Pronto Pago" >Pronto Pago</a>
                            <% } else { %>
                                 <span class='informacion'> No se puede realizar pronto pago, por este valor de la liquidacion</span>
                            <% } %>
                            <br>
                            <br>
                            <!--Julio Barros-->
                            <% if (planillasConEerrores!=0) {%>
                                       <a href='<%= CONTROLLER %>?estado=Liquidar&accion=OC&Opcion=RetirarOcAll' class='Simulacion_Hiper'>Retirar todas la planillas malas de la liquidación</a><br>
                            <% } %>	 
			<br>
                    <% } // end if de base%>
			<br>
            <% } // end if de liquidaciones %>   
      			
            
            <img src='<%=BASEURL%>/images/botones/imprimir.gif'        style='cursor:hand'    title='Imprimir....'  name='i_imprimir'    onclick="window.open('<%=BASEURL%>/jsp/cxpagar/liquidacion/impLiquidacionOP.jsp?base=<%= base %>','','menubar=yes, resizable=yes, scrollbars=yes')"     onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
            <% if (base==null) { %>
            <img src='<%=BASEURL%>/images/botones/regresar.gif'        style='cursor:hand'    title='Regresar....'  name='i_regresar'    onclick="window.location.href='<%=BASEURL%>/jsp/cxpagar/liquidacion/liquidacion.jsp'" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
            <% } %>            
            <img src='<%=BASEURL%>/images/botones/salir.gif'           style='cursor:hand'    title='Salir...'      name='i_salir'       onclick='parent.close();'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
            </center>	 
        </div>
    </body>
</html>
<script>
   contadorPlanillasMalas = <%= planillasConEerrores %>;
   
</script>

<!--
     Entregado a jbarros 21 Febrero 2007
  -->
