<!--
     - Author(s)       :      JULIO BARROS RUEDA
     - Date            :      26 - 11 - 2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: formulario donde se muestra el resultado de la liquidacion de una planilla especifica sacada de un extracto
 --%>

<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>
<%@ page import    ="com.tsp.operation.model.DAOS.*"%>

<html>
    <head>

        <title>.: Extracto de liquidacion OC</title>
  
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/transferencias.js"></script> 
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    

    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Liquidacion de OCs"/>
        </div>

        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">  
            <center>
   
  <% 
     Usuario usuario            = (Usuario) session.getAttribute("Usuario");
	 
     Vector  liquidaciones      = model.ExtractoPPSvc.getExtractoDetallePP1();
     Vector  ajustesPropietario = model.ExtractoPPSvc.getExtractoDetallePP2();
     Vector  ajustesPlaca       = model.ExtractoPPSvc.getExtractoDetallePP0();
	 
	 
     String  msjEstado          =  "";
	 double totalPlanillas      = 0;
	 double totalDescuentoPro   = 0;
	 double totalImpuestosPro   = 0;
	 double totalDescuentoPla   = 0;
	 double totalImpuestosPla   = 0;
	 int   planillasConEerrores = 0;
	 int   cent                 = 0; 
	 String    Dcoument         = ""; 
	 float     valorF           = 0;
	 float totalsaldo			= 0;
	 int   secu                 = 0;
	 String ProveedorP          = "";
         
         if (liquidaciones!=null && !liquidaciones.isEmpty()){ 
             ExtractoDetalle liquidacion1 = (ExtractoDetalle) liquidaciones.get(0);
             Dcoument = liquidacion1.getDocumento();
         }else if(ajustesPlaca!=null && !ajustesPlaca.isEmpty()){
             ExtractoDetalle ajustesPlaca1 = (ExtractoDetalle) liquidaciones.get(0);
             Dcoument = ajustesPlaca1.getDocumento();
         }else if(ajustesPropietario!=null && !ajustesPropietario.isEmpty()){
             ExtractoDetalle ajustesPropietario1 = (ExtractoDetalle) liquidaciones.get(0);
             Dcoument = ajustesPropietario1.getDocumento();
         }
         
         
	 
	 
	 if (liquidaciones!=null && !liquidaciones.isEmpty()){ 
	   for (int i = 0; i<liquidaciones.size(); i++){
	      ExtractoDetalle liquidacion = (ExtractoDetalle) liquidaciones.get(i);
	      cent=0;
		  
		  model.ExtractoPPSvc.buscarExtractoPP(liquidacion.getNit(),liquidacion.getFecha());
		  Extracto     Ext            = model.ExtractoPPSvc.getExtractoPPU();
                 if (i==0) {
				 	valorF = model.ExtractoPPSvc.retornar_vlr_pp_e(Ext.getFecha()) ;
					secu=liquidacion.getSecuencia();
					ProveedorP = Ext.getNit();
					%>
					
			<table border="1" width="500">
			  	<tr>
					<td>			
						<table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
						<tr class="subtitulo">
							<td class="bordereporte"> LIQUIDACION </td>
							<td class="bordereporte" align="right"><%= liquidacion.getSecuencia() %></td>
						</tr>		
						</table>
					</td>
				</tr>
			</table>
			<br>
			<br>
					
					
					
		    <table border="1" width="630">
			<tr><td>
				<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td>
						<table width='100%'  class="barratitulo">
							<tr class="fila">
								<td align="left" width='46%' class="subtitulo1" nowrap> DATOS DEL PROVEEDOR </td>
								<td width='54%' ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"align="left"></td>
							</tr>
					  </table>							
					</td>
				</tr>	
				<tr>
				    <td>
					<table width="100%"  border="0" class="tablaInferior">
						<tr class="fila">
							<td width="120">PROPIETARIO</td>
							<td width="139"><%= Ext.getNit()%></td>
							<td width="105">NOMBRE</td>
							<td width="266"><%= Ext.getNombre_trans() %></td>
						</tr>
						<tr class="fila">
							<td>BANCO</td>
							<td><%=Ext.getBanco()      %></td>
							<td>SUCURSAL</td>
							<td><%=Ext.getSucursal()   %></td>
						</tr>
						<tr class="letra">
						<td colspan="4">&nbsp;</td>
					</tr>		
					</table>		
				</td></tr>
				</table>
				</td>
			  </tr>
			  </table>
				<br>				
			<%}//final de contliquidaciones%>
			

			<table width="630" border="2" cellspacing="0" cellpadding="0">
				<tr>
					<td>
							<table width="100%"  border="0" class="tablaInferior">
								<tr>
								<td colspan="4">
									<table width="100%"  border="0" cellpadding="1" cellspacing="0" class="tablaInferior">								        
										<tr class="letra">
										<td width="16%" class="letraresaltada">OC</td>
										<td width="23%" ><%= liquidacion.getDocumento()%></td>
										<td width="13%" class="letraresaltada">PLACA</td>
										<td width="20%" ><%= liquidacion.getPlaveh()  %></td>
										<td width="10%" align="right" class="letraresaltada"><%= ( !liquidacion.getUnit_vlr().equals (" ") )?liquidacion.getUnit_vlr():"" %></td>
										<td width="5%">&nbsp;</td>
										<td width="13%" ><%= ( !liquidacion.getPesoreal().equals (" ") )?  Utility.customFormat( Float.parseFloat( liquidacion.getPesoreal() ) ) :"" %></td>
										</tr> 
										<tr class="letra">
										<td class="letraresaltada">CLIENTE</td>
										<td colspan="6" ><%= liquidacion.getNomc()  %></td>
										</tr>
										<tr class="letra">
										<td class="letraresaltada">RUTA</td>
										<td colspan="4" ><%= liquidacion.getOripla() %> - <%= liquidacion.getDespla()%></td>
										<td colspan='2'><%=(!liquidacion.getStapla().equals("C")&&!liquidacion.getStapla().equals("A"))?"NO CUMPLIDA":(liquidacion.getStapla().equals("C"))?"CUMPLIDA":"" %></td>
										</tr >
										<tr class="letra" >
										<td class="letraresaltada">F. DESCPACHO </td>
										<td colspan="6" ><%= liquidacion.getFecdsp()%></td>
										</tr>
										<tr class="letra">
										<td class="letraresaltada">REMESA</td>
										<td colspan="2" ><%= liquidacion.getNumrem()  %></td>
										<td class="letraresaltada">REMISION</td>
										<td colspan="3" ><%= liquidacion.getRemision()  %></td>
										</tr>
										<tr class="letra">
										<td colspan="7"><HR></td>
										</tr>
										
										
												<tr class="letra">
												<td class="letraresaltada">FACTURA</td>
												<td colspan="6" ><%=liquidacion.getDocumento()  %></td>
												</tr>
												<tr>
												<td colspan="7">
													<table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
														<tr align="center" class="tblTitulo">
															<td >ITEM</td>
															<td >DESCRIPCION</td>
															<td >VALOR</td>
															<td >RETEFUENTE</td>
															<td >RETEICA</td>
														</tr>
                                                                                          <%double tv = 0;
                                                                                          double trfte = 0;
                                                                                          double trica = 0;
                                                                                          %>
											<% while ( cent == 0) {
                                                                                            String Comp="";
                                                                                            liquidacion = (ExtractoDetalle) liquidaciones.get(i);%>
														<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
															<td class="bordereporte"><%= liquidacion.getConcepto() %></td>
															<td class="bordereporte"><%= liquidacion.getDescripcion() %></td>
															<td align="right" class="bordereporte"><%= Utility.customFormat(liquidacion.getVlr() ) %></td>
															<td align="right" class="bordereporte"><%= Utility.customFormat(liquidacion.getRetefuente() ) %></td>
															<td align="right" class="bordereporte"><%= Utility.customFormat(liquidacion.getReteica() ) %></td>
														</tr>
                                                                                         <%	tv    += liquidacion.getVlr() ;
                                                                                                trfte += liquidacion.getRetefuente();
                                                                                                trica += liquidacion.getReteica(); 
                                                                                                if ( (i+1) < liquidaciones.size()){
                                                                                                    Comp = ((ExtractoDetalle) liquidaciones.get(i+1)).getDocumento();  
                                                                                                }else{
                                                                                                    Comp="";
                                                                                                    }%>
                                                                                                                                                                 
                                                                                       <%   if ( liquidacion.getDocumento().equals( Comp) ) {%>                                                                                  
                                                                                                          <%i++;
                                                                                            }else{//fin if
                                                                                                   cent++;
                                                                                            %>
                                                                                                     <tr class="fila">
														<td colspan="2" class="bordereporte">TOTAL FACTURAS </td>
														<td align="right" class="filaamarilla bordereporte"><%= Utility.customFormat(tv) %></td>
														<td align="right" class="filaroja bordereporte"><%= Utility.customFormat(trfte) %></td>
														<td align="right" class="filaroja bordereporte"><%= Utility.customFormat(trica) %></td>
														</tr>
														<tr class="subtitulo">
															<td colspan="2" class="bordereporte">TOTAL A PAGAR PLANILLA</td>
															<td colspan="3" align="right" class="bordereporte">$ <%= Utility.customFormat(tv+trfte+trica) %></td>
															<%   totalPlanillas += tv+trfte+trica; %>
														</tr>
													</table>
												</td>
												</tr>     
                                                                                                    <%}//fin else
                                                                                   }//fin while del op
                                                                                   %>		
										
									</table>
								</td>
								</tr>
							</table>
				  </td>
			  </tr>
			  </table>
		
		
				 <br>
				 <br>
				 <%}//termina la lista de liquidaciones%>
		  
		  
		<% }// end if de liquidaciones 
                    else{%>      
		
					<table border="2" align="center">
						<tr>
							<td>
							<table width="100%"  border="0" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
							<tr>
							<td width="500" align="center" class="mensajes">No se encontraron facturas  para la planilla : <%= Dcoument %></td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58">&nbsp; </td>
						</tr>
							</table>
						</td>
						</tr>
					</table> 
					<br>
					  
		  <%}//else de liquidaciones %>   
     
     
		  
     
     
		 <%if (ajustesPropietario!=null && !ajustesPropietario.isEmpty()){%>
		   <BR><BR>
 			<table border="1" width="630">
				<tr >
					<td>
 						<table width='100%'  class="barratitulo">
                                <tr class="fila">
                                        <td align="left" width='46%' class="subtitulo1" nowrap> FACTURAS DE PROPIETARIO </td>
                                        <td width='54%' ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"align="left"></td>
                                </tr>
                      </table>						
						<table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
						<tr class="tblTitulo">
							<th>Factura</th>
							<th>Item</th>							
							<th>Descripcion</th>
							<th>Valor</th>
							<th>Impuestos</th>
							<th>Saldo</th>
						</tr>
						
                       <%  for (int k = 0; k<ajustesPropietario.size(); k++){
                       			ExtractoDetalle descuento = (ExtractoDetalle) ajustesPropietario.get(k);
                                totalDescuentoPro += descuento.getVlr();
                                totalImpuestosPro += descuento.getImpuestos();
								totalsaldo        += descuento.getVlr_pp_item();%>
                               
					   						
						<tr class="<%= (k%2==0?"filaazul":"filagris") %>">
							<td class="bordereporte" align="center" nowrap><%= descuento.getFactura() %></td>
							<td class="bordereporte" align="center" nowrap><%= descuento.getConcepto()        %></td>
							<td class="bordereporte" align="left"   nowrap><%= descuento.getDescripcion() %></td>
							<td class="bordereporte" align="right"  nowrap><%= Utility.customFormat(descuento.getVlr())  %></td>
							<td class="bordereporte" align="right"  nowrap><%= Utility.customFormat(descuento.getImpuestos() )  %></td>
							<td class="bordereporte" align="right"  nowrap><%= Utility.customFormat(descuento.getVlr_pp_item() )  %></td>
						</tr>							
						<%    } // end for de descuentos %>					
						<tr  class="subtitulo">
							<td class="bordereporte" colspan="3" nowrap>TOTAL FACTURAS PROPIETARIO</td>
							<td class="bordereporte" align="right" nowrap ><%= Utility.customFormat(totalDescuentoPro)  %></td>
							<td class="bordereporte" align="right" nowrap ><%= Utility.customFormat(totalImpuestosPro )  %></td>
							<td class="bordereporte" align="right" nowrap ><%= Utility.customFormat(totalsaldo )  %></td>
						</tr>							
						</table>				
					</td>
				</tr>
			</table>				     
		    
  		 <%  } // end if de ajustes propietario %>	
  		 <br>
  		 <br>	 
		  <%if (ajustesPlaca!=null && !ajustesPlaca.isEmpty()){%>
		   
		   
 			<table border="1" width="630">
				<tr >
					<td>
                          <table width='100%'  class="barratitulo">
                                <tr class="fila">
                                        <td align="left" width='46%' class="subtitulo1" nowrap> FACTURAS DE CORRIDAS NEGATIVAS </td>
                                        <td width='54%' ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"align="left"></td>
                                </tr>
                          </table>							
						<table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
						<tr class="tblTitulo">
							<th>FACTURA</th>
							<th>DESCRIPCION</th>
							<th>CORRIDA</th>	
							<th>VALOR</th>
						</tr>
						<%  for (int j = 0; j<ajustesPlaca.size(); j++){
                                ExtractoDetalle descuento = (ExtractoDetalle) ajustesPlaca.get(j);
                                totalDescuentoPla += descuento.getVlr();
                        %>
					    						
						<tr class="<%= (j%2==0?"filaazul":"filagris") %>">
							<td class="bordereporte" align="center" nowrap><%= descuento.getFactura() %></td>
							<td class="bordereporte" align="left"   nowrap><%= descuento.getDescripcion() %></td>
							<td class="bordereporte" align="center" nowrap><%= descuento.getDocumento() %></td>	
							<td class="bordereporte" align="right"  nowrap><%= Utility.customFormat(descuento.getVlr())  %></td>
						</tr>							
						<%    } // end for de descuentos %>					
						<tr  class="subtitulo">
							<td class="bordereporte" colspan="3" nowrap>TOTAL CORIDA NEGATIVA </td>
							<td class="bordereporte" align="right" nowrap ><%= Utility.customFormat(totalDescuentoPla)  %></td>
						</tr>							
						</table>				
					</td>
				</tr>
			</table>				     
  		 <%  } // end if de ajustes placa	%>
		
		 <br><br>
 			<table border="1" width="500">
			  	<tr>
					<td>			
						<table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
						<tr>
							<td colspan="2" class="bordereporte">
								<table width='100%'  class="barratitulo">
									<tr class="fila">
										<td align="left" width='46%' class="subtitulo1" nowrap> DATOS DE GENERALES DE LIQUIDACION </td>
										<td width='54%' ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"align="left"></td>
									</tr>
							  </table>								
							</td>
						</tr>
						<tr class="filaazul">
							<td class="bordereporte"> TOTAL PLANILLAS </td>
							<td class="bordereporte" align="right"><%= Utility.customFormat(totalPlanillas) %></td>
						</tr>
						<tr class="filagris">
							<td class="bordereporte"> &nbsp;&nbsp;&nbsp;&nbsp;FACTURAS CORIDA NEGATIVA </td>
							<td class="bordereporte" align="right"><%= Utility.customFormat(totalDescuentoPla ) %></td>
						</tr>		
						<tr class="filagris">
							<td class="bordereporte"> &nbsp;&nbsp;&nbsp;&nbsp;FACTURAS PROPIETARIO </td>
							<td class="bordereporte" align="right"><%= Utility.customFormat( totalsaldo ) %></td>
						</tr>		
						<tr class="filaazul">
							<td class="bordereporte"> TOTAL FACTURAS </td>
							<td class="bordereporte" align="right"><%= Utility.customFormat(totalDescuentoPla + totalImpuestosPla +  totalsaldo ) %></td>
						</tr>						
						<tr class="subtitulo">
							<td class="bordereporte"> TOTAL LIQUIDACION </td>
							<td class="bordereporte" align="right"><%= Utility.customFormat(totalPlanillas+ totalDescuentoPla  +  totalsaldo ) %></td>
						</tr>		
						</table>
					</td>
				</tr>
			</table>
			
			<br>
			<br>
			
			<table border="1" width="500">
			  	<tr>
					<td>			
						<table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
						<tr>
							<td colspan="2" class="bordereporte">
								<table width='100%'  class="barratitulo">
									<tr class="fila">
										<td align="left" width='46%' class="subtitulo1" nowrap> VALOR ABONADO </td>
										<td width='54%' ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"align="left"></td>
									</tr>
							  </table>								
							</td>
						</tr>
						
						
							
										
						<tr class="subtitulo">
							<td class="bordereporte"> TOTAL LIQUIDADO </td>
							<td class="bordereporte" align="right"><%= Utility.customFormat( valorF  ) %></td>
						</tr>		
						</table>
					</td>
				</tr>
			</table>
	   	  <% //} // end if de liquidaciones %>   
      			<br>
			<br>
			<table border="1" width="500">
			  	<tr>
					<td>			
						<table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
						<tr>
							<td colspan="2" class="bordereporte">
								<table width='100%'  class="barratitulo">
									<tr class="fila">
										<td align="left" width='46%' class="subtitulo1" nowrap> VALOR NETO DE CONSIGNACION </td>
										<td width='54%' ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"align="left"></td>
									</tr>
							  </table>								
							</td>
						</tr>
										
										
						<% 	    
								double porcentaje = model.AnticiposPagosTercerosSvc.descuento( ProveedorP );
								double ElValor = (valorF);
								double descuento       = ( ElValor * porcentaje )/100;
                                double neto            =  ElValor - descuento ;
								double combank         = 0;
								double consignacion    = 0;
								List Datos = model.AnticiposPagosTercerosSvc.Bancos_AnticipoPT( usuario.getDstrct() ,ProveedorP , ""+secu);
								if (Datos.size() > 0){
									Hashtable  Rep  =  new  Hashtable();
									Rep = (Hashtable)Datos.get(0);
									String cuenta = (String) Rep.get("Cuenta");
									String banco = (String) Rep.get("banco");
									String tcta = (String) Rep.get("tcta");
									System.out.println("cuenta  "+cuenta+"   banco  "+banco+"  tcta  "+tcta);
									if(cuenta.equals("69225845948") || cuenta.equals("69226034878") ){
										cuenta = "BANCOLOMBIA";
									}else{
										cuenta = "BANCO OCCIDENTE";
									}
									
									if ( cuenta.equals(banco) && !tcta.equals("EF") ){
										combank = 2000;
									}else if ( !cuenta.equals(banco) && !tcta.equals("EF") ){
										combank = 3500;
									}else{
										combank = 6000;
									}
									consignacion = neto - combank;
								}
						%>
						<tr class="subtitulo">
							<td class="bordereporte"> TOTAL CONSIGNACION </td>
							<td class="bordereporte" align="right"> <%= Utility.customFormat(consignacion) %> </td>
						</tr>		
						</table>
					</td>
				</tr>
			</table>
			<br>
            <br>
			
            
            <img src='<%=BASEURL%>/images/botones/imprimir.gif'        style='cursor:hand'    title='Imprimir....'  name='i_imprimir'    onclick="window.open('<%=BASEURL%>/jsp/cxpagar/liquidacion/ImprimirExtractoPP.jsp','','menubar=yes, resizable=yes, scrollbars=yes')"     onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
            <img src='<%=BASEURL%>/images/botones/salir.gif'           style='cursor:hand'    title='Salir...'      name='i_salir'       onclick='parent.close();'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
            </center>	 
        </div>
    </body>
</html>
