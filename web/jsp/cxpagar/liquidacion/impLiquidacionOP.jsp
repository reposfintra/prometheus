<!--
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      07/10/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: vista previa del formulario de impresion de liquidacion de oc
 --%>
<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>.: Impresion Liquidacion OC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%=BASEURL%>/css/print.css" rel="stylesheet" type="text/css">
<link href="../../../css/print.css" rel="stylesheet" type="text/css">
</head>

<body onLoad="window.print();">
<center>
<%
Vector  liquidaciones      = model.LiqOCSvc.getLiquidaciones();
Vector  ajustesPropietario = model.LiqOCSvc.getDescuentosPropietario();
Vector  corridasNegativas       = model.LiqOCSvc.getCorridasNegativas();
	 double totalPlanillas      = 0;
	 double totalDescuentoPro   = 0;
	 double totalImpuestosPro   = 0;
	 double totalDescuentoPla   = 0;
	 double totalImpuestosPla   = 0;
     double totalDescuentoProS  = 0;
         
 if (liquidaciones!=null && !liquidaciones.isEmpty()){ 
   OP opg = null;
   for (int contLiquidaciones = 0; contLiquidaciones<liquidaciones.size(); contLiquidaciones++){
      Liquidacion liquidacion = (Liquidacion) liquidaciones.get(contLiquidaciones);
      String      oc          =  model.LiqOCSvc.getOC();
      List        facturas    =  liquidacion.getFacturas();
      Planilla    pla         =  liquidacion.getPlanilla();  
      opg = (OP) facturas.get(0);

Util u = new Util();
  
for(int i=0;i<facturas.size();i++){
	OP  op  = (OP) facturas.get(i);
	
%>
<table width="630"  border="0" cellspacing="1" cellpadding="0">

  <% if (contLiquidaciones==0) { %>
  <!--<tr class="textoresaltado">
    <td colspan="4">LIQUIDACION OC No. <%= pla.getNumpla()%></td>
  </tr>-->
  <tr class="textoresaltado">
    <td width="120">PROPIETARIO</td>
    <td width="139"><%= op.getProveedor()%></td>
    <td width="105">NOMBRE</td>
    <td width="266"><%= op.getNameProveedor() %></td>
  </tr>
  <tr class="textoresaltado">
    <td>BANCO</td>
    <td><%=op.getBanco()      %></td>
    <td>SUCURSAL</td>
    <td><%=op.getSucursal()   %></td>
  </tr>
  <tr>
    <td colspan="4">&nbsp;</td>
  </tr>
  <% } %>
  <tr>
    <td colspan="4"><table width="100%"  border="0" cellpadding="0" cellspacing="1" class="tabla_liq">
           
      <tr>
        <td width="15%" class="textoresaltado">OC</td>
        <td width="24%" class="textosimple"><%= pla.getNumpla()%></td>
        <td width="13%" class="textoresaltado">PLACA</td>
        <td width="20%" class="textosimple"><%= pla.getPlaveh()  %></td>
        <td width="10%" align="right" class="textoresaltado"><%= ( !op.getTipocant().equals (""))?op.getTipocant():"" %></td>
        <td width="5%">&nbsp;</td>
		<%   if(! op.isTieneOp() ){   %>
        	<td width="13%" class="textosimple"><%= ( !op.getTipocant().equals (""))?op.getCantiCump():"" %></td>
		<%} else{%>
			<td width="13%" class="textosimple"></td>
		<%}%>
      </tr> 
      <tr>
        <td class="textoresaltado">CLIENTE</td>
        <td colspan="6" class="textosimple"><%= pla.getClientes()  %></td>
        </tr>
      <tr>
        <td class="textoresaltado">RUTA</td>
        <td colspan="4" class="textosimple"><%= pla.getNomori() %> - <%= pla.getNomdest()%></td>
        <td colspan='2' class="textoresaltado"><%=(!pla.getReg_status().equals("C")&&!pla.getReg_status().equals("A"))?"NO CUMPLIDA":(pla.getReg_status().equals("C"))?"CUMPLIDA":"" %></td>
      </tr >
      <tr>
        <td class="textoresaltado">F. DESPACHO</td>
        <td colspan="6" class="textosimple"><%= pla.getFecdsp()  %></td>
      </tr>
      <tr>
        <td class="textoresaltado">REMESA</td>
        <td colspan="6" class="textosimple"><%= pla.getNumrem()  %></td>
      </tr>
      <tr>
        <td colspan="7"><HR></td>
        </tr>
        
        
      <% if ( op.getComentario().equals("") ){
	  totalPlanillas += op.getVlrNeto();
	  %>  
      <tr>
        <td class="textoresaltado">FACTURA</td>
        <td colspan="6" class="textosimple"><%=op.getDocumento()  %></td>
      </tr>
      <tr>
        <td colspan="7"><table width="100%"  border="0" cellspacing="0" cellpadding="1">
          <tr align="center">
            <td class="textoresaltado">ITEM</td>
            <td class="textoresaltado">DESCRIPCION</td>
            <td class="textoresaltado">VALOR</td>
            <td class="textoresaltado">RETEFUENTE</td>
            <td class="textoresaltado">RETEICA</td>
          </tr>
		  <%double tv = 0;
		  double trfte = 0;
		  double trica = 0;
		  List listItem = op.getItem();
				if ( listItem!=null ) {
					for(int j=0;j<listItem.size();j++){
                    	                OPItems  item = (OPItems)listItem.get(j);
                                             if(item.isVisible()  || op.isTieneOp() ){//Ivan Gomez 2006-03-24 %>
          <tr class="textosimple">
            <td><%= item.getItem() %></td>
            <td><%= item.getDescconcepto() %></td>
            <td align="right"><%= Utility.customFormat(item.getVlr() ) %></td>
            <td align="right"><%= Utility.customFormat(item.getVlrReteFuente() ) %></td>
            <td align="right"><%= Utility.customFormat(item.getVlrReteIca() ) %></td>
          </tr>
		  <%			tv += item.getVlr();
		  				trfte += item.getVlrReteFuente();
						trica += item.getVlrReteIca();
                                                }//fin IF(item.getVisible()) ivan Gomez 20006-03-24     
		  			}//fin for
				}%>
          <tr >
            <td colspan="2" class="textoresaltado">TOTAL FACTURAS </td>
            <td align="right" class="parcial"><%= Utility.customFormat(tv) %></td>
            <td align="right" class="descuento"><%= Utility.customFormat(trfte) %></td>
            <td align="right" class="descuento"><%= Utility.customFormat(trica) %></td>
          </tr>
          <tr class="filaTotal"  >
            <td colspan="2">TOTAL A PAGAR </td>
            <td colspan="3" align="right">$ <%= Utility.customFormat(op.getVlrNeto() ) %></td>
            </tr>
        </table></td>
        </tr>

        <%}else{%>

                    <tr class="letra">
                       <td class="textoresaltado"><font color='red'>ERROR</td>
                       <td class="textosimple" colspan="6" ><%= op.getComentario() %></td>
                    </tr>

        <%}%>           
    </table></td>
  </tr>
</table>
	<% } %>
<% } %>
<BR><BR>
		 <% if (ajustesPropietario!=null && !ajustesPropietario.isEmpty()){%>
		   
 			<table width="630"  border="0" cellspacing="1" cellpadding="0" class="tabla_liq">
			<tr >
			<td>
 			  <table width='100%'  class="barratitulo">
                                <tr class="fila">
                                        <td align="left" width='46%' class="textoresaltado" nowrap> FACTURAS DE PROPIETARIO </td>
                                </tr>
                                <tr class="fila">
                                        <td><hr></td>
                                </tr>                                
                          </table>						
						<table  width="630"  border="0" cellspacing="1" cellpadding="0">
						<tr class="textoresaltado">
							<th>Factura</th>
							<th>Item</th>							
							<th>Descripcion</th>
							<th>Valor</th>
							<th>Impuestos</th>
							<th>Saldo</th>
						</tr>
						<%  for (int contDescuentos = 0; contDescuentos<ajustesPropietario.size(); contDescuentos++){
								  DescuentoMIMS descuento = (DescuentoMIMS) ajustesPropietario.get(contDescuentos);		
								  totalDescuentoPro += descuento.getValor_item();
								  totalImpuestosPro += descuento.getIva()  -descuento.getRiva() -descuento.getRica() - descuento.getRetefuente();
								  totalDescuentoProS  += descuento.getValor() ;
					    %>						
						<tr class="textosimple">
							<td align="left" nowrap><%= descuento.getFactura() %></td>
							<td align="left" nowrap><%= descuento.getItem()        %></td>
							<td align="left"   nowrap><%= descuento.getDescripcion() %></td>
							<td align="right"  nowrap><%= Utility.customFormat(descuento.getValor_item())  %></td>
							<td align="right"  nowrap><%= Utility.customFormat(descuento.getIva()  -descuento.getRiva() -descuento.getRica() - descuento.getRetefuente())  %></td>
							<td align="right"  nowrap><%= Utility.customFormat( descuento.getValor() )  %></td>
						</tr>							
						<%    } // end for de descuentos %>					
						<tr  class="filaTotal">
							<td class="bordereporte" colspan="3" nowrap>TOTAL FACTURAS PROPIETARIO</td>
							<td class="bordereporte" align="right" nowrap ><%= Utility.customFormat(totalDescuentoPro)  %></td>
							<td class="bordereporte" align="right" nowrap ><%= Utility.customFormat(totalImpuestosPro )  %></td>
							<td class="bordereporte" align="right" nowrap ><%= Utility.customFormat(totalDescuentoProS)  %></td>
						</tr>							
						</table>				
					</td>
				</tr>
			</table>				     
  		 <%  } // end if de descuentos %>	
  		 
 		 <br><BR>
		 
		 <%if (corridasNegativas!=null && !corridasNegativas.isEmpty()){%>
		   
 			<table width="630"  border="0" cellspacing="1" cellpadding="0" class="tabla_liq">
			<tr >
			<td>
                          <table width='100%'  class="barratitulo">
                                <tr class="fila">
                                        <td align="left" width='46%' class="textoresaltado" nowrap>FACTURAS DE CORRIDAS NEGATIVAS   </td>
                                </tr>
                                <tr class="fila">
                                        <td><hr></td>
                                </tr>
                          </table>							
						<table  width="630"  border="0" cellspacing="1" cellpadding="0">
						<tr class="textoresaltado">
							<th>FACTURA</th>					
							<th>DESCRIPCION</th>
							<th>CORRIDA</th>
							<th>VALOR</th>
						</tr>
						<%  for (int contDescuentos = 0; contDescuentos<corridasNegativas.size(); contDescuentos++){
								  DescuentoMIMS descuento = (DescuentoMIMS) corridasNegativas.get(contDescuentos);		
								  totalDescuentoPla += descuento.getValor();
					    %>						
						<tr class="textosimple">					
							<td  align="left" nowrap><%= descuento.getFactura() %></td>
							<td  align="left"   nowrap><%= descuento.getDescripcion() %></td>
							<td  align="right"  nowrap><%= descuento.getFactura_ext()  %></td>
							<td  align="right"  nowrap><%= Utility.customFormat(descuento.getValor())  %></td>
						</tr>							
						<%    } // end for de descuentos %>					
						<tr  class="filaTotal">
							<td colspan="3" nowrap>TOTAL CORIDA NEGATIVA </td>
							<td align="right" nowrap ><%= Utility.customFormat(totalDescuentoPla)  %></td>						</tr>							
						</table>				
					</td>
				</tr>
			</table>				     
  		 <%  } // end corridas negativas	%>
	 	 <br>
		 <br>
                <table width="370" border="0" cellspacing="1" cellpadding="0" class="tabla_liq">
			  	<tr>
					<td>			
						<table width="100%" >
						<tr>
							<td colspan="2" class="textoresaltado">
								<table width='100%'  class="barratitulo">
									<tr class="fila">
										<td align="left" width='46%' class="textoresaltado" nowrap> DATOS DE GENERALES DE LIQUIDACION </td>
									</tr>
									<tr class="fila">
										<td><hr></td>
									</tr>
							  </table>								
							</td>
						</tr>
						<tr class="textosimple">
							<td > TOTAL PLANILLAS </td>
							<td align="right"><%= Utility.customFormat(totalPlanillas) %></td>
						</tr>
						<tr class="textosimple">
							<td > &nbsp;&nbsp;&nbsp;&nbsp;FACTURAS CORIDA NEGATIVA </td>
							<td align="right"><%= Utility.customFormat(totalDescuentoPla ) %></td>
						</tr>		
						<tr class="textosimple">
							<td > &nbsp;&nbsp;&nbsp;&nbsp;FACTURAS PROPIETARIO </td>
							<td align="right"><%= Utility.customFormat(totalDescuentoProS) %></td>
						</tr>		
						<tr class="textosimple">
							<td > TOTAL FACTURAS </td>
							<td align="right"><%= Utility.customFormat(totalDescuentoPla + totalDescuentoProS) %></td>
						</tr>						
						<tr class="filaTotal">
							<td > TOTAL LIQUIDACION </td>
							<td align="right"><%= Utility.customFormat(totalPlanillas+ totalDescuentoPla + totalDescuentoProS) %></td>
						</tr>		
						</table>
					</td>
				</tr>
			</table>		 
				
		

<% } %>

</center>
</body>
</html>
<!--
     Entregado a jbarros 21 Febrero
  -->
