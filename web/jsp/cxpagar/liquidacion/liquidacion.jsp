<!--
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      07/10/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: formulario q permite ingresar la planilla para realizar la consulta de liquidacion
 --%>

<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>
<%
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>

    <title>.:: Liquidacion</title>
    <link href="<%= BASEURL %>/css/estilostsp.css"     rel="stylesheet" type="text/css"> 
    <link href="/css/estilostsp.css" rel="stylesheet"  type="text/css">
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    
    <script>
       function cambiarLeyenda (tipo){
	      switch (parseInt(tipo)){
		     case 0: leyenda.innerHTML = "&nbsp;Planilla";  filaVP.style.display = "none" ; 
				  formulario.Opcion.value = "Liquidar";
				  formulario.vistaPreviaOcs.checked =  false;			 
			 break;
			 case 1: leyenda.innerHTML = "&nbsp;Proveedor"; filaVP.style.display = "block"; break;						 
 		     case 2: leyenda.innerHTML = "&nbsp;Id MIMS";   filaVP.style.display = "block"; break;
 		     case 3: leyenda.innerHTML = "&nbsp;Placa ";    filaVP.style.display = "block"; break;			 
		  }
	   }
       function  searchLiq(theForm){
         if( theForm.parametro.value==''){
            alert('Deber� digitar el codigo para poder continuar');
            return false;
         }
         else{
           document.i_aceptar.src='<%=BASEURL%>/images/botones/aceptarDisable.gif';
           document.i_aceptar.onmouseover = new Function('');
           document.i_aceptar.onmouseout  = new Function('');
           document.i_aceptar.onclick     = new Function('');
           theForm.submit();
         }
       }
    </script>

</head>
<body onLoad="redimensionar();formulario.parametro.focus();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Liquidacion de OCs"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
   
    <form action="<%=CONTROLLER%>?estado=Liquidar&accion=OC" method="post" name="formulario">
    
    <table width="440"  border="2" align="center">
     <tr>
         <td width="428" colspan='2'>           
    
                <table width="100%"  align="center">
                  <tr >
                     <td colspan="2">
                          <table width='100%'  class="barratitulo">
                                <tr class="fila">
                                        <td align="left" width='46%' class="subtitulo1" nowrap> Liquidaci&oacute;n </td>
                                        <td width='54%' ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"align="left"><%=datos[0]%></td>
                                </tr>
                          </table>
                     </td>
                  </tr>
                  <tr class='fila'>
                     <td colspan="2">&nbsp;Liquidar por :</td>
                  </tr>				  
                  <tr class='fila'>
                     <td colspan='2' > 

				        <span style="width:25%;">
							  &nbsp;<input type='radio' value='0' name='tipo' onClick="cambiarLeyenda(this.value);"  checked  >
							  Planilla 						
						</span>
				        <span style="width:25%;">
							 &nbsp;<input type='radio' value='1' name='tipo' onClick="cambiarLeyenda(this.value);"  >
							 Propietario						
						</span>						
				        <span style="width:25%;">
							 &nbsp;<input type='radio' value='2' name='tipo' onClick="cambiarLeyenda(this.value);"  >
							 Id MIMS 						
						</span>						
				        <span style="width:24%;">
							 &nbsp;<input type='radio' value='3' name='tipo' onClick="cambiarLeyenda(this.value);" >
							 Placa						
						</span>
					</td>					 
                  </tr>				  
				  
                  <tr class='fila'>
                     <td width="36%" ><div id='leyenda'>&nbsp;Numero Planilla </div></td>
                     <td width="64%" > <input type='text' maxlength='10' name='parametro' class="textbox" > </td>
                  </tr>
                  <tr class='fila' id="filaVP" style="display:none ">
                     <td colspan="2">&nbsp;<input type="checkbox" name="vistaPreviaOcs"  onClick="Opcion.value=(!this.checked?'Liquidar':'VistaPrevia'); ">&nbsp;Mostrar Numero de planillas a liquidar.</td>
                  </tr>				  
                </table>
                
            </td>
         </tr>
       </table>  
            
        <br>
        <center>
            <img src='<%=BASEURL%>/images/botones/aceptar.gif'    style="cursor:hand"   title='Aceptar...'    name='i_aceptar'   onclick="javascript:searchLiq(formulario)"     onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
            <img src='<%=BASEURL%>/images/botones/salir.gif'      style='cursor:hand'   title='Salir...'      name='i_salir'     onclick='parent.close();'                      onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
        </center>
	<br>
		 <%if (request.getParameter("msg") != null ){%>      
            <table border="2" align="center">
              <tr>
                <td>
					<table width="100%"  border="0" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                  		<tr>
                    		<td width="500" align="center" class="mensajes"><%= request.getParameter("msg") %></td>
                    		<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    		<td width="58">&nbsp; </td>
                  		</tr>
                	</table>
				</td>
              </tr>
     </table>               
     <%}%>   
	 <input type="hidden" name="Opcion" value="Liquidar">
    </form>
	

	
</div>
<%=datos[1]%>
</body>
</html>
