<!--
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      07/10/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: vista previa del formulario de impresion de liquidacion de oc
 --%>
<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>.: Impresion Extracto Liquidacion OC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%=BASEURL%>/css/print.css" rel="stylesheet" type="text/css">
<link href="../../../css/print.css" rel="stylesheet" type="text/css">
</head>

<body onLoad="window.print();">
<center>
<% 	 
     Vector  liquidaciones      = model.ExtractoPPSvc.getExtractoDetallePP1();
     Vector  ajustesPropietario = model.ExtractoPPSvc.getExtractoDetallePP2();
     Vector  ajustesPlaca       = model.ExtractoPPSvc.getExtractoDetallePP0();
	 
	 
     String  msjEstado          =  "";
	 double totalPlanillas      = 0;
	 double totalDescuentoPro   = 0;
	 double totalImpuestosPro   = 0;
	 double totalDescuentoPla   = 0;
	 double totalImpuestosPla   = 0;
	 int   planillasConEerrores = 0;
	 int   cent                 = 0; 
	 String    Dcoument         = ""; 
	 float     valorF           = 0;
 	 float totalsaldo			= 0;
	 
         if (liquidaciones!=null && !liquidaciones.isEmpty()){ 
             ExtractoDetalle liquidacion1 = (ExtractoDetalle) liquidaciones.get(0);
             Dcoument = liquidacion1.getDocumento();
         }else if(ajustesPlaca!=null && !ajustesPlaca.isEmpty()){
             ExtractoDetalle ajustesPlaca1 = (ExtractoDetalle) liquidaciones.get(0);
             Dcoument = ajustesPlaca1.getDocumento();
         }else if(ajustesPropietario!=null && !ajustesPropietario.isEmpty()){
             ExtractoDetalle ajustesPropietario1 = (ExtractoDetalle) liquidaciones.get(0);
             Dcoument = ajustesPropietario1.getDocumento();
         }
         
         
	 
	 
	 if (liquidaciones!=null && !liquidaciones.isEmpty()){ 
	   for (int i = 0; i<liquidaciones.size(); i++){
	      ExtractoDetalle liquidacion = (ExtractoDetalle) liquidaciones.get(i);
	      cent=0;
		  model.ExtractoPPSvc.buscarExtractoPP(liquidacion.getNit(),liquidacion.getFecha());
		  Extracto     Ext            = model.ExtractoPPSvc.getExtractoPPU();
                 if (i==0) {
				 	valorF = model.ExtractoPPSvc.retornar_vlr_pp_e(Ext.getFecha()) ;
				 %>
				 
				 
				 
				 
				  <br>
                <table width="307" border="0" cellspacing="1" cellpadding="0" class="tabla_liq">
			  	<tr>
					<td>			
						<table width="100%" >
						
						
						<tr class="filaTotal">
							<td > LIQUIDACION </td>
							<td align="right"><%= liquidacion.getSecuencia() %></td>
						</tr>		
						</table>
					</td>
				</tr>
			</table>
			<br>
			<br>
				 
<table width="630"  border="0" cellspacing="1" cellpadding="0">



  
  <tr class="textoresaltado">
    <td width="120">PROPIETARIO</td>
    <td width="139"><%= Ext.getNit()%></td>
    <td width="105">NOMBRE</td>
    <td width="266"><%= Ext.getNombre_trans() %></td>
  </tr>
  <tr class="textoresaltado">
    <td>BANCO</td>
    <td><%=Ext.getBanco()     %></td>
    <td>SUCURSAL</td>
    <td><%=Ext.getSucursal()   %></td>
  </tr>
  <tr>
    <td colspan="4">&nbsp;</td>
  </tr>
  <% } %>
  <tr>
  <table width="630"  border="0" cellspacing="1" cellpadding="0">
    <td colspan="4"><table width="100%"  border="0" cellpadding="0" cellspacing="1" class="tabla_liq">
          
      <tr>
        <td width="15%" class="textoresaltado">OC</td>
        <td width="24%" class="textosimple"><%= liquidacion.getDocumento()%></td>
        <td width="13%" class="textoresaltado">PLACA</td>
        <td width="20%" class="textosimple"><%= liquidacion.getPlaveh()  %></td>
        <td width="10%" align="right" class="textoresaltado"><%= ( !liquidacion.getUnit_vlr().equals (" ") )?liquidacion.getUnit_vlr():"" %></td>
        <td width="5%">&nbsp;</td>
        <td width="13%" class="textosimple"><%= ( !liquidacion.getPesoreal().equals (" ") )?  Utility.customFormat( Float.parseFloat( liquidacion.getPesoreal() ) ) :"" %></td>
      </tr> 
      <tr>
        <td class="textoresaltado">CLIENTE</td>
        <td colspan="6" class="textosimple"><%= liquidacion.getNomc()  %></td>
        </tr>
      <tr>
        <td class="textoresaltado">RUTA</td>
        <td colspan="4" class="textosimple"><%= liquidacion.getOripla() %> - <%= liquidacion.getDespla()%></td>
        <td colspan='2' class="textoresaltado"><%=(!liquidacion.getStapla().equals("C")&&!liquidacion.getStapla().equals("A"))?"NO CUMPLIDA":(liquidacion.getStapla().equals("C"))?"CUMPLIDA":"" %></td>
      </tr >
      <tr>
        <td class="textoresaltado">F. DESPACHO</td>
        <td colspan="6" class="textosimple"><%= liquidacion.getFecdsp()  %></td>
      </tr>
      <tr>
        <td class="textoresaltado">REMESA</td>
        <td colspan="2" class="textosimple"><%= liquidacion.getNumrem()  %></td>
		<td class="textoresaltado">REMISION</td>
        <td colspan="3" class="textosimple"><%= liquidacion.getRemision()  %></td>
      </tr>
      <tr>
        <td colspan="7"><HR></td>
        </tr>
      <tr>
        <td class="textoresaltado">FACTURA</td>
        <td colspan="6" class="textosimple"><%=liquidacion.getDocumento()  %></td>
      </tr>
      <tr>
        <td colspan="7"><table width="100%"  border="0" cellspacing="0" cellpadding="1">
          <tr align="center">
            <td class="textoresaltado">ITEM</td>
            <td class="textoresaltado">DESCRIPCION</td>
            <td class="textoresaltado">VALOR</td>
            <td class="textoresaltado">RETEFUENTE</td>
            <td class="textoresaltado">RETEICA</td>
          </tr>
		  <%double tv = 0;
		  double trfte = 0;
		  double trica = 0;
		   %>
           <% while ( cent == 0) {
          String Comp="";
          liquidacion = (ExtractoDetalle) liquidaciones.get(i);%>
          <tr class="textosimple">
            <td><%= liquidacion.getConcepto() %></td>
            <td><%= liquidacion.getDescripcion() %></td>
            <td align="right"><%= Utility.customFormat(liquidacion.getVlr() ) %></td>
            <td align="right"><%= Utility.customFormat(liquidacion.getRetefuente() ) %></td>
            <td align="right"><%= Utility.customFormat(liquidacion.getReteica()  ) %></td>
          </tr>
		  <%	tv    += liquidacion.getVlr() ;
							trfte += liquidacion.getRetefuente();
							trica += liquidacion.getReteica(); 
							if ( (i+1) < liquidaciones.size()){
								Comp = ((ExtractoDetalle) liquidaciones.get(i+1)).getDocumento();  
							}else{
								Comp="";
								}%>

				   <%   if ( liquidacion.getDocumento().equals( Comp) ) {%>                                                                                  
									  <%i++;
						}else{//fin if
							   cent++;
						%>
          <tr>
            <td colspan="2" class="textoresaltado">TOTAL FACTURAS </td>
            <td align="right" class="parcial"><%= Utility.customFormat(tv) %></td>
            <td align="right" class="descuento"><%= Utility.customFormat(trfte) %></td>
            <td align="right" class="descuento"><%= Utility.customFormat(trica) %></td>
          </tr>
          <tr class="filaTotal">
            <td colspan="2">TOTAL A PAGAR </td>
            <td colspan="3" align="right">$ <%= Utility.customFormat( tv+trfte+trica ) %></td>
			<%   totalPlanillas += tv+trfte+trica; %>
            </tr>
        </table></td>
        </tr>
       </table></td>
      </tr>
     </table>
		<%}//fin else
      }//fin while del op
       %>
<% } %>

<% }// end liquidaciones%>
    <br>
		
		 <% if (ajustesPropietario!=null && !ajustesPropietario.isEmpty()){%>
		  
 			<table width="630"  border="0" cellspacing="1" cellpadding="0" class="tabla_liq">
			<tr >
			<td>
 			  <table width='100%'  class="barratitulo">
                                <tr class="fila">
                                        <td align="left" width='46%' class="textoresaltado" nowrap> FACTURAS DE PROPIETARIO </td>
                                </tr>
                                <tr class="fila">
                                        <td><hr></td>
                                </tr>                                
                          </table>						
						<table  width="630"  border="0" cellspacing="1" cellpadding="0">
						<tr class="textoresaltado">
							<th>Factura</th>
							<th>Item</th>							
							<th>Descripcion</th>
							<th>Valor</th>
							<th>Impuestos</th>
							<th>Saldo</th>
						</tr>
						<%  for (int k = 0; k<ajustesPropietario.size(); k++){
                       			ExtractoDetalle descuento = (ExtractoDetalle) ajustesPropietario.get(k);
                                totalDescuentoPro += descuento.getVlr();
                                totalImpuestosPro += descuento.getImpuestos();
								totalsaldo        += descuento.getVlr_pp_item();%>				
						<tr class="textosimple">
							<td align="left" nowrap><%= descuento.getFactura() %></td>
							<td align="left" nowrap><%= descuento.getConcepto()        %></td>
							<td align="left"   nowrap><%= descuento.getDescripcion() %></td>
							<td align="right"  nowrap><%= Utility.customFormat(descuento.getVlr())  %></td>
							<td align="right"  nowrap><%= Utility.customFormat(descuento.getImpuestos())  %></td>
							<td align="right"  nowrap><%= Utility.customFormat(descuento.getVlr_pp_item() )  %></td>
						</tr>							
						<%    } // end for de descuentos %>					
						<tr  class="filaTotal">
							<td class="bordereporte" colspan="3" nowrap>TOTAL FACTURAS PROPIETARIO</td>
							<td class="bordereporte" align="right" nowrap ><%= Utility.customFormat(totalDescuentoPro)  %></td>
							<td class="bordereporte" align="right" nowrap ><%= Utility.customFormat(totalImpuestosPro )  %></td>
							<td class="bordereporte" align="right" nowrap ><%= Utility.customFormat(totalsaldo )  %></td>
						</tr>							
						</table>				
					</td>
				</tr>
			</table>				     
  		 <%  } // end if de descuentos %>	
  		 
 		 <%if (ajustesPlaca!=null && !ajustesPlaca.isEmpty()){%>
		   <BR>
 			<table width="630"  border="0" cellspacing="1" cellpadding="0" class="tabla_liq">
			<tr >
			<td>
                          <table width='100%'  class="barratitulo">
                                <tr class="fila">
                                        <td align="left" width='46%' class="textoresaltado" nowrap> FACTURAS DE CORRIDAS NEGATIVAS </td>
                                </tr>
                                <tr class="fila">
                                        <td><hr></td>
                                </tr>
                          </table>							
						<table  width="630"  border="0" cellspacing="1" cellpadding="0">
						<tr class="textoresaltado">
							<th>FACTURA</th>					
							<th>DESCRIPCION</th>
							<th>CORRIDA</th>
							<th>VALOR</th>
						</tr>
						<%  for (int j = 0; j<ajustesPlaca.size(); j++){
                                ExtractoDetalle descuento = (ExtractoDetalle) ajustesPlaca.get(j);
                                totalDescuentoPla += descuento.getVlr();
                                totalImpuestosPla += descuento.getImpuestos();
                        %>					
						<tr class="textosimple">
							<td  align="left" nowrap><%= descuento.getFactura() %></td>
							<td  align="left"   nowrap><%= descuento.getDescripcion() %></td>
							<td  align="left" nowrap><%= descuento.getDocumento() %></td>						
							<td  align="right"  nowrap><%= Utility.customFormat(descuento.getVlr())  %></td>
						</tr>							
						<%    } // end for de descuentos %>					
						<tr  class="filaTotal">
							<td colspan="3" nowrap>TOTAL CORIDA NEGATIVA </td>
							<td align="right" nowrap ><%= Utility.customFormat(totalDescuentoPla)  %></td>
						</tr>							
						</table>				
					</td>
				</tr>
			</table>				     
  		 <%  } // end if de descuentos propietario	%>
	 
		 <br>
                <table width="370" border="0" cellspacing="1" cellpadding="0" class="tabla_liq">
			  	<tr>
					<td>			
						<table width="100%" >
						<tr>
							<td colspan="2" class="textoresaltado">
								<table width='100%'  class="barratitulo">
									<tr class="fila">
										<td align="left" width='46%' class="textoresaltado" nowrap> DATOS DE GENERALES DE LIQUIDACION </td>
									</tr>
									<tr class="fila">
										<td><hr></td>
									</tr>
							  </table>								
							</td>
						</tr>
						<tr class="textosimple">
							<td > TOTAL PLANILLAS </td>
							<td align="right"><%= Utility.customFormat(totalPlanillas) %></td>
						</tr>
						<tr class="textosimple">
							<td > &nbsp;&nbsp;&nbsp;&nbsp;FACTURAS CORIDA NEGATIVA </td>
							<td align="right"><%= Utility.customFormat(totalDescuentoPla + totalImpuestosPla) %></td>
						</tr>		
						<tr class="textosimple">
							<td > &nbsp;&nbsp;&nbsp;&nbsp;FACTURAS PROPIETARIO </td>
							<td align="right"><%= Utility.customFormat(totalsaldo) %></td>
						</tr>		
						<tr class="textosimple">
							<td > TOTAL FACTURAS </td>
							<td align="right"><%= Utility.customFormat(totalDescuentoPla + totalImpuestosPla + totalsaldo ) %></td>
						</tr>						
						<tr class="filaTotal">
							<td > TOTAL LIQUIDACION </td>
							<td align="right"><%= Utility.customFormat(totalPlanillas+ totalDescuentoPla + totalImpuestosPla + totalsaldo ) %></td>
						</tr>		
						</table>
					</td>
				</tr>
			</table>		 
				
				 <br>
                <table width="370" border="0" cellspacing="1" cellpadding="0" class="tabla_liq">
			  	<tr>
					<td>			
						<table width="100%" >
						
						
						<tr class="filaTotal">
							<td > TOTAL LIQUIDADO </td>
							<td align="right"><%= Utility.customFormat(valorF) %></td>
						</tr>		
						</table>
					</td>
				</tr>
			</table>
				
		

<% //} %>

</center>
</body>
</html>
