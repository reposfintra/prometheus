<!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      21/02/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description:  Vista que del extracto
 --%>

<%@page    session   ="true"%> 
<%@page    errorPage ="/error/ErrorPage.jsp"%>
<%@page    import    ="com.tsp.operation.model.beans.*"%>
<%@include file      ="/WEB-INF/InitModel.jsp"%>
<%@page    import    ="com.tsp.util.*"%>
<%@page    import    ="java.util.*" %>
<%  
	String path     = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[]  = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);   
	String periodoI = (String) request.getParameter("fini");
	String periodoF = (String) request.getParameter("ffin");
	Proveedor pro   = model.ConsultaExtractoSvc.getProveedor();
	String show     = Util.coalesce((String) request.getParameter("show"),"");
%>
<html>
<head>

    <title>Listado de Extractos</title>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listado de Extractos."/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>

	<table border="0" width="900">
	<tr class="tblTituloSinFondo">
		<td align="left"><%=  (pro!=null? "[" + pro.getC_nit() + "] " + pro.getC_payment_name():"") %></td>
		<td align="right">EXTRACTO <%= Util.getFechaActual_String(6) %></td>
	</tr>
	<tr><td colspan="2"><hr width="100%" color="#000066"></td></tr>
	</table>

<% Vector v = model.ConsultaExtractoSvc.getVectorEgresos();
   if (v!=null && !v.isEmpty()) {
%>

     <form action='<%=CONTROLLER%>?estado=Consulta&accion=Extracto&opcion=ConsultaDetalleEgresos' method='post' name='formulario'>
		 
         <table  width="900" border="2" align="center">
             <tr>
                  <td>  
                       <table width='100%' align='center' class='tablaInferior'>

                              <tr class="barratitulo">
                                    <td colspan='10' >
                                           <table cellpadding='0' cellspacing='0' width='100%'>
                                                 <tr class="subtitulo1">
                                                      <td align="center" class="subtitulo1" colspan="2" height="35" >
													  PAGOS REALIZADOS <BR> PERIODO &nbsp;  <%= periodoI + " AL " + periodoF %>
													  </td>
                                             </tr>
                                      </table>
                                    </td>
                             </tr>
                             
                             <tr  class="tblTitulo">                                   
									<td width='89' align="center" class="bordereporte">Banco</td>
                                    <td width='220' align="center" class="bordereporte">Sucursal</td>
									<td width="98" align="center" class="bordereporte">Cheque</td>
                                    <td width='98' align="center" class="bordereporte">Fecha</td>
                                    <td width='154' align="center" class="bordereporte">Estado</td>                                    
									<td width='81' align="center" class="bordereporte">Moneda</td>
									<td width='128' align="center" class="bordereporte">Valor</td>
									<td width='34' align="center" class="bordereporte" title="Mostrar Detalle Egreso">Det. </td>
                             </tr>	
							 <% double totalEgreso = 0; 
							 	 for (int i =0; i<v.size(); i++){
								 	Egreso e = (Egreso) v.get(i); 
									totalEgreso += e.getVlr();
									String es_lis_egr = (i%2==0?"filagris":"filaazul"); 
									String rowspan = (e.isMostrarDetalleEgreso()?" rowspan=" + (e.getDetalle().size() + 2)+ " ":"");
									
							 %>

								 <tr  class="<%= es_lis_egr %>" >
								   		<td align="left" class="bordereporte" >&nbsp;<%= e.getBranch_code() %></td>
										<td align="left" class="bordereporte" >&nbsp;<%= e.getBank_account_no() %></td>
										<td align="left" class="bordereporte" nowrap>&nbsp;<%= e.getDocument_no() + (e.getConcept_code().equals("TR")?" <SUP><B>TRANS</B></SUP>":"") %></td>
										<td align="center" class="bordereporte">&nbsp;<%= e.getPrinter_date() %></td>
								        <td align="center" class="bordereporte">&nbsp;<%= (e.getFechaEnvio().equals("0099-01-01 00:00:00")?"":"ENTREGADO") %></td>								        
 							            <td align="center" class="bordereporte">&nbsp;<%= e.getCurrency() %></td>
										<td align="right" class="bordereporte">&nbsp;<%= UtilFinanzas.customFormat("#,##0",e.getVlr(),0) %>&nbsp;</td>
										<td align="center" class="<%= (e.isMostrarDetalleEgreso ()?"filaverde":"")%>"  ><input type="checkbox" name="dEgr" value="<%= i %>" <%= (e.isMostrarDetalleEgreso ()?"checked":"")%> onClick=" this.form.submit(); //this.parentNode.className=(this.checked?'filaverde':'<%= es_lis_egr %>');  "></td>										
								 </tr>						
								 <% Vector detalle = e.getDetalle();
								 	if (e.isMostrarDetalleEgreso() && detalle!=null && !detalle.isEmpty()) { %>
								 <tr>
								 	<td></td>
									<td colspan="6" class="bordereporte">
										<table width="100%">
											<tr class="tblTitulo2" >
												<td width="9%" align="center" class="bordereporte" >Item</td>
												<td width="67%" align="center" class="bordereporte" >Descripcion</td>
												<td width="18%" align="center" class="bordereporte" >Valor</td>
												<td width="6%" align="center" class="bordereporte" >Det</td>
											</tr>
											
											<!-- seccion item detalle egreso -->
											<% 
												 for (int j=0; j<detalle.size(); j++) {
													Egreso d = (Egreso) detalle.get(j);	 
													String es_detalle = "fila2"; //(j%2==0?"fila2":"fila1"); 
											%>
											<tr class="<%= es_detalle %>" >
												<td align="center" class="bordereporte"><%= d.getItem_no() %></td>																	
												<td class="bordereporte">&nbsp;<%= d.getDescripcion() %></td>																	
												<td align="right" class="bordereporte"><%= UtilFinanzas.customFormat("#,##0",d.getVlr(),0) %>&nbsp;</td>		
												<td align="center"class="<%= (d.isMostrarDetalleEgreso()?"filaverde":"bordereporte") %>"><input type="checkbox" name="dFacE<%= i %>" value="<%= j %>" <%= (d.isMostrarDetalleEgreso()?"checked":"") %> onClick="this.form.submit();" ></td>
											</tr> 
											
											
											
											<!-- seccion de la factura del detalle del  cheque -->
											<%  
												CXP_Doc factura   = d.getFactura();
												Planilla planilla = d.getPlanilla();
												if (d.isMostrarDetalleEgreso() && (d.getConcept_code().equals("FAC") ||  d.getConcept_code().equals("TR")) && factura!=null){ %>	
											<tr>
												<td></td>
												<td colspan="3">
													<table width="100%">
														<tr class="fila2">
															<td width="17%" class="bordereporte">&nbsp;<b>Factura</b> </td>
															<td class="bordereporte" colspan="3">&nbsp;<%= factura.getDocumento() %> </td>
															<td width="10%" class="bordereporte">&nbsp;<b>Fecha</b></td>
															<td width="14%" class="bordereporte" align="center"><%= factura.getFecha_documento() %></td>				
															<td width="10%" class="bordereporte">&nbsp;<b>Agencia</b></td>
															<td width="12%" class="bordereporte" align="center"><%= factura.getAgencia()     %></td>																											
														</tr>
														<tr class="fila2">
															<td class="bordereporte" >&nbsp;<b>Descripcion</b></td>
															<td colspan="7" class="bordereporte" >&nbsp;<%= factura.getDescripcion() %></td>																																												
														</tr>
														
														
														<!-- detalle de la factura del item del cheque -->
														<tr >
															<td colspan="8" >
																<table width="100%">
																	<tr class="tblTitulo3">
																		<th width="5%" class="bordereporte">Item</th>
																		<th width="34%" class="bordereporte">&nbsp;Descripcion</th>
																		<th width="13%" class="bordereporte">Valor</th>
																		<th width="12%" class="bordereporte">IVA</th>
																		<th width="12%" class="bordereporte">RIVA</th>
																		<th width="12%" class="bordereporte">RICA</th>
																		<th width="12%" class="bordereporte">RFTE</th>
																	</tr>	
																	<% Vector itemFact = factura.getItems();
																	   double tvlr = 0, tiva = 0, triva = 0, trica = 0, trfte = 0;
																	   for (int k=0; itemFact!=null && k<itemFact.size(); k++){
																	   	   CXPItemDoc item = (CXPItemDoc) itemFact.get(k);
																		   tvlr  += item.getVlr();
																		   tiva  += item.getVlr_iva();
																		   triva += item.getVlr_riva();
																		   trica += item.getVlr_rica();
																		   trfte += item.getVlr_rfte();
																		   String es_item_fact = "fila1"; //(k%2==0?"fila2":"fila1");
																	%>
																	<tr class="<%= es_item_fact %>" style="font-size:11px ">
																		<td width="6%" align="center" class="bordereporte"><%= item.getItem() %></td>
																		<td width="33%" class="bordereporte" nowrap >&nbsp;<%= item.getDescripcion() %></td>
																		<td width="13%" class="bordereporte" nowrap align="right"><%= UtilFinanzas.customFormat("#,##0",item.getVlr()     ,0) %>&nbsp;</td>
																		<td width="10%" class="bordereporte" nowrap align="right"><%= UtilFinanzas.customFormat("#,##0",item.getVlr_iva() ,0) %>&nbsp;</td>
																		<td width="13%" class="bordereporte" nowrap align="right"><%= UtilFinanzas.customFormat("#,##0",item.getVlr_riva(),0) %>&nbsp;</td>
																		<td width="13%" class="bordereporte" nowrap align="right"><%= UtilFinanzas.customFormat("#,##0",item.getVlr_rica(),0) %>&nbsp;</td>
																		<td width="12%" class="bordereporte" nowrap align="right"><%= UtilFinanzas.customFormat("#,##0",item.getVlr_rfte(),0) %>&nbsp;</td>
																	</tr>
																	<% } %>						
																	
																	<% if(itemFact!=null && itemFact.size()>1) { %>
																		<tr class="tblTitulo" style="font-size:11px ">
																			<td colspan="2" class="bordereporte" align="right"><b>Subtotal </b>&nbsp;</td>
																			<td class="bordereporte" align="right"><%= UtilFinanzas.customFormat("#,##0",tvlr  ,0) %>&nbsp;</td>
																			<td class="bordereporte" align="right"><%= UtilFinanzas.customFormat("#,##0",tiva  ,0) %>&nbsp;</td>
																			<td class="bordereporte" align="right"><%= UtilFinanzas.customFormat("#,##0",triva ,0) %>&nbsp;</td>
																			<td class="bordereporte" align="right"><%= UtilFinanzas.customFormat("#,##0",trica ,0) %>&nbsp;</td>
																			<td class="bordereporte" align="right"><%= UtilFinanzas.customFormat("#,##0",trfte ,0) %>&nbsp;</td>
																		</tr>																	
																	<% } %>																				
																</table>
															</td>	
														</tr>
														<!-- fin detalle de la factura del item del cheque -->
														
													</table>
												</td>											
											</tr>
											<!-- fin seccion de la factura del detalle del  cheque -->
											
											
											<!-- fin seccion de la planilla del detalle del  cheque -->											
											<% } else if (d.isMostrarDetalleEgreso() && !d.getTipo_documento().equals("FAC") && planilla!=null) { %>	
											<tr>
												<td></td>
												<td colspan="3">
													<table width="100%">
														<tr class="tblTitulo3">
															<th width="10%" class="bordereporte">Placa</th>
															<th width="53%" class="bordereporte">Ruta</th>
															<th width="12%" class="bordereporte">Fecha</th>
															<th width="16%" class="bordereporte">Estado</th>
															<th width="9%" class="bordereporte">Agencia</th>
														</tr>														
														<tr class="fila1" style="font-size:11px ">
															<td class="bordereporte" nowrap align="center"><%= planilla.getPlaveh()%> </td>
															<td class="bordereporte" nowrap >&nbsp;<%= planilla.getRuta_pla() + (planilla.getRemision().equals("")?"":", "+planilla.getRemision())  + ", " + Util.customFormat(planilla.getVlrParcial()) + " " + planilla.getUnidcam() %> </td>
															<td class="bordereporte" nowrap align="center"><%= planilla.getFecdsp() %></td>
															<td class="bordereporte" nowrap align="center"><%= (planilla.getReg_status().equals("C")?"Cumplida":"Sin Cumplir") %></td>				
															<td class="bordereporte" nowrap align="center"><%= planilla.getAgcpla()     %></td>																											
														</tr>
													</table>
												</td>
											</tr>			
											<% } %>
											
											
											
											<%  } %>
											<!-- fin seccion item detalle egreso -->
										</table>																						
										 								
									</td>
									<td></td>
								 </tr>
								 <tr><td colspan="10">&nbsp;</td></tr>		
								 
								 <% } // fin mostrar egreso %>
								 
							 <%  }  // end for listado egresos %>	
							 
								 <tr  class="tblTitulo">
										<td colspan="6" align="right" class="bordereporte">Total&nbsp;</td>
										<td align="right" class="bordereporte"><%= UtilFinanzas.customFormat("#,##0",totalEgreso,0) %>&nbsp;</td>
								        <td align="right" class="bordereporte">&nbsp;</td>
								 </tr>					 
								 <!--<tr  class="transparente">
										<td colspan="10" align="right" class="bordereporte">
										<span class="msgLogin" >Para mostrar los detalles de los items seleccionados haga click sobre el boton Aceptar </span>
										</td>
						 		</tr>-->													 
					</table>
				</td>	
			</tr>
		</table>
	 	<img src = "<%=BASEURL%>/images/botones/aceptar.gif" style = "cursor:hand; display:none" name = "imgaceptar" onClick = "formulario.submit(); " onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">       		
       	<input type="hidden" name="fini" value="<%= periodoI %>">
		<input type="hidden" name="ffin" value="<%= periodoF %>">		
		<input type="hidden" name="show" value="<%= show %>">
     </form>
	 <br>
<% } %>	 
	 
	 
	 
	 
	 
	 <!-- SECCION DE FACTURAS PENDIENTES DE PROPIEARIOS -->
	 
	 <% Vector fp = model.ConsultaExtractoSvc.getVectorFacturasPendientes();
	    if (fp!=null && !fp.isEmpty()) { %>
     <form action='<%=CONTROLLER%>?estado=Consulta&accion=Extracto&opcion=ConsultaDetalleFacturasPendientes' method='post' name='formularioFacturasPendientes'>
		 
         <table  width="900" border="2" align="center">
             <tr>
                  <td>  
                       <table width='100%' align='center' class='tablaInferior'>

                              <tr class="barratitulo">
                                    <td colspan='9' >
                                           <table cellpadding='0' cellspacing='0' width='100%'>
                                                 <tr class="subtitulo1">
                                                      <td align="LEFT" class="subtitulo1" colspan="2"  height="25">FACTURAS PENDIENTES POR CANCELAR</td>
                                             </tr>
                                      </table>
                                    </td>
                             </tr>
                             
							 
							 <tr class="tblTitulo">                                   
									<td width='60' align="center" class="bordereporte" >Tipo</td>
									<td width='135' align="center" class="bordereporte">Documento</td>
									<td width='55' align="center" class="bordereporte">Placa</td>		
                                    <td width='310' align="center" class="bordereporte">Descripcion</td>                                    							
									<td width="80" align="center" class="bordereporte">Fecha</td>
                                    <td width='30' align="center" class="bordereporte" title="Agencia">Ag</td>
									<td width='100' align="center" class="bordereporte">Saldo </td>
									<td width='100' align="center" class="bordereporte">Total</td>
									<td width='30' align="center" class="bordereporte">Det</td>
                             </tr>
							 							 
							 <% double totalFactPendientes = 0; 
							    double totalPlaca = 0;				
								String  prevplaca = "";
							 	for (int i =0; i<fp.size(); i++){
								 	CXP_Doc f = (CXP_Doc) fp.get(i); 
									totalFactPendientes += f.getVlr_saldo();
									String es_lis_egr = (i%2==0?"filagris":"filaazul"); 		
									
									String nextplaca = f.getPlaca();							
									if ( (i+1)< fp.size() )
										nextplaca = ((CXP_Doc) fp.get(i+1)).getPlaca();										
									if (!prevplaca.equals( f.getPlaca() ) ) totalPlaca = 0;
									totalPlaca += f.getVlr_saldo();
									prevplaca = f.getPlaca();
							%>
							 
							 
								 <tr  class="<%= es_lis_egr %>" >
								   		<td align="center" class="bordereporte" nowrap>&nbsp;<%= (f.getClase_documento().equals("0")?"Planilla":"Factura") %></td>
										<td align="left" class="bordereporte" nowrap>&nbsp;<%= (f.getClase_documento().equals("0")?f.getPlanilla():f.getDocumento()) %></td>										
										<td align="center" class="bordereporte" nowrap>&nbsp;<%= f.getPlaca() %></td>
										<td align="left" class="bordereporte" nowrap>&nbsp;<%= (f.getClase_documento().equals("0")?f.getDocumento() + " " + f.getObservacion():f.getDescripcion()) %></td>
										<td align="center" class="bordereporte" nowrap>&nbsp;<%= (f.getClase_documento().equals("0")?f.getFecha_planilla():f.getFecha_documento()) %></td>
										<td align="center" class="bordereporte" nowrap>&nbsp;<%= f.getAgencia() %></td>
										<td align="right" class="bordereporte" nowrap>&nbsp;<%= UtilFinanzas.customFormat("#,##0",f.getVlr_saldo(),0) %>&nbsp;&nbsp;</td>		
										<td align="right" class="bordereporte" nowrap style="font-weight:bold ">&nbsp;<%= (!nextplaca.equals(f.getPlaca()) || !(i+1<fp.size())) ? UtilFinanzas.customFormat("#,##0",totalPlaca,0) : "" %>&nbsp;&nbsp;</td>		
									    <td align="center"  nowrapclass="<%= (f.isMostrarDetalle()?"filaverde":"bordereporte") %>" ><input type="checkbox" name="dPen" value="<%= i %>" <%= (f.isMostrarDetalle()?"checked":"") %> onClick="this.form.submit(); //this.parentNode.className=(this.checked?'filaverde':'<%= es_lis_egr %>');  "></td>																																						
								 </tr>			
								 <% if (f.isMostrarDetalle() && f.getItems()!=null && !f.getItems().isEmpty()) { %>
								 
								 <tr>
								 	<td></td>
									<td colspan="7" class="bordereporte">
										<table width="100%">
											<tr class="tblTitulo2">
												<th width="6%" class="bordereporte">Item</th>
												<th width="33%" class="bordereporte">&nbsp;Descripcion</th>
												<th width="13%" class="bordereporte">Valor</th>
												<th width="10%" class="bordereporte">IVA</th>
												<th width="13%" class="bordereporte">RIVA</th>
												<th width="13%" class="bordereporte">RICA</th>
												<th width="12%" class="bordereporte">RFTE</th>
											</tr>	
											<% Vector itemFact = f.getItems();
											   double tvlr = 0, tiva = 0, triva = 0, trica = 0, trfte = 0;
											   for (int k=0; itemFact!=null && k<itemFact.size(); k++){
												   CXPItemDoc item = (CXPItemDoc) itemFact.get(k);
												   tvlr  += item.getVlr();
												   tiva  += item.getVlr_iva();
												   triva += item.getVlr_riva();
												   trica += item.getVlr_rica();
												   trfte += item.getVlr_rfte();												   
												   String es_item_fact = "fila2"; //(k%2==0?"fila2":"fila1");
											%>
											
											<tr class="<%= es_item_fact %>" style="font-size:11px ">
												<td width="6%" align="center" class="bordereporte"><%= item.getItem() %></td>
												<td width="33%" class="bordereporte" nowrap >&nbsp;<%= item.getDescripcion() %></td>
												<td width="13%" class="bordereporte" nowrap align="right"><%= UtilFinanzas.customFormat("#,##0",item.getVlr()     ,0) %>&nbsp;</td>
												<td width="10%" class="bordereporte" nowrap align="right"><%= UtilFinanzas.customFormat("#,##0",item.getVlr_iva() ,0) %>&nbsp;</td>
												<td width="13%" class="bordereporte" nowrap align="right"><%= UtilFinanzas.customFormat("#,##0",item.getVlr_riva(),0) %>&nbsp;</td>
												<td width="13%" class="bordereporte" nowrap align="right"><%= UtilFinanzas.customFormat("#,##0",item.getVlr_rica(),0) %>&nbsp;</td>
												<td width="12%" class="bordereporte" nowrap align="right"><%= UtilFinanzas.customFormat("#,##0",item.getVlr_rfte(),0) %>&nbsp;</td>
											</tr>
											<% } // end for%>												
											
											<% if(itemFact!=null && itemFact.size()>1) { %>
												<tr class="tblTitulo" style="font-size:11px ">
													<td colspan="2" class="bordereporte" align="right"><b>Subtotal </b>&nbsp;</td>
													<td class="bordereporte" align="right"><%= UtilFinanzas.customFormat("#,##0",tvlr  ,0) %>&nbsp;</td>
													<td class="bordereporte" align="right"><%= UtilFinanzas.customFormat("#,##0",tiva  ,0) %>&nbsp;</td>
													<td class="bordereporte" align="right"><%= UtilFinanzas.customFormat("#,##0",triva ,0) %>&nbsp;</td>
													<td class="bordereporte" align="right"><%= UtilFinanzas.customFormat("#,##0",trica ,0) %>&nbsp;</td>
													<td class="bordereporte" align="right"><%= UtilFinanzas.customFormat("#,##0",trfte ,0) %>&nbsp;</td>
												</tr>																	
											<% } %>																							
										</table>
									</td>	
									<td></td>
								 </tr>
								 <tr>
									<td colspan="8">&nbsp;</td>
								</tr>		
								 
								 <% } %>
								 

							 <%  }  %>	
							 
								 <tr  class="tblTitulo">
										<td colspan="7" align="right" class="bordereporte">Total </td>
										<td align="right" class="bordereporte"><%= UtilFinanzas.customFormat("#,##0",totalFactPendientes,0) %>&nbsp;</td>
								        <!--<td align="right" class="bordereporte">&nbsp;</td>
										<td align="right" class="bordereporte">&nbsp;</td>-->
								 </tr>					 
								<!-- <tr  class="transparente">
										<td colspan="10" align="right" class="bordereporte">
										<span class="msgLogin" >Para mostrar los detalles de los items seleccionados haga click sobre el boton Aceptar </span>
										</td>
						 		</tr>					-->								 
					</table>
				</td>	
			</tr>
	   </table>
      	<input type="hidden" name="fini" value="<%= periodoI %>">
		<input type="hidden" name="ffin" value="<%= periodoF %>">		
		<input type="hidden" name="show" value="<%= show %>">
	
	<img src = "<%=BASEURL%>/images/botones/aceptar.gif" style = "cursor:hand; display:none" name = "imgaceptar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="formularioFacturasPendientes.submit();">
    </form>
	 <br>
     <% } %>
	 




	<!-- seccion de planillas pendientes por cancelar -->
	 <% Vector liq = model.ConsultaExtractoSvc.getVectorPlanillasPendientes();
	 	if (liq!=null && !liq.isEmpty()) {
	 %>	 
	<form action="<%=CONTROLLER%>?estado=Consulta&accion=Extracto&opcion=ConsultaDetalleLiquidaciones"	 method="post" name="formularioLiquidacion">
	<table  width="900" border="2" align="center">
             <tr>
                  <td>  
                       <table width='100%' align='center' class='tablaInferior'>

                              <tr class="barratitulo">
                                    <td colspan='10' >
                                           <table cellpadding='0' cellspacing='0' width='100%'>
                                                 <tr class="subtitulo1">
                                                      <td align="LEFT" class="subtitulo1" colspan="2"  height="25">
													  &nbsp;PLANILLAS PENDIENTES POR CANCELAR</td>
                                             </tr>
                                      </table>
                                    </td>
                             </tr>
                             
                             <tr class="tblTitulo">                                   
									<td width='70' align="center" class="bordereporte" >Planilla</td>
									<td width='55' align="center" class="bordereporte">Placa</td>
									<td width='260' align="center" class="bordereporte">Ruta</td>                                    
                                    <td width='75' align="center" class="bordereporte">Estado</td>
                                    <td width='85' align="center" class="bordereporte">Fec Dsp.</td>									
									<td width="85" align="center" class="bordereporte">Fec Cum. </td>
                                    <td width='40' align="center" class="bordereporte" title="Agencia">Ag</td>
									<td width='90' align="center" class="bordereporte">Valor</td>
									<td width='90' align="center" class="bordereporte">Total</td>									
									<td width='40' align="center" class="bordereporte">Det</td>
                             </tr>	
							 <%  double totalPlanillas = 0; 		
							 	 String  prevplaca = ""; 	
								 double totalPlaca = 0;	
							 	 for (int i =0; i<liq.size(); i++){
									Planilla p = (Planilla) liq.get(i); 
									String es_pla = (i%2==0?"filagris":"filaazul"); 
									
									String nextplaca = p.getPlaveh();							
									if ( (i+1)< liq.size() )
										nextplaca = ((Planilla) liq.get(i+1)).getPlaveh();										
									if (!prevplaca.equals( p.getPlaveh() ) ) totalPlaca = 0;
									totalPlaca += p.getVlrPlanilla();
									prevplaca = p.getPlaveh();					
									
									totalPlanillas += p.getVlrPlanilla();				
							 %>
                             <tr class="<%= es_pla %>">                                   
									<td nowrap align="center" class="bordereporte" ><%= p.getNumpla() %></td>
									<td nowrap align="center" class="bordereporte"><%= p.getPlaveh() %></td>		
									<td nowrap align="left" class="bordereporte">&nbsp;<%= p.getRuta_pla()   %></td>                                    																
                                    <td nowrap align="center" class="bordereporte"><%= (p.getReg_status().equals("C")?"Cumplida":"Sin Cumplir") %></td>
                                    <td nowrap align="center" class="bordereporte"><%= (!p.getFecdsp().equals("")?p.getFecdsp():"&nbsp;") %></td>									
									<td nowrap align="center" class="bordereporte"><%= (!p.getFeccum().equals("")?p.getFeccum():"&nbsp;") %></td>
                                    <td nowrap align="center" class="bordereporte"><%= (!p.getAgcpla().equals("")?p.getAgcpla():"&nbsp;") %></td>
									<td nowrap align="right" class="bordereporte"><%= UtilFinanzas.customFormat("#,##0",p.getVlrPlanilla(),0) %>&nbsp;</td>
									<td nowrap align="right" class="bordereporte"><b><%= (!nextplaca.equals(p.getPlaveh()) || !(i+1<liq.size())) ? UtilFinanzas.customFormat("#,##0",totalPlaca,0) : "" %></b>&nbsp;</td>
									<td nowrap align="center" class="<%= (p.isMostrarDetalles()?"filaverde":"bordereporte")%>" ><input type="checkbox" name="dPla" value="<%= i %>" <%= (p.isMostrarDetalles()?"checked":"") %> onClick="this.form.submit(); //this.parentNode.className=(this.checked?'filaverde':'<%= es_pla %>');  "></td>																			
                             </tr>		
 
							 
							 <!-- detalle liquidacion -->
							 <% if (p.isMostrarDetalles()) {
							     Vector listItem = p.getRemesas();
							 %>
							 	<tr >
								<td></td>
								<td colspan="7" class="bordereporte">
									<table width="100%">
										<tr class="tblTitulo2">
											<th width="7%" class="bordereporte">Item</th>
											<th width="45%" class="bordereporte">Descripcion</th>
											<th width="16%" class="bordereporte">Valor</th>
											<th width="16%" class="bordereporte">RFTE</th>
											<th width="16%" class="bordereporte">RICA</th>
										</tr>
										<%  if (listItem!=null && !listItem.isEmpty()){			
												double tvlr = 0, tiva = 0, triva = 0, trica = 0, trfte = 0;
												for (int k=0; k<listItem.size(); k++){
													String  es_item_pla =  "fila2";  //(k%2==0?"fila2":"fila1"); 
													OPItems  item = (OPItems) listItem.get(k);
													tvlr  += item.getVlr();
													trfte += item.getVlrReteFuente();
													trica += item.getVlrReteIca();
										%>
										<tr class="<%= es_item_pla%>" style="font-size:11px ">
											<td class="bordereporte" align="center"><%= item.getItem() %></td>
											<td class="bordereporte" nowrap >&nbsp;<%= item.getDescripcion() %></td>
											<td class="bordereporte" nowrap align="right"><%= Utility.customFormat(item.getVlr() ) %>&nbsp;</td>
											<td class="bordereporte" nowrap align="right"><%= Utility.customFormat(item.getVlrReteFuente() ) %>&nbsp;</td>
											<td class="bordereporte" nowrap align="right"><%= Utility.customFormat(item.getVlrReteIca() ) %>&nbsp;</td>
										</tr>
										<% } //endfor %> 
										<% if (listItem.size() > 1) { %>
										<tr class="<%= es_pla%>" style="font-size:11px ">
											<td class="bordereporte" align="right" colspan="2"><b>Subtotal</b>&nbsp;</td>
											<td class="bordereporte" align="right"><%= Utility.customFormat(tvlr  ) %>&nbsp;</td>
											<td class="bordereporte" align="right"><%= Utility.customFormat(trfte ) %>&nbsp;</td>
											<td class="bordereporte" align="right"><%= Utility.customFormat(trica ) %>&nbsp;</td>
										</tr>										
										<% } // endif totales%>
										
										<% } //endif  %>
										
									</table>
								</td>								
								</tr>
								<tr><td colspan="9">&nbsp;</td></tr>																 
							 <% } %>
							 <!-- fin detalle liquidacion -->
							 							
							 <%  }  %>	
							 
								 <tr  class="tblTitulo">
										<td colspan="8" align="right" class="bordereporte">Total &nbsp;</td>
										<td align="right" class="bordereporte" nowrap><%= UtilFinanzas.customFormat("#,##0",totalPlanillas,0) %>&nbsp;</td>
										<td align="right" class="bordereporte">&nbsp;</td>
								 </tr>					  
					</table>
				</td>	
			</tr>
    </table>
       	<input type="hidden" name="fini" value="<%= periodoI %>">
		<input type="hidden" name="ffin" value="<%= periodoF %>">		
		<input type="hidden" name="show" value="<%= show %>">
	
	<img src = "<%=BASEURL%>/images/botones/aceptar.gif" style = "cursor:hand; display:none" name = "imgaceptar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="formularioLiquidacion.submit();">
	</form>	 	
	<br>
	 <% } %>
	
     <img src = "<%=BASEURL%>/images/botones/regresar.gif" style = "cursor:hand" name = "imgregresar" onClick = "document.location='<%= BASEURL %>/jsp/cxpagar/consultaExtractos/consulta.jsp?show=<%= show %>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">		 	<img src = "<%=BASEURL%>/images/botones/imprimir.gif" style = "cursor:hand" name = "imgregresar" onClick = "window.open('<%= BASEURL %>/jsp/cxpagar/consultaExtractos/versionImprimible.jsp?fini=<%= periodoI %>&ffin=<%= periodoF %>','','menubar=no, scrollbars=yes, status=yes')" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">			
            <img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> </p>
	 
	 
</center>
</div>
<%=datos[1]%> 
<iframe width=188 height=166 name="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins_24.js" id="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins_24.js" src="<%=BASEURL%>/js/calendartsp/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px; width: 1px;">
</iframe>
</body>	
</html>
<script>
  function validarListadoEgreso(){
  	/*var egresos = formulario.dEgr;
  	for (i=0; i<egresos.length; i++){
		if (egresos[i].checked){
			return true;
		}
	}
	
	alert('Seleccione por lo menos un egreso para consultar su detalle.');
	return false;*/
	return true;
	
  }
  
  window.focus();
</script>