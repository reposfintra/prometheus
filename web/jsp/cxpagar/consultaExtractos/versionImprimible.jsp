<!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      21/02/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description:  Version Imprimible del extracto
 --%>

<%@page    session   ="true"%> 
<%@page    errorPage ="/error/ErrorPage.jsp"%>
<%@page    import    ="com.tsp.operation.model.beans.*"%>
<%@include file      ="/WEB-INF/InitModel.jsp"%>
<%@page    import    ="com.tsp.util.*"%>
<%@page    import    ="java.util.*" %>
<%  
	String path     = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[]  = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);   
	String periodoI = (String) request.getParameter("fini");
	String periodoF = (String) request.getParameter("ffin");
	Proveedor pro = model.ConsultaExtractoSvc.getProveedor();
%>
<html>
<head>

    <title>.:: Impresion Extracto</title>
    <link href="<%= BASEURL %>/css/estiloimpresionextracto.css" rel="stylesheet" type="text/css"> 
    <link href="../../../css/estiloimpresionextracto.css"  rel="stylesheet" type="text/css"> 
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
</head>
<body>
<center>

	<table border="0" width="600">
	<tr class="tblTituloSinFondo">
		<td align="left"><%=  (pro!=null? "[" + pro.getC_nit() + "] " + pro.getC_payment_name():"") %></td>
		<td align="right">EXTRACTO <%= Util.getFechaActual_String(6) %></td>
	</tr>
	<tr><td colspan="2"><hr width="100%" color="#000066"></td></tr>
	</table>
	



<% Vector v = model.ConsultaExtractoSvc.getVectorEgresos();
   if (v!=null && !v.isEmpty()) {
%>
  <div id="pagos" style="">
     <form action='<%=CONTROLLER%>?estado=Consulta&accion=Extracto&opcion=ConsultaDetalleEgresos' method='post' name='formulario'>
		 
         <table  width="600" border="0" align="center" cellpadding="0" cellspacing="0" >
             <tr>
                  <td>  
                       <table width='100%' align='center' >

                              <tr>
                                    <td colspan='10'  >
                                           <table cellpadding='0' cellspacing='0' width='100%'>
                                                 <tr >
                                                      <td width="20%" height="40" align="center" ><img src="../../../images/topTSP.gif"> </td>
                                                      <td width="3%" align="center" class="subtitulo1">&nbsp;</td>
                                                      <td width="77%" align="left" class="subtitulo1">PAGOS REALIZADOS <BR> PERIODO &nbsp;  <%= periodoI + " AL " + periodoF %></td>
                                             </tr>
                                      </table>
                                    </td>
                             </tr>
                             
                             <tr class="tblTitulo">
									<td width='89' align="center" class="bordereporte">Banco</td>
                                    <td width='220' align="center" class="bordereporte">Sucursal</td>
									<td width="98" align="center" class="bordereporte">Cheque</td>
                                    <td width='98' align="center" class="bordereporte">Fecha</td>
                                    <td width='154' align="center" class="bordereporte">Estado</td>                                    
									<td width='81' align="center" class="bordereporte">Moneda</td>
									<td width='128' align="center" class="bordereporte">Valor</td>
                             </tr>	
                           							 
							 <% double totalEgreso = 0; 
							 	 for (int i =0; i<v.size(); i++){
								 	Egreso e = (Egreso) v.get(i); 
									totalEgreso += e.getVlr();
									String es_lis_egr = (i%2==0?"filagris":"filaazul"); 
									String rowspan = (e.isMostrarDetalleEgreso()?" rowspan=" + (e.getDetalle().size() + 2)+ " ":"");
									
							 %>

								 <tr  class="<%= es_lis_egr %>" >
								   		<td width='89' align="left" class="bordereporte" >&nbsp;<%= e.getBranch_code() %></td>
										<td width='220' align="left" class="bordereporte" >&nbsp;<%= e.getBank_account_no() %></td>
										<td width="98"  align="left" class="bordereporte" nowrap>&nbsp;<%= e.getDocument_no() + (e.getConcept_code().equals("TR")?" <SUP><B>TRANS</B></SUP>":"") %></td>
										<td width='98' align="center" class="bordereporte">&nbsp;<%= e.getPrinter_date() %></td>
								        <td width='154' align="center" class="bordereporte">&nbsp;<%= (e.getFechaEnvio().equals("0099-01-01 00:00:00")?"":"ENTREGADO") %></td>								        
 							            <td width='81' align="center" class="bordereporte">&nbsp;<%= e.getCurrency() %></td>
										<td width='128' align="right" class="bordereporte">&nbsp;<%= UtilFinanzas.customFormat("#,##0",e.getVlr(),0) %>&nbsp;</td>
								 </tr>						
								 <% Vector detalle = e.getDetalle();
								 	if (e.isMostrarDetalleEgreso() && detalle!=null && !detalle.isEmpty()) { %>
								 <tr>
								 	<td></td>
									<td colspan="6" class="bordereporte">
										<table width="100%">
											<tr class="tblTitulo2" >
												<td width="9%" align="center" class="bordereporte" >Item</td>
												<td width="67%" align="center" class="bordereporte" >Descripcion</td>
												<td width="18%" align="center" class="bordereporte" >Valor</td>
											</tr>
											
											<!-- seccion item detalle egreso -->
											<% 
												 for (int j=0; j<detalle.size(); j++) {
													Egreso d = (Egreso) detalle.get(j);	 
													String es_detalle = "fila2"; //(j%2==0?"fila2":"fila1"); 
											%>
											<tr class="<%= es_detalle %>" >
												<td align="center" class="bordereporte"><%= d.getItem_no() %></td>																	
												<td class="bordereporte">&nbsp;<%= d.getDescripcion() %></td>																	
												<td align="right" class="bordereporte"><%= UtilFinanzas.customFormat("#,##0",d.getVlr(),0) %>&nbsp;</td>		
											</tr> 
											
											
											<!-- seccion de la factura del detalle del  cheque -->
											<%  
												CXP_Doc factura   = d.getFactura();
												Planilla planilla = d.getPlanilla();
												if (d.isMostrarDetalleEgreso() && (d.getConcept_code().equals("FAC") || d.getConcept_code().equals("TR")) && factura!=null){ %>	
											<tr>
												<td></td>
												<td colspan="3">
													<table width="100%">
														<tr class="fila2">
															<td width="17%" class="bordereporte">&nbsp;<b>Factura</b> </td>
															<td class="bordereporte" colspan="3">&nbsp;<%= factura.getDocumento() %> </td>
															<td width="10%" class="bordereporte">&nbsp;<b>Fecha</b></td>
															<td width="14%" class="bordereporte" align="center"><%= factura.getFecha_documento() %></td>				
															<td width="10%" class="bordereporte">&nbsp;<b>Agencia</b></td>
															<td width="12%" class="bordereporte" align="center"><%= factura.getAgencia()     %></td>																											
														</tr>
														<tr class="fila2">
															<td class="bordereporte" >&nbsp;<b>Descripcion</b></td>
															<td colspan="7" class="bordereporte" >&nbsp;<%= factura.getDescripcion() %></td>																																												
														</tr>
														
														
														<!-- detalle de la factura del item del cheque -->
														<tr >
															<td colspan="8" >
																<table width="100%">
																	<tr class="tblTitulo3">
																		<th width="4%" class="bordereporte">Item</th>
																		<th width="36%" class="bordereporte">&nbsp;Descripcion</th>
																		<th width="12%" class="bordereporte">Valor</th>
																		<th width="12%" class="bordereporte">IVA</th>
																		<th width="12%" class="bordereporte">RIVA</th>
																		<th width="12%" class="bordereporte">RICA</th>
																		<th width="12%" class="bordereporte">RFTE</th>
																	</tr>	
																	<% Vector itemFact = factura.getItems();
																	   double tvlr = 0, tiva = 0, triva = 0, trica = 0, trfte = 0;																	   
																	   double aj_vlr = 0, aj_iva = 0, aj_riva = 0, aj_rica = 0, aj_rfte = 0;
																	   for (int k=0; itemFact!=null && k<itemFact.size(); k++){
																		   CXPItemDoc item = (CXPItemDoc) itemFact.get(k);
																		   if (item.getConcepto().equals("09")){
																			   aj_vlr  += item.getVlr();
																			   aj_iva  += item.getVlr_iva();
																			   aj_riva += item.getVlr_riva();
																			   aj_rica += item.getVlr_rica();
																			   aj_rfte += item.getVlr_rfte();	
																		   }
																	   }
																		   
																	   
																	   boolean sumar = true;																	   
																	   for (int k=0; itemFact!=null && k<itemFact.size(); k++){
																	   	   CXPItemDoc item = (CXPItemDoc) itemFact.get(k);
  																		   tvlr  += item.getVlr();
																		   tiva  += item.getVlr_iva();
																		   triva += item.getVlr_riva();
																		   trica += item.getVlr_rica();
																		   trfte += item.getVlr_rfte();																		   
																		   String es_item_fact = "fila1"; //(k%2==0?"fila2":"fila1");
																		   
																		   double saj_vlr = 0, saj_iva = 0, saj_riva = 0, saj_rica = 0, saj_rfte = 0;	
																		   if (item.getConcepto().equals("01") && sumar) {	
																				saj_vlr  += aj_vlr ;
																				saj_iva  += aj_iva ;
																				saj_riva += aj_riva;										
																				saj_rica += aj_rica;
																				saj_rfte += aj_rfte; 
																				sumar = !sumar;
																		   }																			   
																	%>
																	
																	<% if (!item.getConcepto().equals("09")) { %>
																	<tr class="<%= es_item_fact %>">
																		<td align="center" class="bordereporte"><%= item.getItem() %></td>
																		<td class="bordereporte" nowrap>&nbsp;<%= item.getDescripcion() %></td>
																		<td class="bordereporte" nowrap align="right"><%= UtilFinanzas.customFormat("#,##0",(item.getVlr()     + saj_vlr   ),0) %>&nbsp;</td>
																		<td class="bordereporte" nowrap align="right"><%= UtilFinanzas.customFormat("#,##0",(item.getVlr_iva() + saj_iva   ),0) %>&nbsp;</td>
																		<td class="bordereporte" nowrap align="right"><%= UtilFinanzas.customFormat("#,##0",(item.getVlr_riva()+ saj_riva  ),0) %>&nbsp;</td>
																		<td class="bordereporte" nowrap align="right"><%= UtilFinanzas.customFormat("#,##0",(item.getVlr_rica()+ saj_rica  ),0) %>&nbsp;</td>
																		<td class="bordereporte" nowrap align="right"><%= UtilFinanzas.customFormat("#,##0",(item.getVlr_rfte()+ saj_rfte  ),0) %>&nbsp;</td>
																	</tr>
																	<% } // end if %>
																	<% } // end for %>									
																	
																	<% if(itemFact!=null && itemFact.size()>1) { %>
																		<tr class="<%= es_lis_egr %>">
																			<td colspan="2" class="bordereporte" align="right"><b>Subtotal </b>&nbsp;</td>
																			<td class="bordereporte" align="right"><%= UtilFinanzas.customFormat("#,##0",tvlr  ,0) %>&nbsp;</td>
																			<td class="bordereporte" align="right"><%= UtilFinanzas.customFormat("#,##0",tiva  ,0) %>&nbsp;</td>
																			<td class="bordereporte" align="right"><%= UtilFinanzas.customFormat("#,##0",triva ,0) %>&nbsp;</td>
																			<td class="bordereporte" align="right"><%= UtilFinanzas.customFormat("#,##0",trica ,0) %>&nbsp;</td>
																			<td class="bordereporte" align="right"><%= UtilFinanzas.customFormat("#,##0",trfte ,0) %>&nbsp;</td>
																		</tr>																	
																	<% } %>
																</table>
															</td>	
														</tr>
														<!-- fin detalle de la factura del item del cheque -->
														
													</table>
												</td>											
											</tr>
											<!-- fin seccion de la factura del detalle del  cheque -->
											
											
											<!-- fin seccion de la planilla del detalle del  cheque -->											
											<% } else if (d.isMostrarDetalleEgreso() && !d.getTipo_documento().equals("FAC") && planilla!=null) { %>	
											<tr>
												<td></td>
												<td colspan="3">
													<table width="100%">
														<tr class="tblTitulo3">
															<th width="10%" class="bordereporte">Placa</th>
															<th width="50%" class="bordereporte">Ruta</th>
															<th width="13%" class="bordereporte">Fecha</th>
															<th width="17%" class="bordereporte">Estado</th>
															<th width="10%" class="bordereporte">Agencia</th>
														</tr>														
														<tr class="fila1">
															<td class="bordereporte" align="center"><%= planilla.getPlaveh()%> </td>
															<td class="bordereporte">&nbsp;<%= planilla.getRuta_pla() + (planilla.getRemision().equals("")?"":", "+planilla.getRemision())  + ", " + Util.customFormat(planilla.getVlrParcial()) + " " + planilla.getUnidcam() %></td>
															<td class="bordereporte" align="center"><%= planilla.getFecdsp() %></td>
															<td class="bordereporte" align="center"><%= (planilla.getReg_status().equals("C")?"Cumplida":"Sin Cumplir") %></td>				
															<td class="bordereporte" align="center"><%= planilla.getAgcpla()     %></td>																											
														</tr>
													</table>
												</td>
											</tr>			
											<% } %>
											
											
											
											<%  } %>
											<!-- fin seccion item detalle egreso -->
										</table>																						
										 								
									</td>
									<td></td>
								 </tr>
								 <tr><td colspan="10">&nbsp;</td></tr>		
								 
								 <% } // fin mostrar egreso %>
								 
							 <%  }  // end for listado egresos %>	
							 
								 <tr  class="tblTitulo">
										<td colspan="6" align="right" class="bordereporte">Total&nbsp;</td>
										<td align="right" class="bordereporte"><%= UtilFinanzas.customFormat("#,##0",totalEgreso,0) %>&nbsp;</td>
								 </tr>					 
								 <!--<tr  class="transparente">
										<td colspan="10" align="right" class="bordereporte">
										<span class="msgLogin" >Para mostrar los detalles de los items seleccionados haga click sobre el boton Aceptar </span>
										</td>
						 		</tr>-->													 
					</table>
				</td>	
			</tr>
		</table>
	 	<img src = "<%=BASEURL%>/images/botones/aceptar.gif" style = "cursor:hand; display:none" name = "imgaceptar" onClick = "formulario.submit(); " onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">       		
       	<input type="hidden" name="fini" value="<%= periodoI %>">
		<input type="hidden" name="ffin" value="<%= periodoF %>">		
     </form>
</div>	 
	 <br>
<% } %>	 
	 
	 
	 
	 
	 
	 <!-- SECCION DE FACTURAS PENDIENTES DE PROPIEARIOS -->
	 
	 <% Vector fp = model.ConsultaExtractoSvc.getVectorFacturasPendientes();
	    if (fp!=null && !fp.isEmpty()) { %>
     <form action='<%=CONTROLLER%>?estado=Consulta&accion=Extracto&opcion=ConsultaDetalleFacturasPendientes' method='post' name='formularioFacturasPendientes'>
		 
         <table  width="600" border="0" align="center">
             <tr>
                  <td>               
				  <table width='100%' align='center' class='tablaInferior'>
                    <tr >
                      <td colspan='8' >
                        <table cellpadding='0' cellspacing='0' width='100%'>
                          <tr >
                            <td width="20%"  height="40" align="center" > &nbsp;<img src="../../../images/topTSP.gif"></td>
                            <td width="3%" align="LEFT" class="subtitulo1">&nbsp;</td>
                            <td width="77%"  height="25" align="LEFT" class="subtitulo1">FACTURAS PENDIENTES POR CANCELAR</td>
                          </tr>
                      </table></td>
                    </tr>
                    <tr class="tblTitulo">
                      <td width='30' align="center" class="bordereporte" >Tipo</td>
                      <td width='90' align="center" class="bordereporte">Documento</td>
					  <td width='40' align="center" class="bordereporte">Placa</td>
                      <td width='220' align="center" class="bordereporte">Descripcion</td>                      
                      <td width="50" align="center" class="bordereporte">Fecha</td>
                      <td width='30' align="center" class="bordereporte">AG</td>
                      <td width='70' align="center" class="bordereporte">Saldo </td>
                      <td width='70' align="center" class="bordereporte">Total</td>
                    </tr>
                    <%  double totalFactPendientes = 0; 	
					    double totalPlaca = 0;				
						String  prevplaca = "";
					    for (int i =0; i<fp.size(); i++){
							CXP_Doc f = (CXP_Doc) fp.get(i); 
							totalFactPendientes += f.getVlr_saldo();
							String es_lis_egr = (i%2==0?"filagris":"filaazul"); 
							
							String nextplaca = f.getPlaca();							
							if ( (i+1)< fp.size() )
								nextplaca = ((CXP_Doc) fp.get(i+1)).getPlaca();										
							if (!prevplaca.equals( f.getPlaca() ) ) totalPlaca = 0;
							totalPlaca += f.getVlr_saldo();
							prevplaca = f.getPlaca();									
					 %>
                    <tr  class="<%= es_lis_egr %>" >
                      <td align="center" class="bordereporte"  nowrap>&nbsp;<%= (f.getClase_documento().equals("0")?"Planilla":"Factura") %></td>
                      <td align="left" class="bordereporte"  nowrap>&nbsp;<%= (f.getClase_documento().equals("0")?f.getPlanilla():f.getDocumento()) %></td>
                      <td align="center" class="bordereporte"  nowrap>&nbsp;<%= f.getPlaca() %></td>					  
                      <td align="left" class="bordereporte" nowrap>&nbsp;<%= (f.getClase_documento().equals("0")?f.getDocumento() + " " + f.getObservacion():f.getDescripcion()) %></td>
                      <td align="center" class="bordereporte" nowrap>&nbsp;<%= (f.getClase_documento().equals("0")?f.getFecha_planilla():f.getFecha_documento()) %></td>
                      <td align="center" class="bordereporte" nowrap>&nbsp;<%= f.getAgencia() %></td>
                      <td align="right" class="bordereporte" nowrap>&nbsp;<%= UtilFinanzas.customFormat("#,##0",f.getVlr_saldo(),0) %>&nbsp;</td>
					  <% if ((!nextplaca.equals(f.getPlaca()) || !(i+1<fp.size()))) { %>
					  <td align="right" class="bordereporte" nowrap style="font-weight:bold ">&nbsp;<%= UtilFinanzas.customFormat("#,##0",totalPlaca,0)  %>&nbsp;</td>		
					  <% } %>
                    </tr>
                    <% if (f.isMostrarDetalle() && f.getItems()!=null && !f.getItems().isEmpty()) { %>
                    <tr>
                      <td></td>
                      <td colspan="6" class="bordereporte">
                        <table width="100%">
                          <tr class="tblTitulo2">
                            <th width="5%" class="bordereporte">Item</th>
                            <th width="40%" class="bordereporte">&nbsp;Descripcion</th>
                            <th width="11%" class="bordereporte">Valor</th>
                            <th width="11%" class="bordereporte">IVA</th>
                            <th width="11%" class="bordereporte">RIVA</th>
                            <th width="11%" class="bordereporte">RICA</th>
                            <th width="11%" class="bordereporte">RFTE</th>
                          </tr>
                          <% Vector itemFact = f.getItems();
							   double tvlr = 0, tiva = 0, triva = 0, trica = 0, trfte = 0;
							   double aj_vlr = 0, aj_iva = 0, aj_riva = 0, aj_rica = 0, aj_rfte = 0;
							   for (int k=0; k<itemFact.size(); k++){
								   CXPItemDoc item = (CXPItemDoc) itemFact.get(k);
								   if (item.getConcepto().equals("09")){
									   aj_vlr  += item.getVlr();
									   aj_iva  += item.getVlr_iva();
									   aj_riva += item.getVlr_riva();
									   aj_rica += item.getVlr_rica();
									   aj_rfte += item.getVlr_rfte();	
								   }
							   }
								   
							   
							   boolean sumar = true;
							   for (int k=0; k<itemFact.size(); k++){
								   CXPItemDoc item = (CXPItemDoc) itemFact.get(k);
								   tvlr  += item.getVlr();
								   tiva  += item.getVlr_iva();
								   triva += item.getVlr_riva();
								   trica += item.getVlr_rica();
								   trfte += item.getVlr_rfte();											   
								   String es_item_fact = "fila2"; //(k%2==0?"fila2":"fila1");
								   
								   
								   double saj_vlr = 0, saj_iva = 0, saj_riva = 0, saj_rica = 0, saj_rfte = 0;	
								   if (item.getConcepto().equals("01") && sumar) {	
										saj_vlr  += aj_vlr ;
										saj_iva  += aj_iva ;
										saj_riva += aj_riva;										
										saj_rica += aj_rica;
										saj_rfte += aj_rfte; 
										sumar = !sumar;
								   }								   
							%>
							<% if (!item.getConcepto().equals("09")) { %>
								  <tr class="<%= es_item_fact %>">
									<td align="center" class="bordereporte"><%= item.getItem() %></td>
									<td class="bordereporte" nowrap>&nbsp;<%= item.getDescripcion() %></td>
									<td class="bordereporte" nowrap align="right"><%= UtilFinanzas.customFormat("#,##0",(item.getVlr()     + saj_vlr   ),0) %>&nbsp;</td>
									<td class="bordereporte" nowrap align="right"><%= UtilFinanzas.customFormat("#,##0",(item.getVlr_iva() + saj_iva   ),0) %>&nbsp;</td>
									<td class="bordereporte" nowrap align="right"><%= UtilFinanzas.customFormat("#,##0",(item.getVlr_riva()+ saj_riva  ),0) %>&nbsp;</td>
									<td class="bordereporte" nowrap align="right"><%= UtilFinanzas.customFormat("#,##0",(item.getVlr_rica()+ saj_rica  ),0) %>&nbsp;</td>
									<td class="bordereporte" nowrap align="right"><%= UtilFinanzas.customFormat("#,##0",(item.getVlr_rfte()+ saj_rfte  ),0) %>&nbsp;</td>
								  </tr>
						  <% } // end if %>	
						  <% } // end for%>										  
                          <% if(itemFact!=null && itemFact.size()>1) { %>
                          <tr class="<%= es_lis_egr %>">
                            <td colspan="2" class="bordereporte" align="right"><b>Subtotal </b>&nbsp;</td>
                            <td class="bordereporte" align="right"><%= UtilFinanzas.customFormat("#,##0",tvlr  ,0) %>&nbsp;</td>
                            <td class="bordereporte" align="right"><%= UtilFinanzas.customFormat("#,##0",tiva  ,0) %>&nbsp;</td>
                            <td class="bordereporte" align="right"><%= UtilFinanzas.customFormat("#,##0",triva ,0) %>&nbsp;</td>
                            <td class="bordereporte" align="right"><%= UtilFinanzas.customFormat("#,##0",trica ,0) %>&nbsp;</td>
                            <td class="bordereporte" align="right"><%= UtilFinanzas.customFormat("#,##0",trfte ,0) %>&nbsp;</td>
                          </tr>
                          <% } %>
                      </table></td>
                    </tr>
                    <tr>
                      <td colspan="8">&nbsp;</td>
                    </tr>
                    <% } %>
                    <%  }  %>
                    <tr  class="tblTitulo">
                      <td colspan="7" align="right" class="bordereporte">Total &nbsp;</td>
                      <td align="right" class="bordereporte"><%= UtilFinanzas.customFormat("#,##0",totalFactPendientes,0) %>&nbsp;</td>
                    </tr>
                  </table></td>	
			</tr>
	   </table>
      	<input type="hidden" name="fini" value="<%= periodoI %>">
		<input type="hidden" name="ffin" value="<%= periodoF %>">		
	
	<img src = "<%=BASEURL%>/images/botones/aceptar.gif" style = "cursor:hand; display:none" name = "imgaceptar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="formularioFacturasPendientes.submit();">
    </form>
	 <br>
     <% } %>
	 




	<!-- seccion de planillas pendientes por cancelar -->
	 <% Vector liq = model.ConsultaExtractoSvc.getVectorPlanillasPendientes();
	 	if (liq!=null && !liq.isEmpty()) {
	 %>	 
	<form action="<%=CONTROLLER%>?estado=Consulta&accion=Extracto&opcion=ConsultaDetalleLiquidaciones"	 method="post" name="formularioLiquidacion">
	<table  width="600" border="0" align="center" cellpadding="1">
             <tr>
                  <td>  
                       <table width='100%' align='center' class='tablaInferior'>

                              <tr >
                                    <td colspan='10' >
                                           <table cellpadding='0' cellspacing='0' width='100%'>
                                                 <tr>
                                                      <td width="18%"  height="40" align="center" ><img src="../../../images/topTSP.gif"></td>
                                                      <td width="4%" align="LEFT" class="subtitulo1">&nbsp;</td>
                                                      <td width="78%"   align="LEFT" class="subtitulo1">PLANILLAS PENDIENTES POR CANCELAR</td>
                                             </tr>
                                      </table>
                                    </td>
                             </tr>
                             
                             <tr class="tblTitulo">                                   
									<td width='50' align="center" class="bordereporte">Planilla</td>
									<td width='40' align="center" class="bordereporte">Placa</td>
									<td width='200' align="center" class="bordereporte">Ruta</td>                                    
                                    <td width='60' align="center" class="bordereporte">Estado</td>
                                    <td width='50' align="center" class="bordereporte">Fec Dsp.</td>									
									<td width="50" align="center" class="bordereporte">Fec Cum. </td>
                                    <td width='30' align="center" class="bordereporte">Ag</td>
									<td width='60' align="center" class="bordereporte">Valor</td>
									<td width='60' align="center" class="bordereporte">Total</td>
                             </tr>	
							 <%  double totalPlanillas = 0; 			
  							     String  prevplaca = ""; 	
								 double totalPlaca = 0;					 	
							 	 for (int i =0; i<liq.size(); i++){
									Planilla p = (Planilla) liq.get(i);									
									String es_pla = (i%2==0?"filagris":"filaazul"); 
									String nextplaca = p.getPlaveh();							
									if ( (i+1)< liq.size() )
										nextplaca = ((Planilla) liq.get(i+1)).getPlaveh();										
									if (!prevplaca.equals( p.getPlaveh() ) ) totalPlaca = 0;
									totalPlaca += p.getVlrPlanilla();
									prevplaca = p.getPlaveh();		
									
									totalPlanillas += p.getVlrPlanilla();
							 %>
                             <tr class="<%= es_pla %>">                                   
									<td nowrap align="center" class="bordereporte" ><%= p.getNumpla() %></td>
                                    <td nowrap align="center" class="bordereporte"><%= p.getPlaveh() %></td>									
									<td nowrap align="left" class="bordereporte">&nbsp;<%= p.getRuta_pla()   %></td>
                                    <td nowrap align="center" class="bordereporte"><%= (p.getReg_status().equals("C")?"Cumplida":"Sin Cumplir") %></td>
                                    <td nowrap align="center" class="bordereporte"><%= (!p.getFecdsp().equals("")?p.getFecdsp():"&nbsp;") %></td>									
									<td nowrap align="center" class="bordereporte"><%= (!p.getFeccum().equals("")?p.getFeccum():"&nbsp;") %></td>
                                    <td nowrap align="center" class="bordereporte"><%= (!p.getAgcpla().equals("")?p.getAgcpla():"&nbsp;") %></td>
									<td nowrap align="right" class="bordereporte"><%= UtilFinanzas.customFormat("#,##0",p.getVlrPlanilla(),0) %>&nbsp;</td>
									<% if ((!nextplaca.equals(p.getPlaveh()) || !(i+1<liq.size()))) { %>
									<td nowrap align="right" class="bordereporte"><b><%= UtilFinanzas.customFormat("#,##0",totalPlaca,0)  %></b>&nbsp;</td>
									<% } %>
                             </tr>		
							 
							
							 <!-- detalle liquidacion -->
							 <% if (p.isMostrarDetalles()) {
							     List listItem = p.getRemesas();
							 %>
							 	<tr >
								<td></td>
								<td colspan="7" class="bordereporte">
									<table width="100%">
										<tr class="tblTitulo2">
											<th width="10%" class="bordereporte">Item</th>
											<th width="45%" class="bordereporte">Descripcion</th>
											<th width="15%" class="bordereporte">Valor</th>
											<th width="15%" class="bordereporte">RFTE</th>
											<th width="15%" class="bordereporte">RICA</th>
										</tr>
										<%  if (listItem!=null && !listItem.isEmpty()){			
												double tvlr = 0, tiva = 0, triva = 0, trica = 0, trfte = 0;
												double aj_vlr = 0, aj_iva = 0, aj_triva = 0, aj_rica = 0, aj_rfte = 0;													
												for (int k=0; k<listItem.size(); k++){
													OPItems  item = (OPItems)listItem.get(k);
													if (item.getConcepto().equals("09") ){
														   aj_vlr  += item.getVlr();
														   aj_rica += item.getVlrReteIca();
														   aj_rfte += item.getVlrReteFuente();														
													}													
												}
												
												boolean sumar = true;
												for (int k=0; k<listItem.size(); k++){
													String  es_item_pla =  "fila2";  //(k%2==0?"fila2":"fila1"); 
													OPItems  item = (OPItems)listItem.get(k);
													
													double saj_vlr = 0, saj_iva = 0, saj_triva = 0, saj_rica = 0, saj_rfte = 0;	
													if (item.getConcepto().equals("01") && sumar) {	
														saj_vlr  += aj_vlr;
														saj_rica += aj_rica;
														saj_rfte += aj_rfte; 
														sumar = !sumar;
													}

													
													tvlr  += item.getVlr();
													trfte += item.getVlrReteFuente();
													trica += item.getVlrReteIca();													
										%>
										
										<% if (!item.getConcepto().equals("09")) { %>
										<tr class="<%= es_item_pla%>">
											<td class="bordereporte" align="center"><%= item.getItem() %></td>
											<td class="bordereporte">&nbsp;<%= item.getDescripcion() %></td>
											<td class="bordereporte" align="right"><%= Utility.customFormat(item.getVlr()           + saj_vlr  ) %>&nbsp;</td>
											<td class="bordereporte" align="right"><%= Utility.customFormat(item.getVlrReteFuente() + saj_rfte ) %>&nbsp;</td>
											<td class="bordereporte" align="right"><%= Utility.customFormat(item.getVlrReteIca()    + saj_rica ) %>&nbsp;</td>
										</tr>
										<% } // end if exclusion de ajustes %>
										<% } // end for %>
										
										<% if (listItem.size() > 1) { %>
										<tr class="<%= es_pla%>">
											<td class="bordereporte" align="right" colspan="2"><b>Subtotal</b>&nbsp;</td>
											<td class="bordereporte" align="right"><%= Utility.customFormat(tvlr  ) %>&nbsp;</td>
											<td class="bordereporte" align="right"><%= Utility.customFormat(trfte ) %>&nbsp;</td>
											<td class="bordereporte" align="right"><%= Utility.customFormat(trica ) %>&nbsp;</td>
										</tr>										
										<% } %>
										
										<% } // end if%>
										
									</table>
								</td>								
								</tr>
								<tr><td colspan="8">&nbsp;</td></tr>
							 <% } %>
							 <!-- fin detalle liquidacion -->
							 							
							 <%  }  %>	
							 
								 <tr  class="tblTitulo">
										<td colspan="8" align="right" class="bordereporte">Total  &nbsp;</td>
										<td align="right" class="bordereporte"><%= UtilFinanzas.customFormat("#,##0",totalPlanillas,0) %>&nbsp;</td>
								 </tr>					  
					</table>
				</td>	
			</tr>
    </table>
       	<input type="hidden" name="fini" value="<%= periodoI %>">
		<input type="hidden" name="ffin" value="<%= periodoF %>">		
	
	<img src = "<%=BASEURL%>/images/botones/aceptar.gif" style = "cursor:hand; display:none" name = "imgaceptar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="formularioLiquidacion.submit();">
	</form>	 	
	<br>
	 <% } %>
	 
</center>
</div>
</body>	
</html>
<script>
  window.print();
  parent.close();
</script>