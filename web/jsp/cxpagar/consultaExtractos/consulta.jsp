<!--
     - Author(s)       :      MARIO FONTALVO
     - Date            :      21/02/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)
     - Description:  Vista que permite capturar tipo de filtros para consultar los extractos
 --%>

<%@page    session   ="true"%>
<%@page    errorPage ="/error/ErrorPage.jsp"%>
<%@page    import    ="com.tsp.operation.model.beans.*,com.tsp.util.*" %>
<%@include file      ="/WEB-INF/InitModel.jsp"%>
<%@page    import    ="com.tsp.util.*"%>
<%@page    import    ="java.util.*" %>
<%
	String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
	Usuario user   = (Usuario) session.getAttribute("Usuario");
	String mensaje = (String) request.getAttribute("mensaje");

	String periodoInicial = (String) request.getParameter("fini");
	String periodoFinal   = (String) request.getParameter("ffin");
	String show           = Util.coalesce((String) request.getParameter("show"),"");

    periodoInicial = Util.coalesce(periodoInicial, Util.getFechaActual_String(1)+ "-" + Util.getFechaActual_String(3) + "-01");
	periodoFinal   = com.tsp.util.Util.coalesce(periodoFinal, Util.getFechaActual_String(4));


	Proveedor p = model.ConsultaExtractoSvc.getProveedor();
	String msg = (String) request.getAttribute("msg");

%>
<html>
<head>

    <title>Consulta de Extracto</title>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script>
<script language="JavaScript1.2">
<!--
   // Maximizar Ventana por Nick Lowe (nicklowe@ukonline.co.uk)

   window.moveTo(0,0);
   if (document.all) {
  	 top.window.resizeTo(screen.availWidth,screen.availHeight);
   }
   else if (document.layers||document.getElementById) {
	   if(top.window.outerHeight<screen.availHeight||top.window.outerWidth<screen.availWidth){
	   top.window.outerHeight = screen.availHeight;
	   top.window.outerWidth = screen.availWidth;
	   }
   }
//-->
</script>
</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Consulta de Extractos."/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<center>
     <form action='<%=CONTROLLER%>?estado=Consulta&accion=Extracto&opcion=consulta' method='post' name='formulario'>
         <table  width="450" border="2" align="center">
             <tr>
                  <td>
                       <table width='100%' align='center' class='tablaInferior'>

                              <tr class="barratitulo">
                                    <td colspan='3' >
                                           <table cellpadding='0' cellspacing='0' width='100%'>
                                                 <tr class="">
                                                      <td align="left" width='55%' class="subtitulo1">&nbsp;Consulta de Extractos</td>
                                                      <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32"  height="20" align="left"><%=datos[0]%></td>
                                             </tr>
                                      </table>
                                    </td>
                             </tr>

                             <tr  class="fila">
                                    <td height="26" width="123" >&nbsp;&nbsp;&nbsp;&nbsp;Propietario
									</td>
                                    <td height="26" width='303' >&nbsp; <input type="<%= (show.equals("ok")?"text":"hidden") %>" name="nit" style="text-align:center;width:100" value="<%= (p!=null?p.getC_nit():"") %>" onFocus="this.select();" >
										<%=  (!show.equals("ok") &&  p!=null? "[" + p.getC_nit() + "] " + p.getC_payment_name():"") %>
									</td>
                             </tr>
                             <tr  class="fila">
                               		<td colspan="2">&nbsp;
                               		  <input type="checkbox" name="egPagados" value="ok" onClick=" f1.style.display=(this.checked?'block':'none'); f2.style.display = f1.style.display;">
                               		&nbsp;Mostrar Cheques Pagados</td>
							 </tr>
                             <tr  class="filagris" id="f1" style="display:none; ">
                               		<td width='123'>
                               		   &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Fecha Inicial
                           		    </td>
									<td width='303' align="left">&nbsp;
										<input type="text" name="fini" value="<%= periodoInicial %>" style="width:100; text-align:center; " readonly>
										<span class="comentario"><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(fini);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt=""></a> </span>
									</td>
                             </tr>
                             <tr  class="filagris" id="f2" style="display:none; ">
                               		<td width='123'>
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Fecha Final </td>
									<td width='303' align="left">&nbsp;
										<input type="text" name="ffin" value="<%= periodoFinal %>" style="width:100; text-align:center " readonly>
										<span class="comentario"><a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(ffin);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt=""></a> </span>
									</td>
                             </tr>
                             <tr  class="fila">
                               		<td colspan="2">&nbsp;
                               		  <input type="checkbox" name="egPendientes" checked value="ok">
                               		&nbsp;Mostrar Planillas y Facturas pendientes por cancelar.</td>
                             </tr>
					</table>
				</td>
			</tr>
		</table>
	    <p>
		 	<img src = "<%=BASEURL%>/images/botones/aceptar.gif" style = "cursor:hand" name = "imgaceptar" id="imgaceptar" onClick = "validarConsultaExtracto();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
		 	<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
		</p>

		<input type="hidden" name="dstrct" value="<%= (p!=null?p.getDistrito():user!=null?user.getDstrct():"") %>">
		<input type="hidden" name="show" value="<%= show %>">
     </form>

	 <div style="font-family:Tahoma; font-weight:bold; font-size: 11px; color: #003399;" id="imgload"></div>

	<% if(msg!=null && !msg.equals("")){%>
	<br><br>

   		<table width="436" border="2" align="center">
     		<tr>
    			<td>
					<table width="103%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      					<tr>
        					<td width="364" align="center" class="mensajes"><%=msg%></td>
        				    <td width="19" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        					<td width="19">&nbsp;</td>
      					</tr>
    				</table>
				</td>
  			</tr>
		</table>
	<% } %>
</center>
</div>
<%=datos[1]%>
<iframe width=188 height=166 name="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" id="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins_24.js" src="<%=BASEURL%>/js/calendartsp/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
</body>
</html>
<script>
  function validarConsultaExtracto(){
  	if (formulario.nit.value==''){
		alert ('El proveedor no se encuentra registrado en el sistema.');
		return false;
	}


	if (formulario.fini.value!='' && formulario.ffin.value!=''){
	    var fi = parseFloat(formulario.fini.value.replace(/-|:|\/| /g,''));
		var ff = parseFloat(formulario.ffin.value.replace(/-|:|\/| /g,''));
		if (fi>ff){
			alert ('La fecha final debe ser mayor o igual que la fecha inicial');
			return false;
		}
	}
	else{
		alert ('Debe indicar el rango de fechas.');
		return false;
	}



	var imgaceptar  = document.getElementById('imgaceptar');
	if (imgaceptar){
		imgaceptar.style.display = 'none';
		//imgload.style.visibility = 'visible';
		imgload.innerHTML = "<img src='<%= BASEURL %>/images/cargando.gif'> Procesando su solicitud por favor espere ...";
	}

	formulario.submit();

  }
  window.focus();
</script>