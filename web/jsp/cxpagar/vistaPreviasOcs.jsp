<!--
     - Author(s)       :      MARIO FONTALVO
     - Date            :      17/28/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: listado de ocs a liquidar
 --%>

<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>
<%
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
	<title>Vista Previa de Ocs a liquidar</title>
    <link href="<%= BASEURL %>/css/estilostsp.css"     rel="stylesheet" type="text/css"> 
    <link href="/css/estilostsp.css" rel="stylesheet"  type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script>
		function validarSeleccion(){
		   var elementos = document.getElementsByName("idListOc");
		   
		   for (var i=0;i<elementos.length; i++){
		      if (elementos[i].checked){
			    return true;			
			  }  
		   }		   
		   alert ('Seleccione por lo menos una planilla para poder iniciar el proceso de liquidacion.');
		   return false;
		}
    </script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listado de Ocs"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
   
<%
   Vector ocs = model.LiqOCSvc.getOcsALiquidar();
   if (ocs!=null && !ocs.isEmpty() ){
%>   
   
    <form action="<%=CONTROLLER%>?estado=Liquidar&accion=OC" method="post" name="formulario">
    
    <table width="689"  border="2" align="center">
     <tr>
         <td >           
    
                <table width="100%"  align="center">
                  <tr >
                     <td>
                          <table width='100%'  class="barratitulo">
                                <tr class="fila">
                                        <td align="left" width='46%' class="subtitulo1" nowrap> Planillas a Liquidar </td>
                                        <td width='54%' ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"align="left"><%=datos[0]%></td>
                                </tr>
                          </table>
                     </td>
                  </tr>
                  <tr class='fila'>
                     <td>
					 	
							<table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
								<tr class="tblTitulo">
									<th width="4%" >&nbsp;  </th>								
									<th width="14%" > NIT      </th>
									<th width="10%" > ID_MIMS  </th>
									<th width="20%" > NOMBRE   </th>
									<th width="12%" > PLANILLA </th>
									<th width="20%" > ORIGEN   </th>
									<th width="20%" > DESTINO  </th>
								</tr>
								<% for (int i=0; i<ocs.size(); i++) { 
								   	Planilla p = (Planilla) ocs.get(i);
								%>
									<tr class="<%= (i%2==0?"filagris":"filaazul")%>">
										<td class="bordereporte" nowrap align="center"> <input type="checkbox" name="idListOc" value="<%=  p.getNumpla() %>" onclick=" this.parentNode.parentNode.className=(this.checked?'filaresaltada':'<%= (i%2==0?"filagris":"filaazul")%>') ">  </td>
										<td class="bordereporte" nowrap align="center"> <%= p.getNitpro()    %></td>
										<td class="bordereporte" nowrap align="center"> <%= p.getProveedor() %></td>
										<td class="bordereporte" nowrap> <%= p.getNomprop()   %></td>
										<td class="bordereporte" nowrap align="center"> <%= p.getNumpla()    %></td>
										<td class="bordereporte" nowrap> <%= p.getNomori()    %></td>
										<td class="bordereporte" nowrap> <%= p.getNomdest()   %></td>
									</tr>								
								<% } %>
								
							</table>						
					 
					 </td>
                  </tr>				  
		  
                </table>
                
        </td>
      </tr>
      </table>  
            
        <br>
        <center>
	<img src='<%=BASEURL%>/images/botones/aceptar.gif'    style=" cursor:hand"  title='Aceptar...'     name='i_crear'       onclick="if (validarSeleccion()) { this.src='<%=BASEURL%>/images/botones/aceptarDisable.gif';  this.onmousemove=new Function (''); this.onmouseover= new Function(''); this.onclick=new Function(''); formulario.submit();  } "     onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
        <img src='<%=BASEURL%>/images/botones/regresar.gif'   style=" cursor:hand"  title='Aceptar...'     name='i_crear'       onclick="document.location = '<%= BASEURL %>/jsp/cxpagar/liquidacion/liquidacion.jsp' "     onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
        <img src='<%=BASEURL%>/images/botones/salir.gif'      style='cursor:hand'    title='Salir...'      name='i_salir'       onclick='parent.close();'           onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
		</center>
	     <br>
		 <%if (request.getParameter("msg") != null ){%>      
            <table border="2" align="center">
              <tr>
                <td>
					<table width="100%"  border="0" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                  		<tr>
                    		<td width="500" align="center" class="mensajes"><%= request.getParameter("msg") %></td>
                    		<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    		<td width="58">&nbsp; </td>
                  		</tr>
                	</table>
				</td>
              </tr>
     </table>               
     <%}%>   
	 <input type="hidden" name="Opcion" value="ConfirmacionOcs">
    </form>
<% } %>	

	
</div>
<%=datos[1]%>
	


</body>
</html>
