<%@ page session="true"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>

<title>Consulta de Facturas</title>
<script language="javascript" src="../../../js/validar.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div class="fila" style="position:absolute; visibility:hidden; width:350px; height:50px; z-index:1; border: 2 outset #003399; " id="msg">
</div>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Facturas Proveedor"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<%    
    List cxp_Docs = (List) request.getAttribute("Items");
	
	if( cxp_Docs!=null && cxp_Docs.size()!=0 ) {
%>
<table border="2" align="center">
  <tr>
    <td><table width="100%">
      <tr>
        <td width="50%" class="subtitulo1">Lista  de Documentos Relacionados </td>
        <td width="50%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif"><%=datos[0]%></td>
      </tr>
    </table>      
      <table border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4" width="100%">
      <tr class="tblTitulo"  >
        <td nowrap>&nbsp;</td>
		<td ><div align="center">Documento</div></td>
        <td nowrap><div align="center">Tipo de Doc.</div></td>
        <td ><div align="center">Moneda</div></td>
        <td ><div align="center">Valor Doc.</div></td>
        <td ><div align="center">Valor en Pesos </div></td>
        <td nowrap><div align="center">Vlr. Saldo</div></td>
<%

      double ttl_neto = 0;
      double ttl_neto_me = 0;
      double ttl_saldo_me = 0;	  
      for (int i = 0; i < cxp_Docs.size(); i++){
          CXP_Doc doc = (CXP_Doc) cxp_Docs.get(i);
		  String tdoc = "";
		  String tdoc_rel = "";
		  
		  if( doc.getTipo_documento().equals("010") ){
			  	tdoc = "FACTURA";
		  } else if( doc.getTipo_documento().equals("035") ){
			  	tdoc = "NOTA CREDITO";
				doc.setVlr_neto_me(-1*doc.getVlr_neto_me());
				doc.setVlr_neto(-1*doc.getVlr_neto());
				doc.setVlr_saldo_me(-1*doc.getVlr_saldo_me());
		  } else if( doc.getTipo_documento().equals("036") ){
			  	tdoc = "NOTA DEBITO";				
		  }
		  
		  ttl_neto_me += doc.getVlr_neto_me();
		  ttl_neto += doc.getVlr_neto();
		  ttl_saldo_me += doc.getVlr_saldo_me();
%>
      <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
        <td class="bordereporte" nowrap><img title="Ver detalles" src="<%= BASEURL %>/images/botones/iconos/detalles.gif" style="cursor:hand"  name="imgsalir"  onClick="window.open('<%= CONTROLLER %>?estado=Factura&accion=Detalle&documento=<%= doc.getDocumento().replaceAll("#","-_-") %>&prov=<%= doc.getProveedor()%>&tipo_doc=<%= doc.getTipo_documento()%>','','status=yes,scrollbars=no,width=780,height=650,resizable=yes');"></td>
        <td class="bordereporte" nowrap><%= doc.getDocumento() %></td>
        <td class="bordereporte" nowrap><%= tdoc %></td>
        <td class="bordereporte" nowrap><div align="center"><%= doc.getMoneda() %></div></td>
        <td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(doc.getVlr_neto_me()) %></div></td>
        <td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(doc.getVlr_neto()) %>&nbsp;</div></td>
        <td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(doc.getVlr_saldo_me()) %></div></td>
	</tr>	
<%
	}
%>
		  <tr class="subtitulo1">
			<td class="bordereporte" nowrap colspan="4"><div align="right">Totales</div></td>
			<td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(ttl_neto_me) %></div></td>
			<td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(ttl_neto) %>&nbsp;</div></td>
			<td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(ttl_saldo_me) %></div></td>
		</tr>	
    </table></td>
  </tr>
</table>
<br>  
<%
	}
%>  
<% if( request.getParameter("msg") != null ){%>
<p>
<table border="2" align="center">
  <tr>
    <td><table border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
          <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="58">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<p></p>
<p>
    <%} %>
</p>
<table width="580" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr align="left">
    <td><img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" id="c_salir" style="cursor:hand " onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;   </tr>
</table>
</div>
<%=datos[1]%>
</body>
</html>



