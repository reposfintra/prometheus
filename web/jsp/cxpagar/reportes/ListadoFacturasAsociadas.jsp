<%@ page session="true"%>
<%@ page errorPage="/error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>

<title>|Consulta de Facturas Recurrentes|</title>
<script language="javascript" src="../../../js/validar.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script>
function showMsg (message){
	var obj = document.getElementById('msg');
	if (obj){
		/*var rightedge = document.body.clientWidth - event.clientX;
		var bottomedge = document.body.clientHeight - event.clientY;
		
		if (rightedge < obj.offsetWidth)
			obj.style.left = document.body.scrollLeft + event.clientX - obj.offsetWidth;
		else
			obj.style.left = document.body.scrollLeft + event.clientX;

		if (bottomedge < obj.offsetHeight)
			obj.style.top = document.body.scrollTop + event.clientY - obj.offsetHeight;
		else
			obj.style.top = document.body.scrollTop + event.clientY;*/
		obj.style.top =  document.body.clientHeight/4;		
		obj.style.left = document.body.clientWidth/4;
		
		obj.innerHTML  = message;
		obj.style.visibility = 'visible';
		//obj.style.top  = event.clientY;
		//obj.style.left = event.clientX;
	}
}
  
function hiddenMsg (){
  var obj = document.getElementById('msg');
  if (obj) obj.style.visibility = 'hidden';
}


</script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div class="fila" style="position:absolute; visibility:hidden; width:350px; height:50px; z-index:1; border: 2 outset #003399; " id="msg">
</div>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Facturas Asociadas CXP"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<%    
    Vector cxp_Docs = model.cXP_DocService.getFacturasAsociadasRxp();
%>
<table border="2" align="center" width='100%'>
  <tr>
    <td>
    <table width="100%">
      <tr>
        <td width="50%" class="subtitulo1">Lista de Facturas Asociadas</td>
        <td width="50%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif"><%=datos[0]%></td>
      </tr>
    </table>
      
      <table border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4" width='100%'>
      <tr class="tblTitulo" >
        <td nowrap><div align="center">Detalles</div></td>
        <td nowrap><div align="center">Estado</div></td>
        <td nowrap><div align="center">Proveedor</div></td>
        <td ><div align="center">Documento</div></td>
        <td nowrap><div align="center">Tipo de Doc.</div></td>
        <td nowrap><div align="center">Descripci&oacute;n</div></td>
        <td nowrap><div align="center">Fecha Doc.</div></td>
        <td ><div align="center">Valor Factura</div></td>        
        <td ><div align="center">Moneda</div></td>
        <td ><div align="center">Saldo Factura</div></td>
        <td ><div align="center">Banco</div></td>
        <td ><div align="center">Sucursal</div></td>
        <td ><div align="center">Corrida</div></td>
        <td ><div align="center">Cheque</div></td>
        <td ><div align="center">Aprobador</div></td>
        <td ><div align="center">Fecha Aprobacion</div></td>
        </tr>
        
        
        <form action='<%=CONTROLLER%>?estado=ReporteFacturas&accion=Recurrentes&evento=anular' name='form' id='form' method='post'>
<%
      double total = 0;
      for (int i = 0; i < cxp_Docs.size(); i++){
          CXP_Doc doc = (CXP_Doc) cxp_Docs.elementAt(i);		
%>
      <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
        <% String estado = ( doc.getReg_status().equals("A") )?"ANULADO":"";%>
        <td class="bordereporte" nowrap align="center"><img title="Ver Factura" src="<%= BASEURL %>/images/botones/iconos/detalles.gif" style="cursor:hand"  name="imgsalir"  onClick="window.open('<%= CONTROLLER %>?estado=Factura&accion=Detalle&documento=<%= doc.getDocumento().replaceAll("#","-_-") %>&prov=<%= doc.getProveedor()%>&tipo_doc=<%= doc.getTipo_documento()%>','DETALL2','status=yes,scrollbars=no,width=780,height=650,resizable=yes');"></td>
        <td class="bordereporte" nowrap align="center"><%=estado%></td>
        <td class="bordereporte" nowrap align="right"><%= doc.getProveedor() %></td>        
        <td class="bordereporte" nowrap><%= doc.getDocumento() %> </td>
        <td class="bordereporte" nowrap align="CENTER">FACTURA</td>
        <td class="bordereporte" nowrap><%= doc.getDescripcion() %></td>
        <td class="bordereporte" nowrap align="CENTER"><%= doc.getFecha_documento() %></td>
        <td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(doc.getVlr_neto_me()) %></div></td>
        <td class="bordereporte" nowrap align="CENTER"><%= doc.getMoneda() %></td>
        <td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(doc.getVlr_saldo_me()) %></div></td>
        <td class="bordereporte" nowrap align="CENTER"><%= doc.getBanco() %></td>
        <td class="bordereporte" nowrap align="CENTER"><%= doc.getSucursal() %></td>
        <td class="bordereporte" nowrap align="CENTER"><%= doc.getCorrida() %></td>
        <td class="bordereporte" nowrap align="CENTER"><%= doc.getCheque() %></td>
        <td class="bordereporte" nowrap align="CENTER"><%= doc.getAprobador() %></td>
        <td class="bordereporte" nowrap align="CENTER"><%= doc.getFecha_aprobacion()%></td>
        </tr>
<%
	}
%>
        <input type='hidden' name='factura' value='<%=request.getParameter("factura")%>'>
        <input type='hidden' name='tipo_doc' value='<%=request.getParameter("tipo_doc")%>'>
        <input type='hidden' name='proveedor' value='<%=request.getParameter("proveedor")%>'>
        <input type='hidden' name='planilla' value='<%=request.getParameter("planilla")%>'>
        <input type='hidden' name='agencia' value='<%=request.getParameter("agencia")%>'>
        <input type='hidden' name='banco' value='<%=request.getParameter("banco")%>'>
        <input type='hidden' name='sucursal' value='<%=request.getParameter("sucursal")%>'>
        <input type='hidden' name='FechaI' value='<%=request.getParameter("FechaI")%>'>
        <input type='hidden' name='FechaF' value='<%=request.getParameter("FechaF")%>'>
        <input type='hidden' name='pagada' value='<%=request.getParameter("pagada")%>'>
        
    </form>
    </table></td>
  </tr>
</table>
<br>  
  
<% if( request.getParameter("msg") != null ){%>
<p>
<table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
          <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="58">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<p></p>
<p>
    <%} %>
</p>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr align="center">
    <td><img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>  
  </tr>
</table>
</div>
<%=datos[1]%>
</body>
</html>

<script>
    
    function anular_facturas(){
        
        var facs = 0;
        
        for( var i=0; i < form.length; i++ ){
            var ele  = form.elements[i];      
            if(ele.type=='checkbox'  && ele.checked){
                facs = 1;
                break;
            }
        }         
        
        if( facs != 0 ){
        
            if( confirm('Desea anular las facturas seleccionadas') ){ 
                anularBtn ( c_regresar  );
                anularBtn ( c_anular  );
                anularBtn ( c_salir  );
                form.submit(); 
            }
                                    
        }else{
            alert('Debe seleccionar las facturas que desea anular');
        }
    }

</script>


