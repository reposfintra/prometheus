<!--
- Autor : Ing. Andr�s Maturana De La Cruz
- Date  : 27.06.2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la generaci�n del reporte de facturas de clientes.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
</script>
<title>Detalle Factura Recurrente</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostspFactura.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Estilo1 {color: #D0E8E8}
-->
</style>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
<script>
 <%= model.ChequeFactXCorridasSvc.getSucursalesJS() %>
 function loadSucursal(sucursal, banco){
            var vector =  sucursales.split("||");
            var ele    = '';
            for(var i=0;i<vector.length;i++){
                var vectorSuc  = vector[i].split("|");
                if( vectorSuc[0] == banco ){
                    if ( ele != '' )
                         ele += '|';
                    ele += vectorSuc[1];
                }
            }
            
            if( ele != '' ){ 
                var vecEle = ele.split('|');
                sucursal.length = vecEle.length + 1;
                sucursal.options[0].value = '';
                sucursal.options[0].text  = 'Seleccionar';
                sucursal.options[0].selected='selected';
                for(var i=0; i <vecEle.length;i++){
                    sucursal.options[i+1].value = vecEle[i];
                    sucursal.options[i+1].text  = vecEle[i];
                }
            }               
        }
 
        
        function modificarBanco(){
           if(form2.banco.value==''){
               alert('El banco no puede estar vacio'); 
               form2.banco.focus();
           }else if(form2.sucursal.value == ''){
               alert('La sucursal  no puede estar vacia');
               form2.sucursal.focus();
           }else{
               document.form2.submit(); 
           }
           
        }
</script>


<body onresize="redimensionar()" onload = 'redimensionar()'>

<div id="capaSuperior" style="position:absolute; width:100%; height:110px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Reporte Facturas Recurrente"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 109px; overflow: scroll;">
<%
	CXP_Doc factura = (CXP_Doc) request.getAttribute("Main");
	Vector vItems = (Vector) request.getAttribute("Items");
        List     bancos  =  model.ChequeFactXCorridasSvc.getListBancos(); 
%>
<% String aplazada  = request.getAttribute("aplazada")!=null?(String)request.getAttribute("aplazada"):"";%>      
<form name='form2' action='<%= CONTROLLER %>?estado=Factura&accion=Servicios&OP=UPDATE_BANCO_RECURRENTE' method="post">
<input type="hidden" name='distrito' value='<%=factura.getDstrct()%>'>
<input type="hidden" name='proveedor' value='<%=factura.getProveedor()%>'>
<input type="hidden" name='tipo' value='<%=factura.getTipo_documento()%>'>
<input type="hidden" name='documento' value='<%=factura.getDocumento()%>'>

<%
  String mens= (String) request.getAttribute("ms");
    if ( mens != null && !mens.equals("")){
%>

<table border="2" align="center">
  <tr>
    <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="282" align="center" class="mensajes"><%=mens%></td>
          <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="78">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<br>
<%
    }   
%>

  <table width="980"  border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table border="2"  >
        <tr >
          <td width="1001">
            <table width="100%" align="center"  >
              <tr >
                <td width="50%"  class="subtitulo1"><p align="left">Documento</p></td>
                <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" class="Estilo1" height="20" align="left"><%=datos[0]%></td>
              </tr>
            </table>
            <table width="100%" align="center" cols="7">
              <tr class="filaFactura">
                <td width="118"  >Documento:</td>
                <!--<td width="117" ><%= factura.getDocument_name()%></td>-->
                <td colspan="2" ><%= factura.getDocumento() %></td>
                <td width="109" >Proveedor:</td>
                <td width="94" ><%= factura.getProveedor()%></td>
                <td colspan="3" nowrap><div title="Beneficiario: 
<%= factura.getBeneficiario()%>"><%= factura.getNomProveedor()%></div></td>
              </tr>
              <tr class="filaFactura">
                <td height="21" >Num cuotas:</td>
                <td width="93" ><%=factura.getNum_cuotas()%></td>
                <td width="149" >Fecha Inicio </td>
                <td width="109" ><%=factura.getFecha_inicio_transfer()%></td>
                <td >Fecha :</td>
                <td width="104" colspan="1"  ><%= factura.getFecha_documento()%></td>
                <td width="97" colspan="1" >Plazo(d&iacute;as):</td>
                <td width="195" colspan="1" ><%= factura.getPlazo()%></td>
              </tr>
              <tr class="filaFactura">
                <td height="23">Cuotas Generadas: </td>
                <td ><%=factura.getNum_cuotas_transfer()%></td>
                <td >Valor Transferido</td>
                
                <td ><%= factura.getMoneda().equals("DOL")? com.tsp.util.UtilFinanzas.customFormat2(factura.getVlr_transfer_me()) : com.tsp.util.UtilFinanzas.customFormat(factura.getVlr_transfer_me())%></td>
                <td >Moneda:</td>
                <td colspan="1" ><%= factura.getMoneda()%></td>
                <td colspan="1" >Frecuencia:</td>
                <td colspan="1" ><%=factura.getFrecuencia()%></td>
              </tr>
              <tr class="filaFactura">
                <td><p>Referencia:</p></td>
                <td colspan="3" >  <input type="text" name="referencia"  readonly maxlength="30" size="55" class="textboxFactura" value="<%=factura.getReferencia()%>"> </td>
                <td width='94' >Valor:</td>
                <td colspan="1" >
                  <%= factura.getMoneda().equals("DOL")? com.tsp.util.UtilFinanzas.customFormat2(factura.getVlr_neto()) : com.tsp.util.UtilFinanzas.customFormat(factura.getVlr_neto())%></td>
                <td colspan="1" >Banco:</td>
                <td colspan="1" >
                     <select  style='width:80%'  name='banco'  onchange='loadSucursal(this.form.sucursal, this.value);'>
                              <option value=''> Seleccionar </option>
                              <% for(int i=0;i<bancos.size();i++){
                                     String code = (String) bancos.get(i);%>
                                     <option value='<%=code%>' <%=(code.equals(factura.getBanco()))?"selected":""%>> <%=code%> </option>
                              <%}%>
                     </select> 
                     
                  </td>
              </tr>
              <tr class="filaFactura">
                <td >Descripci&oacute;n:</td>
                <td colspan="3"><textarea name="descripcion"  cols="55" rows="1" readonly class="textareaFactura"><%= factura.getDescripcion()%></textarea>                  </td>
                <td>Valor Neto: </td>
                
                <td colspan="1"><%= factura.getMoneda().equals("DOL")? com.tsp.util.UtilFinanzas.customFormat2(factura.getVlr_neto_me()) : com.tsp.util.UtilFinanzas.customFormat(factura.getVlr_neto_me())%><!-- neto de la factura --> </td>
                <td colspan="1" class="filaFactura">Sucursal:</td>
                <td colspan="1" ><select  style='width:80%'  name='sucursal'  id='sucursal'>
                                       <option value='<%= factura.getSucursal()%>'><%= factura.getSucursal()%> </option>
                                 </select>
                </td>
              </tr>
              <tr>
              
              </form>
                <td rowspan="2" class="filaFactura">Observaci&oacute;n: </td>
                <td colspan="3" rowspan="2" class="fila">
                      <textarea name="observacion"  cols="55" rows="3" readonly class="textboxFactura"><%= factura.getObservacion()%></textarea>
                </td>
                <% String estado = ""; %>                
                <% if( factura.getReg_status().equals("A") ) estado = "ANULADA"; %>                
                <% if( factura.getNum_cuotas() == factura.getNum_cuotas_transfer() && estado.equals("") ) estado = "PAGADA"; %>
                
                <td colspan='2' rowspan='2' class='fila' align='center'><%=estado%></td>      
                <td colspan="1" class="filaFactura">Autorizador: </td>
                <td colspan="1"  class="filaFactura">

                        <%=(factura.getUsuario_aprobacion()!= null)?factura.getUsuario_aprobacion():""%>
 </td>
              </tr>
              <tr class="filaFactura">
                <td valign="top"  >Handle Code: </td>
                <td valign="top" colspan="1" ><%=(factura.getHandle_code()!= null)?factura.getHandle_code():""%></td>
              </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
	<tr>
      <td>


<% if( factura.getNum_cuotas() != factura.getNum_cuotas_transfer() && !factura.getReg_status().equals("A") ){ %>
<table border="2" width = '980' >

<tr >
          <td>
            <table width="100%" align="center"  >
              <tr >
                <td width="50%"  class="subtitulo1"><p align="left">Modificar Fecha pr�xima cuota</p></td>
                <td width="40%"  class="barratitulo" ><img src="<%=BASEURL%>/images/titulo.gif" class="Estilo1" height="20" align="left"></td><td class='barratitulo' align=""  width="10%"> <a onclick='modificarBanco();' style="cursor:hand;" class="Simulacion_Hiper" HIDEFOCUS>Modificar Banco</a></td>
              </tr>
            </table>            
            <table width="100%" align="center">
                <form name="form" id="form" method="post" action='<%=CONTROLLER%>?estado=FacturaRecurrente&accion=Detalle'>
                <% String proxima = request.getAttribute("proxima")!=null?(String)request.getAttribute("proxima"):"";%>
                <% String ultima  = request.getAttribute("ultima")!=null?(String)request.getAttribute("ultima"):"";%>                
                <% ultima = ( ultima.equals("0099-01-01") || factura.getNum_cuotas_transfer() == 0 )? "NO REGISTRA" : ultima;%>
                <tr class='filaFactura'>
                <td align='left' width='25%'>&nbsp;Fecha �ltima cuota: &nbsp; <%=ultima%> </td>                
                <td align='left' width='25%'>&nbsp;Fecha pr�xima cuota: &nbsp; <%=proxima%> </td>                
                <td> &nbsp; Suspender por &nbsp;<input type='text' value='<%=Util.coalesce( request.getParameter("periodos"),"")%>' name='periodos' id='periodos' size='2' maxlength='2' onKeyPress="soloDigitos(event,'decNo');" onblur='calcular(this);'> &nbsp; periodos
                <%if( !aplazada.equals("") ){%>
                    <td align='left' width='25%'>&nbsp;Nueva pr�xima cuota: &nbsp; <%=aplazada%> </td>                
                <%}%>
                </td>
                </tr>
            </table>
            
            <input type="hidden" name='documento' value='<%=Util.coalesce( request.getParameter("documento"),"")%>'>
            <input type="hidden" name='prov' value='<%=Util.coalesce( request.getParameter("prov"),"")%>'>
            <input type="hidden" name='tipo_doc' value='<%=Util.coalesce( request.getParameter("tipo_doc"),"")%>'>
            <input type="hidden" name='aplazada' value='<%=aplazada%>'>
            
            </form>
            
            </td>
           </tr> 
           
        </table>

<%}%>           

<!-- DETALLE DE LA FACTURA-->
<table border="2" width = '980' >
        <tr >
          <td>
            <table width="100%" align="center"  >
              <tr >
                <td width="50%"  class="subtitulo1"><p align="left">Detalle del Documento </p></td>
                <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" class="Estilo1" height="20" align="left"></td>
              </tr>
            </table>
            <table id="detalle" width="100%" >
              <tr  id="fila1" class="tblTituloFactura">
                <td align="center" width="50">Item</td>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr align="center"  class="tblTituloFactura">
                      <td width="13">&nbsp;</td>
                      <td width="310" align="left">Descripcion</td>
                      <td width="160" align="left">Concepto</td>
                      <td width="130" align="center">Planilla</td>
                      <td align="left" >Centro</td>
                      <td width="250"  align="left" colspan='3'>ABC</td>
                    </tr>
                  </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr align="center" class="tblTituloFactura">
                        <td width="14">&nbsp;</td>
                        <td width="150" align="left">Cuenta</td>
                        <td width="200" align="left" >Auxiliar</td>
                        <td width="170" align="left">Valor</td>
                        <td width="125" align="left" title='<%=factura.getTotalIva()%>'>IVA</td>
                        <td width="100" align="left" title='<%=factura.getTotalRiva()%>'>RIVA</td>
                        <td width="110" align="left" title='<%=factura.getTotalRica()%>'>RICA</td>
                        <td width="100" align="left" title='<%=factura.getTotalRfte()%>'>RFTE</td>
                        <td width="125" align="left">Valor Neto</td>
                      </tr>
                  </table></td>
              </tr>
<% 
for( int i=0; i<vItems.size(); i++){
	CXPItemDoc item = (CXPItemDoc) vItems.elementAt(i);
	Vector Copia = item.getVCopia();
	String iva = "";
        String riva = "";
	String rica = "";
	String rfte = "";
        double Vlr_iva = 0;
        double Vlr_riva = 0;
        double Vlr_rica = 0;
        double Vlr_rfte = 0;
        
	for(int j=0;j< Copia.size();j++){ 
		CXPImpItem impuestoCopia = (CXPImpItem)Copia.elementAt(j); 
		String impuesto = impuestoCopia.getCod_impuesto();
                double vlr      = impuestoCopia.getVlr_total_impuesto();
		String tipo 	= impuestoCopia.getTipo_impuesto();
		if( tipo!=null && tipo.equals("IVA") ) {
			iva = impuesto;
                        Vlr_iva = vlr;
		}else if( tipo!=null && tipo.equals("RIVA") ) {
			riva = impuesto;
                        Vlr_riva = vlr;
		} else if( tipo!=null && tipo.equals("RICA") ) {
			rica = impuesto;
                        Vlr_rica = vlr;
		} else if( tipo!=null && tipo.equals("RFTE") ) {
			rfte = impuesto;
                        Vlr_rfte = vlr;
		}
	}
		
%>
           <tr class="filaFactura"  nowrap  bordercolor="#D1DCEB" >
		<td nowrap><%=i+1%></a></td>
		    <td>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
                         <tr align="center">
                            
                            <td   nowrap  align="left" width='310'>&nbsp;<textarea name="desc"  id="desc" cols="55" rows="1" class="textboxFactura"  readonly ><%= item.getDescripcion()%></textarea></td>
                            <td   nowrap align="left" width='180'><input name="descripcion_i" id="descripcion_i" type="text" size="30" class="textboxFactura"  maxlength="4" readonly value='<%= item.getConcepto()%>'> </td>
                            <td   nowrap width='100'><input name="planilla" id="planilla" type="text" size="8"  class="textboxFactura" readonly  maxlength="8" value='<%= item.getPlanilla()%>' ></td>
                            <td   nowrap width='100'><input name="oc" type="text" id="oc" size="6" maxlength="3" class="textboxFactura" readonly value='<%=item.getCodcliarea()%>'></td>                            
                            <td   nowrap width='250' colspan='3' align="left"><input name="codigo_abc" style="text-transform:uppercase" id="codigo_abc" type="text" size="4" maxlength="4" class="textboxFactura" readonly value='<%= item.getCodigo_abc()%>'></td>
                         </tr>
                       </table>
		       
                       <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr align="center">
			      <td width="108"> &nbsp;<input name="codigo_cuenta" id="codigo_cuenta" class="textboxFactura" type="text" size="14" maxlength="20" readonly  value='<%= item.getCodigo_cuenta()%>'></td>
			      <td align="center" width="200"><input type="text" maxlength='25'  style='width:90' name='auxiliar' id='auxiliar' class="textboxFactura" readonly  value='<%= item.getAuxiliar()%>'></td> 
                              <td width="239"><input name="valor1"  class="textboxFactura"   id="valor1" type="text" size="16"  style="text-align:right;"   maxlength="11"  align="right" readonly value='<%= factura.getMoneda().equals("DOL")? com.tsp.util.UtilFinanzas.customFormat2(item.getVlr_me()) : com.tsp.util.UtilFinanzas.customFormat(item.getVlr_me()) %>'></td>
			      <td width="125" align="left"><input name="impuesto"   style="text-transform:uppercase" id ="impuesto" class='textareaFactura' type="text" size="8" maxlength="13" readonly value='<%= (Vlr_iva == 0)?"":String.valueOf(Vlr_iva)%>'  title='<%= iva%>'></td>
                              <td width="100" align="left"><input name="impuesto"   style="text-transform:uppercase" id ="impuesto" class='textareaFactura' type="text" size="8" maxlength="13" readonly value='<%= (Vlr_riva ==0)?"":String.valueOf(Vlr_riva)%>' title='<%= riva%>'></td>
                              <td width="120" align="left"><input name="impuesto"   style="text-transform:uppercase" id ="impuesto" class='textareaFactura' type="text" size="8" maxlength="13" readonly value='<%= (Vlr_rica ==0)?"":String.valueOf(Vlr_rica)%>' title='<%= rica%>'></td>
                              <td width="100" align="left"><input name="impuesto"   style="text-transform:uppercase" id ="impuesto" class='textareaFactura' type="text" size="8" maxlength="13" readonly value='<%= (Vlr_rfte ==0)?"":String.valueOf(Vlr_rfte)%>' title='<%= rfte%>'></td>
                              <% double neto = item.getVlr_me() + Vlr_iva + Vlr_riva + Vlr_rica + Vlr_rfte; %>
                              <!--<td width="143"><input name="valorNeto" id="valorNeto" class="textboxFactura"  style="text-align:right;" type="text" size="16"   maxlength="11" align="right" readonly value='<%= factura.getMoneda().equals("DOL")? com.tsp.util.UtilFinanzas.customFormat2(item.getVlr()) : com.tsp.util.UtilFinanzas.customFormat(item.getVlr()) %>'></td>-->
                              <td width="143"><input name="valorNeto" id="valorNeto" class="textboxFactura"  style="text-align:right;" type="text" size="16"   maxlength="11" align="right" readonly value='<%= factura.getMoneda().equals("DOL")? com.tsp.util.UtilFinanzas.customFormat2( neto ) : com.tsp.util.UtilFinanzas.customFormat( neto ) %>'></td>
			    </tr>
                        </table>
                      </td>
	      </tr>  
 
 
<% } %>
</table>









































<!-- FIN DETALLE DE LA FACTURA -- >
	</td>
    </tr>
  </table>
  <br>
  
  <%
  String mensaje = ""+request.getParameter("msj");
    if ( !(mensaje.equals("") || mensaje.equals("null"))){
%>
<table border="2" align="center">
  <tr>
    <td><table width="500" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="282" align="center" class="mensajes"><%=mensaje%></td>
          <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="78">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<br>
<%
    }   
%>
  
  
  <br>
  <center> 
    <%if( !aplazada.equals("") ){%> <img src="<%=BASEURL%>/images/botones/modificar.gif" name="c_modificar" onClick="aplazar();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> <%}%>    
    <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
  </center>
</div>
<%=datos[1]%>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</body>
</html>



<script>

    function calcular( plazo ){
    
        if( plazo.value!= '' ){

            var act = form.action;
            act += '&evento=plazo';
            form.action = act;
            form.submit();
            
        }
        
    }
    
    function aplazar(){
    
        plazo = document.getElementById("periodos").value;
        if( plazo.value!= '' ){

            var act = form.action;
            act += '&evento=aplazar';
            form.action = act;
            form.submit();
            
        }
        
    }
    
</script>
