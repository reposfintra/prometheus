<%@ page session="true"%>
<%@ page errorPage="/error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>

<title>Facturas Proveedor</title>
<script language="javascript" src="../../../js/validar.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Facturas Proveedor"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<%    
    String idprov = (String) request.getAttribute("idprov");
    Vector cxp_Docs = (Vector) request.getAttribute("Vec_cxp_Docs");
    Identidad prov = (Identidad) request.getAttribute("proveedor");
%>
<table width="100%"  border="2" align="center">
  <tr>
    <td><table width="100%">
      <tr>
        <td width="50%" class="subtitulo1">Lista  de Facturas </td>
        <td width="50%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif"><%=datos[0]%></td>
      </tr>
    </table>      
      <table width="100%" align="center" cellpadding="1" cellspacing="1" >
        <tr class="fila">
          <td width="12%" height="22" class="filaresaltada">Identificaci&oacute;n</td>
          <td><%= idprov %><% //prov.getCedula() %></td>
          </tr>
        <tr class="fila">
          <td height="22" class="filaresaltada" >Proveedor</td>
          <td height="22"><%= prov.getNombre() %></td>
          </tr>
      </table>
      <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
      <tr class="tblTitulo"  >
        <td width="85" nowrap><div align="center">Documento</div></td>
        <td width="65" ><div align="center">Banco</div></td>
        <td width="82" ><div align="center">Sucursal</div></td>
        <td width="89" ><div align="center">Fecha Factura </div></td>
        <td width="124"> <div align="center">Fecha Vencimiento </div></td>
        <td width="78"><div align="center">Ultima Fecha Pago </div></td>
        <td width="136"><div align="center">Descripci&oacute;n</div></td>
        <td width="151"><div align="center">Observaciones</div></td>
        <td width="114"><div align="center">Valor Neto </div></td>
      </tr>
<%
      double total = 0;
      for (int i = 0; i < cxp_Docs.size(); i++){
          CXP_Doc doc = (CXP_Doc) cxp_Docs.elementAt(i);
		  total += doc.getVlr_neto();
%>
      <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand"
	  onClick="window.open('<%= CONTROLLER %>?estado=Factura&accion=Detalle&documento=<%= doc.getDocumento() %>&prov=<%= doc.getProveedor()%>','DETALL','status=yes,scrollbars=no,width=780,height=650,resizable=yes');">
        <td class="bordereporte"><%= doc.getDocumento() %></td>
        <td class="bordereporte"><%= doc.getBanco() %></td>
        <td class="bordereporte"><%= doc.getSucursal() %></td>
        <td class="bordereporte"><%= (doc.getFecha_documento()!=null)? doc.getFecha_documento().substring(0, 10) : "&nbsp;" %></td>
        <td class="bordereporte"><%= (doc.getFecha_vencimiento()!=null)? doc.getFecha_vencimiento().substring(0, 10) : "&nbsp;" %></td>
        <td class="bordereporte"><%= (doc.getUltima_fecha_pago()!=null)? doc.getUltima_fecha_pago() : "&nbsp;"  %></td>
        <td class="bordereporte"><%= doc.getDescripcion() %></td>
        <td class="bordereporte"><%= doc.getObservacion() %></td>
        <td class="bordereporte"><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(doc.getVlr_neto()) %></div></td>
      </tr>
<%
	}
%>      <tr>
        <td  colspan="8"><div class="letraresaltada" align="right">Total Facturas</div></td>
        <td ><div align="right" class="letraresaltada"><%= com.tsp.util.UtilFinanzas.customFormat2(total) %></div></td>
      </tr>
    </table></td>
  </tr>
</table>
<br>  
  
<% if( request.getParameter("msg") != null ){%>
<p>
<table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
          <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="58">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<p></p>
<p>
    <%} %>
</p>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr align="left">
    <td><img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" style="cursor:hand " onClick="window.location.href = '<%= BASEURL%>/jsp/cxpagar/reportes/consultaFacturasProveedor.jsp';" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp; <img src="<%=BASEURL%>/images/botones/exportarExcel.gif" name="c_regresar" id="c_regresar" style="cursor:hand " onClick="window.location.href = '<%=CONTROLLER%>?estado=Reporte&accion=FacturasProveedorXLS&nit=' + '<%= idprov%>';" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">  </tr>
</table>
</div>
<%=datos[1]%>
</body>
</html>



