<%@ page session="true"%>
<%@ page errorPage="/error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>

<title>Consulta de Facturas</title>
<script language="javascript" src="../../../js/validar.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script>
function showMsg (message){
	var obj = document.getElementById('msg');
	if (obj){
		/*var rightedge = document.body.clientWidth - event.clientX;
		var bottomedge = document.body.clientHeight - event.clientY;
		
		if (rightedge < obj.offsetWidth)
			obj.style.left = document.body.scrollLeft + event.clientX - obj.offsetWidth;
		else
			obj.style.left = document.body.scrollLeft + event.clientX;

		if (bottomedge < obj.offsetHeight)
			obj.style.top = document.body.scrollTop + event.clientY - obj.offsetHeight;
		else
			obj.style.top = document.body.scrollTop + event.clientY;*/
		obj.style.top =  document.body.clientHeight/4;		
		obj.style.left = document.body.clientWidth/4;
		
		obj.innerHTML  = message;
		obj.style.visibility = 'visible';
		//obj.style.top  = event.clientY;
		//obj.style.left = event.clientX;
	}
}
  
function hiddenMsg (){
  var obj = document.getElementById('msg');
  if (obj) obj.style.visibility = 'hidden';
}

function showInfoPago (fra, banco, suc, cta_no, cta_tipo, cta_nom, cta_nit){
	var obj = document.getElementById('msg');
	obj.style.height = '25px';

	var tabla = "<br><table width='97%' align='center' border='2' cellpadding='2' cellspacing='1' bgcolor='#F7F5F4'>";

	var bnc = "<table width='100%' class='tablaInferior'>";
	bnc += "<tr class='fila'><td width='30%'>Banco</td><td width='70%' nowrap>" + banco + "</td></tr>";
	bnc += "<tr class='fila'><td width='30%'>Sucursal</td><td width='70%' nowrap>" + suc + "</td></tr>";
	bnc += "<tr class='fila'><td width='30%'>Cuenta Nro.</td><td width='70%' nowrap>" + cta_no + "</td></tr>";
	bnc += "<tr class='fila'><td width='30%'>Tipo Cta.</td><td width='70%' nowrap>" + cta_tipo + "</td></tr>";
	bnc += "<tr class='fila'><td width='30%'>Nombre</td><td width='70%' nowrap>" + cta_nom + "</td></tr>";
	bnc += "<tr class='fila'><td width='30%'>C�dula/Nit</td><td width='70%' nowrap>" + cta_nit + "</td></tr></table>";
	
	tabla += "<tr class='subtitulo1'><th colspan='1'>Detalles Pago por Transferencia Documento: " + fra + "</th></tr>";
	tabla += "<tr><td><div class='scroll' style='overflow:auto ; WIDTH: 100%; HEIGHT:200px;' >";	
	tabla += bnc;	  
	tabla += "</div></td></tr>";	
	tabla += "</table><br>&nbsp;";
	tabla += "<center><a class='Simulacion_Hiper' onClick=\"msg.style.visibility = 'hidden';\" href='JavaScript:void(0);'>Cerrar esta Ventana</a></center><br>&nbsp;";  
	showMsg(tabla);
}
</script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div class="fila" style="position:absolute; visibility:hidden; width:350px; height:50px; z-index:1; border: 2 outset #003399; " id="msg">
</div>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Facturas Proveedor"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<%    
    Usuario usuarioLogin  = (Usuario) session.getAttribute("Usuario");
    String empresa = usuarioLogin.getEmpresa();
    Vector cxp_Docs = model.cxpDocService.getVecCxp_doc();
%>
<table border="2" align="center">
  <tr>
    <td><table width="100%">
      <tr>
        <td width="50%" class="subtitulo1">Lista  de Documentos </td>
        <td width="50%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif"><%=datos[0]%></td>
      </tr>
    </table>      
      <table border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
      <tr class="tblTitulo"  >
        <td nowrap><div align="center"></div></td>
        <td nowrap><div align="center">Nit</div></td>
        <td nowrap><div align="center">Nombre</div></td>
        <td nowrap><div align="center">Documento</div></td>
        <td nowrap><div align="center">Tipo de Doc. </div></td>
        <td nowrap><div align="center">Descripci&oacute;n</div></td>
        <td nowrap><div align="center">Estado</div></td>
        <td nowrap><div align="center">Retenci&oacute;n de Pago </div></td>
        <td nowrap><div align="center">Clase de Documento</div></td>
        <td nowrap><div align="center">Vlr. Doc. </div></td>
        <td nowrap><div align="center">Vlr. en Pesos </div></td>
        <td nowrap><div align="center">Vlr. Pagar</div></td>
        <td nowrap><div align="center">Vlr. Pagar Pesos </div></td>
        <td nowrap>Vlr. Saldo</td>
        <td nowrap><div align="center">Moneda</div></td>
        <td nowrap><div align="center">Pagada</div></td>
        <td nowrap><div align="center">Agencia</div></td>
        <td nowrap><div align="center">Banco</div></td>
        <td nowrap><div align="center">Sucursal</div></td>
        <td nowrap><div align="center">Corrida</div></td>
        <td nowrap><div align="center">Cheque</div></td>
        <td nowrap><div align="center">Doc. Relacionado</div></td>
        <td nowrap><div align="center">Tipo de Doc. Rel.</div></td>
        <td nowrap><div align="center">Ver Documentos<br>
  Relacionados</div></td>
        <td nowrap><div align="center">Ver Cheques <br>
  Relacionados </div></td>
        <td nowrap><div align="center">Ver Cheques <br>
            Pendientes</div></td>
        <td nowrap>Planilla</td>
        <td nowrap><div align="center">Usuario <br>
          Creaci&oacute;n
        </div></td>
        <td nowrap><div align="center">Fecha <br>
          Creaci&oacute;n
        </div></td>
        <td nowrap><div align="center">Fecha Doc. </div></td>
        <td nowrap><div align="center">Fecha Vencimiento </div></td>
        <td nowrap><div align="center">Ultima Fecha Pago</div></td>
        <td nowrap><div align="center">Aprobador</div></td>
        <td nowrap><div align="center">Fecha Aprobaci&oacute;n </div></td>
        <td nowrap><div align="center">Usuario Aprobaci&oacute;n </div></td>
        <td nowrap><div align="center">Fecha de Anulaci&oacute;n</div></td>
        <td nowrap><div align="center">Usuario Anul&oacute;</div></td>
        <td nowrap><div align="center">Tipo de Pago </div></td>
        <td nowrap><div align="center">Fecha Contabilizaci&oacute;n </div></td>
        <td nowrap><div align="center">Usuario Contabiliz&oacute;</div></td>
        <td nowrap><div align="center">Per&iacute;odo</div></td>
        <td nowrap><div align="center">Transacci&oacute;n</div></td>
        <td nowrap><div align="center">Transacci&oacute;n <br>
    Anulaci&oacute;n </div></td>
        <td nowrap><div align="center">Fecha Cont. Anulaci&oacute;n</div></td>
        <%if (empresa.equals("INYM")||empresa.equals("STRK")){%>
            <td nowrap><div align="center"># MS <br>
                       
        <%}%>
      </tr>
<%
      double total = 0;
      for (int i = 0; i < cxp_Docs.size(); i++){
          CXP_Doc doc = (CXP_Doc) cxp_Docs.elementAt(i);
		  String tdoc = "";
		  String tdoc_rel = "";
		  
		  if( doc.getTipo_documento().equals("FAP") ){
			  tdoc = "FACTURA";
		  } else if( doc.getTipo_documento().equals("NC") ){
			  tdoc = "NOTA CREDITO";
		  } else if( doc.getTipo_documento().equals("ND") ){
			  tdoc = "NOTA DEBITO";
		  }
		  
		  if( doc.getTipo_documento_rel().equals("FAP") ){
			  tdoc_rel = "FACTURA";
		  } else if( doc.getTipo_documento_rel().equals("NC") ){
			  tdoc_rel = "NOTA CREDITO";
		  } else if( doc.getTipo_documento_rel().equals("ND") ){
			  tdoc_rel = "NOTA DEBITO";
		  } else if( doc.getTipo_documento_rel().equals("NEG") ){
			  tdoc_rel = "NEGOCIO";
		  }
%>
      <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
        <td class="bordereporte" nowrap><img title="Ver detalles" src="<%= BASEURL %>/images/botones/iconos/detalles.gif" style="cursor:hand"  name="imgsalir"  onClick="window.open('<%= CONTROLLER %>?estado=Factura&accion=Detalle&documento=<%= doc.getDocumento().replaceAll("#","-_-") %>&prov=<%= doc.getProveedor()%>&tipo_doc=<%= doc.getTipo_documento()%>','DETALL','status=yes,scrollbars=no,width=780,height=650,resizable=yes');"></td>
        <td class="bordereporte" nowrap><%= doc.getProveedor() %></td>
        <td class="bordereporte" nowrap><%= doc.getNomProveedor() %></td>
        <td class="bordereporte" nowrap><%= doc.getDocumento() %></td>
        <td class="bordereporte" nowrap><div align="center"><%= tdoc %></div></td>
        <td class="bordereporte" nowrap><%= doc.getDescripcion() %></td>
        <td class="bordereporte" nowrap><div align="center"><%= ( !doc.getReg_status().equals("A"))? "NO ANULADA" : "ANULADA" %></div></td>
        <td class="bordereporte" nowrap><div align="center"><%= doc.getRetencion_pago()!=null && doc.getRetencion_pago().equals("S") ? "SI" : "NO" %></div></td>
        <td class="bordereporte" nowrap><div align="center"><%= ( !doc.getClase_documento().equals("") )? doc.getClase_documento() : "NO REGISTRA"  %></div></td>
        <td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(doc.getVlr_bruto_me()) %></div></td>
        <td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(doc.getVlr_bruto_ml()) %></div></td>
        <td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(doc.getVlr_neto_me()) %></div></td>
        <td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(doc.getVlr_neto()) %></div></td>
        <td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(doc.getVlr_saldo_me()) %></div></td>
        <td class="bordereporte" nowrap><div align="center"><%= doc.getMoneda() %></div></td>
        <td class="bordereporte" nowrap><div align="center"><%=  doc.getVlr_saldo()==0 ? "SI" : "NO" %></div></td>
        <td class="bordereporte" nowrap><div align="center"><%= doc.getAgencia() %></div></td>
        <td class="bordereporte" nowrap><div align="center"><%= doc.getBanco() %></div></td>
        <td class="bordereporte" nowrap><div align="center"><%= doc.getSucursal() %></div></td>
        <td class="bordereporte" nowrap><div align="center"><%= doc.getCorrida().length() == 0 ? "NO REGISTRA" : doc.getCorrida() %></div></td>
        <td class="bordereporte" nowrap><div align="center"><%= doc.getCheque().length() == 0 ? "NO REGISTRA" : doc.getCheque() %></div></td>
        <td class="bordereporte" nowrap><div align="center"><%= doc.getDocumento_relacionado() %></div></td>
        <td class="bordereporte" nowrap><div align="center"><%= tdoc_rel %></div></td>
        <td class="bordereporte" nowrap><div align="center"> <strong>
            <% if ( doc.getTipo_documento().equals("010")  && !doc.getDocumentos_relacionados().equals("") ) { %>
            </strong> <img title="Ver documentos relacionados a la factura." src="<%= BASEURL %>/images/botones/iconos/detalles.gif" style="cursor:hand"  name="imgsalir"  onClick="window.open('<%= CONTROLLER %>?estado=ReporteFacturas&accion=DocRel&documento=<%= doc.getDocumento().replaceAll("#","-_-") %>&prov=<%= doc.getProveedor()%>&tipo_doc=<%= doc.getTipo_documento()%>','DOC_REL','status=yes,scrollbars=no,width=750,height=650,resizable=yes');">
            <% } else { out.print("&nbsp;"); }%>
        </div></td>
        <td nowrap class="bordereporte"><div align="center"><strong>
            <% if ( doc.getTipo_documento().equals("010")  && !doc.getEgresos_relacionados().equals("") ) { %>
            </strong> <img title="Ver cheques relacionados a la factura." src="<%= BASEURL %>/images/botones/iconos/detalles.gif" style="cursor:hand"  name="imgsalir"  onClick="window.open('<%= CONTROLLER %>?estado=ReporteFacturas&accion=DocRel&documento=<%= doc.getDocumento().replaceAll("#","-_-") %>&prov=<%= doc.getProveedor()%>&tipo_doc=<%= doc.getTipo_documento()%>&opc=CHEQ','DOC_REL','status=yes,scrollbars=no,width=750,height=650,resizable=yes');">
            <% } else { out.print("&nbsp;"); }%>
        </div></td>
        <td nowrap class="bordereporte"><div align="center"><strong>
            <% if ( doc.getTipo_documento().equals("010")  && !doc.getEgresos_pendientes().equals("") ) { %>
          </strong> <img title="Ver cheques pendientes por imprimir relacionados a la factura." src="<%= BASEURL %>/images/botones/iconos/detalles.gif" style="cursor:hand"  name="imgsalir"  onClick="window.open('<%= CONTROLLER %>?estado=ReporteFacturas&accion=DocRel&documento=<%= doc.getDocumento().replaceAll("#","-_-") %>&prov=<%= doc.getProveedor()%>&tipo_doc=<%= doc.getTipo_documento()%>&opc=PRECHEQ','DOC_PEND_REL','status=yes,scrollbars=no,width=750,height=650,resizable=yes');">
          <% } else { out.print("&nbsp;"); }%>
        </div></td>
        <td class="bordereporte" nowrap><%= doc.getNumpla() %></td>
        <td class="bordereporte" nowrap><div align="center"><%= doc.getCreation_user() %></div></td>
        <td class="bordereporte" nowrap><div align="center"><%= doc.getCreation_date() %></div></td>
        <td class="bordereporte" nowrap><div align="center"><%= doc.getFecha_documento() %></div></td>
        <td class="bordereporte" nowrap><div align="center"><%= ( !doc.getFecha_vencimiento().equals("0099-01-01") )? doc.getFecha_vencimiento() : "NO REGISTRA"  %></div></td>
        <td class="bordereporte" nowrap><div align="center"><%= ( !doc.getUltima_fecha_pago().equals("0099-01-01") )? doc.getUltima_fecha_pago() : "NO REGISTRA"  %></div></td>
        <td class="bordereporte" nowrap><div align="center"><%= ( !doc.getAprobador().equals("") )? doc.getAprobador() : "NO REGISTRA"  %></div></td>
        <td class="bordereporte" nowrap><div align="center"><%= ( !doc.getFecha_aprobacion().equals("0099-01-01") )? doc.getFecha_aprobacion() : "NO REGISTRA"  %></div></td>
        <td class="bordereporte" nowrap><div align="center"><%= ( !doc.getUsuario_aprobacion().equals("") )? doc.getUsuario_aprobacion() : "NO REGISTRA"  %></div></td>
        <td class="bordereporte" nowrap><div align="center"><%= ( !doc.getFecha_anulacion().equals("0099-01-01 00:00") )? doc.getFecha_anulacion() : "NO REGISTRA"  %></div></td>
        <td class="bordereporte" nowrap><div align="center"><%= ( !doc.getUsuario_anulo().equals("") )? doc.getUsuario_anulo() : "NO REGISTRA"  %></div></td>
        <td class="bordereporte" align="center"><%= doc.getProv_tipo_pago() %>
          <% if( doc.getProv_tipo_pago().compareTo("T")==0 ){%>
          <img src="<%= BASEURL %>/images/iconplus.gif" alt="Ver Detalles Pago por Transferencia." width="18" style="cursor:hand " onClick="showInfoPago ('<%= doc.getDocumento() %>', '<%= doc.getProv_banco_transfer() %>', '<%= doc.getProv_suc_transfer() %>', '<%= doc.getProv_no_cuenta() %>', '<%= doc.getProv_tipo_cuenta() %>', '<%= doc.getProv_no_cuenta() %>', '<%= doc.getProv_cedula_cuenta() %>');" onMouseMove=" //showInfoChq (<%= "" %>); " onMouseOut=" //hiddenMsg(); ">
          <%}%></td>
        <td class="bordereporte" nowrap><div align="center"><%= ( !doc.getFecha_contabilizacion().equals("0099-01-01") )? doc.getFecha_contabilizacion() : "NO REGISTRA"  %></div></td>
        <td class="bordereporte" nowrap><div align="center"><%= ( !doc.getUsuario_contabilizo().equals("") )? doc.getUsuario_contabilizo() : "NO REGISTRA"  %></div></td>
        <td class="bordereporte" nowrap><div align="center"><%= ( !doc.getPeriodo().equals("") )? doc.getPeriodo() : "NO REGISTRA"  %></div></td>
        <td class="bordereporte" nowrap><% 
		   String f  = "FAP";
		   if( doc.getClase_documento().equals("0") ) f = "FAC"; 
		   else if ( doc.getClase_documento().equals("2") ) f = "FNC";  
		   else if ( doc.getClase_documento().equals("3") ) f = "FND"; 

           String fecha = "0099-01-01";
           String transaccion = ( doc.getTransaccion() != 0 )? String.valueOf(doc.getTransaccion()) : "NO REGISTRA";
           %>
          <%if( !transaccion.equals("NO REGISTRA") ){%>
          <span class="Simulacion_Hiper" style="cursor:pointer" onClick="window.open('<%=CONTROLLERCONTAB%>?estado=AdminComprobantes&accion=Manager&opc=ver&dstrct=<%=doc.getDstrct()%>&tipodoc=<%=f%>&numdoc=<%=doc.getDocumento()%>&grupo=<%=doc.getTransaccion()%>&fechaapply=<%=fecha%>','','HEIGHT=600,WIDTH=940,STATUS=YES,SCROLLBARS=YES,RESIZABLE=YES,top=0,left=50')"> <%=  transaccion %> </span>
          <%}else{%>
          &nbsp; <%=  transaccion %>
          <%}%></td>
        <td class="bordereporte" align="center"><% 
		   String f2     =  "FAP";
		   if( doc.getClase_documento().equals("0") ) f2 = "FAC"; 
		   else if ( doc.getClase_documento().equals("2") ) f2 = "FNC";  
		   else if ( doc.getClase_documento().equals("3") ) f2 = "FND";
           String fecha2 = "0099-01-01";
           String transaccion_anulacion = ( doc.getTransaccion_anulacion() != 0 )? String.valueOf(doc.getTransaccion_anulacion()) : "NO REGISTRA";
           %>
          <%if (!transaccion_anulacion.equals("NO REGISTRA")){%>
          <span class="Simulacion_Hiper" style="cursor:pointer" onClick="window.open('<%=CONTROLLERCONTAB%>?estado=AdminComprobantes&accion=Manager&opc=ver&dstrct=<%=doc.getDstrct()%>&tipodoc=<%=f2%>&numdoc=<%=doc.getDocumento()%>&grupo=<%=doc.getTransaccion_anulacion()%>&fechaapply=<%=fecha2%>','','HEIGHT=600,WIDTH=940,STATUS=YES,SCROLLBARS=YES,RESIZABLE=YES,top=0,left=50')"> <%= transaccion_anulacion %> </span>
          <%}else{%>
          &nbsp; <%= transaccion_anulacion %>
          <%}%></td>
        <td class="bordereporte" nowrap><div align="center"><%= ( !doc.getFecha_contabilizacion_anulacion().equals("0099-01-01 00:00") )? doc.getFecha_contabilizacion_anulacion() : "NO REGISTRA"  %></div></td>
        <%if (empresa.equals("INYM")||empresa.equals("STRK")){%>        
        <td class="bordereporte" nowrap><%=doc.getMultiservicio()%></td> 
        <%}%>
      </tr>
<%
	}
%>
    </table></td>
  </tr>
</table>
<br>  
  
<% if( request.getParameter("msg") != null ){%>
<p>
<table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
          <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="58">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<p></p>
<p>
    <%} %>
</p>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr align="left">
    <td><img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" style="cursor:hand " onClick="window.location.href = '<%= CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/cxpagar/reportes&pagina=ReporteFacturas.jsp&marco=no&opcion=33&item=14';" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp; <img src="<%=BASEURL%>/images/botones/exportarExcel.gif" name="c_regresar" id="c_regresar" style="cursor:hand " onClick="window.open('<%=CONTROLLER%>?estado=Reporte&accion=FacturasXLS','XLS','status=yes,scrollbars=no,width=300,height=100,resizable=yes');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">  </tr>
</table>
</div>
<%=datos[1]%>
</body>
</html>


<!--
Entregado  a tito 27 Marzo
-->


