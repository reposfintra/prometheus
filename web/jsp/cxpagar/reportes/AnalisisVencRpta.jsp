<!--
- Autor : Ing. Tito Andr�s Maturana
- Date  : 4 enero del 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la generaci�n del reporte
				de coherecia de reportes en puestos de control.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head><title>An&aacute;lisis de Vencimiento de Facturas</title> 
 <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script>
	function exportar(){
		if(confirm('Desea exportar Reporte Detallado?')){
			window.open('<%= CONTROLLER %>?estado=Analisis&accion=VencCXP&prove=<%= request.getAttribute("prove")%>&fecha=<%= request.getAttribute("lim01")%>&exp=0','EXP','status=yes,scrollbars=yes,width=400,height=150,resizable=no');
		} else {
			window.open('<%= CONTROLLER %>?estado=Analisis&accion=VencCXP&prove=<%= request.getAttribute("prove")%>&fecha=<%= request.getAttribute("lim01")%>&exp=1','EXP','status=yes,scrollbars=yes,width=400,height=150,resizable=no');
		}
	}
</script>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=An�lisis de Vencimiento de Facturas"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<% 
    Vector vec = (Vector) request.getAttribute("detall");
	//System.out.println("... detall: " + vec);
	if (vec.size() > 0) {
%>
<br>
<table border="2" align="center" width="1250">
  <tr>
    <td>
	  <table width="100%">
        <tr>
          <td width="50%" class="subtitulo1">Reporte Detallado </td>
          <td width="50%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
        </tr>
      </table>
	  <table width="1250" border="1" cellpadding="4" cellspacing="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
        <tr class="tblTitulo" align="center">
          <td width="80">NIT</td>
          <td width="350">NOMBRE</td>
          <td width="120">FACTURA</td>
          <td width="80">FECHA</td>
          <td width="80">VENCE</td>
          <td width="130" nowrap>VENCIDA</td>
          <td width="130" nowrap>NO VENCIDA 1 <br><%= request.getAttribute("lim01")%></td>
          <td width="130" nowrap>NO VENCIDA 2 <br><%= request.getAttribute("lim02")%> </td>
          <td width="130" nowrap>NO VENCIDA 3 <br><%= request.getAttribute("lim03")%> </td>
          <td width="130" nowrap>NO VENCIDA 4 <br><%= request.getAttribute("lim04")%> </td>
		</tr>
<%
    for (int i = 0; i < vec.size(); i++){
        Hashtable ht = (Hashtable) vec.elementAt(i);
		if( ht.get("ttl")==null ){			
%>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" style="cursor:hand">
          <td class="bordereporte"><%= ht.get("nit") %></td>
          <td class="bordereporte"><%= ht.get("nomprov") %></td>
          <td class="bordereporte"><%= ht.get("no") %></td>
          <td class="bordereporte" nowrap><%= ht.get("fec").toString().substring(0, 10) %></td>
          <td class="bordereporte" nowrap><%= ht.get("fecvenc") %></td>
          <td class="bordereporte" nowrap><div align="right"><%= ht.get("col01")==null? "&nbsp" :  "" + com.tsp.util.Util.customFormat(Double.parseDouble(ht.get("col01").toString())) %></div></td>
          <td class="bordereporte" nowrap><div align="right"><%= ht.get("col02")==null? "&nbsp" :  "" + com.tsp.util.Util.customFormat(Double.parseDouble(ht.get("col02").toString())) %></div></td>
          <td class="bordereporte" nowrap><div align="right"><%= ht.get("col03")==null? "&nbsp" :  "" + com.tsp.util.Util.customFormat(Double.parseDouble(ht.get("col03").toString())) %></div></td>
          <td class="bordereporte" nowrap><div align="right"><%= ht.get("col04")==null? "&nbsp" :  "" + com.tsp.util.Util.customFormat(Double.parseDouble(ht.get("col04").toString())) %></div></td>
		  <td class="bordereporte" nowrap><div align="right"><%= ht.get("col05")==null? "&nbsp" :  "" + com.tsp.util.Util.customFormat(Double.parseDouble(ht.get("col05").toString())) %></div></td>          
		</tr>
<%
		} else {
%>		
		<tr class="subtitulo1" >
          <td colspan="5" class="bordereporte"><div align="right">Totales</div></td>
          <td class="bordereporte" nowrap><div align="right"><%= ht.get("col01")==null? "&nbsp" :  "" + com.tsp.util.Util.customFormat(Double.parseDouble(ht.get("col01").toString())) %></div></td>
          <td class="bordereporte" nowrap><div align="right"><%= ht.get("col02")==null? "&nbsp" :  "" + com.tsp.util.Util.customFormat(Double.parseDouble(ht.get("col02").toString())) %></div></td>
          <td class="bordereporte" nowrap><div align="right"><%= ht.get("col03")==null? "&nbsp" :  "" + com.tsp.util.Util.customFormat(Double.parseDouble(ht.get("col03").toString())) %></div></td>
          <td class="bordereporte" nowrap><div align="right"><%= ht.get("col04")==null? "&nbsp" :  "" + com.tsp.util.Util.customFormat(Double.parseDouble(ht.get("col04").toString())) %></div></td>
		  <td class="bordereporte" nowrap><div align="right"><%= ht.get("col05")==null? "&nbsp" :  "" + com.tsp.util.Util.customFormat(Double.parseDouble(ht.get("col05").toString())) %></div></td>          
		</tr>
<%  
		}
	}
%>       
      </table>
    </table>
<%} else {%>
    <table width="564" border="2" align="center">
      <tr>
        <td>
	      <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
              <td align="center" class="mensajes">No se encontraron resultados. </td>
              <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
            </tr>
          </table>
	    </td>
      </tr>
  </table>
<%}%>
<p>
<table width="100%" align="center">
  <tr>
  	<td>
      <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="window.location =  '<%= CONTROLLER %>?estado=Analisis&accion=VencCXP&prove=&fecha=<%= request.getAttribute("lim01")%>';">
      <% if (vec.size() > 0) { %>
      <img src="<%=BASEURL%>/images/botones/exportarExcel.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="exportar();">
      <% } %>
</td>
  </tr>
</table>
</div>
<%=datos[1]%>
</body>
</html>
