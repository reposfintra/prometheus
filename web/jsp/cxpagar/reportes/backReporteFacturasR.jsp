<%@ page session="true"%>
<%@ page errorPage="/error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>

<title>Consulta de Facturas</title>
<script language="javascript" src="../../../js/validar.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script>
function showMsg (message){
	var obj = document.getElementById('msg');
	if (obj){
		/*var rightedge = document.body.clientWidth - event.clientX;
		var bottomedge = document.body.clientHeight - event.clientY;
		
		if (rightedge < obj.offsetWidth)
			obj.style.left = document.body.scrollLeft + event.clientX - obj.offsetWidth;
		else
			obj.style.left = document.body.scrollLeft + event.clientX;

		if (bottomedge < obj.offsetHeight)
			obj.style.top = document.body.scrollTop + event.clientY - obj.offsetHeight;
		else
			obj.style.top = document.body.scrollTop + event.clientY;*/
		obj.style.top =  document.body.clientHeight/4;		
		obj.style.left = document.body.clientWidth/4;
		
		obj.innerHTML  = message;
		obj.style.visibility = 'visible';
		//obj.style.top  = event.clientY;
		//obj.style.left = event.clientX;
	}
}
  
function hiddenMsg (){
  var obj = document.getElementById('msg');
  if (obj) obj.style.visibility = 'hidden';
}

function showInfoPago (fra, banco, suc, cta_no, cta_tipo, cta_nom, cta_nit){
	var obj = document.getElementById('msg');
	obj.style.height = '25px';

	var tabla = "<br><table width='97%' align='center' border='2' cellpadding='2' cellspacing='1' bgcolor='#F7F5F4'>";

	var bnc = "<table width='100%' class='tablaInferior'>";
	bnc += "<tr class='fila'><td width='30%'>Banco</td><td width='70%' nowrap>" + banco + "</td></tr>";
	bnc += "<tr class='fila'><td width='30%'>Sucursal</td><td width='70%' nowrap>" + suc + "</td></tr>";
	bnc += "<tr class='fila'><td width='30%'>Cuenta Nro.</td><td width='70%' nowrap>" + cta_no + "</td></tr>";
	bnc += "<tr class='fila'><td width='30%'>Tipo Cta.</td><td width='70%' nowrap>" + cta_tipo + "</td></tr>";
	bnc += "<tr class='fila'><td width='30%'>Nombre</td><td width='70%' nowrap>" + cta_nom + "</td></tr>";
	bnc += "<tr class='fila'><td width='30%'>C�dula/Nit</td><td width='70%' nowrap>" + cta_nit + "</td></tr></table>";
	
	tabla += "<tr class='subtitulo1'><th colspan='1'>Detalles Pago por Transferencia Documento: " + fra + "</th></tr>";
	tabla += "<tr><td><div class='scroll' style='overflow:auto ; WIDTH: 100%; HEIGHT:200px;' >";	
	tabla += bnc;	  
	tabla += "</div></td></tr>";	
	tabla += "</table><br>&nbsp;";
	tabla += "<center><a class='Simulacion_Hiper' onClick=\"msg.style.visibility = 'hidden';\" href='JavaScript:void(0);'>Cerrar esta Ventana</a></center><br>&nbsp;";  
	showMsg(tabla);
}
</script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div class="fila" style="position:absolute; visibility:hidden; width:350px; height:50px; z-index:1; border: 2 outset #003399; " id="msg">
</div>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Facturas Proveedor"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<%    
    Vector cxp_Docs = model.cxpDocService.getVecCxp_doc();
%>
<table border="2" align="center">
  <tr>
    <td><table width="100%">
      <tr>
        <td width="50%" class="subtitulo1">Lista  de Documentos </td>
        <td width="50%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif"><%=datos[0]%></td>
      </tr>
    </table>      
      <table border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
      <tr class="tblTitulo"  >
        <td nowrap>&nbsp;</td>
        <td nowrap><div align="center">Nit</div></td>
        <td ><div align="center">Nombre</div></td>
        <td ><div align="center">Documento</div></td>
        <td nowrap><div align="center">Tipo de Doc.</div></td>
        <td nowrap><div align="center">Doc. Relacionado</div></td>
        <td nowrap><div align="center">Tipo de Doc. Rel.</div></td>
        <td nowrap><div align="center">Descripci&oacute;n</div></td>
        <td ><div align="center">Agencia</div></td>
        <td ><div align="center">Planilla</div></td>
        <td ><div align="center">Moneda</div></td>
        <td ><div align="center">Valor Dodc.</div></td>
        <td ><div align="center">Valor en Pesos </div></td>
        <td ><div align="center">Corrida</div></td>
        <td ><div align="center">Cheque</div></td>
        <td nowrap><div align="center">Pagada</div></td>
        <td ><div align="center">Banco</div></td>
        <td ><div align="center">Sucursal</div></td>
        <td nowrap><div align="center">Tipo de Pago </div></td>
        <td nowrap><div align="center">Fecha Doc. </div></td>
        <td nowrap>
          <div align="center">Fecha Vencimiento </div></td>
        <td nowrap><div align="center">Ultima Fecha Pago </div></td>
        </tr>
<%
      double total = 0;
      for (int i = 0; i < cxp_Docs.size(); i++){
          CXP_Doc doc = (CXP_Doc) cxp_Docs.elementAt(i);
		  String tdoc = "";
		  String tdoc_rel = "";
		  
		  if( doc.getTipo_documento().equals("010") ){
			  tdoc = "FACTURA";
		  } else if( doc.getTipo_documento().equals("035") ){
			  tdoc = "NOTA CREDITO";
		  } else if( doc.getTipo_documento().equals("036") ){
			  tdoc = "NOTA DEBITO";
		  }
		  
		  if( doc.getTipo_documento_rel().equals("010") ){
			  tdoc_rel = "FACTURA";
		  } else if( doc.getTipo_documento_rel().equals("035") ){
			  tdoc_rel = "NOTA CREDITO";
		  } else if( doc.getTipo_documento_rel().equals("036") ){
			  tdoc_rel = "NOTA DEBITO";
		  }
%>
      <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
        <td class="bordereporte" nowrap><img title='Ver detalles' src="<%= BASEURL %>/images/botones/iconos/detalles.gif" style="cursor:hand"  name="imgsalir"  onClick="window.open('<%= CONTROLLER %>?estado=Factura&accion=Detalle&documento=<%= doc.getDocumento().replaceAll("#","-_-") %>&prov=<%= doc.getProveedor()%>&tipo_doc=<%= doc.getTipo_documento()%>','DETALL','status=yes,scrollbars=no,width=780,height=650,resizable=yes');" style="cursor:hand"></td>
        <td class="bordereporte" nowrap><%= doc.getProveedor() %></td>
        <td class="bordereporte" nowrap><%= doc.getNomProveedor() %></td>
        <td class="bordereporte" nowrap><%= doc.getDocumento() %></td>
        <td class="bordereporte" nowrap><%= tdoc %></td>
        <td class="bordereporte" nowrap><%= doc.getDocumento_relacionado() %></td>
        <td class="bordereporte" nowrap><%= tdoc_rel %></td>
        <td class="bordereporte" nowrap><%= doc.getDescripcion() %></td>
        <td class="bordereporte" nowrap><%= doc.getAgencia() %></td>
        <td class="bordereporte" nowrap><%= doc.getNumpla() %></td>
        <td class="bordereporte" nowrap><div align="center"><%= doc.getMoneda() %></div></td>
        <td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(doc.getVlr_neto_me()) %></div></td>
        <td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(doc.getVlr_neto()) %>&nbsp;</div></td>
        <td class="bordereporte" nowrap><div align="center"><%= doc.getCorrida().length() == 0 ? "NO REGISTRA" : doc.getCorrida() %></div></td>
        <td class="bordereporte" nowrap><div align="center"><%= doc.getCheque().length() == 0 ? "NO REGISTRA" : doc.getCheque() %></div></td>
        <td class="bordereporte" nowrap><div align="center"><%=  doc.getVlr_saldo()==0 ? "SI" : "NO" %></div></td>
        <td class="bordereporte" nowrap><%= doc.getBanco() %></td>
        <td class="bordereporte" nowrap><%= doc.getSucursal() %></td>
        <td class="bordereporte" nowrap><%= doc.getProv_tipo_pago() %> <% if( doc.getProv_tipo_pago().compareTo("T")==0 ){%> <img src="<%= BASEURL %>/images/iconplus.gif" alt="Ver Detalles Pago por Transferencia." width="18" style="cursor:hand " onClick="showInfoPago ('<%= doc.getDocumento() %>', '<%= doc.getProv_banco_transfer() %>', '<%= doc.getProv_suc_transfer() %>', '<%= doc.getProv_no_cuenta() %>', '<%= doc.getProv_tipo_cuenta() %>', '<%= doc.getProv_no_cuenta() %>', '<%= doc.getProv_cedula_cuenta() %>');" onMouseMove=" //showInfoChq (<%= "" %>); " onMouseOut=" //hiddenMsg(); "><%}%></td>
        <td class="bordereporte" nowrap><div align="center"><%= doc.getFecha_documento() %></div></td>
        <td class="bordereporte" nowrap><div align="center"><%= ( !doc.getFecha_vencimiento().equals("0099-01-01") )? doc.getFecha_vencimiento() : "NO REGISTRA"  %></div></td>
        <td class="bordereporte" nowrap><div align="center"><%= ( !doc.getUltima_fecha_pago().equals("0099-01-01") )? doc.getUltima_fecha_pago() : "NO REGISTRA"  %></div></td>
        </tr>
<%
	}
%>
    </table></td>
  </tr>
</table>
<br>  
  
<% if( request.getParameter("msg") != null ){%>
<p>
<table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
          <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="58">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<p></p>
<p>
    <%} %>
</p>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr align="left">
    <td><img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" style="cursor:hand " onClick="window.location.href = '<%= CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/cxpagar/reportes&pagina=ReporteFacturas.jsp&marco=no&opcion=33&item=14';" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp; <img src="<%=BASEURL%>/images/botones/exportarExcel.gif" name="c_regresar" id="c_regresar" style="cursor:hand " onClick="window.open('<%=CONTROLLER%>?estado=Reporte&accion=FacturasXLS','XLS','status=yes,scrollbars=no,width=300,height=100,resizable=yes');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">  </tr>
</table>
</div>
<%=datos[1]%>
</body>
</html>



