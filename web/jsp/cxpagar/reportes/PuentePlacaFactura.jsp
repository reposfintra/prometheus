<!--  
     - Author(s)       :      JDELAROSA
     - Date            :      14/03/2007  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description:  Vista que permite realizar actualizacion en los filtros de corridas
 --%>

<%@page    session   ="true"%> 
<%@page    errorPage ="/error/ErrorPage.jsp"%>
<%@page    import    ="com.tsp.operation.model.beans.*"%>
<%@include file      ="/WEB-INF/InitModel.jsp"%>
<%@page    import    ="com.tsp.util.*"%>
<%@page    import    ="java.util.*" %>

<html>
<head>
       <title>Puente Factura</title>       
       <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
</head>
<body>
  
   <%  String  placa    =  request.getParameter("placa").toUpperCase(); 
       String  msj      =  request.getParameter("msj").trim(); 
       if( msj.equals("") ){%>
       
          <script> 
          
                var Ele    = document.createElement("OPTION");
                Ele.value  = '<%= placa  %>';
                Ele.text   = '<%= placa  %>';               
                var selec  =  parent.document.getElementById('placasFiltro');
                selec.add(Ele);
                deleteRepeat( selec );
   
          </script>
           
      <%}else{%>
            <script>  
                  alert('<%=msj%>'); 
            </script>
      <%}%>
   
      <script>
          var ele  =  parent.document.getElementById('placa');
          ele.value='';
          ele.focus();
      </script>
       
</body>
</html>
