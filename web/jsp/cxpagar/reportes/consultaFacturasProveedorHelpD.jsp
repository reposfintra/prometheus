<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
</head>

<body>
<br>


<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Reporte Facturas Proveedor </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Reporte de facturas de un proveedor</td>
        </tr>
        <tr>
          <td class="fila">Nit</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el n&uacute;mero del NIT o la c&eacute;dula de ciudadan&iacute;a del proveedor. El n&uacute;mero de la identificaci&oacute;n se debe digitar sin puntos o comas. </td>
        </tr>
        <tr>
          <td width="149" class="fila"><div align="center">
            <input type="image" src = "<%= BASEURL%>/images/botones/buscar.gif" style="cursor:default ">
          </div></td>
          <td width="525"  class="ayudaHtmlTexto">Bot&oacute;n que inicia la b&uacute;squeda de las facturas del proveedor identificado con el n&uacute;mero digitado en el campo nit.</td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
            <input type="image" src = "<%= BASEURL%>/images/botones/cancelar.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto"> Bot&oacute;n que resetea los campos del formulario. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
            <input type="image" src = "<%=BASEURL%>/images/botones/salir.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que cierra el programa. </td>
        </tr>
        
      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
