<%@ page session="true"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>

<title>Consulta de Facturas</title>
<script language="javascript" src="../../../js/validar.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div class="fila" style="position:absolute; visibility:hidden; width:350px; height:50px; z-index:1; border: 2 outset #003399; " id="msg">
</div>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Facturas Proveedor"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<%    
    Vector cxp_Docs = (Vector) request.getAttribute("Items");
	
	if( cxp_Docs!=null && cxp_Docs.size()!=0 ) {
%>
<table border="2" align="center">
  <tr>
    <td><table width="100%">
      <tr>
        <td width="50%" class="subtitulo1">Lista  de Cheques Pendientes Relacionados </td>
        <td width="50%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif"><%=datos[0]%></td>
      </tr>
    </table>      
      <table border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4" width="100%">
      <tr class="tblTitulo"  >
        <td nowrap>&nbsp;</td>
		<td ><div align="center">Cheque</div></td>
        <td nowrap><div align="center">Banco</div></td>
        <td >Sucursal</td>
        <td ><div align="center">Beneficiario</div></td>
        <td ><div align="center">Moneda</div></td>
        <td ><div align="center">Valor</div></td>
        <td ><div align="center">Agencia</div></td>
        <td ><div align="center">Fecha Creaci&oacute;n </div></td>
        <td ><div align="center">Usuario Cre&oacute; </div></td>
        <%

      double ttl_neto = 0;
	  	  
      for (int i = 0; i < cxp_Docs.size(); i++){
          Hashtable doc = (Hashtable) cxp_Docs.elementAt(i);
		  String dstrct = doc.get("dstrct").toString();
		  String banco = doc.get("branch_code").toString();
		  String sucursal = doc.get("bank_account_no").toString();
		  String doc_no = doc.get("document_no").toString();
		  String moneda = doc.get("currency").toString();
		  String beneficiario = doc.get("beneficiario").toString() + " " + doc.get("nom_beneficiario").toString();
		  String agencia = doc.get("agencia").toString();
		  String fecha_crea = doc.get("creation_date").toString();
		  String user_crea = doc.get("creation_user").toString();
		  double vlr_for = Double.parseDouble(doc.get("vlr_for").toString());
		  
		  ttl_neto += vlr_for;
%>
      <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
        <td class="bordereporte" nowrap><img src="<%= BASEURL %>/images/botones/iconos/detalles.gif" alt="Ver detalle del cheque"  name="imgsalir" style="cursor:hand" title="Ver detalles"  onClick="window.open('<%= CONTROLLER %>?estado=ConsultaPrecheque&accion=Manager&opc=ver&id=<%= doc_no%>','DETALLCHEQ','status=yes,scrollbars=no,width=780,height=650,resizable=yes');"></td>
        <td class="bordereporte" nowrap><%= doc_no %></td>
        <td class="bordereporte" nowrap><%= banco %></td>
        <td class="bordereporte" nowrap><%= sucursal %></td>
        <td class="bordereporte" nowrap><%= beneficiario %></td>
        <td nowrap class="bordereporte"><div align="center"><%= moneda %></div></td>
        <td nowrap class="bordereporte"><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(vlr_for) %></div></td>
        <td class="bordereporte" nowrap><div align="center"><%= agencia %></div></td>
        <td class="bordereporte" nowrap><div align="center"><%= fecha_crea %></div></td>
        <td class="bordereporte" nowrap><div align="center"><%= user_crea %></div></td>
        </tr>	
<%
	}
%>
		  <tr class="subtitulo1">
			<td colspan="6" nowrap class="bordereporte"><div align="right">Totales</div></td>
			<td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(ttl_neto) %></div></td>
			<td colspan="3" nowrap class="bordereporte">&nbsp;</td>
		  </tr>	
    </table></td>
  </tr>
</table>
<br>  
<%
	}
%>  
<% if( request.getParameter("msg") != null ){%>
<p>
<table border="2" align="center">
  <tr>
    <td><table border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
          <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="58">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<p></p>
<p>
    <%} %>
</p>
<table width="441" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr align="left">
    <td><img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" id="c_salir" style="cursor:hand " onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;   </tr>
</table>
</div>
<%=datos[1]%>
</body>
</html>



