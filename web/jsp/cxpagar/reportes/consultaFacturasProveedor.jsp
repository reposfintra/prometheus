<!--
- Autor : Ing. Andr�s Maturana De La Cruz
- Date  : 28 diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la generaci�n del reporte
				de facturas de proveedpr.
--%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Reporte Facturas Proveedor</title>
<script language="javascript" src="<%= BASEURL %>/js/validar.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte Facturas Proveedor"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
	String nit = (request.getParameter("nit")!=null) ? request.getParameter("nit") : "" ;
%>
<form id="forma" name="forma" method="post" action="">
  <table width="301"  border="2" align="center">
    <tr>
      <td><table width="100%">
        <tr>
          <td width="50%" class="subtitulo1">Datos del Proveedor </td>
          <td width="50%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif"><%=datos[0]%></td>
        </tr>
      </table>
        <table width="100%" align="center" cellpadding="3" class="tablaInferior">
          <tr class="fila">
            <td width="60" nowrap>Nit</td>
            <td width="209" nowrap><input name="nit" type="text" class="textbox" id="nit" maxlength="10" value="<%= nit %>" onKeyUp="if (window.event.keyCode==13){validaDatos();}"> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          </tr>
        </table>      </td>
    </tr>
  </table>
  <div align="center"><br>
      <img src="<%= BASEURL%>/images/botones/buscar.gif" style="visibility:visible;cursor:hand" name="imgaceptar" id="imgaceptar" onClick="validaDatos();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp; <img src="<%= BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);"  style="cursor:hand ">&nbsp; <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);"  style="cursor:hand ">
  </div>
</form>
<%if(request.getParameter("mensaje")!=null){%>
	<table width="512" border="2" align="center">
      <tr>
        <td><table width="100%" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
              <td width="282" align="center" class="mensajes">No se encontraron resultados en la b&uacute;squeda</td>
              <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
              <td width="78">&nbsp;</td>
            </tr>
        </table></td>
      </tr>
    </table>
	<%}%>
</div>
<%=datos[1]%>
</body>
</html>
<script>
function validaDatos(){
		document.forma.imgaceptar.src = "<%=BASEURL%>/images/botones/buscarDisable.gif";
		document.imgaceptar.disabled = true;
		forma.action= "<%=CONTROLLER%>?estado=Reporte&accion=FacturasProveedor";
		forma.submit();
}
function validarDatosEnter(){
	if (window.event.keyCode==13){ 
		validaDatos();
	}
}
</script>
