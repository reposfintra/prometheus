<%@ page session="true"%>
<%@ page errorPage="/error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>

<title>|Consulta de Facturas Recurrentes|</title>
<script language="javascript" src="../../../js/validar.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script>
function showMsg (message){
	var obj = document.getElementById('msg');
	if (obj){
		/*var rightedge = document.body.clientWidth - event.clientX;
		var bottomedge = document.body.clientHeight - event.clientY;
		
		if (rightedge < obj.offsetWidth)
			obj.style.left = document.body.scrollLeft + event.clientX - obj.offsetWidth;
		else
			obj.style.left = document.body.scrollLeft + event.clientX;

		if (bottomedge < obj.offsetHeight)
			obj.style.top = document.body.scrollTop + event.clientY - obj.offsetHeight;
		else
			obj.style.top = document.body.scrollTop + event.clientY;*/
		obj.style.top =  document.body.clientHeight/4;		
		obj.style.left = document.body.clientWidth/4;
		
		obj.innerHTML  = message;
		obj.style.visibility = 'visible';
		//obj.style.top  = event.clientY;
		//obj.style.left = event.clientX;
	}
}
  
function hiddenMsg (){
  var obj = document.getElementById('msg');
  if (obj) obj.style.visibility = 'hidden';
}


</script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div class="fila" style="position:absolute; visibility:hidden; width:350px; height:50px; z-index:1; border: 2 outset #003399; " id="msg">
</div>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Facturas Recurrentes"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<%    
    Vector cxp_Docs = model.factrecurrService.getVecCxp_doc();
%>
<table border="2" align="center">
  <tr>
    <td><table width="100%">
      <tr>
        <td width="50%" class="subtitulo1">Lista de Facturas  &nbsp;-&nbsp; <%=cxp_Docs.size()%>&nbsp;registros encontrados</td>
        <td width="50%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif"><%=datos[0]%></td>
      </tr>
    </table>      
      <table border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
      <tr class="tblTitulo"  >
        <td nowrap>&nbsp;</td>
        <td nowrap>&nbsp;</td>
        <td nowrap><div align="center">Nit</div></td>
        <td ><div align="center">Nombre</div></td>
        <td ><div align="center">Documento</div></td>
        <td ><div align="center">Estado</div></td>
        <td nowrap><div align="center">Tipo Recurrente</div></td>
        <td nowrap><div align="center">Descripci&oacute;n</div></td>
        <td ><div align="center">Agencia</div></td>
        <td ><div align="center">Moneda</div></td>
        <td ><div align="center">Valor Doc.</div></td>
        <td ><div align="center">Valor en Pesos </div></td>
        <td nowrap><div align="center">Pagada</div></td>
        <td nowrap><div align="center">Vlr. Saldo</div></td>
        <td ><div align="center">Banco</div></td>
        <td ><div align="center">Sucursal</div></td>
        <td nowrap><div align="center">Fecha Doc. </div></td>
        <td nowrap><div align="center">Fecha Vencimiento </div></td>
        <td nowrap><div align="center"># Cuotas </div></td>
        <td nowrap><div align="center"># Cuotas Transfer</div></td>
        <td nowrap><div align="center">Valor Transfer</div></td>
        <td nowrap><div align="center">Ultima Fecha Transfer </div></td>
        </tr>
        <form action='<%=CONTROLLER%>?estado=ReporteFacturas&accion=Recurrentes&evento=anular' name='form' id='form' method='post'>
<%
      double total = 0;
      for (int i = 0; i < cxp_Docs.size(); i++){
          CXP_Doc doc = (CXP_Doc) cxp_Docs.elementAt(i);
		
%>
      <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
        <td class="bordereporte" nowrap> <input type="checkbox" name="anular" value="<%= doc.getDstrct() + "-_-" + doc.getProveedor() + "-_-" + doc.getDocumento() + "-_-" + doc.getTipo_documento()%>"> </td>
        <td class="bordereporte" nowrap><img title='Ver detalles' src="<%= BASEURL %>/images/botones/iconos/detalles.gif" style="cursor:hand"  name="imgsalir"  onClick="window.open('<%= CONTROLLER %>?estado=FacturaRecurrente&accion=Detalle&documento=<%= doc.getDocumento().replaceAll("#","-_-") %>&prov=<%= doc.getProveedor()%>&tipo_doc=<%= doc.getTipo_documento()%>','DETALL','status=yes,scrollbars=no,width=780,height=650,resizable=yes');" style="cursor:hand"></td>
        <td class="bordereporte" nowrap><%= doc.getProveedor() %></td>
        <td class="bordereporte" nowrap><%= doc.getNomProveedor() %></td>
        <td class="bordereporte" nowrap><%= doc.getDocumento() %> <% if( doc.getNum_cuotas_transfer() > 0 ){%><img title='Ver Facturas Asociadas' src="<%= BASEURL %>/images/botones/iconos/detalles.gif" style="cursor:hand"  name="imgsalir"  onClick="window.open('<%= CONTROLLER %>?estado=ReporteFacturas&accion=Recurrentes&factura=<%= doc.getDocumento()%>&proveedor=<%= doc.getProveedor()%>&tipo_doc=<%= doc.getTipo_documento()%>&evento=buscar','DETALL','status=yes,scrollbars=no,width=780,height=650,resizable=yes');" style="cursor:hand"><%}%></td>
        
        <%String estado = doc.getReg_status().equals("A")? "ANULADA" : "NO ANULADA"; %>
        <% estado = doc.getNum_cuotas() == doc.getNum_cuotas_transfer()? "PAGADA": estado; %>
        <td class="bordereporte" nowrap><%= estado %></td>
        <td class="bordereporte" align="center" nowrap><%= doc.getTipo()%></td>
        <td class="bordereporte" nowrap><%= doc.getDescripcion() %></td>
        <td class="bordereporte" nowrap><%= doc.getAgencia() %></td>
        <td class="bordereporte" nowrap><div align="center"><%= doc.getMoneda() %></div></td>
        <td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(doc.getVlr_neto_me()) %></div></td>
        <td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(doc.getVlr_neto()) %>&nbsp;</div></td>
        <td class="bordereporte" nowrap><div align="center"><%=  doc.getNum_cuotas()==doc.getNum_cuotas_transfer() ? "SI" : "NO" %></div></td>
        <td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(doc.getVlr_saldo_me()) %></div></td>
        <td class="bordereporte" nowrap><%= doc.getBanco() %></td>
        <td class="bordereporte" nowrap><%= doc.getSucursal() %></td>
        <td class="bordereporte" nowrap><div align="center"><%= doc.getFecha_documento() %></div></td>
        <td class="bordereporte" nowrap><div align="center"><%= ( !doc.getFecha_vencimiento().equals("0099-01-01") )? doc.getFecha_vencimiento() : "NO REGISTRA"  %></div></td>
        <td class="bordereporte" nowrap><%= doc.getNum_cuotas() %></td>
        <td class="bordereporte" nowrap><%= doc.getNum_cuotas_transfer() %></td>
        <td class="bordereporte" nowrap><%= com.tsp.util.UtilFinanzas.customFormat2( doc.getVlr_transfer_me() ) %></td>
        <td class="bordereporte" nowrap><div align="center"><%= ( !doc.getUltima_fecha_pago().equals("0099-01-01") )? doc.getUltima_fecha_pago() : "NO REGISTRA"  %></div></td>
        </tr>
<%
	}
%>
        <input type='hidden' name='factura' value='<%=request.getParameter("factura")%>'>
        <input type='hidden' name='tipo_doc' value='<%=request.getParameter("tipo_doc")%>'>
        <input type='hidden' name='proveedor' value='<%=request.getParameter("proveedor")%>'>
        <input type='hidden' name='planilla' value='<%=request.getParameter("planilla")%>'>
        <input type='hidden' name='agencia' value='<%=request.getParameter("agencia")%>'>
        <input type='hidden' name='banco' value='<%=request.getParameter("banco")%>'>
        <input type='hidden' name='sucursal' value='<%=request.getParameter("sucursal")%>'>
        <input type='hidden' name='FechaI' value='<%=request.getParameter("FechaI")%>'>
        <input type='hidden' name='FechaF' value='<%=request.getParameter("FechaF")%>'>
        <input type='hidden' name='pagada' value='<%=request.getParameter("pagada")%>'>
        <input type='hidden' name='anuladas' value='<%=request.getParameter("anuladas")%>'>
        <input type='hidden' name='tipo_factura' value='<%=request.getParameter("tipo_factura")%>'>
    </form>
    </table></td>
  </tr>
</table>
<br>  
  
<% if( request.getParameter("msg") != null ){%>
<p>
<table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
          <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="58">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<p></p>
<p>
    <%} %>
</p>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr align="left">
    <td><img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" style="cursor:hand " onClick="window.location.href = '<%= CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/cxpagar/reportes&pagina=ReporteFacturasRecurrentes.jsp&marco=no&opcion=33&item=14';" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
        &nbsp;&nbsp;&nbsp;<img src="<%=BASEURL%>/images/botones/anular.gif" name="c_anular" id="c_anular" style="cursor:hand " onClick="anular_facturas();" onMouseOut="botonOut(this);" onMouseOver="botonOver(this);"> 
        &nbsp;&nbsp;&nbsp;<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>  
  </tr>
</table>
</div>
<%=datos[1]%>
</body>
</html>

<script>
    
    function anular_facturas(){
        
        var facs = 0;
        
        for( var i=0; i < form.length; i++ ){
            var ele  = form.elements[i];      
            if(ele.type=='checkbox'  && ele.checked){
                facs = 1;
                break;
            }
        }         
        
        if( facs != 0 ){
        
            if( confirm('Desea anular las facturas seleccionadas') ){ 
                anularBtn ( c_regresar  );
                anularBtn ( c_anular  );
                anularBtn ( c_salir  );
                form.submit(); 
            }
                                    
        }else{
            alert('Debe seleccionar las facturas que desea anular');
        }
    }

</script>


