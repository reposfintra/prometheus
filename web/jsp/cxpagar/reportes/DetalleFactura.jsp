<!--
- Autor : Ing. Andr�s Maturana De La Cruz
- Date  : 27.06.2006
- Copyrigth Notice : Transporte Sanchez Polo S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la generaci�n del reporte de facturas de clientes.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
</script>
<title>Detalle Factura</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostspFactura.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Estilo1 {color: #D0E8E8}
-->
</style>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>

<script>
 <%= model.ChequeFactXCorridasSvc.getSucursalesJS() %>
 function loadSucursal(sucursal, banco){
            var vector =  sucursales.split("||");
            var ele    = '';
            for(var i=0;i<vector.length;i++){
                var vectorSuc  = vector[i].split("|");
                if( vectorSuc[0] == banco ){
                    if ( ele != '' )
                         ele += '|';
                    ele += vectorSuc[1];
                }
            }
            
            if( ele != '' ){ 
                var vecEle = ele.split('|');
                sucursal.length = vecEle.length + 1;
                sucursal.options[0].value = '';
                sucursal.options[0].text  = 'Seleccionar';
                sucursal.options[0].selected='selected';
                for(var i=0; i <vecEle.length;i++){
                    sucursal.options[i+1].value = vecEle[i];
                    sucursal.options[i+1].text  = vecEle[i];
                }
            }               
        }
 
        
        function modificarBanco(isOP){
           if(form.banco.value==''){
               alert('El banco no puede estar vacio'); 
               form.banco.focus();
           }else if(form.sucursal.value == ''){
               alert('La sucursal  no puede estar vacia');
               form.sucursal.focus();
           }else if(isOP=='true' && form.plazo.value==''){
                alert('El campo plazo no debe estar vacio')
                form.plazo.focus();
                
           }else{
               document.form.submit(); 
           }
           
        }
</script>

<body onResize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:110px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Reporte Facturas Proveedor"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 109px; overflow: scroll;">
<%
        Usuario usuarioLogin  = (Usuario) session.getAttribute("Usuario");
	CXP_Doc factura = (CXP_Doc) request.getAttribute("Main");
        Vector vItems = (Vector) request.getAttribute("Items");
        boolean isSaldoDif = request.getParameter("isSaldoDif")!=null ?true:false;
        List     bancos  =  model.ChequeFactXCorridasSvc.getListBancos();
        String empresa = usuarioLogin.getEmpresa();
%>
<form name='form' action='<%= CONTROLLER %>?estado=Factura&accion=Servicios&OP=UPDATE_BANCO' method="post">
<input type="hidden" name='distrito' value='<%=factura.getDstrct()%>'>
<input type="hidden" name='proveedor' value='<%=factura.getProveedor()%>'>
<input type="hidden" name='tipo' value='<%=factura.getTipo_documento()%>'>
<input type="hidden" name='documento' value='<%=factura.getDocumento()%>'>


<%
  String mensaje= (String) request.getAttribute("ms");
    if ( mensaje != null && !mensaje.equals("")){
%>

<table border="2" align="center">
  <tr>
    <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="282" align="center" class="mensajes"><%=mensaje%></td>
          <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="78">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<br>
<%
    }   
%>
     



  <table width="980"  border="0" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table border="2"  >
        <tr >
          <td width="1001">
            <table width="100%" align="center"  >
              <tr >
                <td width="50%"  class="subtitulo1"><p align="left">Documento</p></td>
                <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" class="Estilo1" height="20" align="left"><%=datos[0]%></td>
              </tr>
            </table>
            <table width="100%" align="center" cols="7">
              <tr class="filaFactura">
                <td width="103"  >Documento:</td>
                <td width="117" ><%= factura.getDocument_name()%></td>
                <td width="103" ><%= factura.getDocumento() %><% if ( factura.isOP() ) { %>
                  <a href="JavaScript:void(0);" class="Simulacion_Hiper" onClick="window.open('<%=CONTROLLER%>?estado=Consultar&accion=PlanillaRemesa&numeroOC=<%= factura.getPlanilla()%>&numeroOT=<%= factura.getRemesa() %>&distrito=<%= factura.getDstrct() %>','DETOC','scroll=no, resizable=yes')"><br>
                  Consultar Planilla</a>
                  <% } %></td>
                <td width="72" >Proveedor:</td>
                <td width="169" ><%= factura.getProveedor()%></td>
                <td colspan="2" nowrap><div title="Beneficiario: 
<%= factura.getBeneficiario()%>"><%= factura.getNomProveedor()%></div>
                </td>
                <td colspan="1">
                     <%if (empresa.equals("INYM")||empresa.equals("STRK")){%>
                        <label># MS: </label>
                        <%=factura.getMultiservicio()%>
                     <%}%>
                </td>
               
              </tr>
              <!-- tr class="filaFactura">     
              <td>Estado :</td>
              <% /*String estado = ( factura.getReg_status() != null )?factura.getReg_status():""; 
                 estado = ( estado.equals("A"))?"ANULADA":"ACTIVA"; 
                 System.out.println("QUERY " + estado );*/
                %>
              
              <td colspan='7'><% //=estado%></td>
              </tr -->
			  <tr class="filaFactura">                
                <td >Estado :</td>
                <td colspan="2" ><% String estado = ( factura.getReg_status() != null )?factura.getReg_status():""; 
                 estado = ( estado.equals("A"))?"ANULADA":"ACTIVA"; 
                 System.out.println("QUERY " + estado );
                %><%=estado%></td>
                <td >Fecha :</td>
                <td colspan="2"  ><%= factura.getFecha_documento()%></td>
                <td width="121" colspan="-1" >Plazo(d&iacute;as):</td>
                 <input type="hidden" name='fechaDoc' value='<%= factura.getFecha_documento()%>'>
                <td width="266" colspan="-1" >
                <%if(factura.isOP() && isSaldoDif){%> 
                    <input type="text" maxlength='2' size='3' name='plazo'  onkeypress="Digitos(event,'DecNO');" value='<%= factura.getPlazo()%>'>
                <%}else{%>
                    <input type="hidden" name='plazo'  value='<%= factura.getPlazo()%>'>
                    <%= factura.getPlazo()%>
                   
                <%}%>
                </td>
              </tr>
              <tr class="filaFactura">                
                <td >Documento Rel:</td>
                <% String docu_rel = factura.getDocumentos_relacionados(); %>
                <% String cheques  = factura.getEgresos_relacionados();%>
                
                <!-- Si es factura y tiene cheques o NC-ND -->
                <%if( factura.getTipo_documento().equals("010") &&  ( (docu_rel!=null && !docu_rel.equals("")) || (cheques!=null && !cheques.equals("")) ) ){%>
                    <td colspan='2'>
                        <table>
                        <%if( docu_rel!=null && !docu_rel.equals("") ){%>
                            <tr><td style='cursor:hand; height:10' class='Simulacion_Hiper' onClick="window.open('<%= CONTROLLER %>?estado=ReporteFacturas&accion=DocRel&documento=<%= factura.getDocumento().replaceAll("#","-_-") %>&prov=<%= factura.getProveedor()%>&tipo_doc=<%= factura.getTipo_documento()%>','','status=yes,scrollbars=no,width=750,height=650,resizable=yes');" >Documentos relacionados</td></tr>
                        <%}%>
                        <% if( cheques!=null && !cheques.equals("") ){ %>
                            <tr><td style='cursor:hand; height:10' class='Simulacion_Hiper' onClick="window.open('<%= CONTROLLER %>?estado=ReporteFacturas&accion=DocRel&documento=<%= factura.getDocumento().replaceAll("#","-_-") %>&prov=<%= factura.getProveedor()%>&tipo_doc=<%= factura.getTipo_documento()%>&opc=CHEQ','DOC_REL','status=yes,scrollbars=no,width=750,height=650,resizable=yes');">Cheques relacionados</td></tr>
                        <%}%>
                        </table>
                    </td>
                <%}else{ %>
                    <td ><%= factura.getDocumento_rel_name()%></td>
                    <td ><%= factura.getDocumento_relacionado()%></td>
                <%}%>
                <td >Fecha Venc.:</td>
                <td colspan="2"  ><%=factura.getFecha_vencimiento()%></td>
                <td width="121" colspan="-1" >Corrida:</td>
                <td width="266" colspan="-1" ><%= factura.getCorrida()%></td>
              </tr>
              <tr class="filaFactura">
                <td><p>Valor:</p></td>
                <td colspan="2" ><div align="right"><%= !factura.getMoneda().equals("PES")? com.tsp.util.UtilFinanzas.customFormat2(factura.getVlr_bruto_me()) : com.tsp.util.UtilFinanzas.customFormat(factura.getVlr_bruto_me())%></div></td>
                <td >Moneda:</td>
                <td colspan="2" >
                  <div  id="CabMoneda"><%= factura.getMoneda()%> </div></td>
                <td colspan="-1" >Banco:</td>
                <td colspan="-1" >
                   <%if(isSaldoDif){%>
                               <select  style='width:80%'  name='banco'  onchange='loadSucursal(this.form.sucursal, this.value);'>
                                                  <option value=''> Seleccionar </option>
                                                  <% for(int i=0;i<bancos.size();i++){
                                                         String code = (String) bancos.get(i);%>
                                                         <option value='<%=code%>' <%=(code.equals(factura.getBanco()))?"selected":""%>> <%=code%> </option>
                                                  <%}%>
                                              </select> 
                   <%}else{%>
                  <div  id="CabBanco"><%= factura.getBanco()%> </div>
                  <%}%>
                 </td>
              </tr>
              <tr class="filaFactura">
                <td >Valor a Pagar: </td>
                <td colspan="2"><div align="right"><%= !factura.getMoneda().equals("PES")? com.tsp.util.UtilFinanzas.customFormat2(factura.getVlr_neto_me()) : com.tsp.util.UtilFinanzas.customFormat(factura.getVlr_neto_me())%></div></td>
                <td>Moneda:</td>
                <td colspan="2"><%= factura.getMoneda()%></td>
                <td colspan="-1">Sucursal:</td>
                <td colspan="-1" >
                  <%if(isSaldoDif){%> 
                      <select  style='width:80%'  name='sucursal'  id='sucursal'>
                          <option value='<%= factura.getSucursal()%>'><%= factura.getSucursal()%> </option>
                      </select>
                   <%}else{%>
                       <div  id="CabSucursal"><%= factura.getSucursal()%> </div>
                   <%}%>  
                </td>
              </tr>
              </form>
              <tr class="filaFactura">
                <td >Descripci&oacute;n:</td>
                <td colspan="5"><textarea name="descripcion"  cols="70" rows="1" readonly class="textareaFactura"><%= factura.getDescripcion()%></textarea></td>
                <td colspan="-1">Autorizador: </td>
                <td colspan="-1">
                  <div  id="CabAutorizador">
                    <div><%= (factura.getUsuario_aprobacion()!= null)?factura.getUsuario_aprobacion():""%> </div>
                </div></td>
              </tr>
              <tr>
                <td class="filaFactura">Observaci&oacute;n: </td>
                <td colspan="5" class="fila"><table width="100%">
                    <tr class="filaFactura">
                      <td width="58%"><textarea name="observacion"  cols="70" rows="3" readonly class="textboxFactura"><%= factura.getObservacion()%></textarea></td>
                  </tr>
                </table></td>
                <td valign="top" class="filaFactura"  >Handle Code: </td>
                <td valign="top" class="filaFactura" ><%= (factura.getHandle_code()!= null)?factura.getHandle_code():""%></td>
              </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
	<tr>
      <td>
<!-- DETALLE DE LA FACTURA-->
<table border="2" width = '980' >
        <tr >
          <td>
            <table width="100%" align="center"  >
              <tr >
                <td width="50%"  class="subtitulo1"><p align="left">Detalle del Documento </p></td>
                <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" class="Estilo1" height="20" align="left"></td>
              </tr>
            </table>
            <table id="detalle" width="100%" >
              <tr  id="fila1" class="tblTituloFactura">
                <td align="center" width="50">Item</td>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr align="center"  class="tblTituloFactura">
                      <td width="13">&nbsp;</td>
                      <td width="310" align="left">Descripcion</td>
                      <td width="160" align="left">Concepto</td>
                      <td width="130" align="center">Planilla</td>
                      <td align="left" >Centro</td>
                      <td width="250"  align="left" colspan='3'>ABC</td>
                    </tr>
                  </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr align="center" class="tblTituloFactura">
                        <td width="14">&nbsp;</td>
                        <td width="150" align="left">Cuenta</td>
                        <td width="200" align="left" >Auxiliar</td>
                        <td width="170" align="left">Valor</td>
                        <td width="125" align="left" title='<%=factura.getTotalIva()%>'>IVA</td>
                        <td width="100" align="left" title='<%=factura.getTotalRiva()%>'>RIVA</td>
                        <td width="110" align="left" title='<%=factura.getTotalRica()%>'>RICA</td>
                        <td width="100" align="left" title='<%=factura.getTotalRfte()%>'>RFTE</td>
                        <td width="125" align="left">Valor Neto</td>
                      </tr>
                  </table></td>
              </tr>
<% 
for( int i=0; i<vItems.size(); i++){
	CXPItemDoc item = (CXPItemDoc) vItems.elementAt(i);
	Vector Copia = item.getVCopia();
	String iva = "";
        String riva = "";
	String rica = "";
	String rfte = "";
        double Vlr_iva = 0;
        double Vlr_riva = 0;
        double Vlr_rica = 0;
        double Vlr_rfte = 0;
        
	for(int j=0;j< Copia.size();j++){ 
		CXPImpItem impuestoCopia = (CXPImpItem)Copia.elementAt(j); 
		String impuesto = impuestoCopia.getCod_impuesto();
                double vlr      = impuestoCopia.getVlr_total_impuesto();
		String tipo 	= impuestoCopia.getTipo_impuesto();
		if( tipo!=null && tipo.equals("IVA") ) {
			iva = impuesto;
                        Vlr_iva = vlr;
		}else if( tipo!=null && tipo.equals("RIVA") ) {
			riva = impuesto;
                        Vlr_riva = vlr;
		} else if( tipo!=null && tipo.equals("RICA") ) {
			rica = impuesto;
                        Vlr_rica = vlr;
		} else if( tipo!=null && tipo.equals("RFTE") ) {
			rfte = impuesto;
                        Vlr_rfte = vlr;
		}
	}
		
%>
           <tr class="filaFactura"  nowrap  bordercolor="#D1DCEB" >
		<td nowrap><%=i+1%></a></td>
		    <td>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
                         <tr align="center">
                            
                            <td   nowrap  align="left" width='310'>&nbsp;<textarea name="desc"  id="desc" cols="55" rows="1" class="textboxFactura"  readonly ><%= item.getDescripcion()%></textarea></td>
                            <td   nowrap align="left" width='180'><input name="descripcion_i" id="descripcion_i" type="text" size="30" class="textboxFactura"  maxlength="4" readonly value='<%= item.getConcepto()%>'> </td>
                            <td   nowrap width='100'><input name="planilla" id="planilla" type="text" size="8"  class="textboxFactura" readonly  maxlength="8" value='<%= item.getPlanilla()%>' ></td>
                            <td   nowrap width='100'><input name="oc" type="text" id="oc" size="6" maxlength="3" class="textboxFactura" readonly value='<%=item.getCodcliarea()%>'></td>                            
                            <td   nowrap width='250' colspan='3' align="left"><input name="codigo_abc" style="text-transform:uppercase" id="codigo_abc" type="text" size="4" maxlength="4" class="textboxFactura" readonly value='<%= item.getCodigo_abc()%>'></td>
                         </tr>
                       </table>
		       
                       <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr align="center">
			      <td width="108"> &nbsp;<input name="codigo_cuenta" id="codigo_cuenta" class="textboxFactura" type="text" size="14" maxlength="20" readonly  value='<%= item.getCodigo_cuenta()%>'></td>
			      <td align="center" width="200"><input type="text" maxlength='25'  style='width:90' name='auxiliar' id='auxiliar' class="textboxFactura" readonly  value='<%= item.getAuxiliar()%>'></td> 
                              <td width="239"><input name="valor1"  class="textboxFactura"   id="valor1" type="text" size="16"  style="text-align:right;"   maxlength="11"  align="right" readonly value='<%= !factura.getMoneda().equals("PES")? com.tsp.util.UtilFinanzas.customFormat2(item.getVlr_me()) : com.tsp.util.UtilFinanzas.customFormat(item.getVlr_me()) %>'></td>
			      <td width="125" align="left"><input name="impuesto"   style="text-transform:uppercase" id ="impuesto" class='textareaFactura' type="text" size="8" maxlength="13" readonly value='<%= (Vlr_iva == 0)?"":String.valueOf(Vlr_iva)%>'  title='<%= iva%>'></td>
                              <td width="100" align="left"><input name="impuesto"   style="text-transform:uppercase" id ="impuesto" class='textareaFactura' type="text" size="8" maxlength="13" readonly value='<%= (Vlr_riva ==0)?"":String.valueOf(Vlr_riva)%>' title='<%= riva%>'></td>
                              <td width="120" align="left"><input name="impuesto"   style="text-transform:uppercase" id ="impuesto" class='textareaFactura' type="text" size="8" maxlength="13" readonly value='<%= (Vlr_rica ==0)?"":String.valueOf(Vlr_rica)%>' title='<%= rica%>'></td>
                              <td width="100" align="left"><input name="impuesto"   style="text-transform:uppercase" id ="impuesto" class='textareaFactura' type="text" size="8" maxlength="13" readonly value='<%= (Vlr_rfte ==0)?"":String.valueOf(Vlr_rfte)%>' title='<%= rfte%>'></td>
                              <td width="143"><input name="valorNeto" id="valorNeto" class="textboxFactura"  style="text-align:right;" type="text" size="16"   maxlength="11" align="right" readonly value='<%= !factura.getMoneda().equals("PES")? com.tsp.util.UtilFinanzas.customFormat2(item.getVlr()) : com.tsp.util.UtilFinanzas.customFormat(item.getVlr()) %>'></td>
			    </tr>
                        </table>
                      </td>
	      </tr>  
 
 
<% } %>
</table>
<!-- FIN DETALLE DE LA FACTURA -- >
	</td>
    </tr>
  </table>
  <br>
  
  <br>
  <center>
     <%if(isSaldoDif){%> 
          <img src="<%=BASEURL%>/images/botones/modificar.gif" name="c_salir" onClick="modificarBanco('<%=factura.isOP()%>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
     <%}%>
    <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
  </center>
</div>
<%=datos[1]%>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</body>
</html>
