<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
  <title>Generaci�n de Corridas - Ayuda</title>
  <META http-equiv=Content-Type content="text/html; charset=windows-1252">
  <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
  <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body>
<center>


<% String BASEIMG = BASEURL +"/images/ayuda/cxpagar/extractos/"; %>

<table width="100%"  border="2" align="center">
    <tr>
      <td >
 
            <table width='99%' align="center" cellpadding='0' cellspacing='0'>
                 <tr class="subtitulo" ><td height="20"><div align="center">MANUAL DE GENERACI�N DE CORRIDAS </div></td></tr>
                 <tr class="subtitulo1"> <td>Descripci&oacute;n del funcionamiento </td> </tr>

        
                 <tr>
                      <td  class="ayudaHtmlTexto" height='40'>
                         El programa permitir� generar las corridas, para eso deber� seleccionar los filtros que desee aplicar
                         en la generaci�n.<br>
                         Ofrece cuatro(4) tipo de Filtros:
                          <ul>
                             <li> Distrito  </li>
                             <li> Banco     </li>
                             <li> Proveedor : Filtrar por nit � tambi�n por tipo de proveedor (HC)</li>
                             <li> Fecha de creaci�n de la factura</li>
                          </ul>                         
                         Para la selecci�n de dichos filtros, se presentar� una vista como lo indica la figura 1.
                      </td>
                 </tr>
                         
                 <tr><td  align="center" > 
                     <br><br>
                     <img  src="<%= BASEIMG%>Filtros.JPG" >                  
                     <br>
                     <strong>Figura 1</strong>
                     <br><br>
                     <br><br>
                 </td>
                 </tr>
                  
                 <tr>
                      <td  class="ayudaHtmlTexto" height='40'>
                         Deber� realizar click en la casilla que est� al lado de cada filtro que desee aplicar y posteriormente realizar click en
                         el bot�n Aceptar.
                         El programa tomar� dichos filtros y buscar� informaci�n correspondiente a cada uno de ellos teniendo en cuenta
                         la agencia del usuario.
                         <br><br>
                      </td>
                 </tr>
          

                 
                 
                 
                 
           </table>
            
      </td>
  </tr>
</table>
<br><br>
<img src="<%=BASEURL%>/images/botones/salir.gif"      name="exit"    height="21"  title='Salir'                onClick="parent.close();"                 onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">          

</body>
</html>
