<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
  <title>Generaci�n de Corridas - Ayuda</title>
  <META http-equiv=Content-Type content="text/html; charset=windows-1252">
  <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
  <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body>
<center>


<% String BASEIMG = BASEURL +"/images/ayuda/cxpagar/extractos/"; %>

<table width="100%"  border="2" align="center">
    <tr>
      <td >
 
            <table width='99%' align="center" cellpadding='0' cellspacing='0'>
                 <tr class="subtitulo" ><td height="20"><div align="center">MANUAL DE GENERACI�N DE CORRIDAS </div></td></tr>
                 <tr class="subtitulo1"> <td>Descripci&oacute;n del funcionamiento </td> </tr>

        
                 <tr>
                      <td  class="ayudaHtmlTexto" height='40'>
                         Una vez se hayan establecidos los filtros a aplicar, se deber� dar valores a los mismos. El programa permite
                         distintos filtros para generar la corrida, los cuales se describir�n a continuaci�n:
                         <ul>
                             <li>Distrito</li>
                             <li>Banco</li>
                             <li>Proveedor</li>
                             <li>Fecha de factura</li>
                         </ul>
                      </td>
                 </tr>
                 
                 
                 <tr>
                     <td  class="ayudaHtmlTexto" >
                        <b>Distrito:</b><br>
                         Se muestra el distrito al cual pertenece el usuario en sessi�n,  tal como lo indica la figura 1.
                     </td>
                </tr>   
                 
                <tr>
                    <td  align="center" > 
                         <br>
                         <img  src="<%= BASEIMG%>Distrito.JPG" >                  
                         <br>
                         <strong>Figura 1</strong>
                         <br><br><br><br>
                     </td>
                </tr>
                
                 
                 <tr>
                     <td  class="ayudaHtmlTexto" >
                        <b>Banco:</b><br>
                         Se muestran los bancos asociados a la agencia del usuario y del mismo distrito. Si el usurio pertenece  a "Oficina Principal (OP)"
                         se muestran todos los bancos asociados a su distrito.
                         Para la selecci�n de los bancos a la corrida se muestra un formulario como lo indica la figura 2
                     </td>
                </tr>   
                 
                 <tr>
                    <td  align="center" > 
                         <br>
                         <img  src="<%= BASEIMG%>Bancos.JPG" >                  
                         <br>
                         <strong>Figura 2</strong>
                         <br><br><br><br>
                     </td>
                </tr>
                
                
                <tr>
                     <td  class="ayudaHtmlTexto" >
                        Para la selecci�n de bancos, deber� hacer uso de los botones de Agregar y Quitar bancos a la corrida. Tales botones se describen en la
                        figura 3.
                        Para lo cual deber� seleccionar los bancos que desee agregar y/o quitar a la corrida y luego dar click en el bot�n deseado.
                     </td>
                </tr>  
                
                <tr>
                    <td  align="center" > 
                         <br>
                         <img  src="<%= BASEIMG%>Seleccion.JPG" >                  
                         <br>
                         <strong>Figura 3</strong>
                         <br><br><br>
                     </td>
                </tr>
                
                
                <tr>
                     <td  class="ayudaHtmlTexto" >
                       Si desea realizar filtro por sucursales especificas de los bancos seleccionados para la corrida, deber� realizar click en link
                       "Ver Sucursales" descrito en la figura 2, le aparecer� un ventana en la cual podr� seleccionar la(s) sucursal(es) deseada para el banco
                       tal como lo indica la figura 4.
                     </td>
                </tr>  
                
                
                 <tr>
                    <td  align="center" > 
                         <br>
                         <img  src="<%= BASEIMG%>Sucursales.JPG" >                  
                         <br>
                         <strong>Figura 4</strong>
                         <br><br><br>
                     </td>
                </tr>
                
                
                 <tr>
                     <td  class="ayudaHtmlTexto" >
                        <b>Proveedor:</b><br>
                        Se presenta el filtro seleccionado para la corrida, ya sea por nit o por tipo de proveedor.<br>
                        Por nit, se presenta un formulario como lo indica la figura 5, en el cual se presenta los proveedores asociados a la agencia
                        y los proveedores para la corrida.
                        La forma de agregar o quitar proveedores para la corrida se maneja de la misma forma que para los bancos.
                        
                     </td>
                </tr>   
                 
                
                <tr>
                    <td  align="center" > 
                         <br>
                         <img  src="<%= BASEIMG%>Nit.JPG" >                  
                         <br>
                         <strong>Figura 5</strong>
                         <br><br><br>
                     </td>
                </tr>
                
                
                <tr>
                     <td  class="ayudaHtmlTexto" >
                        
                        Por tipo de proveedor, se presenta una lista de tipo de proveedores en la cual deber� seleccionar que tipo desea para la corrida, 
                        como lo indica la figura 6.
                        
                     </td>
                </tr>   
                
                
                <tr>
                    <td  align="center" > 
                         <br>
                         <img  src="<%= BASEIMG%>HC.JPG" >                  
                         <br>
                         <strong>Figura 6</strong>
                         <br><br><br>
                     </td>
                </tr>
                
                 
                <tr>
                     <td  class="ayudaHtmlTexto" >
                        <b>Fecha de Factura:</b><br>
                        Deber� seleccionar el rango de fecha en el cual se generar�n las facturas que desea incluir en la corrida, le aparecer� 
                        un formulario como lo indica la figura 7.
                     </td>
                </tr>   
                
                
                <tr>
                    <td  align="center" > 
                         <br>
                         <img  src="<%= BASEIMG%>Fechas.JPG" >                  
                         <br>
                         <strong>Figura 7</strong>
                         <br><br><br>
                     </td>
                </tr>
                
                
                
                
                <tr>
                     <td  class="ayudaHtmlTexto" >
                        <br><br><br>
                        Cada filtro mencionado anteriormente, aparecer� siempre y cuando sea seleccionado.
                        En la parte inferior del formulario de generaci�n de corrida, aparecer� una lista de botones que permiten realizar acciones,
                        tal como lo indica la figura 8.
                     </td>
                </tr>  
                
                
                <tr>
                    <td  align="center" > 
                         <br>
                         <img  src="<%= BASEIMG%>Botones.JPG" >                  
                         <br>
                         <strong>Figura 8</strong>
                         <br><br><br>
                     </td>
                </tr>
                
                
                 <tr>
                     <td  class="ayudaHtmlTexto" >
                        <br><br><br>
                        Al dar click en el bot�n  "Aceptar", el programa generar� la corrida a partir de los datos establecidos, este proceso
                        se ejecutar� en una clase Threads y su estado de ejecuci�n lo podr� visualizar en el log de procesos del sitio.
                        <br><br><br><br><br><br>
                     </td>
                </tr>  
                
                
                
          </table>
            
      </td>
  </tr>
</table>
<br><br>
<img src="<%=BASEURL%>/images/botones/salir.gif"      name="exit"    height="21"  title='Salir'                onClick="parent.close();"                 onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">          

                 





</body>
</html>
