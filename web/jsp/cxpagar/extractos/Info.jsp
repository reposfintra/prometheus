<!--  
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      18/10/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description:  Vista que permite capturar detalle de los filtros
 --%>

<%@page    session   ="true"%> 
<%@page    errorPage ="/error/ErrorPage.jsp"%>
<%@page    import    ="com.tsp.operation.model.beans.*"%>
<%@include file      ="/WEB-INF/InitModel.jsp"%>
<%@page    import    ="com.tsp.util.*"%>
<%@page    import    ="java.util.*" %>
<html>
<head>
    <title>Información</title>       
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body>
  <center>

<% Hashtable info = (Hashtable)session.getAttribute("infocorrida");
   if( info != null ){%>
       
            <table width='98%'   class='tablaInferior' cellpadding='0' cellspacing='0'   >       
                 <tr>
                        <td colspan='2'>
                               <table cellpadding='0' cellspacing='0' width='100%'>
                                       <tr class="fila">
                                              <td align="left" width='55%' class="subtitulo1">&nbsp INFORMACIÓN DE LA CORRIDA</td>
                                              <td align="left" width='*' class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"></td>
                                       </tr>
                               </table>
                      </td>
                 </tr>
                 
                 <tr>
                     <td colspan='2'>
                     
                             <table width='100%'   class='tablaInferior'   > 
                  
                                 <tr  class="fila" valign='top'>                                        
                                         <TD width='40%'   class='informacion'>&nbspDISTRITO</TD>
                                         <TD width='*'     class='informacion'><%=(String)info.get("dstrct")%></TD>
                                 </tr>
                                 <tr  class="fila" valign='top'>                                        
                                         <TD    class='informacion'>&nbspCORRIDA</TD>
                                         <TD    class='informacion'><%=(String)info.get("corrida")%></TD>
                                 </tr>
                                 <tr  class="fila" valign='top'>                                        
                                         <TD    class='informacion'>&nbspTIPO DE PAGO</TD>
                                         <TD    class='informacion'><%=(String)info.get("tpago")%></TD>
                                 </tr>
                                 <tr  class="fila" valign='top'>                                        
                                         <TD    class='informacion'>&nbspTIPO DE VIAJE</TD>
                                         <TD    class='informacion'><%=(String)info.get("tviaje")%></TD>
                                 </tr>
                                 <tr  class="fila" valign='top'>                                        
                                         <TD    class='informacion'>&nbspFECHA DE CUMPLIDO</TD>
                                         <TD    class='informacion'><%=(String)info.get("fechacumini")%> &nbsp <%=(String)info.get("fechacumfin")%></TD>
                                 </tr>
                                 <tr  class="fila" valign='top'>                                        
                                         <TD    class='informacion'>&nbspBANCOS</TD>
                                         <TD    class='informacion'><%=(String)info.get("bancos")%></TD>
                                 </tr>
                                 <tr  class="fila" valign='top'>                                        
                                         <TD    class='informacion'>&nbspTIPO PROVEEDOR</TD>
                                         <TD    class='informacion'><%=(String)info.get("fproveedor")%></TD>
                                 </tr>
                                 <tr  class="fila" valign='top'>                                        
                                         <TD    class='informacion'>&nbspPROVEEDORES</TD>
                                         <TD    class='informacion'><%=(String)info.get("proveedores")%></TD>
                                 </tr>
                                 <tr  class="fila" valign='top'>                                        
                                         <TD    class='informacion'>&nbspPLACAS</TD>
                                         <TD    class='informacion'><%=(String)info.get("placas")%></TD>
                                 </tr>
                                 <tr  class="fila" valign='top'>                                        
                                         <TD    class='informacion'>&nbspFECHA DE VENCIMIENTO</TD>
                                         <TD    class='informacion'><%=(String)info.get("fechavenini")%> &nbsp <%=(String)info.get("fechavenfin")%></TD>
                                 </tr>
                                 <tr  class="fila" valign='top'>                                        
                                         <TD    class='informacion'>&nbspCHEQUES EN CERO</TD>
                                         <TD    class='informacion'><%=(String)info.get("cheque_cero")%></TD>
                                 </tr>
                          </table>
                          
                    </td>
                </tr>
                
                <tr>
                        <td colspan='2'>
                               <table cellpadding='0' cellspacing='0' width='100%'>
                                       <tr class="fila">
                                              <td align="left" width='55%' class="subtitulo1">&nbsp CHEQUES GENERADOS</td>
                                              <td align="left" width='*' class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"></td>
                                       </tr>
                               </table>
                      </td>
                 </tr>
                 
                 
                  <tr>
                     <td colspan='2' width='100%'>
                     
                             <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center"  > 
                  
                                 <tr   class="tblTitulo"  valign='top'>                                        
                                         <TH width='20%'   class='informacion'>BANCO    </TH>
                                         <TH width='20%'   class='informacion'>SUCURSAL </TH>
                                         <TH width='20%'   class='informacion'>CHEQUE   </TH>
                                         <TH width='20%'   class='informacion'>FECHA    </TH>
                                         <TH width='20%'   class='informacion'>USUARIO  </TH>
                                 </tr>
                                 
                                 <%List cheques = (List) info.get("cheques");
                                   for(int i=0;i<cheques.size();i++){
                                       Hashtable ch = (Hashtable) cheques.get(i);%> 
                                       
                                       <tr class='<%= (i%2==0?"filagris":"filaazul") %>'  valign='top' class="bordereporte">                                        
                                             <TD   align='center'><%=(String)ch.get("banco")   %>    </TD>
                                             <TD   align='center'><%=(String)ch.get("sucursal")%>    </TD>
                                             <TD   align='center'><%=(String)ch.get("cheque")  %>    </TD>
                                             <TD   align='center'><%=(String)ch.get("fecha")   %>    </TD>
                                             <TD   align='center'><%=(String)ch.get("user")    %>    </TD>
                                       </tr>
                                       
                                  <%}%>
                                 
                                 
                         
                          </table>
                          
                    </td>
                </tr>
                 
                 
          </table>
       
   <%}else{%>
   
       <table border="2" align="center">
          <tr>
            <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
              <tr>
                <td width="470" align="center" class="mensajes">No se encontró información de la corrida</td>
                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                <td width="58">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
      </table>
       
   <%}%>
   
  <p>
    <img src="<%=BASEURL%>/images/botones/salir.gif"          name="exit"    height="21"          onClick="parent.close();"                                                                 onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
  </p>



</body>
</html>
