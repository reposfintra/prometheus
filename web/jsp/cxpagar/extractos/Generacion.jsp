<!--
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      18/10/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description:  Vista que permite capturar detalle de los filtros
 --%>

<%@page    session   ="true"%> 
<%@page    errorPage ="/error/ErrorPage.jsp"%>
<%@page    import    ="com.tsp.operation.model.beans.*"%>
<%@include file      ="/WEB-INF/InitModel.jsp"%>
<%@page    import    ="com.tsp.util.*"%>
<%@page    import    ="java.util.*" %>


<html>
<head>


    <title>Generacion de Corridas</title>
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validar.js"></script> 
    
    <link rel="stylesheet" type="text/css" href="/fintra/css/jCal/jscal2.css" />
    <script type="text/javascript" src="/fintra/js/jCal/jscal2.js"></script>
    <script type="text/javascript" src="/fintra/js/jCal/lang/es.js"></script>
    
    
<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>

    
     <!-- VARIABLES -->
  <%  Hashtable  filtros      =  model.ExtractosSvc.getFiltros();        
      Hashtable  datosFiltros =  model.ExtractosSvc.getDatosFiltros();   
      String     msj          =  request.getParameter("msj");            
      String     hoy          =  Utility.getHoy("-"); 
      String     hoyHora      =  Util.getFechaActual_String(9).substring(0,16);
      String     activo       =  "ON";              %>
      
      
     <!-- FILTROS -->
  <%  String  chDistrito      = (String) filtros.get("distrito");
      String  chBancos        = (String) filtros.get("banco");    
      String  chProveedor     = (String) filtros.get("proveedor");
      String  filtroProveedor = (String) filtros.get("filtroProveedor");
      String  chFechas        = (String) filtros.get("fecha"); 
      String  ckViaje         = (String) filtros.get("viaje"); 
      String  ckPlaca         = (String) filtros.get("placa");%>
                            
         
    <script>     
          function anularBtn(btn){ 
              btn.style.visibility='hidden';
          }
              
          function activarBtn(btn){
             btn.style.visibility='';            
          }
          
          function sendFiltros(theForm, opcion){ 
		// alert("MENSAJE TEMPORAL:"+theForm.proveedoresFiltro.options[0].value);               
                  var sw = 0;
                  anularBtn(acept);
                  theForm.evento.value =opcion;                  
                  if(opcion=='DATOSFILTROS'){
                          <% if( chDistrito.equals(activo) ) {%>  
                                 if( theForm.distrito.value=='' )
                                     sw = 1;
                          <%}%>
                          <% if( chBancos.equals(activo) )   {%>  
                                 selectAll( theForm.bancosFiltro );
                                 selectAll( theForm.sucursalesFiltro ); 
                                 if( theForm.bancosFiltro.value==''  )
                                     sw = 1;
                          <%}%>                          
                          <% if( chProveedor.equals(activo) ){%>                                  
                                <% if( filtroProveedor != null ){%>                                
                                      selectAll( theForm.proveedoresFiltro );
                                      if( theForm.proveedoresFiltro.value=='' )
                                          sw = 1;
                               <%}%>                                     
                          <%}%>
                          
                          <% if( ckPlaca.equals(activo) ){%>
                                 selectAll( theForm.placas      );
                                 if( theForm.placas.value=='' )
                                     sw = 1;
                          <%}%>
                          
                          
                  }
                  if( sw == 1){
                      alert('Hay filtros sin establecer sus valores, por favor seleccionelos...');
                      activarBtn(acept);
                      desabledALL(theForm.bancosFiltro);
                      desabledALL(theForm.sucursalesFiltro);
                      desabledALL(theForm.proveedoresFiltro);
                  }
                  else{
                      if( theForm.activarInclude.checked ) {
                          if(  theForm.corrida.value=='' ){
                             alert('Deber�  digitar la corrida a la cual desea incluir los nuevos valores');
                             activarBtn(acept);
                             theForm.corrida.focus();   
                          }else
                             theForm.submit();
                      }else
                        theForm.submit();                      
                  }
          }
          
          
          
          
          
          function  loadSucursales(theForm, opcion){
              var sw = 0;              
              theForm.evento.value =opcion;              
              if(opcion=='SUCURSALES'){
                       selectAll( theForm.bancosFiltro );                       
                       if ( theForm.bancosFiltro.selectedIndex!=-1){
                              var parameter  = theForm.action; 
                              for(i=0;i<theForm.length;i++)   {
                                 if (theForm.elements[i].type=='select-multiple'  &&  theForm.elements[i].name=='bancosFiltro' ){
                                      var cmb = theForm.elements[i];
                                      for (j=0;j<cmb.length;j++)
                                         if (cmb[j].selected)
                                             parameter += '&'+ theForm.elements[i].name + '=' +  cmb[j].value;
                                 }
                                 else
                                   parameter += '&'+ theForm.elements[i].name + '=' +  theForm.elements[i].value;
                              }
                              var win =window.open(parameter,'Sucursales',' scrollbars=yes, top=100,left=200, width=450, height=500,  status=yes, resizable=yes  ');
                              win.focus();
                       }
                       else
                           sw=1;
                       desabledALL(theForm.bancosFiltro);
               }
               
               if(opcion=='PLACAS'){
                     selectAll( theForm.proveedoresFiltro ); 
                     if ( theForm.proveedoresFiltro.selectedIndex!=-1){
                          var parameter  = theForm.action + '&evento=PLACAS';
                          
                          for (j=0;j<theForm.proveedoresFiltro.length;j++)
                               parameter += '&'+ theForm.proveedoresFiltro.name + '=' +  theForm.proveedoresFiltro[j].value;      
                         
                          var win =window.open(parameter,'Placas',' scrollbars=yes, top=100,left=200, width=450, height=500,  status=yes, resizable=yes  ');
                          win.focus();                            
                     }else
                          sw=1;                          
                     desabledALL(theForm.proveedoresFiltro);               
               }
               
               if( sw == 1){
                     if (  opcion=='SUCURSALES' ) alert('Deber� seleccionar bancos para poder mostrar sucursales...');
                     if (  opcion=='PLACAS' )     alert('Deber� establecer los nit a los cuales desea filtrar por placa...');
               }
              
          }
          
          
          
          
          function desabledALL(cmb){
              for (i=0;i<cmb.length;i++)
                  cmb[i].selected = false;
          }
          
          
         function searchCombo(data, cmb){
               data = data.toLowerCase();
               for (i=0;i<cmb.length;i++){
                  cmb[i].selected = false; 
               }           
               for (i=0;i<cmb.length;i++){                
                    var txt = cmb[i].text.toLowerCase( );
                    if( txt.indexOf(data)==0 ){
                        cmb[i].selected = true;
                        break;
                   }
                   else
                      cmb[i].selected = false;
                }
          }
          
    </script>
    
    
</head>
<BODY>
  

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Generar Corridas de Pagos"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>
   
     
      <form action='<%=CONTROLLER%>?estado=Generar&accion=Extractos' method='post' name='formulario' >
      
          <table width="650" border="2" align="center">
              <tr>
                 <td ALIGN='center'>  
                         <table width='100%'   class='tablaInferior'   >       
                              <tr>
                                 <td colspan='2'>
                                   <table cellpadding='0' cellspacing='0' width='100%'>
                                       <tr class="fila">
                                              <td align="left" width='55%' class="subtitulo1">&nbsp FILTROS DE GENERACI�N</td>
                                              <td align="left" width='*' class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                                       </tr>
                                   </table>
                                </td>
                              </tr>
                              
                                
                              
                              
                              <TR class="fila">   
                                 <TD colspan='2' width='100%'>
                                     <table cellpadding='0' cellspacing='0' width='100%' >
                                        <tr  class="fila">                                        
                                             <TD width='50%'   class='informacion'>
                                                  <table cellpadding='0' cellspacing='0' width='100%' >
                                                      <tr >
                                                          <td class="fila"> &nbsp TIPO DE PAGO                                                       </td>
                                                          <td class='informacion'> <input type='radio' name='tpago' value='B'  >Cheque   
                                                                                   &nbsp&nbsp
                                                                                   <input type='radio' name='tpago' value='T'  checked >Transferencia 
                                                                                   &nbsp&nbsp
                                                          </td>
                                                          
                                                          <td class='informacion'>
                                                              &nbsp&nbsp&nbsp&nbsp
                                                              <input type='checkbox'    name='activarInclude' id='activarInclude' onClick=" if( this.checked ){ form.corrida.disabled=false ; form.corrida.focus();  }else{  form.corrida.value=''; this.form.corrida.disabled=true; }" > &nbsp <B>INCLUIR EN LA CORRIDA &nbsp
                                                              <input type='text'  style='text-align:center' disabled  name='corrida' maxlength='7' size='7' onKeyPress="soloDigitos(event,'decNO')" title='Digite el n�mero de la corrida en la cual desea incluir el resultado de esta'>                                                                                               
                                                          
                                                          </td>
                                                          
                                                          
                                                          <td class='informacion'>
                                                              <B>CHEQUE CERO <input type='checkbox'    name='cheque_cero'  id='cheque_cero'  >                                                           
                                                          </td>
                                                          
                                                      </tr>
                                                  </table>
                                            </TD>
                              
                                           
                                        
                                        </tr>
                                     </table>
                                 </TD>
                              
                              
                              
                              
                                           
                             </TR>
                                    
                                    
                                  
                                                         
                             <TR class="fila"  >
                                       <TD width='25%' >&nbsp DISTRITO</TD>
                                       <TD width='*'>
                                           <select name='distrito' id='distrito' class="textbox"   style='width:20%'> 
                                           <% List distritos = (List)datosFiltros.get("distrito");
                                              for (int i=0;i<distritos.size();i++){
                                                  String dist = (String)distritos.get(i);%> 
                                                  <option value='<%=dist%>'> <%=dist%>
                                          <% }%>
                                           </select>
                                           
                                       </TD>
                                       
                                       
                             </TR>
                             
                               
                               
                               
                               <!-- VIAJES -->   
                               <% if( ckViaje.equals(activo) ){%>    
                           
                                    <TR class="fila">
                                           <TD width='25%' >&nbsp TIPO DE VIAJES</TD>
                                           <TD width='*'>
                                               <select id='tipoViajes' name='tipoViajes' class="textbox"   style='width:32%'> 
                                                      <option value='C'> CARBON
                                                      <option value='G'> GENERAL
                                               </select>
                                           </TD>
                                    </TR>
                                    
                                    
                                    <TR class="fila" >
                                           <TD colspan='2'  height='20'>&nbsp RANGO DE FECHAS VIAJES (CUMPLIDO) </TD>
                                    </TR>
                                    
                                     
                                    <TR class="fila" >
                                           <TD colspan='2'  height='20' align='center'>
                                                  <table cellpadding='0' cellspacing='0'  width='90%'>
                                                      <tr  class="fila">
                                                           <td  width='18%'  align='center' style="font-size:12"> Fecha Inicial</td>
                                                           <td  width='35%'  align='center'>
                                                               
                                                                 <input type='text' class="textbox" id='fechaIniViaje' name='fechaIniViaje' readonly value="<%= hoyHora %>" style="width: 85%;">
                                                                 <img src="/fintra/images/cal.gif" id="imgFechaIniViaje" alt="fecha" title="Seleccion de fecha" />
                                                                    <script type="text/javascript">
                                                                        Calendar.setup({
                                                                            inputField: "fechaIniViaje",
                                                                            trigger: "imgFechaIniViaje",
                                                                            align: "top",
                                                                            showTime: true,
                                                                            dateFormat: "%Y-%m-%d %H:%M:00",
                                                                            onSelect: function () {
                                                                                this.hide();
                                                                            }
                                                                        });
                                                                    </script>                                                           
                                                           </td>
                                                           <td  width='20%'  align='center' style="font-size:12"> Fecha Final </td>
                                                           <td  width='*'    align='center' >
                                                              
                                                                 <input type='text' class="textbox" id='fechaFinViaje' name='fechaFinViaje' readonly value="<%= hoyHora %>" style="width: 85%;">                                                        
                                                                 <img src="/fintra/images/cal.gif" id="imgFechaFinViaje" alt="fecha" title="Seleccion de fecha" />
                                                                    <script type="text/javascript">
                                                                        Calendar.setup({
                                                                            inputField: "fechaFinViaje",
                                                                            trigger: "imgFechaFinViaje",
                                                                            align: "top",
                                                                            showTime: true,
                                                                            dateFormat: "%Y-%m-%d %H:%M:00",
                                                                            onSelect: function () {
                                                                                this.hide();
                                                                            }
                                                                        });
                                                                    </script>    
                                                           </td>                                                      
                                                      </tr>
                                                  </table>
                                                  <BR>
                                           </TD>
                                    </TR>
                                    
                                    
                               <%}%>
                               
                               
                               
                               <!-- BANCOS -->
                               <% if( chBancos.equals(activo) ){%> 
                                      <TR class="fila" >
                                           <TD colspan='2'  height='20'>&nbsp BANCOS</TD>
                                      </TR>
                                      <TR class="fila">
                                           <TD  colspan='2' >
                                                 <table cellpadding='0' cellspacing='0' width='100%'>
                                                     <tr class="fila">
                                                         <td width='45%' align='center' style="font-size:12">
                                                                Banco de Agencia
                                                                
                                                                <input type='text' id='busqueda' name='busqueda' style="width:90%" title='Buscar por palabras' onkeyup='searchCombo(this.value, this.form.bancos)'>

                                                                <select multiple size='10' class='select' style='width:90%' id='bancos' name='bancos' onChange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text; }">                                                     
                                                                 <% List bancos = (List)datosFiltros.get("banco");
                                                                    for (int i=0;i<bancos.size();i++){
                                                                          String banck = (String)bancos.get(i);%> 
                                                                          <option value='<%=banck%>'> <%=banck%>
                                                                  <% }%>
                                                                </select>
                                                                <br><br>
                                                         </td>
                                                         <td width='10%' align='center'>
                                                                 <img src='<%=BASEURL%>/images/botones/envDer.gif' style='cursor:hand' name='imgEnvDer'  onclick="move   (bancos,  bancosFiltro); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>             
                                                                 <img src='<%=BASEURL%>/images/botones/envIzq.gif' style='cursor:hand' name='imgEnvIzq'  onclick="move   (bancosFiltro, bancos ); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>                                                             
                                                         </td>
                                                         <td width='45%' align='center' style="font-size:12">
                                                                <br>
                                                                Banco de la Corrida
                                                                <select multiple size='12' class='select' style='width:90%' name='bancosFiltro' id='bancosFiltro' onchange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text; }"></select>

                                                                <br>
                                                                <a href='#' class='Simulacion_Hiper' onClick="loadSucursales(formulario,'SUCURSALES');"> <font style="font-size:12" >Ver Sucursales<font></a>

                                                         </td> 
                                                     </tr>
                                                 </table>
                                           </TD>
                                      </TR>
                                      
                                                                            
                                      <TR class="fila" >
                                               <TD colspan='2'  height='20'  align='center'  style="font-size:12">&nbsp SUCURSALES POR BANCOS</TD>
                                      </TR>
                                      <TR class="fila" >
                                               <TD colspan='2' align='center'>    
                                                     <select   multiple size='7'   name='sucursalesFiltro' style="width:95%"  >
                                                     </select>
                                                     <br><br>
                                               </TD> 
                                      </TR>
                                      
                               <%}%>
                               
                               
                               
                               
                               <!-- PROVEEDORES -->
                               <% if( chProveedor.equals(activo) ){%> 
                                      <TR class="fila">
                                           <TD colspan='2'  height='20'>&nbsp PROVEEDORES</TD>
                                      </TR>
                                      
                                     <% if( filtroProveedor != null ){%>
                                     
                                     
                                     
                                             <!-- POR TIPO -->
                                              <%if( filtroProveedor.equals("HC") ){%>                                             
                                                 <TR class="fila">
                                                       <TD  colspan='2'  style="font-size:12" >
                                                       
                                                       
                                                           <table cellpadding='0' cellspacing='0' width='100%'>
                                                                 <tr class="fila">
                                                                     <td width='45%' align='center' style="font-size:12" >
                                                                                Tipo HC de Proveedores:

                                                                               <select id='hcProveedor' name='hcProveedor' multiple size='10' class='select' style='width:90%'>
                                                                                  <% List proveedores = (List)datosFiltros.get("tipoProveedores");
                                                                                     for(int i=0;i<proveedores.size();i++){
                                                                                        Hashtable   tipo = (Hashtable)proveedores.get(i);
                                                                                        String      code = (String) tipo.get("codigo");
                                                                                        String      desc = (String) tipo.get("descripcion");%>
                                                                                        <option value='<%=code%>'><%=desc %>
                                                                                    <%}%>
                                                                                 </select>
                                                                                 <br><br>
                                                                                 
                                                                     </td>
                                                                     <td width='10%' align='center'>
                                                                             <img src='<%=BASEURL%>/images/botones/envDer.gif' style='cursor:hand' name='imgEnvDer'  onclick="move   (hcProveedor,  proveedoresFiltro); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>             
                                                                             <img src='<%=BASEURL%>/images/botones/envIzq.gif' style='cursor:hand' name='imgEnvIzq'  onclick="move   (proveedoresFiltro, hcProveedor ); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>                                                             
                                                                     </td>
                                                                     <td width='45%' align='center' style="font-size:12">
                                                                                 HC para de la Corrida:
                                                                                 <select multiple size='10' class='select' style='width:90%' id='proveedoresFiltro' name='proveedoresFiltro' onchange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text; }"></select>
                                                                                 <br><br>
                                                                     </td> 
                                                                 </tr>
                                                             </table>
                                                       
                                                          
                                                       </TD>
                                                  </TR>  
                                              <%}%>
                                              
                                              

                                              <!-- POR LISTADO NOMBRES -->
                                              <%if( filtroProveedor.equals("LISTADO") ){%>
                                                  <TR class="fila">
                                                       <TD  colspan='2'>
                                                             <table cellpadding='0' cellspacing='0' width='100%'>
                                                                 <tr class="fila">
                                                                     <td width='45%' align='center' style="font-size:12" >
                                                                                Proveedores de Agencia:

                                                                                <input type='text' id='busquedaProv' name='busquedaProv' style="width:90%" title='Buscar por palabras' onkeyup='searchCombo(this.value, this.form.proveedores)'>

                                                                                <select multiple size='10' class='select' style='width:90%' id='proveedores' name='proveedores' onChange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text; }">                                                     
                                                                                 <% List proveedores = (List)datosFiltros.get("proveedores");
                                                                                    for (int i=0;i<proveedores.size();i++){
                                                                                          Hashtable  pro   = (Hashtable)proveedores.get(i);
                                                                                          String     code  = (String) pro.get("nit");
                                                                                          String     name  = (String) pro.get("nombre");%> 
                                                                                          <option value='<%=code%>'> <%=name%>
                                                                                  <% }%>
                                                                                </select>
                                                                                <br><br>
                                                                     </td>
                                                                     <td width='10%' align='center'>
                                                                                 <img src='<%=BASEURL%>/images/botones/envDer.gif' style='cursor:hand' name='imgEnvDer'  onclick="move   (proveedores,  proveedoresFiltro); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>             
                                                                                 <img src='<%=BASEURL%>/images/botones/envIzq.gif' style='cursor:hand' name='imgEnvIzq'  onclick="move   (proveedoresFiltro, proveedores ); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>                                                             
                                                                     </td>
                                                                     <td width='45%' align='center' style="font-size:12">
                                                                                  Proveedores de la Corrida:<br>
                                                                                  <select multiple size='12' class='select' style='width:90%' id='proveedoresFiltro' name='proveedoresFiltro' onchange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text; }"></select>
                                                                     </td> 
                                                                 </tr>
                                                             </table>
                                                       </TD>
                                                  </TR>
                                            <%}%>
                                            
                                            
                                            
                                           <%if( filtroProveedor.equals("NIT") ){%>
                                                    
                                            <TR class="fila">
                                                       <TD  colspan='2'>
                                                             <table cellpadding='0' cellspacing='0' width='100%' align='center'>
                                                             
                                                                 <tr class="fila">                                                                     
                                                                     <td width='50%' style="font-size:12" >                                                                     
                                                                           &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp NIT &nbsp                                                                   
                                                                           <input type='text' name='busquedaNIT' id='busquedaNIT' maxlength='15' style='width:25%' title='Buscar por Nit' >                                                                           
                                                                           <img src='<%=BASEURL%>/images/botones/iconos/validar.gif' width='20'   style=" cursor:hand"    name='i_load'      onclick="if( busquedaNIT.value==''){ alert('Deber� digitar un nit de proveedor'); busquedaNIT.focus(); }else{  enviar('?estado=Generar&accion=Extractos&evento=VALIDARNIT&nit='+ busquedaNIT.value )  } "  title='Aplicar nit a la Corrida'   >                                                                          
                                                                     </td>
                                                                     <!--
                                                                     <td align='center'>
                                                                          <a href='#' class='Simulacion_Hiper' onClick="loadSucursales(formulario,'PLACAS');"> <font style="font size:12" >Aplicar Placas<font></a>
                                                                     </td>-->
                                                                 </tr>
                                                                 
                                                                 <tr class="fila">                                                                     
                                                                     <td width='90%' colspan='2'  align='center' style="font-size:12" > 
                                                                         <select multiple size='8' class='select' style='width:95%' id='proveedoresFiltro' name='proveedoresFiltro'></select>                                                                          
                                                                      </td> 
                                                                 </tr>
                                                                 
                                                             </table>
                                                       </TD>
                                                  </TR>
                                                  <!--
                                                  <TR class="fila" >
                                                           <TD colspan='2'  height='20' align='center'  style="font-size:12">PLACAS POR NIT</TD>
                                                  </TR>
                                                  <TR class="fila" >
                                                           <TD colspan='2' align='center'>    
                                                                 <select   multiple size='7'   name='placasFiltro' style="width:95%"  >
                                                                 </select>
                                                                 <br><br>
                                                           </TD> 
                                                  </TR>-->
                                            
                                            <%}%>
  
                                              
                                     <%}%>
                                      
                               <%}%>
                               
                               
                               
                               
                               <!-- PLACAS -->
                               <% if( ckPlaca.equals(activo) ){%>   
      
                                     <TR class="fila">
                                             <td width='100%' style="font-size:12" colspan='2'  >                                                                     
                                                   &nbsp <b> PLACA  &nbsp                                                                 
                                                   <input type='text' name='busquedaPLACA' id='busquedaPLACA' maxlength='8' style='width:12%' title='Buscar por Placas' >                                                                           
                                                   <img src='<%=BASEURL%>/images/botones/iconos/validar.gif' width='20'   style=" cursor:hand"    name='i_load'      onclick="if( busquedaPLACA.value==''){ alert('Deber� digitar el n�mero de la placa'); busquedaPLACA.focus(); }else{  enviar('?estado=Generar&accion=Extractos&evento=VALIDARPLACA&placa='+ busquedaPLACA.value )  } "  title='Aplicar placa a la Corrida'   >                                                                          
                                             </td>
                                             
                                      </TR>                                
                                      <TR class="fila">                                                                          
                                            <td width='90%' colspan='2'  align='center' style="font-size:12" > 
                                                  <select multiple size='8' class='select' style='width:95%' id='placas' name='placas'></select>                                                                          
                                            </td> 
                                      </TR>
                                                 
                                   <%}%>   
                                      
                                      
                               
                               
                               <!-- FECHAS  FACTURAS-->
                               <% if( chFechas.equals(activo) ){%> 
                                    <TR class="fila" >
                                           <TD colspan='2'  height='20'>&nbsp RANGO DE FECHAS POR VENCIMIENTO DE FACTURAS </TD>
                                    </TR>
                                     
                                    <TR class="fila" >
                                           <TD colspan='2'  height='20' align='center'>
                                                  <table cellpadding='0' cellspacing='0'  width='90%'>
                                                      <tr  class="fila">
                                                           <td  width='18%'  align='center' style="font-size:12"> Fecha Inicial</td>
                                                           <td  width='35%'  align='center'>
                                                               
                                                                 <input type='text' class="textbox" id='fechaIni' name='fechaIni' readonly value="<%= hoy %>" style="width: 85%;">    
                                                                 <img src="/fintra/images/cal.gif" id="imgFechaIni" alt="fecha" title="Seleccion de fecha" />
                                                                    <script type="text/javascript">
                                                                        Calendar.setup({
                                                                            inputField: "fechaIni",
                                                                            trigger: "imgFechaIni",
                                                                            align: "top",
                                                                            onSelect: function () {
                                                                                this.hide();
                                                                            }
                                                                        });
                                                                    </script>                                                            
                                                           </td>
                                                           <td  width='20%'  align='center' style="font-size:12"> Fecha Final </td>
                                                           <td  width='*'    align='center' >
                                                              
                                                                 <input type='text' class="textbox"  id='fechaFin' name='fechaFin' readonly value="<%= hoy %>" style="width: 85%;">                                                        
                                                                 <img src="/fintra/images/cal.gif" id="imgFechaFin" alt="fecha" title="Seleccion de fecha" />
                                                                    <script type="text/javascript">
                                                                        Calendar.setup({
                                                                            inputField: "fechaFin",
                                                                            trigger: "imgFechaFin",
                                                                            align: "top",
                                                                            onSelect: function () {
                                                                                this.hide();
                                                                            }
                                                                        });
                                                                    </script>   
                                                           
                                                           </td>                                                      
                                                      </tr>
                                                  </table>
                                                  <BR>
                                           </TD>
                                    </TR>
                               <%}%>
                               
                              
                              
                               
                               
                         </table>
                 </td>
              </tr>
           </table>
           
           <input  type='hidden' name='evento' >
           
     </form>
                 
     
         
          
     <p>
           <img src="<%=BASEURL%>/images/botones/aceptar.gif"        name="acept"   height="21"          onClick="sendFiltros(formulario,'DATOSFILTROS');"                                         onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">          
           <img src="<%=BASEURL%>/images/botones/regresar.gif"       name="regre"   height="21"          onClick="window.location.href='<%=BASEURL%>/jsp/cxpagar/extractos/FiltrosCorridas.jsp'"   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
           <img src="<%=BASEURL%>/images/botones/salir.gif"          name="exit"    height="21"          onClick="parent.close();"                                                                 onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
     </p>
     
     
     
     
     
     
      <!-- mensajes -->
    <% if(msj!=null && !msj.equals("")){%>        
         <table border="2" align="center">
          <tr>
            <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
              <tr>
                <td width="470" align="center" class="mensajes"><%=msj%></td>
                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                <td width="58">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
        </table>
    <%  } %>
 
  
     
</div> 
 <%=datos[1]%> 
 
 
<script>
    function enviar(url){
         var accion = "<iframe name='ejecutor' style='visibility:hidden'  src='<%= CONTROLLER %>" + url + "'> ";
         frameEjecutor.innerHTML = accion;
    }	
</script>

<font id='frameEjecutor'></font>

</body>
</html>
