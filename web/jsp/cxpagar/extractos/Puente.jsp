<!--  
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      10/11/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description:  Vista que permite realizar actualizacion en los filtros de corridas
 --%>

<%@page    session   ="true"%> 
<%@page    errorPage ="/error/ErrorPage.jsp"%>
<%@page    import    ="com.tsp.operation.model.beans.*"%>
<%@include file      ="/WEB-INF/InitModel.jsp"%>
<%@page    import    ="com.tsp.util.*"%>
<%@page    import    ="java.util.*" %>

<html>
<head>
       <title>Puente Corrida</title>       
       <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
</head>
<body>
  
   <% 
       String  evento   =  request.getParameter("evento");
       String  select   =  request.getParameter("select");
       String  elemento =  request.getParameter("ele");
       String  valor    =  "";
       String  texto    =  "";
       String  msj      =  request.getParameter("msj"); 
       if( msj.equals("") ){

           if( evento.equals("VALIDARNIT")  ){
               String  nombre =  request.getParameter("nombre");
               String  nit    =  request.getParameter("nit"); 
                       valor  =  nit;
                       texto  =  nombre;
           }

           if( evento.equals("VALIDARPLACA")  ){
               String  placa =  request.getParameter("placa");
                       valor =  placa;
                       texto =  placa;  
           }%>
                      
           <script>           
                    var Ele    = document.createElement("OPTION");
                    Ele.value  = '<%= valor    %>';
                    Ele.text   = '<%= texto    %>';               
                    var selec  =  parent.document.getElementById('<%= select %>');
                    selec.add(Ele);
                    deleteRepeat( selec );
            </script> 
           
           
      <%}else{%>
            
            <script>alert('<%=msj%>');</script>
            
      <%}%>
   
      <script>                  
           var ele  =  parent.document.getElementById('<%= elemento %>');
           ele.value='';
           ele.focus();
     </script>
       
</body>
</html>
