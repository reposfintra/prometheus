<!--
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      18/10/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description:  Vista que permite capturar sucursales de los bancos
 --%>

<%@page    session   ="true"%> 
<%@page    errorPage ="/error/ErrorPage.jsp"%>
<%@page    import    ="com.tsp.operation.model.beans.*"%>
<%@include file      ="/WEB-INF/InitModel.jsp"%>
<%@page    import    ="com.tsp.util.*"%>
<%@page    import    ="java.util.*" %>


<html>
<head>

    <title>Lista de Sucursales</title>
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js">   </script>
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/corrida.js">   </script>
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/calendartsp/"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 




    
</head>
<body>

<center>

  <% List sucursales  = model.ExtractosSvc.getSucursales();  %>

   <form  method='post' name='formulario' >
        <table width="400" border="2" align="center">
              <tr>
                 <td ALIGN='center'>  
                         <table width='100%'   class='tablaInferior'   >       
                              <tr>
                                     <td colspan='2'>
                                           <table cellpadding='0' cellspacing='0' width='100%'>
                                               <tr class="fila">
                                                      <td align="left" width='55%' class="subtitulo1">&nbsp SUCURSALES DE  BANCOS </td>
                                                      <td align="left" width='*' class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                                               </tr>
                                           </table>
                                    </td>
                              </tr>                              
                              
                              <%  if( sucursales.size() > 0){
                                      String bancoActual = "";
                                      for (int i=0; i<sucursales.size(); i++ ){
                                           Hashtable sucursal = (Hashtable) sucursales.get(i);
                                           String  banco  = (String) sucursal.get("banco");
                                           String  suc    = (String) sucursal.get("sucursal");
                                           if( ! bancoActual.equals( banco ) ){
                                                 bancoActual = banco;%>
                                                  <tr class='fila'>
                                                      <td  colspan='2'>  <%= banco%>  </td>                                         
                                                  </tr> 
                                               
                                          <%}%>                                
                                          <tr class='fila'>
                                              <td width='5%'                     >  <input type='checkbox' value='<%=banco+suc%> ' name='<%=  "["+banco + "] - "+  suc %>' > </td>
                                              <td width='*'  style="font size:12">  <%= suc %>             </td>                                              
                                          </tr>                                           
                                     <% }
                                  }%>
                              
                         </table>
                    </td>
                </tr>
         </table>
   </form>
   
   
    <p>
           <img src="<%=BASEURL%>/images/botones/aceptar.gif"   name="acept"   height="21"          onClick="sendSucursales(formulario);"                                         onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">          
           <img src="<%=BASEURL%>/images/botones/salir.gif"     name="exit"    height="21"          onClick="parent.close();"                                                                 onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
    </p>



</body>
</html>
