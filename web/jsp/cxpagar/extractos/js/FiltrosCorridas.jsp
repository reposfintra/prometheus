<!--  
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      21/02/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description:  Vista que permite capturar tipo de filtros para generar la corrida
 --%>

<%@page    session   ="true"%> 
<%@page    errorPage ="/error/ErrorPage.jsp"%>
<%@page    import    ="com.tsp.operation.model.beans.*"%>
<%@include file      ="/WEB-INF/InitModel.jsp"%>
<%@page    import    ="com.tsp.util.*"%>
<%@page    import    ="java.util.*" %>



<html>
<head>

    <title>Generacion de Corridas</title>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
    
    <script>
        function sendFiltro(theForm, img){
                 var sw = 0;
                 anularBtn(img);
                 for(i=0;i<theForm.length;i++){
                    if( theForm.elements[i].type=='checkbox' && theForm.elements[i].checked ){
                        sw = 1;
                        break;
                    }
                 }
                 if(sw==0){
                      alert('Deber� seleccionar filtros ');
                      activarBtn(img);
                 }
                 else
                    theForm.submit();
        }
    </script>

</head>
<body>



<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Generar Corridas"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>


  <% String     msj   =  request.getParameter("msj");  %>


     <form action='<%=CONTROLLER%>?estado=Generar&accion=Extractos&evento=FILTROS' method='post' name='formulario'>
         <table  width="400" border="2" align="center">
             <tr>
                  <td>  
                       <table width='100%' align='center' class='tablaInferior'>

                              <tr class="barratitulo">
                                    <td colspan='2' >
                                           <table cellpadding='0' cellspacing='0' width='100%'>
                                                 <tr>
                                                      <td align="left" width='55%' class="subtitulo1">&nbsp;Filtros de Generaci�n</td>
                                                      <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                                                </tr>
                                           </table>
                                    </td>
                             </tr>
                             
                             <tr  class="fila">
                                    <td width='7%'> <input type='checkbox'   name='ckDistrito'></td>
                                    <td width='*' > Distrito </td>
                             </tr>
                             
                             <tr  class="fila">
                                    <td> <input type='checkbox'   name='ckBancos'></td>
                                    <td> Bancos </td>
                             </tr>

                             <tr  class="fila">
                                    <td> <input type='checkbox'  name='ckProveedor'></td>
                                    <td> Proveedores </td>
                             </tr>

                             <tr  class="fila">
                                    <td> <input type='checkbox' name='ckFechas'></td>
                                    <td> Fechas de facturas </td>
                             </tr>


                     </table>
                  </td>
               </tr>       
         </table>
     </form>
     
     
     <p>
           <img src="<%=BASEURL%>/images/botones/aceptar.gif"    name="acept"   height="21"          onClick="sendFiltro(formulario, this);"   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
           <img src="<%=BASEURL%>/images/botones/salir.gif"      name="exit"    height="21"          onClick="parent.close();"                onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
     </p>
     
     
     
     
     
     
     
      <!-- mensajes -->
    <% if(msj!=null && !msj.equals("")){%>        
         <table border="2" align="center">
          <tr>
            <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
              <tr>
                <td width="470" align="center" class="mensajes"><%=msj%></td>
                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                <td width="58">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
        </table>
    <%  } %>
         

</div>

</body>
</html>
