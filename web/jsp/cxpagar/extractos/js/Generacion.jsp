<!--  
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      18/10/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description:  Vista que permite capturar detalle de los filtros
 --%>

<%@page    session   ="true"%> 
<%@page    errorPage ="/error/ErrorPage.jsp"%>
<%@page    import    ="com.tsp.operation.model.beans.*"%>
<%@include file      ="/WEB-INF/InitModel.jsp"%>
<%@page    import    ="com.tsp.util.*"%>
<%@page    import    ="java.util.*" %>


<html>
<head> 


    <title>Generacion de Corridas</title>
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
    
    
     <!-- VARIABLES -->
  <%  Hashtable  filtros      =  model.ExtractosSvc.getFiltros();        
      Hashtable  datosFiltros =  model.ExtractosSvc.getDatosFiltros();   
      String     msj          =  request.getParameter("msj");            
      String     hoy          =  Utility.getHoy("-"); 
      String     activo       =  "ON";                                 %>
      
      
     <!-- FILTROS -->
  <%  String  chDistrito  = (String) filtros.get("distrito"); 
      String  chBancos    = (String) filtros.get("banco");    
      String  chProveedor = (String) filtros.get("proveedor");
      String  chFechas    = (String) filtros.get("fecha");  %>
                            
         
    <script>     
          function anularBtn(btn){ 
              btn.style.visibility='hidden';
          }
              
          function activarBtn(btn){
             btn.style.visibility='';            
          }
          
          function sendFiltros(theForm, opcion){                
                  var sw = 0;
                  anularBtn(acept);
                  theForm.evento.value =opcion;                  
                  if(opcion=='DATOSFILTROS'){
                          <% if( chDistrito.equals(activo) ) {%>  
                                 if( theForm.distrito.value=='' )
                                     sw = 1;
                          <%}%>
                          <% if( chBancos.equals(activo) )   {%>  
                                 selectAll( theForm.bancosFiltro );
                                 selectAll( theForm.sucursalesFiltro ); 
                                 if( theForm.bancosFiltro.value==''  )
                                     sw = 1;
                          <%}%>
                          <% if( chProveedor.equals(activo) ){%>  
                                selectAll( theForm.proveedoresFiltro );
                                if( theForm.proveedoresFiltro.value=='' )
                                     sw = 1;
                          <%}%>
                  }
                  if( sw == 1){
                      alert('Hay valores sin seleccionar, por favor seleccionelos...');
                      activarBtn(acept);
                      desabledALL(theForm.bancosFiltro);
                      desabledALL(theForm.sucursalesFiltro);
                      desabledALL(theForm.proveedoresFiltro);
                  }
                  else
                      theForm.submit();
          }
          
          function  loadSucursales(theForm, opcion){
              var sw = 0;
              theForm.evento.value =opcion;              
              if(opcion=='SUCURSALES'){
                       selectAll( theForm.bancosFiltro );                       
                       if ( theForm.bancosFiltro.selectedIndex!=-1){
                              var parameter  = theForm.action; 
                              for(i=0;i<theForm.length;i++)   {
                                 if (theForm.elements[i].type=='select-multiple'  &&  theForm.elements[i].name=='bancosFiltro' ){
                                      var cmb = theForm.elements[i];
                                      for (j=0;j<cmb.length;j++)
                                         if (cmb[j].selected)
                                             parameter += '&'+ theForm.elements[i].name + '=' +  cmb[j].value;
                                 }
                                 else
                                   parameter += '&'+ theForm.elements[i].name + '=' +  theForm.elements[i].value;
                              }
                              var win =window.open(parameter,'Sucursales',' scrollbars=yes, top=100,left=200, width=450, height=500,  status=yes, resizable=yes  ');
                              win.focus();
                       }
                       else
                           sw=1;
                       desabledALL(theForm.bancosFiltro);
               }
               if( sw == 1)
                     alert('Deber� seleccionar bancos para poder mostrar sucursales...');
          }
          
          
          function desabledALL(cmb){
              for (i=0;i<cmb.length;i++)
                  cmb[i].selected = false;
          }
          
          
         function searchCombo(data, cmb){
               data = data.toLowerCase();
               for (i=0;i<cmb.length;i++){
                  cmb[i].selected = false; 
               }           
               for (i=0;i<cmb.length;i++){                
                    var txt = cmb[i].text.toLowerCase( );
                    if( txt.indexOf(data)==0 ){
                        cmb[i].selected = true;
                        break;
                   }
                   else
                      cmb[i].selected = false;
                }
          }
          
    </script>
    
    
</head>
<BODY>
  

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Generar Corridas"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>
   
     
      <form action='<%=CONTROLLER%>?estado=Generar&accion=Extractos' method='post' name='formulario' >
      
          <table width="650" border="2" align="center">
              <tr>
                 <td ALIGN='center'>  
                         <table width='100%'   class='tablaInferior'   >       
                              <tr>
                                 <td colspan='2'>
                                   <table cellpadding='0' cellspacing='0' width='100%'>
                                       <tr class="fila">
                                              <td align="left" width='55%' class="subtitulo1">&nbsp Datos de los Filtros</td>
                                              <td align="left" width='*' class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                                       </tr>
                                   </table>
                                </td>
                              </tr>
                              
                                
                                  
                               <!-- DISTRITOS -->   
                               <% if( chDistrito.equals(activo) ){%>                               
                                    <TR class="fila">
                                           <TD width='20%' >&nbsp DISTRITOS</TD>
                                           <TD width='*'>
                                               <select name='distrito' class="textbox"   style='width:32%'> 
                                               <% List distritos = (List)datosFiltros.get("distrito");
                                                  for (int i=0;i<distritos.size();i++){
                                                      String dist = (String)distritos.get(i);%> 
                                                      <option value='<%=dist%>'> <%=dist%>
                                              <% }%>
                                               </select>
                                           </TD>
                                    </TR>
                               <%}%>
                               
                               
                               
                               <!-- BANCOS -->
                               <% if( chBancos.equals(activo) ){%> 
                                      <TR class="fila" >
                                           <TD colspan='2'  height='20'>&nbsp BANCOS</TD>
                                      </TR>
                                      <TR class="fila">
                                           <TD  colspan='2' >
                                                 <table cellpadding='0' cellspacing='0' width='100%'>
                                                     <tr class="fila">
                                                         <td width='45%' align='center' style="font size:12">
                                                                Banco de Agencia
                                                                
                                                                <input type='text' name='busqueda' style="width:90%" title='Buscar por palabras' onkeyup='searchCombo(this.value, this.form.bancos)' onkeypress='searchCombo(this.value, this.form.bancos)'>

                                                                <select multiple size='10' class='select' style='width:90%' name='bancos' onchange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text; }">                                                     
                                                                 <% List bancos = (List)datosFiltros.get("banco");
                                                                    for (int i=0;i<bancos.size();i++){
                                                                          String banck = (String)bancos.get(i);%> 
                                                                          <option value='<%=banck%>'> <%=banck%>
                                                                  <% }%>
                                                                </select>
                                                                <br><br>
                                                         </td>
                                                         <td width='10%' align='center'>
                                                                 <img src='<%=BASEURL%>/images/botones/envDer.gif' style='cursor:hand' name='imgEnvDer'  onclick="move   (bancos,  bancosFiltro); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>             
                                                                 <img src='<%=BASEURL%>/images/botones/envIzq.gif' style='cursor:hand' name='imgEnvIzq'  onclick="move   (bancosFiltro, bancos ); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>                                                             
                                                         </td>
                                                         <td width='45%' align='center' style="font size:12">
                                                                <br>
                                                                Banco de la Corrida
                                                                <select multiple size='12' class='select' style='width:90%' name='bancosFiltro'     onchange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text; }"></select>

                                                                <br>
                                                                <a href='#' onClick="loadSucursales(formulario,'SUCURSALES');"> <font style="font size:12" >Ver Sucursales<font></a>

                                                         </td> 
                                                     </tr>
                                                 </table>
                                           </TD>
                                      </TR>
                                      
                                                                            
                                      <TR class="fila" >
                                               <TD colspan='2'  height='20'>&nbsp SUCURSALES POR BANCOS</TD>
                                      </TR>
                                      <TR class="fila" >
                                               <TD colspan='2' align='center'>    
                                                     <select   multiple size='7'   name='sucursalesFiltro' style="width:95%"  >
                                                     </select>
                                                     <br><br>
                                               </TD> 
                                      </TR>
                                      
                               <%}%>
                               
                               
                               
                               
                               <!-- PROVEEDORES -->
                               <% if( chProveedor.equals(activo) ){%> 
                                      <TR class="fila">
                                           <TD colspan='2'  height='20'>&nbsp PROVEEDORES</TD>
                                      </TR>
                                      <TR class="fila">
                                           <TD  colspan='2'>
                                                 <table cellpadding='0' cellspacing='0' width='100%'>
                                                     <tr class="fila">
                                                         <td width='45%' align='center' style="font size:12" >
                                                                    Proveedores de Agencia:

                                                                    <input type='text' name='busquedaProv' style="width:90%" title='Buscar por palabras' onkeyup='searchCombo(this.value, this.form.proveedores)' onkeypress='searchCombo(this.value, this.form.proveedores)'>

                                                                    <select multiple size='10' class='select' style='width:90%' name='proveedores' onchange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text; }">                                                     
                                                                     <% List proveedores = (List)datosFiltros.get("proveedores");
                                                                        for (int i=0;i<proveedores.size();i++){
                                                                              Hashtable  pro   = (Hashtable)proveedores.get(i);
                                                                              String     code  = (String) pro.get("nit");
                                                                              String     name  = (String) pro.get("nombre");%> 
                                                                              <option value='<%=code%>'> <%=name%>
                                                                      <% }%>
                                                                    </select>
                                                                    <br><br>
                                                         </td>
                                                         <td width='10%' align='center'>
                                                                     <img src='<%=BASEURL%>/images/botones/envDer.gif' style='cursor:hand' name='imgEnvDer'  onclick="move   (proveedores,  proveedoresFiltro); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>             
                                                                     <img src='<%=BASEURL%>/images/botones/envIzq.gif' style='cursor:hand' name='imgEnvIzq'  onclick="move   (proveedoresFiltro, proveedores ); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>                                                             
                                                         </td>
                                                         <td width='45%' align='center' style="font size:12">
                                                                      Proveedores de la Corrida:<br>
                                                                      <select multiple size='12' class='select' style='width:90%' name='proveedoresFiltro'     onchange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text; }"></select>
                                                         </td> 
                                                     </tr>
                                                 </table>
                                           </TD>
                                      </TR>
                               <%}%>
                               
                               
                               
                               
                               <!-- FECHAS -->
                               <% if( chFechas.equals(activo) ){%> 
                               
                                    <TR class="fila" >
                                           <TD colspan='2'  height='20'>&nbsp RANGO DE FECHAS FACTURAS </TD>
                                    </TR>
                                     
                                    <TR class="fila" >
                                           <TD colspan='2'  height='20' align='center'>
                                                  <table cellpadding='0' cellspacing='0'  width='90%'>
                                                      <tr  class="fila">
                                                           <td  width='18%'  align='center' style="font size:12"> Fecha Inicial</td>
                                                           <td  width='35%'  align='center'>
                                                               
                                                                 <input type='text' class="textbox"  name='fechaIni'     readonly       value="<%= hoy %>">                                                        
                                                                 <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fechaIni);return false;" HIDEFOCUS>
                                                                    <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha">
                                                                 </a>
                                                           
                                                           </td>
                                                           <td  width='20%'  align='center' style="font size:12"> Fecha Final </td>
                                                           <td  width='*'    align='center' >
                                                              
                                                                 <input type='text' class="textbox"  name='fechaFin'     readonly       value="<%= hoy %>">                                                        
                                                                 <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fechaFin);return false;" HIDEFOCUS>
                                                                    <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha">
                                                                 </a>
                                                           
                                                           </td>                                                      
                                                      </tr>
                                                  </table>
                                                  <BR>
                                           </TD>
                                    </TR>
                                    
                               
                               <%}%>
                               
                              
                         </table>
                 </td>
              </tr>
           </table>
           
           <input  type='hidden' name='evento' >
           
     </form>
                 
     
         
          
     <p>
           <img src="<%=BASEURL%>/images/botones/aceptar.gif"        name="acept"   height="21"          onClick="sendFiltros(formulario,'DATOSFILTROS');"                                         onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">          
           <img src="<%=BASEURL%>/images/botones/regresar.gif"       name="regre"   height="21"          onClick="window.location.href='<%=BASEURL%>/jsp/cxpagar/extractos/FiltrosCorridas.jsp'"   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
           <img src="<%=BASEURL%>/images/botones/salir.gif"          name="exit"    height="21"          onClick="parent.close();"                                                                 onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
     </p>
     
     
     
     
     
     
      <!-- mensajes -->
    <% if(msj!=null && !msj.equals("")){%>        
         <table border="2" align="center">
          <tr>
            <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
              <tr>
                <td width="470" align="center" class="mensajes"><%=msj%></td>
                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                <td width="58">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
        </table>
    <%  } %>
 
  
     
</div>  
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js"  style='cursor: move;' ondragstart='' onMouseDown="_initMove(this);"    id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
 
</body>
</html>