<!--
- Autor : Ing. Andrés Maturana De La Cruz
- Date  : 27.06.2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la generación del reporte de facturas de clientes.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
</script>
<title>Retenci&oacute;n de Pagos</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
<body onresize="redimensionar()" onload = 'redimensionar();'>
<div id="capaSuperior" style="position:absolute; width:100%; height:110px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Retención de Pagos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 109px; overflow: scroll;">
<%
	Proveedor nit = (Proveedor) request.getAttribute("nit");
	TreeMap si_no = new TreeMap();
	si_no.put("SI", "S");
	si_no.put("NO", "N");
%>
<form name="forma" action='<%=CONTROLLER%>?estado=RetencionPago&accion=Manager&opc=4&proveedor=<%= nit.getC_nit()%>' id="forma" method="post">
  <table width="862"  border="2" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td width="856"><table width="100%" class="tablaInferior">
        <tr class="fila">
          <td colspan="4"><table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
            <tr>
              <td width="56%" class="subtitulo1">&nbsp;Retenci&oacute;n de Pagos a Proveedor </td>
              <td width="44%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
            </tr>
          </table></td>
          </tr>
        <tr class="fila">
          <td width="19%">Documento</td>
          <td width="24%" nowrap><%= nit.getC_nit()%></td>
          <td width="20%" nowrap>Nombre</td>
          <td width="37%" nowrap><%= nit.getNombre()%></td>
        </tr>
        <tr class="fila">
          <td>Sede de Pago </td>
          <td nowrap><%= nit.getC_agency_id()%></td>
          <td nowrap>Banco</td>
          <td nowrap><%= nit.getC_branch_code()%></td>
        </tr>
        <tr class="fila">
          <td>Agencia</td>
          <td nowrap><%= nit.getC_bank_account()%></td>
          <td nowrap>Clasificacion</td>
          <td nowrap><%= (nit.getC_clasificacion()!=null)? nit.getC_clasificacion() : ""%></td>
        </tr>
        <tr class="fila">
          <td>Handle Code </td>
          <td nowrap><%= (nit.getHandle_code()!=null)? nit.getHandle_code() : "" %></td>
          <td nowrap>Nit Beneficiario </td>
          <td nowrap><%= nit.getNit_beneficiario() %></td>
        </tr>
        <tr class="fila">
          <td>Nombre Beneficiario </td>
          <td nowrap><%= nit.getNom_beneficiario()!=null ? nit.getNom_beneficiario() : ""%></td>
          <td nowrap>Aplica Retenci&oacute;n de Pago </td>
          <td nowrap><input type="hidden" name="ret_pago0" value="<%= nit.getRetencion_pago() %>"> <input type="hidden" name="dstrct" value="<%= nit.getDistrito() %>">            <input:select name="ret_pago" attributesText="class=listmenu" options="<%= si_no %>" default="<%= nit.getRetencion_pago() %>" /></td>
        </tr>
      </table></td>
    </tr>
  </table>
  <br>
  
  <br>
  <center>
    <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="c_aceptar" onClick="if( validarTCamposLlenos() ) forma.submit(); " onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
    <img src="<%=BASEURL%>/images/botones/cancelar.gif" name="c_cancelar" onClick="forma.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
    <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_salir" onClick="window.location.href = '<%=CONTROLLER%>?estado=RetencionPago&accion=Manager&opc=1';" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
    <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
  </center>
</form>
<% if( request.getParameter("msg") != null ){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="363" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%} %>
</div>
<%=datos[1]%>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</body>
</html>
