<%@ page session="true"%>
<%@ page errorPage="/error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>

<title>Retenci&oacute;n de Pagos</title>
<script language="javascript" src="../../../js/validar.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script>
function checkear(vlr) {
	var obj = document.getElementsByName("ret_pago");
	if( obj!= null )
		for( var i= 0; i<obj.length; i++){
			obj[i].checked = vlr;
		}
}

function checkear2(vlr) {
	var obj = document.getElementsByName("ret_pago");
	var c = 0;
	var obj0 = document.getElementById("all");
	for( var i= 0; i<obj.length; i++){
		if ( obj[i].checked == vlr ){
			c++;
		}
	}//alert("C: "+ c + " de " + obj.length + " valor " + vlr);
	
	if( c == obj.length ){
		obj0.checked = vlr;
	} else {
		//obj0.checked = obj0.checked ? false : true;
		obj0.checked = false;
	}	
}
</script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div class="fila" style="position:absolute; visibility:hidden; width:350px; height:50px; z-index:1; border: 2 outset #003399; " id="msg">
</div>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Retención de Pagos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<%    
    Vector cxp_Docs = model.cxpDocService.getFacturas_ret();
%>
<form action="<%=CONTROLLER%>?estado=RetencionPago&accion=Manager&opc=6" id="forma" method="post">
<table border="2" align="center">
  <tr>
    <td><table width="100%">
      <tr>
        <td width="50%" class="subtitulo1">Lista  de Documentos </td>
        <td width="50%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif"><%=datos[0]%></td>
      </tr>
    </table>      
      <table border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
      <tr class="tblTitulo"  >
        <td nowrap><div align="center"></div></td>
        <td nowrap><div align="center">Nit</div></td>
        <td nowrap><div align="center">Nombre</div></td>
        <td nowrap><div align="center">Documento</div></td>
        <td nowrap><div align="center">Tipo de Doc. </div></td>
        <td nowrap><div align="center">Descripci&oacute;n</div></td>
        <td nowrap><div align="center">Vlr. Doc. </div></td>
        <td nowrap><div align="center">Vlr. Pagar</div></td>
        <td nowrap><div align="center">Vlr. Saldo</div></td>
        <td nowrap><div align="center">Moneda</div></td>
        <td nowrap><div align="center">Fecha Doc. </div></td>
        <td nowrap><div align="center">Banco</div></td>
        <td nowrap><div align="center">Sucursal</div></td>
        <td nowrap><div align="center">
          <input name="all" type="checkbox" id="all" value="checkbox" onClick="checkear(this.checked);">
          Aplica<br> 
          Retenci&oacute;n Pago </div></td>
      </tr>
<%
      double total = 0;
      for (int i = 0; i < cxp_Docs.size(); i++){
          CXP_Doc doc = (CXP_Doc) cxp_Docs.elementAt(i);
		  String tdoc = "";
		  String tdoc_rel = "";
		  
		  if( doc.getTipo_documento().equals("010") ){
			  tdoc = "FACTURA";
		  } else if( doc.getTipo_documento().equals("035") ){
			  tdoc = "NOTA CREDITO";
		  } else if( doc.getTipo_documento().equals("036") ){
			  tdoc = "NOTA DEBITO";
		  }
		  
		  if( doc.getTipo_documento_rel().equals("010") ){
			  tdoc_rel = "FACTURA";
		  } else if( doc.getTipo_documento_rel().equals("035") ){
			  tdoc_rel = "NOTA CREDITO";
		  } else if( doc.getTipo_documento_rel().equals("036") ){
			  tdoc_rel = "NOTA DEBITO";
		  }
%>
      <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
        <td class="bordereporte" nowrap><img title="Ver detalles" src="<%= BASEURL %>/images/botones/iconos/detalles.gif" style="cursor:hand"  name="imgsalir"  onClick="window.open('<%= CONTROLLER %>?estado=Factura&accion=Detalle&documento=<%= doc.getDocumento().replaceAll("#","-_-") %>&prov=<%= doc.getProveedor()%>&tipo_doc=<%= doc.getTipo_documento()%>','DETALL','status=yes,scrollbars=no,width=780,height=650,resizable=yes');"></td>
        <td class="bordereporte" nowrap><%= doc.getProveedor() %></td>
        <td class="bordereporte" nowrap><%= doc.getNomProveedor() %></td>
        <td class="bordereporte" nowrap><%= doc.getDocumento() %></td>
        <td class="bordereporte" nowrap><div align="center"><%= tdoc %></div></td>
        <td class="bordereporte" nowrap><%= doc.getDescripcion() %></td>
        <td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(doc.getVlr_bruto_me()) %></div></td>
        <td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(doc.getVlr_neto_me()) %></div></td>
        <td class="bordereporte" nowrap><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(doc.getVlr_saldo_me()) %></div></td>
        <td class="bordereporte" nowrap><div align="center"><%= doc.getMoneda() %></div></td>
        <td class="bordereporte" nowrap><div align="center"><%= doc.getFecha_documento() %></div></td>
        <td class="bordereporte" nowrap><div align="center"><%= doc.getBanco()%></div></td>
        <td class="bordereporte" nowrap><div align="center"><%= doc.getSucursal()%></div></td>
        <td class="bordereporte" nowrap><div align="center">
          <input type="checkbox" id="ret_pago" name="ret_pago" value="<%= doc.getDstrct() + "_" + doc.getProveedor() + "_" + doc.getDocumento()%>" <%= doc.getRetencion_pago().equals("S") ? "checked" : "" %> onClick="checkear2(this.checked);">
        </div></td>
      </tr>
<%
	}
%>
    </table>
    </td>
  </tr>
</table>
</form>
<br>  
  
<% if( request.getParameter("msg") != null ){%>
<p>
<table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
          <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="58">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<p></p>
<p>
    <%} %>
</p>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr align="left">
    <td><%if ( cxp_Docs.size()>0 ) { %><img src="<%=BASEURL%>/images/botones/aceptar.gif" name="c_aceptar" onClick="forma.submit(); " onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
	<%} else { %><img src="<%=BASEURL%>/images/botones/aceptarDisable.gif" name="c_aceptar" onClick="JavaScript: void(0);" style="cursor:hand"> <% } %> 
	<img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" style="cursor:hand " onClick="window.location.href = '<%= CONTROLLER%>?estado=RetencionPago&accion=Manager&opc=2';" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp; <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" id="c_salir" style="cursor:hand " onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">  
  </tr>
</table>
</div>
<%=datos[1]%>
</body>
</html>


<!--
Entregado  a tito 15 Febrero 2007
-->
