<%@ include file="/WEB-INF/InitModel.jsp"%>
<head>
      <title>Pago por Transferencia - Ayuda</title>
      <META http-equiv=Content-Type content="text/html; charset=windows-1252">
      <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
      <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body>
<center>

<% String BASEIMG = BASEURL +"/images/ayuda/cxpagar/transferencia/"; %>



<table width="100%"  border="2" align="center">
    <tr>
      <td >
 
            <table width='99%' align="center" cellpadding='0' cellspacing='0'>
                 <tr class="subtitulo" ><td height="20"><div align="center">MANUAL PAGO POR TRANSFERENCIA</div></td></tr>
                 <tr class="subtitulo1"> <td>Descripci&oacute;n del funcionamiento </td> </tr>

     
                  <tr>
                      <td  class="ayudaHtmlTexto" height='40'>
                         A partir de las facturas seleccionadas, se agrupan por informaci�n de transferencia para formar cada registro a migrar,
                         y la lista agrupada se muestra como lo indica la figura 1.
                         El archivo a generar, deber� presentar la estructura descrita en el formato de banco, para conocer dicho formato, deber�
                         realizar click  
                         <a href='#' onclick=" var win = window.open('<%= BASEIMG%>BancocreditoTEF.doc','planViaje',' top=100,left=100, width=700, height=600, scrollbars=yes, status=yes, resizable=yes  ');">aqui</a>
                         <br><br>
                      </td>
                 </tr>
                 
                 
                 <tr>
                      <td  align="center" > 
                         <br><br><img  src="<%= BASEIMG%>viewPrevia.JPG" ><br>
                         <strong>Figura 1</strong>
                         <br><br><br><br>
                     </td>
                 </tr>
                 
                 
                 
                 <tr>
                      <td  class="ayudaHtmlTexto" height='40'>
                         Para conocer las facturas que conforman cada valor a transferir  deber� realizar click en el valor deseado, le aparecer�
                         una ventana con informaci�n de las facturas tal como lo indica la figura 2.
                      </td>
                 </tr>
                 
                 
                 <tr>
                      <td  align="center" > 
                         <br><br><img  src="<%= BASEIMG%>Detalle.JPG" ><br>
                         <strong>Figura 2</strong>
                         <br><br><br><br>
                     </td>
                 </tr>
                 
                 
                 
                 <tr>
                      <td  class="ayudaHtmlTexto" height='40'>                         
                         Para generar el archivo a migrar, deber� realizar click en el bot�n  "Aceptar", el programa crear� el archivo y lo guardar�
                         en la carpeta del usuario en sessi�n.
                         
                         Para acceder a el, deber� dirigirse a la vista de 
                         archivos del usuario realizando click en el icono ubicado en la parte superior de la p�gina principal del sitio, tal
                         como lo indica la figura 3.
                         <br><br>
                      </td>
                 </tr>
                 
                 
                 <tr><td  align="center" > 
                     <br><br><img  src="<%= BASEIMG%>dir.JPG" > <br>
                     <strong>Figura 3</strong>
                     <br><br><br><br>
                 </td></tr>
                
                 
                 
                 
            </table>
            
      </td>
  </tr>
</table>
<br><br>
<img src="<%=BASEURL%>/images/botones/salir.gif"      name="exit"    height="21"  title='Salir'                onClick="parent.close();"                 onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">          


</body>
</html>
