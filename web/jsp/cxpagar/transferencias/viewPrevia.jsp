<!--
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      14/13/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Muestra  los cheques de la corridas establecida
 --%>

<%@ page session   ="true"%> 

<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>



<html>
<head>
        <title>Pago por Transferencia - Vista Previa</title>
        <link href="<%= BASEURL %>/css/estilostsp.css"     rel="stylesheet" type="text/css"> 
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
        
        <% String URL  =  CONTROLLER + "?estado=Transferencia&accion=Corrida"; %>
        <script>
            function send(evento){
                var msj = '';
                if(evento=='MIGRAR')  msj='Generando archivo de migración, por favor espere.....';
                if(evento=='RESET')   msj='Cargando lista de facturas.....';
                mensaje.innerHTML = msj;
                anularBtn(i_crear);
                anularBtn(i_regre);
                location.href = '<%=URL%>&evento=' + evento;
            }
        </script>
        
</head>
<body>

  
    

<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    %>

    
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Pago por Transferencia"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>

 
 <% String  msj            = (request.getParameter("msj")==null)?"": request.getParameter("msj") ; %>


  <form action="" method="post" name="formulario">  
        <table   border="2" align="center" width='95%'>
                 <tr>
                    <td colspan='2'>             

                                <table width="100%"   align="center">
                                          <tr>
                                             <td >
                                                  <table width='100%'  class="barratitulo">
                                                        <tr class="fila">
                                                                <td align="left" width='70%' class="subtitulo1" nowrap> PAGO POR TRANSFERENCIA [  <%= model.TransferenciaSvc.getBancoTR()%> &nbsp <%= model.TransferenciaSvc.getSucursalTR() %>   &nbsp <%= model.TransferenciaSvc.getMonedaBancoTR() %> ] - VISTA PREVIA </td>
                                                                <td align="left" width='*' ><img src="<%=BASEURL%>/images/titulo.gif" width="32"    height="20" align="left"><%=datos[0]%></td>
                                                        </tr>
                                                  </table>
                                             </td>
                                          </tr>
                 
                                                                                   
                                           <tr class="fila">
                                                <td width='100%'>
                                                      <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                                                      
                                                          <tr class="tblTitulo" >
                                                              <TH   colspan='3' >CONTROL        </TH>
                                                              <TH   colspan='3' >REGISTRO       </TH>
                                                              <TH   colspan='9' >TRANSFERENCIA  </TH>
                                                              <TH   colspan='4' >CLIENTE        </TH>     
                                                          </tr>
                                                         
                                                          <tr class="tblTitulo" >
                                                             
                                                              <TH style="font size:11">CHEQUE            </TH>
                                                              <TH style="font size:11">PROPIETARIO       </TH>
                                                              <TH style="font size:11">NOMBRE            </TH>                                                              
                                                              
                                                              <TH style="font size:11">SECUENCIA REGISTRO</TH>
                                                              <TH style="font size:11">SECUENCIA CUENTA  </TH>
                                                              <TH style="font size:11">FECHA             </TH>
                                                              
                                                              <TH style="font size:11">NIT CUENTA        </TH>
                                                              <TH style="font size:11">NOMBRE CUENTA     </TH>
                                                              <TH style="font size:11">COD. BANCO        </TH>                                                              
                                                              <TH style="font size:11">TIPO CUENTA       </TH>
                                                              <TH style="font size:11">NO. CUENTA        </TH>
                                                              <TH style="font size:11">TIPO TRANS.       </TH>
                                                              <TH style="font size:11">VALOR             </TH>
                                                              <TH style="font size:11">OBSERVACION       </TH>
                                                              <TH style="font size:11">NOTA ADICIONAL    </TH>
                                                              
                                                              <TH style="font size:11">NIT CLIENTE       </TH> 
                                                              <TH style="font size:11">NO CUENTA CLI.    </TH>
                                                              <TH style="font size:11">TIPO CUENTA CLI.  </TH>
                                                              <TH style="font size:11">TIPO OPERACION    </TH>
                                                         </tr>
                                                         
                                                         
                                                         <% List lista = model.TransferenciaSvc.getListMigracion();
                                                            for(int i=0;i<lista.size();i++){
                                                                Hashtable  tran = ( Hashtable ) lista.get(i); 
                                                                List listaFacturas = (List)tran.get("facturas"); %>
                                                               
                                                               <tr  class='<%= (i%2==0?"filagris":"filaazul") %>'>
                                                                         
                                                                         <td nowrap class="bordereporte" style="font size:11" align='center'> <%=  (String)tran.get("cheque")    %>            </td>
                                                                         <td nowrap class="bordereporte" style="font size:11"> <%=  (String)tran.get("proveedor") %>            </td>
                                                                         <td nowrap class="bordereporte" style="font size:11"> <%=  (String)tran.get("nombre") %>  </td>
                                                                         
                                                                         <td nowrap class="bordereporte" style="font size:11"> <%=  (String)tran.get("secuencia") %>            </td>
                                                                         <td nowrap class="bordereporte" style="font size:11"> <%=  (String)tran.get("secuencia_proveedor") %>  </td>
                                                                         <td nowrap class="bordereporte" style="font size:11"> <%=  (String)tran.get("fecha") %>                </td>
                                                                         
                                                                         <td nowrap class="bordereporte" style="font size:11">                <%=  (String)tran.get("cedula_cuenta")  %></td>
                                                                         <td nowrap class="bordereporte" style="font size:11">                <%=  (String)tran.get("nombre_cuenta")  %></td>                                                                        
                                                                         <td nowrap class="bordereporte" style="font size:11" align='center'> <%=  (String)tran.get("codigo_banco")   %></td>
                                                                         <td nowrap class="bordereporte" style="font size:11" align='center'> <%=  (String)tran.get("tipo_cuenta")    %></td>
                                                                         <td nowrap class="bordereporte" style="font size:11">                <%=  (String)tran.get("no_cuenta")      %></td>
                                                                         <td nowrap class="bordereporte" style="font size:11" align='center'> <%=  (String)tran.get("tipo_operacion_proveedor") %></td>
                                                                         <td nowrap class="bordereporte" style="font size:11" align='right' > 

                                                                                <a href='#' class='Simulacion_Hiper' onclick="vista<%=(String)tran.get("secuencia")%>.style.visibility=''" title='Ver Detalle'>
                                                                                    <%=  Util.customFormat( Double.parseDouble( (String)tran.get("valor") ) )  %> 
                                                                                </a>
                                                                                 
                                                                                
                                                                                <div id='vista<%=(String)tran.get("secuencia")%>'   style=" position:absolute; left:500;  visibility:hidden; z-index:0; overflow: scroll;" >
                                                                                        <table   cellpadding='0' cellspacing='0'    align="center" border='1' bordercolor="#123456" >
                                                                                               <tr class='fila'>
                                                                                                  <td align='center'> 
                                                                                                          <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                                                                                                             <tr class="tblTitulo" style="font size:9" >
                                                                                                                  <TH >CORRIDA       </TH>
                                                                                                                  <TH >BANCO         </TH>
                                                                                                                  <TH >SUCURSAL      </TH>
                                                                                                                  <TH >PROVEEDOR     </TH>
                                                                                                                  <TH >FACTURA       </TH>
                                                                                                                  <TH >DESCRIPCION   </TH>
                                                                                                                  <TH >VALOR         </TH>     
                                                                                                              </tr>
                                                                                                                    <% for(int j=0;j<listaFacturas.size();j++){
                                                                                                                       FacturasCheques  factura  = (FacturasCheques) listaFacturas.get(j);%>
                                                                                                                       <tr class='<%=(i%2==0?"filagris":"filaazul")%>' style="font size:10">
                                                                                                                           <td class="bordereporte" nowrap              > <%= factura.getCorrida()        %> </td>
                                                                                                                           <td class="bordereporte" nowrap              > <%= factura.getBanco()          %> </td>
                                                                                                                           <td class="bordereporte" nowrap              > <%= factura.getSucursal()       %> </td>
                                                                                                                           <td class="bordereporte" nowrap title='NIT:<%= factura.getProveedor() %>'><%= factura.getNomProveedor()   %> </td>
                                                                                                                           <td class="bordereporte" nowrap              > <%=  factura.getDocumento()     %>  </td>
                                                                                                                           <td class="bordereporte" nowrap              > <%=  factura.getDescripcion()   %>  </td>
                                                                                                                           <td class="bordereporte" nowrap align='right'> <%=  Util.customFormat(factura.getValorTR() )  %> </td>
                                                                                                                      </tr> 
                                                                                                                  <%}%>
                                                                                                         </table> 
                                                                                                         <a href='#' class='Simulacion_Hiper' onclick="vista<%=(String)tran.get("secuencia")%>.style.visibility='hidden'" title='Cerrar Detalle'>Cerrar</a>                                                                                                      
                                                                                                         <br><br>
                                                                                                    </td>
                                                                                              </tr>
                                                                                        </table>
                                                                                </div>
                                                                                
                                                                         
                                                                         </td>
                                                                         <td nowrap class="bordereporte" style="font size:11"> <%=  (String)tran.get("observacion")    %></td>
                                                                         <td nowrap class="bordereporte" style="font size:11" align='center'> <%=  (String)tran.get("secuencia_nota") %></td>

                                                                         <td nowrap class="bordereporte" style="font size:11"> <%=  (String)tran.get("nit_cliente")            %></td>
                                                                         <td nowrap class="bordereporte" style="font size:11"> <%=  (String)tran.get("cuenta_cliente")         %></td>
                                                                         <td nowrap class="bordereporte" style="font size:11" align='center'> <%=  (String)tran.get("tipo_cuenta_cliente")    %></td>
                                                                         <td nowrap class="bordereporte" style="font size:11" align='center'> <%=  (String)tran.get("tipo_operacion_cliente") %></td>
                                                               </tr>
                                                         <%}%>
                                                         
                                                   </table>
                                             </td>
                                       </tr>
                              </table>
                      </td>
                  </tr>
        </table>
  </form>
     
     
     
   <p>       
         <img src='<%=BASEURL%>/images/botones/aceptar.gif'     style=" cursor:hand"   title='Aceptar....'     name='i_crear'      onclick="javascript: send('MIGRAR')"    onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>        
         <img src='<%=BASEURL%>/images/botones/regresar.gif'    style=' cursor:hand'   title='Regresar...'     name='i_regre'      onclick="javascript: send('RESET')"     onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
         <img src='<%=BASEURL%>/images/botones/salir.gif'       style=' cursor:hand'   title='Salir...'        name='i_salir'      onclick='parent.close();'               onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
   </p>    
   <font id='mensaje' class='informacion'></font>
  
  
  <!-- Mensaje --> 
   <% if( ! msj.equals("") ){%>
        <table border="2" align="center">
              <tr>
                    <td>
                          <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                              <tr>
                                    <td width="550" align="center" class="mensajes"><%=  msj %> </td>
                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                    <td width="58">&nbsp;</td>
                              </tr>
                          </table>
                    </td>
              </tr>
        </table>
   <%}%>
     

</div>
<%=datos[1]%>  


</body>
</html>
