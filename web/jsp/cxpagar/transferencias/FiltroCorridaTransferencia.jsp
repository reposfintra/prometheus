<!--  
     - Author(s)       :      Osvaldo P�rez Ferrer
     - Date            :      25-01-2007
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Filtro para facturas por corrida
 --%>

<%@ page session   ="true"%> 
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>


<%      String  path      = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String  datos[]   = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
        TreeMap bancos    = model.servicioBanco.getBanco();
        TreeMap sbancos   = model.servicioBanco.getSucursal(); %>
        

<html>
<head>

    <title>Pago por Transferencia Corrida</title>    
    <link href="<%= BASEURL %>/css/estilostsp.css"     rel="stylesheet" type="text/css"> 
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validate.js"></script> 
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validar.js"></script> 
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    
    <script>
        function sendForm( f ){        
            if( (trim( f.corrida.value ) == "") || (f.banco.value == "") || (f.cuenta.value == "") ){
                alert("Deber� llenar todos los campos");
            }else{
                location.replace(f.action+"&evento=FILTER&corrida="+f.corrida.value+"&banco="+f.banco.value+"&sucursal="+f.cuenta.value);
            }            
        }    
    </script>
     
</head>
<body>


<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Pago por Transferencia"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>


 
  <% Usuario  usuario               = (Usuario) session.getAttribute("Usuario");
     String   distrito              =  usuario.getDstrct(); 
     String   msj                   =  request.getParameter("msj"); 
     String   url                   =  CONTROLLER +"?estado=Transferencia&accion=Corrida";  %>
    
  <form action="<%=url%>" method="post" name="formulario" id="formulario">
  
     <table width="400"  border="2" align="center">
         <tr>
            <td colspan='2'>             

                        <table width="100%"   align="center">
                                  <tr>
                                     <td colspan='2'>
                                          <table width='100%'  class="barratitulo">
                                                <tr class="fila">
                                                        <td align="left" width='50%' class="subtitulo1" nowrap> &nbsp;FILTROS </td>
                                                        <td align="left" width='50%' ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                                                </tr>
                                          </table>
                                     </td>
                                  </tr>
                                  
                                   <tr class="fila">
                                        <td>BANCO</td>
                                        <td><input:select name="banco"  attributesText="style='width:100%;' class='listmenu'  onChange='cargarSucursales()'"  options="<%=bancos%>"  /></td>                      
                                   </tr> 
                                   <tr class='fila'>
                                        <td>SUCURSAL</td>
                                        <td ><input:select name="cuenta" attributesText="style='width:100%;' class='listmenu'"  options="<%=sbancos%>" /></td>
                                   </tr>
                                   <tr class="fila">
                                           <td>CORRIDA</td>
                                           <td>
                                              <input name='corrida' id='corrida' type="textbox" style='width:100' maxlength='6' onkeyPress='soloDigitos(event,"decNo")' >&nbsp;<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">                                                                                             
                                           </td>
                                   </tr>
                                  
                                                                                                                                                                               
                        </table>

                </td>
             </tr>
       </table>     
       <input type="hidden" name="evento" value="">
  <form>
                
  <p>       
       <img src='<%=BASEURL%>/images/botones/aceptar.gif'    style=" cursor:hand"  title='Crear....'       name='i_crear'       onclick="sendForm(formulario)" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>               
       <img src='<%=BASEURL%>/images/botones/salir.gif'      style=' cursor:hand'  title='Salir...'        name='i_salir'       onclick='parent.close();'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
  </p> 
       

  
  <font id='msj' class='informacion'></font>
  
  
  <!-- Mensaje -->
   <% if(msj!=null && !msj.equals("")){%>        
         <table border="2" align="center">
          <tr>
            <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
              <tr>
                <td width="550" align="center" class="mensajes"><%=msj%></td>
                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                <td width="58">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
        </table>
    <%  } %>


</div>
<%=datos[1]%>
</body>
</html>

<script>
function cargarSucursales(){
   formulario.evento.value = 'BuscarS';
   document.formulario.submit();
}
</script>
