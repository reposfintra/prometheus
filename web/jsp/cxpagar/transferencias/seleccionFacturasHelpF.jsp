<%@ include file="/WEB-INF/InitModel.jsp"%>
<head>
      <title>Pago por Transferencia - Ayuda</title>
      <META http-equiv=Content-Type content="text/html; charset=windows-1252">
      <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
      <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body>
<center>

<% String BASEIMG = BASEURL +"/images/ayuda/cxpagar/transferencia/"; %>




<table width="100%"  border="2" align="center">
    <tr>
      <td >
 
            <table width='99%' align="center" cellpadding='0' cellspacing='0'>
                 <tr class="subtitulo" ><td height="20"><div align="center">MANUAL PAGO POR TRANSFERENCIA</div></td></tr>
                 <tr class="subtitulo1"> <td>Descripci&oacute;n del funcionamiento </td> </tr>

        
                 <tr>
                      <td  class="ayudaHtmlTexto" height='40'>
                         El programa permitir� realizar pago por transferencia, para lo cual generar� un archivo de migraci�n con informaci�n
                         de las transfencias a realizar.<br>
                         
                         Deber� seleccionar las facturas que desea pagar por transferencia, para lo cual se muestra un listado de las facturas
                         aprobadas cuyos propietarios requieran que sus pago se realicen por transferencia tal como lo indica la figura 1.
                         
                         
                      </td>
                 </tr>
                         
                 <tr>
                      <td  align="center" > 
                         <br><br><img  src="<%= BASEIMG%>Listado.JPG" ><br>
                         <strong>Figura 1</strong>
                         <br><br><br><br>
                     </td>
                 </tr>
                 
                 
                 <tr>
                      <td  class="ayudaHtmlTexto" height='40'>
                         En el listado aparecen clasificadas las facturas por corrida, banco, sucursal y proveedor, mostrando su descripci�n y saldo
                         actual de la factura.
                         Para seleccionarlas, deber� activar la casilla  que se encuentra al lado de cada factura. Una vez seleccionadas las facturas a pagar,
                         deber� realizar click en el bot�n "Aceptar" el programa la mostrar� una vista previa del archivo a generar.
                         <br><br>
                      </td>
                 </tr>
                 
         
           </table>
            
      </td>
  </tr>
</table>
<br><br>
<img src="<%=BASEURL%>/images/botones/salir.gif"      name="exit"    height="21"  title='Salir'                onClick="parent.close();"                 onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">          





</body>
</html>
