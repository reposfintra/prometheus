<!--
- Autor : Osvaldo P�rez Ferrer
- Date : 7 de Julio de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP que sirve para iniciar el proceso de transferencia
                de facturas recurrentes a facturas por pagar
--%>

<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.operation.model.*, com.tsp.operation.model.beans.*, com.tsp.util.*,java.util.*, java.text.*"%>

<html>
<head>
    <title>Iniciar Transferencia de Facturas Recurrentes</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

    <script src="<%=BASEURL%>/js/utilidades.js"></script>
    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<%
    String mensaje = (String) request.getAttribute("mensaje");
    
%>
</head>

<body>
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=Transferir Facturas Recurrentes"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">        
        <form id="form" method="post" action="<%=CONTROLLER%>?estado=CXPTransferir&accion=Recurrentes">
        <table width="350" border="2" align="center">
	<tr>
		<td align="center">
		
		<!-- cabecera de la seccion -->
		<table width="100%" >
			<tr>
			<td class="subtitulo1"  width="50%">Transferencia de Facturas Recurrentes</td>
			<td class="barratitulo" width="50%">
			  <img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20">
			</td>
			</tr>
		</table>
		<!-- fin cabecera -->

		<table  width="100%" >
			<tr class="fila">
			   <td>&nbsp; <input type="radio" name='filtro' id='radio1' value='NIT' onclick="campo_nit();" > Por NIT &nbsp;&nbsp; <input type="radio" id='radio2' name='filtro' value='ALL' checked onclick="campo_nit();">  Todos </td>
			</tr>
			
			<tr class="fila" style='display:none' id='campo_nit'>
			   <td> &nbsp; NIT Proveedor <input type="text" name='nit'> </td>
			</tr>
			
		 </table>
        </td>         
      </tr>
      </table>
        <br>
        <div align="center">      
            <img id="baceptar" name="baceptar"  src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="sbmt();">            
       
            <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">            
        </div>
        
        <table width="40%"  align="center">
          <tr>
            <td>
              <FIELDSET>
              <legend><span class="letraresaltada">Nota</span></legend>
              <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="informacion">
                <tr>
                  <td nowrap align="center">&nbsp; Si desea transferir facturas de un Proveedor espec�fico, seleccione la opcion 'Por NIT' y digite el nit correspondiente </td>
                </tr>
              </table>
            </FIELDSET></td>
          </tr>
        </table>
        
        </form>
        <br>
	
        
	<div id="noexiste">
       <%if(mensaje != null){ %>
        
            <table border="2" align="center">
                <tr>                
                    <td height="45"><table width="410" height="41" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                        <tr>
                            <td width="282" height="35" align="center" class="mensajes"><%=mensaje%></td>
                            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                            <td width="78">&nbsp;</td>
                        </tr>
                    </table></td>
                </tr>
            </table>        
               
       <%}%>     
       </div>
    	            
</body>
</html>


<script>

    function campo_nit(){
        if( form.radio1.checked ){
            document.getElementById("campo_nit").style.display = "block";
        }else{
            document.getElementById("campo_nit").style.display = "none";
        }
    }
    
    function sbmt(){
    
        if( form.radio1.checked ){
            if( trim( form.nit.value ) == '' ){
                alert("Debe digitar el nit del proveedor");
                form.nit.focus();
                return false;
            }
        }
        form.submit();
        
    }

</script>
