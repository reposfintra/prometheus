<!--
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      02/06/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Muestra  las  Factura aprobadas para pago por transferencia dentro de una corrida,
                    Agrupadas por corridas,banco, sucursal y proveedor.
 --%>
 

<%@ page session   ="true"%> 
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

 
<html>
<head>
          <title>Pago por Transferencia</title>          
          <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
          <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
          <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>          
          
          <% String URL  =  CONTROLLER + "?estado=Transferencia&accion=Corrida"; %>          
          
          <script>          
              function sendFacturas(theForm){
                     anularBtn(i_crear);
                     var url       = '<%=URL%>' +'&bancoTR='+  theForm.bancoTR.value  +'&sucursalTR=' +  theForm.sucursalTR.value +'&evento=FORMAR';                    
                     mensaje.innerHTML    = 'Generando vista....';
                     window.location.href = url;                     
              }
          </script>
          
</head>
<body>



<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Pago por Transferencia"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>

  <% List     listaFacturas     = model.TransferenciaSvc.getListGeneral();
     String   msj               = (request.getParameter("msj")==null)?"": request.getParameter("msj") ;
     String   puede_seleccionar = request.getAttribute("puede_seleccionar")!=null? (String)request.getAttribute("puede_seleccionar") : "";
     boolean  selecc            = true;
     TreeMap  bancos            = model.servicioBanco.getBanco();
     TreeMap  sucursales        = model.servicioBanco.getSucursal();
     selecc                     = puede_seleccionar.equals("S")? true: false;



     if ( listaFacturas.size()>0 ) {%>
     
          <form action="<%=URL%>" method="post" name="formulario">  
             <table   border="2" align="center" width='80%'>
                 <tr>
                     <td colspan='2'>             

                            <table width="100%"   align="center">
                                   <tr>
                                         <td >
                                                  <table width='100%'  class="barratitulo">
                                                        <tr class="fila">
                                                                <td align="left" width='70%' class="subtitulo1" nowrap> PAGO POR TRANSFERENCIA &nbsp&nbsp [ <%= model.TransferenciaSvc.getBanco()%> &nbsp <%= model.TransferenciaSvc.getSucursal() %> &nbsp  <%= model.TransferenciaSvc.getCorrida()  %> &nbsp  <%= model.TransferenciaSvc.getMonedaBanco()  %> &nbsp] </td>
                                                                <td align="left" width='*' ><img src="<%=BASEURL%>/images/titulo.gif" width="32"   height="20" align="left"></td>
                                                        </tr>
                                                  </table>
                                        </td>
                                   </tr>
                                   
                              <!-- BANCO QUE GENERA EL FORMATO -->
                                   <tr  class="fila">
                                         <td >
                                               &nbsp BANCO PAGO TRANSFERENCIA &nbsp&nbsp
                                               <input:select name="bancoTR"      attributesText="style='width:25%;' class='listmenu'  onChange='cargarSucursales()'"  options="<%=bancos%>"     default="<%= model.TransferenciaSvc.getBancoTR()%>"  />
                                               <input:select name="sucursalTR"   attributesText="style='width:25%;' class='listmenu'"                                 options="<%=sucursales%>" default="<%= model.TransferenciaSvc.getSucursalTR() %>" />
                                               <script>
                                                    function cargarSucursales(){
                                                       formulario.action += '&evento=BuscarSTR'; 
                                                       document.formulario.submit();
                                                    }
                                               </script>
                                               
                                        </td>
                                   </tr>                
                                    
                                                                   
                                   
                                   <tr class="fila">
                                            <td width='100%'>
                                                   <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">                      
                                                       <tr class="tblTitulo" >
                                                              <TH align='center'  width='10%'  >NIT         </TH>
                                                              <TH align='center'  width='20%'  >PROVEEDOR   </TH>
                                                              <TH align='center'  width='17%'  >FACTURA     </TH>
                                                              <TH align='center'  width='38%'  >DESCRIPCI�N </TH>
                                                              <TH align='center'  width='18%'  >VALOR       </TH> 
                                                        </tr>
                                                        
                                                        
                                                       <% String antProveedor = "";
                                                          double total        = 0;
                                                          int    cant         = 0;
                                                          for(int i=0;i<listaFacturas.size();i++){
                                                                  FacturasCheques  factura        = (FacturasCheques) listaFacturas.get(i);
                                                                  String           descProveedor  =  factura.getNomProveedor();
                                                                  total                          +=  factura.getVlrTotalPagar();
                                                                  if (  antProveedor.equals( factura.getNomProveedor()  ) )descProveedor = "";
                                                                  else                                                     cant++;
                                                                  antProveedor  =  factura.getNomProveedor();%>
                                                                   
                                                                 <tr class='<%=(cant%2==0?"filagris":"filaazul")%>'>                                                                  
                                                                      <td class="bordereporte"   style="font-size:11" >               <%=( !descProveedor.equals("") )?  factura.getProveedor()  :"" %> </td>
                                                                      <td class="bordereporte"   style="font-size:11" >               <%=  descProveedor  %> </td>
                                                                      <td class="bordereporte"   style="font-size:11" >               <%=  factura.getDocumento()     %> </td>
                                                                      <td class="bordereporte"   style="font-size:11" >               <%=  factura.getDescripcion()   %> </td>
                                                                      <td class="bordereporte"   style="font-size:11" align='right' title='<%= Util.customFormat(factura.getVlr_saldo_me())%>' > <%=  Util.customFormat(factura.getVlrTotalPagar() )  %> &nbsp <%=  factura.getMonedaBanco()   %></td>
                                                                 </tr> 
                                                                
                                                          <%}%>
                                                    </table>
                                               </td>      
                                   </tr>
                                   
                                   
                                  <tr class="fila">
                                      <TD  align='right' style="font-size:12"><b><%= Util.customFormat( total ) %>  </TD>          
                                  </tr>
                                  
                          </table>                  
                    
                     </td>
                  </tr>
             </table>
        </form>
        <p>       
           <img src='<%=BASEURL%>/images/botones/aceptar.gif'     style=" cursor:hand"   title='Aceptar....'     name='i_crear'       onclick="javascript:sendFacturas(formulario);"   onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
           <img src='<%=BASEURL%>/images/botones/regresar.gif'    style=" cursor:hand"   title='Regresar....'    name='i_regresar'    onclick="location.href = '<%=BASEURL%>/jsp/cxpagar/transferencias/FiltroCorridaTransferencia.jsp';"   onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
           <img src='<%=BASEURL%>/images/botones/salir.gif'       style=' cursor:hand'   title='Salir...'        name='i_salir'       onclick='parent.close();'                        onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
        </p> 
        <font id='mensaje' class='informacion'></font>
     
    <%}else  msj="No presenta facturas aprobadas para pago por transferencia dentro de corridas � la corrida no existe"; %>
 
    
     
     
     
   <!-- Mensaje -->
   <% if( ! msj.equals("") ){%>
        <table border="2" align="center">
              <tr>
                    <td>
                          <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                              <tr>
                                    <td width="550" align="center" class="mensajes"><%= msj %></td>
                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                    <td width="58">&nbsp;</td>
                              </tr>
                          </table>
                    </td>
              </tr>
        </table>
        
        <% if ( listaFacturas.size()==0 ){%>
            <p>    
               <img src='<%=BASEURL%>/images/botones/regresar.gif'     style=" cursor:hand"   title='Regresar....'    name='i_regresar'    onclick="location.href('<%=BASEURL%>/jsp/cxpagar/transferencias/FiltroCorridaTransferencia.jsp')"   onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
               <img src='<%=BASEURL%>/images/botones/salir.gif'      style=' cursor:hand'   title='Salir...'        name='i_salir'       onclick='parent.close();'        onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
            </p>   
        <%}%>
        
   <%}%>
     

     

</div>



</body>
</html>

<script>

    function seleccionar(theForm,ele){
        var sel = <%=selecc%>;        
        if( sel ){  
            selectChecked(theForm,ele)
        }
        else{
            if( ele.checked ){ ele.checked = false; }
            if( !ele.checked ){ ele.checked = true; }
            alert("Usted no tiene permitido excluir Facturas");
        }
    }

</script>
