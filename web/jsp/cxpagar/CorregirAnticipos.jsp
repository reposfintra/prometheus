<%-- 
    Document   : CorregirAnticipos
    Created on : 11/10/2014, 11:53:44 AM
    Author     : egonzlaez
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Corregir Anticipos</title>

        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/CorregirAnticipos.css" type="text/css" rel="stylesheet" />
        <link href="<%=BASEURL%>/css/popup.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/jquery/jquery-ui/jquery-ui.css" type="text/css" rel="stylesheet" />

        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/CorregirAnticipos.js"></script>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=CORREGIR ANTICIPOS"/>
        </div>

        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow:auto">
            <center>
                
                  <div class="form_editar_cliente" style="text-align: center;margin-right: 28px">

                    <div id="contenido">
                        
                        <br/>
                        <button id="actualizar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false">
                            <span class="ui-button-text">Actualizar Lista</span>
                        </button>
                        <button id="validar_sun_dif" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false">
                            <span class="ui-button-text">Validar Diferencias</span>
                        </button>
                        <button id="salir" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false">
                            <span class="ui-button-text">Salir</span>
                        </button>
                    </div>   
                </div> 
                
                <br><br><br><br>
                <table id="tabla_planillas"  ></table>
                <div id="page_tabla_planillas"></div>
                <br>
            </center>
        </div> 
        <div id="extracto_detalle" class="ventana" title="Extracto Detalle">
            <center>
                <table id="tabla_extracto_detalle"  ></table>
                <div id="page_extracto_detalle"></div>
                <br>
                <div>
                    <p style="font-size: 14px; font-weight: bold; color: red" id="diferencia"></p>
                </div>
            </center>
        </div>

        <div id="verificar_tablas" class="ventana" title="Verificar Tablas">
            <center>
                <table id="tabla_verificar_tabla" ></table>
                <div id="page_verificar_tabla"></div>
            </center>
        </div>

        <div id="ver_anticipos" class="ventana" title="Anticipos Pagos Tercero">
            <center>
                <table id="tabla_anticipos" ></table>
                <div id="page_tabla_anticipos"></div>
                <div>
                    <p style="font-size: 14px; font-weight: bold; color: red" id="valor_refrencia"></p>
                </div>
            </center>
        </div>

        <div id="ver_extracto" class="ventana" title="Extracto">
            <center>
                <table id="tabla_extracto" ></table>
                <div id="page_tabla_extracto"></div>
            </center>
        </div>


        <div id="dialogo" class="ventana" title="Mensaje">
            <p  id="msj" >texto </p>
        </div>
        <div id="dialogo2" class="ventana">
            <p  id="msj2">texto </p> <br/>
            <center>
                <img src="<%=BASEURL%>/images/cargandoCM.gif"/>
            </center>
        </div>

    </body>
</html>
