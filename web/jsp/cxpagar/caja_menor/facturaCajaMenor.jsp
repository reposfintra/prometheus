<%-- 
    Document   : facturaCajaMenor
    Created on : 26/07/2016, 09:14:50 AM
    Author     : egonzalez
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>  
        <script type="text/javascript" href="http://www.ddginc-usa.com/spanish/editcheck.js"></script>
        <script type="text/javascript" src="./js/facturaCajaMenor.js"></script> 
        <title>CAJA MENOR</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=CAJA MENOR"/>
        </div>
        <style>

            .editable{
                width: 80% !important;
            }

            #gbox_tabla_cuentas{
                margin-left: 58px;
            }
            .cvteste {
                background-color: green;
            }

        </style>
    <center>
        <div id="tablita" style="top: 170px;width: 920px;height: 95px;" >
            <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                    &nbsp;
                    <label class="titulotablita" ><b>FILTRO DE BUSQUEDA</b></label>
                </span>
            </div>
            <div class="row" style="padding: 5px">
                <div class="col-md-12 ">
                    <table id="tablainterna" style="height: 53px; width: 214px"  >
                        <tr>
                            <td>
                                <label style="margin-left: 10px;">Fecha</label>
                            </td>
                            <td>
                                <input type="datetime" id="fechaini"  style="width: 100px ;height: 14px;"  readonly>
                            </td>
                            <td>
                                --
                            </td>
                            <td>
                                <input  type="datetime" id="fechafin"  style="width: 100px;height: 14px;" readonly>
                            </td>
                            <td>
                                <label>Empleado</label>
                            </td>
                            <td>
                                <input type="text" id="empleado" name="empleado" style="height: 15px;width: 110px;color: #070708;" >
                            </td>
                            <td>
                                <label>Anticipo</label>
                            </td>
                            <td>
                                <input type="text" id="anticipo" name="anticipo" style="height: 15px;width: 110px;color: #070708;" >
                            </td>
                            <td>
                                <hr style="width: 2px;height: 39px;top: -25px;margin-left: 13px;color: rgba(128, 128, 128, 0.39);" /> 
                            </td>
                            <td>
                                <div style="padding-top: 10px">
                                    <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;top: -7px;" > 
                                        <span class="ui-button-text">Buscar</span>
                                    </button>  
                                </div>
                            </td>
                            <td>
                                <div style="padding-top: 10px">
                                    <button id="guardar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;top: -7px;" > 
                                        <span class="ui-button-text">Guardar</span>
                                    </button>  
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div id="dialogMsj" style="display:none;">
            <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
        </div>
        <div id="dialogMsjProveedor" class="ventana">
            <table id="tablainterna" >
                <tr>
                    <td>
                        <label>Buscar por:</label>
                    </td>
                    <td>
                        <select id="tipo_busqueda_">
                            <option value="nit">Nit</option>
                            <option value="nombre">Nombre</option>
                        </select>
                    </td>
                    <td>
                        <label>Proveedor</label>
                    </td>
                    <td>
                        <input type="text" id="proveedor"  style="width: 290px" >
                    </td>
                </tr>
            </table>
        </div>
        <div id="dialogMsjcuenta" class="ventana">
            <table id="tablainterna" >
                <tr>
                    <td>
                        <label>Buscar por:</label>
                    </td>
                    <td>
                        <select id="tipo_busqueda">
                            <option value="cuentas">Cuenta</option>
                            <option value="nombre">Nombre cuenta</option>
                        </select>
                    </td>
                    <td>
                        <input type="text" id="busqueda"  style="width: 290px">
                    </td>
                    <td>
                        <div style="padding-top: 10px">
                            <button id="buscarCuenta" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;top: -7px;" > 
                                <span class="ui-button-text">Buscar</span>
                            </button>  
                        </div>
                    </td>
                </tr>
            </table>
            <table id="tabla_cuentas" ></table>
            <div id="pager3"></div> 
        </div>
        <div id="dialogMsjimpuesto" class="ventana">
            <table id="tabla_impuesto" ></table>
            <div id="pager2"></div>
        </div>
        <div id="dialogMsjNumos" class="ventana">
            <table id="tablainterna" >
                <tr>
                    <td>
                        <label>Multiservicio</label>
                    </td>
                    <td>
                        <input type="text" id="multiservicio"  style="width: 290px" >
                    </td>
                </tr>
            </table>
        </div>
        <div style="position: relative;top: 231px;">
            <table id="tabla_anticipo" ></table>
            <div id="pager4"></div>
        </div>
        <div id="dialogMsjLegalizar" class="ventana">
            <div id="tablainterna" style="width: 540px;margin-bottom: 19px;" >
                <table id="tablainterna"  >
                    <tr>
                        <td>
                            <label><b>Anticipo</b></label>
                            <input type="text" id="anticipo_" style="width: 95px;color: black;margin-left: 28px;border: 0px none;"readonly>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label><b>Identificacion:</b></label>
                            <input type="text" id="identificacion_" style="width: 95px;color: black;border: 0px none;"readonly>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label><b>Empleado:</b></label>
                            <input type="text" id="empleado_" style="width: 236px;color: black;border: 0px none;"readonly>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label hidden><b>Autorizador</b></label>
                            <input type="text" id="autorozador_" style="width: 95px;"readonly hidden>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="tablainterna_" style="width: 520px;margin-left: 621px;margin-top: -90px;" >
                <table id="tablainterna" style="border: 1px solid grey !important;" >
                    <tr>
                        <td>
                            <label style="margin-left: 15px;"><b>Numero CXC</b></label>
                            <input type="text" id="num_factura" style="width: 95px;color: black;margin-left: 28px;border: 0px none;" readonly>
                            <label><b>Valor CXC</b></label>
                            <input type="text" id="valor_factura" style="width: 95px;color: black;margin-left: 48px;text-align: right;border: 0px none;"readonly>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label style="margin-left: 227px;"><b>Valor legalizar</b></label>
                            <input type="text" id="valor_legalizar" style="width: 95px;color: black;margin-left: 17px;text-align: right;border: 0px none" class="solo-numeric" onkeypress="return onKeyDecimal(event, this)"onkeyup="calcularTotal()" readonly>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <hr style="width: 230px;margin-left: 229px;">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label style="margin-left: 229px;"><b>Total</b></label>
                            <input type="text" id="total_" style="width: 95px;color: black;margin-left: 81px;text-align: right;border: 0px none;"class="solo-numeric"readonly>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="position: relative;top: 45px;">
                <table id="tabla_factura" ></table>
                <div id="pager"></div>
            </div>
            <div style="position: relative;top: 80px;">
                <table id="tabla_factura_detalle" ></table>
                <div id="pager1"></div>
            </div>
        </div>
    </center>
</body>
<script>
    $(document).ready(function () {
        $('.solo-numero').keyup(function () {
            this.value = (this.value + '').replace(/[^0-9]/g, '');
        });

        $('.solo-numeric').live('blur', function (event) {
            this.value = numberConComas(this.value);
        });

        function numberConComas(x) {
            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        }
    });
</script>
</html>
