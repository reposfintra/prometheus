
<!--
- Autor : Ing. Andrés Maturana
- Date  : 22 Enero 2007
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite mostrar cheques impresos
--%>

<%-- Declaracion de librerias--%>
<%@page session="true"%> 
<%@page import="java.util.*" %>
<%@page import="com.tsp.operation.model.*, com.tsp.operation.model.beans.*,com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>


<html>
<head>
    <title>Anulaci&oacute;n de Cheques</title>
	<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
	<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>    
	<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>      
	<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/Validaciones.js"></script>  
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
 <%
 	Egreso egreso = model.ChequeXFacturaSvc.getEgreso();
 %>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
  <input type="HIDDEN" name="sucursalbanco" value="<%= egreso.getBank_account_no()%>">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Anulación de Cheques"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
 <form name="forma" method="post" action="<%=CONTROLLER%>?estado=ChequeCxP&accion=Anular&opcion=3">
   <table width="821" border="2" align="center">
    <tr>
      <td>
		<TABLE width='100%' class="tablaInferior">
				<tr class="fila">
					<td width="50%" colspan="2" class="subtitulo1">&nbsp;Anular Cheques Impresos</td>
					<td width="50%" colspan="2" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
				</tr>
                <TR class='fila'>
                     <TH width='16%' align='center' > <div align="left"><strong>NUMERO</strong></div></TH>
                  <TH width='33%' align='center'><%= egreso.getDocument_no()%>
				  
                       <input name="cheque" type="hidden" id="cheque" value="<%= egreso.getDocument_no()%>"> 
					   <input name="banco" type="HIDDEN" id="banco" value="<%= egreso.getBranch_code()%>"> 
					   <input name="sucursal" type="HIDDEN" id="sucursal" value="<%= egreso.getBank_account_no()%>">
                  </TH>
                  <TH width='17%' align='center'> Valor         </TH>
                     <TH width='34%' align='center' >  <%= Util.customFormat(egreso.getVlr_for())%>&nbsp;<%= egreso.getCurrency()%> </TH>
                </TR>
                <TR class='fila'> 
                     <TD>Beneficiario</TD>
                  <TD align='left'   ><%= egreso.getPayment_name()%></TD>
                       <TD align="center">Banco</TD>
                       <TD align='center' ><%= egreso.getBranch_code() + " " + egreso.getBank_account_no()%> </TD>                 
    			</TR>
                <TR class='fila'>
                  <TD >Agencia</TD>
                  <TD align='left'><%= egreso.getAgency_id()%></TD>
                  <TD align='center' >Usuario</TD>
                  <TD align='center' ><%= egreso.getCreation_date()%></TD>
                </TR>
                <TR class='fila'>
                  <TD colspan="4" align='center' ><strong>Información de la Anulación</TD>
                </TR>
                <TR class='fila'>
                  <TD >Causa</TD>
                  <TD align='left'   ><%TreeMap causas= model.causas_anulacionService.getTcausasa(); %>
          			<input:select name="causas" options="<%=causas%>" attributesText="style='width:100%;'" /></TD>
                  <TD >Tipo de Recuperación</TD>
                  <TD align='center' >CHEQUE TSP ANULADO
  <input name="trecuperacion" type="hidden" value="CA"></TD></TR>
                <TR class='fila'>
                  <TD height="20" >Observación</TD>
                  <TD colspan="3" align='left'   ><textarea name="observacion" rows="6" class="textbox" id="observacion" style="width:100% "></textarea></TD>
                </TR>
		  </TABLE>
	  </td>
    </tr>
  </table>
		<p align="center">
			<img src="<%=BASEURL%>/images/botones/aceptar.gif" name="c_aceptar" onClick="if (TCamposLlenos()){ forma.submit(); }" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;	 	
			<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >
		</p>
 </form>
 </div>
</body>
</html>
