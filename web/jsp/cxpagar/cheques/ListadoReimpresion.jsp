<!--
- Autor : Ing. Henry Osorio
- Modificado Ing Sandra Escalante
- Date  : 02 Febrero 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Vista que muestra las corridas 
--%>

<%-- Declaracion de librerias--%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.util.Util"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
  String msg = (request.getParameter("msg")!=null)? request.getParameter("msg"):"";
  List listado = model.ReimpresionSvc.getListaCheques();
  String visible ="visible";
  
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);

%>
<html>
<head>
<title>.: Autorizar Reimpresion de Cheques</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type='text/javascript' src="<%=BASEURL%>/js/reporte.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/script.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Autorizar Reimpresion de Cheques"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<br>
<form name="form1" method="post" id="form1"  action="<%=CONTROLLER%>?estado=Reim&accion=presion&opcion=UPDATE">
<br>
	<table width="600" border="2" align="center">
    	<tr>
      	<td>
	  		<table width="100%" align="center">
        		<tr>
            		<td width="50%"  height="22" class="subtitulo1">Listado de cheques </td>
                	<td width="411" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
          		</tr>
		  </table>
  		  <table width="100%" border="0" height="10">
			    <tr>
                            <td>
			<table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
	 			<tr class="tblTitulo">
    				<td width='150' align="center" >Banco</td>    				
				<td width='200' align="center" >Sucursal</td>
				<td width='180' align="center" >Cheque</td>
				<td width='*' align="center">Reimpresion</td> 
    			</tr>
    			</table>
    			</td>
			    </tr>
			    <tr>
			    <td><div  style=" overflow:auto ; WIDTH: 100%; HEIGHT:340px; "><table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4" id="principal">

				<%Iterator it = listado.iterator();
                                 int c = 0;
    			while(it.hasNext()){
    				 Egreso eg  =  (Egreso) it.next(); 
                                 String Check = (eg.isReimpresion())?"checked":"";
				%>
	
  				<tr class="<%=(c%2==0?"filagris":"filaazul")%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand">        				
				<td  width='135'   align="center" class="bordereporte" ><%=eg.getBranch_code()%></td>        
    				<td  width='180'   align="center" class="bordereporte" ><%=eg.getBank_account_no()%></td>
    				<td  width='158'   align="center"  class="bordereporte" ><%=eg.getDocument_no()%></td>
    				<td  width='*'    align="center"><input type="checkbox" <%=Check%> value='<%=eg.getDocument_no()%>' name='chequesSelec'></td>
    				</tr>
  				<%}c++;//fin while%>
				</table></div>
				</td>
                            </tr>
		  </table>
		</td>
		</tr>
	</table>
	<br>
	<% if(!msg.equals("")){ visible ="hidden";}%>
	<div id='boton' style='visibility:<%=visible%>'>
	<table width="76%"  border="0" align="center">
  	<tr>
    	<td align="center">
		    <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar"   height="21"onMouseOver="botonOver(this);" onClick="form1.submit();" onMouseOut="botonOut(this);" style="cursor:hand"> 
		    <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" height="21" onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">
		    
		</td>
	</tr>
	</table>
	</div>
    <%if(!msg.equals("")){%>
    <div id="msg" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;visibility:visible;"> 
            <table width="400" border="1" align="center"  bordercolor="#123456" cellpadding='0' cellspacing='0' >
                <tr >
                    <td>
                        <table width="103%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                            <tr>
                                <td width="290" align="center" class="mensajes"><%=msg%><br><br><img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imga" height="21" onMouseOver="botonOver(this);" onClick="msg.style.visibility = 'hidden';boton.style.visibility = 'visible';" onMouseOut="botonOut(this);" style="cursor:hand"></td>
                                <td width="38" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="68">&nbsp;</td>
                            </tr>
                           
                        </table>
                    </td>
                </tr>
  			
            </table>
        </div>	
  <%}%>
	
</form>

</div>
<%=datos[1]%>
</body>
</html>


