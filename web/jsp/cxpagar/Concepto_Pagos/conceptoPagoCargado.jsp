<!--
- Autor : Ing. Armando Oviedo C
- Date  : 10 de Enero de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra un mensaje de confirmación cuando se carga un objeto tipo ConceptoPago
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>Concepto De Pago - Cargado</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body ONLOAD="redimensionar();" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Concepto de pago cargado"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
  if(request.getParameter("mensaje")!=null ) {
            out.print("<table width=450 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=350 align=center class=mensajes>"+request.getParameter("mensaje")+"</td><td width=100><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");            
  }
  %>
  <br>
  <table align=center width='100%'>
    <tr>
	<td align="center" colspan="2">
            <img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="parent.close();"></img>
	</td>		
    </tr>
  </table>
  </div>
</body>
</html>
