<!--
- Autor : Ing. Armando Oviedo C
- Date  : 10 de Enero de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra las opciones de agregación, modificación y eliminación
--%>

<%-- Declaracion de librerias--%>
<%@ page session="true" %>
<%@ page import="java.util.*" %>
<%@ page import="com.tsp.operation.model.beans.*" %>
<%@page import="com.tsp.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp" %>
<%    
    List lista = model.ConceptoPagosvc.getListaFolder();
    String rec = request.getParameter("recargarDirs");
    
%>
<html>
<head>                
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
	<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/finanzas/presupuesto/move.js"></script>
    <title>Conceptos De Pago</title>
    <script src='<%= BASEURL %>/js/ConceptoPago.js'></script>
    <script src='<%= BASEURL %>/js/Validaciones.js'></script>
    <script>                        
        var opNew  = new objetoOpcion('','','','','','','','','');
		BASEURL    = '<%= BASEURL %>';
		CONTROLLER = '<%= CONTROLLER %>';		
        <%= model.ConceptoPagosvc.getVarJSSeparador()%>        
    </script>    
</head>
<body onload="<%=(request.getParameter("reload")!=null? "parent.code.location.reload();": "")%>" onMouseMove="_Move('formNuevo');" onMouseUp="_stopMove();">    
    <div style="visibility:hidden;position:absolute" id="listaNombres" >
        <select name='nombres' style="width:247" onchange="colocarInfo( this.value );">
            <option value="" SELECTED>Seleccione</option>
            <%
                LinkedList tablas = model.tablaGenService.obtenerTablas();
                if( tablas != null ){
                    for( int i=0;i< tablas.size(); i++ ){
                        TablaGen tgen = (TablaGen)( tablas.get( i ) );
                        String nombre = tgen.getDescripcion();
                        String elemento = tgen.getTable_code();
                        String referencia = tgen.getReferencia();
                        String cadTemp = nombre+";"+elemento+";"+referencia;
                        out.println("<option value='"+cadTemp+"'> " + tgen.getDescripcion() + "</option>");
                    }
                }
            %>
        </select>        
    </div>
    <div style="visibility:hidden;position:absolute" id="soloNombres" >
        <input name='nombres' style="width:247" type='text' class='textbox' onchange="formularioNuevo.Nombre.value=this.value;" >
    </div>
    <FORM METHOD='POST' ACTION="<%= CONTROLLER %>?estado=ConceptoPago&accion=Entrar&pagina=conceptosindex.jsp&carpeta=jsp/cxpagar/Concepto_Pagos" NAME='formularioNuevo' onsubmit='javascript: return validarFormulario(this);'>
    <div style="HEIGHT:170px; WIDTH:450px; TOP:20px; LEFT:50px; background-color:#EEEEEE; BORDER-RIGHT: #666666 1px solid; PADDING-RIGHT: 2px; BORDER-TOP: #666666 1px solid; PADDING-LEFT: 2px;  VISIBILITY: hidden; PADDING-BOTTOM: 2px; BORDER-LEFT: #666666 1px solid; PADDING-TOP: 2px; BORDER-BOTTOM: #666666 1px solid; POSITION: absolute; " id='formNuevo'>
        <TABLE align="center" width='100%' class="tablaInferior" border='2'>   
            <TR style='cursor: move;' ondragstart='' onMouseDown="_initMove('formNuevo');">
              <TD>                                
                    <TABLE width='100%' class="tablaInferior" cellspacing='1'>
                        <tr>
                            <td class="subtitulo1" width='50%'>Nueva Opcion</td>
                            <td class="barratitulo" width='50%'><img src="<%=BASEURL%>/images/titulo.gif" ></td>                                                    
                        </tr>
                    </table>
                    <table width='100%' class='tablaInferior' >
                        <tr class='fila' valign="top">
                            <TD>Nombre</TD>
                            <TD>
                                <div id="seleccion" >                                    
                                </div>
                                <input name='Nombre' type='hidden' value="" >
                            </TD>
                            <TD rowspan='5' align=left>Tipo Opcion<br><input type='radio' name='idFolder' id='idFolder1' onclick="cambiarLista()" value='Y' checked>Subcuenta<br><input type='radio' name='idFolder' id='idFolder2' onclick="cambiarLista();" value='N'>Cuenta</TD>
                        </tr>                        
                        <TR class='fila'>    
                            <TD>Referencia 1</TD>
                            <TD><input name='ref1' type='text' class='textbox' value='' size="45" maxlength="45"></TD>                            
                        </TR>
                        <TR class='fila'>    
                            <TD>Referencia 2</TD>
                            <TD><input name='ref2' type='text' class='textbox' value='' size="45" maxlength="45"></TD>                            
                        </TR>
                        <TR class='fila'>    
                            <TD>Referencia 3</TD>
                            <TD><input name='ref3' type='text' class='textbox' value='' size="45" maxlength="45"></TD>                            
                        </TR>
                        <TR class='fila'>    
                            <TD>Referencia 4</TD>
                            <TD><input name='ref4' type='text' class='textbox' value='' size="45" maxlength="45"></TD>                            
                        </TR>
                        <TR class='fila'>
                            <td valign=center><input type=checkbox name=movera value='Mover a: ' onclick="moveradest(this.form);">Mover a:</td>
                            <td colspan='2' id="moverselect">
                                <select name='moveradestino'>
                                    <option value='0' SELECTED>RAIZ</option>
                                    <%                                                                                 
                                        List listafs = model.ConceptoPagosvc.getSoloFolders();										
                                        if(listafs!=null){											
                                            for(int i=0;i<listafs.size();i++){
                                                ConceptoPago tmp = (ConceptoPago)(listafs.get(i));
                                                String subcuenta = "";
                                                for(int j=0;j<tmp.getNivel();j++){
                                                    subcuenta += "- ";
                                                }
                                                out.print("<option value="+tmp.getCodigo()+"> "+subcuenta+" "+tmp.getDescripcion()+"</option>");
                                            }
                                        }
                                    %>
                                </select>
                            </td>                            
                        </TR>
                    </TABLE>                                                
                </TD>
            </TR>            
      </TABLE>
	  <br>
	  <table align="center">
	  	<tr>
			<td>
				<input type='hidden' name='Id' value=''>
				<input type='hidden' name='mover' value=''>
				<input type='hidden' name='nivel' value=''>
				<input type='hidden' name='index' value=''>
				<input type=hidden name='Opcion' id='Opcion'>
				<img name='botonVariable' id='botonVariable' onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="validar(formularioNuevo);"> 
				<img src="<%= BASEURL %>/images/botones/cancelar.gif" onclick="formNuevo.style.visibility='hidden';" onmouseover="botonOver(this);" onmouseout="botonOut(this);">     
			</td>
		</tr>
  	  </table>              
      </div>
    </FORM>
<center>
<form action='<%= CONTROLLER %>?estado=ConceptoPago&accion=Entrar&pagina=conceptosindex.jsp&carpeta=jsp/cxpagar/Concepto_Pagos' method='post' name='FormularioListado' onsubmit='javascript: return validarListado(); '>
<TABLE align="center" width='100%' class="tablaInferior">   
    <TR>
        <TD>  
            <TABLE width='100%'class="tablaInferior">
                <TR>
                    <td class="subtitulo1" colspan='5' align=center>Opcion : <%= model.ConceptoPagosvc.getTrazado() %></td>
                    <td class="barratitulo" colspan='3'><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>                                                                
                </TR>                
                <% if (lista!=null && lista.size()>0){ %>                         
                <tr class='tblTitulo'>
                    <td width='20' align=center><input type='checkbox' name='All' onclick='jscript: selAll();'></td>
                    <td width='230' align=center colspan='2'>Opcion</td>
                    <td width='60' align=center>Nivel</td>
                    <td width='60' align=center>Codigo</td>
                    <td width='85' align=center>Referencia 3</td>
                    <td width='85' align=center>Referencia 4</td>
                    <td width='100' align=center >&nbsp</td>
                </tr>
                <%   for (int i=0;i<lista.size();i++){
                        ConceptoPago cpt = (ConceptoPago) lista.get(i); %>
                <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style='cursor:hand'>
                    <td><input type='checkbox' name='LOV' value='<%= cpt.getCodigo() %>' onclick='jscript: actAll();'></td> 
                    <td align='center'><img src="<%= BASEURL %>/images/menu-images/<%= (cpt.getId_folder().equalsIgnoreCase("Y")?"menu_folder_closed.gif":"menu_link_local.gif") %>"></td>
                    <td><%= cpt.getDescripcion()%></td>
                    <td align=center><%= cpt.getReferencia1()%></td>
                    <td align=center><%= cpt.getReferencia2()%></td>
                    <td align=center><%= (cpt.getReferencia3()!= null)? cpt.getReferencia3() : "" %></td>
                    <td align=center><%= (cpt.getReferencia4()!= null)? cpt.getReferencia4() : "" %></td>
                    <td align='center'>
                            <img title='Modificar' src="<%= BASEURL %>/images/botones/iconos/modificar.gif"  name="imgsalir"  onClick="goYaNIN ('<%= i %>'); /*edicion('<%= i %>', true);*/" style="cursor:hand">
							<%= (cpt.getId_folder().equalsIgnoreCase("Y")? "<img title='Ver Detalles' src="+BASEURL+"/images/botones/iconos/detalles.gif width='16px'  name=imgcancelar onClick=document.location.href='"+CONTROLLER+"?estado=ConceptoPago&accion=Entrar&pagina=conceptosindex.jsp&carpeta=jsp/cxpagar/Concepto_Pagos&Opcion=Ver&Index="+i+"';": "") %>                                       
                    </td>
                </tr>
                <script>var op<%= i %> = new objetoOpcion('<%= cpt.getCodigo() %>','<%= cpt.getDescripcion() %>','<%= cpt.getId_folder()%>','<%= cpt.getReferencia1()%>','<%= cpt.getReferencia2()%>','<%= cpt.getNivel()%>','<%= cpt.getReferencia3()%>','<%= cpt.getReferencia4()%>','<%= cpt.getDescripcion()+";"+cpt.getReferencia1()+";"+cpt.getReferencia2() %>');  </script>
<%   } %>            
<% } %>
			</TABLE>
        </TD>
    </TR>    
</table>    
<br>
<table align="center">
	<tr>
		<td>
			<input type=hidden name=Opcion value=''>
			<img src="<%= BASEURL %>/images/botones/agregar.gif" value='Nuevo' onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="botonAgregar(formularioNuevo);edicion('New', false);" title='Agregar Nuevo'></img>
			<img title='Eliminar' src="<%= BASEURL %>/images/botones/eliminar.gif" value='Eliminar' onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="(validarListado(FormularioListado))?eliminar(FormularioListado):''" class='boton'></img>
			<% 
				if(model.ConceptoPagosvc.getListaTraza()!=null && model.ConceptoPagosvc.getListaTraza().size()>0){
					out.print("<img title='Regresar' src="+BASEURL+"/images/botones/regresar.gif type='button' class='boton' onClick=document.location.href='"+CONTROLLER+"?estado=ConceptoPago&accion=Entrar&pagina=conceptosindex.jsp&carpeta=jsp/cxpagar/Concepto_Pagos&Opcion=Reg' value='Regresar'></img>");
				}else{
					out.print("<img src="+BASEURL+"/images/botones/regresarDisable.gif type='button' class='boton' onClick=document.location.href='"+CONTROLLER+"?estado=ConceptoPago&accion=Entrar&pagina=conceptosindex.jsp&carpeta=jsp/cxpagar/Concepto_Pagos&Opcion=Reg' value='Regresar'></img>"); 
				}
			%>
			<img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close()">
		</td>
	</tr>
</table>
</form>
</body>
</html>

<% if (request.getParameter("index")!=null && !request.getParameter("index").equals("")){ %>
<script>
  edicion('<%= request.getParameter("index") %>', true);
</script>
<% } %>

