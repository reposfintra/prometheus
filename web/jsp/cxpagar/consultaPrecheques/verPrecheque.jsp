<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.finanzas.contab.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.finanzas.contab.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
    <head><title>Ver Detalle Precheque</title>
        
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="/css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/finanzas/contab/comprobante.js"></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/contabilidad.js"></script>
        
        <style type="text/css">
            <!--
            .Estilo1 {color: #D0E8E8}
            -->
        </style>
    </head>

<% 
    Egreso cabecera = model.prechequeSvc.getEgreso();
    Vector detalle = model.prechequeSvc.getEgresos();
    //session.setAttribute( "resultadoE", null );
%>  
    <body onresize="redimensionar()" >

        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Ver detalle Precheque"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <form name="forma1" id="forma1" action="" method="post">
            <table width="900" align="center">				
                <tr>
                    <td>       
                        <table width='100%' border="2">
                            <tr>
                            <td>
                                <table width="100%" align="center"  >
                                    <tr >
                                    <td width="50%"  class="subtitulo1"><p align="left">Documento</p></td>
                                    <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" class="Estilo1" height="20" align="left"><%=datos[0]%></td>
                                    </tr>
                                </table>
                                <table width="100%" align="center" cols="7">
                                    <tr >                
                                    <td class="fila" width="110">&nbsp;Distrito</td>
                                    <td class="letra" width="100"><%= cabecera.getDstrct() %>&nbsp;</td>
                                    <td class="fila" width="100">&nbsp;Banco </td>
                                    <td width="154" class="letra"><%= cabecera.getBranch_code() %>&nbsp;</td>
                                    <td class="fila" width="115">&nbsp;Sucursal </td>
                                    <td width="273" class="letra"><%= cabecera.getBank_account_no() %>&nbsp;</td>
                                    </tr>    
                                    <tr >
                                      <td class="fila" align="left">&nbsp;Estado</td>
                                      <td class="letra" align="left"><%= cabecera.getEstado() %></td>
                                    <td class="fila">&nbsp;Serial</td>
                                    <td class="letra"><%= cabecera.getDocument_no() %>&nbsp;</td>
                                    <td class="fila">&nbsp;Fecha Cheque</td>
                                    <td class="letra" align="left"><%= cabecera.getFechaEntrega() %>&nbsp;</td>
                                    </tr>    
                                    <tr >
                                    <td class="fila">&nbsp;Nit</td>
                                    <td class="letra"><%= cabecera.getNit() %>&nbsp;</td>
                                    <td class="fila">&nbsp;Beneficiario</td>
                                    <td class="letra"><%= cabecera.getPayment_name() %>&nbsp;</td>
                                    <td class="fila">&nbsp;Agencia</td>
                                    <td class="letra"><%= cabecera.getAgency_id() %>&nbsp;</td>
                                    </tr>
                                </table>
                            </td>                
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table width="900" align="center" border="2">
                <tr>
                    <td >
                    <table width="100%" align="center" >
                        <tr >
                            <td width="50%"   class="subtitulo1"><p align="left">Detalle del Documento </p></td>
                            <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" class="Estilo1" height="20" ></td>
                        </tr>
                    </table>                        
                    <table id="detalle" width="100%" >
                        <tr  id="fila1" class="tblTitulo">
                            <td align="center" nowrap>&nbsp;Item&nbsp;  </td>
                            <td align="center">Proveedor</td>
                            <td align="center">Tipo Documento</td>
                            <td align="center">Documento</td>
                            <td align="center">Tipo Pago </td>
                            <td align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Moneda&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td align="center">&nbsp;Valor&nbsp;</td>
                        </tr>				
				<%                                 
				if ( detalle != null){
                                   for( int i = 0; i < detalle.size(); i++ ){
                                        Egreso eg = (Egreso) detalle.get( i );
                                %>							   
                        <tr class="<%= (i%2==0)? "filagris" : "filaazul" %>" style="cursor:default" bordercolor="#D1DCEB" align="center">                            
                            <td align="center" class="informacion"><%=eg.getItem_no()%>&nbsp;</td>
                            <td align="left" class="informacion" nowrap ><%=eg.getNit() + " " + eg.getPayment_name()%>&nbsp;</td>
                            <td align="center" class="informacion" nowrap><%=eg.getTipo_documento()%>&nbsp;</td>
                            <td align="center" class="informacion" ><%=eg.getDocumento()%></td>
                            <td align="center" class="informacion" ><%=eg.getTipo_pago()%></td>
                            <td align="right" ><div align="center"><span class="informacion"><%= cabecera.getCurrency()%></span></div></td>
                            <td align="right" ><%=UtilFinanzas.customFormat2(eg.getVlr())%></td>
                        </tr>	   
                                   <%}%>
                            <%}%>
                        <tr class="tblTitulo" style="cursor:default" align="center">
                          <td colspan="5" align="center" class="informacion"><div align="right">TOTAL</div></td>
                          <td align="right" ><div align="center"><span class="informacion"><%= cabecera.getCurrency()%></span></div></td>
                          <td align="right" ><%=UtilFinanzas.customFormat2( cabecera.getVlr() )%></td>
                        </tr>			
                    </table>
                    </td>
                </tr>
            </table>
            <br>    
            <center>                
                &nbsp;&nbsp;<img src='<%=BASEURL%>/images/botones/salir.gif' name='Buscar' align="absmiddle" style='cursor:hand'   onClick="window.close();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
            </center>
	
        </div>
        <tr>
        </form>
        </div>
    </body>    
<%=datos[1]%>
</html>s