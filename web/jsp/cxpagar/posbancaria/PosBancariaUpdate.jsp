<!--
- Autor : Ing. Tito Andr�s Maturana
- Date  : 18 enenro del 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la consulta en el archivo de posici�n bancaria.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %><html>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page errorPage="../../error/ErrorPage.jsp"%>
<head>
<title>Posici&oacute;n Bancaria - Modificar</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Estilo1 {color: #D0E8E8}
-->
</style>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/posbancaria.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/posbancaria.js"></script>
</head>

<body onresize="redimensionar()" onload = "redimensionar();<%if(request.getParameter("mensaje").equalsIgnoreCase("MsgModificado") ){ %>window.opener.location.reload();<%}%>">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Posici�n Bancaria - Modificar"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
	PosBancaria b = (PosBancaria) model.posbancariaSvc.getPosbanc();
%>
<form name="forma" method="post" action="<%= CONTROLLER%>?estado=PosBancaria&accion=Update&cmd=show" id="forma">
  <table  width="463" border="2" align="center">
    <tr >
      <td>        <table width="100%">
        <tr>
          <td width="50%" class="subtitulo1"> Banco            </td>
          <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
        </tr>
      </table>
        <table width="100%" align="center" class="tablaInferior">
          <tr>
            <td width="14%" class="fila"><div align="left">Agencia</div></td>
            <td width="33%" class="letra"><%= request.getAttribute("agencia") %></td>
            <td width="21%" class="fila">Banco</td>
            <td width="32%" class="letra"><%= request.getAttribute("banco") %></td>
          </tr>
          <tr>
            <td class="fila">Sucursal</td>
            <td class="letra"><%= request.getAttribute("sucursal") %>              <input name="agency_id" type="hidden" id="agency_id" value="<%= request.getAttribute("agency_id") %>">
              <input name="saldo_ant" type="hidden" id="saldo_ant" value="<%= com.tsp.util.UtilFinanzas.customFormat(b.getSaldo_anterior()) %>">
              <input name="agencia" type="hidden" class="textbox" id="agencia" value="<%= request.getAttribute("agencia") %>" size="35" readonly>
              <input name="banco" type="hidden" class="textbox" id="banco" value="<%= request.getAttribute("banco") %>" size="30" readonly>
              <input name="sucursal" type="hidden" class="textbox" id="sucursal" value="<%= request.getAttribute("sucursal") %>" size="35" readonly>
              <input name="fecha" type="hidden" class="textbox" id="fecha" value="<%= request.getAttribute("fecha") %>" readonly></td>
            <td class="fila">Fecha</td>
            <td class="letra"><%= request.getAttribute("fecha") %></td>
          </tr>
        </table>
        <table width="100%">
          <tr>
            <td width="50%" class="subtitulo1"> Posici&oacute;n Bancaria </td>
            <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif">              </td>
          </tr>
        </table>
        <table width="100%" align="center" class="tablaInferior">
          <tr class="fila">
            <td width="28%"><div align="left">Saldo Inicial </div></td>
            <td width="23%"><span class="bordereporte">
            <input name="campo1" id="campo1" type="text" class="textbox" size="14" maxlength="9" onFocus="this.value = this.value.replace(/,/g,'');" onBlur="if(this.value=='') this.value=0; formatear(this);"  onChange="if(this.value=='') this.value=0; formatear(this); actualizarPendiente2(); actualizarNSaldo2();"  onKeyPress="soloDigitos2(event,'decNo',this)" value="<%= com.tsp.util.UtilFinanzas.customFormat(b.getSaldo_inicial()) %>" style="text-align:right">
</span></td>
            <td width="26%">Pendiente</td>
            <td width="23%"><span class="bordereporte">
              <input name="campo2" id="campo2" type="text" class="textbox" size="14" maxlength="9" value="<%= com.tsp.util.UtilFinanzas.customFormat(b.getSaldo_anterior() - b.getSaldo_inicial()) %>" style="text-align:right" readonly>
            </span></td>
          </tr>
          <tr class="fila">
            <td>Anticipos y Cancelaciones OP </td>
            <td><span class="bordereporte">
              <input name="campo3" id="campo3" type="text" class="textbox" size="14" maxlength="9" value="<%= com.tsp.util.UtilFinanzas.customFormat(b.getAnticipos()) %>" style="text-align:right"  readonly>
            </span></td>
            <td>Proveedores</td>
            <td><span class="bordereporte">
              <input name="campo4" id="campo4" type="text" class="textbox" size="14" maxlength="9" value="<%= com.tsp.util.UtilFinanzas.customFormat(b.getProveedores()) %>" style="text-align:right"  readonly>
            </span></td>
          </tr>
          <tr class="fila">
            <td>Cupo</td>
            <td><span class="bordereporte">
              <input name="campo5" id="campo5" type="text" class="textbox" size="14" maxlength="9" onFocus="this.value = this.value.replace(/,/g,'');" onBlur="if(this.value=='') this.value=0; formatear(this);"  onChange="if(this.value=='') this.value=0; formatear(this);"  onKeyPress="soloDigitos(event,'decNo')" value="<%= com.tsp.util.UtilFinanzas.customFormat(b.getCupo()) %>" style="text-align:right">
            </span></td>
            <td>Nuevo Saldo </td>
            <td><span class="bordereporte">
              <input name="nuevo_saldo" id="nuevo_saldo" type="text" class="textbox" size="14" maxlength="9" value="<%= com.tsp.util.UtilFinanzas.customFormat(b.getNuevo_saldo()) %>" style="text-align:right" readonly>
            </span></td>
          </tr>
        </table></td>
    </tr>
  </table>
  <div align="center"><br>
      <img src="<%=BASEURL%>/images/botones/modificar.gif" name="c_modificar" onClick="if( validarFormaUpdate()) forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"  style="cursor:hand ">&nbsp; <img src="<%=BASEURL%>/images/botones/anular.gif" name="c_anular" onClick="if ( validarFormaUpdate() ) forma.action= forma.action + '&anular=OK'; forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">&nbsp; <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_buscar" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand "></div>
  </form>
<%
if(request.getParameter("msg")!=null){%>
<table border="2" align="center">
  <tr>
    <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="282" align="center" class="mensajes"><%=request.getParameter("msg").toString() %></td>
          <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="78">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<br>
<%
	}
%>
</table>
<br>
</div>
</body>
</html>
