<!--
- Autor : Ing. Tito Andr�s Maturana
- Date  : 18 enenro del 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la actualizacion en el archivo de posici�n bancaria.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %><html>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page errorPage="../../error/ErrorPage.jsp"%>
<head>
<title>Posici&oacute;n Bancaria - Listar</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/posbancaria.js"></script>
<script>
		var url = '<%= CONTROLLER %>';
		function actualizarBancos() {
            forma.action = "<%= CONTROLLER%>?estado=PosBancaria&accion=Refresh&cmd=show&listar=OK";
            forma.agenciaSelec.value = forma.agencia.options(forma.agencia.selectedIndex).value;
            forma.submit();
        }  
</script>
<script>
maximizar();
</script>
</head>

<body onresize="redimensionar()" onload = "redimensionar();">
<%
	TreeMap agencias = model.agenciaService.getAgencia();
        TreeMap bancos = model.servicioBanco.getBanco();
	
	agencias.remove("");
	agencias.put(" Todas", "");  
        bancos.put(" Todos", "");
	
	String hoy = Utility.getDate(4).replaceAll("/","-");
	
	String agenciasel = ( request.getAttribute("agencia")!=null ) ? (String) request.getAttribute("agencia") : "";	
        String bancosel = ( request.getAttribute("banco")!=null ) ? (String) request.getAttribute("banco") : "";
		String fechasel = ( request.getAttribute("fecha")!=null ) ? (String) request.getAttribute("fecha") : hoy;
	
        Vector vector = (Vector) request.getAttribute("bancos");
		
		double ttl_saldoi = 0;
		double ttl_pend = 0;
		double ttl_op = 0;
		double ttl_prov = 0;
		double ttl_cupo = 0;
		double ttl_nsaldo = 0;
%>	
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Posici�n Bancaria - Listar"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: -1px; top: 100px; overflow: scroll;">
<form name="forma" method="post" action="" id="forma">
  <table width="1000" border="2" align="center">
    <tr>
      <td><table width="100%">
        <tr>
          <td width="373" class="subtitulo1"> Banco </td>
          <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif">
            <input name="agenciaSelec" type="hidden" id="agenciaSelec"></td>
        </tr>
      </table>
        <table width="100%"  border="0" class="tablaInferior">
          <tr class="fila">
            <td width="5%">Agencia</td>
            <td width="13%"><input:select name="agencia" default="<%= agenciasel %>" attributesText="class=textbox onChange=actualizarBancos();" options="<%= agencias %>"/></td>
            <td width="5%">Banco</td>
            <td width="12%"><input:select name="banco" default="<%= bancosel %>" attributesText="class=textbox;" options="<%= bancos %>"/></td>
            <td width="5%">Fecha</td>
            <td width="21%"><input name="fecha" type='text' class="textbox" id="fecha" style='width:120' value='<%= fechasel%>' readonly>
              <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma.fecha);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16"
               border="0" alt="De click aqu&iacute; para escoger la fecha de la posici&oacute;n bancaria"></a></td>
            <td width="5%"><img src="<%= BASEURL%>/images/botones/iconos/buscar.gif" alt="Buscar posici&oacute;n bancara en la fecha seleccionada."  name="imgsalir" align="absmiddle" style="cursor:hand " onClick="if(validarForma()) window.location.href='<%= CONTROLLER%>?estado=PosBancaria&accion=Refresh&cmd=show&listar=OK&cargar=OK&agenciaSelec=' + forma.agencia.options(forma.agencia.selectedIndex).value + '&bancoSelec=' + forma.banco.options(forma.banco.selectedIndex).value + '&fecha=' + forma.fecha.value;"></td>
            <td width="14%" class="Simulacion_Hiper"><a href="JavaScript:void(0);" onClick="window.open('<%=CONTROLLER%>?estado=Banco&accion=Serch&listar=True&sw=True&c_agencia=' +  forma.agencia.options(forma.agencia.selectedIndex).value + '&c_distrito=<%= (String) session.getAttribute("Distrito") %>&c_banco=&c_sucursal=&c_cuenta&c_codigo_cuenta=&posbancaria=','BuscarBancos','status=yes,scrollbars=yes,width=780,height=450,resizable=yes');">Buscar Bancos</a> </td>
            <td width="20%"><% if ( vector!= null && vector.size()>0 && request.getAttribute("NoTraslado")==null ) { %>
		<a class="Simulacion_Hiper" href = "JavaScript:trasladarSaldos(url);">              
              Trasladar Saldos</a> <%}//href="JavaScript:void(0);  + '&fechatr='; //,'Traslado','status=yes,scrollbars=no,width=450,height=200,resizable=no'); //window.location.reload();" %></td>
          </tr>
        </table>
<%
    if ( vector!= null && vector.size()>0 ) {
%>        
        <table width="100%">
          <tr>
            <td width="373" class="subtitulo1"> Posici&oacute;n Bancaria </td>
            <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
          </tr>
        </table>
        <table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4">
          <tr class="tblTitulo">
            <td width="12%" rowspan="2" align="center">Agencia</td>
            <td width="13%" rowspan="2" align="center">Banco</td>
            <td width="13%" rowspan="2"  align="center">Sucursal</td>
            <td width="11%" rowspan="2"  align="center">Saldo Inicial </td>
            <td width="9%" rowspan="2"  align="center">Pendiente</td>
            <td colspan="2"  align="center">Corridas Generadas </td>
            <td width="9%" rowspan="2"  align="center">Cupo</td>
            <td width="10%" rowspan="2"  align="center">Nuevo Saldo </td>
          </tr>
          <tr class="tblTitulo">
            <td width="13%"  align="center">Anticipos y Cancelaciones OP</td>
            <td width="10%"  align="center">Proveedores</td>
            </tr>
<%
      for (int i = 0; i<vector.size(); i++){
          PosBancaria b = (PosBancaria) vector.elementAt(i);
		  String link = CONTROLLER + "?estado=PosBancaria&accion=Obtener&cmd=show";
		  
		  ttl_saldoi += b.getSaldo_inicial();
		  ttl_pend += (b.getSaldo_anterior() - b.getSaldo_inicial());
		  ttl_op += b.getAnticipos();
		  ttl_prov += b.getProveedores();
		  ttl_cupo += b.getCupo();
		  ttl_nsaldo += b.getNuevo_saldo();
%>
          <tr  class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand"
		  	<% if( request.getAttribute("NoTraslado") == null ) {%>onClick="if (<%= vector.size() %>> 1) { window.open('<%= link %>&agency_id=' + forma.agency_id[<%= i%>].value + '&banco=' + forma.bancos[<%= i%>].value + '&sucursal=' + forma.sucursal[<%= i%>].value.replace(/ /g, '_') + '&fecha=' + forma.fecha.value,'PosBancariaUp','status=yes,scrollbars=yes,width=780,height=450,resizable=yes'); } else { window.open('<%= link %>&agency_id=' + forma.agency_id.value + '&banco=' + forma.bancos.value + '&sucursal=' + forma.sucursal.value.replace(' ', '_') + forma.fecha.value,'PosBancariaUp','status=yes,scrollbars=yes,width=780,height=450,resizable=yes'); }"<% } %> >
            <td width="12%" class="bordereporte"><%= (b.getNom_agencia().length()==0)? "&nbsp;" : b.getNom_agencia() %></td>
            <td width="13%" class="bordereporte"><%= b.getBranch_code() %>
              <input name="bancos" type="hidden" id="bancos" value="<%= b.getBranch_code() %>">
              <input name="agency_id" type="hidden" id="agency_id2" value="<%= b.getAgency_id()%>">              </td>
            <td width="13%" class="bordereporte"><%= b.getBank_account_no()%>
              <input name="sucursal" type="hidden" id="sucursal" value="<%= b.getBank_account_no()%>"></td>
            <td width="11%" class="bordereporte"><div align="right"><span>
                <%= com.tsp.util.UtilFinanzas.customFormat(b.getSaldo_inicial()) %>
            </span></div></td>
            <td width="9%" class="bordereporte"><div align="right"><span >
            <%= com.tsp.util.UtilFinanzas.customFormat(b.getSaldo_anterior() - b.getSaldo_inicial()) %>
</span></div></td>
            <td width="13%" class="bordereporte"><div align="right"><span >
            <%= com.tsp.util.UtilFinanzas.customFormat(b.getAnticipos()) %>
</span></div></td>
            <td width="10%" class="bordereporte"><div align="right"><span>
            <%= com.tsp.util.UtilFinanzas.customFormat(b.getProveedores()) %>
            </span></div></td>
            <td width="9%" class="bordereporte"><div align="right"><span>
            <%= com.tsp.util.UtilFinanzas.customFormat(b.getCupo()) %>
</span></div></td>
            <td width="10%" class="bordereporte"><div align="right"><span>
            <%= com.tsp.util.UtilFinanzas.customFormat(b.getNuevo_saldo()) %>
</span></div></td>
          </tr>
<%  }%>
          <tr  class="fila">
            <td colspan="3"><div align="right">Totales</div></td>
            <td align="right"><%= com.tsp.util.UtilFinanzas.customFormat(ttl_saldoi)%></td>
            <td align="right"><%= com.tsp.util.UtilFinanzas.customFormat(ttl_pend)%></td>
            <td align="right"><%= com.tsp.util.UtilFinanzas.customFormat(ttl_op)%></td>
            <td align="right"><%= com.tsp.util.UtilFinanzas.customFormat(ttl_prov)%></td>
            <td align="right"><%= com.tsp.util.UtilFinanzas.customFormat(ttl_cupo)%></td>
            <td align="right"><%= com.tsp.util.UtilFinanzas.customFormat(ttl_nsaldo)%></td>
          </tr>
        </table>        
<%
    }
%></td>
    </tr>
  </table>
<br>
<div align="center"><img src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="if(validarForma()) window.location.href='<%= CONTROLLER%>?estado=PosBancaria&accion=Refresh&cmd=show&listar=OK&cargar=OK&agenciaSelec=' + forma.agencia.options(forma.agencia.selectedIndex).value + '&bancoSelec=' + forma.banco.options(forma.banco.selectedIndex).value + '&fecha=' + forma.fecha.value;" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">&nbsp;
      <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);"  style="cursor:hand ">
    </div>
</form>
<%if(vector!= null && vector.size()<1){%>
<table border="2" align="center">
  <tr>
    <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="282" align="center" class="mensajes">No se encuentra bancos asociados a la agencia indicada. </td>
          <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="78">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<p><br>
    <%
}
%>
<%if( request.getParameter("traslado")!=null ){%>
</p>
<table border="2" align="center">
  <tr>
    <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="282" align="center" class="mensajes">El proceso de traslado de saldos ha sido realizado exitosamente.</td>
          <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="78">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<br>
<%
}
%>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>
