<!--
- Autor : Ing. Tito Andr�s Maturana
- Date  : 18 enenro del 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el ingreso en el archivo de posici�n bancaria.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %><html>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page errorPage="../../error/ErrorPage.jsp"%>
<head>
<title>Posici&oacute;n Bancaria - Ingresar</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/posbancaria.js"></script>
<script>
function actualizarBancos(url) {
    forma.action = "<%= CONTROLLER%>?estado=PosBancaria&accion=Refresh&cmd=show";
    forma.agenciaSelec.value = forma.agencia.options(forma.agencia.selectedIndex).value;
    forma.submit();
}  		

</script>
<script>
maximizar();
</script>
</head>

<body onresize="redimensionar()" onload = "redimensionar();">
<%
	TreeMap agencias = model.agenciaService.getAgencia();
        TreeMap bancos = model.servicioBanco.getBanco();
	
	agencias.remove("");
	agencias.put(" Todas", ""); 
        bancos.put(" Todos", "");
		
		String hoy = Utility.getDate(4).replaceAll("/","-");
	
	String agenciasel = ( request.getAttribute("agencia")!=null ) ? (String) request.getAttribute("agencia") : "";	
        String bancosel = ( request.getAttribute("banco")!=null ) ? (String) request.getAttribute("banco") : "";
		String fechasel = ( request.getAttribute("fecha")!=null ) ? (String) request.getAttribute("fecha") : hoy;
	
        Vector vector = (Vector) request.getAttribute("bancos");
	String readonly = request.getAttribute("NoTraslado")==null ? "" : "readonly";
	
%>	
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Posici�n Bancaria - Ingresar"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%if(request.getParameter("msg")!=null){%>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes"><%=request.getParameter("msg").toString() %></td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>	
  <br>
  <table width="42%" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr align="left">
      <td><img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="window.location.href='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/cxpagar/posbancaria&pagina=PosBancariaInsert.jsp&marco=no&opcion=24'">   
    </tr>
  </table>
  <%
}
else{
%>
<form name="forma" method="post" action="<%= CONTROLLER%>?estado=PosBancaria&accion=Insert&cmd=show" id="forma">
  <table width="1000" border="2" align="center">
    <tr>
      <td><table width="100%">
        <tr>
          <td width="50%" class="subtitulo1"> Banco </td>
          <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif">
            <input name="agenciaSelec" type="hidden" id="agenciaSelec"></td>
        </tr>
      </table>
        <table width="100%"  border="0" class="tablaInferior">
          <tr class="fila">
            <td width="6%">Agencia</td>
            <td width="18%"><input:select name="agencia" default="<%= agenciasel %>" attributesText="class=textbox onChange=actualizarBancos();" options="<%= agencias %>"/></td>
            <td width="5%">Banco</td>
            <td width="11%"><input:select name="banco" default="<%= bancosel %>" attributesText="class=textbox;" options="<%= bancos %>"/></td>
            <td width="3%">Fecha</td>
            <td><input name="fecha" type='text' class="textbox" id="fecha" style='width:120' value='<%= fechasel%>' readonly>
              <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma.fecha);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16"
               border="0" alt="De click aqu&iacute; para escoger la fecha inicial"></a></td>
            <td width="41%"><img src="<%= BASEURL%>/images/botones/iconos/buscar.gif"  alt="Buscar posici&oacute;n bancara en la fecha seleccionada."    name="imgsalir" align="absmiddle" style="cursor:hand " onClick="if(validarForma()) window.location.href='<%= CONTROLLER%>?estado=PosBancaria&accion=Refresh&cmd=show&cargar=OK&agenciaSelec=' + forma.agencia.options(forma.agencia.selectedIndex).value + '&bancoSelec=' + forma.banco.options(forma.banco.selectedIndex).value + '&fecha=' + forma.fecha.value; "> </td>
          </tr>
        </table>
        
        <%
    if ( vector!= null && vector.size()>0) {
%>        
        <table width="100%">
          <tr>
            <td width="50%" class="subtitulo1"> Posici&oacute;n Bancaria </td>
            <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
          </tr>
        </table>
        <table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4">
          <tr class="tblTitulo">
            <td width="13%" rowspan="2" align="center">Agencia</td>
            <td width="13%" rowspan="2" align="center">Banco</td>
            <td width="14%" rowspan="2"  align="center">Sucursal</td>
            <td width="9%" rowspan="2"  align="center">Saldo Inicial </td>
            <td width="9%" rowspan="2"  align="center">Pendiente</td>
            <td colspan="2"  align="center">Corridas Generadas </td>
            <td width="8%" rowspan="2"  align="center">Cupo</td>
            <td width="9%" rowspan="2"  align="center">Nuevo Saldo </td>
          </tr>
          <tr class="tblTitulo">
            <td width="14%"  align="center">Anticipos y Cancelaciones OP </td>
            <td width="11%"  align="center">Proveedores</td>
            </tr>
<%
      for (int i = 0; i<vector.size(); i++){
          PosBancaria b = (PosBancaria) vector.elementAt(i);
%>
          <tr class="fila">
            <td width="13%"><%= (b.getNom_agencia().length()==0)? "&nbsp;" : b.getNom_agencia() %></td>
            <td width="13%"><%= b.getBranch_code() %>
              <input name="agency_id" type="hidden" id="agency_id" value="<%= b.getAgency_id()%>">
              <input name="bancos" type="hidden" id="bancos" value="<%= b.getBranch_code() %>"></td>
            <td width="14%"><%= b.getBank_account_no()%>
              <input name="sucursal" type="hidden" id="sucursal" value="<%= b.getBank_account_no()%>">
              <input name="saldo_ant" type="hidden" id="saldo_ant" value="<%= com.tsp.util.UtilFinanzas.customFormat(b.getSaldo_anterior()) %>">
              </td>
            <td width="9%"><div align="right"><span class="bordereporte">
                <input name="campo1" type="text" class="textbox" size="14" maxlength="9" onFocus="this.value = this.value.replace(/,/g,'');" onBlur="if(this.value=='') this.value=0; formatear(this);" onChange="if(this.value=='') this.value=0; formatear(this); actualizarPendiente('<%= vector.size() %>','<%= i%>'); actualizarNSaldo('<%= vector.size() %>','<%= i%>');"  onKeyPress="soloDigitos2(event,'decNo',this)" value="<%= com.tsp.util.UtilFinanzas.customFormat(b.getSaldo_inicial()) %>" style="text-align:right" <%=readonly%>>
            </span></div></td>
            <td width="9%"><div align="right"><span class="bordereporte">
            <input name="campo2" type="text" class="textbox" size="14" maxlength="9" value="<%= com.tsp.util.UtilFinanzas.customFormat(b.getSaldo_anterior() - b.getSaldo_inicial()) %>" style="text-align:right" readonly>
</span></div></td>
            <td width="14%" align="right">
                <input name="campo3" type="text" class="textbox" size="14" maxlength="9" value="<%= com.tsp.util.UtilFinanzas.customFormat(b.getAnticipos()) %>" style="text-align:right; width:100%"  readonly>            </td>
            <td width="11%"><div align="right"><span class="bordereporte">
            <input name="campo4" type="text" class="textbox" size="14" maxlength="9" value="<%= com.tsp.util.UtilFinanzas.customFormat(b.getProveedores()) %>" style="text-align:right"  readonly>
            </span></div></td>
            <td width="8%"><div align="right"><span class="bordereporte">
            <input name="campo5"type="text" class="textbox" size="14" maxlength="9" onFocus="this.value = this.value.replace(/,/g,'');" onBlur="if(this.value=='') this.value=0; formatear(this);"  onChange="if(this.value=='') this.value=0; formatear(this);" value="<%= com.tsp.util.UtilFinanzas.customFormat(b.getCupo()) %>"  onKeyPress="soloDigitos(event,'decNo')" style="text-align:right" <%=readonly%>>
</span></div></td>
            <td width="9%"><div align="right"><span class="bordereporte">
            <input name="nuevo_saldo" type="text" class="textbox" size="14" maxlength="9" value="<%= com.tsp.util.UtilFinanzas.customFormat(b.getNuevo_saldo()) %>" style="text-align:right" readonly>
</span></div></td>
          </tr>
<%  }%>
        </table>        
<%
    }
%></td>
    </tr>
  </table>
<%
    if ( vector!= null) {
%>      
  <div align="center"><br>
    <img src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="if( validarForma() ) forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">&nbsp; <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);"  style="cursor:hand "></div>
  
    <div align="left">
      <%
    } else {
%>
      <br>
      <div align="center"><img src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="if( validarForma() ) window.location.href='<%= CONTROLLER%>?estado=PosBancaria&accion=Refresh&cmd=show&cargar=OK&agenciaSelec=' + forma.agencia.options(forma.agencia.selectedIndex).value + '&bancoSelec=' + forma.banco.options(forma.banco.selectedIndex).value + '&fecha=' + forma.fecha.value; " onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand "> &nbsp; 
      <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);"  style="cursor:hand ">
      </div>
      <%}%>
      <%if(vector!= null && vector.size()<1){%>
      <table border="2" align="center">
        <tr>
          <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
              <tr>
                <td width="282" align="center" class="mensajes">No se encuentra bancos asociados a la agencia indicada. </td>
                <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                <td width="78">&nbsp;</td>
              </tr>
          </table></td>
        </tr>
      </table>
      <br>
      <%
}
%>
</form>
<%    
}
%>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>
