<!--  
     - Author(s)       :      Osvaldo P�rez Ferrer
     - Date            :      26/11/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Filtro para facturas por formato
 --%>

<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<%
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
        
        // Titulos de la tabla
        String[] titulos = model.formato_tablaService.getTitulos();
        // Nombres de los campos de la tabla
        String[] nombres_campos  = model.formato_tablaService.getCampos();
        // Datos de la lista
        Vector data      = model.formato_tablaService.getDatos();
        // Campos de la tabla, para verificar si son primary_key
        Vector campos  = model.formato_tablaService.getVector();
        String formato = request.getParameter("nombre_formato");
%>

<html>
<head>

    <title>Migraci�n Facturas</title>    
    <link href="<%= BASEURL %>/css/estilostsp.css"     rel="stylesheet" type="text/css"> 
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validate.js"></script> 
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validar.js"></script> 
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    
    <script>
        

        
        function sendForm( con ){
            var items = "";
            for( var i=0; i < formulario.length; i++ ){   
                var ele = formulario.elements[i];
                if( ele.type == 'checkbox' && ele.checked ){
                    items += ele.value+",";
                }
            }
            //alert( items );
            if( items != "" ){
                location.replace( con+'&evento=FACTURAR&items='+items );
            }else{
                alert("Debe seleccionar por lo menos un registro");
            }
        }                
    
    </script>
     
</head>
<body>


<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Migraci�n Facturas"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>


 
  <% Usuario  usuario               = (Usuario) session.getAttribute("Usuario");     
     String   msj                   =  request.getParameter("mensaje")!=null? request.getParameter("mensaje"):""; 
     String   url                   =  CONTROLLER +"?estado=Factura&accion=Migracion";      
     
     %>
    
  <form action="<%=url%>" method="post" name="formulario" id="formulario">
    
    <% if( msj.equals("") ){%>
            <% if( data != null && data.size() > 0  ){%>
                <% if( campos!=null && campos.size() > 0 ){%>                    
                     <table width="100%"  border="2" align="center">
                         <tr>
                            <tr>
                                                     <td colspan='2'>
                                                          <table width='100%'  class="barratitulo">
                                                                <tr class="fila">
                                                                        <td align="left" width='50%' class="subtitulo1" nowrap> &nbsp;Migraci�n Facturas - Formato <%=formato%></td>
                                                                        <td align="left" width='50%' ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                                                                </tr>
                                                          </table>
                                                     </td>
                                                  </tr>
                            <td colspan='2'>   


                                        <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">

                                                  <tr class="tblTitulo">
                                                    <td align="center" width='60'>Facturar
                                                    </td>
                                                    <%for( int i=0; i<titulos.length; i++){%>
                                                        <td nowrap align="center">
                                                        <%=titulos[i]%>  
                                                        </td>
                                                    <%}%>                                     
                                                  </tr>                                                                                                                                                                                                                                                                                                                        


                                                  <% for( int i=0; i<data.size(); i++ ){ 
                                                         Hashtable fila = (Hashtable)data.get(i);%>  
                                                        <tr class="<%=i%2==0?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' 
                                                        align="center">    
                                                           <td class='bordeReporte' align="center" style="cursor:hand"><input value='<%=fila.get("id")%>' type="checkbox">
                                                            </td> 
                                                           <%String action = ""; 
                                                             for( int j=0; j<campos.size(); j++ ){
                                                                 action = "";
                                                                 Hashtable h = (Hashtable)campos.get(j);
                                                                 String nom  = (String)h.get("campo_tabla");
                                                                 String pk = h.get("pk")!=null? (String)h.get("pk") : "";%>                                                         
                                                           <td  class='bordeReporte'>                                                                                                
                                                                     <%=fila.get( nom )%>
                                                           </td>
                                                           <%}%>
                                                   </tr>
                                                  <%}%>

                                                  </table>    

                                </td>
                             </tr>                          
                       </table>  

                       <p>
                                <img src='<%=BASEURL%>/images/botones/aceptar.gif'    style=" cursor:hand"  title='Aceptar....'       name='i_crear'       onclick="sendForm('<%=url%>');" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>               
                                <img src='<%=BASEURL%>/images/botones/regresar.gif'    style=" cursor:hand"  title='Regresar....'       name='i_crear'       onclick="history.back();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>               
                                <img src='<%=BASEURL%>/images/botones/salir.gif'      style=' cursor:hand'  title='Salir...'        name='i_salir'       onclick='parent.close();'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
                             </p>   
                   <%}%>          
               <%}else{%>        
                 <table border="2" align="center">
                  <tr>
                    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                      <tr>
                        <td width="400" align="center" class="mensajes">Su b�squeda no arroj� resultados</td>
                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                      </tr>
                    </table></td>
                  </tr>
                </table>

                <br>

                   <img src='<%=BASEURL%>/images/botones/regresar.gif'    style=" cursor:hand"  title='Regresar....'       name='i_crear'       onclick="history.back();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>               
                   <img src='<%=BASEURL%>/images/botones/salir.gif'      style=' cursor:hand'  title='Salir...'        name='i_salir'       onclick='parent.close();'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
            <%  } %>
        <%}else{%>        
                 <table border="2" align="center">
                  <tr>
                    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                      <tr>
                        <td width="400" align="center" class="mensajes"><%=msj%></td>
                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                      </tr>
                    </table></td>
                  </tr>
                </table>

                <br>

                   <img src='<%=BASEURL%>/images/botones/regresar.gif'    style=" cursor:hand"  title='Regresar....'       name='i_crear'       onclick="history.back(-2);" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>               
                   <img src='<%=BASEURL%>/images/botones/salir.gif'      style=' cursor:hand'  title='Salir...'        name='i_salir'       onclick='parent.close();'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
            <%  } %>
  <form>                            
  
  
  <!-- Mensaje -->
   


</div>
<%=datos[1]%>
</body>
</html>
