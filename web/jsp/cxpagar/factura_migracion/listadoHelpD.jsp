<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Descripci�n del programa de Migraci�n de Facturas</title>

<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<br>
<table width="594"  border="2" align="center">
  <tr>
    <td width="635" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">MIGRACION DE FACTURAS </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>          
          <td colspan='2' width="551"  class="ayudaHtmlTexto"> Se listan los registros pendientes por facturar, seleccione los registros que desea facturar y a continuaci�n click en ACEPTAR, cuando termine el proceso se generar� un archivo de texto en el cual se detalla el proceso: registros procesados y los NO procesados as� como el motivo por el cual no se proces�.</td>
        </tr>
        
        <tr>
          <td colspan='2' class="fila"> MAS INFORMACION </td>          
        </tr>
          
        <tr>
            <td colspan='2' class="ayudaHtmlTexto"> Para informaci�n mas detallada y t�cnica del proceso de migraci�n para cada formato, consulte los siguientes enlaces.</td>
        </tr>
        
	<tr>
          <td width="40%"  class="fila"> Formato SANCION </td>
          <td colspan="2" class="Simulacion_Hiper" style="cursor:hand" onclick="location.replace('<%=BASEURL%>/images/ayuda/cxpagar/migracion_facturas/desc_sancion.pdf');">&nbsp;Ver</td>
	</tr>	
	
	<tr>
          <td width="40%"  class="fila"> Formato COMBUSTIBLE </td>
          <td colspan="2" class="Simulacion_Hiper" style="cursor:hand" onclick="location.replace('<%=BASEURL%>/images/ayuda/cxpagar/migracion_facturas/desc_combustible.pdf');">&nbsp;Ver</td>
	</tr>	
	
      </table>
    </td>
  </tr>
</table>
<br>
<table width="416" align="center">
	<tr>
	<td align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	</td>
	</tr>
</table>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
