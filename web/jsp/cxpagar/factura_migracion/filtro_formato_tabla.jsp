<!--  
     - Author(s)       :      Osvaldo Pérez Ferrer
     - Date            :      26/11/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Filtro para facturas por formato
 --%>

<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<%
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
        
        Vector formatos = model.formato_tablaService.getFormatos();
        Vector campos   = model.formato_tablaService.getVector();
        String opcion   = request.getParameter("opcion")!=null? request.getParameter("opcion") : "" ;
        String formato  = request.getParameter("formato")!=null? request.getParameter("formato") : "" ;
        String hoy = Util.getFechaActual_String(4);
%>

<html>
<head>

    <title>Migración Facturas</title>    
    <link href="<%= BASEURL %>/css/estilostsp.css"     rel="stylesheet" type="text/css"> 
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validate.js"></script> 
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validar.js"></script> 
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    
    <script>
        

        
        function sendForm( f ){
        
            if( trim( f.formato.value ) == "" ){
                alert("Debe seleccionar un formato");
            }else{
                location.replace(f.action+"&evento=FILTERS&formato="+f.formato.value);
            }
            
        }
        
        function search( f ){
            f.action += "&evento=SEARCH&formato=<%=formato%>";
            f.submit();
        }
    
    </script>
     
</head>
<body>


<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Migración Facturas"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>


 
  <% Usuario  usuario               = (Usuario) session.getAttribute("Usuario");
     
     
     String   msj                   =  request.getParameter("msj"); 
     String   url                   =  CONTROLLER +"?estado=Factura&accion=Migracion";      
     
     %>
    
  <form action="<%=url%>" method="post" name="formulario" id="formulario">
    
    <% if( formatos != null && formatos.size() > 0 && opcion.equals("") ){%>
     <table width="400"  border="2" align="center">
         <tr>
            <td colspan='2'>             

                        <table width="100%"   align="center">
                                  <tr>
                                     <td colspan='2'>
                                          <table width='100%'  class="barratitulo">
                                                <tr class="fila">
                                                        <td align="left" width='50%' class="subtitulo1" nowrap> &nbsp;Formatos Migración Facturas</td>
                                                        <td align="left" width='50%' ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                                                </tr>
                                          </table>
                                     </td>
                                  </tr>
                                  
                                  
                                   <tr class="fila">
                                           <td width='30%' >&nbsp Formato</td>
                                           <td>
                                              <select name='formato' id='formato'>
                                                  <option value=''>- Seleccione -  
                                                  <% for( int i=0; i<formatos.size(); i++ ){ 
                                                         Hashtable h = (Hashtable)formatos.get(i);%>  
                                                      <option value='<%=h.get("formato")+"-_-"+h.get("nombre_formato")%>'><%=h.get("nombre_formato")%>
                                                  <%}%>
                                              </select>
                                           </td>
                                   </tr>
                                  
                                                                                                                                                                               
                        </table>

                </td>
             </tr>
       </table>     
       
       <p>       
           <img src='<%=BASEURL%>/images/botones/aceptar.gif'    style=" cursor:hand"  title='Aceptar....'       name='i_crear'       onclick="sendForm(formulario)" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>               
           <img src='<%=BASEURL%>/images/botones/salir.gif'      style=' cursor:hand'  title='Salir...'        name='i_salir'       onclick='parent.close();'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
       </p> 
       
       <%}else if( opcion.equals("filtros") ){%>
            <% if( campos!=null && campos.size() > 0 ){%>                    
                    <table width="400"  border="2" align="center">
                         <tr>
                            <td colspan='2'>             

                                        <table width="100%"   align="center">
                                                  <tr>
                                                     <td colspan='2'>
                                                          <table width='100%'  class="barratitulo">
                                                                <tr class="fila">
                                                                        <td align="left" width='50%' class="subtitulo1" nowrap> &nbsp;Filtros Migración Facturas</td>
                                                                        <td align="left" width='50%' ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                                                                </tr>
                                                          </table>
                                                     </td>
                                                  

                                                     </tr>
                                                                                                                 
                                                                  <% for( int i=0; i<campos.size(); i++ ){ 
                                                                         Hashtable h = (Hashtable)campos.get(i);
                                                                         String pk = h.get("pk")!=null? (String)h.get("pk") : "";
                                                                         if( pk.equals("S") ){%>                                                                              
                                                                             <% if( ((String)h.get("tipo")).equals("DATE") ){ %>
                                                                                <tr class="fila">                
                                                                                    <td class="fila">&nbsp <%=h.get("campo_jsp")%></td>
                                                                                    <td>&nbsp;<input type="textbox" name="<%=h.get("campo_tabla")%>" id="<%=h.get("campo_tabla")%>" size="10" readonly value="" >
                                                                                        <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(<%=h.get("campo_tabla")%>);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="Click aqui para escoger la fecha"></a>
                                                                                    </input></td>
                                                                                </tr>
                                                                             <%}else{%>
                                                                                <tr class="fila" >
                                                                                    <td width='30%' > &nbsp <%=h.get("campo_jsp")%></td>
                                                                                <td>&nbsp;<input type="textbox" id='<%=h.get("campo_tabla")%>' name='<%=h.get("campo_tabla")%>'> </td>
                                                                                <tr> 
                                                                            <%}%>    
                                                                        <%}%>    
                                                                  <%}%>
                                                   <tr class="fila">                
                                                        <td class="fila">&nbsp; Creado desde : </td>
                                                        <td>&nbsp;<input type="textbox" name="inicial" id="inicial" size="10" readonly value="<%=hoy%>" >
                                                            <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(inicial);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="Click aqui para escoger la fecha"></a>
                                                        </input></td>
                                                    </tr>


                                                    <tr class="fila">                
                                                        <td class="fila">&nbsp; Hasta : </td>
                                                        <td>&nbsp;<input type="textbox" name="final" id="final" size="10" readonly value="<%=hoy%>" >
                                                            <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(final);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="Click aqui para escoger la fecha"></a>
                                                        </input></td>
                                                    </tr>                                                                                                          
                                        </table>

                                </td>
                             </tr>
                       </table>   
           <%}%>            

       <p>       
           <img src='<%=BASEURL%>/images/botones/aceptar.gif'    style=" cursor:hand"  title='Aceptar....'       name='i_crear'       onclick="search(formulario)" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>               
           <img src='<%=BASEURL%>/images/botones/regresar.gif'    style=" cursor:hand"  title='Regresar....'       name='i_crear'       onclick="location.replace('<%=url%>&evento=LOAD')" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>               
           <img src='<%=BASEURL%>/images/botones/salir.gif'      style=' cursor:hand'  title='Salir...'        name='i_salir'       onclick='parent.close();'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
       </p
       <%}else{%>
          <table border="2" align="center">
              <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                  <tr>
                    <td width="550" align="center" class="mensajes">No existen formatos de Migración de Facturas</td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="58">&nbsp;</td>
                  </tr>
                </table></td>
              </tr>
            </table> 
 
            <p>                      
               <img src='<%=BASEURL%>/images/botones/salir.gif'      style=' cursor:hand'  title='Salir...'        name='i_salir'       onclick='parent.close();'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
           </p
       <%}%>
       
  <form>
                
  
       

  
  <font id='msj' class='informacion'></font>
  
  
  <!-- Mensaje -->
   <% if(msj!=null && !msj.equals("")){%>        
         <table border="2" align="center">
          <tr>
            <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
              <tr>
                <td width="550" align="center" class="mensajes"><%=msj%></td>
                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                <td width="58">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
        </table>
    <%  } %>


</div>
<%=datos[1]%>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>        
</body>
</html>
