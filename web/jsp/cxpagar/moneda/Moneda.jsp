<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>MONEDA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body <%if(request.getParameter("reload")!=null){%>onLoad="parent.opener.location.reload();"<%}%>>
<% 
   Moneda moneda;
   String mensaje =(request.getParameter("mensaje")!=null)?request.getParameter("mensaje"):"";
   String men="",cod = "",des = "",ini="";
   String Titulo="Ingresar Moneda";//AGREGAR Icoterm
   String Subtitulo ="Informacion";
   String action = CONTROLLER + "?estado=Moneda&accion=Ingresar";
   String a = (request.getParameter("sw")!=null)?request.getParameter("sw"):"-1";
   int swcon=-1;
   String tipo="text";
   
   if ( a != null ){
       swcon = Integer.parseInt( a );
   }
   

   if ( swcon == 0 ){ //swcon = 0 capturo la informacion de la pag anterior
       cod = request.getParameter("c_codigo");
       des = request.getParameter("c_des");
	   ini = request.getParameter("c_ini");
   } 
   else if ( swcon == 1) { //swcon = 1 capturo el objeto con la información
        tipo = "hidden";
        moneda = model.monedaSvc.getMoneda();
        cod = moneda.getCodMoneda();
        des = moneda.getNomMoneda();
		ini = moneda.getInicial();
        Titulo="Modificar Moneda";//MODIFICAR ICOTERM 
        action = CONTROLLER + "?estado=Moneda&accion=Modificar";
    }
%>
<form name="forma" method="post" action="<%=action%>" >
  <table width="370" border="2" align="center">
    <tr>
      <td><table width="100%"  border="0">
        <tr>
          <td width="48%" class="subtitulo1"><%=Subtitulo%></td>
          <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
      <table width="100%" class="tablaInferior">
        <tr class="fila">
          <td width="115" >Codigo</td>
          <td width="226" >
            <input name="c_codigo" type="<%=tipo%>" class="textbox" maxlength="3" value="<%=cod%>" ><%if(swcon==1){%><%=cod%><%}else{%>
			<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
			<%}%></td>
        </tr>
		<tr class="fila">
          <td width="115" >Inicial</td>
          <td width="226" >
            <input name="c_ini" type="<%=tipo%>" class="textbox" id="c_ini" value="<%=ini%>" maxlength="2" ><%if(swcon==1){%> <%=ini%> <%}else{%>
			<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
			<%}%></td>
        </tr>
        <tr class="fila">
          <td valign="middle" >Descripcion</td>
          <td valign="middle">
            <input name="c_des" type="text" class="textbox" maxlength="35" value="<%=des%>"></td>
        </tr>
      </table></td>
    </tr>
  </table>
<br>
 <% if ((swcon==0) || (swcon==-1) ){%>
	       <div align="center">
			   <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="return TCamposLlenos();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
			   <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;
               <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
           </div>
		  <%} else {%>
		  	<div align="center">
			  <img src="<%=BASEURL%>/images/botones/modificar.gif"  name="imgmodificar" onClick="return TCamposLlenos();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
              <img src="<%=BASEURL%>/images/botones/anular.gif"  name="imganular" onClick="window.location='<%=CONTROLLER%>?estado=Moneda&accion=Anular&c_codigo=<%=cod%>&c_des=<%=des%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
              <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
          </div>
		  <%}%>
		  <br>
  <% if( !mensaje.equalsIgnoreCase("")){%>
<table border="2" align="center">
  <tr>
    <td><table width="100%" class="tablaInferior">
      <tr>
        <td width="229" align="center" class="mensajes"><%=mensaje%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>    <% }%>
</form>
</body>
</html>
