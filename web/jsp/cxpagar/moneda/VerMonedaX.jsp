<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<html>
<head>
<title>Listado de Moneda</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
  <% 
   
   String style = "simple";
   String position =  "bottom";
   String index =  "center";
   int maxPageItems = 10;
   int maxIndexPages = 10;
   String nombre = (String) session.getAttribute("nom");
   Vector VecMon = model.monedaSvc.buscarXMonedas(nombre+"%");
   Moneda moneda;  

   if ( VecMon.size() > 0 ) { 
   %>
  <table width="380" border="2" align="center">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td width="50%" class="subtitulo1">&nbsp;Datos Moneda </td>
          <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"> </td>
        </tr>
      </table>
        <table width="99%"  border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
        <tr class="tblTitulo" align="center">
          <td width="103" align="center">Codigo<input name="nom" type="hidden" id="nom" value="<%=nombre%>">
          </td>
          <td nowrap width="342">Descripcion</td>
        </tr>
        <pg:pager
    items="<%=VecMon.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
        <%for (int i = offset.intValue(), l = Math.min(i + maxPageItems, VecMon.size()); i < l; i++){
          moneda = (Moneda) VecMon.elementAt(i);%>
        <pg:item>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand"  
      onClick="window.open('<%=CONTROLLER%>?estado=Moneda&accion=Buscar&pagina=Moneda.jsp&carpeta=jsp/cxpagar/moneda&codigo=<%=moneda.getCodMoneda()%>','','status=yes,scrollbars=no,width=620,height=330,resizable=yes');">
          <td height="22" class="bordereporte"><%=moneda.getCodMoneda()%></td>
          <td nowrap class="bordereporte"><%=moneda.getNomMoneda()%></td>
        </tr>
        </pg:item>
        <%}%>
        <tr class="fila">
          <td colspan="2" align="center" class="bordereporte"> <pg:index>
            <jsp:include page="../../../WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
    </pg:index> </td>
        </tr>
        </pg:pager>
      </table></td>
    </tr>
  </table>
  <%}
 else { %>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="38" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>

<%}%>
<br>
<table width="425" border="0" align="center">
   <tr>
     <td width="425"><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgaceptar" onclick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/cxpagar/moneda&pagina=BuscarMoneda.jsp&titulo=Buscar Moneda'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
   </tr>
</table>

</body>
</html>
