<%@ page session="true"%>
<%@ page errorPage="/error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>

<title>Consulta de Serie de Documentos Anulados</title>
<script language="javascript" src="../../../js/validar.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script>
function showMsg (message){
	var obj = document.getElementById('msg');
	if (obj){
		/*var rightedge = document.body.clientWidth - event.clientX;
		var bottomedge = document.body.clientHeight - event.clientY;
		
		if (rightedge < obj.offsetWidth)
			obj.style.left = document.body.scrollLeft + event.clientX - obj.offsetWidth;
		else
			obj.style.left = document.body.scrollLeft + event.clientX;

		if (bottomedge < obj.offsetHeight)
			obj.style.top = document.body.scrollTop + event.clientY - obj.offsetHeight;
		else
			obj.style.top = document.body.scrollTop + event.clientY;*/
		obj.style.top =  document.body.clientHeight/4;		
		obj.style.left = document.body.clientWidth/4;
		
		obj.innerHTML  = message;
		obj.style.visibility = 'visible';
		//obj.style.top  = event.clientY;
		//obj.style.left = event.clientX;
	}
}
  
function hiddenMsg (){
  var obj = document.getElementById('msg');
  if (obj) obj.style.visibility = 'hidden';
}

function showInfoPago (fra, banco, suc, cta_no, cta_tipo, cta_nom, cta_nit){
	var obj = document.getElementById('msg');
	obj.style.height = '25px';

	var tabla = "<br><table width='97%' align='center' border='2' cellpadding='2' cellspacing='1' bgcolor='#F7F5F4'>";

	var bnc = "<table width='100%' class='tablaInferior'>";
	bnc += "<tr class='fila'><td width='30%'>Banco</td><td width='70%' nowrap>" + banco + "</td></tr>";
	bnc += "<tr class='fila'><td width='30%'>Sucursal</td><td width='70%' nowrap>" + suc + "</td></tr>";
	bnc += "<tr class='fila'><td width='30%'>Cuenta Nro.</td><td width='70%' nowrap>" + cta_no + "</td></tr>";
	bnc += "<tr class='fila'><td width='30%'>Tipo Cta.</td><td width='70%' nowrap>" + cta_tipo + "</td></tr>";
	bnc += "<tr class='fila'><td width='30%'>Nombre</td><td width='70%' nowrap>" + cta_nom + "</td></tr>";
	bnc += "<tr class='fila'><td width='30%'>C�dula/Nit</td><td width='70%' nowrap>" + cta_nit + "</td></tr></table>";
	
	tabla += "<tr class='subtitulo1'><th colspan='1'>Detalles Pago por Transferencia Documento: " + fra + "</th></tr>";
	tabla += "<tr><td><div class='scroll' style='overflow:auto ; WIDTH: 100%; HEIGHT:200px;' >";	
	tabla += bnc;	  
	tabla += "</div></td></tr>";	
	tabla += "</table><br>&nbsp;";
	tabla += "<center><a class='Simulacion_Hiper' onClick=\"msg.style.visibility = 'hidden';\" href='JavaScript:void(0);'>Cerrar esta Ventana</a></center><br>&nbsp;";  
	showMsg(tabla);
}
</script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div class="fila" style="position:absolute; visibility:hidden; width:350px; height:50px; z-index:1; border: 2 outset #003399; " id="msg">
</div>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Consulta de Serie de Documentos Anulados"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<%    
    Vector vector = model.docAnuladoSvc.getDocumentos();
%>
<table border="2" align="center">
  <tr>
    <td><table width="100%">
      <tr>
        <td width="50%" class="subtitulo1">Lista  de Documentos </td>
        <td width="50%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif"><%=datos[0]%></td>
      </tr>
    </table>      
      <table border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
      <tr class="tblTitulo"  >
        <td nowrap><div align="center">Distrito</div></td>
        <td nowrap><div align="center">Tipo de Documento </div></td>
        <td nowrap><div align="center">No. Documento </div></td>
        <td nowrap><div align="center">Referencia 1 </div></td>
        <td nowrap><div align="center">Referencia 2 </div></td>
        <td nowrap><div align="center">Referencia 3 </div></td>
        <td nowrap><div align="center">Causa de Anulaci&oacute;n </div></td>
        <td width="400"><div align="center" style="width:400 ">Observaci&oacute;n</div></td>
        <td nowrap><div align="center">Fecha de Creaci&oacute;n</div></td>
        <td nowrap><div align="center">Usuario Cre&oacute;</div></td>
        <td nowrap><div align="center">Ultima Modificaci&oacute;n</div></td>
        <td nowrap><div align="center">Usuario Modific&oacute; </div></td>
        <td nowrap><div align="center">Base</div></td>
        </tr>
<%
      Iterator it = vector.iterator();
	  int i = 0;
      while( it.hasNext() ){
          SerieDocAnulado doc = (SerieDocAnulado) it.next();
%>
      <tr valign="top" class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" style="cursor:hand"
	  	onClick="window.open('<%= CONTROLLER %>?estado=SerieDocAnulado&accion=Manager&opc=3&dstrct=<%= doc.getDstrct() %>&tipo_doc=<%= doc.getTipo_documento() %>&nodoc=<%= doc.getNo_documento() %>&ref1=<%= doc.getReferencia_1() %>&ref2=<%= doc.getReferencia_2() %>&ref3=<%= doc.getReferencia_3() %>','DETALL','status=yes,scrollbars=no,width=780,height=650,resizable=yes');" onMouseOver='cambiarColorMouse(this)'>
        <td nowrap class="bordereporte"><div align="left"><%= doc.getDstrct() %></div></td>
        <td nowrap class="bordereporte"><div align="left"><%= doc.getTipo_documento_desc() %></div></td>
        <td nowrap class="bordereporte"><div align="left"><%= doc.getNo_documento() %></div></td>
        <td nowrap class="bordereporte"><div align="left"><%= doc.getReferencia_1() %></div></td>
        <td nowrap class="bordereporte"><div align="left"><%= doc.getReferencia_2() %></div></td>
        <td nowrap class="bordereporte"><div align="left"><%= doc.getReferencia_3() %></div></td>
        <td nowrap class="bordereporte"><div align="left"><%= doc.getCausa_anulacion_desc()%></div></td>
        <td class="bordereporte" style="text-align:justify;font-size:11px;" width="500"><%= doc.getObservacion()%></td>
        <td nowrap class="bordereporte"><div align="left"><%= doc.getCreation_date()%></div></td>
        <td nowrap class="bordereporte"><div align="left"><%= doc.getCreation_user()%></div></td>
        <td nowrap class="bordereporte"><div align="left"><%= doc.getLast_update()%></div></td>
        <td nowrap class="bordereporte"><div align="left"><%= doc.getUser_update()%></div></td>
        <td nowrap class="bordereporte"><div align="left"><%= doc.getBase()%></div></td>
        </tr>
<%
		i++;
	}
%>
    </table></td>
  </tr>
</table>
<br>  
  
<% if( request.getParameter("msg") != null ){%>
<p>
<table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
          <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="58">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<p></p>
<p>
    <%} %>
</p>
<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr align="left">
    <td><img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" style="cursor:hand " onClick="window.location.href = '<%= CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/cxpagar/seriedoc_anul&pagina=search.jsp&marco=no&opcion=33&item=20';" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp; <img src="<%=BASEURL%>/images/botones/exportarExcel.gif" name="c_regresar" id="c_regresar" style="cursor:hand " onClick="window.open('<%=CONTROLLER%>?estado=SerieDocAnulado&accion=Manager&opc=6','XLS','status=yes,scrollbars=no,width=300,height=100,resizable=yes');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">  </tr>
</table>
</div>
<%=datos[1]%>
</body>
</html>


<!--
Entregado  a tito 15 Febrero 2007
-->
