<!--
- Autor : Ing. Andr�s Maturana De La Cruz
- Date  : 27.06.2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la generaci�n del reporte de facturas de clientes.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
</script>
<title>Serie Documentos Anulados</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validate.js"></script>
<script>
function validarForma(){
	for (i = 0; i < forma.elements.length; i++){
        if (trim(forma.elements[i].value) == "" 
			&& forma.elements[i].name != "ref1" 
			&& forma.elements[i].name != "ref2" 
			&& forma.elements[i].name != "ref3" ){
				alert("Este campo no puede ir en blanco");
				forma.elements[i].focus();
            	return false;
        }
    }
	
	return true;
}	
</script>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:110px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Serie Documentos Anulados"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 109px; overflow: scroll;">
<%
	TreeMap tdocs = (TreeMap) request.getAttribute("tdoc");
	TreeMap tcanul = (TreeMap) request.getAttribute("tcanul");
	Calendar FechaHoy = Calendar.getInstance();
	Date d = FechaHoy.getTime();
	SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");
%>
<form name="forma" action='<%=CONTROLLER%>?estado=SerieDocAnulado&accion=Manager&opc=1' id="forma" method="post">
  <table width="500"  border="2" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td colspan="2" ><table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
              <tr>
                <td width="56%" class="subtitulo1">Serie Documentos Anulados</td>
                <td width="44%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
              </tr>
          </table></td>
        </tr>
        <tr class="fila">
          <td width="27%">Tipo de Documento</td>
          <td width="73%" nowrap><input:select name="tipo_doc" attributesText="class=textbox" options="<%=tdocs %>"/></td>
        </tr>
        <tr class="fila">
          <td>No. Documento </td>
          <td nowrap><input name="nodoc" type="text" class="textbox" id="nodoc" value="<%= request.getAttribute("nodoc")!=null ? request.getAttribute("nodoc") : "" %>" size="40" maxlength="30"></td>
        </tr>
        <tr class="fila">
          <td>Referencia 1 </td>
          <td nowrap><input name="ref1" type="text" class="textbox" id="ref1"  value="<%= request.getAttribute("ref1")!=null ? request.getAttribute("ref1") : "" %>" size="60" maxlength="50"></td>
        </tr>
        <tr class="fila">
          <td>Referencia 2 </td>
          <td nowrap><input name="ref2" type="text" class="textbox" id="ref2"  value="<%= request.getAttribute("ref2")!=null ? request.getAttribute("ref2") : "" %>" size="60" maxlength="50"></td>
        </tr>
        <tr class="fila">
          <td>Referencia 3 </td>
          <td nowrap><input name="ref3" type="text" class="textbox" id="ref3"  value="<%= request.getAttribute("ref3")!=null ? request.getAttribute("ref3") : "" %>" size="60" maxlength="50"></td>
        </tr>
        <tr class="fila">
          <td valign="top">Causa Anulaci&oacute;n </td>
          <td nowrap><input:select name="causa_anul" attributesText="class=textbox" options="<%=tcanul %>"/></td>
        </tr>
        <tr class="fila">
          <td valign="top">Observaciones</td>
          <td nowrap><textarea name="observacion" rows="6" class="textbox" id="observacion" style="width:100%"><%= request.getAttribute("obs")!=null ? request.getAttribute("obs") : "" %></textarea></td>
        </tr>
      </table></td>
    </tr>
  </table>
  <br>
  
  <br>
  <center>
    <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="c_aceptar" onClick="if (validarForma()) forma.submit(); " onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
    <img src="<%=BASEURL%>/images/botones/cancelar.gif" name="c_cancelar" onClick="forma.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
    <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
  </center>
</form>
<% if( request.getParameter("msg") != null ){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="363" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%} %>
</div>
<%=datos[1]%>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</body>
</html>
