
<!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      09/02/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Vista de tabla de Amorización
 --%>

<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>



<html>
<head>

     <title>Vista de Amortizacion</title>
     <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
     <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 	 
     <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
     <script src="<%= BASEURL %>/js/validacionesPrestamos.js"></script>
     <script src="<%= BASEURL %>/js/validar.js"></script>

</head>
<body onresize="redimensionar()" onload = "redimensionar();">
 <center>

 
 
 
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Detalle Prestamos"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
 


<% 
   Prestamo prestamo         = (Prestamo) request.getAttribute("prestamo"); 
   List     listAmortizacion = prestamo.getAmortizacion();
   String   index            = (String) request.getAttribute("indexSel"); 
   String   Mensaje          = (request.getParameter("Mensaje")==null)?"":request.getParameter("Mensaje");
   boolean  normal           = (request.getParameter("noedit")==null)?true:false;
   int cuotasNP = 0;
%>

 <form action='<%= CONTROLLER %>?estado=Consulta&accion=Prestamos' method='post' name='formulario'>
 <table width="816"  border="2" align="center">
     <tr>
      <td>
          <table  width='100%' class='tablaInferior' cellpadding='0' cellspacing='0'>
          <tr>
            <td>
                  <table width='100%'  class="barratitulo" cellpadding='0' cellspacing='0'>
                     <tr class="fila">
                            <td align="left" width='50%' class="subtitulo1" nowrap>&nbsp;Datos del Prestamo</td>
                            <td align="left" width='50%' ><img src="<%=BASEURL%>/images/titulo.gif"></td>
                     </tr>
                  </table>
            </td>
          </tr>
          <tr>
             <td>
             <table width='100%'>
                     <tr class='fila'>
                         <td width='25%' >&nbsp;BENEFICIARIO              </td>
                         <td style="font size:12" colspan='5'>&nbsp;[<%= prestamo.getBeneficiario() %>]&nbsp;<%= prestamo.getBeneficiarioName() %> </td>
                     </tr>
                     <tr class='fila'>
                           <td width='25%'>&nbsp;TIPO DE PRESTAMO   </td><td width='25%' style="font size:12">&nbsp;<%= prestamo.getTipoPrestamoName() %></td>
                           <td width='16%'>&nbsp;CUOTAS             </td><td width='9%'  align='center' style="font size:12"><input type='text' value='<%= prestamo.getCuotas() %>' style='width:100%; text-align:center;' name='cuotas' onkeypress="soloDigitos(event,'decNO');" onfocus='this.select()'>   </td>
                           <td width='16%'>&nbsp;TASA               </td><td width='9%'  align='center' style="font size:12"> <%= prestamo.getTasa()   %>% </td>
                     </tr>
                     <tr class='fila'>
                           <td >&nbsp;FECHA ENTREGA DINERO  </td>
                           <td align='center'  style="font size:12">&nbsp;<%= prestamo.getFechaEntregaDinero() %></td>
                           <td >&nbsp;PERIODO               </td>
                           <td align='center'  style="font size:12"><%= prestamo.getFrecuencias() %>  </td>
                           <td colspan='2'>&nbsp;</td>
                     </tr>
                     <tr class='fila'>
                           <td width='13%'>&nbsp;VALOR PRESTAMO    </td>
                           <td align='center' style="font size:12">$&nbsp;<%= UtilFinanzas.customFormat2( prestamo.getMonto() ) %></td>
                           <td width='13%'>&nbsp;VALOR INTERESES   </td>
                           <td colspan='3' align='center' style="font size:12"> $&nbsp;<%= UtilFinanzas.customFormat2( prestamo.getIntereses()) %></td>
                     </tr>
                     <% if (prestamo.getTipoPrestamo().equals("IA")) {%>
                     <tr class='fila'>
                           <td width='13%'>&nbsp;VALOR A ENTREGAR  </td>
                           <td align='center' style="font size:12"> $&nbsp;<%= UtilFinanzas.customFormat2( prestamo.getMonto() -prestamo.getIntereses()) %></td>
                           <td colspan='4'>&nbsp;</td>
                     </tr>
                     <% } %>

             </table>
          </td>   
          </tr>
          <tr>
            <td>
                  <table width='100%'  class="barratitulo" cellpadding='0' cellspacing='0'>
                     <tr class="fila">
                            <td align="left" width='50%' class="subtitulo1" nowrap>&nbsp;Tabla de Amortizaciones</td>
                            <td align="left" width='50%' ><img src="<%=BASEURL%>/images/titulo.gif"></td>
                     </tr>
                  </table>
            </td>
          </tr>  
          <tr>
             <td>
             <!-- amotizaciones -->
             
             
                <div style='width:832; overflow:scroll;'>
                <table width='2300' border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
                  <tr class="tblTitulo">
                    <th colspan='8' >DATOS GENERALES DE LA AMORTIZACION</th>
                    <th colspan='6' >BENEFICIARIO</th>
                    <th colspan='6' >TERCERO</th>
                  </tr>
                  <tr class="tblTitulo">
                      <TH  width='35'  >No              </TH>
                      <TH  width='120' >FECHA            </TH>
                      <TH  width='140' >MONTO           </TH>
                      <TH  width='140' >CAPITAL         </TH>
                      <TH  width='140' >INTERES         </TH>
                      <TH  width='140' >TOTAL A PAGAR   </TH>
                      <TH  width='140' >SALDO           </TH>
                      <TH  width='120' >TRANSFERENCIA   </TH>

                      <TH  width='120' >ESTADO MIMS      </TH>
                      <TH  width='120' >BANCO            </TH>
                      <TH  width='120' >SUCURSAL         </TH>
                      <TH  width='120' >CHEQUE           </TH>
                      <TH  width='120' >FECHA DESCUENTO  </TH>
                      <TH  width='120' >VALOR DESCONTADO </TH>

                      <TH  width='120' >ESTADO MIMS     </TH>
                      <TH  width='120' >BANCO           </TH>
                      <TH  width='120' >SUCURSAL        </TH>
                      <TH  width='120' >CHEQUE          </TH>
                      <TH  width='120' >FECHA PAGO      </TH>
                      <TH  width='120' >VALOR PAGADO    </TH>

                 </tr>

                    <% for(int i=0; i<listAmortizacion.size();i++){
                           Amortizacion amort = ( Amortizacion )listAmortizacion.get(i);
                           String estilo  = (i%2==0?"filagris":"filaazul");
                           boolean editar = false;
                           //if (!amort.getFechaMigracion().equals("0099-01-01 00:00:00")) {
                               if (amort.getEstadoDescuento().equals("50") && amort.getEstadoPagoTercero().equals("50"))
                                  estilo = "filaverde";
                               else if (!amort.getEstadoDescuento().equals("") || !amort.getEstadoPagoTercero().equals(""))
                                  estilo = "filaamarilla";
                               else if (!amort.getFechaMigracion().equals("0099-01-01 00:00:00"))
                                  estilo = "filaroja";
                               else{
                                   cuotasNP++;
                                   editar = true;
                               }
                           //}
                           String fecPago = (!( (prestamo.getTipoPrestamo().equals("IA") || prestamo.getTipoPrestamo().equals("CV")) && normal && editar )?
                                             amort.fmt.format( amort.getFecha())+ "<input type='hidden' name='fechas' value='" + amort.fmt.format( amort.getFecha() ) + "'> " :
                                             "<input type='text' id='fec"+ i +"' name='fechas' readonly style='width:75%;text-align:center; font-size:11px; height:18;' value='" + amort.fmt.format( amort.getFecha() ) + "'> " +
                                             "<a href='javascript:void(0)' onclick='if(self.gfPop)gfPop.fPopCalendar(document.formulario.fec"+ i +");return false;' HIDEFOCUS><img src='" + BASEURL +"/js/Calendario/cal.gif' width='16' height='16' border='0' alt='De click aqui para escoger la fecha'></a>");
                    %>
                          <tr class='<%= estilo %>'   >
                                <td nowrap class='bordereporte'  align='center'><%= amort.getItem() %>                       </td>
                                <td nowrap class='bordereporte'  align='center' valign='middle'><%= fecPago %></td>
                                <td nowrap class='bordereporte'  align='right' ><%= UtilFinanzas.customFormat2(amort.getMonto()      )%>     </td>
                                <td nowrap class='bordereporte'  align='right' ><%= UtilFinanzas.customFormat2(amort.getCapital()    )%>     </td>
                                <td nowrap class='bordereporte'  align='right' ><%= UtilFinanzas.customFormat2(amort.getInteres()    )%>     </td>
                                <td nowrap class='bordereporte'  align='right' ><%= UtilFinanzas.customFormat2(amort.getTotalAPagar())%>     </td>
                                <td nowrap class='bordereporte'  align='right' ><%= UtilFinanzas.customFormat2(amort.getSaldo()      )%>     </td>
                                <td nowrap class='bordereporte'  align='center'><%= (!amort.getFechaMigracion().equals("0099-01-01 00:00:00")?amort.getFechaMigracion():"&nbsp;")  %></td>

                                <td nowrap class='bordereporte'  align='center'><%= amort.getDescripcionEstado(0) %></td>
                                <td nowrap class='bordereporte'                ><%= (!amort.getBancoDescuento().equals("")   ?amort.getBancoDescuento()   :"&nbsp;")  %></td>
                                <td nowrap class='bordereporte'                ><%= (!amort.getSucursalDescuento().equals("")?amort.getSucursalDescuento():"&nbsp;")  %></td>
                                <td nowrap class='bordereporte'                ><%= (!amort.getChequeDescuento().equals("")  ?amort.getChequeDescuento()  :"&nbsp;")  %></td>
                                <td nowrap class='bordereporte'  align='center'><%= (!amort.getFechaDescuento().equals("0099-01-01")?amort.getFechaDescuento():"&nbsp;")   %></td>
                                <td nowrap class='bordereporte'  align='right' ><%= UtilFinanzas.customFormat2(amort.getValorDescuento()) %></td>

                                <td nowrap class='bordereporte'  align='center'><%= amort.getDescripcionEstado(1) %></td>
                                <td nowrap class='bordereporte'                ><%= (!amort.getBancoPagoTercero().equals("")   ?amort.getBancoPagoTercero()   :"&nbsp;")  %></td>
                                <td nowrap class='bordereporte'                ><%= (!amort.getSucursalPagoTercero().equals("")?amort.getSucursalPagoTercero():"&nbsp;")  %></td>
                                <td nowrap class='bordereporte'                ><%= (!amort.getChequePagoTercero().equals("")  ?amort.getChequePagoTercero():"&nbsp;")  %></td>
                                <td nowrap class='bordereporte'  align='center'><%= (!amort.getFechaPagoTercero().equals("0099-01-01")?amort.getFechaPagoTercero():"&nbsp;")   %></td>
                                <td nowrap class='bordereporte'  align='right' ><%= UtilFinanzas.customFormat2(amort.getValorPagoTercero()) %></td>

                         </tr>
                  <%}%>

               </table>
               </div>
             
             <!-- fin amortizaciones -->
             </td>
          </tr>
          </table>
       </td>
     </tr>
</table>




 
  <p>
  <% if (normal && prestamo!=null && (prestamo.getTipoPrestamo().equals("IA") || prestamo.getTipoPrestamo().equals("CV")) ) {%>
     <img src='<%=BASEURL%>/images/botones/modificar<%= ( cuotasNP == 0?"Disable":"") %>.gif'   style='cursor:hand'    title='Modificar fechas...'  <% if (cuotasNP !=0 ) { %>   name='i_aceptar'     onclick="validarFormularioAmortizacion(formulario);"  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);' <% } %>>
     <img src='<%=BASEURL%>/images/botones/restablecer<%= (prestamo.getAprobado().equals("S")?"Disable":"") %>.gif' style='cursor:hand'    title='Restablecer fechas...'  name='i_restablecer' <% if (!prestamo.getAprobado().equals("S")) { %> onclick="formulario.Opcion.value='RestablecerAmortizacion'; formulario.submit();"  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);' <% } %>>
  <% } %>   
  
  <% if (normal) { %>
     <img src='<%=BASEURL%>/images/botones/exportarExcel.gif' style='cursor:hand'    title='Exportar Excel...'      name='i_exportar'    onclick="descargar('001');"     onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
  <% } %>   
     <img src='<%=BASEURL%>/images/botones/salir.gif'         style='cursor:hand'    title='Salir...'               name='i_salir'       onclick='parent.close();'       onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
  </p>
  <br>
  <input type='hidden' name='fechaED' value='<%= prestamo.getFechaEntregaDinero() %>'>
  <input type='hidden' name='Opcion'  value='ActualizarAmortizacion'>
  <input type='hidden' name='Index'   value='<%= index %>'>
  <input type='hidden' name='coutasTM' value='<%= prestamo.getCuotas() %>'>
  <input type='hidden' name='cuotasNP' value='<%= cuotasNP %>'>
  <input type='hidden' name='cuotasPR' value='<%= prestamo.getCuotas() - cuotasNP %>'>
  <input type='hidden' name='TipoPRES' value='<%= prestamo.getTipoPrestamo() %>'>
  </form>
  
 <% if( ! Mensaje.equals("") ){%>
          
          <table border="2" align="center">
              <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                  <tr>
                    <td width="500" align="center" class="mensajes"><%= Mensaje %></td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="58">&nbsp; </td>
                  </tr>
                </table></td>
              </tr>
            </table>
   <%}%>  
  
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>
<script>
   if ( formulario.cuotasNP.value=='0' || (formulario.TipoPRES.value != 'IA' && formulario.TipoPRES.value != 'CV')) formulario.cuotas.readOnly = true;
</script>
