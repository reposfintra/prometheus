<!--
     - Author(s)       :      MARIO FONTALVO
     - Date            :      12/02/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   
     - @(#)  
     - Description: Listado de proveedores.
--%> 
<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<% List listProveedores  = model.PrestamoSvc.getBeneficiarios(); %>
<html>
    <head>

        <title>Listado de Proveedores Generales</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <link href="../../css/estilostsp.css" rel="stylesheet" type="text/css">	
        <script src="<%= BASEURL %>/js/boton.js"></script>
        <script src="<%= BASEURL %>/js/validacionesPrestamos.js"></script>
        <script>
            var BASEURL    = '<%= BASEURL    %>';
            var CONTROLLER = '<%= CONTROLLER %>';
        </script>

    </head>
<body>
 
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listado de Proveedores"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
 

        <center>
        <br>

        <table align="center"  border="2" bgcolor="#F7F5F4" width='501'>
        <tr>
            <td width="489" align='center' >
            <table class='tablaInferior' width='100%'>
            <tr>
                <td >
                    <table border='0' width='100%' cellspacing='0'>
                        <tr>
                            <td class='subtitulo1'  width="50%">&nbsp; Listado de Proveedores Generales</td>
                            <td class='barratitulo' width="25%">
                                <img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20" align='left'> 
                            </td>
                            <td class='barratitulo' width="25%" align='right'>
                                <a href='<%= CONTROLLER %>?estado=Consulta&accion=Prestamos&Opcion=ConsultaProveedores&op=reload' class='Simulacion_Hiper'>Recargar Listado</a>
                                &nbsp;
                            </td>                            
                        </tr>
                    </table>             
                </td> 
            </tr>  
            <tr class='fila'>
                <td>Indique el nombre del proveedor que desea consultar</td>
            </tr>        
            <tr class='fila'>
            <td><input type='text' name='tabla' style="width:100%" onkeyup="filtrarCombo (this.value, lista)"></td>
        </tr>
        <tr class='fila'>
        <td>
            <select id='lista' name='lista' style='width:100%;' class='textbox'  size='15' onchange='tabla.value=this[this.selectedIndex].text' ondblclick='retornar(this);'  title='Proveedores'>
           <% for(int i=0; i< listProveedores.size(); i++){
                         Hashtable tabla = (Hashtable)listProveedores.get(i);
                         String nit  = (String) tabla.get("nit");
                         String name = (String) tabla.get("nombre");%>
            <option value='<%= nit %>'>  <%= name %> </option>
            <% }%>                                         
            </select>			
            </tr>
            </table>
        </td>
        </tr>   
        </table>


        <br >
        <img name="imgGrabar" src="<%=BASEURL%>/images/botones/aceptar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" retornar(lista); ">
        &nbsp;
        <img name="imgSalir" src="<%=BASEURL%>/images/botones/salir.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" window.close(); " >
        <br>

        </center>
        
</div>

    </body>
</html>
