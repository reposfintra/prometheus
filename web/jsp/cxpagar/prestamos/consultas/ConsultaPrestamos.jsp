<!--
     - Author(s)       :      MARIO FONTALVO SOLANO
     - Date            :      12/02/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Formulario para consultar prestamos
 --%>

<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>
<%
  String  usuario          = ((Usuario) session.getAttribute("Usuario")).getLogin();
  List    listTerceros     = model.PrestamoSvc.getTerceros();
  List    listTipoPrestamo = model.PrestamoSvc.getTipoPrestamo();
  List    listPeriodos     = model.PrestamoSvc.getPeriodos();
  List    listClasificacion= model.PrestamoSvc.getClasificaciones();
  String  Mensaje          = (request.getParameter("Mensaje")==null)?"":request.getParameter("Mensaje");
%>
<html>
    <head>
        <title>Consulta de Prestamos</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <link href="../../css/estilostsp.css" rel="stylesheet" type="text/css">
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        <script src="<%= BASEURL %>/js/validacionesPrestamos.js"></script>
        <script src="<%= BASEURL %>/js/validar.js"></script>
        <script>
            var BASEURL    = '<%= BASEURL    %>';
            var CONTROLLER = '<%= CONTROLLER %>';
            function seleccionarPorCheck (input){
               input.parentNode.parentNode.className = (input.checked?'filaamarilla':'fila'); 
            }
            
            function validarFormPrestamo( form ){
              with (form){
                 if (opBeneficiario.checked && Beneficiario.value==''){
                    alert ('Debe indicar la identificacion del Beneficiario para poder continuar');
                    return false;
                 }
                 
                 if (opFecha.checked && (FechaInicial.value=='' || FechaFinal.value=='')){
                    alert ('Debe indicar el rango de fecha de desembolso para poder continuar');
                    return false;
                 }
              
                 if (opId.checked && Id.value==''){
                    alert ('Debe indicar el numero del prestamo para poder continuar');
                    return false;
                 }
                 
                 if (opTipoPrestamo.checked && TipoPrestamo.selectedIndex == -1){
                    alert ('Debe indicar por lo menos un tipo de prestamo para poder continuar');
                    return false;
                 }                 

                 
              }
              return true;
            }
        </script>
    </head>
<body onresize="redimensionar()" onload = "redimensionar(); focus(formulario.Estado);">

 
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Consulta de Prestamos"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
 

        <center>
        <br>
        <form name="formulario" action="<%= CONTROLLER %>?estado=Consulta&accion=Prestamos&Opcion=Consultar" method="post">
        <table width="500" border="2">
        <tr>
        <td width="500"><table width="100%" class="tablaInferior" >
        <tr class="barratitulo">
            <td colspan="3">
                <table width="100%" cellpadding='0' cellspacing='0'>
                    <tr>
                        <td class="subtitulo1">&nbsp;Consulta Prestamos </td>
                        <td width="52%"><img src="<%= BASEURL %>/images/titulo.gif" width="33" height="20"></td>			
                    </tr>
                </table>
            </td>
        </tr>
        <tr class="fila">
            <td width="6%" align="center"><input name="opEstado" type="checkbox" id="opEstado" value="ok" onclick="seleccionarPorCheck(this); "></td>
            <td width="27%">Estado Prestamo </td>
            <td width="67%"><select name="Estado" class="listmenu" id="Estado">
                <option value="S">Aprobado    </option>
                <option value="N">No Aprobados</option>
                <option value="P">Pagados     </option>
                <option value="V">Vigentes    </option>
				<option value="T">Transferidos    </option>
            </select></td>
        </tr>
        <tr class="fila">
        <td align="center"><input name="opTercero" type="checkbox" id="opTercero" value="ok" onclick="seleccionarPorCheck(this); "></td>
        <td>Tercero</td>
        <td>
            <select name='Tercero' style='width:100%'   title='Tercero'>
               <% for(int i=0; i< listTerceros.size(); i++){
                     Hashtable tabla = (Hashtable)listTerceros.get(i);
                     String code = (String) tabla.get("codigo");
                     String desc = (String) tabla.get("descripcion");%>
                <option  value='<%= code %>'> <%=desc %> </option>
               <% }%>                                         
            </select>
        </td>
        </tr>
        <tr class="fila">
            <td align="center"><input name="opBeneficiario" type="checkbox" id="opBeneficiario" value="ok" onclick="seleccionarPorCheck(this); "></td>
            <td>Beneficiario</td>
        <td>
            <input name="Beneficiario" type="text" id="Beneficiario" style='text-align:center; width:30%' onkeyup="NombreBeneficiario.value=''; " onfocus='this.select();' >
            <input name="NombreBeneficiario" type="text" class="textbox" id="NombreBeneficiario" style="width:60%" readonly>
            <img src="<%= BASEURL %>/images/botones/iconos/lupa.gif" width="15" height="16" style="cursor:hand; " onClick="encontrarProveedor();"></td>
        </tr>
        <tr class="fila">
        <td align="center"><input name="opClasificacion" type="checkbox" id="opClasificacion" value="ok" onclick="seleccionarPorCheck(this); "></td>
        <td>Clasificacion</td>
        <td>
            <select name='Clasificacion' style='width:100%'   title='Clasificacion del prestamo'>
               <% for(int i=0; i< listClasificacion.size(); i++){
                     Hashtable tabla = (Hashtable) listClasificacion.get(i);
                     String code = (String) tabla.get("codigo");
                     String desc = (String) tabla.get("descripcion"); %>
                <option value='<%= code %>'> <%=desc %> </option>
               <% }%>                                         
            </select>           
        </td>
        </tr>        

        <tr class="fila">
        <td align="center"><input name="opPeriodo" type="checkbox" id="opPeriodo" value="ok" onclick="seleccionarPorCheck(this); "></td>
        <td>Periodos</td>
        <td>
            <select name='Periodo' style='width:100%'   title='Periodos de Pago'>
               <% for(int i=0; i< listPeriodos.size(); i++){
                     Hashtable tabla = (Hashtable)listPeriodos.get(i);
                     String code = (String) tabla.get("referencia");
                     String desc = (String) tabla.get("descripcion"); %>
                <option value='<%= code %>'> <%=desc %> </option>
               <% }%>                                         
            </select>        
        </td>
        </tr>
        
        
        <tr class="fila">
        <td align="center"><input name="opId" type="checkbox" id="opId" value="ok" onclick="seleccionarPorCheck(this); "></td>
        <td>Id del prestamo</td>
        <td>
            <input name="Id" type="text" id="Id" value="" onkeypress="soloDigitos(event,'decNO');" style='text-align:center' onfocus='this.select();'>
        </td>
        </tr>
        <tr class="fila">
        <td align="center"><input name="opFecha" type="checkbox" id="opFecha" value="ok" onclick="seleccionarPorCheck(this); "></td>
        <td>Fecha Desembolso</td>
        <td>
            <input name="FechaInicial" type="text" id="FechaInicial" value="" readonly  style='text-align:center' onfocus='this.select();'>
            <a href='javascript:void(0)' onclick='if(self.gfPop)gfPop.fPopCalendar(document.formulario.FechaInicial);return false;' HIDEFOCUS><img src='<%= BASEURL %>/js/Calendario/cal.gif' width='16' height='16' border='0' alt='De click aqui para escoger la fecha'></a>
            <input name="FechaFinal" type="text" id="FechaFinal"   value="" readonly  style='text-align:center' onfocus='this.select();'>
            <a href='javascript:void(0)' onclick='if(self.gfPop)gfPop.fPopCalendar(document.formulario.FechaFinal);return false;' HIDEFOCUS><img src='<%= BASEURL %>/js/Calendario/cal.gif' width='16' height='16' border='0' alt='De click aqui para escoger la fecha'></a>
        </td>
        </tr>   
     
        <tr class="fila">
        <td align="center" rowspan='2'><input name="opTipoPrestamo" type="checkbox" id="opTipoPrestamo" value="ok" onclick="seleccionarPorCheck(this); "></td>
        <td colspan='2'>Tipo Prestamos</td></tr>
        <tr><td colspan='2'>
            <select name='TipoPrestamo' style='width:100%'   title='Tipo de prestamos' MULTIPLE size='5'>
           <% for(int i=0; i< listTipoPrestamo.size(); i++){
                 Hashtable tabla = (Hashtable)listTipoPrestamo.get(i);
                 String code = (String) tabla.get("codigo");
                 String desc = (String) tabla.get("descripcion"); %>
                <option value='<%= code %>' > <%=desc %> </option>
           <% }%>                                         
            </select>
        
        </td>
        </tr>        
        
        
        <tr class="fila">
            <td align="center"><input name="opUsuario" type="checkbox" id="opUsuario" value="ok" onclick="seleccionarPorCheck(this); "></td>
            <td colspan="2">Prestamos creado por el usuario en sesion (<%= usuario %> )
            <input name="Usuario" type="hidden" id="Usuario" value="<%= usuario %>"></td>
        </tr>
        </table></td>
        </tr>
        </table>
        <br>
        <img src='<%=BASEURL%>/images/botones/buscar.gif'     name='i_buscar' id="i_buscar"    style="cursor:hand"  title='Consultar Prestamos....'   onclick=" if (validarFormPrestamo(formulario) ) { formulario.submit(); }"     onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
        &nbsp;
        <img src='<%=BASEURL%>/images/botones/salir.gif'      style='cursor:hand'    title='Salir...'               name='i_salir' onclick="window.close();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
        <input type="hidden" name="TipoListado" value="Programa">
        </form>
        </center>

<!-- Mensaje -->
   
   <% if( ! Mensaje.equals("") ){%>
          <br><br>
          <table border="2" align="center">
              <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                  <tr>
                    <td width="500" align="center" class="mensajes"><%= Mensaje %></td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="58">&nbsp; </td>
                  </tr>
                </table></td>
              </tr>
            </table>
   <%}%>
   
        <iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins_24.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
    </body>
</html>

<script>
<% 
   String opestado =request.getParameter("opEstado");
   String estado =request.getParameter("Estado");
   if (opestado!=null) { out.print("formulario.Estado.value='"+ estado +"'; formulario.opEstado.checked=true; seleccionarPorCheck(formulario.opEstado); "); }
        
   String optercero =request.getParameter("opTercero");
   String tercero =request.getParameter("Tercero");
   if (optercero!=null) { out.print("formulario.Tercero.value='"+ tercero +"'; formulario.opTercero.checked=true; seleccionarPorCheck(formulario.opTercero); "); }
   
   String opbeneficiario  = request.getParameter("opBeneficiario");
   String beneficiario  = request.getParameter("Beneficiario");
   String nbeneficiario = request.getParameter("NombreBeneficiario");
   if (opbeneficiario!=null) { out.print("formulario.Beneficiario.value='"+ beneficiario +"'; formulario.NombreBeneficiario.value='"+ nbeneficiario +"'; formulario.opBeneficiario.checked=true; seleccionarPorCheck(formulario.opBeneficiario); "); }
   
   String opclasificacion =request.getParameter("opClasificacion");
   String clasificacion =request.getParameter("Clasificacion");
   if (opclasificacion!=null) { out.print("formulario.Clasificacion.value='"+ clasificacion +"'; formulario.opClasificacion.checked=true; seleccionarPorCheck(formulario.opClasificacion); "); }

   String optipo =request.getParameter("opTipoPrestamo");
   String tipo =request.getParameter("TipoPrestamo");
   if (optipo!=null) { out.print("formulario.TipoPrestamo.value='"+ tipo +"'; formulario.opTipoPrestamo.checked=true; seleccionarPorCheck(formulario.opTipoPrestamo); "); }

   String opperiodo = request.getParameter("opPeriodo");
   String periodo = request.getParameter("Periodo");
   if (opperiodo!=null) { out.print("formulario.Periodo.value='"+ periodo +"'; formulario.opPeriodo.checked=true; seleccionarPorCheck(formulario.opPeriodo); "); }

   String opid = request.getParameter("opId");
   String id = request.getParameter("Id");
   if (opid!=null) { out.print("formulario.Id.value='"+ id +"'; formulario.opId.checked=true; seleccionarPorCheck(formulario.opId); "); }

   String opfechas = request.getParameter("opFecha");
   String FechaInicial = request.getParameter("FechaInicial");
   String FechaFinal   = request.getParameter("FechaFinal");
   if (opfechas!=null) { out.print("formulario.FechaInicial.value='"+ FechaInicial +"'; formulario.FechaFinal.value='"+ FechaFinal +"'; formulario.opFecha.checked=true; seleccionarPorCheck(formulario.opFecha); "); }
   
   String opusuario = request.getParameter("opUsuario");
   if (opusuario!=null) { out.print("formulario.opUsuario.checked=true; seleccionarPorCheck(formulario.opUsuario); "); }

   
%>

</script>
