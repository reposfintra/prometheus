<!--
     - Author(s)       :      MARIO FONTALVO SOLANO
     - Date            :      12/02/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Formulario para definir los parametro de consulta de transferencias
 --%>

<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
  String  usuario          = ((Usuario) session.getAttribute("Usuario")).getLogin();
  List    listTerceros     = model.PrestamoSvc.getTerceros();
  String  Mensaje          = (request.getParameter("Mensaje")==null)?"":request.getParameter("Mensaje");
%>
<html>
    <head>
        <title>Consulta de Amortizaciones Transferidas</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <link href="../../css/estilostsp.css" rel="stylesheet" type="text/css">
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        <script src="<%= BASEURL %>/js/validacionesPrestamos.js"></script>
        <script>
            var BASEURL    = '<%= BASEURL    %>';
            var CONTROLLER = '<%= CONTROLLER %>';
        </script>
    </head>
    <body onresize="redimensionar()" onload = "redimensionar(); focus(formulario.distrito);">

 
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Consulta Cuotas Migradas"/>
        </div>

        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
 

            <center>
            <br>
            <form name="formulario" action="<%= CONTROLLER %>?estado=Consulta&accion=Transferencias&Opcion=Consultar" method="post">
            <table width="479" border="2">
            <tr>
            <td width="467"><table width="100%" class="tablaInferior" >
                <tr class="barratitulo">
                    <td colspan="3">
                        <table width="100%" cellpadding='0' cellspacing='0'>
                            <tr>
                                <td class="subtitulo1">&nbsp;Consulta Migraciones </td>
                                <td width="52%"><img src="<%= BASEURL %>/images/titulo.gif" width="33" height="20"></td>			
                            </tr>
                        </table>
                    </td>
                </tr>
                
                <tr class="fila">
                  <td>&nbsp;Distrito</td>
                  <td>
                       <input:select name="distrito" attributesText="<%= "id='distrito'  style='width:45%;' class='textbox'  " %>"  default="FINV" options="<%=  model.ciaService.getCbxCia() /*model.CiaSvc.getLista()*/ %>" />
                  </td>
                </tr>

                <tr class="fila">
                <td>&nbsp;Tercero</td>
                <td>
                    <select name='tercero' style='width:45%'   title='Tercero'>
               <% for(int i=0; i< listTerceros.size(); i++){
                     Hashtable tabla = (Hashtable)listTerceros.get(i);
                     String code = (String) tabla.get("codigo");
                     String desc = (String) tabla.get("descripcion");%>
                        <option  value='<%= code %>'> <%=desc %> </option>
               <% }%>                                         
                    </select>
                </td>
                </tr>
                <tr class="fila">
                <td>&nbsp;Fecha Migración</td>
                <td>
                    <input name="fechaInicial" type="text" id="fechaInicial" readonly style="text-align:center " value='<%= UtilFinanzas.getFechaActual_String(4) + " 00:00" %>'>
                    <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fechaInicial);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a>          
                    <input name="fechaFinal"   type="text" id="fechaFinal" readonly style="text-align:center " value='<%= UtilFinanzas.getFechaActual_String(4) + " 23:59" %>'>
                    <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fechaFinal  );return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a>
                </td>
                </tr>
                </table>
            </td>
            </tr>
            </table>
            <br>
            <img src='<%=BASEURL%>/images/botones/buscar.gif'     name='i_buscar' id="i_buscar"    style="cursor:hand"  title='Consultar Prestamos....'   onclick="formulario.submit();"     onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
            &nbsp;
            <img src='<%=BASEURL%>/images/botones/salir.gif'      style='cursor:hand'    title='Salir...'               name='i_salir' onclick="window.close();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
        
            </form>
            </center>

            <!-- Mensaje -->
   
   <% if( ! Mensaje.equals("") ){%>
            <br><br>
            <table border="2" align="center">
                <tr>
                    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                        <tr>
                            <td width="500" align="center" class="mensajes"><%= Mensaje %></td>
                            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                            <td width="58">&nbsp; </td>
                        </tr>
                    </table></td>
                </tr>
            </table>
   <%}%>
   

        </div>
            <iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins_24.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
    </body>
</html>
