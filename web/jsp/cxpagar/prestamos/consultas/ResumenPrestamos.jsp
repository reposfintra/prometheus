<!--
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      04/03/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Vista que filtra para  mostrar el estado general  de los prestamos
 --%>


<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>


<html>
<head>
         <title>Resumen Prestamo</title>
         <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
         <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>  
         <script src="<%= BASEURL %>/js/validacionesPrestamos.js"></script>
    
</head>
<body  onresize="redimensionar()" onload = "redimensionar(); focus(formulario.tercero);">

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Resumen General de Prestamos"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>

<% List     listTerceros          = model.PrestamoSvc.getTerceros();         
   List     listClasificacion     = model.PrestamoSvc.getClasificaciones();%>

<form action="<%=CONTROLLER%>?estado=Resumen&accion=Prestamo&evento=BUSCAR"  method='post' name='formulario'>


         <table border="2" align="center" width='400'>
              <tr>
                   <td>      
                            <table  width='100%' class='tablaInferior' >
                                   <tr>
                                      <td colspan='7'>
                                          <table width='100%'  class="barratitulo" cellpadding='0' cellspacing='0'>
                                             <tr class="fila">
                                                    <td align="left" width='60%' class="subtitulo1" nowrap> &nbsp Resumen General </td>
                                                    <td align="left" width='*' ><img src="<%=BASEURL%>/images/titulo.gif"></td>
                                             </tr>
                                          </table>
                                      </td>
                                   </tr> 
                                   <tr class='fila'>
                                       <td width='30%'>&nbsp Tercero             </td>
                                       <td width='*'>                                       
                                                 <select name='tercero' style='width:60%'   title='Tercero'>
                                                   <% for(int i=0; i< listTerceros.size(); i++){
                                                         Hashtable tabla = (Hashtable)listTerceros.get(i);
                                                         String code = (String) tabla.get("codigo");
                                                         String desc = (String) tabla.get("descripcion");%>
                                                         <option  value='<%= code %>'   > <%=desc %> </option>
                                                   <% }%>                                         
                                                  </select>
                                       </td>
                                   </tr>
                                   <tr class='fila'>
                                       <td width='20%'>&nbsp Clasificación             </td>
                                       <td width='*'>    
                                   
                                                 <select name='clasificacion' style='width:60%'   title='Clasificación de Prestamos'>
                                                   <% for(int i=0; i< listClasificacion.size(); i++){
                                                         Hashtable tabla = (Hashtable)listClasificacion.get(i);
                                                         String code = (String) tabla.get("codigo");
                                                         String desc = (String) tabla.get("descripcion");%>
                                                         <option  value='<%= code %>'   > <%=desc %> </option>
                                                   <% }%>                                  
                                                  </select>
                                       </td>
                                   </tr>
                         </table>
                   </td>
              </tr>
         </table>
 
 
 
 


  <p>
       <img src='<%=BASEURL%>/images/botones/aceptar.gif'    style="cursor:hand"   title='Cosulta resumen....'     name='i_crear'   onclick="formulario.submit()"    onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
       <img src='<%=BASEURL%>/images/botones/salir.gif'      style='cursor:hand'    title='Salir...'               name='i_salir'   onclick='parent.close();'        onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
   </p>





</div>

</body>
</html>
