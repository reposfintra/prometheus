
<!--  
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      13/03/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: listado de prestamos de los proveedores para migrar
 --%>

 
<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>



<html>
<head>
       <title>Migración de Cuotas por Proveedor</title>
       <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
       <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>  
       <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
       <script src="<%= BASEURL %>/js/validacionesPrestamos.js"></script>
       <SCRIPT>
           function sendForm(url){
              location.href= (url);
           }
       </SCRIPT>
      
</head>
<body onresize="redimensionar()" onload = "redimensionar(); ">


<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Migración de Cuotas por Proveedor"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center> 


 
    <% List listPrestamos  =  model.CuotasSvc.getListPrestamos();  
       String   urlAccion  = CONTROLLER + "?estado=Cuotas&accion=Manager&evento=VIEW_CUOTAS_SELECT"; 
       String   msj          = request.getParameter("msj");%>
    
    
    <table border="2" align="center" >
              <tr>
                   <td>      
                            <table  width='100%' class='tablaInferior' >
                                   <tr>
                                      <td colspan='13'>
                                          <table width='100%'  class="barratitulo" cellpadding='0' cellspacing='0'>
                                             <tr class="fila">
                                                    <td align="left" width='50%' class="subtitulo1" nowrap> &nbsp Lista de Prestamos </td>
                                                    <td align="left" width='*' ><img src="<%=BASEURL%>/images/titulo.gif"></td>
                                             </tr>
                                          </table>
                                      </td>
                                   </tr> 
                                   
                                  <tr class="tblTitulo" style="font size:11" >
                                      <TH  nowrap width='250'                                           >BENEFICIARIO         </TH>
                                      <TH  nowrap width='70'                                            >PRESTAMO No          </TH>
                                      <TH  nowrap width='120' title='Valor Prestdo'                     >CAPITAL              </TH>               
                                      <TH  nowrap width='120' title='Valor Total Interes del Prestamo'  >INTERESES            </TH> 
                                      <TH  nowrap width='120' title='Valor Capiltal Descontado'         >CAPITAL DESC         </TH>               
                                      <TH  nowrap width='120' title='Valor Interes  Descontado'         >INTERES DESC         </TH>
                                      <TH  nowrap width='120'                                           >VLR MIGRADO<br>MIMS  </TH>              
                                      <TH  nowrap width='120'                                           >VLR REGISTRADO MIMS  </TH>               
                                      <TH  nowrap width='120'                                           >VLR DESCONTADO PROP. </TH>               
                                      <TH  nowrap width='120'                                           >VLR PAGADO <br>FINTRA</TH> 
                                      <TH  nowrap width='120'                                           >SALDO <br>NO MIGRADO     </TH>               
                                      <TH  nowrap width='120'                                           >SALDO <br>DESCONTADO     </TH>               
                                      <TH  nowrap width='120'                                           >SALDO <br>FINTRA         </TH>
                                 </tr>
                                 
                                 
                                 <%    double  vlrCapital        = 0;
                                       double  vlrIntereses      = 0;
                                       double  vlrCapitalPagado  = 0;
                                       double  vlrInteresPagado  = 0;
                                       double  vlrMigradoMims    = 0;
                                       double  vlrRegistradoMims = 0;
                                       double  vlrDescontadoPro  = 0;
                                       double  vlrPagadoTercero  = 0;
                                       double  vlrNoMigrado      = 0;
                                       double  vlrDescontado     = 0;
                                       double  vlrSaldo          = 0;

                                     for(int i=0;i<listPrestamos.size();i++){
                                       ResumenPrestamo  ptm = (ResumenPrestamo) listPrestamos.get(i); 
                                       vlrCapital        += ptm.getValor();
                                       vlrIntereses      += ptm.getIntereses();
                                       vlrCapitalPagado  += ptm.getValorDesc();
                                       vlrInteresPagado  += ptm.getInteresesDesc();
                                       vlrMigradoMims    += ptm.getVlrMigradoMims();
                                       vlrRegistradoMims += ptm.getVlrRegistradoMims();
                                       vlrDescontadoPro  += ptm.getVlrDescontadoProp();
                                       vlrPagadoTercero  += ptm.getVlrPagadoFintra();
                                       vlrNoMigrado      += ptm.saldoNoMigrado();
                                       vlrDescontado     += ptm.saldoDescontado();
                                       vlrSaldo          += ptm.saldoPagadoTercero();%>
                                       
                                       <tr class='<%= (i%2==0?"filagris":"filaazul") %>'  style="cursor:hand; font size:12" onMouseOver='cambiarColorMouse(this)' onclick=" newWindow('<%=CONTROLLER %>?estado=Cuotas&accion=Manager&evento=VIEW_AMORT&id=<%=ptm.getId()%>', 'detPrestamos'); "  >
                                          <td class='bordereporte'               ><%= ptm.getNombre()  %>    </td>
                                          <td class='bordereporte' align='right' ><%= ptm.getId()      %>    </td>
                                          <td class='bordereporte' align='right' ><%= UtilFinanzas.customFormat2(ptm.getValor()             )%></td>
                                          <td class='bordereporte' align='right' ><%= UtilFinanzas.customFormat2(ptm.getIntereses()         )%></td>                                          
                                          <td class='bordereporte' align='right' ><%= UtilFinanzas.customFormat2(ptm.getValorDesc()         )%></td>
                                          <td class='bordereporte' align='right' ><%= UtilFinanzas.customFormat2(ptm.getInteresesDesc()     )%></td>
                                          <td class='bordereporte' align='right' ><%= UtilFinanzas.customFormat2(ptm.getVlrMigradoMims()    )%></td>
                                          <td class='bordereporte' align='right' ><%= UtilFinanzas.customFormat2(ptm.getVlrRegistradoMims() )%></td>
                                          <td class='bordereporte' align='right' ><%= UtilFinanzas.customFormat2(ptm.getVlrDescontadoProp() )%></td>
                                          <td class='bordereporte' align='right' ><%= UtilFinanzas.customFormat2(ptm.getVlrPagadoFintra()   )%></td>
                                          <td class='bordereporte' align='right' ><%= UtilFinanzas.customFormat2( ptm.saldoNoMigrado()      )%></td>
                                          <td class='bordereporte' align='right' ><%= UtilFinanzas.customFormat2( ptm.saldoDescontado()     )%></td>
                                          <td class='bordereporte' align='right' ><%= UtilFinanzas.customFormat2( ptm.saldoPagadoTercero()  )%></td>
                                          
                                      </tr>
                                  <%}%>
                                  
                                  
                                  <tr  class="tblTitulo" style="font size:12">
                                          <th class='bordereporte' align='left'  colspan='2'   >TOTALES                                                 </th>
                                          <th class='bordereporte' align='right' ><%= UtilFinanzas.customFormat2(  vlrCapital          )%></th>
                                          <th class='bordereporte' align='right' ><%= UtilFinanzas.customFormat2(  vlrIntereses        )%></th>                                          
                                          <th class='bordereporte' align='right' ><%= UtilFinanzas.customFormat2(  vlrCapitalPagado    )%></th>
                                          <th class='bordereporte' align='right' ><%= UtilFinanzas.customFormat2(  vlrInteresPagado    )%></th>
                                          <th class='bordereporte' align='right' ><%= UtilFinanzas.customFormat2(  vlrMigradoMims      )%></th>
                                          <th class='bordereporte' align='right' ><%= UtilFinanzas.customFormat2(  vlrRegistradoMims   )%></th>
                                          <th class='bordereporte' align='right' ><%= UtilFinanzas.customFormat2(  vlrDescontadoPro    )%></th>
                                          <th class='bordereporte' align='right' ><%= UtilFinanzas.customFormat2(  vlrPagadoTercero    )%></th>                                          
                                          <th class='bordereporte' align='right' ><%= UtilFinanzas.customFormat2(  vlrNoMigrado        )%></th>
                                          <th class='bordereporte' align='right' ><%= UtilFinanzas.customFormat2(  vlrDescontado       )%></th>
                                          <th class='bordereporte' align='right' ><%= UtilFinanzas.customFormat2(  vlrSaldo            )%></th>
                                          
                                  </tr>
                                 

                          </table>
                    </td>
               </tr>
      </table>
              
        
      
  <!-- Botones --> 
   <p>
      <img src='<%=BASEURL%>/images/botones/aceptar.gif'    style="cursor:hand"  title='Migrar Cuotas'     name='i_crear'   onclick="sendForm('<%= urlAccion%>');"    onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
      <img src='<%=BASEURL%>/images/botones/salir.gif'      style='cursor:hand'   title='Salir...'          name='i_salir'   onclick='parent.close();'                 onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
  </p>
  
  
  
   <!--MENSAJE-->
   
   <% if( msj!=null  && ! msj.equals("") ) { %>          
          <table border="2" align="center">
               <tr>
                   <td>
                       <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                          <tr>
                            <td width="500" align="center" class="mensajes"> <%= msj %> </td>                            
                            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                            <td width="58">&nbsp; </td>
                          </tr>
                      </table>
                  </td>
                </tr>
            </table>  
   <% } %>
                                 
                                 
</div>                               
</body>
</html>
