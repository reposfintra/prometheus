
<!--  
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      13/03/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Parametros para migrar  a mims por proveedor
 --%>

 
<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>

<html>
<head>
        <title>Migrar cuotas por Proveedor</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>  
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
    
      
   <script>      
      function buscarCuotas(theForm, img){ 
          anularBtn(img);
          selectAll( theForm.proveedoresFiltro );
          if ( theForm.proveedoresFiltro.selectedIndex!=-1){
               theForm.submit();
          }
          else{
              alert('Deber� seleccionar proveedores');   
              activarBtn(img);
          }
      }
   </script>
   
   
        
</head>
<body onresize="redimensionar()" onload = "redimensionar(); focus(formulario.tercero);">


<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Migraci�n de Cuotas por Proveedor"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center> 

<%  List    listTerceros     = model.PrestamoSvc.getTerceros();
    List    listProveedores  = model.PrestamoSvc.getBeneficiarios();
    String  urlAccion        = CONTROLLER + "?estado=Cuotas&accion=Manager"; %>
    
    
  <form  method='post' name='formulario' action="<%= urlAccion %>&evento=SEARCH_PROVEEDOR"> 
   
   <table width="650"  border="2" align="center">
     <tr>
      <td>
      
            <table  width='100%' class='tablaInferior' >  
                   <tr>
                      <td colspan='2'>
                          <table width='100%'  class="barratitulo" cellpadding='0' cellspacing='0'>
                             <tr class="fila">
                                    <td align="left" width='60%' class="subtitulo1" nowrap> &nbsp Consultar Cuotas a Migrar por Proveedor</td>
                                    <td align="left" width='*' ><img src="<%=BASEURL%>/images/titulo.gif"></td>
                             </tr>
                          </table>
                      </td>
                   </tr>  
                   
                   <tr class="fila">                   
                        <td width='20%'> &nbsp Tercero  </td>
                        <td width='*'>                                       
                             <select name='tercero' style='width:40%'   title='Tercero'>
                               <% for(int i=0; i< listTerceros.size(); i++){
                                     Hashtable tabla = (Hashtable)listTerceros.get(i);
                                     String code = (String) tabla.get("codigo");
                                     String desc = (String) tabla.get("descripcion");%>
                                     <option  value='<%= code %>'  > <%=desc %> </option>
                               <% }%>                                         
                              </select>
                        </td>
                   </tr>
                   
                   
                   <tr class='fila'>
                          <td colspan='2'> &nbsp Proveedor</td>
                   </tr>   
     
                   <tr class='fila'>
                         <td colspan='2' >
                            <table cellpadding='0' cellspacing='0' width='100%'>
                                 <tr>
                                      <td width='45%'>
                                          
                                            <input type='text' name='busqueda'    style="width:100%"  title='Buscar por palabras' onkeyup='searchCombo(this.value, this.form.proveedores)' onkeypress='searchCombo(this.value, this.form.proveedores)'>
                                            <select id='lista' multiple name='proveedores' style='width:100%;' class='textbox'  size='10'  title='Proveedores' onchange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text; }">
                                               <% for(int i=0; i< listProveedores.size(); i++){
                                                             Hashtable tabla = (Hashtable)listProveedores.get(i);
                                                             String nit  = (String) tabla.get("nit");
                                                             String name = (String) tabla.get("nombre");%>
                                                <option value='<%= nit %>'>  <%= name %> </option>
                                                <% }%>                                         
                                            </select>
                                      </td>
                                      <td width='10%' align='center'>
                                                <img src='<%=BASEURL%>/images/botones/envDer.gif' style='cursor:hand' name='imgEnvDer'  onclick="move   (proveedores, proveedoresFiltro); "  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>             
                                                <img src='<%=BASEURL%>/images/botones/envIzq.gif' style='cursor:hand' name='imgEnvIzq'  onclick="move   (proveedoresFiltro, proveedores ); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>                                                                                                                      
                                      </td>
                                      <td width='45%'>
                                            <select id='lista' name='proveedoresFiltro' style='width:100%;' class='textbox'  size='12'  title='Proveedores' onchange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text; }">
                                     </td>
                                </tr>
                            </table>
                            <br>
                         </td>
                   </tr> 
            </table>


         </td>
      </tr>        
 </table>


   
  <!-- Botones --> 
   <p>
      <img src='<%=BASEURL%>/images/botones/aceptar.gif'    style="cursor:hand"  title='Crear Prestamo....'     name='crear'    onclick="buscarCuotas(formulario, this)"  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>          
      <img src='<%=BASEURL%>/images/botones/salir.gif'      style='cursor:hand'   title='Salir...'               name='i_salir'   onclick='parent.close();'                onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
  </p>

  
  
  

</div>

</body>
</html>
