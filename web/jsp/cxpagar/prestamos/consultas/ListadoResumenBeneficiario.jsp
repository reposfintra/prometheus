<!--
     - Author(s)       :      MARIO FONTALVO SOLANO
     - Date            :      12/02/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)
     - Description: Listado de prestamos
 --%>

<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>
<%
    String Mensaje     = (request.getParameter("Mensaje")==null)?"":request.getParameter("Mensaje");
    String TipoListado = (request.getParameter("TipoListado")==null)?"":request.getParameter("TipoListado");
   /**
    * Perfiles validos para la vista
    * AP: aprobacion
    * CO: consulta
    * VI: vista
    */
    String Perfil  = (String) session.getAttribute("PerfilPrestamo");
    if (Perfil==null) Perfil = "AP";
%>
<html>
<head>
    <title>Listado de Prestamos</title>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <link href="../../css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script src="<%= BASEURL %>/js/validacionesPrestamos.js"></script>
</head>
<body onresize="redimensionar()" onload = "redimensionar(); ">


<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listado de Prestamos"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
 

<center>

  <% Prestamo pr = (Prestamo) model.PrestamoSvc.getListaPrestamos().get(0); %>   
  <br>
  <table border="2">
  <tr>
    <td >
      <table  width="100%" class="tablaInferior" >
      <tr class="barratitulo">
   	    <td colspan="<%= (Perfil.equals("AP")?11:10) %> ">
	  		<table width="100%" cellpadding='0' cellspacing='0'>
			<tr>
				<td class="subtitulo1">&nbsp;Resumen Prestamos Beneficiario</td>
				<td width="52%"><img src="<%= BASEURL %>/images/titulo.gif" width="33" height="20"></td>			
			</tr>
			</table>
   	     </td>
      </tr>
      <tr >
        <td width='180' class='fila'>&nbsp;Beneficiario </td><td class='letra'>&nbsp;[<%= pr.getBeneficiario() %>]&nbsp;<%= pr.getBeneficiarioName() %></td>
      </tr>  
      <tr >
        <td width='180' class='fila'>&nbsp;Clasificacion Prestamo </td><td class='letra'>&nbsp;<%= pr.getClasificacionName() %> </td>
      </tr>
      </table>
      <table width="100%" class="tablaInferior" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
      <tr class="tblTitulo">
        <td width="40"  nowrap>&nbsp;ID</td>
        <td width="140" nowrap>&nbsp;Tipo Prestamo</td>
        <td width="50"  nowrap>&nbsp;Cuotas</td>
        <td width="50"  nowrap>&nbsp;Tasa</td>
        <td width="120" nowrap>Entrega Dinero</td>
        <td width="120" nowrap>&nbsp;Val. Prestamo</td>
        <td width="120" nowrap>&nbsp;Val. Migrado</td>
        <td width="120" nowrap>&nbsp;Val. Registrado</td>
        <td width="120" nowrap>&nbsp;Val. Descontado</td>
        <td width="120" nowrap>&nbsp;Val. Pagado</td>
        <td width="120" nowrap>&nbsp;Capital Pagado</td>
        <td width="120" nowrap>&nbsp;Saldo Capital</td>
        
      </tr>
   	  <%  List lista = model.PrestamoSvc.getListaPrestamos();
	      for (int i = 0; i < lista.size(); i++){
                Prestamo pt = (Prestamo) lista.get(i); 
                String style = (i%2==0?"filagris":"filaazul"); %>
		  <tr class="<%= style %>" onclick=' viewAmortizaciones(<%= i %>) ' style='cursor:hand;' onMouseOver='cambiarColorMouse(this)'>
			<td class='bordereporte' align='center' nowrap><%= pt.getId()               %></td>
			<td class='bordereporte'                nowrap><%= pt.getTipoPrestamoName() %></td>
			<td class='bordereporte' align='right'  nowrap><%= pt.getCuotas()           %></td>
			<td class='bordereporte' align='right'  nowrap><%= UtilFinanzas.customFormat2(pt.getTasa())%>&nbsp;%</td>
			<td class='bordereporte' align='center' nowrap><%= pt.getFechaEntregaDinero()                         %></td>
			<td class='bordereporte' align='right'  nowrap><%= UtilFinanzas.customFormat(pt.getMonto())           %></td>
			<td class='bordereporte' align='right'  nowrap><%= UtilFinanzas.customFormat(pt.getValorMigrado())    %></td>
			<td class='bordereporte' align='right'  nowrap><%= UtilFinanzas.customFormat(pt.getValorRegistrado()) %></td>
			<td class='bordereporte' align='right'  nowrap><%= UtilFinanzas.customFormat(pt.getValorDescontado()) %></td>
			<td class='bordereporte' align='right'  nowrap><%= UtilFinanzas.customFormat(pt.getValorPagado())     %></td>
			<td class='bordereporte' align='right'  nowrap><%= UtilFinanzas.customFormat(pt.getCapitalDescontado() ) %></td>
			<td class='bordereporte' align='right'  nowrap><%= UtilFinanzas.customFormat(pt.getMonto() - pt.getCapitalDescontado() )     %></td>
		  </tr>
	   <% } %>
	   
	   <% Prestamo pt = model.PrestamoSvc.getTotalesPr(); 
              if (pt!=null){ %>
		  <tr class="filaresaltada" >
			<td class='bordereporte' align='center' colspan='5' >TOTALES</td>			 
			<td class='bordereporte' align='right' ><%= UtilFinanzas.customFormat( pt.getMonto())           %></td>
			<td class='bordereporte' align='right' ><%= UtilFinanzas.customFormat( pt.getValorMigrado())    %></td>
			<td class='bordereporte' align='right' ><%= UtilFinanzas.customFormat( pt.getValorRegistrado()) %></td>
			<td class='bordereporte' align='right' ><%= UtilFinanzas.customFormat( pt.getValorDescontado()) %></td>
			<td class='bordereporte' align='right' ><%= UtilFinanzas.customFormat( pt.getValorPagado ())    %></td>
			<td class='bordereporte' align='right' ><%= UtilFinanzas.customFormat( pt.getCapitalDescontado()) %></td>
			<td class='bordereporte' align='right' ><%= UtilFinanzas.customFormat( pt.getMonto() - pt.getCapitalDescontado() )%></td>
		  </tr>	 
            <% } %>	   
		
    </table>
	</td>
  </tr>
</table>
<br>
<img src='<%=BASEURL%>/images/botones/exportarExcel.gif' style='cursor:hand'    title='Exportar Excel...'      name='i_exportar'    onclick="descargar('003');"     onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
<img src='<%=BASEURL%>/images/botones/salir.gif'      style='cursor:hand'    title='Salir...'               name='i_salir' onclick="window.close();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
</center>
</div>
</body>
</html>

