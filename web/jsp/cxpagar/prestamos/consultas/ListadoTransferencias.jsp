<!--
     - Author(s)       :      MARIO FONTALVO SOLANO
     - Date            :      20/02/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Listado de amortizciones tranferidas
 --%>

<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
  String Mensaje = (request.getParameter("Mensaje")==null)?"":request.getParameter("Mensaje");
%>
<html>
<head>
    <title>Listado de Prestamos</title>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <link href="../../css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script src="<%= BASEURL %>/js/validacionesPrestamos.js"></script>
    <script>
       function seleccionar(obj){
         var tr = obj.parentNode.parentNode;
         if (obj.checked)
           tr.className = 'filaseleccion';
         else
           tr.className = (tr.rowIndex%2==0?'filagris':'filaazul');
       }
    </script>
</head>
<body onresize="redimensionar()" onload = "redimensionar(); ">

 
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listado de Amortizaciones"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
 

<center>
  <br>
  <form name="formulario" action="<%= CONTROLLER %>?estado=Consulta&accion=Prestamos&Opcion=AprobarPrestamo" method="post">
  <table width="1460" border="2">
  <tr>
    <td >
      <table width="100%" class="tablaInferior" >
      <tr class="barratitulo">
   	    <td colspan="11">
	  		<table width="100%" cellpadding='0' cellspacing='0'>
			<tr>
				<td class="subtitulo1">&nbsp;Resultados de la Busqueda de Amortizaciones Transferidas Pendientes</td>
				<td width="40%"><img src="<%= BASEURL %>/images/titulo.gif" width="33" height="20"></td>			
			</tr>
			</table>
   	     </td>
      </tr>
      <table>
      
      <%  List lista = model.PrestamoSvc.getListaAmortizaciones();
          int maxPageItems  = 15;
          int maxIndexPages = 10;
      %>
      <pg:pager items="<%= lista.size()%>" index="center" maxPageItems="<%= maxPageItems %>" maxIndexPages ="<%= maxIndexPages %>" isOffset = "true" export ="offset,currentPageNumber=pageNumber" scope="request">
      <table width="100%" class="tablaInferior" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
      <tr class="tblTitulo">
        <th  colspan='8'  >DATOS DE LA AMORTIZACION</th>
        <th  colspan='2'  >BENEFICIARIO</th>
        <th  colspan='2'  >TERCERO</th>
      </tr>      
      
      <tr class="tblTitulo">
        <td  width="40" >Distrito</td>
        <td  width="80" >&nbsp;Tercero</td>
        <td  width="100">&nbsp;Nit</td>
        <td  width="250">&nbsp;Beneficiario</td>
        <td  width="20">&nbsp;Prestamo</td>
        <td  width="20">&nbsp;Cuota</td>
        <td  width="100">&nbsp;Fecha Pago</td>
        <td  width="120">&nbsp;Valor a pagar</td>
        <td  width="120">&nbsp;Fecha Desc. MIMS</td>
        <td  width="130">&nbsp;Valor Desc. MIMS</td>
        <td  width="120">&nbsp;Fecha Pago MIMS</td>
        <td  width="130">&nbsp;Valor Pagado MIMS</td>
        
      </tr>
		
   	  <%  for (int i = offset.intValue(), l = Math.min(i + maxPageItems, lista.size()); i < l; i++){ 
	         Amortizacion am = (Amortizacion) lista.get(i);
                 String estilo = (i%2==0?"filagris":"filaazul");
                 if (!am.getFechaMigracion().equals("0099-01-01 00:00:00")) {
                   if (am.getEstadoDescuento().equals("50") && am.getEstadoPagoTercero().equals("50"))
                      estilo = "filaverde";
                   else if (!am.getEstadoDescuento().equals("") || !am.getEstadoPagoTercero().equals(""))
                      estilo = "filaamarilla";
                   else 
                      estilo = "filaroja";
                 } %>
                  <pg:item>
		  <tr class="<%= estilo %>" >
			<td class='bordereporte' align='center'><%= am.getDstrct()           %></td>
			<td class='bordereporte' ><%= am.getNameTercero()      %></td>
			<td class='bordereporte' align='right' ><%= am.getBeneficiario() %></td>
			<td class='bordereporte' ><%= am.getNameBeneficiario() %></td>
			<td class='bordereporte' align='center'><%= am.getPrestamo()         %></td>
			<td class='bordereporte' align='center'><%= am.getItem()             %></td>
			<td class='bordereporte' align='center'><%= am.getFechaPago()        %></td>
			<td class='bordereporte' align='right' ><%= (am.getTotalAPagar()    !=0?UtilFinanzas.customFormat2(am.getTotalAPagar())    :"&nbsp;") %></td>
			<td class='bordereporte' align='center'><%= (am.getFechaDescuento  ()==null || am.getFechaDescuento().equals("0099-01-01")?"&nbsp;":am.getFechaDescuento())  %></td>
			<td class='bordereporte' align='right' ><%= (am.getValorDescuento  ()!=0?UtilFinanzas.customFormat2(am.getValorDescuento()):"&nbsp;") %></td>
			<td class='bordereporte' align='center'><%= (am.getFechaPagoTercero()==null || am.getFechaPagoTercero().equals("0099-01-01")?"&nbsp;":am.getFechaPagoTercero())  %></td>
			<td class='bordereporte' align='right' ><%= (am.getValorPagoTercero()!=0?UtilFinanzas.customFormat2(am.getValorPagoTercero()):"&nbsp;") %></td>
			
		  </tr>
		  </pg:item>
	   <% } Amortizacion am = model.PrestamoSvc.getTotalesAm(); %>
	   <% if (am!=null) {%>
		  <tr class="filaresaltada" >
			<td class='bordereporte' align='center' colspan='7'>TOTALES</td>
			<td class='bordereporte' align='right' ><%= (am.getTotalAPagar()    !=0?UtilFinanzas.customFormat2(am.getTotalAPagar())    :"&nbsp;") %></td>
			<td class='bordereporte' align='center'>&nbsp;</td>
			<td class='bordereporte' align='right' ><%= (am.getValorDescuento  ()!=0?UtilFinanzas.customFormat2(am.getValorDescuento()):"&nbsp;") %></td>
			<td class='bordereporte' align='center'>&nbsp;</td>
			<td class='bordereporte' align='right' ><%= (am.getValorPagoTercero()!=0?UtilFinanzas.customFormat2(am.getValorPagoTercero()):"&nbsp;") %></td>
			
		  </tr>
             <% } %>
	   
		  
                <tr>
                    <td colspan='12'><pg:index><jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/></pg:index></td>
                </tr>	   
		
    </table>
    </pg:pager>
	</td>
  </tr>
</table>
<br>
<img src='<%=BASEURL%>/images/botones/exportarExcel.gif' style='cursor:hand'    title='Exportar Excel...'      name='i_exportar'    onclick="descargar('005');"     onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
<img src='<%=BASEURL%>/images/botones/regresar.gif'      style='cursor:hand'    title='Regresar...'            name='i_regresar' onclick="document.location.href='<%= BASEURL %>/jsp/cxpagar/prestamos/consultas/ConsultaTransferencias.jsp'" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
&nbsp;
<img src='<%=BASEURL%>/images/botones/salir.gif'         style='cursor:hand'    title='Salir...'               name='i_salir' onclick="window.close();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>

</form>
<!--
<span class='informacion'>Para aprobar los prestamos por favor seleccione de la lista y haga click sobre el boton Aceptar, <br>si desea conocer las amortizaciones de haga click sobre el icono respectivo al prestamo </span>
-->

</center>

   <!-- Mensaje -->
   
   <% if( ! Mensaje.equals("") ){%>
          <br><br>
          <table border="2" align="center">
              <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                  <tr>
                    <td width="500" align="center" class="mensajes"><%= Mensaje %></td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="58">&nbsp; </td>
                  </tr>
                </table></td>
              </tr>
            </table>
   <%}%>
   
   
 <iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins_24.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
 
</div>
</body>
</html>


