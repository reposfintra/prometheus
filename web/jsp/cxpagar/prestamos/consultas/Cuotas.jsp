<!--
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      16/02/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Formulario de consulta de Cuotas
 --%>

 
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>

<html>
<head>
   <title>Migracion de Cuotas</title>
   <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
   <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>  
   <script type='text/javascript' src="<%= BASEURL %>/js/Validaciones.js">          </script> 
   
   <script>      
      function buscarCuotas(theForm){ 
          var parameter  = theForm.action; 
          for(i=0;i<theForm.length;i++) 
                parameter += '&'+ theForm.elements[i].name + '=' +  theForm.elements[i].value;
                
           var win = window.open(parameter,'Cuotas',' top=100,left=100, width=900, height=500, scrollbar=no, status=yes, resizable=yes  ');
           win.focus();
      }
   </script>
   
   
   
</head>
<body onresize="redimensionar()" onload = "redimensionar(); focus(formulario.tercero);">

 
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Migración de Cuotas en General"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center> 

<%    Usuario  User         = (Usuario) session.getAttribute("Usuario");
      String   hoy          = UtilFinanzas.getFechaActual_String(4);
      List     listTerceros = model.PrestamoSvc.getTerceros();
      String   urlAccion    = CONTROLLER + "?estado=Cuotas&accion=Manager&distrito="+ User.getDstrct() +"&usuario="+ User.getLogin() + "&nitUsuario=" + User.getNit() ; %>

   <form  method='post' name='formulario' action="<%= urlAccion %>&evento=SEARCH"> 
   
   <table width="400"  border="2" align="center">
     <tr>
      <td>
      
            <table  width='100%' class='tablaInferior' >     
 
                   <tr>
                      <td colspan='2'>
                          <table width='100%'  class="barratitulo" cellpadding='0' cellspacing='0'>
                             <tr class="fila">
                                    <td align="left" width='60%' class="subtitulo1" nowrap> &nbsp Consultar Cuotas por Migrar</td>
                                    <td align="left" width='*' ><img src="<%=BASEURL%>/images/titulo.gif"></td>
                             </tr>
                          </table>
                      </td>
                   </tr>  
                   
                   <tr class="fila">                   
                        <td width='30%'> &nbsp Tercero  </td>
                        <td width='*'>                                       
                             <select name='tercero' style='width:40%'   title='Tercero'>
                               <% for(int i=0; i< listTerceros.size(); i++){
                                     Hashtable tabla = (Hashtable)listTerceros.get(i);
                                     String code = (String) tabla.get("codigo");
                                     String desc = (String) tabla.get("descripcion");%>
                                     <option  value='<%= code %>'  > <%=desc %> </option>
                               <% }%>                                         
                              </select>
                        </td>
                   </tr>
                   
                   <tr class="fila">                   
                        <td >&nbsp Fecha Corte:  </td>
                        <td >
                        
                               <table cellpadding='0' cellspacing='0' width='100%'>
                                  <tr>
                                     <td width='50%'>

                                         <input type='text' class="textbox"  name='fecha'  value='<%= hoy %>' size='15'    readonly      title='Fecha entrega del dinero'>                                                        
                                         <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fecha);return false;" HIDEFOCUS>
                                            <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha">
                                         </a>
                                         
                                     </td>                                     
                                  </tr>
                               </table>
                        
                        </td>
                   </tr>
                   
                   <input type='hidden' name='cantidad' value='1'>
                   
                   
                   
              </table>
              
        </td>
     </tr>
   </table>
   </form>
   
   
   
   
  <!-- Botones --> 
   <p>
      <img src='<%=BASEURL%>/images/botones/aceptar.gif'    style="cursor:hand"  title='Consulta Coutas....'     name='i_crear'   onclick="buscarCuotas(formulario)"  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
      <img src='<%=BASEURL%>/images/botones/salir.gif'      style="cursor:hand"  title='Salir...'               name='i_salir'   onclick='parent.close();'           onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
  </p>
  
  
</div>
   <iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>
