<!--
     - Author(s)       :      MARIO FONTALVO SOLANO
     - Date            :      12/02/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)
     - Description: Listado de prestamos
 --%>

<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>
<%
    String Mensaje     = (request.getParameter("Mensaje")==null)?"":request.getParameter("Mensaje");
    String TipoListado = (request.getParameter("TipoListado")==null)?"":request.getParameter("TipoListado");
   /**
    * Perfiles validos para la vista
    * AP: aprobacion
    * CO: consulta
    */
    String Perfil  = (String) session.getAttribute("PerfilPrestamo");
    if (Perfil==null) Perfil = "AP";
%>
<html>
<head>
    <title>Listado de Prestamos</title>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script src="<%= BASEURL %>/js/validacionesPrestamos.js"></script>
    <script>
       function seleccionar(obj){
         var tr = obj.parentNode.parentNode;
         if (obj.checked)
           tr.className = 'filaseleccion';
         else
           tr.className = (tr.rowIndex%2==0?'filagris':'filaazul');
       }
    </script>
</head>
<body onresize="redimensionar()" onload = "redimensionar(); ">


<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listado de Prestamos"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
 

<center>
  <br>
  <form name="formulario" action="<%= CONTROLLER %>?estado=Consulta&accion=Prestamos" method="post">
  <table border="2">
  <tr>
    <td >
      <table  width="100%" class="tablaInferior" >
      <tr class="barratitulo">
   	    <td colspan="<%= (Perfil.equals("AP")?14:13) %> ">
	  		<table width="100%" cellpadding='0' cellspacing='0'>
			<tr>
				<td class="subtitulo1">&nbsp;Resultados de la Busqueda de Prestamos </td>
				<td width="52%"><img src="<%= BASEURL %>/images/titulo.gif" width="33" height="20"></td>			
			</tr>
			</table>
   	     </td>
      </tr>
      </table>
      <table width="100%" class="tablaInferior" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
      <tr class="tblTitulo">
        <% if (Perfil.equals("AP")) { %> <td width="30" >&nbsp;Sel</td>  <% } %>
        <td width="40" >&nbsp;Amort</td>
        <td width="40" >&nbsp;ID</td>
        <td width="60" >&nbsp;Tercero</td>
        <td width="250">&nbsp;Beneficiario</td>
        <td width="140" >&nbsp;Tipo Prestamo</td>
        <td width="100">Entrega Dinero</td>
        <td width="100">&nbsp;Valor</td>
        <td width="100" >&nbsp;Saldo</td>        
        <td width="50" >&nbsp;Cuotas</td>
        <td width="50" >&nbsp;Tasa</td>
        <td width="80" >&nbsp;Clasificacion</td>
		<td width="80" >&nbsp;Placa</td>
        
      </tr>
		
   	  <%  List lista = model.PrestamoSvc.getListaPrestamos();
	      for (int i = 0; i < lista.size(); i++){
                Prestamo pt = (Prestamo) lista.get(i);
                String style = (i%2==0?"filagris":"filaazul");
	  %>
		  <tr class="<%= style %>" >
		        <% if (Perfil.equals("AP")) { %>
			<td class='bordereporte' align='center'><input type='checkbox' name='LOV' value='<%= pt.getDistrito() + "~" + pt.getId() + "~" + i %>' <%= (pt.getAprobado().equals("S")?"checked disabled":"") %> onclick='seleccionar(this);' ></td>
			<% } %>
			<td class='bordereporte' align='center'><img src='<%=BASEURL%>/images/botones/iconos/clasificar.gif' style="cursor:hand"  title='Vista de Amortizaciones....' width="22" onclick=' viewAmortizaciones(<%= i %>) '></td>			
			<td class='bordereporte' align='center'><%= pt.getId()       %></td>
			<td class='bordereporte' nowrap ><%= pt.getTerceroName()            %></td>
			<td class='bordereporte' ><%= pt.getBeneficiarioName()       %></td>
			<td class='bordereporte' ><%= pt.getTipoPrestamoName() %></td>
			<td class='bordereporte' align='center'><%= pt.getFechaEntregaDinero()              %></td>
			<td class='bordereporte' align='right' ><%= UtilFinanzas.customFormat(  pt.getMonto()  )%></td>
			<td class='bordereporte' align='right' ><%= UtilFinanzas.customFormat(  pt.getSaldo_prestamo() )%></td>
			<td class='bordereporte' align='right' ><%= pt.getCuotas()                          %></td>
			<td class='bordereporte' align='right' ><%= UtilFinanzas.customFormat2(pt.getTasa())%>&nbsp;%</td>			
			<td class='bordereporte' ><%= pt.getClasificacionName()      %></td>
			<td class='bordereporte' ><%= pt.getPlaca()                  %></td>			
		  </tr>
	   <% } %>
	   <% Prestamo pt = model.PrestamoSvc.getTotalesPr(); 
              if (pt!=null){ %>
		  <tr class="filaresaltada" >
			<td class='bordereporte' align='center' colspan='<%= (Perfil.equals("AP")?7:6) %>' >TOTALES</td>			
			<td class='bordereporte' align='right' ><%= UtilFinanzas.customFormat( pt.getMonto() ) %></td>
			<td class='bordereporte' align='right' ><%= UtilFinanzas.customFormat(  pt.getSaldo_prestamo() )%></td>
			<td class='bordereporte' align='right' >&nbsp;</td>
			<td class='bordereporte' align='right' >&nbsp;</td>			
			<td class='bordereporte' >&nbsp;</td>
			<td class='bordereporte' >&nbsp;</td>
		  </tr>	 
            <% } %>
		
    </table>
	</td>
  </tr>
</table>
<br>

<% if (Perfil.equals("AP")) { %>
<img src='<%=BASEURL%>/images/botones/aceptar.gif'     name='i_aceptar' id="i_aceptar"    style=" cursor:hand"  title='Aprobar Prestamos....'   onclick="if (validarListaSeleccion(formulario)) { formulario.Opcion.value='AprobarPrestamo'; formulario.submit(); }else { alert ('Seleccione primero los prestamos para aprobarlos'); } "     onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
&nbsp;
<img src='<%=BASEURL%>/images/botones/eliminar.gif'     name='i_anular' id="i_anular"    style=" cursor:hand"  title='Eliminar Prestamos....'   onclick="if (validarListaSeleccion(formulario)) { formulario.Opcion.value='EliminarPrestamo'; formulario.submit(); }else { alert ('Seleccione primero los prestamos para eliminarlos'); } "     onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
&nbsp;
<% } %>
<img src='<%=BASEURL%>/images/botones/exportarExcel.gif' style='cursor:hand'    title='Exportar Excel...'      name='i_exportar'    onclick="descargar('002');"     onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
<img src='<%=BASEURL%>/images/botones/regresar.gif'      style='cursor:hand'    title='Regresar...'            name='i_regresar'    onclick="window.location.href='<%= BASEURL %>/jsp/cxpagar/prestamos/consultas/ConsultaPrestamos.jsp'" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
<img src='<%=BASEURL%>/images/botones/restablecer.gif'   style='cursor:hand'    title='Actualizar listado...'  name='i_actualizar'  onclick="window.location.href='<%= CONTROLLER %>?estado=Consulta&accion=Prestamos&Opcion=ActualizarListadoPrestamos'; " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
<img src='<%=BASEURL%>/images/botones/salir.gif'         style='cursor:hand'    title='Salir...'               name='i_salir' onclick="window.close();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
<input type="hidden" name="TipoListado" value="<%= TipoListado %>">
<input type="hidden" name="Opcion" value="">
</form>


<% if (Perfil.equals("AP")) { %>
<span class='informacion'>Para aprobar o eliminar los prestamos por favor seleccione de la lista y haga click sobre el boton Aceptar o Eliminar segun lo que desee hacer, <br>si desea conocer las amortizaciones de haga click sobre el icono respectivo al prestamo </span>
<%} %>

</center>

   <!-- Mensaje -->
   
   <% if( ! Mensaje.equals("") ){%>
          <br><br>
          <table border="2" align="center">
              <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                  <tr>
                    <td width="500" align="center" class="mensajes"><%= Mensaje %></td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="58">&nbsp; </td>
                  </tr>
                </table></td>
              </tr>
            </table>
   <%}%>
   
   
 <iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins_24.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
 
</div>
</body>
</html>

