<!--
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      09/02/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Formulario de captura para generar Prestamos
 --%>

<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>

<html>
<head>
         <title>Crear Prestamo</title>
         <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">          
		 <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">                   
         <script>
              var BASEURL    = '<%= BASEURL    %>';
              var CONTROLLER = '<%= CONTROLLER %>';
         </script>
         <script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>    
		 <script type='text/javascript' src="<%= BASEURL %>/js/script.js"></script>       
		 <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
         <script type='text/javascript' src="<%= BASEURL %>/js/validacionesPrestamos2.js"> </script>         
         <script>
            function sendPrestamo(theForm,opcion){ 
                 var parameter  = theForm.action + '&evento=' + opcion; 
                 theForm.monto.value = theForm.monto.value.replace(/,/g,'');
				 theForm.cuota_inicial.value = theForm.cuota_inicial.value.replace(/,/g,'');
				 theForm.cuota_monitoreo.value = theForm.cuota_monitoreo.value.replace(/,/g,'');
                 
                 if (theForm.tipoPrestamo.selectedIndex!=-1)
                 	theForm.tipoPrestamoName.value = theForm.tipoPrestamo[theForm.tipoPrestamo.selectedIndex].text;
				 else	
  				    theForm.tipoPrestamoName.value = '';
               
                 for(i=0;i<theForm.length;i++) {
				      if ( !theForm.elements[i].disabled ) {
	                      parameter += '&'+ theForm.elements[i].name + '=' +  theForm.elements[i].value;
					  }
				 }
                                  
                  if( opcion=='CANCEL' ){
                      window.location.href = parameter;
                  }
                  if(opcion=='SAVE'  ||  opcion=='VIEW' ){
                  
                       if(  theForm.Beneficiario.value==''  ||  theForm.tercero.value  ==''  ||  theForm.monto.value==''   ||   theForm.periodo.value ==''  || 
                            theForm.tipoPrestamo.value==''  ||  theForm.tasa.value     ==''  ||  theForm.demora.value==''  ||   theForm.entrega.value ==''  ||  theForm.primerPago.value==''  || 
                            theForm.aprobado.value    ==''  ||  theForm.tasa.value     =='0' ||  theForm.tasa.value=='0.0' ||   theForm.tasa.value    =='0.'||  theForm.tasa.value     =='.0' ||
							((theForm.cuotas.value    ==''  ||  theForm.cuotas.value=='0') && theForm.tipoPrestamo.value!='AP') ||
							( theForm.tipoPrestamo.value=='EQ' &&  (theForm.placa.value=='' || theForm.tipoEquipovalue=='' || theForm.cobrar_financiacion.value == '0' || theForm.cobrar_monitoreo.value == '0')  )
						 )
                            alert('Deberá llenar los campos del prestamo');
                       else{  
                             if( opcion=='SAVE' )
                                  window.location.href = parameter;
                             if( opcion=='VIEW' )
                                  var win = newWindow(parameter,'Amortizacion');
                       }
                  }
                  formatear(theForm.monto);
				  if (theForm.tipoPrestamo.value=='EQ'){
					  formatear(theForm.cuota_inicial);
					  formatear(theForm.cuota_monitoreo);
				  }
            }
            
			
			function validarPlaca(theForm){ 
                 var parameter  = theForm.action + '&evento=COMPLETAR'; 
                 theForm.monto.value = theForm.monto.value.replace(/,/g,'');
				 theForm.cuota_inicial.value = theForm.cuota_inicial.value.replace(/,/g,'');
				 theForm.cuota_monitoreo.value = theForm.cuota_monitoreo.value.replace(/,/g,'');
                 
				 if (theForm.tipoPrestamo.selectedIndex!=-1)
                 	theForm.tipoPrestamoName.value = theForm.tipoPrestamo[theForm.tipoPrestamo.selectedIndex].text;
				 else	
  				    theForm.tipoPrestamoName.value = '';
               
                 for(i=0;i<theForm.length;i++) {
				      if ( !theForm.elements[i].disabled ) {
	                      parameter += '&'+ theForm.elements[i].name + '=' +  theForm.elements[i].value;
					  }
				 }                                  
                 window.location.href = parameter;
				 
                 //formatear(theForm.monto);
				 /*if (theForm.monto.tipoPrestamo.value=='EQ'){
					  formatear(theForm.cuota_inicial);
					  formatear(theForm.cuota_monitoreo);
				 }*/
            }
            
         </script>
         
 </head>
    
<body onresize="redimensionar()" onload = "redimensionar(); focus(formulario.Beneficiario);">


<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Prestamos"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
 
<center>
 
  <!-- Cargamos las lista para los combos -->
  <% 
     Usuario  User              = (Usuario) session.getAttribute("Usuario");
     Prestamo prestamo          = model.PrestamoSvc.getPrestamo();
     List     listTerceros      = model.PrestamoSvc.getTerceros();
     List     listTipoPrestamo  = model.PrestamoSvc.getTipoPrestamo();
     List     listPeriodos      = model.PrestamoSvc.getPeriodos();
     List     listConceptos     = model.PrestamoSvc.getConceptos();
     List     listClasificacion = model.PrestamoSvc.getClasificaciones();
	 List     listEquipos       = model.PrestamoSvc.getEquipos();
     String   msj               = (request.getParameter("msj")==null)?"":request.getParameter("msj");
  %>
     
 
  
   <form  method='post' name='formulario' action="<%= CONTROLLER %>?estado=Crear&accion=Prestamo">
   <table width="700"  border="2" align="center">
     <tr>
      <td>
      
            <table  width='100%' class='tablaInferior' cellpadding='0' cellspacing='0'>     
 
                   <tr>
                      <td>
                          <table width='100%'  class="barratitulo" cellpadding='0' cellspacing='0'>
                             <tr class="fila">
                                    <td align="left" width='50%' class="subtitulo1" nowrap>&nbsp;&nbsp; CREACION DE PRESTAMO</td>
                                    <td align="left" width='*' ><img src="<%=BASEURL%>/images/titulo.gif"></td>
                             </tr>
                          </table>
                      </td>
                   </tr>   
                  
				  
				 	<tr><td></td></tr>
					
					
					 <tr >
                      <td>
                          <table class="tablaInferior" width='100%' >
                              <tr class="fila">
                                        <td width='15%'>&nbsp;Placa rel. </td>
                                        <td width='85%'>
											<input type='text'   name='placa'  value='<%= prestamo.getPlaca()     %>' style="width:92" title='Placa relacionada al prestamo'   ><!--onChange="validarPlaca(this.form)"-->
                                        </td>                                        
                                        
                              </tr>
                          </table>
                      </td>
                   </tr>
                   <tr>
                      <td>
                          <table class="tablaInferior" width='100%' >
                             <tr class="fila">
                                    <td width='15%'>&nbsp;Beneficiario               </td>
                                    <td width='55%'>
                                        <input type='text'   name='Beneficiario'                            value='<%= prestamo.getBeneficiario()     %>' style="width:25%">
                                        <input type='text'   name='NombreBeneficiario' readonly='readonly'  value='<%= prestamo.getBeneficiarioName() %>' style="width:60%"> &nbsp;
                                        
                                        <img src='<%=BASEURL%>/images/botones/iconos/buscar.gif'  width="16"  style="cursor:hand"  title='Seleccionar Beneficiario'   name='i_load'      onclick="encontrarProveedor();"    >
                                    </td>
                                    
                                    <td width='10%'>&nbsp;Tercero             </td>
                                    <td width='20%'>                                       
                                         <select name='tercero' style='width:100%'   title='Tercero'>
                                           <% /*for(int i=0; i< listTerceros.size(); i++){
                                                 Hashtable tabla = (Hashtable)listTerceros.get(i);
                                                 String code = (String) tabla.get("codigo");
                                                 String desc = (String) tabla.get("descripcion");
                                                 String sele = (code.equals( prestamo.getTercero() )?"selected='selected'":"");*/%>
                                                 <option  value="FINTRA"   <%/*=sele*/%>   > <%/*=desc*/ %> FINTRA </option>
                                           <%// }%>                                         
                                          </select>
                                    </td>
                                    
                             </tr>
                           </table>
                      </td>
                   </tr>
                  
                  <tr>
                      <td>
                          <table class="tablaInferior" width='100%' >
                              <tr class="fila">                          
                                        <td width='15%'>&nbsp;Tipo Prestamo</td>
                                        <td width='31%' id="celda_tipoPrestamo">   
                                           <select name='tipoPrestamo' style='width:200'   title='Frecuencia de Pago' onChange=" seleccionarPrestamoEquipo(this.form); ">
                                           <% for(int i=0; i< listTipoPrestamo.size(); i++){
                                                 Hashtable tabla = (Hashtable)listTipoPrestamo.get(i);
                                                 String code = (String) tabla.get("codigo");
                                                 String desc = (String) tabla.get("descripcion");
                                                 String sele = (code.equals( prestamo.getTipoPrestamo() )?" selected ":"");
                                                 if( !code.equals("POC")  ){%>
                                                        <option value='<%= code %>'   <%= sele %>  > <%=desc %> </option>
                                           <%    } 
                                            }%>                                         
                                          </select>
                                        
                                        </td>                                    
                                        <td width='8%' id="celda_tasa0">&nbsp;Tasa</td>
                                        <td width='16%' id="celda_tasa1">
                                             <input type='text' name='tasa'    onfocus='this.select()'  onKeyPress="jscript: soloDigitos(event, 'decOK');"    onBlur='formatear(this)'  value='<%= prestamo.getTasa() %>'          dir='rtl'  maxlength='5' style='width:100' title='Tasa de Interes'>
                                        </td>                                    
                                        <td width='15%' id="celda_tasa2">&nbsp; Int. Demora  </td>
                                        <td width='15%' id="celda_tasa3">
                                             <input type='text' name='demora'  onkeyup='formatear(this)' onfocus='this.select()'  value='<%= prestamo.getInteresDemora() %>' dir='rtl'  maxlength='5' style='width:100' title='Interes demora'>
                                        </td>
                              </tr>
                          </table>
                      </td>
                   </tr>  

                   <tr style="display:none " id="fila_equipo">
                      <td>
                          <table class="tablaInferior" width='100%' >
                              <tr class="fila">
                                        <td width='15%'>&nbsp;Tipo Equipo </td>
                                        <td width='85%'>
											<select name='tipoEquipo' style='width:370'   title='Tipo de equipo' onChange=" loadDatosEquipo(this.form);">
											   <% for(int i=0; i< listEquipos.size(); i++){
													 Hashtable tabla = (Hashtable)listEquipos.get(i);
													 String code = (String) tabla.get("codigo");
													 String desc = (String) tabla.get("descripcion");
													 String dato = (String) tabla.get("dato");
													 String sele = (code.equals( prestamo.getEquipo() )?" selected ":"");%>
												     <option value='<%= code %>'   <%= sele %>  datos='<%= dato.trim() %>'> <%=desc %> </option>
											   <% } %>                                         
											  </select>                                           
                                        </td>                                        
                                        
                              </tr>
                          </table>
                      </td>
                   </tr>
				   
				   


                     <tr>
                      <td>
                          <table class="tablaInferior" width='100%' >
                              <tr class="fila">
                                        <td width='15%'>&nbsp;Monto     </td>
                                        <td width='31%'>
											   <input type='text' name='monto'     onfocus='this.select()'  onKeyPress="jscript: soloDigitos(event, 'decNO');"    onBlur='formatear(this)'    value='<%= UtilFinanzas.customFormat2( prestamo.getMonto()) %>'  maxlength='13' style=' width:97%' dir='rtl' title='Total a Prestar'>
                                        </td>
                                        <td width='8%'>&nbsp;Cuotas     </td>
                                        <td width='16%'>
                                           <input type='text' name='cuotas'    onfocus='this.select()'  onKeyPress="jscript: soloDigitos(event, 'decNO');"    onBlur='formatear(this)'     value='<%= prestamo.getCuotas() %>'   maxlength='3' style='width:100;' dir='rtl'  title='Cantidad de cuotas de pagos' >
                                        </td>
                                        <td width='15%'>&nbsp;Periodo  </td>
                                        <td width='15%'>
                                            
                                            <select name='periodo' style='width:100%'   title='Periodos de Pago'>
                                               <% for(int i=0; i< listPeriodos.size(); i++){
                                                     Hashtable tabla = (Hashtable)listPeriodos.get(i);
                                                     String code = (String) tabla.get("referencia");
                                                     String desc = (String) tabla.get("descripcion");
                                                     String sele = (code.equals( String.valueOf( prestamo.getFrecuencias() ) )?"selected='selected'":"");%>
                                                     <option value='<%= code %>'   <%= sele %>   > <%=desc %> </option>
                                               <% }%>                                         
                                              </select>
                                              
                                              
                                          
                                        </td>
                              </tr>
                          </table>
                      </td>
                   </tr>
            
                   <tr style="display:none " id="fila_equipo_1">
                      <td>
                          <table class="tablaInferior" width='100%' >
                              <tr class="fila">
                                        <td >
										
										&nbsp;<span style="width:30% "><input type="checkbox" name="ck_ci" value="ok" checked onClick="  cuota_inicial.disabled = !this.checked;  ">Incluir Cuota Inicial </span>
											  <span style="width:20% "><input type='text'   name='cuota_inicial'  value='<%= UtilFinanzas.customFormat(prestamo.getCuotaInicial()) %>' style='width:100; text-align:right'    readonly      title='Cuota inicial de financiacion' ></span>
											  <span style="width:29% "><input type="checkbox" name="ck_cm" checked value="ok" onClick="  cuota_monitoreo.disabled = !this.checked; cobrar_monitoreo.disabled=cuota_monitoreo.disabled;  ">Incluir Cuota Monitoreo </span>
											  <span style="width:19% "><input type='text'   name='cuota_monitoreo'  value='<%= UtilFinanzas.customFormat(prestamo.getCuotaMonitoreo()) %>' style='width:100; text-align:right'    readonly      title='Cuota monitoreo'></span>
										</td>
                                        
                              </tr>
                          </table>
                      </td>
                   </tr>
				   
				   <tr style="display:none " id="fila_equipo_2">
                      <td>
                          <table class="tablaInferior" width='100%' >
                              <tr class="fila">
                                        <td >										
										&nbsp;Empezar a cobrar financiacion a partir de la <input type="text" name="cobrar_financiacion"   value="1" style=" width:40; text-align:center; " maxlength="2" onFocus="this.select();" onKeyPress="jscript: soloDigitos(event, 'decNO') " > cuota
										</td>                                        
                              </tr>
                          </table>
                      </td>
                   </tr>
				   
				   
				   <tr style="display:none " id="fila_equipo_3">
                      <td>
                          <table class="tablaInferior" width='100%' >
                              <tr class="fila">
                                        <td >&nbsp;Empezar a cobrar monitoreo a partir de la <input type="text" name="cobrar_monitoreo" id="cobrar_monitoreo"  value="1" style=" width:40; text-align:center; " maxlength="2" onFocus="this.select();" onKeyPress="jscript: soloDigitos('decOK') " > cuota</td>                                       
                              </tr>
                          </table>
                      </td>
                   </tr>
				   
				   
                   <tr>
                      <td>
                          <table class="tablaInferior" width='100%' >
                             <tr class="fila">
                                    <td width='15%' id="celda_entrega">&nbsp;Entrega Dinero        </td>
                                    <td width='31%'>
                                    
                                        <input type='text' class="textbox"  name='entrega'  value='<%=  prestamo.getFechaEntregaDinero() %>' style='width:70%'    readonly      title='Fecha entrega del dinero'>                                                        
                                        <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.entrega);return false;" HIDEFOCUS>
                                            <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha">
                                        </a>
                                        
                                    </td>
                                    <td width='24%'>&nbsp;Primera Cuota   </td>
                                    <td width='30%'>
                                        &nbsp;
                                        <input type='text' class="textbox"  name='primerPago'   value='<%=  prestamo.getFechaInicialCobro() %>'  style='width:70%'  readonly     title='Fecha del primer pago'>                                                        
                                        <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.primerPago);return false;" HIDEFOCUS>
                                            <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha">
                                        </a>
                                        
                                    </td>
                             </tr>
                           </table>
                      </td>
                   </tr>
                                    
                   
               
                  
                   <tr>
                      <td>
                              <table class="tablaInferior" width='100%' >
                                 <tr class="fila">
                                         <td width='15%'>&nbsp;Concepto  </td>
                                         <td width='31%'>
                                         
                                              <select name='concepto' style='width:100%'   title='Concepto de Prestamo'>
                                               <% for(int i=0; i< listConceptos.size(); i++){
                                                     Hashtable tabla = (Hashtable)listConceptos.get(i);
                                                     String code = (String) tabla.get("codigo");
                                                     String desc = (String) tabla.get("descripcion");
                                                     String sele = (code.equals( prestamo.getConcepto() )?"selected='selected'":"");
                                                     if( !code.equals("OC")  ){%>
                                                          <option value='<%= code %>'   <%= sele %>  > <%=desc %> </option>
                                                     <%}
                                                  }%>                                         
                                              </select>
                                             
                                         
                                         </td>
                                         
                                         <td width='24%'>&nbsp;Clasificación  </td>
                                         <td width='30%'>&nbsp;
                                         
                                              <select name='clasificacion' style='width:95%'   title='Clasificación del Prestamo'>
                                               <% for(int i=0; i< listClasificacion.size(); i++){
                                                     Hashtable tabla = (Hashtable)listClasificacion.get(i);
                                                     String code = (String) tabla.get("codigo");
                                                     String desc = (String) tabla.get("descripcion");
                                                     String sele = (code.equals( prestamo.getClasificación() )?"selected='selected'":""); %>
                                                     <option value='<%= code %>'   <%= sele %>  > <%=desc %> </option>
                                               <% }%>                                         
                                              </select>
                                             
                                         
                                         </td>
                                         
                                 </tr>
                              </table>
                      </td>
                  </tr>
                  
                  
                 
				   
                  
                  <tr  class="fila">
                      <td height='120'>
                            <table class="tablaInferior" width='100%' >
                                 <tr class="fila">
                                     <td width='100%'>&nbsp;Observación<br><br>
                                            <textarea name='observacion' rows='10'   class='textbox'  style='width:100%'><%= prestamo.getObservacion() %></textarea>
                                     </td>
                                 </tr>
                           </table>
                           <br>
                      </td>
                  </tr>
                  
                  
            </table>
      
       </td>
      </tr>
   </table>
   <input type='hidden' name='tipoPrestamoName'>
   <input type='hidden' value='<%= User.getDstrct() %>' name='distrito'>
   <input type='hidden' value='<%= User.getLogin()  %>' name='usuario'>
   <input type='hidden' value='N' name='aprobado'> 
   
   </form>
   
   
   
   <br> 
   
   <img src='<%=BASEURL%>/images/botones/aceptar.gif'    style="cursor:hand"  title='Crear Prestamo....'     name='i_crear'    onclick="sendPrestamo(formulario,'SAVE');"     onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
   <img src='<%=BASEURL%>/images/botones/detalles.gif'   style="cursor:hand"  title='Vista Amortización'     name='i_view'     onclick="sendPrestamo(formulario,'VIEW');"     onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
   <img src='<%=BASEURL%>/images/botones/cancelar.gif'   style="cursor:hand"  title='Limpiar.....'           name='i_cancel'   onclick="sendPrestamo(formulario,'CANCEL');"   onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
   <img src='<%=BASEURL%>/images/botones/salir.gif'      style="cursor:hand"   title='Salir...'               name='i_salir'    onclick='parent.close();'                      onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>



   <!-- Mensaje -->
   
   <% if( ! msj.equals("") ){%>
          <br><br>
          <table border="2" align="center">
              <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                  <tr>
                    <td width="500" align="center" class="mensajes"><%=msj%></td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="58">&nbsp; </td>
                  </tr>
                </table></td>
              </tr>
            </table>
   <%}%>
   
</div> 
   
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins_24.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>



</body>
</html>
<script>
	if ( formulario.tipoPrestamo.value == 'EQ' ){
		seleccionarPrestamoEquipo(formulario);
		//loadDatosEquipo(formulario);
		formulario.tipoEquipo.value= '<%= prestamo.getEquipo() %>';
	}
	
</script>
