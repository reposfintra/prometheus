
<!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      09/02/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Vista de tabla de Amorización
 --%>

<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>
<html>
<head>
     <title>Exportacion Prestamos</title>
     <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
     <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body>
<%
   String msg = (String) request.getAttribute("msg");
%> 
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Exportacion Prestamos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>
          
<br>
  <table border="2" align="center" width='450'>
  <tr>
      <td>
          <table  width='100%' class='tablaInferior' cellpadding='0' cellspacing='0'>
          <tr>
              <td>          
                  <table width='100%'  class="barratitulo" cellpadding='0' cellspacing='0'>
                  <tr class="fila">
                      <td align="left" width='70%' class="subtitulo1" nowrap>&nbsp;Resultado Exportacion Prestamo</td>
                      <td align="left" width='30%' ><img src="<%=BASEURL%>/images/titulo.gif"></td>
                  </tr>
                  </table>
              </td>
          </tr>
          <tr class='fila'>
            <td align=center>
            <br><%= msg %><br>&nbsp;
            </td>
          </tr>
          </table>
      </td>
  </tr>
  </table>
  
  <br>
  <img src='<%=BASEURL%>/images/botones/salir.gif'      style='cursor:hand'    title='Salir...'               name='i_salir'   onclick='parent.close();'        onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
  
  
</center>
</div>
</body>
</html>
