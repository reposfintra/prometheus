<!--
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      04/03/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Vista que busca la cuota de un determinado prestamo para realizar el ingreso
 --%>

 
<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>


<html>
<head>
         <title>Ingresos de Prestamos</title>
         <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
         <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>  
         <script type='text/javascript' src="<%= BASEURL %>/js/script.js"></script>
         <script type='text/javascript' src="<%= BASEURL %>/js/validacionesPrestamos.js"></script>
         
         <script>
            function searchCuota(theForm){
               if( theForm.prestamo.value==''  ||  isNaN( theForm.prestamo.value )    ){
                   alert('Deber� digitar el n�mero del prestamo , debe ser numerico.');
                   theForm.prestamo.focus();
               }
               else            
                  theForm.submit();
            }
            
            function cancelarCuotas(theForm){
                 var parameter = "";
                 var cont      = 0;
                 for (i=0;i<theForm.length;i++){
                      if (theForm.elements[i].type=='checkbox' ){
                          if (  theForm.elements[i].checked==true   &&  theForm.elements[i].name=='cuota' ){
                               parameter += "&"+ theForm.elements[i].name +"="+ theForm.elements[i].value;
                               cont++;
                          }
                      }
                      else
                          parameter += "&"+theForm.elements[i].name +"="+ theForm.elements[i].value;
                 }
                 if(cont==0)  alert('Deber� seleccionar la(s) cuota(s) que desee  cancelar...');
                 else  if( confirm('Desea cancelar la(s) ' + cont +' cuota(s) ? ') )
                          window.location.href = ( theForm.action + parameter );
            }
         </script>
         
         
         
</head>
<body onresize="redimensionar()" onload = "redimensionar(); ">

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Registrar ingresos"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>


<% List   listTerceros  = model.PrestamoSvc.getTerceros();
   String capIngresos   = request.getParameter("capturar");
   String msj           = request.getParameter("msj");   %>
   
   
   
<% if (  capIngresos==null ){%>
<form action="<%=CONTROLLER%>?estado=Ingreso&accion=Prestamo&evento=BUSCAR"  method='post' name='formulario'>
         <table border="2" align="center" width='350' >
              <tr>
                   <td>      
                            <table  width='100%' class='tablaInferior' >
                                   <tr>
                                      <td colspan='2' >
                                          <table width='100%'  class="barratitulo" cellpadding='0' cellspacing='0'>
                                             <tr class="fila">
                                                    <td align="left" width='60%' class="subtitulo1" nowrap> &nbsp; Buscar Cuota</td>
                                                    <td align="left" width='*' ><img src="<%=BASEURL%>/images/titulo.gif"></td>
                                             </tr>
                                          </table>
                                      </td>
                                   </tr>
                                   <tr class='fila'>
                                               <td width='30%'>&nbsp Tercero             </td>
                                               <td width='*'>                                       
                                                         <select name='tercero' style='width:55%'   title='Tercero'>
                                                           <% for(int i=0; i< listTerceros.size(); i++){
                                                                 Hashtable tabla = (Hashtable)listTerceros.get(i);
                                                                 String code = (String) tabla.get("codigo");
                                                                 String desc = (String) tabla.get("descripcion");%>
                                                                 <option  value='<%= code %>'   > <%=desc %> </option>
                                                           <% }%>                                         
                                                          </select>
                                               </td>
                                   </tr>                                   
                                   <tr class='fila'>
                                       <td >&nbsp No Prestamo    </td>
                                       <td >  
                                            <input type='text' name='prestamo' maxlength='5'>
                                       </td>
                                   </tr>
                           </table>
                   </td>
              </tr>
         </table>
 </form>
<p>
   <img src='<%=BASEURL%>/images/botones/buscar.gif'    style="cursor:hand"   title='Buscar...'     name='i_crear'   onclick="searchCuota(formulario)"    onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
   <img src='<%=BASEURL%>/images/botones/salir.gif'      style='cursor:hand'    title='Salir...'      name='i_salir'   onclick='parent.close();'            onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
</p>

<%}else{
    Prestamo pt = model.IngPrestamoSvc.getPrestamo();%>

<form action="<%=CONTROLLER%>?estado=Ingreso&accion=Prestamo&evento=SAVE"  method='post' name='formularioCuota'>
         <table border="2" align="center" width='750' >
              <tr>
                   <td>      
                        <table  width='100%' class='tablaInferior' >
                               <tr>
                                  <td colspan='6' >
                                      <table width='100%'  class="barratitulo" cellpadding='0' cellspacing='0'>
                                         <tr class="fila">
                                                <td align="left" width='60%' class="subtitulo1" nowrap> &nbsp Cancelar Cuota</td>
                                                <td align="left" width='*' ><img src="<%=BASEURL%>/images/titulo.gif"></td>
                                         </tr>
                                      </table>
                                  </td>
                               </tr>                               
                               <tr >
                                       <td  width='20%' class='fila' >&nbsp TERCERO    </td><td  width='20%' class='letra' > <%= pt.getTercero() %> </td>
                                       <td  width='15%' class='fila'  >&nbsp PRESTAMO   </td><td  width='20%' class='letra' > <%= pt.getId()      %> </td>
                               </tr>                               
                               <tr >
                                       <td  class='fila' >&nbsp BENEFICIARIO</td><td colspan='5'  class='letra'  ><%= pt.getBeneficiario()%> &nbsp&nbsp <%=(pt.getBeneficiarioName()!=null?pt.getBeneficiarioName():"") %></td>
                               </tr>                               
                               <tr>
                                     <td colspan='6'>
                                        <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                                               <tr class="tblTitulo">
                                                  <TH  >VALOR PRESTAMO  </TH>
                                                  <TH  >VALOR INTERESES </TH>
                                                  <TH  >CAPITAL DESC    </TH>
                                                  <TH  >INTERES DESC    </TH>
                                                  <TH  >SALDO CAPITAL   </TH>
                                                  <TH  >SALDO INTERES   </TH>
                                               </tr>
                                               
                                               <tr class='filaazul'>
                                                  <td align='right' class='bordereporte' ><%=  UtilFinanzas.customFormat2(pt.getMonto()     )%> </td>
                                                  <td align='right' class='bordereporte'><%=  UtilFinanzas.customFormat2(pt.getIntereses() )%> </td>
                                                  <td align='right' class='bordereporte'><%=  UtilFinanzas.customFormat2(pt.getCapitalDescontado()) %>   </td>
                                                  <td align='right' class='bordereporte'><%=  UtilFinanzas.customFormat2(pt.getInteresDescontado()) %>   </td>
                                                  <td align='right' class='bordereporte'><%=  UtilFinanzas.customFormat2(pt.getMonto()     - pt.getCapitalDescontado()) %>  </td>
                                                  <td align='right' class='bordereporte'><%=  UtilFinanzas.customFormat2(pt.getIntereses() - pt.getInteresDescontado()) %>  </td>
                                               </tr>
                                               
                                        </table>
                                     </td>
                               </tr>
                               <tr class='fila'>
                                       <td  colspan='6'> &nbsp CUOTA NO CANCELADAS</td>
                               </tr>              
                               <tr>
                                     <td colspan='6'>
                                        <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                                               <tr class="tblTitulo">
                                                  <TH  > <input type='checkbox' id='All' name='All' onclick="selectChecked(this.form, this)"  >  </TH>
                                                  <TH  >CUOTA           </TH>
                                                  <TH  >FECHA           </TH>
                                                  <TH  >MONTO           </TH>
                                                  <TH  >CAPITAL         </TH>
                                                  <TH  >INTERES         </TH>
                                                  <TH  >VALOR A PAGAR   </TH>
                                                  <TH  >SALDO           </TH>
                                               </tr>
                                               <% List listAmort = pt.getAmortizacion();
                                                  for(int i=0;i<listAmort.size();i++){
                                                     Amortizacion am = (Amortizacion)listAmort.get(i); %>
                                                     <tr class="<%= (i%2==0?"filagris":"filaazul") %>">
                                                          <td class='bordereporte' align='center'><input type='checkbox' name='cuota' value='<%=am.getItem()%>' onclick="selectChecked(this.form, this)">  </td>                                                           
                                                          <td class='bordereporte' align='center'><%= am.getItem()      %> </td>
                                                          <td class='bordereporte' align='center'><%= am.getFechaPago() %> </td>
                                                          <td class='bordereporte' align='right'><%= UtilFinanzas.customFormat2(am.getMonto()   )    %> </td>
                                                          <td class='bordereporte' align='right'><%= UtilFinanzas.customFormat2(am.getCapital() )    %> </td>
                                                          <td class='bordereporte' align='right'><%= UtilFinanzas.customFormat2(am.getInteres() )    %> </td>
                                                          <td class='bordereporte' align='right'><%= UtilFinanzas.customFormat2(am.getTotalAPagar() )%> </td>
                                                          <td class='bordereporte' align='right'><%= UtilFinanzas.customFormat2(am.getSaldo()   )    %>  </td>
                                                    </tr>
                                               <%}%>
                                               
                                               
                                               
                                        </table>
                                     </td>
                               </tr> 
                               
                               <tr class='fila'>
                                   <td >&nbsp Banco  Pago    </td>
                                   <td colspan='5' >   <input type='text' name='banco'     style="width:50%"> </td>
                               </tr>
                               
                               <tr  class='fila'>
                                   <td >&nbsp Sucursal Pago  </td>
                                   <td colspan='5'  >  <input type='text' name='sucursal' style="width:50%">  </td>
                               </tr>
                               
                               <tr  class='fila'>
                                   <td >&nbsp Cheque </td>
                                   <td  colspan='5' >  <input type='text' name='cheque'    style="width:20%"> </td>
                               </tr>
                               
                               <tr  class='fila'>
                                   <td>&nbsp Corrida   </td>
                                   <td colspan='5' >   <input type='text' name='corrida'   style="width:20%"> </td>
                               </tr>     

                        </table>
                   </td>
              </tr>
         </table>
    </form>
          
<p>
   <img src='<%=BASEURL%>/images/botones/aceptar.gif'    style="cursor:hand"   title='Grabar Cancelaciones...'     name='i_crear'   onclick="cancelarCuotas(formularioCuota)"                                                         onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
   <img src='<%=BASEURL%>/images/botones/regresar.gif'   style="cursor:hand"   title='Atras...'      name='i_atras'   onclick="window.location.href=('<%=CONTROLLER%>?estado=Ingreso&accion=Prestamo&evento=ATRAS');"   onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
   <img src='<%=BASEURL%>/images/botones/salir.gif'      style="cursor:hand"   title='Salir...'      name='i_salir'   onclick='parent.close();'                                                                         onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
</p>
<%}%>





   <!-- Mensaje -->
   
   <% if( msj!=null && ! msj.equals("") ){%>
          <table border="2" align="center">
              <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                  <tr>
                    <td width="500" align="center" class="mensajes"><%=msj%></td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="58">&nbsp; </td>
                  </tr>
                </table></td>
              </tr>
            </table>
   <%}%>
   
 



</div>

</body>
</html>
