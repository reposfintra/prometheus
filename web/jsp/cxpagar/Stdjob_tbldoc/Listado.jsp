<!--
- Autor : Ing. Juan Manuel Escandon Perez
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja los eventos de Listado, Activacion,
                Anulacion y Eliminacion de los registros
                a la tabla stdjob_tbldoc
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<% 
    model.stdjob_tbldocService.List();
    List Listado = model.stdjob_tbldocService.getList();
    String Mensaje       = (request.getParameter("Mensaje")!=null) ? request.getParameter("Mensaje") : "";
    if( Listado==null && Listado.size()==0){
        Mensaje = "No hay Registros";
    }	
%>

<html>
<head>
    <title>Listado General</title>    
    <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>  
    <link href="../css/estilostsp.css" rel='stylesheet'> 
    <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <%--Funciones Javascript --%>
    <script>
        function SelAll(){
            for(i=0;i<FormularioListado.length;i++)
                FormularioListado.elements[i].checked=FormularioListado.All.checked;
        }
        
        function ActAll(){
            FormularioListado.All.checked = true;
            for(i=1;i<FormularioListado.length;i++)	
                if (!FormularioListado.elements[i].checked){
                    FormularioListado.All.checked = false;
                break;
            }
        }      
    </script>
    <%--Fin Funciones Javascript --%>
</head>
<%-- Inicio Body --%>
<body>
    <% if(Listado!=null && Listado.size()>0) { %>
        <%-- Inicio Formulario --%>
        <form action="<%=CONTROLLER%>?estado=Stdjob_tbldoc&accion=Manager" method='post' name='FormularioListado'>
            <div align="center"><br>
            </div>  
            <table width="80%" border="2" align="center">
                <tr>
                    <td>
                        <table width="100%" align="center" class="tablaInferior">
                            <tr>
                                <td width="304"  height="24"  class="subtitulo1">
                                    <p align="left">Listado</p>
                                </td>
                                <td width="299"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                            </tr>
                        </table>
                        <table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
                            <tr class="tblTitulo">
                                <th width="41" >N&ordm;</th>
                                <th width="41" >
                                    <input type='checkbox' name='All' onClick='jscript: SelAll();'>
                                </th>
                                <th width='312' >ESTANDARD JOB</th>
                                <th width='108' >TIPO DE DOCUMENTO</th>
                            </tr>
                           <% int Cont = 1;
                              Iterator it2 = Listado.iterator();
                              while(it2.hasNext()){
                                Stdjob_tbldoc dat  = (Stdjob_tbldoc) it2.next();
				String est =  (Cont % 2 == 0 ) ? "filaazul" : "filagris";
				String Estilo = (dat.getReg_status().equals("A")) ? "filaseleccion" : est;
                               %>
                            <tr class='<%= Estilo %>' align="center">
                                <td class="bordereporte"><%= Cont++ %></td>
                                <td class="bordereporte"><input type='checkbox' name='LOV' value='<%= dat.getDocument_type()+"/"+dat.getStd_job_no() %>' onClick='jscript: ActAll();'></td>
                                <td class="bordereporte"><%=dat.getStd_job_no()%></td>
                                <td class="bordereporte"><%= dat.getDocument_type() %> </td>
                            </tr>
                            <% } %>
                        </table>
                    </td>
                </tr>
            </table>
            <div align="center">
                <p>
                    <br>
                    <input type='hidden' name='Opcion'/>
                    <img src="<%=BASEURL%>/images/botones/anular.gif"  name="imganular" onClick="FormularioListado.Opcion.value='Anular';FormularioListado.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> <img src="<%=BASEURL%>/images/botones/eliminar.gif"  name="imgeliminar" onClick="FormularioListado.Opcion.value='Eliminar';FormularioListado.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> <img src="<%=BASEURL%>/images/botones/restablecer.gif"  name="imgrestablecer" onClick="FormularioListado.Opcion.value='Activar';FormularioListado.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
                </p>
                <table width="80%" align="center">
                    <tr align="left">
                        <td>
                            <p align="left"><a style="cursor:hand" class="Simulacion_Hiper" onClick="window.close();">Cerrar</a> <br></p>
                        </td>
                    </tr>
                </table>
            </div>
        </form>

        <center class='comentario'>
        
        <%--Mensaje de informacion--%>
        <%if(!Mensaje.equals("")){%>
            <p>
                <table border="2" align="center">
                    <tr>
                        <td>
                            <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                <tr>
                                    <td width="229" align="center" class="mensajes"><%= Mensaje %></td>
                                    <td width="29" background="<%= BASEURL %>/images/cuadronaranja.JPG">&nbsp;</td>
                                    <td width="58">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </p>
        <% } %>
        </center>

    <%  } %>
    </body>
</html>
