<!--
- Autor : Ing. Juan Manuel Escandon Perez
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el ingreso de los registros
                a la tabla stdjob_tbldoc
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<%
  String Mensaje       = (request.getParameter("Mensaje")!=null) ? request.getParameter("Mensaje") : "";
%>

<html>
    <head>
        <title>Standart</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>  
        <link href="../css/estilostsp.css" rel='stylesheet'> 
        <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">        
        <script language="javascript" src="<%= BASEURL %>/js/validar.js"></script>  
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        
        <%--Funciones Javascript --%>
        <script>
            function validar(form){
                if (form.Opcion.value == 'Guardar' || form.Opcion.value == 'Modificar'){
                    if (form.codigo.value == ''){
                        alert('Defina el codigo para poder continuar...')
                        return false;
                    }
                    if (form.descripcion.value == ''){
                        alert('Defina el descripcion para poder continuar...')
                        return false;
                    }
                }
                return true;
            }
            
            function SelAll(){
                for(i=0 ; i < FormularioListado.length ; i++)
                    FormularioListado.elements[i].checked=FormularioListado.All.checked;
            }
            
            function ActAll(){
                FormularioListado.All.checked = true;
                for(i=1 ; i < FormularioListado.length ; i++)	
                    if (!FormularioListado.elements[i].checked){
                        FormularioListado.All.checked = false;
                    break;
                }
            }      
    
            function validarlistado(form){
                for(i=1 ; i < FormularioListado.length ; i++)	
                    if (FormularioListado.elements[i].checked)
                        return true;
                alert('Por favor seleccione un registro para poder continuar');
                return false;
            }    
	
            /*JuanManuel  04-11-05*/
            function verificarTecla5(e) {
                var key = (isIE) ? window.event.keyCode : e.which;                
                var isNum = (key == 13) ? false:true;
                if(!isNum){
                    buscarClient5();
                }
                return (isNum);
            }


            function buscarDestinos5(){            
                window.location='<%= CONTROLLER %>?estado=Buscar&accion=StandardColpapel&op=2&cliente='+form1.clienteR.value+'&origen='+form1.ciudadOri.value+'&standart=kk'; 
            }
            
            function buscarStandard5(){            
                window.location='<%= CONTROLLER %>?estado=Buscar&accion=StandardColpapel&op=3&cliente='+form1.clienteR.value+'&origen='+form1.ciudadOri.value+'&destino='+form1.ciudadDest.value+'&standart=kk'; 
            }
            
            function irADespacho5(){            
                window.location='colpapel/segundaDespacho.jsp?'; 
            }
            
            function buscarClient5(){
                form1.click_buscar.value="ok";
                if(form1.cliente.value.length < 6){
                    var tamano = 6-form1.cliente.value.length;
                    var ceros='';
                    i =1;
                    while(i <= tamano){
                        ceros = ceros + '0';
                        i++;
                    }
                    form1.cliente.value = ceros +form1.cliente.value;
                    window.location='<%= CONTROLLER %>?estado=Buscar&accion=StandardColpapel&op=1&cliente='+form1.cliente.value+'&standart=kk';
                }else if(form1.cliente.value!=''){
                    window.location='<%= CONTROLLER %>?estado=Buscar&accion=StandardColpapel&op=1&cliente='+form1.cliente.value+'&standart=kk';
                }else{
                    alert('Escriba el codigo del cliente o el Standard correcto');
                }
            }	  
        </script>
        <%--Fin de funciones Javascript--%>
    </head>
    <%-- Inicio Body --%>
    <body>
        <%-- Inicio Formulario --%>
        <FORM action="<%= CONTROLLER %>?estado=Stdjob_tbldoc&accion=Manager" method='POST'  name='form1' id="form1">
            <%--Tabla exterior--%>
            <table width='506' align='center' border='2'>
                <tr>
                    <td width="494">
                        <%--Tabla titulo--%>
                        <table width="100%" class="tablaInferior" align="center">
                            <tr>
                                <td width="48%" class="subtitulo1">&nbsp;Informacion </td>
                                <td width="52%" class="barratitulo"><img src="<%= BASEURL %>/images/titulo.gif" width="33" height="20"></td>
                            </tr>
                        </table>   
                        <%--Fin tabla titulo--%>
                        <%--Fin tabla interior--%>
                        <table width="100%" class="tablaInferior" align="center">
                            <tr class="fila">
                                <td width='47%'>&nbsp Standart/Cliente  :</td>
                                <td width="53%">
                                    <p>
                                        <span class="Simulacion_Hiper" style="cursor:hand " onClick="window.open('<%= BASEURL %>/consultas/consultasClientes.jsp','','HEIGHT=200,WIDTH=600,SCROLLBARS=YES,RESIZABLE=YES')">
                                            Consultar clientes...
                                        </span>
                                    </p>          
                                    &nbsp;<input name="cliente" type="text" id="cliente" maxlength="6" onKeyPress="return verificarTecla5(event);" class="textbox">
                                    <img src="<%= BASEURL %>/images/botones/iconos/lupa.gif"  name="imglupa" width="20" height="20" style="cursor:hand" onClick="buscarClient5();">
                                    <%--Inicializacion de Variables Ocultas--%>
                                    <input name="clienteR" type="hidden" id="clienteR" value="<%= request.getParameter("cliente") %>">
                                    <input name="standard_nom" type="hidden" id="standard_nom" value="<%= (String)request.getAttribute("std") %>">
                                    <input name="remitentes" type="hidden" id="remitentes" value=" ">
                                    <input name="docinterno" type="hidden" id="docinterno" value=" ">
                                    <input name="fechadesp" type="hidden" id="fechadesp" value=" ">
                                    <input name="destinatarios" type="hidden" id="destinatarios" value=" ">
                                    <input name="trailer" type="hidden" id="trailer" value=" ">
                                    <input name="conductor" type="hidden" id="conductor" value=" ">
                                    <input name="ruta" type="hidden" id="ruta" value=" ">
                                    <input name="toneladas" type="hidden" id="toneladas" value=" ">
                                    <input name="valorpla" type="hidden" id="valorpla3" value=" ">
                                    <input name="anticipo" type="hidden" id="anticipo" value=" ">
                                    <input type="hidden" name="hiddenField7">
                                    <input name="precintos" type="hidden" id="precintos" value=" ">
                                    <input name="observacion" type="hidden" id="observacion" value=" ">
                                    <input name="click_buscar" type="hidden" id="click_buscar">
                                    <br>
                                    <%if(request.getAttribute("cliente")!=null){%>
                                        <input name="valorpla" type="hidden" id="valorpla">
                                        <table width="100%" border="0" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" class="fila">
                                            <tr>
                                                <td>
                                                    <strong><%= (String)request.getAttribute("cliente") %></strong>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="27"><strong>Agencia Due&ntilde;a del Cliente : <%= (String)request.getAttribute("agency") %></strong></td>
                                            </tr>
                                        </table>
                                    <%}%>
                                </td>                        
                           </tr>
                           <% if(request.getAttribute("std") == null){%>
                            <tr class="fila">
                                <td>&nbsp; Ruta</td>
                                <td>
                                    <table width="100%" border="0" align="center" cellpadding="3" cellspacing="2" bordercolor="#CCCCCC" class="Letras">      
                                        <tr class="fila">
                                            <td width="86"><strong>Origen</strong></td>
                                            <td width="371">
                                                <% TreeMap ciudades = model.stdjobdetselService.getCiudadesOri(); 
						   String corigen="";
						   if(request.getParameter("origen") != null){
                                                        corigen = request.getParameter("origen");
						   }
						   String cdest="";
						   if(request.getParameter("destino") != null){
                                                        cdest = request.getParameter("destino");
                                                   }%>
                                                    <input:select name="ciudadOri" options="<%= ciudades %>" attributesText="style='width:100%;' onChange='buscarDestinos5()'; class='listmenu'" default='<%= corigen %>'/> 
                                            </td>
                                        </tr>      	
                                        <tr class="fila">
                                            <td><strong>Destino</strong></td>
                                            <td>
                                                <% TreeMap ciudadesDest = model.stdjobdetselService.getCiudadesDest(); %>
                                                <input:select name="ciudadDest" options="<%= ciudadesDest %>" attributesText="style='width:100%;' class='listmenu'"  default='<%= cdest %>'/> 
                                            </td>
                                        </tr>			
                                        <%if(ciudadesDest.size()>0){%>
                                    </table>
                                </td>
                            </tr>
                            <tr class="fila" align="center">
                                <td colspan="2"><img src="<%= BASEURL %>/images/botones/buscar.gif"  name="imgbuscar" onClick="buscarStandard5();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
                            </tr>			
                                        <% } %>
                                        <% if(request.getAttribute("ok")!=null){ %>
                                <tr class="fila">
                                    <td>&nbsp Estandart Job :</td>
                                    <td><% TreeMap stdjob = model.stdjobdetselService.getStdjobTree(); %>
                                        <input:select name="standard" options="<%= stdjob %>" attributesText="style='width:100%;' class='listmenu'"/> 
                                    </td>				
                                </tr>
                                <tr class="fila">
                                    <td width='47%'>&nbsp Tipo de documento :</td>
                                    <td>
                                        <input name='document_type' type='text' class="textbox" id="document_type"style='width:100%;' size="15" maxlength='15'>
                                    </td>
                                </tr>
                                <tr class="fila" align="center">
                                    <td colspan="2">
                                        <img src="<%= BASEURL %>/images/botones/aceptar.gif"  name="imgaceptar" onClick="form1.Opcion.value='Guardar';form1.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
                                    </td>
                                </tr>
                                        <% }
                            }else{%>
                                <tr class="fila">
                                    <td>&nbsp Estandart Job :</td> 
                                    <td><% TreeMap stdjob = model.stdjobdetselService.getStdjobTree(); %>
                                        <input:select name="standard" options="<%= stdjob %>" attributesText="style='width:100%;' class='listmenu'"/> 
                                    </td>				
                                </tr>
			   
                                <tr class="fila">
                                    <td>&nbsp Tipo de documento :</td>
                                    <td>
                                        <input name='document_type' type='text' class="textbox" id="document_type"style='width:100%;' size="15" maxlength='15'>
                                    </td>
                                </tr> 					
                                <tr class="fila" align="center">
                                    <td colspan="2">
                                        <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="form1.Opcion.value='Guardar';form1.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
                                    </td>
                                </tr> 
			   <%}%>                  
                        </table>
                    </td>
                </tr>
            </table>
            <br>
            <input type='hidden' name='Opcion'/> 

            <%-- Link listado de registros --%>
            <table width="507" align="center">
                <tr align="left">
                    <td>
                        <span class="Simulacion_Hiper" style="cursor:hand " onClick="window.open('<%=BASEURL%>/jsp/cxpagar/Stdjob_tbldoc/Listado.jsp','','HEIGHT=400,WIDTH=600,SCROLLBARS=YES,RESIZABLE=YES')">Listado general</span>
                    </td>
                </tr>
            </table>
            
            <center class='comentario'>

            <%--Mensaje de informacion--%>
            <%if(!Mensaje.equals("")){%>
                <p>
                    <table border="2" align="center">
                        <tr>
                            <td>
                                <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                    <tr>
                                        <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
                                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                        <td width="58">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </p>
            <%}%>

            </center>
        </FORM>
    </body>
</html>
