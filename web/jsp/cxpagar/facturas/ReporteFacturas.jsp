<!--
- Autor : Ing. Andr�s Maturana De La Cruz
- Date  : 27.06.2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la generaci�n del reporte de facturas de clientes.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="java.text.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import    ="com.tsp.finanzas.contab.model.beans.Hc" %>
<%@page import    ="com.tsp.finanzas.contab.model.DAO.GestionarHcsDAO" %>

<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
</script>
<title>Consulta de Facturas</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script>
<script>
function loadBancos(){
	forma.action = '<%= CONTROLLER%>?estado=ReporteFacturas&accion=Cargar&target=bancos';
	forma.submit();
}

function loadSucursales(){
	forma.action = '<%= CONTROLLER%>?estado=ReporteFacturas&accion=Cargar&target=sucursales';
	forma.submit();
}

function validarForma(){
	var sw = false;
	if( !validaFechas() )
		return false;
	for (i = 0; i < forma.elements.length; i++){
            if (forma.elements[i].value != "" && forma.elements[i].name!="text" && forma.elements[i].name!="login"
					&& forma.elements[i].name!="pagada" ){
                    sw = true;
            }
    }

	if( !sw ){
		alert('Debe seleccionar por lo menos un filtro.');
		return false;
	}

	// Proveedor o Banco o Sucursal
	if( forma.factura.value == ''
		
		&& ( forma.agencia.value != ''
		|| forma.banco.value != ''
		|| forma.sucursal.value != ''
		|| forma.proveedor.value != ''
		|| forma.tipo_doc.value != ''
		|| forma.ret_pago.value != '')
		&& forma.FechaI.value == ''
		&& forma.FechaF.value == ''  ) {
			alert('Debe seleccionar un rango de fechas.');
			return false;
	}









	//esta validacion es para que se ingresen ambas fechas( inicial y final ), ANTES si se ingresaba una sola fecha, era equivalente a select * from cxp
	if( (forma.FechaI.value != '' && forma.FechaF.value == '') || (forma.FechaI.value == '' && forma.FechaF.value != '')){
		alert('Debe ingresar fecha Inicial y Final.');
		return false;
	}

    return true;
}

function sameMonth(){
  	var fecha1 = document.forma.FechaI.value.replace(/-/g,'').replace('-','').substring(0, 6);
   	var fecha2 = document.forma.FechaF.value.replace(/-/g,'').replace('-','').substring(0, 6);;
	var fech1 = parseFloat(fecha1);
	var fech2 = parseFloat(fecha2);
	if( fech1!=fech2 ){
		alert('El per�odo debe estar comprendido en el mismo mes.');
		return false;
	} else {
		return true;
	}
}

function validaFechas(){
  	var fecha1 = document.forma.FechaI.value.replace(/-/g,'').replace('-','');
   	var fecha2 = document.forma.FechaF.value.replace(/-/g,'').replace('-','');
	var fech1 = parseFloat(fecha1);
	var fech2 = parseFloat(fecha2);
	if(fech1>fech2) {
   		alert('La fecha final debe ser mayor que la fecha inicial.');
		return (false);
    }
	return true;
}
</script>
<body onResize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:110px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Consulta de Facturas"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 109px; overflow: scroll;">
<%

	TreeMap bancos = model.servicioBanco.getBanco();
	TreeMap sucursales = model.servicioBanco.getSucursal();
	TreeMap agencias = (TreeMap) request.getAttribute("agencias_tm");
	TreeMap tdocs = new TreeMap();
	TreeMap si_no = new TreeMap();

	bancos.put( " Seleccione", "");
	sucursales.put( " Seleccione", "");
	agencias.put( " Seleccione", "");
	tdocs.put( " Seleccione", "");
	tdocs.put( "FACTURA", "FAP");
	tdocs.put( "NOTA CREDITO", "NC");
	tdocs.put( "NOTA DEBITO", "ND");
	si_no.put( " seleccione", "");
	si_no.put( "SI", "S");
	si_no.put( "NO", "N");

	Calendar FechaHoy = Calendar.getInstance();
	Date d = FechaHoy.getTime();
	SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd");

	List<Hc> lista_hc = new LinkedList<Hc>();
lista_hc= modelcontab.Hcsevice.ListarHc();
%>
<form name="forma" action='<%=CONTROLLER%>?estado=Reporte&accion=Facturas' id="forma" method="post">
  <table width="549"  border="2" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td colspan="2" ><table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
              <tr>
                <td width="56%" class="subtitulo1">&nbsp;Detalles</td>
                <td width="44%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
              </tr>
          </table></td>
        </tr>
        <tr class="fila">
          <td width="27%">Documento</td>
          <td width="73%" nowrap><input name="factura" type="text" class="textbox" id="factura"  value="<%= request.getAttribute("factura")!=null ? request.getAttribute("factura") : "" %>" maxlength="30"></td>
        </tr>
        <tr class="fila">
          <td>Tipo de Documento </td>
          <td nowrap><input:select name="tipo_doc" attributesText="class=textbox" options="<%=tdocs %>"/></td>
        </tr>
        <tr class="fila">
          <td>Proveedor</td>
          <td nowrap><input name="proveedor" type="text" class="textbox" id="proveedor"  value="<%= request.getAttribute("nit")!=null ? request.getAttribute("nit") : "" %>" maxlength="15"></td>
        </tr>
        <tr class="fila">
          <td>Agencia</td>
          <td nowrap><input:select name="agencia" attributesText="class=textbox onChange='loadBancos();'" options="<%=agencias %>"/></td>
        </tr>
        <tr class="fila">
          <td>Banco</td>
          <td nowrap><input:select name="banco" attributesText="class=textbox onChange='loadSucursales();'" options="<%=bancos %>"/></td>
        </tr>
        <tr class="fila">
          <td>Sucursal</td>
          <td nowrap><input:select name="sucursal" attributesText="class=textbox" options="<%=sucursales %>"/></td>
        </tr>
        <tr class="fila">
          <td>Fecha Inicial </td>
          <td nowrap>
            <input name="FechaI" type='text' class="textbox" id="FechaI" style='width:120' value='<%= request.getAttribute("fechai")!=null ? request.getAttribute("fechai") : "" %>' readonly>
            <span class="Letras"><img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onClick="if(self.gfPop)gfPop.fPopCalendar(document.forma.FechaI);return false;" HIDEFOCUS></span></td>
        </tr>
        <tr class="fila">
          <td>Fecha Final </td>
          <td nowrap>
            <input name="FechaF" type='text' class="textbox" id="FechaF" style='width:120' value='<%= request.getAttribute("fechaf")!=null ? request.getAttribute("fechaf") : "" %>' readonly>
            <span class="Letras"><img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onClick="if(self.gfPop)gfPop.fPopCalendar(document.forma.FechaF);return false;" HIDEFOCUS></span></td>
        </tr>
        <tr class="fila">
          <td>Hc </td>
          <td nowrap>
            <select name="hc" style="width:50; "  id="hc">
              <option value="">...</option>
              
              
              <%for(int i=0; i< lista_hc.size();i++){%>
              <option value="<%=lista_hc.get(i).getTableCode()%>"><%=lista_hc.get(i).getTableCode()%></option>
              <%}%>
              </select>
            
            </td>
        </tr>
        <tr valign="top" class="fila">
          <td>Aplica Retenci&oacute;n Pago </td>
          <td nowrap><input:select name="ret_pago" attributesText="class=textbox" options="<%=si_no %>"/></td>
        </tr>
        <tr class="fila">
          <td>&nbsp;</td>
          <td nowrap><select multiple size='8' class='select' style='width:95%' id='placasFiltro' name='placasFiltro'>
            </select></td>
        </tr>

		<tr class="fila">
          <td width="27%">Inicio</td>
          <td width="73%" nowrap><input name="iniciox" type="text" class="textbox" id="iniciox"  value="<%= request.getAttribute("iniciox")!=null ? request.getAttribute("iniciox") : "" %>" maxlength="30"></td>
        </tr>

      </table></td>
    </tr>
  </table>
  <br>

  <br>
  <center>
    <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="c_aceptar" onClick="if (validarForma()){ selectAll( forma.placasFiltro ); forma.submit(); }" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
    <img src="<%=BASEURL%>/images/botones/cancelar.gif" name="c_cancelar" onClick="forma.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
    <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
  </center>
</form>
<% if( request.getParameter("msg") != null ){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="363" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%} %>
</div>

<script>
    function enviar(url){
         var accion = "<iframe name='ejecutor' style='visibility:hidden'  src='<%= CONTROLLER %>" + url + "'> ";
         frameEjecutor.innerHTML = accion;
    }
</script>

<font id='frameEjecutor'></font>
<%=datos[1]%>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>

</body>
</html>

