<!--
- Autor : Ing. Henry Osorio
- Date  : 1 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que genera LA INFORMACION DE LA OBSERVACION DEL ITEM
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html; charset=iso-8859-1"%>
<%@page session="true"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
  CXPObservacionItem observacion = (CXPObservacionItem)session.getAttribute("observacion");  
  String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";
  String TextoActivo  = (request.getParameter("TextoActivo")!=null)?request.getParameter("TextoActivo"):"";
  String Auto         = (String)session.getAttribute("Autorizadas");
  boolean Autorizadas = (Auto.equals("true"))?true:false;
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Autorizacion de facturas</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
</head>

<body onLoad="window.opener.location.reload();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Observación Factura "/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="formobs" action="<%=CONTROLLER%>?estado=CXPObservacionItem&accion=Insert&opc=INSERT" method="post">
<table border="2" align="center" width="426">
  <tr>
    <td >
      <table width="99%" align="center">
        <tr>
          <td width="392" height="24"  class="subtitulo1"><p align="left">Crear Observaci&oacute;n</p></td>
          <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
     <table width="99%" align="center">
        <tr class="fila">
          <td width="84" class="filaresaltada">Historial</td>
        </tr>
        <tr class='fila'>
          <td width="272"><textarea name="pagador" cols="120" rows="20" readonly class="textbox" id="pagador"><%if(observacion.getObservacion_autorizador()!=null) out.print(observacion.getObservacion_autorizador());%></textarea></td>
        </tr>
        <%if(!Autorizadas){%>
        <tr class="fila">
          <td width="200" class="filaresaltada">Escriba su observación</td>
        </tr>
        <tr class="fila">
          <td><textarea name="aprobador" cols="120" rows="6"  class="textbox" id="aprobador" onFocus="document.formobs.aprobador.value=''"></textarea></td>
        </tr>
        <%}%>
       </table>
    </td>
  </tr>
</table>
<br>
<table width="595" border="0" align="center">
  <tr>
    <td align="center">
      <%if(!Autorizadas){%>
          <%if(TextoActivo.equals("R")||TextoActivo.equals("V")){%>
          <img src="<%=BASEURL%>/images/botones/modificar.gif" name="mod"  height="21" onClick="modificarObs('<%=CONTROLLER%>')"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
          <%}%>
             <%if(TextoActivo.equals("")){%>
             <img src="<%=BASEURL%>/images/botones/modificar.gif" name="mod"  height="21" onClick="formobs.submit();"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
             <%}%>
                     <%if(TextoActivo.equals("V")){%>
                     <img src="<%=BASEURL%>/images/botones/observacion.gif" name="mod"  height="21" onClick="location.href='<%=CONTROLLER%>?estado=CXPObservacionItem&accion=Insert&opc=CIERRE'"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
              <%}
       }%>
        
       <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
    </td>
    </tr>
</table>

<%if(!msg.equals("")){%>
	<p>
   <table border="2" align="center">
     <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
  <%}%>

</form>
</div>
</body>
</html>
<script>
function modificarObs(CONTROLLER){
    location.href=CONTROLLER+'?estado=CXPObservacionItem&accion=Insert&opc=UPDATE&aprobador='+formobs.aprobador.value;
}

</script>