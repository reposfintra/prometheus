<!--
- Autor : Ing. Henry Osorio
- Date  : 1 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que genera el reporte de INFORMACION CLIENTE CARAVANA
--%>

<%-- Declaracion de librerias--%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%
  String msg = (request.getParameter("msg")!=null)? request.getParameter("msg"):"";
  Vector vecFacturas = model.cxpDocService.getVectorFacturas();
  boolean Autorizada = (request.getParameter("Autorizadas").equals("true"))?true:false;
%>
<html>
<head>
<title>Autorizacion de facturas</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type='text/javascript' src="<%=BASEURL%>/js/estilotsp.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/reporte.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Autorizacion de Facturas"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="buscar" method="post" id="busq" action="<%=CONTROLLER%>?estado=CXPagar&accion=Autorizar">
<table width="990" height="122" border="2" align="center">
    <tr>
      <td height="114">
	  <table width="100%" align="center" bgcolor="#FFFFFF">
              <tr>
                <td width="50%"  height="22" class="subtitulo1">AUTORIZACI&Oacute;N DE FACTURAS</td>
                <td width="411" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
  </table>
  <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
  <tr class="fila">
    <td colspan="4" class="textSinBorde" >&nbsp;</td>
    <td height="21" colspan="11" class="textSinBorde" ><span class="filaresaltada">Listado de Facturas <%=(Autorizada)?"Autorizadas":"No Autorizadas"%></span> <span class="bordereporte">
    </span> </td>
    </tr>
  <tr class="tblTitulo">
    <td colspan="4" class="letra_resaltada" nowrap align="center"><%if(!Autorizada){%>OB  -  A<%}else{%>OB<%}%></td>
    <td  class="letra_resaltada"  nowrap align="center">Proveedor</td>
    <td  class="letra_resaltada"  nowrap align="center">Tipo Doc</td>
    <td  class="letra_resaltada"  nowrap align="center">Documento</td>
    <td  class="letra_resaltada"  nowrap align="center">Fecha Factura</td>
        <td  class="letra_resaltada"  nowrap align="center">Valor</td>
    <td  class="letra_resaltada"  nowrap align="center">Descripci&oacute;n</td>

    <td  class="letra_resaltada"  nowrap align="center">Moneda</td>
    <td  class="letra_resaltada"  nowrap align="center">Agencia </td>
    <td  class="letra_resaltada"  nowrap align="center">Usuario registro </td>
    <td  class="letra_resaltada"  nowrap align="center" title='Cantidad de Dias sin aprobar'>DSA</td>    
  </tr>
  <%for (int i=0; i<vecFacturas.size(); i++) {
        CXP_Doc factura = (CXP_Doc) vecFacturas.elementAt(i);
  %>
  <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">    
    <td colspan="4" class="tdSinBorde"><table width="99%"  border="0">
      <tr>
    <%if(Autorizada){
          if(factura.isBanderaVerde()){%>
              <td width="33%" align="right" nowrap><img src="<%=BASEURL%>/images/verde.JPG" width="11" height="13" title='Tiene Historial de observaciones'></td>
              <td width="33%" align="right"  title='Ver items de la factura' nowrap style="cursor:hand"   onClick="window.open('<%=CONTROLLER%>?estado=CXPBuscar&accion=Documento&doc=factura&pos=<%=i%>','',' top=0,left=100, width=900, height=500, scrollbar=no, status=yes, resizable=yes  ')"><img src="<%=BASEURL%>/images/nota.gif" width="15" height="12"></td>
          <%}%>
        
    <%}else{%>
         <td width="33%" align="right" nowrap><%if(factura.isBanderaVerde()){%><img src="<%=BASEURL%>/images/verde.JPG" width="11" height="13" title='Se ha contestado todas las observaciones'><%}%><%if(factura.isBanderaRoja()){%><img src="<%=BASEURL%>/images/roja.JPG" width="11" height="13" title='Tiene observaciones pendientes'><%}%></td>	
         <td width="33%" align="right"  title='Ver items de la factura' nowrap style="cursor:hand"   onClick="window.open('<%=CONTROLLER%>?estado=CXPBuscar&accion=Documento&doc=factura&pos=<%=i%>','',' top=0,left=100, width=900, height=500, scrollbar=no, status=yes, resizable=yes  ')"><img src="<%=BASEURL%>/images/nota.gif" width="15" height="12"></td>
         <td width="3%" align="right" nowrap><input type="checkbox" title='Seleccionar Para Autorizar' name="check<%=i%>" value="<%=i%>"></td>
    <%}%>
      </tr>
    </table>
      </td>
    <td  class="bordereporte" nowrap               ><%=factura.getNomProveedor()%></td>    
    <td  align="center" class="bordereporte" nowrap><%=factura.getTipo_documento()%></td>
    <td  align="center" class="bordereporte" nowrap <%if(!Autorizada){%>onClick="window.open('<%=CONTROLLER%>?estado=CXPBuscar&accion=Documento&doc=factura&pos=<%=i%>','',' top=0,left=100, width=900, height=500, scrollbar=no, status=yes, resizable=yes  ')"<%}%> style="cursor:hand"><%=factura.getDocumento()%></td>
    <td  class="bordereporte" nowrap align="center"><%=factura.getFecha_documento().substring(0,10)%></td>
    <td  class="bordereporte" nowrap align="right" ><%=com.tsp.util.UtilFinanzas.customFormat2(factura.getVlr_saldo())%></td>
    <td  class="bordereporte" nowrap               ><%=factura.getDescripcion()%></td>

    <td  class="bordereporte" nowrap align="center"><%=factura.getMoneda()%></td>
    <td  class="bordereporte" nowrap align="center"><%=factura.getAgencia()%></td>
    <td  class="bordereporte" nowrap               ><%=factura.getCreation_user()%></td>   
    <td  class="bordereporte" nowrap align="center"><%=factura.getNumDias().replaceAll("day","dia")%></td>
   </tr>
   
   <%}%>
</table></td>
</tr>
</table>
<br>
<table width="85%"  border="0" align="center">
  <tr>
    <td align="center">
        <%if((vecFacturas.size()>0)&&(!Autorizada)){%>
        <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgcancelar"  height="21"onMouseOver="botonOver(this);" onClick="buscar.submit();" onMouseOut="botonOut(this);" style="cursor:hand">
        <%}%>        &nbsp; <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></td>
    </tr>
</table>


<%if(vecFacturas.size()==0)
    msg = "No hay facturas pendientes para usted!";
  if(!msg.equals("")){%>
	<p>
   <table width="434" border="2" align="center">
     <tr>
    <td><table width="103%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="274" align="center" class="mensajes"><%=msg%></td>
        <td width="38" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="68">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
  <%}%>
</form>
</div>
</body>
</html>
