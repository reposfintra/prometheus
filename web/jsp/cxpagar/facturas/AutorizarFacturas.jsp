<%-- 
    Document   : AutorizarFacturas
    Created on : Jun 15, 2012, 11:58:01 AM
    Author     : agutierrez
--%>

<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="java.util.TreeMap"%>
<%@page import="com.tsp.util.Util"%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file ="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html>
    <head>

        <title>Autorizacion de facturas</title>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1"/>  
        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'/>
        <link href="<%= BASEURL%>/css/letras.css" rel='stylesheet'/>
        <link href="<%=BASEURL%>/css/default.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/captaciones.js"></script>
        <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"/> 
        <link href="<%= BASEURL%>/css/jquery/jquery-ui/jquery-ui.css" rel="stylesheet" type="text/css"/>
        <link href="<%= BASEURL%>/css/jquery/jquery-ui/jquery.ui.selectmenu.css" rel="stylesheet" type="text/css"/>
        <link href="<%= BASEURL%>/css/jquery/jquery.jqGrid/ui.jqgrid.css" rel="stylesheet" type="text/css"/>
        <script type='text/javascript' src="<%= BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.min.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.ui.min.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.ui.selectmenu.js"></script>

        <%
            model.agenciaService.loadAgencias();
            TreeMap listaAgencias = model.agenciaService.getAgencia();
            listaAgencias.put("TODAS", "TODAS");
            String fechaActual = Util.getFechahoy();
        %>
        <style type="text/css">
            div.ui-dialog{
                font-size:12px;            
            }

            .no-close.ui-dialog-titlebar-close {
                display: none 
            }

            .link_green{
                color: #063; cursor:pointer; text-decoration:underline 
            }

        </style>
        <script>
       
           
           
            $(document).ready( function() {                                     
                $("#tabla_filtros").styleTable();
                
                jQuery("#facturas").jqGrid({
                    caption: "Facturas por Autorizar" ,
                    url: "<%=CONTROLLER%>?estado=Administrador&accion=Cxp",
                                        
                    datatype: "json",
                    width: 1350,
                    height: 500,
                    colNames:['#','Nombre Proveedor','Proveedor','Tipo Doc','Documento','Fecha Factura','Valor','Descripción','Moneda','Agencia','Usuario registro'],	 	 	 	
                    colModel:[
                        {name:'key',index:'key', key:true, hidden:false, align:'center' , width: 20},
                        {name:'nomProveedor',index:'nomProveedor', sortable:true, align:'center',width: 200},                        
                        {name:'proveedor',index:'proveedor', sortable:true, align:'center',width: 85},
                        {name:'tipo_documento',index:'tipo_documento', sortable:true,align:'center',width: 85},
                        {name:'documento',index:'documento', sortable:true, align:'center',width: 85},
                        {name:'fecha_documento',index:'fecha_documento', sortable:true,align:'center',width: 90,formatter: 'date'},
                        {name:'vlr_saldo',index:'vlr_saldo', sortable:true, align:'center',width: 135,
                            formatter:'currency', formatoptions:{decimalSeparator:",", thousandsSeparator: ".", decimalPlaces: 2, prefix: "$ "}},
                        {name:'descripcion',index:'descripcion', sortable:true,align:'center',width: 300},
                        {name:'moneda',index:'moneda', sortable:true,align:'center',width: 90},
                        {name:'agencia',index:'agencia', sortable:true,align:'center',width: 90},
                        {name:'creation_user',index:'creation_user', sortable:true,align:'center',width: 90}
                        
                    ],
                    rowNum:2000,                    
                    rowTotal: 500000,
                    loadonce:true,
                    rownumWidth: 40,
                    gridview: true,
                    viewrecords: true,
                    hidegrid: false,
                    multiselect: true,
                    pager: '#facturas_pager',
                    jsonReader : {
                        root:"rows",
                        repeatitems: false,
                        id: "0"
                    },
                    loadError: function(xhr, status, error){
                        alert(error);
                    }
                });
                
                $('#fechainicio').datepicker({                    
                    maxDate: new Date(),                   
                    dateFormat: 'yy-mm-dd',
                    constrainInput: true,
                    changeMonth: true,                            
                    changeYear: true
                    // showOtherMonths: true,
                    // selectOtherMonths: true 
                });
                                        
                $('#fechafinal').datepicker({						
                    maxDate: new Date(),
                    dateFormat: 'yy-mm-dd',
                    constrainInput: true,
                    changeMonth: true,                            
                    changeYear: true
                    // showOtherMonths: true,
                    // selectOtherMonths: true 
                }); 
                
                $("#divCrearMovimiento").dialog({
                    autoOpen: false,
                    height: "auto",
                    width: "35%",
                    modal: true,
                    autoResize: true,
                    resizable: false,
                    position: "center"
                });                
            }
        );
            
            (function ($) {
                $.fn.styleTable = function (options) {
                    var defaults = {
                        css: 'styleTable'
                    };
                    options = $.extend(defaults, options);

                    return this.each(function () {

                        input = $(this);
                        input.addClass(options.css);


                        input.find("th").addClass("ui-state-default");
                        input.find("td").addClass("ui-widget-content");

                        input.find("tr").each(function () {
                            $(this).children("td:not(:first)").addClass("first");
                            $(this).children("th:not(:first)").addClass("first");
                        });
                    });
                };
            })(jQuery);
            

            function refreshGrid() {          
           
                jQuery("#facturas").setGridParam({
                    url: "<%=CONTROLLER%>?estado=Administrador&accion=Cxp&op="+$("#mes").val()+"&fechaInicial="+$("#fechainicio").val()+"&fechaFinal="+$("#fechafinal").val()+"&agencia="+$("#agencia").val(),
                    datatype: 'json'                    
                });
                jQuery("#facturas").trigger("reloadGrid");                    
            }
            
            
            function refreshGrid2() {
                jQuery("#facturas").setGridParam({
                    url: "<%=CONTROLLER%>?estado=Administrador&accion=Cxp&op=3",
                    datatype: 'json'                    
                });
                jQuery("#facturas").trigger("reloadGrid");  
                
                alert("entro.");
            }
            
            
            function registrarRetiro(){                
                $("#divCrearMovimiento").html("");
                $("#divCrearMovimiento").dialog("open");
                $("#divCrearMovimiento").css('height', 'auto');   
                $("#divCrearMovimiento").dialog("option", "position", "center");               
               
            }
            
            function aprobarFacturas(){
                
                $("#divCrearMovimiento").html("<div id='resp'></div>");
                $("#divCrearMovimiento").dialog("open");
                $("#divCrearMovimiento").css('height', 'auto');   
                $("#divCrearMovimiento").dialog("option", "position", "center");               
                
                
               var keys = jQuery("#facturas").jqGrid('getGridParam','selarrrow');                    
                if(keys != ""){
                    var ind = 0;
                    var ArrayKeys = keys.toString().split(",");     
                    var Json = new Array() ;
                    $.each(ArrayKeys, function (index, key) {
                       var vectorJson=new Object();
                        vectorJson.documento=jQuery("#facturas").getCell(key,'documento');
                        vectorJson.proveedor=jQuery("#facturas").getCell(key,'proveedor');
                        vectorJson.tipo_documento=jQuery("#facturas").getCell(key,'tipo_documento');   
                        Json.push(vectorJson);
                        ind++;
                    }); 
                    
                    var myJsonString = JSON.stringify(Json);                            
                    $.ajax({
                        type: "POST",
                        dataType: 'html',
                        url: "<%=CONTROLLER%>?estado=Administrador&accion=Cxp",
                        data:{
                            listado: myJsonString,
                            op: 2,
                            tam: ind
                        },
                        success: function(resp){
                            var boton = "<div style='text-align:center'><img src='<%=BASEURL%>/images/botones/salir.gif'  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'  onclick='cerrarDiv(\""+"#divCrearMovimiento" +"\");refreshGrid()' /></div>"
                            document.getElementById('resp').innerHTML = resp+ "<br/><br/><br/><br/>"+boton                              
                        },
                        error: function(){
                            alert("Ocurrio un error enviando los datos al servidor");            
                        }
                    });                  
                }else{
                    alert("Debe seleccionar por lo menos una factura");
                }                 
            }  
            
            
            
            function cerrarDiv(div) {
                $(div).dialog('close')
            }
            
            
            function habilitar(){                
                $("#opciones").toggle(0);
            }
           
        </script>
        <style type="text/css">
            div.ui-dialog{
                font-size:12px;
            }

            div.ui-datepicker{
                font-size:12px;
            }
        </style>     
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Autorizacion de Facturas"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

            <br/>

            <table  width="35%" align="center" id="tabla_filtros"  style="font-size:63%" >

                <tr>
                    <th width="50%" align="left" colspan="2" ><strong> FILTROS PARA LA AUTORIZACION </strong></th>
                    <td  width="50%" align="left" colspan="2"  >
                        <img alt="" src="<%= BASEURL%>/images/titulo.gif" width="32" height="20" align="left"/>
                    </td>
                </tr>


                <tr>
                    <th width="300"><b>Agencias:</b></th>
                    <td width="200">                        
                        <input:select name="agencia" attributesText=" id='agencia'"   default="TODAS" options="<%= listaAgencias%>"   />                       

                    </td>                
                    <th width="300"><b>Factura</b></th>

                    <td width="200">
                        <select name="factura" id="mes" onchange="habilitar()"  style="border:0;width:99%;background: white">                           
                            <option value="1" selected="true" >No Autorizadas</option>
                            <option value="4">Autorizadas</option>
                        </select>
                    </td>
                </tr>


                <tr>
                    <th width="350"><b>Fecha Inicial:</b></th>
                    <td width="50">
                        <input name='fechainicio' type='text'  class="textbox" id="fechainicio" style='border: 0;width:50'  value="<%=fechaActual%>" readonly/>
                    </td>   

                    <th width="350"><b>Fecha Final:</b></th>
                    <td width="50">
                        <input name='fechafinal' type='text' class="textbox" id="fechafinal" style='border:0;width:50' value="<%=fechaActual%>"  readonly/>                        
                    </td>
                </tr>

            </table>

            <br/>

            <table width="55%"  border="0" align="center">
                <tr>
                    <td align="center" colspan="4" >  
                        <img src="<%=BASEURL%>/images/botones/buscar.gif"  name="imgbuscar" height="21" onMouseOver="botonOver(this);" onClick="refreshGrid();" onMouseOut="botonOut(this);" style="cursor:hand"/>
                    </td>
                </tr>
            </table>

            <br/>
            <div align="center" width="55%" >
                <table id="facturas"  width="55%"  border="0" align="center" ></table>
                <div id="facturas_pager"></div>
            </div> 
            <br/>
            <div align="center" id="opciones" >
                <table width="55%"  border="0" align="center">
                    <tr>
                        <td align="center">                      
                            <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgcancelar"  height="21"onMouseOver="botonOver(this);" onClick="aprobarFacturas()" onMouseOut="botonOut(this);" style="cursor:pointer"/>

                            <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:pointer"/>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div id="divCrearMovimiento" title="Autorizacion de facturas" style=" display:none ; alignment-adjust: center; alignment-baseline: center" >    

        </div>

        <div id="facturas_pager" style="alignment-adjust: center; alignment-baseline: center" >    

        </div>
    </body>
</html>

