<!--
- Autor : Ing. Enrique De Lavalle.
- Date  : 30 de Enero 2007
- Copyrigth Notice : Fintravalores S.A. S.A
-->

<%-- Declaracion de librerias--%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>.:Factura Proveedor Anticipo Items</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type='text/javascript' src="<%=BASEURL%>/js/reporte.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/script.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Factura Proveedor Anticipo"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; overflow:scroll"> 
<br>
<%
  List Listado  = model.facturaProveedorAnticipoService.getItems();
  UtilFinanzas u = new UtilFinanzas();
  
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
  
  String proveedor   = ( request.getParameter("prove")!=null)?request.getParameter("prove"):"";
  String fechainicial = ( request.getParameter("fechainicial")!=null )?request.getParameter("fechainicial"):"";
  String fechafinal   = ( request.getParameter("fechafinal")!=null)?request.getParameter("fechafinal"):"";
  
%>

<%  if(Listado!=null && Listado.size()>0) { %>  

<form name="forma" method="post" id="forma" action="<%=CONTROLLER%>?estado=FacturaProveedor&accion=Anticipo">

<table width="650" border="2" align="center" >
  <tr>
    <td>  
      <table width="650" border="0" >
			<tr>
   				<td width="154"  height="24"  class="subtitulo1"><p align="left">Pago Anticipo</p></td>
            	<td width="636"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>						
            </tr>
	  </table>			
	
	<table  width="650" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4" id="principal">
        <tr class="tblTitulo" >
				<td align="center" title="Agencia generadora OC">Planilla</div></td>    				
			 	<td align="center" title="Usuario generador OC">Concepto</div></td>    				
			  	<td  align="center" title="OC: Numero de planilla">Fecha</div></td>    				
				<td align="center" title="Contador de planillas ">Valor</div></td>    				
				<td align="center" title="Valor de la planilla">Moneda Planilla</div></td>    								
		</tr>

          <%-- keep track of preference --%>
         <%     for(int i=0; i<Listado.size();i++) {
		        FacturaProveedorAnticipo pA = (FacturaProveedorAnticipo) Listado.get(i);     %>
         
			 <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">						 			
				<td align="center" title="Agencia generadora OC"><% if( !pA.getPlanilla().equals("")){%> <%=pA.getPlanilla()%>  <%}else{%>&nbsp;<%}%></td>  
				<td align="center" title="Usuario generador OC"><% if( !pA.getConcepto().equals("")){%> <%=pA.getConcepto()%>  <%}else{%>&nbsp;<%}%></td>    				
				<td align="center" title="OC: Numero de planilla"><% if( !pA.getFecha().equals("")){%> <%=pA.getFecha().substring(0,16)%>  <%}else{%>&nbsp;<%}%> </td>    				
				<td align="center" title="Contador de planillas "><% if( !pA.getValor().equals("")){%> <%=pA.getValor()%>  <%}else{%>&nbsp;<%}%></td>    				    				
				<td align="center" title="Valor de la planilla"><% if( !pA.getMoneda().equals("")){%> <%=pA.getMoneda()%>  <%}else{%>&nbsp;<%}%></td>					
			</tr>
			
		<%}%>
		<%Listado.clear();%>
	</table>	  
    </td>
  </tr>
</table>
 <input type=hidden id='Opcion' name='Opcion' value=''> 
 <input type=hidden id='proveedor' name='proveedor' value='<%=proveedor%>'> 
 <input type=hidden id='fechaini' name='fechaini' value='<%=fechainicial%>'> 
 <input type=hidden id='fechafin' name='fechafin' value='<%=fechafinal%>'> 

 <table width="800" border="0" align="center">
    <tr>
	  <td align="left" width="27%">&nbsp;</td>
	</tr>
</table>
<table width="800" border="0" align="center">
    <tr>
	  <td align="left" width="27%">&nbsp;</td>
	  <td align="left" width="17%"><img src="<%=BASEURL%>/images/botones/regresar.gif"      name="imgaceptar" onclick="Opcion.value = 'Regresar'; forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
      <td align="left" width="15%"><img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod"  height="21"  onclick="Opcion.value = 'Generar';forma.submit();"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
	  <td align="left" width="67%"><img src="<%=BASEURL%>/images/botones/salir.gif"   name="mod"  height="21"  onclick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
	  
    </tr>
</table>
</form>
<%}else{
 String mensaje = "No hay datos para estos parametros";%>
 <table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=mensaje%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="100%" border="0" align="center">
    <tr>
      <td align="left">&nbsp;</td>
      <td align="left">&nbsp;</td>
      <td align="left">&nbsp;</td>
    </tr>
	<form name="form1" method="post" id="form1" action="<%=CONTROLLER%>?estado=FacturaProveedor&accion=Anticipo">  
	 <input type=hidden id='Opcion' name='Opcion' value=''> 
  <tr>
	  <td align="left" width="37%">&nbsp;</td>	  
      <td align="left" width="17%"><img src="<%=BASEURL%>/images/botones/regresar.gif"      name="imgaceptar" onclick="Opcion.value = 'Regresar'; form1.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
	  <td align="left" width="67%"><img src="<%=BASEURL%>/images/botones/salir.gif"   name="mod"  height="21"  onclick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
	</form>  
    </tr>
</table>
 <%}%>

	


</div>
</body>
</html>


