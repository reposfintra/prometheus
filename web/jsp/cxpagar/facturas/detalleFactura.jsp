<!--
- Autor : Ing. Henry Osorio
- Date  : 1 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que genera LA INFORMACION DETALLE FACTURA
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html; charset=iso-8859-1"%>
<%@page session="true"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%
  CXP_Doc factura = (CXP_Doc)session.getAttribute("factura");
  String  Opcion  = (request.getParameter("Opcion")!=null)?request.getParameter("Opcion"):"Observacion";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>INFORMACION DETALLE FACTURA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type='text/javascript' src="<%=BASEURL%>/js/caravanas.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/reporte.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Observci�n Factura "/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<table width="788" border="2" align="center">
    <tr>
      <td>
<table width="99%" align="center" bgcolor="#FFFFFF">
              <tr>
                <td width="50%" height="22" class="subtitulo1">Informaci&oacute;n del Documento No. <%=factura.getDocumento()%></td>
                <td width="411" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
  </table>
      <table width="99%" align="center" cellpadding="1" cellspacing="1" >        
        <tr class="fila">
          <td width="12%" height="22" class="filaresaltada">Fecha Factura </td>
          <td width="12%"><%=factura.getFecha_documento().substring(0,10)%></td>
		  <td width="16%" class="filaresaltada" >Fecha Vencimiento </td>
          <td width="13%" ><%=factura.getFecha_vencimiento().substring(0,10)%></td>
          <td width="16%" class="filaresaltada" >Valor a Cancelar</td>
          <td width="30%" colspan='2'><%=com.tsp.util.UtilFinanzas.customFormat2(factura.getVlr_saldo())%></td>
        </tr>
        <tr class="fila">
          <td height="22" class="filaresaltada" >Proveedor</td>
          <td width="350" colspan="4"><%=factura.getProveedor()%>&nbsp;-&nbsp;<%=factura.getNomProveedor()%></td>
          <td width="16%" class="filaresaltada" >Dias sin Aprobar </td>
          <td height="22"><%=factura.getNumDias().replaceAll("day","dia")%></td>
        </tr>		
      </table></td>
    </tr>
</table>
<table width="788" border="2" align="center">
    <tr>
      <td>
	  <table width="99%" align="center" bgcolor="#FFFFFF">
              <tr>
                <td width="50%" height="22" class="subtitulo1">Items de la Factura</td>
                <td width="411" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
  </table>
  <table width="99%" align="center">
  <tr class="tblTitulo">
    <td colspan="3"  width="5%" bordercolor="#FFFFFF">&nbsp;</td>
    <td width="28%" > <span class="letra_resaltada">Descripci&oacute;n</span></td>
    <td width="12%"  align="center">Valor</td>
	<td width="15%"  align="center">Codigo Cuenta </td>
    <td width="15%" >Nombre Cuenta </td>      
    <td class="letra_resaltada" nowrap align="center" >Nombre Codigo ABC</td>
   
  </tr>
 <%
  Vector vecItems = model.cxpItemDocService.getVectorItemsFactura();
  for (int i=0; i<vecItems.size(); i++) {
        CXPItemDoc items = (CXPItemDoc) vecItems.elementAt(i);			
 %>
 <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">    
    <td colspan="3" class="tdSinBorde">
	<table width="10"  border="0">
      <tr>    
	<td width="33%" bordercolor="#FFFFFF" align="right" ><%if(items.isBanderaVerde()){%><img src="<%=BASEURL%>/images/verde.JPG" width="11" height="13" title='Se ha contestado la observacion'><%}%></td>
    <td width="33%" bordercolor="#FFFFFF" align="right"><%if(items.isBanderaRoja()){%><img onClick="" src="<%=BASEURL%>/images/roja.JPG" width="11" height="13" title='Tiene Observaciones pendientes'><%}%></td>    
    <td width="33%" bordercolor="#FFFFFF" align="right" style="cursor:hand"><img onClick="location.href='<%=CONTROLLER%>?estado=CXPBuscar&accion=Documento&doc=<%=Opcion%>&pos=<%=i%>'" src="<%=BASEURL%>/images/nota.gif" width="15" height="13" title='Ver Observaciones'></td>
      </tr>
    </table></td>   
    <td width="215" class="bordereporte"><%=items.getDescripcion()%></td>    
    <td width="70" class="bordereporte" align="right"><%=com.tsp.util.UtilFinanzas.customFormat2(items.getVlr())%></td>
    <td width="75" class="bordereporte"><%=items.getCodigo_cuenta()%></td>
    <td width="79" class="bordereporte"><%=items.getRef3()%></td>
   <td width="120" class="bordereporte">&nbsp;</td>
   </tr>
  
   <%}%>
</table>
</td>
</tr>
</table>
<br>
<table width="80%"  border="0" align="center">
  <tr>
    <td align="center">    <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></td>
  </tr>
</table>

<p>&nbsp;</p>
</div>
</body>
</html>
