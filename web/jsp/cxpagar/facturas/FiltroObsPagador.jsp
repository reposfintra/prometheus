<!--
- Autor : Ing. Ivan Dario Gomez Vanegas
- Date  : 1 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que toma los parametro para las facturas con observación para el pagador
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html; charset=iso-8859-1"%>
<%@page session="true"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<% TreeMap Agencia = model.agenciaService.getAgencia();
 Agencia.put(" TODAS","TODAS");
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
</head>

<body onLoad="window.opener.location.reload();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Observación de facturas "/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="forma">
<table border="2" align="center" width="426">
  <tr>
    <td >
      <table width="99%" align="center">
        <tr>
          <td width="392" height="24"  class="subtitulo1"><p align="left">Filtro para la Autorización</p></td>
          <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
      <table width="99%" align="center">
        <tr class="fila">
          <td width="84" class="filaresaltada">Agencia:</td>
          <td><input:select name="Agencia" attributesText="<%= "id='Agencia'  style='width:50%;' class='textbox'  " %>"  default="TODAS" options="<%= Agencia %>" />
          <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
         </td>
        </tr>
        </tr>
        <tr class="fila">
            <td><strong>Fecha inicial </strong></td>
            <td>
              <input name='fechai' type='text' class="textbox" id="fechai" style='width:120' value='' readonly>
              <a href="javascript:void(0);" class="link" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.forma.fechai);return false;"  HIDEFOCUS > <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16"
                border="0" alt="De click aqu&iacute; para ver el calendario."></a> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
         </tr>
         <tr class="fila" >
            <td><strong>Fecha final </strong></td>
            <td>
              <input name='fechaf' type='text' class="textbox" id="fechaf" style='width:120' value='' readonly>
              <a href="javascript:void(0);" class="link" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.forma.fechaf);return false;"  HIDEFOCUS > <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16"
              border="0" alt="De click aqu&iacute; para ver el calendario."></a> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          </tr>
       </table>
     </td>
  </tr>
</table>
<br>
<table width="595" border="0" align="center">
  <tr>
    <td align="center">
      <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod"  height="21" onClick="javascript:enviarFormulario('<%=CONTROLLER%>',forma);"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
      <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></td>
    </tr>
</table>


</form>

</div>
 <iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
     </iframe>
<script>
   function enviarFormulario(CONTROLLER,frm){
       if((frm.fechai.value != "" )&&(frm.fechaf.value != "" )){
           if(frm.fechai.value <= frm.fechaf.value){
               window.open(CONTROLLER+'?estado=CXPObservacion&accion=Pagador&doc=ObservacionPagador&Agencia='+frm.Agencia.value+
                                                                                                    '&fechai= '+frm.fechai.value +
                                                                                                    '&fechaf= '+frm.fechaf.value ,'',' top=0,left=100, width=900, height=500, scrollbar=no, status=yes, resizable=yes');
            }else{
                alert("La fecha inicial debe ser menor o igual a la final");     
            }
       }else{
           alert("La campos fecha no pueden estar vacios"); 
       }                                                                                      
   }
</script>
</body>
</html>
