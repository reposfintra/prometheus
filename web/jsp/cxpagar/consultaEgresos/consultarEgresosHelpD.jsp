<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Descripci&oacute;n de campos de Consulta de Egresos</title>

<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<% String BASEIMG = BASEURL +"/images/botones/"; %> 
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">CONSULTA DE EGRESOS</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">INFORMACI&Oacute;N DEL EGRESO</td>
        </tr>
        <tr>
          <td width="123" class="fila">Banco</td>
          <td width="551"  class="ayudaHtmlTexto"> Seleccione el nombre del banco </td>
        </tr>
        <tr>
          <td  class="fila"> Sucursal</td>
          <td  class="ayudaHtmlTexto"> Seleccione la sucursal del banco escogido</td>
        </tr>
        <tr>
          <td  class="fila">Tipo de Documento</td>
          <td  class="ayudaHtmlTexto"> Seleccione el tipo de documento del detalle del egreso </td>
        </tr>
        <tr>
          <td width="123"  class="fila"> No. Documento </td>
          <td width="551"  class="ayudaHtmlTexto">Seleccione el numero de documento del cheque o del tipo de documento escogido</td>
        </tr>
        <tr>
          <td  class="fila">Fecha Inicial</td>
          <td  class="ayudaHtmlTexto"> Rango inicial de creaci&oacute;n de cheque </td>
        </tr>
        <tr>
          <td width="123"  class="fila"> Fecha Final</td>
          <td width="551"  class="ayudaHtmlTexto">Rango Final de creaci&oacute;n de cheque</td>
        </tr>		
		<tr>
          <td width="123"  class="fila"> Bot&oacute;n Buscar </td>
          <td width="551"  class="ayudaHtmlTexto">Realiza la b&uacute;squeda</td>
        </tr>
		<tr>
          <td width="123"  class="fila"> Bot&oacute;n Salir </td>
          <td width="551"  class="ayudaHtmlTexto">Cierra la ventana. </td>
        </tr>
		<tr>
          <td width="123"  class="fila"> Bot&oacute;n Cancelar </td>
          <td width="551"  class="ayudaHtmlTexto">Reinicia la p&aacute;gina, para volver a ingresar los datos. </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<br>
<table width="416" align="center">
	<tr>
	<td align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	</td>
	</tr>
</table>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
