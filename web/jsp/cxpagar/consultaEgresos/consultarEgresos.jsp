<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.finanzas.contab.model.*,com.tsp.finanzas.contab.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-1.0" prefix="input" %>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%      
        String resultado      = (String)session.getAttribute( "resultadoE" );
        String branch_code    = (String)session.getAttribute( "branch_code" )!= null?(String)session.getAttribute( "branch_code" ):"";
        String bank_account_no= (String)session.getAttribute( "bank_account_no" )!= null?(String)session.getAttribute( "bank_account_no" ):"";
        String document_no    = (String)session.getAttribute( "document_no" )!= null?(String)session.getAttribute( "document_no" ):"";
        String tipo_documento = (String)session.getAttribute( "tipo_documento" )!= null?(String)session.getAttribute( "tipo_documento" ):"";
        String fechaInicial   = (String)session.getAttribute( "fechaInicial" )!= null?(String)session.getAttribute( "fechaInicial" ):"";
        String fechaFinal     = (String)session.getAttribute( "fechaFinal" )!= null?(String)session.getAttribute( "fechaFinal" ):"";
        String conceptos      = (String)session.getAttribute( "conceptos" )!= null?(String)session.getAttribute( "conceptos" ):"";
        String usuario_creacion= (String)session.getAttribute( "usuario_creacion" )!= null?(String)session.getAttribute( "usuario_creacion" ):"";
		String proveedor= (String)session.getAttribute( "proveedor" )!= null?(String)session.getAttribute( "proveedor" ):"";
		String cxp= (String)session.getAttribute( "cxp" )!= null?(String)session.getAttribute( "cxp" ):"";
                session.removeAttribute("cxp");
        TreeMap opc_docs      = model.egresoService.getListaTipoEgresos();
        TreeMap opc_conceptos = model.egresoService.getListaTipoConceptos();
        TreeMap opc_sucursales = (TreeMap)session.getAttribute( "opc_sucursales" );
        if( opc_sucursales == null ){
            opc_sucursales = new TreeMap();
            opc_sucursales.put( " Seleccione Sucursal", "" );
        }
        
        Vector lstBancos = model.servicioBanco.getBancos();
        
        String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
        String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
    <head>
        <title>Buscar Egresos</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
        <script src='<%=BASEURL%>/js/boton.js'></script>
        <script language="javascript" src="<%=BASEURL%>/js/general.js"></script>
        <script language="javascript" src="<%=BASEURL%>/js/utilidades.js"></script>
        <style>
            A:visited {color:#FFFFFF;}
            A:hover {color:red;}
        </style>
        <script>
            function validarEnviar(){
             /*   if( formBuscar.tipo_documento.value!="" && formBuscar.document_no.value=="" ){
                    alert( "Debe Ingresar el N�mero del Documento" );
                    formBuscar.document_no.focus();
                    return false;
                }else if( formBuscar.branch_code.value == "" && formBuscar.tipo_documento.value=="" ){
                    alert( "Debe ingresar el Banco" );
                    formBuscar.branch_code.focus();
                    return false;
                }else if( formBuscar.branch_code.value != "" && formBuscar.tipo_documento.value=="" ){
                    if( formBuscar.fechaInicial.value == "" || formBuscar.fechaFinal.value == "" ){
                        if( formBuscar.bank_account_no.value=="" || formBuscar.document_no.value=="" ){
                            alert( "Debe seleccionar un rango de fechas" );                            
                            return false;
                        }                        
                    }
                    if( formBuscar.fechaInicial.value > formBuscar.fechaFinal.value ){
                        alert( "La fecha fin no puede ser menor que la fecha inicio." );                            
                        return false;
                    }
                    
                }*/
                enviarPaginaForma('<%=CONTROLLER%>?estado=ConsultarEgresos&accion=Manager&opc=buscar','formBuscar');
            }
        </script>
    </head>
    <body >
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Egresos"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <br>
            <form name="formBuscar" id="formBuscar" method="post" action="<%=CONTROLLER%>?estado=ConsultarEgresos&accion=Manager&opc=buscar" >
            <table border="2" align="center" width="650">
                <tr>
                    <td>
                    <table width="100%" align="center" cellpadding="3" cellspacing="2" class="tablaInferior">
                    <tr class="fila">
                        <td height="28" colspan="2" class="subtitulo1">Criterios de B�squeda</td>
                        <td colspan="2" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" align="left"><%=datos[0]%></td>
                    </tr>
                    <tr class="fila">
                        <td >Banco</td>
                        <td >
                            <select name="branch_code" id="branch_code" class="textbox" style="width:160" onChange="enviarPaginaForma('<%=CONTROLLER%>?estado=ConsultarEgresos&accion=Manager&opc=sucursales','formBuscar');">
                                <option value="">Seleccione Banco</option>
                                <%
                                    for( int i = 0; i < lstBancos.size(); i++ ){
                                        Banco b = (Banco)lstBancos.get( i );
                                        String selected = ( branch_code.equalsIgnoreCase(b.getBanco()) )? "selected" : "";
                                        out.println("<option "+selected+" value='"+b.getBanco()+"'>"+b.getBanco()+"</option>");
                                    }
                                %>
                            </select>
                        </td>
                        <td >Sucursal</td>
                        <td >
                            <input:select name="bank_account_no" options="<%= opc_sucursales %>" attributesText="class='textbox' id='bank_account_no' style='width:160'" default="<%=bank_account_no%>" />                            
                        </td>
                    </tr>
                    <tr class="fila">
                        <td width="152">Proveedor</td>
                        <td width="174"><input type="text" name="proveedor" id="proveedor" class="textbox" maxlength="12" value="<%=proveedor%>" >
                    </td>
                        <td width="110">No Egreso</td>
                        <td width="166">                        
                            <input type="text" name="document_no" id="document_no" class="textbox" maxlength="12" value="<%=document_no%>" >
                        </td>
                    </tr>

                    <tr class="fila">
                        <td width="152">No CXP</td>
                        <td width="174"><input type="text" name="cxp" id="cxp" class="textbox" maxlength="12" value="<%=cxp%>" >
                           
                      </td>
                        <td width="110">Usuario Creacion</td>
                        <td width="166">                        
                            <input type="text" name="usuario_creacion" id="usuario_creacion" class="textbox" maxlength="12" value="<%=usuario_creacion %>" style="text-transform:uppercase " >
                        </td>
                    </tr>
                    <tr class="fila">
                      <td>Fecha Inicial</td>
                      <td><input type="text" name="fechaInicial" id="fechaInicial" class='textbox' size="10" readonly value="<%=fechaInicial%>" style='width:100'>
                        <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.formBuscar.fechaInicial);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="Click aqui para escoger la fecha"></a></td>
                      <td>Fecha Final</td>
                      <td ><input type="text" name="fechaFinal" id="fechaFinal" class='textbox' size="10" readonly value="<%=fechaFinal%>" style='width:100'>
                        <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.formBuscar.fechaFinal);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="Click aqui para escoger la fecha"></a></td>
                    </tr>					
                    </table>
                </td>
                </tr>
            </table>     
            <div align="center"><br>
                <img src="<%= BASEURL%>/images/botones/buscar.gif"  name="imgaceptar" onClick="validarEnviar();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"  style="cursor:hand ">&nbsp;
                <img src="<%= BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar" onClick="enviarPaginaForma('<%=CONTROLLER%>?estado=ConsultarEgresos&accion=Manager&opc=cancelar','formBuscar');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"  style="cursor:hand ">&nbsp;
                <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand ">
            </div>
            </form>              
            <%
                String mensaje = (String)request.getAttribute( "mensaje" );               
                if( mensaje != null ){
            %>    
            <table border="2" align="center">
                <tr>
                    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr> 
                    <td width="350" align="center" class="mensajes"><%= mensaje %></td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="58">&nbsp;</td>
                </tr>
                </table></td>
                </tr>
            </table>                   
            <%}
            //Vector egresos = model.egresoService.getEgresos(); 
            Vector egresos = (Vector)session.getAttribute( "egresos" );
            if( resultado != null && egresos.size() != 0 ){%>
			 <%  
				String style = "simple";
				String position =  "bottom";
				String index =  "center";
				int maxPageItems = 12;
				int maxIndexPages = 10;                                
			%>
			<pg:pager
						items="<%= egresos.size()%>"
						index="<%= index %>"
						maxPageItems="<%= maxPageItems %>"
						maxIndexPages="<%= maxIndexPages %>"
						isOffset="<%= true %>"
						export="offset,currentPageNumber=pageNumber"
						scope="request">
						<%-- keep track of preference --%>			
                <table width="98%" border="1" align="center">
                <tr>
                    <td>                        
                        <table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4">
							<tr class="fila">
		                        <td colspan="3" class="subtitulo1" style="border:0 ">Documentos Encontrados</td>
		                        <td colspan="6" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" align="left"></td>
		                    </tr>						
                            <tr class="tblTitulo">                            
								<td width="5%" align="center">Distrito</td>
								<td width="16%" align="center">Banco</td>
								<td width="15%" align="center">Sucursal</td>
								<td width="7%" align="center">No.<br>Documento</td>
								<td width="22%" align="center">Beneficiario</td>
								<td width="6%" align="center">Anulado</td>
								<td width="8%" align="center">Fecha<br>Cheque</td>
								<td width="11%" align="center">Valor<br> Moneda Extr.</td>
								<td width="10%" align="center">Valor<br> Local</td>
                            </tr> 
                            <form name="formAccion" id="formAccion" method="post" action="" >
                           
                            <%
                                  int j = 0;
                                  for (int i = offset.intValue(), l = Math.min(i + maxPageItems, egresos.size()); i < l; i++){
                                      Egreso eg = (Egreso) egresos.elementAt( i );
                                      String color = (i % 2 == 0 )?"filagris":"filaazul";
                            %>
                            <pg:item>
                                
                                <tr class="<%=color%>" style="cursor:pointer" onMouseOver='cambiarColorMouse(this)' onClick="parent.location='<%=CONTROLLER%>?estado=ConsultarEgresos&accion=Manager&opc=ver&dstrct=<%=eg.getDstrct()%>&branch_code=<%=eg.getBranch_code()%>&bank_account_no=<%=eg.getBank_account_no()%>&document_no=<%=eg.getDocument_no()%>'">
                                    <td class="bordereporte" align="center"><%= eg.getDstrct() %>&nbsp;</td>
                                    <td class="bordereporte" align="left"><%= eg.getBranch_code() %>&nbsp;</td>
                                    <td class="bordereporte" align="left"><%= eg.getBank_account_no() %>&nbsp;</td>
                                    <td class="bordereporte" align="center"><%= eg.getDocument_no() %>&nbsp;</td>
                                    <td class="bordereporte" align="left"><%= eg.getPayment_name() %>&nbsp;</td>
									<td class="bordereporte" align="center"><%= eg.getReg_status() %>&nbsp;</td>
                                    <td class="bordereporte" align="center"><%= eg.getFechaEntrega() %>&nbsp;</td>
                                    <td class="bordereporte" align="right"><%= UtilFinanzas.customFormat2( eg.getValor_for() ) %>&nbsp;</td>
                                    <td class="bordereporte" align="right"><%= UtilFinanzas.customFormat2( eg.getValor() ) %>&nbsp;</td>
                                </tr>
                                
                            </pg:item>
                            <%}%>
                            </form>
                            <tr   class="bordereporte">
                            <td colspan="9" nowrap align="center">
                            <pg:index>
                                <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
                            </pg:index> 
                            </td>
                            </tr>
                            
                            </table>
                    </td>
                </tr>
                </table>     
				</pg:pager>           
            <%}%>
        </div>
        <iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
        <%=datos[1]%>
    </body>
</html>
