<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.finanzas.contab.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.finanzas.contab.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
    <head><title>Ver Detalle Egreso</title>
        
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="/css/estilostsp.css" rel="stylesheet" type="text/css"> 
        <script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/finanzas/contab/comprobante.js"></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/contabilidad.js"></script>
        
        <style type="text/css">
            <!--
            .Estilo1 {color: #D0E8E8}
            -->
        </style>
    </head>

<% 
    Egreso cabecera = model.egresoService.getEgreso();
    Vector detalle = model.egresoService.getEgresos();
    //session.setAttribute( "resultadoE", null );
%>  
    <body onresize="redimensionar()" >

        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Ver detalle Egreso"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <form name="forma1" id="forma1" action="" method="post">
            <table width="900" align="center">				
                <tr>
                    <td>       
                        <table width='100%' border="2">
                            <tr>
                            <td>
                                <table width="100%" align="center"  >
                                    <tr >
                                    <td width="50%"  class="subtitulo1"><p align="left">Documento</p></td>
                                    <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" class="Estilo1" height="20" align="left"><%=datos[0]%></td>
                                    </tr>
                                </table>
                                <table width="100%" align="center" cols="7">
                                    <tr >                
                                    <td class="fila" width="110">&nbsp;Distrito</td>
                                    <td class="letra" width="100"><%= cabecera.getDstrct() %>&nbsp;</td>
                                    <td class="fila" width="100">&nbsp;Banco </td>
                                    <td width="307" class="letra"><%= cabecera.getBranch_code() %>&nbsp;</td>
                                    <td class="fila" width="69">&nbsp;Sucursal </td>
                                    <td width="166" class="letra"><%= cabecera.getBank_account_no() %>&nbsp;</td>
                                    </tr>    
                                    <tr >
                                    <td class="fila">&nbsp;Tipo Documento</td>
                                    <td class="letra"><%= cabecera.getItem_no() %>&nbsp;</td>
                                    <td class="fila">&nbsp;No. Documento</td>
                                    <td class="letra"><%= (cabecera.getTransaccion()==0? cabecera.getDocument_no() : "<a href='#' style='color:red' onclick=\"window.open('"+ CONTROLLERCONTAB +"?estado=AdminComprobantes&accion=Manager&opc=ver&dstrct="+ cabecera.getDstrct() +"&tipodoc=EGR&numdoc="+ cabecera.getDocument_no() +"&grupo="+ cabecera.getTransaccion() +"&fechaapply=','','menubar=no'); \">"+ cabecera.getDocument_no() +"</a>") %>&nbsp;</td>
                                    <td class="fila">&nbsp;Concepto</td>
                                    <td class="letra"><%= cabecera.getConcept_code() %>&nbsp;</td>
                                    </tr>    
                                    <tr >
                                    <td class="fila">&nbsp;Nit</td>
                                    <td class="letra"><%= cabecera.getNit() %>&nbsp;</td>
                                    <td class="fila">&nbsp;Beneficiario</td>
                                    <td class="letra"><%= cabecera.getPayment_name() %>&nbsp;</td>
                                    <td class="fila">&nbsp;Agencia</td>
                                    <td class="letra"><%= cabecera.getAgency_id() %>&nbsp;</td>
                                    </tr>
                                    <tr >
                                    <td class="fila">&nbsp;Fecha Cheque</td>
                                    <td class="letra" align="left"><%= cabecera.getFechaEntrega() %>&nbsp;</td>                                    
                                    <td class="fila" align="left">&nbsp;Estado</td>
                                    <td class="letra" align="left"><%= cabecera.getEstado() %></td>
                                    <td class="letra" align="left">&nbsp;</td>
                                    <td class="letra" align="left">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>                
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <table width="900" align="center" border="2">
                <tr>
                    <td >
                    <table width="100%" align="center" >
                        <tr >
                            <td width="50%"   class="subtitulo1"><p align="left">Detalle del Documento </p></td>
                            <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" class="Estilo1" height="20" ></td>
                        </tr>
                    </table>                        
                    <table id="detalle" width="100%" >
                        <tr  id="fila1" class="tblTitulo">
                            <td align="center" nowrap>&nbsp;Item&nbsp;  </td>
                            <td align="center">Descripción </td>
                            <td align="center">Documento</td>
                            <td align="center">Planilla</td>
                            <td align="center">Moneda </td>
                            <td align="center">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Valor&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            <td align="center">&nbsp;Valor Local&nbsp;</td>
                        </tr>				
				<%                                 
				if ( detalle != null){
                                   for( int i = 0; i < detalle.size(); i++ ){
                                        Egreso eg = (Egreso) detalle.get( i );
                                %>							   
                        <tr class="<%= (i%2==0)? "filagris" : "filaazul" %>" style="cursor:default" bordercolor="#D1DCEB" align="center">                            
                            <td align="center" class="informacion"><%=eg.getItem_no()%>&nbsp;</td>
                            <td align="left" class="informacion" nowrap ><%=eg.getPayment_name()%>&nbsp;</td>
                            <td align="center" class="informacion" nowrap><%=eg.getDocument_no()%>&nbsp;</td>
                            <td align="center" class="informacion" ><%=eg.getOc()%></td>
                            <td align="center" class="informacion" ><%=eg.getCurrency()%></td>
                            <td align="right" ><%=UtilFinanzas.customFormat( "#,##0.00", eg.getValor_for(), 2)%></td>
                            <td align="right" ><%=UtilFinanzas.customFormat( "#,##0.00", eg.getValor(), 2)%></td>
                        </tr>				   
                                   <%}%>
                            <%}%>
                    </table>
                    </td>
                </tr>
                <tr >
                    <td >
                        <table width="100%" >
                            <tr  class="tblTitulo" >
                            <td width="70%" ><div align="right">TOTAL</div></td>
                            <td width="15%" align="right"><%=UtilFinanzas.customFormat( "#,##0.00", cabecera.getValor_for(), 2 )%></td>
                            <td width="15%" align="right"><%=UtilFinanzas.customFormat( "#,##0.00", cabecera.getValor(), 2 )%></td>
                            </tr>
                         </table>
                    </td>
                </tr>
            </table>
            <br>    
            <center>                
                &nbsp;<img src='<%=BASEURL%>/images/botones/regresar.gif' name='Regresar' align="absmiddle" style='cursor:hand'   onClick="history.back()" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
                &nbsp;<img src='<%=BASEURL%>/images/botones/salir.gif' name='Buscar' align="absmiddle" style='cursor:hand'   onClick="window.close();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
            </center>
	

        </form>
        </div>
    </body>    
<%=datos[1]%>
</html>s
