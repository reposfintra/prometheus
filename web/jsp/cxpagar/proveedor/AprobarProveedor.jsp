<!--
- Autor : Ing. Tito Andr�s Maturana
- Date  : 30 septiembre del 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : P�gina JSP, que maneja el ingreso en el archivo de proveedor.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Proveedor - Ingresar</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">

<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/hvida.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/referencia.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
<%
        String url = CONTROLLER + "?estado=ProveedorUpdate&accion=Ingresar&cmd=show";
		String url2 = CONTROLLER + "?estado=Consultar&accion=Nit&carpeta=/jsp/cxpagar/proveedor/&pagina=ProveedorInsert.jsp";
%>
<script>        
        function actualizar(){
                forma.action = '<%=url%>';
                forma.submit();
        }
		function actualizar2(){
                forma.action = '<%=url%>&agc=OK';
                forma.submit();
        }
		function actualizar3(){
                forma.action = '<%=url2%>';
                forma.submit();
        }
</script>
<body onresize="redimensionar()" onload = "imgProveedor();redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Aprobar Proveedor y/o Proprietario"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">


<%
        Proveedor prov = model.proveedorService.getProveedor();
        if( prov == null ) prov = new Proveedor();

        Identidad id = model.identidadService.obtIdentidad();
        
        String pais = id.getPais_name();        
        String dpto = id.getEstado_name();
        String ciudad = id.getCiudad_name();
        String sw = (request.getParameter("sw")!=null)?request.getParameter("sw"):""; 
        TreeMap agencias = model.agenciaService.getAgencia();
        TreeMap ciudades = model.ciudadService.getTreMapCiudades();
        TreeMap bancos = model.servicioBanco.getBanco();
		TreeMap sucursales = model.servicioBanco.getSucursal();
        TreeMap tipcuenta = model.tblgensvc.getLista_des();
	   
	   agencias.put(" Seleccione", "");
	   agencias.remove("");
	   bancos.put(" Seleccione", "");
	   sucursales.put(" Seleccione", "");
	   ciudades.put(" Seleccione", "");        
        
        TreeMap clasificacion = new TreeMap();
        clasificacion.put("Default","Default");
       
		
%>
<form id ="forma" name="forma" action="<%= CONTROLLER%>?estado=Proveedor&accion=Actualizar&cmd=show" method="post">

  <table width="785" border="2"align="center">
    <tr>
      <td>
	  <table width="100%" align="center" >
        <tr>
          <td colspan="2" align="left" class="subtitulo1">&nbsp;Documentaci&oacute;n</td>
          <td colspan="4" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
        </tr>
        <tr>
          <td width="133" align="left" class="fila" > Documento </td>
          <td width="236" valign="middle" class="letra" ><%= id.getCedula() %></td>
          <td width="172" valign="middle" class="fila">Tipo Documento</td>
          <td colspan="3" valign="middle" class="letra"><%= ( id.getTipo_iden().matches("CED"))? "C�dula" : "NIT" %></td>
        </tr>
        <tr>
          <td align="left" class="fila">Id Mims</td>
          <td width="236" valign="middle" class="letra"><%= id.getId_mims() %></td>
          <td width="172" valign="middle" class="fila"><input type="hidden" name="evento" value="aprobar"></td>
          <td colspan="3" valign="middle" class="fila">&nbsp;</td>
        </tr>
        <tr class="fila">
          <td colspan="2" align="left" class="subtitulo1">&nbsp;Datos Personales</td>
          <td colspan="4" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
        </tr>
        <tr>
          <td align="left" valign="top" class="fila">Nombre</td>
          <td colspan="5" valign="middle" class="letra"><%= id.getNombre()%><span class="subtitulos">          </span></td>
        </tr>
        <tr>
          <td width="133"align="left" valign="top" class="fila" >Direcci&oacute;n</td>
          <td valign="middle" class="letra"><%= id.getDireccion() %></td>          
          <td valign="top" class="fila">Barrio</td>
          <td colspan="3" valign="middle" class="letra"><%= id.getBarrio() %></td>        
        </tr>
		<tr>
		  <td align="left" valign="top" class="fila">Telefonos</td>
		  <td colspan="5" valign="middle" class="letra"><%= id.getTelefono() %>
              <input name="c_nit" type="hidden" id="c_nit" value="<%= id.getCedula() %>">
              <input name="c_idMims" type="hidden" id="c_idMims" value="<%= id.getId_mims() %>">
              <input name="c_tipo_doc" type="hidden" id="c_tipo_doc" value="<%= id.getTipo_iden() %>">
			  <input name="cla_iden" type="hidden" id="cla_iden" value="<%= id.getClasificacion() %>">
          </tr>
		<tr>
		  <td align="left" valign="top" class="fila" >Celular</td>
		  <td valign="middle" class="letra"><%= id.getCelular() %>        
		  <td valign="top" class="fila">Email        
		  <td colspan="3" valign="middle" class="letra"><%= id.getE_mail() %>        
		  </tr>
		<tr>
          <td width="133" align="left" valign="top" class="fila" >Pais</td>
          <td valign="middle" class="letra"><%= pais %></td>
          <td valign="top" class="fila">Estado</td>    
          <td colspan="3" valign="middle" class="letra"><%= dpto %></td>
          </tr>
        <tr>
          <td width="133"align="left" valign="top" class="fila" >Ciudad</td>
          <td valign="middle" class="letra"><%= ciudad %></td>
          <td valign="top" class="fila"></td>          
          <td colspan="3" valign="middle" class="fila"></td>        
        </tr>        
        <tr class="fila">
          <td colspan="2" align="left" class="subtitulo1">&nbsp;Datos Proveedor</td>
          <td colspan="4" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
        </tr>
        <tr class="fila">
          <td>Sede de Pago  </td>		  
          <td><input:select name="c_agency_id" attributesText="class=textbox  onChange='actualizar2()' disabled" options="<%= agencias %>" default="<%= prov.getC_agency_id() %>" /> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
            <input type="hidden" name="c_agency_id" value="<%= prov.getC_agency_id() %>"></td>
          <td>Banco 
            <input name="c_payment_name" id="c_payment_name2" value="<%= prov.getC_payment_name().equals("")?id.getNombre():prov.getC_payment_name() %>"  type="hidden"></td>
          <td colspan="3"><input:select name="c_branch_code" default="<%= prov.getC_branch_code() %>" attributesText="class=textbox onChange='actualizar()' disabled" options="<%= bancos %>" /> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
            <input type="hidden" name="c_branch_code" value="<%= prov.getC_branch_code() %>"></td>
        </tr>
        <tr class="fila">
          <td>Agencia</td>
          <td><input:select name="c_bank_account" default="<%= prov.getC_bank_account() %>" attributesText="class=textbox disabled" options="<%= sucursales %>" />            <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
            <input type="hidden" name="c_bank_account" value="<%= prov.getC_bank_account() %>"></td>
          <td>Clasificacion</td>
          <td colspan="3"><select name="c_clasificacion" class="textbox" id="c_clasificacion" disabled>
            <option value="">Seleccione Un Item</option>
            <%  LinkedList tblcla = model.tablaGenService.getTblclasificacion();
			      String var = (prov.getC_clasificacion()!=null)?prov.getC_clasificacion():""; 
               for(int i = 0; i<tblcla.size(); i++){
                       TablaGen cla = (TablaGen) tblcla.get(i); %>
            <option value="<%=cla.getTable_code()%>" <%=(cla.getTable_code().equals(var))?"selected":""%>  ><%=cla.getDescripcion()%></option>
            <%}%>
          </select>
            <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
            <input type="hidden" name="c_clasificacion" value="<%=(prov.getC_clasificacion()!=null)?prov.getC_clasificacion():""%>"></td>
          </tr>
		        <tr class="fila">
          <td>No aplica retenciones</td>
          <td><select name="c_agente_retenedor" >
            <option value="N" <%= (prov.getC_agente_retenedor ().equals("N"))?"selected":"" %> >No</option>
            <option value="S" <%= (prov.getC_agente_retenedor ().equals("S"))?"selected":"" %> >Si</option>
          </select></td>      
          <td>Gran Contribuyente</td>
          <td width="91"><select name="c_gran_contribuyente">
  		   <option value="N"  <%= (prov.getC_gran_contribuyente().equals("N"))?"selected":"" %> >No</option>
   		   <option value="S" <%= (prov.getC_gran_contribuyente().equals("S"))?"selected":"" %> >Si</option>
          </select> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          <td width="46">Plazo</td>
          <td width="67"><input name="plazo" type="text" id="plazo" size="4" maxlength="3" onKeyPress="soloDigitos(event,'decNO')" value="<%= prov.getPlazo() %>" readonly>
            <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">d&iacute;as</td>
	        </tr>
        <tr class="fila">
          <td>Aplica IVA</td>
          <td><select name="c_autoretenedor_iva">
            <option value="N" <%= (prov.getC_autoretenedor_iva().equals("N"))?"selected":"" %> >No</option>
            <option value="S" <%= (prov.getC_autoretenedor_iva().equals("S"))?"selected":"" %> >Si</option>
          </select>     <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          <td>Aplicar Retefuente</td>       
          <td colspan="3"><select name="c_autoretenedor_rfte" >
  		   <option value="N"  <%= (prov.getC_autoretenedor_rfte().equals("N"))?"selected":"" %> >No</option>
   		   <option value="S" <%= (prov.getC_autoretenedor_rfte().equals("S"))?"selected":"" %> >Si</option>
          </select>   <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
		  </td>      

      </tr>
        <tr class="fila">        
          <td>Aplica&nbsp;ICA </td>    
          <td>
            <select name="c_autoretenedor_ica">
              <option value="N" <%= (prov.getC_autoretenedor_ica().equals("N"))?"selected":"" %> >No</option>
			  <option value="S" <%= (prov.getC_autoretenedor_ica().equals("S"))?"selected":"" %> >Si</option>
            </select> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
		  <td>Handle Code </td>       
          <td colspan="3"><select name="handle_code" class="textbox" id="handle_code" disabled>
            <option value="">Seleccione Un Item</option>
            <%  LinkedList tblhc = model.tablaGenService.getTblhandlecode();
			      String varhc = (prov.getHandle_code()!=null)?prov.getHandle_code():""; 
               for(int i = 0; i<tblhc.size(); i++){
                       TablaGen hc = (TablaGen) tblhc.get(i); %>
            <option value="<%=hc.getTable_code()%>" <%=(hc.getTable_code().equals(varhc))?"selected":""%>  ><%=hc.getDescripcion()%></option>
            <%}%>
          </select>
            <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
            <input type="hidden" name="handle_code" value="<%=(prov.getHandle_code()!=null)?prov.getHandle_code():""%>"></td>
        </tr>        
		
		<tr class="fila">
		<td>Nit Beneficiario
		</td>
		<td><input name="nit_ben" type="text" class="textbox"  value="<%=prov.getNit_beneficiario()!=null?prov.getNit_beneficiario():""%>" readonly>		</td>
		<td>Nombre Beneficiario
		</td >
		<td colspan="3"><input name="nom_ben" type="text" class="filaresaltada" readonly style='width:90%;text-align:right; border:0;' value="<%=prov.getNom_beneficiario()!=null?prov.getNom_beneficiario():""%>">
          <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
		</tr>
		<tr class="fila">
		 	<td>Concepto</td>
 		     <td><select name="concept_code" class="textbox" id="concept_code" disabled>
               <option value="">Seleccione Un Item</option>
               <%  LinkedList tblcon_pro = model.tablaGenService.getCon_proveedor();
			      String con = (prov.getConcept_code()!=null)?prov.getConcept_code():""; 
               for(int i = 0; i<tblcon_pro.size(); i++){
                       TablaGen con_pro = (TablaGen) tblcon_pro.get(i); %>
               <option value="<%=con_pro.getTable_code()%>" <%=(con_pro.getTable_code().equals(con))?"selected":""%>  ><%=con_pro.getDescripcion()%></option>
               <%}%>
             </select>
	          <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
	          <input type="hidden" name="concept_code" value="<%=(prov.getConcept_code()!=null)?prov.getConcept_code():""%>"></td>
 		     <td>&nbsp;</td >
		     <td colspan="3">&nbsp;</td>
		</tr> 
		<tr class="fila">         
           <td colspan="2">Realizar pagos por transferencia
             <input type="checkbox" name="pago" id="pago"  <%=(prov.getTipo_pago().equals("T"))?"checked":""%> >
             <input name="tipo_pago" type="hidden" id="tipo_pago" value="<%=(prov.getTipo_pago().equals("T"))?"T":"B"%>"></td>    
          <td>&nbsp;</td>
          <td colspan="3">&nbsp;</td>
        </tr>
		<tr class="fila" id="titulo" style="display:none">
          <td colspan="2" align="left" class="subtitulo1">&nbsp;Transferencia</td>
          <td colspan="4" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
        </tr>
        <tr class="fila" id="fila1" style="display:none">
          <td>Banco Transferencia </td>
          <td><select name="c_banco_transfer" class="textbox" id="select" disabled >
            <option value="">Seleccione Un Item</option>
            <%  LinkedList tblban = model.tablaGenService.getTblBanco();
			      String varban = (prov.getC_banco_transfer()!=null)?prov.getC_banco_transfer():""; 
               for(int i = 0; i<tblban.size(); i++){
                       TablaGen ban = (TablaGen) tblban.get(i); %>
            <option value="<%=ban.getTable_code()%>" <%=(ban.getTable_code().equals(varban))?"selected":""%>  ><%=ban.getTable_code()%></option>
            <%}%>
          </select>
            <img name="logo"  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" >
            <input type="hidden" name="c_banco_transfer" value="<%=(prov.getC_banco_transfer()!=null)?prov.getC_banco_transfer():""%>"></td>
          <td>Sucursal Transferencia </td>
          <td colspan="3"><input name="c_sucursal_transfer" type="text" class="textbox" id="c_sucursal_transfer" value="<%= prov.getC_sucursal_transfer() %>" readonly maxlength="15">
            <img name="logo"  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" > </td>
        </tr>
        <tr class="fila" id="fila2" style="display:none"> 
          <td>Tipo de Cuenta</td>
          <td><input:select name="c_tipo_cuenta" attributesText="class=listmenu disabled" options="<%= tipcuenta %>" default="<%= prov.getC_tipo_cuenta() %>" /> <img name="logo" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" > <input type="hidden" name="c_tipo_cuenta" value="<%= prov.getC_tipo_cuenta() %>"></td>
          <td>No. Cuenta</td>
          <td colspan="3"><input name="c_numero_cuenta" type="text" class="textbox" id="c_numero_cuenta" value="<%= prov.getC_numero_cuenta()%>" size="35" maxlength="20" onKeyPress="soloDigitos(event,'decNO')" readonly> <img name="logo2"  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" ></td>
        </tr>
        <tr class="fila" id="fila3" style="display:none">
          <td>Ciudad</td>
          <td><input:select name="c_codciudad_cuenta" attributesText="class=textbox disabled" options="<%= ciudades %>" default="<%= prov.getC_codciudad_cuenta() %>" /> <img name="logo1" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" > <input type="hidden" name="c_codciudad_cuenta" value="<%= prov.getC_codciudad_cuenta() %>"></td>
          <td>Cedula de la cuenta </td>
          <td colspan="3"><input name="cedula_cuenta" type="text" class="textbox" value="<%= prov.getCedula_cuenta() %>" maxlength="15" onKeyPress="soloDigitos(event,'decNO')" readonly> <img name="logo3"  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" > </td>
        </tr>
        <tr class="fila" id="fila4" style="display:none">
          <td>Nombre Cuenta </td>
          <td colspan="5"><input name="nombre_cuenta" type="text" size="100" class="textbox" maxlength="100" value="<%= prov.getNombre_cuenta() %>" readonly> <img name="logo4" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" > </td>
        </tr>
      </table>	  </td>
    </tr>
  </table>
  <div align="center">
    <br>
    <img src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="validarProveedor();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">&nbsp; <img src="<%= BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/cxpagar/proveedor&pagina=ProveedorConsultar.jsp&marco=no'" onMouseOut="botonOut(this);"  style="cursor:hand ">&nbsp; <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);"  style="cursor:hand "></div>
</form>
<%
      if( request.getParameter("msj")!=null ) {
%>
<table border="2" align="center">
  <tr>
    <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="282" align="center" class="mensajes"><%=request.getParameter("msj").toString() %></td>
          <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="78">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<%
	}
%>
</div>
</body>
</html>
