<!--
- Autor : Ing. Tito Andres Maturana
- Date  : 30 septiembre del 2005
- Modified: Enrique De Lavalle 17/04/2007
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la actualizacion en el archivo de proveedor.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Proveedor - Modificar</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type="text/javascript" SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">

<script type='text/javascript' src="<%= BASEURL %>/js/referencia.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/hvida.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/prototype.js"></script>


<%
    String control = session.getAttribute("Control")!=null?(String) session.getAttribute("Control"):"";
        String mensaje = request.getParameter("mensaje")!=null?request.getParameter("mensaje"):"";
		String norefresh = request.getParameter("norefresh")!=null ? "&norefresh=OK" : "" ;
                String pag=request.getParameter("pag")!=null?request.getParameter("pag"):"";
		String url = CONTROLLER + "?estado=ProveedorUpdate&accion=Actualizar&cmd=show" + norefresh;
        String url2 = CONTROLLER + "?estado=Proveedor&accion=Anular&cmd=show" + norefresh;
		String url3 = CONTROLLER + "?estado=Consultar&accion=Nit&carpeta=/jsp/cxpagar/proveedor/&pagina=ProveedorUpdate.jsp";
                String url4 = CONTROLLER + "?estado=Proveedor&accion=Actualizar&cmd=show";
    //<!-- 20101111  -->
    ArrayList lista = null;
    String cd = model.proveedorService.getProveedor().getC_nit();
    String sede = model.proveedorService.getProveedor().getSede()==null?"":model.proveedorService.getProveedor().getSede();
    try{
        if(sede.equals("S")){
            lista = model.proveedorService.buscarConvs(cd);
        }
        else{
            lista = model.proveedorService.buscarConvs();
        }
    }
    catch(Exception e){
        System.out.println("error: "+e.toString());
        e.printStackTrace();
    }
    ArrayList lista2 = null;
    try{
        if(sede.equals("S")){
            lista2 = model.proveedorService.buscarSects(cd);
        }
        else{
            lista2 = model.proveedorService.buscarSects();
        }
    }
    catch(Exception e){
        System.out.println("error: "+e.toString());
        e.printStackTrace();
    }
    //<!-- 20101111  -->
%>
    <script type="text/javascript">
            function actualizar(){
                forma.action = '<%=url%>';
                forma.submit();
            }
            function actualizar2(){
                forma.action = '<%=url%>&agc=OK';
                forma.submit();
            }
            function actualizar3(){
                forma.action = '<%=url2%>';
                forma.submit();
            }
            
            function activacion(){
               // alert("si");
                var seleccionado = document.getElementById("pago").checked;
                if(seleccionado==true){
                    $("select").disabled=false;
                    $("cedula_cuenta").disabled=false;
                    $("nombre_cuenta").disabled=false;
                    $("c_sucursal_transfer").disabled=false;
                    $("c_sucursal_transfer").readOnly=false;
                    $("c_tipo_cuenta").disabled=false;
                    $("c_numero_cuenta").readOnly=false;
                    $("c_numero_cuenta").disabled=false;
                    $("c_codciudad_cuenta").disabled=false;
                    $("cedula_cuenta").readOnly=false;
                    $("nombre_cuenta").readOnly=false;
                }
                else{
                  //  alert("no");
                    $("select").disabled=false;
                    $("select").options[11].selected = true;
                    $("c_sucursal_transfer").readOnly=true;
                    $("c_sucursal_transfer").value="";
                    $("c_tipo_cuenta").disabled=true;
                    $("c_tipo_cuenta").options[0].selected = true;
                    $("c_numero_cuenta").readOnly=true;
                    $("c_numero_cuenta").value="";
                    $("c_codciudad_cuenta").disabled=true;
                    $("c_codciudad_cuenta").options[0].selected = true;
                    $("cedula_cuenta").readOnly=false;
                   // $("cedula_cuenta").value="";
                    $("nombre_cuenta").readOnly=false;
                  //  $("nombre_cuenta").value="";
                }
            }

            function checkselect(idselect,indice){
                if(indice>0){
                    if(indice==1){
                        $(idselect).disabled=false;
                    }
                    else{
                        $(idselect).disabled=true;
                        $(idselect).options[0].selected = true;
                    }
                }
                else{
                    $(idselect).disabled=true;
                    $(idselect).options[0].selected = true;
                }
            }

            function activecheck1(indice){            
                checkselect("c_gran_contribuyente",indice);
                if(indice!=1){
                    checkselect("c_agente_retenedor",0);
                    checkselect("c_autoretenedor_iva",0);
                    checkselect("c_autoretenedor_ica",0);
                    checkselect("c_autoretenedor_rfte",0);
                }else if(indice ===1){
                    checkselect("c_agente_retenedor",indice);
                    checkselect("c_autoretenedor_iva",indice);
                    checkselect("c_autoretenedor_ica",indice);
                    checkselect("c_autoretenedor_rfte",indice);
                    
                }
            }

            function activecheck2(indice){
//                checkselect("c_agente_retenedor",indice);
//                if(indice==0){
//                    checkselect("c_autoretenedor_iva",0);
//                    checkselect("c_autoretenedor_ica",0);
//                    checkselect("c_autoretenedor_rfte",0);
//                }
//                else{
//                    checkselect("c_autoretenedor_iva",indice);
//                    checkselect("c_autoretenedor_ica",indice);
//                    checkselect("c_autoretenedor_rfte",indice);
//                }
            }

            function activecheck3(indice){
//                if(indice==0){
//                    checkselect("c_autoretenedor_iva",1);
//                    checkselect("c_autoretenedor_ica",1);
//                    checkselect("c_autoretenedor_rfte",1);
//                }
//                else{
//                    checkselect("c_autoretenedor_iva",0);
//                    checkselect("c_autoretenedor_ica",0);
//                   checkselect("c_autoretenedor_rfte",0);
//                }
            }
            //<!-- 20101111  -->
            function uncheck(checkElement,fila){
                if(checkElement.checked==true){
                    document.getElementById('porc_afil'+fila).readOnly=false;
                    document.getElementById('cuenta'+fila).readOnly=false;
                }
                else{
                    document.getElementById('porc_afil'+fila).readOnly=true;
                    document.getElementById('porc_afil'+fila).value='0';
                    document.getElementById('porc_ven'+fila).value='0';
                    document.getElementById('cuenta'+fila).readOnly=true;
                    document.getElementById('cuenta'+fila).value='';
                }
            }

            var items=1;
            var fily = 1;
            var maxfilas = 1;
            function soloNumeros(id) {
                 var precio = document.getElementById(id).value;
                 precio =  precio.replace(/[^0-9^.]+/gi,"");
                 document.getElementById(id).value=precio;
            }

            function formatNumber(num,prefix){
                prefix = prefix || '';
                num += '';
                var splitStr = num.split('.');
                var splitLeft = splitStr[0];
                var splitRight = splitStr.length > 1 ? '.' + splitStr[1] : '';
                splitRight = splitRight.substring(0,3);//090929
                var regx = /(\d+)(\d{3})/;
                while (regx.test(splitLeft)) {
                    splitLeft = splitLeft.replace(regx, '$1' + ',' + '$2');
                }
                return prefix + splitLeft + splitRight;
            }

            function unformatNumber(num) {
                return num.replace(/([^0-9\.\-])/g,'')*1;
            }

            function TDEliminarRegistro(imagen,tablaId){
                var i=imagen.parentNode.parentNode.rowIndex;
                alert(tablaId);
                $(tablaId).deleteRow(i);
                items = items - 1;
            }

            function TDAgregarRegistro(tablaId,prefix){
                myTable = $(tablaId);
                //alert(tablaId);
                items = (myTable.rows.length)*1 - 1;
                //alert('rf'+prefix+items);
                var val = (document.getElementById('rf'+prefix+items).value)*1 + 1;
                if(navigator.appName=='Microsoft Internet Explorer')
                {   oRow = myTable.insertRow();
                }else{
                    oRow = myTable.insertRow(-1);
                }
                items = items + 1;
                maxfilas = items>maxfilas? items: maxfilas;
                document.getElementById("forma").action = '<%= CONTROLLER%>?estado=Proveedor&accion=Actualizar&cmd=show&maxfilas='+maxfilas+'&fils='+fily;
                //alert(maxfilas);
                if(navigator.appName=='Microsoft Internet Explorer'){oCell = oRow.insertCell();}else{oCell = oRow.insertCell(-1);}oCell.align='center';oCell.style.backgroundColor='#EAFFEA';
                oCell.innerHTML = '<img style="cursor: pointer" onclick="TDAgregarRegistro(\''+tablaId+'\',\''+prefix+'\')" src=\'<%=BASEURL%>/images/botones/iconos/mas.gif\' width=\'12\' height=\'12\'><img style="cursor: pointer" onclick="TDEliminarRegistro(this,\''+tablaId+'\')" src=\'<%=BASEURL%>/images/botones/iconos/menos1.gif\' width=\'12\' height=\'12\' >';
                if(navigator.appName=='Microsoft Internet Explorer'){oCell = oRow.insertCell();}else{oCell = oRow.insertCell(-1);}oCell.align='left';oCell.style.backgroundColor='#EAFFEA';
                oCell.innerHTML = '<input  onkeyup="soloNumeros(this.id);" type="text" id="ri'+prefix+items+'" name="ri'+prefix+items+'" value='+val+' maxlength="5" size="5" style="text-align: right;">';
                if(navigator.appName=='Microsoft Internet Explorer'){oCell = oRow.insertCell();}else{oCell = oRow.insertCell(-1);}oCell.align='left';oCell.style.backgroundColor='#EAFFEA';
                oCell.innerHTML = '<input  onkeyup="soloNumeros(this.id);validateValueAnt(\''+fily+"_"+items+'\');" onblur="validateValue(\''+fily+"_"+items+'\');validateValueAnt(\''+fily+"_"+items+'\');" type="text" id="rf'+prefix+items+'" name="rf'+prefix+items+'" maxlength="5" size="5" style="text-align: right;">';
                if(navigator.appName=='Microsoft Internet Explorer'){oCell = oRow.insertCell();}else{oCell = oRow.insertCell(-1);}oCell.align='left';oCell.style.backgroundColor='#EAFFEA';
                oCell.innerHTML = '<input onkeyup="soloNumeros(this.id);this.value = formatNumber(this.value);" type="text" id="p'+prefix+items+'" name="p'+prefix+items+'" maxlength="5" size="5" style="text-align: right;">';//items++;
            }

            function validateValue(id){
                var vali = document.getElementById("riavales"+id).value * 1;
                var valf = document.getElementById("rfavales"+id).value * 1;
                if(valf<=vali){
                    document.getElementById("rfavales"+id).value = vali + 1;
                }
            }

            function validateValueAnt(id){
                var indic = id.substring(id.indexOf("_") + 1);
                var prev = id.substring(0,id.indexOf("_"));
                if((id.substring(id.indexOf("_") + 1 )*1)>1){
                    var vali = document.getElementById("rfavales"+ prev + '_' + (indic - 1)).value * 1;
                    var valf = document.getElementById("riavales"+id).value * 1;
                    if(valf<=vali){
                        document.getElementById("riavales"+id).value = vali + 1;
                    }
                }
            }

            function calculador(idv1,idv2){
                document.getElementById(idv2).value = 100 - unformatNumber(document.getElementById(idv1).value) * 1;
            }

            function delrow(row){
                var myTable = $("tconvs");
                var i=row.parentNode.parentNode.rowIndex;
                myTable.deleteRow(i);
                myTable.deleteRow(i);
            }

            function addrow(fit){
                fily += 1;
                fit = fily;
                fit -= 1;
                var celda = '<img alt="+" style="cursor: pointer" onclick="addrow('+fily+');" src="<%=BASEURL%>/images/botones/iconos/mas.gif" width="12" height="12" >'
                celda += '<img alt="-"style="cursor: pointer" onclick="delrow(this);" src="<%=BASEURL%>/images/botones/iconos/menos1.gif" width="12" height="12" >'
                var myTable = $("tconvs");
                var myRow = null;
                if(navigator.appName=='Microsoft Internet Explorer') myRow = myTable.insertRow();
                else myRow = myTable.insertRow(-1);
                myRow.className = "fila";
                var myCell = null;
                myCell = insertcelda(myRow,celda);
                myCell.style.width="26px";
                celda = '<select name="conv'+fily+'" id="conv'+fily+'" style="width: 100%;"  onchange="buscarDats('+fily+');">' +
                                '<option selected="selected" value="">...</option>'+
                                <%
                                    if(lista!=null){
                                        String[] split = null;
                                        for(int i=0;i<lista.size();i++){
                                            split = ((String)(lista.get(i))).split(";_;");
                                %>
                                 '<option value="<%=split[0]%>"><%=split[1]%></option>' +
                                <%
                                        }
                                    }
                                %>
                            '</select>';
                myCell = insertcelda(myRow,celda);
                myCell.style.width="85px";
                celda = '<select name="sector'+fily+'" id="sector'+fily+'" style="width: 100%;" onchange="buscarSubs('+fily+');">' +
                                '<option selected="selected" value="">...</option>'+
                                <%
                                    if(lista2!=null){
                                        String[] split = null;
                                        for(int i=0;i<lista2.size();i++){
                                            split = ((String)(lista2.get(i))).split(";_;");
                                %>
                                 '<option value="<%=split[0]%>"><%=split[1]%></option>' +
                                <%
                                        }
                                    }
                                %>
                            '</select>';
                myCell = insertcelda(myRow,celda);
                myCell.style.width="83px";
                celda = '<div id="subsector_"'+fily+'><select name="subsector'+fily+'" id="subsector'+fily+'" style="width: 100%;">' +
                                '<option selected="selected" value="">...</option>'+
                            '</select></div>';//<!-- 20101111 -->
                myCell = insertcelda(myRow,celda);
                myCell.style.width="83px";
                celda = '<input type="text" value="0" id="cobertura'+fily+'" name="cobertura'+fily+
                    '" onkeyup="soloNumeros(this.id);this.value = formatNumber(this.value);" style="text-align: right; width: 100%">';
                myCell = insertcelda(myRow,celda);
                myCell.style.width="77px";
                celda = '<input type="text" size="5" value="0" id="cobert_flotante'+fily+'" name="cobert_flotante'+fily+
                    '" onkeyup="soloNumeros(this.id);this.value = formatNumber(this.value);" style="text-align: right; width: 100%">' ;
                myCell = insertcelda(myRow,celda);
                myCell.style.width="45px";
                celda = '<input type="text" size="5" value="0" id="tasa'+fily+'" name="tasa'+fily+
                    '" onkeyup="soloNumeros(this.id);this.value = formatNumber(this.value);" style="text-align: right; width: 100%">' ;
                myCell = insertcelda(myRow,celda);
                myCell.style.width="45px";
                celda = '<input type="text" size="5" value="0" id="custodia'+fily+'" name="custodia'+fily+
                    '" onkeyup="soloNumeros(this.id);this.value = formatNumber(this.value);" style="text-align: right; width: 100%">' ;
                myCell = insertcelda(myRow,celda);
                myCell.style.width="47px";
                celda = '<input name="comision'+fily+'" id="comision'+fily+'" value="true" type="checkbox" onclick="uncheck(this,\''+fily+'\');">';
                myCell = insertcelda(myRow,celda);
                myCell.style.width="45px";
                myCell.align = "center";
                /*celda = '<select name="cuenta'+fily+'" id="cuenta'+fily+'" style="width: 100%;">' +
                                '<option selected="selected" value="">...</option>'+
                            '</select>';*/
                celda = '<input type="text" id="cuenta'+fily+'" name="cuenta'+fily+'" readonly value="" style="text-align: right; width: 100%">';
                myCell = insertcelda(myRow,celda);
                //myCell.style.width="85px";
                myCell.style.width="78px";
                celda = '<input type="text" size="5" value="0" id="porc_afil'+fily+'" name="porc_afil'+fily+
                    '" readonly onkeyup="soloNumeros(this.id);this.value = formatNumber(this.value);calculador(\'porc_afil'+fily+'\',\'porc_ven'+fily+'\');" style="text-align: right; width: 100%">' ;
                myCell = insertcelda(myRow,celda);
                myCell.style.width="50px";
                celda = '<input type="text" size="5" value="0" id="porc_ven'+fily+'" name="porc_ven'+fily+
                    '" onkeyup="soloNumeros(this.id);this.value = formatNumber(this.value);" readonly style="text-align: right; width: 100%">' ;
                myCell = insertcelda(myRow,celda);
                myCell.style.width="50px";
                if(navigator.appName=='Microsoft Internet Explorer') myRow = myTable.insertRow();
                else myRow = myTable.insertRow(-1);
                myRow.className = "fila";
                celda = '<table border="0" width="100%" id="tavales'+fily+'" style="text-align: left; margin-left: 30px; margin-right: auto; width: 200px;">' +
                                '<tr>' +
                                    '<td align="left" width="20%" class="fila">&nbsp;</td>' +
                                    '<td align="left" width="35%" class="fila">Inicio</td>' +
                                    '<td align="left" width="15%" class="fila">Fin</td>' +
                                    '<td align="left" width="25%" class="fila">Porcentaje</td>' +
                                '</tr>' +
                                '<tr>' +
                                    '<td align="center" class="fila">' +
                                        '<img alt="+" style="cursor: pointer" onclick="TDAgregarRegistro(\'tavales'+fily+'\',\'avales'+fily+'_\')" src="<%=BASEURL%>/images/botones/iconos/mas.gif" width="12" height="12" ><img alt="-" style="cursor: pointer" onclick="TDEliminarRegistro(this,\'tavales'+fily+'\')" src="<%=BASEURL%>/images/botones/iconos/menos1.gif" width="12" height="12" >' +
                                    '</td>' +
                                    '<td align="left" class="fila">' +
                                        '<input onkeyup="soloNumeros(this.id);" type="text" id="riavales'+fily+'_1" name="riavales'+fily+'_1" value="1" readonly maxlength="5" size="5" style="text-align: right;">' +
                                    '</td>' +
                                    '<td align="left" class="fila">' +
                                        '<input onkeyup="soloNumeros(this.id);" onblur="validateValue(\''+fily+'_1\');" type="text" id="rfavales'+fily+'_1" name="rfavales'+fily+'_1" maxlength="5" size="5" style="text-align: right;">' +
                                    '</td>' +
                                    '<td align="left" class="fila">' +
                                        '<input onkeyup="soloNumeros(this.id);this.value = formatNumber(this.value);" type="text" id="pavales'+fily+'_1" name="pavales'+fily+'_1" maxlength="5" size="5" style="text-align: right;">' +
                                    '</td>' +
                                '</tr>' +
                            '</table>';
                myCell = insertcelda(myRow,celda);
                myCell.colSpan = "12";
                document.getElementById("forma").action = '<%= CONTROLLER%>?estado=Proveedor&accion=Actualizar&cmd=show&maxfilas='+maxfilas+'&fils='+fily;
            }

            function insertcelda(rowobject,dato){
                var celdita = null;
                if(navigator.appName=='Microsoft Internet Explorer') celdita = rowobject.insertCell();
                else celdita = rowobject.insertCell(-1);
                celdita.innerHTML = dato;
                return celdita;
            }

            var indicador = 1;
            var codsust = '';
            function buscarSubs(filind, codsub){
                indicador = filind;
                codsust = codsub;
                var sel = document.getElementById("sector"+filind).selectedIndex;
                var valor = document.getElementById("sector"+filind).options[sel].value;
                var url = "<%= CONTROLLER%>?estado=Proveedor&accion=Ingresar";
                var p =  'opcion=buscarsub&idsect='+valor;
                new Ajax.Request(
                    url,
                    {
                        method: 'post',
                        parameters: p,
                        onComplete: llenarDiv
                    }
                );

            }

            function llenarDiv(response){//<!-- 20101111 -->
                document.getElementById("subsector_"+indicador).innerHTML = '<select name="subsector'+indicador+'" id="subsector'+indicador+'" style="width: 100%;">'+response.responseText+'</select>';
                escogerOption("subsector"+indicador,codsust);
            }

            function buscarDats(idfil){
                indicador = idfil;
                var sel = document.getElementById("conv"+idfil).selectedIndex;
                var valor = document.getElementById("conv"+idfil).options[sel].value;
                var url = "<%= CONTROLLER%>?estado=Proveedor&accion=Ingresar";
                var p =  'opcion=viewdats&idconv='+valor;
                new Ajax.Request(
                    url,
                    {
                        method: 'post',
                        parameters: p,
                        onComplete: llenarDiv2
                    }
                );
            }

            function llenarDiv2(response){
                var texto = response.responseText;
                var array = texto.split(";_;");
                document.getElementById("tasa"+indicador).value = formatNumber(array[0]);
                document.getElementById("custodia"+indicador).value = formatNumber(array[1]);
                document.getElementById("forma").action = '<%= CONTROLLER%>?estado=Proveedor&accion=Actualizar&cmd=show&maxfilas='+maxfilas+'&fils='+fily;
            }

            function escogerOption(id_sel, valor){
                var numero = $(id_sel).length;
                for(var ind = 0; ind < numero ; ind++){
                    if($(id_sel).options[ind].value==valor){
                        $(id_sel).options[ind].selected = true;
                        break;
                    }
                }
            }
            //<!-- 20101111  -->
    </script>

</head>
<body style="overflow: auto;">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Modificar Proveedor y/o Proprietario"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: auto;">
<%
        Proveedor prov = model.proveedorService.getProveedor();
		if( prov == null ) prov = new Proveedor();

		Identidad id = model.identidadService.obtIdentidad();
		
		String pais = id.getPais_name();        
		String dpto = id.getEstado_name();
		String ciudad = id.getCiudad_name();
		String sw = (request.getParameter("sw")!=null)?request.getParameter("sw"):"";
		
		TreeMap agencias = model.agenciaService.getAgencia();
		TreeMap ciudades = model.ciudadService.getTreMapCiudades();
		TreeMap bancos = model.servicioBanco.getBanco();
		TreeMap sucursales = model.servicioBanco.getSucursal();
	   
	   
	   agencias.put(" Seleccione", "");
	   agencias.remove("");
	   bancos.put(" Seleccione", "");
	   sucursales.put(" Seleccione", "");
	   ciudades.put(" Seleccione", "");        
		
		TreeMap clasificacion = new TreeMap();
		clasificacion.put("Default","Default");
		
		/*TreeMap tipcuenta = new TreeMap();
		tipcuenta.put("Ahorros","Ahorros");
		tipcuenta.put("Corriente","Corriente");*/
		TreeMap tipcuenta = model.tblgensvc.getLista_des();
		//System.out.println("Clasificacion "+prov.getC_clasificacion());
%>
<%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
<%String user = usuario.getLogin();%>
<%//Vector pvpag = model.perfil_vistaService.consultPerfil_vistas(user,"ProveedorUpdate.jsp");
	Vector pvpag= new Vector();%>
<%  //variables para el historial de modificacion
	//String agency = (String) session.getAttribute("agency");
	//String branch_code = (String) session.getAttribute("branch_code");
	//String bank_account = (String) session.getAttribute("bank_account");
%>
<form id ="forma" name="forma" action="<%= CONTROLLER%>?estado=Proveedor&accion=Actualizar&cmd=show<%= norefresh %>" method="post" bean="proveedor">

  <table width="785" border="2"align="center">
    <tr>
      <td>
          <input type="hidden" name="pag" id="pag" value="<%=pag%>">
       <table width="100%" align="center" class="tablaInferior" >
        <tr class="fila">
          <td colspan="2" align="left" class="subtitulo1">&nbsp;Documentaci&oacute;n</td>
          <td colspan="2" align="left" class="barratitulo"><img alt="" src="<%=BASEURL%>/images/titulo.gif"></td>
        </tr>
        <tr >
          <td width="133" align="left" class="fila" > Documento </td>
          <td width="236" valign="middle" class="letra" ><%= id.getCedula() %></td>
          <td width="172" valign="middle" class="fila">Tipo Documento</td>
          <td valign="middle" class="letra"><%= ( id.getTipo_iden().matches("CED"))? "C�dula" : "NIT" %></td>
        </tr>
        <!-- <tr>
          <td align="left" class="fila" >Id Mims</td>
          <td width="190" valign="middle" class="letra"><%= id.getId_mims() %></td>
          <td width="189" valign="middle" class="fila"><%if(control.equals("S")){%><input type="checkbox" name="aprobado" value="S" <%=prov.getAprobado().equals("S")?"checked":""%>>
          Aprobado
            <input type="hidden" name="evento" value="aprobar">
            <%}%></td>
          <td colspan="3" valign="middle" class="fila">&nbsp;</td>
        </tr> -->
        <%if(control.equals("S")){%>
        <tr>
            <td colspan="4" valign="middle" class="fila">
                <input type="checkbox" name="aprobado" value="S" <%=prov.getAprobado()!=null?prov.getAprobado().equals("S")?"checked":"":""%>>
                Aprobado
                <input type="hidden" name="evento" value="aprobar">
            </td>
        </tr>
        <%}%>
        <tr class="fila">
          <td colspan="2" align="left" class="subtitulo1">&nbsp;Datos Personales</td>
          <td colspan="2" align="left" class="barratitulo"><img alt="" src="<%=BASEURL%>/images/titulo.gif"></td>
        </tr>
        <tr>
          <td align="left" valign="top" class="fila">Nombre</td>
          <td colspan="3" valign="middle" class="letra"><%= id.getNombre()%></td>
        </tr>
        <tr>
          <td width="133"align="left" valign="top" class="fila" >Direcci&oacute;n</td>
          <td valign="middle" class="letra"><%= id.getDireccion() %></td>
          <td valign="top" class="fila">Barrio</td>
          <td valign="middle" class="letra"><%= id.getBarrio() %></td>
        </tr>
        <tr>
          <td width="133" align="left" valign="top" class="fila" >Pais</td>
          <td valign="middle" class="letra"><%= pais %></td>
          <td valign="top" class="fila">Estado</td>
          <td valign="middle" class="letra"><%= dpto %></td>
        </tr>
        <tr>
          <td width="133"align="left" valign="top" class="fila" >Ciudad</td>
          <td valign="middle" class="letra"><%= ciudad %></td>
          <td valign="top" class="fila">Email        
	  <td valign="middle" class="letra"><%= id.getE_mail() %>
        </tr>
		<tr>
		  <td align="left" valign="top" class="fila" >Telefonos</td>
		  <td colspan="3" valign="middle" class="letra"><%= id.getTelefono() %>
                      
              <input name="c_nit" type="hidden" id="c_nit" value="<%= id.getCedula() %>">
              <input name="c_idMims" type="hidden" id="c_idMims" value="">
              <input name="c_tipo_doc" type="hidden" id="c_tipo_doc" value="<%= id.getTipo_iden() %>">
         <input name="cla_iden" type="hidden" id="cla_iden" value="<%= id.getClasificacion() %>">          </tr>
		<tr>
		  <td align="left" valign="top" class="fila">Celular</td>
		  <td valign="middle" class="letra"><%= id.getCelular() %>
                  <td valign="top" class="fila">Tipo Proveedor</td>
                  <td>
                    <select id="tipo_proveedor" name="tipo_proveedor" style="width: 170px">
                    
                        <!--<option value="">...</option>
                        <option value="EMP">EMPLEADO</option>
                        <option value="ARL">ADMINISTRADORA RIESGOS LABORALES</option>
                        <option value="EPS">ENTIDAD PROMOTORA DE SALUD</option>
                        <option value="AFP">ADMINISTRADORA FONDO DE PENSIONES</option>
                        <option value="CCF">CAJA DE COMPENSACION FAMILIAR</option>-->
                        
                    </select>
                    <input name="id_tipo_proveedor" type="hidden" id="id_tipo_proveedor" value="<%= (prov.getTipoProveedor() != null) ? prov.getTipoProveedor():"" %>">
                  </td>
                  
		  <!--<td valign="top" class="fila">Email        
		  <td valign="middle" class="letra"><%= id.getE_mail() %>-->
		  </tr>
		     
        <tr class="fila">
          <td colspan="2" align="left" class="subtitulo1">&nbsp;Datos Proveedor</td>
          <td colspan="2" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
        </tr>
        <%
            //se obtiene la agencia del usuario en sesion para validaciones
                String agenciaP   = usuario.getId_agencia();
        %>
	<%
         if(pag.equals("S")){
        %>
        <tr class="fila">
            <td>Afiliado</td>
            <td>
                <input type="checkbox" name="afil" id="afil"  value="S" checked disabled>
            </td>
            <td>Sede</td>
            <td>
                <input type="checkbox" name="sede" id="sede" value="<%=sede%>" <% if(sede.equals("S")){%>checked<%}%> disabled>
            </td>
        </tr>
         <tr class="fila">
            <td>Codigo</td>
            <td>
                <input name="codigo" type="text" id="codigo" maxlength="2" value="<%=prov.getCodfen()!=null&&prov.getCodfen().length()==9?prov.getCodfen().substring(0, 2):""%>" size="2">
                <input name="codigo1" type="text" id="codigo1" maxlength="5" value="<%=prov.getCodfen()!=null&&prov.getCodfen().length()==9?prov.getCodfen().substring(2, 7):""%>" size="5">
                <input name="codigo2" type="text" id="codigo2" maxlength="2"value="<%=prov.getCodfen()!=null&&prov.getCodfen().length()==9?prov.getCodfen().substring(7, 9):""%>" size="2" >
            </td>
            <td></td>
            <td>
            </td>
        </tr>
         <%   }  %>
        <tr class="fila">
            <!-- 20100827 -->
            <td>Clasificacion</td>
            <td>
                <select name="c_clasificacion" class="textbox" id="select2">
                    <option value="">Seleccione Un Item</option>
                    <%
                        LinkedList tblcla = model.tablaGenService.getTblclasificacion();
			String var = (prov.getC_clasificacion()!=null)?prov.getC_clasificacion():"";
                        for(int i = 0; i<tblcla.size(); i++){
                            TablaGen cla = (TablaGen) tblcla.get(i);
                    %>
                    <option value="<%=cla.getTable_code()%>" <%=(cla.getTable_code().equals(var))?"selected":""%>  ><%=cla.getDescripcion()%></option>
                    <%}%>
                </select>
                <img alt="" src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
            </td>
            <td>Regimen</td>
            <td>
                <% String regimen = prov.getRegimen()!=null?prov.getRegimen():""; %>
                <select name="regimen" id="regimen" onChange="activecheck1(this.selectedIndex);">
                    <option value=""<%=regimen.equals("")?" selected":""%>>...</option>
                    <option value="C"<%=regimen.equals("C")?" selected":""%>>Comun</option>
                    <option value="S"<%=regimen.equals("S")?" selected":""%>>Simplificado</option>
                </select>
            </td>
        </tr>
        <tr class="fila">
            <td>Gran Contribuyente</td>
            <td>
                <select id="c_gran_contribuyente" name="c_gran_contribuyente" <%=regimen.equals("C")?"":""%> onChange="activecheck2(this.selectedIndex);">
  		   <option value="N"  <%= (prov.getC_gran_contribuyente().equals("N"))?"selected":"" %> >No</option>
   		   <option value="S" <%= (prov.getC_gran_contribuyente().equals("S"))?"selected":"" %> >Si</option>
                </select>
		<%if(control.equals("")){%>
		<input type="hidden" name="c_gran_contribuyente" value="<%=prov.getC_gran_contribuyente()%>">
		<%}%>
            </td>
            <td>Autoretenedor</td>
            <td>
                <select id="c_agente_retenedor" name="c_agente_retenedor"  <%=prov.getC_gran_contribuyente().equals("S")?"":""%> onChange="activecheck3(this.selectedIndex);">
                    <option value="N" <%= (prov.getC_agente_retenedor ().equals("N"))?"selected":"" %> >No</option>
                    <option value="S" <%= (prov.getC_agente_retenedor ().equals("S"))?"selected":"" %> >Si</option>
                </select>
		<%if(control.equals("")){%>
                <input type="hidden" name="c_agente_retenedor" value="<%=prov.getC_agente_retenedor ()%>">
		<%}%>
            </td>
        </tr>
        <tr class="fila">
          <td>Aplica IVA</td>
          <td>
              <select id="c_autoretenedor_iva" name="c_autoretenedor_iva" <%=prov.getC_agente_retenedor ().equals("N") && prov.getC_gran_contribuyente().equals("S")?"":""%>>
                <option value="N" <%= (prov.getC_autoretenedor_iva().equals("N"))?"selected":"" %> >No</option>
                <option value="S" <%= (prov.getC_autoretenedor_iva().equals("S"))?"selected":"" %> >Si</option>
              </select>
              <%if(control.equals("")){%>
              <input type="hidden" name="c_autoretenedor_iva" value="<%=prov.getC_autoretenedor_iva()%>">
              <%}%>
          </td>
          <td>Aplica ICA</td>
          <td>
                <select id="c_autoretenedor_ica" name="c_autoretenedor_ica" <%=prov.getC_agente_retenedor ().equals("N") && prov.getC_gran_contribuyente().equals("S")?"":""%>>
                    <option value="N" <%= (prov.getC_autoretenedor_ica().equals("N"))?"selected":"" %> >No</option>
                    <option value="S" <%= (prov.getC_autoretenedor_ica().equals("S"))?"selected":"" %> >Si</option>
                </select>
<!--                <input type="hidden" name="c_autoretenedor_ica" value="<%=prov.getC_autoretenedor_ica()%>">-->
          </td>
        </tr>
        <tr class="fila">
            <td>Aplica Retefuente</td>
            <td colspan="3">
              <select id="c_autoretenedor_rfte" name="c_autoretenedor_rfte" <%=prov.getC_agente_retenedor ().equals("N") && prov.getC_gran_contribuyente().equals("S")?"":""%> >
                <option value="N"  <%= (prov.getC_autoretenedor_rfte().equals("N"))?"selected":"" %> >No</option>
                <option value="S" <%= (prov.getC_autoretenedor_rfte().equals("S"))?"selected":"" %> >Si</option>
              </select>
              <%if(control.equals("")){%>
              <input type="hidden" name="c_autoretenedor_rfte" value="<%=prov.getC_autoretenedor_rfte()%>">
              <%}%>
            </td>
        </tr>
        <tr class="fila">
            <td>Handle Code </td>
            <td>
                <select name="handle_code" class="textbox" id="handle_code">
                  <option value="">Seleccione Un Item</option>
                  <%
                    LinkedList tblhc = model.tablaGenService.getTblhandlecode();
                    String varhc = (prov.getHandle_code()!=null)?prov.getHandle_code():"";
                    for(int i = 0; i<tblhc.size(); i++){
                        TablaGen hc = (TablaGen) tblhc.get(i);
                  %>
                  <option value="<%=hc.getTable_code()%>" <%=(hc.getTable_code().equals(varhc))?"selected":""%>  ><%=hc.getDescripcion()%></option>
                  <%}%>
                </select>
                <img alt="" src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
            </td>
            <td>Concepto</td>
            <td>
                <select name="concept_code" class="textbox" id="concept_code">
                    <option value="">Seleccione Un Item</option>
                    <%
                        System.out.println("..."+(prov.getC_payment_name())+"..."+(prov.getC_agency_id())+"..."+(prov.getC_bank_account())+"...");
                        LinkedList tblcon_pro = model.tablaGenService.getCon_proveedor();
                        String con = (prov.getConcept_code()!=null)?prov.getConcept_code():"";
                        for(int i = 0; i<tblcon_pro.size(); i++){
                            TablaGen con_pro = (TablaGen) tblcon_pro.get(i);
                    %>
                    <option value="<%=con_pro.getTable_code()%>" <%=(con_pro.getTable_code().equals(con))?"selected":""%>  ><%=con_pro.getDescripcion()%></option>
                    <%}%>
                </select>
            </td>
        </tr>
        <tr class="fila">
            <td>Nit Beneficiario</td>
            <td>
                <input name="nit_ben" type="text" class="textbox" onBlur="actualizar3();" value="<%=prov.getNit_beneficiario()!=null?prov.getNit_beneficiario():id.getCedula()%>">
		<a class="Simulacion_Hiper" style="cursor:pointer;" onClick="window.open('<%=BASEURL%>/consultas/consultasNit.jsp','','height=450,width=750,left=300,scrollbars=no,status=yes,resizable=yes')" >
                    Buscar Nit
                </a>
            </td>
            <td>Nombre Beneficiario</td >
	    <td colspan="3"><input name="nom_ben" type="text" class="filaresaltada" readonly style='width:90%;text-align:right; border:0;' value="<%=prov.getNom_beneficiario()!=null?prov.getNom_beneficiario():""%>">
            <img alt="" src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
        </tr>

        <tr class="fila">
          <td colspan="2" align="left" class="subtitulo1">&nbsp;Datos de pago</td>
          <td colspan="2" align="left" class="barratitulo"><img alt="" src="<%=BASEURL%>/images/titulo.gif">
          </td>
        </tr>
        <tr class="fila">
          <td>Sede de Pago  </td>
          <td><input:select name="c_agency_id" attributesText="class=textbox  onChange='actualizar2();'" options="<%= agencias %>" default="<%= prov.getC_agency_id() %>" />
              <img alt="" src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
          </td>
          <td>Banco
            <input name="c_payment_name" type="hidden" class="textbox" id="c_payment_name" value="<%= prov.getC_payment_name().equals("")?id.getNombre():prov.getC_payment_name() %>"></td>
          <td colspan="3">
              <input:select name="c_branch_code" default="<%= prov.getC_branch_code() %>" attributesText="class=textbox onChange='actualizar();'" options="<%= bancos %>" />
              <img alt="" src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
          </td>
        </tr>
        <tr class="fila">
          <td>Agencia</td>
          <td>
              <input:select name="c_bank_account" default="<%= prov.getC_bank_account() %>" attributesText="class=textbox" options="<%= sucursales %>" />
              <img alt="" src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
          </td>
          <td width="46">Plazo </td>
          <td width="67">
              <input name="plazo" type="text" id="plazo" size="4" maxlength="3" onKeyPress="soloDigitos(event,'decNO')" value="<%= prov.getPlazo() %>">
              <img alt="" src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">d&iacute;as
          </td>
	</tr>
        <tr class="fila">
           <!-- <td colspan="2">Realizar pagos por transferencia -->
           <td colspan="4">Realizar pagos por transferencia
               <input type="checkbox" name="pago" id="pago" onload ="activacion();" checked disabled >
               <!-- <input type="checkbox" name="pago" id="pago" onClick="imgProveedor();" <%=(prov.getTipo_pago().equals("T"))?"checked":""%> > -->
               <input name="tipo_pago" type="hidden" id="tipo_pago" value="T">
               <img alt="" src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
           </td>
          <!--<td>Porc Remesa </td> 20100812 rhonalf
          <td colspan="3"><input name="rem" type="text" class="textbox" id="rem" value="1.642" maxlength="5">
          %</td>-->
          <%String act = "";%>
        </tr>

        <!-- <tr class="fila" id="titulo">
          <td colspan="2" align="left" class="subtitulo1">&nbsp;Transferencia</td>
          <td colspan="2" align="left" class="barratitulo"><img alt="" src="<%=BASEURL%>/images/titulo.gif"></td>
        </tr> -->
        <!-- <tr class="fila" id="fila1" style="display:none"> -->
        <tr class="fila" id="fila1">
          <td>Banco Transferencia </td>
          <td>
              <select name="c_banco_transfer" class="textbox" id="select" <%=act%>>
                <option value="">Seleccione Un Item</option>
                <%
                    LinkedList tblban = model.tablaGenService.getTblBanco();
		    String varban = (prov.getC_banco_transfer()!=null)?prov.getC_banco_transfer():"";
                    for(int i = 0; i<tblban.size(); i++){
                       TablaGen ban = (TablaGen) tblban.get(i);
                %>
                <option value="<%=ban.getTable_code()%>" <%=(ban.getTable_code().equals(varban))?"selected":""%>  ><%=ban.getTable_code()%></option>
                <%}%>
              </select>
              <img alt="" name="logo"  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" ></td>
            <td>Sucursal Transferencia </td>
            <td>
                <input name="c_sucursal_transfer" type="text" class="textbox" id="c_sucursal_transfer" value="<%= prov.getC_sucursal_transfer() %>" maxlength="15" <%=act%>>
                <img alt="" name="logo"  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" >
            </td>
        </tr>
        <tr class="fila" id="fila2">
          <td>Tipo de Cuenta</td>
          <td>
              <input:select name="c_tipo_cuenta" options="<%= tipcuenta %>" default="<%= prov.getC_tipo_cuenta() %>" attributesText=" class= 'listmenu <%=act%>' id='c_tipo_cuenta' " />

               
              <img alt="" name="logo" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" >
          </td>
          <td>No. Cuenta</td>
          <td>
              <input name="c_numero_cuenta" type="text" class="textbox" id="c_numero_cuenta" value="<%= prov.getC_numero_cuenta()%>" size="20" maxlength="20" onKeyPress="soloDigitos(event,'decNO')" <%=act%>>
              <img alt="" name="logo2" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" >
          </td>
        </tr>
        <tr class="fila" id="fila3">
          <td>Ciudad</td>
          <td>
              <input:select name="c_codciudad_cuenta" attributesText="class='textbox <%=act%>' id='c_codciudad_cuenta'" options="<%= ciudades %>" default="<%= prov.getC_codciudad_cuenta() %>" />
              <img alt="" name="logo1" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" >
          </td>
          <td>Cedula de la cuenta </td>
          <td>
              <input id="cedula_cuenta" name="cedula_cuenta" type="text" class="textbox" value="<%= prov.getCedula_cuenta() %>" maxlength="15" onKeyPress="soloDigitos(event,'decNO')" <%=act%>>
              <img alt="" name="logo3"  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" >
          </td>
        </tr>
        <tr class="fila" id="fila4">
          <td>Nombre Cuenta </td>
          <td colspan="3">
              <input id="nombre_cuenta" name="nombre_cuenta" type="text" size="84" class="textbox" maxlength="100" value="<%= prov.getNombre_cuenta() %>" <%=act%>>
              <img alt="" name="logo4" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" >
          </td>
        </tr>
		
          
      </table>	  </td>
    </tr>
  <!-- </table>
  <table width="783" border="2" align="center"> -->
    <tr>
      <td>
        <table width="100%" class="tablaInferior">
          <tr>
            <td width="392" height="24"  class="subtitulo1">Referencias</td>
            <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20">
            </td>
          </tr>
        </table>
        <table width="100%" class="tablaInferior">
          <tr class="letra">
            <td height="22" ><a style="cursor:pointer;" class="Simulacion_Hiper" onClick="ReferenciaPropietario('<%=CONTROLLER%>','2')">Modificar referencias a Propietario </a></td>
          </tr>
      </table></td> 
    </tr>
  </table>
  <div align="center"><br>
      <img alt="" src="<%=BASEURL%>/images/botones/modificar.gif" name="c_modificar" onClick="forma.action = '<%=url4%>'; validarProveedor();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"  style="cursor:hand ">
      &nbsp; <img src="<%=BASEURL%>/images/botones/anular.gif" name="c_anular" onClick="forma.action = '<%=url2%>'; forma.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand "><%if(norefresh.equals("OK")){%>
      &nbsp;<img src="<%= BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="window.location='<%=CONTROLLER%>?estado=Proveedor&accion=Buscar&new=ok'" onMouseOut="botonOut(this);"  style="cursor:hand "><%}%>
      &nbsp;<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_buscar" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">
  </div>
</form>
<%
      if( request.getParameter("msg")!=null ) {
%>
<table border="2" align="center">
  <tr>
    <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="282" align="center" class="mensajes"><%=request.getParameter("msg").toString() %></td>
          <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="78">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<%
	}
%>
</div>
</body>
</html>
<%if(request.getAttribute ("msg")!=null){%>
	<script>
		alert('<%=request.getAttribute ("msg")%>');
        </script>
<%}%>
<link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
<script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script> 
<script type="text/javascript" src="./js/recursosHumanos/admonRecursosHumanos.js"></script>
<script>
        $j = jQuery.noConflict();
        inicio1();
</script>