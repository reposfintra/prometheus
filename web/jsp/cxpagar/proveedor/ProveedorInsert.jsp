<!--
- Autor : Ing. Tito Andr�s Maturana
- Date  : 30 septiembre del 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : P�gina JSP, que maneja el ingreso en el archivo de proveedor.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Proveedor - Ingresar</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">

<script type="text/javascript" src='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">      
        
<script type='text/javascript' src="<%= BASEURL %>/js/hvida.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/referencia.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/prototype.js"></script>
<%
        String control = session.getAttribute("Control")!=null?(String) session.getAttribute("Control"):"";
        Proveedor prov = model.proveedorService.getProveedor();
        if( prov == null ) prov = new Proveedor();

        Identidad id = model.identidadService.obtIdentidad();

        String pais = id.getPais_name();
        String dpto = id.getEstado_name();
        String ciudad = id.getCiudad_name();
        String sw = (request.getParameter("sw")!=null)?request.getParameter("sw"):"";
        TreeMap agencias = model.agenciaService.getAgencia();
        TreeMap ciudades = model.ciudadService.getTreMapCiudades();
        TreeMap bancos = model.servicioBanco.getBanco();
	TreeMap sucursales = model.servicioBanco.getSucursal();
        TreeMap tipcuenta = model.tblgensvc.getLista_des();


	   agencias.put(" Seleccione", "");
	   agencias.remove("");
	   bancos.put(" Seleccione", "");
	   sucursales.put(" Seleccione", "");
	   ciudades.put(" Seleccione", "");
           

        TreeMap clasificacion = new TreeMap();
        clasificacion.put("Default","Default");

        /*TreeMap tipcuenta = new TreeMap();
        tipcuenta.put("Ahorros","Ahorros");
        tipcuenta.put("Corriente","Corriente");*/

%>

<%
        String url = CONTROLLER + "?estado=ProveedorUpdate&accion=Ingresar&cmd=show";
	String url2 = CONTROLLER + "?estado=Consultar&accion=Nit&carpeta=/jsp/cxpagar/proveedor/&pagina=ProveedorInsert.jsp";

        String sede = request.getParameter("sede")!=null ? request.getParameter("sede") : "";
        String afil = request.getParameter("afil")!= null ? request.getParameter("afil") : "N";


%>
<script type="text/javascript">
        function actualizar(){
            forma.action = '<%=url%>&afil=<%=afil%>&sede=<%=sede%>';
            forma.submit();
        }
        function actualizar2(){
            forma.action = '<%=url%>&agc=OK&afil=<%=afil%>&sede=<%=sede%>';
            forma.submit();
        }
        function actualizar3(){
            forma.action = '<%=url2%>';
            forma.submit();
        }
</script>
<script type="text/javascript">
            function activacion(){
                var seleccionado = document.getElementById("pago").checked;
                if(seleccionado==true){
                    $("select").disabled=false;
                    $("c_sucursal_transfer").disabled=false;
                    $("c_tipo_cuenta").disabled=false;
                    $("c_numero_cuenta").disabled=false;
                    $("c_codciudad_cuenta").disabled=false;
                    $("cedula_cuenta").disabled=false;
                    $("nombre_cuenta").disabled=false;
                }
                else{
                    $("select").disabled=true;
                    $("select").options[0].selected = true;
                    $("c_sucursal_transfer").disabled=true;
                    $("c_sucursal_transfer").value="";
                    $("c_tipo_cuenta").disabled=true;
                    $("c_tipo_cuenta").options[0].selected = true;
                    $("c_numero_cuenta").disabled=true;
                    $("c_numero_cuenta").value="";
                    $("c_codciudad_cuenta").disabled=true;
                    $("c_codciudad_cuenta").options[0].selected = true;
                    $("cedula_cuenta").disabled=true;
                    $("cedula_cuenta").value="";
                    $("nombre_cuenta").disabled=true;
                    $("nombre_cuenta").value="";
                }
            }

            function checkselect(idselect,indice){
                if(indice>0){
                    if(indice==1){
                        $(idselect).disabled=false;
                    }
                    else{
                        $(idselect).disabled=true;
                        $(idselect).options[0].selected = true;
                    }
                }
                else{
                    $(idselect).disabled=true;
                    $(idselect).options[0].selected = true;
                }
            }

            function activecheck1(indice){
                checkselect("c_gran_contribuyente",indice);
                if(indice!=1){
                    checkselect("c_agente_retenedor",0);
                    checkselect("c_autoretenedor_iva",0);
                    checkselect("c_autoretenedor_ica",0);
                    checkselect("c_autoretenedor_rfte",0);
                }else if(indice ===1){
                    checkselect("c_agente_retenedor",indice);
                    checkselect("c_autoretenedor_iva",indice);
                    checkselect("c_autoretenedor_ica",indice);
                    checkselect("c_autoretenedor_rfte",indice);
                    
                }
            }

            function activecheck2(indice){
//                checkselect("c_agente_retenedor",indice);
//                if(indice==0){
//                    checkselect("c_autoretenedor_iva",0);
//                    checkselect("c_autoretenedor_ica",0);
//                    checkselect("c_autoretenedor_rfte",0);
//                }
//                else{
//                    checkselect("c_autoretenedor_iva",indice);
//                    checkselect("c_autoretenedor_ica",indice);
//                    checkselect("c_autoretenedor_rfte",indice);
//                }
            }

            function activecheck3(indice){
 //               if(indice==0){
 //                  checkselect("c_autoretenedor_iva",1);
 //                   checkselect("c_autoretenedor_ica",1);
 //                   checkselect("c_autoretenedor_rfte",1);
 //               }
 //               else{
 //                   checkselect("c_autoretenedor_iva",0);
 //                   checkselect("c_autoretenedor_ica",0);
 //                   checkselect("c_autoretenedor_rfte",0);
 //               }
            }
</script>
<!-- <body onresize="redimensionar()" onload = "imgProveedor();redimensionar();"> -->
</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Ingresar Proveedor y/o Proprietario"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<form id ="forma" name="forma" action="<%= CONTROLLER%>?estado=Proveedor&accion=Ingresar&cmd=show" method="post">

  <table width="785" border="2"align="center">
    <tr>
      <td>
	  <table width="100%" align="center" >
        <tr>
          <td colspan="2" align="left" class="subtitulo1">&nbsp;Documentaci&oacute;n</td>
          <td colspan="2" align="left" class="barratitulo"><img alt="" src="<%=BASEURL%>/images/titulo.gif"></td>
        </tr>
        <tr>
          <td width="133" align="left" class="fila" > Documento </td>
          <td width="236" valign="middle" class="letra" ><%= id.getCedula() %></td>
          <td width="172" valign="middle" class="fila">Tipo Documento</td>
          <td valign="middle" class="letra"><%= ( id.getTipo_iden().matches("CED"))? "C�dula" : "NIT" %></td>
        </tr>
        <%if(control.equals("S")){%>
        <tr>
            <td colspan="4" valign="middle" class="fila">
                <input type="checkbox" name="aprobado" value="S" <%=prov.getAprobado()!=null?prov.getAprobado().equals("S")?"checked":"":""%>>
                Aprobado
                <input type="hidden" name="evento" value="aprobar">
            </td>
        </tr>
        <%}%>
        <tr class="fila">
          <td colspan="2" align="left" class="subtitulo1">&nbsp;Datos Personales</td>
          <td colspan="2" align="left" class="barratitulo"><img alt="" src="<%=BASEURL%>/images/titulo.gif"></td>
        </tr>
        <tr>
          <td align="left" valign="top" class="fila">Nombre</td>
          <td colspan="3" valign="middle" class="letra"><%= id.getNombre()%></td>
        </tr>
        <tr>
          <td width="133"align="left" valign="top" class="fila" >Direcci&oacute;n</td>
          <td valign="middle" class="letra"><%= id.getDireccion() %></td>
          <td valign="top" class="fila">Barrio</td>
          <td valign="middle" class="letra"><%= id.getBarrio() %></td>
        </tr>
        <tr>
          <td width="133" align="left" valign="top" class="fila" >Pais</td>
          <td valign="middle" class="letra"><%= pais %></td>
          <td valign="top" class="fila">Estado</td>
          <td valign="middle" class="letra"><%= dpto %></td>
        </tr>
        <tr>
          <td width="133"align="left" valign="top" class="fila" >Ciudad</td>
          <td valign="middle" class="letra"><%= ciudad %></td>
          <td valign="top" class="fila">Email        
	  <td valign="middle" class="letra"><%= id.getE_mail() %>
        </tr>
	<tr>
		  <td align="left" valign="top" class="fila">Telefonos</td>
		  <td colspan="3" valign="middle" class="letra"><%= id.getTelefono() %>
              <input name="c_nit" type="hidden" id="c_nit" value="<%= id.getCedula() %>">
              <input name="c_idMims" type="hidden" id="c_idMims" value="">
              <!-- <input name="c_idMims" type="hidden" id="c_idMims" value="<%= id.getId_mims() %>"> -->
              <input name="c_tipo_doc" type="hidden" id="c_tipo_doc" value="<%= id.getTipo_iden() %>">
			  <input name="cla_iden" type="hidden" id="cla_iden" value="<%= id.getClasificacion() %>">
        </tr>
	<tr>
		  <td align="left" valign="top" class="fila" >Celular</td>
		  <td valign="middle" class="letra"><%= id.getCelular() %>
                  <td valign="top" class="fila">Tipo Proveedor</td>
                  <td>
                    <select id="tipo_proveedor" name="tipo_proveedor" style="width: 170px">
                        <!--<option value="">...</option>
                        <option value="EMP">EMPLEADO</option>
                        <option value="ARL">ADMINISTRADORA RIESGOS LABORALES</option>
                        
                        
                        <option value="EPS">ENTIDAD PROMOTORA DE SALUD</option>
                        <option value="AFP">ADMINISTRADORA FONDO DE PENSIONES</option>
                        <option value="CCF">CAJA DE COMPENSACION FAMILIAR</option>-->
                        
                    </select>
                  </td>
		  <!--<td valign="top" class="fila">Email
		  <td valign="middle" class="letra"><%= id.getE_mail() %>-->
	</tr>

        <tr class="fila">
          <td colspan="2" align="left" class="subtitulo1">&nbsp;Datos Proveedor</td>
          <td colspan="2" align="left" class="barratitulo"><img alt="" src="<%=BASEURL%>/images/titulo.gif"></td>
        </tr>
        <input type="hidden" name="pag" id="pag" value="<%=afil%>">
        <%
         if(afil.equals("S")){
        %>
        <tr class="fila">
            <td>Afiliado</td>
            <td>
                <input type="checkbox" name="afil" id="afil"  value="<%=afil%>" checked disabled>
            </td>
            <td>Sede</td>
            <td>
                <input type="checkbox" name="sede" id="sede" value="<%=sede%>" <% if(sede.equals("S")){%>checked<%}%> disabled >
            </td>
        </tr>
        <tr class="fila">
            <td>Codigo</td>
            <td>
                <input name="codigo" type="text" id="codigo" maxlength="2" value="<%=prov.getCodfen()!=null&&prov.getCodfen().length()==9?prov.getCodfen().substring(0, 2):""%>" size="2">
                <input name="codigo1" type="text" id="codigo1" maxlength="5" value="<%=prov.getCodfen()!=null&&prov.getCodfen().length()==9?prov.getCodfen().substring(2, 7):""%>" size="5">
                <input name="codigo2" type="text" id="codigo2" maxlength="2"value="<%=prov.getCodfen()!=null&&prov.getCodfen().length()==9?prov.getCodfen().substring(7, 9):""%>" size="2" >
            </td>
            <td></td>
            <td>
            </td>
        </tr>
         <%   }  %>
        <tr class="fila">
            <!-- 20100827 -->
            <td>Clasificacion</td>
            <td>
                <select name="c_clasificacion" class="textbox" id="select2">
                    <option value="">Seleccione Un Item</option>
                    <%
                        LinkedList tblcla = model.tablaGenService.getTblclasificacion();
			String var = (prov.getC_clasificacion()!=null)?prov.getC_clasificacion():"";
                        for(int i = 0; i<tblcla.size(); i++){
                            TablaGen cla = (TablaGen) tblcla.get(i);
                    %>
                    <option value="<%=cla.getTable_code()%>" <%=(cla.getTable_code().equals(var))?"selected":""%>  ><%=cla.getDescripcion()%></option>
                    <%}%>
                </select>
                <img alt="" src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
            </td>
            <td>Regimen</td>
            <td>
                <% String regimen = prov.getRegimen()!=null?prov.getRegimen():""; %>
                <select name="regimen" id="regimen" onchange="activecheck1(this.selectedIndex);">
                    <option value=""<%=regimen.equals("")?" selected":""%>>...</option>
                    <option value="C"<%=regimen.equals("C")?" selected":""%>>Comun</option>
                    <option value="S"<%=regimen.equals("S")?" selected":""%>>Simplificado</option>
                </select>
            </td>
        </tr>
        <tr class="fila">
            <td>Gran Contribuyente</td>
            <td>
                <select id="c_gran_contribuyente" name="c_gran_contribuyente" <%=control.equals("S")?"":""%> onchange="activecheck2(this.selectedIndex);">
  		   <option value="N"  <%= (prov.getC_gran_contribuyente().equals("N"))?"selected":"" %> >No</option>
   		   <option value="S" <%= (prov.getC_gran_contribuyente().equals("S"))?"selected":"" %> >Si</option>
                </select>
		<%if(control.equals("")){%>
		<input type="hidden" name="c_gran_contribuyente" value="<%=prov.getC_gran_contribuyente()%>">
		<%}%>
            </td>
            <td>Autoretenedor</td>
            <td>
                <select id="c_agente_retenedor" name="c_agente_retenedor"  <%=control.equals("S")?"":""%> onchange="activecheck3(this.selectedIndex);">
                    <option value="N" <%= (prov.getC_agente_retenedor ().equals("N"))?"selected":"" %> >No</option>
                    <option value="S" <%= (prov.getC_agente_retenedor ().equals("S"))?"selected":"" %> >Si</option>
                </select>
		<%if(control.equals("")){%>
                <input type="hidden" name="c_agente_retenedor" value="<%=prov.getC_agente_retenedor ()%>">
		<%}%>
            </td>
        </tr>
        <tr class="fila">
          <td>Aplica IVA</td>
          <td>
              <select id="c_autoretenedor_iva" name="c_autoretenedor_iva" <%=control.equals("S")?"":""%>>
                <option value="N" <%= (prov.getC_autoretenedor_iva().equals("N"))?"selected":"" %> >No</option>
                <option value="S" <%= (prov.getC_autoretenedor_iva().equals("S"))?"selected":"" %> >Si</option>
              </select>
              <%if(control.equals("")){%>
              <input type="hidden" name="c_autoretenedor_iva" value="<%=prov.getC_autoretenedor_iva()%>">
              <%}%>
          </td>
          <td>Aplica ICA</td>
          <td>
                <select id="c_autoretenedor_ica" name="c_autoretenedor_ica" >
                    <option value="N" <%= (prov.getC_autoretenedor_ica().equals("N"))?"selected":"" %> >No</option>
                    <option value="S" <%= (prov.getC_autoretenedor_ica().equals("S"))?"selected":"" %> >Si</option>
                </select>
<!--             <input type="hidden" name="c_autoretenedor_ica" value="<%=prov.getC_autoretenedor_ica()%>">-->
          </td>
        </tr>
        <tr class="fila">
            <td>Aplica Retefuente</td>
            <td colspan="3">
              <select id="c_autoretenedor_rfte" name="c_autoretenedor_rfte" <%=control.equals("S")?"":""%> >
                <option value="N"  <%= (prov.getC_autoretenedor_rfte().equals("N"))?"selected":"" %> >No</option>
                <option value="S" <%= (prov.getC_autoretenedor_rfte().equals("S"))?"selected":"" %> >Si</option>
              </select>
              <%if(control.equals("")){%>
              <input type="hidden" name="c_autoretenedor_rfte" value="<%=prov.getC_autoretenedor_rfte()%>">
              <%}%>
            </td>
        </tr>
        <tr class="fila">
            <td>Handle Code </td>
            <td>
                <select name="handle_code" class="textbox" id="handle_code">
                  <option value="">Seleccione Un Item</option>
                  <%
                    LinkedList tblhc = model.tablaGenService.getTblhandlecode();
                    String varhc = (prov.getHandle_code()!=null)?prov.getHandle_code():"";
                    for(int i = 0; i<tblhc.size(); i++){
                        TablaGen hc = (TablaGen) tblhc.get(i);
                  %>
                  <option value="<%=hc.getTable_code()%>" <%=(hc.getTable_code().equals(varhc))?"selected":""%>  ><%=hc.getDescripcion()%></option>
                  <%}%>
                </select>
                <img alt="" src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
            </td>
            <td>Concepto</td>
            <td>
                <select name="concept_code" class="textbox" id="concept_code">
                    <option value="">Seleccione Un Item</option>
                    <%
                        LinkedList tblcon_pro = model.tablaGenService.getCon_proveedor();
                        String con = (prov.getConcept_code()!=null)?prov.getConcept_code():"";
                        for(int i = 0; i<tblcon_pro.size(); i++){
                            TablaGen con_pro = (TablaGen) tblcon_pro.get(i);
                    %>
                    <option value="<%=con_pro.getTable_code()%>" <%=(con_pro.getTable_code().equals(con))?"selected":""%>  ><%=con_pro.getDescripcion()%></option>
                    <%}%>
                </select>
            </td>
        </tr>
        <tr class="fila">
            <td>Nit Beneficiario</td>
            <td>
                <input name="nit_ben" type="text" class="textbox" onblur="actualizar3();" value="<%=prov.getNit_beneficiario()!=null?prov.getNit_beneficiario():id.getCedula()%>">
		<a class="Simulacion_Hiper" style="cursor:pointer;" onClick="window.open('<%=BASEURL%>/consultas/consultasNit.jsp','','height=450,width=750,left=300,scrollbars=no,status=yes,resizable=yes')" >
                    Buscar Nit
                </a>
            </td>
            <td>Nombre Beneficiario</td >
	    <td colspan="3"><input name="nom_ben" type="text" class="filaresaltada" readonly style='width:90%;text-align:right; border:0;' value="<%=prov.getNom_beneficiario()!=null?prov.getNom_beneficiario():""%>">
            <img alt="" src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
        </tr>

        <tr class="fila">
          <td colspan="2" align="left" class="subtitulo1">&nbsp;Datos de pago</td>
          <td colspan="2" align="left" class="barratitulo"><img alt="" src="<%=BASEURL%>/images/titulo.gif"></td>
        </tr>
        <tr class="fila">
          <td>Sede de Pago  </td>
          <td><input:select name="c_agency_id" attributesText="class=textbox  onChange='actualizar2()'" options="<%= agencias %>" default="<%= prov.getC_agency_id() %>" />
              <img alt="" src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
          </td>
          <td>Banco
            <input name="c_payment_name" type="hidden" class="textbox" id="c_payment_name" value="<%= prov.getC_payment_name().equals("")?id.getNombre():prov.getC_payment_name() %>"></td>
          <td colspan="3">
              <input:select name="c_branch_code" default="<%= prov.getC_branch_code() %>" attributesText="class=textbox onChange='actualizar()'" options="<%= bancos %>" />
              <img alt="" src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
          </td>
        </tr>
        <tr class="fila">
          <td>Agencia</td>
          <td>
              <input:select name="c_bank_account" default="<%= prov.getC_bank_account() %>" attributesText="class=textbox" options="<%= sucursales %>" />
              <img alt="" src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
          </td>
          <td width="46">Plazo </td>
          <td width="67">
              <input name="plazo" type="text" id="plazo" size="4" maxlength="3" onKeyPress="soloDigitos(event,'decNO')" value="<%= prov.getPlazo() %>">
              <img alt="" src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">d&iacute;as
          </td>
	</tr>

		 <!-- 20101208 rhonalf
		<tr class="fila">
           <td >Tasa de Redescuento </td>
		   <td ><input name="tasafenalco" type="text" class="textbox" id="tasafenalco" value="2.3" maxlength="7">
	          %</td>
           <td>Valor Custodia Cheque </td>
          <td colspan="3"><input name="custodiacheque" type="text" class="textbox" id="custodiacheque" value="1450" maxlength="15">            </td>
        </tr>
		-->
	<tr class="fila">
           <!-- <td colspan="2">Realizar pagos por transferencia -->
           <td colspan="4">Realizar pagos por transferencia
               <input type="checkbox" name="pago" id="pago" onload ="activacion();" checked disabled  >
               <!-- <input type="checkbox" name="pago" id="pago" onClick="imgProveedor();" <%=(prov.getTipo_pago().equals("T"))?"checked":""%> > -->
               <input name="tipo_pago" type="hidden" id="tipo_pago" value="T">
               <img alt="" src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
           </td>
          <!--<td>Porc Remesa </td> 20100812 rhonalf
          <td colspan="3"><input name="rem" type="text" class="textbox" id="rem" value="1.642" maxlength="5">
          %</td>-->
        </tr>

        <!-- <tr class="fila" id="titulo">
          <td colspan="2" align="left" class="subtitulo1">&nbsp;Transferencia</td>
          <td colspan="2" align="left" class="barratitulo"><img alt="" src="<%=BASEURL%>/images/titulo.gif"></td>
        </tr> -->
        <!-- <tr class="fila" id="fila1" style="display:none"> -->
        <tr class="fila" id="fila1">
          <td>Banco Transferencia </td>
          <td>
              <select name="c_banco_transfer" class="textbox" id="select" disabled>
                <option value="">Seleccione Un Item</option>
                <%
                    LinkedList tblban = model.tablaGenService.getTblBanco();
		    String varban = (prov.getC_banco_transfer()!=null)?prov.getC_banco_transfer():"";
                    for(int i = 0; i<tblban.size(); i++){
                       TablaGen ban = (TablaGen) tblban.get(i);
                %>
                <option value="<%=ban.getTable_code()%>" <%=(ban.getTable_code().equals(varban))?"selected":""%>  ><%=ban.getTable_code()%></option>
                <%}%>
              </select>
              <img alt="" name="logo"  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" ></td>
            <td>Sucursal Transferencia </td>
            <td>
                <input name="c_sucursal_transfer" type="text" class="textbox" id="c_sucursal_transfer" value="<%= prov.getC_sucursal_transfer() %>" maxlength="15" disabled>
                <img alt="" name="logo"  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" >
            </td>
        </tr>
        <tr class="fila" id="fila2">
          <td>Tipo de Cuenta</td>
          <td>
              <input:select name="c_tipo_cuenta" attributesText="class=listmenu disabled id='c_tipo_cuenta'" options="<%= tipcuenta %>" default="<%= prov.getC_tipo_cuenta() %>" />
              <img alt="" name="logo" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" >
          </td>
          <td>No. Cuenta</td>
          <td>
              <input name="c_numero_cuenta" type="text" class="textbox" id="c_numero_cuenta" value="<%= prov.getC_numero_cuenta()%>" size="20" maxlength="20" onKeyPress="soloDigitos(event,'decNO')" disabled>
              <img alt="" name="logo2" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" >
          </td>
        </tr>
        <tr class="fila" id="fila3">
          <td>Ciudad</td>
          <td>
              <input:select name="c_codciudad_cuenta" attributesText="class=textbox disabled id='c_codciudad_cuenta'" options="<%= ciudades %>" default="<%= prov.getC_codciudad_cuenta() %>" />
              <img alt="" name="logo1" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" >
          </td>
          <td>Cedula de la cuenta </td>
          <td>
              <input id="cedula_cuenta" name="cedula_cuenta" type="text" class="textbox" value="<%= prov.getCedula_cuenta() %>" maxlength="15" onKeyPress="soloDigitos(event,'decNO')" disabled>
              <img alt="" name="logo3"  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" >
          </td>
        </tr>
        <tr class="fila" id="fila4">
          <td>Nombre Cuenta </td>
          <td colspan="3">
              <input id="nombre_cuenta" name="nombre_cuenta" type="text" size="84" class="textbox" maxlength="100" value="<%= prov.getNombre_cuenta() %>" disabled>
              <img alt="" name="logo4" src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" >
          </td>
        </tr>
      </table>
     </td>
    </tr>
  <!-- </table>
   <div align="center">
    <table width="783" border="2" align="center">-->
      <tr>
        <td>
          <table width="100%" class="tablaInferior">
            <tr>
              <td width="392" height="24"  class="subtitulo1">Referencias</td>
              <td width="392"  class="barratitulo"><img alt="" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
            </tr>
          </table>
          <table width="100%" class="tablaInferior">
            <tr class="letra">
              <td height="22" ><a style="cursor:pointer;" class="Simulacion_Hiper" onClick="ReferenciaPropietario('<%=CONTROLLER%>','1')">Agregar referencias a Propietario</a> </td>
            </tr>
        </table></td>
      </tr>
    </table>
    <br>
    <div align="center">
        <img alt="" src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="validarProveedor();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:pointer;">
        &nbsp; <img alt="" src="<%= BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="window.location='<%=CONTROLLER%>?estado=Proveedor&accion=Buscar&new=ok'" onMouseOut="botonOut(this);"  style="cursor:pointer;">
        &nbsp; <img alt="" src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);"  style="cursor:pointer;">
    </div>
</form>
</div>
</body>
</html>
<%if(request.getAttribute ("msg")!=null){%>
	<script type="text/javascript">
		alert('<%=request.getAttribute ("msg")%>');
	</script>
<%}%>
<script type="text/javascript">
    activacion();
</script>
<link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
<script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script> 
<script type="text/javascript" src="./js/recursosHumanos/admonRecursosHumanos.js"></script>
<script>
        $j = jQuery.noConflict();
        inicio1();
</script>