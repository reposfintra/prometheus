<!--
- Autor : Ing. Tito Andr�s Maturana
- Date  : 30 septiembre del 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : P�gina JSP, que maneja el ingreso en el archivo de proveedor.
--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Proveedor - Aprobar</title>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script src="<%=BASEURL%>/js/validar.js"></script>
</head>
<body onresize="redimensionar()" onload = "redimensionar();doFieldFocus( forma );">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Aprobar Proveedor y/o Propietario"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="forma" id="forma" method="post" action="<%=CONTROLLER%>?estado=Proveedor&accion=Buscar&cmd=show"  onSubmit="return validarTCamposLlenos();">
  <table width="400" border="2" align="center">
    <tr>
      <td ><table width="100%">
        <tr>
          <td width="173" class="subtitulo1">&nbsp;Informaci&oacute;n</td>
          <td width="205" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
        </tr>
      </table>
      <table width="100%" align="center">
          <tr class="fila">
            <td width="22%"><div align="left">Documento</div></td>
            <%
        String nit = "";
        nit = request.getParameter("nit");
        if(nit==null) nit = "";              
%>
            <td width="78%"><input name="nit" type="text" class="textbox" id="nit2" value="<%=nit %>" onKeyPress="soloDigitos(event,'decNo')" onKeyUp="if(event.keyCode== 13){forma.submit()}"> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
              <input type="hidden" name="evento" value="consultar"></td>
          </tr>
        </table>        </td>
    </tr>
  </table>
  <div align="center"><br>
    <img src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="if ( validarTCamposLlenos() ) forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">&nbsp; <img src="<%= BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="forma.nit.value='';" onMouseOut="botonOut(this);"  style="cursor:hand ">&nbsp; <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);"  style="cursor:hand "></div>
</form>

<%if(request.getParameter("msg")!=null){%>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="40" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes"><%=request.getParameter("msg").toString() %></td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
	
<%}%>
</div>
<%=datos[1]%>
</body>
</html>
