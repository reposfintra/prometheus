<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE></TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<style type="text/css">
<!--
.Estilo2 {
	font-size: 16pt;
	color: #FF0000;
}
-->
</style>
</HEAD>
<BODY> 

        <table width="90%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20" align="center">MANUAL DE REGISTRO DE PROVEEDOR Y / O PROPIETARIO</td>
          </tr>
          <tr class="subtitulo1">
            <td> Registro de Proveedor y / o Propietario </td>
          </tr>
          <tr>
            <td  height="8"  class="ayudaHtmlTexto"><p>Para poder almacenar un proveedor o propietario es necesario que el documento (Cedula de Ciudadan&iacute;a o Nit) se encuentre registrado en nuestra base de datos y tenga la clasificaci&oacute;n de proveedor o propietario.</p>            </td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/proveedor/imagen1.JPG" ></div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><p>Si el Documento no existe almacenado en la base datos el sistema le muestra el siguiente mensaje</p>
            </td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center">
              <img src="<%=BASEURL%>/images/ayuda/proveedor/imagen2.JPG" > </div></td>
          </tr>
<tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><p>Si el Documento existe almacenado en la base datos, pero la clasificaci&oacute;n no corresponde a proveedor y/o propietario el sistema le muestra el siguiente mensaje</p>
            </td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center">
              <img src="<%=BASEURL%>/images/ayuda/proveedor/image2.JPG"> </div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center">Si el documento digitado se encuentra almacenado en la base de datos el sistema muestra la informaci&oacute;n correspondiente a ese documento. </td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/proveedor/imagen3.JPG" ></div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center">Para registrar que un proveedor se le efectu&eacute; el pago por transferencia, debe chequear esta opci&oacute;n y el sistema le adiciona al formulario los campos que debe llenar para poder registrar este tipo de pago. Si usted no chequea autom&aacute;ticamente el sistema registra el pago por banco.</td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/proveedor/imagen3_1.JPG"></div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center">Luego de haber llenado la informaci&oacute;n solicitada por el formulario de ingreso, teniendo en cuenta que los que tienen el chulo verde son obligatorios. <BR>Procede a presionar aceptar para almacenar la informaci&oacute;n a la base de datos.</td>
          </tr>
          <tr align="">
            <td height="18"  class="ayudaHtmlTexto" align="center"><div align="center"><img src="<%=BASEURL%>/images/ayuda/proveedor/imagen4.JPG"></div></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto" align="center">&nbsp;</td>
          </tr>
         
      </table>
</BODY>
</HTML>
