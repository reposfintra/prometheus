<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Descripcion Campos agregar planilla</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<br>


<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center"> INGRESO PROVEEDOR Y / O PROPIETARIO</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2"> Informaci&oacute;n del Proveedor </td>
        </tr>
        <tr>
          <td width="149" class="fila"> Sede de Pago</td>
          <td width="525"  class="ayudaHtmlTexto"> Campo de selecci&oacute;n para escoger el nombre de la sede el pago del proveedor </td>
        </tr>
        <tr>
          <td class="fila">Banco</td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n para escoger el nombre del banco del proveedor </td>
        </tr>
        <tr>
          <td class="fila"> Agencia</td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n para escoger la agencia del banco del proveedor  </td>
        </tr>
        <tr>
          <td class="fila">Nombre del beneficiario</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el Nombre del beneficiario </td>
        </tr>
        <tr>
          <td class="fila">Clasificaci&oacute;n</td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n para escoger la clasificaci&oacute;n del proveedor </td>
        </tr>
        <tr>
          <td class="fila">Gran Contribuyente</td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n para escoger Si el proveedor es gran contribuyente o No</td>
        </tr>
        <tr>
          <td class="fila">Plazo</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el plazo que tiene el proveedor para pago</td>
        </tr>
        <tr>
          <td class="fila">Autoretenedor IVA</td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n para escoger Si el proveedor es Autoretenedor IVA o No</td>
        </tr>
        <tr>
          <td class="fila">Autoretenedor Retefuente</td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n para escoger Si el proveedor es Autoretenedor IVA o No</td>
        </tr>
        <tr>
          <td class="fila">Autoretenedor ICA</td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n para escoger Si el proveedor es Autoretenedor ICA o No</td>
        </tr>
        <tr>
          <td class="fila">Handle Code</td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n para escoger el Handle Code</td>
        </tr>
        <tr>
          <td colspan="2" class="subtitulo1">Informaci&oacute;n Transferencia </td>
        </tr>
        <tr>
          <td class="fila">Banco Transferencia</td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n para escoger el nombre del banco de la transferencia</td>
        </tr>
        <tr>
          <td class="fila">Sucursal Transferencia</td>
          <td  class="ayudaHtmlTexto">Campo para digitar la sucursal de la transferencia</td>
        </tr>
        <tr>
          <td class="fila">Tipo de Cuenta</td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n para escoger el tipo de la cuenta</td>
        </tr>
        <tr>
          <td class="fila">No. Cuenta</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el numero de la cuenta a transferir</td>
        </tr>
        <tr>
          <td class="fila">Ciudad</td>
          <td  class="ayudaHtmlTexto">Campo de selecci&oacute;n para escoger la ciudad del la transferencia </td>
        </tr>
        <tr>
          <td class="fila">Cedula de la cuenta</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el numero de la cedula de la cuenta a transferir</td>
        </tr>
        <tr>
          <td class="fila">Nombre de la cuenta</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el nombre de la cuenta a transferir</td>
        </tr>
        
      </table>
    </td>
  </tr>
</table>

</body>
</html>
