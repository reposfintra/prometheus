<!--
- Autor : Ing. Tito Andr�s Maturana
- Date  : 30 septiembre del 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : P�gina JSP, lista proveedor.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
 
<html>
<head>
<title>Listado de Proveedor</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listar Proveedor y/o Proprietario"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
  <%  String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
	String nit = (String)session.getAttribute("nit");
    String nombre = (String)session.getAttribute("nombre");
    Vector provs = model.proveedorService.searchProveedores (nit,nombre);
    Proveedor prov;

	if ( provs.size() > 0 ){  
%>
<table width="500" border="2" align="center">
    <tr>
      <td>
	  <table width="100%">
              <tr>
                <td width="373" class="subtitulo1">&nbsp;Datos Proveedor </td>
                <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
        </table>
		  <table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4">
          <tr class="tblTitulo">
          <td width="21%" align="center">Documento</td>
          <td width="79%"  align="center">Nombres y Apellidos</td>
          </tr>
        <pg:pager
         items="<%= provs.size()%>"
         index="<%= index %>"
         maxPageItems="<%= maxPageItems %>"
        maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
        <%-- keep track of preference --%>
        <%
      for (int i = offset.intValue(), l = Math.min(i + maxPageItems, provs.size()); i < l; i++)
	  {
          prov = (Proveedor) provs.elementAt(i);
          String link = CONTROLLER + "?estado=Proveedor&accion=Obtener&cmd=show&nit=" + prov.getC_nit() +
                            "&cia=" + prov.getDistrito() + "&mensaje=";	
%>
        <pg:item>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand"
        onClick="window.open('<%= link %>','','status=yes,scrollbars=no,width=780,height=650,resizable=yes');" >
          <td width="21%"  class="bordereporte"><%=prov.getC_nit()%></td>
          <td width="79%"  class="bordereporte"><%=prov.getNombre()%></td>
        </tr>
        </pg:item>
        <%}%>
        <tr  class="bordereporte">
          <td td height="20" colspan="2" nowrap align="center">
		   <pg:index>
            <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
           </pg:index> 
	      </td>
        </tr>
        </pg:pager>
      </table></td>
    </tr>
</table>
<p>
      <%}
 else { %>
</p>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p>&nbsp; </p>
<%}%>
<br>
<table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr align="left">
    <td><img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="window.location='<%=BASEURL%>/jsp/cxpagar/proveedor/ProveedorBuscar.jsp'" style="cursor:hand "> 
  </tr>
</table>
</div>
</body>
</html>
