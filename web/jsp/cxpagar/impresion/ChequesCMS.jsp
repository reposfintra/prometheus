<!--
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      27/12/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Imprime los cheques generados de las facturas aprobadas en las corridas
 --%>

<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>


<html>
<head>
     <title>Impresion de Cheque</title>
     
     <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
     <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
     <style>.comentario{color:000000;font-family:  Verdana,Arial, Helvetica, sans-serif;font-size: 11px; }</style>
     <style>.cifras{color:000000;font-family:  Verdana,Arial, Helvetica, sans-serif;font-size: 10px; }</style>            
     <style>H1.SaltoDePagina{ PAGE-BREAK-AFTER: always } </style> 
     
     <script type='text/javascript' src="<%= BASEURL %>/js/mouse.js"></script>
     
     <script>
        function closePrint(){
            window.print();
            window.close();
            parent.opener.location.href= '<%=CONTROLLER%>?estado=ChequeFacturas&accion=Corridas&evento=NEXT';
        }
     </script>
	 <script>
        function closePrintCero(){
            window.close();
            parent.opener.location.href= '<%=CONTROLLER%>?estado=ChequeFacturas&accion=Corridas&evento=NEXT';
        }
     </script>
    
</head>

<% 
     Usuario    Usuario       = (Usuario) session.getAttribute("Usuario");
     String     usuario       = Usuario.getLogin(); 
     Hashtable  bloque        = model.ChequeFactXCorridasSvc.getBloque();
     List       listaCheques  = (List)   bloque.get("cheques");
     int        TopCheque     = 0;
     int        tamanoLinea   = 13; 
     int        tamanoColumn  = 1;  
     int        left          = 10;
     String     laser         = request.getParameter("laser")!=null? (String)request.getParameter("laser") : "";
     //double     tamanoPage  = Double.parseDouble( (String) bloque.get("pageBancoCMS") );
     //int        copias      = laser.equals("S")? 2 : 1;
     double     margen        = 0.423;
     int        cont          = 0;
     double     ajuste        = 0;
	 int        cero          = 0;
     %>

<%	
	for(int kk=0;kk<listaCheques.size();kk++){
        Cheque  cheque        = (Cheque)listaCheques.get(kk);
		Series  serie  = cheque.getSerie();
		if( ( serie != null ) && ( serie.getPrefijo().equals("CC") ) ){
			cero++;
		}
	}
%>

<body onload="<%=( cero == listaCheques.size() )?"closePrintCero();":"closePrint();"%>">



<% int c = 0;%>

  <% for(int kk=0;kk<listaCheques.size();kk++){
        Cheque  cheque        = (Cheque)listaCheques.get(kk);
		Series  serie  = cheque.getSerie();
		if( ( serie != null ) && ( !serie.getPrefijo().equals("CC") ) ){
			List    facturasList  = cheque.getFacturas();
			List    esquemasList  = cheque.getEsquema();
			cont++;%>
			
			
			 <!--<DIV  style="position:absolute; left:0px;  top:'<%= TopCheque %>cm'; " > -->
			<DIV  style="position:relative">
					
					<% if( cont%9 == 0 ) {%>
						<% ajuste = ajuste - 0.097; c++; %>
						
					<%}%>
				  <DIV style='position:absolute; top:<%=ajuste%>cm; left:0;'>
				   <!-- ESQUEMA DE IMPRESION-->
				   <%for(int j=0;j<esquemasList.size();j++){
						EsquemaCheque  esquema = (EsquemaCheque) esquemasList.get(j);
						String  valor   =  cheque.formatEsquemaCMS(esquema);
						double     linea   =  esquema.getTop() - margen;
						double     columna =  esquema.getLeft(); %>
						<div  class=comentario style="position:absolute; overflow:hidden; width:700;  left:<%=columna%>cm;  top:<%=linea%>cm; width:'600' "><%=valor%></div>
					<%}%>               
					
				  <!-- TITULOS -->
	
					  <DIV class=comentario STYLE="position:absolute; left:301px; top:307px; "><b><%=cheque.getNumero()%></b> </DIV>            
					  <DIV class=comentario STYLE="position:absolute; left:228px; top:321px; ">   <%=cheque.getAno()%>-<%=cheque.getMes()%> -<%=cheque.getDia()%></DIV>     
					  <DIV class=comentario STYLE="position:absolute; left:353px; top:321px; ">   <%=cheque.getBanco() %> &nbsp <%=cheque.getSucursal()%></DIV>
					  <DIV class=comentario STYLE="position:absolute; left:42px;  top:336px; ">   <%=cheque.getNombre()%>     </DIV> 
					  <DIV class=comentario STYLE="position:absolute; left:353px; top:336px; "><b> NIT        </b>        </DIV>  
					  <DIV class=comentario STYLE="position:absolute; left:384px; top:336px; "> <%=cheque.getBeneficiario()%> </DIV>                    
					  
					  <DIV class=comentario STYLE="position:absolute; left:<%=left   %>px;  top:360px; "><b> FACTURA        </b></DIV> 
					  <DIV class=comentario STYLE="position:absolute; left:<%=left + 95 %>px;  top:360px; "><b> VALOR FACT     </b></DIV>            
					  
					  <DIV class=comentario STYLE="position:absolute; left:<%=left + 223 %>px;  top:360px; "><b> RIVA           </b></DIV> 
					  <DIV class=comentario STYLE="position:absolute; left:<%=left + 302 %>px;  top:360px; "><b> RICA           </b></DIV> 
					  <DIV class=comentario STYLE="position:absolute; left:<%=left + 376 %>px;  top:360px; "><b> RFTE           </b></DIV> 
	
					  <DIV class=comentario STYLE="position:absolute; left:<%=left + 442 %>px;  top:360px; "><b> VRL &nbspNETO   </b></DIV> 
					  <DIV class=comentario STYLE="position:absolute; left:<%=left + 560 %>px;  top:360px; "><b> SALDO          </b></DIV> 
	
					  
					  <!-- DETALLES DEL CHEQUE -->  
	
					 <% int  top     = 379;
						int  max     = 0;
						int  contRow = 0;
						for(int j=0;j<facturasList.size();j++){
						  contRow++;
						  FacturasCheques   factura = (FacturasCheques)   facturasList.get(j); %> 
	
							<!-- Facturas-->
							 
							 <DIV class=comentario STYLE="position:absolute; left:<%=left   %>px;   top:<%=top + max%>px;"> <%=factura.getDocumento()%>   </DIV>
							 
							 <DIV class=cifras STYLE="position:absolute; left:<%=left + 85 %>px;   top:<%=top + max%>px;  width:90">
								  <table width='100%'  cellpadding='0' cellspacing='0'  >
									  <tr><td align='right' class=cifras>
											<%=Util.customFormat( factura.getVlrFactura() + factura.getValor_iva() )%>  
									  </td></tr>
								  </table>
							 </DIV> 
							 <DIV class=cifras STYLE="position:absolute; left:<%=left + 165 %>px; top:<%=top + max%>px;   width:90"> 
								   <table width='100%'  cellpadding='0' cellspacing='0'  >
									  <tr><td align='right' class=cifras>
										   <%=Util.customFormat(  factura.getValor_riva() )  %>  
									 </td></tr>
								  </table>
							 </DIV>
							 <DIV class=cifras STYLE="position:absolute; left:<%=left + 242 %>px; top:<%=top + max%>px;   width:90"> 
								   <table width='100%'  cellpadding='0' cellspacing='0'  >
									  <tr><td align='right' class=cifras>
										   <%=Util.customFormat(  factura.getValor_rica() )  %>  
									 </td></tr>
								  </table>
							 </DIV>
							 
							 <DIV class=cifras STYLE="position:absolute; left:<%=left + 318 %>px; top:<%=top + max%>px;   width:90"> 
								   <table width='100%'  cellpadding='0' cellspacing='0'  >
									  <tr><td align='right' class=cifras>
										   <%=Util.customFormat( factura.getValor_rtfe() )  %>  
									 </td></tr>
								  </table>
							 </DIV>
							 <DIV class=cifras STYLE="position:absolute; left:<%=left + 416 %>px; top:<%=top + max%>px;   width:90"> 
								  <table width='100%'   cellpadding='0' cellspacing='0'  >
									  <tr><td align='right' class=cifras>
										  <%=Util.customFormat(factura.getVlrNetoPago() )%> 
									 </td></tr>
								 </table>
							 </DIV>
							 <DIV class=cifras STYLE="position:absolute; left:<%=left + 514 %>px; top:<%=top + max%>px;   width:90"> 
								  <table width='100%'   cellpadding='0' cellspacing='0'  >
									  <tr><td align='right' class=cifras>
											<%=Util.customFormat(  factura.getVlrPagar() )%>  
									 </td></tr>
								 </table>
							 </DIV>
	
	
							 <!-- Documentos Relacionados-->
	
							 <% List docRel = factura.getDocumnetosRelacionados();
								for(int i=0;i<docRel.size();i++){
									if(contRow == cheque.getTopeDetalle() )
									   break;
									contRow++;
									max+= tamanoLinea;
									FacturasCheques   factRel = (FacturasCheques)   docRel.get(i);%>   
									
									<%String tipo = "";%>
									<%int signo = 1;%>
									<% if( factRel.getTipo_documento().equals("010") ) tipo = "CH"; %>
									<% if( factRel.getTipo_documento().equals("035") ) tipo = "NC"; %>
									<% if( factRel.getTipo_documento().equals("036") ){ tipo = "ND"; signo = -1; }%>
									
									<% if( tipo.equals("") ) tipo = "CH"; %>
									
									 <DIV class=comentario STYLE="position:absolute; left:<%=left %>px;  top:<%=top + max%>px;"> <%=tipo%>&nbsp;<%=factRel.getDocumento()%>    </DIV>                         
									 <DIV class=cifras STYLE="position:absolute; left:<%=left + 85 %>px;  top:<%=top + max%>px;  width:90">
										  <table width='100%'  cellpadding='0' cellspacing='0'  >
											  <tr><td align='right' class=cifras>
													<%=Util.customFormat( (factRel.getVlrFactura() + factRel.getValor_iva())*signo )%>  
											  </td></tr>
										  </table>
									 </DIV> 
									 <DIV class=cifras STYLE="position:absolute; left:<%=left + 165 %>px; top:<%=top + max%>px;   width:90"> 
										   <table width='100%'  cellpadding='0' cellspacing='0'  >
											  <tr><td align='right' class=cifras>
												   <%=Util.customFormat(  factRel.getValor_riva()*signo )  %>  
											 </td></tr>
										  </table>
									 </DIV>
									 <DIV class=cifras STYLE="position:absolute; left:<%=left + 242 %>px; top:<%=top + max%>px;   width:90"> 
										   <table width='100%'  cellpadding='0' cellspacing='0'  >
											  <tr><td align='right' class=cifras>
												   <%=Util.customFormat( factRel.getValor_rica()*signo )  %>  
											 </td></tr>
										  </table>
									 </DIV>
									 <DIV class=cifras STYLE="position:absolute; left:<%=left + 318 %>px; top:<%=top + max%>px;   width:90"> 
								   <table width='100%'  cellpadding='0' cellspacing='0'  >
									  <tr><td align='right' class=cifras>
										   <%=Util.customFormat( factRel.getValor_rtfe()*signo )  %>  
									 </td></tr>
								  </table>
							 </DIV>
									 <DIV class=cifras STYLE="position:absolute; left:<%=left + 416 %>px; top:<%=top + max%>px;   width:90"> 
										  <table width='100%'   cellpadding='0' cellspacing='0' >
											  <tr><td align='right' class=cifras>
												  <%=Util.customFormat(factRel.getVlrNetoPago()*signo)%> 
											 </td></tr>
										 </table>
									 </DIV>
	
							  <%}
							if(contRow==cheque.getTopeDetalle())
								break;
							max+= tamanoLinea;
					   }%>
	
					   <%if( (contRow == cheque.getTopeDetalle()) &&  cheque.getContDetalle()> cheque.getTopeDetalle()  ){%>
							  <DIV class=comentario STYLE="position:absolute; left:<%=left  %>px; top:'510px'; width:140;">Ver Anexo...</div>
					   <%}%>
	
					   <!-- MONTO CHEQUE Y USUARIO --> 
						  <DIV class=comentario STYLE="position:absolute; left:<%=left + 440 %>px; top:'510px'; width:140;">
								   <table width='100%'   cellpadding='0' cellspacing='0'>
									 <tr><td align='right' class=comentario>
											_________________ 
									  </td></tr>
								</table>
						  </DIV>  
						  <DIV class=comentario STYLE="position:absolute; left:<%=left + 440 %>px; top:'530px';  width:140">
								<table width='100%'   cellpadding='0' cellspacing='0' >
									 <tr><td align='right' class=comentario>
											$&nbsp <%= Util.customFormat( cheque.getVlrPagar() ) %>  
									 </td></tr>
								</table>
						  </DIV>
						  <DIV class=comentario STYLE="position:absolute; left:<%=left + 20 %>px;  top:'580px'; "> <%=usuario%> </DIV>  
	
								 
				  </DIV>                
						  
			</DIV>   
			 
			<%if( kk < (listaCheques.size()-1-cero) ){%>
				<H1 class='SaltoDePagina'></H1>
			<%}%>
			 <!-- Para el salto de pagina -->
							 
    	<%}
	}%>


               
</body>
</html>
