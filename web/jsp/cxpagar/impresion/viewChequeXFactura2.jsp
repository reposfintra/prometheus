<!--
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      07/10/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Generacion Cheque X Factura
 --%>

<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>

<html>
<head>
        <title>Vista previa de cheque</title>    
        <link href="<%= BASEURL %>/css/estilostsp.css"     rel="stylesheet" type="text/css"> 
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        
        
        
<%  int totalChequesfaltantes =  model.ChequeXFacturaSvc.getListCheques().size();
    Cheque   cheque           =  model.ChequeXFacturaSvc.getCheque(); 
    if( cheque!=null ){
    String   msj              =  request.getParameter("msj"); 
    String   laser            =  request.getParameter("laser"); 
    boolean  activo           = (cheque.getMonto()>0)?true:false; 
    Precheque pre = model.ChequeXFacturaSvc.getPrecheque();
    String   print = request.getParameter("print");
    
%>
    
      
        <script>
            
            function printCheque(theForm, vlrCheque){
                anularBtn ( i_print  );
                
                var vlrPagar  = parseFloat ( replaceALL(theForm.vlrPagar.value,',','') ); 
                vlrCheque     = parseFloat ( replaceALL(vlrCheque             ,',','') );
                               
                if( vlrCheque>=vlrPagar  &&  vlrPagar>0 ){ 
                    if (confirm('Desea imprimir el cheque ?') ){
                        if( theForm.laser.value == 'S' ){
                            if (confirm('El cheque se imprimira en LASER ?') ){                               
                                //window.open('controller?estado=Cheque&accion=XFactura&evento=IMPRIMIR&vlr='+ vlrPagar+'&laser=S','cheque','top=2000,left=100, width=900, height=500, scrollbar=no, status=yes, resizable=yes  ');
                                location.replace('controller?estado=Cheque&accion=XFactura&evento=IMPRIMIR&vlr='+ vlrPagar+'&laser=S','cheque','top=2000,left=100, width=900, height=500, scrollbar=no, status=yes, resizable=yes  ');
                            }else{ activarBtn ( i_print  ); }
                        }else{
                            window.open('controller?estado=Cheque&accion=XFactura&evento=IMPRIMIR&vlr='+ vlrPagar,'cheque','top=2000,left=100, width=900, height=500, scrollbar=no, status=yes, resizable=yes  ');
                        }
                    }else{ activarBtn ( i_print  ); }
                }
                
            }
            
            function imprimirAnexo(){
                anularBtn ( i_detail  );
                if (confirm('Desea imprimir detalle del cheque ?') )
                    var win = window.open('<%=BASEURL%>/jsp/cxpagar/impresion/Anexo.jsp','anexo',' top=2500,left=100, width=900, height=500, scrollbar=no, status=yes, resizable=yes  ');                                                              
                    activarBtn( i_detail );
            }
        
       </script>
        
</head>
<body <%if( laser != null && !laser.equals("error") ){%> onload="window.open('<%=BASEURL%>/jsp/cxpagar/impresion/ChequeCMS.jsp?msj=&laser=<%=laser%>','cheque',' top=2000,left=100, width=900, height=500, scrollbar=no, status=yes, resizable=yes  ')" <%}%> >



<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Impresi�n de Cheques"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>


  

     <form action="<%=CONTROLLER%>?estado=Cheque&accion=XFactura&evento=IMPRIMIR" method="post" name="formulario">  
     <table width='90%' border="2" align="center">
         <tr>
            <td colspan='2'>            

                        <table width="100%"   align="center">
                                  <tr>
                                     <td width='100%' colspan='2' >
                                          <table width='100%'  class="barratitulo">
                                                <tr class="fila">
                                                        <td align="left" width='50%' class="subtitulo1" nowrap> VISTA PREVIA DE CHEQUE</td>
                                                        <td align="left" width='*' ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                                                </tr>
                                          </table>
                                     </td>
                                  </tr>
                                  
                                   <tr class="fila">
                                           <td width='20%' >&nbsp BANCO </td>
                                           <td width='*' style="font size:12">
                                               <%= cheque.getBanco()      %> &nbsp&nbsp <%= cheque.getSucursal()%> &nbsp&nbsp  <%=cheque.getCuentaBanco() %>
                                           </td>
                                   </tr>
                                   
                                   <tr class="fila">
                                           <td >&nbsp CHEQUE </td>
                                           <td style="font size:12">
                                              <%=  cheque.getNumero() %>
                                           </td>
                                    </tr>
                                  
                                   <tr class="fila">                                      
                                           <td >&nbsp PROVEEDOR </td>
                                           <td  style="font size:12">
                                              <%=  cheque.getBeneficiario() +" &nbsp&nbsp "+ cheque.getNombre()  %>
                                           </td>
                                    </tr>
 
                                   <tr class="fila">                                      
                                           <td >&nbsp BENEFICIARIO </td>
                                           <td  style="font size:12">
                                              <%=  cheque.getNit_beneficiario() +" &nbsp&nbsp "+ cheque.getNom_beneficiario() %>
                                           </td>
                                    </tr>
                                    
                                    <!--
                                    <tr class="fila">
                                          <td >&nbsp TOTAL A PAGAR</td>
                                          <td style="font size:12">
                                                  <%=  Util.customFormat(cheque.getMonto()) %> &nbsp  <%= cheque.getMoneda() %> 
                                          </td>
                                    </tr>                                                                                                            
                                    -->
                                    <tr class="fila">
                                          <td >&nbsp LASER</td>
                                          <td nowrap class="bordereporte">
                                                     <select name='laser'>
                                                         <option value='N'> NO </option>
                                                         <option value='S'> SI </option>
                                                     </select>
                                          </td>
                                    </tr>
                                                                        
                             <!-- DETALLE DEL CHEQUE : FORMADO POR LAS FACTURAS -->
                                    
                                    <tr>
                                         <td width='100%' colspan='2' >
                                              <table width='100%'  class="barratitulo">
                                                    <tr class="fila">
                                                            <td align="left" width='50%' class="subtitulo1" nowrap> DETALLE</td>
                                                            <td align="left" width='*' ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                                                    </tr>
                                              </table>
                                         </td>
                                   </tr>
                                    
                                    <tr class="fila">
                                        <td width='100%' colspan='2'>
                                              <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                                                  <tr class="tblTitulo" >
                                                      <TH  width='30'  title='Item'                    >ITEM           </TH>
                                                      <TH  width='80'  title='N�mero de factura'       >FACTURA        </TH>
                                                      <TH  width='230' title='Descripci�n factura'     >DESCRIPCION    </TH>
                                                      <TH  width='90'  title='Valor de la factura'     >VALOR FACT     </TH>
                                                      
                                                      <!--
                                                      <TH  width='90'  title='Rica + Riva '            >DESCUENTOS     </TH>
                                                      <TH  width='90'  title='Retefuente'              >RETENCION      </TH>
                                                      -->
                                                      
                                                      <TH  width='45'  title='IVA'                     >IVA     </TH>
                                                      <TH  width='45'  title='RIVA'                    >RIVA     </TH>
                                                      <TH  width='45'  title='RICA'                    >RICA     </TH>
                                                      <TH  width='45'  title='Retefuente'              >RFTE     </TH>
                                                      
                                                      <TH  width='90'  title='Valor neto factura'      >VLR   NETO     </TH>
                                                      <TH  width='100' title='Valor saldo factura'     >SALDO          </TH>
                                                      <TH  width='100' title='Valor saldo factura'     >ABONO          </TH>
                                                 </tr>
                                                 
                                                 <!-- Lista de facturas -->
                                                 <% List listFacturas =  cheque.getFacturas();
                                                   for(int i=0;i<listFacturas.size();i++){
                                                      FacturasCheques    factura = (FacturasCheques) listFacturas.get(i);  %>
                                                      <tr class='<%= (i%2==0?"filagris":"filaazul") %>' style="font size:11" >
                                                          <td class="bordereporte" align='center' style="font size:11"> <%= i+1 %>                      </td>
                                                          <td class="bordereporte"                style="font size:11"> <%= factura.getDocumento()   %> </td>
                                                          <td class="bordereporte"                style="font size:11"> <%= factura.getDescripcion() %> </td>                                                                                                                    
                                                          <td class="bordereporte" align='right'  style="font size:11"> <%= Util.customFormat( factura.getVlrFactura()   ) %> </td>
                                                          
                                                          <!--
                                                          <td class="bordereporte" align='right'  style="font size:11"> <%//= Util.customFormat( factura.getVlrDescuentos() ) %> </td>                                                                                                                    
                                                          <td class="bordereporte" align='right'  style="font size:11"> <%//= Util.customFormat( factura.getVlrRetencion()  ) %> </td>
                                                          -->
                                                          
                                                          <td class="bordereporte" align='right'  style="font size:11"> <%= Util.customFormat( factura.getValor_iva()    ) %> </td>
                                                          <td class="bordereporte" align='right'  style="font size:11"> <%= Util.customFormat( factura.getValor_riva()    ) %> </td>
                                                          <td class="bordereporte" align='right'  style="font size:11"> <%= Util.customFormat( factura.getValor_rica()    ) %> </td>
                                                          <td class="bordereporte" align='right'  style="font size:11"> <%= Util.customFormat( factura.getValor_rtfe()    ) %> </td>
                                                          
                                                          <td class="bordereporte" align='right'  style="font size:11"> <%= Util.customFormat( factura.getVlrNetoPago()   ) %> </td>
                                                          <td class="bordereporte" align='right'  style="font size:11"> <%= Util.customFormat( factura.getVlrPagar()      ) %> </td>
                                                         
                                                          <td class="bordereporte" align='right'  style="font size:11"> <%= Util.customFormat( factura.getVlr_total_abonos_me() ) %> </td>
                                                          
                                                      </tr>
                                                      
                                                      <!-- Documentos Relacionados -->
                                                      <% List docRel = factura.getDocumnetosRelacionados();
                                                         for(int j=0;j<docRel.size();j++){
                                                             FacturasCheques    facturaRel = (FacturasCheques)docRel.get(j);%>
                                                             
                                                             <%String tipo = "";%>
                                                             <% int signo = 1;%>
                                                             <% if( facturaRel.getTipo_documento().equals("010") ) tipo = "CH"; %>
                                                             <% if( facturaRel.getTipo_documento().equals("035") ){ tipo = "NC"; signo = -1;}%>
                                                             <% if( facturaRel.getTipo_documento().equals("036") ){ tipo = "ND"; }%>
                                                             
                                                             <tr class='<%= (i%2==0?"filagris":"filaazul") %>' style="font size:11" >
                                                                  <td class="bordereporte" align='center' style="font size:11"> </td>
                                                                  <td class="bordereporte"                style="font size:11"><%=tipo %>&nbsp;<%= facturaRel.getDocumento()   %> </td>
                                                                  <td class="bordereporte"                style="font size:11"> <%= facturaRel.getDescripcion() %> </td>
                                                                  
                                                                  <td class="bordereporte" align='right'  style="font size:11"> <%= Util.customFormat( facturaRel.getVlrFactura()*signo    ) %> </td>
                                                                  <td class="bordereporte" align='right'  style="font size:11"> <%= Util.customFormat( facturaRel.getValor_iva()*signo ) %> </td>
                                                                  <td class="bordereporte" align='right'  style="font size:11"> <%= Util.customFormat( facturaRel.getValor_riva()*signo  ) %> </td>
                                                                  <td class="bordereporte" align='right'  style="font size:11"> <%= Util.customFormat( facturaRel.getValor_rica()*signo  ) %> </td>
                                                                  <td class="bordereporte" align='right'  style="font size:11"> <%= Util.customFormat( facturaRel.getValor_rtfe()*signo  ) %> </td>                                                                  
                                                                  <td class="bordereporte" align='right'  style="font size:11"> <%= Util.customFormat( facturaRel.getVlrNetoPago()*signo   ) %> </td>

                                                                  <td class="bordereporte" align='right'  style="font size:11">  </td>
                                                              </tr>
                                                      <%}%>
                                                      
                                                      
                                                 <%}%>
                                                 
                                              </table>
                                       </td>
                                    </tr>
                                                                        
                                    
                                    <tr class="fila">
                                          <td colspan='2' align='right' >
                                                 &nbsp TOTAL A PAGAR
                                                 <input type='text' name='vlrPagar' readonly style="text-align:right; width:110" onkeyup="if(this.value!='')formatear(this);"      value='<%=Util.customFormat( cheque.getVlrPagar() )%>' title='Valor a cancelar' >                                                  
                                          </td>
                                    </tr>
                                    
                                  
                        </table>
                        
             </td>
         </tr>
     </table>          
     
     </form>
     

     
     
     
     
     <!--Botones -->
     <p>       
           <% if( activo ){%>
                <img src='<%=BASEURL%>/images/botones/imprimir.gif'         style=" cursor:hand"  title='Imprimir Cheque'    name='i_print'       onclick="printCheque(formulario,'<%=Util.customFormat( cheque.getMonto() ) %>');"    onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>                 
                <% if( cheque.getContDetalle()> cheque.getTopeDetalle() ){%>
                      <img src='<%=BASEURL%>/images/botones/detalles.gif'   style=" cursor:hand"  title='Imprimir Detalle'   name='i_detail'      onclick="imprimirAnexo()"                                                            onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
                <% }%>
           <%}%>
           <img src='<%=BASEURL%>/images/botones/regresar.gif'              style=" cursor:hand"  title='Regresar'           name='i_regre'       onclick="location.replace('<%=CONTROLLER%>?estado=Cheque&accion=XFactura&evento=LIST_PRECHEQUES')" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
           <img src='<%=BASEURL%>/images/botones/salir.gif'                 style=' cursor:hand'   title='Salir...'           name='i_salir'       onclick='parent.close();'                                                            onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
     </p> 

     
     
     
    <font  class='informacion'> Cantidad de cheques faltantes <%= totalChequesfaltantes %> </font> 
     
     
     
     
     <!-- Mensaje -->
   <% if(msj!=null && !msj.equals("")){%>        
         <table border="2" align="center">
          <tr>
            <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
              <tr>
                <td width="550" align="center" class="mensajes"><%=msj%></td>
                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                <td width="58">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
        </table>
    <%  } %>
    
    <%}else{%>
        cheque nulo
    <%}%>
     
</div>

</body>
</html>
