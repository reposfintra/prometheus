<!--  
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      07/10/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Muestra  las  Factura aprobadas para pago dentro de una corrida,
                    que han sido seleccionadas para generar sus cheques.
                    La vista muestra los cheques para cada factura.
 --%>

<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>

<html>
<head>
        <title>Cheques - Vista Previa</title>
        <link href="<%= BASEURL %>/css/estilostsp.css"     rel="stylesheet" type="text/css"> 
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
        
        <script>            
            
            var esquema_laser = 0;
            var banco = "";
            var laser = "";
            
            function imprimir(){                   
               
                laser = formulario.laser.value;
                if( laser == "S" ){
                    if( esquema_laser == 0 ){
                        alert("No hay esquema de impresion LASER para el banco "+banco);
                    }else{
                        if(confirm("El cheque se imprimira en LASER?") )
                            printer();
                    }
                }else{
                    printer();
                }
            }
            
            function printer(){
                disabledAll();
                if( laser == "S" ){
                    var win = window.open('<%=BASEURL%>/jsp/cxpagar/impresion/Cheques.jsp?laser='+laser,'impresion',' top=2000,left=100, width=700, height=600, scrollbar=yes, status=yes, resizable=yes  ');
                }else{
                    window.open('<%=BASEURL%>/jsp/cxpagar/impresion/ChequesCMS.jsp','impresion',' top=2000,left=100, width=700, height=600, scrollbar=yes, status=yes, resizable=yes  ');
                }
            }
            
            function disabledAll(){
               document.i_print.style.visibility='hidden';
               <% if ( model.ChequeFactXCorridasSvc.isEstadoBtnRegresar() ){%>
               document.i_regre.style.visibility='hidden';
               <%}%>
               document.i_salir.style.visibility='hidden';
               msj.innerHTML = 'Procesando, por favor espere....';
            }
        </script>
   
</head>
<body>

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Impresión de Cheques"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>
 
  <% Hashtable  bloque    = model.ChequeFactXCorridasSvc.getBloque();
     String msj           = request.getParameter("msj");   
     String     laser     = request.getParameter("laser")!=null? request.getParameter("laser") : "";


     if ( bloque !=null ){     
         List   listaCheques  = (List) bloque.get("cheques");  
         String neto          = (String) bloque.get("neto");   
         model.ChequeFactXCorridasSvc.copyBloque();          %>


     <form action="" method="post" name="formulario"> 
       <table  border="2"    align="center">
         <tr>
            <td> 
               <table width='100%' align='center' class='tablaInferior'>  

                        <tr class="barratitulo">
                            <td colspan='2' >
                               <table cellpadding='0' cellspacing='0' width='100%'>
                                     <tr>
                                          <td align="left" width='55%' class="subtitulo1">&nbsp;VISTA PREVIA</td>
                                          <td align="left" width='*'  ><img align="left" src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
                                     </tr>
                               </table>
                            </td>
                         </tr>    

                         <tr class='fila'>
                               <td width='15%' > CORRIDA                       </td>
                               <td > <%= (String) bloque.get("corrida")     %> </td>
                         </tr>

                         <tr class='fila'>
                              <td  width='15%'> BANCO                                   </td>
                              <td> <%= (String) bloque.get("banco") %> &nbsp <%=  (String) bloque.get("sucursal")  %>  </td>
                              <script> banco = '<%=(String) bloque.get("banco")%>'</script>
                         </tr>

                        <tr class='fila'>
                              <td  width='15%'> CUENTA                                 </td>
                              <td> <%=  (String) bloque.get("cuenta")  %> </td>
                        </tr>
                        
                        <tr class="fila">
                            <td >LASER</td>
                            <td nowrap class="bordereporte">
                                <select name='laser' id='laser'>
                                    <option value='N'> NO </option>
                                    <option value='S'> SI </option>
                                </select>
                            </td>
                       </tr>
                        
                       <tr>
                          <td colspan='2' align='center' >
                              <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">

                                         <tr class="tblTitulo" >
                                                  <TH  width='250'  title='Nombre Beneficiario'  >BENEFICIARIO        </TH>
                                                  <TH  width='120'  title='Identificación'       >NIT                 </TH>               
                                                  <TH  width='100'  title='Número del cheque'    >CHEQUE              </TH>               
                                                  <TH  width='120'  title='Valor a Pagar'        >VALOR               </TH>      
                                          </tr>
                                       <%
                                            for(int i=0;i<listaCheques.size();i++){
                                                Cheque  cheque = (Cheque)listaCheques.get(i);%>
                                                
                                                <% if( cheque.getEsquemaLaser()!=null ){ %> <script> esquema_laser = <%=cheque.getEsquemaLaser().size()%> </script>  <%}%>
                                                <script> banco = '<%=cheque.getBanco()%>'  </script>
                                                
                                                <tr class='<%= (i%2==0?"filagris":"filaazul") %>' style="font size:11">
                                                     <td class="bordereporte"               >  <%= cheque.getNombre()       %> </td>
                                                     <td class="bordereporte" align='right' >  <%= cheque.getBeneficiario() %> </td>
                                                     <td class="bordereporte" align='center'>  <%= cheque.getNumero()       %> </td>
                                                     <td class="bordereporte" align='right' >  
                                                     
                                                            <a href='#' class='Simulacion_Hiper' onclick="vista<%=cheque.getBeneficiario()%>.style.visibility=''" title='Ver Detalle'>
                                                               <%= Util.customFormat(cheque.getMonto())%> 
                                                            </a>                                                            
                                                           
                                                            
                                                            <div id='vista<%=cheque.getBeneficiario()%>'   style=" position:absolute; left:500;  visibility:hidden; z-index:0;" >
                                                                 <table   cellpadding='0' cellspacing='0'    align="center" border='1' bordercolor="#123456" >
                                                                       <tr class='fila'>
                                                                            <td align='center'> 
                                                                            
                                                                                    <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">                                                                                                          
                                                                                             <tr class="tblTitulo" style="font size:9" >
                                                                                                  <TH width='50'  >FACTURA       </TH>
                                                                                                  <TH width='200' >DESCRIPCIÓN   </TH>
                                                                                                  <TH width='100' >VALOR         </TH>
                                                                                             </tr>
                                                                                             <% List listaFacturas = cheque.getFacturas();
                                                                                                for(int j=0;j<listaFacturas.size();j++){
                                                                                                      FacturasCheques  factura  = (FacturasCheques) listaFacturas.get(j);%>
                                                                                                      <tr class='<%=(i%2==0?"filagris":"filaazul")%>' style="font size:10">
                                                                                                           <td class="bordereporte" nowrap  > <%= factura.getDocumento()    %> </td>
                                                                                                           <td class="bordereporte" nowrap  > <%= factura.getDescripcion()  %> </td>
                                                                                                           <td class="bordereporte" nowrap align='right'  > <%= Util.customFormat( factura.getVlrPagar() ) %> </td>
                                                                                                        </tr> 
                                                                                                <%}%>
                                                                                                                  
                                                                                     </table> 
                                                                                     <a href='#' class='Simulacion_Hiper' onclick="vista<%=cheque.getBeneficiario()%>.style.visibility='hidden'" title='Cerrar Detalle'>Cerrar</a>                                                                                                      
                                                                                     <br><br>
                                                                         </td>
                                                                     </tr>
                                                                 </table>
                                                             </div>
                                                                    
                                                          
                                                     </td>
                                                </tr>
                                       <%}%>

                              </table>
                          </td>
                      </tr>


               </table>
           </td>
        </tr>
      </table>
     </form>







       <p>       
          <img src='<%=BASEURL%>/images/botones/imprimir.gif'    style=" cursor:hand"  title='Crear....'        name='i_print' id='i_print'       onclick="imprimir(this);"                                                                onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>        
          <% if ( model.ChequeFactXCorridasSvc.isEstadoBtnRegresar() ){%>
          <img src='<%=BASEURL%>/images/botones/regresar.gif'    style=" cursor:hand"   title='Regresar....'    name='i_regre' id='i_regre'       onclick=" disabledAll(); location.replace('<%=CONTROLLER%>?estado=ChequeFacturas&accion=Corridas&evento=LOAD');" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>        
         <%}%>
          <img src='<%=BASEURL%>/images/botones/salir.gif'       style=' cursor:hand'   title='Salir...'        name='i_salir' id='i_salir'      onclick='parent.close();'                                                                                onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
       </p> 

       

       <font id='msj'  class='informacion'>
               <!-- Bloques faltantes -->
               <fieldset style="width:300" class='informacion'>
                  Bloques de Impresiones Faltantes : &nbsp <%=   model.ChequeFactXCorridasSvc.getListCheques().size()  %>
               </fieldset>
               
     
                <br><br>
               <!-- Mensaje -->
                 <%  if ( msj!=null  && !msj.equals("")){%>
                         <table border="2" align="center">
                               <tr>
                                    <td>
                                           <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                              <tr>
                                                    <td width="500" align="center" class="mensajes"><%=msj%></td>
                                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                                    <td width="58">&nbsp; </td>
                                              </tr>
                                            </table>
                                   </td>
                               </tr>
                        </table>
                 <%}%>
       </font>     
       
        
  <%}else{%>
      
             <table border="2" align="center">
                   <tr>
                        <td>
                               <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                  <tr>
                                        <td width="500" align="center" class="mensajes"><%=msj%></td>
                                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                        <td width="58">&nbsp; </td>
                                  </tr>
                                </table>
                       </td>
                   </tr>
              </table>
              <p>
                 <img src='<%=BASEURL%>/images/botones/regresar.gif'    style=" cursor:hand"   title='Regresar....'    name='i_regre'  onclick="location.href='<%=BASEURL%>/jsp/cxpagar/impresion/ChequesFacturaXCorrida.jsp';" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>                
                 <img src='<%=BASEURL%>/images/botones/salir.gif'       style=' cursor:hand'   title='Salir...'        name='i_salir'  onclick='parent.close();'                                                                onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
              </p>
  
  <%}%>

</div>
<%=datos[1]%>
</body>
</html>
