<!--
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      07/10/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)
     - Description: Muestra  las  Factura aprobadas para pago dentro de una corrida,
                    Agrupadas por corridas, proveedor.
 --%>

<%@ page session   ="true"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>



<html>
<head>
    <title>Cheque Facturas Aprobadas por Corridas</title>
    <link href="<%= BASEURL %>/css/estilostsp.css"     rel="stylesheet" type="text/css"> 
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/validate.js"></script>
   <style> 
        a:link , a:active ,  a:visited, a:hover{
            text-decoration: none;
            color: #003399;
        }
        a:hover{
            text-decoration: none;
            color: #003399;
        }
  </style>
    
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
    
  <%
 
     Usuario u                 = (Usuario)session.getAttribute("Usuario");
     List    listBloques       = model.ChequeFactXCorridasSvc.getVistaFiltros(); 
     String  ON                = model.ChequeFactXCorridasSvc.getON();
     int     TOTAL_APROBADAS   = model.ChequeFactXCorridasSvc.getCantAprobadas();
     String  URL               = CONTROLLER + "?estado=ChequeFacturas&accion=Corridas"; 
     String  puede_seleccionar = u.getId_agencia().equals("OP")? "SI" : "NO";
     String  msg               = request.getAttribute("msg")!=null? (String) request.getAttribute("msg") : "";
  %>
    
<script>

        var cant_proveedores = 0;
    
	function sendFacturas(theForm){
		<% if( TOTAL_APROBADAS > 0){%>
			anularBtn( i_crear );
			msj.innerHTML = 'Generando cheques, por favor espere...';
			document.location.href=('<%= URL %>&evento=FORMARCHEQUES');
		<%}else{%>
			alert('Deber� seleccionar las facturas que desee pagar');                 
		<%}%>
	}     
	function filtro(theForm, llave)    {  
		var parameter = "";
		for(var i=0;i<theForm.length;i++){
			if( theForm.elements[i].type=='checkbox'  &&   theForm.elements[i].name=='factura'  &&  theForm.elements[i].checked==true )
			parameter += "&" + theForm.elements[i].name + "=" + theForm.elements[i].value;
		}
		window.location.href ="<%= URL %>&evento=FILTRO&llave=" + llave + parameter;
	}       
	function selectBloque(id, ck){
		var estCk = (ck.checked==true)?'checked':'';
		window.location.href ="<%= URL %>&evento=SELECTION&id=" + id  +"&ck="+ estCk ; 
	}
	function selectBloque2(id, ck){
		/*
		var puede_seleccionar = "<%=puede_seleccionar%>";
		if( puede_seleccionar == "SI"){
		*/
		var estCk = (ck.checked==true)?'checked':'';            
		window.location.href ="<%= URL %>&evento=SELECTION&id=" + id  +"&ck="+ estCk ;
		/*    
		}else if( puede_seleccionar == "NO"){
		if( ck.checked ){ ck.checked = false; }
		if( !ck.checked ){ ck.checked = true; }
		alert("Usted no tiene permitido excluir Facturas de los cheques");
		}*/
	}
	function selectBloque3(id, ck){
		var sw = false;
		if( ck.checked ){ ck.checked = false; sw = true; }
		if( !ck.checked && sw == false){ ck.checked = true; }
		alert("No se puede excluir Facturas de los cheques");
	}
	   
	function selec_rango(){
	
            var ini  = document.getElementById("inicial");
            var fin  = document.getElementById("final");
            var form = document.formulario;
            var sw   = 0;
            
            if( trim(ini.value) != '' && trim(fin.value) != ''){
                
                if( ini.value > cant_proveedores ){
                    alert("El numero inicial esta fuera del rango");
                    ini.focus();
                    sw  = 1;
                    return false;
                }if( fin.value > cant_proveedores ){
                    alert("El numero final esta fuera del rango");
                    fin.value = cant_proveedores;
                    fin.focus();
                    sw  = 1;
                    return false;
                }if( ini.value == 0 || fin.value == 0 ){
                    alert("El rango inicial debe ser 1 ");
                    ini.focus();
                    sw  = 1;
                    return false;
                }
                
            }else{
                sw  = 1;
            }
            
            var cont = 0;
            
            if( sw == 0 ){                
                
                window.location.href ="<%= URL %>&evento=SELECTION&rango=si&ini="+ini.value+"&fin="+fin.value+"&ck=checked";
            
            }
	
	}
</script>

</head>
<body>

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Impresi�n de Cheques"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>
     <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
     

   
   <%if( msg.equals("") ){
         if ( listBloques.size()>0 ) {%>
   
            <form action="" method="post" name="formulario">  
                 <table   border="2" align="center" width='100%'>
                     <tr>
                        <td colspan='2'>             

                                    <table width="100%"   align="center">
                                              <tr>
                                                 <td >
                                                      <table width='100%'  class="barratitulo">
                                                            <tr class="fila">
                                                                    <td align="left" width='70%' class="subtitulo1" nowrap> GENERACION DE CHEQUES A FACTURA APROBADAS EN CORRIDAS</td>
                                                                    <td align="left" width='*' ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
                                                            </tr>
                                                      </table>
                                                 </td>
                                              </tr>

                                              <tr class='fila'>
                                                <% String ini = request.getParameter("ini")!=null? request.getParameter("ini") : "";%>
                                                <% String fin = request.getParameter("fin")!=null? request.getParameter("fin") : "";%>
                                                <td> &nbsp; Seleccionar proveedores  del <input type='textbox' id='inicial' name='inicial' size='4' maxlength='4' onKeyPress="soloDigitos(event,'decNo');" value='<%=ini%>'> al <input type='textbox' id='final' name='final' size='4' maxlength='4' onKeyPress="soloDigitos(event,'decNo');" onblur='selec_rango();' value='<%=fin%>'> </td>
                                              
                                              </tr>

                                            <tr class="fila">
                                                <td width='100%'>
                                                       <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">                      
                                                           <tr class="tblTitulo" >                                                      
                                                                  <% String[]  titles        = model.ChequeFactXCorridasSvc.getTITULOS();         
                                                                     String[]  tamanoTitles  = model.ChequeFactXCorridasSvc.getTAMANO_TITULOS();  
                                                                     int       totalFiltros  = model.ChequeFactXCorridasSvc.getCANTIDAD_FILTROS();
                                                                     for(int i=0;i<totalFiltros;i++){%>                                                             
                                                                      <TH  width='<%= tamanoTitles[i]%>' align='center'  > <%= titles[i] %> </TH>
                                                                    <%}%>

                                                                  <TH  width='10%'  title='Factura' > 
                                                                       <table cellpadding='0' cellspacing='0' width='100%' >
                                                                         <tr class="tblTitulo" >
                                                                            <td align='center' width='*'  > FACTURA</td>
                                                                         </tr>
                                                                       </table>
                                                                  </TH>               
                                                                  <TH  width='20%'  title='Descripci�n'         >DESCRIPCION   </TH>               
                                                                  <TH  width='10%'  title='Valor Pagar factura' >SALDO         </TH>           
                                                            </tr>
                                                        </table>
                                                   </td>
                                              </tr>

                                             <tr>
                                                 <td width='100%'>
                                                      <table width='100%' cellpadding='0' cellspacing='0' >

                                                                         <!-- CORRIDA -->
                                                                         <% for (int i=0;i<listBloques.size();i++){
                                                                                  Hashtable  bloqueLevel0       =  ( Hashtable) listBloques.get(i);
                                                                                  String     titleBloque0       =  ( String   ) bloqueLevel0.get("titulo");
                                                                                  String     totalBloque0       =  ( String   ) bloqueLevel0.get("total");
                                                                                  String     imgBloque0         =  ( String   ) bloqueLevel0.get("img");
                                                                                  String     estadoBloque0      =  ( String   ) bloqueLevel0.get("estado");
                                                                                  String     ckBloque0          =  ( String   ) bloqueLevel0.get("ck");
                                                                                  String     estadoHijoBloque0  =  ( String   ) bloqueLevel0.get("hijos");
                                                                                  String     llaveBloque0       =  ( String   ) bloqueLevel0.get("llave");
                                                                                  String     idBloque0          =  ( String   ) bloqueLevel0.get("id");
                                                                                  String     selectionBloque0   =  ( String   ) bloqueLevel0.get("selection");
                                                                                  String     levelBloque0       =  ( String   ) bloqueLevel0.get("level");
                                                                                  List       itemsBloque0       =  ( List     ) bloqueLevel0.get("items");%>


                                                                                 <% if ( estadoBloque0.equals(ON) ) { %> 
                                                                                  <tr   class='<%= (i%2==0?"filagris":"filaazul") %>' valign='top' style="font size:11" >
                                                                                     <td width='10%'   class="bordereporte"  style="font size:11"> <input type='checkbox'  <%= ckBloque0 %>  onclick="selectBloque2('<%= idBloque0%>', this )" title='Seleccionar'>       <a href="javascript:filtro(formulario,'<%= llaveBloque0 %>')"  title='Expandir/Colapsar'><img src="<%=BASEURL%>/images/botones/iconos/<%=imgBloque0%>" border='0'>  <font color='<%= selectionBloque0 %>'>   <%= titleBloque0 %> </a> </td>
                                                                                     <td width='*'     class="bordereporte" >
                                                                                         <table cellpadding='0' cellspacing='0' width='100%'>

                                                                                              <!-- BANCOS -->                                                                         
                                                                                              <%  if( estadoHijoBloque0.equals( ON ) ){ 
                                                                                                  for (int j=0;j<itemsBloque0.size();j++){
                                                                                                      Hashtable  bloqueLevel1       =  ( Hashtable) itemsBloque0.get(j);
                                                                                                      String     titleBloque1       =  ( String   ) bloqueLevel1.get("titulo");
                                                                                                      String     totalBloque1       =  ( String   ) bloqueLevel1.get("total");
                                                                                                      String     imgBloque1         =  ( String   ) bloqueLevel1.get("img");
                                                                                                      String     estadoBloque1      =  ( String   ) bloqueLevel1.get("estado");
                                                                                                      String     ckBloque1          =  ( String   ) bloqueLevel1.get("ck");
                                                                                                      String     idBloque1          =  ( String   ) bloqueLevel1.get("id");
                                                                                                      String     selectionBloque1   =  ( String   ) bloqueLevel1.get("selection");
                                                                                                      String     estadoHijoBloque1  =  ( String   ) bloqueLevel1.get("hijos");
                                                                                                      String     llaveBloque1       =  ( String   ) bloqueLevel1.get("llave");
                                                                                                      String     levelBloque1       =  ( String   ) bloqueLevel1.get("level");
                                                                                                      List       itemsBloque1       =  ( List     ) bloqueLevel1.get("items");%>

                                                                                                   <% if ( estadoBloque1.equals(ON) ) { %> 
                                                                                                      <tr    valign='top' class='<%= (j%2==0?"filagris":"filaazul") %>' style="font size:11" >
                                                                                                         <td width='11%'  class="bordereporte"  style="font size:11"> <input type='checkbox'  <%= ckBloque1 %> onclick="selectBloque2('<%= idBloque1%>', this )" title='Seleccionar'>    <a href="javascript:filtro(formulario,'<%= llaveBloque1%>')" title='Expandir/Colapsar'><img src="<%=BASEURL%>/images/botones/iconos/<%=imgBloque1%>" border='0'><font color='<%= selectionBloque1 %>'> <%= titleBloque1 %> </a>  </td>
                                                                                                         <td width='*'  class="bordereporte">
                                                                                                              <table cellpadding='0' cellspacing='0' width='100%'  >

                                                                                                                         <!-- SUCURSALES -->
                                                                                                                         <% if( estadoHijoBloque1.equals( ON ) ){ 
                                                                                                                               for (int k=0;k<itemsBloque1.size();k++){
                                                                                                                                  Hashtable  bloqueLevel2       =  ( Hashtable) itemsBloque1.get(k);
                                                                                                                                  String     titleBloque2       =  ( String   ) bloqueLevel2.get("titulo");
                                                                                                                                  String     totalBloque2       =  ( String   ) bloqueLevel2.get("total");
                                                                                                                                  String     imgBloque2         =  ( String   ) bloqueLevel2.get("img");
                                                                                                                                  String     estadoBloque2      =  ( String   ) bloqueLevel2.get("estado");
                                                                                                                                  String     ckBloque2          =  ( String   ) bloqueLevel2.get("ck");
                                                                                                                                  String     idBloque2          =  ( String   ) bloqueLevel2.get("id");
                                                                                                                                  String     selectionBloque2   =  ( String   ) bloqueLevel2.get("selection");
                                                                                                                                  String     estadoHijoBloque2  =  ( String   ) bloqueLevel2.get("hijos");
                                                                                                                                  String     llaveBloque2       =  ( String   ) bloqueLevel2.get("llave");
                                                                                                                                  String     levelBloque2       =  ( String   ) bloqueLevel2.get("level");
                                                                                                                                  List       itemsBloque2       =  ( List     ) bloqueLevel2.get("items");%>

                                                                                                                                           <% if ( estadoBloque2.equals(ON) ) { %> 
                                                                                                                                              <tr  valign='top'  class='<%= (k%2==0?"filagris":"filaazul") %>' style="font size:11" >
                                                                                                                                                 <td width='19%'  class="bordereporte"  style="font size:11"> <input type='checkbox' <%= ckBloque2 %>  onclick="selectBloque2('<%= idBloque2%>',this )" title='<%=idBloque2%>'> <a href="javascript:filtro(formulario,'<%= llaveBloque2 %>')" title='Expandir/Colapsar' ><img src="<%=BASEURL%>/images/botones/iconos/<%=imgBloque2%>" border='0'> <font color='<%= selectionBloque2 %>'> <%= titleBloque2 %> </a> </td>
                                                                                                                                                 
                                                                                                                                                 <td width='*'    class="bordereporte">
                                                                                                                                                      <table cellpadding='0' cellspacing='0' width='100%' >                                                         

                                                                                                                                                             <!-- PROVEEDOR -->
                                                                                                                                                            <% if( estadoHijoBloque2.equals( ON ) ){
                                                                                                                                                                  for (int l=0;l<itemsBloque2.size();l++){
                                                                                                                                                                      Hashtable  bloqueLevel3       =  ( Hashtable) itemsBloque2.get(l);
                                                                                                                                                                      String     titleBloque3       =  ( String   ) bloqueLevel3.get("titulo");
                                                                                                                                                                      String     imgBloque3         =  ( String   ) bloqueLevel3.get("img");
                                                                                                                                                                      String     totalBloque3       =  ( String   ) bloqueLevel3.get("total");
                                                                                                                                                                      String     estadoBloque3      =  ( String   ) bloqueLevel3.get("estado");
                                                                                                                                                                      String     ckBloque3          =  ( String   ) bloqueLevel3.get("ck");
                                                                                                                                                                      String     idBloque3          =  ( String   ) bloqueLevel3.get("id");
                                                                                                                                                                      String     selectionBloque3   =  ( String   ) bloqueLevel3.get("selection");
                                                                                                                                                                      String     estadoHijoBloque3  =  ( String   ) bloqueLevel3.get("hijos");
                                                                                                                                                                      String     llaveBloque3       =  ( String   ) bloqueLevel3.get("llave");
                                                                                                                                                                      String     levelBloque3       =  ( String   ) bloqueLevel3.get("level");
                                                                                                                                                                      String     beneficiario       =  ( String   ) bloqueLevel3.get("beneficiario");//Osvaldo
                                                                                                                                                                      String     nit_beneficiario   =  ( String   ) bloqueLevel3.get("nit_beneficiario");//Osvaldo
                                                                                                                                                                      List       itemsBloque3       =  ( List     ) bloqueLevel3.get("items");%>
                                                                                                                                                                      <script> cant_proveedores = <%=itemsBloque2.size()%>; </script>
                                                                                                                                                                     <% if ( estadoBloque3.equals(ON) ) { %> 
                                                                                                                                                                          <tr    valign='top'   class='<%= (l%2==0?"filagris":"filaazul") %>' style="font size:11" >
                                                                                                                                                                             <td width='38%'  class="bordereporte"  style="font size:11">&nbsp;<%=l+1%> <%=(l<9)? "&nbsp;&nbsp;":""%>&nbsp;<input type='checkbox' id='ch_provee'  <%= ckBloque3 %>  onclick="selectBloque2('<%= idBloque3%>', this )" title='Seleccionar'>  <a href="javascript:filtro(formulario,'<%= llaveBloque3 %>')" title='Beneficiario:
<%=nit_beneficiario +" - "+ beneficiario%>' ><img src="<%=BASEURL%>/images/botones/iconos/<%=imgBloque3%>" border='0'> <font color='<%= selectionBloque3 %>'> <%= titleBloque3 %> </a> </td>
                                                                                                                                                                             <td width='*'  >
                                                                                                                                                                                  <table cellpadding='0' cellspacing='0' width='100%'>
                                                                                                                                                                                              <!-- FACTURAS -->
                                                                                                                                                                                              <% if( estadoHijoBloque3.equals( ON ) ){
                                                                                                                                                                                                   for (int m=0;m<itemsBloque3.size();m++){
                                                                                                                                                                                                       Hashtable  blFactura  = (Hashtable) itemsBloque3.get(m); 
                                                                                                                                                                                                       String   factura      = (String) blFactura.get("factura");
                                                                                                                                                                                                       String   descfactura  = (String) blFactura.get("descripcion");
                                                                                                                                                                                                       String   saldofact    = (String) blFactura.get("saldo");
                                                                                                                                                                                                       String   idfact       = (String) blFactura.get("id");
                                                                                                                                                                                                       String   selectionfac = (String) blFactura.get("selection");
                                                                                                                                                                                                       String   selec        = (String) blFactura.get("ck");%>

                                                                                                                                                                                                      <tr  valign='top' class='<%= (m%2==0?"filagris":"filaazul") %>' style="font size:11" >
                                                                                                                                                                                                         <td width='25%'  class="bordereporte"  style="font size:11"              > 
                                                                                                                                                                                                               <input type='checkbox' name='factura' <%= selec %> value='<%= idfact %>'  onclick="selectBloque3('<%= idfact%>', this )" > 
                                                                                                                                                                                                               <font color='<%= selectionfac %>'> <%= factura  %>
                                                                                                                                                                                                         </td>
                                                                                                                                                                                                         <td width='50%'  class="bordereporte"  style="font size:11"              >  <font color='<%= selectionfac %>'><%= descfactura %></td>
                                                                                                                                                                                                         <td width='*'    class="bordereporte"  style="font size:11" align='right'>  <font color='<%= selectionfac %>'><%= Util.customFormat(Double.parseDouble(saldofact) )  %></td>                                                                                                                                                                     
                                                                                                                                                                                                      </tr>

                                                                                                                                                                                             <% }%>
                                                                                                                                                                                              <tr class='<%= (l%2==0?"filagris":"filaazul") %>'>
                                                                                                                                                                                                 <td style="font size:11">&nbsp TOTAL:</td>
                                                                                                                                                                                                 <td align='right' colspan='2'  style="font size:11"><b>   <%= Util.customFormat(Double.parseDouble( totalBloque3) ) %></td>
                                                                                                                                                                                              </tr>
                                                                                                                                                                                           <% }else{%>                                                                                                                                                                                       
                                                                                                                                                                                            <tr class='<%= (l%2==0?"filagris":"filaazul") %>'>
                                                                                                                                                                                                 <td align='right' colspan='2'  style="font size:11"><b>   <%= Util.customFormat(Double.parseDouble( totalBloque3) ) %></td>
                                                                                                                                                                                            </tr>
                                                                                                                                                                                           <%}%>
                                                                                                                                                                                 </table>
                                                                                                                                                                             </td>                                                              
                                                                                                                                                                          </tr>                                                                                                                                                                      
                                                                                                                                                                  <% }%>
                                                                                                                                                              <% }%> 
                                                                                                                                                          <% }else{%> 
                                                                                                                                                              <tr class='<%= (k%2==0?"filagris":"filaazul") %>'>
                                                                                                                                                                 <td  align='right' colspan='2'     style="font size:11"><b>   <%= Util.customFormat(Double.parseDouble( totalBloque2) ) %></td>
                                                                                                                                                              </tr>
                                                                                                                                                          <%}%>
                                                                                                                                                     </table>
                                                                                                                                                 </td>                                                              
                                                                                                                                              </tr>                                                                                                            

                                                                                                                                          <%}%>
                                                                                                                                  <% }%>                 
                                                                                                                         <% }else{%>                                                                                                                     
                                                                                                                                  <tr class='<%= (j%2==0?"filagris":"filaazul") %>'>
                                                                                                                                      <td align='right' colspan='2'  style="font size:11"><b>   </td>
                                                                                                                                  </tr>
                                                                                                                         <%}%>
                                                                                                              </table>
                                                                                                          </td>                                                              
                                                                                                      </tr>                                                                 

                                                                                                     <%}%>
                                                                                                <% }%>
                                                                                            <% }else{%>  
                                                                                              <tr class='<%= (i%2==0?"filagris":"filaazul") %>'>
                                                                                                     <td colspan='7' align='right'  style="font size:11"><b>   </td>
                                                                                              </tr>
                                                                                            <%}%>                                                                                          
                                                                                         </table>
                                                                                     </td>                                                              
                                                                                  </tr>
                                                                             <%}%>
                                                                        <% }%>



                                                       </table>
                                                     </td>
                                                  </tr>

                                            </table>
                                         </td>
                                        </tr>



                                </table>

                          </td>
                      </tr>
                 </table>
            </form>

            <p>       
               <img src='<%=BASEURL%>/images/botones/regresar.gif'    style=" cursor:hand"  title='Regresar....'                         onclick="location.href = '<%=CONTROLLER%>?estado=ChequeFacturas&accion=Corridas&evento=FILTER' ; "  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
               <img src='<%=BASEURL%>/images/botones/imprimir.gif'    style=" cursor:hand"  title='Aceptar....'     name='i_crear'       onclick="sendFacturas(formulario)"   onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
               <img src='<%=BASEURL%>/images/botones/salir.gif'      style=' cursor:hand'   title='Salir...'        name='i_salir'       onclick='parent.close();'            onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
            </p> 

            <font id='msj' class='informacion'></font>



       <%}else{%>
            <table border="2" align="center">
                  <tr>
                        <td>
                              <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                  <tr>
                                        <td width="550" align="center" class="mensajes">No presenta facturas aprobadas para pago dentro de corridas</td>
                                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                        <td width="58">&nbsp;</td>
                                  </tr>
                              </table>
                        </td>
                  </tr>
            </table>
            <p>       
               <img src='<%=BASEURL%>/images/botones/salir.gif'      style=' cursor:hand'   title='Salir...'        name='i_salir'       onclick='parent.close();'        onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
            </p> 

       <%}
    }else{%>
        <table border="2" align="center">
                  <tr>
                        <td>
                              <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                  <tr>
                                        <td width="550" align="center" class="mensajes"><%=msg%></td>
                                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                        <td width="58">&nbsp;</td>
                                  </tr>
                              </table>
                        </td>
                  </tr>
            </table>
            <p>   
               <img src='<%=BASEURL%>/images/botones/regresar.gif'    style=" cursor:hand"  title='Aceptar....'     name='i_crear'       onclick="location.href = '<%=CONTROLLER%>?estado=ChequeFacturas&accion=Corridas&evento=FILTER' ; "   onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
               <img src='<%=BASEURL%>/images/botones/salir.gif'      style=' cursor:hand'   title='Salir...'        name='i_salir'       onclick='parent.close();'        onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
            </p> 
    <%}%>
   
</div>
<%=datos[1]%>

</body>
</html>
