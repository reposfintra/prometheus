<!--  
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      07/10/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Generacion Cheque X Factura
 --%>

<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
        <title>Vista previa de cheque</title>    
        <link href="<%= BASEURL %>/css/estilostsp.css"     rel="stylesheet" type="text/css"> 
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        
        
        
<%  int totalChequesfaltantes =  model.ChequeXFacturaSvc.getListCheques().size();
    Cheque   cheque           =  model.ChequeXFacturaSvc.getCheque(); 
    String   msj              =  request.getParameter("msj"); 
    String   laser            =  request.getParameter("laser"); 
    boolean  activo           = (cheque.getMonto()>0)?true:false; 
    Precheque pre = model.ChequeXFacturaSvc.getPrecheque();
    String   print = request.getParameter("print");%>
    
      
	<script>
		function sendCheque(theForm, vlrCheque){
			anularBtn ( i_print  );
			if( theForm.vlrPagar.value ==''){
				  alert('Deber� digitar el valor a pagar');
				  theForm.vlrPagar.focus();
				  activarBtn( i_print );
				  return false;
			}            
			
			var agencia = theForm.agencia.value;
			
			if( agencia == 'ninguna' ){
				alert('Debe seleccionar la agencia para impresion');
				theForm.agencia.focus();
				activarBtn( i_print );
				return false;
			}
			
			if( theForm.tipo.value == 'T'){
				if( theForm.banco.value == ''){
					alert('Debe seleccionar el banco para la transferencia');
					theForm.banco.focus();
					activarBtn( i_print );
					return false;
				}
				if( theForm.sucursal.value == ''){
					alert('Debe seleccionar la sucursal para la transferencia');
					theForm.sucursal.focus();
					activarBtn( i_print );
					return false;
				}
			}
			
			var vlrPagar  = parseFloat ( replaceALL(theForm.vlrPagar.value,',','') ); 
				vlrCheque = parseFloat ( replaceALL(vlrCheque             ,',','') );
			var tipo      = theForm.tipo.value;
			var banco     = theForm.banco.value;
			var sucursal  = theForm.sucursal.value;
				
			if( vlrCheque>=vlrPagar  &&  vlrPagar>0 ){ 
				if( tipo != 'T'){
					if (confirm('Desea generar el cheque ?') ){                        
						location.replace('controller?estado=Cheque&accion=XFactura&evento=PRECHEQUE&vlr='+ vlrPagar+'&agencia='+agencia);
					}
				}
				else{
					location.replace('controller?estado=Cheque&accion=XTransferencia&opcion=PRECHEQUE&vlr='+ vlrPagar+'&agencia='+agencia+'&tipo='+tipo+'&banco='+banco+'&sucursal='+sucursal);
				}      
			}
			else{
				 alert('El valor a pagar no deber� superar el valor neto del cheque, o ser negativo. Favor corregir...');
				 theForm.vlrPagar.value = '<%=Util.customFormat( cheque.getMonto() )%>';
				 theForm.vlrPagar.focus();
			} 
			activarBtn( i_print );
		}
		
		function enviar(url){
			var a = "<iframe name='ejecutor' style='visibility:hidden'  src='<%= CONTROLLER %>" + url + "'> ";
			aa.innerHTML = a;
		}
		
		function tIngreso(){
			var tipo = document.getElementById('tipo').value;
			if(tipo=='T'){
				banco.style.display="block";
			}else{
				banco.style.display="none";
			}
		}
		
		function cargarSelects ( dir ){
			formulario.action = dir;
			formulario.submit();	
		}
   </script>
        
</head>
<body <%if( laser != null && !laser.equals("error") ){%> onload="window.open('<%=BASEURL%>/jsp/cxpagar/impresion/Cheque.jsp?msj=&laser=<%=laser%>','cheque',' top=2000,left=100, width=900, height=500, scrollbar=no, status=yes, resizable=yes  ');tIngreso();" <%}else{%>onLoad='tIngreso();'<%}%> >



<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Generaci�n de Cheques"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>


    <% Banco b            = model.servicioBanco.obtenerBanco( cheque.getDistrito(),cheque.getBanco(), cheque.getSucursal() ); 
       String agencia     = b.getCodigo_Agencia();
       String nom_agencia = model.ciudadService.buscarNomCiudadxCodigo(agencia);
    %>

     <form action="<%=CONTROLLER%>?estado=Cheque&accion=XFactura&evento=IMPRIMIR" method="post" name="formulario">  
     <table  border="2" align="center">
         <tr>
            <td colspan='2'>            

                        <table width="100%"   align="center">
                                  <tr>
                                     <td colspan='3' >
                                          <table width='100%'  class="barratitulo">
                                                <tr class="fila">
                                                        <td align="left" width='50%' class="subtitulo1" nowrap> VISTA PREVIA DE CHEQUE</td>
                                                        <td align="left" width='*' ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                                                </tr>
                                          </table>
                                     </td>
                                  </tr>
                                  
                                   <tr class="fila">
                                           <td width='27%' >&nbsp; BANCO </td>
                                           <td colspan="2" style="font size:12">
                                               <%= cheque.getBanco()      %> &nbsp;&nbsp; <%= cheque.getSucursal()%> &nbsp;&nbsp;  <%=cheque.getCuentaBanco() %>
                                           </td>
                                   </tr>
                                   
                                   <tr class="fila">
                                           <td >&nbsp; CHEQUE</td>
                                           <td colspan="2" style="font size:12">
                                              <%=  cheque.getNumero() %>
                                           </td>
                                    </tr>
                                  
                                   <tr class="fila">                                      
                                           <td >&nbsp; PROVEEDOR </td>
                                           <td colspan="2"  style="font size:12">
                                              <%=  cheque.getBeneficiario() +" &nbsp;&nbsp; "+ cheque.getNombre()  %>
                                           </td>
                                    </tr>
 
                                   <tr class="fila">                                      
                                           <td >&nbsp; BENEFICIARIO </td>
                                           <td colspan="2"  style="font size:12">
                                              <%=  cheque.getNit_beneficiario() +" &nbsp;&nbsp; "+ cheque.getNom_beneficiario() %>
                                           </td>
                                    </tr>
                                    
                                    <tr class="fila">
                                          <td >&nbsp; TOTAL A PAGAR</td>
                                          <td colspan="2" style="font size:12">
                                                  <%=  Util.customFormat(cheque.getMonto()) %> &nbsp;  <%= cheque.getMoneda() %> 
                                          </td>
                                    </tr>
                                    
                                    <tr class="fila">
                                          <td >&nbsp; AGENCIA</td>
                                          <td colspan="2" nowrap class="bordereporte"><%=nom_agencia%>                                                                                      
                                            <input type='hidden' name='agencia' id='agencia' value='<%=agencia%>'>
                                          </td>
                                    </tr>
									
									<tr class="fila">
										<td >&nbsp; TIPO DE PAGO</td>
										<td width="23%" ><select name='tipo' id='tipo' onChange='tIngreso();'>
												<option value='C' <%=(request.getParameter("tipo")!=null?request.getParameter("tipo"):"").equals("C")?"selected":""%> >CHEQUE</option>
												<option value='T' <%=(request.getParameter("tipo")!=null?request.getParameter("tipo"):"").equals("T")?"selected":""%> >TRANSFERENCIA</option>
											 </select>	
										</td>
								        <td width="50%" >
											<table width="100%" id='banco' style="display:none">
											 	<tr class='fila' >
													<td width="11%">Banco</td>
													<td width="39%"><input:select name='banco' default='<%=request.getParameter("banco")!=null?request.getParameter("banco"):""%>' attributesText="class=textbox; onChange=\"cargarSelects('controller?estado=Ingreso&accion=Cargar&carpeta=jsp/cxpagar/impresion&pagina=viewChequeXFactura.jsp&evento=sucursal')\"" options='<%= model.servicioBanco.getBanco() %>'/>
													</td>
													<script>formulario.banco.value = '<%=request.getParameter("banco")!=null?request.getParameter("banco"):"".replaceAll("\n","")%>';</script>
													<td width="15%">Sucursal</td>
													<td width="35%"><input:select name="sucursal" default='<%=request.getParameter("sucursal")!=null?request.getParameter("sucursal"):""%>' attributesText="class=textbox;" options="<%= model.servicioBanco.getSucursal() %>" />
													</td>
													<script>formulario.sucursal.value = '<%=request.getParameter("sucursal")!=null?request.getParameter("sucursal"):"".replaceAll("\n","")%>';</script>
												</tr>
										  </table>
										</td>
									</tr>
                                                                                                                                                
                             <!-- DETALLE DEL CHEQUE : FORMADO POR LAS FACTURAS -->
                                    
                                    <tr>
                                         <td colspan='3' >
                                              <table width='100%'  class="barratitulo">
                                                    <tr class="fila">
                                                            <td align="left" width='50%' class="subtitulo1" nowrap> DETALLE</td>
                                                            <td align="left" width='*' ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                                                    </tr>
                                              </table>
                                         </td>
                                   </tr>
                                    
                                    <tr class="fila">
                                        <td colspan='3'>
                                              <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                                                  <tr class="tblTitulo" >
                                                      <TH  width='30'  title='Item'                    >ITEM           </TH>
                                                      <TH  width='80'  title='N�mero de factura'       >FACTURA        </TH>
                                                      <TH  width='230' title='Descripci�n factura'     >DESCRIPCION    </TH>               
                                                      <TH  width='90'  title='Valor de la factura'     >VALOR FACT     </TH>               
                                                      
                                                      <!--
                                                      <TH  width='90'  title='Rica + Riva '            >DESCUENTOS     </TH>               
                                                      <TH  width='90'  title='Retefuente'              >RETENCION      </TH>
                                                      -->
                                                      
                                                      <TH  width='45'  title='IVA'                     >IVA     </TH>               
                                                      <TH  width='45'  title='RIVA'                    >RIVA     </TH>               
                                                      <TH  width='45'  title='RICA'                    >RICA     </TH>               
                                                      <TH  width='45'  title='Retefuente'              >RFTE     </TH>               
                                                      
                                                      <TH  width='90'  title='Valor neto factura'      >VLR   NETO     </TH>            
                                                      <TH  width='100' title='Valor saldo factura'     >SALDO          </TH>            
                                                 </tr>
                                                 
                                                 <!-- Lista de facturas -->
                                                 <% List listFacturas =  cheque.getFacturas();
                                                   for(int i=0;i<listFacturas.size();i++){
                                                      FacturasCheques    factura = (FacturasCheques) listFacturas.get(i);  %>
                                                      <tr class='<%= (i%2==0?"filagris":"filaazul") %>' style="font size:11" >
                                                          <td class="bordereporte" align='center' style="font size:11"> <%= i+1 %>                      </td>
                                                          <td class="bordereporte"                style="font size:11"> <%= factura.getDocumento()   %> </td>
                                                          <td class="bordereporte"                style="font size:11"> <%= factura.getDescripcion() %> </td>                                                                                                                    
                                                          <td class="bordereporte" align='right'  style="font size:11"> <%= Util.customFormat( factura.getVlrFactura()   ) %> </td>
                                                          
                                                          <!--
                                                          <td class="bordereporte" align='right'  style="font size:11"> <%//= Util.customFormat( factura.getVlrDescuentos() ) %> </td>                                                                                                                    
                                                          <td class="bordereporte" align='right'  style="font size:11"> <%//= Util.customFormat( factura.getVlrRetencion()  ) %> </td>
                                                          -->
                                                          
                                                          <td class="bordereporte" align='right'  style="font size:11"> <%= Util.customFormat( factura.getValor_iva()    ) %> </td>
                                                          <td class="bordereporte" align='right'  style="font size:11"> <%= Util.customFormat( factura.getValor_riva()    ) %> </td>
                                                          <td class="bordereporte" align='right'  style="font size:11"> <%= Util.customFormat( factura.getValor_rica()    ) %> </td>
                                                          <td class="bordereporte" align='right'  style="font size:11"> <%= Util.customFormat( factura.getValor_rtfe()    ) %> </td>
                                                          
                                                          <td class="bordereporte" align='right'  style="font size:11"> <%= Util.customFormat( factura.getVlrNetoPago()   ) %> </td>
                                                          <td class="bordereporte" align='right'  style="font size:11"> <%= Util.customFormat( factura.getVlrPagar()      ) %> </td>
                                                      </tr>
                                                      
                                                      <!-- Documentos Relacionados -->
                                                      <% List docRel = factura.getDocumnetosRelacionados();
                                                         for(int j=0;j<docRel.size();j++){
                                                             FacturasCheques    facturaRel = (FacturasCheques)docRel.get(j);%>
                                                             
                                                             <tr class='<%= (i%2==0?"filagris":"filaazul") %>' style="font size:11" >
                                                                  <td class="bordereporte" align='center' style="font size:11"> </td>
                                                                  <td class="bordereporte"                style="font size:11"> <%= facturaRel.getDocumento()   %> </td>
                                                                  <td class="bordereporte"                style="font size:11"> <%= facturaRel.getDescripcion() %> </td>
                                                                  
                                                                  <td class="bordereporte" align='right'  style="font size:11"> <%= Util.customFormat( facturaRel.getVlrFactura()    ) %> </td>
                                                                  <td class="bordereporte" align='right'  style="font size:11"> <%= Util.customFormat( facturaRel.getValor_iva() ) %> </td>
                                                                  <td class="bordereporte" align='right'  style="font size:11"> <%= Util.customFormat( facturaRel.getValor_riva()  ) %> </td>
                                                                  <td class="bordereporte" align='right'  style="font size:11"> <%= Util.customFormat( facturaRel.getValor_rica()  ) %> </td>
                                                                  <td class="bordereporte" align='right'  style="font size:11"> <%= Util.customFormat( facturaRel.getValor_rtfe()  ) %> </td>                                                                  
                                                                  <td class="bordereporte" align='right'  style="font size:11"> <%= Util.customFormat( facturaRel.getVlrNetoPago()   ) %> </td>

                                                                  <td class="bordereporte" align='right'  style="font size:11">  </td>
                                                              </tr>
                                                      <%}%>
                                                      
                                                      
                                                 <%}%>
                                                 
                                              </table>
                                       </td>
                                    </tr>
                                                                        
                                    
                                    <tr class="fila">
                                          <td colspan='3' align='right' >
                                                 &nbsp; TOTAL A PAGAR
                                                 <input type='text' name='vlrPagar' <%=(activo==true)?"":"readonly='readonly'"%>  style="text-align:right; width:110" onkeyup="if(this.value!='')formatear(this);"      value='<%= request.getParameter("vlrPagar")!=null && !request.getParameter("vlrPagar").equals("")?request.getParameter("vlrPagar"):Util.customFormat( cheque.getMonto() )%>' title='Valor a cancelar' >                                                  
                                          </td>
                                    </tr>
                                    
                                  
                        </table>
                        
           </td>
         </tr>
     </table>          
     
     </form>
     

     
     
     
     
     <!--Botones -->
     <p>       
           <% if( activo ){%>
                <img src='<%=BASEURL%>/images/botones/aceptar.gif'         style=" cursor:hand"  title='Imprimir Cheque'    name='i_print'       onclick="sendCheque(formulario,'<%=Util.customFormat( cheque.getMonto() ) %>');"    onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>                 
                <% if( cheque.getContDetalle()> cheque.getTopeDetalle() ){%>
                      <img src='<%=BASEURL%>/images/botones/detalles.gif'   style=" cursor:hand"  title='Imprimir Detalle'   name='i_detail'      onclick="imprimirAnexo()"                                                            onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
                <% }%>
           <%}%>
           <img src='<%=BASEURL%>/images/botones/regresar.gif'              style=" cursor:hand"  title='Regresar'           name='i_regre'       onclick="location.href='<%=CONTROLLER%>?estado=Cheque&accion=XFactura&evento=RESET'" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> 
           <img src='<%=BASEURL%>/images/botones/salir.gif'                 style=' cursor:hand'   title='Salir...'           name='i_salir'       onclick='parent.close();'                                                            onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
     </p> 

     
     
     
    <font  class='informacion'> Cantidad de cheques faltantes <%= totalChequesfaltantes %> </font> 
     
     
     
     
     <!-- Mensaje -->
   <% if( msj!=null && !msj.equals("") && model.ChequeXFacturaSvc.getListCheques()!=null && model.ChequeXFacturaSvc.getListCheques().size()>=0 ){%>        
         <table border="2" align="center">
          <tr>
            <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
              <tr>
                <td width="550" align="center" class="mensajes"><%=msj%></td>
                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                <td width="58">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
        </table>
    <%  } %>
    
    
     
</div>

</body>
</html>
<font id='aa'></font>