<!--  
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      07/10/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Generacion Cheque X Factura
 --%>

<%@ page session   ="true"%> 
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
        <title>Vista previa de cheque</title>    
        <link href="<%= BASEURL %>/css/estilostsp.css"     rel="stylesheet" type="text/css"> 
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        
        
        
<%  Cheque   cheque           =  model.ChequeXFacturaSvc.getCheque(); 
    String   msj              =  request.getParameter("msj"); 
    boolean  activo           = (cheque.getMonto()>0)?true:false; 
    Precheque pre = model.ChequeXFacturaSvc.getPrecheque();
    String   print = request.getParameter("print");%>
    
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">



<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Generación de Archivos Transferencia"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>


    <% Banco b            = model.servicioBanco.obtenerBanco( cheque.getDistrito(),cheque.getBanco(), cheque.getSucursal() ); 
       String agencia     = b.getCodigo_Agencia();
       String nom_agencia = model.ciudadService.buscarNomCiudadxCodigo(agencia);
    %>

     <form action="<%=CONTROLLER%>?estado=Cheque&accion=XTransferencia&opcion=CREAR" method="post" name="formulario">  
     <table width="600"  border="2" align="center">
         <tr>
            <td>            

				<table width="100%"   align="center">
					  <tr>
						 <td colspan='2' >
							  <table width='100%'  class="barratitulo">
									<tr class="fila">
											<td align="left" width='50%' class="subtitulo1" nowrap> VISTA PREVIA ARCHIVO TRANFERENCIA </td>
											<td align="left" width='*' ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
									</tr>
							  </table>
						 </td>
					  </tr>
					  
					   <tr class="fila">
							   <td width='31%' >&nbsp; BANCO TRANSFERENCIA</td>
							   <td width="69%" style="font size:12">
								   <%= request.getParameter("banco")      %> &nbsp;&nbsp; <%= request.getParameter("sucursal")%> &nbsp;&nbsp;  <%=cheque.getCuentaBanco() %>
							   </td>
					   </tr>
					  
					  <tr class="fila">
							   <td >&nbsp; CHEQUE</td>
							   <td colspan="2" style="font size:12">
								  <%=  cheque.getNumero() %>
							   </td>
						</tr>
						
					   <tr class="fila">                                      
							   <td >&nbsp; PROVEEDOR </td>
							   <td style="font size:12">
								  <%=  cheque.getBeneficiario() +" &nbsp;&nbsp; "+ cheque.getNombre()  %>
							   </td>
						</tr>

					   <tr class="fila">                                      
							   <td >&nbsp; BENEFICIARIO </td>
							   <td style="font size:12">
								  <%=  cheque.getNit_beneficiario() +" &nbsp;&nbsp; "+ cheque.getNom_beneficiario() %>
							   </td>
						</tr>
						
						<tr class="fila">
							  <td >&nbsp; TOTAL A PAGAR</td>
							  <td style="font size:12">
									  <%=  Util.customFormat(cheque.getMonto()) %> &nbsp;  <%= cheque.getMoneda() %> 
							  </td>
						</tr>
						
						<tr class="fila">
							  <td >&nbsp; TOTAL ABONO</td>
							  <td  style="font size:12"><%=Util.customFormat(Double.parseDouble(request.getParameter("vlr")))%> &nbsp; <%= request.getParameter("moneda1") %>                                                                                     
								
							  </td>
						</tr>
																																	
				 <!-- DETALLE DEL CHEQUE : FORMADO POR LAS FACTURAS -->
						
						<tr>
							 <td colspan='2' >
								  <table width='100%'  class="barratitulo">
										<tr class="fila">
												<td align="left" width='50%' class="subtitulo1" nowrap> DETALLE</td>
												<td align="left" width='*' ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
										</tr>
								  </table>
							 </td>
					   </tr>
						
						<tr class="fila">
							<td colspan='2'><textarea name="descripcion" readonly cols="150" rows="3" class="textbox"><%=request.getAttribute("transferencia")%></textarea></td>
						</tr>
				</table>
                        
           </td>
         </tr>
     </table>          
     <input type='hidden' value='<%=request.getParameter("banco")%>' name='banco'>
	 <input type='hidden' value='<%=request.getParameter("sucursal")%>' name='sucursal'>
	 <input type='hidden' value='<%=request.getParameter("vlr")%>' name='vlr'>
	 <input type='hidden' name='agencia' id='agencia' value='<%=agencia%>'>     
     </form>
     <!--Botones -->
     <p>       
           <% if( activo ){%>
                <img src='<%=BASEURL%>/images/botones/aceptar.gif'         style=" cursor:hand"  title='Aceptar Transferencia'    name='i_print'       onclick="formulario.submit();"    onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>                 

           <%}%>
           <img src='<%=BASEURL%>/images/botones/regresar.gif'              style=" cursor:hand"  title='Regresar'           name='i_regre'       onclick="location.href='<%=CONTROLLER%>?estado=Cheque&accion=XFactura&evento=RESET'" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>  
           <img src='<%=BASEURL%>/images/botones/salir.gif'                 style=' cursor:hand'   title='Salir...'           name='i_salir'       onclick='parent.close();'                                                            onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
     </p> 

      <!-- Mensaje -->
   <% if( msj!=null && !msj.equals("") ){%>        
         <table border="2" align="center">
          <tr>
            <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
              <tr>
                <td width="550" align="center" class="mensajes"><%=msj%></td>
                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                <td width="58">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
        </table>
    <%  } %>
    
    
     
</div>
</body>
</html>