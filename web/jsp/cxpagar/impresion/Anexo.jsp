<!--  
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      27/12/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Imprime anexo al detalle de un Cheque, cuando las facturas sobrepasan el espacio disponible.
 --%>

<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>

<html>
<head>
         <title>Anexo del Cheque</title>
         <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
         <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
         <style>.comentario{color:000000;font-family:  Verdana,Arial, Helvetica, sans-serif;font-size: 11px; }</style>
         <style>.cifras{color:000000;font-family:  Verdana,Arial, Helvetica, sans-serif;font-size: 10px; }</style>            
         

</head>
<body onload='window.print(); window.close();'>



 <%  Usuario Usuario       = (Usuario) session.getAttribute("Usuario");
     String  usuario       = Usuario.getLogin();
     Cheque  cheque        = model.ChequeXFacturaSvc.getCheque();
     List    facturasList  = cheque.getFacturas();
     int     tamanoLinea   = 13;
     int     tamanoColumn  = 1;
     int     left          = 0;%>
     
     
     
     
      <!-- TITULOS -->
     
          <DIV class=comentario STYLE="position:absolute; left:58px; top:20px; "><b>DETALLE DE CHEQUE (No v�lido para banco)</b>                     </DIV>            
          <DIV class=comentario STYLE="position:absolute; left:301px; top:40px; ">CHEQUE:&nbsp<b><%=cheque.getNumero()%></b>                         </DIV>            
          <DIV class=comentario STYLE="position:absolute; left:228px; top:53px; ">   <%=cheque.getAno()%>-<%=cheque.getMes()%> -<%=cheque.getDia()%> </DIV>     
          <DIV class=comentario STYLE="position:absolute; left:353px; top:53px; ">   <%=cheque.getBanco() %> &nbsp <%=cheque.getSucursal()%>         </DIV>
          <DIV class=comentario STYLE="position:absolute; left:58px;  top:66px; ">   <%=cheque.getNom_beneficiario()%>                                         </DIV> 
          <DIV class=comentario STYLE="position:absolute; left:353px; top:66px; "><b> NIT </b>                  </DIV>  
          <DIV class=comentario STYLE="position:absolute; left:384px; top:66px; "> <%=cheque.getNit_beneficiario()%> </DIV>  
          
     
          <DIV class=comentario STYLE="position:absolute; left:<%=left   %>px;  top:340px; "><b> FACTURA        </b></DIV> 
          <DIV class=comentario STYLE="position:absolute; left:<%=left + 95 %>px;  top:340px; "><b> VALOR FACT     </b></DIV>            
                  
          <DIV class=comentario STYLE="position:absolute; left:<%=left + 223 %>px;  top:340px; "><b> RIVA           </b></DIV> 
          <DIV class=comentario STYLE="position:absolute; left:<%=left + 302 %>px;  top:340px; "><b> RICA           </b></DIV> 
          <DIV class=comentario STYLE="position:absolute; left:<%=left + 376 %>px;  top:340px; "><b> RFTE           </b></DIV> 

          <DIV class=comentario STYLE="position:absolute; left:<%=left + 442 %>px;  top:340px; "><b> VRL &nbspNETO   </b></DIV> 
          <DIV class=comentario STYLE="position:absolute; left:<%=left + 560 %>px;  top:340px; "><b> SALDO          </b></DIV> 
         
          
          
        <!-- DETALLES DEL CHEQUE -->  
                        
         <% int  top     = 120;
            int  max     = 0;
            for(int j=0;j<facturasList.size();j++){
              FacturasCheques   factura = (FacturasCheques)   facturasList.get(j);%> 
              
                <!-- Facturas-->
                        <DIV class=comentario STYLE="position:absolute; left:<%=left   %>px;   top:<%=top + max%>px;"> <%=factura.getDocumento()%>   </DIV>
                         <DIV class=cifras STYLE="position:absolute; left:<%=left + 85 %>px;   top:<%=top + max%>px;  width:90">
                              <table width='100%'  cellpadding='0' cellspacing='0'  >
                                  <tr><td align='right' class=cifras>
                                        <%=Util.customFormat( factura.getVlrFactura() + factura.getValor_iva() )%>  
                                  </td></tr>
                              </table>
                         </DIV> 
                         <DIV class=cifras STYLE="position:absolute; left:<%=left + 165 %>px; top:<%=top + max%>px;   width:90"> 
                               <table width='100%'  cellpadding='0' cellspacing='0'  >
                                  <tr><td align='right' class=cifras>
                                       <%=Util.customFormat(  factura.getValor_riva() )  %>  
                                 </td></tr>
                              </table>
                         </DIV>
                         <DIV class=cifras STYLE="position:absolute; left:<%=left + 242 %>px; top:<%=top + max%>px;   width:90"> 
                               <table width='100%'  cellpadding='0' cellspacing='0'  >
                                  <tr><td align='right' class=cifras>
                                       <%=Util.customFormat(  factura.getValor_rica() )  %>  
                                 </td></tr>
                              </table>
                         </DIV>
                         
                         <DIV class=cifras STYLE="position:absolute; left:<%=left + 318 %>px; top:<%=top + max%>px;   width:90"> 
                               <table width='100%'  cellpadding='0' cellspacing='0'  >
                                  <tr><td align='right' class=cifras>
                                       <%=Util.customFormat( factura.getValor_rtfe() )  %>  
                                 </td></tr>
                              </table>
                         </DIV>
                         <DIV class=cifras STYLE="position:absolute; left:<%=left + 416 %>px; top:<%=top + max%>px;   width:90"> 
                              <table width='100%'   cellpadding='0' cellspacing='0'  >
                                  <tr><td align='right' class=cifras>
                                      <%=Util.customFormat(factura.getVlrNetoPago() )%> 
                                 </td></tr>
                             </table>
                         </DIV>
                         <DIV class=cifras STYLE="position:absolute; left:<%=left + 514 %>px; top:<%=top + max%>px;   width:90"> 
                              <table width='100%'   cellpadding='0' cellspacing='0'  >
                                  <tr><td align='right' class=cifras>
                                        <%=Util.customFormat(  factura.getVlrPagar() )%>  
                                 </td></tr>
                             </table>
                         </DIV>                 
                 
                 <!-- Documentos Relacionados-->
                 
                 <% List docRel = factura.getDocumnetosRelacionados();
                    for(int i=0;i<docRel.size();i++){
                        max+= tamanoLinea;
                        FacturasCheques   factRel = (FacturasCheques)   docRel.get(i);%>                        

                            <DIV class=comentario STYLE="position:absolute; left:<%=left + 10  %>px;  top:<%=top + max%>px;"> <%=factRel.getDocumento()%>    </DIV>                         
                                 <DIV class=cifras STYLE="position:absolute; left:<%=left + 85 %>px;  top:<%=top + max%>px;  width:90">
                                      <table width='100%'  cellpadding='0' cellspacing='0'  >
                                          <tr><td align='right' class=cifras>
                                                <%=Util.customFormat( factRel.getVlrFactura() + factRel.getValor_iva() )%>  
                                          </td></tr>
                                      </table>
                                 </DIV> 
                                 <DIV class=cifras STYLE="position:absolute; left:<%=left + 165 %>px; top:<%=top + max%>px;   width:90"> 
                                       <table width='100%'  cellpadding='0' cellspacing='0'  >
                                          <tr><td align='right' class=cifras>
                                               <%=Util.customFormat(  factRel.getValor_riva() )  %>  
                                         </td></tr>
                                      </table>
                                 </DIV>
                                 <DIV class=cifras STYLE="position:absolute; left:<%=left + 242 %>px; top:<%=top + max%>px;   width:90"> 
                                       <table width='100%'  cellpadding='0' cellspacing='0'  >
                                          <tr><td align='right' class=cifras>
                                               <%=Util.customFormat( factRel.getValor_rica() )  %>  
                                         </td></tr>
                                      </table>
                                 </DIV>
                                 <DIV class=cifras STYLE="position:absolute; left:<%=left + 318 %>px; top:<%=top + max%>px;   width:90"> 
                               <table width='100%'  cellpadding='0' cellspacing='0'  >
                                  <tr><td align='right' class=cifras>
                                       <%=Util.customFormat( factRel.getValor_rtfe() )  %>  
                                 </td></tr>
                              </table>
                         </DIV>
                                 <DIV class=cifras STYLE="position:absolute; left:<%=left + 416 %>px; top:<%=top + max%>px;   width:90"> 
                                      <table width='100%'   cellpadding='0' cellspacing='0' >
                                          <tr><td align='right' class=cifras>
                                              <%=Util.customFormat(factRel.getVlrNetoPago())%> 
                                         </td></tr>
                                     </table>
                                 </DIV>                         
                 
                 
                  <%}
                max+= tamanoLinea;
           }%>
           
           
        <!-- CONFIGURA MARGEN DE ACUERDO A TOTAL FACTURA -->   
        <% double posLinea  = top + max + (tamanoLinea*3); 
           double posVlr    = posLinea  + (tamanoLinea*2); 
           double posUser   = posVlr    + (tamanoLinea*3); 
        %>
           
           
           
        <!-- MONTO CHEQUE Y USUARIO --> 
          <DIV class=comentario STYLE="position:absolute; left:<%=left + 440 %>px; top:'<%=posLinea%>px'; width:140;">
                   <table width='100%'   cellpadding='0' cellspacing='0'>
                     <tr><td align='right' class=comentario>
                            _________________ 
                      </td></tr>
                </table>
          </DIV>  
          <DIV class=comentario STYLE="position:absolute; left:<%=left + 440 %>px; top:'<%=posVlr%>px';  width:140">
                <table width='100%'   cellpadding='0' cellspacing='0' >
                     <tr><td align='right' class=comentario>
                            $&nbsp <%= Util.customFormat(cheque.getVlrPagar()) %>  
                     </td></tr>
                </table>
          </DIV>
          <DIV class=comentario STYLE="position:absolute; left:<%=left + 20 %>px;  top:'<%=posUser%>px'; "> <%=usuario%> </DIV>  

          
          

</body>
</html>
