<!--  
     - Author(s)       :      Osvaldo P�rez Ferrer
     - Date            :      21/11/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Filtro para facturas por corrida
 --%>

<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<%
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>

<html>
<head>

    <title>Cheque Facturas por Corrida</title>    
    <link href="<%= BASEURL %>/css/estilostsp.css"     rel="stylesheet" type="text/css"> 
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validate.js"></script> 
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validar.js"></script> 
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    
    
    
        
    <script>
    
        <%= model.ChequeFactXCorridasSvc.getSucursalesJS() %>
        
        function sendForm( f ){
            
            var sw=0;
            for(var i=0;i<f.length;i++){
                var ele = f.elements[i];
                if( ele.value=='' && ele.name!='nit' ){
                   alert('Deber� llenar el campo ' +  ele.name );
                   ele.focus();
                   sw=1;
                   break;
                }
            }
            
            if( sw==0 ){
                if( f.nit.value=='' )
                    f.nit.value ='ALL';
                f.submit();
            }
            
        }
        
        
        
        function loadSucursal(sucursal, banco){
            var vector =  sucursales.split("||");
            var ele    = '';
            for(var i=0;i<vector.length;i++){
                var vectorSuc  = vector[i].split("|");
                if( vectorSuc[0] == banco ){
                    if ( ele != '' )
                         ele += '|';
                    ele += vectorSuc[1];
                }
            }
            
            if( ele != '' ){ 
                var vecEle = ele.split('|');
                sucursal.length = vecEle.length + 1;
                sucursal.options[0].value = '';
                sucursal.options[0].text  = 'Seleccionar';
                sucursal.options[0].selected='selected';
                for(var i=0; i <vecEle.length;i++){
                    sucursal.options[i+1].value = vecEle[i];
                    sucursal.options[i+1].text  = vecEle[i];
                }
            }               
        }       
        
    </script>
    
     
</head>
<body>


<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Impresi�n de Cheques en Corridas"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>


 
  <% String   msj     =  request.getParameter("msj"); 
     String   url     =  CONTROLLER +"?estado=ChequeFacturas&accion=Corridas&evento=LOAD";     
     List     bancos  =  model.ChequeFactXCorridasSvc.getListBancos(); %>
    
  <form action="<%=url%>" method="post" name="formulario" id="formulario">
  
     <table width="400"  border="2" align="center">
         <tr>
            <td colspan='2'>             

                        <table width="100%"   align="center">
                                  <tr>
                                     <td colspan='2'>
                                          <table width='100%'  class="barratitulo">
                                                <tr class="fila">
                                                        <td align="left" width='70%' class="subtitulo1" nowrap> &nbsp;IMPRESION DE CHEQUES</td>
                                                        <td align="left" width='30%' ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                                                </tr>
                                          </table>
                                     </td>
                                  </tr>
                                  
                                  
                                   <tr class="fila">
                                           <td width='30%' >&nbsp CORRIDA</td>
                                           <td>
                                              <input name='corrida' id='corrida'  type="textbox" style='width:100' maxlength='6' onkeyPress='soloDigitos(event,"decNo")' >&nbsp;<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">                                                                                             
                                           </td>
                                   </tr>
                                  
                                    <tr class="fila">
                                           <td width='30%' >&nbsp BANCO</td>
                                           <td width='100%'>
                                              <select  style='width:80%'  name='banco'  onchange='loadSucursal(this.form.sucursal, this.value);'>
                                                  <option value=''> Seleccionar </option>
                                                  <% for(int i=0;i<bancos.size();i++){
                                                         String code = (String) bancos.get(i);%>
                                                         <option value='<%=code%>'> <%=code%> </option>
                                                  <%}%>
                                              </select>  &nbsp;<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">
                                           </td>
                                   </tr>
                                   
                                   <tr class="fila">
                                           <td width='30%' >&nbsp SUCURSAL</td>
                                           <td>
                                              <select  style='width:80%'  name='sucursal'  id='sucursal'>
                                                  <option value=''> Seleccionar </option>
                                              </select>  &nbsp;<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">
                                           </td>
                                   </tr>
                                   
                                   <tr class="fila">
                                           <td width='30%' >&nbsp PROPIETARIO </td>
                                           <td>
                                              <input name='nit' id='nit' type="textbox" style='width:50%'  maxlength='15'  onkeyPress='soloDigitos(event,"decNo")' >                                                                                             
                                           </td>
                                   </tr>
                                                                                                                                                                               
                        </table>

                </td>
             </tr>
       </table>     
       
  <form>
                
  <p>       
       <img src='<%=BASEURL%>/images/botones/aceptar.gif'    style=" cursor:hand"  title='Crear....'       name='i_crear'       onclick="sendForm(formulario)" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>               
       <img src='<%=BASEURL%>/images/botones/salir.gif'      style=' cursor:hand'  title='Salir...'        name='i_salir'       onclick='parent.close();'      onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
  </p> 
       

  
  <font id='msj' class='informacion'></font>
  
  
  <!-- Mensaje -->
   <% if(msj!=null && !msj.equals("")){%>        
         <table border="2" align="center">
          <tr>
            <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
              <tr>
                <td width="550" align="center" class="mensajes"><%=msj%></td>
                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                <td width="58">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
        </table>
    <%  } %>


</div>
<%=datos[1]%>
</body>
</html>

