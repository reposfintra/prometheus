<!--
	 - Author(s)       :      Jose de la rosa
	 - Description     :      AYUDA DESCRIPTIVA - Extracto
	 - Date            :      01/06/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>AYUDA DESCRIPTIVA - Cheque por Facturas</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Cheques Por Facturas </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Impresión de Cheques </td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2">INFORMACION</td>
        </tr>
        <tr>
          <td  class="fila">Distrito</td>
          <td  class="ayudaHtmlTexto">Campo para escoger el distrito de la corrida a imprimir.</td>
        </tr>
        <tr>
          <td  class="fila">Proveedor</td>
          <td  class="ayudaHtmlTexto">Campo para escoger al proveedor que se le van a pagar las facturas.</td>
        </tr>
        <tr>
          <td  class="fila">Tipo Factura </td>
          <td  class="ayudaHtmlTexto">Campo para escoger el tipo de las facturas. </td>
        </tr>
        <tr>
          <td  class="fila">Facturas del proveedor </td>
          <td  class="ayudaHtmlTexto">Campo donde aparecen todas las facturas del proveedor.</td>
        </tr>		
        <tr>
          <td  class="fila">Facturas para el cheque </td>
          <td  class="ayudaHtmlTexto">Campo donde aparecen todas las facturas que se la van a aplicar al cheque.</td>
        </tr>	        		
        <tr>
          <td class="fila">Bot&oacute;n Aceptar </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para confirmar y realizar la impresión de corrida.</td>
        </tr>
		<tr>
          <td class="fila">Bot&oacute;n Cancelar </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que resetea la pantalla dejandola en su estado inicial de ingreso.</td>
        </tr>
		<tr>
          <td width="149" class="fila">Bot&oacute;n Salir</td>
          <td width="525"  class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista 'Cheque Por Facturas' y volver a la vista del men&uacute;.</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</body>
</html>
