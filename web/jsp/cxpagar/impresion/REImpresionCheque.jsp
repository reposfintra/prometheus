<!--
     - Author(s)       :      Osvaldo Pérez Ferrer
     - Date            :      26-02-2007
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Reimpresion de cheques
 --%>

<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<%
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>

<html>
<head>

    <title>-- Reimpresión de cheques --</title>    
    <link href="<%= BASEURL %>/css/estilostsp.css"     rel="stylesheet" type="text/css"> 
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validate.js"></script> 
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validar.js"></script> 
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    
    
    
        
    <script>
    
        <%= model.ChequeFactXCorridasSvc.getSucursalesJS() %>
        
        var campos = new Array("seleccionar Banco","seleccionar Sucursal","ingresar Rango Inicial","ingresar Rango Final");
        
        function sendForm( f ){
            
            var sw=0;
            for(var i=0;i<f.length;i++){
                var ele = f.elements[i];
                if( ele.value=='' && ele.name!='nit' ){
                   alert('Debe ' +  campos[i] );
                   ele.focus();
                   sw=1;
                   break;
                }
            }
            
            if( sw == 0 ){
                f.submit();
            }
            
            
        }
        
        
        
        function loadSucursal(sucursal, banco){
            var vector =  sucursales.split("||");
            var ele    = '';
            for(var i=0;i<vector.length;i++){
                var vectorSuc  = vector[i].split("|");
                if( vectorSuc[0] == banco ){
                    if ( ele != '' )
                         ele += '|';
                    ele += vectorSuc[1];
                }
            }
            
            if( ele != '' ){ 
                var vecEle = ele.split('|');
                sucursal.length = vecEle.length + 1;
                sucursal.options[0].value = '';
                sucursal.options[0].text  = 'Seleccionar';
                sucursal.options[0].selected='selected';
                for(var i=0; i <vecEle.length;i++){
                    sucursal.options[i+1].value = vecEle[i];
                    sucursal.options[i+1].text  = vecEle[i];
                }
            }               
        }       
        
    </script>
    
     
</head>
<body>


<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reimpresión de cheques"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>


 
  <% String   msj     =  request.getParameter("msj"); 
     String   url     =  CONTROLLER +"?estado=Cheque&accion=Reimpresion&evento=LISTAR";     
     List     bancos  =  model.ChequeFactXCorridasSvc.getListBancos(); %>
    
     <%
     Vector cheques = model.ChequeXFacturaSvc.getVector();
     List   lista_cheques = model.ChequeXFacturaSvc.getListCheques();
%>
   
  <form action="<%=url%>" method="post" name="formulario" id="formulario">
  
     <table width="400"  border="2" align="center">
         <tr>
            <td colspan='2'>             

                        <table width="100%"   align="center">
                                  <tr>
                                     <td colspan='2'>
                                          <table width='100%'  class="barratitulo">
                                                <tr class="fila">
                                                        <td align="left" width='70%' class="subtitulo1" nowrap> &nbsp;REIMPRESION DE CHEQUES</td>
                                                        <td align="left" width='30%' ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                                                </tr>
                                                                                                
                                                
                                          </table>
                                     </td>
                                  </tr>
                                                                                                     
                                    <tr class="fila">
                                           <td width='30%' >&nbsp Banco</td>
                                           <td width='100%'>
                                              <select  style='width:80%'  name='banco'  onchange='loadSucursal(this.form.sucursal, this.value);'>
                                                  <option value=''> Seleccionar </option>
                                                  <% for(int i=0;i<bancos.size();i++){
                                                         String code = (String) bancos.get(i);%>
                                                         <option value='<%=code%>'> <%=code%> </option>
                                                  <%}%>
                                              </select>  &nbsp;<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">
                                           </td>
                                   </tr>
                                   
                                   <tr class="fila">
                                           <td width='30%' >&nbsp Sucursal</td>
                                           <td>
                                              <select  style='width:80%'  name='sucursal'  id='sucursal'>
                                                  <option value=''> Seleccionar </option>
                                              </select>  &nbsp;<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">
                                           </td>
                                   </tr>
                                   
                                   <tr class="fila">
                                           <td width='30%' >&nbsp Rango inicial </td>
                                           <td>
                                              <input name='ini' id='ini' type="textbox" style='width:50%'  maxlength='15' value='<%=Util.coalesce( request.getParameter("ini"),"" )%>' >
                                           </td>
                                   </tr>
                                   
                                   <tr class="fila">
                                           <td width='30%' >&nbsp Rango final </td>
                                           <td>
                                              <input name='fin' id='fin' type="textbox" style='width:50%'  maxlength='15' value='<%=Util.coalesce( request.getParameter("fin"),"" )%>' >
                                           </td>
                                   </tr>
                                                                                                                                                                               
                        </table>

                </td>
             </tr>
       </table>              
                
  <p>       
       <img src='<%=BASEURL%>/images/botones/buscar.gif'    style=" cursor:hand"  title='Crear....'       name='i_crear'       onclick="sendForm(formulario)" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>               
       <img src='<%=BASEURL%>/images/botones/salir.gif'      style=' cursor:hand'  title='Salir...'        name='i_salir'       onclick='parent.close();'      onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
  </p> 
       
  <form>    
  
  <%
    for( int i=0; i<lista_cheques.size(); i++ ){
        Cheque c = (Cheque) lista_cheques.get(i);
        out.print( c.getNumero() + " - " + c.getBanco()+ " - " + c.getComentario() + " - " + c.getDistrito() + "<br>");
    }
%>
  
  <%if( cheques != null && cheques.size()>0 ){%>
  
  <table width="80%" border="2" align="center">
                    <tr>
                      <td>                                           
                               <table width="100%" align="center">
                                          <tr>
                                                <td width="50%" class="subtitulo1">&nbsp;LISTA DE CHEQUES --- <%=Util.coalesce( request.getParameter("banco"),"" )%>&nbsp;<%=Util.coalesce( request.getParameter("sucursal"),"" )%> del <%=Util.coalesce( request.getParameter("ini"),"" )%> al <%=Util.coalesce( request.getParameter("fin"),"" )%></td>
                                                <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                                          </tr>
                                </table>
                                
                                <TABLE width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                                  <TR class="tblTitulo" align="center">
                                          <TH align='center'  nowrap> </TH>
                                          <TH align='center' nowrap > CHEQUE </TH>
                                          <TH align='center' width='3%' > <img src='<%=BASEURL%>\images\imprimir.gif'>  </TH>                                          
                                          <TH align='center' width='17%'> BENEFICIARIO         </TH>                                          
                                          <TH align='center' nowrap> VALOR                </TH>
                                          <TH align='center' nowrap > MONEDA               </TH>
                                          <TH align='center' nowrap > IMPRIMIÓ              </TH>
                                          <TH align='center' nowrap > FECHA IMP              </TH>
                                          
                                  </TR>
                               
                         <% int cont=0;
                            for( int i=0; i<cheques.size(); i++ ){
                                    Hashtable h = (Hashtable)cheques.get(i);  
                                    cont++;
                                    %>
                                     <form name='formulario_<%=cont%>' id='formulario_<%=cont%>' method='POST'>
                                     
                                         <TR class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">                                                
                                                                                                
                                                <TD nowrap align='center' class="bordereporte"> <%= cont %><input type='hidden' name='idCheque' value='<%=h.get("cheque")%>'></TD>
                                                <TD nowrap align='center' class="bordereporte"> <%=h.get("cheque")%></TD>
                                                <TD nowrap align='center' class="bordereporte"> <input type='checkbox' name='imprimir' > </TD>
                                                                                                
                                                <TD nowrap align='left'   class="bordereporte" title='Proveedor: <%= h.get("prove") + " - " + h.get("nom_prove") %>'><%= h.get("benef") + " - " + h.get("nom_benef") %></TD>                                               
                                                <TD nowrap align='center' class="bordereporte"><%= h.get("valor") %></TD>
                                                <TD nowrap align='center' class="bordereporte"><%= h.get("moneda")%></TD>
                                             
                                                <TD  align='center' class="bordereporte"><%= h.get("usuario_imp")%>            </TD>
                                                <TD  align='center' class="bordereporte"><%= h.get("fecha_imp")%>            </TD>                                                
                                        </TR>
                                    </form>
                       <%}%>
                       
                    </TABLE>
                   </tr>
                </td>
            </table>
            
            <br><br>
            
            <img src="<%=BASEURL%>/images/botones/imprimir.gif" style="cursor:hand" name="Imprimir"  title="Imprimir"                  onclick="print();" onMouseOver="botonOver(this);"   onMouseOut="botonOut(this);">&nbsp;               
               <img src="<%=BASEURL%>/images/botones/salir.gif"    style="cursor:hand" name="salir"     title="Salir al Menu Principal"   onClick="parent.close();"       onMouseOver="botonOver(this);"   onMouseOut="botonOut(this);" >
  
            <%}%>
  
  <font id='msj' class='informacion'></font>
  
  
  <!-- Mensaje -->
    <% if(msj!=null && msj.equals("search") && cheques.size() == 0 ) msj = "No se encontraron cheques para reimpresión con estos parámetros";%>
  
   <% if(msj!=null && !msj.equals("search")){%>
         <table border="2" align="center">
          <tr>
            <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
              <tr>
                <td width="550" align="center" class="mensajes"><%=msj%></td>
                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                <td width="58">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
        </table>
    <%  } %>
   
</div>
<%=datos[1]%>
</body>
</html>


<script>

    var banco = '<%=Util.coalesce( request.getParameter("banco"),"" )%>';        
    
    document.getElementById("banco").value = '<%=Util.coalesce( request.getParameter("banco"),"" )%>';
    
    if( banco != '' ){    
        loadSucursal( document.getElementById('sucursal'),banco );
        document.getElementById('sucursal').value = '<%=Util.coalesce( request.getParameter("sucursal"),"" )%>';
    }
    
    function print(){
        
        var ids = "";
        for(i=0;i<document.forms.length;i++) {
            var forma = document.forms[i];            
            if( forma.imprimir.checked ) { 
                ids += forma.idCheque.value + ",";                
            }
        }        
        if( ids!="" ){            
            location.replace('<%=CONTROLLER%>?estado=Cheque&accion=Reimpresion&evento=REIMPRIMIR&ids='+ids+'&banco=<%=Util.coalesce( request.getParameter("banco"),"" )%>&sucursal=<%=Util.coalesce( request.getParameter("sucursal"),"" )%>');            
        }else{
            alert('Debe seleccionar los cheques');
        }
    }
    
</script>

