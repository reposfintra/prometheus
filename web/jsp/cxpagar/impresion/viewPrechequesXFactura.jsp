<!--
- Autor : Osvaldo P�rez Ferrer
- Date  : 13 de Febrero de 2007 10:39 PM !!
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite la impresion de precheques
--%>

<%-- Declaracion de librerias--%>
<%@page session="true"%> 
<%@page import="java.util.*" %>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*,com.tsp.util.*"%>


<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>


<html>
<head>

	<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
	<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/Validaciones.js"></script>  
        
</head>
<title> -- Cheques pendientes por imprimir -- </title>
<body onLoad="redimensionar();" onResize="redimensionar();" onKeyDown="onKeyDown();" >

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Imprimir Cheques de Facturas Proveedor"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>


<%    
      Vector precheques = model.ChequeXFacturaSvc.getPrecheques();
      String          msj           = request.getParameter("msj");


      if(precheques!=null && precheques.size()>0){ %>     
            <table width="100%" border="2" align="center">
                    <tr>
                      <td>
                      
                               <table width="100%" align="center">
                                          <tr>
                                                <td width="50%" class="subtitulo1">&nbsp;LISTA DE CHEQUES</td>
                                                <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                                          </tr>
                                </table>
                                
                                <TABLE width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                                  <TR class="tblTitulo" align="center">
                                          <TH align='center'  nowrap> No </TH>
                                          <TH align='center' nowrap > PRECHEQUE </TH>
                                          <TH align='center' width='3%' > <img src='<%=BASEURL%>\images\imprimir.gif'>  </TH>
                                          <TH align='center' width='9%' nowrap > FACTURAS             </TH>
                                          <TH align='center' width='17%'> BENEFICIARIO         </TH>
                                          <TH align='center' nowrap> BANCO                </TH>
                                          <TH align='center' nowrap> SUCURSAL             </TH>
                                          <TH align='center' nowrap> VALOR                </TH>
                                          <TH align='center' nowrap > MONEDA               </TH>
                                          <TH align='center' nowrap> AGENCIA              </TH>
                                          <TH align='center' nowrap > GENER�              </TH>
                                          <TH align='center' nowrap > FECHA GENERADO           </TH>
                                          
                                  </TR>
                               
                         <% int cont=0;
                            for( int i=0; i<precheques.size(); i++ ){
                                    Precheque precheque = (Precheque)precheques.get(i);  
                                    cont++;
                                    %>
                                     <form name='formulario_<%=cont%>' id='formulario_<%=cont%>' method='POST'>
                                     
                                         <TR class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">                                                
                                                                                                
                                                <TD nowrap align='center' class="bordereporte"> <%= cont %><input type='hidden' name='idCheque' value='<%=precheque.getId()%>'></TD>
                                                <TD nowrap align='center' class="bordereporte"> <%=precheque.getId()%></TD>
                                                <TD nowrap align='center' class="bordereporte"> <input type='checkbox'   name='Impresa' > </TD>
                                                
                                                <TD  align='center' class="bordereporte">
                                                    <table cellpadding='3' cellspacing='2' width='100%'>                                                        
                                                    
                                                        <% Vector items = precheque.getItems();%> 
                                                        <%
                                                        for( int j=0; j<items.size(); j++ ){
                                                            Precheque it = (Precheque)items.get(j);%>
                                                            <tr style='width:100%' valign='top' class='<%= (j%2==0?"filagris":"filaazul") %>' style="font size:11" >
                                                                <td width='50%'> <%=it.getDocumento()%> </td>
                                                                <td width='50%' align="right"> <%=Util.customFormat( it.getValor() )%> </td>
                                                            </tr>
                                                        <%}%>
                                                    </table>
                                                </TD>
                                                <TD nowrap align='left'   class="bordereporte" title='Proveedor: <%= precheque.getProveedor() + " - " + precheque.getNom_proveedor() %>'><%= precheque.getBeneficiario() + " - " + precheque.getNom_beneficiario() %></TD>
                                                <TD nowrap align='center'  class="bordereporte"><%= precheque.getBanco()%></TD>
                                                <TD nowrap align='center' class="bordereporte"><%= precheque.getSucursal() %></TD>                 
                                                <TD nowrap align='center' class="bordereporte"><%= Util.customFormat( precheque.getValor() )%></TD>
                                                <TD nowrap align='center' class="bordereporte"><%= precheque.getMoneda()%></TD>
                                             
                                                <TD  align='center' class="bordereporte"><%= precheque.getNom_agencia()%>            </TD>
                                                <TD  align='center' class="bordereporte"><%= precheque.getCreation_user()%>            </TD>
                                                <TD  align='center' class="bordereporte"><%= precheque.getCreation_date()%>            </TD>
                                        </TR>
                                    </form>
                       <%}%>
                       
                    </TABLE>
                   </tr>
                </td>
            </table>
                    
            <p align="center">
                <% String menu_cargar = CONTROLLER + "?estado=Menu&accion=Cargar&carpeta=/jsp/cxpagar/impresion&pagina=ChequesXFactura.jsp&marco=no&opcion=31&item=CHEQUEFACTURA"; %>
                
               <img src="<%=BASEURL%>/images/botones/imprimir.gif" style="cursor:hand" name="Imprimir"  title="Imprimir"                  onclick="funcion( 'print' );" onMouseOver="botonOver(this);"   onMouseOut="botonOut(this);">&nbsp;
               <img src="<%=BASEURL%>/images/botones/anular.gif" style="cursor:hand" name="Imprimir"  title="Imprimir"                  onclick="funcion( 'anulate' );" onMouseOver="botonOver(this);"   onMouseOut="botonOut(this);">&nbsp;               
               <img src="<%=BASEURL%>/images/botones/salir.gif"    style="cursor:hand" name="salir"     title="Salir al Menu Principal"   onClick="parent.close();"       onMouseOver="botonOver(this);"   onMouseOut="botonOut(this);" >
            </p>

       <%}else{%>
            <table border="2" align="center">
                  <tr>
                    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                      <tr>
                        <td width="450" align="center" class="mensajes">No se encontraron Cheques pendientes</td>
                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                      </tr>
                    </table></td>
                  </tr>
                </table>
                
               
                
            <%// if( msj!=null && !msj.equals("") ){  %>
                     <br>
                        <img src="<%=BASEURL%>/images/botones/salir.gif"    style="cursor:hand" name="salir"     title="Salir al Menu Principal"   onClick="parent.close();"       onMouseOver="botonOver(this);"   onMouseOut="botonOut(this);" >
                    <br>
                <%//}%>
               
       <%}%>
   
    
    
    
	<!--  Mensaje de Informaci�n-->
	<br>
	
        <% if( msj!=null && !msj.equals("") ){ %>		      
             <table border="2" align="center">
              <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                  <tr>
                    <td width="450" align="center" class="mensajes"><%=msj%></td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="58">&nbsp;</td>
                  </tr>
                </table></td>
              </tr>
            </table>

        <%}%>               	        
   
</div>
<%=datos[1]%>  
</body>
</html>

<script>

    function funcion( f ){
        
        var ids = "";
        for(i=0;i<document.forms.length;i++) {
            var forma = document.forms[i];
            if( forma.Impresa.checked ) { 
                ids += forma.idCheque.value + ",";                
            }
        }        
        if( ids!="" ){
            if( f == 'print' ){
                location.replace('<%=CONTROLLER%>?estado=ChequeX&accion=Factura&evento=PRINT_PRECHEQUES&ids='+ids);
            }else if( f == 'anulate' ){
                if( confirm('Desea anular los cheques seleccionados?') ){
                    location.replace('<%=CONTROLLER%>?estado=ChequeX&accion=Factura&evento=ANULATE_PRECHEQUES&ids='+ids);
                }
            }
        }else{
            alert('Debe seleccionar los cheques');
        }
    }
</script>
