<!--
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      27/12/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Imprime un Cheque 
 --%>

<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>

<html>
<head>
     <title>Impresion de Cheque</title>
     
     <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
     <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
     <style>.comentario{color:000000;font-family:  Verdana,Arial, Helvetica, sans-serif;font-size: 11px; }</style>         

     <style>.cifras{color:000000;font-family:  Verdana,Arial, Helvetica, sans-serif;font-size: 10px; }</style>            
    
     <style>H1.SaltoDePagina{ PAGE-BREAK-AFTER: always } </style>
    
</head>

<% String reimpresion = request.getParameter("reimpresion"); %>

<% if( reimpresion == null ) {%>
    <body onload=" parent.opener.location.href = '<%=CONTROLLER%>?estado=Cheque&accion=XFactura&evento=ACTUALIZAR'; window.print();  window.close();">
<% }else{ %>
    <body onload=" parent.opener.location.href = '<%=CONTROLLER%>?estado=Cheque&accion=Reimpresion&evento=NEXT'; window.print(); window.close();">    
<% } %>



      <% Usuario Usuario       = (Usuario) session.getAttribute("Usuario");
         String  usuario       = Usuario.getLogin(); 
         Cheque  cheque        = model.ChequeXFacturaSvc.getCheque();
         List    facturasList  = cheque.getFacturas();         
         int     tamanoLinea   = 13;
         int     tamanoColumn  = 1;
         int     left          = 15;
         String  laser         = request.getParameter("laser")!=null? request.getParameter("laser") : "N";
         int     copias        = laser.equals("S")? 2 : 1;
         List    esquemasList  = laser.equals("S")? cheque.getEsquemaLaser() : cheque.getEsquema();
         String  abonada       = "";
         double  margen        = 0.423;%>


<% for(int kk = 1; kk<= copias; kk++ ){ %>      
     
    <DIV  STYLE="position:relative">

     <!-- ESQUEMA DE IMPRESION-->
       <%for(int j=0;j<esquemasList.size();j++){
            EsquemaCheque  esquema = (EsquemaCheque) esquemasList.get(j);
            String  valor   =  cheque.formatEsquemaCMS(esquema);
            double     linea   =  esquema.getTop() - margen;
            double     columna =  esquema.getLeft(); %>
            <div  class=comentario style="position:absolute; overflow:hidden; width:700;   left:<%=columna%>cm;  top:<%=linea%>cm; width:'600' "><%=valor%></div>
        <%}%>
                                     
     <!-- TITULOS -->
     
          <DIV class=comentario STYLE="position:absolute; left:301px; top:307px; "><b><%=cheque.getNumero()%></b> </DIV>            
          <DIV class=comentario STYLE="position:absolute; left:228px; top:321px; ">   <%=cheque.getAno()%>-<%=cheque.getMes()%> -<%=cheque.getDia()%></DIV>     
          <DIV class=comentario STYLE="position:absolute; left:353px; top:321px; ">   <%=cheque.getBanco() %> &nbsp <%=cheque.getSucursal()%></DIV>
          <DIV class=comentario STYLE="position:absolute; left:42px;  top:336px; ">   <%=cheque.getNom_beneficiario()%>     </DIV> 
          <DIV class=comentario STYLE="position:absolute; left:353px; top:336px; "><b> NIT            </b>        </DIV>  
          <DIV class=comentario STYLE="position:absolute; left:384px; top:346px; "> <%=cheque.getNit_beneficiario()%> </DIV>  
          
       
          <DIV class=comentario STYLE="position:absolute; left:<%=left   %>px;  top:360px; "><b> FACTURA        </b></DIV> 
          <DIV class=comentario STYLE="position:absolute; left:<%=left + 95 %>px;  top:360px; "><b> VALOR FACT     </b></DIV>                      
          
          <DIV class=comentario STYLE="position:absolute; left:<%=left + 223 %>px;  top:360px; "><b> RIVA           </b></DIV> 
          <DIV class=comentario STYLE="position:absolute; left:<%=left + 302 %>px;  top:360px; "><b> RICA           </b></DIV> 
          <DIV class=comentario STYLE="position:absolute; left:<%=left + 376 %>px;  top:360px; "><b> RFTE           </b></DIV> 
          
          <DIV class=comentario STYLE="position:absolute; left:<%=left + 442 %>px;  top:360px; "><b> VRL &nbspNETO   </b></DIV> 
          <DIV class=comentario STYLE="position:absolute; left:<%= reimpresion == null? (left + 560) : ( left + 540 ) %>px;  top:360px; "><b> <%= reimpresion == null? "SALDO" : "VLR PAGADO"  %>           </b></DIV> 
          
         
        <!-- DETALLES DEL CHEQUE -->  
                        
         <% int  top     = 379;
            int  max     = 0;
            int  contRow = 0;
            for(int j=0;j<facturasList.size();j++){
              contRow++;
              FacturasCheques   factura = (FacturasCheques)   facturasList.get(j); %> 
              
                <!-- Facturas-->
                <% if( factura.getTipo_pago()!=null && factura.getTipo_pago().equals("A")) abonada = factura.getDocumento();%>
                 <DIV class=comentario STYLE="position:absolute; left:<%=left   %>px;   top:<%=top + max%>px;"> <%=factura.getDocumento()%>   </DIV>
                 
                 <DIV class=cifras STYLE="position:absolute; left:<%=left + 85 %>px;   top:<%=top + max%>px;  width:90">
                      <table width='100%'  cellpadding='0' cellspacing='0'  >
                          <tr><td align='right' class=cifras>
                                <%=Util.customFormat( factura.getVlrFactura() + factura.getValor_iva() )%>  
                          </td></tr>
                      </table>
                 </DIV> 
                 
                 
                 
                 <DIV class=cifras STYLE="position:absolute; left:<%=left + 165 %>px; top:<%=top + max%>px;   width:90"> 
                       <table width='100%'  cellpadding='0' cellspacing='0'  >
                          <tr><td align='right' class=cifras>
                               <%=Util.customFormat( factura.getValor_riva() )  %>  
                         </td></tr>
                      </table>
                 </DIV>
                 <DIV class=cifras STYLE="position:absolute; left:<%=left + 242 %>px; top:<%=top + max%>px;   width:90"> 
                       <table width='100%'  cellpadding='0' cellspacing='0'  >
                          <tr><td align='right' class=cifras>
                               <%=Util.customFormat(  factura.getValor_rica() )  %>  
                         </td></tr>
                      </table>
                 </DIV>
                 <DIV class=cifras STYLE="position:absolute; left:<%=left + 318 %>px; top:<%=top + max%>px;   width:90"> 
                       <table width='100%'  cellpadding='0' cellspacing='0'  >
                          <tr><td align='right' class=cifras>
                               <%=Util.customFormat( factura.getValor_rtfe() )  %>  
                         </td></tr>
                      </table>
                 </DIV>
                 
                 <!--  -->
                 
                 
                 <DIV class=cifras STYLE="position:absolute; left:<%=left + 416 %>px; top:<%=top + max%>px;   width:90"> 
                      <table width='100%'   cellpadding='0' cellspacing='0'  >
                          <tr><td align='right' class=cifras>
                              <%=Util.customFormat(factura.getVlrNetoPago())%> 
                         </td></tr>
                     </table>
                 </DIV>
                 <DIV class=cifras STYLE="position:absolute; left:<%=left + 514 %>px; top:<%=top + max%>px;   width:90"> 
                      <table width='100%'   cellpadding='0' cellspacing='0'  >
                          <tr><td align='right' class=cifras>
                                <%=Util.customFormat(factura.getVlrPagar())%>
                         </td></tr>
                     </table>
                 </DIV>
                 
                 
                 <!-- Documentos Relacionados-->
                 
                 <% List docRel = factura.getDocumnetosRelacionados();
                    for(int i=0;i<docRel.size();i++){
                        if(contRow == cheque.getTopeDetalle() )
                           break;
                        contRow++;
                        max+= tamanoLinea;
                        FacturasCheques   factRel = (FacturasCheques)   docRel.get(i);%>   
                     
                        <%String tipo = "";%>
                        <%int signo = 1;%>
                        <% if( factRel.getTipo_documento().equals("010") ) tipo = "CH"; %>
                        <% if( factRel.getTipo_documento().equals("035") ){ tipo = "NC"; signo = -1;}%>
                        <% if( factRel.getTipo_documento().equals("036") ){ tipo = "ND"; }%>
                        
                        <% if( tipo.equals("") ) tipo = "CH"; %>
                        
                         <DIV class=comentario STYLE="position:absolute; left:<%=left %>px;  top:<%=top + max%>px;"> <%=tipo%>&nbsp;<%=factRel.getDocumento()%>    </DIV>                         
                         <DIV class=cifras STYLE="position:absolute; left:<%=left + 85 %>px;  top:<%=top + max%>px;  width:90">
                              <table width='100%'  cellpadding='0' cellspacing='0'  >
                                  <tr><td align='right' class=cifras>
                                        <%=Util.customFormat( (factRel.getVlrFactura() + factRel.getValor_iva())*signo )%>  
                                  </td></tr>
                              </table>
                         </DIV> 
                         <DIV class=cifras STYLE="position:absolute; left:<%=left + 165 %>px; top:<%=top + max%>px;   width:90"> 
                               <table width='100%'  cellpadding='0' cellspacing='0'  >
                                  <tr><td align='right' class=cifras>
                                       <%=Util.customFormat(  factRel.getValor_riva()*signo )  %>  
                                 </td></tr>
                              </table>
                         </DIV>
                         <DIV class=cifras STYLE="position:absolute; left:<%=left + 242 %>px; top:<%=top + max%>px;   width:90"> 
                               <table width='100%'  cellpadding='0' cellspacing='0'  >
                                  <tr><td align='right' class=cifras>
                                       <%=Util.customFormat( factRel.getValor_rica()*signo )  %>  
                                 </td></tr>
                              </table>
                         </DIV>
                         
                         <DIV class=cifras STYLE="position:absolute; left:<%=left + 318 %>px; top:<%=top + max%>px;   width:90"> 
                               <table width='100%'  cellpadding='0' cellspacing='0'  >
                                  <tr><td align='right' class=cifras>
                                       <%=Util.customFormat( factRel.getValor_rtfe()*signo )  %>  
                                 </td></tr>
                              </table>
                         </DIV>
                         
                         <DIV class=cifras STYLE="position:absolute; left:<%=left + 416 %>px; top:<%=top + max%>px;   width:90"> 
                              <table width='100%'   cellpadding='0' cellspacing='0' >
                                  <tr><td align='right' class=cifras>
                                      <%=Util.customFormat(factRel.getVlrNetoPago()*signo)%> 
                                 </td></tr>
                             </table>
                         </DIV>
                        
                  <%}
                if(contRow==cheque.getTopeDetalle())
                    break;
                max+= tamanoLinea;
           }%>
           
           
           <%if( (contRow == cheque.getTopeDetalle()) &&  cheque.getContDetalle()> cheque.getTopeDetalle()  ){%>
                  <DIV class=comentario STYLE="position:absolute; left:<%=left  %>px; top:'510px'; width:140;">Ver Anexo...</div>
           <%}%>
           
           
           
           <%if(  cheque.getMonto() > cheque.getVlrPagar()  ){%>
                  <DIV class=comentario STYLE="position:absolute; left:<%=left  %>px; top:'530px'; ">ABONO <%=abonada%> Nuevo saldo =  <%=   Util.customFormat( cheque.getMonto() - cheque.getVlrPagar() ) %> </div>
           <%}%>
           
           
           
        <!-- MONTO CHEQUE Y USUARIO --> 
          <DIV class=comentario STYLE="position:absolute; left:<%=left + 440 %>px; top:'510px'; width:140;">
                   <table width='100%'   cellpadding='0' cellspacing='0'>
                     <tr><td align='right' class=comentario>
                            _________________ 
                      </td></tr>
                </table>
          </DIV>  
          <DIV class=comentario STYLE="position:absolute; left:<%=left + 440 %>px; top:'530px';  width:140">
                <table width='100%'   cellpadding='0' cellspacing='0' >
                     <tr><td align='right' class=comentario>
                            $&nbsp <%= Util.customFormat( cheque.getVlrPagar() ) %>  
                     </td></tr>
                </table>
          </DIV>
          <DIV class=comentario STYLE="position:absolute; left:<%=left + 20 %>px;  top:'580px'; "> <%=usuario%> </DIV>  

            </DIV>
<!-- Para el salto de pagina -->
     <% if(kk<copias){%>
          <H1 class=SaltoDePagina> </H1>
     <%}
   
  }%>          
               
</body>
</html>
