<!--
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      07/10/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Generacion Cheque X Factura
 --%>

<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>
<%
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>

<html>
<head>

    <title>Cheque X Facturas</title>    
    <link href="<%= BASEURL %>/css/estilostsp.css"     rel="stylesheet" type="text/css"> 
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    
    <script>
        function sendForm(theForm, opcion, img){  
             anularBtn (img);
             theForm.action = theForm.action + opcion;
             if (opcion=='BUSCAR'){
                  msj.innerHTML = 'Buscando facturas disponibles...';				  
                  theForm.submit();
                  
             }
             if (opcion=='CHEQUE'){
                 selectAll( theForm.facturasCheques );
                 if ( theForm.facturasCheques.selectedIndex!=-1) {
                      msj.innerHTML = 'Generando cheque, por favor espere...';
                      theForm.submit();
                 }
                 else{
                      alert('Deber� seleccionar las facturas a pagar');
                      activarBtn( img );
                 }
             }
        }
        
        function clearFacturas(theForm){     
                deleteAll( theForm.facturasCheques   );
                deleteAll( theForm.facturasProveedor );
        }
        
        function buscarProveedor(){
            window.open( "<%=BASEURL%>/consultas/consultaProveedor.jsp",'','top=20,left=200,width=700,scrollbars=yes,status=yes');
        }
       
        function buscarNombre( txt ){  
			clearFacturas(document.formulario);
			location.replace("<%=CONTROLLER%>?estado=ChequeX&accion=Factura&evento=REFRESHNOMBRE&provee="+txt.value);								
			
        }
        function validarProveedor(){
			if(document.formulario.provee.value == ""){
				alert("Debe digitar un proveedor!");
				return (false);
			}
			else{
				return (true);
			}			
		}
		
    </script>
     
</head>
<body>


<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Impresi�n de Cheques"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>


 
  <% Usuario  usuario               = (Usuario) session.getAttribute("Usuario");
     String   distrito              =  usuario.getDstrct();    
     List     listProveedores       =  model.ChequeXFacturaSvc.getProveedores();
     List     listDocumentos        =  model.ChequeXFacturaSvc.getDocumentos(); 
     List     listFacturasProveedor =  model.ChequeXFacturaSvc.getListFacturasPro();
     String   codeProv              =  model.ChequeXFacturaSvc.getProveedor();  
     String   tipoFact              =  model.ChequeXFacturaSvc.getTipoFactura();
     List     listFacturasCheque    =  model.ChequeXFacturaSvc.getFacturasCheque();
     String   msj                   =  request.getParameter("msj"); 
     String   url                   =  CONTROLLER +"?estado=Cheque&accion=XFactura&evento="; 
     String   nombre                =  request.getAttribute("nombre_proveedor")!=null? (String)request.getAttribute("nombre_proveedor") : "" ; 
     String   nit                   =  request.getAttribute("nit")!=null? (String)request.getAttribute("nit") : "" ; 
	 String   rpProveedor           =  request.getAttribute("rpProveedor")!=null? (String)request.getAttribute("rpProveedor") : "";
	 %>
	  
     
    
    
  <form action="<%=url%>" method="post" name="formulario" id="form1">
  
     <table width="650"  border="2" align="center">
         <tr>
            <td colspan='2'>             

                        <table width="100%"   align="center">
                                  <tr>
                                     <td colspan='2'>
                                          <table width='100%'  class="barratitulo">
                                                <tr class="fila">
                                                        <td align="left" width='50%' class="subtitulo1" nowrap> CHEQUE POR FACTURA</td>
                                                        <td align="left" width='50%' ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                                                </tr>
                                          </table>
                                     </td>
                                  </tr>


                                  <tr class="fila">
                                           <td width='20%' >&nbsp Distrito</td>
                                           <td width='*'>
                                               <select name='distrito' class="textbox"   style='width:40%'>                                                       
                                                      <option value='<%=distrito%>'> <%=distrito%> 
                                               </select> 
											   
                                           </td>
                                   </tr>


                                   <tr class="fila">
                                           <td >&nbsp Nit Proveedor</td>
                                           <td >
                                              <input name='provee' id='provee' type="textbox" style='width:120' onblur='if(provee.value != "" ){ buscarNombre(this); }' value="<%=model.ChequeXFacturaSvc.getProveedor() %>"> 
                                              <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif' width='19' style=" cursor:hand"  title='Buscar Proveedor....'  onclick="buscarProveedor()"  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>              
                                               <input type="text" size="55"  class="fila" name="n" style="border:0" id="n" value="<%=nombre%>" readonly>
                                                <input name='nombre' id='nombre' type="hidden" style='width:250' value="<%=nombre%>" readonly>
												<input name='rpProveedor' id='rpProveedor' type="hidden" style='width:250' value="<%=rpProveedor%>" readonly>                                              
                                           </td>
                                   </tr>
                                   
                                   
                                   <tr class="fila">
                                           <td >&nbsp Tipo Factura</td>
                                           <td >
                                               <select name='tipoFactura' class="textbox"   style='width:40%'>                                                       
                                                  <% for(int i=0;i< listDocumentos.size();i++){ 
                                                         Hashtable doc = (Hashtable)listDocumentos.get(i);
                                                         String    sele = ( tipoFact.equals( doc.get("code") ))?" selected='selected' ":"";%>
                                                         <option value='<%= doc.get("code") %>'      <%= sele %> > <%= doc.get("descripcion")%>
                                                   <%}%> 
                                               </select>
                                           </td>
                                   </tr>
                                   
                                   
                                   <tr class="fila">
                                           <td colspan='2' >
                                            &nbsp Facturas
                                                
                                                <table cellpadding='0' cellspacing='0' width='100%'>
                                                         <tr class="fila">
                                                             <td width='47%' align='center'>
                                                              
                                                                   <table  cellpadding='0' cellspacing='0' width='60%' align='center'>
                                                                      <tr  class="fila">
                                                                         <td  style="font size:12">Facturas del proveedor  </td>
                                                                         <td><img src='<%=BASEURL%>/images/botones/iconos/lupa.gif' width='20'   style=" cursor:hand"    name='i_load'      onclick="if(validarProveedor()){sendForm(formulario,'BUSCAR', this);}"  title='Cargar las facturas del proveedor seleccionado'   ></td>
                                                                      </tr>
                                                                   </table>
                                                                   
                                                                    <select multiple size='12' class='select' style='width:90%' name='facturasProveedor' onchange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text; }">                                                     
                                                                     <% for(int i=0; i<listFacturasProveedor.size();i++){
                                                                            FacturasCheques factura = (FacturasCheques) listFacturasProveedor.get(i);%>
                                                                            <option value='<%= factura.getDocumento()%>'> <%= factura.getDocumento()%>
                                                                     <% }%>
                                                                    </select>
                                                             </td>
                                                             <td width='6%' align='center'>
                                                                     <img src='<%=BASEURL%>/images/botones/envDer.gif' style='cursor:hand' name='imgEnvDer'  onclick="move   (facturasProveedor,  facturasCheques); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>             
                                                                     <img src='<%=BASEURL%>/images/botones/envIzq.gif' style='cursor:hand' name='imgEnvIzq'  onclick="move   (facturasCheques, facturasProveedor ); " onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>                                                             
                                                             </td>
                                                             <td width='47%' align='center' style="font size:12">
                                                                    Facturas para el Cheque
                                                                    <select multiple size='12' class='select' style='width:90%' name='facturasCheques'     onchange=" if (this.selectedIndex!=-1) { window.status=this[this.selectedIndex].text; }">
                                                                     <% for(int i=0; i<listFacturasCheque.size();i++){
                                                                            FacturasCheques factura = (FacturasCheques) listFacturasCheque.get(i);%>
                                                                            <option value='<%= factura.getDocumento()%>'> <%= factura.getDocumento()%>
                                                                     <% }%>
                                                                    </select>
                                                             </td> 
                                                         </tr>
                                                  </table>
                                                  <BR>
                                           
                                           </td>
                                   </tr>
                                   

                        </table>

                </td>
             </tr>
       </table>  
	   <script>
	   		var nombre= '<%=nombre%>'; 
            if(document.formulario.rpProveedor.value=='S'){				
				alert('El Proveedor tiene retencion en el pago!');
				document.formulario.provee.value = "";
				document.formulario.n.value = "";
				clearFacturas(document.formulario);
          }
			
			
	   </script>            
       
  <form>
                
  <p>       
       <img src='<%=BASEURL%>/images/botones/aceptar.gif'    style=" cursor:hand"  title='Crear....'       name='i_crear'       onclick="sendForm(formulario,'CHEQUE', this)"                                          onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>        
       <img src='<%=BASEURL%>/images/botones/cancelar.gif'   style=" cursor:hand"  title='Limpiar Datos'   name='i_cancel'      onclick="location.href='<%=CONTROLLER%>?estado=Cheque&accion=XFactura&evento=CANCEL'"  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>      
       <img src='<%=BASEURL%>/images/botones/salir.gif'      style=' cursor:hand'  title='Salir...'        name='i_salir'       onclick='parent.close();'                                                              onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
  </p> 
       

  
  <font id='msj' class='informacion'></font>
  
  
  <!-- Mensaje -->
   <% if(msj!=null && !msj.equals("")){%>        
         <table border="2" align="center">
          <tr>
            <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
              <tr>
                <td width="550" align="center" class="mensajes"><%=msj%></td>
                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                <td width="58">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
        </table>
    <%  } %>


</div>
<%=datos[1]%>
</body>
</html>
