<!--
- Autor : Ing. Henry Osorio
- Modificado Ing Sandra Escalante
- Date  : 02 Febrero 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Vista que permite seleccionar las facturas correspondientes a una corrida a autorizar
--%>

<%-- Declaracion de librerias--%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
  String msg = (request.getParameter("msg")!=null)? request.getParameter("msg"):"";
  Vector listado = model.corridaService.getVectorCorridas();
%>
<html>
<head>
<title>.: Facturas del Proveedor</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type='text/javascript' src="<%=BASEURL%>/js/reporte.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/script.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body onResize="redimensionar();" <%= ( request.getParameter("reload")!=null ) ?"onLoad='redimensionar();window.opener.location.reload();'" : "onLoad='redimensionar();'"%>>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Facturas"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<br>
<%if(listado.size()!=0){%>
<form name="form1" method="post" id="form1" action="<%= CONTROLLER %>?estado=Corrida&accion=Autorizacion&cmd=autorizar" onSubmit="return validar(this);">
<br><table width="400"  border="2" align="center">
  <tr>
    <td><table width="100%"  border="0" class="tablaInferior">
  <tr class="letra">
    <td width="23%" class="letraresaltada">Corrida</td>
    <td width="77%"><input name="corrida" type="hidden" value="<%= request.getParameter("corrida") %>"><%= request.getParameter("corrida") %></td>
  </tr>
  <tr class="letra">
    <td class="letraresaltada">Proveedor</td>
    <td nowrap><input name="proveedor" type="hidden" value="<%= request.getParameter("proveedor") %>"><input name="nproveedor" type="hidden" value="<%= request.getParameter("nproveedor") %>">[ <%= request.getParameter("proveedor") %> ] <%= request.getParameter("nproveedor") %></td>
  </tr>
  <tr class="letra">
    <td class="letraresaltada">Banco</td>
    <td><input name="banco" type="hidden" value="<%= request.getParameter("banco") %>"><%= request.getParameter("banco") %></td>
  </tr>
  <tr class="letra">
    <td class="letraresaltada">Sucursal</td>
    <td><input name="sucursal" type="hidden" value="<%= request.getParameter("sucursal") %>"><%= request.getParameter("sucursal") %></td>
  </tr>
  <tr class="letra">
    <td class="letraresaltada">Agencia</td>
    <td><input name="agencia" type="hidden" value="<%= request.getParameter("agencia") %>"><%= request.getParameter("agencia") %></td>
  </tr>
</table>
</td>
  </tr>  
</table>
<br>
	<table width="650" border="2" align="center">
    	<tr>
      	<td>
	  		<table width="100%" align="center">
        		<tr>
            		<td width="195"  height="22" class="subtitulo1">FACTURAS</td>
                	<td width="431" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          		</tr>
		  </table>
  		
			<table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
	 			<tr class="tblTitulo">    		
					<td> <div align="center"><input name="todasAprobadas" id="todasAprobadas" type="checkbox" value="" onClick="checkearTodos (this.form,this);"></div></td> 		
    				<td> <div align="center">Documento</div></td>    				
					<td> <div align="center">Descripcion</div></td>  
					<td> <div align="center">Neto</div></td>  
					<td> <div align="center">Fecha del Documento </div></td> 
					<td> <div align="center">Fecha de Vencimiento </div></td>    				
    			</tr>

				<%for (int i = 0; i <  model.corridaService.getVectorCorridas().size(); i++){			
    				CXP_Doc factura = (CXP_Doc) listado.elementAt(i);      
					String estilo = (i % 2 == 0 )?"filagris":"filaazul"; 
					String aprobada = "factura"+i;
				%>
	
  				<tr class="<%=estilo%>"
						onclick="jscript: if (!<%= aprobada %>.disabled) {<%= aprobada %>.checked = !<%= aprobada %>.checked;}">        				
					<td class="bordereporte"><input name="<%= aprobada %>" type="checkbox" onclick="jscript: <%= aprobada %>.checked = !<%= aprobada %>.checked;" value="<%= factura.getDocumento() + "/" + factura.getTipo_documento()%>"></td> 
	    			<td class="bordereporte" nowrap><%= factura.getDocumento()%> </td>        				
    				<td  class="bordereporte" nowrap><%=factura.getDescripcion()%></td>
					<td  class="bordereporte" nowrap><%=factura.getVlr_neto()%></td>
					<td  class="bordereporte" nowrap><%=factura.getFecha_documento()%></td>
					<td  class="bordereporte" nowrap><%=factura.getFecha_vencimiento()%></td>
	   			</tr>
  				<%}//fin while%>				
		  </table>
		</td>
		</tr>
	</table>
	<br>
	<table width="76%"  border="0" align="center">
  	<tr>
    	<td align="center">
			<input type="image" src="<%=BASEURL%>/images/botones/autorizar.gif">
			<img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgregresar"  height="21"onMouseOver="botonOver(this);" onClick="location.href='<%=BASEURL%>/jsp/cxpagar/corridas/corridasSearch.jsp'" onMouseOut="botonOut(this);" style="cursor:hand"> 
		    <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" height="21" onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">
		</td>
	</tr>
	</table>
	<%if(request.getParameter("msg")!=null){%>
  <br>  
  <table width="247" border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="122" align="center" class="mensajes"><div align="left">Proceso exitoso!</div></td>
        <td width="34" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="57">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table><br><br>

<table width="400"  border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td><div class="informacion" align="left"><%=request.getParameter("msg")%></div></td>
  </tr>
</table>

 <%}%>
</form>
<%}else {
	msg = "No se encuentran Facturas SIN APROBACION del Proveedor seleccionado...";
  	if(!msg.equals("")){%>
   		<table width="500" border="2" align="center">
     		<tr>
    			<td>
					<table  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      					<tr>
        					<td width="342" align="left" class="mensajes" nowrap><%=msg%></td>
        				    <td width="23" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        					<td width="101">&nbsp;</td>
      					</tr>
    				</table>
				</td>
  			</tr>
  </table>
		<br>
		<center><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgregresar"  height="21" onMouseOver="botonOver(this);" onClick="location.href='<%=BASEURL%>/jsp/cxpagar/corridas/corridasSearch.jsp'"onMouseOut="botonOut(this);" style="cursor:hand"> 
		<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" height="21" onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand"></center>
  <%}
 }%>
</div>
</body>
</html>
<script>
function checkearTodos(theForm,ele){
	if (ele.id=='todasAprobadas'){
    	for (i=0;i<theForm.length;i++){
        	if (theForm.elements[i].type=='checkbox'){
		    	theForm.elements[i].checked=theForm.todasAprobadas.checked;		
			}
		}
	}else
    	theForm.todasAprobadas.checked=true;
     	
	for (i=0;i<theForm.length;i++)
       	if (theForm.elements[i].type=='checkbox' && !theForm.elements[i].checked)
           	theForm.todasAprobadas.checked=false;
}

function validar(theForm){    
	for(i=0;i<=theForm.length-1;i++)  
   		if(theForm.elements[i].type=='checkbox' && theForm.elements[i].checked){
			alert ('Recuerde....Una vez autorizada una factura, esta se eliminara de la lista del Proveedor');  
        	return (true);
		}
	alert('Debe seleccionar por lo menos un item...') ;
	return (false);  
}
</script>
