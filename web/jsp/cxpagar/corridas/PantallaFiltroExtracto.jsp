<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="java.util.TreeMap;"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input"%>
<%
  TreeMap bancos    = model.servicioBanco.getBanco();
  TreeMap sbancos   = model.servicioBanco.getSucursal();
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>

<html>
<head>
     <title>EXTRACTOS</title>
     <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src='<%=BASEURL%>/js/boton.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
</head>
<body onResize="redimensionar();" onLoad="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Impresion de Corridas"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<form name='frmextracto' method='post' action='<%=CONTROLLER%>?estado=Extracto&accion=Search'>
     <table width="380" border='2' align='center'>
           <tr>
              <td>                 
                 <table width="100%" align="center">
                  <tr>
                     <td class="subtitulo1" align="left">Generar Reporte <input type="hidden" name="opcion"></td>
                     <td class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                 </tr>
                       <tr class='fila'>
                          <td width="40%">
                             DISTRITO</td>
                          <td>
                             <select name="distrito" class="listmenu">
                                     <option value='FINV'>FINV</option>
                                     <option value='VEN'>VEN</option>
                             </select>
                          </td>
                   </tr>
                       <tr class="fila">
                          <td>
                             BANCO</td>
                          <td><input:select name="banco"  attributesText="style='width:100%;' class='listmenu'  onChange='cargarSucursales()'"  options="<%=bancos%>"  /></td>                      
                   </tr> 
                       <tr class='fila'>
                          <td>
                             SUCURSAL
                          </td>
                          <td ><input:select name="cuenta" attributesText="style='width:100%;' class='listmenu'"  options="<%=sbancos==null ? new TreeMap() : sbancos %>" />
</td>
                   </tr>
                       <tr class='fila'>
                          <td>
                             CORRIDA</td>
                         <td>
                             <input name='corrida' type='text' class="textbox" size="10" maxlength='10' onKeyPress="soloDigitos(event,'decOK')">
                             <a class="Simulacion_Hiper" href="javascript:enviarBeneficiario(frmextracto);"> Seleccionar Proveedor</a></td>
                   </tr>
                   <tr class='fila'>
                          <td>
                             CHEQUE X CORRIDA</td>
                          <td>
                            <select name='pagadas'>
                                 <option value="S" selected >Pagadas</option>
                                 <option value="N" >No pagadas</option>
                            </select> 
                         </td>
                   </tr>
					<tr class='fila'>
                          <td>
                             TIPO CORRIDA </td>
                          <td>
                            <select name='tipo'>
                                 <option value="A"  >Administrativa</option>
                                 <option value="P"  selected >Propietario</option>
                            </select> 
                         </td>
                   </tr>
                   <tr class='fila'>
                       <td colspan='2' align="CENTER"><input type='checkbox' id="aprobadas" name='aprobadas' value='ok' > APROBADOS
                       </td>
                   </tr>
                 </table>
             </td>
           </tr>
</table>
<br>
<table width="518" border="0" align="center">
  <tr>
    <td height="27" align="center">
        <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="aceptar" id='aceptar' height="21" onClick="return valiTCamposLlenos(frmextracto);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;
        <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="cancelar"  height="21" onMouseOver="botonOver(this);" onClick="frmextracto.reset();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
        <img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
        <img src="<%=BASEURL%>/images/botones/exportarExcel.gif" name="exp"  height="21" onClick="return validarDatosCorrida(frmextracto);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" title="Haga clic aqui para iniciar el proceso de generacion del reporte"></td>
    </tr>
</table>
</form>
<br>
<%String msg = request.getParameter("msg")!=null ?request.getParameter("msg"):"";
if(!msg.equals("")){%>

<table width="505" border="2" align="center">
            <tr>
              <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                  <tr>
                    <td width="306" align="center" class="mensajes"><%=msg%></td>
                    <td width="33" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="51">&nbsp;</td>
                  </tr>
              </table></td>
            </tr>
  </table>
<%} else if(request.getAttribute ("mensaje")!=null){%>
<br>
<table width="550" border="2" align="center">
<tr>
	<td width="707">
		<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
			<tr>
				<td width="50%" nowrap  class="mensajes">
					<%=request.getAttribute ("mensaje")%>
					<br>
					&nbsp;&nbsp;<a href="#" class="Simulacion_Hiper" onClick="window.open('<%= CONTROLLER %>?estado=Log&accion=Proceso&Estado=TODO','log','scroll=no, resizable=yes, width=800, height=600')" style="cursor:hand "><%=request.getParameter("ruta")%></a>
				</td>
				<td width="16%" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
				<td width="34%">&nbsp;</td>
			</tr>
		</table>
	</td>
</tr>
</table>
<%}%>
</div>
<%=datos[1]%>
</body>
</html>

<script>
function cargarSucursales(){
   frmextracto.opcion.value = 'BuscarS';
   document.frmextracto.submit();
}
function valiTCamposLlenos(formula){
	if(formula.cuenta.value == ''){
		alert('El campo de la sucursal no puede ser vacio!');
		formula.cuenta.focus();
		return false;
	}
 		document.frmextracto.aceptar.disabled = true;
 		document.frmextracto.aceptar.src = "<%=BASEURL%>/images/botones/aceptarDisable.gif";
 		document.frmextracto.cancelar.disabled = true;
 		document.frmextracto.cancelar.src = "<%=BASEURL%>/images/botones/cancelarDisable.gif";
 		document.frmextracto.salir.disabled = true;
 		document.frmextracto.salir.src = "<%=BASEURL%>/images/botones/salirDisable.gif";
 		document.frmextracto.exp.disabled = true;
 		document.frmextracto.exp.src = "<%=BASEURL%>/images/botones/exportarExcelDisable.gif";
 		formula.submit();

}

function validarDatosCorrida ( formulario ) {
	if (formulario.cuenta.value == '') {
		alert('El campo de la sucursal no puede ser vacio!');
		formulario.cuenta.focus();
		return false;
	}
		formulario.action = "<%=CONTROLLER%>?estado=ReporteGeneracion&accion=Corridas";
		formulario.submit();
}

function enviarBeneficiario ( formulario ) {
    distrito  = formulario.distrito.value;
    banco     = formulario.banco.value;
    cuenta    = formulario.cuenta.value;
    corrida   = formulario.corrida.value;
    if (formulario.aprobadas.checked==true)
       aprobadas = "ok";
    else
       aprobadas = "";
    pagadas   = formulario.pagadas.value;

	var sw = 0;
    if (formulario.cuenta.value=='') {
		alert('Debe seleccionar una sucursal');
        sw = 1;
    }
    if (formulario.corrida.value=='') {
		alert('Debe digitar el numero de la corrida');
        sw = 1;
    }
    if (sw == 0) {
	    window.open('<%=CONTROLLER%>?estado=Extracto&accion=Search&opcion=srhProveedor&distrito='+distrito+'&banco='+banco+'&cuenta='+cuenta+'&corrida='+corrida+'&aprobadas='+aprobadas+'&pagadas='+pagadas,'BENEFICIARIO','width=750,height=650,resizable=yes,scrollbars=yes,status=yes,resizable=no,top=10,left=120');
	}
}

</script>
