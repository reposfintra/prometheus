<!--
- Autor : Ing Ivan Dario Gomez
- Date  : 02 Febrero 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Vista para mostrar la informacion de las ayudas
--%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE></TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</HEAD>
<BODY>
<% String BASEIMG = BASEURL +"/images/ayuda/cxpagar/corridas/"; %> 
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="1" >      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="20"><div align="center">MANUAL PARA LA AUTORIZACION DE CORRIDAS </div></td>
        </tr>
        <tr class="subtitulo1">
          <td>Descripci&oacute;n del funcionamiento del programa para la Autirizacion de corridas </td>
        </tr>
        <tr>
          <td  height="18"  class="ayudaHtmlTexto"><p>En la primera pantalla se listan todas las corridas que no se han pagado. Las corridas autorizadas estan marcadas con un checkbox, si no esta marcada puede ser que no se haya autorizado alguna factura y el programa automaticamente toma como no autorizada la corrida.</p>
          <p>El boton aceptar que aparece en la figura 1, es para modificar el estado de las corridas, si selecciona todas las corridas y luego da click sobre el boton aceptar, este autoriza todas las facturas de las corridas selecionadas.</p>
          <p>El programa permite autorizar no solo por corrida, tambien por banco - sucursal , proveedor y por factura.</p></td>
        </tr>
        <tr>
          <td height="18"  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td height="18"  class="ayudaHtmlTexto"><img src="<%= BASEIMG%>figura1.jpg"></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><div align="center"></div></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><p>Si da click sobre uno de los registros, le aparecera una pantalla como se muestra en la figura 2, donde se listan los bancos que contienen facturas de la corrida seleccionada, al igual que el patanlla de la figura 1, apareceran seleccionados los bancos autorizados, y al igual que en las corridas se puede  autorizar y desautorizar los bancos..</p>
          </td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEIMG%>figura2.jpg" width="911" height="287"> </div></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">Para ver los proveedores de uno de los bancos listados como se muetra en la figura 2, debe dar click sobre uno de los bancos y le aparecera un lista con los proveedores que contiene el banco seleccionado, apareceran seleccionado los proveedores que tengan todas las facturas autorizadas, como se muestra en la figura 3. </td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><div align="left">El campo valor que aparece en el listado es la suma de todas las facturas que contiene el proveedor.</div></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><img src="<%= BASEIMG%>figura3.jpg" width="912" height="337"></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><div align="center"></div></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><table cellspacing="0" cellpadding="0">
              <tr>
                <td class="ayudaHtmlTexto"><p>Para ver las facturas de un provedor, se realiza el mismo procedimiento de todas las anteriores , dar click sobre el proveedor que desea y le aparecera la lista de facturas que contiene el proveedor, con la cual podra hacer lo mismo que las anteriores.</p>
                <p>las facturas autorizadas aparte de estar seleccionadas tambien le aparecera en el campo &quot;fecha de autorizacion&quot; la fecha, sino tiene la fecha es porque no esta autorizada. </p></td>
              </tr>
              <tr>
                <td class="ayudaHtmlTexto">&nbsp;</td>
              </tr>
              <tr>
                <td width="943" class="ayudaHtmlTexto"><img src="<%= BASEIMG%>figura4.jpg" width="709" height="573"></td>
              </tr>
          </table>
            <div align="center"></div></td>
        </tr>
      </table></td>
    </tr>
  </table>
</BODY>
</HTML>
