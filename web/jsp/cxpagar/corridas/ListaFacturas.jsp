<!--  
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      27/09/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description:  Vista que permite agregar facturas a la corrida
 --%>


<%@page    session   ="true"%> 
<%@page    errorPage ="/error/ErrorPage.jsp"%>
<%@page    import    ="com.tsp.operation.model.beans.*"%>
<%@include file      ="/WEB-INF/InitModel.jsp"%>
<%@page    import    ="com.tsp.util.*"%>
<%@page    import    ="java.util.*" %>


<html>
<head>
      <title>Adicionar Facturas</title>
      <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
      <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>      
      <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
      
      
<% List facturas  = model.ExtractosSvc.getFacturasAdicionales(); 
   String   corrida     = request.getParameter("corrida");
   String   banco       = request.getParameter("banco");
   String   sucursal    = request.getParameter("sucursal");
   String   propietario = request.getParameter("proveedor");
   String   nombre      = request.getParameter("name");
   String   msj         = request.getParameter("msj");
   String   URL         = CONTROLLER +"?estado=AdicionarFacturas&accion=Corridas&evento=ADD&corrida=" + corrida +"&banco="+ banco +"&sucursal=" + sucursal +"&proveedor="+ propietario +"&name="+ nombre;
%>
   
      <script>           
           function send(theForm){                   
               var url  = '<%=URL%>';   
               var cont = 0; 
               for(var i=0;i<theForm.length;i++){
                   var ele = theForm.elements[i];
                   if(  ele.type=='checkbox'  &&  ele.checked  &&  ele.name!='All' ){
                        url += '&' + ele.name + '=' + ele.value;
                        cont++;    
                   }
               }
               if(cont==0)   alert('Deber� seleccionar la factura para adicionar a la corrida...');
               else {  
                       window.close();
                       parent.opener.location.href = url;
                    }
           }      
      </script>
   
</head>
<body>
<center>

      
      
 <% if( facturas.size()>0 ){%>
     <form action=''   method='post' name='formulario'>
             <table  width="400" border="2" align="center">
                 <tr>
                      <td>  
                           <table width='100%' align='center' class='tablaInferior'>

                                  <tr class="barratitulo">
                                        <td colspan='2' >
                                               <table cellpadding='0' cellspacing='0' width='100%'>
                                                     <tr>
                                                          <td align="left" width='55%' class="subtitulo1">&nbsp;Adicionar Facturas</td>
                                                          <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32"  height="20" align="left"></td>
                                                    </tr>
                                               </table>
                                        </td>
                                 </tr>

                                  <tr class="barratitulo">
                                        <td colspan='2' >
                                               <table cellpadding='0' cellspacing='0' width='100%'>
                                                     <tr   class='letra'>
                                                          <td>CORRIDA        </td>
                                                          <td><%= corrida %> </td>
                                                     </tr>
                                                     <tr   class='letra'>
                                                          <td>BANCO       </td>
                                                          <td><%=  banco       +" - "+  sucursal%> </td>
                                                     </tr>
                                                     <tr   class='letra'>
                                                          <td>PROPIETARIO </td>
                                                          <td><%=  propietario +" - "+  nombre   %></td>
                                                     </tr>
                                                     
                                                     
                                               </table>
                                        </td>
                                 </tr>
                                 
                                 

                               <tr class="fila">
                                   <td width='100%' colspan='2'>
                                      <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                                      
                                            <tr class="tblTitulo" >
                                                  <TH   title='N�mero de documento'  nowrap style="font size:11; font weight: bold" ><input  type='checkbox' id='All'  onclick='selectChecked(this.form,this)'>      </TH>
                                                  <TH   title='N�mero de documento'  nowrap style="font size:11; font weight: bold" >FACTURA          </TH>
                                                  <TH   title='valor'                nowrap style="font size:11; font weight: bold" >VALOR            </TH>
                                                  <TH   title='Tipo Pago'            nowrap style="font size:11; font weight: bold" >TIPO PAGO        </TH>       
                                             </tr>
                                                  
                                      
                                             <% for(int i=0; i<facturas.size(); i++){
                                                    Corrida  factura          = (Corrida) facturas.get(i) ;
                                                    String   urlWindowDetalle = CONTROLLER +"?estado=Buscar&accion=Factura&prov="+ propietario + "&tipo_doc=010&documento="+ factura.getDocumento().replaceAll("#","-_-"); %>

                                                  <tr class='<%= (i%2==0?"filagris":"filaazul") %>' style="cursor:hand; font size:12" onMouseOver='cambiarColorMouse(this)'  >
                                                  
                                                       <td class="bordereporte" align='center' nowrap style="font size:11">  
                                                              <input type='checkbox' value='<%=factura.getId()%>' name='factura'  onclick='selectChecked(this.form,this)'>  
                                                       </td>
                                                       <td class="bordereporte" align='center' nowrap style="font size:11"> 
                                                             <a  href="javascript: var win =window.open('<%=urlWindowDetalle%>','Detalle',' scrollbars=yes, top=100,left=200, width=500, height=400,  status=yes, resizable=yes  ');">              
                                                              <%=  factura.getDocumento() %> 
                                                            </a>
                                                       </td>
                                                       <td class="bordereporte" align='right'  nowrap style="font size:11"> <%=  Util.customFormat(factura.getValor() ) %> </td>
                                                       <td class="bordereporte" align='center' nowrap style="font size:9" > <%=  factura.getTipoPago()  %> </td>
                                        
                                                  </tr>    
                                                 
                                            <%}%>
                                                   
                                                   
                                   </table>
                                </td>
                             </tr>
                                   

                         </table>
                      </td>
                   </tr>       
             </table>
         </form>     

         <p>
               <img src="<%=BASEURL%>/images/botones/aceptar.gif"    name="acept"   height="21"  title='Adicionar'    onClick="send(formulario);"   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
               <img src="<%=BASEURL%>/images/botones/salir.gif"      name="exit"    height="21"  title='Salir'        onClick="parent.close();"     onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
         </p>


 <%}else{%>
      <BR><BR>
      <table border="2" align="center">
          <tr>
            <td>
              <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                  <tr>
                    <td width="300" align="center" class="mensajes"><%=msj%></td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="58">&nbsp;</td>
                  </tr>
            </table></td>
          </tr>
      </table>
      
      <p>
         <img src="<%=BASEURL%>/images/botones/salir.gif"      name="exit"    height="21"  title='Salir'        onClick="parent.close();"        onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
      </p>
 
 <%}%>
 
 
         
</body>
</html>
