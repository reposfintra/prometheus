<!--
- Autor : LREALES
- Date  : 19 de septiembre de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de mostrar el reporte de proveedores de fintra
--%>
<%--
The taglib directive below imports the JSTL library. If you uncomment it,
you must also add the JSTL library to the project. The Add Library... action
on Libraries node in Projects view can be used to add the JSTL 1.1 library.
--%>
<%-- Declaracion de librerias--%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.util.*"%>
<% Util u = new Util(); %>
<html>
    <head>        
        <title>Reporte Proveedores Fintra</title>
		<script src='<%=BASEURL%>/js/date-picker.js'></script>
		<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
		<script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script> 
		<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
		<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
		 <script>
		 	function Exportar (){
				
			    alert( 
				
					'Puede observar el seguimiento del proceso en la siguiente ruta del menu:' + '\n' + '\n' +
					'SLT -> PROCESOS -> Seguimiento de Procesos' + '\n' + '\n' + '\n' +
					'Y puede observar el archivo excel generado, en la siguiente parte:' + '\n' + '\n' +
					'En la pantalla de bienvenida de la Pagina WEB,' + '\n' +
					'vaya a la parte superior-derecha,' + '\n' +
					'y presione un click en el icono que le aparece.'
					
				);
				
				
				
			}
		</script>
    </head>
   <body onResize="redimensionar()" onLoad="redimensionar()">
	<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
	 <jsp:include page="/toptsp.jsp?encabezado=Reporte Anticipos Pagados"/>
	</div>
	<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
	<%String msg = request.getParameter("msg");
		if ( msg!=null && !msg.equals("") ){%>							
		  <table border="2" align="center">
			<tr>
				<td>
					<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						<tr>
							<td width="229" align="center" class="mensajes"><%=msg%></td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		  </table>		  
		        <br>
		        <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/finanzas/reportes/reporteAntiposPagados/ReporteAnticiposPagados.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
 	  <input name="salir" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
	  <%}
		else {
          Vector datos = model.reporteAnticiposParaguachonService.getVectorReporte ();  
					
		if( datos != null && datos.size() > 0 ){
			BeanGeneral prom = (BeanGeneral) datos.lastElement();
		%>		
		    <form name="form1" method="post" action="<%=CONTROLLER%>?estado=InformeAnticiposParaguachon&accion=Generar&opcion=1">
      <input name="fechai" type="hidden" id="fechai" value="<%=prom.getValor_18()%>">
	  <input name="fechaf" type="hidden" id="fechaf" value="<%=prom.getValor_19()%>">
	  <input name="agencia" type="hidden" id="agencia" value="<%=prom.getValor_15()%>">
      <input name="Guardar2" src="<%=BASEURL%>/images/botones/exportarExcel.gif" type="image" class="boton" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onclick="Exportar();">
      <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/finanzas/reportes/reporteAntiposPagados/ReporteAnticiposPagados.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
 	  <input name="salir" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
    </form>
		<table width="100%" height="37"  border="2">  
         <tr>
        	<td class="subtitulo1"><p><strong>Reporte de Anticipos Pagado de  </strong><%=prom.getValor_16()%>  De la fecha <%=prom.getValor_18()%> hasta la fecha <%=prom.getValor_19()%></p>
        	</td>
          </tr> 
              <table width="100%"  border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                <tr class="tblTitulo" align="center">
				 <td width="5%" height="24" align="center" nowrap>ITEM</td>
				  <td width="5%" height="24" align="center" nowrap>DISTRITO</td>
				  <td nowrap width="5%" align="center">BANCO</td>
                  <td nowrap width="5%" align="center">SUCURSAL</td>
				  <td nowrap width="5%" align="center"># DEL CHEQUE</td>
				   <td nowrap width="5%" align="center">VALOR CHEQUE</td>
				  <td nowrap width="5%" align="center">VALOR EN PESOS</td>
                  <td nowrap width="5%" align="center">TIPO DE MONEDA </td>
				  <td nowrap width="5%" align="center">PROVEEDOR</td>
				  <td nowrap width="5%" align="center">PROPIETARIO</td>
				  <td nowrap width="5%" align="center">BENEFICIARIO</td>
				  <td nowrap width="5%" align="center">FECHA DE CHEQUE </td>
                  <td nowrap width="10%" align="center">FECHA DE IMPRESION DEL CHEQUE</td>
				  <td nowrap width="5%" align="center">USUARIO DE IMPRESION</td>
				  <td nowrap width="5%" align="center">ESTADO</td>
                  
				
				  
		<%
			for( int i = 0; i<datos.size(); i++ ){
				BeanGeneral info = (BeanGeneral) datos.elementAt(i);
				
				
				
	  %>  	  
                <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_25()!=null)?info.getValor_25():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_01()!=null)?info.getValor_01():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_02()!=null)?info.getValor_02():""%>&nbsp;</td>
                  <td nowrap align="center" class="bordereporte"><%=(info.getValor_03()!=null)?info.getValor_03():""%>&nbsp;</td>
                  <td nowrap align="center" class="bordereporte"><%=(info.getValor_04()!=null)?info.getValor_04():""%>&nbsp;</td>
                  <td nowrap align="center" class="bordereporte"><%=(info.getValor_06()!=null)?info.getValor_06():""%>&nbsp;</td>
   			      <td nowrap align="right" class="bordereporte"><%=(info.getValor_21().equals("DOL"))?UtilFinanzas.customFormat2( (info.getValor_05().equals(""))?"0":info.getValor_05() ):UtilFinanzas.customFormat((info.getValor_05().equals(""))?"0":info.getValor_05())%>&nbsp;</td>
				  <td nowrap align="center" class="bordereporte"><%=(info.getValor_21()!=null)?info.getValor_21():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_07()!=null)?info.getValor_07():""%> - <%=(info.getValor_08()!=null)?info.getValor_08():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_09()!=null)?info.getValor_09():""%> - <%=(info.getValor_10()!=null)?info.getValor_10():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_11()!=null)?info.getValor_11():""%> - <%=(info.getValor_12()!=null)?info.getValor_12():""%>&nbsp;</td>
                  <td nowrap align="center" class="bordereporte"><%=(info.getValor_13()!=null)?info.getValor_13():""%>&nbsp;</td>
				  <td nowrap align="center" class="bordereporte"><%=(info.getValor_14()!=null)?info.getValor_14():""%>&nbsp;</td>
				  <td nowrap align="center" class="bordereporte"><%=(info.getValor_22()!=null)?info.getValor_22():""%>&nbsp;</td>
				  <td nowrap align="left" class="bordereporte"><%=(info.getValor_17().equals("A"))?"Anulado":info.getValor_17()%>&nbsp;</td>
				  
				  
                  
			    </tr>
				         <%
	  	}%>
            </table>
   </table></td>
      </tr>
		    </table>
			<table width="30%"  border="2" align="center">  
			    <tr class="tblTitulo" align="center">
				  <td nowrap width="5%" align="right">Suma Total  :     _____________</td>
				</tr>
				<tr class="letra" align="center">
				  <td nowrap width="5%" align="right"><%=(prom.getValor_21().equals("DOL"))?UtilFinanzas.customFormat2( (prom.getValor_24().equals(""))?"0":prom.getValor_24() ):UtilFinanzas.customFormat((prom.getValor_24().equals(""))?"0":prom.getValor_24())%></td>
				</tr>
			</table>			
		    <p>&nbsp;</p>
			<table width="30%"  border="2" align="center">  
			    <tr class="tblTitulo" align="center">
				  <td width="5%" height="24" align="center" nowrap>Suma Valor Total</td>
				  <td nowrap width="5%" align="center">Promedio</td>
				</tr>
				<tr class="letra" align="center">
				  <td width="5%" height="24" align="center" nowrap><%=(prom.getValor_21().equals("DOL"))?UtilFinanzas.customFormat2( (prom.getValor_24().equals(""))?"0":prom.getValor_24() ):UtilFinanzas.customFormat((prom.getValor_24().equals(""))?"0":prom.getValor_24())%></td>
				  <td nowrap width="5%" align="center"><%=(prom.getValor_21().equals("DOL"))?UtilFinanzas.customFormat2( (prom.getValor_23().equals(""))?"0":prom.getValor_23() ):UtilFinanzas.customFormat((prom.getValor_23().equals(""))?"0":prom.getValor_23())%></td>
				</tr>
			</table>
		    <form name="form1" method="post" action="<%=CONTROLLER%>?estado=InformeAnticiposParaguachon&accion=Generar&opcion=1">
		  	<input name="fechai" type="hidden" id="fechai" value="<%=prom.getValor_18()%>">
		  	<input name="fechaf" type="hidden" id="fechaf" value="<%=prom.getValor_19()%>">
	  	  	<input name="agencia" type="hidden" id="agencia" value="<%=prom.getValor_15()%>">
	  <input name="Guardar22" src="<%=BASEURL%>/images/botones/exportarExcel.gif" type="image" class="boton" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onclick="Exportar();">
      <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" onclick="location.href='<%=BASEURL%>/jsp/finanzas/reportes/reporteAntiposPagados/ReporteAnticiposPagados.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
	  <input name="salir2" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
    </form>	
	  <%}	  
	  }
	  %>	  
</body>
		  </div>
</html>