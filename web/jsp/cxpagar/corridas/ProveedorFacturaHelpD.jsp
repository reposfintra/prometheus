<!--
- Autor : Ing. Ivan Dario Gomez Vanegas
- Date  : 01 Mayo 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Vista para mostrar la informacion de las ayudas 
--%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Descripcion de campos para la Autorizacion de Facturas</title>


<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">CORRIDAS</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2"> Autorizacion de corridas</td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2"> Corridas</td>
        </tr>
        <tr>
          <td width="131" class="fila"> Corrida No.</td>
          <td width="543"  class="ayudaHtmlTexto">Campo donde se muestra el numero de la corrida.</td>
        </tr>
        <tr>
          <td  class="fila"> Fecha Creacion</td>
          <td  class="ayudaHtmlTexto">Campo donde se muestra la fecha de creacion de la corrida </td>
        </tr>
        <tr>
          <td  class="fila">Generado Por</td>
          <td  class="ayudaHtmlTexto">Campo donde se muestra el nombre de la persona que genero la corrida.</td>
        </tr>
        <tr>
          <td  class="fila">Valor</td>
          <td  class="ayudaHtmlTexto">Campo donde se muestra el valor total de la corrida.</td>
        </tr>
        <tr>
          <td  class="fila">Total Fact.</td>
          <td  class="ayudaHtmlTexto">Campo donde se muestra la cantidad de facturas que contiene la corrida.</td>
        </tr>
        <tr>
          <td  class="fila"># Aut.</td>
          <td  class="ayudaHtmlTexto">Campo donde se muestra el numero de facturas autorizadas de la corrida.</td>
        </tr>
        <tr>
          <td  class="fila">Faltantes</td>
          <td  class="ayudaHtmlTexto">Campo donde se muestra el numero de facturas que faltan por autorizar en la corrida.</td>
        </tr>
        <tr>
          <td  class="fila">Boton Aceptar</td>
          <td  class="ayudaHtmlTexto">Boton para autorizar las corridas seleccionadas.</td>
        </tr>
        <tr>
          <td  class="fila">Boton Salir</td>
          <td  class="ayudaHtmlTexto">Boton para cerrar la ventana.</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
