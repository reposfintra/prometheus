<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
  Vector datos =  model.extractoService.getBeneficiarios();
%>

<html>
<head>
<title>BUSQUEDA DE VEHICULOS POR AGENCIA</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type='text/javascript' src="<%=BASEURL%>/js/reporte.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<link href="../../../css/caravana.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/caravana.css" rel="stylesheet" type="text/css">
</head>

<body onLoad="<%if(request.getParameter("cerrar")!=null && request.getParameter("cerrar").equals("ok")){%>window.close();<%}%>">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Seleccionar Beneficiario"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<form name="busq"  method="post" action='<%=CONTROLLER%>?estado=Extracto&accion=Search&opcion=propietario'>
<table width="557" border="2" align="center">
    <tr>
      <td>

<table width="99%" align="center" bgcolor="#FFFFFF">
              <tr>
                <td width="311" class="subtitulo1">Escoger Propietarios  de la corrida </td>
                <td width="285" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
  </table>
<table width="99%" align="center" bgcolor="#FFFFFF">              
  </table>
  <table width="99%" border="1" align="center"  bordercolor="#999999" bgcolor="#F7F5F4">
    <tr class="tblTitulo">
	 <td width="15">&nbsp;</td>
      <td width="134">NIT</td>      
      <td width="333">NOMBRE</td>      
      <td width="30">&nbsp;</td>
    </tr>
	  
	<%
	  for (int i = 0; i<datos.size(); i++){
          Propietario p = (Propietario) datos.elementAt(i);%>

    <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" >
	 <td class="tblTitulo" align="center"><%=i+1%></td>
      <td height="22" class="bordereporte"><%=p.getCedula()%></td>
      
      <td class="bordereporte"><%=p.getP_nombre()%>
            </td>
      <td class="bordereporte">
      </a> <span class="comentario"><span class="letras">
      <input type="checkbox" name="nit<%=i%>" value="<%=p.getCedula()%>">
      </span></span> </td>
      
    </tr>	
    <%}%>	
  </table>
  </td>
  </tr>
  </table>
  <br><table width="382" border="0" align="center">
  <tr>
      <td height="28" colspan="5"><div align="center">     
       <img <%if (datos.size()==0) {%>src="<%=BASEURL%>/images/botones/aceptarDisable.gif" <%} else {%>src="<%=BASEURL%>/images/botones/aceptar.gif" <%}%>name="mod" height="21" <%if (datos.size()>0) {%>onClick="busq.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"<%}%>>&nbsp;
       <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></div></td>
    </tr>
</table>

</form>
</div>
</body>
</html>
