<!--
- Autor : Ing. Ivan Dario Gomez Vanegas
- Date  : 18 Abril 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Vista que permite ver Los proveedores de un banco relacionados a una corrida 
--%>

<%-- Declaracion de librerias--%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.util.Util"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
  String msg = (request.getParameter("msg")!=null)? request.getParameter("msg"):"";
  List listado = model.corridaService.getListado();
  Paginacion paginacion = model.corridaService.paginacion.getPaginacion(); 
  String nroc = (String)session.getAttribute("nrocorrida");
  String BancoSucursal = (String)session.getAttribute("bancoSucursal");
  String Codigo   = (String)session.getAttribute("codigo");
  String Nombre   = (String)session.getAttribute("nombre");
  String HC       = (String)(!session.getAttribute("hc").equals("null") ? session.getAttribute("hc"):"");
  String banco    = (String)session.getAttribute("banco");
  String sucursal = (String)session.getAttribute("sucursal");
  String agencia = (String)session.getAttribute("agenciaBanco");
  String visible ="visible";
 
  
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);

%>
<html>
<head>
    <title>.: Proveedores de la Corrida No. <%= nroc %></title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <script type='text/javascript' src="<%=BASEURL%>/js/reporte.js"></script>
    <script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
    <script type='text/javascript' src="<%=BASEURL%>/js/script.js"></script>
    <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body onLoad="redimensionar();" onResize="redimensionar();">

    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Facturas del Proveedor"/>
    </div>

    <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
    
    
        <br>
<%if(listado.size()!=0){%>
        <form name="form1" method="post" id="form1" action="<%=CONTROLLER%>?estado=Corrida&accion=Search&cmd=AutorizarFactura">
        <br>
        <table width="550" border="2" align="center">
            <tr>
                <td>
                <table width="100%" align="center">
                    <tr>
                        <td width="500"  height="22" class="subtitulo1">CORRIDA No. <%= nroc+" "%></td>
                        <td width="500" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                    </tr>
                </table>
                <table width="100%" align="center">
                <tr >
                <td class='fila'>Banco - sucursal :</td><td  class='letra'> <%=BancoSucursal%></td>
            </tr>
			
                    <tr >
                    <td class='fila'>Codigo Proveedor:</td><td  class='letra'> <%=Codigo%></td>
                    </tr>
                    <tr >
                    <td class='fila'>Nombre Proveedor:</td><td  class='letra'> <%=Nombre%></td>
                    </tr>
					<tr >
                <td class='fila'>HC :</td><td  class='letra'> <%=HC%></td>
            </tr>
                </table>
                <table width="100%" border="0" height="10">
                <tr>
                    <td>
                        <table  width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
                            <tr class="tblTitulo">
                                <td align="center" width='20'><input type="checkbox" name="todasAprobadas"   id="All"   onclick='selectChecked(this.form,this)'></td>
                                <td  width='100' align="center">Factura</td>  
                                <td  width='100' align="center">Fecha Aut.</td>  
                                <td  width='120' align="center">Valor</td>
                                <td  width='100' align="center">Tipo Pago</td>
                            </tr>
                        </table> 	
                    </td>
                </tr>
                <tr>
                <td><div  style=" overflow:auto ; WIDTH: 100%; HEIGHT:340px; "><table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4" id="principal">
				<%Iterator it = listado.iterator();
                                int c = 0;
				
    			while(it.hasNext()){
    				Corrida corrida = (Corrida) it.next();
                                String  noDoc   =  corrida.getDocumento().replaceAll("#","");
                                String Check = (corrida.getSelected().equals("S"))?"checked":"";
				%>
	
                    <tr class="<%=(c%2==0?"filagris":"filaazul")%>"  >
                    <td  width='25' align="center"><input  type="checkbox" <%=Check%>  name="FacturaSelec"  id='FacturaSelec'  onclick='selectChecked(this.form,this)' value='<%=corrida.getDocumento().replaceAll("#","-_-")%>'></td>
                    
                    <td  width='110' align="center" class="bordereporte" >
                    
                        <% String urlWindowDetalle = CONTROLLER +"?estado=Buscar&accion=Factura&prov="+ corrida.getNitProveedor() + "&tipo_doc=010&documento="+ corrida.getDocumento().replaceAll("#","-_-"); %>
                        <a href="javascript: var win =window.open('<%=urlWindowDetalle%>','Detalle',' scrollbars=no, top=100,left=200, width=650, height=5500,  status=yes, resizable=yes  ');">
                           <%=corrida.getDocumento()%> 
                        </a>
                    </td>
                    
                    <td  width='115'align="center" class="bordereporte" ><%=(corrida.getPago().equals("0099-01-01"))?"":corrida.getPago()%></td>
                    <td  width='135'align="right"  class="bordereporte" ><%=Util.customFormat(corrida.getValor())%></td>
                    <td  width='*'align="center"  class="bordereporte" >
                        <a <%if(corrida.getTipoPago().equals("T")){%> onclick="vista<%=noDoc%>.style.visibility=''" style="cursor:hand" class='Simulacion_Hiper'<%}%>><%=corrida.getTipoPago()%></a>
                        
	   			
                        <div id='vista<%=noDoc%>'   style=" position:absolute; top:0; left:100; visibility:hidden;  z-index:0;" >
                                    
                        <table border="1" align="center" bordercolor="#123456" cellpadding='0' cellspacing='0' >
                        <tr class='fila'>
                        <td align='center'>

                            <table  width="100%" border="" align="center" bordercolor="#999999" bgcolor="#F7F5F4" cellpadding='0' cellspacing='0'>
                                <tr >
                                    <td  align="left" width='20' class="tblTitulo" nowrap>Banco</td> <td   nowrap class="filagris" ><%=corrida.getBancoTransferencia()%></td>
                                </tr>
                                <tr>
                                    <td  width='100' align="left" class="tblTitulo" nowrap>Sucursal</td><td   nowrap class="filagris" ><%=corrida.getSucursalTransferencia()%></td>
                                </tr>
                                <tr>
                                    <td  width='100' align="left" class="tblTitulo" nowrap>Tipo de Cuenta</td><td    nowrap class="filagris" ><%=corrida.getTipoCuentaTransferencia()%></td>
                                </tr>
                                <tr>
                                    <td  width='120' align="left" class="tblTitulo" nowrap>No. Cuenta</td><td    nowrap class="filagris" ><%=corrida.getNoCuentaTransferencia()%></td>
                                </tr>
                                <tr>    
                                    <td  width='100' align="left" class="tblTitulo" nowrap>Cedula Cuenta</td><td    nowrap class="filagris" ><%=corrida.getCedulaCuentaTransferencia()%></td>
                                </tr>
                                <tr>
                                    <td  width='100' align="left" class="tblTitulo" nowrap>Nombre Cuenta</td> <td   nowrap  class="filagris" ><%=corrida.getNombreCuentaTransferencia()%></td>
                                </tr>

                            </table> 
                            <a href='#'  onclick="vista<%=noDoc%>.style.visibility='hidden'">Cerrar</a>
                        </td>
                    </td>
                        </table>
                                            
                    </div>
                    
                    
                </td>
                    </tr>
  				<% c++;
                                }//fin while%>
				
                </table></div>
            </td>
                </tr>
            </table>
            </td>
            </tr>
        </table>
        <br>
        
        
        
	<% if(!msg.equals("")){ visible ="hidden";}%>
	
	<% String urlADDFacturas = CONTROLLER +"?estado=AdicionarFacturas&accion=Corridas&evento=BUSCAR&corrida="+ nroc +"&banco="+ banco +"&sucursal="+ sucursal +"&proveedor="+ Codigo +"&name="+ Nombre; %>            
        <div align='center' id='boton' style='visibility:<%=visible%>'>
            <table width="76%"  border="0" align="center">
                <tr>
                    <td align="center">
                        <img src="<%=BASEURL%>/images/botones/aceptar.gif"   name="imgaceptar"   title='Aprobar'            height="21" onMouseOver="botonOver(this);" onClick="form1.submit();" onMouseOut="botonOut(this);" style="cursor:hand">  
                        <img src="<%=BASEURL%>/images/botones/agregar.gif"   name="imgagregar"   title='Agregar Facturas'   height="21" onMouseOver="botonOver(this);" onClick=" var win =window.open('<%=urlADDFacturas%>','Adicionar',' scrollbars=yes, top=100,left=200, width=500, height=400,  status=yes, resizable=yes  ');" onMouseOut="botonOut(this);" style="cursor:hand">  
                        <img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgregresar"  title='Regresar'           height="21" onMouseOver="botonOver(this);" onClick="location.href='<%=CONTROLLER%>?estado=Corrida&accion=Search&cmd=busquedaProveedor&nrocorrida=<%=nroc%>&banco=<%= banco %>&sucursal=<%= sucursal %>&agencia=<%= agencia %>'" onMouseOut="botonOut(this);" style="cursor:hand"> 
                        <img src="<%=BASEURL%>/images/botones/salir.gif"     name="imgsalir"     title='Salir'              height="21" onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">
                    </td>
                </tr>
            </table>
            
             
        </div>
        
        
        
	<%if(!msg.equals("")){%>
        <div id="msg" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;visibility:visible;"> 
            <table width="400" border="1" align="center"  bordercolor="#123456" cellpadding='0' cellspacing='0' >
                <tr >
                    <td>
                        <table width="103%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                            <tr>
                                <td width="290" align="center" class="mensajes"><%=msg%><br><br><img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imga" height="21" onMouseOver="botonOver(this);" onClick="msg.style.visibility = 'hidden';boton.style.visibility = 'visible';" onMouseOut="botonOut(this);" style="cursor:hand"></td>
                                <td width="38" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="68">&nbsp;</td>
                            </tr>
                           
                        </table>
                    </td>
                </tr>
  			
            </table>
        </div>	
  <%}%>
        </form>
<%}else {
	msg = "No se encuentran Proveedores con Facturas SIN APROBACION en Corrida seleccionada...";
  	if(!msg.equals("")){%>
        <table width="436" border="2" align="center">
            <tr>
                <td>
                    <table width="103%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                        <tr>
                            <td width="274" align="center" class="mensajes"><%=msg%></td>
                            <td width="38" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                            <td width="68">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <br>
        <center><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgregresar"  height="21"onMouseOver="botonOver(this);" onClick="window.history.back();" onMouseOut="botonOut(this);" style="cursor:hand"> 
        <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" height="21" onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand"></center>
  <%}
 }%>
    </div>
<%=datos[1]%>



</body>
</html>
