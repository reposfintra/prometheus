 <!--
- Autor : FERNEL VILLACOB DIAZ
- Modifico : ING. ANDRES MATURANA DE LA CRUZ
- Date  : 18/10/2005  
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Vista que permite ejecutar el proceso de liberacion de facturas en la corrida
--%>

<%@page session="true"%> 
<%@page import="com.tsp.operation.model.beans.*, java.util.TreeMap"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.util.*"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
  TreeMap selec = model.LiberarFacturasSvc.getTreemap(); %>
  
<html>
<head>
     <title>Liberar Facturas en </title>
     <link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
     <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
     <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
     <script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
<script>	
	function enviar(){
		if( formulario.corridas.length==0 ){
			alert('No se puede procesar la informaci�n. No ha escogido ninguna corrida');
			return false;
		} else {		
			selectAll(formulario.corridas); 
			formulario.submit();
		}	
	}	
</script>
<script>
	function loadCombo(datos, cmb){
		cmb.length = 0;
		for (i=0;i<datos.length;i++){
			var dat = datos[i].split(SeparadorJS);
			addOption(cmb, dat[0], dat[1]);
		}
		//cmb.style.width = 'auto';
		order(cmb); 
		deleteRepeat(cmb);                       
	}
	
	function unloadCombo(datos, cmb){
		for(j=0; j<datos.length; j++){
			for (i=0;i<cmb.length;i++){
					var dat = datos[j].split(SeparadorJS);
					if( dat[0] == cmb[i].value ){
							cmb.remove(i);
					}
					//addOption(cmb, dat[0], dat[1]);
			}
		}
		order(cmb);
	}

	function addOption(Comb,valor,texto){
	   if(valor!='' && texto!=''){
			var Ele = document.createElement("OPTION");
			Ele.value=valor;
			Ele.text=texto;
			Comb.add(Ele);
	  }
	}

	function deleteRepeat(cmb){
	  var ant='';
	  i=0;
	  while(i<cmb.length){
		 if(ant== cmb[i].value){
			ant = cmb[i].value;
			cmb.remove(i);
			i--;
		 } else {              
			ant = cmb[i].value;
		 }            
		 i++;
	  }
	}

	function order(cmb){
	   for (i=0; i<cmb.length;i++)
		 for (j=i+1; j<cmb.length;j++)
			if (cmb[i].text > cmb[j].text){
				var temp = document.createElement("OPTION");
				temp.value = cmb[i].value;
				temp.text  = cmb[i].text;
				cmb[i].value = cmb[j].value;
				cmb[i].text  = cmb[j].text;
				cmb[j].value = temp.value;
				cmb[j].text  = temp.text;
			}
	}
	
	function order2(cmb){
	   for (i=0; i<cmb.length;i++)
		 for (j=i+1; j<cmb.length;j++)
			if (parseInt(cmb[i].text) > parseInt(cmb[j].text) ){
				var temp = document.createElement("OPTION");
				temp.value = cmb[i].value;
				temp.text  = cmb[i].text;
				cmb[i].value = cmb[j].value;
				cmb[i].text  = cmb[j].text;
				cmb[j].value = temp.value;
				cmb[j].text  = temp.text;
			}
	}

	function move(cmbO, cmbD){
	   for (i=cmbO.length-1; i>=0 ;i--)
		if (cmbO[i].selected){
		   addOption(cmbD, cmbO[i].value, cmbO[i].text)
		   cmbO.remove(i);
		}
	   //order(cmbD);
	   deleteRepeat(cmbD);
	   order2(cmbD);
	}
	
	function move2(cmbO, cmbD){
	   for (i=cmbO.length-1; i>=0 ;i--)
		if (cmbO[i].selected){
		   addOption(cmbD, cmbO[i].value, cmbO[i].text)
		   cmbO.remove(i);
		}
	   order(cmbD);
	   deleteRepeat(cmbD);
	}

	function moveAll(cmbO, cmbD){
	   for (i=cmbO.length-1; i>=0 ;i--){
		 if(cmbO[i].value!='' && cmbO[i].text!=''){
		   addOption(cmbD, cmbO[i].value, cmbO[i].text)
		   cmbO.remove(i);
		 }
	   }
	   order(cmbD);
	   deleteRepeat(cmbD);
	   order2(cmbD);
	}
	
	function selectAll(cmb){
		for (i=0;i<cmb.length;i++)
		  cmb[i].selected = true;
	}        
</script>	   
</head>
<body>

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Liberaci�n de Facturas en Corridas"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>


<% String msj = request.getParameter("msj"); %>

  <form action="<%=CONTROLLER%>?estado=LiberarFacturas&accion=XCorridas&evento=FREE"  method='post' name='formulario' >

       <table width="450" border="2" align="center">
           <tr>
                  <td>  
                           <table width='100%' align='center' class='tablaInferior'>

                                  <tr class="barratitulo">
                                        <td width="100%"  >
                                           <table cellpadding='0' cellspacing='0' width='100%'>
                                             <tr>
                                              <td align="left" width='55%' class="subtitulo1">&nbsp;Liberaci�n de Corridas</td>
                                              <td align="left" width='*'  ><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
                                            </tr>
                                           </table>
                                        </td>
                                 </tr>
                                 
                                 
 
                                      <tr class="barratitulo">
                                        <td class='fila'><table width="100%"  border="0">
                                          <tr>
                                            <td class="subtitulo1"><div align="center"> Disponibles</div></td>
                                            <td>&nbsp;</td>
                                            <td class="subtitulo1"><div align="center"> Seleccionadas </div></td>
                                          </tr>
                                          <tr>
                                            <td width="45%"><input:select name="c_corridas" attributesText="size='10' multiple class='textbox' style='width:100%'" options="<%= selec %>"/></td>
                                                                                    <script>order2(document.formulario.c_corridas);</script>
                                            <td width="10%"><div align="center"><img src="<%=BASEURL%>/images/botones/envDer.gif" name="c_corridasregresar" id="c_corridasregresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick=" move(c_corridas,  corridas ); " style="cursor:hand "><br>
                                              <img src="<%=BASEURL%>/images/botones/enviarDerecha.gif" name="c_corridasregresar" id="c_corridasregresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="moveAll(c_corridas,  corridas ); " style="cursor:hand "> <br>
                                              <img src="<%=BASEURL%>/images/botones/envIzq.gif" name="c_corridasregresar" id="c_corridasregresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="move(corridas, c_corridas ); " style="cursor:hand "><br>
                                              <img src="<%=BASEURL%>/images/botones/enviarIzq.gif" name="c_corridasregresar" id="c_corridasregresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="moveAll(corridas, c_corridas ) ; " style="cursor:hand "> <br>
                                            </div></td>
                                            <td width="45%"><select name="corridas" size="10" multiple class="textbox" id="corridas" style="width:100% ">
                                            </select></td>
                                          </tr>
                                          <tr>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                          </tr>
                                        </table></td>
                                      </tr>
                                      <tr class="barratitulo">
                                            <td height='60'  class='informacion'>
                                               Permite Liberar las facturas  en corridas del usuario que no se han  aprobado
                                               para pago  �  cuya suma de valores sean negativos, permitiendo as� que sean incluidas en otra corrida �
                                               pagadas en forma independientes. 
                                            </td>
                                     </tr>

                                       

                       </table>
                 </td>
          </tr>
      </table>

 </form>

  <p>       
       <img src='<%=BASEURL%>/images/botones/aceptar.gif'    style="cursor:hand"  title='Crear....'       name='i_crear'       onclick="enviar();"  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>        
       <img src='<%=BASEURL%>/images/botones/cancelar.gif'      style=' cursor:hand'   title='Resetear...'        name='i_salir'       onclick='formulario.reset();'       onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
       <img src='<%=BASEURL%>/images/botones/salir.gif'      style=' cursor:hand'   title='Salir...'        name='i_salir'       onclick='parent.close();'       onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
  </p> 
   
  
  
 <!-- Mensaje -->
 <%  if ( msj!=null  && !msj.equals("")){%>
         <table border="2" align="center">
               <tr>
                    <td>
                           <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                              <tr>
                                    <td width="450" align="center" class="mensajes"><%=msj%></td>
                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                    <td width="58">&nbsp; </td>
                              </tr>
                      </table>
                   </td>
               </tr>
        </table>
 <%}%>
     
  

</div>
<%=datos[1]%>
</body>
</html>
