<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%@page import="java.util.*, java.text.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
  Vector extracto = model.extractoService.getExtracto();
  EncPropietario encProp = null;
  String tipo = "";
  DetalleFactura detalleFact = null;
  EncFactura encFact = null;
  Vector facturas = null;
  Run run = null;
  Comentario coment = null;
  DecimalFormat form = new DecimalFormat("#,##0.00;-#,##0.00");

%>
<html>
<head>
     <title>EXTRACTO AGENCIAS</title>
     <link href="<%=BASEURL%>/css/EstilosExtracto.css" rel="stylesheet" type="text/css">
</head>
<body onLoad="<%if(request.getParameter("reload")!=null) {%>window.open('<%=BASEURL%>/jsp/cxpagar/corridas/ReporteExtracto.jsp','ext','width=750,heigth=550,status=yes,menubar=yes,scrollbars=yes,resizable=yes');window.close();<%}%>">
<%-- <jsp:useBean id="beanInstanceName" scope="session" class="beanPackage.BeanClassName" /> --%>
<%-- <jsp:getProperty name="beanInstanceName"  property="propertyName" /> --%>
      <table align='center'>
            <%run = (Run)extracto.get(0);
              out.println("<tr class='TableHeaderDecoration'><td>Distrito: &nbsp;"+run.getAcct_dstrct()+"</td><td colspan='10'>Corrida: &nbsp;"+run.getCheque_run_no()+"</td></tr>");
              out.println("<tr class='TableHeaderDecoration'><td>Banco: &nbsp;"+run.getBanco()+"</td><td colspan = '10'>Cuenta: &nbsp;"+run.getSucursal()+"</td></tr>");
              out.println("<tr class='TableHeaderDecoration'><td colspan='11'>"+run.getMsg()+"</td></tr>");
              out.println("<tr class='TableHeaderDecoration'><td colspan='11'>&nbsp</td></tr>");
              out.print("<tr class='TableHeaderDecoration'><td colspan='11'><h1 class='SaltoDePagina'></h1></td></tr>");
              for (int i = 1; i < extracto.size(); i++){
                 if (i != 1)
                     out.print("<tr class='TableHeaderDecoration'><td colspan='11'><h1 class='SaltoDePagina'></h1></td></tr>");
                 encProp = (EncPropietario)extracto.get(i);
                 facturas = encProp.getFacturas();
                 tipo = encProp.getTipo_pago().equals("B")?"Banco":"Transferencia";
                 //primera linea
                 out.println("<tr class='TableHeaderDecoration'><td colspan='4'>Corrida &nbsp;"+encProp.getCorrida()+"</td><td colspan='7'>Fecha: &nbsp;"+encProp.getFecha()+"</td></tr>");
                 //segunda linea creada por jose
                 out.println("<tr class='TableHeaderDecoration'><td colspan='4'>Tipo de pago:&nbsp;"+ run.getTpago() +"</td>"); if (run.getTpago().equals("Transferencia")) { out.print("<td colspan='7'>Banco Transferencia: "+ encProp.getBanco_transfer() +"</td></tr>"); } else { out.print("<td colspan='7'></td></tr>"); }
                 if (run.getTpago().equals("Transferencia")) {
                     out.println("<tr class='TableHeaderDecoration'><td colspan='2'>Sucursal Trans: "+ encProp.getSuc_transfer() +"</td><td colspan='2'>Tipo cuenta: "+ encProp.getTipo_cuenta() +"</td><td colspan='7'>Numero cuenta: "+ encProp.getNo_cuenta() +"</td></tr>");
                     out.println("<tr class='TableHeaderDecoration'><td colspan='4'>Nombre Cuenta: "+ encProp.getNombre_cuenta() +"</td><td colspan='7'>Cedula cuenta: "+ encProp.getCedula_cuenta() +"</td></tr>");
                 }
                 //segunda linea
                 out.println("<tr class='TableHeaderDecoration'><td colspan='11'>Extracto de pagos de planillas cumplidas hasta:  &nbsp;"+encProp.getFechaCumplido()+"</td></tr>");
                 out.println("<tr class='TableHeaderDecoration'><td colspan='11'>Propietario: &nbsp;"+encProp.getPropietario()+"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Nombre: &nbsp;"+encProp.getNombre()+"</td></tr>");
                 //out.println("<tr class='TableHeaderDecoration'></tr>");

                 for (int j = 0; j < facturas.size(); j++){
                     if (facturas.get(j) instanceof String){
                         out.println("<tr class='TableSubHeaderDecoration'><td colspan='11'>PLACA: &nbsp;"+facturas.get(j)+"</td></tr>");
                         out.println("<tr class='TableSubHeaderDecoration'><th>FACTURA</th><th>ITEM</th><th>DESCRIPCION</th><th>VALOR</th><th>IVA</th><th>RIVA</th><th>RICA</th><th>RFTE</th><th>NETO</th><th colspan='2'>MONEDA</th></tr>");
                     }

                     if (facturas.get(j) instanceof EncFactura){
                        encFact = (EncFactura)facturas.get(j);
                        out.println("<tr class='TableSubHeaderDecoration'><td>PLANILLA: &nbsp;"+encFact.getPo_no()+"</td><td colspan='3'>CONDUCTOR: &nbsp;"+encFact.getConductor()+"</td><td colspan='7'>&nbsp;FECHA PLANILLA:&nbsp;"+encFact.getFecha()+"</td></tr>");
                        if (run.getTipo().equals("P") )
                            out.println("<tr class='TableSubHeaderDecoration'><td>REMISION: &nbsp;"+encFact.getRemision()+"</td><td colspan = '3'>CARGA: &nbsp;"+encFact.getCarga()+"</td><td colspan = '7'>&nbsp;RUTA: &nbsp;"+encFact.getOrigen()+"-"+encFact.getDestino()+"</td></tr>");
                     }

                     if (facturas.get(j) instanceof DetalleFactura){
                         detalleFact = (DetalleFactura)facturas.get(j);
                        if (detalleFact.getFacturaNo().startsWith("<b>TOTAL") || detalleFact.getFacturaNo().startsWith("<b>NO") || detalleFact.getFacturaNo().startsWith("<b>NETO") || detalleFact.getFacturaNo().startsWith("<b>SALDO") || detalleFact.getFacturaNo().startsWith("TOTALES") || detalleFact.getFacturaNo().startsWith("<b>TOTAL BANCO")) {
                             if(detalleFact.getFacturaNo().startsWith("<b>NO"))
                                 out.println("<tr class='TableRowDecoration'><td colspan='11' style='color:#FF0000'>"+detalleFact.getFacturaNo()+"</td></tr>");
                             else if (detalleFact.getFacturaNo().startsWith("TOTALES"))
                                 out.println("<tr class='TableRowDecoration'><td colspan='3'><b>"+detalleFact.getFacturaNo()+"</td><td align='right'><b>"+form.format(detalleFact.getItemValor())+"</td><td align='right'><b>"+form.format(detalleFact.getIva())+"</td><td align='right'><b>"+form.format(detalleFact.getRiva())+"</td><td align='right'><b>"+form.format(detalleFact.getRica())+"</td><td align='right'><b>"+form.format(detalleFact.getRfte())+"</td><td align='right'><b></td><td colspan='2' align='center'>"+detalleFact.getMoneda()+"</td></tr>");
                             else if (run.getTipo().equals("A")) {
                                 out.println("<tr class='TableRowDecoration'><td colspan='3'>"+detalleFact.getFacturaNo()+"</td><td colspan='5'><b>"+detalleFact.getTotalFactProvString()+"</td><td nowrap align='right'><b>"+form.format(detalleFact.getItemValor())+"</b></td><td colspan='2' align='center'>"+detalleFact.getMoneda()+"</td></tr>");
                             } else if (!detalleFact.getFacturaNo().startsWith("<b>SALDO") && !detalleFact.getFacturaNo().startsWith("<b>TOTAL FACTURA"))
                                 out.println("<tr class='TableRowDecoration'><td colspan='3'>"+detalleFact.getFacturaNo()+"</td><td align='right'><b>"+detalleFact.getTotalFactProvString()+"</td><td nowrap colspan='5' align='right'><b>"+form.format(detalleFact.getItemValor())+"</b></td><td colspan='2' align='center'>"+detalleFact.getMoneda()+"</td></tr>");

                         }else
                             out.println("<tr class='TableRowDecoration'><td >"+detalleFact.getFacturaNo()+"</td><td >"+detalleFact.getItemNo()+"</td><td>"+detalleFact.getItemDesc()+"</td><td align='right'>"+form.format(detalleFact.getItemValor())+"</td><td align='right'>"+form.format(detalleFact.getIva())+"</td><td align='right'>"+form.format(detalleFact.getRiva())+"</td><td align='right'>"+form.format(detalleFact.getRica())+"</td><td align='right'>"+form.format(detalleFact.getRfte())+"</td><td align='right'>"+form.format(detalleFact.getNeto())+"</td><td  colspan='2' align='center'>"+detalleFact.getMoneda()+"</td></tr>");
                     }

                     if (facturas.get(j) instanceof Comentario){
                        coment = (Comentario)facturas.get(j);
                        out.println("<tr class='TableSubHeaderDecoration'><td colspan='11'>&nbsp;</td></tr>");
                        out.println("<tr class='TableSubHeaderDecoration'><td colspan='11'>"+coment.MSG1+"</td></tr>");
                        out.println("<tr class='TableSubHeaderDecoration'><td colspan='11'>"+coment.MSG2+"</td></tr>");
                        out.println("<tr class='TableSubHeaderDecoration'><td colspan='11'>"+coment.MSG3+"</td></tr>");
                        out.println("<tr class='TableSubHeaderDecoration'><td colspan='11'>"+coment.MSG4+"</td></tr>");
                        out.println("<tr class='TableSubHeaderDecoration'><td colspan='11'>"+coment.MSG5+"</td></tr>");
                        out.println("<tr class='TableSubHeaderDecoration'><td colspan='11'>"+coment.MSG6+"</td></tr>");
                        out.println("<tr class='TableSubHeaderDecoration'><td colspan='11'>"+coment.MSG7+"</td></tr>");
                        out.println("<tr class='TableSubHeaderDecoration'><td colspan='11'>&nbsp;</td></tr>");
                     }
                 }
              }%>
      </table>

</body>
</html>
