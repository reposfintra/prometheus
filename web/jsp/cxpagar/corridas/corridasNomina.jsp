<%-- 
    Document   : archivoNomina
    Created on : 18/04/2015, 09:15:24 AM
    Author     : egonzalez
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Transferencia Pago de Nomina</title>

        <link href="/fintra/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="/fintra/css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="/fintra/css/jquery/jquery-ui/jquery-ui.css"/>
        <link href="/fintra/css/reestructuracion.css" type="text/css" rel="stylesheet" />
        <link rel="stylesheet" type="text/css" href="/fintra/css/jCal/jscal2.css" />
        
        <script type="text/javascript" src="/fintra/js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="/fintra/js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="/fintra/js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="/fintra/js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>

        <script type="text/javascript" src="/fintra/js/jCal/jscal2.js"></script>
        <script type="text/javascript" src="/fintra/js/jCal/lang/es.js"></script>
        <script type="text/javascript" src="/fintra/js/CorridasNomina.js"></script>

        <style>
            .tablaPrevia {
                width: 97%;
                font-size: 12px;
                border: 1px solid rgba(21, 83, 20, 0.36);
                border-collapse: collapse;
                margin-bottom: 5px;
            }
            select{
                height: 30px  !important;
                font-size: 12px !important;
            } 
        </style>
    </head>
    <body onload="init();">
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Transferencia Pago de Nomina"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <center>
                <div class="k-block" style="height: auto; border-color: #070;">
                    <div class="k-header"> <span class="titulo">Transferencia Pago de Nomina</span> </div>
                    <div id="contenido">
                        <table class="table"  style="width: 471px;">
                            <tr>
                                <td class="td">Banco transferencia</td>
                                <td class="td">
                                    <select id="banco_trans" onchange="buscarSucursal()">
                                        <option value=''>...</option>
                                    </select>
                                </td>
                                <td class="td">
                                    <select id="sucursal_trans" style="width: 174px;">
                                        <option value=''>...</option>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class='td'>Facturas de</td>
                                <td class="td" >
                                    <select id="fil_gerencia" >
                                        <option value=""></option>
                                        <option value="N">Empleados</option>
                                        <option value="S">Gerencia</option>
                                    </select>
                                </td>
                                <td class='td'>A�o:  <input id="anio" value="" size="4" maxlength="4" /> mes:
                                    <select id="mes" style="width: 50px" onchange="getFacturas()">
                                        <option value=""></option>
                                        <option value="01">01</option>
                                        <option value="02">02</option>
                                        <option value="03">03</option>
                                        <option value="04">04</option>
                                        <option value="05">05</option>
                                        <option value="06">06</option>
                                        <option value="07">07</option>
                                        <option value="08">08</option>
                                        <option value="09">09</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                      
                                    </select>
                                </td>
                            </tr>
                        </table>
                        <hr>
                        <div style="text-align: right;margin-right: 28px; margin-bottom: 5px;">
                            <button id="previo" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false"
                                    onclick="generarPrevio()">
                                <span class="ui-button-text">ver previo</span>
                            </button>
                            <button id="aceptar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false"
                                    onclick="generarEgresos()">
                                <span class="ui-button-text">aceptar</span>
                            </button>
                        </div>
                    </div>
                </div>
                <br><br><br>
                <div id="tabla">
                    <table id="documentos_tabla"></table>
                    <div id="documentos_page"></div>
                </div>
            </center>
        </div>
        <div id="capaPreviaExportacion" class="ventana">
            <center>
                <div class="k-block" style="height: auto; width: 820px; border-color: #070;">
                    <div class="k-header"> <span class="titulo">Exportacion Transferencia Pago de Nomina</span> </div>
                    <div id="contenido_prev">
                        <table class="table">
                            <tr>
                            <a id="nit_trans_prev" style="display:none;"></a>
                                <td class="td" colspan="2">BANCO DE TRANSFERENCIA</td>
                                <td class="td">
                                    <a id="banco_trans_prev"></a>
                                </td>
                                <td class="td">
                                    <a id="sucursal_trans_prev"></a>
                                </td>
                                <td class="td" colspan="2">
                                    <a id="cuenta_trans_prev"></a>
                                </td>
                            </tr>
                            <tr>
                                <td class="td">TIPO DE PAGO</td>
                                <td class="td">
                                    <a id="tipo_pago" title="225">NOMINA</a>
                                </td>
                                <td class="td">DESCRIPCION</td>
                                <td class="td">
                                    <input id="descripcion" value="NOM1501" maxlength="10" size="10" ></input>
                                </td>
                                <td class="td">SECUENCIA</td>
                                <td class="td">
                                    <input id="secuencia" value="A" maxlength="2" size="1"></input>
                                </td>
                            </tr>
                            <tr>
                                <td class="td">FECHA DE TRANSMISION</td>
                                <td class="td" colspan="2">
                                    <input type="text" id="fec_transmision" name="fec_transmision" readonly maxlength="10" size="10"/>
                                    <img src="/fintra/images/cal.gif" id="imgFecTransmision" alt="fecha" title="fecha"/>
                                    <script type="text/javascript">
                                            document.getElementById("fec_transmision").value = new Date().toJSON().slice(0,10);
                                             Calendar.setup({
                                                 inputField: "fec_transmision",
                                                 trigger: "imgFecTransmision",
                                                 align: "top",
                                                 onSelect: function() {
                                                     this.hide();
                                                 }
                                             });
                                    </script>   
                                </td>
                                <td class="td">FECHA DE APLICACION</td>
                                <td class="td" colspan="2">
                                    <input type="text" id="fec_aplicacion" name="fec_aplicacion" readonly maxlength="10" size="10"/>
                                    <img src="/fintra/images/cal.gif" id="imgFecAplicacion" alt="fecha" title="fecha" />
                                    <script type="text/javascript">
                                            document.getElementById("fec_aplicacion").value = new Date().toJSON().slice(0,10);
                                            Calendar.setup({
                                                 inputField: "fec_aplicacion",
                                                 trigger: "imgFecAplicacion",
                                                 align: "top",
                                                 onSelect: function() {
                                                     this.hide();
                                                 }
                                             });
                                    </script> 
                                </td>
                            </tr>
                        </table>
                        <hr>
                        <div style="text-align: right;margin-right: 28px; margin-bottom: 5px;">
                            <button id="aceptar_prev" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false"
                                    onclick="generarArchivo()">
                                <span class="ui-button-text">Generar archivo</span>
                            </button>
                        </div>
                    </div>
                    <br>
                    <div>
                        <table id="documentos_prev" class="tablaPrevia">
                            <thead>
                                <tr>
                                    <th class="ui-state-default">Egreso</th>
                                    <th class="ui-state-default">NIT/CC</th>
                                    <th class="ui-state-default">Nombre</th>
                                    <th class="ui-state-default">Banco</th>
                                    <th class="ui-state-default">Cuenta</th>
                                    <th class="ui-state-default">Valor</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                            <tfoot>
                                <tr>
                                    <th class="ui-state-default" colspan="4"></th>
                                    <th class="ui-state-default" id="cant_doc"></th>
                                    <th class="ui-state-default" id="sum_doc"></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </center>
        </div>
    </body>
</html>