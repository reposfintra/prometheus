<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
</head>

<body>
<br>


<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Liberar Facturas en Corridas</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Liberar Facturas en Corridas</td>
        </tr>
        <tr>
          <td class="fila">Corridas Disponibles</td>
          <td  class="ayudaHtmlTexto">Esta lista muestra todos los n&uacute;meros de corridas que contienen facturas sin aprobar que son h&aacute;biles para realizar el proceso de liberaci&oacute;n de facturas. </td>
        </tr>
        <tr>
          <td class="fila">Corridas Seleccionadas </td>
          <td  class="ayudaHtmlTexto">Campo que almacena los n&uacute;meros de corridas que se seleccionan de la lista de corridas disponibles y se utilizar&aacute;n para realizar el proceso de liberaci&oacute;n de facturas. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
            <input name="image23" type="image" style="cursor:default " src = "<%=BASEURL%>/images/botones/envDer.gif">
          </div></td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que permite seleccionar un n&uacute;mero de corrida de la lista de corridas disponibles. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
            <input name="image232" type="image" style="cursor:default " src = "<%=BASEURL%>/images/botones/enviarDerecha.gif">
          </div></td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que permite seleccionar todos los n&uacute;meros de corridas de la lista de corridas disponibles. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
              <input name="image23" type="image" style="cursor:default " src = "<%=BASEURL%>/images/botones/envIzq.gif">
          </div></td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que permite deseleccionar un n&uacute;mero de corrida de la lista de corridas disponibles. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
              <input name="image232" type="image" style="cursor:default " src = "<%=BASEURL%>/images/botones/enviarIzq.gif">
          </div></td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que permite deseleccionar todos los n&uacute;meros de corridas de la lista de corridas disponibles. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
            <input name="image3" type="image" style="cursor:default " src = "<%=BASEURL%>/images/botones/aceptar.gif">
          </div></td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que da inicio al proceso de liberaci&oacute;n de facturas. </td>
        </tr>
        <tr>
          <td width="149" class="fila"><div align="center">
            <input type="image" src = "<%=BASEURL%>/images/botones/cancelar.gif" style="cursor:default ">
          </div></td>
          <td width="525"  class="ayudaHtmlTexto">Bot&oacute;n que resetea el formulario, regres&aacute;ndolo a su estado inicial. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
            <input type="image" src = "<%=BASEURL%>/images/botones/salir.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto"> Bot&oacute;n que cierra el programa.</td>
        </tr>
        
      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
