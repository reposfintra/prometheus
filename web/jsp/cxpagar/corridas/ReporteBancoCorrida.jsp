<!--
- Autor : Ing. Ivan Dario Gomez Vanegas
- Date  : 18 Abril 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Vista que permite ver Los proveedores de un banco relacionados a una corrida 
--%>

<%-- Declaracion de librerias--%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.util.Util"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
 
  List listado = model.extractoService.getCorridaBanco();


  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>.: Corridas de un banco</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type='text/javascript' src="<%=BASEURL%>/js/reporte.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/script.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Corridas del banco"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<br>
<form name="form1" method="post" id="form1" action="">
<br>
	<table width="700" border="2" align="center">
    	<tr>
      	<td>
	  		<table width="100%" align="center">
        		<tr>
            		<td width="500"  height="22" class="subtitulo1">CORRIDAS  </td>
                	<td width="500" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
          		</tr>
		  </table>

			<table width="100%" border="0" height="10">
			    <tr>
                            <td>
                               <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
	 			<tr class="tblTitulo">
                                    <td width='310' align="center"> Banco</td>
                                    <td width='320' align="center"> Sucursal</td>
                                    <td width='100' align="center"> Corrida</td>

    			        </tr>
	 			</table>
                              </td>
                             </tr>
                             <tr>
                             <td><div  style=" overflow:auto ; WIDTH: 100%; HEIGHT:340px; "><table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4" id="principal">
				<%Iterator it =listado.iterator();
                                int c = 0;
    			while(it.hasNext()){
    				Corrida corrida = (Corrida) it.next();
				%>

  				<tr class="<%=(c%2==0?"filagris":"filaazul")%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" onClick="location.href='<%= CONTROLLER %>?estado=Extracto&accion=Search&distrito=<%=model.extractoService.getDistrito()%>&corrida=<%=corrida.getCorrida()%>&banco=<%= corrida.getBanco() %>&cuenta=<%= corrida.getSucursal()%>&aprobadas=<%=model.extractoService.getAprobadas()%>&pagadas=<%=model.extractoService.getPagadas()%>&tipo=<%=model.extractoService.getTipo()%>'" title="Haga clic para imprimir la corrida..." align="center" class="bordereporte" >
    				<td  width='285'class="bordereporte"><%=corrida.getBanco()%> </td>
    				<td  width='295'class="bordereporte"><%=corrida.getSucursal()%></td>
				<td  width='93' class="bordereporte"><%=corrida.getCorrida()%></td>

	   			</tr>
  				<% c++;
                                }//fin while%>
    			</table></div>
    		       </td>
    		    </tr>
		  </table>
		</td>
		</tr>
	</table>
	<br>
	<table width="76%"  border="0" align="center">
  	<tr>
    	<td align="center">
    	            <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" height="21" onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">
		</td>
	</tr>
	</table>
	</div>

</form>

</div>
<%=datos[1]%>
</body>
</html>



