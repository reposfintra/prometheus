<!--
- Autor : Ing. Ivan Dario Gomez Vanegas
- Date  : 18 Abril 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Vista que permite ver Los proveedores de un banco relacionados a una corrida 
--%>

<%-- Declaracion de librerias--%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.util.Util"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
  String msg = (request.getParameter("msg")!=null)? request.getParameter("msg"):"";
  List listado = model.corridaService.getListado();
  Paginacion paginacion = model.corridaService.paginacion.getPaginacion(); 
  String nroc = (String)session.getAttribute("nrocorrida");
  String BancoSucursal = (String)session.getAttribute("bancoSucursal");
  String visible ="visible";
  
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>.: Proveedores de la Corrida No. <%= nroc %></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type='text/javascript' src="<%=BASEURL%>/js/reporte.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/script.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Proveedores del Banco"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<br>
<%if(listado.size()!=0){%>
<form name="form1" method="post" id="form1" action="<%=CONTROLLER%>?estado=Corrida&accion=Search&cmd=AutorizarProveedor">
<br>

	<table width="900" border="2" align="center">
    	<tr>
      	<td>
	  		<table width="100%" align="center">
        		<tr>
            		<td width="500"  height="22" class="subtitulo1">CORRIDA No. <%= nroc+" "%>  -  <%=" "+BancoSucursal%></td>
                	<td width="500" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
          		</tr>
		  </table>
  		
			<table width="100%" border="0" height="10">
			    <tr>
                            <td>
                               <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
	 			<tr class="tblTitulo">
   				    <td width='30' align="center"><input type="checkbox" name="todasAprobadas" id="todasAprobadas" onClick="checkearTodos (this.form,this);"></td>
                                    <td width='110' align="center"> Codigo</td>  
                                    <td width='320' align="center"> Nombre</td>
									<td width='60' align="center"> HC</td>  
                                    <td width='200' align="center"> Valor</td>    				
                                    <td width='80' align="center"> Total Fact.</td>    				
                                    <td width='60' align="center"> # Aut.</td>
    				    <td width='*' align="center"> Faltantes</td> 
						 				
    			        </tr>
	 			</table>
                  </td>
              </tr> 
                             <tr>
                             <td><div  style=" overflow:auto ; WIDTH: 100%; HEIGHT:340px; "><table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4" id="principal">
				<%Iterator it =listado.iterator();
                                int c = 0;
    			while(it.hasNext()){
    				Corrida corrida = (Corrida) it.next();
                                String Check = (corrida.getFaltantes().equals("0"))?"checked":"";
				%>
	
  				<tr class="<%=(c%2==0?"filagris":"filaazul")%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" >
        			<td  width='20' align="center"><input type="checkbox" <%=Check%>  name="ProveedorSelec" value='<%=corrida.getNitProveedor()%>'></td>	
    				<td  width='108' onClick="location.href='<%= CONTROLLER %>?estado=Corrida&accion=Search&cmd=busquedaFactura&nrocorrida=<%=corrida.getCorrida()%>&banco=<%= corrida.getBanco() %>&sucursal=<%= corrida.getSucursal()%>&nitPro=<%= corrida.getNitProveedor()%>&hc=<%=corrida.getNumerohc()%>&nombre=<%=corrida.getNombre()%>'" title="Haga clic para ver las Facturas correpondientes al proveedor..." align="center" class="bordereporte" ><%=corrida.getNitProveedor()%> </td>
    				<td  width='286' onClick="location.href='<%= CONTROLLER %>?estado=Corrida&accion=Search&cmd=busquedaFactura&nrocorrida=<%=corrida.getCorrida()%>&banco=<%= corrida.getBanco() %>&sucursal=<%= corrida.getSucursal()%>&nitPro=<%= corrida.getNitProveedor()%>&hc=<%=corrida.getNumerohc()%>&nombre=<%=corrida.getNombre()%>'" title="Haga clic para ver las Facturas correpondientes al proveedor..." align="center" class="bordereporte" ><%=corrida.getNombre()%></td>
					<td  width='52' onClick="location.href='<%= CONTROLLER %>?estado=Corrida&accion=Search&cmd=busquedaFactura&nrocorrida=<%=corrida.getCorrida()%>&banco=<%= corrida.getBanco() %>&sucursal=<%= corrida.getSucursal()%>&nitPro=<%= corrida.getNitProveedor()%>&hc=<%=corrida.getNumerohc()%>&nombre=<%=corrida.getNombre()%>'" title="Haga clic para ver las Facturas correpondientes al proveedor..." align="center" class="bordereporte" ><%= (corrida.getNumerohc()!=null) ? corrida.getNumerohc() : ""%></td>
				<td  width='179' onClick="location.href='<%= CONTROLLER %>?estado=Corrida&accion=Search&cmd=busquedaFactura&nrocorrida=<%=corrida.getCorrida()%>&banco=<%= corrida.getBanco() %>&sucursal=<%= corrida.getSucursal()%>&nitPro=<%= corrida.getNitProveedor()%>&hc=<%=corrida.getNumerohc()%>&nombre=<%=corrida.getNombre()%>'" title="Haga clic para ver las Facturas correpondientes al proveedor..." align="right"  class="bordereporte" ><%=Util.customFormat(corrida.getValor())%></td>
				<td  width='71' onClick="location.href='<%= CONTROLLER %>?estado=Corrida&accion=Search&cmd=busquedaFactura&nrocorrida=<%=corrida.getCorrida()%>&banco=<%= corrida.getBanco() %>&sucursal=<%= corrida.getSucursal()%>&nitPro=<%= corrida.getNitProveedor()%>&hc=<%=corrida.getNumerohc()%>&nombre=<%=corrida.getNombre()%>'" title="Haga clic para ver las Facturas correpondientes al proveedor..." align="center" class="bordereporte" ><%=corrida.getTotalFacturas()%></td>
				<td  width='58' onClick="location.href='<%= CONTROLLER %>?estado=Corrida&accion=Search&cmd=busquedaFactura&nrocorrida=<%=corrida.getCorrida()%>&banco=<%= corrida.getBanco() %>&sucursal=<%= corrida.getSucursal()%>&nitPro=<%= corrida.getNitProveedor()%>&hc=<%=corrida.getNumerohc()%>&nombre=<%=corrida.getNombre()%>'" title="Haga clic para ver las Facturas correpondientes al proveedor..." align="center" class="bordereporte" ><%=corrida.getAutorizadas()%></td>
				<td  width='56' onClick="location.href='<%= CONTROLLER %>?estado=Corrida&accion=Search&cmd=busquedaFactura&nrocorrida=<%=corrida.getCorrida()%>&banco=<%= corrida.getBanco() %>&sucursal=<%= corrida.getSucursal()%>&nitPro=<%= corrida.getNitProveedor()%>&hc=<%=corrida.getNumerohc()%>&nombre=<%=corrida.getNombre()%>'" title="Haga clic para ver las Facturas correpondientes al proveedor..." align="center" class="bordereporte" ><%=corrida.getFaltantes()%></td>
				</tr>
  				<% c++;
                                }//fin while%>
    			</table></div>
    		       </td>
    		    </tr>
		  </table>
		</td>
		</tr>
	</table>
	<br>
	<% if(!msg.equals("")){ visible ="hidden";}%>
	<div id='boton' style='visibility:<%=visible%>'>
	<table width="76%"  border="0" align="center">
  	<tr>
    	<td align="center">
    	           <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar"   height="21"onMouseOver="botonOver(this);" onClick="form1.submit();" onMouseOut="botonOut(this);" style="cursor:hand">  
		   <img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgregresar"  height="21"onMouseOver="botonOver(this);" onClick="location.href='<%=CONTROLLER%>?estado=Corrida&accion=Search&cmd=busqueda&nrocorrida=<%= nroc %>'" onMouseOut="botonOut(this);" style="cursor:hand"> 
		    <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" height="21" onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">
		</td>
	</tr>
	</table>
	</div>
	<%if(!msg.equals("")){%>
   <div id="msg" style="position:absolute; width:100%; height:85%; z-index:0; left: 17px; top: 251px; overflow: scroll;visibility:visible;"> 
            <table width="400" border="1" align="center"  bordercolor="#123456" cellpadding='0' cellspacing='0' >
                <tr >
                    <td>
                        <table width="103%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                            <tr>
                                <td width="290" align="center" class="mensajes"><%=msg%><br><br><img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imga" height="21" onMouseOver="botonOver(this);" onClick="msg.style.visibility = 'hidden';boton.style.visibility = 'visible';" onMouseOut="botonOut(this);" style="cursor:hand"></td>
                                <td width="38" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="68">&nbsp;</td>
                            </tr>
                           
                        </table>
                    </td>
                </tr>
  			
            </table>
    </div>	
  <%}%>
</form>
<%}else {
	msg = "No se encuentran Proveedores con Facturas SIN APROBACION en Corrida seleccionada...";
  	if(!msg.equals("")){%>
   		<table width="436" border="2" align="center">
     		<tr>
    			<td>
					<table width="103%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      					<tr>
        					<td width="274" align="center" class="mensajes"><%=msg%></td>
        				    <td width="38" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        					<td width="68">&nbsp;</td>
      					</tr>
    				</table>
				</td>
  			</tr>
		</table>
		<br>
		<center><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgregresar"  height="21"onMouseOver="botonOver(this);" onClick="window.history.back();" onMouseOut="botonOut(this);" style="cursor:hand"> 
		<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" height="21" onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand"></center>
  <%}
 }%>
</div>
<%=datos[1]%>
</body>
</html>
<script>
function checkearTodos(theForm,ele){
	if (ele.id=='todasAprobadas'){
    	for (i=0;i<theForm.length;i++){
        	if (theForm.elements[i].type=='checkbox'){
		    	theForm.elements[i].checked=theForm.todasAprobadas.checked;		
			}
		}
	}else
    	theForm.todasAprobadas.checked=true;
     	
	for (i=0;i<theForm.length;i++)
       	if (theForm.elements[i].type=='checkbox' && !theForm.elements[i].checked)
           	theForm.todasAprobadas.checked=false;
}
</script>

