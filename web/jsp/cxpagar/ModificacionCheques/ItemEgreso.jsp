<!--
- Autor : Ing. Juan Manuel Escandon Perez
- Date  : Septiembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra la informacion referente a
--              un item de egreso
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%
String cheque       = request.getParameter("cheque");
ReporteEgreso r     = model.ReporteEgresoSvc.BUSCARITEM(cheque);
Util u              = new Util();
%>
<html>
<head>
    <title>Listado</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>  
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'> 
<body>
    <p>&nbsp;</p>
    <table width="600" border="2" align="center">
        <tr>
            <td>
                <table width="99%" align="center" bgcolor="#FFFFFF">
                    <tr>
                        <td width="373" class="subtitulo1">Item egreso</td>
                        <td width="427" class="barratitulo"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                    </tr>
                </table>
                <table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
                <tr class="tblTitulo">
                    <td width="9%" align="center">Cheque</td>
                    <td width="10%"  align="center">Item</td>
                    <td width="15%"  align="center">Planilla</td>
                    <td width="19%"  align="center">Banco</td>
                    <td width="15%"  align="center">Sucursal</td>
                    <td width="18%"  align="center">Valor</td>
                    <td width="14%"  align="center">Moneda</td>
                </tr>
                <tr class="filaazul">
                    <td class="bordereporte" align="center"><%= r.getNumChk() %></td>
                    <td class="bordereporte" align="center"><%= r.getItem() %></td>
                    <td class="bordereporte" align="center"><%= r.getPlanilla() %></td>
                    <td class="bordereporte" align="center"><%= r.getBanck() %></td>
                    <td class="bordereporte" align="center"><%= r.getSucursal() %></td>
                    <td class="bordereporte" align="center"><%= u.customFormat(r.getValorC()) %></td>
                    <td class="bordereporte" align="center"><%= r.getMoneda() %></td>
                </tr
                ></table>
            </td>
        </tr>
    </table>
</body>
</html>
