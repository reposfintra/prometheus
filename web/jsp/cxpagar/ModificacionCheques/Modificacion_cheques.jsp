<!--
- Autor : Ing. Juan Manuel Escandon Perez
- Date  : Septiembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite la modificacion de la serie de cheques
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
List ListaGnral         =  model.ReporteEgresoSvc.getListRE();

String Mensaje          = (request.getParameter("Mensaje") != null) ? request.getParameter("Mensaje") : "";
String fecha            = (request.getParameter("fecha") != null) ? request.getParameter("fecha") : "";
String banco            = (request.getParameter("banco") != null) ? request.getParameter("banco") : "BANCOLOMBIA";
String list             = (request.getParameter("list") != null) ? request.getParameter("list") : "";
String fechas           = (request.getParameter("fechas") != null) ? request.getParameter("fechas") : "";
String cheques          = (request.getParameter("cheques") != null) ? request.getParameter("cheques") : "";
String sucursal 	= (request.getParameter("sucursal") != null) ? request.getParameter("sucursal") : ""; ;

TreeMap b 		= model.servicioBanco.obtenerNombresBancos();

TreeMap sbancos         = model.servicioBanco.obtenerSucursalesBanco(banco);  
Util u = new Util();

%>

<html>
    <head>
        <title>Modificacion de Cheques</title>      
        <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>  
        <link href="../css/estilostsp.css" rel='stylesheet'> 
        <<script src='<%=BASEURL%>/js/date-picker.js'></script>
	<script language="javaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
        <script>
            
            var valorRadio = "";
            
            function setRadio( value ){
                valorRadio = value;	
                if( value == 'F' ){
                    var obj     = document.getElementById("rfechas"); 
                    var objaux  = document.getElementById("rcheques"); 
                    obj.style.display = "block";
                    objaux.style.display = "none";
                }
                else{
                    var obj     = document.getElementById("rcheques"); 
                    var objaux  = document.getElementById("rfechas"); 
                    obj.style.display = "block";
                    objaux.style.display = "none";
                }
                
            }
	 
            function abrirVentanaBusq(an,al,url,pag) {
                window.open(url,'Trafico','width='+an+',height='+al+',scrollbars=yes,resizable=no,top=10,left=65');
            }

            function bancos(){       
                formulario.Opcion.value = 'BuscarS';
                document.formulario.submit(); 
            }
	
            function completar (codigo){
                var nuevo = codigo;
                if (codigo!='') for (i=codigo.length;i<6;i++) nuevo = '0' + nuevo;
                return nuevo;
            }

            function procesar (element){
                if (window.event.keyCode==13) 
                element.value = completar(element.value);
            }
	
            function realizarValidacion(form){
            //	var form = document.form1;
            var listaOriginal = new Array();
            var listaCampos = new Array();
            for(var i=0, cOriginal = 0, cCampos = 0; i<form.length; i++ ){
                var ele = form.elements[i];
                if ( ele.type == "hidden" && ele.name.charAt(0) == 'o' ){
                    listaOriginal[cOriginal] = ele.value;
                    cOriginal++;
                }
                else if ( ele.type == "text" && ele.id.charAt(0) == 'c' ){
                    if ( ele.value == "" ){
                        listaCampos[cCampos] = listaOriginal[cCampos];
                    }
                    else {
                        listaCampos[cCampos] = ele.value;
                    }
                    cCampos++;
                 }
            }
            //alert("Original: "+listaOriginal);
            //alert("Campos: "+listaCampos);
            for( var i=0; i<listaCampos.length; i++ ){
                if ( !numeroFueModifcado(i,listaCampos[i],listaOriginal,listaCampos) ){
                    alert("El numero "+listaCampos[i]+" ya existe entre los cheques, para seguir tambien debe modificar el cheque " + listaOriginal[i]);
                    return false;
                }
            }
            for( var i=0; i<listaCampos.length; i++ ){
                var aux = listaCampos[i];
                for( var j=0; j<listaCampos.length; j++ ){
                    if ( i != j && listaCampos[j] == aux ){
                        alert("El numero del cheque "+aux+" no puede ser repetido");
                        return false;
                    }
                }
            }

            form.Opcion.value = 'Actualizar';
            form.b.value = formulario.banck.value;
            form.s.value = formulario.sucuralbanco.value;
            form.submit();

            return true;
	
            }

            function numeroFueModifcado(actual,numero,listaO, listaC ){
                var indiceOriginal = -1;
                for( var i=0; i<listaO.length; i++ ){
                    if ( i != actual && listaO[i] == numero ){
                        indiceOriginal = i;
                        break;
                    }
                }
                if ( indiceOriginal == -1){
                    return true;
                }
                return listaO[indiceOriginal] != listaC[indiceOriginal];
            }
            
            function validarForm( form ){
                if( valorRadio == '' ) {
                    alert('Debe seleccionar un parametro de busqueda');
                    return false;
                }
                
                if( valorRadio == 'F' ){                
                    if( form.fechai.value == '' || form.fechaf.value == '' ){
                        alert('Debe escojer una fecha inicial y final')
                        return false;
                    }
                                                            
                }
                
                else{
                    if( form.rangoi.value == '' || form.rangof.value == '' ){
                        alert('Debe escojer un rango de cheques inicial y final')
                        return false;
                    }
                }
                return true;
            }
            
           
        </script>
    </head>
    <body <%
    if(request.getParameter("reload") != null){%>
        parent.close();parent.opener.location.href='<%= BASEURL %>/ModificacionCheques/Modificacion_cheques.jsp?';" 
  <%}%>>
  <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Modificacion de Cheques"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

        <FORM name='formulario' method='POST' action="<%= CONTROLLER %>?estado=Cambio&accion=Chk">
            <table width='70%' align='center' border='2'>
                <tr>
                    <td width="301" align="left" class="subtitulo1">Parametros de Busqueda </td>	 
                    <td width="363" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
                <tr>
                    <th colspan="2">		   
                        <table width="100%"  border="0" class="tablaInferior">
                            <tr class="fila">                                
                                <td width="10%">Banco</td>				
                                <td width="22%">&nbsp;<input:select name="banck"  attributesText=" id='banco' style='width:100%;' class='listmenu'  onChange='bancos();'"  default="<%=banco%>" options="<%=b%>"  /></td>                      
                                <td width="14%">&nbsp;Sucursal</td>
                                <td width="25%">&nbsp;<input:select name="sucuralbanco"   attributesText="style='width:100%;' class='listmenu'"  default="<%=sucursal%>" options="<%=sbancos%>" /></td>                                
                            </tr> 
                            
                            <tr>
                                <td colspan='2' align="left" class="subtitulo1">Filtros</td>	 
                                <td colspan='2' align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                            </tr>
                            
                            <tr class="fila" title='Filtros'> 
                                <td width='5%'>Rango de Fechas</td>
                                <td align="center">&nbsp;<input name="tipo" id="tipo" type="radio" value="F" onClick="setRadio(this.value);"></td>				
				<td width='5%'>&nbsp;Rango de Cheques</td>
                                <td align="center">&nbsp;<input name="tipo" id="tipo" type="radio" value="C" onClick="setRadio(this.value);"></input></td>
                            </tr>
                            
                            <tr class="fila" title='Rango de Fechas' id='rfechas' style="display:none"> 
                                <td width='5%'>Fecha Inicial</td>
                                <td align="left">&nbsp;<input type='text' id="fechai" name="fechai" readonly="true" class="textbox"></input> 														
                                    <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fechai);return false;" HIDEFOCUS>
                                    <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha">
                                    </a>
				</td>
				
				<td width='5%'>&nbsp;Fecha Final</td>
                                <td align="left">&nbsp;<input type='text' id="fechaf" name="fechaf" readonly="true" class="textbox"></input> 														
                                    <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fechaf);return false;" HIDEFOCUS>
                                    <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha">
                                    </a>
				</td>
                            </tr>
                            
                            <tr class="fila" title='Rango de Cheques' id='rcheques' style=" display:none"> 
                                <td width='5%'> Rango Inicial</td>
                                <td align="left">&nbsp;<input type='text' id="rangoi" name="rangoi"  class="textbox"></input></td>				
				<td width='5%'>&nbsp;Rango Final</td>
                                <td align="left">&nbsp;<input type='text' id="rangof" name="rangof"  class="textbox"></input></td>
                            </tr>
                            
                        </table>
                    </th>
                </tr>
                
            </table>
            <input type='hidden' name='Opcion'/> 
            <input name='fecha' type='hidden' id="fecha"/>
            <input name='b' type='hidden' id="b"/>
            <input name='s' type='hidden' id="s"/>
            <table width='672' align='center'  border='0'>
				<tr align="center">
					<td height='50' colspan="2">   
						<img src="<%=BASEURL%>/images/botones/buscar.gif" width="90" height="21" onClick="Opcion.value='GenerarChk';if( validarForm(formulario) ){formulario.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">        
						<img src="<%=BASEURL%>/images/botones/salir.gif" width="90" height="21" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">        
					</td>
				</tr>
			</table>
			</FORM>
        <script>
        </script>
        <center class='comentario'>


        <% if(!Mensaje.equals("")){ %>
        <p>
        <table border="2" align="center">
            <tr>
                <td width="404">
                    <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                        <tr>
                            <td width="277" align="center" class="mensajes"><%                                
                                if(Mensaje.substring(0,5).equals("No se")){%>
                                <a onClick="abrirVentanaBusq(550,450,'<%= BASEURL %>/Marcostsp.jsp?encabezado=Cheques registrados &dir=/jsp/cxpagar/ModificacionCheques/Listado.jsp?cheques=<%=cheques%>-_-fechas=<%=fechas%>')" style="cursor:hand ">
                                    <%= Mensaje %>
                                </a>
                                    <% }else{out.print(Mensaje);} %></td>
                            <td width="15" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                            <td width="67">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        </p>
        <% } %>
        <form action="<%= CONTROLLER %>?estado=Cambio&accion=Chk" method='post' name='FormularioListado' onsubmit='return realizarValidacion(this);'>
        <% fecha    = (request.getParameter("fecha")!=null)?request.getParameter("fecha"):""; 
	 if( ListaGnral != null && ListaGnral.size()>0 ) { %>   
            <br>
            <tr>
                <th height='28' colspan='7'>
                    <table width="576" border="2">
                        <tr>
                            <td width="564"><table width="99%"  cellspacing="1" cellpadding="0" align="center">
                        <tr>
                            <td width="50%" class="subtitulo1">&nbsp;LISTADO DE CHEQUES</td>
                            <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table bordercolor="#F7F5F4" bgcolor="#FFFFFF"border="1" width="100%">
                                    <tr class="letrafila"align="center"></tr> 
                                </table>
                            </td>
                        </table>
                        <table bgcolor="#F7F5F4" bordercolor="#999999" border="1" width="99%" align="center">
                            <tr class="tbltitulo" align="center">
                                <th width="4%" align="center">N�</th>
                                <th width="18%" align="center">CHEQUE</th>
                                <th width="21%" align="center">VALOR CHEQUE</th> 
                                <th width="30%" align="center">BENEFICIARIO</th>
                                <th width="27%" align="center">NUEVO  CHEQUE</th>						
                            </tr>
              <%
                int Cont = 1;
                Iterator it2 = ListaGnral.iterator();
                while(it2.hasNext()){
                    ReporteEgreso re = (ReporteEgreso) it2.next();                
                   %>
                            <tr class="<%= (Cont % 2 == 0 )?"filaazul":"filagris" %>" >
                                <td  align='center' class="bordereporte"><span class='comentario'><%=Cont++%></span></td>
                                <td align='center' class="bordereporte"><a onClick="abrirVentanaBusq(650,550,'<%=BASEURL%>/Marcostsp.jsp?encabezado=Item de egreso &dir=/jsp/cxpagar/ModificacionCheques/ItemEgreso.jsp?cheque=<%=re.getNumeroChk()%>')" style="cursor:hand" title='Click para ver mas informacion del cheque'><%=(re.getNumeroChk()!=null)?re.getNumeroChk():""%></a><input type='hidden' name='original'<%=Cont%> value='<%=re.getNumeroChk()%>'/> </td>
                                <td align="center" class="bordereporte"><%= u.customFormat(re.getValorChk())%></td> 
                                <td align="center" class="bordereporte"><%=(re.getBeneficiario()!=null)?re.getBeneficiario():""%></td>	
                                <td align="center" class="bordereporte"><input type="text" id='campo'<%=Cont%> name='campo<%=Cont-1%>' class="textbox" maxlength="8" onKeyUp="procesar(this);" value="<%=(re.getNuevoNChk()!=null)?re.getNuevoNChk():""%>"></td>		
                                <td align="center" class="bordereporte"><span class="letras"><a onClick="realizarValidacion(FormularioListado);"><img src="<%=BASEURL%>/images/buscar.jpg" width="17" height="19" border="0" align="top" alt="Modificar cheques" style="cursor:hand"></a></span></td>		
                            </tr>
                <%  }   %>        
                        </table>
                      </td>
                    </tr>
                </table>
            </th>
        </tr>

            <br>
            <input name='b' type='hidden' id="b"/>
            <input name='s' type='hidden' id="s"/>
            <input name='fecha' type='hidden' id="fecha"/>
            <input type='hidden' name='Opcion'/> 
        </form>
<%  }   %> 

        </center>
		</div>
		<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
    </body>
</html>
