<!--
- Autor : Ing. Juan Manuel Escandon Perez
- Date  : Septiembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra el listado de los cheques
--              que se encuentran duplicados en la tabla de egreso
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%--Declaracion de variables--%>
<%
String fechas       = request.getParameter("fechas");
String cheques      = request.getParameter("cheques");

String listado []   = cheques.split(",");
String listado2 []  = fechas.split(",");
%>
<html>
<head>
    <title>Listado</title>   
    <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'> 
    <script language="javaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
</head>
<body>
    <p>&nbsp;</p>
    <table width="300" border="2" align="center">
        <tr>
            <td>
                <table width="99%" align="center">
                    <tr>
                        <td width="373" class="subtitulo1">&nbsp;Listado General</td>
                        <td width="427" class="barratitulo"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                    </tr>
                </table>
                <table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
                    <tr class="tblTitulo">
                        <td width="10%" align="center">Cheque</td>
                        <td width="26%"  align="center">Fecha</td>
                    </tr>
                    <% for (int i = 0 ; i < listado.length ; i++){ %>
                    <tr class="<%= (i % 2 == 0 ) ? "filagris" : "filaazul" %>" >
                        <td class="bordereporte" align="center"><%= listado[i] %></td>
                        <td class="bordereporte" align="center"><%= listado2[i] %></td>
                    </tr>
                    <% } %>
                </table>
            </td>
        </tr>
    </table>
    <br>
    <table align="center">
        <tr>
        <td><img src="<%=BASEURL%>/images/botones/salir.gif" width="90" height="21" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>        
        </tr>
    </table>
</body>
</html>
