<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de mostrar un mensaje para la vereficacion de generacion
                            de Reportes de facturas a proveedores en excel
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>Error en la Busqueda</title>
<link href="<%= BASEURL %>/css/estilo.css" rel='stylesheet'>
<link href="../css/estilo.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilo.css" rel="stylesheet" type="text/css">
</head>
 <script>
        function regresar(BASEURL){
            document.forma1.action =BASEURL+"/jsp/cxpagar/facturasxpagar/reporteDXP11.jsp";
            document.forma1.submit(); 
        }
    </script>
<body>
<form name="forma1" method="post">
<p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
<div align="center"><br>
     <p align="center"><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgaceptar" onClick="regresar('<%=BASEURL%>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;</p>
</div>
</form>
</body>
</html>