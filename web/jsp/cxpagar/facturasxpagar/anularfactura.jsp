<%--
/******************************************************************************
 * Nombre clase :                   anularfactura.jsp                         *
 * Descripcion :                    Pagina para Anular las Facturas de        *
 *                                  Cuentas por Pagar.                        *
 * Autor :                          Leonardo Parody                           *
 * Fecha Creado :                   26 de diciembre de 2005                   *
 * Modificado por:                  LREALES                                   *
 * Fecha Modificado:                17 de mayo de 2006, 04:51 PM              *
 * Version :                        1.0                                       *
 * Copyright :                      Fintravalores S.A.                   *
 *****************************************************************************/
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>
<head><title>Anular Factura</title>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script>
		var controlador ="<%=CONTROLLER%>";
		
		function Buscar ( tipo_documento, c_documento, c_proveedor ){
		
			if( ( c_documento != "" ) && ( c_proveedor != "" ) ){
				
				c_documento = c_documento.replace ( "#", "-_-" );
				
				document.forma.action = controlador+"?estado=CXP_Doc&accion=Anular&flag=mostrar&c_documento="+c_documento+"&c_proveedor="+c_proveedor+"&tipo_documento="+tipo_documento;
				document.forma.submit();
				
			} else{
			
				alert( "Por Favor No Deje Los Campos Obligatorios Vacios!" );
				
			}
		}
		
		function Anular ( c_documento ){
			
			c_documento = c_documento.replace ( "#", "-_-" );
			
			document.forma1.action = controlador+"?estado=CXP_Doc&accion=Anular&flag=anular&c_documento="+c_documento;
			document.forma1.submit();
				
		}		
    </script>

<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 1px;">
<jsp:include page="/toptsp.jsp?encabezado=Facturas por Pagar"/></div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: -5px; top: 101px; overflow: scroll;">
	
<% CXP_Doc cXP_Doc = new CXP_Doc(); %>

<form name="forma" id="forma" method="post" action='<%= BASEURL %>/jsp/cxpagar/facturasxpagar/anularfactura.jsp?c_documento=""&flag='>
  <table width="30%"  border="2" align="center">
    <tr>
      <td><table width="100%"  border="0" align="center">
        <tr class="fila">
          <td colspan="2" ><table width="101%"  border="0" cellspacing="1" cellpadding="0">
              <tr>
                <td width="55%" class="subtitulo1">Anulación de Factura</td>
                <td width="45%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
              </tr>
          </table></td>
        </tr>
        <tr class="fila">
          <td width="50%" align="left">N&deg; 
              <select name="tipo_documento" id="tipo_documento">
              <option value="FAP" selected>Factura</option>
              <option value="ND">Nota Debito</option>
              <option value="NC">Nota Credito</option>
            </select>
          </td>
          <td width="50%" nowrap>
            <input name="c_documento" type='text' id="c_documento" value='' size="15" maxlength="30">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
	   </tr>
	   <tr class="fila">
          <td width="50%" align="left">Nit del Proveedor</td>
          <td width="50%" nowrap>
            <input name="c_proveedor" type='text' id="c_proveedor" value='' size="15" maxlength="15">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
	   </tr>
	</table></td>
    </tr>
  </table>
  <br>
  <center>
    <img src="<%=BASEURL%>/images/botones/buscar.gif" name="c_buscar" title="Buscar Factura" style="cursor:hand" onClick="Buscar( tipo_documento.value, c_documento.value, c_proveedor.value );" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
	<img src="<%=BASEURL%>/images/botones/cancelar.gif" name="c_cancelar" title="Resetear" style="cursor:hand" onClick="forma.reset(); forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
	<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" title="Salir al Menu" style="cursor:hand" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
  </center>
 
</form>    

 <% 
 if( request.getParameter("flag").equalsIgnoreCase("detalle") ){
        
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        String user = usuario.getLogin();
		String distrito = usuario.getDstrct();
		
		cXP_Doc = (CXP_Doc)request.getAttribute("CXP");
		
		String r = (String)session.getAttribute("r");
		
		String documento = ( cXP_Doc.getDocumento() != null )?cXP_Doc.getDocumento():"";
		String tip_doc = ( cXP_Doc.getTipo_documento() != null )?cXP_Doc.getTipo_documento():"";
		String tipo = ( model.tdocumentoService.getDescripcion( tip_doc ) != null )?model.tdocumentoService.getDescripcion( tip_doc ):"";
		String proveedor = ( cXP_Doc.getProveedor() != null )?cXP_Doc.getProveedor():"";
		String nombre_proveedor = (String)session.getAttribute("nombre");
		String tip_doc_rel = ( cXP_Doc.getTipo_documento_rel() != null )?cXP_Doc.getTipo_documento_rel():"";
		String tipo_documento_rel = ( model.tdocumentoService.getDescripcion( tip_doc_rel ) != null )?model.tdocumentoService.getDescripcion( tip_doc_rel ):"";
        String documento_rel = ( cXP_Doc.getDocumento_relacionado() != null )?cXP_Doc.getDocumento_relacionado():"";
		String fecha_documento = ( cXP_Doc.getFecha_documento() != null )?cXP_Doc.getFecha_documento():"";
		double vlr_neto = cXP_Doc.getVlr_neto();
		String get_mon = ( cXP_Doc.getMoneda() != null )?cXP_Doc.getMoneda():"";
		model.monedaService.buscarMonedaxCodigo( get_mon );
        String o_moneda = ( model.monedaService.getMoneda().getNomMoneda() != null )?model.monedaService.getMoneda().getNomMoneda():"";
        int plazo = cXP_Doc.getPlazo();
		String banco = ( cXP_Doc.getBanco() != null )?cXP_Doc.getBanco():"";
		String sbancos = ( cXP_Doc.getSucursal() != null )?cXP_Doc.getSucursal():"";
		String descripcion = ( cXP_Doc.getDescripcion() != null )?cXP_Doc.getDescripcion():"";
		String observacion = ( cXP_Doc.getObservacion() != null )?cXP_Doc.getObservacion():"";  
 %>
<form name="forma1" action='<%= BASEURL %>/jsp/cxpagar/facturasxpagar/anularfactura.jsp?c_documento=""&flag=' method="post">
<table width="80%" border="2" align="center" id="tabla1" >
    <tr>
        <td width="" >
           <table width="100%" height="50%" align="center">
              <tr>
                <td class="subtitulo1">Informaci&oacute;n de la Factura # <%=documento%> .</td>
                <td width="30%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
          </table>
         <div class="scroll" style=" overflow:auto ; WIDTH: 100%; HEIGHT:100%; " >
         <table id="tabla3" width="100%">
            <tr id="titulos">
              <td width="20%" align="left"  nowrap class="fila" abbr="">Proveedor:</td>
              <td width="30%" align="left"  nowrap class="letra" abbr=""><%=nombre_proveedor%></td>
              <td align="left"  nowrap class="fila" abbr="">Nit:</td>
              <td width="30%" align="left"  nowrap class="letra" abbr=""><%=proveedor%></td>
              </tr>
            <tr id="titulos">
              <td width="20%" align="left"  nowrap class="fila" abbr="">Tipo Documento:</td>
              <td align="left"  nowrap class="letra" abbr=""><%=tipo%></td>
              <td width="20%" align="left"  nowrap class="fila" abbr="">Documento:</td>
              <td align="left"  nowrap class="letra" abbr=""><%=documento%></td>
              </tr>
            <tr id="titulos">
              <td align="left"  nowrap class="fila" abbr="">Tipo Documento Relacional:</td>
              <td align="left"  nowrap class="letra" abbr=""><%=tipo_documento_rel%></td>
              <td align="left"  nowrap class="fila" abbr="">Documento Relacional:</td>
              <td align="left"  nowrap class="letra" abbr=""><%=documento_rel%></td>
            </tr>
            <tr id="titulos">
              <td align="left"  nowrap class="fila" abbr="">Fecha:</td>
              <td align="left"  nowrap class="letra" abbr=""><%=fecha_documento%></td>
              <td align="left"  nowrap class="fila" abbr="">Valor:</td>
              <td align="left"  nowrap class="letra" abbr=""><%= com.tsp.util.UtilFinanzas.customFormat2( vlr_neto )%></td>
            </tr>
            <tr id="titulos">
              <td align="left"  nowrap class="fila" abbr="">Banco:</td>
              <td align="left"  nowrap class="letra" abbr=""><%=banco%></td>
              <td align="left"  nowrap class="fila" abbr="">Plazo:</td>
              <td align="left"  nowrap class="letra" abbr=""><%=plazo%></td>
              </tr>
            <tr id="titulos">
              <td align="left"  nowrap class="fila" abbr="">Descripci&oacute;n:</td>
              <td colspan="3" align="left"  nowrap class="letra" abbr=""><%=descripcion%></td>
            </tr>
            <tr id="titulos">
              <td align="left"  nowrap class="fila" abbr="">Observaciones:</td>
              <td colspan="3" align="left"  nowrap class="letra" abbr=""><%=observacion%></td>
            </tr>        
          </table> 
		  </div>
        </td>
    </tr>
	
 </table> 

<br>
<center>
  <img align="middle" src="<%=BASEURL%>/images/botones/anular.gif" name="anular" title="Anular Factura" style="cursor:hand" onClick="window.location ='<%=CONTROLLER%>?estado=CXP_Doc&accion=Anular&flag=anular&c_documento=<%=documento%>&r=<%=r%>&c_proveedor=<%=proveedor%>&tipo_documento=<%=tip_doc%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
</center>
 <%}%>
</form>
</div>
<%=datos[1]%>
</body>
