<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar el formulario para la 
                            insercion de documentos por pagar
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head><title>Documentos por pagar</title>
<link href="../../../css/estilostspFactura.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostspFactura.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/cxp.js"></script>

<style type="text/css">
<!--
.Estilo1 {color: #D0E8E8}
-->
</style>
</head>

<% String ErrorCuenta       =   (request.getParameter ("Error")==null?"":request.getParameter ("Error"));
   String iva =""; 
   String Modificar          =  (request.getParameter ("Modificar")==null?"":request.getParameter ("Modificar"));
     
   String maxfila            =  (request.getParameter ("maxfila")==null?"1":request.getParameter ("maxfila")); 
   int    MaxFi              =  Integer.parseInt(maxfila); 
   
   String id_agencia_usuario =  (String)session.getAttribute("id_agencia" );  
   String validar_agencia    =  (request.getParameter ("ag")==null?"":request.getParameter ("ag"));  
   
   String cod_agencia_cont   =  (String)session.getAttribute("agContable");
   String unidad_contable    =  (String)session.getAttribute("unidadC");
     
   String codigocliente      =  (request.getParameter ("cliente")==null?"":request.getParameter ("cliente")); 
   String opcion 		     =  (request.getParameter ("op")==null?"NO":request.getParameter ("op")); 
   String indice		     =  (request.getParameter ("indice")==null?"":request.getParameter ("indice")); 
   String cod_cuenta		 =  (request.getParameter ("cuenta")==null?"":request.getParameter ("cuenta")); 
    
%>
<body  <%if (opcion.equals("cargar")){%> onLoad="LoadCodigo('<%=indice%>');cargar('<%=validar_agencia%>');" <%}else{%>onResize="redimensionar()" <%if(request.getParameter("reload")!=null){%>
        onLoad = "redimensionar();parent.opener.location.href='<%=BASEURL%>/jsp/cxpagar/facturasxpagar/facturaP.jsp?';parent.close();" 
  <%} else {%>
       onLoad = "redimensionar();cargar('<%=validar_agencia%>');";
  <%}
  }%>
>

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Documentos por Pagar"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%


	java.util.Date utilDate = new java.util.Date(); //fecha actual
    long lnMilisegundos = utilDate.getTime();
    java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(lnMilisegundos);
	
    CXP_Doc doc;
    doc = model.cxpDocService.getFactura();
						
    String subtotal=(request.getParameter ("total")==null?"":request.getParameter ("total"));
    String tipo_documento="";
    String documento= ""; 
    String proveedor="";
    String tipo_documento_rel ="";
    String documento_relacionado="";
    String fecha_documento=(""+sqlTimestamp).substring(0,10);
    String c_banco="BANCOLOMBIA";
    String b_sucursal="";
    String descripcion="";
    String observacion="";
    String o_moneda="PES";
    double vlr_neto=0;
	double vlr_total=0;
    String usuario_aprobacion="";
    int plazo =0;
	String moneda_banco = "";
	String CabIva  = "";
	String CabRiva = "";
	String CabRica = "";
	String CabRfte = ""; 
	
	Usuario usuarioLogin  = (Usuario) session.getAttribute("Usuario");
    String us  = usuarioLogin.getLogin();
    String agencia = usuarioLogin.getCia();
    String handle_code="";
    String id_mims="";
    String fecha_aprobacion="";

    String op= (request.getParameter("op")!=null )?request.getParameter("op"):"";
    String para1 = "/jsp/cxpagar/facturasxpagar/buscarnit.jsp?fechadoc="+fecha_documento+"&plazo="+plazo;
    para1=Util.LLamarVentana(para1, "Buscar Proveedor");

    String nombre_proveedor="";
	String agenteRet = "N";
	String rica      = "N";
	String rfte      = "N";
	String riva      = "N";
	
	
    if(doc!=null){
		
        tipo_documento=""+doc.getTipo_documento();
        documento= ( doc.getDocumento() != null )?doc.getDocumento():"";
        proveedor = ""+doc.getProveedor();
	    Proveedor prov  = model.proveedorService.obtenerProveedorPorNit(proveedor);
        if( prov!=null ){
		    agenteRet = prov.getC_agente_retenedor();
			rica      = prov.getC_autoretenedor_ica();
            rfte      = prov.getC_autoretenedor_rfte();
            riva      = prov.getC_autoretenedor_iva(); 
	    }
		
	    tipo_documento_rel =""+doc.getTipo_documento_rel();
        documento_relacionado=""+doc.getDocumento_relacionado();
        fecha_documento = (doc.getFecha_documento() != null )?doc.getFecha_documento() :"";
        c_banco = doc.getBanco();
		model.servicioBanco.loadSusursales(c_banco);
        b_sucursal  = doc.getSucursal();
        descripcion =  ( doc.getDescripcion() != null )?doc.getDescripcion():"";
        observacion =   ( doc.getObservacion() != null)?doc.getObservacion() :"";
        o_moneda    =  ( doc.getMoneda() != null )?doc.getMoneda():"PES";
        vlr_neto = doc.getVlr_total();
		vlr_total = doc.getVlr_neto();
		//out.println("<br><br><br><br><br><br> "+vlr_neto);
        plazo=doc.getPlazo();
		moneda_banco = (doc.getMoneda_banco() != null)?doc.getMoneda_banco():"";
		
        usuario_aprobacion=doc.getUsuario_aprobacion();
		CabIva  = (doc.getIva()!= null )?doc.getIva():"";
	    CabRiva = (doc.getRiva()!= null )?doc.getRiva():"";
	    CabRica = (doc.getRica()!= null )?doc.getRica():"";
	    CabRfte = (doc.getRfte()!= null )?doc.getRfte():"";
		
    }
 
   if (proveedor.equals("null")||proveedor.equals("")){
        proveedor ="";
    }
    else{
        Proveedor o_proveedor = model.proveedorService.obtenerProveedorPorNit(proveedor);
        if( o_proveedor!=null ){
	    agenteRet = o_proveedor.getC_agente_retenedor();
	    rica      = o_proveedor.getC_autoretenedor_ica();
        rfte      = o_proveedor.getC_autoretenedor_rfte();
        riva      = o_proveedor.getC_autoretenedor_iva();
       
            id_mims=""+o_proveedor.getC_idMims();

            if(!op.equals("cargarB")){
			    c_banco=""+o_proveedor.getC_branch_code();
                b_sucursal=""+o_proveedor.getC_bank_account();
				moneda_banco = o_proveedor.getCurrency(); 
							
            }
            nombre_proveedor = o_proveedor.getC_payment_name(); 
			System.out.println("NOMBRE PROVEEDOR   "+nombre_proveedor);
        }
    }
    model.cxpDocService.setFactura(null);
  
    
    
    TreeMap codCliAre = model.cxpDocService.llenarCodCliAre ();
    TreeMap b = model.servicioBanco.obtenerNombresBancosPorAgencia("BQ");
    //TreeMap sbancos = model.servicioBanco.getSucursal();
	Vector sbancso      = model.servicioBanco.getVecBancos(); 
    TreeMap t_moneda = model.monedaService.obtenerMonedas();
    TreeMap documentos = model.documentoSvc.obtenerDocumentosT();
   // TreeMap u = model.usuarioService.getUsuariosAprobacionT();
    TreeMap u = model.tblgensvc.getUsuarios_aut();
    Vector vTiposImpuestos=model.TimpuestoSvc.vTiposImpuestos();
	
	TreeMap agencias = model.agenciaService.getAgencia();
	
    int num_items=1;
   // num_items=Integer.parseInt(""+request.getParameter("num_items"));
    Vector vItems = model.cxpItemDocService.getVecCxpItemsDoc();
    String concepto ="";
	String descripcion_i="";
	//Ivan 26 julio 2006
	String ree = "";
	//////////////////////
    String codigo_cuenta="";
    String codigo_abc="";
    String planilla="";
	String tipcliarea="";
	String codcliarea="";
	String descliarea="";
    Vector vImpuestosI;
	Vector Copia;
	String [] codigos;
    double valor   = 0;
	double valor_t = 0;
	//Ivan 21 julio 2006
	LinkedList tbltipo = null;
	String auxiliar    ="";
	String tipoSubledger="";
	//////////////////////////////////
    if (vItems == null){  	     
        System.out.println("el Vector de items es nulo ");
    }
    else{
        System.out.println("el Vector de items no es nullo "+ vItems.size()+" Max Fila "+MaxFi);
        for(int i=0;i < MaxFi ;i++){
            CXPItemDoc item = (CXPItemDoc)vItems.elementAt(i);
			System.out.println("ITEM - "+ item.getConcepto());
			if(item.getConcepto() != null ){
				concepto        = ""+item.getConcepto();
				descripcion_i   = ""+ item.getDescripcion();
				ree             = ""+ item.getRee(); 
				codigo_cuenta   = ""+ item.getCodigo_cuenta();
				codigo_abc      = ""+ item.getCodigo_abc();
				planilla        = ""+ item.getPlanilla();
				valor	        = item.getVlr_me();			
				valor_t         = item.getVlr_total();
				tipcliarea      = ""+item.getTipcliarea ();
				codcliarea      = ""+item.getCodcliarea ();
				descliarea      = ""+item.getDescliarea ();
				vImpuestosI     = item.getVItems();
				iva             = item.getIva();
				codigos         = item.getCodigos();
				
				//ivan 21 julio 2006
				auxiliar        = item.getAuxiliar();     
				tbltipo         = item.getTipo();
				tipoSubledger   = item.getTipoSubledger();
				
				if (vImpuestosI == null){
					out.print("<br> el vector de Impuestos es null ");
				}
				
				for(int ii=0;ii<vImpuestosI.size();ii++){
					CXPImpItem impuestoItem = (CXPImpItem)vImpuestosI.elementAt(ii);
					String impuesto = impuestoItem.getCod_impuesto();
				}
				
			}
			
        }
		   
    }
	System.out.println("vector facturas");
	Vector NumeroFacturas =  model.cxpDocService.getVFacturas ();
	String estilo ="";
	String clase = "textboxFactura";
	String readonly="";
	String eventoProveedor ="buscarNit('"+validar_agencia +"',maxfila,'ENTER')";
	String eventoLupaProveedor = "buscarProveedores('"+CONTROLLER+"', '"+validar_agencia+"',maxfila);";
	System.out.println("If de modificar");
 if(Modificar.equals("si")){
     estilo ="style='border:0'";
     clase="filaresaltada";
     readonly ="readonly" ;
	 eventoProveedor ="";
	 eventoLupaProveedor="";
 }
System.out.println("Fin Jsp");
%>
<script language="javascript" src="<%= BASEURL %>/js/validarDocPorPagar.js"></script>
<script>
var controlador ="<%=CONTROLLER%>";
var maxfila = <%=maxfila%>

    function insertarItem(validar,BASEURL,Cod_Unid,tipcliarea,codCliAre,Modificar){
		var moneda = document.getElementById("moneda");
		var ii=0;
		var suma=0;
         
		var sw=false;
		
		//alert('FILAS ' + maxfila )
        for (var i=1;i<= maxfila;i++){
             //   alert("I - "+ i)
				var subtotal= document.getElementById("valorNeto"+i);
				
				if(subtotal!= null){
					
					//alert(subtotal.value)
					if(subtotal.value!=""){
							sw=true;
					}else{
					alert("Debe digitar un Valor para el Item");
					return;
					}
					
					asignarImpuesto()	
				}
        }
		//alert("ITEM" + ii);

		
		if(sw==true){
		  //  alert(sw);
		    maxfila++;
		    ii = maxfila;
            //forma1.imgini.style.display="block";
			var numfila = detalle.rows.length ;
			var fila = detalle.insertRow(numfila);
			//alert("FILA" + ii)
			
			  
			fila.id = "filaItem"+ii;  
		    var celda = fila.insertCell();   
			
			//fila.id='fila'+ii;
			//fila.className="fila";
			
		    var tds ="<span><a id='n_i"+ii+"' >"+ii+"</a>";
			tds+="      <input name='cod_item"+ii+"' id='cod_item"+ii+"' value='"+ii+"' type='hidden' size='4' maxlength='5' border='0'>";			        
			tds+="      <a onClick=\"insertarItem('"+validar+"','"+BASEURL+"','"+Cod_Unid+"','"+tipcliarea+"','"+codCliAre+"','"+Modificar+"')\" id='insert_item"+ii+"' style='cursor:hand' ><img src='"+BASEURL+"/images/botones/iconos/mas.gif'   width='12' height='12' ></a>";
			tds+="      <a onClick=\"borrarItem('filaItem"+ii+"')\"  value='1' id='borrarI"+ii+"' style='cursor:hand' ><img src='"+BASEURL+"/images/botones/iconos/menos1.gif'   width='12' height='12' > </a></span>";
			celda.innerHTML = tds;
			//celda.innerHTML = "<p>Alejo</p>";
			
			//celda.colSpan ='7';
			celda = fila.insertCell();
            var iva ="";
			var ItemAnterior =""; 
			for(var i=1;i <= ii; i++){
			   var aux = document.getElementById("iva"+(i)); 
			   if(aux != null){
			      iva = document.getElementById("iva"+(i));
				  ItemAnterior = i;
			   }
			}
			
			
			tds = "";
			tds +="<span><table width='100%' border='0' cellpadding='0' cellspacing='0'>";
			tds +="     <tr align='center'>";
			tds +="        <td width='333'><span><textarea name='desc"+ii+"' id='desc"+ii+"' cols='55' rows='1' class='textareaFactura' ></textarea></span></td>";
			tds +="        <td width='201'><input name='descripcion_i"+ii+"' id='descripcion_i"+ii+"'  class='textareaFactura' type='text' size='30'  maxlength='4' onKeyPress=\"soloDigitos(event, 'decNO')\" onBlur=\"BuscarConcepto('"+ii+"',event, 'decNO')\" >  <img src='"+BASEURL+"/images/botones/iconos/lupa.gif'  name='arbol"+ii+"' width='15' height='15'  id='arbol"+ii+"' style='cursor:hand' title='Buscar'  onClick=\"servicios('"+ii+"','"+controlador+"')\" ></td>";
			tds +="        <td width='35'><input name='REE"+ii+"' style='text-transform:uppercase; width:20;' class='textboxFactura' id='REE"+ii+"' type='text'  maxlength='1'  onKeyUp=\"Ree('"+ii+"');\"></td> ";
			tds +="        <td width='108'><input name='codigo_cuenta"+ii+"' id='codigo_cuenta"+ii+"'   class='textareaFactura' type='text' size='13' maxlength='20' onChange=\"VerificarCuenta('"+ii+"');\" >";      
			tds +="           <input name='cod_cuenta"+ii+"' id='cod_cuenta"+ii+"' type='hidden' size='4' maxlength='5' border='0' value='"+Cod_Unid+"'> ";
			tds +="           <input type='hidden' name='cod1"+ii+"' id='cod1"+ii+"' value='-'>";
			tds +="  	      <input type='hidden' name='cod2"+ii+"' id='cod2"+ii+"' value='--'>";
			tds +="           <input type='hidden' name='cod3"+ii+"' id='cod3"+ii+"' value='---'>";
			tds +="           <input type='hidden' name='cod4"+ii+"' id='cod4"+ii+"' value='---'>";
			tds +="           <input type='hidden' name='cod5"+ii+"' id='cod5"+ii+"' value='---'> ";
			tds +="          </td>";
			
			tds +="    <td  width='50' align='right' id='combotipo"+ii+"'><select name='tipo"+ii+"' style='width:45'   > <option value=''> </option></select></td>";
			tds +="	   <td align='center'>";
            tds +="       <input type='text' maxlength='25'  style='width:90' name='auxiliar"+ii+"' id='auxiliar"+ii+"' onBlur=\"validarAuxiliar('"+BASEURL+"','"+ii+"');\">";
            tds +="       <img src='"+BASEURL+"/images/botones/iconos/lupa.gif'  name='imagenAux"+ii+"' width='15' height='15'  id='imagenAux"+ii+"' style='cursor:hand'  onClick=\"buscarAuxiliar('"+BASEURL+"','"+ii+"',forma1.codigo_cuenta"+ii+".value, forma1.tipo"+ii+".value);\"  >";
			tds +="	  </td>  ";
			
			tds +="        <td width='75'><input name='codigo_abc"+ii+"' style='text-transform:uppercase'  id='codigo_abc"+ii+"' type='text'  class='textareaFactura' size='4' maxlength='2'></td>";
			
			tds +="     </tr> ";
			tds +="   </table>";
			tds +="<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
			tds +="   <tr align='center'>";
			tds +="     <td width='20'>&nbsp;</td>";
			tds +="        <td width='60'><input name='planilla"+ii+"' id='planilla"+ii+"' type='text'  class='textareaFactura' size='8' maxlength='8' onKeyUp=\"disabledPlanilla('"+ii+"','"+validar+"','"+Modificar+"')\"></td>";
			tds +="        <td width='65'><select name='cod_oc"+ii+"' id='cod_oc"+ii+"' style='width:80%;'   onClick=\" buscarCodigoOC('"+ii+"','"+BASEURL+"')\" ><option value='C' selected>C</option></select></td>    ";
			tds +="        <td width='60'><input name='oc"+ii+"' type='text' id='oc"+ii+"' size='6' maxlength='8'  class='textareaFactura'> <input name='doc"+ii+"' id='doc"+ii+"' type='hidden'></td>";
			if(moneda.value =='DOL')
			   tds +="     <td width='239'><input name='valor1"+ii+"' id='valor1"+ii+"'  class='textareaFactura' type='text' size='16' style='text-align:right;' onfocus='this.select()' onBlur='asignarImpuesto("+ii+");' onChange='formatoDolar(this,2);' onKeyPress=\"soloDigitos(event, 'decOK')\"  maxlength='11'  align='right'><input type='hidden' name='iva"+ii+"'  id='iva"+ii+"' value="+iva.value+"></td>";
			else
			   tds +="     <td width='239'><input name='valor1"+ii+"' id='valor1"+ii+"'  class='textareaFactura' type='text' size='16' style='text-align:right;' onfocus='this.select()' onBlur='asignarImpuesto("+ii+");' onChange='formatear(this);' onKeyPress=\"soloDigitos(event, 'decNO')\"  maxlength='11'  align='right'><input type='hidden' name='iva"+ii+"'  id='iva"+ii+"' value="+iva.value+"></td>";
			
			<%  
		    for (int l=0;l<vTiposImpuestos.size();l++){
		    %>
			var impuesto = document.getElementById("impuesto<%=l%>"+(ItemAnterior)); 
			
			tds +="    <td width='97' nowrap><input name='impuesto<%=l%>"+ii+"' id ='impuesto<%=l%>"+ii+"'  class='textareaFactura' style='text-transform:uppercase' value='"+impuesto.value+"' type='text' size='6' maxlength='10' onKeyUp=\"enter('<%=vTiposImpuestos.elementAt(l)%>','<%=l%>"+ii+"','"+BASEURL+"')\" onBlur=\"<%if(!vTiposImpuestos.elementAt(l).equals("RIVA")){%>enterFocus('<%=vTiposImpuestos.elementAt(l)%>','<%=l%>"+ii+"','"+BASEURL+"');<%}%><%if(vTiposImpuestos.elementAt(l).equals("RFTE")){%>insertarItem('"+validar+"','"+BASEURL+"','"+Cod_Unid+"','"+tipcliarea+"','"+codCliAre+"','"+Modificar+"')<%}%>\" ";
		   
		   <% if(!agenteRet.equals("S")&& l!= 0){  
				if((vTiposImpuestos.elementAt(l).equals("RICA")) && (!rica.equals("S")) ){%>
						tds+=" readonly";
					<%}else if((vTiposImpuestos.elementAt(l).equals("RFTE")) && (!rfte.equals("S")) ){%>
						 tds+=" readonly";
					<%}else if(vTiposImpuestos.elementAt(l).equals("RIVA")) {%>
						 tds+=" readonly";
					 <%}%>		
				<%}%>   
				   
		    tds +="	> ";
			<%if(!vTiposImpuestos.elementAt(l).equals("RIVA")){%>
			tds +="      <img src='"+BASEURL+"/images/botones/iconos/lupa.gif'  name='<%=l%>"+ii+"' width='15' height='15'  id='imageB_<%=l%>_"+ii+"' style='cursor:hand' title='Buscar' ";  
			<%if((!agenteRet.equals("S"))||(l==0)){
					if((vTiposImpuestos.elementAt(l).equals("RICA")) && (rica.equals("S")) ){%>
						tds+=" onClick=\"funI('<%= vTiposImpuestos.elementAt(l)%>',this.name,'"+BASEURL+"');\" ";
				  <%}else if((vTiposImpuestos.elementAt(l).equals("RFTE")) && (rfte.equals("S")) ){%>
						tds+=" onClick=\"funI('<%= vTiposImpuestos.elementAt(l)%>',this.name,'"+BASEURL+"');\" ";
				  <%}else if(vTiposImpuestos.elementAt(l).equals("IVA")){%>
						 tds+=" onClick=\"funI('<%= vTiposImpuestos.elementAt(l)%>',this.name,'"+BASEURL+"');\" ";
				  <%}		
              }%>
			
			tds+=" > ";
			<%}%>
			
			tds +=" <input name='tipo_impuesto<%=l%>"+ii+"' id ='tipo_impuesto<%=l%>"+ii+"' type='hidden' size='10' value='<%=vTiposImpuestos.elementAt(l)%>'></td>";
				  
				  
			<%}%> 
			
			if(moneda.value =='DOL')
			   tds +="     <td width='143'><input name='valorNeto"+ii+"' id='valorNeto"+ii+"'  class='textareaFactura' type='text' size='16'  style='text-align:right;' onChange='formatoDolar(this,2);' onKeyPress=\"soloDigitos(event, 'decOK')\"  maxlength='11' align='right' readonly></td>";
			else                      
			   tds +="     <td width='143'><input name='valorNeto"+ii+"' id='valorNeto"+ii+"'  class='textareaFactura' type='text' size='16'  style='text-align:right;' onChange='formatear(this);' onKeyPress=\"soloDigitos(event, 'decNO')\"  maxlength='11' align='right' readonly></td>";
			
			
			tds +="</tr></table>";       
			
			
			 
			
//			alert (tds);
			celda.innerHTML = tds;
			organizar();
			
			}
			else{
			  alert("Debe digitar un Valor para el Item");
			}		
	}
	
	
	
	function borrarItem(indice){
	       // alert("filas = "+detalle.rows.length+", indice = "+indice);	
//			indice = detalle.rows.length;
          
            var tabla = document.getElementById("detalle");
			//alert("indice= "+indice);
			if ( tabla.rows.length <= 2 ){
				return;
			}
			
			var fila = document.getElementById(indice);
			//alert(fila);
		    tabla.deleteRow(fila.rowIndex);				
				
			
			asignarImpuesto()			
			organizar();

			
			/*
			var vlr_total = 0;
		    var idItem = indice;
            with (forma1){
              for (i=0; i<elements.length; i++){                  
				  if (elements[i].name.indexOf('valor1')==0){
                     var valor = elements[i].value.replace( new RegExp(",","g"), "");
                     if (!isNaN ( parseFloat(valor)) ){
                        vlr_total += parseFloat(valor);						
                     }					 
                  }
              }
          }		
		forma1.vlr_neto.value =  formato(vlr_total);*/
	}
	
	function organizar(){
 	    var cont =1;
		for (var i = 1,n = 1;i <= maxfila + 1 ; i++){
			var x = document.getElementById("n_i"+i);
			var y = document.getElementById("filaItem"+i);
			if ( x != null ){
				x.innerText = ""+n;
				n++;
			}
			if( y != null ){
				if( cont % 2 == 0){
				   y.className = "filagrisFac";
				}else{
				   y.className = "filaFactura";
				}
				cont++;
			}
		}
	}
	
		
	
	 function formato(numero){
           
           var tmp = parseInt(numero) ;
           var factor = (tmp < 0 ? - 1 : 1);
           tmp *= factor;
          
           var num = '';
           var pos = 0;
           while(tmp>0){
                  if (pos%3==0 && pos!=0) num = ',' + num;
                  res  = tmp % 10;
                  tmp  = parseInt(tmp / 10);
                  num  = res + num  ;
                  pos++;
           }
           return (factor==-1 ? '-' : '' ) + num ;
        }      
        
        function sinformato(element){
           return element.replace( new RegExp(",","g") ,'');
        }
		
		function disabledPlanilla(x, validar,Modificar ){
		  var comboPlanilla = document.getElementById('cod_oc'+x);
		  var planilla      = document.getElementById('planilla'+x);
		  var descripcion   = document.getElementById('descripcion_i'+x);
		  if( planilla.value.length != 0 )
		  	comboPlanilla.disabled = true;		
		  else  
		    comboPlanilla.disabled = false;		
			
			if(planilla.value != ""){
			    if( descripcion.value.length != 0 ){
					BuscarAccountCode( planilla.value, x, validar,Modificar );
				}
				else{
					alert('Debe seleccionar un concepto de pago');
				}
			}
			
		}
		
		function BuscarAccountCode( numpla, x , validar, Modificar ){
			var cod1   =   document.getElementById("cod1"+x);
	        var cod2   =   document.getElementById("cod2"+x);	
			var cod3   =   document.getElementById("cod3"+x);	
			var cod4   =   document.getElementById("cod4"+x);	
			var cod5   =   document.getElementById("cod5"+x);	
		    var hidden =   document.getElementById("cod_cuenta"+x);							
			var cuenta =   cod1.value + cod2.value+ cod3.value + cod4.value + cod5.value;
			document.forma1.action = controlador+"?estado=FacturaSearch&accion=CodigoCuenta&numpla="+numpla+"&indice="+x+"&hidden="+hidden.value+"&cuenta="+cuenta+"&validar="+validar+"&maxfila="+maxfila+"&Modificar="+Modificar; 
			document.forma1.submit(); 
		} 
	
	 function buscarNit (validar,maxfila,OP){
	    
		if (window.event.keyCode==13){ 
			var ii=0;
			for (var i=0;i< detalle.rows.length;i++){
				ii=ii+1;
			}
			var num_items = document.getElementById("num_items");
			num_items.value=""+(ii-1);
			var url="";
			url = controlador+"?estado=Factura&accion=AProveedores&validar="+validar+"&maxfila="+maxfila+"&OP="+OP;
			for (var x=0;x<document.forma1.length;x++){
				url =  url +"&"+ document.forma1.elements[x].name + "=" + document.forma1.elements[x].value;
			}
			
			document.forma1.action = url; 
			document.forma1.submit();
		}
    }
	
	//Ivan gomez 22 julio 2006
	/*var isIE = document.all?true:false;
    var isNS = document.layers?true:false;*/
	function BuscarConcepto(x,value,e,decReq){
		var concepto = document.getElementById("descripcion_i"+x);
		
			if(concepto.value.length == 4){
			  
		      var url = "?estado=Factura&accion=Servicios&concepto="+concepto.value+"&OP=ENTER&Id="+x;
		      enviar(url)
			 
			}
			
		
		//ivan 22 julio 2006
		/*soloDigitos(e,decReq);*/
		
		
	}
	
	function asignarImpuesto(){
	    
	    var suma = 0;
		var moneda = document.getElementById("moneda"); 
			st = document.getElementById("total");
			//alert(detalle.rows.length);	
			for (var i = 1;i <= maxfila + 1 ; i++){
                var vlrNeto  = document.getElementById("valorNeto"+i);  
				var subtotal = document.getElementById("valor1"+i);
				var imp      = document.getElementById("iva"+i);
 		         
				if ( subtotal != null ){
				    var valor = subtotal.value.replace( new RegExp(",","g"), "");
					var vlr   = "";
					var vlrImpuesto ="";
					
					if (!isNaN ( parseFloat(valor)) ){
					var vnet = parseFloat(valor);
					    if(imp.value !="" ){
						   
                            vlr = imp.value;
							//alert("IMP  "+vlr)
							vlr = parseInt(vlr);
							vlrImpuesto =  ( valor * vlr )/100;
							
							vlrImpuesto = (moneda.value == 'DOL')?redondear(vlrImpuesto):Math.round(vlrImpuesto);
							vnet += parseFloat(vlrImpuesto);
                            suma += parseFloat(vlrImpuesto);
							//alert("SUMA "+suma)
						}
						//Ivan Dario Gomez 24 julio 2006
						if(moneda.value =='DOL'){
						   vlrNeto.value =  vnet ;
						   formatoDolar(vlrNeto,2);
						}else{
					      vlrNeto.value = formato(vnet);
						}
						//////////////////////////////////////////
						suma += parseFloat(valor);
					}					  
				}
			}
			
			if(moneda.value =='DOL'){
			   st.value =  suma;
			   formatoDolar(st,2);
			}else{
			   st.value = formato(suma);
			}
	}
	
  /*function asignarImpuesto(i){
      var campo    = document.getElementById("impuesto0"+i);//este es el campo iva
	  var ValorIva = document.getElementById("iva"+i); 
	  if(campo.value!="" && ValorIva.value!="" ){
		  var ValorIva = document.getElementById("iva"+i); 
		  //valor del item
		  var valor    = document.getElementById("valor1"+i);
		  //valor neto del item
		  var vlrN     = document.getElementById("valorNeto"+i);
		  var vlr      = parseFloat(ValorIva.value.replace( new RegExp(",","g"), ""));
		  
		  //valor total item 
		  var vlrTotal = parseFloat(valor.value.replace( new RegExp(",","g"), ""));
		  var vlrNeto  = parseFloat(vlrN.value.replace( new RegExp(",","g"), ""));
		  var nums =  new String (( vlrTotal * vlr )/100) ;
 		  var TieneDec = nums.indexOf('.');
		  if( TieneDec !=-1  ){
		     var decimales = nums.split('.');
		     var dec       = decimales[1].charAt(0)
		     if(dec > 5){
			    nums =  (parseInt(decimales[0]) + 1);
		     } 
		  }
		
		  vlrN.value   = formato(vlrTotal);
		  
		  
		
		  
		  
		  var form = document.getElementById("forma1");
		  var vlr_total = 0;
		  
		  for (j=0; j<form.elements.length; j++){                  
			if (form.elements[j].name.indexOf('valorNeto')==0){
				var valor = form.elements[j].value.replace( new RegExp(",","g"), "");
					if (!isNaN ( parseFloat(valor)) ){
						vlr_total += parseFloat(valor);
					 }					 
			 }
		  }
		  nums = parseInt(nums)
		  form.total.value =  formato(vlr_total+nums);
	  }

  }*/
	
function cambiar(dato){
  var vlr_neto =  document.getElementById("vlr_neto");
  var vlNet = vlr_neto.value.replace( new RegExp(",","g"), "");
  if(dato == 'DOL'){
    
     vlr_neto.onkeypress = new Function (" soloDigitos(event, 'decOK'); ");  
	 vlr_neto.value     = Fdolar(vlNet,2);
	 vlr_neto.onchange = new Function (" formatoDolar(this,2); ");  
	 
  }else{
     vlr_neto.onkeypress = new Function (" soloDigitos(event, 'decNO'); ");  
	 vlr_neto.value     = formato(Math.round(vlNet));
	 vlr_neto.onchange = new Function (" formatear(this); ");  
  }
   
   for (var i = 1;i <= maxfila + 1 ; i++){ 
	   
		  valor  = document.getElementById("valor1"+i);
		  valorNeto = document.getElementById("valorNeto"+i);
		  if (valor != null){
 		     var val     = valor.value.replace( new RegExp(",","g"), "");
			 var valNeto = valorNeto.value.replace( new RegExp(",","g"), "");
		     if(dato == 'DOL'){  
				 valor.onkeypress = new Function (" soloDigitos(event, 'decOK'); ");  
				 valorNeto.onkeypress = new Function (" soloDigitos(event, 'decOK'); ");
				 
				 
				 valor.value     = Fdolar(val,2);
				 valorNeto.value = Fdolar(valNeto,2)
				 
				 
				 valor.onchange = new Function (" formatoDolar(this,2); ");  
				 valorNeto.onchange = new Function (" formatoDolar(this,2); ");   
				 
			 }else{
			     valor.onkeypress = new Function (" soloDigitos(event, 'decNO'); ");  
				 valorNeto.onkeypress = new Function (" soloDigitos(event, 'decNO'); ");
				 
				 valor.value = formato(Math.round(val));
				 valorNeto.value = formato(Math.round(valNeto));
				   
				 valor.onchange = new Function (" formatear(this); ");  
				 valorNeto.onchange = new Function (" formatear(this); ");      
			 }
		  }
	  
   }
   asignarImpuesto();
}

 function formatoDolar (obj, decimales){
    var numero = obj.value.replace( new RegExp(",","g"), "");
    var nums = ( new String (numero) ).split('.');
	var salida = new String();
	
	var TieneDec = numero.indexOf('.');
	var dato = new String();
	if( TieneDec !=-1  ){
	   var deci = numero.split('.');
	   var dec       = (deci[1].length >2)?deci[1].charAt(2):deci[1].substr(0,deci[1].length);
	   
	   if(dec > 5){
	       dato =  (parseInt(deci[1].substr(0,2)) + 1);
		   if(dato>99){
		     nums[0] = new String (parseInt(nums[0])+1);
			 obj.value = nums[0]+'.00';
		   }else{
		     for (var i=nums[0].length-1, j=0; i>=0; salida = nums[0].charAt(i) + (j%3==0 && j!=0? ',':'') + salida , i--, j++);
	         obj.value = salida + (nums.length > 1 && decimales > 0 ? '.' +((nums[1].length >2)?((nums[1].charAt(2)>5)?(parseInt(nums[1].substr(0,2))+1):nums[1].substr(0,2)):(nums[1].length==1)?nums[1].substr(0,1)+'0':nums[1].substr(0,nums[1].length)) : '.00'); 
		   
		   }
	   }else{
	       for (var i=nums[0].length-1, j=0; i>=0; salida = nums[0].charAt(i) + (j%3==0 && j!=0? ',':'') + salida , i--, j++);
	       obj.value = salida + (nums.length > 1 && decimales > 0 ? '.' +((nums[1].length >2)?((nums[1].charAt(2)>5)?(parseInt(nums[1].substr(0,2))+1):nums[1].substr(0,2)):(nums[1].length==1)?nums[1].substr(0,1)+'0':nums[1].substr(0,nums[1].length)) : '.00'); 
	   } 
    }else{
  	   for (var i=nums[0].length-1, j=0; i>=0; salida = nums[0].charAt(i) + (j%3==0 && j!=0? ',':'') + salida , i--, j++);
	   obj.value = salida + (nums.length > 1 && decimales > 0 ? '.' +((nums[1].length >2)?((nums[1].charAt(2)>5)?(parseInt(nums[1].substr(0,2))+1):nums[1].substr(0,2)):(nums[1].length==1)?nums[1].substr(0,1)+'0':nums[1].substr(0,nums[1].length)) : '.00'); 
	}
	
	
	

 }
  
 function redondear(valor){
		valor = new String(valor);
		var nums =  valor.split('.');
		return nums[0]+ (nums.length > 1  ? '.' +((nums[1].length >2)?((nums[1].charAt(2)>5)?(parseInt(nums[1].substr(0,2))+1):nums[1].substr(0,2)):(nums[1].length==1)?nums[1].substr(0,1)+'0':nums[1].substr(0,nums[1].length)) : '.00'); 
	
    }
 
        
//(nums[1].substr(0, (nums[1].length>decimales?decimales:nums[1].length)))
        


</script>
<form name="forma1" id="forma1" action="" method="post"> 
<%
  String mensaje=""+request.getParameter("ms");
    if (! (mensaje.equals("")|| mensaje.equals("null"))){
%>
<input type="hidden" name="Modificar"  id="Modificar" value="<%=Modificar%>">
<table border="2" align="center">
  <tr>
    <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="282" align="center" class="mensajes"><%=mensaje%></td>
          <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="78">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<br>
<%
    }   
%>
     


<table width="980" align="center">
<tr><td>
<table border="2"  >
  <tr >
      <td> 
        <table width="100%" align="center"  >
          <tr >
            <td width="50%"  class="subtitulo1"><p align="left">Documento</p></td>
            <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" class="Estilo1" height="20" align="left"><%=datos[0]%></td>
          </tr>
        </table>
        <table width="100%" align="center" cols="7">
          <tr class="filaFactura">
            <td width="103"  >Documento:</td>
            <td width="117" >
                <input:select name="tipo_documento"      attributesText=" id='documentos' style='width:80%;' onChange='docRel(this.value);'"  default="010" options="<%=documentos%>" />
            </td>
            <td width="103" ><input name="documento" type="text" onKeyPress="soloAlfa(this)"  class="<%=clase%>" onfocus='this.select()' id="documento" <%=readonly%> size="20" maxlength="30" value="<%=documento%>" <%=estilo%>>            </td>
            <td width="72" >Proveedor:</td>
            <td width="169" ><input name="proveedor"  type="text" class="<%=clase%>" value="<%=proveedor%>" onfocus='this.select()' id="proveedor" size="18" maxlength="15"  <%=readonly%> onKeyUp="<%=eventoProveedor%>" <%=estilo%> >
			              &nbsp;&nbsp;
						  <% if(!Modificar.equals("si")){%>
						  <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  width="15" height="15"  onClick="<%=eventoLupaProveedor%>" title="Buscar" style="cursor:hand" >
						  <%}%>
						  <input type="hidden" name="ValidarProv" value="<%=proveedor%>">
						  <input type="hidden" name="riva" value="<%=riva%>">
						  <input type="hidden" name="agenteRet" value="<%=agenteRet%>">
						  </td>
            <td colspan="3" nowrap>
            <input type="hidden" name="nombre_proveedor" id="nombre_proveedor" size="50" class="textbox" value='<%=nombre_proveedor%>'> 
            <div id="nombreP">&nbsp;&nbsp;<%=nombre_proveedor%></div></td>
            </tr>
		  <tr class="filaFactura">
		  	<td >Documento Rel:</td>
			<td >
           		<input:select name="tipo_documento_rel"      attributesText=" id='tipo_documento_rel' style='width:80%;'  disabled"  default="010" options="<%=documentos%>" />
			</td>
			<td >
			 
			  <select name="documento_relacionado" id="documento_relacionado"   disabled>
			  <%   
			    if (NumeroFacturas!= null){
			     for(int i=0;i<NumeroFacturas.size();i++){%>
			       <option value ="<%=NumeroFacturas.elementAt(i)%>><%=NumeroFacturas.elementAt(i)%>"</option> 
			  <% }
			  }%>  
			  </select>
			
			</td>
			<td >Fecha :</td>
			<td colspan="2"  ><input name="fecha_documento" type="text" id="fecha_documento2" size="11" value="<%=fecha_documento%>">
              <span class="comentario"></span> <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.forma1.fecha_documento);return false;" hidefocus> <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a>			</td>				
			<td width="121" colspan="-1" >Plazo(d&iacute;as):</td>
			<td width="266" colspan="-1" ><input name="plazo" type="text"  value="<%=plazo%>"   id="plazo2" size="4" maxlength="3">
			 </td>
			<script>
			forma1.plazo.value = '<%=plazo%>';
			</script>
		  </tr>
		  <tr class="filaFactura">
		  	<td><p>Valor:</p></td>
			<td ><input name="vlr_neto" type="text"  class="textboxFactura"  <%if(o_moneda.equals("DOL")){%>onKeyPress="soloDigitos(event, 'decOK')" onChange="formatoDolar(this,2);"  <%}else{%>onKeyPress="soloDigitos(event, 'decNO')" onChange="formatear(this);"<%}%>  onFocus='this.select()'  id="vlr_neto3"  value='<%=(o_moneda.equals("DOL"))?UtilFinanzas.customFormat2(vlr_neto):UtilFinanzas.customFormat(vlr_neto)%>' maxlength="10"  align="right" style="text-align:right;"></td>
			<td ><input name="total" type="text" id="total4" value='<%= (o_moneda.equals("DOL"))?UtilFinanzas.customFormat2(vlr_total):UtilFinanzas.customFormat(vlr_total) %>'   readonly align="right"  class="filaresaltada" style="border:0; text-align:right; width:100;" ></td>
			<td >Moneda:</td>
			<td colspan="2" ><input:select name="moneda" attributesText=" id='moneda' style='width:80%;' onChange='cambiar(this.value)'  "  default="<%=o_moneda%>" options="<%=t_moneda%>" /></td>		                     
			<td colspan="-1" >Banco:</td>	
			<td colspan="-1" ><input:select name="c_banco"      attributesText="<%=" id='c_banco' style='width:150px;'   onChange=\\" bancos('"+CONTROLLER+"','"+validar_agencia+"',maxfila,'"+Modificar+"'); \\" "%>"  default="<%=c_banco%>" options="<%=b%>" /></td>		
			<script>
			 forma1.moneda.value ='<%=o_moneda%>';
			 forma1.c_banco.value = '<%=c_banco%>';
			</script>
  		</tr>
		  <tr class="filaFactura">
		  	  <td >Descripci&oacute;n:</td>
			  <td colspan="5"><textarea name="descripcion" cols="70" rows="1" class="textareaFactura"><%=descripcion%></textarea></td>
				<td colspan="-1" class="filaFactura">Sucursal:</td>
			    <td colspan="-1" >
				<select name="c_sucursal"     id='c_sucursal' style='width:80%;' onChange=" moneda_banco.value = this.options[this.selectedIndex].moneda; " >
				 <option  moneda ="" value=""></option>
				 <%for(int i=0; i<sbancso.size(); i++){
					  Banco banc = (Banco) sbancso.elementAt(i);
						
					  if( banc.getBanco().matches(c_banco) ){
							%><option moneda='<%= banc.getMoneda() %>' value="<%=banc.getBank_account_no()%>"><%=banc.getBank_account_no()%></option><%
					  }
			  	  }  %>
				</select> <input type="text" name="moneda_banco"  class="filaresaltada" style="border:0; width:30; " value="<%=moneda_banco%>"> </td>		
				<script>forma1.c_sucursal.value = '<%=b_sucursal%>';</script>
	      </tr>
  		
		    <tr>
		  	  <td rowspan="2" class="filaFactura">Observaci&oacute;n: </td>
			  <td colspan="5" rowspan="2" class="fila"><table width="100%">
                <tr class="filaFactura">
                  <td width="58%" rowspan="2"><textarea name="observacion" cols="70" rows="3" class="textboxFactura"><%=observacion%></textarea></td>
                  <td width="9%">IVA</td>
                  <td width="13%" nowrap><input name="CabIva" id ="CabIva" style='text-transform:uppercase' class='textareaFactura' type="text" size="6" maxlength="10"   onBlur="enterImpALL('IVA','CabIva','<%=BASEURL%>',maxfila);" value="<%=CabIva%>">
                   &nbsp;<img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name="" width="15" height="15"  id="imageB" style="cursor:hand" title="Buscar"  onClick="funImpALL('IVA','CabIva','<%=BASEURL%>','',maxfila);"> </td>
                  <td width="6%">RIVA</td>
                  <td width="14%" nowrap><input name="CabRiva" id ="CabRiva" style='text-transform:uppercase'   class='textareaFactura' type="text" size="6" maxlength="10"  value="<%=CabRiva%>"  readonly>
                    </td>
                </tr>
                <tr class="filaFactura">
                  <td>RICA</td>
                  <td nowrap><input name="CabRica" id ="CabRica"  class='textareaFactura' style='text-transform:uppercase' type="text" size="6" maxlength="10"   value="<%=CabRica%>" onBlur="enterImpALL('RICA','CabRica','<%=BASEURL%>',maxfila);" <%if(!agenteRet.equals("S")&&(!rica.equals("S")) ){%>readonly<%}%>>
                   &nbsp;<img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name="" width="15" height="15"  id="imageB" style="cursor:hand" title="Buscar" <%if((!agenteRet.equals("S"))&&(rica.equals("S"))){ %>onClick="funImpALL('RICA','CabRica','<%=BASEURL%>','',maxfila);" <%}%>></td>
                  <td>RFTE </td>
                  <td nowrap><input name="CabRfte" id ="CabRfte" type="text" style='text-transform:uppercase' class='textareaFactura'  size="6" maxlength="10"   value="<%=CabRfte%>" onBlur="enterImpALL('RFTE','CabRfte','<%=BASEURL%>',maxfila);" <%if(!agenteRet.equals("S")&&(!rfte.equals("S")) ){%>readonly<%}%>>
                    &nbsp;<img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name="" width="15" height="15"  id="imageB" style="cursor:hand" title="Buscar" <%if((!agenteRet.equals("S"))&&(rfte.equals("S"))){ %>onClick="funImpALL('RFTE','CabRfte','<%=BASEURL%>','',maxfila);" <%}%>></td>
                </tr>
              </table></td>
			  <td colspan="-1" class="filaFactura">Autorizador: </td>
			  <td colspan="-1" class="filaFactura" ><input:select name="usuario_aprobacion" attributesText="style='width:80%;' " default="<%=usuario_aprobacion%>" options="<%=u%>" /></td>
		    </tr>
		    <tr class="filaFactura">
		      <td height="26" colspan="-1" id="ag">Agencia :</td>
		      <td colspan="-1"><input:select name="agencia" attributesText="<%=" id='agencia' style='width:80%;'  onChange=\\" BuscarAgencias('"+CONTROLLER+"',maxfila,'"+Modificar+"'); \\" "%>"  options="<%=agencias%>" /></td>
			  <script>forma1.agencia.value = '<%=id_agencia_usuario%>';</script>
		    </tr>
	  </table></td>
</tr>
</table>
	<table width="100%" border="2" align="left">
		<tr>
		<td >
			<table width="100%" align="center"  >
          <tr >
            <td width="50%"  class="subtitulo1"><p align="left">Detalle del Documento </p></td>
            <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" class="Estilo1">
              <input name="num_items" id="num_items4" type="hidden" size="16" value='1' maxlength="16"></td>
          </tr>
        </table>
			<div align="left">
			<table id="detalle" width="100%" >
				<tr  id="fila1" class="tblTituloFactura">
					<td align="center">Item</td>
					<td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr align="center"  class="tblTituloFactura">
					    <td width="18">&nbsp;</td>
                        <td width="320" align="left">Descripcion</td>
                        <td width="208" align="left">Concepto</td>
						<td width="35" align="left">Ree</td>
                        <td width="100" align="left">Cuenta</td>
						<td width="60" align="left">Sub</td>
						<td align="left">Auxiliar</td>
                        <td width="47"  align="left">ABC</td>
                       
                        
                      </tr>
                    </table>
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr align="center" class="tblTituloFactura">
						<td width="20">&nbsp;</td>
						 <td width="72" align="left">Planilla</td>
                         <td width="52" align="left" >Tipo</td>
                         <td width="108" align="left">Centro</td>
                         <td width="179" align="left">Valor</td>
                         <td width="115" align="left">IVA</td>
						 <td width="97" align="left">RIVA</td>
						 <td width="100" align="left">RICA</td>
						 <td width="110" align="left" >RFTE</td>
				        
    					  <td width="125" align="left">Valor Neto</td>
                        </tr>
                      </table></td>
					</tr>
				
				<% int x=1;
				if ( vItems == null){%>
				 <tr class="filaFactura" id="filaItem<%=x%>" nowrap  bordercolor="#D1DCEB" >
				    <td nowrap><a id="n_i<%=x%>" ><%=x%></a>
				        <input name="cod_item<%=x%>" id="cod_item<%=x%>" value="<%=x%>" type="hidden" size="4" maxlength="5" border="0">
			          <a onClick="insertarItem('<%=validar_agencia%>', '<%=BASEURL%>','<%=cod_agencia_cont + "," + unidad_contable%>','<%=tipcliarea%>','<%=codCliAre%>','<%=Modificar%>');"  id="insert_item<%=x%>" style="cursor:hand" ><img src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width="12" height="12"  ></a>
					  <a onClick="borrarItem('filaItem<%=x%>');"  value="1" id="borrarI<%=x%>" style="cursor:hand" ><img id="imgini" name="imgini" src='<%=BASEURL%>/images/botones/iconos/menos1.gif'   width="12" height="12"  > </a></span></td>
					     
				    <td>
					<table width="100%" border="0" cellpadding="0" cellspacing="0">
                      <tr align="center">
                        <td width="333"><span>
						<textarea name="desc<%=x%>" id="desc<%=x%>" cols="55" rows="1" class="textboxFactura" ></textarea>
						</span></td>
                        <td width="201"><input name="descripcion_i<%=x%>" id="descripcion_i<%=x%>" type="text" size="30" class="textboxFactura"  maxlength="4"  onKeyPress="soloDigitos(event, 'decNO')" onBlur="BuscarConcepto('<%=x%>',event, 'decNO');">                          <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name="arbol<%=x%>" width="15" height="15"  id="arbol<%=x%>" style="cursor:hand" title="Buscar"  onClick="servicios('<%=x%>','<%=CONTROLLER%>');" ></td>
                        <td width="35"><input name="REE<%=x%>" style="text-transform:uppercase; width:20;" class="textboxFactura" id="REE<%=x%>" type="text"  maxlength="1"   onKeyUp="Ree('<%=x%>');"></td>
						<td width="108">
						<input name="codigo_cuenta<%=x%>" id="codigo_cuenta<%=x%>" class="textboxFactura" type="text" size="13" maxlength="20" onChange="VerificarCuenta(<%=x%>)"> 
						<input type="hidden" name="cod1<%=x%>" id="cod1<%=x%>" value="-">
						<input type="hidden" name="cod2<%=x%>" id="cod2<%=x%>" value="--">
						<input type="hidden" name="cod3<%=x%>" id="cod3<%=x%>" value="---">
						<input type="hidden" name="cod4<%=x%>" id="cod4<%=x%>" value="---">
						<input type="hidden" name="cod5<%=x%>" id="cod5<%=x%>" value="----">
						                           
                          <input name="cod_cuenta<%=x%>" id="cod_cuenta<%=x%>"       type="hidden" size="4" maxlength="5" border="0" value="<%=cod_agencia_cont + "," + unidad_contable%>"></td>
						 <td  width="50" align="right" id="combotipo<%=x%>"><select name='tipo<%=x%>' style="width:45 " >
                                  <option value=''>  </option>
                                </select></td>
						 <td align="center">
                            <input type="text" maxlength='25'  style='width:90' name='auxiliar<%=x%>' id='auxiliar<%=x%>' onBlur="validarAuxiliar('<%=BASEURL%>','<%=x%>');">
                            <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name="imagenAux<%=x%>" width="15" height="15"  id="imagenAux<%=x%>" style="cursor:hand"  onClick="buscarAuxiliar('<%=BASEURL%>','<%=x%>',forma1.codigo_cuenta<%=x%>.value, forma1.tipo<%=x%>.value);"  >
                       
                          </td>  
						<td width="75"><input name="codigo_abc<%=x%>" style="text-transform:uppercase" id="codigo_abc<%=x%>" type="text" size="4" maxlength="2"></td>
                        
                       
                      </tr>
                    </table>
				      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                         <tr align="center">
						 <td width="20">&nbsp;</td>
						  <td width="60"><input name="planilla<%=x%>" id="planilla<%=x%>" type="text" size="8"  class="textboxFactura"  maxlength="8" onBlur="disabledPlanilla('<%=x%>', '<%=validar_agencia%>', '<%=Modificar%>')" ></td>
                          <td width="65"><div align="center">
                            <select name="cod_oc<%=x%>" id="cod_oc<%=x%>" style='width:80%;'   onClick=" buscarCodigoOC('<%=x%>','<%=BASEURL%>') " >
                              <option value="C" selected>C</option>
                            </select>
                          </div></td>
                          <td width="60"><div align="center">
                            <input name="oc<%=x%>" type="text" id="oc<%=x%>" size="6" maxlength="8" class="textboxFactura" >
                            <input name="doc<%=x%>" id="doc<%=x%>" type="hidden">
                          </div></td>
                            <td width="239"><input name="valor1<%=x%>"  onfocus='this.select()' class="textboxFactura"   id="valor1<%=x%>" type="text" size="16"  style="text-align:right;" onBlur="asignarImpuesto()"  onChange="formatear(this);" onKeyPress="soloDigitos(event, 'decNO')"  maxlength="11"  align="right"></td>
							<input type="hidden" name="iva<%=x%>" id="iva<%=x%>">
							<%for(int l = 0;l < vTiposImpuestos.size();l++){ %>
                            <td width="97" nowrap>
							  
							<input name="impuesto<%=l%><%=x%>"  style="text-transform:uppercase" id ="impuesto<%=l%><%=x%>" class='textareaFactura' type="text" size="6" maxlength="10"  onKeyUp="enter('<%=vTiposImpuestos.elementAt(l)%>','<%=l%><%=x%>','<%=BASEURL%>');" onBlur="<%if(!vTiposImpuestos.elementAt(l).equals("RIVA")){%>enterFocus('<%=vTiposImpuestos.elementAt(l)%>','<%=l%><%=x%>','<%=BASEURL%>');<%}%> <%if(vTiposImpuestos.elementAt(l).equals("RFTE")){%>insertarItem('<%=validar_agencia%>','<%=BASEURL%>','<%=cod_agencia_cont + "," + unidad_contable%>','<%=tipcliarea%>','<%=codCliAre%>','<%=Modificar%>');<%}%>"
							<%if(!agenteRet.equals("S")&& l!= 0){  
                                if((vTiposImpuestos.elementAt(l).equals("RICA")) && (!rica.equals("S")) ){%>
                                         readonly
                                    <%}else if((vTiposImpuestos.elementAt(l).equals("RFTE")) && (!rfte.equals("S")) ){%>
                                         readonly
                                    <%}else if(vTiposImpuestos.elementAt(l).equals("RIVA")) {%>
                                          readonly
                                     <%}%>		
                                <%}%>
							 >
							 <%if(!vTiposImpuestos.elementAt(l).equals("RIVA")){%>
                                <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name="<%=l%><%=x%>" width="15" height="15"  id="imageB_<%=l%>_<%=x%>" style="cursor:hand" title="Buscar"  
                            <%
							 if((!agenteRet.equals("S"))||(l==0)){ 
                                if((vTiposImpuestos.elementAt(l).equals("RICA")) && (rica.equals("S")) ){%>
                                           onClick="funI('<%=vTiposImpuestos.elementAt(l)%>',this.name,'<%=BASEURL%>');"
                                    <%}else if((vTiposImpuestos.elementAt(l).equals("RFTE")) && (rfte.equals("S")) ){%>
                                         onClick="funI('<%=vTiposImpuestos.elementAt(l)%>',this.name,'<%=BASEURL%>');"
                                    <%}else if((vTiposImpuestos.elementAt(l).equals("RIVA")) && (riva.equals("S")) ){%>
                                          onClick="funI('<%=vTiposImpuestos.elementAt(l)%>',this.name,'<%=BASEURL%>');"	
                                    <%}else if(vTiposImpuestos.elementAt(l).equals("IVA")){%>
                                          onClick="funI('<%=vTiposImpuestos.elementAt(l)%>',this.name,'<%=BASEURL%>');"
                                     <%}%>		
                                <%}%> >
						  	<%}%><input name="tipo_impuesto<%=l%><%=x%>" id ="tipo_impuesto<%=l%><%=x%>" type="hidden" size="10" value="<%=vTiposImpuestos.elementAt(l)%>"></td>
                              <%}%> 
                             <td width="143"><input name="valorNeto<%=x%>" id="valorNeto<%=x%>" class="textboxFactura"  style="text-align:right;" type="text" size="16"  onChange="formatear(this);" onKeyPress="soloDigitos(event, 'decNO')"  maxlength="11" align="right" readonly></td>
						</tr>
                      </table></td>
				    </tr>   
				<% }
				else{
				 System.out.println("Los Items en la pagina no son null y tienen tama�o "+ vItems.size());
				
				for( x=1;x<=MaxFi;x++){
				                CXPItemDoc item = (CXPItemDoc)vItems.elementAt(x-1);
				  if(item.getConcepto() != null ){
                      
                                    concepto     =""+ item.getConcepto(); 
									descripcion_i=""+ item.getDescripcion();
									ree          =""+ item.getRee(); 
                                    codigo_cuenta=""+ item.getCodigo_cuenta();
                                    codigo_abc=""+ item.getCodigo_abc();
                                    planilla=""+ item.getPlanilla();
									tipcliarea=""+item.getTipcliarea ();
									codcliarea=""+item.getCodcliarea ();
									descliarea=""+item.getDescliarea ();
                                    valor	=	item.getVlr_me();			
									valor_t = 	item.getVlr_total();
									iva     =   item.getIva();
									codigos =   item.getCodigos();
									//ivan 21 julio 2006
									auxiliar  = item.getAuxiliar();
									tbltipo   = item.getTipo();
									tipoSubledger   = item.getTipoSubledger();
									////////////////////////////////
				%>
                <tr class="<%=(x%2!=0)?"filaFactura":"filagrisFac"%>" id="filaItem<%=x%>" nowrap  bordercolor="#D1DCEB" >
                  <td  nowrap> <a id="n_i<%=x%>" ><%=x%></a>
                      <input name="cod_item<%=x%>" id="cod_item<%=x%>" value="<%=x%>" type="hidden" size="4" maxlength="5" border="0">
                      <%//if( x == 1 ){ %>
                      <a onClick="insertarItem('<%=validar_agencia%>','<%=BASEURL%>','<%=cod_agencia_cont + "," + unidad_contable%>','<%=tipcliarea%>','<%=codCliAre%>','<%=Modificar%>');"  id="insert_item<%=x%>" style="cursor:hand" ><img src='<%=BASEURL%>/images/botones/iconos/mas.gif'   width="12" height="12"  >
					  </a> <a onClick="borrarItem('filaItem<%=x%>');"  value="1" id="borrarI<%=x%>" style="cursor:hand" ><img id="imgini" src='<%=BASEURL%>/images/botones/iconos/menos1.gif'   width="12" height="12"  ></a>
                      <%//}%>
                  </td>
                  <td>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                      <tr align="center">
                        <td width="333"><span><span class="bordereporte">
                          <textarea name="desc<%=x%>" id="desc<%=x%>" cols="55" rows="1" class="textareaFactura"  ><%=descripcion_i%></textarea>
                        </span>
                            </span></td>
                        <td width="201"><input name="descripcion_i<%=x%>" id="descripcion_i<%=x%>" type="text" size="30" class="textboxFactura"  value='<%=concepto%>' maxlength="4"  onKeyPress="soloDigitos(event, 'decNO')" onBlur="BuscarConcepto('<%=x%>',event, 'decNO');">
                          <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name="arbol<%=x%>" width="15" height="15"  id="arbol<%=x%>" style="cursor:hand" title="Buscar"  onClick="servicios('<%=x%>','<%=CONTROLLER%>');" > </td>
                        <td width="35"><input name="REE<%=x%>" style="text-transform:uppercase; width:20;" class="textboxFactura" id="REE<%=x%>" type="text"  maxlength="1" value="<%=ree%>"  onKeyUp="Ree('<%=x%>');"></td>
						<td width="108">
							<%String EstiloError = ""; 
							 if((!item.isExiteCuenta())&&(ErrorCuenta.equals("true"))){
							    EstiloError = "background-color:#CC0000; color:#FFFFFF;";
							 }%>
						 <input name="codigo_cuenta<%=x%>" id="codigo_cuenta<%=x%>"  style='<%=EstiloError%> ' class="textboxFactura" type="text" size="13" value='<%=codigo_cuenta%>' maxlength="20"   onChange="VerificarCuenta(<%=x%>);">
                          <input name="cod_cuenta<%=x%>" id="cod_cuenta<%=x%>" value="<%=cod_agencia_cont+","+unidad_contable%>" type="hidden" size="4" maxlength="5" border="0">
						  <input type="hidden" name="cod1<%=x%>" id="cod1<%=x%>" value="<%=codigos[0]%>">
					  	  <input type="hidden" name="cod2<%=x%>" id="cod2<%=x%>" value="<%=codigos[1]%>">
						  <input type="hidden" name="cod3<%=x%>" id="cod3<%=x%>" value="<%=codigos[2]%>">
						  <input type="hidden" name="cod4<%=x%>" id="cod4<%=x%>" value="<%=codigos[3]%>">
						  <input type="hidden" name="cod5<%=x%>" id="cod5<%=x%>" value="<%=codigos[4]%>">
						  </td>
						  <td  width="50" align="right" id="combotipo<%=x%>">
						  
						  <%////////Ivan 21 julio 2006
						   if(tbltipo != null ){
                          %>    <select name='tipo<%=x%>' class='textbox' id='tipo<%=x%>' style='width:90% ' onChange="forma1.auxiliar<%=x%>.value='';"> <option value=''>  </option> ";
                         <% for(int i = 0; i<tbltipo.size(); i++){
								TablaGen tipo = (TablaGen) tbltipo.get(i); %>
								<option value='<%=tipo.getTable_code()%>'<%if(tipo.getTable_code().equals(tipoSubledger)){%>selected<%}%> ><%=tipo.getTable_code()%></option>";
                          <%}%>
                          </select>
					      <%}else{%>
								<select name='tipo<%=x%>' style="width:45 " >
                                  <option value=''>  </option>
                                </select>	
							<%}%>	
						</td>
						 <td align="center">
                            <input type="text" maxlength='25' <%if(item.isReqAuxilar()){%> style='width:90; background-color:#CC0000; color:#FFFFFF;'<%}else{%>style='width:90'<%}%> name='auxiliar<%=x%>' id='auxiliar<%=x%>' onBlur="validarAuxiliar('<%=BASEURL%>','<%=x%>');" value="<%=auxiliar%>">
                            <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name="imagenAux<%=x%>" width="15" height="15"  id="imagenAux<%=x%>" style="cursor:hand"  onClick="buscarAuxiliar('<%=BASEURL%>','<%=x%>',forma1.codigo_cuenta<%=x%>.value, forma1.tipo<%=x%>.value);"  >
                       
                          </td> 
                        <td width="75"><input name="codigo_abc<%=x%>" style="text-transform:uppercase"  id="codigo_abc<%=x%>" type="text" size="4" class="textboxFactura"  value="<%=codigo_abc%>" maxlength="2"></td>
                       </tr>
                    </table>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr align="center">
                        <td width="20">&nbsp;</td>
						 <td width="60"><input name="planilla<%=x%>" id="planilla<%=x%>" type="text" size="8"class="textboxFactura"   value="<%=planilla%>" maxlength="8" onBlur="disabledPlanilla('<%=x%>', '<%=validar_agencia%>','<%=Modificar%>')"></td>
                        <td width="65"><select name="cod_oc<%=x%>" id="cod_oc<%=x%>" style='width:80%;'   onClick=" buscarCodigoOC('<%=x%>','<%=BASEURL%>') " >
                            <option value="C" selected>C</option>
                        </select></td>
                        <td width="60"><input name="oc<%=x%>" type="text" id="oc<%=x%>" size="6" maxlength="8"  class="textboxFactura"  value="<%=codcliarea%>"  title="<%=descliarea%>"readonly>                            
                          <input name="doc<%=x%>" type="hidden" id="doc<%=x%>" value="<%=descliarea%>"></td>
                        <td width="239"><input name="valor1<%=x%>" id="valor1<%=x%>" onFocus='this.select()'  class="textboxFactura" style="text-align:right;"  onBlur="asignarImpuesto(<%=x%>)" <%if(o_moneda.equals("DOL")){%>onKeyPress="soloDigitos(event, 'decOK')" onChange="formatoDolar(this,2);"  <%}else{%>onKeyPress="soloDigitos(event, 'decNO')" onChange="formatear(this);"<%}%> type="text"   size="16" value="<%=(o_moneda.equals("DOL"))?UtilFinanzas.customFormat2(valor):UtilFinanzas.customFormat(valor) %>" maxlength="11" align="right"></td>
                        <input type="hidden" name="iva<%=x%>" id="iva<%=x%>" value="<%=iva%>">
                        <%
                            Copia = item.getVCopia();
                            for(int i=0;i< Copia.size();i++){ 
                                CXPImpItem impuestoCopia = (CXPImpItem)Copia.elementAt(i);
                                String impuesto = impuestoCopia.getCod_impuesto();
                                String tipo 	= impuestoCopia.getTipo_impuesto();
                        %>
                        <td width="97" nowrap>
                             <input name="impuesto<%=i%><%=x%>" style="text-transform:uppercase" id ="impuesto<%=i%><%=x%>" type="text" size="6" value="<%=impuesto%>" class='textareaFactura' maxlength="10" onKeyUp="enter('<%=vTiposImpuestos.elementAt(i)%>','<%=i%><%=x%>','<%=BASEURL%>');" onBlur="<%if(!vTiposImpuestos.elementAt(i).equals("RIVA")){%>enterFocus('<%=vTiposImpuestos.elementAt(i)%>','<%=i%><%=x%>','<%=BASEURL%>');<%}%> <%if(vTiposImpuestos.elementAt(i).equals("RFTE")){%>insertarItem('<%=validar_agencia%>','<%=BASEURL%>','<%=cod_agencia_cont + "," + unidad_contable%>','<%=tipcliarea%>','<%=codCliAre%>','<%=Modificar%>');<%}%>"
							 <%if(!agenteRet.equals("S")&& i!= 0){ 
                                if((vTiposImpuestos.elementAt(i).equals("RICA")) && (!rica.equals("S")) ){%>
                                         readonly
                                    <%}else if((vTiposImpuestos.elementAt(i).equals("RFTE")) && (!rfte.equals("S")) ){%>
                                         readonly
                                    <%}else if(vTiposImpuestos.elementAt(i).equals("RIVA")) {%> 
                                          readonly
                                     <%}%>		
                                <%}%>
							 >
							 <%if(!vTiposImpuestos.elementAt(i).equals("RIVA")){%> 
                             <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'    name="<%=i%><%=x%>" width="15" height="15"   id="imageB_<%=i%>_<%=x%>" style="cursor:hand" title="Buscar" 
                            <%if((!agenteRet.equals("S"))||(i==0)){
                                  if((vTiposImpuestos.elementAt(i).equals("RICA")) && (rica.equals("S")) ){%>
                                      onClick="funI('<%=vTiposImpuestos.elementAt(i)%>',this.name,'<%=BASEURL%>');"
                                <%}else if((vTiposImpuestos.elementAt(i).equals("RFTE")) && (rfte.equals("S")) ){%>
                                       onClick="funI('<%=vTiposImpuestos.elementAt(i)%>',this.name,'<%=BASEURL%>');"
                                <%}else if(vTiposImpuestos.elementAt(i).equals("IVA")){%>
                                       onClick="funI('<%=vTiposImpuestos.elementAt(i)%>',this.name,'<%=BASEURL%>');"
                                <%}%>	
                            <%}%>  ><%}%><input name="tipo_impuesto<%=i%><%=x%>" id ="tipo_impuesto<%=i%><%=x%>3" type="hidden"size="10" value="<%=vTiposImpuestos.elementAt(i)%>">
                            </td>
                        <%}%>
                        <td width="143"><input  name="valorNeto<%=x%>" id="valorNeto<%=x%>" class="textboxFactura"   style="text-align:right;" type="text" size="16"  <%if(o_moneda.equals("DOL")){%>onKeyPress="soloDigitos(event, 'decOK')" onChange="formatoDolar(this,2);"  <%}else{%>onKeyPress="soloDigitos(event, 'decNO')" onChange="formatear(this);"<%}%>  maxlength="11" align="right" value="<%=(o_moneda.equals("DOL"))?UtilFinanzas.customFormat2(valor_t):UtilFinanzas.customFormat(valor_t)%>" readonly></td>
                      </tr>
                  </table></td>
                </tr>  
            <%  }
			  }%>
			   <script>
			    organizar();
			   </script>
            <%}%>
			</table>
		  </div>		  </td>
		</tr>
	</table>
</td>
</tr>
</table>
  <tr>

    <div align="center">
	   <%if(Modificar.equals("si")){%>
	      <img src='<%=BASEURL%>/images/botones/agregar.gif' name='Buscar' align="absmiddle" style='cursor:hand'   onClick="insertarItem('<%=validar_agencia%>','<%=BASEURL%>','<%=cod_agencia_cont + "," + unidad_contable%>','<%=tipcliarea%>','<%=codCliAre%>','<%=Modificar%>');" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
	      &nbsp;<img src='<%=BASEURL%>/images/botones/modificar.gif' name='Buscar' align="absmiddle" style='cursor:hand'   onclick="validarDocumento('<%=CONTROLLER%>', '<%=validar_agencia%>',maxfila,'<%=Modificar%>');" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
	      &nbsp;<img src='<%=BASEURL%>/images/botones/salir.gif' name='Buscar' align="absmiddle" style='cursor:hand'   onClick="window.close();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
		  
	   <%}else{%>
		<img src='<%=BASEURL%>/images/botones/iconos/guardar.gif' name='Buscar' width="25" height="25" align="absmiddle" style='cursor:hand' title='Guardar...'  onclick="escribirArchivo('<%=CONTROLLER%>',maxfila);" >
		&nbsp;<img src='<%=BASEURL%>/images/botones/agregar.gif' name='Buscar' align="absmiddle" style='cursor:hand'   onClick="insertarItem('<%=validar_agencia%>','<%=BASEURL%>','<%=cod_agencia_cont + "," + unidad_contable%>','<%=tipcliarea%>','<%=codCliAre%>','<%=Modificar%>');" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
		 &nbsp;<img src='<%=BASEURL%>/images/botones/aceptar.gif' name='Buscar' align="absmiddle" style='cursor:hand'   onclick="validarDocumento('<%=CONTROLLER%>', '<%=validar_agencia%>',maxfila,'<%=Modificar%>');" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
        &nbsp;<img src='<%=BASEURL%>/images/botones/restablecer.gif' name='restablecer' align="absmiddle" style='cursor:hand'   onClick="Restablecer('<%=CONTROLLER%>');"  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
		&nbsp;<img src='<%=BASEURL%>/images/botones/salir.gif' name='Buscar' align="absmiddle" style='cursor:hand'   onClick="window.close();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
		
		<%}%>
    </div>
  <tr>
  </form>
</div>
</body>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
<%=datos[1]%>
<script>
    function enviar(url){
         var a = "<iframe name='ejecutor' style='visibility:hidden'  src='<%= CONTROLLER %>" + url + "'> ";
         aa.innerHTML = a;
    }
	function enviar2(url){
	     var a = "<iframe name='ejecutor' style='visibility:hidden'  src='" + url + "'> ";
         aa.innerHTML = a;
    }

</script>
<font id='aa'></font>
</html>

<script>
    document.forma1.documento.focus();
	window.resizeTo(1024,745);
	window.moveTo(0,0);
</script>
    