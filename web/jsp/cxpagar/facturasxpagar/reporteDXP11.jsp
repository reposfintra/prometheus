<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar el formulario para
                            generacion de reportes de documentos por pagar
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page errorPage= "../error/ErrorPage.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head><title>JSP Page</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<style type="text/css">
<!--
.Estilo1 {color: #D0E8E8}
-->
</style>
<script language="javascript" src="<%= BASEURL %>/js/boton.js"></script>
<script>

    function buscarProveedores(BASEURL){
        var hijo=window.open(BASEURL+'/jsp/cxpagar/facturasxpagar/buscarnit2.jsp?num_items=1','Trafico','width=700,height=520,scrollbars=no,resizable=yes,top=10,left=65,status=yes');
        hijo.opener=document.window;
    }
	
    function mostrarReporte(CONTROLLER){
	document.forma1.action = CONTROLLER+"?estado=Factura&accion=Rmostrar"; 
        document.forma1.submit(); 
    }
    
    var valorRadio = "";
    function setRadio(value){
        valorRadio = value; 
    }
    
    function validar(CONTROLLER){
        var totales = document.getElementsByName("c_criterio_t");
        var despliegues = document.getElementsByName("c_criterio_d");
        var hayTotales = false;
        var hayDespliegues = false;
        for( var i=0; i<totales.length; i++ ){
            if ( totales[i].checked ){
                    hayTotales = true;
            }
            if ( despliegues[i].checked ){
                    hayDespliegues = true;
            }
        }
        if ( !hayTotales ){
            alert("Debe seleccionar algun Criterio total");
            return false;
        }
        else if ( !hayDespliegues ){
            alert("Debe seleccionar algun Criterio de Despliegue");
            return false;
        }
        else{
            mostrarReporte(CONTROLLER);
        }
    }
    
    function resumen(valor){
        if( valor =="general"){
            forma1.c_proveedores.disabled=true;
            forma1.c_nombre.disabled=true;
            forma1.c_banco.disabled=true;
            forma1.c_sucursal.disabled=true;
        }
        else{		
            forma1.c_proveedores.disabled=false;
            forma1.c_nombre.disabled=false;
            forma1.c_banco.disabled=false;
            forma1.c_sucursal.disabled=false;
        }	
    }
    
    function bancos(valor,CONTROLLER){
        if(valor!= "todos"){
            for (var i=0;i< forma1.c_proveedores.length;i++){
                forma1.c_proveedores[i].selectedIndex;
            }
            document.forma1.action = CONTROLLER+"?estado=Factura&accion=RcargarBancos"; 
            document.forma1.submit(); 
        }
        else{	
            for (k=forma1.c_sucursal.length-1; k>=0 ;k--)
                forma1.c_sucursal.remove(k);
        } 
    }
	
    function rango(valor){
        var v=parseInt(valor);
        v= v+ 7
        for (k=forma1.c_rango_f.length-1; k>=0 ;k--)
            forma1.c_rango_f.remove(k);
        if(valor==1){v=v-1;}
            for( i =v ;i<=32;i=i+8){
		if(i==31){ i=30;}
	    var Ele = document.createElement("OPTION");
            Ele.value=i;
            Ele.text=i;
            forma1.c_rango_f.add(Ele);
        }
        var Ele = document.createElement("OPTION");
        Ele.value="32";
        Ele.text="+30";
        forma1.c_rango_f.add(Ele);
    }
	
    function todosBancos(){
        var Ele = document.createElement("OPTION");
        Ele.value="todos";
        Ele.text="Todos los bancos";
        forma1.c_banco.add(Ele);
    }
</script>
<body onLoad="redimensionar()" onresize="redimensionar()">
</head>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Reporte de Facturas Vencidas"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
    String c_banco=""+request.getParameter("c_banco");
    if(c_banco.equals("null")){
        c_banco="";
    }
    String proveedores =""+request.getParameter("proveedores");
    if(proveedores.equals("null")){
        proveedores="";
    }
    String nombre =""+request.getParameter("c_nombre");
    if(nombre.equals("null")){
        nombre="";
    }
    TreeMap b = model.servicioBanco.obtenerNombresBancosPorAgencia("BQ");
    TreeMap sbancos = model.servicioBanco.obtenerSucursalesBanco(c_banco);
%>
<form name="forma1" action="" method="post">    
<br>  
  <p>
	
<table width="519" height="76" border="2" align="center">
	<tr >
		<td height="68">
             <table width="100%" class="tablaInferior">
			<tr class="tablaInferior">
			<td width="271" height="24"  class="subtitulo1"><p align="left">Tipo de Reporte </p></td>
			<td width="224"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
			</tr>
		  </table>
			<table width="100%" height="34" align="center" class="tablaInferior" cols="6" >
			  <tr class="fila">
                <td height="28" colspan="2">Reporte  General:</td>
                <td width="81"><input name="c_general" id ="c_general" type="radio" value="general" onClick="resumen(this.value);"></td>
                <td colspan="2">Reporte por Proveedor: </td>
		        <td width="94"><input name="c_general" id="c_general" type="radio" value="" onClick="resumen(this.value);"></td>
			  </tr>
			  <tr class="fila">
            <td colspan="2">Numero de dias vencidos:</td>
            <td colspan="2"><p>Rango Inicial:
                
            </p>
            <p>Rango Final:            </p></td>
            <td colspan="2"><p>
              <select name="c_rango_i" id="c_rango_i" onChange="rango(this.value);">
                  <option value="1">1</option>
                  <option value="8">8</option>
                  <option value="16">16</option>
                  <option value="24">24</option>
              </select>
            </p>
            <p>
              <select name="c_rango_f" id="c_rango_f" >
                <option value="7">7</option>
                <option value="15">15</option>
                <option value="23">23</option>
                <option value="30">30</option>
				<option value="32">+ 30</option>
              </select>          
              </p>
            </tr>
			
      </table>	  </td>
	</tr>
  </table>
  <table width="519" height="156" border="2" align="center">
	<tr >
		<td height="148">
             <table width="100%" class="tablaInferior">
			<tr class="tablaInferior">
			<td width="271" height="24"  class="subtitulo1"><p align="left">Criterios de Selecci&oacute;n para Reporte por Proveedor </p></td>
			<td width="224"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
			</tr>
		  </table>
			<table width="100%" height="108" align="center" class="tablaInferior" >
          <tr class="fila">
            <td width="131" height="43" valign="baseline"><div align="left">Proveedor:</div></td>
            <td colspan="2" > 
              <input name="c_proveedores"  id="c_proveedores" type="text" size="25" value="<%=proveedores%>">
            	<img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  width="15" height="15"  onClick="buscarProveedores('<%=BASEURL%>');" title="Buscar" style="cursor:hand" >
             <div align="left" id="nombre_p"><%=nombre%></div> <input name="c_nombre" type="hidden" id="c_nombre" size="10" value="<%=nombre%>">
</td>
            </tr>
          
		   <tr class="fila">
            <td height="28">Banco:</td>
            <td colspan="2"><input:select name="c_banco"      attributesText="<%=" id='c_banco' style='width:80%;' class='listmenu'  onChange=\\" bancos(this.value,'"+CONTROLLER+"');\\" "%>"  default="<%=c_banco%>" options="<%=b%>" /></td>
            </tr>
		   <tr class="fila">
            <td>Sucursal:</td>
            <td colspan="2"><input:select name="c_sucursal"    attributesText=" id='c_sucursal' style='width:80%;' class='listmenu'" default="" options="<%=sbancos%>" /></td>
            </tr>
      </table>
	  </td>
	</tr>
  </table>
  <table width="519" height="68" border="2" align="center">
	<tr >
		<td >
                <table width="100%" class="tablaInferior">
			<tr class="tablaInferior">
			<td width="271" height="24"  class="subtitulo1"><p align="left">Criterios Totales </p></td>
			<td width="224"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
			</tr>
		  </table>
			<table width="100%" align="center" class="tablaInferior" >
          <tr class="fila">
            <td width="182" height="44">Mostrar Totales por Factura:</td>
            <td width="47" ><input name="c_criterio_t" id="c_criterio_t" type="radio" value="detalle" onClick="setRadio(this.value);"></td>
            <td width="204" >Mostrar Totales por Proveedor  :           
            <td width="54" ><input name="c_criterio_t" id="c_criterio_t" type="radio" value="resumen" onClick="setRadio(this.value);">            
          </tr>
      </table>
	  </td>
	</tr>
  </table>
  <table width="521" height="68" border="2" align="center">
	<tr >
		<td >
                <table width="100%"  class="tablaInferior">
			<tr class="tablaInferior">
			<td width="271" height="24"  class="subtitulo1"><p align="left">Criterios de Despliegue</p></td>
			<td width="226"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
			</tr>
		  </table>
			<table width="100%" align="center" class="tablaInferior" >
          <tr class="fila">
            <td width="183">Generar Reporte en Pantalla:</td>
            <td width="48" ><input name="c_criterio_d" id="c_criterio_d" type="radio" value="si"></td>
            <td width="204" >Generar Archivo de Excel:           
            <td width="54" ><input name="c_criterio_d"  id="c_criterio_d" type="radio" value="no">            
          </tr>
      </table>
	  </td>
	</tr>
  </table>
  <p align="center"><img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="validar('<%=CONTROLLER%>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; <img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir..." onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></p>
</form>
</body>
</body>
</html>
    
