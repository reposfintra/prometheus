<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de realizar y mostrar busqueda de los nits de proveedores
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script language="javascript">
    function addOption(Comb,valor,texto){
        if(valor!='' && texto!=''){
            var Ele = document.createElement("OPTION");
            Ele.value=valor;
            Ele.text=texto;
            Comb.add(Ele);
        }
    }    
    
    function empanada(nit,nombre,banco,sucursal){
        var combo = parent.opener.document.forma1.c_proveedores;
        combo.value=nit;
        var n = parent.opener.document.forma1.c_nombre;
        n.value=nombre;
        var nombre_p = parent.opener.document.getElementById("nombre_p");
        nombre_p.innerHTML=""+nombre;
        var combo = parent.opener.document.forma1.c_banco;
        var combo2 = parent.opener.document.forma1.c_sucursal;
        var Ele = parent.opener.document.createElement("OPTION");
        Ele.value=sucursal;
        Ele.text=sucursal;
        combo2.add(Ele);
	for( var i=0; i<combo.length; i++ ){
            if (combo[i].value == banco ) {
                combo.selectedIndex = i;
                break;
            }
	}
	
	for( var i=0; i<combo2.length; i++ ){
            if (combo2[i].value == sucursal) {
                combo2.selectedIndex = i;
                break;
            }
	}
	parent.close();	
    }

    function procesar (element){
        if (window.event.keyCode==13) {
            listaProveedores(CONTROLLER);
 		}
    }
	
	
	
	function listaProveedores(CONTROLLER){
		document.forma1.action = CONTROLLER+"?estado=Factura&accion=ProveedoresnitR"; 
		document.forma1.submit(); 
	} 
	
</script>
<title>JSP Page</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onLoad="forma1.proveedor.focus();" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Buscar Nit"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
  String accion = "" +request.getParameter("accion");
%>
<form name="forma1" action="" method="post">      
  <table border="2" align="center" width="555">
    <tr>
    <td width="610" height="73">
        <table width="100%" class="tablaInferior">
          <tr>
            <td width="295" height="24"  class="subtitulo1"><p align="left">Documento </p></td>
            <td width="265"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
        </table>        
        <table width="547" border="1" align="center" bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr class="fila">
            <td width="107" height="30">Id del Proveedor: </td>
            <td width="424" > 
              <input name="proveedor" type="text" class="textbox" id="proveedor" size="18" maxlength="15"   onKeyUp="procesar(this);">
<img src='<%=BASEURL%>/images/botones/buscar.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Aceptar...'  onclick="listaProveedores('<%=CONTROLLER%>');" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'></td>
          </tr>
        </table></td>
</tr>
</table>

<%
    if (accion.equals("1")){
        Vector vProveedores=  model.proveedorService.getProveedores ();
%>
  <table width="555" border="2" align="center">
    <tr>
    <td width="556">
<table width="100%" class="tablaInferior">
          <tr>
    <td width="346" height="24"  class="subtitulo1"><p align="left">Proveedor</p></td>
    <td width="188"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
        <table width="551" border="1" align="center" bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr class="tblTitulo" id="titulos">
    <td width="79" height="22">Nit</td>
    <td width="456" >nombre</td>
    </tr>
<%
        for(int i=0; i< vProveedores.size();i++){
            Proveedor o_proveedor=(Proveedor)vProveedores.elementAt(i);
%>  
  <tr class="fila">
    <td width="79" height="22"><%=o_proveedor.getC_nit()%></td>
    <td width="456" ><a  onClick="empanada('<%=o_proveedor.getC_nit()%>','<%=o_proveedor.getC_payment_name()%>','<%=o_proveedor.getC_branch_code()%>','<%=o_proveedor.getC_bank_account()%>')" style="cursor:hand" ><%=o_proveedor.getC_payment_name()%></a></td>
    </tr>
<%
        }
    }
%>  
</table>
</td>
</tr>
</table>
	<p>
    </p>     
</form>
</div>
</body>
</html>
