<%--
- Autor : JPacosta
- Date  : 2013
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de gestionar el formulario para la
                            insercion de documentos por pagar
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head><title>Documentos por pagar</title>
        <link href="../../../css/estilostspFactura.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/estilostspFactura.css" rel="stylesheet" type="text/css">

        <script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        <script src="<%=BASEURL%>/js/boton.js" type="text/javascript"></script>
        <script src="<%=BASEURL%>/js/prototype.js" type="text/javascript"></script>

        <script type='text/javascript' src="<%= BASEURL %>/js/cxp.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.min.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/jquery/jquery-ui/jquery.alphanumeric.pack.js"></script>

        <style type="text/css">
            <!--
            .Estilo1 {color: #D0E8E8}
            -->
        </style>
    </head>

    <% 
       String iva =""; 
       String vlr_rica   = "";
       String vlr_riva   = "";
       String vlr_rfte   = "";
       model.tblgensvc.buscarAutXCP();
       List ListAutxcp = model.tblgensvc.getAutXCP();
       String Modificar          =  (request.getParameter ("Modificar")==null?"":request.getParameter ("Modificar"));

       String maxfila            =  (request.getParameter ("maxfila")==null?"1":request.getParameter ("maxfila")); 
       int    MaxFi              =  Integer.parseInt(maxfila);
       String nArchivo           =  (request.getParameter("nombrearchivo")==null?"":request.getParameter("nombrearchivo"));
   
       String id_agencia_usuario =  (String)session.getAttribute("id_agencia" );  
       String validar_agencia    =  (request.getParameter ("ag")==null?"":request.getParameter ("ag"));  
   
       String cod_agencia_cont   =  (String)session.getAttribute("agContable");
       String unidad_contable    =  (String)session.getAttribute("unidadC");
     
       String codigocliente      =  (request.getParameter ("cliente")==null?"":request.getParameter ("cliente")); 
       String opcion 		 =  (request.getParameter ("op")==null?"NO":request.getParameter ("op")); 
       String indice		 =  (request.getParameter ("indice")==null?"":request.getParameter ("indice")); 
       String cod_cuenta	 =  (request.getParameter ("cuenta")==null?"":request.getParameter ("cuenta"));    
   
       String foco               = 	(request.getParameter ("foco")==null?"tipo_documento":request.getParameter ("foco"));  
    %>
    <body  <%if (opcion.equals("cargar")){%>  
        onLoad="LoadCodigo('<%=indice%>');" 
        <%} else if (opcion.equals("cargarB")){%>
          onLoad = "document.forma1.<%=foco%>.focus();asignarImpuesto(); " 
        <%}else{%>onResize="redimensionar()" 
        <%if(request.getParameter("reload")!=null){%>
        onLoad = "redimensionar();
                parent.opener.location.href = '<%=BASEURL%>/jsp/cxpagar/facturasxpagar/facturaP.jsp?';
                parent.close();" 
        <%} else {%>
        onLoad = "redimensionar();"
        <%}
        }%>
        >


        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Documentos por Pagar"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <%


                    java.util.Date utilDate = new java.util.Date(); //fecha actual
                long lnMilisegundos = utilDate.getTime();
                java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(lnMilisegundos);
	
                CXP_Doc doc;
                doc = model.cxpDocService.getFactura();
						
                String subtotal=(request.getParameter ("total")==null?"":request.getParameter ("total"));
                String tipo_documento="";
                String documento= ""; 
                String proveedor="";
                String tipo_documento_rel ="";
                String documento_relacionado="";
                String fecha_documento=(""+sqlTimestamp).substring(0,10);
                String c_banco="BANCOLOMBIA";
                String b_sucursal="";
                String descripcion="";
                String observacion="";
                String o_moneda="PES";
                double vlr_neto=0;
                    double vlr_total=0;
                    double total_neto =0;
                String usuario_aprobacion="";
                int plazo =0;
                    String moneda_banco = "";
	
                    //Ivan DArio 28 Octubre 2006
                    String agenciaBanco="";
                    //////////////////////////
	
                    String CabIva  = "";
                    String CabRiva = "";
                    String CabRica = "";
                    String CabRfte = ""; 
                    //Ivan 08 agosto 2006
                    List Listdocumentos = model.cxpDocService.getDocumentos();
	
                    Usuario usuarioLogin  = (Usuario) session.getAttribute("Usuario");
                String us  = usuarioLogin.getLogin();
                String agencia = usuarioLogin.getCia();
                String handle_code="";
                String id_mims="";
                String fecha_aprobacion="";
                    String usu_ap =""; 

                String op= (request.getParameter("op")!=null )?request.getParameter("op"):"";
                String para1 = "/jsp/cxpagar/facturasxpagar/buscarnit.jsp?fechadoc="+fecha_documento+"&plazo="+plazo;
                para1=Util.LLamarVentana(para1, "Buscar Proveedor");
                String HC ="";
                    String beneficiario="";
                String nombre_proveedor="";
                    String agenteRet = "N";
                    String rica      = "N";
                    String rfte      = "N";
                    String riva      = "N";
	
	
                    double saldo_anterior=0;
                    double saldo_me_anterior=0;
	
                if(doc!=null){
		
                    tipo_documento=""+doc.getTipo_documento();
                    documento= ( doc.getDocumento() != null )?doc.getDocumento():"";
                    proveedor = ""+doc.getProveedor();
                        Proveedor prov  = model.proveedorService.obtenerProveedorPorNit(proveedor);
                    if( prov!=null ){
                                agenteRet = prov.getC_agente_retenedor();
                                    rica      = prov.getC_autoretenedor_ica();
                        rfte      = prov.getC_autoretenedor_rfte();
                        riva      = prov.getC_autoretenedor_iva(); 
                                    beneficiario = prov.getNom_beneficiario();
                        }
		
                        tipo_documento_rel =""+doc.getTipo_documento_rel();
                    documento_relacionado=""+doc.getDocumento_relacionado();
                    fecha_documento = (doc.getFecha_documento() != null )?doc.getFecha_documento() :"";
                    
                        //c_banco = doc.getBanco();
                        model.servicioBanco.loadSusursales(c_banco);
                        b_sucursal  = doc.getSucursal();
                        moneda_banco = (doc.getMoneda_banco() != null)?doc.getMoneda_banco():"";
                        //Ivan DArio 28 Octubre 2006
                        agenciaBanco = (doc.getAgenciaBanco() != null)?doc.getAgenciaBanco():"";
                        HC = (doc.getHandle_code() != null)?doc.getHandle_code():"";
                        usu_ap = doc.getAprobador();
                        usuario_aprobacion=doc.getUsuario_aprobacion();
                    
                    descripcion =  ( doc.getDescripcion() != null )?doc.getDescripcion():"";
                    observacion =   ( doc.getObservacion() != null)?doc.getObservacion() :"";
                    o_moneda    =  ( doc.getMoneda() != null )?doc.getMoneda():"PES";
                    vlr_neto = doc.getVlr_total();
                            vlr_total = doc.getVlr_neto();
                            total_neto = doc.getTotal_neto();
                            //Ivan 21 sep
                            saldo_anterior = doc.getValor_saldo_anterior();
                            saldo_me_anterior = doc.getValor_saldo_me_anterior();
                            //System.out.println("saldo_anterior  " + doc.getValor_saldo_anterior());
                            //System.out.println("saldo_me_anterior  "+ doc.getValor_saldo_me_anterior());
		
                            //out.println("<br><br><br><br><br><br> "+vlr_neto);
                    plazo=doc.getPlazo();
		
                            ////////////////////////////////////////////////////////////////
		
                            fecha_aprobacion = doc.getFecha_aprobacion();
		
                            CabIva  = (doc.getIva()!= null )?doc.getIva():"";
                        CabRiva = (doc.getRiva()!= null )?doc.getRiva():"";
                        CabRica = (doc.getRica()!= null )?doc.getRica():"";
                        CabRfte = (doc.getRfte()!= null )?doc.getRfte():"";
		
                }
 
               if (proveedor.equals("null")||proveedor.equals("")){
                    proveedor ="";
                    if (!nArchivo.equals("")) {     //Acosta Jesid
                        c_banco="BANCOLOMBIA";
                        b_sucursal="CA";
                        moneda_banco = "PES";
                        agenciaBanco = "BQ";
                        HC = "NP";
                        usu_ap = "";
                        usuario_aprobacion = "";
                    }
                }
                else{
                    Proveedor o_proveedor = model.proveedorService.obtenerProveedorPorNit(proveedor);
                    if( o_proveedor!=null ){
                        agenteRet = o_proveedor.getC_agente_retenedor();
                        rica      = o_proveedor.getC_autoretenedor_ica();
                    rfte      = o_proveedor.getC_autoretenedor_rfte();
                    riva      = o_proveedor.getC_autoretenedor_iva();
       
                        id_mims=""+o_proveedor.getC_idMims();

                        nombre_proveedor = o_proveedor.getC_payment_name(); 
                                    //System.out.println("NOMBRE PROVEEDOR   "+nombre_proveedor);
                    }
                }
                model.cxpDocService.setFactura(null);
  
    
                List listHC = model.cxpDocService.getListHC();
                TreeMap codCliAre = model.cxpDocService.llenarCodCliAre ();
    
    
                TreeMap b = model.servicioBanco.getBanco();
                //TreeMap sbancos = model.servicioBanco.getSucursal();
                    Vector sbancso      = model.servicioBanco.getVecBancos(); 
                TreeMap t_moneda = model.monedaService.obtenerMonedas();
                TreeMap documentos = model.documentoSvc.obtenerDocumentosT();
               // TreeMap u = model.usuarioService.getUsuariosAprobacionT();
                TreeMap u = model.tblgensvc.getUsuarios_aut();
                Vector vTiposImpuestos=model.TimpuestoSvc.vTiposImpuestos();
	
                    TreeMap agencias   = model.agenciaService.getAgencia();
                    Vector VecAgencias = (model.cxpDocService.getAgencias() == null)
                                            ? new Vector() 
                                            : model.cxpDocService.getAgencias();
                int num_items=1;
   // num_items=Integer.parseInt(""+request.getParameter("num_items"));
                Vector vItems = model.cxpItemDocService.getVecCxpItemsDoc();
                    //System.out.println("TIPO DOC REL   "+tipo_documento_rel);
                String DescDocRel = (tipo_documento_rel.equals("FAP")?"FACTURAS":(tipo_documento_rel.equals("NC")?"NOTA CREDITO":(tipo_documento_rel.equals("ND")?"NOTA DEBITO":"") ));
                    String DescDoc = (tipo_documento.equals("FAP")?"FACTURAS":(tipo_documento.equals("NC")?"NOTA CREDITO":(tipo_documento.equals("ND")?"NOTA DEBITO":"" )));
                    String concepto ="";
                    String descripcion_i="";
                    //Ivan 26 julio 2006
                    String ree  = "";
                    String Ref3 = "";
                    String Ref4 = "";
                    String agenciaItem ="";
                    //////////////////////
                String codigo_cuenta="";
                String codigo_abc="";
                String planilla="";
                    String tipcliarea="";
                    String codcliarea="";
                    String descliarea="";
                Vector vImpuestosI;
                    Vector Copia;
                    String [] codigos;
                double valor   = 0;
                    double valor_t = 0;
                    //Ivan 21 julio 2006
                    LinkedList tbltipo = null;
                    String auxiliar    ="";
                    String tipoSubledger="";
                    //////////////////////////////////
        
                    boolean ErrorCuenta = false;
	
		
		
                if (vItems == null){  	     
                    //System.out.println("el Vector de items es nulo ");
                }
                else{
                    //System.out.println("el Vector de items no es nullo "+ vItems.size()+" Max Fila "+MaxFi);
                    for(int i=0;i < MaxFi ;i++){
                        CXPItemDoc item = (CXPItemDoc)vItems.elementAt(i);
                                    //System.out.println("ITEM - "+ item.getConcepto());
                                    if(item.getConcepto() != null ){
                                            concepto        = ""+item.getConcepto();
                                            descripcion_i   = ""+ item.getDescripcion();
                                            ree             = ""+ item.getRee();
                                            Ref3            = ""+ item.getRef3();
                                            Ref4            = ""+ item.getRef4();				   
                                            codigo_cuenta   = ""+ item.getCodigo_cuenta();
                                            codigo_abc      = ""+ item.getCodigo_abc();
                                            planilla        = ""+ item.getPlanilla();
                                            valor	        = item.getVlr_me();			
                                            valor_t         = item.getVlr_total();
                                            tipcliarea      = ""+item.getTipcliarea ();
                                            codcliarea      = ""+item.getCodcliarea ();
                                            descliarea      = ""+item.getDescliarea ();
                                            vImpuestosI     = item.getVItems();
                                            iva             = item.getIva();
                                            codigos         = item.getCodigos();
				
                                            //ivan 21 julio 2006
                                            auxiliar        = item.getAuxiliar();     
                                            tbltipo         = item.getTipo();
                                            tipoSubledger   = item.getTipoSubledger();
                                            agenciaItem     = (item.getAgencia() != null)?item.getAgencia():id_agencia_usuario;
                                            ErrorCuenta     = item.isErrorCuenta();
                                
                                
                                            if (vImpuestosI == null){
                                                    out.print("<br> el vector de Impuestos es null ");
                                            }
				
                                            for(int ii=0;ii<vImpuestosI.size();ii++){
                                                    CXPImpItem impuestoItem = (CXPImpItem)vImpuestosI.elementAt(ii);
                                                    String impuesto = impuestoItem.getCod_impuesto();
                                            }
				
                                    }
			
                    }
		   
                }
                    //System.out.println("vector facturas");
                    Vector NumeroFacturas =  model.cxpDocService.getVFacturas ();
                    String estilo ="";
                    String clase = "textboxFactura";
                    String readonly="";
                    String eventoProveedor ="buscarNit('"+validar_agencia +"',maxfila,'ENTER')";
                    String eventoLupaProveedor = "buscarProveedores('"+CONTROLLER+"', '"+validar_agencia+"',maxfila);";
                    //System.out.println("If de modificar");
             if(Modificar.equals("si")){
                 estilo ="style='border:0'";
                 clase="filaresaltada";
                 readonly ="readonly" ;
                     eventoProveedor ="";
                     eventoLupaProveedor="";
             }
            //System.out.println("Fin Jsp");
            %>

            <script>
            var controlador = "<%=CONTROLLER%>";
            var maxfila = <%=maxfila%>;
	
            function formato(numero) {

                var tmp = parseInt(numero);
                var factor = (tmp < 0 ? -1 : 1);
                tmp *= factor;

                var num = '';
                var pos = 0;
                while (tmp > 0) {
                    if (pos % 3 == 0 && pos != 0)
                        num = ',' + num;
                    res = tmp % 10;
                    tmp = parseInt(tmp / 10);
                    num = res + num;
                    pos++;
                }
                return (factor == -1 ? '-' : '') + num;
            }



            function sinformato(element) {
                return element.replace(new RegExp(",", "g"), '');
            }

            function disabledPlanilla(x, validar, Modificar) {
                var comboPlanilla = document.getElementById('cod_oc' + x);
                var planilla = document.getElementById('planilla' + x);
                var descripcion = document.getElementById('descripcion_i' + x);
                var REE = document.getElementById('REE' + x);
                var centro = document.getElementById('oc' + x);
                var auxiliar = document.getElementById('auxiliar' + x);

                var CRE = REE.value.toUpperCase();
                if (planilla.value != "") {
                    if (CRE != "C" && CRE != "P") {

                        if (planilla.value.length != 0)
                            comboPlanilla.disabled = true;
                        else
                            comboPlanilla.disabled = false;

                        if (planilla.value != "") {
                            if (descripcion.value.length != 0) {
                                BuscarAccountCode(planilla.value, x, validar, Modificar);
                            }
                            else {
                                alert('Debe seleccionar un concepto de pago');
                            }
                        }
                    } else {
                        centro.value = "";
                        auxiliar.value = "";
                        enviar('?estado=Factura&accion=Clientes&OP=validarPlanilla&OC=' + planilla.value + '&x=' + x)
                    }
                }
            }

            function BuscarAccountCode(numpla, x, validar, Modificar) {
                var cod1 = document.getElementById("cod1" + x);
                var cod2 = document.getElementById("cod2" + x);
                var cod3 = document.getElementById("cod3" + x);
                var cod4 = document.getElementById("cod4" + x);
                var cod5 = document.getElementById("cod5" + x);
                var hidden = document.getElementById("cod_cuenta" + x);
                var cuenta = cod1.value + cod2.value + cod3.value + cod4.value + cod5.value;
                document.forma1.action = controlador + "?estado=FacturaSearch&accion=CodigoCuenta&numpla=" + numpla + "&indice=" + x + "&hidden=" + hidden.value + "&cuenta=" + cuenta + "&validar=" + validar + "&maxfila=" + maxfila + "&Modificar=" + Modificar;
                document.forma1.submit();
            }

            function buscarNit(validar, maxfila, OP) {

                var proveedor = document.getElementById("proveedor");
                if (proveedor.value != "") {
                    var ii = 0;
                    for (var i = 0; i < detalle.rows.length; i++) {
                        ii = ii + 1;
                    }
                    var num_items = document.getElementById("num_items");
                    num_items.value = "" + (ii - 1);
                    var url = "";
                    url = controlador + "?estado=Factura&accion=AProveedores&validar=" + validar + "&maxfila=" + maxfila + "&OP=" + OP;
                    /*for (var x=0;x<document.forma1.length;x++){
                     url =  url +"&"+ document.forma1.elements[x].name + "=" + document.forma1.elements[x].value;
                     }*/

                    document.forma1.action = url;
                    document.forma1.submit();
                }
            }

            function buscarDocRe(validar, maxfila, OP) {
                var proveedor = document.getElementById("proveedor");
                if (proveedor.value != "") {
                    buscarNit(validar, maxfila, OP);
                } else {
                    alert("Debe seleccionar un proveedor")
                    proveedor.focus();
                }

            }

            function buscarDatosProveedor(validar, maxfila, OP) {
                var proveedor = document.getElementById("proveedor");
                var documento_relacionado = document.getElementById("documento_relacionado");

                if (proveedor.value != "" && documento_relacionado.value == "") {
                    buscarNit(validar, maxfila, OP);
                }

            }

            //Ivan gomez 22 julio 2006
            /*var isIE = document.all?true:false;
             var isNS = document.layers?true:false;*/
            function BuscarConcepto(x, value, e, decReq) {
                var concepto = document.getElementById("descripcion_i" + x);

                if (concepto.value.length == 4) {

                    var url = "?estado=Factura&accion=Servicios&concepto=" + concepto.value + "&OP=ENTER&Id=" + x;
                    enviar(url)

                }


                //ivan 22 julio 2006
                /*soloDigitos(e,decReq);*/


            }

            function asignarImpuesto() {
                var suma_total = 0;
                var suma = 0;
                var moneda = document.getElementById("moneda");
                st = document.getElementById("total");
                total_neto = document.getElementById("total_neto");
                //alert(detalle.rows.length);	
                for (var i = 1; i <= maxfila + 1; i++) {
                    var vlrNeto = document.getElementById("valorNeto" + i);
                    var subtotal = document.getElementById("valor1" + i);
                    var imp = document.getElementById("iva" + i);
                    var vlr_riva = document.getElementById("vlr_riva" + i);
                    var vlr_rica = document.getElementById("vlr_rica" + i);
                    var vlr_rfte = document.getElementById("vlr_rfte" + i);

                    if (subtotal != null) {
                        var valor = subtotal.value.replace(new RegExp(",", "g"), "");
                        var vlr = "";
                        var vlrImpuesto = "";

                        if (!isNaN(parseFloat(valor))) {
                            var vnet = parseFloat(valor);
                            var vlr_iva_aplicado = 0;
                            if (imp.value != "") {

                                vlr = imp.value;
                                //alert("IMP  "+vlr)
                                vlr = parseFloat(vlr);
                                vlrImpuesto = (valor * vlr) / 100;

                                vlrImpuesto = (moneda.value == 'DOL') ? redondear(vlrImpuesto) : Math.round(vlrImpuesto);
                                vlr_iva_aplicado = vlrImpuesto;
                                vnet += parseFloat(vlrImpuesto);
                                suma += parseFloat(vlrImpuesto);
                                suma_total += parseFloat(vlrImpuesto);
                                //alert("SUMA "+suma)
                            }
                            if (vlr_riva.value != "") {
                                vlr_iva_aplicado = parseInt(vlr_iva_aplicado);
                                vlr = vlr_riva.value;
                                //alert("IMP  "+vlr)
                                vlr = parseFloat(vlr);
                                vlrImpuesto = (vlr_iva_aplicado * vlr) / 100;

                                vlrImpuesto = (moneda.value == 'DOL') ? redondear(vlrImpuesto) : Math.round(vlrImpuesto);
                                suma_total -= parseFloat(vlrImpuesto);

                            }
                            if (vlr_rica.value != "") {
                                vlr = vlr_rica.value;
                                //alert("IMP  "+vlr)
                                vlr = parseFloat(vlr);
                                vlrImpuesto = (valor * vlr) / 100;

                                vlrImpuesto = (moneda.value == 'DOL') ? redondear(vlrImpuesto) : Math.round(vlrImpuesto);
                                suma_total -= parseFloat(vlrImpuesto);

                            }
                            if (vlr_rfte.value != "") {
                                vlr = vlr_rfte.value;
                                //alert("IMP  "+vlr)
                                vlr = parseFloat(vlr);
                                vlrImpuesto = (valor * vlr) / 100;
                                vlrImpuesto = (moneda.value == 'DOL') ? redondear(vlrImpuesto) : Math.round(vlrImpuesto);
                                suma_total -= parseFloat(vlrImpuesto);

                            }
                            //Ivan Dario Gomez 24 julio 2006
                            if (moneda.value == 'DOL') {
                                vlrNeto.value = vnet;
                                formatoDolar(vlrNeto, 2);
                            } else {
                                vlrNeto.value = formato(vnet);
                            }
                            //////////////////////////////////////////
                            suma += parseFloat(valor);
                            suma_total += parseFloat(valor);

                        }
                    }
                }

                if (moneda.value == 'DOL') {
                    st.value = suma;
                    formatoDolar(st, 2);
                    total_neto.value = suma_total;
                    formatoDolar(total_neto, 2);
                } else {
                    st.value = formato(suma);
                    total_neto.value = formato(suma_total);
                }
            }

            function cambiar(dato) {

                var vlr_neto = document.getElementById("vlr_neto");
                var vlNet = vlr_neto.value.replace(new RegExp(",", "g"), "");
                if (dato == 'DOL') {

                    vlr_neto.onkeypress = new Function(" Digitos(event, 'decOK'); ");
                    vlr_neto.value = Fdolar(vlNet, 2);
                    vlr_neto.onchange = new Function(" formatoDolar(this,2); ");

                } else {
                    vlr_neto.onkeypress = new Function(" Digitos(event, 'decNO'); ");
                    vlr_neto.value = formato(Math.round(vlNet));
                    vlr_neto.onchange = new Function(" formatear(this); ");
                }

                for (var i = 1; i <= maxfila + 1; i++) {

                    valor = document.getElementById("valor1" + i);
                    valorNeto = document.getElementById("valorNeto" + i);
                    if (valor != null) {
                        var val = valor.value.replace(new RegExp(",", "g"), "");
                        var valNeto = valorNeto.value.replace(new RegExp(",", "g"), "");
                        if (dato == 'DOL') {
                            valor.onkeypress = new Function(" Digitos(event, 'decOK'); ");
                            valorNeto.onkeypress = new Function(" Digitos(event, 'decOK'); ");


                            formatoDolar(valor, 2);
                            formatoDolar(valorNeto, 2)


                            valor.onchange = new Function(" formatoDolar(this,2); ");
                            valorNeto.onchange = new Function(" formatoDolar(this,2); ");

                        } else {
                            valor.onkeypress = new Function(" Digitos(event, 'decNO'); ");
                            valorNeto.onkeypress = new Function(" Digitos(event, 'decNO'); ");

                            valor.value = formato(Math.round(val));
                            valorNeto.value = formato(Math.round(valNeto));

                            valor.onchange = new Function(" formatear(this); ");
                            valorNeto.onchange = new Function(" formatear(this); ");
                        }
                    }

                }
                asignarImpuesto();
            }

            function formatoDolar(obj, decimales) {
                var numero = obj.value.replace(new RegExp(",", "g"), "");
                var nums = (new String(numero)).split('.');
                var salida = new String();

                var TieneDec = numero.indexOf('.');
                var dato = new String();
                if (TieneDec != -1) {
                    var deci = numero.split('.');
                    var dec = (deci[1].length > 2) ? deci[1].charAt(2) : deci[1].substr(0, deci[1].length);

                    if (dec > 5) {
                        dato = (parseInt(deci[1].substr(0, 2)) + 1);
                        if (dato > 99) {
                            nums[0] = new String(parseInt(nums[0]) + 1);
                            obj.value = nums[0] + '.00';
                        }
                    }
                }
	   var signo = (parseFloat(nums[0])<0?-1:1);
	   nums[0] = new String(parseFloat(nums[0])*signo);
  	   for (var i=nums[0].length-1, j=0; i>=0; salida = nums[0].charAt(i) + (j%3==0 && j!=0? ',':'') + salida , i--, j++);
	   obj.value = salida + (nums.length > 1 && decimales > 0 ? '.'+((nums[1].length >2)?((nums[1].charAt(2)>5)?(parseInt(nums[1].substr(0,2))+1):nums[1].substr(0,2)):(nums[1].length==1)?nums[1].substr(0,1)+'0':nums[1].substr(0,nums[1].length)) : '.00'); 
	   if (signo==-1) obj.value = "-" + obj.value;

            }

            function redondear(valor) {
                valor = new String(valor);
                var nums = valor.split('.');
                return nums[0] + (nums.length > 1 ? '.' + ((nums[1].length > 2) ? ((nums[1].charAt(2) > 5) ? (parseInt(nums[1].substr(0, 2)) + 1) : nums[1].substr(0, 2)) : (nums[1].length == 1) ? nums[1].substr(0, 1) + '0' : nums[1].substr(0, nums[1].length)) : '.00');

            }

            function vrificarRet() {
                agenteRet = document.getElementById("agenteRet");
                agenteRica = document.getElementById("agenteRica");
                agenteRfte = document.getElementById("agenteRfte");
                agenteRiva = document.getElementById("agenteRiva");
                if (agenteRet.value == 'N') {
                    msg = (agenteRica.value == 'S') ? " RICA " : "";
                    msg += (agenteRfte.value == 'S') ? " RFTE " : "";
                    msg += (agenteRiva.value == 'S') ? " RIVA " : "";

                    sw = 0;
                    for (var i = 1; i <= maxfila; i++) {
                        var valor = "valor1" + i;
                        valorItem = document.getElementById(valor);
                        if (valorItem != null) {
                            riva = document.getElementById("impuesto1" + i);
                            rica = document.getElementById("impuesto2" + i);
                            rfte = document.getElementById("impuesto3" + i);
                            if (riva.value == '' && agenteRiva.value == 'S') {
                                sw = 1;
                                break;
                            } else if (rica.value == '' && agenteRica.value == 'S') {
                                sw = 1;
                                break;
                            } else if (rfte.value == '' && agenteRfte.value == 'S') {
                                sw = 1;
                                break;
                            }

                        }
                    }

                    if (sw == 1) {
                        return (confirm('El proveedor requiere los tipos de impuestos: ' + msg + '  y algunos de sus items no presentan impuesto, si desea continuar presione ACEPTAR.\n\nSi desea verificar y aplicar los impuestos persione CANCELAR. '));
                    } else {
                        return true;
                    }

                } else {
                    return true;
                }
            }
            
            function cambiarCheckBoxes(value) {
                var todos = (value === 'sub')
                        ? true
                        : document.getElementById('chkG').checked;
                for(i = 0; i < detalle.rows.length; i++) {
                    if (value === 'sub') {
                        if (document.getElementById("chk"+i).checked === false) {
                            todos = false; break;
                        }
                    } else {
                        document.getElementById("chk"+i).checked = todos;
                    }
                } document.getElementById('chkG').checked = todos;
            }
//(nums[1].substr(0, (nums[1].length>decimales?decimales:nums[1].length)))



            </script>
            <script type="text/javascript">
                $(document).ready(
                        function() {
                            $('#documento').alphanumeric({allow:"a"});
                        });

            </script>
            <form name="forma1" id="forma1" action="" method="post"> 
            <%
                  String mensaje=""+request.getParameter("ms");
                    if (! (mensaje.equals("")|| mensaje.equals("null"))){
                %>

                <input type="hidden" name="Modificar"  id="Modificar" value="<%=Modificar%>">
                <table border="2" align="center">
                    <tr>
                        <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                <tr>
                                    <td width="282" align="center" class="mensajes"><%=mensaje%></td>
                                    <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                    <td width="78">&nbsp;</td>
                                </tr>
                            </table></td>
                    </tr>
                </table>
                <br>
                <%
                    }   
                %>

                <table width="980" align="center">
                    <tr><td>
                            <table border="2"  >
                                <tr >
                                    <td> 
                                        <table width="100%" align="center"  >
                                            <tr >
                                                <td width="50%"  class="subtitulo1"><p align="left">Documento</p></td>
                                                <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" class="Estilo1" height="20" align="left"><%=datos[0]%></td>
                                            </tr>
                                        </table>
                                        <table width="100%" align="center" cols="7">
                                            <tr class="filaFactura">
                                                
                                        <td class="filaFactura">C&oacute;digo Cuenta:</td>
                                        <td class="filaFactura">
                                            <input id="codigo_cuenta0" class="textareaFactura" type="text" value="<%=cod_cuenta%>" onblur="VerificarCuenta('0');" maxlength="10" size="6" 
                                                   style="text-transform:uppercase; width:100%" name="cod_cuenta">
                                        </td>
                                                <td width="103" class="filaFactura" >Documento:</td>
                                                <td width="117" class="filaFactura">

                                                    <input type="hidden" name="agenteRet" value="<%=agenteRet%>">
                                                    <input type="hidden" name="agenteRica" value="<%=rica%>">
                                                    <input type="hidden" name="agenteRfte" value="<%=rfte%>">
                                                    <input type="hidden" name="agenteRiva" value="<%=riva%>">

                                                    <input type="hidden" name="fecha_aprobacion" value="<%=fecha_aprobacion%>">
                                                    <input type="hidden" name="usu_ap" value="<%=usu_ap%>"> 
                                                    <input type="hidden" name="saldo_me_anterior" value="<%=saldo_me_anterior%>">
                                                    <input type="hidden" name="saldo_anterior" value="<%=saldo_anterior%>"> 
                                                    <input type="hidden" name="beneficiario" value="<%=beneficiario%>">

                                            <input:select name="tipo_documento"      attributesText=" id='documentos' style='width:80%;'  onChange=\"docRel(this.value);buscarDatosProveedor('<%=validar_agencia%> ',maxfila,'ENTER');\""  default="<%=tipo_documento%>" options="<%=documentos%>" />
                                        </td>
                                            
                                <td >Fecha :</td>
                                <td ><input name="fecha_documento" type="text"  id="fecha_documento2" size="11" value="<%=fecha_documento%>" onBlur="forma1.plazo.focus();" readonly>
                                    <span class="comentario"></span> <a href="javascript:void(0)" onClick="if (self.gfPop)
                        gfPop.fPopCalendar(document.forma1.fecha_documento);
                    return false;" hidefocus> <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a>			</td>				
                                <td width="121" colspan="-1" >Plazo(d&iacute;as):</td>
                                <td width="266" colspan="-1" >
                                    <div  id="CabPlazo">
                                        <%if( tipo_documento.equals("FAP") || tipo_documento.equals("") ){%>
                                        <input name="plazo"  type="text"  value="<%=plazo%>" onKeyPress="soloDigitos(event,'decNo');"  id="plazo2" size="4" maxlength="3">
                                        <%}else{%>
                                        <input name="plazo"  type="text"  readonly value="<%=plazo%>" class='filaresaltada' onKeyPress="soloDigitos(event,'decNo');" style='border:0; width:30; '  id="plazo2" size="4" maxlength="3">
                                        <%}%>
                                    </div>
                                </td>
                                <script>
                forma1.plazo.value = '<%=plazo%>';
                                </script>
                    </tr>
                    <tr class="filaFactura">
                        <td >Documento Rel:</td>
                                    <td >
                                <input:select name="tipo_documento_rel"      attributesText=" id='tipo_documento_rel' style='width:80%;'   onChange=\"buscarDocRe('<%=validar_agencia%> ',maxfila,'ENTER');\" ');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\"');\""  default="FAP" options="<%=documentos%>" />
                                
                                </td>
                                <td >
                                    <select name="documento_relacionado"  id="documento_relacionado"   style='width:150px;'  disabled  onChange="asignarDocumento(this.options[this.selectedIndex].plazo, this.options[this.selectedIndex].banco, this.options[this.selectedIndex].sucursal, this.options[this.selectedIndex].monbanco, this.options[this.selectedIndex].moneda, this.options[this.selectedIndex].aprobador, this.options[this.selectedIndex].agenciaDocumento, this.options[this.selectedIndex].handle);
                    cambiar(this.options[this.selectedIndex].moneda);">
                                        <option value=""  plazo ="" banco ="" sucursal="" monbanco="" moneda="" aprobador ="" agenciaDocumento ="" handle=""> </option>
                                        <% 
                      
                                          if (Listdocumentos!= null){
                               for(int i=0;i<Listdocumentos.size();i++){
                                                  CXP_Doc docxx = (CXP_Doc)Listdocumentos.get(i);  %>
                                        <option plazo ="<%=docxx.getPlazo()%>" banco ="<%=docxx.getBanco()%>" sucursal="<%=docxx.getSucursal()%>" monbanco="<%=docxx.getMoneda_banco()%>" moneda="<%=docxx.getMoneda()%>" aprobador="<%=docxx.getUsuario_aprobacion()%>" agenciaDocumento="<%=docxx.getAgenciaBanco()%>"  handle="<%=docxx.getHandle_code()%>" value ="<%=docxx.getDocumento()%>" <%if(docxx.getDocumento().equals(documento_relacionado)){%>selected<%}%>  >  <%=docxx.getDocumento()%></option> 
                                        <% }
			  }%>  
                                    </select>
                                    <script>
                docRel(forma1.tipo_documento.value)
                                    </script>
                                    

                                </td> 
                     
                    <td >Moneda:</td>
                    <td colspan="2" >
                        <div  id="CabMoneda">
                            <%if( tipo_documento.equals("FAP") || tipo_documento.equals("") ){%>
			     <input:select name="moneda" attributesText=" id='moneda' style='width:80%;'  onChange='cambiar(this.value)'  "  default="<%=o_moneda%>" options="<%=t_moneda%>" />
                            <%}else{%>
                            <input name='moneda'  type='text' readonly  value='<%=o_moneda%>'  class='filaresaltada' style='border:0; width:150px;'  id='moneda'>
                            <%}%>
                        </div></td>		                     
                    <td colspan="-1" >Banco:</td>	
                    <td colspan="-1" >
                        <div  id="CabBanco">
                            <%if( (tipo_documento.equals("FAP") || tipo_documento.equals("")) && nArchivo.equals("")){%>
                            <input:select name="c_banco" attributesText="id='c_banco' style='width:150px;' onChange='bancos();'"  default="<%=c_banco%>" options="<%=b%>" />
                            <input type="hidden" name="CONTROLLER" id="CONTROLLER" value="<%=CONTROLLER%>"/>
                            <input type="hidden" name="validar_agencia" id="validar_agencia" value="<%=validar_agencia%>"/>
                            <input type="hidden" name="maxfila_banco" id="maxfila_banco" value="<%=maxfila%>"/>
                            <input type="hidden" name="Modificar" id="Modificar" value="<%=Modificar%>"/>
                            <%}else{%>
                            <input name='c_banco'  type='text' readonly  value='<%=c_banco%>'  class='filaresaltada' style='border:0; width:150px;'  id='c_banco'>
                            <%}%>	
                        </div></td>		
                    <script>
                forma1.moneda.value = '<%=o_moneda%>';
                forma1.c_banco.value = '<%=c_banco%>';
                    </script>
                    </tr>
                    <tr class="filaFactura">
                        <td >Descripci&oacute;n:</td>
                        <td colspan="5"><textarea name="descripcion" id="descripcion" cols="70" rows="1" class="textareaFactura"><%=descripcion%></textarea></td>
                        <td colspan="-1" class="filaFactura">Sucursal:</td>
                        <td colspan="-1" ><div  id="CabSucursal">
                                <%if( (tipo_documento.equals("FAP") || tipo_documento.equals("")) && nArchivo.equals("")){%>
                                <!-- Ivan dario 2006-->
                                <select name="c_sucursal"    onBlur="forma1.observacion.focus();"    id='c_sucursal' style='width:80%;' onChange=" moneda_banco.value = this.options[this.selectedIndex].moneda;
                    agenciaBanco.value = this.options[this.selectedIndex].AgenciaBancoSucursal;" >
                                    <option  moneda ="" AgenciaBancoSucursal="" value=""></option>

                                    <%for(int i=0; i<sbancso.size(); i++){
                                             Banco banc = (Banco) sbancso.elementAt(i);
						
                                             if( banc.getBanco().matches(c_banco) ){
                                    %><option moneda='<%= banc.getMoneda() %>' AgenciaBancoSucursal = '<%=banc.getCodigo_Agencia()%>' value="<%=banc.getBank_account_no()%>"><%=banc.getBank_account_no()%></option><%
                                             }
                                     }  %>
                                </select>
                                <input type="text" name="moneda_banco"  class="filaresaltada" style="border:0; width:30; " value="<%=moneda_banco%>">
                                <%}else{%>
                                <input name='c_sucursal'  type='text' readonly value='<%=b_sucursal%>'  class='filaresaltada' style='border:0; width:150px;'  id='c_sucursal'><input type='text' name='moneda_banco'  class='filaresaltada' style='border:0; width:30; ' value='<%=moneda_banco%>'>
                                <%}%>
                            </div>
                        </td>		
                    <script>forma1.c_sucursal.value = '<%=b_sucursal%>';</script>
                    <!-- Ivan Dario Octubre 28 2006 -->
                    <input type='hidden' name='agenciaBanco' id='agenciaBanco' value='<%=agenciaBanco%>' >
                    <input type='hidden' name='agenciaBancoAnterior' id='agenciaBancoAnterior' value='<%=agenciaBanco%>' > 
                    </tr>

                    <tr>
                            <td rowspan="2" class="filaFactura">Observaci&oacute;n: </td>
                        <td colspan="5" rowspan="2" class="fila"><table width="100%">
                                <tr class="filaFactura">
                                    <td width="58%" rowspan="2"><textarea name="observacion"  cols="70" rows="3" class="textboxFactura"><%=observacion%></textarea></td>
                                    <td width="9%">IVA</td>
                                    <td width="13%" nowrap><input name="CabIva"  id ="CabIva" style='text-transform:uppercase' class='textareaFactura' type="text" size="6" maxlength="10"   onBlur="enterImpALL('IVA', 'CabIva', '<%=BASEURL%>', maxfila);
                    forma1.CabRica.focus();" value="<%=CabIva%>">
                                        &nbsp;<img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name="" width="15" height="15"  id="imageB" style="cursor:hand" title="Buscar"  onClick="funImpALL('IVA', 'CabIva', '<%=BASEURL%>', '', maxfila);"> </td>
                                    <td width="6%">&nbsp;</td>
                                    <td width="14%" nowrap>&nbsp;
                                    </td>
                                </tr>
                                <tr class="filaFactura">
                                    <td>RICA</td>
                                    <td nowrap><input name="CabRica" id ="CabRica"  class='textareaFactura' style='text-transform:uppercase' type="text" size="6" maxlength="10"   value="<%=CabRica%>" onBlur="enterImpALL('RICA', 'CabRica', '<%=BASEURL%>', maxfila);" <%if(!agenteRet.equals("S")&&(!rica.equals("S")) ){%>readonly<%}%>>
                                        &nbsp;<img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name="" width="15" height="15"  id="imageB" style="cursor:hand" title="Buscar" <%if((!agenteRet.equals("S"))&&(rica.equals("S"))){ %>onClick="funImpALL('RICA', 'CabRica', '<%=BASEURL%>', '', maxfila);" <%}%>></td>
                                    <td>RFTE </td>
                                    <td nowrap><input name="CabRfte" id ="CabRfte" type="text"  style='text-transform:uppercase' class='textareaFactura'  size="6" maxlength="10"   value="<%=CabRfte%>" onBlur="enterImpALL('RFTE', 'CabRfte', '<%=BASEURL%>', maxfila);" <%if(!agenteRet.equals("S")&&(!rfte.equals("S")) ){%>readonly<%}%>>
                                        &nbsp;<img src='<%=BASEURL%>/images/botones/iconos/lupa.gif'  name="" width="15" height="15"  id="imageB" style="cursor:hand" title="Buscar" <%if((!agenteRet.equals("S"))&&(rfte.equals("S"))){ %>onClick="funImpALL('RFTE', 'CabRfte', '<%=BASEURL%>', '', maxfila);" <%}%>></td>
                                </tr>
                            </table></td>
                        <td height="26" colspan="-1" class="filaFactura">Autorizador: </td>
                        <td colspan="-1"  class="filaFactura">
                            <div  id="CabAutorizador">
                                <%
                                 String tope_usu =""; 
                               if( (tipo_documento.equals("FAP") || tipo_documento.equals("") ||tipo_documento.equals("null")) && !nArchivo.equals("")){%>
                                <select name="usuario_aprobacion" style='width:80%;'  onchange='tope_usu.value = forma1.usuario_aprobacion.options[selectedIndex].tope;'>
                                    <% for(int i=0; i<ListAutxcp.size();i++){
                                  TablaGen t = (TablaGen) ListAutxcp.get(i);%>
                                    <option tope='<%=t.getReferencia()%>' value="<%=t.getTable_code()%>" <%=(usuario_aprobacion.equals(t.getTable_code()))?"selected":""%>><%=t.getTable_code()%></option>
                                    <%if(usuario_aprobacion.equals(t.getTable_code())){
                                           tope_usu = (t.getReferencia()!=null)?t.getReferencia():"";
                                      }%>
                                    <%}%>
                                </select>
                                <%}else{%>
                                <input name='usuario_aprobacion'  readonly type='text'  value='<%=usuario_aprobacion%>'  class='filaresaltada' style='border:0; width:150px;'  id='usuario_aprobacion'>
                                <%}%>
                                <input name='tope_usu' type="hidden" value='<%=tope_usu%>'>
                                <div>
                                    </td>
                                    </tr>
                                    <tr class="filaFactura">
                                        <td  >HC:</td>
                                        <td  >
                                            
                                            <input type="text" name="hc"  class="filaresaltada" style="border:0; width:30; " value="<%=HC%>">
                                            <br>
                                            
                                        </td>
                                    </tr>
                                    </table></td>
                                    </tr>
                                    </table>
                                    <table width="100%" border="2" align="left">
                                        <tr>
                                            <td >
                                                <table width="100%" align="center"  >
                                                    <tr >
                                                        <td width="50%"  class="subtitulo1"><p align="left">Detalle del Documento </p></td>
                                                        <td width="20%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" class="Estilo1">
                                                            <input name="num_items" id="num_items4" type="hidden" size="16" value='1' maxlength="16">
                                                        </td>
                                                        <td class="barratitulo" width="*" >
                                                            
                                                        </td>
                                                    </tr>
                                                </table>
                                                <div align="left">
                                                    <table id="detalle" width="100%" >
                                                        <tr  id="fila1" class="tblTituloFactura">

                                                            <td align="center" width="30">
                                                                <input type="checkbox" id="chkG" checked="true"  onchange="cambiarCheckBoxes('tab')"></td> 
                                                            <td align="center" width="50">numero</td>
                                                            <td align="center" width="100">cedula</td>
                                                            <td align="center">nombre completo</td>
                                                            <td align="center" width="100">valor a pagar</td>
                                                        </tr>
                                                    </table>
                                </div>		  </td>
                    </tr>
                </table>
                </td>
                </tr>
                </table>
                <tr>
                <script>importarNomina('<%=CONTROLLER%>','<%=nArchivo%>');</script>
                
                <div align="center">
                    <img src='<%=BASEURL%>/images/botones/aceptar.gif' name='Buscar' align="absmiddle" style='cursor:hand'   
                         onclick="if (detalle.rows.length > 1) {
                                    if(codigo_cuenta0.value == '') {
                                        alert('El numero de cuenta no puede estar vacio');
                                    } else {
                                        guardarNomina('<%=CONTROLLER%>', '<%=validar_agencia%>', maxfila, '<%=Modificar%>');
                                    }
                                  } else {alert('Lista vacia');}" 
                         onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
                    &nbsp;<img src='<%=BASEURL%>/images/botones/salir.gif' name='Buscar' align="absmiddle" style='cursor:hand'   onClick="window.close();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
                    
                </div>
                <tr>
            </form>
        </div>
    </body>
    <iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
    <%=datos[1]%>
    <script>
                function enviar(url) {
                    var a = "<iframe name='ejecutor' style='visibility:hidden'  src='<%= CONTROLLER %>" + url + "'> ";
                    aa.innerHTML = a;
                }
                function enviar2(url) {
                    var a = "<iframe name='ejecutor' style='visibility:hidden'  src='" + url + "'> ";
                    aa.innerHTML = a;
                }

    </script>
    <font id='aa'></font>
</html>
