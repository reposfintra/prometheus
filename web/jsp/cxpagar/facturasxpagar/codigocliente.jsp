<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de realizar y mostrar busqueda de los nits de proveedores
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Buscar Cliente</title>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script language="javascript">

	function listarClientes(x,CONTROLLER,cre){
		document.forma1.action = CONTROLLER+"?estado=Factura&accion=Clientes&x="+x+"&cre="+cre; 
		document.forma1.submit(); 
	} 
	function procesarClientes(element,x,CONTROLLER){
        if (window.event.keyCode==13){ 
            listarClientes(x,CONTROLLER);
		}	
    }
	
	function asignarCodigo(x,codigo,nombre,unidad){	
		var campo = parent.opener.document.getElementById("oc"+x);
		campo.value=codigo.substr(3,codigo.length);		
		campo.title=nombre;
		var texto=parent.opener.document.getElementById("doc"+x);
		texto.value=nombre;		
		var sele = window.opener.document.getElementById("codigo_cuenta"+x);	
		
		var cod1   =   window.opener.document.getElementById("cod1"+x);	
		var cod2   =   window.opener.document.getElementById("cod2"+x);	
		var cod3   =   window.opener.document.getElementById("cod3"+x);	
		var cod4   =   window.opener.document.getElementById("cod4"+x);	
		var cod5   =   window.opener.document.getElementById("cod5"+x);
		

		var cuenta = cod1.value + cod2.value + cod3.value + cod4.value + cod5.value;			
		var cod_cuenta = "";
		if ( codigo == 'SN' ){
		    if( cod2.value != '01' ){
			    cod4.value = '000';
				cod_cuenta  = cod1.value + cod2.value + cod3.value + cod4.value + cod5.value;			
				parent.close();	
			}
			else{
				var hijo=window.open("<%=BASEURL%>/jsp/cxpagar/facturasxpagar/codigoarea.jsp?x="+x+"&cuenta="+cuenta,'Trafico','width=700,height=520,scrollbars=no,resizable=yes,top=10,left=65,status=yes');
			}
		}
		else{
		    cod3.value = unidad;
			cod4.value =  codigo.substring(codigo.length - 3 , codigo.length );
			cod_cuenta  = cod1.value + cod2.value + cod3.value + cod4.value + cod5.value;			
			parent.close();	
		}
		
		sele.value = cod_cuenta;
		
    }
	
	function buscar (x,CONTROLLER){
	    if (window.event.keyCode==13) 
		   listarClientes(x,CONTROLLER);
	}
	
	function asignarAuxiliar(x, aux){
	   var auxiliar   = window.opener.document.getElementById("auxiliar"+x);	
	   auxiliar.value = aux; 
	   parent.close();	
	}
</script>
<title>JSP Page</title>
</head>

<body onLoad="forma1.cliente.focus();" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Buscar Clientes"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
  String x = (request.getParameter("x")==null?"":request.getParameter("x"));
  String accion = (request.getParameter("accion")==null?"":request.getParameter("accion"));
  String cre    = (request.getParameter("cre")!=null)?request.getParameter("cre"):"";
%>
<form name="forma1" action="<%=CONTROLLER%>?estado=Factura&accion=Clientes&x=<%=x%>&cre=<%=cre%>" method="post">      
  <table border="2" align="center" width="566">
    <tr>
    <td width="610" >
        <table width="100%" align="center"  >
          <tr>
            <td width="295" height="24"  class="subtitulo1"><p align="left">Cliente</p></td>
            <td width="265"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
          </tr>
        </table>        
        <table width="100%" align="center" >
          <tr class="fila">
            <td width="107" >Nombre: </td>
            <td width="424" > 
<input name="cliente" type="text" class="textbox" id="cliente" size="18" maxlength="15"  >
&nbsp;

<img src='<%=BASEURL%>/images/botones/buscar.gif' name='Buscar' align="absmiddle" style='cursor:hand' title='Aceptar...'  onclick="listarClientes('<%=x%>','<%=CONTROLLER%>','<%=cre%>');" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'></td>
          </tr>
        </table></td>
</tr>
</table>
<%
    if (accion.equals("1")){
      Vector vClientes = model.clienteService.getClientesVec();
%>
  <table width="566" border="2" align="center">
    <tr>
    <td width="585">
<table width="555"  >
          <tr>
    <td width="346" height="38"  class="subtitulo1"><p align="left">Clientes</p></td>
    <td width="193"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
  </tr>
</table>
        <table width="556" >
          <tr class="tblTitulo" id="titulos">
    <td width="56" height="22">Codigo</td>
    <td width="334" >Clientes</td>
    </tr>
<%
      for(int i=0; i< vClientes.size();i++){
        Cliente fila= (Cliente)vClientes.elementAt(i);
		
%>  
  <tr class="fila">
    <td width="56" height="22"><%=fila.getCodcli()%></td>
    <td width="334" >
	<% if(cre.equals("")){
		    if(fila.getUnidad().length() >3){%>
			<a  onClick="javascript: window.open('<%=BASEURL%>/jsp/cxpagar/facturasxpagar/listarUnidad.jsp?idcampo=<%=x%>&unidad=<%=fila.getUnidad()%>&codcli=<%=fila.getCodcli()%>&nombre=<%=fila.getNomcli()%>&opcion=LUPA','listar','width=300,height=120,scrollbars=no,resizable=yes,top=300,left=550,status=yes')" style="cursor:hand" ><%=fila.getNomcli()%></a>
		 <%}else{%>
			<a  onClick="javascript: asignarCodigo('<%=x%>','<%=fila.getCodcli()%>','<%=fila.getNomcli()%>','<%=fila.getUnidad()%>')" style="cursor:hand" ><%=fila.getNomcli()%></a>
		 <%}
	  }else{%>
	        <a  onClick="javascript: asignarAuxiliar('<%=x%>','<%=fila.getNit()%>')" style="cursor:hand" ><%=fila.getNomcli()%></a>
	  <%}%>
	</td>
    </tr>
<%
      }
%>  
</table>
</td>
</tr>
</table>
    
            <%
    }
%>
    <div align="center">
          <p>  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir..." onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">      </p>
    </div>
</form>
</div>
</body>
</html>
    
