<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de realizar la generacion de reportes de documentos por pagar
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page errorPage= "../error/ErrorPage.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
    <head><title>JSP Page</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    <script language="javascript" src="<%= BASEURL %>/js/boton.js"></script>
    <script>
        function regresar(BASEURL){
            document.forma1.action =BASEURL+"/jsp/cxpagar/facturasxpagar/reporteDXP11.jsp"; 
            document.forma1.submit(); 
        }
    </script>
<body onLoad="" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Reporte de Facturas Vencidas"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<%
    java.util.Date utilDate = new java.util.Date(); //fecha actual
    long lnMilisegundos = utilDate.getTime();
    java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(lnMilisegundos);
    String criterio_t =""+request.getParameter("criterio_t");
    int rango_i =Integer.parseInt(""+request.getParameter("rango_i"));
    int rango_f =Integer.parseInt(""+request.getParameter("rango_f"));
    Vector vecDoc= model.cxpDocService.getVecCxp_doc();
%>
        <form name="forma1" action="" method="post">      
        <p>	
<%
    if((vecDoc != null)&& (vecDoc.size()!=0 )){
    double vt0=0;
    double vt1=0;
    double vt2=0;
    double vt3=0;
    double vt4=0;
    double vt5=0;
    for(int i =0; i< vecDoc.size();i++){
        double v0=0;
        double v1=0;
        double v2=0;
        double v3=0;
        double v4=0;
        double v5=0;
        long dias=0;
        Vector vProveedor = (Vector)vecDoc.elementAt(i);
        CXP_Doc doc1=(CXP_Doc)vProveedor.elementAt(0);
        Proveedor proveedor = model.proveedorService.obtenerProveedorPorNit(doc1.getProveedor());
        String nombreP="";
        if (proveedor != null){
            nombreP=proveedor.getC_payment_name();
        }    
        //out.print("Proveedor "+doc1.getProveedor());%>
        <table width="978" height="68" border="2" align="center">
            <tr class="fila">
            <td width="966" class="fila">
                <table width="963" border="1" align="center"   bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr class="tablaInferior">
                        <td width="381" height="24"  class="subtitulo1"><p align="left">Proveedor &nbsp;&nbsp;<%=doc1.getProveedor()+"   "+nombreP%></p></td>
                        <td width="566"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                    </tr>
                </table>
                <div align="left">
                    <table id="detalle" width="966" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
                        <tr class="tblTitulo" id="fila1">
                            <td width="102" nowrap><div align="center">Documento</div></td>
                            <td width="98" height="30"><div align="center">Banco</div></td>
                            <td width="96" ><div align="center">Sucursal</div></td>
                            <td width="85" ><div align="center">Fecha Factura </div></td>
                            <td width="91" ><div align="center">Fecha Vencimiento</div></td>
                            <td width="36" ><div align="center">N Dias </div></td>
                            <td width="56"  nowrap><div align="center">NV </div></td>
                            <%if((rango_i <=7)&&(rango_f>=1)){%>
                            <td width="56"  nowrap><div align="center">1-7</div></td>
                            <%}
                               if((rango_i <=15)&&(rango_f>=8)){  
                            %>
                            <td width="68"  nowrap><div align="center">8-15</div></td>
                            <%}
                               if((rango_i <=23)&&(rango_f>=16)){  
                            %>
                            <td width="68"  nowrap><div align="center">16-23</div></td>
                            <%}
                               if((rango_i <=30)&&(rango_f>=24)){  
                            %>
                            <td width="68"  nowrap><div align="center">24-30</div></td>
                            <%}
                               if((rango_i <=30)&&(rango_f>=32)){    
                            %>
                            <td width="66"  nowrap><div align="center">+30</div></td>
                            <%}%>
                        </tr>
               <%       for(int x=0; x<vProveedor.size();x++){
                            CXP_Doc doc=(CXP_Doc)vProveedor.elementAt(x); 
                            String fecha_documento = doc.getFecha_documento().substring(0,10);
                            String fecha_vencimiento =doc.getFecha_vencimiento().substring(0,10);
                            java.sql.Timestamp fecha_v =java.sql.Timestamp.valueOf(doc.getFecha_vencimiento());
                            dias = model.cxpDocService.getDiasDiferencia(sqlTimestamp,fecha_v);
                           // out.print(sqlTimestamp.getTime()+"-"+fecha_v.getTime()+"= "+ dias);
                            String d0="";
                            String d1_7="";
                            String d8_15="";
                            String d16_23="";
                            String d24_30="";
                            String m_30="";
                            if(dias <=0 ){
                                d0=""+doc.getVlr_neto();
                                v0= v0+doc.getVlr_neto();
                            }
                            if((dias >=1) && (dias <7)){
                                d1_7=""+doc.getVlr_neto();
                                v1= v1+doc.getVlr_neto();
                            }
                            if((dias >=8) && (dias <15)){
                                d8_15=""+doc.getVlr_neto();
                                v2= v2+doc.getVlr_neto();
                            }
                            if((dias >=16) && (dias <23)){
                                d16_23=""+doc.getVlr_neto();
                                v3= v3+doc.getVlr_neto();
                            }
                            if((dias >=24) && (dias <=30)){
                                d24_30=""+doc.getVlr_neto();
                                v4= v4+doc.getVlr_neto();
                            }
                            if(dias >30){
                                m_30 =""+doc.getVlr_neto();
                                v5= v5+doc.getVlr_neto();
                            }
                            if(criterio_t.equals("detalle")){
                 %>
                        <tr class="fila">
                        <td width="102" class="bordereporte"><div align="center"><a  onclick="programaExterno('<%=doc.getDstrct()%>','<%=doc.getProveedor()%>','<%=doc.getTipo_documento()%>','<%=doc.getDocumento()%>');" style="cursor:hand"><%=model.cxpDocService.nombreDocumento(""+doc.getTipo_documento())+ " " %><%=doc.getDocumento()%></a></div></td>
                        <td width="98" height="30" class="bordereporte"><div align="center"><%=doc.getBanco()%></div></td>
                        <td width="96" class="bordereporte"> <div align="center"><%=doc.getSucursal()%></div></td>
                        <td width="85" class="bordereporte"><div align="center"><%=fecha_documento%></div></td>
                        <td width="91" class="bordereporte"><div align="center" ><%=fecha_vencimiento%></div></td>
                        <td width="36" class="bordereporte"><div align="center"><%=dias%></div></td>
                        <td width="56" class="bordereporte"><div align="center"></div><%=d0%></td>
                            <%if((rango_i <=7)&&(rango_f>=1)){%>
                        <td width="56" class="bordereporte"><div align="center"></div><%=d1_7%></td>
                            <%}
                               if((rango_i <=15)&&(rango_f>=8)){  
                            %>
                        <td width="68" class="bordereporte"><div align="center"></div><%=d8_15%></td>
                             <%}
                               if((rango_i <=23)&&(rango_f>=16)){  
                            %>
                        <td width="68" class="bordereporte"><div align="center"></div><%=d16_23%></td>
                             <%}
                               if((rango_i <=30)&&(rango_f>=24)){  
                            %>
                        <td width="68" class="bordereporte"><div align="center"></div><%=d24_30%></td>
                              <%}
                                 if((rango_i <=30)&&(rango_f>=32)){    
                                %>
                        <td width="66" class="bordereporte"><div align="center"></div><%=m_30%></td>
                            <%}%>
                        </tr>	
                    <%}}
                        vt0=vt0+v0;
                        vt1=vt1+v1;
                        vt2=vt2+v2;
                        vt3=vt3+v3;
                        vt4=vt4+v4;
                        vt5=vt5+v5;
                    %>
                        <tr class="fila">
                            <td height="30" colspan="4" class="bordereporte">&nbsp;</td>
                            <td colspan="2" class="bordereporte"><div align="right">Total:                </div></td>
                            <td class="bordereporte"><%=v0%></td>
                              <%if((rango_i <=7)&&(rango_f>=1)){%>
                            <td class="bordereporte"><%=v1%></td>
                              <%}
                               if((rango_i <=15)&&(rango_f>=8)){  
                            %>
                            <td class="bordereporte"><%=v2%></td>
                               <%}
                               if((rango_i <=23)&&(rango_f>=16)){  
                            %>
                            <td class="bordereporte"><%=v3%></td>
                                 <%}
                               if((rango_i <=30)&&(rango_f>=24)){  
                            %>
                            <td class="bordereporte"><%=v4%></td>
                                <%}
                                  if((rango_i <=30)&&(rango_f>=32)){       
                                %>
                            <td class="bordereporte"><%=v5%></td>
                            <%}%>
                        </tr>   
                    </table>
                </div>
            </td>
            </tr>
        </table>
<%}%>
	
        <table width="978" height="68" border="2" align="center">
            <tr class="fila">
                <td width="966" class="fila">
                    <table width="963" border="1" align="center"   bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                        <tr class="tablaInferior">
                            <td width="381" height="24"  class="subtitulo1"><p align="left">Resumen Total: </p></td>
                            <td width="566"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                    </table>
                    <div align="left">
                        <table id="detalle" width="966" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
                            <tr class="tblTitulo" id="fila1">
                                <td width="102" nowrap><div align="center">Documento</div></td>
                                <td width="98" height="30"><div align="center">Banco</div></td>
                                <td width="96" ><div align="center">Sucursal</div></td>
                                <td width="85" ><div align="center">Fecha Factura </div></td>
                                <td width="91" ><div align="center">Fecha Vencimiento</div></td>
                                <td width="36" ><div align="center">N Dias </div></td>
                                <td width="56"  nowrap><div align="center">NV </div></td>
                                <%if((rango_i <=7)&&(rango_f>=1)){%>
                                <td width="56"  nowrap><div align="center">1-7</div></td>
                                 <%}
                               if((rango_i <=15)&&(rango_f>=8)){  
                            %>
                                <td width="68"  nowrap><div align="center">8-15</div></td>
                                    <%}
                               if((rango_i <=23)&&(rango_f>=16)){  
                            %>
                                <td width="68"  nowrap><div align="center">16-23</div></td>
                                  <%}
                               if((rango_i <=30)&&(rango_f>=24)){  
                            %>
                                <td width="68"  nowrap><div align="center">24-30</div></td>
                                 <%}
                                  if((rango_i <=30)&&(rango_f>=32)){       
                                %>
                                <td width="66"  nowrap><div align="center">+30</div></td>
                                <%}%>
                            </tr>
                            <tr class="fila">
                                <td height="30" colspan="4" class="bordereporte">&nbsp;</td>
                                <td colspan="2" class="bordereporte"><div align="right">Total:                </div></td>
                                 
                                <td class="bordereporte"><%=vt0%></td>
                                <%if((rango_i <=7)&&(rango_f>=1)){%>
                                <td class="bordereporte"><%=vt1%></td>
                               <%}
                               if((rango_i <=15)&&(rango_f>=8)){  
                            %>
                                <td class="bordereporte"><%=vt2%></td>
                                   <%}
                               if((rango_i <=23)&&(rango_f>=16)){  
                            %>
                                <td class="bordereporte"><%=vt3%></td>
                                   <%}
                               if((rango_i <=30)&&(rango_f>=24)){  
                            %>
                                <td class="bordereporte"><%=vt4%></td>
                                 <%}
                                  if((rango_i <=30)&&(rango_f>=32)){       
                                %>
                                <td class="bordereporte"><%=vt5%></td>
                                 <%}%>
                            </tr>   
                        </table>
                    </div>
                </td>
            </tr>
        </table>
	<%}
           else{%>
 <table border="2" align="center">
  <tr>
    <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="282" align="center" class="mensajes">No se encontraron registros </td>
          <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="78">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
           
           
    <%            }
            %>
        <p align="center"><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgaceptar" onClick="regresar('<%=BASEURL%>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;</p>

        </form>
		</div>
    </body>
</html>
    