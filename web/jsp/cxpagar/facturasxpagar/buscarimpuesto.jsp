<!--
- Autor : Ing. David Lamadrid
- Date  : 5 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que se encarga de realizar y mostrar busqueda de los tipos de impuestos
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head><title>JSP Page</title>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script>
function asignarImpuesto(id,codigo,vl,codContable,porc_riva,maxfila){
  porc_riva = (porc_riva == 0)?"":porc_riva;
  var moneda   = parent.opener.document.getElementById("moneda"); 
  var TOTAL    = parent.opener.document.getElementById("total");
  var iva      = parent.opener.document.getElementById("iva"+id.substr(1,id.length)); 
  //ivan 24 julio 2006
  var Imp_riva  = parent.opener.document.getElementById("impuesto1"+id.substr(1,id.length)); 
  var riva      = parent.opener.document.getElementById("riva");
  var agenteRet = parent.opener.document.getElementById("agenteRet");
  //////////////// 
  var campo    = parent.opener.document.getElementById("impuesto"+id);
  var i   	   = id.substr(1,id.length);  
  //valor del item
  var valor    = parent.opener.document.getElementById("valor1"+i);
 	
  //valor neto del item
  var vlrN     = parent.opener.document.getElementById("valorNeto"+i);
  var vlr      = parseFloat(vl.replace( new RegExp(",","g"), ""));
  campo.value  = codigo;

  //valor total item 
  var vlrTotal = parseFloat(valor.value.replace( new RegExp(",","g"), ""));
  var vlrNeto  = parseFloat(vlrN.value.replace( new RegExp(",","g"), ""));
// alert("vlrNeto ="+ vlrNeto+"  vlrTotal  =" +vlrTotal + "   vlr="+vlr)
  
  if( id.substr(0,1) == 0 ){//el numero 0 es el que indica si es iva
  	
	/*var nums =  new String (vlrTotal + (( vlrTotal * vlr )/100)) ;*/
	var nums =  new String (( vlrTotal * vlr )/100) ;
	nums = (moneda.value == 'DOL')?redondear(nums):Math.round(nums);
	var vnet = parseFloat(vlrTotal) + parseFloat( nums);
   
	if( moneda.value == 'DOL' ){
	   vlrN.value = formatoDolar(vnet,2)
	   
	}else{
	   vlrN.value   = formato(vnet);
	}
	
	if((agenteRet.value !="S")&&(riva.value == 'S')){
	   Imp_riva.value   = codContable;//el codigo contable es el riva asociado
	   var vlr_riva      = parent.opener.document.getElementById("vlr_riva"+id.substr(1,id.length));  
	   vlr_riva.value    =  porc_riva; 
	}
	
	iva.value    = vl;
		
  }else if( id.substr(0,1) == 1 ){//el numero 1 es el que indica si es riva
       var vlr_riva      = parent.opener.document.getElementById("vlr_riva"+id.substr(1,id.length)); 
       vlr_riva.value = vl;
	   
  }else if( id.substr(0,1) == 2 ){//el numero 2 es el que indica si es rica
       var vlr_rica      = parent.opener.document.getElementById("vlr_rica"+id.substr(1,id.length)); 
       vlr_rica.value = vl;
	   
  }else if( id.substr(0,1) == 3 ){//el numero 3 es el que indica si es rfte
       var vlr_rfte      = parent.opener.document.getElementById("vlr_rfte"+id.substr(1,id.length)); 
       vlr_rfte.value = vl;
  }
  /*
  if( id.substr(0,1) != 1 ){
     vlrN.value   = formato( vlrNeto - (( vlrNeto * vlr )/100) );	 
  }
  else{
	  vlrN.value   = formato( vlrNeto + (( vlrTotal * vlr )/100) );	
  }*/

  
  if( id.substr(0,1) == 0 ){
  var form = parent.opener.document.getElementById("forma1");
  var vlr_total = 0;
  
  
  for (j=0; j<form.elements.length; j++){                  
	if (form.elements[j].name.indexOf('valorNeto')==0){
    	var valor = form.elements[j].value.replace( new RegExp(",","g"), "");
			if (!isNaN ( parseFloat(valor)) ){
			   vlr_total += parseFloat(valor);
		    }					 
     }
  }
  //nums = parseInt(nums)
  form.total.value = (moneda.value == 'DOL')?formatoDolar(vlr_total,2):formato(vlr_total);
  
 }
 
 sumatoria(maxfila);
  parent.close();
}

function sumatoria(maxfila){
	    var suma_total=0;
	    var suma = 0;
		var moneda = parent.opener.document.getElementById("moneda"); 
			st = parent.opener.document.getElementById("total");
			total_neto = parent.opener.document.getElementById("total_neto");
			//alert(detalle.rows.length);	
			for (var i = 1;i <= maxfila + 1 ; i++){
                var vlrNeto  = parent.opener.document.getElementById("valorNeto"+i);  
				var subtotal = parent.opener.document.getElementById("valor1"+i);
				var imp      = parent.opener.document.getElementById("iva"+i);
				var vlr_riva      = parent.opener.document.getElementById("vlr_riva"+i);
				var vlr_rica      = parent.opener.document.getElementById("vlr_rica"+i);
				var vlr_rfte      = parent.opener.document.getElementById("vlr_rfte"+i);
 		        
				if ( subtotal != null ){
				    var valor = subtotal.value.replace( new RegExp(",","g"), "");
					var vlr   = "";
					var vlrImpuesto ="";
					
					if (!isNaN ( parseFloat(valor)) ){
					var vnet = parseFloat(valor);
					var vlr_iva_aplicado = 0;
					    if(imp.value !="" ){
						   
                            vlr = imp.value;
							//alert("IMP  "+vlr)
							vlr = parseFloat(vlr);
							vlrImpuesto =  ( valor * vlr )/100;
							
							vlrImpuesto = (moneda.value == 'DOL')?redondear(vlrImpuesto):Math.round(vlrImpuesto);
							vlr_iva_aplicado = vlrImpuesto; 
							vnet += parseFloat(vlrImpuesto);
                            suma += parseFloat(vlrImpuesto);
							suma_total += parseFloat(vlrImpuesto);
							//alert("SUMA "+suma)
						}
						 if(vlr_riva.value !="" ){
						    vlr_iva_aplicado = parseInt(vlr_iva_aplicado);
						    vlr = vlr_riva.value;
							//alert("IMP  "+vlr)
							vlr = parseFloat(vlr);
							vlrImpuesto =  ( valor * vlr )/100;//fdiaz 2011-03-14 - GEOTECH - Contabilizacion IVA en CxP
							
							vlrImpuesto = (moneda.value == 'DOL')?redondear(vlrImpuesto):Math.round(vlrImpuesto);
							suma_total -= parseFloat(vlrImpuesto);
							
						 }
						 if(vlr_rica.value !="" ){
						    vlr = vlr_rica.value;
							//alert("IMP  "+vlr)
							vlr = parseFloat(vlr);
							vlrImpuesto =  ( valor * vlr )/100;
							
							vlrImpuesto = (moneda.value == 'DOL')?redondear(vlrImpuesto):Math.round(vlrImpuesto);
							suma_total -= parseFloat(vlrImpuesto);
							
						 }
						 if(vlr_rfte.value !="" ){
						    vlr = vlr_rfte.value;
							//alert("IMP  "+vlr)
							vlr = parseFloat(vlr);
							vlrImpuesto =  ( valor * vlr )/100;
							vlrImpuesto = (moneda.value == 'DOL')?redondear(vlrImpuesto):Math.round(vlrImpuesto);
							suma_total -= parseFloat(vlrImpuesto);
							
						 }
						//Ivan Dario Gomez 24 julio 2006
						if(moneda.value =='DOL'){
						   vlrNeto.value =  formatoDolar(vnet,2);
						   
						}else{
					      vlrNeto.value = formato(vnet);
						}
						//////////////////////////////////////////
						suma += parseFloat(valor);
						suma_total += parseFloat(valor);
						
					}					  
				}
			}
			
			if(moneda.value =='DOL'){
			   st.value =  formatoDolar(suma,2);
			   total_neto.value = formatoDolar(suma_total,2);
			   
			}else{
			   st.value = formato(suma);
			   total_neto.value = formato(suma_total);
			}
	}

 function formato(numero){
           
           var tmp = parseInt(numero) ;
		   var factor = (tmp < 0 ? - 1 : 1);
           tmp *= factor;
          
           var num = '';
           var pos = 0;
           while(tmp>0){
                  if (pos%3==0 && pos!=0) num = ',' + num;
                  res  = tmp % 10;
                  tmp  = parseInt(tmp / 10);
                  num  = res + num  ;
                  pos++;
           }
           return (factor==-1 ? '-' : '' ) + num ;
 } 


/*funcion para asignar los impuestos a todos los item*/
function asignarImpuestoCab(id,codigo,vl,codContable,maxfila,tipoImp,porc_riva){
  porc_riva = (porc_riva == 0)?"":porc_riva;
  //ivan 24 julio 2006
  var riva      = parent.opener.document.getElementById("riva");
  var agenteRet = parent.opener.document.getElementById("agenteRet");
  //////////////// 
  var moneda   = parent.opener.document.getElementById("moneda");
  var campo =  parent.opener.document.getElementById(id);
  var imp   =(tipoImp =='IVA')?'impuesto0':(tipoImp =='RIVA')?'impuesto1':(tipoImp =='RICA')?'impuesto2':(tipoImp =='RFTE')?'impuesto3':'';
 
  for (var i=1;i<= maxfila;i++){
      var iva      = parent.opener.document.getElementById("iva"+i);
	  
      if(iva != null){
		 
		  //ivan 24 julio 2006
		  var Imp_riva  = parent.opener.document.getElementById("impuesto1"+i); 
		  //////////////// 
		  
		  var impItem =  parent.opener.document.getElementById(imp+""+i); 
		  //valor del item
		  var valor    = parent.opener.document.getElementById("valor1"+i);
		  //valor neto del item
		  var vlrN     = parent.opener.document.getElementById("valorNeto"+i);
		  var vlr      = parseFloat(vl.replace( new RegExp(",","g"), ""));
		  campo.value  = codigo;
		  impItem.value = codigo;
		  //valor total item 
		  var vlrTotal = parseFloat(valor.value.replace( new RegExp(",","g"), ""));
		  var vlrNeto  = parseFloat(vlrN.value.replace( new RegExp(",","g"), ""));
		// alert("vlrNeto ="+ vlrNeto+"  vlrTotal  =" +vlrTotal + "   vlr="+vlr)
		   if( tipoImp == 'IVA' ){
		      iva.value    = vl;	
			  if((agenteRet.value !="S")&&(riva.value == 'S')){
	             Imp_riva.value   = codContable;
				 var vlr_riva      = parent.opener.document.getElementById("vlr_riva"+i);  
	             vlr_riva.value    =  porc_riva; 
	          }
			    var nums =  new String (( vlrTotal * vlr )/100) ;
				nums = (moneda.value == 'DOL')?redondear(nums):Math.round(nums);
				var vnet = parseFloat(vlrTotal) + parseFloat( nums);
			   
				if( moneda.value == 'DOL' ){
				   vlrN.value = formatoDolar(vnet,2)
				   
				}else{
				   vlrN.value   = formato(vnet);
				}
			  
			    
		   }else  if( tipoImp == 'RICA' ){
		       var vlr_rica      = parent.opener.document.getElementById("vlr_rica"+i); 
               vlr_rica.value = vl;
			   
		   }else  if( tipoImp == 'RFTE' ){
		       var vlr_rfte      = parent.opener.document.getElementById("vlr_rfte"+i); 
               vlr_rfte.value = vl;
		   }   
			  
		 
		
		  
		  
		 
		 
		  
	  }
  }
      if( tipoImp == 'IVA' ){
	  var form = parent.opener.document.getElementById("forma1");
		  var vlr_total = 0;
		  for (j=0; j<form.elements.length; j++){                  
	        if (form.elements[j].name.indexOf('valorNeto')==0){
    	       var valor = form.elements[j].value.replace( new RegExp(",","g"), "");
			   if (!isNaN ( parseFloat(valor)) ){
				  vlr_total += parseFloat(valor);
              }					 
           }
        }
		  
		  form.total.value = (moneda.value == 'DOL')?formatoDolar(vlr_total,2):formato(vlr_total);
		 
		  }
		  sumatoria(maxfila);
		  parent.close();
}
function redondear(valor){
    valor = new String(valor);
    var nums =  valor.split('.');
	return nums[0]+ (nums.length > 1  ? '.' +((nums[1].length >2)?((nums[1].charAt(2)>5)?(parseInt(nums[1].substr(0,2))+1):nums[1].substr(0,2)):(nums[1].length==1)?nums[1].substr(0,1)+'0':nums[1].substr(0,nums[1].length)) : '.00'); 
	
}
function formatoDolar (valor, decimales){
    valor = new String (valor); 
    var nums =  valor.split('.');
	var salida = new String();
	
	var TieneDec = valor.indexOf('.');
	var dato = new String();
	if( TieneDec !=-1  ){
	   var deci = valor.split('.');
	   var dec       = (deci[1].length >2)?deci[1].charAt(2):deci[1].substr(0,deci[1].length);
	   
	   if(dec > 5){
	       dato =  (parseInt(deci[1].substr(0,2)) + 1);
		   if(dato>99){
		     nums[0] = new String (parseInt(nums[0])+1);
			 return nums[0]+'.00';
		   }
	   }
    }
	
	var signo = (parseFloat(nums[0])<0?-1:1);
	   nums[0] = new String(parseFloat(nums[0])*signo);
  	   for (var i=nums[0].length-1, j=0; i>=0; salida = nums[0].charAt(i) + (j%3==0 && j!=0? ',':'') + salida , i--, j++);
	   var sal  = salida + (nums.length > 1 && decimales > 0 ? '.'+((nums[1].length >2)?((nums[1].charAt(2)>5)?(parseInt(nums[1].substr(0,2))+1):nums[1].substr(0,2)):(nums[1].length==1)?nums[1].substr(0,1)+'0':nums[1].substr(0,nums[1].length)) : '.00'); 
	   if (signo==-1) sal = "-" + sal;
		
		return sal;  
	   
	    
	   

 }

/*****************************************************/
</script>
</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Impuestos "/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
   
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String dstrct   = usuario.getDstrct();
    String agenciaBanco  = ""+request.getParameter("agenciaBanco");
    String t_i = ""+request.getParameter("tipo");
    String idI = ""+request.getParameter("id");
    String op  = ""+request.getParameter("OP");
    String codigo =""+request.getParameter("codigo").toUpperCase();
    String maxfila =  (request.getParameter ("maxfila")==null?"":request.getParameter ("maxfila")); 
    String maxf =  (request.getParameter ("maxf")==null?"":request.getParameter ("maxf"));
    java.util.Date utilDate = new java.util.Date(); //fecha actual
    long lnMilisegundos = utilDate.getTime();
    java.sql.Timestamp sqlTimestamp = new java.sql.Timestamp(lnMilisegundos);
	Vector vImpuestos= model.TimpuestoSvc.buscarImpuestoPorTipo(t_i,""+sqlTimestamp,dstrct,agenciaBanco);
	Tipo_impuesto datos = new  Tipo_impuesto();
	if(op.equals("ENTER")){
	    datos = model.TimpuestoSvc.buscarImpuestoPorCodigos(t_i,codigo,dstrct,agenciaBanco);
		if(datos ==null){
		   op = "";
		   %><script>
		        alert("El codigo de impuesto digitado no existe en la bd")
		     </script>
		   <%
		   
		}
	}
	
%>
<form name="forma1" action="" method="post">      
<table width="312" border="2" align="center">
  <tr>
    <td width="600">
<table width="110%"  align="center" > 
  <tr>
    <td width="50%" height="24"  class="subtitulo1"><p align="left">Impuestos Tipo <%=t_i%></p></td>
    <td width="50%"  class="barratitulo" ><img src="<%=BASEURL%>/images/titulo.gif"></td>
  </tr>
</table>
<table width="100%"  align="center" >
 <tr class="tblTitulo" id="titulos">
   <%if(t_i.equals("RICA")){%>
    <td width="136" align="center">Agencia </td>
   <%}%>
    <td width="136" align="center">Codigo </td>
    <td width="146" align="center">Impuesto</td>
    <td width="136" align="center">Concepto </td>
    <td width="146" align="center">Porcentaje</td>
    <td width="300" nowrap align="center">Descripcion</td>
 </tr>
<% 
    String fecha =""+sqlTimestamp; 
    for(int i=0; i< vImpuestos.size();i++){
        Tipo_impuesto tipo=(Tipo_impuesto)vImpuestos.elementAt(i);
%>  
        <tr class="fila">
		
		 <%if(maxfila.equals("")){%>
		      <%if(t_i.equals("RICA")){%>
		         <td align="center"><a  onClick="asignarImpuesto('<%=idI%>','<%=tipo.getCodigo_impuesto()%>', '<%=tipo.getPorcentaje1()%>','<%=tipo.getCod_cuenta_contable()%>','<%=tipo.getPorcentaje2()%>','<%=maxf%>' );" style="cursor:hand" ><%=tipo.getAgencia()%></a></td> 
                      <%}%>
			<td align="center"><a  onClick="asignarImpuesto('<%=idI%>','<%=tipo.getCodigo_impuesto()%>', '<%=tipo.getPorcentaje1()%>','<%=tipo.getCod_cuenta_contable()%>','<%=tipo.getPorcentaje2()%>','<%=maxf%>' );" style="cursor:hand" ><%=tipo.getCodigo_impuesto()%></a></td>
			<td align="center"><a  onClick="asignarImpuesto('<%=idI%>','<%=tipo.getCodigo_impuesto()%>', '<%=tipo.getPorcentaje1()%>','<%=tipo.getCod_cuenta_contable()%>','<%=tipo.getPorcentaje2()%>','<%=maxf%>' );" style="cursor:hand" ><%=tipo.getPorcentaje1()+ " % "%></a></td>
			<td align="center"><a  onClick="asignarImpuesto('<%=idI%>','<%=tipo.getCodigo_impuesto()%>', '<%=tipo.getPorcentaje1()%>','<%=tipo.getCod_cuenta_contable()%>','<%=tipo.getPorcentaje2()%>','<%=maxf%>' );" style="cursor:hand" ><%=tipo.getCod_cuenta_contable()%></a></td>
			<td align="center"><a  onClick="asignarImpuesto('<%=idI%>','<%=tipo.getCodigo_impuesto()%>', '<%=tipo.getPorcentaje1()%>','<%=tipo.getCod_cuenta_contable()%>','<%=tipo.getPorcentaje2()%>','<%=maxf%>' );" style="cursor:hand" ><%=(tipo.getPorcentaje2()==0)?"":tipo.getPorcentaje2()+ " % "%></a></td>
			<td align="center"><a  onClick="asignarImpuesto('<%=idI%>','<%=tipo.getCodigo_impuesto()%>', '<%=tipo.getPorcentaje1()%>','<%=tipo.getCod_cuenta_contable()%>','<%=tipo.getPorcentaje2()%>','<%=maxf%>' );" style="cursor:hand" ><%=tipo.getDescripcion()%></a></td>
        <%}else{%>
                     <%if(t_i.equals("RICA")){%>
		         <td align="center"><a  onClick="asignarImpuestoCab('<%=idI%>','<%=tipo.getCodigo_impuesto()%>', '<%=tipo.getPorcentaje1()%>','<%=tipo.getCod_cuenta_contable()%>','<%=maxfila%>','<%=t_i%>','<%=tipo.getPorcentaje2()%>');" style="cursor:hand" ><%=tipo.getAgencia()%></a></td>
                      <%}%>
		    <td align="center"><a  onClick="asignarImpuestoCab('<%=idI%>','<%=tipo.getCodigo_impuesto()%>', '<%=tipo.getPorcentaje1()%>','<%=tipo.getCod_cuenta_contable()%>','<%=maxfila%>','<%=t_i%>','<%=tipo.getPorcentaje2()%>');" style="cursor:hand" ><%=tipo.getCodigo_impuesto()%></a></td>
		    <td align="center"><a  onClick="asignarImpuestoCab('<%=idI%>','<%=tipo.getCodigo_impuesto()%>', '<%=tipo.getPorcentaje1()%>','<%=tipo.getCod_cuenta_contable()%>','<%=maxfila%>','<%=t_i%>','<%=tipo.getPorcentaje2()%>');" style="cursor:hand" ><%=tipo.getPorcentaje1()+ " % "%></a></td> 
		    <td align="center"><a  onClick="asignarImpuestoCab('<%=idI%>','<%=tipo.getCodigo_impuesto()%>', '<%=tipo.getPorcentaje1()%>','<%=tipo.getCod_cuenta_contable()%>','<%=maxfila%>','<%=t_i%>','<%=tipo.getPorcentaje2()%>');" style="cursor:hand" ><%=tipo.getCod_cuenta_contable()%></a></td>
		    <td align="center"><a  onClick="asignarImpuestoCab('<%=idI%>','<%=tipo.getCodigo_impuesto()%>', '<%=tipo.getPorcentaje1()%>','<%=tipo.getCod_cuenta_contable()%>','<%=maxfila%>','<%=t_i%>','<%=tipo.getPorcentaje2()%>');" style="cursor:hand" ><%=(tipo.getPorcentaje2()==0)?"":tipo.getPorcentaje2()+ " % "%></a></td> 
		    <td align="center"><a  onClick="asignarImpuestoCab('<%=idI%>','<%=tipo.getCodigo_impuesto()%>', '<%=tipo.getPorcentaje1()%>','<%=tipo.getCod_cuenta_contable()%>','<%=maxfila%>','<%=t_i%>','<%=tipo.getPorcentaje2()%>');" style="cursor:hand" ><%=tipo.getDescripcion()%></a></td>
		<%}%>
	    </tr>
<%
    }
%>  
</table>
</td>
</tr>
</table>
</form>
<div>
<iframe width=188 height=166 name="gToday:datetime:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins_24.js" id="gToday:datetime:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins_24.js" src="<%=BASEURL%>/js/calendartsp/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
<%if(op.equals("ENTER")){%>
<center><img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir..." onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></center></body>
<%}%>
</html>
<%if(op.equals("ENTER")){
     if(maxfila.equals("")){%>
		<script>
		 asignarImpuesto('<%=idI%>','<%=datos.getCodigo_impuesto()%>', '<%=datos.getPorcentaje1()%>','<%=datos.getCod_cuenta_contable()%>');
		</script> 
   <%}else{%>
      <script>
	     asignarImpuestoCab('<%=idI%>','<%=datos.getCodigo_impuesto()%>', '<%=datos.getPorcentaje1()%>','<%=datos.getCod_cuenta_contable()%>','<%=maxfila%>','<%=t_i%>');
	  </script>  
  <% }
}%>
