<!--
- Date  : 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, permite realizar las funciones basicas sobre el programa de tipo de impuesto
--%>     
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<%  
        String style = "simple";
        String position =  "bottom";
        String index =  "center";
        int maxPageItems = 8;
        int maxIndexPages = 10;
%>
<%
List                ListaGnral  = model.TimpuestoSvc.getList();
Tipo_impuesto       Datos       = model.TimpuestoSvc.getDato();
List				ListaAgencias 	= model.ciudadService.getList();
/*DATOS IMPUESTO*/
String              Tipo        = (Datos!=null)?Datos.getTipo_impuesto():"";
String              Codigo      = (Datos!=null)?Datos.getCodigo_impuesto() :"";
String              Descripcion = (Datos!=null)?Datos.getDescripcion():"";
String              Concepto    = (Datos!=null)?Datos.getConcepto():"";
String              FechaV      = (Datos!=null)?Datos.getFecha_vigencia():"";
double              Porcentaje1 = (Datos!=null)?Datos.getPorcentaje1():0;
double              Porcentaje2 = (Datos!=null)?Datos.getPorcentaje2():0;
String              Cuenta      = (Datos!=null)?Datos.getCod_cuenta_contable():"";
String              Agencia     = (Datos!=null)?Datos.getAgencia():"";
String				signo 		=	Datos!=null ? "" + Datos.getInd_signo() : "1";


String Mensaje       = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
String NombreBoton   = (Datos==null)?"Guardar":"Modificar";
String TNombreBoton   = (Datos==null)?"aceptar":"modificar";
String NombreBoton1  = (ListaGnral!=null && ListaGnral.size()>0)?"Ocultar Lista":"Listado";
String TNombreBoton1  = (ListaGnral!=null && ListaGnral.size()>0)?"btnOcultar":"detalles";
String BloquearText  = (Datos==null)?"":"ReadOnly";
String BloquearSelect= "";
String BloquearBoton = (Datos!=null)?"":"Disabled";

String impuesto = (request.getParameter("impuesto")!=null)?request.getParameter("impuesto"):"";
String agencias = (String) request.getAttribute("agenciasLst");//model.agenciaService.Agencias();

Util u = new Util();

%>

<html>
<head>
  <title>Tipo de impuesto</title>
  <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>  
  <link href="../css/estilostsp.css" rel='stylesheet'> 
 
  <script src='<%=BASEURL%>/js/validarImpuesto.js'></script>
  <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
  <script src='<%=BASEURL%>/js/date-picker.js'></script>  
  <script>
	controler = '<%=CONTROLLER%>';
	function SelAll(){
            for(i=0;i<FormularioListado.length;i++)
                    FormularioListado.elements[i].checked=FormularioListado.All.checked;
    }
    function ActAll(){
            FormularioListado.All.checked = true;
            for(i=1;i<FormularioListado.length;i++)	
                    if (!FormularioListado.elements[i].checked){
                            FormularioListado.All.checked = false;
                            break;
                    }
    }      
	
/*	function verificar_checkbox(){    
    	if( FormularioListado.Opcion.value != 'Listado'){
			for(i=0;i<=FormularioListado.length-1;i++)    
        		if(FormularioListado.elements[i].type=='checkbox' && formulario.elements[i].checked)        
         	   	return true;
   	 			alert('Debe seleccionar por lo menos un item...')                                    
   		 	return false;
		}
	}*/
	function verificar_checkbox(){
		if( FormularioListado.Opcion.value != 'Listado'){
			for(i=1;i<FormularioListado.length;i++)	
                    if (FormularioListado.elements[i].checked)
                            return true;
            alert('Por favor seleccione un registro para poder continuar');
            return false;
		}
		
    }      
  </script>
  
</head>
<body onResize="redimensionar()" onLoad="Opciones2('<%=BASEURL%>','<%=agencias%>','<%=impuesto%>', '<%=Tipo%>','<%=Codigo%>','<%=Descripcion%>','<%=Concepto%>', '<%=FechaV%>', '<%=Porcentaje1%>', '<%=Porcentaje2%>', '<%=Cuenta%>', '<%=Agencia%>', '<%= signo%>')";"redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Tipo de impuesto"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<FORM name='formulario' method='POST' action="<%=CONTROLLER%>?estado=Tipo_impuesto&accion=Manager">

    <table width='479' align='center' border='2'>
      <tr>
       <td>	<table width="100%">
        <tr>
          <td width="50%" class="subtitulo1"> Informaci&oacute;n de impuesto</td>
          <td width="50%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif"><%=datos[0]%>
            <input name="agenciaSelec" type="hidden" id="agenciaSelec"></td>
        </tr>
      </table>
          <table  border='0' width='100%'  id="tabla" align="center" class="tablaInferior">                                
			   <tr class="fila">
                    <td width='29%'> Impuesto </td>
                    <td>
					<select name='impuesto' id="impuesto" class="textbox" onChange="Opciones('<%=BASEURL%>','<%=agencias%>')">
                      <option value="1">Impuesto..</option>
					  <option value="IVA">IVA</option>
					  <option value="RICA">RICA</option>
					  <option value="RIVA">RIVA</option>
					  <option value="RFTE">RFTE</option>  
					  <option value="RFTECL">RFTECL</option>  
					  <option value="RICACL">RICACL</option>                   
                    </select>		
				   <input type="hidden" name="Tipo" id="Tipo">
                   <input type="hidden" name="original2" id="original2" value='<%=Codigo+","+Concepto+","+FechaV+","+Agencia+","+Tipo%>'>
</td>                        
               </tr>                    
          </table></td>
      </tr>
        <tr>            
        </tr>
  </table>
  <br>
    <table align="center">
  <td colspan="2" align="center"> <img src="<%=BASEURL%>/images/botones/<%=TNombreBoton%>.gif"  name="imgsalir" onClick="Opcion.value='Guardar';if(ValidarImpuesto(formulario))formulario.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgdetalles" onClick="formulario.reset(); formulario.Tipo.value = formulario.impuesto.value = timpuestoselec; resetDate();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; <img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgrestablecer" onClick="Opcion.value='Nuevo';formulario.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> </td>
    </table>
    <br>
<input type='hidden' name='Opcion'/> 
</FORM>



<center class='comentario'>


 <%if(!Mensaje.equals("")){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%}%>

</div>
</center>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
<%=datos[1]%>
</body>
</html>
