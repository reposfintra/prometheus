<!--
- Date  : 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, permite realizar las funciones basicas sobre el programa de tipo de impuesto
--%>     
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<%  
        String style = "simple";
        String position =  "bottom";
        String index =  "center";
        int maxPageItems = 20;
        int maxIndexPages = 10;
%>
<%
List                ListaGnral  = model.TimpuestoSvc.getList();
Tipo_impuesto       Datos       = model.TimpuestoSvc.getDato();
List				ListaAgencias 	= model.ciudadService.getList();
/*DATOS IMPUESTO*/
String              Tipo        = (Datos!=null)?Datos.getTipo_impuesto():"";
String              Codigo      = (Datos!=null)?Datos.getCodigo_impuesto() :"";
String              Descripcion = (Datos!=null)?Datos.getDescripcion():"";
String              Concepto    = (Datos!=null)?Datos.getConcepto():"";
String              FechaV      = (Datos!=null)?Datos.getFecha_vigencia():"";
double              Porcentaje1 = (Datos!=null)?Datos.getPorcentaje1():0;
double              Porcentaje2 = (Datos!=null)?Datos.getPorcentaje2():0;
String              Cuenta      = (Datos!=null)?Datos.getCod_cuenta_contable():"";
String              Agencia     = (Datos!=null)?Datos.getAgencia():"";


String Mensaje       = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
String NombreBoton   = (Datos==null)?"Guardar":"Modificar";
String TNombreBoton   = (Datos==null)?"aceptar":"modificar";
String NombreBoton1  = (ListaGnral!=null && ListaGnral.size()>0)?"Ocultar Lista":"Listado";
String TNombreBoton1  = (ListaGnral!=null && ListaGnral.size()>0)?"btnOcultar":"detalles";
String BloquearText  = (Datos==null)?"":"ReadOnly";
String BloquearSelect= "";
String BloquearBoton = (Datos!=null)?"":"Disabled";

String impuesto = (request.getParameter("impuesto")!=null)?request.getParameter("impuesto"):"";
String agencias = (String) request.getAttribute("agenciasLst"); //model.agenciaService.Agencias();

Util u = new Util();

%>

<html>
<head>
  <title>Tipo de impuesto</title>
  <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>  
  <link href="../css/estilostsp.css" rel='stylesheet'> 
 
  <script src='<%=BASEURL%>/js/validarImpuesto.js'></script>
  <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
  <script src='<%=BASEURL%>/js/date-picker.js'></script>  
  <script>
	controler = '<%=CONTROLLER%>';
	function SelAll(){
            for(i=0;i<FormularioListado.length;i++)
                    FormularioListado.elements[i].checked=FormularioListado.All.checked;
    }
    function ActAll(){
            FormularioListado.All.checked = true;
            for(i=1;i<FormularioListado.length;i++)	
                    if (!FormularioListado.elements[i].checked){
                            FormularioListado.All.checked = false;
                            break;
                    }
    }      
	
/*	function verificar_checkbox(){    
    	if( FormularioListado.Opcion.value != 'Listado'){
			for(i=0;i<=FormularioListado.length-1;i++)    
        		if(FormularioListado.elements[i].type=='checkbox' && formulario.elements[i].checked)        
         	   	return true;
   	 			alert('Debe seleccionar por lo menos un item...')                                    
   		 	return false;
		}
	}*/
	function verificar_checkbox(){
		if( FormularioListado.Opcion.value != 'Listado'){
			for(i=1;i<FormularioListado.length;i++)	
                    if (FormularioListado.elements[i].checked)
                            return true;
            alert('Por favor seleccione un registro para poder continuar');
            return false;
		}
		
    }      
  </script>
  
</head>
<body onResize="redimensionar()" onLoad="<% if ( request.getParameter("impuesto")!=null ) { %> FormularioListado.TipoI.value = '<%= request.getParameter("impuesto")%>';<% } %>redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Tipo de impuesto"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%if(!Mensaje.equals("")){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%}%>
 <form action="<%=CONTROLLER%>?estado=Tipo_impuesto&accion=Manager" method='post' name='FormularioListado' id="FormularioListado" onSubmit="return verificar_checkbox();">
  
        <br>

        <tr><th height='28' colspan='7'>
		<table width="100%" border="2" align="center">
          <tr>
            <td><table width="100%" align="center" class="">
              <tr>
                <td width="50%" class="subtitulo1">&nbsp;LISTADO GENERAL DE REGISTROS</td>
                <td width="50%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
              </tr>
			  <tr>
			  <td colspan="2">
			  <table bordercolor="#F7F5F4" bgcolor="#FFFFFF"border="1" width="100%">
		   		<tr class="tblTitulo"align="center">
            	<th width='87' align="center">Filtro</th>
            	<th width='111' align="center"><select class="textbox" id="TipoI" name="TipoI" onChange="Validar()">
              	<option value="ALL">Todos</option>
              	<option value="IVA">IVA</option>
              	<option value="RIVA">RIVA</option>
              	<option value="RICA">RICA</option>
              	<option value="RFTE">RFTE</option>
              	<option value="RICACL">RICACL</option>
              	<option value="RFTECL">RFTECL</option>
            	</select></th>
				<th><select name='filtroagencias' id="select2" class="textbox" disabled>                             
				  <option value="%">Todas</option>	  
				  <%
                       if(ListaAgencias.size()>0) {
                           Iterator It3 = ListaAgencias.iterator();
                           while(It3.hasNext()) {
                               Ciudad  datos2 =  (Ciudad) It3.next();
                               out.print("<option value='"+datos2.getCodCiu()+"'>["+datos2.getCodCiu()+"] "+datos2.getNomCiu()+"</option> \n");
                           }
                       }
                       %>
                </select></th>
				<th><input type="text" id="fconcepto" class="textbox" maxlength="40" size="40" name="fconcepto"></th>
            	<th width="343" align="center"><INPUT type="image"  onclick="Opcion.value='Listado';"    value='Anular' src="<%=BASEURL%>/images/botones/buscar.gif" align="middle" height="21"></th>
        	    <th width="405" align="left"></th>
				</tr> 
        	 </table>
			  </td>
			 
            </table>
 <%  if(ListaGnral!=null && ListaGnral.size()>0) { %> 			
			<table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
		   <tr class="tblTitulo" align="center">
            <th width='5%' align="center">N�</th>
            <th width='8%' align="center">ESTADO</th>
            <th width='8%' align="center">IMPUESTO</th>
			<th width='15%' align="center">AGENCIA</th> 
			<th width='8%' align="center">CODIGO </th>
            <th width='20%' align="center">CONCEPTO</th>			
			<th width='10%' align="center"> VIGENCIA</th>
			<th width='6%' align="center">%</th>
			<th width='6%' align="center">% RIVA</th>
			<th width='25%' align="center">CUENTA CONTABLE</th>
			<th width='25%' align="center">INDICADOR<br>
			  SIGNO</th>
			
          </tr>
        <pg:pager
         items="<%= ListaGnral.size()%>"
         index="<%= index %>"
         maxPageItems="<%= maxPageItems %>"
         maxIndexPages="<%= maxIndexPages %>"
         isOffset="<%= true %>"
         export="offset,currentPageNumber=pageNumber"
         scope="request">		
          <%
            int Cont = 1;
            /*Iterator it2 = ListaGnral.iterator();
            while(it2.hasNext()){*/
			String link = CONTROLLER + "?estado=Tipo_impuesto&accion=Manager&cmd=show";
			for (int i = offset.intValue(), l = Math.min(i + maxPageItems, ListaGnral.size()); i < l; i++){
                //Tipo_impuesto dat = (Tipo_impuesto) it2.next();
				Tipo_impuesto dat = (Tipo_impuesto) ListaGnral.get(i);
                //String e = (Cont % 2 == 0 )?"filaazul":"filagris";
				String e = (i % 2 == 0 )?"filaazul":"filagris";
                String Estilo = (dat.getReg_status().equals("A"))?"filaresaltada":e;
               %><pg:item>
        <tr class='<%= Estilo %>' bgcolor="#F7F5F4" bordercolor="#999999"  onMouseOver='cambiarColorMouse(this)'   style="cursor:hand"
				onClick = "window.open('<%= link %>&Opcion=Seleccionar&original2=<%=dat.getCodigo_impuesto()+","+dat.getConcepto()+","+dat.getFecha_vigencia()+","+dat.getAgencia()+","+dat.getTipo_impuesto()%>','TipoImp','status=yes,scrollbars=no,width=780,height=450,resizable=yes');">
            <td  align='center' class="bordereporte"><span class='comentario'><%//Cont++%><%= (i + 1) %></span></td>
            <td align='center' class="bordereporte"><%= (dat.getReg_status()!=null && dat.getReg_status().length()==0 ) ? "&nbsp;" : dat.getReg_status() %></td>
            <td align='center' class="bordereporte"><%=dat.getTipo_impuesto()%></td>
            <td align="center" class="bordereporte"><%=(!dat.getAgencia().equals(""))?model.ciudadService.obtenerNombreCiudad(dat.getAgencia()):"&nbsp;"%></td> 
			<td align="center" class="bordereporte"><%=dat.getCodigo_impuesto()%></td>
			<td align="center" class="bordereporte"><%=(!dat.getConcepto().equals(""))?dat.getConcepto():"&nbsp;"%></td>			
			<td align="center" class="bordereporte"><%=dat.getFecha_vigencia()%></td>
			<td align="center" class="bordereporte"><%=dat.getPorcentaje1()%></td>
			<td align="center" class="bordereporte"><%=dat.getPorcentaje2()%></td>
			<td align="center" class="bordereporte"><%=dat.getCod_cuenta_contable()%></td>
			<td align="center" class="bordereporte"><%=dat.getInd_signo() == -1 ? "Disminuye" : "Aumenta"%></td>
						
        </tr></pg:item>
        <%  }   %>
        <tr   class="bordereporte">
          <td td height="20" colspan="13" nowrap align="center">
		   <pg:index>
            <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
           </pg:index> 
	      </td>
        </tr>
        </pg:pager>
      <%  }   %> </table></td>
          </tr>
        </table></th>
        </tr>

        <br>
        <input type="hidden" name="agenciah" id="agenciah">
        <input type="hidden" name="conceptoh" id="conceptoh">
        <input type='hidden' name='Opcion'/> 
		<input type="hidden" name="Tipo" id="Tipo">
		<input type="hidden" name="cadena" id="cadena">        
  </form>

 <table width="1000" border="0" align="center" cellpadding="0" cellspacing="0">
   <tr align="left">
     <td><img src="<%=BASEURL%>/images/botones/salir.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="window.close();"  style="cursor:hand ">  
   </tr>
 </table>
</div>
</center>
<%=datos[1]%>
</body>
</html>
