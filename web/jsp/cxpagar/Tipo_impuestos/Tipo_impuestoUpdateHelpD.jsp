<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Descripcion Campos Tipo de Impuestos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
</head>

<body>
<br>


<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Tipo de Impuestos </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Modificar Tipo de Impuestos</td>
        </tr>
        <tr>
          <td width="149" class="fila">Codigo impuesto</td>
          <td width="525"  class="ayudaHtmlTexto">Campo donde se digita el c&oacute;digo del impuesto. </td>
        </tr>
        <tr>
          <td class="fila">Concepto</td>
          <td  class="ayudaHtmlTexto">Campo donde se digita el concepto del impuesto. </td>
        </tr>
        <tr>
          <td class="fila">Fecha de vigencia</td>
          <td  class="ayudaHtmlTexto">Campo donde se selecciona la fecha de vigencia del impuesto. </td>
        </tr>
        <tr>
          <td class="fila">Pocentaje IVA </td>
          <td  class="ayudaHtmlTexto">Campo donde se digita el valor n&uacute;merico correspondiente al porcentaje del IVA. </td>
        </tr>
        <tr>
          <td class="fila">Porcentaje RIVA </td>
          <td  class="ayudaHtmlTexto">Campo donde se digita el valor n&uacute;merico correspondiente al porcentaje del RIVA.</td>
        </tr>
        <tr>
          <td class="fila">Cuenta contable RIVA </td>
          <td  class="ayudaHtmlTexto">Campo donde se digita la cuenta contable del impuesto RIVA. </td>
        </tr>
        <tr>
          <td class="fila">Agencia</td>
          <td  class="ayudaHtmlTexto">Campo donde se selecciona la agencia relacionada al tipo de ompuesto seleccionado. </td>
        </tr>
        <tr>
          <td class="fila">Cuenta contable</td>
          <td  class="ayudaHtmlTexto">Campo donde se digita la cuenta contable del impuesto. </td>
        </tr>
        <tr>
          <td class="fila">Porcentaje RFTE</td>
          <td  class="ayudaHtmlTexto">Campo donde se digita el valor n&uacute;merico correspondiente al porcentaje del RFTE.</td>
        </tr>
        <tr>
          <td class="fila">Descripci&oacute;n</td>
          <td  class="ayudaHtmlTexto">Campo donde se digita la descripci&oacute;n del impuesto de tipo RFTE. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
            <input name="image" type="image" style="cursor:default " src = "<%=BASEURL%>/images/botones/modificar.gif">
          </div></td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que valida el la informaci&oacute;n digitada en los campos para modificar la informaci&oacute;n del impuesto. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
            <input type="image" src = "<%=BASEURL%>/images/botones/anular.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto"> Bot&oacute;n que anula el registro del impuesto en pantalla. </td>
        </tr>
        <tr>
          <td class="fila"><div align="center">
            <input type="image" src = "<%=BASEURL%>/images/botones/salir.gif" style="cursor:default ">
          </div></td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que cierra  la ventana. </td>
        </tr>
        
      </table>
    </td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
