<!--
- Autor : 
- Date  : 
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la b�squeda en el archivo de bancos.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Buscar Banco</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
</head>

<body onresize="redimensionar();" onload = "redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Buscar Banco"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<% String tipid="";%>
<form name="forma" method="post" action="<%=CONTROLLER%>?estado=Banco&accion=Serch&listar=True&sw=True">
  <table width="400" border="2" align="center">
    <tr>
      <td><table width="100%" align="center"  class="tablaInferior">
        <tr class="fila">
          <td width="169" class="subtitulo1">&nbsp;Informaci&oacute;n</td>
          <td width="207" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
        </tr>
      <tr class="fila">
        <td width="169" align="left" valign="middle" >Nombre del Banco </td>
        <td width="207" valign="middle"><input name="c_banco" type="text" class="textbox" id="c_banco" size="15" maxlength="15" ></td>
      </tr>
    <tr class="fila">
      <td width="169" valign="middle" >Sucursal</td>
      <td valign="middle"><input name="c_sucursal" type="text" class="textbox" id="c_sucursal" size="30" maxlength="30" ></td>
    </tr> 
    <tr class="fila">
      <td width="169" valign="middle" >Agencia</td>
      <td valign="middle"><select name="c_agencia" class="textbox" id="c_agencia">
		<option> </option>
		<% //model.agenciaService.cargarAgencias();
		Vector VecAgen = model.agenciaService.getVectorAgencias();
		Agencia a;
		for(int i = 0; i<VecAgen.size(); i++){	
			a = (Agencia) VecAgen.elementAt(i);
			if ( VecAgen.size() >0 ){ %>
				<option value="<%=a.getId_agencia()%>"><%=a.getNombre()%></option><%}
			else{%>
				<option> </option><%} 
			}%>
	   </select>
	  </td>
    </tr> 
    <tr class="fila">
      <td width="169" valign="middle" >Distrito</td>
      <td valign="middle"><select name="c_distrito" id="c_distrito" class="textbox">
		<option> </option>
		<option value="FINV">FINV</option>
		<option value="VEN">VEN</option>
       </select></td>
    </tr> 		
    <tr class="fila">
      <td width="169" valign="middle" >Cuenta</td>
      <td valign="middle"><input name="c_cuenta" type="text" onKeyPress="soloDigitos(event,'decOK')" class="textbox" id="c_cuenta" size="30" maxlength="30" ></td>
    </tr> 
    <tr class="fila">
      <td width="169" valign="middle" >Codigo Cuenta </td>
      <td valign="middle"><input name="c_codigo_cuenta" onKeyPress="soloDigitos(event,'decOK')" type="text" class="textbox" id="c_codigo_cuenta" size="25" maxlength="25" ></td>
    </tr>
    <tr class="fila">
      <td valign="middle" >Posici&oacute;n Bancaria </td>
      <td valign="middle"><select name="posbancaria" id="posbancaria">
        <option value=""></option>
        <option value="S">SI</option>
        <option value="N">NO</option>
      </select></td>
    </tr> 		
  </table></td>
    </tr>
  </table>
<p>
<div align="center"><img src="<%=BASEURL%>/images/botones/buscar.gif" style="cursor:hand" title="Buscar un codigo de demora" name="buscar"  onClick="forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/detalles.gif" style="cursor:hand" title="Buscar todos los codigos de demora" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Banco&accion=Serch&listar=True&sw=False'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
</p>	 
</form>
</div>
<%=datos[1]%>
</body>
</html>
