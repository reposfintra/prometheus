<!--
- Autor : 
- Date  : 
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra un listado de registros del archivo de bancos.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
 
<html>
<head>
<title>Listado de Bancos</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
</head>


<body onresize="redimensionar()" onload = "redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Listado de Bancos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%  String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 16;
    int maxIndexPages = 10;
    Banco b;
	/*String d = (String)session.getAttribute("distrito_ban");
	String ba = (String)session.getAttribute("banco_ban");
	String s = (String)session.getAttribute("sucursal_ban");
	String c = (String)session.getAttribute("cuenta_ban");
	String a = (String)session.getAttribute("agencia_ban");
	String cc = (String)session.getAttribute("codigo_cuenta_ban");
	String pb = (String)session.getAttribute("posbancaria");
	model.servicioBanco.searchDetalleBanco(d, ba, s, c, a, cc, pb);*/ 
    Vector vec = model.servicioBanco.getBancos();
    if ( vec.size() >0 ){
%>
</p>
<table width="950" border="2" align="center">
    <tr>
      <td>
	  <table width="100%" align="center">
              <tr>
                <td width="373" class="subtitulo1">&nbsp;Datos Banco </td>
                <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
        </table>
		  <table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
          <tr class="tblTitulo" align="center">
          <td width="11%" align="center">Nombre Banco </td>
          <td width="17%"  align="center">Sucursal</td>
          <td width="14%" align="center">Agencia</td>
          <td width="8%" align="center">Distrito</td>
          <td width="23%" align="center">Cuenta</td>
          <td width="18%" align="center">Codigo Cuenta </td>
          <td width="9%" align="center">Posici&oacute;n<br>
            Bancaria</td>
          </tr>
        <pg:pager
         items="<%=vec.size()%>"
         index="<%= index %>"
         maxPageItems="<%= maxPageItems %>"
        maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
        <%-- keep track of preference --%>
        <%
      for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vec.size()); i < l; i++)
	  {
          b = (Banco) vec.elementAt(i);%>
        <pg:item>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand" 
        onClick="window.open('<%=CONTROLLER%>?estado=Banco&accion=Serch&listar=False&pagina=BancoModificar.jsp&carpeta=jsp/cxpagar/banco&c_banco=<%=b.getBanco()%>&c_distrito=<%=b.getDstrct()%>&c_cuenta=<%=b.getCuenta()%>&c_sucursal=<%=b.getBank_account_no()%>&msg=&mensaje=' ,'','status=yes,scrollbars=yes,width=700,height=420,resizable=yes');">
          <td align="center" class="bordereporte"><%=b.getBanco()%></td>
          <td align="center" class="bordereporte"><%=b.getBank_account_no()%></td>  
          <td align="center" class="bordereporte"><%=b.getNombre_Agencia()%></td>
          <td align="center" class="bordereporte"><%=b.getDstrct()%></td>
          <td align="center" class="bordereporte"><%=b.getCuenta()%></td>
          <td align="center" class="bordereporte"><%=b.getCodigo_cuenta()%></td>
          <td align="center" class="bordereporte"><%=b.getPosbancaria()%></td>
        </tr>
        </pg:item>
        <%}%>
        <tr  class="pie" align="center">
          <td td height="20" colspan="7" nowrap align="center">
		   <pg:index>
            <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
           </pg:index> 
	      </td>
        </tr>
        </pg:pager>
      </table></td>
    </tr>
</table>
<br>
      <%}
 else { %>

  <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <br>
  <%}%>
   <table width="950" border="0" align="center">
    <tr>
      <td>
	  <img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Buscar todos los bancos" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/cxpagar/banco&pagina=BancoBuscar.jsp&titulo=Buscar Bancos&marco=no&opcion=27'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	  </td>
	</tr>
</table>
</div>
</body>
</html>
