<!--
- Autor : 
- Date  : 
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que modifica un registro del archivo de bancos.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Modificar Banco</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<body onresize="redimensionar()" onload = "redimensionar();<%if(request.getParameter("reload")!=null){%>window.opener.location.reload();<%}%>">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Modificar Banco"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%	String mensaje = (String) request.getAttribute("msg");
	Banco banc = model.servicioBanco.getBannco();
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<FORM name='forma' method='POST'  action="<%=CONTROLLER%>?estado=Banco&accion=Update">
  <table width="680" border="2" align="center">          
    <tr>
      <td>
	    <table width="100%" class="tablaInferior">
          <tr class="fila">
            <td colspan="2" align="left" class="subtitulo1">&nbsp;Banco</td>
            <td colspan="2" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
          </tr>
          <tr class="fila">
            <td align="left" >Nombre del Banco </td>
            <td valign="middle"><%=banc.getBanco()%><input name="c_banco" type="hidden" id="c_valor" value="<%=banc.getBanco()%>"></td>
            <td valign="middle">Sucursal</td>
            <td valign="middle"><%=banc.getBank_account_no()%><input name="c_sucursal" type="hidden" id="c_sucursal" value="<%=banc.getBank_account_no()%>"></td>
          </tr>
          <tr class="fila" id="cantidad">
            <td width="126" align="left" > Agencia </td>
            <td width="226" valign="middle">
              <select name="c_agencia" class="textbox" id="c_agencia">
                <%
				Vector VecAgen = model.agenciaService.getVectorAgencias();
				Agencia a;
				for(int i = 0; i<VecAgen.size(); i++){	
            	a = (Agencia) VecAgen.elementAt(i);
				if ( VecAgen.size() >0 ){ %>
                <option value="<%=a.getId_agencia()%>" <% if(a.getId_agencia().equals(banc.getCodigo_Agencia())){%> selected <% } %>><%=a.getNombre()%></option>
                <%}
				else{%>
                <option> </option>
                <%} 
				}%>
            </select><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
            <td width="129" valign="middle">Distrito</td>
            <td width="167" valign="middle"><%=banc.getDstrct()%><input name="c_distrito" type="hidden" id="c_distrito" value="<%=banc.getDstrct()%>">
            </td>
          </tr>
          <tr class="fila">
            <td width="126" align="left" > Cuenta </td>
            <td width="226" valign="middle"><input name="c_cuenta" type="text" class="textbox" id="c_cuenta" onKeyPress="soloAlfa(event)" value="<%=banc.getCuenta()%>" size="25" maxlength="25"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
            <td width="129" valign="middle">Codigo de Cuenta </td>
            <td width="167" valign="middle"><input name="c_codigo_cuenta" type="text" class="textbox" id="c_codigo_cuenta" onKeyPress="soloAlfa(event)" value="<%=banc.getCodigo_cuenta()%>" size="25" maxlength="25">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">
            </td>
          </tr>
          <tr class="fila">
            <td width="126" align="left" >Moneda</td>
            <td valign="middle"><select name="c_moneda" class="textbox" id="c_moneda">
                <%
				Vector mone = model.monedaSvc.getMonedas();
				  	for(int i = 0; i<mone.size(); i++){	
					Moneda m = (Moneda) mone.elementAt(i);	%>
                <option value="<%=m.getCodMoneda()%>" <% if(m.getCodMoneda().equals(banc.getMoneda())){%> selected <% } %> ><%=m.getNomMoneda()%></option>
                <%}%>
            </select><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
            <td valign="middle">Posici&oacute;n Bancaria </td>
            <td valign="middle"><select name="posbancaria" id="posbancaria">
              <option value="S" <%= (banc.getPosbancaria().compareTo("S")==0)? "selected" : "" %>>SI</option>
              <option value="N" <%= (banc.getPosbancaria().compareTo("N")==0)? "selected" : "" %>>NO</option>
            </select>
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
          </tr>
        </table></td>
    </tr>
  </table>
	<p>
	<div align="center"><img src="<%=BASEURL%>/images/botones/modificar.gif" style="cursor:hand" title="Modificar un banco" name="modificar"  onclick="return validarBanco();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
	<img src="<%=BASEURL%>/images/botones/anular.gif" style="cursor:hand" title="Anular un banco" name="anular"  onClick="window.location='<%=CONTROLLER%>?estado=Banco&accion=Anular&c_banco=<%=banc.getBanco()%>&c_distrito=<%=banc.getDstrct()%>&c_cuenta=<%=banc.getCuenta()%>&c_sucursal=<%=banc.getBank_account_no()%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
	<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
	</p>  
  <% if(mensaje!=null){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=mensaje%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%}%>
</form>
</div>
<%=datos[1]%>
</body>
</html>
