<!--  
	 - Author(s)       :      Jose de la rosa
	 - Description     :      AYUDA DESCRIPTIVA - Buscar Bancos
	 - Date            :      23/05/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>AYUDA DESCRIPTIVA - Ingresar Bancos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Bancos</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Buscar Bancos </td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2">INFORMACION DEL BANCO </td>
        </tr>
        <tr>
          <td  class="fila">Nombre del Banco</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el nombre del banco que desea agregar. Este campo es de m&aacute;ximo 15 caracteres. </td>
        </tr>
        <tr>
          <td  class="fila">Sucursal</td>
          <td  class="ayudaHtmlTexto">Campo para digitar la sucursal del banco que desea agregar. Este campo es de m&aacute;ximo 30 caracteres. </td>
        </tr>
        <tr>
          <td  class="fila">Agencia</td>
          <td  class="ayudaHtmlTexto">Campo para escoger la agencia relacionada al banco. </td>
        </tr>
        <tr>
          <td  class="fila">Distrito</td>
          <td  class="ayudaHtmlTexto">Campo para escoger el distrito del banco. </td>
        </tr>
        <tr>
          <td  class="fila">Cuenta</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el numero de cuenta que desea agregar. Este campo es de m&aacute;ximo 45 caracteres. </td>
        </tr>		
        <tr>
          <td  class="fila">Codigo de Cuenta</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el codigo de cuenta que desea agregar. Este campo es de m&aacute;ximo 25 caracteres. </td>
        </tr>
        <tr>
          <td  class="fila">Moneda</td>
          <td  class="ayudaHtmlTexto">Campo para escoger la moneda del banco. </td>
        </tr>			
        <tr>
          <td  class="fila">Posicion Bancaria</td>
          <td  class="ayudaHtmlTexto">Campo para escoger la posici&oacute;n bancaria. </td>
        </tr>			
        <tr>
          <td class="fila">Bot&oacute;n Buscar </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para confirmar y realizar la busqueda del banco.</td>
        </tr>
		<tr>
          <td class="fila">Bot&oacute;n Detalle </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que permite realizar una busqueda de todos los datos.</td>
        </tr>
		<tr>
          <td width="149" class="fila">Bot&oacute;n Salir</td>
          <td width="525"  class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista 'Buscar Banco' y volver a la vista del men&uacute;.</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</body>
</html>
