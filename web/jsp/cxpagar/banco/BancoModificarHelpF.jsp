<!--  
	 - Author(s)       :      Jose de la Rosa
	 - Description     :      AYUDA FUNCIONAL - Modificar Bancos
	 - Date            :      23/05/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>AYUDA FUNCIONAL - Modificar Bancos</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/cxpagar/banco/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE EQUIPOS WEB</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa para Ingresar Descuentos de Equipos.</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p>&nbsp;</p>
              </div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">En la siguiente pantalla se modifican los datos del banco.</p>
            </td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>ImagenModificar.JPG" border=0 ></div></td>
          </tr>

		<tr>
		<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
		<p class="ayudaHtmlTexto">El sistema verifica si el codigo de cuenta del banco esta lleno, si no lo estan le saldr&aacute; en la pantalla el siguiente mensaje. </p>
		</td>
		</tr>
		<tr>
		<td  class="ayudaHtmlTexto"><div align="center"><IMG  src="<%=BASEIMG%>ErrorCodigoCuenta.JPG" border=0 ></div></td>
		</tr>
		
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Si el banco se anula, aparecer&aacute; el siguiente mensaje. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>MensajeAnular.JPG" border=0 ></div></td>
</tr>
      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
