<!--
- Autor : 
- Date  : 
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el ingreso en el archivo de bancos.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Ingresar Banco</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<body onresize="redimensionar()" onload = "redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Ingresar Banco"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%	String mensaje = request.getParameter("mensaje");
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<FORM name='forma' method='POST'  action="<%=CONTROLLER%>?estado=Banco&accion=Insert">
  <table width="740" border="2" align="center">          
    <tr>
      <td>
	  <table width="100%" class="tablaInferior">
        <tr class="fila">
          <td colspan="2" align="left" class="subtitulo1">&nbsp;Banco</td>
          <td colspan="2"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
        </tr>
        <tr class="fila">
          <td align="left" >Nombre del Banco </td>
          <td valign="middle"><input name="c_banco" type="text" class="textbox" id="c_banco" size="15" maxlength="15">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
          <td valign="middle">Sucursal</td>
          <td valign="middle"><input name="c_sucursal" type="text" class="textbox" id="c_sucursal" size="30" maxlength="30">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
        </tr>
        <tr class="fila" id="cantidad">
          <td width="115" align="left" > Agencia </td>
          <td width="274" valign="middle">
		  <select name="c_agencia" class="textbox" id="c_agencia">
				<% 
				Vector VecAgen = model.agenciaService.getVectorAgencias();
				Agencia a;
				for(int i = 0; i<VecAgen.size(); i++){	
            	a = (Agencia) VecAgen.elementAt(i);
				if ( VecAgen.size() >0 ){ %>
                <option value="<%=a.getId_agencia()%>"><%=a.getNombre()%></option>
                <%}
				else{%>
                <option> </option>
                <%} 
				}%>
           </select>
		  <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
          <td width="123" valign="middle">Distrito</td>
          <td width="196" valign="middle">           
            <select name="c_distrito" id="c_distrito" class="textbox">
              <option value="FINV">FINV</option>
              <option value="VEN">VEN</option>
            </select>
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">            </td>
        </tr>
        <tr class="fila">
          <td width="115" align="left" > Cuenta </td>
          <td width="274" valign="middle"><input name="c_cuenta" onKeyPress="soloDigitos(event,'decOK')" type="text" class="textbox" id="c_cuenta" size="45" maxlength="45">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
          <td width="123" valign="middle">Codigo de Cuenta </td>
          <td width="196" valign="middle"><input name="c_codigo_cuenta" onKeyPress="soloAlfa(event)" type="text" class="textbox" id="c_codigo_cuenta" value="" size="25" maxlength="25">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">            </td>
        </tr>
		<tr class="fila">
          <td width="115" align="left" >Moneda</td>
          <td valign="middle"><select name="c_moneda" class="textbox" id="c_moneda">
            <%Vector mone = model.monedaSvc.getMonedas();
				  	for(int i = 0; i<mone.size(); i++){	
					Moneda m = (Moneda) mone.elementAt(i);	%>
            <option value="<%=m.getCodMoneda()%>"><%=m.getNomMoneda()%></option>
            <%}%>
          </select>
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
          <td valign="middle">Posicion Bancaria </td>
          <td valign="middle"><select name="c_posbancaria" id="c_posbancaria">
            <option value="S">SI</option>
            <option value="N">NO</option>
          </select>
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
		</tr>
      </table>
	  </td>
    </tr>
  </table>
<p>
<div align="center"><img src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand" title="Agregar un banco" name="imgaceptar"  onclick="return validarBanco();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/cancelar.gif" style="cursor:hand" name="regresar" title="Limpiar todos los registros" onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);" >&nbsp;
<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></div>
</p>  
  <% if(mensaje!=null){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=mensaje%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%}%>
</form>
</div>
<%=datos[1]%>
</body>
</html>
