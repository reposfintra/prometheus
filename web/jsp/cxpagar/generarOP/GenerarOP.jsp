<!--
- Autor : Ing. Fernell Villacob
- Modificado: Ing Sandra Escalante
- Date  : 02 Febrero 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Vista que permite capturar fechas de cumplido  de OC para generar sus OPs correspondientes
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>  
        <title>.: Generacion de OPs</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
	<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script>  
	<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>     
 
	<script>

       </script>

</head>
   
<body onLoad="redimensionar();" onResize="redimensionar();">



<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    %>

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Generación de Facturas OP"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
   <% Usuario Usuario   = (Usuario) session.getAttribute("Usuario"); 
      String  login     = Usuario.getLogin();  
      String  hoy       = Utility.getHoy("-"); 
      String  msj       = request.getParameter("msj");
      List    lista     = model.OpSvc.getHc();%>
   

   <form action="" method='post' name='formulario' >
 
   <table width="400" border="2" align="center">
   <tr>
      <td>  
           <table width='100%' align='center' class='tablaInferior'>
           
              <tr class="barratitulo">
                <td colspan='2' >
                   <table cellpadding='0' cellspacing='0' width='100%'>
                     <tr>
                      <td align="left" width='65%' class="subtitulo1">&nbsp;Rango de Fecha planillas cumplidas</td>
                      <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"  height="20" align="left"><%=datos[0]%></td>
                    </tr>
                   </table>
                </td>
             </tr>
              
            
              <tr class="fila">
                <td class='letrafila' width='35%'> Fecha Inicial</td>
                <td width='*'>
                      <!-- Fecha Inicial -->
                      <input  name='fechaInicio' size="20" readonly="true" class="textbox" value='<%=hoy%>' style='width:50%'> 
                      <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(fechaInicio);return false;" hidefocus>
                      <img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt="">
                      </a>
                </td>
             </tr>
              
              <tr class="fila">
                <td class='letrafila'>Fecha Final </td>
                <td>
                     
                     <!-- Fecha Final -->                   
                     <input  name='fechaFinal' size="20" readonly="true" class="textbox" value='<%=hoy%>' style='width:50%'> 
                     <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(fechaFinal);return false;" hidefocus>
                      <img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt="">
                     </a>
                     
                </td>
             </tr>
             <tr class="fila">
                <td class='letrafila'>Tipo HC </td>
                <td>
                     
                     <!-- Campo para el Handle Code -->
                     <select name="HC" class="textbox" >
                      <%for(int i=0; i < lista.size();i++ ){
                           Hashtable fila = (Hashtable) lista.get(i);
                           String Codigo  = (String)fila.get("codigo"); 
                           String Descrip = (String)fila.get("descripcion"); 
                          %>
                          <option value='<%=Codigo%>'><%=Descrip%></option>  

                       <%}%>
                     </select>
                      
                    
                     
                </td>
             </tr>
        </table>
      </td>
   </tr>
   </table>   
   <br>
   <center>                  
   <input type='hidden'  value='<%=login%>' name='usuario' >
   <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="a"  height="21" title='Generar facturas' onClick="formulario.action='<%=CONTROLLER%>?estado=Generar&accion=OP'; if ( validarFechaIF(formulario)  ) {formulario.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
   <img src="<%=BASEURL%>/images/botones/salir.gif"   name="s"  height="21" title='Salir'            onClick="window.close();"                                                                                                         onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
</center>
 </form>
 
 
  
 
  <!-- mensajes -->
    <% if(msj!=null &&  !msj.equals("") ){%>        
         <table border="2" align="center" width="600">
              <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                  <tr>
                    <td width="420" class="mensajes"><%=msj%></td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="58">&nbsp;</td>
                  </tr>
                </table></td>
              </tr>
        </table>
    <%  } %>
    
    
    
</div> 

<%=datos[1]%>  


<!-- Necesario para los calendarios-->
<iframe width="188" height="166" name="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" id="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" src="<%=BASEURL%>/js/calendartsp/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"></iframe>

</body>
</html>
