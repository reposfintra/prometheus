<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
  <title>Generaci�n de Facturas OP - Ayuda</title>
  <META http-equiv=Content-Type content="text/html; charset=windows-1252">
  <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
  <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body>
<center>
<body>


<% String BASEIMG = BASEURL +"/images/ayuda/cxpagar/op/"; %>


<table width="100%"  border="2" align="center">
    <tr>
      <td >
 
            <table width='99%' align="center" cellpadding='0' cellspacing='0'>
                 <tr class="subtitulo" ><td height="20"><div align="center">MANUAL DE GENERACI�N DE OPs </div></td></tr>
                 <tr class="subtitulo1"> <td>Descripci&oacute;n del funcionamiento </td> </tr>

                 <tr>
                      <td  class="ayudaHtmlTexto" height='40'>
                         El programa permitir� generar las facturas de proveedor tipo  OP. Para lo cual, deber� seleccionar un rango de fechas
                         en el cual se buscar�n las facturas cumplidas, y un tipo handle code de proveedor(Filtro), para lo cual, de las facturas cumplidas 
                         seleccionadas, solo se seleccionar�n aquellas cuyo proveedor cumpla con el tipo establecido.                         
                      </td>
                 </tr>
                 
                 <tr>
                      <td  class="ayudaHtmlTexto" height='40'>
                         Para establecer dichos parametros, deber� definirlo en el formulario tal como lo indica la figura 1.                     
                      </td>
                 </tr>                 
                 
                 <tr><td  align="center" > 
                     <br><br>
                     <img  src="<%= BASEIMG%>OP.JPG" >                  
                     <br>
                     <strong>Figura 1</strong>
                     <br><br>
                     <br><br>
                 </td>
                 </tr>
                     
  
                <tr>
                    <td  class="ayudaHtmlTexto" height='40'>
                        Una vez se hallan definidos los parametros de generaci�n, deber� realizar cl�ck en el bot�n Aceptar. El programa
                        ejecutar� el proceso el cual podr� ver su estado en el log de procesos del sistema.
                        <br><br>
                    </td>
                </tr>  
 
                
                <tr>
                    <td  class="ayudaHtmlTexto" height='40'>
                        El valor de la factura, ser� el valor de la liquidaci�n de la planilla.
                        <br><br>
                    </td>
                </tr>  
                
                
                <tr>
                    <td  class="ayudaHtmlTexto" height='40'>
                        Si la busqueda con dichos parametros arroja planillas, el programa generar� archivos que le permiten visualizar cuales facturas ha creado
                        y cuales dej� de crear( Estos archivos podra visualizarlo en su directorio del sistema ), en caso contrario ecribir� un mensaje en el 
                        log de procesos.
                        <br><br><br><br>
                    </td>
                </tr> 
                
                 
                 
           </table>
            
      </td>
  </tr>
</table>
<br><br>
<img src="<%=BASEURL%>/images/botones/salir.gif"      name="exit"    height="21"  title='Salir'                onClick="parent.close();"                 onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">          





</body>
</html>
