<!--  
     - Author(s)       :      Osvaldo P�rez Ferrer
     - Date            :      12-03-2007
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description: Cambio de propietario a OP
 --%>

<%@ page session   ="true"%> 
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="com.tsp.operation.model.beans.*" %>
<%@ include file   ="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<%
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>

<html>
    <head>

        <title>-- Cambiar Propietario a OP --</title>    
        <link href="<%= BASEURL %>/css/estilostsp.css"     rel="stylesheet" type="text/css"> 
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validate.js"></script> 
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validar.js"></script> 
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    
    
    
    
     
    </head>
    <body>


        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Cambio Propietario OP"/>
        </div>

        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
            <center>


 
  <% 
     String  msj        =  request.getParameter("msj");
     String  url        =  CONTROLLER +"?estado=CambioPropietario&accion=OP";
     Object  obj        =  model.OpSvc.getObjeto();
     String  nombre     =  request.getAttribute("nombre_proveedor")!=null? (String)request.getAttribute("nombre_proveedor") : "" ;
     String propietario = "";
     boolean modify     = true;
%>
   
<% if( request.getParameter("load")!=null ){ obj = null; }%>

<% if( obj==null ){ %>

          <form action='<%=url%>&evento=SEARCH' method="post" name="form1" id="form1">
  
            <table width="400"  border="2" align="center">
                <tr>
                    <td colspan='2'>             

                        <table width="100%"   align="center">
                            <tr>
                                <td colspan='2'>
                                    <table width='100%'  class="barratitulo">
                                        <tr class="fila">
                                            <td align="left" width='70%' class="subtitulo1" nowrap> &nbsp;Cambio Propietario OP</td>
                                            <td align="left" width='30%' ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                                        </tr>
                                                                                                
                                                
                                    </table>
                                </td>
                            </tr>
                                                                                                                                        
                            <tr class="fila">
                                <td width='30%' >&nbsp Planilla </td>
                                <td>
                                    <input name='planilla' id='planilla' type="textbox" style='width:50%'  maxlength='10'>
                                </td>
                            </tr>
                                                                                                                                                                               
                        </table>

                    </td>
                </tr>
            </table>                                                      
            
            <p>       
            <img src='<%=BASEURL%>/images/botones/buscar.gif'    style=" cursor:hand"  title='Crear....'       name='i_crear'       onclick="form1.submit();" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>               
            <img src='<%=BASEURL%>/images/botones/salir.gif'      style=' cursor:hand'  title='Salir...'        name='i_salir'       onclick='parent.close();'      onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
            </p> 
       
            <%if( request.getParameter("load")==null ){%>
                <table border="2" align="center">
                    <tr>
                        <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                            <tr>
                                <td width="550" align="center" class="mensajes">No se encontr� la planilla</td>
                                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="58">&nbsp;</td>
                            </tr>
                        </table></td>
                    </tr>
                </table>
            <%}%>
            
            </form>
            
<%}else{%>  
        
        <% Hashtable h = (Hashtable)obj; %>
        
        <% String modificado = request.getParameter("ok")!=null? request.getParameter("ok"):""; %>
        
        <% if( !modificado.equals("ok") ){ %>
        
            <% if(  h.get("documento")!=null  && !((String)h.get("documento")).equals("") ){ %>
            <form action="<%=url%>" method="post" name="form1" id="form1">

                <table width="400"  border="2" align="center">
                    <tr>
                        <td colspan='2'>             

                            <table width="100%"   align="center">
                                <tr>
                                    <td colspan='2'>
                                        <table width='100%'  class="barratitulo">
                                            <tr class="fila">
                                                <td align="left" width='70%' class="subtitulo1" nowrap> &nbsp;Cambio Propietario OP</td>
                                                <td align="left" width='30%' ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
                                            </tr>


                                        </table>
                                    </td>
                                </tr>
                                <% propietario    = (String)h.get("proveedor");
                                   String neto    = (String)h.get("neto");
                                   String saldo   = (String)h.get("saldo");
                                   String corrida = (String)h.get("corrida");
                                   String abono   = (String)h.get("abono");%>
                                   
                                <tr>    <td class="fila" width='40%' >&nbsp;Planilla: </td><td width='60%' class='letra'><%=h.get("numpla")%></td>  </tr>
                                <tr>    <td class="fila" width='40%' >&nbsp;Distrito: </td><td class='letra'><%=h.get("dstrct")%></td>  </tr>
                                <tr>    <td class="fila" width='40%' >&nbsp;OP: </td><td class='letra'><%=h.get("documento")%></td>     </tr>
                                <tr>    <td class="fila" width='40%' >&nbsp;Propietario: </td><td class='letra'><%=propietario%></td>  </tr>
                                <tr>    <td class="fila" width='40%' >&nbsp;Nombre: </td><td class='letra'><%=h.get("nom_proveedor")%></td> </tr>
                                <tr>    <td class="fila" width='40%' >&nbsp;Moneda:</td><td class='letra'><%=h.get("moneda")%></td>   </tr>
                                <tr>    <td class="fila" width='40%' >&nbsp;Valor Neto: </td><td class='letra'><%=Util.customFormat( Double.valueOf( neto ) )%></td>    </tr>
                                <tr>    <td class="fila" width='40%' >&nbsp;Saldo: </td><td class='letra'><%=Util.customFormat( Double.valueOf( saldo ) )%></td>   </tr>

                                <tr>
                                    <td class='fila'>&nbsp;Nuevo Propietario:</td>
                                    <td class='fila'> 
                                        <input type='textbox' name='provee' value="<%=model.ChequeXFacturaSvc.getProveedor() %>" id='provee' onblur='if( trim(provee.value) != ""){ buscarNombre(this); }'>
                                        <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif' width='19' style=" cursor:hand"  title='Buscar Proveedor....'  onclick="buscarProveedor();"  onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>                                    
                                    </td> 
                                </tr>                                                                
                                
                                <%
                                   //Si esta en corrida sin pagar
                                  if( neto.equals( saldo ) && !corrida.equals("") ){
                                      modify = false;
                                      msj    = "No es posible cambiar el propietario a la OP, se encuentra incluida en la corrida "+ corrida;
                                  }
                                  if( !abono.equals("0.00") && !saldo.equals( "0.00" ) ){
                                      modify = false;
                                      msj    = "No es posible cambiar el propietario a la OP, tiene abonos";
                                  }
                                  %>
                                
                                <% if( !nombre.equals("") ){ %>
                                    <tr><td class='fila'>&nbsp;Nombre:<td class='fila'><input style='border:0; width:100%' class='fila' readonly type='textbox' name='nombre' id='nombre' value='<%=nombre%>'></td></tr>
                                <%}else{%>
                                    <input type='hidden' id='nombre'>
                                <%}%>
                            </table>

                        </td>
                    </tr>
                </table>              


                </form>

                <%}else{%>
                    <table border="2" align="center">
                        <tr>
                            <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                <tr>
                                    <td width="550" align="center" class="mensajes">La planilla digitada no posee OP</td>
                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                    <td width="58">&nbsp;</td>
                                </tr>
                            </table></td>
                        </tr>
                    </table>
                    <% modify = false;%>
                    <%}%>
            <%}%>
            
            <% if( modificado.equals("ok") ) modify = false; %>
            
            <p>
            <img src='<%=BASEURL%>/images/botones/regresar.gif'    style=" cursor:hand"  title='Crear....'       name='i_regresar'       onclick="location.replace('<%=BASEURL%>/jsp/cxpagar/generarOP/cambiarPropietarioOP.jsp?load=yes')" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>               
            <% if( modify == true ){ %> <img src='<%=BASEURL%>/images/botones/modificar.gif'    style=" cursor:hand"  title='Crear....'       name='i_crear'       onclick="modificar(form1,'<%=nombre%>')" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'> <%}%>
            <img src='<%=BASEURL%>/images/botones/salir.gif'      style=' cursor:hand'  title='Salir...'        name='i_salir'       onclick='parent.close();'      onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'>
            </p> 

<%}%>
            <!-- Mensaje -->

  
   <% if(msj!=null && !msj.equals("")){%>
            <table border="2" align="center">
                <tr>
                    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                        <tr>
                            <td width="550" align="center" class="mensajes"><%=msj%></td>
                            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                            <td width="58">&nbsp;</td>
                        </tr>
                    </table></td>
                </tr>
            </table>
    <%  } %>
        
    <div style='position:relative'>
            <table>
                <tr>
                <td id='loading' class='informacion'></td>
                <tr>
            </table>
        </div>     
    
        </div>                                
        
<%=datos[1]%>
    </body>
</html>

<script>
    function buscarNombre( txt ){
        location.replace("<%=CONTROLLER%>?estado=CambioPropietario&accion=OP&evento=NOMBRE&provee="+txt.value);        
    }
    
    function buscarProveedor(){
        window.open( "<%=BASEURL%>/consultas/consultaProveedor.jsp",'','top=20,left=200,width=700,scrollbars=yes,status=yes');
    }
    
    function modificar( form, nombre ){
        
        if( trim(form.provee.value) != '' ){
            if( trim(form.provee.value) != '<%=propietario%>' ){
                if( nombre != '' ){
                    anularBtn( i_crear );
                    anularBtn( i_regresar );
                    anularBtn( i_salir );
                    document.getElementById("loading").innerHTML="<img src='<%=BASEURL%>/images/cargando.gif'> &nbsp;Procesando...";
                    location.replace("<%=CONTROLLER%>?estado=CambioPropietario&accion=OP&evento=MODIFICAR&provee="+form.provee.value);
                }else{
                    alert('No se encontro proveedor con el nit dado');
                    form.provee.focus();
                }
            }else{
                alert("Debe ingresar un nit diferente al propietario actual");
            }
        }else{
            alert('Debe ingresar el NIT del nuevo Propietario');
            form.provee.focus();
        }
    
    }
</script>
