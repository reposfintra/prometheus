<!--
- Autor     : Ing. Fernell Villacob
- Modificado: Ing Sandra Escalante
- Date      : 29 Noviembre 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Vista que permite formar facturas de la tabla movpla
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<!-- Para los botones -->
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>

<html>
<head>
        <title>Facturas Mov Planilla</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
	<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script>  
	<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script> 
	
</head>
<body>


<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Facturas Adicionales"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
 <center> 
   
 <% String msj =  request.getParameter("msj"); %>
 
       
 <form action='<%=CONTROLLER%>?estado=Adicion&accion=FacturasOP&evento=GENERAR' method='post' name='formulario' onsubmit='return false;'>
 
           <table width="500" border="2" align="center">
                  <tr>
                     <td>
                        <table width='100%' align='center' class='tablaInferior'>

                                  <tr>
                                        <td class="barratitulo" colspan='2' >
                                               <table cellpadding='0' cellspacing='0' width='100%'>
                                                     <tr class="fila">
                                                         <td width="70%" align="left" class="subtitulo1">&nbsp;Generaci�n Facturas Adicionales </td>
                                                         <td width="*"   align="left"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"  align="left"></td>
                                                    </tr>
                                               </table>   
                                        </td>
                                  </tr>

                                  <tr class="barratitulo">
                                         <td  class='informacion' height='60'>
                                                   <br>
                                                   <b>1.</b> Permite generar facturas a partir de los movimientos de planillas. Para lo cual:<br>
                                                   - La planilla debe tener factura OP.<br>
                                                   - Dichos movimientos no deben pertenecer a ninguna factura.<br>
                                                   Si la factura se encuentra pagada genera otra factura, de lo contrario genera ND,NC.<br><br>
                                                   
                                                   <b>2.</b> Incluye tambi�n planillas anuladas para generar facturas que permitan recuperar anticipos.

                                                   <br><br>

                                          </td>
                                  </tr> 
                        </table>
                    </td>
                </tr>
            </table>  
            
     </form>
    
     <!--botones -->   
      <p>
            <tsp:boton value="aceptar"   onclick="formulario.submit();"/>                 
            <tsp:boton value="salir"     onclick="parent.close();"/> 
      </p>

      
      
      
 
     <% if( ! msj.equals("") ){%>
          <table border="2" align="center">
                  <tr>
                    <td>
                        <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                              <tr>
                                    <td width="400" align="center" class="mensajes"><%=msj%></td>
                                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                    <td width="58">&nbsp; </td>
                              </tr>
                         </table>
                    </td>
                  </tr>
          </table>
    <%}%>
   

</div> 


</body>
</html>
