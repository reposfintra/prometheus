<%-- 
    Document   : modificarCompraCartera
    Created on : 27/09/2018, 03:56:51 PM
    Author     : jbermudez
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Modificar Compra Cartera</title>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>       
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>
        <style>
            .content-wrapper {
                max-width: 700px;
                margin: 0 auto;
                text-align: center;
                font-family: Verdana, Tahoma, Arial, Helvetica, sans-serif;
            }

            .searcher-wrapper {
                background-color: white;
                border: 1px solid black;
                padding-bottom: 5px;
            }

            h5 {
                background-color: #2A88C8;
                color: white;
                margin: 0 0 5px 0;
            }

            #negocio {
                color: black;
            }

            #lupa {
                width: 15px;
                height: 15px;
                cursor: pointer;
            }

            .results-wrapper {
                margin-top: 30px;
                display: none;
                padding: 5px;
            }

            #titulo {
                text-align: center;
                padding: 1px;
            }

            #tResults {
                margin: 5px auto;
                border: 1px solid black;
                border-collapse: collapse;
                width: 95%;
            }

            #tResults th, #tResults td {
                border: 1px solid black;
                text-align: center;
            }
        </style>
    </head>
    <body>
        <div id="capaSuperior" style="width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Modificar Compra Cartera"/>
        </div>
        <div class="content-wrapper">
            <div class="searcher-wrapper">
                <h5>FILTRO DE BÚSQUEDA</h5>
                <label for="negocio">Negocio: </label>
                <input type="text" id="negocio">
                <img id="lupa" src="./images/botones/iconos/lupa.gif" alt="lupa" title="Buscar">
            </div>
            <div class="results-wrapper ui-jqgrid ui-widget ui-widget-content ui-corner-all" id="result">
                <div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" id="titulo">COMPRA DE CARTERA</div>
                <table id="tResults">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Entidad</th>
                            <th>Nit</th>
                            <th>Cuenta</th>
                            <th>Valor</th>
                        </tr>
                    </thead>
                    <tbody></tbody>
                </table>
                <div class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" id="aceptar">
                    <span class="ui-button-text">Aceptar</span>
                </div>
            </div>
        </div>
        <div id="dialogo" class="ventana" style="display:none;">
            <p id="msj"></p>
        </div>
        <script>
            window.onload = function () {
                document.getElementById("lupa").addEventListener("click", function () {
                    buscarNegocio();
                });
                document.getElementById("aceptar").addEventListener("click", function () {
                    aceptar();
                });
            }

            let secuencia = [];
            let nit = [];
            let cuenta = [];
            let valor = [];

            function buscarNegocio() {
                let negocio = document.getElementById("negocio").value;
                let result = document.getElementById("tResults");
                let resultBody = result.childNodes[3];
                let xhr;

                if (window.XMLHttpRequest) {
                    xhr = new XMLHttpRequest();
                } else {
                    xhr = new ActiveObject();
                }

                xhr.onreadystatechange = function () {
                    if (xhr.status === 200) {
                        if (xhr.readyState === 4) {
                            let json = JSON.parse(xhr.responseText);
                            resultBody.innerHTML = "";
                            if (json.respuesta) {
                                mensajesDelSistema("Información", json.respuesta, "300px");
                            } else if (json.length > 0) {
                                for (let i = 0; i < json.length; i++) {
                                    resultBody.innerHTML += '<tr><td>' + json[i].secuencia + '. </td><td>' + json[i].entidad + '</td><td>' + json[i].nit + '</td><td>' + json[i].cuenta
                                            + '</td><td><input type="number" name="valor" value="' + json[i].valor + '" onchange="agregarModificado(' + json[i].secuencia + ',' + json[i].nit + ',\''
                                            + json[i].cuenta + '\', this.value)"></td></tr>';
                                }
                                result.parentNode.style.display = "block";
                            } else {
                                resultBody.innerHTML = "";
                                mensajesDelSistema("Informacion", "No se encontraron resultados para el negocio digitado.");
                            }
                            limpiarModificado();
                        }
                    }
                };
                xhr.open("GET", "./controller?estado=GestionSolicitud&accion=Aval&opcion=cargarCompraCartera&negocio=" + negocio, true);
                xhr.send();
            }

            function agregarModificado(s, n, c, v) {
                let index = secuencia.indexOf(s);
                if (index != -1) {
                    valor[index] = v;
                } else {
                    secuencia.push(s);
                    nit.push(n);
                    cuenta.push(c);
                    valor.push(v);

                    console.log(secuencia);
                    console.log(nit);
                    console.log(cuenta);
                    console.log(valor);
                }
            }

            function limpiarModificado() {
                secuencia.length = 0;
                nit.length = 0;
                cuenta.length = 0;
                valor.length = 0;
            }

            function aceptar() {
                if (secuencia.length == 0) {
                    mensajesDelSistema("Información", "No se ha modificado ningún dato");
                } else {
                    let xhr, data = {
                        negocio: document.getElementById("negocio").value,
                        secuencia: secuencia,
                        nit: nit,
                        cuenta: cuenta,
                        valor: valor
                    };

                    if (window.XMLHttpRequest) {
                        xhr = new XMLHttpRequest();
                    } else {
                        xhr = new ActiveObject();
                    }

                    xhr.onreadystatechange = function () {
                        switch (xhr.status) {
                            case 200:
                                if (xhr.readyState === 4) {
                                    let json = JSON.parse(xhr.responseText);
                                    if (json.respuesta) {
                                        mensajesDelSistema("Informacion", json.respuesta);
                                        buscarNegocio();
                                    } else {
                                        mensajesDelSistema("Error", json.error);
                                    }
                                }
                                break;
                            case 400:
                            case 404:
                            case 500:
                                mensajesDelSistema("Error", "No se pudo establecer la conexión con el servidor");
                                break;
                        }
                    };
                    xhr.open("POST", "./controller?estado=GestionSolicitud&accion=Aval&opcion=modificarCompraCartera", true);
                    xhr.send(JSON.stringify(data));
                }
            }

            function mensajesDelSistema(titulo, msj, width) {

                var botones, title;
                $("#msj").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
                title = titulo;
                botones = {"Aceptar": function () {
                        $(this).dialog("close");
                    }};
                $("#dialogo").dialog({
                    open: function (event, ui) {
                        $(".ui-dialog-titlebar-close", ui.dialog | ui).hide();
                    },
                    width: width,
                    height: "auto",
                    show: "scale",
                    hide: "scale",
                    title: title,
                    resizable: true,
                    position: "center",
                    modal: true,
                    closeOnEscape: false,
                    buttons: botones
                });
            }
        </script>
    </body>
</html>
