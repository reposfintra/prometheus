<%-- 
    Document   : auditoriaVentas
    Created on : 10/09/2015, 10:10:26 AM
    Author     : mariana
--%>

<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page session="true" %>
<%@page errorPage="/error/ErrorPage.jsp" %>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/CambioTercero.js"></script> 
        <script type="text/javascript" href="http://www.ddginc-usa.com/spanish/editcheck.js"></script>
        <title>CAMBIO TERCERO</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=CAMBIO TERCERO "/>
        </div>
        <%
            Usuario usuario = (Usuario) request.getSession().getAttribute("Usuario");
            String empresa = usuario.getEmpresa();
            if (empresa == "FINV") {
                empresa = "FINTRA";
            } else {
                if (empresa == "STRK") {
                    empresa = "SELECTTICK";
                }
            }

        %>
    <center>
        <div id="tablita" style="top: 170px;width: 451px;border-radius: 4px 4px 0px 0px;" >
            <div id="encabezadotablita" style="width: 441px">
                <label class="titulotablita"><b>FILTRO DE BUSQUEDA</b></label>
            </div> 
            <label>Numero de Comprobante</label>
            <input type="text" id="comprobante" name = "comprobante" style="margin-top: 10px;margin-left: 9px; margin-bottom: 5px;width: 90px">
            <label>Empresa: </label>
            <label><%= empresa%></label>

        </div>
        <div id="tablita" style="top: 170px;width: 451px;border-radius: 0;">

            <div id ='botones'>
                <button id="buscar" style="margin-top: 6px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                        role="button" aria-disabled="false" onclick="">
                    <span class="ui-button-text">Buscar</span>
                </button> 
                <button id="limpiar" style="margin-top: 6px;" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                        role="button" aria-disabled="false" onclick="">
                    <span class="ui-button-text">Limpiar</span>
                </button> 
            </div>
        </div>


        <div  id="contenedor1"  style="display: none;position: relative;top: 231px;width: 1531px;height: 520px; border-radius: 4px;border: 1px solid #234684; background: #FFF none repeat scroll 0% 0%;margin-top: -36px;">   
            <br>
            <table id="tabla_comprobantes" ></table>
            <div id="pager1"></div>
        </div>

        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>
        <div id="dialogo2" class="ventana">
            <p id="msj2">texto </p> <br/>
            <center>
                <img src="./images/cargandoCM.gif"/>
            </center>
        </div>
    </center>
</body>
</html>
