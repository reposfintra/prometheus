<%-- 
    Document   : reversarPlanillasTsp
    Created on : 14/10/2014, 09:10:25 AM
    Author     : lcanchila
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Reversar Planillas TSP</title>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/reestructuracion.css" type="text/css" rel="stylesheet" />
        <link href="<%=BASEURL%>/css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/jquery/jquery-ui/jquery-ui.css"/>

        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <!--<script type="text/javascript" src="<%=BASEURL%>/js/reestructurarNegocio.js"></script> -->
        
        <script>
            $(document).ready(
                function(){
                   var f = new Date();
                   document.getElementById("fecha").value = f.getFullYear()+ "-"+ (f.getMonth() +1) + "-" + f.getDate();
               }     
            )
        </script>    
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
           
        </div>


        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow:auto">
            <center>
                <table>
                    <tr>
                        <td>
                            <div class="k-block">
                                <div class="k-header"><span class="titulo">BUSCAR PLANILLAS TSP</span> </div>
                                <div id="contenido">
                                    <table border="0" class="table"  >
                                        <tr>
                                            <td class="td"><label for="fecha"> Fecha </label></td>
                                            <td class="td"><input type="text" required="true" name="fecha" readonly="true" id="fecha" /></td>
                                            <td class="td"><label for="user"> Usuario </label></td>
                                            <td class="td"><input type="text"  required="true" name="user" id="user"/></td>
                                            <td class="td">
                                                <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false">
                                                    <span class="ui-button-text">Buscar</span>
                                                </button>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </td>
                    </tr>
                </table>
            </center>    
        </div>        
    </body>
</html>
