<%-- 
    Document   : aplicacion_pagos_masivos_x_excel
    Created on : 18/12/2018, 10:56:16 AM
    Author     : jzapata
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="UTF-8">
        <title>APLICACIÓN PAGOS MASIVOS</title>
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />        
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script> 
        
        <style>
            #content-wrapper {
                width: 650px;
                margin: 0 auto;
            }

            fieldset {
                box-sizing: border-box;
                padding: 10px;
                border-width: thin;
                border-style: outset;
                border-color: black;
                border-spacing: 2px;
                border-collapse: separate;
                background: white;
            }

            legend {
                background: #2A88C8;
                color: white;
                padding: 5px;
            }

            #buttons-wrapper {
                margin-top: 10px;
                text-align: center;
            }

            #boton-aceptar:hover {
                cursor: pointer;
                background: url('/fintra/images/botones/aceptarOver.gif') no-repeat;
            }

            #periodo {
                width: 40px;
                margin-right: 10px;
            }

            #boton-aceptar {
                width: 90px;
                height: 21px;
                background: url('/fintra/images/botones/aceptar.gif') no-repeat;
                display: inline-block;
            }

            #boton-salir:hover {
                cursor: pointer;
                background: url('/fintra/images/botones/salirOver.gif') no-repeat;
            }

            #boton-salir {
                width: 90px;
                height: 21px;
                background: url('/fintra/images/botones/salir.gif') no-repeat;
                display: inline-block;
            }

            .ventana {
                display: none;
            }
            label{
                text-align: center;
                font-family: 'Lucida Grande','Lucida Sans',Arial,sans-serif;
                font-size: 15px;
                
            }
            .labelaso{
                font-weight: bold;
            }
            .tabla_Refe {
            font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
            border-collapse: collapse;
            width: 100%;
          }

            .tabla_Refe td, .tabla_Refe th {
              border: 1px solid #ddd;
              padding: 8px;
            }

            .tabla_Refe tr:nth-child(even){background-color: #f2f2f2;}

            .tabla_Refe tr:hover {background-color: #ddd;}

            .tabla_Refe th {
              padding-top: 12px;
              padding-bottom: 12px;
              text-align: left;
              background-color: #2A88C8;
              color: white;
            }
            .select {
                font-size: 14px;
                background: #2A88C8 ;
                color: white;
                font-weight: bold;
            }
        </style>
    </head>
    <body>
            <div id="capaSuperior" style="height:100px;">
                <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=APLICACIÓN PAGOS MASIVOS"/>
            </div>
            <div id="content-wrapper">
                <!--<form id="formulario" action="/fintra/controller?estado=AplicacionPagos&accion=Masivos&opcion=1" method="post" enctype="multipart/form-data">-->
                 <fieldset>  
                     <legend>Archivo de Aplicación de Pago</legend>
                <form id="formulario" name="formulario">
                   
                                       
                        
                        <label for="Tipo Archivo" class="labelaso">Seleccione tipo archivo</label>
                        <select id="tipo_archivo" onchange="mostrar_div(this.value)" class="select">
                            <option value="">...</option>
                            <option value="ASOBANCARIA">ASOBANCARIA</option>
                            <option value="PLANTILLA">PLANTILLA</option>
                        </select>
                        <!--<label for="archivo">Seleccione su archivo</label>-->
                        <!--<input type="file" name="archivo" required id="archivo" accept=".xlsx,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet">-->
                        <input type="file" id="archivo"  name="archivo">                        
                        <!--label for="periodo">Periodo</label>
                        <input type="text" id="periodo" maxlength="6">
                        <label for="periodos-checkbox">Múltiples periodos</label>
                        <input type="checkbox" id="periodos-checkbox"/-->
                        
                    
                </form>
                <div id="buttons-wrapper">
<!--                           <div id="boton-aceptar" onclick="cargarArchivo()"></div>
                           <div id="boton-salir" onclick="salir()"></div>-->
                            
                            <button style="font-weight: bold; font-size: 12px;" id="aceptar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"  onclick="cargarArchivo()">
                                <span class="ui-button-text">Aceptar</span>
                            </button>  

                           <button style="font-weight: bold; font-size: 12px;" id="salir" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" onclick="salir();">
                            <span class="ui-button-text">Salir</span>
                           </button>
                        
                        </div>
                     
                     </fieldset>
            </div>
            <center>
        <div id="div_asobancaria" style="display:none; margin-top: 10px;">
            <div  style="background: white; border:1px solid #2A88C8; width: 650px; height:450px;  border-radius: 0px 0px 8px 8px;">
                <legend>Resumen Archivo Cargado</legend><br>
                    <!--<div class="col-md-12">-->
                        
                        <TABLE  CELLPADDING=5 CELLSPACING=5>
                                <TR> 
                                        <TD style="width: 180px;"><label class="labelaso"> Archivo por generar lote: </label></TD>
                                        <TD><select id="idCboArchivo" onchange="resumenArchivoAsobancaria(this.value);"></select></TD>
                                </TR>
                                <TR>
                                        <TD><label class="labelaso"> Id Archivo: </label></TD>
                                        <TD><label id="id_archivo"></label></TD> 
                                        <TD style="width: 50px;"><label class="labelaso"> Entidad: </label></TD>
                                        <TD style="width: 210px;"><label id="id_banco"></label></TD>
                                </TR>
                                
                                <TR>
                                        <TD><label class="labelaso"> Fecha recaudo: </label></TD>
                                        <TD><label id="id_fecha_recaudo"></label></TD> 
                                        <TD><label class="labelaso"> Banco Interno: </label></TD>
                                        <TD><label id="id_banco_interno"></label></TD>
                                        
                                </TR>
                                
                                <TR>
                                        <TD><label class="labelaso"> Fecha Carga: </label></TD>
                                        <TD><label id="id_fecha_carga"></label></TD>
                                        <TD><label class="labelaso"> Sucursal: </label></TD>
                                        <TD><label id="id_sucursal"></label></TD>
                                        
                                </TR>
                                
                                <TR>
                                        <TD><label class="labelaso"> Usuario Carga: </label></TD>
                                        <TD><label id="id_usuario_carga"></label></label></TD>
                                        <TD><label class="labelaso"> Total Comisión: </label></TD>
                                        <TD><label id="id_total_comision"></label></label></TD>
                                </TR>
                                
                                <TR>
                                        <TD><label class="labelaso"> Total Items: </label></TD>
                                        <TD><label id="id_total_items"></label></TD>
                                </TR>
                                
                                <TR>
                                        <TD><label class="labelaso"> Valor Archivo: </label></TD>
                                        <TD><label id="id_valor_archivo"></label></TD>
                                </TR>
                                
                                <TR>
                                        <TD><label class="labelaso"> Pagos Encontrados: </label></TD>
                                        <TD style="width: 200px; display: flex; align-items: center;"><label id="id_pagos_encontrados"></label><a id="glp" href="javascript:generarLotePagos();" style="margin-left: 5px;"><img src="/fintra/images/botones/iconos/dinero.png"/></a></TD>
                                        <TD><label class="labelaso"> Valor: </label></TD>
                                        <TD><label id="id_vlr_pagos_enc"></label></TD>
                                </TR>
                                
                                <TR>
                               
                                        <TD><label class="labelaso"> Pagos Inconsistentes: </label></TD>                                        
                                        <TD style="display: flex; align-items: center;"><label id="id_pagos_inconsistente"></label><a  id="vzl" href="javascript:visualizarInconsistentes();" style="margin-left: 5px;"><img src="/fintra/images/botones/iconos/busqueda.png"/></a></TD>
                                        <TD><label class="labelaso"> Valor: </label></TD>
                                        <TD><label id="id_vlr_pagos_inc"></label></TD>
                                </TR>
                                
                                <TR>
                                    <TD><a  id="logcl" href="javascript:visualizarLog();" style="margin-left: 5px;">Ver log de clasificacion</a></TD>
                                </TR>
                                
                                
                        </TABLE>
                       
                       
                        <!--<textarea id="id_descripcion_articulo" rows="15" style="width: 400px;"></textarea><br><br>-->
<!--                        <button style="font-weight: bold; font-size: 12px;" id="aceptar" onclick="generarLotePagos();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"> 
                    <span class="ui-button-text">Aceptar</span>
                   </button>  -->
                        
                    <!--</div>-->
                        
              
                    </div>
             </div>  </div>     
                </div>
        </center> 
             <div id="div_plantilla" style="display: none">
              <div style=" width: 100%; margin-top: 20px; ">           
                   
                 <center>
                    <div  style="margin-top: 12px; " >
                      <!--<label>Lote:</label>-->
                      <div  style="background: white; border:1px solid #2A88C8; width: 650px; height:80px;  border-radius: 0px 0px 8px 8px;">
                    <legend>Resumen Archivo Cargado</legend><br>
                      
                      <label class="labelaso">Lote:</label>
                      <select id="lote" name="lote" class="select" onchange=" CargarLogicaAplicacion();"></select>
                      <label class="labelaso">Motor De Clasificación:</label>
                      <select id="logica_aplicacion" name="logica_aplicacion" class="select" onchange="CargarPagosMasivos()"></select>
                      <button style=" margin-left: 50px; font-weight: bold; font-size: 12px;" id="aceptar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only"  onclick="confirmarCierre('Desea Realizar el cierre de mes de los alivios financieros?', '250', '150');">
                                <span class="ui-button-text">Cierre Alivios Financieros</span>
                            </button>  
                     </div>  
                
                      
                <div  style="margin-top: 20px; " >
               <table id="tabla_pagos_masivos" ></table>
               <div id="pager"></div>
               </div>
                 </center>
                 </div>
                     
           
            </div>
            </div>

            <div id="info"  class="ventana" >
                <p id="notific">EXITO AL GUARDAR</p>
            </div>
            <div id="dialogLoading"  class="ventana" >
                <p  id="msj2">texto </p> <br/>
                <center>
                    <img src="/fintra/images/cargandoCM.gif"/>
                </center>
            </div> 
            
            <div id="tablaRefeInconsistentes"  class="ventana" >
                <table id="tabla_RefeInconsistentes" class="tabla_Refe">
                    <tbody id="tabla_body_RefeInconsistentes">
                    <thead>
                        <tr>
                            <th class="th1">Id</th>
                            <th class="th1">Orden</th>
                            <th class="th1">Clasificación</th>
                            <th class="th1">Referencia</th>
                            <th class="th1">Negocio</th>
                            <th class="th1">Extracto</th>
                            <th class="th1" style="width: 100px;">Valor Recaudado</th>
                            <th class="th1">Banco</th>
                            <th class="th1">Branch code</th>
                            <th class="th1">Bank account no</th>
                            <th class="th1" style="width: 800px;">Log</th>
                            <th class="th1" style="width: 100px;">Editar</th>
                        </tr>
                    </thead>
                    </tbody>
                </table>
            </div>
            <div id="tablaLogRefePagos"  class="ventana" >
                <table id="tablaLogRefePagos" class="tabla_Refe">
                    <tbody id="tabla_body_tablaLogRefePagos">
                    <thead>
                        <tr>
                            <th class="th1">Id</th>
                            <th class="th1">Orden</th>
                            <th class="th1">Clasificación</th>
                            <th class="th1">Referencia</th>
                            <th class="th1">Negocio</th>
                            <th class="th1">Extracto</th>
                            <th class="th1" style="width: 100px;">Valor Recaudado</th>
                            <th class="th1">Banco</th>
                            <th class="th1">Branch code</th>
                            <th class="th1">Bank account no</th>
                            <th class="th1" style="width: 1000px;">Log</th>
                        </tr>
                    </thead>
                    </tbody>
                </table>
            </div>
        <script>
        
        CargarComboLotes();
        function submit1() {
                let archivo = document.getElementById("archivo");
                /*let periodo = document.getElementById("periodo");
                let periodos = document.getElementById("periodos-checkbox");*/

                if (archivo.files.length === 0) {
                    mensajesDelSistema("Falta informaciòn por digitar")
                } else {
                    loading("Esta operación puede tardar varios minutos, por favor espere.", 450);

                    let xhr;
                    if (window.XMLHttpRequest) {
                        xhr = new XMLHttpRequest();
                    } else {
                        xhr = new ActiveXObject();
                    }

                    let fd = new FormData();
                    fd.append("archivo", archivo.files[0]);

                    xhr.onreadystatechange = function () {
                        if (xhr.status === 200) {
                            if (xhr.readyState === 4) {
                                $("#dialogLoading").dialog("close");
                                  CargarLogicaAplicacion();
                                  CargarComboLotes();
                                  CargarPagosMasivos();
                                let json = JSON.parse(xhr.responseText);
                                if (json.error) {
                                    mensajesDelSistema(json.error, "300", "170", true);
                                } else {
                                    mensajesDelSistema(json.respuesta, "300", "170", true);
                                }
                            }
                        }
                    }
                    xhr.open("POST", "/fintra/controller?estado=AplicacionPagos&accion=Masivos&opcion=1" ,true);
                    xhr.send(fd);
                }
            }

            function loading(msj, width) {
                $("#msj2").html(msj);
                $("#dialogLoading").dialog({
                    width: width,
                    height: 130,
                    show: "scale",
                    hide: "scale",
                    resizable: false,
                    position: "center",
                    modal: true,
                    closeOnEscape: true
                });

                $("#dialogLoading").siblings('div.ui-dialog-titlebar').remove();
            }

            function salir() {
                window.close();
            }

            function mensajesDelSistema(msj, width, height, swHideDialog) {
                if (swHideDialog) {
                    $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
                } else {
                    $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
                }
                $("#info").dialog({
                    width: width,
                    height: height,
                    show: "scale",
                    hide: "scale",
                    resizable: false,
                    position: "center",
                    modal: true,
                    closable: true,
                    closeOnEscape: false,
                    buttons: {
                        "Aceptar": function () {
                            $(this).dialog("destroy");
                        }
                    }
                });
                $("#info").siblings('div.ui-dialog-titlebar').remove();
            }
            
            
    function CargarPagosMasivos() {
    
    var grid_tabla = $("#tabla_pagos_masivos"); 
    if ($("#gview_tabla_pagos_masivos").length) {
        reloadGridTabla(grid_tabla, 2);
    } else {
        
        grid_tabla.jqGrid({
            
           caption: "PAGOS MASIVOS",
            url: "./controller?estado=AplicacionPagos&accion=Masivos",
            mtype: "POST",
            datatype: "json",
            height: '500',
            rownumbers: true,
            width: '1780',
            colNames: ['ORDEN','NEGOCIO', 'NIT', 'ID UNIDAD','UNIDAD NEGOCIO', 'TIPO PAGO', 'FECHA RECAUDO', 'BANCO', 'SUCURSAL', 'VALOR APLICAR', 'USUARIO', 'PROCESADO', 'CREATION DATE', 'LOTE PAGO', 'EXTRACTO', 'MOTOR CLASIFICACION'],
            colModel: [                
                {name: 'orden',       index: 'orden',       width: 80, sortable: true, align: 'center',   hidden: false}, 
                {name: 'negasoc',        index: 'negasoc',        width: 90,sortable: true, align: 'center', hidden: false},
                {name: 'nitcli',index: 'nitcli',width: 100, sortable: true, align: 'center',   hidden: false},
                {name: 'unidad_negocio',index: 'unidad_negocio',width: 80, sortable: true, align: 'center',   hidden: false},
                {name: 'descripcion',       index: 'descripcion',       width: 120, sortable: true, align: 'center',   hidden: false},
                {name: 'tipo_pago',       index: 'tipo_pago',       width: 100, sortable: true, align: 'center',   hidden: false},
                {name: 'fecha_pago',       index: 'fecha_pago',       width: 110, sortable: true, align: 'center',   hidden: false},                
                {name: 'banco',       index: 'banco',       width: 110, sortable: true, align: 'center',   hidden: false},                
                {name: 'sucursal',       index: 'sucursal',       width: 120, sortable: true, align: 'center',   hidden: false},                
                {name: 'valor_aplicar_neto', index: 'valor_aplicar_neto', width: 90, sortable: true, align: 'center', hidden: false,
                formatter: 'currency', formatoptions: {decimalSeparator: ",", thousandsSeparator: ".",decimalPlaces: 0,  prefix: "$ "}},
                {name: 'usuario',       index: 'usuario',       width: 110, sortable: true, align: 'center',   hidden: false}, 
                {name: 'procesado',       index: 'procesado',       width: 110, sortable: true, align: 'center',   hidden: false}, 
                {name: 'creation_date',       index: 'creation_date',       width: 150, sortable: true, align: 'center',   hidden: false}, 
                {name: 'lote_pago',       index: 'lote_pago',       width: 100, sortable: true, align: 'center',   hidden: false}, 
                {name: 'extracto',       index: 'extracto',       width: 100, sortable: true, align: 'center',   hidden: false}, 
                {name: 'logica_aplicacion',       index: 'logica_aplicacion',       width: 120, sortable: true, align: 'center',   hidden: false} 
                                
            ],
            rowNum: 1000,
            rowTotal: 1000,
            loadonce: true,
            rownumWidth: 40,
            gridview: true,
            viewrecords: false,           
            hidegrid: false,
            shrinkToFit: false,
            footerrow: true,
            restoreAfterError: true,
            pager:'#pager',
            pgtext: null,
            pgbuttons: false,
            multiselect:false,           
            
            jsonReader: {
                root: "rows",
                repeatitems: false,
                id: "0"
                
            },
            ajaxGridOptions: {                
                data: {
                    opcion: 2,
                    lote: $("#lote").val(),
                    logica_aplicacion: $("#logica_aplicacion").val()
                }                                
            },
            gridComplete: function() { 
                
                var colSumTotal = jQuery("#tabla_pagos_masivos").jqGrid('getCol', 'valor_aplicar_neto', false, 'sum'); 
                jQuery("#tabla_pagos_masivos").jqGrid('footerData', 'set', {valor_aplicar_neto: colSumTotal});
                
                //CargarComboLotes();
            }, 
            loadError: function (xhr, status, error) {
                mensajesDelSistema(error, 250, 150);
                
            }
        }).navGrid('#pager', {add: false, edit: false, del: false, search: false, refresh: false});
        $("#tabla_pagos_masivos").navButtonAdd('#pager', {
            caption: "Aplicar Pagos",
            title: "Aplicar Pagos",
            onClickButton: function () {
                 if ($("#lote").val()==null){
                     mensajesDelSistema("No hay lotes para aplicar pagos", '250', '150');
                    
                }else{
                loading("Procesando Pagos, Por Favor Espere.", 450);
                AplicarPagos();
            }
            }
        });
        $("#tabla_pagos_masivos").navButtonAdd('#pager', {
            caption: "Eliminar lote",
            title: "Eliminar lote",
            onClickButton: function () {
                
                if ($("#lote").val()==null){
                     mensajesDelSistema("No hay lotes para eliminar", '250', '150');
                    
                }else{
                confirmar("Desea Eliminar este lote: <b>"+ $("#lote").val()+"</b>", '250', '150');
            
        }
            }
        });
//        $("#tabla_pagos_masivos").navButtonAdd('#pager', {
//            caption: "Cierre Mes Alivios",
//            title: "Cierre Mes Alivios",
//            onClickButton: function () {
//                
//                if ($("#lote").val()==null){
//                     mensajesDelSistema("No hay lotes para Actualizar", '250', '150');
//                    
//                }else{
//                confirmarCierre("Desea Realizar el cierre de mes de los alivios financieros?", '250', '150');
//            
//        }
//            }
//        });  
        $("#tabla_pagos_masivos").navButtonAdd('#pager', {
            caption: "Reclasificar Lote",
            title: "Reclasificar Lote",
            onClickButton: function () {
               if ($("#lote").val()==null){
                     mensajesDelSistema("No hay lotes para reclasificar", '250', '150');
                    
                }else{
                loading("Reclasificando Lotes, Por Favor Espere.", 450);
                ReclasificarLotes();
            }  
            }
            
        });   
    }

    }
    
function reloadGridTabla(grid_tabla, op) {
    grid_tabla.setGridParam({
        datatype: 'json',
        url: "./controller?estado=AplicacionPagos&accion=Masivos",
       
        ajaxGridOptions: {
            type: "POST",
            data: {
                opcion: op,
                lote:$("#lote").val(),
                logica_aplicacion: $("#logica_aplicacion").val()
            }
        }
    });
    grid_tabla.trigger("reloadGrid");    
     
}

function AplicarPagos() {    
            $.ajax({
                type: 'POST',
                url: "./controller?estado=AplicacionPagos&accion=Masivos",
                dataType: 'json',
                data: {
                    opcion: 3,
                    lote: $("#lote").val(),
                    logica_aplicacion: $("#logica_aplicacion").val()
                },
                success: function (json) {             
                    if (!isEmptyJSON(json)) {
                       
                        if (json.respuesta === "OK") {
                            $("#dialogLoading").dialog("close");
                            mensajesDelSistema("Pagos procesados correctamente", '250', '150', true);   
                            CargarLogicaAplicacion();
                            CargarComboLotes();
                            CargarPagosMasivos();
                        } 
                    } else {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("Lo sentimos ocurrió un error!!", '250', '150');

                    }
                },
                error: function (xhr) {
                    $("#dialogLoading").dialog('close');
                    mensajesDelSistema("Error al generar al procesar los pagos: " + "\n" +
                             xhr.responseText, '350', '250', true);
                }
            });
       
        
    }
    
    
    function CargarComboLotes() {
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AplicacionPagos&accion=Masivos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 4
        },
        success: function (json) {
            if (json.error) {
                return;
            }
           
                $('#lote').html('');                
                $('#lote').append("<option value='' >Seleccione</option>");
                for (var datos in json) {
                    $('#lote').append('<option value=' + json[datos] + ' >' + json[datos] + '</option>');
                }
           

        }, error: function (xhr, thrownError) {
            alert("Error: " + xhr.status + "\n" + 
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function CargarLogicaAplicacion() {
    clearDataJgrip();
    $.ajax({
        type: 'POST',
        url: "./controller?estado=AplicacionPagos&accion=Masivos",
        dataType: 'json',
        async: false,
        data: {
            opcion: 6,
            lote:$("#lote").val()
        },
        success: function (json) {
            if (json.error) {
                return;
            }
           
                $('#logica_aplicacion').html('');
                $('#logica_aplicacion').append("<option value='' >Seleccione</option>");
                for (var datos in json) {
                    $('#logica_aplicacion').append('<option value=' + json[datos] + ' >' + json[datos] + '</option>');
                }
           

        }, error: function (xhr, thrownError) {
            alert("Error: " + xhr.status + "\n" + 
                    "Message: " + xhr.statusText + "\n" +
                    "Response: " + xhr.responseText + "\n" + thrownError);
        }
    });
}

function isEmptyJSON(obj) {
    for (var i in obj) {
        return false;
    }
    return true;
}
    
function confirmar(msj, width, height, swHideDialog) {
                if (swHideDialog) {
                    $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
                } else {
                    $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
                }
                $("#info").dialog({
                    width: width,
                    height: height,
                    show: "scale",
                    hide: "scale",
                    resizable: false,
                    position: "center",
                    modal: true,
                    closable: true,
                    closeOnEscape: false,
                    buttons: {
                        "Si": function () {
                           EliminarLote();
                        },
                        "No": function () {
                            $(this).dialog("destroy");
                        }
                    }
                });
                $("#info").siblings('div.ui-dialog-titlebar').remove();
}


function EliminarLote() {    
            $.ajax({
                type: 'POST',
                url: "./controller?estado=AplicacionPagos&accion=Masivos",
                dataType: 'json',
                data: {
                    opcion: 5,
                    lote: $("#lote").val()
                },
                success: function (json) {             
                    if (!isEmptyJSON(json)) {
                       
                        if (json.respuesta === "OK") {
                            
                             mensajesDelSistema("El lote No. <b>"+$("#lote").val()+"</b> se eliminó correctamente", '250', '150', true);                           
                            CargarLogicaAplicacion(); 
                            CargarComboLotes();            
                            CargarPagosMasivos();
                            $(this).dialog("destroy");
                        } else {
                            
                            $(this).dialog("destroy");
                             mensajesDelSistema("ERROR: "+json.respuesta, '250', '150', true); 
                        }

                    } else {
                        $(this).dialog("destroy");
                        mensajesDelSistema("Lo sentimos ocurrió un error!!", '250', '150');

                    }
                },
                error: function (xhr) {
                    $(this).dialog("destroy");
                    mensajesDelSistema("Error al generar al procesar los pagos: " + "\n" +
                             xhr.responseText, '350', '250', true);
                }
            });
       
        
    }
    
function confirmarCierre(msj, width, height, swHideDialog) {
                if (swHideDialog) {
                    $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
                } else {
                    $("#notific").html("<span class='ui-icon ui-icon-alert' style='float: left; margin: 0 7px 20px 0;'></span> " + msj);
                }
                $("#info").dialog({
                    width: width,
                    height: height,
                    show: "scale",
                    hide: "scale",
                    resizable: false,
                    position: "center",
                    modal: true,
                    closable: true,
                    closeOnEscape: false,
                    buttons: {
                        "Si": function () {
                           cierrePlanAlDia();
                        },
                        "No": function () {
                            $(this).dialog("destroy");
                        }
                    }
                });
                $("#info").siblings('div.ui-dialog-titlebar').remove();
}

function cierrePlanAlDia() {    
            $.ajax({
                type: 'POST',
                url: "./controller?estado=AplicacionPagos&accion=Masivos",
                dataType: 'json',
                data: {
                    opcion: 7,
                    lote: $("#lote").val()
                },
                success: function (json) {             
                    if (!isEmptyJSON(json)) {
                       
                        if (json.respuesta === "OK") {
                            
                             mensajesDelSistema("Se realizó el proceso correctamente", '250', '150', true);                           
                            CargarLogicaAplicacion(); 
                            CargarComboLotes();            
                            CargarPagosMasivos();
                            $(this).dialog("destroy");
                        } else {
                            
                            $(this).dialog("destroy");
                             mensajesDelSistema("ERROR: "+json.respuesta, '250', '150', true); 
                        }

                    } else {
                        $(this).dialog("destroy");
                        mensajesDelSistema("Lo sentimos ocurrió un error!!", '250', '150');

                    }
                },
                error: function (xhr) {
                    $(this).dialog("destroy");
                    mensajesDelSistema("Error al generar al procesar los pagos: " + "\n" +
                             xhr.responseText, '350', '250', true);
                }
            });
       
        
    }
     
    function clearDataJgrip(){
        $('#tabla_pagos_masivos').jqGrid('clearGridData')
        $('#tabla_pagos_masivos').jqGrid('setGridParam', {data: {}, page: 1})
        $('#tabla_pagos_masivos').trigger('reloadGrid');        
    }
    
    function ReclasificarLotes() {    
            $.ajax({
                type: 'POST',
                url: "./controller?estado=AplicacionPagos&accion=Masivos",
                dataType: 'json',
                data: {
                    opcion: 14,
                    lote: $("#lote").val()
                },
                success: function (json) {             
                    if (!isEmptyJSON(json)) {
                       
                        if (json.respuesta === "OK") {
                            $("#dialogLoading").dialog("close");
                            mensajesDelSistema("Lotes reclasificados correctamente", '250', '150', true);   
                            CargarLogicaAplicacion();
                            CargarComboLotes();
                            CargarPagosMasivos();
                        } 
                    } else {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("Lo sentimos ocurrió un error!!", '250', '150');

                    }
                },
                error: function (xhr) {
                    $("#dialogLoading").dialog('close');
                    mensajesDelSistema("Error al reclasificar lotes: " + "\n" +
                             xhr.responseText, '350', '250', true);
                }
            });
       
        
    }

    function cargarArchivoRecaudoAsobancaria() {
        
//      var extension = $('#archivo').val().split('.').pop().toLowerCase();
//      var filetype = document.getElementById('archivo').files[0].type;
        var fd = new FormData(document.getElementById('formulario'));
        var archivo = document.getElementById('archivo').value;
      
        if (archivo === "") {
            mensajesDelSistema("No ha seleccionado ning&uacute;n archivo. Por favor, seleccione uno!!", '250', '150');
            return;
        }
        var fsize=$('#archivo')[0].files[0].size;
        if (fsize > 4096 * 1024){
            $("#archivo").val("");
            mensajesDelSistema("Archivo no cargado. El tama&ntilde;o del archivo no puede ser superior a los 4MB", '250', '175');
            return;
        }
        loading("Espere un momento por favor...", "270", "140");
        setTimeout(function(){
        $.ajax({
            async: false,
            url: "./controller?estado=Archivo&accion=Asobancaria&opcion=1",
            type: 'POST',
            dataType: 'json',
            data: fd,
            enctype: 'multipart/form-data',
            processData: false,
            contentType: false,
            success: function(json) {
                $("#archivo").val("");
                if (!isEmptyJSON(json)) {

                    if (json.error) {
                        
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema(json.error, '250', '175');
                        return;
                    }

                    if (json.respuesta === "OK") { 
                       
                        resumenArchivoAsobancaria(json.idRecaudo,function(error){
                            
                            if (!error){
                                 $("#dialogLoading").dialog('close');
                                 mensajesDelSistema("Archivo cargado exitosamente, Id recaudo "+json.idRecaudo, '250', '150', true);
                            }
                            
                        });     
                    }

                } else {
                    $("#dialogLoading").dialog('close');
                    mensajesDelSistema("Lo sentimos no se pudo efectuar la operaci&oacute;n!!", '250', '150');
                }

            }, error: function(xhr, ajaxOptions, thrownError) {
                $("#dialogLoading").dialog('close');
                alert("Error: " + xhr.status + "\n" +
                        "Message: " + xhr.statusText + "\n" +
                        "Response: " + xhr.responseText + "\n" + thrownError);
            }
        });
        },500);
}

function cargarArchivo(){
    
    let tipo_archivo=$("#tipo_archivo").val();
    
        if(tipo_archivo==="ASOBANCARIA"){        
            cargarArchivoRecaudoAsobancaria();        
        }else if (tipo_archivo==="PLANTILLA") {
            submit1();
        }else{
         mensajesDelSistema("Por favor seleccione tipo archivo", '250', '150'); 
        }
}
 
 function mostrar_div(tipo){
        $("#id_archivo").text("");
        $("#id_fecha_recaudo").text("");
        $("#id_fecha_carga").text("");
        $("#id_usuario_carga").text("");
        $("#id_total_items").text("");
        $("#id_valor_archivo").text("");
        $("#id_pagos_encontrados").text("");
        $("#id_vlr_pagos_enc").text("");
        $("#id_total_comision").text("");
        $("#id_vlr_pagos_inc").text("");
        $("#id_pagos_inconsistente").text("");
        $("#id_banco").text("");
        $("#id_banco_interno").text("");
        $("#id_sucursal").text("");
     if (tipo==="ASOBANCARIA"){
         archivoGenerarLote();
        $("#div_plantilla").hide();
        $("#glp").hide();
        $("#vzl").hide();
        $("#div_asobancaria").show();
        
      }else if (tipo==="PLANTILLA") {
       $("#div_plantilla").show();   
       $("#div_asobancaria").hide();       
     }
  }
  
  function resumenArchivoAsobancaria(idArchivo,callback){
           $.ajax({
                type: 'POST',
                url: "./controller?estado=AplicacionPagos&accion=Masivos",
                dataType: 'json',
                data: {
                    opcion: 8,
                    idArchivo: idArchivo
                },
                success: function (json) {             
                    if (!isEmptyJSON(json)) {
                       
                        if (json.respuesta === "OK") {
                            
                           
                            $("#id_archivo").text(json.id_archivo);
                            $("#id_fecha_recaudo").text(json.fecha_recaudo);
                            $("#id_fecha_carga").text(json.fecha_carga);
                            $("#id_usuario_carga").text(json.usuario);
                            $("#id_total_items").text(json.total_pagos);
                            $("#id_valor_archivo").text(numberConComas(json.valor_recaudo));
                            $("#id_pagos_encontrados").text(json.pagosEncontrados);
                            $("#id_vlr_pagos_enc").text(numberConComas(json.valorpagosEncontrados));
                            $("#id_vlr_pagos_inc").text(numberConComas(json.valorPagosInconsistentes));
                            $("#id_pagos_inconsistente").text(json.PagosInconsistentes);
                            $("#id_banco").text(json.banco);
                            $("#id_banco_interno").text(json.branch_code);
                            $("#id_sucursal").text(json.bank_account_no);
                            $("#id_total_comision").text(numberConComas(json.total_comision));
                           if (json.flag=='f'){
                                $("#glp").hide();                                
                            }else{
                                 $("#glp").show();  
                            } 
                            $("#vzl").show();
                            archivoGenerarLote();
                            callback(false);
                           
                       
                        } else {
                            
                            $(this).dialog("destroy");
                             mensajesDelSistema("ERROR: "+json.respuesta, '250', '150', true); 
                        }

                    } else {
                        $(this).dialog("destroy");
                        mensajesDelSistema("Lo sentimos ocurrió un error!!", '250', '150');

                    }
                },
                error: function (xhr) {
                    $(this).dialog("destroy");
                    mensajesDelSistema("Error al generar al procesar los pagos: " + "\n" +
                             xhr.responseText, '350', '250', true);
                }
            });
  }
  
  function numberConComas(x) {
    return "$ "+x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}

function archivoGenerarLote(){ 
        
//        $("#div_plantilla").show();   
//        $("#div_asobjson.rowsancaria").hide();

        $.ajax({
         type: 'POST',
         url: "./controller?estado=AplicacionPagos&accion=Masivos",
         dataType: 'json',
         async: false,
         data: {
             opcion: 9
         },
         success: function (json) {
             if (json.error) {
                 return;
             }

                 $('#idCboArchivo').html('');
                 $('#idCboArchivo').append('<option value="" >...</option>');
                 for (var datos in json) {
                     $('#idCboArchivo').append('<option value="' + datos + '" >' + json[datos] + '</option>');
                 }


         }, error: function (xhr, thrownError) {
             alert("Error: " + xhr.status + "\n" + 
                     "Message: " + xhr.statusText + "\n" +
                     "Response: " + xhr.responseText + "\n" + thrownError);
         }
     });

  }
  var inconsistentes=[];
  function visualizarInconsistentes(){

      $("#tablaRefeInconsistentes").dialog({
                    width: 1300,
                    height: 800,
                    show: "scale",
                    hide: "scale",
                    resizable: false,
                    position: "center",
                    modal: true,
                    closable: true,
                    closeOnEscape: false,                    
                    buttons: {                        
                        "Salir": function () {
                            $(this).dialog("destroy");
                        }
                    }
        });
                
        $(".referencias").remove();
       
        $.ajax({
        caption: "Inconsistentes",
            url: "./controller?estado=AplicacionPagos&accion=Masivos&opcion=10&IdArchivo=" + $("#id_archivo").text(),
            mtype: "POST",
            datatype: "json",
            success: function(json) {
            inconsistentes=json.rows;
            inconsistentes.forEach(referencias => {
              var fila = '<tr id="' + referencias.id + '" class="referencias">\n\
                <td id="id" class="td1">' + referencias.id  + '</td>\n\
                <td id="orden" class="td1">' + referencias.orden  + '</td>\n\
                <td id="clasificacion" class="td1">' + referencias.clasificacion  + '</td>\n\
                <td id="referencia" class="td1">' + referencias.referencia  + '</td>\n\
                <td id="negocio" class="td1"><span id="span'+referencias.id+'">' + referencias.negocio  + '</span><input id="negocio'+referencias.id+'" style="display:none;" value=' + referencias.negocio  + '></td>\n\
                <td id="extracto" class="td1">' + referencias.extracto  + '</td>\n\
                <td id="valor_recaudo" class="td1">' + numberConComas(referencias.valor_recaudo)  + '</td>\n\
                <td id="banco" class="td1">' + referencias.banco  + '</td>\n\
                <td id="branch_code" class="td1">' + referencias.branch_code  + '</td>\n\
                <td id="bank_account_no" class="td1">' + referencias.bank_account_no  + '</td>\n\
                <td id="condicional" class="td1">' + referencias.condicional  + '</td>\n\
                <td id="id_accion" class="boton"><div class="btn-group"><button id="btn_editar' + referencias.id + '"  value="' + referencias.id + '"onclick="editarNegocios(this.value);" style="display: inline-block;" class="editar">Editar</button><button id="btn_guardar'+ referencias.id  +'" onclick="guardarNegocio('+referencias.id+');" value='+referencias.id+' style="display:none;" class="guardar">Guardar</button><br><br><button id="btn_cancelar'+ referencias.id  +'" onclick="CancelarUpdate('+referencias.id+');" value='+referencias.id+' style="display:none;" class="cancelar">Cancelar</button></div></td>\n\
               /</tr>'; 
               
                $('#tabla_RefeInconsistentes tbody').append(fila);
            });
                        
        },
        error: function(xhr) {
              alert(xhr, '350', '150',  true); 
        }
    }); 
  }
  
  function generarLotePagos(){
      $("#notific").html("<span class='ui-icon ui-icon-info' style='float: left; margin: 0 7px 20px 0;'></span> " + "¿Desea generar lote de pago?");
      $("#info").dialog({
            width: 250,
            height: 150,
            show: "scale",
            hide: "scale",
            resizable: false,
            position: "center",
            modal: true,
            closable: true,
            closeOnEscape: false,
            buttons: {
                "Si": function () {
                    $(this).dialog("destroy");
                   loading("Espere un momento por favor...", "270", "140");
       setTimeout(function(){
       $.ajax({
                type: 'POST',
                url: "./controller?estado=AplicacionPagos&accion=Masivos",
                dataType: 'json',
                data: {
                    opcion: 11,
                    idArchivo: $("#id_archivo").text()
                },
                success: function (json) {             
                    if (!isEmptyJSON(json)) {
                       
                        if (json.respuesta === "OK") {
                             $("#dialogLoading").dialog('close');
                            mensajesDelSistema("Lote Generado Correctamente!", '250', '150');
                            CargarComboLotes();
                            select_tipo("PLANTILLA");
                             $("#div_plantilla").show();   
                            $("#div_asobancaria").hide();
                        } else {
                            
                             $("#dialogLoading").dialog('close');
                             mensajesDelSistema("ERROR: "+json.respuesta, '250', '150', true); 
                        }

                    } else {
                        $(this).dialog("destroy");
                        mensajesDelSistema("Lo sentimos ocurrió un error!!", '250', '150');

                    }
                },
                error: function (xhr) {
                   $("#dialogLoading").dialog('close');
                    mensajesDelSistema("Error al generar al procesar los pagos: " + "\n" +
                             xhr.responseText, '350', '250', true);
                }
            });},500);
      
                },
                "No": function () {
                    $(this).dialog("destroy");
                }
            }
        });
      
      
  }
  
  function editarNegocios(v){
      $("#negocio"+v+"").show();
      $("#span"+v+"").hide();
      $("#btn_guardar"+v+"").show();
      $("#btn_cancelar"+v+"").show();
      $("#btn_editar"+v+"").hide();
      
  }
  
  function CancelarUpdate(v){
      $("#negocio"+v+"").hide();
      $("#span"+v+"").show();
      $("#btn_guardar"+v+"").hide();
      $("#btn_cancelar"+v+"").hide();
      $("#btn_editar"+v+"").show();
      
  }

function select_tipo(tipo){
    $("#tipo_archivo option:contains('"+tipo+"')").each(function(){
         if ($(this).text() == tipo) {
             $(this).attr('selected', 'selected');
         }
     });
          
   }    
   
   function guardarNegocio(Idreferencias){
      loading("Espere un momento por favor...", "270", "140");
       var referencia = inconsistentes.filter(function(i){
           return i.id == Idreferencias;
       })[0]       
         $.ajax({
                type: 'POST',
                url: "./controller?estado=AplicacionPagos&accion=Masivos",
                dataType: 'json',
                data: {
                    opcion: 12,
                    idArchivo: $("#id_archivo").text(),
                    idNegocio: $("#negocio"+referencia.id).val(),
                    idOrden: referencia.orden,
                    idRows: referencia.id
                },
                success: function (json) {             
                    if (!isEmptyJSON(json)) {
                        $("#dialogLoading").dialog('close');
                        if (json.respuesta === "OK") {
                            mensajesDelSistema("Se actualizaron los negocios correctamente", '250', '150');
                            visualizarInconsistentes();
                            setTimeout(function(){
                            resumenArchivoAsobancaria($("#id_archivo").text());
                             },500);
                        }else {
                            
                            
                             mensajesDelSistema("ERROR: "+json.respuesta, '300', '250', true); 
                        }

                    }else {
                        $("#dialogLoading").dialog('close');
                        mensajesDelSistema("Lo sentimos ocurrió un error!!", '250', '150');

                    }
                },
                error: function (xhr) {
                    $("#dialogLoading").dialog('close');
                    mensajesDelSistema("Error al generar al procesar los pagos: " + "\n" +
                             xhr.responseText, '350', '250', true);
                }
            });
       
     }
     
     var LogReferencia=[];
  function visualizarLog(){

      $("#tablaLogRefePagos").dialog({
                    width: 1300,
                    height: 800,
                    show: "scale",
                    hide: "scale",
                    resizable: false,
                    position: "center",
                    modal: true,
                    closable: true,
                    closeOnEscape: false,                    
                    buttons: {                        
                        "Salir": function () {
                            $(this).dialog("destroy");
                        }
                    }
        });
                
        $(".Log").remove();
       
        $.ajax({
        caption: "Log",
            url: "./controller?estado=AplicacionPagos&accion=Masivos&opcion=13&IdArchivo=" + $("#id_archivo").text(),
            mtype: "POST",
            datatype: "json",
            success: function(json) {
            LogReferencia=json.rows;
            LogReferencia.forEach(Log => {
              var fila = '<tr id="' + Log.id + '" class="Log">\n\
                <td id="idLog" class="td1">' + Log.id  + '</td>\n\
                <td id="ordenLog" class="td1">' + Log.orden  + '</td>\n\
                <td id="clasificacionLog" class="td1">' + Log.clasificacion  + '</td>\n\
                <td id="referenciaLog" class="td1">' + Log.referencia  + '</td>\n\
                <td id="negocioLog" class="td1">' + Log.negocio  + '</td>\n\
                <td id="extractoLog" class="td1">' + Log.extracto  + '</td>\n\
                <td id="valor_recaudoLog" class="td1">' + numberConComas(Log.valor_recaudo)  + '</td>\n\
                <td id="bancoLog" class="td1">' + Log.banco  + '</td>\n\
                <td id="branch_codeLog" class="td1">' + Log.branch_code  + '</td>\n\
                <td id="bank_account_noLog" class="td1">' + Log.bank_account_no  + '</td>\n\
                <td id="condicionalLog" class="td1">' + Log.condicional  + '</td>\n\
               /</tr>'; 
               
                $('#tablaLogRefePagos tbody').append(fila);
            });
                        
        },
        error: function(xhr) {
              alert(xhr, '350', '150',  true); 
        }
    }); 
  }
  

        </script>
    </body>
</html>
