<%@ page session="true"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%
String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>

<html>
<head>
<title>Publicación</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
  <script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
  <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>
 <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">

</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="4" ></td>
  </tr>
  <tr>
    <td width="50%" height="20">&nbsp;</td>
    <td width="3%"><img src="<%=BASEURL%>/images/Botonera.gif"></td>
    <td width="44%" class="subtitulo" align="right">PUBLICACIONES</td>
    <td width="3%" class="subtitulo">&nbsp;</td>
  </tr>
</table>

<p>
  <%
  
  Usuario usuario = (Usuario) session.getAttribute("Usuario");
  String Mensaje = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";%>
</p>
<table border="2" width="100%">
<tr>
<td>
<table width="100%" align="center"  >
  <tr>
    <td width="50%" height="24"  class="subtitulo1"><p align="left">Lista de Publicaciones </p></td>
    <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
  </tr>
</table>
</td>
</tr>
</table>
<table width="100%"  border="0">
  <tr>
    <td align="left"><img src="<%=BASEURL%>/images/botones/regresar.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="forma.submit();" onMouseOut="botonOut(this);" style="cursor:hand"></td>
  </tr>
</table>

  <form name="forma" method="post" action="<%=CONTROLLER%>?estado=Publicaciones&accion=Search&inicio=ok">
        <%
	  Vector a = model.publicacionService.getVecPublicaciones();
      if(a.size()>0){
	  for( int i = 0; i<a.size(); i++ ){

	  	Vector b = (Vector)a.get(i);

	  	 Publicacion p= (Publicacion)b.get(0);

	  %>

	  
	  <br>
	  
	  <table width="99%"  border="2" align="center">
    <tr>
      <td>	  
	<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="barratitulo">
	  <tr>
        <td width="50%" class="subtitulo1" colspan='3'><%=p.getTitulo()%></td>
        <td width="50%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif"></td>
      </tr>
    </table>
    <table width="100%" border="1" align="center" bordercolor="#999999">       
	  <tr class="tblTitulo">
        <td width="110" nowrap>Fecha Ingreso</td>
        <td width="625"><div align="center"></div>          <div align="center"></div>          <div align="center"><strong>Descripcion</strong><strong></strong></div></td>
		</tr>
      <%
	  
	    for( int j = 0; j<b.size(); j++ ){
	  	Publicacion pp= (Publicacion)b.get(j); 
	  %>
      <tr class="<%=i%2==0?"filagris":"filaazul"%>"  >
        <td width="110" class="bordereporte" > <%=pp.getFecha().substring (0,16)%></td>
        <td class="bordereporte">     <%=pp.getDescripcion()%> </td>
		</tr>
      <%    }
	
		%>
    </table>
	</td>
      </tr>
    </table>
	  <%   }
	}else{%>
	
	
	<table border="2" align="center" >
				<tr>
				  <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
					  <tr>
						<td width="229" align="center" class="mensajes">No hay publicaciones disponibles para el usuario, puede ver historial de publicaciones en el link historial.</td>
						<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
						<td width="58">&nbsp;</td>
					  </tr>
				  </table></td>
				</tr>
		    </table>
	<%}%>
		<br>
<center>
<a  class="Simulacion_Hiper" href="#" onclick="window.open('<%=CONTROLLER%>?estado=Publicacion&accion=Ejecutar&sw=False&pagina=historialPublicacion.jsp&carpeta=jsp/publicacion&marco=no','','status=no,scrollbars=no,width=720,height=400,resizable=yes')">historial</a>
</center>
</form>
<%=datos[1]%> 
</body>
</html>
