<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE></TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/publicaciones/lista_publicaciones/"; %> 

  <table width="95%"  border="2" align="center">
    <tr>
      <td height="" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL PUBLICACIONES WEB </div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa lista de publicaciones </td>
          </tr>
          
          <tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">Aparece la pagina principal. Se puede ver una alerta de publicaiones nuevas o sin ver. </span>              <div align="center"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><span class="ayudaHtmlTexto">Para ver las publicaiones solo basta con un click en el icono del alert.</span></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEIMG%>Dibujo1.JPG"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto">Al ingresar en la pantalla de lista de publicaciones, vemos que cada publicaiones tiene un titulo que corresponde a un modulo en especifico.</td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEIMG%>Dibujo2.JPG" ></div></td>
          </tr>
<tr>
          <td  class="ayudaHtmlTexto"><div align="center"></div>
            Tambien se visualiza un fecha de ingreso, que corresponde a la fecha de creacion de la publicacion.</td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEIMG%>Dibujo3.JPG" ></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto">Se encuentra una breve descripcion del funcionamiento, novedades o modificaciones de la publicacion.</td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEIMG%>Dibujo4.JPG" >    </div>
    <div align="center"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto">En la pantalla un boton que habilita la posibiladad de regresar a la pantalla principal.</td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEIMG%>Dibujo5.JPG" ></div></td>
</tr>
      </table></td>
    </tr>
  </table>
</BODY>
</HTML>
