<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA FUNCIONAL - Eliminar Publicacion
	 - Date            :      22/04/2006  
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>AYUDA FUNCIONAL - Eliminar Publicacion</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/publicacion/eliminar/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE PUBLICACIONES WEB</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa para eliminar una publicaci&oacute;n. </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"></div></td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">En la siguiente pantalla aparecen las publicaciones que ha ingresado el usuario en sessi&oacute;n.</p>            </td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=141 src="<%=BASEIMG%>image_001.JPG" width=879 border=0 v:shapes="_x0000_i1143"></div></td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>S&iacute; el usuario en sessi&oacute;n no ha ingresado ninguna publicaci&oacute;n, no podr&aacute; eliminar ninguna. Por lo tanto le saldr&aacute; el siguiente mensaje. </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=89 src="<%=BASEIMG%>image_error001.JPG" width=380 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Al escoger del campo de selecci&oacute;n una publicaci&oacute;n que desees eliminar, el sistema te llevar&aacute; a la siguiente pantalla..</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center"><IMG height=175 src="<%=BASEIMG%>image_002.JPG" width=753 border=0 v:shapes="_x0000_i1052"></div></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Al comprobar que esa es la publicaci&oacute;n que se desea eliminar del sistema, se procede a presionar en el bot&oacute;n ELIMINAR, y en la pantalla aparecer&aacute; el mensaje siguiente.</p></td>
</tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG height=97 src="<%=BASEIMG%>image_003.JPG" width=379 border=0 v:shapes="_x0000_i1052"></div></td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>