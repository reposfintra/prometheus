<!--
- Autor : LREALES
- Date  : 21 Abril 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Eliminar Publicacion</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../fintravalores/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script>
<script>
	var controlador ="<%=CONTROLLER%>";
	
	function EliminarPublicacion( opc, fec ){
	
		var url = controlador+"?estado=Publicacion&accion=Delete&num_dir="+opc+"&fecha_creacion="+fec;
		document.location.href = url;
			
	}
</script>
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/toptsp.jsp?encabezado=PUBLICACION"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px;">
<% 
	String op = ""; 
	String m = request.getParameter("m");
%>
<FORM name='form1' id='form1' method='post'>
<% 
	if( m != null && m.equals("no") ){ 
%>
  <table width="80%" border="2" align="center">  
    <td>
	  <table width="100%" class="tablaInferior">
        <tr align="center">
          <td colspan="4">
		    <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="barratitulo">
              <tr>
				<td width="40%" class="subtitulo1">Eliminar Publicaci&oacute;n</td>
				<td width="60%"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
			  </tr>
	        </table>
          </td>
<%
	Vector publicaciones = model.publicacionService.getVectorPublicaciones();
	PublicacionNoVencida pnv = ( PublicacionNoVencida ) publicaciones.elementAt( 0 );	
	int opcion = pnv.getOpcion();
	String nombre = pnv.getNombre_dir();
	String titulo = pnv.getTitulo();
	String tex = pnv.getTexto();
	String fecha_creacion = pnv.getFecha_creacion();
	String ultima_modificacion = pnv.getUltima_modificacion();
%>
        <tr>
          <td width="17%" align="left" class="fila">Fecha de creaci&oacute;n:</td>
          <td width="33%" align="left" class="letra"><%=fecha_creacion.substring(0,19)%>.</td>
          <td width="17%" align="left" class="fila">Ultima modificaci&oacute;n:</td>
          <td width="33%" align="left" class="letra"><%=ultima_modificacion.substring(0,19)%> .</td>
        </tr>
        <tr>
    <td align="left" class="fila">T&iacute;tulo:</td>
    <td align="left" class="letra"><%=titulo%> . </td>
    <td align="left" class="fila"> Direcci&oacute;n:</td>
    <td align="left" class="letra">N� <%=opcion%> - <%=nombre%> . </td>
        </tr>  
  <tr class="fila">
    <td colspan="4" align="left">Texto:</td>
  </tr>
  <tr>
    <td colspan="4" align="left" class="letra">
	  <div style=" overflow:auto ; WIDTH:100%; height:100; max-width:none ">
	       <%=tex%>
	  </div>
	  </td>
  </tr>
</table>
  </td>
  </tr>
</table>
<p align="center">
  <img src="<%=BASEURL%>/images/botones/regresar.gif" name="img_regresar" onClick = "window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/publicacion/eliminarPublicacion&pagina=EliminarPublicacion1.jsp&marco=no&opcion=30'; this.disabled=true;" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle" style="cursor:hand">
  
  <img src="<%=BASEURL%>/images/botones/eliminar.gif" name="img_eliminar" onClick="EliminarPublicacion('<%=opcion%>', '<%=fecha_creacion%>'); this.disabled=true;" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle" style="cursor:hand">
  
  <img src="<%=BASEURL%>/images/botones/salir.gif" name="img_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle" style="cursor:hand">
</p>
<%
}//cierro if ( m = no )
	if ( m != null && m.equals("si") ){
%>                        
<%
	out.print("<table width=379 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=350 align=center class=mensajes>La Publicacion Ha Sido Eliminada Del Sistema!</td><td width=100><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");%>
	<br>
	<p align="center">
	  <img src="<%=BASEURL%>/images/botones/regresar.gif" name="img_regresar" onClick = "window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/publicacion/eliminarPublicacion&pagina=EliminarPublicacion1.jsp&marco=no&opcion=30'; this.disabled=true;" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle" style="cursor:hand">
	  
	  <img src="<%=BASEURL%>/images/botones/salir.gif" name="img_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle" style="cursor:hand">
	</p>
<%
	}//cierro if ( m = si )
%>
</form>
</div>
<%=datos[1]%>
</body>
</html>