<!--  
	 - Author(s)       :      LREALES
	 - Description     :      AYUDA DESCRIPTIVA - Modificar Publicacion
	 - Date            :      21/04/2006  
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>AYUDA DESCRIPTIVA - Modificar Publicacion</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Publicaci&oacute;n</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Modificar Publicaci&oacute;n  - Pantalla Preliminar</td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2">INFORMACION</td>
        </tr>
        <tr>
          <td width="149" class="fila">Cuadro de selecci&oacute;n </td>
          <td width="525"  class="ayudaHtmlTexto">Campo de selecci&oacute;n donde escoges la publicaci&oacute;n que deseas modificar. </td>
        </tr>
        <tr>
          <td  class="fila">Texto</td>
          <td  class="ayudaHtmlTexto">Campo donde se modifica la descripci&oacute;n de la publicaci&oacute;n.</td>
        </tr>
        <tr>
          <td class="fila">Bot&oacute;n Regresar </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para regresar a la vista anterior ( Cuadro de selecci&oacute;n ). </td>
        </tr>
		<tr>
          <td class="fila">Bot&oacute;n Restablecer</td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para restablecer la p&aacute;gina con los valores iniciales. </td>
        </tr>
		<tr>
          <td width="149" class="fila">Bot&oacute;n Modificar </td>
          <td width="525"  class="ayudaHtmlTexto">Bot&oacute;n para modificar la publicaci&oacute;n. </td>
        </tr>
        <tr>
          <td width="149" class="fila">Bot&oacute;n Salir</td>
          <td width="525"  class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista actual y volver a la vista del men&uacute;.</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</body>
</html>
