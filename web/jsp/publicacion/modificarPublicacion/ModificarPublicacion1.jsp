<!--
- Autor : LREALES
- Date  : 18 Abril 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Modificar Publicacion</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../../fintravalores/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="file:///C|/Documents%20and%20Settings/EQUIPO10/Mis%20documentos/L!SsEtT/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/toptsp.jsp?encabezado=Publicacion"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
		  <% Vector publicaciones = model.publicacionService.getVectorPublicaciones();
	 String fecha_creacion = "";
	 int num_dir = 0;
	  if( publicaciones != null  && publicaciones.size() > 0 ){ %>
	  <table width="95%" border="2" align=center>
    <tr>
        <td>
            <table width="100%" align="center" class="tablaInferior"> 
                <tr>
                    <td colspan='2'>                                                
                        <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="barratitulo">
                            <tr>
                                <td width="30%" class="subtitulo1" colspan='3'>PUBLICACIONES A MODIFICAR</td>
                                <td width="70%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif" align="left"><%=datos[0]%></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
					<div class="scroll" style=" overflow:auto ; WIDTH: 100%; HEIGHT:100%; " >
                        <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
                            <tr class="tblTitulo">
                                <td nowrap width="35%"><div align="center">T&iacute;tulo</div></td>
                                <td nowrap width="35%"><div align="center">Texto</div></td>
                                <td nowrap width="15%"><div align="center">Fecha de Creaci&oacute;n</div></td>
                                <td nowrap width="15%"><div align="center">Ultima Modificaci&oacute;n</div></td>
                            </tr>
							  <%
								for( int i = 0; i < publicaciones.size(); i++ ){
									PublicacionNoVencida pnv = ( PublicacionNoVencida ) publicaciones.elementAt( i );
							  %>
							<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" title="Modificar Publicacion..." onClick="window.location='<%=CONTROLLER%>?estado=Publicacion&accion=Select&fecha_creacion=<%=pnv.getFecha_creacion()%>&num_dir=<%=pnv.getOpcion()%>'; this.disabled=true;">
							  <td nowrap  align="left" abbr="" class="bordereporte"><%=pnv.getTitulo()%></td>
							  <td align="left" abbr="" nowrap class="bordereporte"><%=pnv.getTexto()%>...</td>
							  <td align="center" abbr="" nowrap class="bordereporte"><%=pnv.getFecha_creacion().substring(0,19)%></td>
							  <td align="center" abbr="" nowrap class="bordereporte"><%=pnv.getUltima_modificacion().substring(0,19)%></td>
							</tr>
						  
								<%}%>
                        </table>
					  </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
 </table>
 <br>
	<%}
	 else { %>                    
	  <%out.print("<table width=379 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=350 align=center class=mensajes>No Tiene Publicaciones a Modificar!!</td><td width=100><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");%>
	  <br>
	<%}%>
<table width='95%' align=center>
  <tr class="titulo">
    <td align=right colspan='2'>
        <div align="center">		
		<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">
        </div></td>
  </tr>
</table>
</div>
<%=datos[1]%>
</body>
</html>