<!--
- Autor : LREALES
- Date  : 18 Abril 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Modificar Publicacion</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../fintravalores/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script>
<script>
	var controlador ="<%=CONTROLLER%>";
	
	function ModificarPublicacion( tex, opc, fec ){
	
		if( tex != "" ){
		
			var url = controlador+"?estado=Publicacion&accion=Update&texto="+tex+"&num_dir="+opc+"&fecha_creacion="+fec;
			document.location.href = url;
			
		} else{
		
			alert('Por Favor Llene El Campo De Texto!!');
			
		}
	}
	
	function RestablecerPublicacion( opc, fec ){
		
		var url = controlador+"?estado=Publicacion&accion=Select&fecha_creacion="+fec+"&num_dir="+opc; 
		document.location.href = url;
			
	}	
</script>
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/toptsp.jsp?encabezado=PUBLICACION"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px;">
<% 
	String op = ""; 
	String m = request.getParameter("m");
%>
<FORM name='form1' id='form1' method='post'>
<% 
	if( m != null && m.equals("no") ){ 
%>
  <table width="80%" border="2" align="center">  
    <td>
	  <table width="100%" class="tablaInferior">
        <tr align="center">
          <td colspan="4">
		    <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="barratitulo">
              <tr>
				<td width="40%" class="subtitulo1">Modificar Publicaci&oacute;n</td>
				<td width="60%"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
			  </tr>
	        </table>
          </td>
	<%
	Vector publicaciones = model.publicacionService.getVectorPublicaciones();
	PublicacionNoVencida pnv = ( PublicacionNoVencida ) publicaciones.elementAt( 0 );	
	int opcion = pnv.getOpcion();
	String nombre = pnv.getNombre_dir();
	String titulo = pnv.getTitulo();
	String tex = pnv.getTexto();
	String fecha_creacion = pnv.getFecha_creacion();
	%>    
  <tr>
    <td width="9%" align="left" class="fila">T&iacute;tulo:</td>
    <td width="41%" align="left" class="letra"><%=titulo%> . </td>
    <td width="9%" align="left" class="fila"> Direcci&oacute;n:</td>
    <td width="41%" align="left" class="letra">N&deg; <%=opcion%> - <%=nombre%> . </td>
  </tr>  
  <tr class="fila">
    <td colspan="4" align="center">
	    <textarea name="texto" cols="150" rows="10" class="textbox" id="texto" onKeyPress="soloAlfaNumEnterPuntoyComa(event)"><%=tex%></textarea>
    </td>
  </tr>
</table>
  </td>
  </tr>
</table>
<p align="center">
  <img src="<%=BASEURL%>/images/botones/regresar.gif" name="img_regresar" onClick = "window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/publicacion/modificarPublicacion&pagina=ModificarPublicacion1.jsp&marco=no&opcion=29';" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle" style="cursor:hand">
  
  <img src="<%=BASEURL%>/images/botones/restablecer.gif" name="img_restablecer" onClick="RestablecerPublicacion('<%=opcion%>', '<%=fecha_creacion%>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle" style="cursor:hand">
  
  <img src="<%=BASEURL%>/images/botones/modificar.gif" name="img_modificar" onClick="ModificarPublicacion(texto.value, '<%=opcion%>', '<%=fecha_creacion%>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle" style="cursor:hand">
  
  <img src="<%=BASEURL%>/images/botones/salir.gif" name="img_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle" style="cursor:hand">
</p>
<%
}//cierro if ( m = no )
	if ( m != null && m.equals("si") ){
%>                        
<%
	out.print("<table width=379 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=350 align=center class=mensajes>La Publicacion Ha Sido Modificada Satisfactoriamente!</td><td width=100><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");%>
	<br>
	<p align="center">
	  <img src="<%=BASEURL%>/images/botones/regresar.gif" name="img_regresar" onClick = "window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/publicacion/modificarPublicacion&pagina=ModificarPublicacion1.jsp&marco=no&opcion=29'; this.disabled=true;" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle" style="cursor:hand">
	  
	  <img src="<%=BASEURL%>/images/botones/salir.gif" name="img_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle" style="cursor:hand">
	</p>
<%
	}//cierro if ( m = si )
%>
</form>
</div>
<%=datos[1]%>
</body>
</html>
