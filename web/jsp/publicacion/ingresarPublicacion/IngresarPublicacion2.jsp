<!--
- Autor : LREALES
- Date  : 10 Abril 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Ingresar Publicacion</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../fintravalores/css/estilostsp.css" rel="stylesheet" type="text/css">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script>
<script>
	var controlador ="<%=CONTROLLER%>";
	
	function AgregarPublicacion ( texto, enviar_opcion, enviar_tipo, titulos ){
	
		if( texto != "" && enviar_opcion != "" && enviar_tipo != "" && titulos != "" ){
		
			document.forma.action = controlador+"?estado=Publicacion&accion=Insert&direccion=/jsp/publicacion/ingresarPublicacion/IngresarPublicacion2.jsp&msg='";
			document.forma.submit();
			
		} else{
		
			alert( "Los Campos Estan Vacios! Por Favor Verificar!!" );
			
		}
	}
</script>

</head>
<%	

	TreeMap titulos = model.publicacionService.getTitulosTree();
		
	String tipo = "MENU";
	
%>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=PUBLICACION"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px;">
<%String nom="", desc="", url="", carp = "", pag="", otro="";
int subm = 0, padre = 0, niv = 0, opcion = 0, morden = 0;%>
<FORM name='forma' id='forma' method='POST' action='<%= BASEURL %>/jsp/publicacion/ingresarPublicacion&pagina=IngresarPublicacion2.jsp'>
  <table width="90%" border="2" align="center">  
    <td colspan="7">
	  <table width="100%" class="tablaInferior">
        <tr align="center">
          <td colspan="5">
		    <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="barratitulo">
              <tr>
				<td width="40%" class="subtitulo1">Ingresar Publicaci&oacute;n</td>
				<td width="60%"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
				  <input name="opcion" type="hidden" class="textbox" id="opcion3" value="<%=opcion%>" size="8" maxlength="8">
				  <input name="id_padre" type="hidden" class="textbox" id="id_padre3" value="<%=padre%>" size="8" maxlength="8" onKeyPress="soloDigitos(event,'decNO');">
				  <input name="orden" type="hidden" class="textbox" value="<%=morden%>" size="8" maxlength="4" onKeyPress="soloDigitos(event,'decNO');">
			  </tr>
	        </table>
          </td>
	<%
	Menu m = model.menuService.getMenu();
	opcion = m.getIdopcion();
	nom = m.getNombre();
	desc = m.getDescripcion();
	url = m.getUrl();
	padre = m.getIdpadre();
	subm = m.getSubmenu();
	niv = m.getNivel();
	morden = m.getOrden();
	%>
    
	<%
    if ( (m.getSubmenu() == 2)){ 
    url = m.getUrl();
	String cola = url.substring(48, url.length());
    int asp = cola.indexOf("&");
	carp = cola.substring(0, asp);
	cola = cola.substring(asp+1, cola.length());
    int igual = cola.indexOf("=");
    asp = cola.indexOf("&");
	if ( asp != -1){
		pag = cola.substring(igual+1, asp); 
		otro = cola.substring(asp, cola.length());
	}
	else {
		pag = cola.substring(igual+1, cola.length());
	}%>
	    
  <%}%>
  <tr>
    <td colspan="5" class="fila" align="center">Direcci&oacute;n N� <%=opcion%> : <%=nom%> .</td>
  </tr>
  
  <tr class="fila">
    <td colspan="5">
      <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr class="subtitulo1" onMouseOver="bgColor= '#0092A6'" onMouseOut="bgColor='#4D71B0'" style="cursor:hand" 
			onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/publicacion/ingresarPublicacion&pagina=IngresarPublicacion1.jsp?msg=&marco=no&opcion=25'">
          <td width="8%"><img src="<%= BASEURL %>/images/menu-images/home.gif" border="0"></td>
          <td width="92%">[ FINTRAVALORES  S. A. ]</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>
			<table width="100%"  cellspacing="0" cellpadding="0">
			<%Vector vmenu = model.menuService.getVMenu();
		      for (int i = 0; i < vmenu.size(); i++){
              	   Menu menu = (Menu) vmenu.elementAt(i);
		           int midop = menu.getIdopcion();
    		        String mnom = menu.getNombre();
        		    int midpa = menu.getIdpadre();            		
					String mnompa = menu.getPnombre();
					if ( midpa != 0 ){%>
              <tr class="letraresaltada" onMouseOver="bgColor= '#99cc99'" onMouseOut="bgColor='#D1DCEB'" 
			  	onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=CargaxPerfil&direccion=/jsp/publicacion/ingresarPublicacion/IngresarPublicacion2.jsp&idop=<%=midpa%>&msg=&cmd=modop'" style="cursor:hand"> 
                 <td width="5%"><img src="<%= BASEURL %>/images/menu-images/menu_folder_open.gif" border="0"></td>
                 <td width="79%"><font size="-2"><%=mnompa%></font></td>
              </tr>
              <%}%>
              <tr align="center"> 
                <td colspan="3" align="right">
				  <table width="95%" border="0" cellpadding="0" cellspacing="0">
                    <tr class="fila"> 
                      <td width="10%">
					  <%if(menu.getSubmenu() == 2){%>
						<img src="<%= BASEURL %>/images/menu-images/menu_link_default.gif" border="0">  
					  <%}else {//carpeta%>
					  	<img src="<%= BASEURL %>/images/menu-images/menu_folder_open.gif" border="0">  
					  <%}%>
					  </td>
                      <td width="90%"><font size="-2"><%=mnom%></font></td>
                    </tr>
                    <tr align="center"> 
                      <td>&nbsp;</td>
                      <td colspan="2"> 
					    <table width="100%" border="0" cellpadding="0" cellspacing="2">
						<%Vector vhijos = model.menuService.getVhijos();
             			  for (int j = 0; j < vhijos.size(); j++){
                				Menu mhijos = (Menu) vhijos.elementAt(j);
								String hnom = mhijos.getNombre();
								int hidop = mhijos.getIdopcion();
								int hsubm = mhijos.getSubmenu();%>
						  <tr class="letraTitulo" onMouseOver=" bgColor='#99cc99'" onMouseOut= " bgColor='#D1DCEB'" 
								onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=CargaxPerfil&direccion=/jsp/publicacion/ingresarPublicacion/IngresarPublicacion2.jsp&idop=<%=hidop%>&cmd=modop&msg='">  
                            <td width="5%">
								<%if (hsubm == 1){
			  						out.println("<img src=" + BASEURL + "/images/menu-images/menu_folder_closed.gif style=cursor:hand>");
									out.println("</td><td width='79%' style=cursor:hand>"); 
								}
								else{
									out.println("<img src=" + BASEURL + "/images/menu-images/menu_link_default.gif  style=cursor:hand>");
									out.println("</td><td width='79%'  style=cursor:hand>"); 
			  					}%>
								<font size="-2"><%out.println(hnom);%></font>
							</td>
                          </tr>
                          <%}%>
                        </table>
					  </td>
                    </tr>
                  </table>
				</td>
              </tr>
              <%}%>
            </table>
          </td>
        </tr>
      </table>
	  <input type="hidden" name="sub" value="<%=subm%>">
	  <input name="nivel" type="hidden" value="<%=niv%>">
	</td>
  </tr>

  <tr class="fila">
    <td width="30%"><div align="center"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" name="obligatorio"> T&iacute;tulo</div></td>
    <td width="70%" colspan="4"><div align="center"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" name="obligatorio"> Texto</div></td>
  </tr>
  <tr class="fila">
    <td>
      <div align="left">
        <input:select name="titulos" size="10" options="<%=titulos%>" attributesText="style='width:100%;' class='listmenu';"/><script>if( forma.titulos.length>0) forma.titulos[0].selected = true;</script></div></td>
    <td colspan="4"><div align="center">
        <textarea name="texto" rows="10" class="textbox" id="texto" style="width:100%" onKeyPress="soloAlfaNumEnterPuntoyComa(event)"></textarea>
    </div></td>
  </tr>
</table>
  </td>
  </tr>
</table>
<input name="enviar_opcion" id="enviar_opcion" value="<%=opcion%>" type="hidden" class="textbox">
<input name="enviar_tipo" id="enviar_tipo" value="<%=tipo%>" type="hidden" class="textbox">
<p align="center">
  <img src="<%=BASEURL%>/images/botones/regresar.gif" name="regresar" onClick = "window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/publicacion/ingresarPublicacion&pagina=IngresarPublicacion1.jsp?msg=&marco=no&opcion=25'; this.disabled=true;" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle" style="cursor:hand">

  <img src="<%=BASEURL%>/images/botones/agregar.gif" name="agregar" onClick = "AgregarPublicacion(texto.value, enviar_opcion.value, enviar_tipo.value, titulos.value);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle" style="cursor:hand">
  
  <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle" style="cursor:hand">
<br>
</p>
<%String msg = request.getParameter("msg");
	if (!msg.equals("")){%>
                        
	  <table border="2" align="center">
   		<tr>
        	<td>
            	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                	<tr>
                    	<td width="229" align="center" class="mensajes"><%=msg%></td>
                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                    </tr>
                </table>
            </td>
		</tr>
	  </table>
	<%}%>
</form>
</div>
<%=datos[1]%>
</body>
</html>
