<!--
- Autor : Ing. Andres Martinez
- Date  : 18 de Abril de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja .....
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Historial de Publicaciones</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
  <script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
	<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Historial de Publicaciones"/>
	</div>
	<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">  
    <%
	  Vector a = (Vector)request.getAttribute("publicaciones");
	  if(a.size()>0){
		for( int i = 0; i<a.size(); i++ ){
			Vector b = (Vector)a.get(i);
			Publicacion p = (Publicacion)b.get(0); %>
			<br>
			<table width="1000"  border="2" align="center">
				<tr>
					<td>	  
						<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="barratitulo">       
							<tr>
								<td width="50%" class="subtitulo1"><%=p.getTitulo()%></td>
								<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
							</tr>
						</table>
						<table width="100%" border="1" align="center" bordercolor="#999999">       
							<tr class="tblTitulo">
								<td width="15%" nowrap>Fecha Ingreso</td>
								<td width="85%"><div align="center"></div>          <div align="center"></div>          <div align="center"><strong>Descripcion</strong><strong></strong></div></td>
							</tr>
							<% int l=-1;
							for( int j = 0; j<b.size(); j++ ){
								Publicacion pp= (Publicacion)b.get(j); 
								l++; %>
								<tr class="<%=l%2==0?"filagris":"filaazul"%>" >
									<td class="bordereporte" > <%=pp.getFecha().substring (0,16)%></td>
									<td class="bordereporte" >     <%=pp.getDescripcion()%> </td>
								</tr>
							<% }%>
						</table>
					</td>
				</tr>
			</table><% 
		}
	  }
		%>
		<br>
		<div align="left">&nbsp;<img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close()"></img></div>
</div>
</body>
</html>
