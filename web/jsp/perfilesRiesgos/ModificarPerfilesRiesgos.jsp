<%-- 
    Document   : ModificarPerfilesRiesgos
    Created on : 21/10/2018, 12:22:14 PM
    Author     : Roberto Parra
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>

<%	
    String idperfil = request.getParameter("idperfil");
%>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Modificar Perfil</title>
            <link href="fintra/css/jquery/jquery-ui/jquery-ui.css" rel="stylesheet" type="text/css"/>
            <link href="css/popup.css" rel="stylesheet" type="text/css">
    
            <script type='text/javascript' src="js/jquery/jquery-ui/jquery.min.js"></script>
            <script type='text/javascript' src="js/jquery/jquery-ui/jquery.ui.min.js"></script>
            <script type="text/javascript" src="js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
            <script type="text/javascript" src="js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
            <script type="text/javascript" src="js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>
      
   <script type='text/javascript' src="js/PerfilesDeRiesgos.js"></script>
    </head>
<body>
    
         
     <!--Encabezado (Inicio)-->
     <div id="capaSuperior" style="position:absolute; width:100%; z-index:0; left: 0px; top: 0px;">
         <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=VER DATOS DEL NEGOCIO"/>
     </div>
     <!--Encabezado (Fin)-->
         <!--Cuerpo (Inicio)-->
    <div id="capaCentral" style="position: absolute; width: 100%; height: 557px; z-index: 0; left: 0px; top: 100px; overflow: scroll;">        
      <p>&nbsp;</p>
        <div id="capaCentral" style="text-align: center;">
            <center>
              <div id="info"  class="ventana" >
                <p id="notific" ></p>
              </div>
                <div class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="min-width: 550px; max-width: 900px; padding: 2em; margin: 2em;">
                    <table>
                        <tbody><tr><td colspan="4" class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" style="text-align: center;">INFORMACION DEL PERFIL</td></tr>
                        <tr>
                            <td>Nombre<span style="color:red;">*</span></td>
                            <td><input type="text" id="descripcion"class="mayuscula" maxlength="30" value=""></td>
                        </tr>
                        <tr>
                            <td>Monto minimo <span style="color:red;">*</span></td>
                            <td colspan="3">
                                <input type="text"  id="monto_minimo" class="solo_numero puntos_de_mil" maxlength="12" value="" style="width: 96%;">
                            </td>
                        </tr>
                        <tr>
                            <td>Monto maximo <span style="color:red;">*</span></td>
                            <td colspan="3">
                                <input type="text"  id="monto_maximo" class="solo_numero puntos_de_mil" maxlength="12" value="" style="width: 96%;">
                            </td>
                        </tr>
                        <tr>
                            <td>Cantidad maxima de Creditos<span style="color:red;">*</span></td>
                            <td colspan="3">
                                <input type="text"  id="cant_max_cred" class="solo_numero puntos_de_mil" maxlength="4" value="" pattern=".+" style="width: 96%;">
                            </td>
                        </tr>
                        <tr><td colspan="4" class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix" style="text-align: center;">ESPECIFICAR LOS CREDITOS PERMITIDOS</td></tr>
                          <tr>
                            <td colspan="4">
                                <table id="TabUniNeg"></table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="border-top: 1px solid rgb(0, 128, 0); padding: 0.5em;">
                                <span id="mensaje" style="color:red;"></span>
                            </td>
                            <td style="border-top: 1px solid rgb(0, 128, 0); padding: 0.5em; text-align: right;">
                                 <form id="FormCrearPerfil" name="FormCrearPerfil" action="controller?estado=Perfiles&accion=Riesgos&carpeta=/jsp/perfilesRiesgos&pagina=ListarPerfilesRiesgos.jsp" autocomplete="on" method="post">                          
         
                                     <p class="login button"> 
                                       <input id="btnCrearPerfil" name="btnCrearPerfil" type="submit" value="Volver" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" /> 
                                       <input type="button" value="Refrescar" onclick="location.reload();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" />
                                     </p>
                                 </form>
                            </td>
                            <td style="border-top: 1px solid rgb(0, 128, 0); padding: 0.5em; text-align: right;"> 
                                
                                <button id="aceptar_btn" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="ModificarPerfil('<%=idperfil%>')">
                                    <span class="ui-button-text">Modificar</span>
                                </button>
                            </td>
                        </tr>
                    </tbody>
                  </table>
                </div>
            </center>
        </div>

    </div>
   <!--Cuerpo (Fin)-->
</body>
</html>

<script>
    
       retornarDatosPerfil("<%=idperfil%>");       
       
</script>