<%-- 
    Document   : causacionInteresesMultiservicio
    Created on : 30/03/2017, 11:12:58 AM
    Author     : mariana
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>  
        <link type="text/css" rel="stylesheet" href="./css/loaderApi.css">
        <script type="text/javascript" src="./js/utilidadInformacion.js"></script> 
        <script type="text/javascript" src="./js/causacionInteresesMultiservicio.js"></script> 
        <title>CAUSACION INTERESES MULTISERVICIO</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=CAUSACION INTERESES MULTISERVICIO"/>
        </div>

    <center>
        <div id="tablita" class="ui-jqgrid ui-widget ui-widget-content center-block" style="width: 590px; margin-top: 80px;margin-left: 60px;padding: 8px;">
            <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                <label class="titulotablita"><b>FILTRO</b></label>
            </div>
            <table id="tablainterna" style="height: 53px; width: 703px"  >
                <tr>
                    <td>
                        <label>Periodo</label>
                        <input type="text" id="periodo"  style="width: 59px;" class="requerido" >
                    </td>
                    <td>
                        <label>Cmc</label>
                        <select id="cmc"  style="width: 200px;" class="requerido" onchange="asignacionCuenta(this)"></select>
                    </td>
                    <td>
                        <label>Cuenta Ingreso</label>
                        <input type="text" id="cta_ingreso"  style="width: 134px;" class="requerido" readonly>
                    </td>
                    <td>
                        <div>
                            <center>
                                <button type="button" id="buscar" name="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-right: 118px;height: 29px;width: 75px;" > 
                                    <span class="ui-button-text">Buscar</span>
                                </button>  
                            </center>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div style="position: relative;top: 135px;">
            <table id="tabla_facturas" ></table>
            <div id="pager"></div>
        </div>
        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>
        <div id="loader-wrapper" style="display: none">
            <img src="http://www.fintra.co/wp-content/uploads/2016/04/fintrafooter.png" alt="" class='loaderimg' />
            <div id="loader">          </div>
            <div class="loader-section section-left"></div>
            <div class="loader-section section-right"></div>
        </div>
        <div id="divSalidaEx" title="Exportacion" style=" display: block" >
            <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
            <center>
                <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
            </center>
            <div id="respEx" style=" display: none"></div>
        </div> 
    </center>
</body>
</html>
