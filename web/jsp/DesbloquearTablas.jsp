<%-- 
    Document   : DesbloquearTablas
    Created on : 5/10/2015, 02:42:28 PM
    Author     : mariana
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/DesbloquearTablas.js"></script> 
        <script type="text/javascript" href="http://www.ddginc-usa.com/spanish/editcheck.js"></script>
        <title>DESBLOQUEAR TABLAS</title>
        <META HTTP-EQUIV="REFRESH" CONTENT="60">


    </head>
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=DESBLOQUEAR TABLAS"/>
    </div>

    <center>
        <div style="position: relative;top: 231px;">
            <div id="contenedor" >
                <div id="letras" style="margin-top: 31px;">
                    <label style="font-size: 16px;">Este modulo permite la visualización del estado de las tablas del web service(ws) con TSP,<br>
                        se debe tener en cuenta que para el buen funcionamiento el estado de las tablas deben estar en <span style="color: #0000CC">Sin procesar.</span></label>
                </div>
            </div>
        </div>
        <div style="position: relative;top: 235px;">
            <table id="tabla_desbloquear_tablas" ></table>
            <div id="pager1"></div>
        </div>
        <div id="info"  class="ventana" >
            <p id="notific">mensaje</p>
        </div>
    </center>
</body>
</html>
