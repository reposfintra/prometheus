<!--
- Autor : Ing. Julio Ernesto Barros Rueda
- Date  : 14 de Mayo de 2007
- Copyrigth Notice : Fintravalores S. A.
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el ingreso de identidades

--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html;"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%> 
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.util.Util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Captaciones</title>

<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 

<style type="text/css">
<!--
.style1 {font-size: 14px}
-->
</style>
</head>

<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/general.js"></script>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script src="<%= BASEURL %>/js/transferencias.js"></script>

<%  Usuario usuario            = (Usuario) session.getAttribute("Usuario");
	Propietario  Propietario  = new Propietario();
	String Retorno   = "";
	String Captacion = "";
	String Reembolso = "";
	String Findemes  = "";
	String  vista    = "";
	String  Nit      =" ";
	Captacion = ""+model.CaptacionesFintraSvc.ConsecutivoCaptaciones("CAPTACION");
	Reembolso = ""+model.CaptacionesFintraSvc.ConsecutivoCaptaciones("REEMBOLSO");
	Findemes = ""+model.CaptacionesFintraSvc.ConsecutivoCaptaciones("FINDEMES");
	Propietario = model.CaptacionesFintraSvc.getPropietario();
	Retorno = request.getParameter("retorno"); 
	vista   = request.getParameter("vista"); 
	Nit     = request.getParameter("Nit");
	if ( vista != null && vista.equals("Operacion") && Propietario.getFrecuencia()== null ){
		Retorno = "El  Nit  del Proveedor  "+Nit+"  No se encuentra registrado en la Base de Datos  ";
		Propietario  = new Propietario();
		vista    = "";
		Nit      =" ";
		
	}
	if( Nit == null ){
		Nit="";
		Propietario.setS_nombre      ( "" );
		Propietario.setFrecuencia    ( "" );
		Propietario.setTasa_captacion( 0 );
	}
	
	//System.out.println("terminao parte java = "+Nit+"    vista  "+vista);
%>

<script>
	   function enviarFormulario(CONTROLLER,frm){
			var sen = 0 ;
			
			if(frm.Nit.value == null)
				frm.Nit.value ="";
			if(frm.tDocumento.value == '03'){
				var capA = parseFloat(frm.cAptal.value);
				var nCap = parseFloat(frm.vlr.value);
				if(nCap > capA ){
					sen=1;
				    alert(frm.vlr.value+'El valor del reembolso no puede ser superior al valor actual del capital'+frm.cAptal.value);
				}
			}
			if(sen == 0 ){
				document.imgaceptar.src='<%=BASEURL%>/images/botones/aceptarDisable.gif';
				document.imgaceptar.onmouseover = new Function('');
				document.imgaceptar.onmouseout  = new Function('');
				document.imgaceptar.onclick     = new Function('');
			   frm.submit();
			}
	   }
	   
	   function cargarConsecutivo(theForm,capta,reint,finmes){
			 var con=0;               
			 if(theForm.tDocumento.value==''){
				alert('Deberá seleccionar un tipo de operación');
				con=1;
			 }
             if(con==0){	
                if( theForm.tDocumento.value == "01" ){
					theForm.nodocumento.value = capta ;
				}
				if( theForm.tDocumento.value == "03" ){
					theForm.nodocumento.value = reint ;
				}
				if( theForm.tDocumento.value == "04" ){
					theForm.nodocumento.value = finmes ;
				}
				
             }
	   }
	   
	</script>




<body   >
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/toptsp.jsp?encabezado=CAPTACIONES"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<FORM action="<%=CONTROLLER%>?estado=Captaciones&accion=Fintra&opcion=EjecutarOperacion" name='forma' method="post" >
  <table border="2"align="center">
    <tr>
      <td>
	  <table width="100%" class="tablaInferior" >
        <tr class="fila">
          <td colspan="2" align="left" nowrap class="subtitulo">&nbsp;Captaci&oacute;n</td>
          <td colspan="2" align="left" nowrap class="bordereporte"><img src="<%=BASEURL%>/images/fintra.gif" width="166" height="28" class="bordereporte"> </td>
        </tr>
        <tr class="fila">
          <td width="141" height="23"  colspan="4" align="left" class="fila" >Proveedor</td>
        </tr>
        <tr class="fila">
          <td width="141" align="left" > Nit </td>
          <td width="225" valign="middle">
            <input id="Nit" name="Nit" type="text" class="textbox" value="<%=Nit%>" maxlength="15">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> 
			
			<img width='14' title='Cargar Datos Propietario' style="cursor:hand" src='<%=BASEURL%>/images/botones/iconos/modificar.gif'
             onclick="location.href ='<%=CONTROLLER%>?estado=Captaciones&accion=Fintra&opcion=CargarDatos&Nit='+Nit.value">
			
			</td>
          <td width="151" valign="middle">Frecuencia</td>
          <td width="201" valign="middle"><select name="frecu" class="listmenu">
		  
						<option value='' selected> - - </option> 
						<option value='Mensual'>Mensual</option>
						<option value='Semanal'>Semanal</option> 
						<option value='Trimestral'>Trimestral</option>
						<%
						//System.out.println("cargando la frecuencia 01= ");
						//System.out.println("el valor es : "+Propietario.getFrecuencia());
						 if ( Propietario.getFrecuencia()!= null && !Propietario.getFrecuencia().equals("") ) { 
						 		//System.out.println("cargando la frecuencia = "+Propietario.getFrecuencia());
						 %>
							<option  value='<%=Propietario.getFrecuencia()%>' selected><%=Propietario.getFrecuencia()%> </option>
						<% } %>
					    </select></td>
        </tr>
        <tr class="fila">
          <td height="27" align="left" >Nombre</td>
  		  <%  //System.out.println("cargando nombre propietario  "+Propietario.getS_nombre());
		     if ( Propietario.getFrecuencia()!=null &&!Propietario.getFrecuencia().equals("") ) {  %>
          		<td width="225" valign="middle"> <%= Propietario.getS_nombre() %>          </td>
		  <% }else{%>
		  		<td width="225" valign="middle">   </td>
		 <% } %>
          <td width="151" valign="middle" >% de Interes </td>
		  <% if ( Propietario.getFrecuencia()!=null  && !Propietario.getFrecuencia().equals("") ) {  %>
          			<td width="201" valign="middle"><input name="PorInt" type="text"  class="textbox" id="c_libreta" onKeyPress="" value="<%=Propietario.getTasa_captacion()%>" maxlength="15">            </td>
		  <% }else{%>
		  	<td width="201" valign="middle"> </td>
		  <% } %>
        </tr>
      </table>	
	<% //System.out.println("vista "+vista);
	   if ( vista != null && vista.equals("Operacion") ){
	     //System.out.println(" entro a la vista "+vista);  %>
		   
			  <table width="748" height="54" class="tablaInferior">
					<tr class="fila">
					  <td width="141" height="24" class="fila">Numero Documento </td>
					  <td width="230" class="fila"><input name="nodocumento" type="text" class="textbox" readonly></td>
					  <td width="154" class="fila">Tipo Documento</td>
					  <td width="203" class="fila">		  
					  <select name="tDocumento" class="listmenu" onChange="cargarConsecutivo(forma,'<%=Captacion%>','<%=Reembolso%>','<%=Findemes%>')">
						<option value="" selected> - - </option>
						<option value="01">CAPTACION</option>
						<option value="03">REEMBOLSO</option>
						<option value="04">FIN DE MES</option>
					  </select></td>
					
					</tr>
					<tr class="fila">
					  <td class="fila">Fecha Inicio</td>
					  <td class="fila">
					  
					  <input name='fechainicio' type='text' class="textbox" id="fechainicio" style='width:120' value='<%=com.tsp.util.Util.AnoActual()+"-"+com.tsp.util.Util.getFechaActual_String(3)+"-"+com.tsp.util.Util.getFechaActual_String(5)%>' readonly>
							  <a href="javascript:void(0);" class="link" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.forma.fechainicio);return false;"  HIDEFOCUS > <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16"
								border="0" alt="De click aqu&iacute; para ver el calendario."></a> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
					  
					  
					  
					  </td>
					  <td colspan="2" class="fila">&nbsp;</td>
				    </tr>
				
			  </table>	
			  
			  <table width="748" height="53" class="tablaInferior">
				<tr class="fila">
				  <td width="154" height="24" class="fila">Clase de Movimiento </td>
				  <td width="357" class="fila">
				  <select name="cMovimiento" class="listmenu">
					<option value="" selected> - - </option>
					<option value="CO">Consignación</option>
					<option value="TR">Traslado</option>
					<option value="CH">Cheque</option>
					<option value="EF">Efectivo</option>
				  </select></td>
				  <td width="357" class="fila">Numero de Movimiento </td>
				  <td width="357" class="fila"><input name="nMovimiento" type="text" class="textbox"></td>
				</tr>
				  <td width="154" class="fila">Monto</td>
				  <%//  onBlur="this.value=formatear(this.value,2); actualizarSaldo();" onFocus="this.value=sinformato(this); this.select();"><%>
				    <td colspan="3" class="fila"><input name="vlr" type="text" class="textbox"  onKeyPress="soloDigitos(event,'decNO')" ></td>
				  </tr>
		  </table>	
	   <%}%> 
	  </td>
    </tr>
  </table>
  <p>
  
  <%
  if( Propietario.getCapital() != 0 && vista != null && vista.equals("Operacion") ){
  		//System.out.println("jsp el valor es : "+Propietario.getCapital());
  %>
	<table width="302" height="167" border="2"align="center">
		
		<tr>
		  <td height="159">
		  <table width="100%" height="72%" class="tablaInferior" >
			<tr class="fila">
			  <td height="16%"  colspan="4" align="left" class="fila" ><div align="center">Estado Actual del Capital </div></td>
			</tr>
			<tr class="fila">
			  <td width="51%" height="20%" align="left" > Valor Capital </td>
			  <td width="49%" valign="middle"><input name="capitalA" type="text" class="textbox" value='<%= Utility.customFormat(Propietario.getCapital())%>' readonly>
			                                  </td>
		    </tr>
			<tr class="fila">
			  <td height="20%" align="left" >Fecha de Inicio </td>
			  <td valign="middle"><input name="fechaA" type="text" class="textbox" value='<%=Propietario.getInicio().substring(0,10)%>' readonly></td>
			  
			  <%
			  String fecha_corte  ="";
              int    dias_tras    =0;
			  double vlr_interes  =0;
              double retefuente   =0;
              double vlr_Tcapital =0;
			  
			  fecha_corte = com.tsp.util.Util.fechaMenosUnDia(com.tsp.util.Util.AnoActual()+"-"+com.tsp.util.Util.getFechaActual_String(3)+"-"+com.tsp.util.Util.getFechaActual_String(5));

              dias_tras   = com.tsp.util.Util.diasTranscurridosFintra(Propietario.getInicio(),fecha_corte+" 00:00:00") + 1;
			  //System.out.println("jsp dias que pasaron entre  : "+Propietario.getInicio()+"   y  "+fecha_corte);
			  //System.out.println("son : "+ dias_tras );
			  if(dias_tras < 0)dias_tras = 0;
              vlr_interes = Math.round(((double)dias_tras/30.0)*(double)((double)Propietario.getTasa_capital()/100.0)*Propietario.getCapital());
              retefuente  = Math.round((7.0/100.0)*vlr_interes);
              vlr_Tcapital= Math.round(Propietario.getCapital()+vlr_interes-retefuente);
			  %>
		    </tr>
			<tr class="fila">
			  <td height="20%" align="left" >Intereses Generados </td>
			  <td valign="middle"> <input name="InteresGenerdoA" type="text" class="textbox" value='<%= Utility.customFormat(vlr_interes) %>' readonly> </td>
			</tr>
			<tr class="fila">
			  <td height="24%" align="left" >Valor Actual Capital </td>
			  <td valign="middle"><input name="ValorcapitalA" type="text" class="textbox" value=' <%= Utility.customFormat(vlr_Tcapital) %>' readonly>
			  					  <input type="hidden" name="cAptal" value='<%= vlr_Tcapital %>'></td>
			</tr>
		  </table>	    
		  <table width="100%" class="tablaInferior" >
			
			<tr class="fila">
			  <td width="50%" align="left" > <div align="center">Frecuencia </div> </td>
			  <td width="50%" valign="middle"><div align="center">% de Interes </div></td>
		    </tr>
			<tr class="fila">
			  <td align="left" ><input name="frecuenciaA" type="text" class="textbox" value='<%=Propietario.getFrecuencia_capital()%>' readonly> </td>
			  <td valign="middle"><input name="TasaA" type="text" class="textbox" value='<%=Propietario.getTasa_capital()%>' readonly>
			   </td>
			</tr>
			
			
		  </table>	  
		  </td>
		</tr>
	</table>
		<%}%>
  <p><br>
    </p>
  <div align="center">
  		<% if ( vista != null && vista.equals("Operacion") ){ %>
	   			<img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="javascript:enviarFormulario('<%=CONTROLLER%>',forma);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;       &nbsp;
	    <%}%>
       <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
   </div>

  
  
  </p>
  <p>
<%if( Retorno != null ){%>  
  <table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=Retorno%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%>

</p>
 <br>
</form>
</div>

<iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
		 </iframe>
</body>
</html>


