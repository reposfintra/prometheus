<!--
     - Author(s)       :      JULIO BARROS RUEDA
     - Date            :      22/11/2006  
     - Copyright Notice:      Fintravalores S.A.
  -->
 <%--   - @(#)  
     - Description: 
 --%>


<%@page contentType="text/html; charset=iso-8859-1"%>
<%@page session="true"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<% TreeMap Agencia = model.agenciaService.getAgencia();
 Agencia.put(" TODAS","TODAS");
%>

<html>
<head>
	<title>.:: Consulta Mes a Mes </title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
	<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
	<script type='text/javascript' src="<%=BASEURL%>/js/Validacion.js"></script>
	<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
	<script src='<%=BASEURL%>/js/date-picker.js'></script>
	
	<script>
	   function enviarFormulario(CONTROLLER,frm){
		   if((frm.fechai.value != "" )&&(frm.fechaf.value != "" )){
				 if(frm.fechai.value <= frm.fechaf.value){
				 	document.mod.src='<%=BASEURL%>/images/botones/aceptarDisable.gif';
           			document.mod.onmouseover = new Function('');
           			document.mod.onmouseout  = new Function('');
           			document.mod.onclick     = new Function('');
					frm.submit();
				 }else{
					 alert("La fecha inicial debe ser menor o igual a la final");     
				 }
		   }else{
			   alert("La campos fecha no pueden estar vacios"); 
		   }                                                                                      
	   }
	</script>
	
</head>
	
	
		<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
			<jsp:include page="/WEB-INF/toptsp.jsp?encabezado= Captaciones "/>
		</div>
		<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
			<form action="<%=CONTROLLER%>?estado=Captaciones&accion=Fintra&opcion=VistaCaptacionesMesAMes" method="post" name="formulario">
				<table border="2" align="center" width="451">
				  <tr>
					<td width="439" >
					  <table width="99%" align="center">
						<tr>
						  <td width="392" height="24"  class="subtitulo1"><p align="left">Filtro para Captaciones Mes a Mes </p></td>
						  <td width="392"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
						</tr>
					  </table>
					  <table width="99%" align="center">
					    
                  <tr class='fila'>
                     
                  </tr>  
						<tr class="fila">
							<td><strong>&nbsp; Fecha inicial </strong></td>
							<td>
							  <input name='fechai' type='text' class="textbox" id="fechai" style='width:120' value='<%=com.tsp.util.Util.AnoActual()+"-"+com.tsp.util.Util.getFechaActual_String(3)+"-01"%>' readonly>
							  <a href="javascript:void(0);" class="link" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fechai);return false;"  HIDEFOCUS > <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16"
								border="0" alt="De click aqu&iacute; para ver el calendario."></a> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
						 </tr>
						 <tr class="fila" >
							<td><strong>&nbsp; Fecha final </strong></td>
							<td>
							  <input name='fechaf' type='text' class="textbox" id="fechaf" style='width:120' value='<%=com.tsp.util.Util.AnoActual()+"-"+com.tsp.util.Util.getFechaActual_String(3)+"-"+com.tsp.util.Util.diasDelMes(com.tsp.util.Util.MesActual())%>' readonly>
							  <a href="javascript:void(0);" class="link" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fechaf);return false;"  HIDEFOCUS > <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16"
							  border="0" alt="De click aqu&iacute; para ver el calendario."></a> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
						  </tr>
					   </table>
					 </td>
				  </tr>
				</table>
				<br>
				<table width="595" border="0" align="center">
				  <tr>
					<td align="center">
					  <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod"  height="21" onClick="javascript:enviarFormulario('<%=CONTROLLER%>',formulario);"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
					  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></td>
					</tr>
				</table>
				<br>
		 <%if (request.getParameter("msg") != null ){%>      
            <table border="2" align="center">
              <tr>
                <td>
					<table width="100%"  border="0" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                  		<tr>
                    		<td width="500" align="center" class="mensajes"><%= request.getParameter("msg") %></td>
                    		<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    		<td width="58">&nbsp; </td>
                  		</tr>
                	</table>
				</td>
              </tr>
     </table>               
     <%}%>   
				
			</form>
		</div>
	<iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
		 </iframe>
		 
		 
	
	</body>
	</html>