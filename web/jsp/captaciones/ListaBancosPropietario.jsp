<!--
- Autor      : Ing. Fernell Villacob
- Date       : 05  Agosto 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Vista que permite escojer banco para la transferencia
--%>


<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>



<html>
<head>
     <title>Lista de Bancos</title>
     <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
     <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
     <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/transferencias.js"></script>
     
     

 <% 
    String  nit       = request.getParameter("nit");
    String  nombre    = request.getParameter("nombre");
    String  anticipo  = request.getParameter("anticipo"); 
    String  global    =  request.getParameter("global");
	String  secue 	  =  request.getParameter("secu");
	String  desBanco  = request.getParameter("infoCTA"); 
	String  Ncuenta   = request.getParameter("Ncuenta");
    String  URL       = CONTROLLER+"?estado=Captaciones&accion=Fintra&opcion=ASIGNARBANCO&nit="+ nit +"&anticipo="+ anticipo +"&global="+ global +"&desBanco="+ desBanco +"&secue="+secue+"&Ncuenta="+Ncuenta;
    List    listCTA   = (List) request.getAttribute("listaBancos");
    String  sendPP    = request.getParameter("sendPP");
%>
    
    
     
     <script>
           function  asignar(theForm){    
             <% if( listCTA.size()>0){%> 
                  var sec = '';
                  for(var i=0;i<theForm.length;i++)
                      if(theForm.elements[i].type=='radio'  &&  theForm.elements[i].checked )
                           sec = theForm.elements[i].value;                       
                  window.close();
                  parent.opener.location.href = '<%=URL%>&sec='+ sec;
             <%}else{%>
                 alert('No presenta cuentas para pago');
             <%}%>
           }
    </script>

     
     
</head>

<body onLoad="redimensionar();" onResize="redimensionar();">


<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    %>
    
    
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/toptsp.jsp?encabezado=Lista de Bancos"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>
  

    

  <form action="" method='post' name='formulario' >
 
   <table width="98%" border="2" align="center">
       <tr>
          <td>  
               <table width='100%' align='center' class='tablaInferior'>

                      <tr class="barratitulo">
                        <td colspan='2' >
                           <table cellpadding='0' cellspacing='0' width='100%'>
                             <tr>
                              <td align="left" width='65%' class="subtitulo1">&nbsp;LISTA DE CUENTAS  A CONSIGNAR <br> <%= nit %> &nbsp <%=nombre%></td>
                              <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"  height="20" align="left"><%=datos[0]%></td>
                            </tr>
                           </table>
                        </td>
                     </tr>
                     
                     <tr class="fila">
                            <td width='100%' colspan='2'>
                                <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">

                                     <TR  class="tblTitulo"  >      
                                            <TH nowrap style="font size:11; font weight: bold">&nbsp      </TH>
                                            <TH nowrap style="font size:11; font weight: bold">NOMBRE     </TH>
                                            <TH nowrap style="font size:11; font weight: bold">NIT        </TH>
                                            <TH nowrap style="font size:11; font weight: bold">TIPO       </TH>
                                            <TH nowrap style="font size:11; font weight: bold">N�MERO     </TH>                                            
                                            <TH nowrap style="font size:11; font weight: bold">BANCO      </TH>
                                            <TH nowrap style="font size:11; font weight: bold">SUCURSAL   </TH>
                                            <TH nowrap style="font size:11; font weight: bold">DEFAULT    </TH>
                                            <TH nowrap style="font size:11; font weight: bold">LIQ</TH>
                                      </TR>

                                     <% if(listCTA != null &&  listCTA.size()>0 ){
                                           for(int i=0;i<listCTA.size();i++){
                                               Hashtable  cta   = (Hashtable)listCTA.get(i) ;
                                               String     sec   = (String) cta.get("primaria");
                                               String     sel   = (sec.equals("S"))?"checked":"";
                                               String     param = "beneficiarios('"+ cta.get("cedula_cuenta") +"','"+ cta.get("nombre_cuenta") +"','"+ cta.get("tipo_cuenta") +"','"+ cta.get("banco") +"','"+ cta.get("sucursal") +"','"+ cta.get("cuenta") +"')";
                                               if (sel.equals("checked")) { out.print("<script> beneficiario = "+ param +"; </script>"); }
                                    %>

                                              <tr class='<%= (i%2==0?"filagris":"filaazul") %>'   style=" font size:12" onMouseOver='cambiarColorMouse(this)'  >         

                                                    <TD  class="bordereporte" align='center' nowrap style="font size:11"  ><input type='radio' <%= sel %>  name='CTA' value='<%= cta.get("secuencia") %>' onchange=" if(this.checked) { beneficiario = <%= param %>; } " ></TD>
                                                    <TD  class="bordereporte" align='center' nowrap style="font size:11"  ><%= cta.get("nombre_cuenta") %>  </TD>
                                                    <TD  class="bordereporte" align='center' nowrap style="font size:11"  ><%= cta.get("cedula_cuenta") %>  </TD>
                                                    <TD  class="bordereporte" align='center' nowrap style="font size:11"  ><%= cta.get("tipo_cuenta")   %>  </TD>
                                                    <TD  class="bordereporte" align='center' nowrap style="font size:11"  ><%= cta.get("cuenta")        %>  </TD>
                                                    <TD  class="bordereporte" align='center' nowrap style="font size:11"  ><%= cta.get("banco")         %>  </TD>
                                                    <TD  class="bordereporte" align='center' nowrap style="font size:11"  ><%= cta.get("sucursal")      %>  </TD>
                                                    <TD  class="bordereporte" align='center' nowrap style="font size:11"  ><%= cta.get("primaria")      %>  </TD>
                                                    <TD  class="bordereporte" align='center' nowrap style="font size:11"  ><%= cta.get("secuencia")     %>  </TD>
                                             </tr>
                                          <%}
                                        }else{%>               
                                           <TR><Th colspan='7' class="bordereporte" align='center' nowrap style="font size:11" > No presenta cuenta... </Th></TR>
                                     <%}%>

                                 </table>
                           </td>
                       </tr> 
         
              </table>
         </td>
      </tr>
   </table> 
  
   </form>
   
   
   <br>
                 
   <img src="<%=BASEURL%>/images/botones/aplicar.gif"    height="21"  title='Aplicar'    onclick='<% if (sendPP==null) { %> asignar(formulario); <% } else { %> retornarParametrosPago(beneficiario); <% } %>' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
   <img src="<%=BASEURL%>/images/botones/agregar.gif"    height="21"  title='Agregar'    onclick="location.href ='<%=CONTROLLER%>?estado=Cuentas&accion=Propietarios&evento=INIT&nit=<%=nit%>';"               onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
   <img src="<%=BASEURL%>/images/botones/salir.gif"      height="21"  title='Salir'      onClick="window.close();"                                                                                             onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
   

</div>
<%=datos[1]%>  

</body>
</html>
