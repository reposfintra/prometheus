<!--
- Autor      : Ing. Fernell Villacob
- Date       : 04  Agosto 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Vista que permite mostrar anticipos pagos terceros
--%>


<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>



<html>
<head>
        <title>Listado Anticipos Pagos Terceros</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
		<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script> 	
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/Validaciones.js"></script>
        
        
 <%  String  vista  = request.getParameter("vista"); 
     String  msj    = request.getParameter("msj"); 
     String  title  = "TRANSFERIR ANTICIPOS - ";
     String  opcion = "TRANSFERIR";
     String  colums = "26";  //?estado=Captaciones&accion=Fintra&opcion=BUSCARAPROBADAS";
     String  URL    = CONTROLLER +"?estado=Captaciones&accion=Fintra&opcion="+opcion;
     List    lista  = (List) request.getAttribute("listaAnticipos");
	 String  Banco  = request.getParameter("desBanco");
	 String  Ncuenta = request.getParameter("Ncuenta");
	 
	 %>
     
        <script>
            function send(theForm){
               var con=0;               
               <% if( vista.equals("TRANSFERIR") ){%>
                      if(theForm.infoCTA.value==''){
                         alert('Deberá establecer la cuenta del proveedor del anticipo');
                         theForm.infoCTA.focus();
                         con=1;
                      }
               <%}%>
                if(con==0){
                   for(var i=0;i<theForm.length;i++){
                       var ele = theForm.elements[i];
                       if(ele.type=='checkbox'  &&  ele.id!='All'  && ele.checked  )
                          con++;
                   }               
                   if(con==0)  alert('Deberá seleccionar por lo menos un anticipo');
                   else        theForm.submit();
                }
            }
			
			function comision (form){ 
				form.action =  "<%=CONTROLLER%>?estado=Captaciones&accion=Fintra&opcion=BUSCARAPROBADAS";
				form.submit();
			}
			
			function Confirm(form,secuencia){
				if( confirm('Desea Reversar la Transaccion?') ){
					//alert('values  '+secuencia);
					form.action =  "<%=CONTROLLER%>?estado=Anticipos&accion=PagosTerceros&evento=ELIMINARTRANSACCION&sec="+secuencia;
					//alert('al submit');
					form.submit();
				}
			} 
			
        </script>
        
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">


 
<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    %>

    
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Anticipos Pagos Terceros"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>



     

 
        <form action="<%=URL%>" method='post' name='formulario' id='formulario' >
        
         
           <table width="98%" border="2" align="center">
               <tr>
                  <td>  
                       <table width='100%' align='center' class='tablaInferior'>

                          <tr class="barratitulo">
                            <td colspan='<%= colums %>' >
                               <table cellpadding='0' cellspacing='0' width='100%'>
                                     <tr>
                                          <td align="left" width='70%' class="subtitulo1">&nbsp;<%=title%>  [ <%=  model.AnticiposPagosTercerosSvc.getProveedor() +" "+ model.AnticiposPagosTercerosSvc.getNameProveedor()%> ]</td>
                                          <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"  height="20" align="left"><%=datos[0]%></td>
                                    </tr>
                               </table>
                            </td>
                         </tr>
                         
                         
                         <tr class="fila">
                            <td width='100%' colspan='2'>
                                <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
								
								<% if( vista.equals("TRANSFERIR") ){
                                                List bancosTercero =  model.AnticiposPagosTercerosSvc.getListCTATercero();%>
                                                
										<tr  class="fila">  
										   <td colspan='<%= colums %>'>
													<table  width='100%' bordercolor="#999999" bgcolor="#F7F5F4" align="center">
														  <tr  class="fila">
															   <td width='20%'  > CUENTAS PROVEEDOR ANTICIPO      </td>
															   
															   <td >
																	<select name='infoCTA' style='width:30%' onChange='comision(formulario)' class="listmenu" >
																	   <%
																	   
																	   if( bancosTercero.size()>0){
																		   for(int i=0;i<bancosTercero.size();i++){
																				 Hashtable  bk            = (Hashtable)bancosTercero.get(i);
																				
																				 String     aliasBK       = (String) bk.get("codigo");
																				 String     descripcion   = (String) bk.get("descripcion");
																				 String     vec[]         =  descripcion.split(",");
																				 if( vec.length== 5){
																					 String codeBK  = vec[0];
																					 String descBK  = vec[1];
																					 String ctaBK   = vec[2];
																					 String tipoCTA = vec[3];
																					 //System.out.println("a if   "+ctaBK+"  "+Ncuenta); %>
																					 
																					 <%if(Ncuenta != null && Ncuenta.equals(ctaBK) ){%>
																						   <option selected value='<%=codeBK%>-<%=descBK%>-<%=ctaBK%>-<%=tipoCTA%>'> <%=aliasBK%> - <%=ctaBK%>  - <%=tipoCTA%>  </option>
																					 <%}else{ 
																							if ( Ncuenta == null && i==0){
																							     Ncuenta = ctaBK;
																							}%>	
																								<option value='<%=codeBK%>-<%=descBK%>-<%=ctaBK%>-<%=tipoCTA%>'> <%=aliasBK%> - <%=ctaBK%>  - <%=tipoCTA%>  </option>
																							
																					 <%}%>
																			  <%}else{%>
																					 <option value=''> <%=aliasBK%> - Información de Cuenta Incompleta </option>
																				<%}
																			}
																		 }else{%>
																			   <option value=''> No presenta información de cuentas registrada </option>
																	   <%}%>
																		
																	</select>
															   </td>
														  </tr>
													</table>                                                              
											</td>
										</tr>
                               
                                        <%}%>
								
					
                                         <% if( lista !=null && lista.size()>0){%>

                                         
                                         <tr class="tblTitulo" >
                                              <TH   nowrap style="font size:11; font weight: bold" height='25'       colspan='5' >DATOS ANTICIPO        </TH>                                              
                                              <% if( vista.equals("TRANSFERIR") ){%> 
                                                    <TH   nowrap style="font size:11; font weight: bold" colspan='5' >VALORES               </TH>
                                                    <TH   nowrap style="font size:11; font weight: bold" colspan='9' >CUENTA A TRANSFERIR   </TH>
                                                    <TH   nowrap style="font size:11; font weight: bold" colspan='2' >APROBACION            </TH>
                                              <%}%>                                              
                                         </tr>
                                         
                                         
                                         <tr class="tblTitulo" >
					
                                              <TH   nowrap style="font size:11; font weight: bold" >No                </TH>
                                              
                                              <TH   nowrap style="font size:11; font weight: bold" >PROPIETARIO       </TH>              
                                              <TH   nowrap style="font size:11; font weight: bold" >FECHA             </TH>
                                              <TH   nowrap style="font size:11; font weight: bold" >REANT             </TH>
                                              <TH   nowrap style="font size:11; font weight: bold" >VALOR             </TH>       
                                              
                                              <% if( vista.equals("TRANSFERIR") ){%>
                                                    
                                                    <TH   nowrap style="font size:11; font weight: bold" >% DESC            </TH>
                                                    <TH   nowrap style="font size:11; font weight: bold" >VLR DESC          </TH>
                                                    <TH   nowrap style="font size:11; font weight: bold" >VLR NETO          </TH>
                                                    <TH   nowrap style="font size:11; font weight: bold" >COMISION          </TH>
                                                    <TH   nowrap style="font size:11; font weight: bold" >CONSIGNAR         </TH> 
                                                    <TH   nowrap style="font size:11; font weight: bold" >CUENTA GIRADORA  </TH> 
                                                    <TH   nowrap style="font size:11; font weight: bold" >LQ</TH> 
                                                    
                                                    <!-- Julio Barros 17-11-2006 cambio de pocision-->
                                                    <TH   nowrap style="font size:11; font weight: bold" > <input type='checkbox'  id='All' onclick="Sell_all(this.form,this);">  </TH> 

                                                    <TH   nowrap style="font size:11; font weight: bold" >BANCO             </TH>
                                                    <TH   nowrap style="font size:11; font weight: bold" >SUCURSAL          </TH>
                                                    <TH   nowrap style="font size:11; font weight: bold" >CUENTA            </TH>
                                                    <TH   nowrap style="font size:11; font weight: bold" >TIPO CTA          </TH>
                                                    <TH   nowrap style="font size:11; font weight: bold" >NOMBRE CTA        </TH>
                                                    <TH   nowrap style="font size:11; font weight: bold" >NIT    CTA        </TH> 
                                                    
                                                   
                                                    <TH   nowrap style="font size:11; font weight: bold" >FECHA             </TH>
                                                    <TH   nowrap style="font size:11; font weight: bold" >USUARIO           </TH>
                                                    
                                              <%}%>
                                              
                                         </tr>


                                         <%for(int i=0;i<lista.size();i++){
                                                 AnticiposTerceros anticipo = (AnticiposTerceros) lista.get(i);%>     
                                                         <% if( vista.equals("TRANSFERIR") ){%>
                                                           <tr class='<%= (i%2==0?"filagris":"filaazul") %>' id='fila<%=i%>'   style=" font size:12"   >
	                                                         <!-- Julio Barros 17-11-2006 cambio de pocision-->
                                                              
                                                                 <td class="bordereporte"                nowrap style="font size:10"> <%=  i+1           %> </td>                       
                                                                 <td class="bordereporte"                nowrap style="font size:10" onclick= "javascript: var x= window.open( '<%= CONTROLLER%>?estado=Captaciones&accion=Fintra&opcion=LISTABANCOS&nit=<%=anticipo.getConductor()%>&anticipo=<%= anticipo.getId()%>&global=S&Ncuenta=<%=Ncuenta%>&infoCTA=<%=Banco%>','Asignar','top=100,left=100, width=700, height=400, scrollbars=yes, status=yes, resizable=yes  ');  x.focus();" >&nbsp  
                                                                 <%=  anticipo.getPla_owner()               %> - <%=  anticipo.getNombrePropietario()       %> </td> 
																 
                                                                 <td class="bordereporte" align='center' nowrap style="font size:10"> <%=  anticipo.getFecha_anticipo()          %> </td>
                                                                 <td class="bordereporte" align='center' nowrap style="font size:10"> <%=  anticipo.getReanticipo()              %> </td>
                                                                 <td class="bordereporte" align='right'  nowrap style="font size:10"> <%=  Util.customFormat(anticipo.getVlr() ) %> </td>
                                                         <%}%> 
														     
														 
														 
                                                         <% if( vista.equals("TRANSFERIR")){%>
                                                             <!-- Valores -->
                                                              <td class="bordereporte" align='center' nowrap> 
                                                                       <input type='text' style="font size:10" style="width:30"  name='porcentaje<%=i%>' id='porcentaje<%=i%>'    dir='rtl'        value='<%= Util.customFormat( anticipo.getPorcentaje()   ) %>' onfocus='this.select()' maxlength='4' >                                                                           
                                                                       
																		 <img width='14'    title='Aplicar descuento' style="cursor:hand" src='<%=BASEURL%>/images/botones/iconos/modificar.gif'
																		 onclick="location.href = '<%=CONTROLLER%>?estado=Captaciones&accion=Fintra&opcion=ASIGNARDESCUENTO&nit=<%=anticipo.getPla_owner()%>&anticipo=<%= anticipo.getId()%>&infoCTA=<%=Banco%>&Ncuenta=<%=anticipo.getCuenta_transferencia()%>&valor=' + porcentaje<%=i%>.value; "> 
																   
                                                              </td>
                                                              <td class="bordereporte" align='center' nowrap> <input type='text' style="font size:10" style="width:70"  name='vlrDescuento<%=i%>'  dir='rtl' readonly value='<%= Util.customFormat( anticipo.getVlrDescuento() ) %>'> </td>                                                                
                                                              <td class="bordereporte" align='center' nowrap> <input type='text' style="font size:10" style="width:80"  name='vlrNeto<%=i%>'       dir='rtl' readonly value='<%= Util.customFormat( anticipo.getVlrNeto()      ) %>'> </td>
                                                              <td class="bordereporte" align='center' nowrap> <input type='text' style="font size:10" style="width:80"  name='vleComision<%=i%>'   dir='rtl' readonly value='<%= Util.customFormat( anticipo.getVlrComision()  ) %>'> </td>
                                                              <td class="bordereporte" align='center' nowrap> <input type='text' style="font size:10" style="width:80"  name='vlrConsignar<%=i%>'  dir='rtl' readonly value='<%= Util.customFormat( anticipo.getVlrConsignar() ) %>'> </td> 
                                                           <%
                                                                  List bancosTercero =  model.AnticiposPagosTercerosSvc.getListCTATercero();
                                                                  int centinel = 0;
                                                                  if( bancosTercero.size()>0){
                                                                    for(int m=0;m<bancosTercero.size();m++){
                                                                        Hashtable  bk            = (Hashtable)bancosTercero.get(m);
                                                                        String     aliasBK       = (String) bk.get("codigo");
                                                                        String     descripcion   = (String) bk.get("descripcion");
                                                                        String     vec[]         =  descripcion.split(",");
                                                                        if( vec.length == 5){
                                                                                String codeBK  = vec[0];
                                                                                String descBK  = vec[1];
                                                                                String ctaBK   = vec[2];
                                                                                String tipoCTA = vec[3];
                                                                                //System.out.println( "para 1   "+vec[0] );
                                                                                //System.out.println( "para 2   "+anticipo.getBanco_transferencia()+"   descBK  "+descBK );
                                                                                if( anticipo.getBanco_transferencia().equals(codeBK)  ){%>
                                                                                           <td class="bordereporte"  nowrap style="font size:11"><img width='14' title='Asignar bancos' style="cursor:hand" src='<%=BASEURL%>/images/botones/iconos/buscar.gif' onclick= "javascript: var x= window.open( '<%= CONTROLLER%>?estado=Captaciones&accion=Fintra&opcion=LISTABANCOS_TRANSACCION&nit=<%=anticipo.getConductor()%>&anticipo=<%= anticipo.getId()%>&global=N&infoCTA=<%=descBK%>&Ncuenta=<%=Ncuenta%>&secue='+ secuencia<%=i%>.value,'Asignar','top=100,left=100, width=700, height=400, scrollbars=yes, status=yes, resizable=yes  ');  x.focus();" >&nbsp <%= descBK %> </td>

                                                                          <%     Banco = descBK;
                                                                                 centinel++;
                                                                                 m=bancosTercero.size()-1;%>

                                                                                <%
                                                                                           //m=bancosTercero.size()-1;
                                                                                }
                                                                    }
                                                            }
                                                                if (centinel < 1 ){%>
                                                                        <td class="bordereporte"  nowrap style="font size:11"><img width='14' title='Asignar bancos' style="cursor:hand" src='<%=BASEURL%>/images/botones/iconos/buscar.gif' onclick= "javascript: var x= window.open( '<%= CONTROLLER%>?estado=Captaciones&accion=Fintra&opcion=LISTABANCOS_TRANSACCION&nit=<%=anticipo.getConductor()%>&anticipo=<%= anticipo.getId()%>&global=N&infoCTA=<%=""%>&Ncuenta=<%=Ncuenta%>&secue='+ secuencia<%=i%>.value,'Asignar','top=100,left=100, width=700, height=400, scrollbars=yes, status=yes, resizable=yes  ');  x.focus();" >&nbsp </td>
                                                                <%}
                                                   }%>

											      <td class="bordereporte" align='center' nowrap> <input type='text' id='secuencia<%=i%>' name='secuencia<%=i%>'dir='rtl' readonly value='<%=  ( (anticipo.getSecuencia()==0)? "" : ""+anticipo.getSecuencia() ) %>' style="width:50;font size:10;border=0" class='<%= (i%2==0?"filagris":"filaazul") %>'></td>  
                                                       
                                                             
                                                              <!-- Julio Barros 17-11-2006 cambio de pocision-->
                                                              <td class="bordereporte" align='center' nowrap style="font size:11">  
                                                              <% if( vista.equals("TRANSFERIR")   ){
															  		 %>
                                                                     <input type='checkbox' name='anticipo' value='<%= anticipo.getId()%>' id='GROUP<%=i%>' onclick=" cambiarColorMouse(fila<%=i%>); Sell_all_G(formulario,secuencia<%=i%>.value,GROUP<%=i%>.checked,<%=lista.size()%>);"> 
                                                              <%}%>
                                                              </td>  
                                                              
                                                              <!-- Bancos -->
                                                              <td class="bordereporte"  nowrap style="font size:11"><img width='14' title='Asignar bancos' style="cursor:hand" src='<%=BASEURL%>/images/botones/iconos/buscar.gif' onclick= "javascript: var x= window.open( '<%= CONTROLLER%>?estado=Captaciones&accion=Fintra&opcion=LISTABANCOS&nit=<%=anticipo.getPla_owner()%>&anticipo=<%= anticipo.getId()%>&global=N&infoCTA=<%=Banco%>&Ncuenta=<%=Ncuenta%>&secue='+ secuencia<%=i%>.value,'Asignar','top=100,left=100, width=700, height=400, scrollbars=yes, status=yes, resizable=yes  ');  x.focus();" >&nbsp <%= anticipo.getBanco()%> </td>
                                                              <td class="bordereporte" align='center' nowrap style="font size:10"> <%= anticipo.getSucursal()      %> </td>
                                                              <td class="bordereporte" align='center' nowrap style="font size:10"> <%= anticipo.getCuenta()        %> </td>
                                                              <td class="bordereporte" align='center' nowrap style="font size:10"> <%= anticipo.getTipo_cuenta()   %> </td>
                                                              <td class="bordereporte" align='center' nowrap style="font size:10"> <%= anticipo.getNombre_cuenta() %> </td>
                                                              <td class="bordereporte" align='center' nowrap style="font size:10"> <%= anticipo.getNit_cuenta()    %> </td> 
                                                       
                                                              <!-- Aprobador -->
                                                              <td class="bordereporte" align='center' nowrap style="font size:10"> <%= anticipo.getFecha_autorizacion() %> </td>
                                                              <td class="bordereporte" align='center' nowrap style="font size:10"> <%= anticipo.getUser_autorizacion()  %> </td>
															 
														 <%    
														   }%>
                                                         
                                               </tr><%--Conductor:<%=anticipo.getConductor()%>:id:<%= anticipo.getId()%> --%>
                                          <%}%>
                                   <%}%>
           
                                 </table>
                             </td>
                          </tr> 
  
                                  

                   </table>
                 </td>
              </tr>
           </table> 
           
           
          </form>
          
           <br>   
           <% if( lista !=null && lista.size()>0  ){%>   
           <img src="<%=BASEURL%>/images/botones/aceptar.gif"       height="21"  title='Aceptar'    onclick='send(formulario)'                                                                                         onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
           <% } %>
           <img src="<%=BASEURL%>/images/botones/restablecer.gif"   height="21"  title='Refrescar'  onClick="location.href='<%=CONTROLLER%>?estado=Captaciones&accion=Fintra&opcion=REFRESCAR&vista=<%=vista%>'"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
          
           <img src="<%=BASEURL%>/images/botones/salir.gif"         height="21"  title='Salir'      onClick="window.close();"                                                                                          onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
          
           
           

  
     <% if(msj!=null  &&  !msj.equals("") ){%>
                <BR><BR>
                <table border="2" align="center">
                      <tr>
                        <td>
                            <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                  <tr>
                                        <td width="450" align="center" class="mensajes"><%= msj %></td>
                                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                        <td width="58">&nbsp; </td>
                                  </tr>
                             </table>
                        </td>
                      </tr>
                </table>
                
           
     <%}%>
           
           
           

  
</div>
<%=datos[1]%> 

</body>
</html>
