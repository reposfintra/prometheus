<%@page contentType="text/html;"%>
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.*" %>
<%@page import="java.util.*" %>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
       <title>Lista Captaciones Propietario </title>
       
	   <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
		

		
       <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
       
       
       <script>
          function send(theForm,evento){
             theForm.action += evento;
             theForm.submit();
          }
       </script>
       
</head>
<body>


<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte de Captaciones"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>


   <% 
   Propietario  Propietario  = new Propietario();
   
   Vector  lista   = model.CaptacionesFintraSvc.getCaptaciones();
   Propietario     = model.CaptacionesFintraSvc.getPropietario();
    %>
   
   <FORM method='post' action="<%= CONTROLLER %>?estado=Captaciones&accion=Fintra&opcion=" name='formulario'  id='formulario' >
   
    <table width="620" border="2" align="center">
       <tr>
          <td>  
               <table width='100%' align='center' class='tablaInferior'>

                      <tr class="barratitulo">
                        <td colspan='4' >
                           <table cellpadding='0' cellspacing='0' width='100%'>
                             <tr>
                              <td align="left" width='55%' class="subtitulo1">&nbsp;DETALLE</td>
                              <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"  height="20" align="left"></td>
                            </tr>
                           </table>
                        </td>
                     </tr>
                     <tr class="fila">
					 
					 <td align="left" width='55%' class="fila"> <%= Propietario.getS_nombre() %> </td>
					 </tr>
                     
                      <tr class="fila">
                            <td width='100%' colspan='2'>
                                <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
                                
                                    <TR  class="tblTitulo"  >  
                                            <TH nowrap style="font size:11; font weight: bold">FECHA INICIAL        </TH>
                                            <TH nowrap style="font size:11; font weight: bold">SALDO INICIAL        </TH>
											<TH nowrap style="font size:11; font weight: bold">DIAS                 </TH>
											<TH nowrap style="font size:11; font weight: bold">%                    </TH>
                                            <TH nowrap style="font size:11; font weight: bold">NUEVO APORTE         </TH>
                                            <TH nowrap style="font size:11; font weight: bold">REINTEGRO            </TH>
                                            <TH nowrap style="font size:11; font weight: bold">SUBTOTAL             </TH>
                                            <TH nowrap style="font size:11; font weight: bold" width='100'>VALOR INTERESES          </TH> 
                                            <TH nowrap style="font size:11; font weight: bold" width='50'>VALOR RETEFUENTE         </TH>
                                            <TH nowrap style="font size:11; font weight: bold" width='80'>VALORLR RETEICA</TH>
                                            <TH nowrap style="font size:11; font weight: bold" width='80'>CAPITALIZADO      </TH>
                                            <TH nowrap style="font size:11; font weight: bold" width='100'>SALDO FINAL          </TH>
                                            <TH nowrap style="font size:11; font weight: bold" width='80'>FECHA FINAL </TH>
                                      </TR>  
      
                                      
                                      <% double vlr_captacion   = 0; double vlr_descuento  = 0; double vlr_neto = 0; double vlr_comision = 0;
                                         double vlr_consignar  = 0; String fechaAnterior  = ""; double Naporte = 0; double Reinteg = 0;
										 String fechaCorte=""; double TotAporte = 0; double TotReintg = 0; double TotalRTFT = 0;double TotalRTICA = 0;
										 double TotalIntr = 0; double Intereses = 0; double Retefuente= 0; double saldoInicial=0;double Reteica= 0;
										 
										 double IntAcum = 0; double ReteAcumula = 0; boolean print = true;
									     double AporAcum =0; double ReinAcum =0; int dia2 = 0; 
										 
                                          for(int i=0;i<lista.size();i++){
										  	if(print){
												IntAcum = 0; ReteAcumula = 0; 
									     		AporAcum =0; ReinAcum =0;
											}
											print = true;
										    Captacion  captacion   = (Captacion)lista.get(i);
											Captacion  captacion2  = new Captacion();
											saldoInicial      = captacion.getVlr_total_capital() - captacion.getVlr_retefuente() - captacion.getVlr_reteica();
											if(captacion.getTipo_documento().equals("01")||captacion.getTipo_documento().equals("04")){Naporte = captacion.getVlr_capital();} else {Naporte = 0;}
											if(captacion.getTipo_documento().equals("03")){Reinteg = captacion.getVlr_capital();} else {Reinteg = 0;}
											if( i < lista.size()-1 ){
											    captacion2  = (Captacion)lista.get(i+1);
												fechaCorte = com.tsp.util.Util.fechaMenosUnDia(captacion2.getFecha_inicio().substring(0,10)) ;
												Intereses  = captacion2.getVlr_intereses() ;
												Retefuente = captacion2.getVlr_retefuente();
                                                                                                Reteica = captacion2.getVlr_reteica();
											}else { 
												fechaCorte = com.tsp.util.Util.AnoActual()+"-"+com.tsp.util.Util.getFechaActual_String(3)+"-"+com.tsp.util.Util.getFechaActual_String(5);
											}
											int dias = com.tsp.util.Util.diasTranscurridos( captacion.getFecha_inicio().substring(0,10) ,fechaCorte.substring(0,10)) + 1 ;
											if( i == lista.size() -1 ){
												//System.out.println("Intereses antes"+Intereses+"dias"+dias+"dia2"+dia2+"(double)captacion.getInteres()"+(double)captacion.getInteres()+"saldoInicial"+saldoInicial);
												//Intereses  = Math.round(((double)(dias+dia2)/30.0)*((double)captacion.getInteres()/100.0)*(double)saldoInicial);
											Intereses  = Math.round(((double)(dias+dia2)/30.0)*((double)captacion.getInteres()/100.0)*(double)( saldoInicial + Naporte - Reinteg ));
												
												//System.out.println("Intereses despues"+Intereses);
												Retefuente = Math.round((7.0/100.0)*Intereses);
												//Reteica = Math.round((0.5/100.0)*Intereses);
											}
                                                                                        
                                                                                        if(Propietario.getPerfil().equals("INVERSIONISTA US"))
                                                                                           Retefuente  = 0;
                                                                                        if(!Propietario.getPerfil().equals("INVERSIONISTA US")) 
                                                                                         Reteica = 0;
											
											if( captacion.getFecha_inicio().equals(captacion2.getFecha_inicio())){
													print = false;
													IntAcum += Intereses;
													ReteAcumula += Retefuente;
													AporAcum += Naporte;
													ReinAcum += Reinteg;
													dia2 += dias;
											}
											
											TotalRTFT += Retefuente;
											TotalRTICA += Reteica;
                                            TotalIntr += Intereses;
											TotAporte += Naporte ;
											TotReintg += Reinteg ;
											
											if(print){
												Intereses  += IntAcum;
												Retefuente += ReteAcumula;
												Naporte    += AporAcum;
												Reinteg    += ReinAcum;
                                            %>
                                            <TR class='<%= (i%2==0?"filagris":"filaazul") %>' >
                                                     <td class="bordereporte" align='center' nowrap style="font size:10"> <%=  captacion.getFecha_inicio().substring(0,10)%> </td> 
                                                     <td class="bordereporte"                nowrap style="font size:10"> <%=  Util.customFormat( saldoInicial )%> </td>  
													 <td class="bordereporte" align='center' nowrap style="font size:10"> <%=  dias  %> </td>   
													 <td class="bordereporte" align='center' nowrap style="font size:10"> <%=  captacion.getInteres()%> </td>                                                    
                                                     <td class="bordereporte"                nowrap style="font size:10"> <%=  Util.customFormat( Naporte )%></td> 
                                                     <td class="bordereporte" align='center' nowrap style="font size:10"> <%=  Util.customFormat( Reinteg )%> </td>	 
                                                     <td class="bordereporte" align='center' nowrap style="font size:9" > <%=  Util.customFormat( saldoInicial + Naporte - Reinteg )%> </td>                                                     
                                                     <td class="bordereporte" align='right'  nowrap style="font size:10"> <%=  Util.customFormat( Intereses )%> </td>
													 <td class="bordereporte" align='right'  nowrap style="font size:10"> <%=  Util.customFormat( Retefuente )%> </td>
								   					<td class="bordereporte" align='center' nowrap style="font size:10"><span class="bordereporte" style="font size:10"><%=  Util.customFormat( Reteica )%></span></td>    
                                                     <td class="bordereporte" align='center' nowrap style="font size:10"> <%=  Util.customFormat( Intereses - Retefuente -Reteica)%> </td>                                                                
                                                     <td class="bordereporte" align='right'  nowrap style="font size:10"> <%=  Util.customFormat( captacion.getVlr_nuevo_capital() +Intereses - Retefuente-Reteica )%> </td>                                                                
                                                     <td class="bordereporte" align='center' nowrap style="font size:10"> <%=  fechaCorte.substring(0,10)%> </td>  
                                           </TR>
                                          <%}
									  }%>
                                      
                                          <TR class='fila'>
                                                  
                                                     <td class="bordereporte"                nowrap style="font size:11"><b> TOTALES </td>                                                     
                                                     <td class="bordereporte"                nowrap style="font size:11">&nbsp;</td>
                                                     <td class="bordereporte"                nowrap style="font size:11">&nbsp;</td>
                                                     <td class="bordereporte"                nowrap style="font size:11"><b> </td>
                                                     <td class="bordereporte"                nowrap style="font size:11"><b> <%=Util.customFormat(TotAporte)%></td>
                                                     <td class="bordereporte"                nowrap style="font size:11"><b> <%=Util.customFormat(TotReintg)%></td>
													 <td class="bordereporte"                nowrap style="font size:11"><b></td>
                                                     <td class="bordereporte" align='right'  nowrap style="font size:11"><b> <%=Util.customFormat(TotalIntr)%> </td>  
                                                     <td class="bordereporte" align='center' nowrap style="font size:11"><b> <%=Util.customFormat(TotalRTFT)%> </td>
                                                     <td class="bordereporte" align='right'  nowrap style="font size:11"><b><%=Util.customFormat(TotalRTICA)%></b></td>                                                                
                                                     <td class="bordereporte" align='right'  nowrap style="font size:11"><b> <%=Util.customFormat(TotalIntr-TotalRTFT-TotalRTICA)%> </td>                                                                
                                                     <td class="bordereporte" align='right'  nowrap style="font size:11"><b>  </td>
                                                     <td class="bordereporte" align='right'  nowrap style="font size:11"><b>  </td> 
                                                       
                                  </TR>
                                      
                                      
                              </table>
                          </td>
                     </tr>
                     
                     
              </table>
         </td>
      </tr>
   </table>   
   
 </FORM>
 
 
 
 <p> 
     <img src="<%=BASEURL%>/images/botones/exportarExcel.gif"    height="21"  title='Exportar Excel'    onClick="send(formulario,'EXCEL')"    onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">           
     <img src="<%=BASEURL%>/images/botones/salir.gif"            height="21"  title='Salir'             onClick="window.close();"             onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">           
 </p>
            


</div>

</body>
</html>
