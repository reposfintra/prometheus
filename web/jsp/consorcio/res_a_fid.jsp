<%-- 
    Document   : res_a_fid
    Created on : 16/06/2010, 08:41:23 AM
    Author     : maltamiranda
--%>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<link   href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script src="<%= BASEURL%>/js/boton.js"></script>
<script src="<%=BASEURL%>/js/prototype.js"      type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/jscal2.css" />
<link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/border-radius.css" />
<script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
<script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>
<script src="<%=BASEURL%>/js/effects.js"        type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window.js"         type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window_effects.js" type="text/javascript"></script>
<link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css">

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Reporte de Recaudos a la Fiducia</title>
        <script>
            function init(){
                Calendar.setup({
                    inputField : "fecha",
                    trigger    : "calendar-trigger",
                    onSelect   : function() { this.hide() }
                });
            }

            function sendAction(url,opcion,parametro,formx){
                var p="";
                if(parametro=='BUSCAR_FACTURAS'){
                    p="parametro="+parametro;
                    new Ajax.Request(
                    url,
                    {
                        method: 'post',
                        parameters: p,
                        onComplete: showTable
                    });
                }
                else{
                    if(parametro=='GENERAR_REPORTE'){
                        if($('fecha').value=='')
                        {   Dialog.alert("Debe escojer una fecha para el reporte...", {
                                width:250,
                                height:100,
                                windowParameters: {className: "alphacube"}
                            });
                        }
                        else{
                            if (typeof formx != 'undefined') {
                                for (i=0;i<$(formx).length;i++){
                                    if ($(formx).elements[i].type=='checkbox' && $(formx).elements[i].name=='fact' && $(formx).elements[i].checked==true){
                                        p=p+"&fact="+$(formx).elements[i].value;
                                    }
                                }
                            }
                            p="fecha="+$('fecha').value+"&parametro="+parametro+p;
                            openInfoDialog('<b> El proceso ha empezado. <br/>Por favor espere...</b>');
                            new Ajax.Request(
                            url,
                            {
                                method: 'post',
                                parameters: p,
                                onComplete: showTableII
                            });
                        }
                    }
                }
            }

            function showTable (response){
                $('tabla_generica').innerHTML=response.responseText;
            }
            function showTableII (response){
                Dialog.closeInfo();
                Dialog.alert("Proceso Terminado ...", {
                    width:250,
                    height:100,
                    windowParameters: {className: "alphacube"}
                });
                $('tabla_generica').innerHTML=response.responseText;
            }

            function Sell_all_col(nombre){
                theForm=$('formulario');
                //alert("nombre"+nombre+"theform"+theForm);
                for (i=0;i<theForm.length;i++){
                    if (theForm.elements[i].type=='checkbox' && theForm.elements[i].name==nombre){
                        theForm.elements[i].checked=theForm.All.checked;
                    }
                }
            }
            function color_fila(id,color){
                $(id).style.backgroundColor=color;
            }

            function openInfoDialog(mensaje) {
                Dialog.info(mensaje, {
                    width:250,
                    height:100,
                    showProgress: true,
                    windowParameters: {className: "alphacube"}
                });
            }
        </script>
    </head>
    <body onload="init();sendAction('<%=CONTROLLER%>?estado=Facturas&accion=Corficolombiana','sql_totales','BUSCAR_FACTURAS',formulario);">
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=RECAUDOS A CORFICOLOMBIANA"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <br>
            <form action='' method='post' name='formulario' id='formulario' onsubmit='return false;'>
                <center>
                    <div id="tabla_generica" align="center"></div>
                    <br><span style="font-weight: bold;font-size:small">FECHA DE REPORTE</span> <br>
                    <input id="fecha" name="fecha" type="text" style="text-align: center" readonly>
                    <img name="popcal" style="cursor:pointer" id="calendar-trigger" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt="">
                    <br>
                    <br>
                    <img src="<%=BASEURL%>/images/botones/generar.gif"  onMouseOver="botonOver(this);" onClick="sendAction('<%=CONTROLLER%>?estado=Facturas&accion=Corficolombiana','sql_gtxt','GENERAR_REPORTE','formulario');" onMouseOut="botonOut(this);" style="cursor:hand">
                    <tsp:boton value="salir"     onclick="parent.close();"/>
                </center>
            </form>
        </div>
    </body>
</html>
