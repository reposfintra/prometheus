<%--
    Document   : refinanciarCalculo
    Created on : 17/11/2010, 11:22:05 AM
    Author     : Alvaro
--%>

<%@page contentType="text/html" pageEncoding="windows-1258"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>


<html>
<body onload="setFocus();">
<head>
        <title>Refinanciar las facturas de los clientes </title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
	<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL%>/js/consorcio/validar.js"></script>

        <script  language="JavaScript" type="text/javascript" >


            function setFocus(){
                 document.getElementById("numeroCuota").focus();
            }

            function modificarAction(theForm,evento)  {
                accion        = document.getElementById("formulario");
                accion.action = accion.action + "&evento="+evento;
                theForm.submit();
                return;
            }

        </script>




</head>




<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
    FinanciacionPM financiacionPM = model.consorcioService.getFinanciacionPM();
    List listaFacturaPM = financiacionPM.getListaFacturaPM();
%>


<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
    <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modulo de Refinanciacion"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; ">


   <% String  msj       = request.getParameter("msj");
   %>

 <center>


  <%-- La accion es modificada a traves del onclick de los botones, para poder pasar el parametro evento que indicara
       en ConsorcioAccesoAction cual evento se efectuara --%>



  <form action="<%=CONTROLLER%>?estado=Consorcio&accion=Acceso" method='post' name='formulario' id="formulario" >

  <table width="930"  border="2" align="center" cellpadding="0" cellspacing="0">

    <tr>
      <td
      <table width="100%" class="tablaInferior">
        <tr>
          <td colspan="2" >
          <table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
              <tr>
                <td width="40%" class="subtitulo1">&nbsp;IDENTIFICACION DE LA FACTURA</td>
                <td width="60%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
              </tr>


                <table width="930"  border="1"  align="center">

                     <tr class="fila" >
                          <td width="15%"  rowspan="2"  > NOMBRE DEL CLIENTE  </td>
                          <td width="50%"  rowspan="2" class="celdaTexto"> <%= financiacionPM.getNombreCliente() %>  </td>
                          <td width="5%"> NIC </td>
                          <td class="celdaTexto"> <%= financiacionPM.getNic() %> </td>
                     </tr>

                     <tr  class="fila">
                          <td width="5%" > NIT </td>
                          <td class="celdaTexto"> <%= financiacionPM.getNit() %> </td>
                     </tr>

                     <tr class="fila">
                          <td width="15%"> SIMBOLO VARIABLE </td>
                          <td colspan = "3" class="celdaTexto" > <%= financiacionPM.getSimboloVariable() %>  </td>
                     </tr>

                     <tr class="fila">
                          <td width="15%"> MULTISERVICIO </td>
                          <td colspan = "3" class="celdaTexto" > <%= financiacionPM.getMultiservicio() %>  </td>
                     </tr>


                </table>


          </table>
          </td>
        </tr>



      </table>
      </td>
    </tr>
  </table>


  <table width="930"  border="2" align="center" cellpadding="0" cellspacing="0">

    <tr>

      <td>
      <table width="100%" class="tablaInferior">
        <tr>
          <td colspan="2" >
          <table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
              <tr>
                <td width="40%" class="subtitulo1">&nbsp;ESTADO DE LA FACTURA A REFINANCIAR</td>
                <td width="60%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
              </tr>
          </table>
          </td>
        </tr>


        <table width="930"  border="1"  align="center">


              <TR class="tblTitulo1"  align="center">

                      <TH  width="70"  align="center"> FACTURA</TH>
                      <TH  width="60"  align="center"> FECHA</TH>
                      <TH  width="90"  align="center"> FECHA VENCIMIENTO</TH>
                      <TH  width="110" align="center"> VALOR FACTURA</TH>
                      <TH  width="110" align="center"> VALOR SALDO</TH>
                      <TH  width="90"  align="center"> ESTADO</TH>
                      <TH  width="70"  align="center"> DIAS ATRASO</TH>
		      <TH  width="110" align="center"> CAPITAL </TH>
                      <TH  width="110" align="center"> INTERES VENCIDO</TH>
                      <TH  width="110" align="center"> MORA</TH>

              </TR>



              <%
                int i=0;
                Iterator it = listaFacturaPM.iterator();
                FacturaPM  factura = null;

                while (it.hasNext()) {

                  i++;
                  factura = (FacturaPM)it.next();

              %>

                  <tr class='<%= (i%2==0?"filagris1":"filaazul1") %>'>

                    <td width="70"  align="center"> <%= factura.getDocumento() %> </td>
                    <td width="60"  align="center"> <%= factura.getFecha_factura() %> </td>
                    <td width="90"  align="center"> <%= factura.getFecha_vencimiento() %>  </td>
                    <td width="110" align="right">  <%= Util.customFormat(factura.getValor_factura(),2,"##,###,###,##0.00") %>  </td>
                    <td width="110" align="right">  <%= Util.customFormat(factura.getValor_saldo(),2,"##,###,###,##0.00") %>  </td>
                    <td width="90"  align="center"> <%= factura.getEstado() %>  </td>
                    <td width="70"  align="right">  <%= factura.getDiasAtraso() %>  </td>

                    <td width="110" align="right">  <%= Util.customFormat(factura.getCapital(),2,"##,###,###,##0.00") %>  </td>
                    <td width="110" align="right">  <%= Util.customFormat(factura.getInteresVencido(),2,"##,###,###,##0.00") %>  </td>
                    <td width="110" align="right">  <%= Util.customFormat(factura.getMora(),2,"##,###,###,##0.00") %>  </td>

                  </tr>
              <%
                }
              %>


                   <tr class='filaTotal'>

                        <td width="70"  align="center"> TOTAL </td>
                        <td width="60"  align="center" > </td>
                        <td width="90"  align="center">  </td>
                        <td width="110"  align="right">  <%= Util.FormatoMiles(financiacionPM.getValorFacturaTotal() ) %> </td>
                        <td width="110"  align="right">  <%= Util.FormatoMiles(financiacionPM.getValorSaldoTotal() ) %> </td>
                        <td width="90"  align="center">  </td>
                        <td width="70"  align="right">   </td>
                        <td width="110"  align="right">  <%= Util.FormatoMiles(financiacionPM.getValorFinanciarTotal() ) %> </td>
                        <td width="110"  align="right">  <%= Util.FormatoMiles(financiacionPM.getValorInteresesVencidosTotal() ) %> </td>
                        <td width="110"  align="right">  <%= Util.FormatoMiles(financiacionPM.getValorMoraTotal() ) %> </td>

                      </tr>
        </table>
      </table>
      </td>
    </tr>
  </table>

  &nbsp;

  <table width="930"  border="2" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td>
      <table width="100%" class="tablaInferior">
        <tr>
          <td colspan="2" >
              
              <table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
                  <tr>
                    <td width="40%" class="subtitulo1">&nbsp;PARAMETROS PARA LA REFINANCIACION</td>
                    <td width="60%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
                  </tr>
              </table>

          </td>
        </tr>


        <table width="1150"  border="1"  align="center">


              <tr class="tblTitulo1"  align="center">

                      <td  width="40" align="center"> NUMERO DE CUOTAS</td>
                      <td  width="40" align="center"> TASA INTERES MENSUAL</td>
                      <td  width="40" align="center"> TASA INTERES MORA MENSUAL</td>
                      <td  width="150" align="center"> CAPITAL</td>
                      <td  width="120" align="center"> CUOTA INICIAL</td>
                      <td  width="150" align="center"> VALOR A FINANCIAR</td>
                      <td  width="110" align="center"> INTERESES DE FINANCIACION</td>
		      <td  width="130" align="center"> INTERESES VENCIDOS </td>
                      <td  width="110" align="center"> INTERESES DE MORA</td>
                      <td  width="150" align="center"> TOTAL A PAGAR</td>
                      <td  width="110" align="center"> VALOR CUOTA</td>

              </tr>


              <tr class="fila"  align="center">

                  <td align="center"> <input size ="5"  maxlength ="2"  name=numeroCuota  type="text" class="textbox" id= numeroCuota onkeypress="return validar(numeroCuota,2,0)"  value= "<%= financiacionPM.getNumeroCuotas()%>" >  </td>
                  <td align="center"> <input size ="5"  maxlength ="5"  name=tasaInteres  type="text" class="textbox" id= tasaInteres  onkeypress="return validar(tasaInteres,1,3)"  value= "<%= financiacionPM.getTasaInteres() %>" > </td>
                  <td align="center"> <input size ="5"  maxlength ="5"  name=tasaMora     type="text" class="textbox" id= tasaMora     onkeypress="return validar(tasaMora,1,3)"  value= "<%= financiacionPM.getTasaMora() %>" > </td>
                  <td align="right"> <%= Util.FormatoMiles(financiacionPM.getValorCapital()  ) %>  </td>
                  <td align="center"> <input size ="15" maxlength ="13" name=cuotaInicial type="text" class="textbox" id= cuotaInicial onkeypress="return validar(cuotaInicial,8,0)" value= "<%= financiacionPM.getCuotaInicial() %>" >  </td>
                  <td align="right"> <%= Util.FormatoMiles(financiacionPM.getValorFinanciar() ) %>  </td>
                  <td align="right"> <%= Util.FormatoMiles(financiacionPM.getInteresesFinanciacion() ) %>  </td>
                  <td align="right"> <%= Util.FormatoMiles(financiacionPM.getInteresesVencidos() ) %>  </td>
                  <td align="right"> <%= Util.FormatoMiles(financiacionPM.getInteresesMora() ) %>  </td>
                  <td align="right"> <%= Util.FormatoMiles(financiacionPM.getValorDeuda() )%>  </td>
                  <td align="right"> <%= Util.FormatoMiles(financiacionPM.getValorCuota() )%>  </td>

              </tr>
              
        </table>

     </table>
     </td>
   </tr>


  </table>







   <br>
   <img src="<%=BASEURL%>/images/botones/restablecer.gif"    height="21"  title='Reinicializa los estados de las facturas'  onclick="javascript:modificarAction(formulario,'REFINANCIAR_RESTABLECER');"   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
   <img src="<%=BASEURL%>/images/botones/aplicar.gif"    height="21"  title='Calcula una refinanciacion'   onclick="javascript:modificarAction(formulario,'REFINANCIAR_CALCULO');"    onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
   <img src="<%=BASEURL%>/images/botones/aceptar.gif"    height="21"  title='Registra en forma definitiva la refinanciacion'  onclick='send(formulario);'   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
   <img src="<%=BASEURL%>/images/botones/salir.gif"      height="21"  title='Salir'      onClick="window.close();"     onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">



   <!-- MENSAJE -->
   <% if(msj!=null  &&  !msj.equals("") ){%>
        <br><br>
        <table border="2" align="center">
              <tr>
                <td>
                    <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                          <tr>
                                <td width="450" align="center" class="mensajes"><%= msj %></td>
                                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="58">&nbsp; </td>
                          </tr>
                     </table>
                </td>
              </tr>
        </table>

    <%}%>

      </form >

</center>

</div>


<%=datos[1]%>


</body>
</html>