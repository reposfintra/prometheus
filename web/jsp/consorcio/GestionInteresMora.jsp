<%-- 
    Document   : GestionInteresMora
    Created on : 21/06/2010, 03:24:31 PM
    Author     : maltamiranda
--%>

<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<link   href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script src="<%= BASEURL%>/js/boton.js"></script>
<script src="<%=BASEURL%>/js/prototype.js"      type="text/javascript"></script>
<script src="<%=BASEURL%>/js/InteresMora.js"      type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/jscal2.css" />
<link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/border-radius.css" />
<script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
<script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>
<script src="<%=BASEURL%>/js/effects.js"        type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window.js"         type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window_effects.js" type="text/javascript"></script>
<link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/mac_os_x.css"      rel="stylesheet" type="text/css">

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>GESTION INTERESES MORA</title>
<style type="text/css">
    div.fixedHeaderTable {
            position: relative;
    }
    div.fixedHeaderTable tbody {
        height: 200px;
        overflow-y: scroll;
        overflow-x: scroll;
    }
    div.fixedHeaderTable thead td, div.fixedHeaderTable thead th {
            position:relative;
    }

    /* IE7 hacks */
    div.fixedHeaderTable {
        *position: relative;
        *height: 200px;
        *overflow-y: scroll;
        *overflow-x: scroll;
        *padding-right:16px;
    }

    div.fixedHeaderTable thead tr {
        *position: relative;
        _position: absolute;
        *top: expression(this.offsetParent.scrollTop-2);
        *background:none;
        background-color:green;
        color: white;
        border-color: silver;
    }

    div.fixedHeaderTable tbody {
        *height: auto;
        *position:absolute;
        *top:50px;
    }
    /* IE6 hacks */
    div.fixedHeaderTable {
            _width:expression(this.offsetParent.clientWidth-20);
            _overflow: auto;
        _overflow-y: scroll;
        _overflow-x: scroll;
    }
    div.fixedHeaderTable thead tr {
            _position: relative
    }
</style>
    </head>
    <body onload="init();sendAction('<%=CONTROLLER%>?estado=Negocios&accion=Applus','','idclie','')">
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=GESTION INTERESES MORA"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <br><br>
            <center>
                <form id="formulario" name="formulario">
                    <table border="2">
                        <tr>
                            <td>
                                <!-- encabezado -->
                                <table class="tablaInferior" border="0" width="100%">
                                    <tr >
                                        <td width="50%" align="left" class="subtitulo1">&nbsp;Parametros Reporte</td>
                                        <td width="50%" align="left" class="barratitulo"><img src="<%= BASEURL%>/images/titulo.gif" width="32" height="20"  align="left"></td>
                                    </tr>
                                </table>
                                <!-- datos del formulario -->
                                <table class="tablaInferior" border="0">
                                    <tr class="fila">
                                        <td>&nbsp;&nbsp;Nombre Cliente:&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;<input name="nomclie" id="nomclie" type="text" class="textbox" id="campo" style="width:200;"  onChange="sendAction('<%=CONTROLLER%>?estado=Negocios&accion=Applus',document.getElementById('nomclie').value,'idclie','');" size="15">
                                            <br><table><tr><td><div id="cliselect"></div></td><td><img src="<%=BASEURL%>/images/cargando.gif" id="imgworking" style="visibility: hidden"></td></tr></table>
                                        </td>
                                    </tr>
                                    <tr class="fila">
                                        <td>&nbsp;&nbsp;Fecha inicial:&nbsp;&nbsp;</td>
                                        <td>
                                            &nbsp;&nbsp;<input id="fechai" name="fechai" type="text" style="text-align: center" readonly>
                                            <img name="popcal" style="cursor:pointer" id="calendar-triggeri" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt="">
                                        </td>
                                    </tr>
                                    <tr class="fila">
                                        <td>&nbsp;&nbsp;Fecha final&nbsp;&nbsp;</td>
                                        <td>
                                            &nbsp;&nbsp;<input id="fechaf" name="fechaf" type="text" style="text-align: center" readonly>
                                            <img name="popcal" style="cursor:pointer" id="calendar-triggerf" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt="">
                                        </td>
                                    </tr>
                                    <tr class="fila">
                                        <td>&nbsp;&nbsp;Factura:&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;<input type="text" value="" id="factura"></td>
                                    </tr>
                                    <tr class="fila">
                                        <td>&nbsp;&nbsp;Nota:&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;<input type="text" value="" id="nota"></td>
                                    </tr>
                                    <tr class="fila">
                                        <td>&nbsp;&nbsp;Multiservicio:&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;<input type="text" value="" id="ms"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <br><br>
                    <img src="<%=BASEURL%>/images/botones/buscar.gif"  onMouseOver="botonOver(this);" onClick="enviar('<%=CONTROLLER%>?estado=Intereses&accion=Mora','BUSCAR_INTERESES','formulario');" onMouseOut="botonOut(this);" style="cursor:hand">
                    <tsp:boton value="salir" onclick="parent.close();"/>
                    <br><br><br>
                    <div id="tabla_generica"></div>
                </form>
            </center>


        </div>
    </body>
</html>
