<%--     - Description: Permite generar pms para reemplazar a las nms y notas de ajuste --%>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<!-- Para los botones -->
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<script src="<%=BASEURL%>/js/prototype.js"      type="text/javascript"></script>
<link href="<%=BASEURL%>/css/mac_os_x.css"      rel="stylesheet" type="text/css">

<script src="<%=BASEURL%>/js/effects.js"        type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window.js"         type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window_effects.js" type="text/javascript"></script>
<link href="<%=BASEURL%>/css/default.css"       rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>

<html>
    <head>
        <title>Facturas a Corficolombiana</title>
        <link   href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script src="<%= BASEURL%>/js/boton.js"></script>
        <script>
            function seleccionarGrupo(orden,che){
                theForm=formulario;
                //alert("orden:"+orden+"che"+che+"theform"+theForm);
                //if (!	formulario.esfactconformada.checked){
                for (i=0;i<theForm.length;i++){
                    if (theForm.elements[i].type=='checkbox' && theForm.elements[i].title==orden){
                        theForm.elements[i].checked=che;
                    }
                }
                //}
                contar_chulos(theForm);
            }

            function Sell_all_col(nombre){
                theForm=formulario;
                //alert("nombre"+nombre+"theform"+theForm);
                for (i=0;i<theForm.length;i++){
                    if (theForm.elements[i].type=='checkbox' && theForm.elements[i].name==nombre){
                        theForm.elements[i].checked=theForm.All.checked;
                    }
                }
                contar_chulos(theForm);
            }

            function contar_chulos(theForm){
                var chuleados=0;
                var chulos=0;
                for (i=0;i<theForm.length;i++){
                    if (theForm.elements[i].type=='checkbox' && theForm.elements[i].name=='fact'){
                        chulos=chulos+1;
                        //alert("v"+theForm.elements[i].checked);
                        if (theForm.elements[i].checked==true){
                            chuleados=chuleados+1;
                        }
                    }
                }
                $('chulos').innerHTML="filas:&nbsp;"+chulos+"&nbsp;,&nbsp;seleccionadas:&nbsp;"+chuleados+"&nbsp;,&nbsp;pendientes:&nbsp;"+(chulos-chuleados);
            }

        </script>
    </head>
    <body>

        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=FACTURAS A CORFICOLOMBIANA"/>
        </div>

        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

            <% String msj = (request.getParameter("msj") == null) ? "" : request.getParameter("msj");
                        //ArrayList reportes=model.corficolombianaSvc.getReportes();
            %>
            <br><br>
            <form action='' method='post' name='formulario' onsubmit='return false;'>
                <div id="tabla_generica" align="center"></div>
                <p align="center">
                    <img src="<%=BASEURL%>/images/botones/buscar.gif"  onMouseOver="botonOver(this);" onClick="sendAction('<%=CONTROLLER%>?estado=Facturas&accion=Corficolombiana','sql_totales','',formulario);" onMouseOut="botonOut(this);" style="cursor:hand">
                    <img src="<%=BASEURL%>/images/botones/generar.gif"  onMouseOver="botonOver(this);" onClick="sendAction('<%=CONTROLLER%>?estado=Facturas&accion=Corficolombiana','sql_gtxt','GENERAR_FACTURAS',formulario);" onMouseOut="botonOut(this);" style="cursor:hand">
                    <tsp:boton value="salir"     onclick="parent.close();"/>
                </p>
            </form>
            <div id="chulos" name="chulos" align="center" class="letraTitulo"></div>
            <br><br>
            <% if (!msj.equals("")) {%>
            <table border="2" align="center">
                <tr>
                    <td>
                        <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                            <tr>
                                <td width="400" align="center" class="mensajes"><%=msj%></td>
                                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="58">&nbsp; </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <%}%>
            <script>
                
                function sendAction(url,opcion,parametro,formx){
                    //var p = "reporte=" + $('reporte').value + "&periodo=" + $('periodo').value + "&tipo="+opcion+ "&parametro="+parametro;
                    var p="";
                    if(parametro==''){
                        p="parametro="+parametro;
                        openInfoDialog('<b> Ejecutando<br/>Por favor espere...</b>');
                        new Ajax.Request(
                        url,
                        {
                            method: 'post',
                            parameters: p,//formulario.fact
                            onComplete: showTable
                        });
                    }
                    else{
                        if(parametro=='GENERAR_FACTURAS'){
                            if (typeof formx != 'undefined') {
                                for (i=0;i<formx.length;i++){
                                    if (formx.elements[i].type=='checkbox' && formx.elements[i].name=='fact' && formx.elements[i].checked==true){
                                        p=p+"&fact="+formx.elements[i].value;
                                    }
                                }
                            }
                            //alert("p"+p);
                            p="parametro="+parametro+p;
                            openInfoDialog('<b> El proceso ha empezado. <br/>Por favor espere...</b>');
                            new Ajax.Request(
                            url,
                            {
                                method: 'post',
                                parameters: p,
                                onComplete: showReport
                            });
                        }
                    }
                    //contar_chulos(formulario);
                }
                function showReport (response){
                    Dialog.closeInfo();
                    //formulario.refresh();
                    //formulario.All.checked=false;
                    //Sell_all_col(formulario,'fact');
                    $('tabla_generica').innerHTML=response.responseText;
                    Dialog.alert("Proceso Terminado ...", {
                        width:250,
                        height:100,
                        windowParameters: {className: "alphacube"}
                    });
                    
                    $('chulos').innerHTML="";
                    contar_chulos(formulario);//20100526
                }

                function showTable (response){
                    Dialog.closeInfo();
                    $('tabla_generica').innerHTML=response.responseText;
                    
                    $('chulos').innerHTML="";
                    contar_chulos(formulario);//20100526
                }

                function openInfoDialog(mensaje) {
                    Dialog.info(mensaje, {
                        width:250,
                        height:100,
                        showProgress: true,
                        windowParameters: {className: "alphacube"}
                    });
                }

            </script>
            <script>
                function color_fila(id,color){
                    $(id).style.backgroundColor=color;
                }
            </script>
        </div>
    </body>
</html>

