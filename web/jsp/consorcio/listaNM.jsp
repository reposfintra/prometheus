<%-- 
    Document   : listaNM
    Created on : 07-mar-2013, 19:25:36
    Author     : Alvaro
--%>



<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<%@page import="com.tsp.operation.model.beans.*"%>



<%

    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL, request.getRequestURI(), path);
    
    String retorno = (request.getParameter("retorno") != null)
                        ? request.getParameter("retorno")
                        : "normal";
    if(retorno.equals("normal") || retorno.equals("PMF") || retorno.equals("RMF")){
    model.tablaGenService.buscarRegistrosOrdenadosByDesc("FIDUCIA");
    } else {
        model.tablaGenService.buscarRegistroByDesc("FIDUCIA","FIDFIV");
    }
    LinkedList listFiducia = model.tablaGenService.obtenerTablas();
          
    
    List listaFacturaNM = model.fiduciaService.getListaFacturaNM();
    
    int listaNM = listaFacturaNM.size();

    String  msj           = request.getParameter("msj");
    String aceptarDisable = request.getParameter("aceptarDisable");
    int  opcion           = Integer.parseInt(request.getParameter("fiducia"));

    String valorFiducia   = request.getParameter("valor_fiducia");
    
    double  valor_total   = 0.00 ;
    double valorFactura   = 0.00 ;
%>


    
<html>
    <head>
        
        <title>Lista de facturas para convertir en PM</title>
            
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
        <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>
            
        <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
         
   
        
        
        
        
        
        
    </head>
    
    
    <body onLoad ="cambio_seleccion_fiducia()" >
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=Lista de facturas para convertir en PM"/>
        </div>
            
            
        <div id="capaSiguiente" style="position:absolute; width:1420; height:87%; z-index:0; left: 0px; top: 100px; ">
            
            
            <form name='formulario' method='post' id="formulario" action="<%=CONTROLLER %>?estado=Fiducia&accion=Acceso&evento=GENERAR_FACTURAR_PM&retorno=<%=retorno%>" >
                <p>&nbsp;</p>
                    
                <table width="1125"  border="1"  align="center">
                    
                    
                    <TR class="tblTitulo1"  align="center">
                        
                        <TH  width="1" align="center">  </TH>
                        <TH  width="15" align="center"> SELECCION</TH>
                        <TH  width="60" align="center"> FIDUCIA</TH>
                        <TH  width="100" align="center"> NUM_OS</TH>
                        <TH  width="60" align="center"> FACTURA NM</TH>
                        <TH  width="60" align="center"> NUMERO CUOTAS</TH>
                        <TH  width="60" align="center"> CUOTAS SELECCIONADAS</TH>
                        
                        <TH  width="70" align="center"> NIT </TH>                        
                        <TH  width="60" align="center"> CLIENTE</TH>
                        <TH  width="350" align="center"> NOMBRE CLIENTE</TH>
                        <TH  width="100" align="center"> FECHA FACTURA</TH>
                        <TH  width="90" align="center"> VALOR FACTURAS</TH>

                    </TR>
    

                    <%
                      int i=0; int k=0; 
                      Iterator it = listaFacturaNM.iterator();
                      NM  nm = null;
                      String factura = "";
                      while (it.hasNext()) {

                        i++;
                        nm = (NM)it.next();
                        if(!factura.equalsIgnoreCase(nm.getDocumento_maestro())) {
                            k++;    valorFactura = 0;
                            factura = nm.getDocumento_maestro();
                    %>
                        <tr class='tblTitulo1' name='tab<%=k%>'>
                          <td width="10"  align="center" nowrap onclick="desplegar('sub<%=k%>')" style="cursor:pointer"> + </td>
                          <td width="15" align="center" nowrap >   
                               <input vspace="10" name="ckEstadoB" type="checkbox" id="ckEstadoB<%= k %>"  
                                      onchange="seleccionar('<%=k%>', 'tab'); " >
                          </td>                            

                          <td width="60" align="center" nowrap >  </td>
                          
                          <td width="100"  align="center"> <%= nm.getFom() %> </td>
                          <td width="60"  align="center"> <%= nm.getDocumento_maestro() %>  </td>
                          <td width="60"  align="center" id='cuotas<%=k%>'> 0 </td>
                          <td width="60"  align="center" id='cuotasSel<%=k%>'> 0 </td>
                          <td width="70"  align="center"> <%= nm.getNit() %>  </td>
                          <td width="60"  align="center"> <%= nm.getCodcli() %> </td>
                          <td width="350"  align="left"> <%= nm.getNomcli() %> </td>
                          <td width="100"  align="center"> <%= nm.getFecha_factura() %> </td>
                          <td width="90" align="right" id="valorSaldoItemB<%= k %>"  > 0.00 </td>
                        
                        </tr>
                    <% } %>
                        <tr class='<%= (i%2==0?"filagris1":"filaazul1") %>' name='sub<%=k%>' style='display:none'>
                          <td width="10"  align="center" nowrap ></td>
                          <td width="15" align="center" nowrap >   
                              <input vspace="10" name="ckEstado" type="checkbox" id="ckEstado<%= i %>" 
                                     value='<%= (nm.getDocumento_maestro()+"_"+nm.getCuota_primera())%> <%=-i%>'
                                     onchange="valorTotalTraslado(<%=i%>); seleccionar('<%=k%>', 'sub')">
                          </td>                            
                            
                          <td width="60" align="center" nowrap >   
                               <select id='id_fiducia<%= i %>' name='id_fiducia'   size='1' >
                            <option value = 'NaO' style="display:none"></option>
                            <%    for(int j=0; j< listFiducia.size();j++){
                                    TablaGen fiducia = (TablaGen) listFiducia.get(j);
                            %>  
                                    <option value = '<%= fiducia.getTable_code() %>' >  <%= fiducia.getDescripcion() %> </option>
                            <%
                                  }                                   
                            %> 
                                   
                                   
                               </select>
                          </td>                              
                          
                          
                          <td width="100"  align="center"> <%= nm.getFom() %> </td>
                          <td width="60"  align="center"> <%= nm.getDocumento_maestro() %>  </td>
                          <td width="60"  align="center"> <%= nm.getCuota_primera() %>  </td>
                          <td width="60"  align="center"> -- </td-->
                          <td width="70"  align="center"> <%= nm.getNit() %>  </td>
                          <td width="60"  align="center"> <%= nm.getCodcli() %> </td>
                          <td width="350"  align="left"> <%= nm.getNomcli() %> </td>
                          <td width="100"  align="center"> <%= nm.getFecha_factura() %> </td>
                          <td width="90" align="right" id="valorSaldoItem<%= i %>"  > <%= Util.FormatoMiles(nm.getValor_saldo()) %> </td>
                        <script>
                            
                            var elemento  = document.getElementById('cuotas<%=k%>');
                            elemento      = elemento.firstChild.nodeValue;
                            var valorItem = parseInt(elemento.replace(/\,/g,""));
                            document.getElementById("cuotas<%= k %>").innerHTML = (parseInt(valorItem+1));
                        </script>
                        </tr>
                     <%
                          }
                     %>     



                        <tr class='filaTotal'>
                            <td colspan="11" align="right">TOTAL</td>                            
                            <td width="90"   align="right" name="valorSaldoTotal" id="valorSaldoTotal">  <%= Util.FormatoMiles(valor_total) %> </td>
                        </tr>                     

                     
                     
                     
                </table>
                    
            
                     
                     
            <p>&nbsp;</p>

            <table align="center">
              <tr>
                <td colspan="2" nowrap align="center">

                    <img src="<%=BASEURL%>/images/botones/aceptar.gif"    height="21"  title='Genera las facturas PM'  onClick='ValidarSeleccion();'   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
                    <img src="<%=BASEURL%>/images/botones/salir.gif"      height="21"  title='Salir'                   onClick='window.close();'     onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
              </tr>
            </table>                     


                     
                     


                   <!-- MENSAJE -->
                <% if(msj!=null  &&  !msj.equals("") ){%>
                     <br><br>
                     <table border="2" align="center">
                           <tr>
                             <td>
                                 <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                       <tr>
                                             <td width="450" align="center" class="mensajes"><%= msj %></td>
                                             <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                             <td width="58">&nbsp; </td>
                                       </tr>
                                  </table>
                             </td>
                           </tr>
                     </table>

                 <%}%>



    
	<script>

           

           function ValidarSeleccion() {
          
                var item = 0;
                finItem = false;
                marcado = false;

                while (finItem == false){
                   item++;
                   if (document.getElementById("ckEstado"+item) != null){
                       if (document.getElementById("ckEstado"+item).checked == true){
                           marcado = true;
                           finItem = true;
                       }
                   }
                   else
                       finItem = true;
                }


                if (marcado) {
                    formulario.submit();
                }
                else
                    alert("Para trasladar a PM se requiere marcar alguno de los items");
            }           
           
           
           function cambio_seleccion_fiducia() {
               
               var indice_opcion = <%= opcion%>;
               
               var numeroFacturasNM = <%= listaNM %>;
               
               for (var i=1;i<= numeroFacturasNM ;i++) {
                    document.forms['formulario'].elements['id_fiducia'+i].options[indice_opcion].selected = true;
               }   
           } 
           
            function valorTotalTraslado(item)  {
                
                

                
                var elemento  = document.getElementById("valorSaldoItem"+item);
                elemento      = elemento.firstChild.nodeValue;
                var valorItem = parseFloat(elemento.replace(/\,/g,""));

                elemento      = document.getElementById("valorSaldoTotal");
                elemento      = elemento.firstChild.nodeValue;
                var valorTotal= parseFloat(elemento.replace(/\,/g,""));

                var estado = document.getElementById("ckEstado"+item).checked;
                if (estado)
                   valorTotal = valorTotal + valorItem;
                else
                   valorTotal = valorTotal - valorItem;

                


                document.getElementById("valorSaldoTotal").innerHTML = Formatear(valorTotal,2,true);
            }           
           
           
            function Formatear(numero,decimales,miles)
            {
              var numero = new oNumero(numero);
              return numero.formato(decimales, miles);
            }           
           

            //Objeto oNumero
            function oNumero(numero) {
                
                //Propiedades
                
                this.valor = numero || 0
                this.dec = -1;
                
                //M�todos
                
                this.formato = numFormat;
                this.ponValor = ponValor;

                //Definici�n de los m�todos

                function ponValor(cad) {
                    if (cad =='-' || cad=='+') return
                    if (cad.length ==0) return
                    if (cad.indexOf('.') >=0)
                        this.valor = parseFloat(cad);
                    else
                        this.valor = parseInt(cad);
                }

                function numFormat(dec, miles) {

                    var num = this.valor, signo=3, expr;
                    var cad = ""+this.valor;
                    var ceros = "", pos, pdec, i;
                    for (i=0; i < dec; i++)
                    ceros += '0';
                    pos = cad.indexOf('.')
                    if (pos < 0)
                        cad = cad+"."+ceros;
                    else
                        {
                        pdec = cad.length - pos -1;
                        if (pdec <= dec)
                            {
                            for (i=0; i< (dec-pdec); i++)
                                cad += '0';
                            }
                        else
                            {
                            num = num*Math.pow(10, dec);
                            num = Math.round(num);
                            num = num/Math.pow(10, dec);
                            cad = new String(num);
                            }
                        }
                    pos = cad.indexOf('.')
                    if (pos < 0) pos = cad.lentgh
                    if (cad.substr(0,1)=='-' || cad.substr(0,1) == '+')
                           signo = 4;
                    if (miles && pos > signo)
                        do{
                            expr = /([+-]?\d)(\d{3}[\.\,]\d*)/
                            cad.match(expr)
                            cad=cad.replace(expr, RegExp.$1+','+RegExp.$2)
                            }
                    while (cad.indexOf(',') > signo)
                        if (dec<0) cad = cad.replace(/\./,'')
                            return cad;
                }
            }//Fin del objeto oNumero:
           
           function desplegar(hijos) {
               lHijos = document.getElementsByName(hijos);
               for (x = 0; x < lHijos.length; x++) {
                   lHijos[x].style.display = (lHijos[x].style.display === 'none')?'':'none';
               }
           }
           
           function seleccionar(item, activa){ 
               lHijos = document.getElementsByName('sub'+item);
               padre = document.getElementsByName('tab'+item);
           
               elemento      = document.getElementById("valorSaldoTotal");
               elemento      = elemento.firstChild.nodeValue;
               var valorTotal= parseFloat(elemento.replace(/\,/g,""));
               var valorItem = 0;
               var valorFactura = 0;
               var cuotasFactura = 0;
               todos = (activa === 'sub')
                     ? true 
                     : padre[0].cells[1].children["ckEstadoB"].checked;
               for(x = 0; x < lHijos.length; x++) {
                   valorItem = parseFloat(lHijos[x].cells[11].innerHTML.replace(/\,/g,""));
                   if(activa === 'sub') {
                       if(lHijos[x].cells[1].children["ckEstado"].checked === false) {
                            todos = false;  
                       }
                   } else {
                       if (lHijos[x].cells[1].children["ckEstado"].checked !== todos) {
                            lHijos[x].cells[1].children["ckEstado"].checked = todos;
                            
                            if(todos) {
                                valorTotal += valorItem;
                            } else {
                                valorTotal -= valorItem;
                            }
                            document.getElementById("valorSaldoTotal").innerHTML = Formatear(valorTotal,2,true);
                       }
                   }
                   if(lHijos[x].cells[1].children["ckEstado"].checked === true) {
                       valorFactura += valorItem;
                       cuotasFactura ++;
                   }
               }
               document.getElementById('cuotasSel'+item).innerHTML = cuotasFactura;
               document.getElementById('valorSaldoItemB'+item).innerHTML = Formatear(valorFactura,2,true);
               padre[0].cells[1].children["ckEstadoB"].checked = todos;
           }
	</script>         
    
    
    
                     
    </body>
</html>
