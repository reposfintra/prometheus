<%-- 
    Document   : recaudoElectrificadora
    Created on : 11/07/2010, 12:28:46 PM
    Author     : Alvaro
--%>


<%-- Declaracion de librerias--%>



<%@page contentType="text/html" pageEncoding="windows-1258"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">


<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>


<html>
<head>
        <title>Procesamiento de los Recaudos de Electrificadora </title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
	<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>

        <script  >
	   function send(theForm){
	              theForm.submit();
	   }
	</script>

</head>

<body onLoad="redimensionar();" onResize="redimensionar();">


<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    %>

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modulo de Recaudos"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; ">


   <% String  msj           = request.getParameter("msj");
      String aceptarDisable = request.getParameter("aceptarDisable");

   %>

 <center>

   <form action="<%=CONTROLLER%>?estado=Consorcio&accion=Acceso&evento=LEER_RECAUDO_EXCEL"    ENCTYPE="multipart/form-data"  METHOD='POST' name='formulario' >


   <table width="800" border="2" align="center">
       <tr>
          <td>
               <table width='100%' align='center' class='tablaInferior'>

                  <tr class="barratitulo">
                    <td colspan='2' >
                       <table cellpadding='0' cellspacing='0' width='100%'>
                         <tr>
                          <td align="left" width='65%' class="subtitulo1">&nbsp;PROCESAR RECAUDOS</td>
                          <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"  height="20" align="left"><%=datos[0]%></td>
                        </tr>
                       </table>
                    </td>
                 </tr>


                 <tr  class="fila">
                       <td colspan='2' >
                          <table class='tablaInferior' width='100%'>


                               <tr  class="fila">
                                   <td align="center" ><BR><BR>
                                       Especifique el nombre del archivo de recaudos <INPUT TYPE=FILE NAME=file> <BR><BR>
                                   </td>
                               </tr>


                               <tr  class="fila">
                                   <td align="center" >
                                       Presione ACEPTAR para iniciar la lectura del archivo de Recaudos <BR><BR>
                                   </td>
                               </tr>
                          </table>
                       </td>
                 </tr>

           </table>
         </td>
      </tr>
   </table>

   <br>















   <% if ( (aceptarDisable == null) || (aceptarDisable.equals(""))  ) { %>
        <img src="<%=BASEURL%>/images/botones/aceptar.gif"    height="21"  title='Consultar'  onclick='send(formulario);'   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
   <% }
      else { %>
        <img src="<%=BASEURL%>/images/botones/aceptarDisable.gif"    height="21"  style="cursor:hand">
   <% } %>

   <img src="<%=BASEURL%>/images/botones/salir.gif"      height="21"  title='Salir'      onClick="window.close();"     onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">





   <!-- MENSAJE -->
   <% if(msj!=null  &&  !msj.equals("") ){%>
        <br><br>
        <table border="2" align="center">
              <tr>
                <td>
                    <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                          <tr>
                                <td width="450" align="center" class="mensajes"><%= msj %></td>
                                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="58">&nbsp; </td>
                          </tr>
                     </table>
                </td>
              </tr>
        </table>

    <%}%>

      </form >

</center>

</div>


<%=datos[1]%>


</body>
</html>
