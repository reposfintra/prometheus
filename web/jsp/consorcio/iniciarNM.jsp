<%--
    Document   : iniciarNM
    Created on : 5/03/2013, 11:06:55 PM
    Author     : Alvaro
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.util.*"%>

<%@page import="com.tsp.operation.model.beans.* "%>


<%@include file="/WEB-INF/InitModel.jsp"%>


<html>
<head>
        <title>Presentacion para iniciar proceso de seleccion de NM a PM </title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
	<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>

	<script>
	   function send(formulario){
               
             var indice = document.formulario.id_fiducia_inicial.selectedIndex;                
             var  valor = document.formulario.id_fiducia_inicial.options[indice].value; 
             var filtro = document.formulario.filtro.options[document.formulario.filtro.selectedIndex].value;

             window.close();
             window.open('<%= CONTROLLER %>?estado=Fiducia&accion=Acceso&evento=CREAR_LISTA_'+filtro+'&id_fiducia_inicial='+indice+'&valor_fiducia_inicial='+valor );
	   }
          
          function changeSelect(seleccionado) {
              var sf = document.formulario.id_fiducia_inicial;
              sf.selectedIndex = 0;
             if(seleccionado === 'NM') {
                 sf.options[1].style.display = 'block';
                 sf.options[2].style.display = 'block';
                 sf.options[3].style.display = 'block';
                 sf.options[4].style.display = 'block';
                 sf.options[5].style.display = 'block';
             } else if (seleccionado === 'PM' || seleccionado === 'RM') {
                 sf.options[1].style.display = 'block';
                 sf.options[2].style.display = 'block';
                 sf.options[3].style.display = 'block';
                 sf.options[4].style.display = 'block';
                 sf.options[5].style.display = 'none';
             } else if (seleccionado === 'PMF') {
                 sf.options[1].style.display = 'block';
                 sf.options[2].style.display = 'block';
                 sf.options[3].style.display = 'block';
                 sf.options[4].style.display = 'block';
                 sf.options[5].style.display = 'block';
             } else {
                 return;
             }
          }
	</script>

</head>

<body onLoad="redimensionar();" onResize="redimensionar();">


<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    %>

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
    
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modulo de Fiducia"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; ">


   <% String  msj           = request.getParameter("msj");
      String aceptarDisable = request.getParameter("aceptarDisable");
      
      model.tablaGenService.buscarRegistrosOrdenadosByDesc("FIDUCIA");
      LinkedList listFiducia = model.tablaGenService.obtenerTablas();
      
   %>

 <center>

   <form action="<%=CONTROLLER%>?estado=Fiducia&accion=Acceso&evento=CREAR_LISTA_NM" method='post' name='formulario' >


   <table width="450" border="2" align="center">
       <tr>
          <td>
               <table width='100%' align='center' class='tablaInferior'>

                  <tr class="barratitulo">
                    <td colspan='2' >
                       <table cellpadding='0' cellspacing='0' width='100%'>
                         <tr>
                          <td align="left" width='65%' class="subtitulo1">&nbsp;INICIAR SELECCION DE NM PARA TRASLADAR A PM</td>
                          <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"  height="20" align="left"><%=datos[0]%></td>
                        </tr>
                       </table>
                    </td>
                 </tr>

                 <tr class="fila">
                     <td align='center' nowrap >
                         SELECCIONE 
                     </td>

                     <td align='center' nowrap>
                         <select name='filtro' id='filtro' onchange="changeSelect(this.value)" >
                             <option value='NM'>NM a PM</option>
                             <option value='PM'>PM a Fintra</option>
                             <option value='RM'>RM a Fintra</option>
                             <option value='PMF'>PM Fintra a Fiducia</option>
                         </select>
                     </td>
                 </tr>
                 
                 <tr  class="fila">                 
                 
                 
                          <td width="200" align="center" nowrap id="texto_fil" >   
                               SELECCIONE LA FIDUCIA PARA TRABAJAR
                          </td>                       
                     
                          <td width="60" align="center" nowrap > 
                               <select name="id_fiducia_inicial" id="id_fiducia_inicial" size="1" >
                                  <option value = 'NaO' >   </option> 
                            <%    for(int i=0; i< listFiducia.size();i++){
                                    TablaGen fiducia = (TablaGen) listFiducia.get(i);
                            %>  
                                    <option value = '<%= fiducia.getTable_code() %>' >  <%= fiducia.getDescripcion() %> </option>
                            <%
                                  }                                   
                            %> 

                               </select>                     
                          </td> 
                 </tr>                 
                 
                 
                 <tr  class="fila">
                       <td colspan='2' >
                          <table class='tablaInferior' width='100%'>
                               <tr  class="fila">
                                   <td align="center" > Presione ACEPTAR para iniciar la seleccion de las NM </td>
                               </tr>
                          </table>
                       </td>
                 </tr>

           </table>
         </td>
      </tr>
   </table>

   <br>



   <% if ( (aceptarDisable == null) || (aceptarDisable.equals(""))  ) { %>
        <img src="<%=BASEURL%>/images/botones/aceptar.gif"    height="21"  title='Presentacion de las NM'  onclick='send(formulario);'   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" >
   <% }
      else { %>
        <img src="<%=BASEURL%>/images/botones/aceptarDisable.gif"    height="21"  style="cursor:hand">
   <% } %>

   <img src="<%=BASEURL%>/images/botones/salir.gif"      height="21"  title='Salir'      onClick="window.close();"     onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">


   <!-- MENSAJE -->
   <% if(msj!=null  &&  !msj.equals("") ){%>
        <br><br>
        <table border="2" align="center">
              <tr>
                <td>
                    <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                          <tr>
                                <td width="450" align="center" class="mensajes"><%= msj %></td>
                                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="58">&nbsp; </td>
                          </tr>
                     </table>
                </td>
              </tr>
        </table>

    <%}%>



      </form >

</center>

</div>


<%=datos[1]%>


</body>
</html>

