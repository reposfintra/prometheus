<%--
    Document   : prefacturaContratista
    Created on : 04/25/2010, 07:08:38 AM
    Author     : Alvaro
--%>


<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>


<html>
<head>
        <title>Prefactura contratista </title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
	<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>

	<script>
	   function send(theForm){

	              theForm.submit();
	   }
	</script>

</head>

<body onLoad="redimensionar();" onResize="redimensionar();">


<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    %>

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modulo de Prefacturacion"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; ">


   <% String  msj       = request.getParameter("msj");

   System.out.println("mensaje : " + msj);

   %>

 <center>

   <form action="<%=CONTROLLEROPAV%>?estado=Consorcio&accion=Acceso&evento=ACCIONES_SIN_PREFACTURAR_CONTRATISTA" method='post' name='formulario' >


   <table width="500" border="2" align="center">
       <tr>
          <td>
               <table width='100%' align='center' class='tablaInferior'>

                  <tr class="barratitulo">
                    <td colspan='2' >
                       <table cellpadding='0' cellspacing='0' width='100%'>
                         <tr>
                          <td align="left" width='65%' class="subtitulo1">&nbsp;PREFACTURA CONTRATISTA</td>
                          <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"  height="20" align="left"><%=datos[0]%></td>
                        </tr>
                       </table>
                    </td>
                 </tr>


                 <tr  class="fila">
                       <td colspan='2' >
                          <table class='tablaInferior' width='100%'>
                               <tr  class="fila">
                                   <td>A�o del impuesto</td>
                                   <td><select id="anio_impuesto" name="anio_impuesto"></select></td>
                                   <td align="center" > Presione ACEPTAR para iniciar la prefacturacion </td>                                 
                               </tr>
                          </table>
                       </td>
                 </tr>

           </table>
         </td>
      </tr>
   </table>

   <br>
   <img src="<%=BASEURL%>/images/botones/aceptar.gif"    height="21"  title='Consultar'  onclick='send(formulario);'   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
   <img src="<%=BASEURL%>/images/botones/salir.gif"      height="21"  title='Salir'      onClick="window.close();"     onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">






   <!-- MENSAJE -->
   <% if(msj!=null  &&  !msj.equals("") ){%>
        <br><br>
        <table border="2" align="center">
              <tr>
                <td>
                    <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                          <tr>
                                <td width="450" align="center" class="mensajes"><%= msj %></td>
                                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="58">&nbsp; </td>
                          </tr>
                     </table>
                </td>
              </tr>
        </table>

    <%}%>

      </form >

</center>

</div>
    <script>
    $(document).ready(function () {
//        var anio = 2015;
//        for(var i = 0; i < 5; i++){
//            anio = anio+1;
//           $('#anio_impuesto').append('<option value="' + anio + '">' + anio + '</option>');
//        }
        anio_impuesto();
           
      
    });
    
      function anio_impuesto() {
            $.ajax({
                type: 'POST',
                url: "./controller?estado=Fintra&accion=Soporte",
                dataType: 'json',
                async: false,
                data: {
                    opcion: 109
                },
                success: function (json) {
                    if (json.error) {
                        // mensajesDelSistema(json.error, '250', '180');
                        return;
                    }
                    try {
                    var fecha = new Date();
                    var anio = fecha.getFullYear();
                    for (var datos in json) {
                        if(json[datos] == anio){
                            $('#anio_impuesto').append('<option selected value="' + datos + '" >' + json[datos] + '</option>');
                        }else{
                            $('#anio_impuesto').append('<option value="' + datos + '">' + json[datos] + '</option>');  
                        }
                        
                            
                    }
                    } catch (exception) {
                       //     mensajesDelSistema('error : ' + datos + '>' + json[datos][datos], '250', '180');
                    }

                }, error: function (xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
        } 
    </script>


<%=datos[1]%>


</body>
</html>