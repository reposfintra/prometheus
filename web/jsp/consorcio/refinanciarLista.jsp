<%--
    Document   : refinanciarLista
    Created on : 17/11/2010, 11:22:05 AM
    Author     : Alvaro
--%>

<%@page contentType="text/html" pageEncoding="windows-1258"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.opav.model.beans.*"%>



<html>
    <body onload="setFocus();">
    <head>
        <title>Seleccion clientes y facturas a refinanciar  </title>
        <link href="<%= BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">


        <link href="<%= BASEURL%>/css/estilostsp.css" rel='stylesheet'>
        <link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="/css/estilostsp.css" rel="stylesheet" type="text/css">        





        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>

        <script  language="JavaScript" type="text/javascript" >


            function setFocus(){
                document.getElementById("idCliente").focus();
                return;
            }

            function crearAction(theForm,evento)  {
                accion        = document.getElementById("formulario");
                accion.action = accion.action + "&evento="+evento;
                
                var codigoCliente = document.getElementById("codigoCliente").innerHTML;

                accion.action = accion.action + "&codigoCliente="+codigoCliente;

                theForm.submit();
                return;
            }


            function seleccionOpcion(theForm,evento) {

                opcion1="";
                var elementos = document.getElementsByName("chkSeleccionarFactura");

                for ( var i=0; i< elementos.length; ++i)
                {
                    if (elementos[i].checked)
                    {
                        opcion1=opcion1 + "&chkSeleccion=on";
                    }
                    else {
                        opcion1=opcion1 + "&chkSeleccion=off";
                    }
                }

                opcion2 = "";
                for ( var i=0; i< elementos.length; ++i)
                {
                    if (elementos[i].checked)
                    {
                        opcion2=opcion2 + "&chkEliminacion=on";
                    }
                    else {
                        opcion2=opcion2 + "&chkEliminacion=off";
                    }
                }
                accion        = document.getElementById("formulario");
                accion.action = accion.action + "&evento="+evento + opcion1 + opcion2;

                theForm.submit();
                return

            }


        </script>



    </head>



    <%  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
        String datos[] = model.menuService.getContenidoMenu(BASEURL, request.getRequestURI(), path);
        
        
        List listaFactura = modelopav.consorcioService.getFacturaCliente();
        String hoy = Utility.getHoy("-");
    %>

    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modulo de Refinanciacion"/>
    </div>

    <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; ">


        <% String msj = request.getParameter("msj");
            String idCliente = request.getParameter("idCliente");
            String nombreCliente = request.getParameter("nombreCliente");

            String numeroCuota = (request.getParameter("numeroCuota") != null) ? request.getParameter("numeroCuota") : "";
            String tasaInteres = (request.getParameter("interes") != null) ? request.getParameter("interes") : "";
            String tasaMora = (request.getParameter("mora") != null) ? request.getParameter("mora") : "";

            
            String cuotaInicial = (request.getParameter("cuotaInicial") != null) ? request.getParameter("cuotaInicial") : "";
            String fechaFinanciacion = (request.getParameter("fechaFinanciacion") != null) ? request.getParameter("fechaFinanciacion") : hoy;
            String fechaVencimiento = (request.getParameter("fechaVencimiento") != null) ? request.getParameter("fechaVencimiento") : hoy;
            
            String mostrarLiquidacion = (request.getParameter("mostrarLiquidacion") != null) ? request.getParameter("mostrarLiquidacion") : "NO";
            String capitalPorCuota = (request.getParameter("capitalPorCuota") != null) ? request.getParameter("capitalPorCuota") : "0.00";
            String interesPorCuota = (request.getParameter("interesPorCuota") != null) ? request.getParameter("interesPorCuota") : "0.00";
            String vlrInteresesPendientesPorCuota = (request.getParameter("vlrInteresesPendientesPorCuota") != null) ? request.getParameter("vlrInteresesPendientesPorCuota") : "0.00";
            String capitalPorCuotaUltimaFactura = (request.getParameter("capitalPorCuotaUltimaFactura") != null) ? request.getParameter("capitalPorCuotaUltimaFactura") : "0.00";
            String interesPorCuotaUltimaFactura = (request.getParameter("interesPorCuotaUltimaFactura") != null) ? request.getParameter("interesPorCuotaUltimaFactura") : "0.00";
            String vlrInteresesPendientesPorCuotaUltimaFactura = (request.getParameter("vlrInteresesPendientesPorCuotaUltimaFactura") != null) ? request.getParameter("vlrInteresesPendientesPorCuotaUltimaFactura") : "0.00";
            String vlrTotalMora = (request.getParameter("vlrTotalMora") != null) ? request.getParameter("vlrTotalMora") : "0.00";
            String valorFinanciar = (request.getParameter("valorFinanciar") != null) ? request.getParameter("valorFinanciar") : "0.00";
            Double dValorFinanciar = Double.parseDouble(valorFinanciar);
            
            FinanciacionPM financiacionPM =  modelopav.consorcioService.getFinanciacionPM();
            
            financiacionPM.getNumeroCuotas();

            
            if(tasaInteres.isEmpty()){
                tasaInteres = Double.toString(financiacionPM.getTasaMaxima() );
            }
            
            if(tasaMora.isEmpty()){
                tasaMora = Double.toString(financiacionPM.getTasaMaxima() );
            }
            

        %>

        <center>

            <form action="<%=CONTROLLEROPAV%>?estado=Consorcio&accion=Acceso" method='post' name='formulario' id="formulario">


                <input type ="hidden" name="idCliente" value= <%= idCliente%> >


                <table width="800"  border="2" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td><table width="100%" class="tablaInferior">
                                <tr>
                                    <td colspan="4" ><table width="100%"  border="1" cellpadding="0" cellspacing="1" class="barratitulo">
                                            <tr>
                                                <td width="45%" class="subtitulo1">&nbsp;CONDICIONES PARA REFINANCIAR FACTURAS</td>
                                                <td width="55%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
                                            </tr>
                                        </table></td>
                                </tr>

                                <tr class="fila">
                                    <td align="center">Codigo del cliente</td>
                                    <td align="left" id="codigoCliente" name="codigoCliente"> <%= financiacionPM.getCodcli() %>    </td>
                                    <td colspan="2" align="left"> <%= financiacionPM.getNombreCliente() %> </td>
                                </tr>

                                <tr class="fila">
                                    <td align="center">Numero de cuotas</td>
                                    <td nowrap><input name="numeroCuota" type="text" class="textbox" id="numeroCuota"  maxlength="10"  value="<%= numeroCuota%>"  </td>
                                    <td align="center">Fecha de Financiacion</td>



                                    <td  width="15%" align="center">
                                        <!-- Fecha -->
                                        <input  name=fechaFinanciacion id=fechaFinanciacion  size="20" readonly="true" class="textbox" value='<%=fechaFinanciacion%>' style='width:50%'>
                                        <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(fechaFinanciacion);return false;" >
                                            <img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt="">
                                        </a>
                                    </td>          



                                </tr>     

                                <tr class="fila">
                                    <td align="center">% Interes mensual</td>
                                    <td nowrap><input name="tasaInteres" type="text" class="textbox" id="tasaInteres"  maxlength="10"  value="<%= tasaInteres%>"  </td>
                                    <td align="center">Fecha de Vencimiento</td>
                                    <td  width="15%" align="center">
                                        <!-- Fecha -->
                                        <input  name=fechaVencimiento id=fechaVencimiento  size="20" readonly="true" class="textbox" value='<%= fechaVencimiento %>' style='width:50%'>
                                        <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(fechaVencimiento);return false;" >
                                            <img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt="">
                                        </a>
                                    </td>          

                                    
                                    
                                </tr>     

                                <tr class="fila">
                                    <td align="center">% Interes Mora</td>
                                    <td nowrap><input name="tasaMora" type="text" class="textbox" id="tasaInteres"  maxlength="10"  value="<%= tasaMora%>"  </td>
                                    <td align="center"></td>
                                    <td align="center"></td>

                                </tr>                                   
                                
                                
                                
                                
                                <tr class="fila">
                                    <td align="center">Valor a Financiar</td>
                                    <td align="left"> <%=Util.FormatoMiles(dValorFinanciar) %> </td>


                                    <td align="center">Cuota inicial</td>
                                    <td nowrap><input name="cuotaInicial" type="text" class="textbox" id="cuotaInicial"  maxlength="10"  value="<%= cuotaInicial%>"  </td>
                                </tr>     



                            </table></td>
                    </tr>
                </table>




                <br>  


                <table width="1750"  border="2" align="center" cellpadding="0" cellspacing="0">
                    <tr>
                        <td>
                            <table width="100%" class="tablaInferior">
                                <tr>
                                    <td colspan="2" >
                                        <table width="100%"  border="1" cellpadding="0" cellspacing="1" class="barratitulo">
                                            <tr>
                                                <td width="30%" class="subtitulo1">FACTURAS DEL CLIENTE A REFINANCIAR</td>
                                                <td width="70%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>


                                <tr>
                                <table width="100%"  border="1" cellpadding="0" cellspacing="1" class="tblTitulo1">


                                    <tr class="tblTitulo1">
                                        <td  width="5%"  align="center"> SELECCIONAR</td>
                                        <td  width="5%"  align="center"> FACTURA</td>
                                        <td  width="5%"  align="center"> FECHA FACTURA</td>
                                        <td  width="6%"  align="center"> FECHA VENCIMIENTO</td>
                                        <td  width="6%" align="center"> MULTISERVICIO</td>
                                        <td  width="7%" align="center"> SIMBOLO VARIABLE</td>
                                        <td  width="5%"  align="center"> VALOR FACTURA</td>
                                        <td  width="5%"  align="center"> VALOR ABONO</td>
                                        <td  width="6%"  align="center"> VALOR SALDO</td>
                                        <td  width="6%"  align="center"> VALOR CAPITAL</td>
                                        <td  width="5%"  align="center"> VALOR INTERESES</td>
                                        <td  width="6%"  align="center"> SALDO CAPITAL</td>
                                        <td  width="5%"  align="center"> IVA NM</td>
                                        <td  width="5%"  align="center"> IVA NM FINAL</td>
                                        <td  width="5%"  align="center"> VALOR INTERESES PENDIENTES</td>
                                        
                                        <td  width="4%"  align="center"> DIAS ADICIONALES</td>
                                        <td  width="5%"  align="center"> VALOR ADICIONAL</td>                                        

                                        <td  width="4%"  align="center"> DIAS MORA</td>
                                        <td  width="5%"  align="center"> VALOR MORA</td>

                                    </tr>                  


                                    <%
                                        int i = 0;

                                        for (i = 0; i < listaFactura.size(); i++) {

                                            FacturaRefinanciada factura = (FacturaRefinanciada) listaFactura.get(i);
                                            String chequeado = "";

                                    %>      
                                    
                                    
                                    <%     if( factura.isSeleccionada()) {
                                              
                                               chequeado = "checked";

                                                                                                                      
                                    %>
                                              <tr class="filaresaltada">
                                        
                                    <%     }
                                            else {
                                    %>
                                              <tr class="fila_provision">
                                    <%                
                                            }
                                    %>

                                    <td><input type="checkbox" name="chkSeleccionarFactura"  id="chkSeleccionarFactura"     <%= chequeado %>   ></td>
                                                <td><%= factura.getDocumento()%></td>
                                                <td><%= factura.getFechaFactura()%></td>
                                                <td><%= factura.getFechaVencimiento()%></td>
                                                <td><%= factura.getMultiservicio()%></td>
                                                <td><%= factura.getSimboloVariable()%></td>
                                                <td><%= Util.FormatoMiles(factura.getValorFactura())%></td>
                                                <td><%= Util.FormatoMiles(factura.getValorAbono())%></td>
                                                <td><%= Util.FormatoMiles(factura.getValorSaldo())%></td>
                                                <td><%= Util.FormatoMiles(factura.getValorCapital())%></td>
                                                <td><%= Util.FormatoMiles(factura.getValorIntereses())%></td>
                                                <td><%= Util.FormatoMiles(factura.getValorRefinanciar())%></td>
                                                <td><%= Util.FormatoMiles(factura.getIvaNM() )%></td>
                                                <td><%= Util.FormatoMiles(factura.getIvaNMFinal())%></td>
                                                <td><%= Util.FormatoMiles(factura.getValorInteresesPendientes())%></td>
                                                
                                                <td><%= Util.FormatoMilesSinDecimal(factura.getDiasAdicionales())%></td>
                                                <td><%= Util.FormatoMilesSinDecimal(factura.getValorAdicional())%></td>                                                
                                                
                                                <td><%= Util.FormatoMilesSinDecimal(factura.getDiasMora())%></td>
                                                <td><%= Util.FormatoMilesSinDecimal(factura.getValorMora())%></td>
                                            </tr>                



                                    <%
                                        }
                                    %>


                                </table>

                    </tr>

                </table>
                </td>
                </tr>
                </table>



                <br>
                <img src="<%=BASEURL%>/images/botones/aceptar.gif"    height="21"  title='Calcular la refinanciacion'  onclick="javascript:seleccionOpcion(formulario,'REFINANCIAR_CALCULO');"   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
                <img src="<%=BASEURL%>/images/botones/salir.gif"      height="21"  title='Salir'      onClick="window.close();"     onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
                <br> <br>






                <%
                     if (mostrarLiquidacion.equalsIgnoreCase("SI")) {

                        int iNumeroCuota = Integer.parseInt(numeroCuota);
                        Double dCapitalPorCuota = Double.parseDouble(capitalPorCuota);
                        Double dInteresPorCuota = Double.parseDouble(interesPorCuota);
                        Double dVlrInteresesPendientesPorCuota = Double.parseDouble(vlrInteresesPendientesPorCuota);
                        Double dVlrTotalMora = Double.parseDouble(vlrTotalMora);

                        Double dCapitalPorCuotaUltimaFactura = Double.parseDouble(capitalPorCuotaUltimaFactura);
                        Double dInteresPorCuotaUltimaFactura = Double.parseDouble(interesPorCuotaUltimaFactura);
                        Double dVlrInteresesPendientesPorCuotaUltimaFactura = Double.parseDouble(vlrInteresesPendientesPorCuotaUltimaFactura);

                        Double dCuotaInicial = Double.parseDouble(cuotaInicial);

                        String fechaVencimientoFactura = ( request.getParameter("fechaVencimientoFactura")!= null) ? request.getParameter("fechaVencimientoFactura") : "error";          





                %>      


                                <table  width="1394"  align="center"  border="1" cellpadding="0" cellspacing="1" >


                                    <tr class="tblTitulo1">
                                        <td  width="6%"  align="center"> FACTURA</td>
                                        <td  width="6%"  align="center"> CANTIDAD FACTURAS</td>
                                        <td  width="6%"  align="center"> FECHA FACTURA</td>
                                        <td  width="6%"  align="center"> FECHA VENCIMIENTO</td>
                                        <td  width="8%"  align="center"> VALOR CAPITAL</td>
                                        <td  width="7%"  align="center"> VALOR INTERESES PENDIENTES</td>
                                        <td  width="7%"  align="center"> VALOR IVA</td>
                                        <td  width="8%"  align="center"> SALDO CAPITAL</td>
                                        <td  width="7%"  align="center"> VALOR MORA</td>
                                        <td  width="7%"  align="center"> VALOR ADICIONAL</td>
                                        <td  width="8%"  align="center"> TOTAL A REFINANCIAR</td>
                                        <td  width="8%"  align="center"> VALOR INTERESES</td>
                                        <td  width="8%"  align="center"> TOTAL INTERESES</td>
                                        <td  width="8%"  align="center"> TOTAL FACTURA</td>
                                    </tr>

                                    <tr class="fila_provision">

                                        <td><%= financiacionPM.getDocumento() %></td>
                                        <td><%= financiacionPM.getNumeroCuotas() - 1 %></td>
                                        <td><%= fechaFinanciacion %></td>
                                        <td><%= fechaVencimientoFactura %></td>
                                        
                                        
                                        <td><%= Util.FormatoMilesSinDecimal(financiacionPM.getCapitalPorCuota()  - dVlrInteresesPendientesPorCuota - financiacionPM.getValorIvaPorCuota() -
                                                                            financiacionPM.getVlrMoraPorCuota()  - financiacionPM.getVlrAdicionalCuota ()        ) %></td>
                                        <td><%= Util.FormatoMilesSinDecimal(dVlrInteresesPendientesPorCuota) %></td>
                                        <td><%= Util.FormatoMilesSinDecimal(financiacionPM.getValorIvaPorCuota() ) %></td>
                                        <td><%= Util.FormatoMilesSinDecimal( financiacionPM.getCapitalPorCuota()  - financiacionPM.getVlrMoraPorCuota()  - financiacionPM.getVlrAdicionalCuota ()     ) %></td>
                                        <td><%= Util.FormatoMilesSinDecimal(financiacionPM.getVlrMoraPorCuota() ) %></td>
                                        <td><%= Util.FormatoMilesSinDecimal(financiacionPM.getVlrAdicionalCuota()) %></td>
                                        <td><%= Util.FormatoMilesSinDecimal(financiacionPM.getVlrTotalRefinanciarPorCuota() ) %></td>

                                        
                                        
                                        <td><%= Util.FormatoMilesSinDecimal( financiacionPM.getInteresPorCuota()) %></td>
                                        
                                        <td><%= Util.FormatoMilesSinDecimal(dVlrInteresesPendientesPorCuota + financiacionPM.getVlrMoraPorCuota()  + financiacionPM.getVlrAdicionalCuota() + financiacionPM.getInteresPorCuota()) %></td>
                                        
                                        <td><%= Util.FormatoMilesSinDecimal(dCapitalPorCuota + dInteresPorCuota ) %></td>
                                    </tr>   

                                    
                                    
                                    <tr class="fila_provision">
                                        <td><%= financiacionPM.getDocumento() %></td>
                                        <td><%= "1"%></td>
                                        <td><%= fechaFinanciacion%></td>
                                        <td><%= fechaVencimientoFactura %></td>
                                        
                                        
                                        <td><%= Util.FormatoMilesSinDecimal( financiacionPM.getCapitalPorCuotaUltimaFactura() -  dVlrInteresesPendientesPorCuotaUltimaFactura - financiacionPM.getValorIvaUltimaCuota() -
                                                                             financiacionPM.getVlrMoraUltimaCuota()  -  financiacionPM.getVlrAdicionalUltimaCuota()    ) %></td>
                                        <td><%= Util.FormatoMilesSinDecimal(dVlrInteresesPendientesPorCuotaUltimaFactura) %></td>
                                        <td><%= Util.FormatoMilesSinDecimal( financiacionPM.getValorIvaUltimaCuota() ) %></td>
                                        <td><%= Util.FormatoMilesSinDecimal( financiacionPM.getCapitalPorCuotaUltimaFactura() -  financiacionPM.getVlrMoraUltimaCuota()  -  financiacionPM.getVlrAdicionalUltimaCuota()   ) %></td>
                                        <td><%= Util.FormatoMilesSinDecimal(financiacionPM.getVlrMoraUltimaCuota()  ) %></td>
                                        <td><%= Util.FormatoMilesSinDecimal(financiacionPM.getVlrAdicionalUltimaCuota() ) %></td>
                                        <td><%= Util.FormatoMilesSinDecimal(financiacionPM.getVlrTotalRefinanciarUltimaCuota()) %></td>

                                        
                                        
                                        
                                        <td><%= Util.FormatoMilesSinDecimal(financiacionPM.getInteresPorCuotaUltimaFactura() ) %></td>
                                        
                                        <td><%= Util.FormatoMilesSinDecimal(dVlrInteresesPendientesPorCuotaUltimaFactura + financiacionPM.getVlrMoraUltimaCuota()  + financiacionPM.getVlrAdicionalUltimaCuota() + financiacionPM.getInteresPorCuotaUltimaFactura()) %></td>
                                        
                                        <td><%= Util.FormatoMilesSinDecimal(dCapitalPorCuotaUltimaFactura + dInteresPorCuotaUltimaFactura ) %></td>
                                    </tr>   
                                    
                                    

                                    
                                    <tr class="filaTotalAmarilla12">
                                        <td  colspan="4"   align="center"  > TOTALES  </td>

                                        <td > <%= Util.FormatoMilesSinDecimal(  (dCapitalPorCuota - dVlrInteresesPendientesPorCuota -  financiacionPM.getValorIvaPorCuota() -  financiacionPM.getVlrMoraPorCuota()  - financiacionPM.getVlrAdicionalCuota ()  ) * ( iNumeroCuota - 1) + 
                                                                                (dCapitalPorCuotaUltimaFactura - dVlrInteresesPendientesPorCuotaUltimaFactura - financiacionPM.getValorIvaUltimaCuota() - financiacionPM.getVlrMoraUltimaCuota()  -  financiacionPM.getVlrAdicionalUltimaCuota()  )     ) %>  </td>
                                        
                                        
                                        <td><%= Util.FormatoMilesSinDecimal( dVlrInteresesPendientesPorCuota * ( iNumeroCuota - 1) + dVlrInteresesPendientesPorCuotaUltimaFactura) %></td>
                                        <td><%= Util.FormatoMilesSinDecimal( financiacionPM.getValorIva() ) %></td>
                                        
                                        <td><%= Util.FormatoMilesSinDecimal( (dCapitalPorCuota  -  financiacionPM.getVlrMoraPorCuota()  - financiacionPM.getVlrAdicionalCuota () ) * ( iNumeroCuota - 1)         +  dCapitalPorCuotaUltimaFactura -  financiacionPM.getVlrMoraUltimaCuota()  -  financiacionPM.getVlrAdicionalUltimaCuota() ) %></td>
                                        <td><%= Util.FormatoMilesSinDecimal( financiacionPM.getVlrTotalMora() ) %></td>
                                        <td><%= Util.FormatoMilesSinDecimal(financiacionPM.getVlrTotalAdicional() ) %></td>
                                        
                                        
                                        
                                        <td><%= Util.FormatoMilesSinDecimal( financiacionPM.getVlrTotalRefinanciar() ) %></td>
                                        <td><%= Util.FormatoMilesSinDecimal(financiacionPM.getTotal_intereses()) %></td>
                                        
                                        
                                        
                                        
                                        <td><%= Util.FormatoMilesSinDecimal(  ( dVlrInteresesPendientesPorCuota + financiacionPM.getVlrMoraPorCuota()  + financiacionPM.getVlrAdicionalCuota() + financiacionPM.getInteresPorCuota() )  * ( iNumeroCuota - 1)  +   
                                                                                dVlrInteresesPendientesPorCuotaUltimaFactura + financiacionPM.getVlrMoraUltimaCuota()  + financiacionPM.getVlrAdicionalUltimaCuota() + financiacionPM.getInteresPorCuotaUltimaFactura()   ) %></td>
                                        
                                        
                                        <td><%= Util.FormatoMilesSinDecimal(dCapitalPorCuota * ( iNumeroCuota - 1) + dInteresPorCuota * ( iNumeroCuota - 1)  + dCapitalPorCuotaUltimaFactura + dInteresPorCuotaUltimaFactura      )  %></td>

                                        
                                        
                                        
                                    </tr>   
                                                                        
          
                                    


                                    <tr class="fila_provision">

                                        <td><%= "CU" + financiacionPM.getNumeracion() + "_1" %></td>
                                        <td><%= 1%></td>
                                        <td><%= fechaFinanciacion %></td>
                                        <td><%= fechaFinanciacion %></td>
                                        <td><%= Util.FormatoMilesSinDecimal(dCuotaInicial) %></td>
                                        <td><%= Util.FormatoMilesSinDecimal(0.00) %></td>
                                        <td><%= Util.FormatoMilesSinDecimal(0.00) %></td>
                                        <td><%= Util.FormatoMilesSinDecimal(0.00) %></td>
                                        <td><%= Util.FormatoMilesSinDecimal(0.00) %></td>
                                        <td><%= Util.FormatoMilesSinDecimal(0.00) %></td>
                                        <td><%= Util.FormatoMilesSinDecimal(0.00) %></td>
                                        <td><%= Util.FormatoMilesSinDecimal(0.00) %></td>
                                        <td><%= Util.FormatoMilesSinDecimal(0.00) %></td>
                                        <td><%= Util.FormatoMilesSinDecimal(dCuotaInicial) %></td>
                                    </tr>

                                </table>
                                    
                                    
                                <br>         


                                    
                                <table  width="1000"  align="center"  border="1" cellpadding="0" cellspacing="1" >    
                                    
                                    
                                    <tr class="tblTitulo1">
                                        <td  width="10%" align="center"> SIMBOLO VARIABLE FACTURA</td>
                                        <td  width="10%" align="center"> SIMBOLO VARIABLE CUOTA INICIAL</td>
                                        <td  width="80%" align="center"> OBSERVACIONES</td>
                                    </tr>                                          

                                    <tr class="fila">

                                        <td   align="center"> <input name="simboloVariableFactura" id="simboloVariableFactura" type="text" class="textbox"  value = '<%= financiacionPM.getSimboloVariableFactura() %>'     </td>
                                        <td   align="center"> <input name="simboloVariableCuota"   id="simboloVariableCuota"  type="text" class="textbox"  value = '<%= financiacionPM.getSimboloVariableCuota() %>'     </td>
                                        <td   align="center"> <input name="observacion" id="observacion" type="text"  size = "140" value = '<%= financiacionPM.getObservaciones()%>'     </td>
                                        
                                   
                                        
                                    </tr>
                                                                
                                </table>      
                                    

                                <br> 
                                <img src="<%=BASEURL%>/images/botones/aplicar.gif"    height="21"  title='Generar las nuevas facturas'   onclick="javascript:crearAction(formulario,'GENERAR_FACTURAS');"    
                                     onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">                
                                       

                <%
                    }
                %>

                <!-- MENSAJE -->
                <% if(msj!=null  &&  !msj.equals("") ){%>
                        <br><br>
                        <table border="2" align="center">
                            <tr>
                                <td>
                                    <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                        <tr>
                                                <td width="450" align="center" class="mensajes"><%= msj %></td>
                                                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                                <td width="58">&nbsp; </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>

                    <%}%>
             
                

            </form >

        </center>

    </div>

    <!-- Necesario para los calendarios-->
    <iframe width="188" height="166" name="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" id="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins.js" src="<%=BASEURL%>/js/calendartsp/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"></iframe>

    <%=datos[1]%>


</body>
</html>