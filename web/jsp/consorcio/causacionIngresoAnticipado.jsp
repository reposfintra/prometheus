<%-- 
    Document   : causacionIngresoAnticipado
    Created on : 16/12/2010, 09:05:06 PM
    Author     : Alvaro
--%>


<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>


<html>
<head>
        <title>Generacion de causacion ingresos INM e IPM </title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
	<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>

	<script>
	   function send(theForm){

             var elemento_ano  = document.getElementById("ano").value;
             var elemento_mes  = document.getElementById("mes").value;


             var parametro = "&ano="+elemento_ano;
             parametro     = parametro + "&mes=" + elemento_mes;

             window.close();
             window.open('<%= CONTROLLER %>?estado=Consorcio&accion=Acceso&evento=CAUSACION_INGRESOS' + parametro );
	   }
            function FillCombo() {

              var anoInicial = 2009;
              var fecha=new Date();
              var anoActual=fecha.getFullYear();
              //marco el n�mero de anos en el select
              var numeroAnos = anoActual - anoInicial + 1;
              document.formulario.ano.length = numeroAnos;

              //introducir los anos en el select
              for(i=0;i<numeroAnos;i++){
                document.formulario.ano.options[i].value=anoInicial+i;
                document.formulario.ano.options[i].text=anoInicial+i;
              }
            }

	</script>

</head>

<body onLoad="FillCombo();">


<%  String path    = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    %>

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modulo de Causacion"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; ">


   <% String  msj           = request.getParameter("msj");
      String aceptarDisable = request.getParameter("aceptarDisable");
   %>

 <center>

   <form action="<%=CONTROLLER%>?estado=Consorcio&accion=Acceso&evento=CAUSACION_INGRESOS" method='post' name='formulario' >


   <table width="450" border="2" align="center">
       <tr>
          <td>
               <table width='100%' align='center' class='tablaInferior'>

                  <tr class="barratitulo">
                    <td colspan='4' >
                       <table cellpadding='0' cellspacing='0' width='100%'>
                         <tr>
                          <td align="left" width='65%' class="subtitulo1">&nbsp;GENERACION DE CAUSACION INM E IPM</td>
                          <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"  height="20" align="left"><%=datos[0]%></td>
                        </tr>
                       </table>
                    </td>
                 </tr>




                 <tr  class="fila">
                       <td >
                           A&ntildeo
                       </td>
                       <td >
                           <select id="ano" name="ano">
                           </select>
                       </td>
                       <td >
                            Mes
                       </td>
                       <td >
                           <select id="mes" name="mes" >
                               <option value="01">1</option>
                               <option value="02">2</option>
                               <option value="03">3</option>
                               <option value="04">4</option>
                               <option value="05">5</option>
                               <option value="06">6</option>
                               <option value="07">7</option>
                               <option value="08">8</option>
                               <option value="09">9</option>
                               <option value="10">10</option>
                               <option value="11">11</option>
                               <option value="12">12</option>
                           </select>
                       </td>





                 </tr>


                 <tr  class="fila">
                       <td colspan='4' >
                          <table class='tablaInferior' width='100%'>
                               <tr  class="fila">
                                   <td align="center" > Presione ACEPTAR para generar las INM e IPM </td>
                               </tr>
                          </table>
                       </td>
                 </tr>

           </table>
         </td>
      </tr>
   </table>

   <br>



   <% if ( (aceptarDisable == null) || (aceptarDisable.equals(""))  ) { %>
        <img src="<%=BASEURL%>/images/botones/aceptar.gif"    height="21"  title='Genera las INM e IPM'  onclick='send(formulario);'   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" >
   <% }
      else { %>
        <img src="<%=BASEURL%>/images/botones/aceptarDisable.gif"    height="21"  style="cursor:hand">
   <% } %>

   <img src="<%=BASEURL%>/images/botones/salir.gif"      height="21"  title='Salir'      onClick="window.close();"     onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">


   <!-- MENSAJE -->
   <% if(msj!=null  &&  !msj.equals("") ){%>
        <br><br>
        <table border="2" align="center">
              <tr>
                <td>
                    <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                          <tr>
                                <td width="450" align="center" class="mensajes"><%= msj %></td>
                                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="58">&nbsp; </td>
                          </tr>
                     </table>
                </td>
              </tr>
        </table>

    <%}%>



      </form >

</center>

</div>


<%=datos[1]%>


</body>
</html>
