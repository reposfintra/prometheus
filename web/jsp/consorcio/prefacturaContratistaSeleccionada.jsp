<%-- 
    Document   : prefacturaContratistaSeleccionada
    Created on : 04/25/2010, 09:57:26 PM
    Author     : Alvaro
--%>



<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.opav.model.services.ClientesVerService"%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<%@page import="com.tsp.opav.model.beans.*"%>

<%
             Usuario usuario = (Usuario) session.getAttribute("Usuario");
            String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
            String datos[] = model.menuService.getContenidoMenu(BASEURL, request.getRequestURI(), path);
            List listaAccionesContratista = modelopav.consorcioService.getAccionesContratista() ;
            
            //calcular a�o de vigencia para la prefacturacion de contratistas.
            //autor: egonzalez 
                    Date dNow = new Date();
                    SimpleDateFormat ft = new SimpleDateFormat ("yyyy");
                    String currentDate = ft.format(dNow);
                    int ano = Integer.parseInt(currentDate);
                    String anoVigencia= String.valueOf(ano);
                    
            //fin...
                   
            double por_iva_contratista  = modelopav.consorcioService.getPorcentaje("IVA",anoVigencia);//20100617


            Contratista contratista = modelopav.consorcioService.getContratista();
            int secuencia_prefactura = contratista.getSecuencia_prefactura() + 1;
            String proxima_prefactura = contratista.getId_contratista() + '-' + Util.llenarConCerosALaIzquierda( secuencia_prefactura, 6 );

            double porcentajeIva = modelopav.consorcioService.getPorcentaje("IVA",anoVigencia);//20100617

            ClientesVerService clvsrv = new ClientesVerService(usuario.getBd());
            String perfil = clvsrv.getPerfil(usuario.getLogin());




%>
<html>
  <head>

    <title>Consultar items para prefacturar</title>

    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
    <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script>




    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">

  </head>
  <body  >
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
      <jsp:include page="/toptsp.jsp?encabezado=Prefacturar Ordenes"/>
    </div>


    <div id="capaSiguiente" style="position:absolute; width:1600; height:87%; z-index:0; left: 0px; top: 100px; ">
        <table width="580"  border="1"  align="center">
          <tr>
            <td class= "leyenda"   width="130">ID CONTRATISTA</td>
            <td class= "filagris1" width="250"><%= contratista.getId_contratista() %> </td>
            <td class= "leyenda"   width="130">NUMERO PREFACTURA</td>
            <td class= "filagris1" width="70"><%= proxima_prefactura %></td
          ></tr>
          <tr>
            <td class= "leyenda"   width="130">NOMBRE CONTRATISTA</td>
            <td class= "filagris1" width="250"><%= contratista.getNombre_contratista() %></td>
            <td class= "leyenda"   width="130">VALOR PREFACTURA</td>
            <td class= "filagris1" width="70" name="totalPrefactura" id="totalPrefactura">0.00</td
          ></tr>
        </table>



      <form name='formulario' method='post' id="formulario" action="<%=CONTROLLEROPAV%>?estado=Consorcio&accion=Acceso&evento=PROCESAR_ACCIONES_CONTRATISTA" >


        <table width="1580"  border="1"  align="center">

              <TR class="tblTitulo1" align="center">
                      <TH  width="10" align="center">
                         <input name="ckTotal" type="checkbox" id="ckTotal" onclick="seleccionTotal() ">
                      </TH>

                      <TH  width="30" align="center"> SOLICITUD         </TH>
                      <TH  width="30" align="center"> PARCIAL         </TH>

                      <TH  width="35"> ACCION        </TH>
                      <TH  width="80"> NUM OS</TH>

                      <TH  width="60" align="center"> MATERIAL  </TH>
                      <TH  width="60"> MANO OBRA </TH>
                      <TH  width="40"> OTROS     </TH>
                      <TH  width="60"> ADMINISTRACION </TH>
                      <TH  width="60"> IMPREVISTO </TH>
                      <TH  width="60"> UTILIDAD </TH>

                      <TH  width="60"> SUBTOTAL </TH>

                      <TH  width="60"> BONIFICACION </TH>
                      <TH  width="70"> SUBTOTAL 2 </TH>
                      <TH  width="70"> BASE IVA </TH>

                      <TH  width="80"> IVA </TH>
                      <TH  width="80"> TOTAL </TH>

                      <TH  width="200"> NOMBRE DEL CLIENTE  </TH>
                      <TH  width="170"> DIRECCION           </TH>
                      <TH  width="50"> CIUDAD              </TH>
                      <TH  width="205"> DESCRIPCION            </TH>


              </TR>




          <%
            int i=0;
            Iterator it = listaAccionesContratista.iterator();
            while (it.hasNext()) {

              i++;
              AccionContratista accionContratista = (AccionContratista)it.next();
              String id_solicitud   = accionContratista.getId_solicitud();
              int    parcial        = accionContratista.getParcial();
              String id_accion      = accionContratista.getId_accion();
              String id_cliente     = accionContratista.getId_cliente();
              String nombre_cliente = accionContratista.getNombre();
              String direccion      = accionContratista.getDireccion();
              String ciudad         = accionContratista.getCiudad();
              String descripcion    = accionContratista.getDescripcion();
	      String num_os         = accionContratista.getNum_os();



              double material       = accionContratista.getMaterial();
              double mano_obra      = accionContratista.getMano_obra();
              double otros          = accionContratista.getOtros();
              double administracion  = accionContratista.getAdministracion();
              double imprevisto      = accionContratista.getImprevisto();
              double utilidad        = accionContratista.getUtilidad();



              double total_contratista = material + mano_obra + otros + administracion + imprevisto + utilidad;
              double bonificacion = -accionContratista.getBonificacion();
              double subtotal = total_contratista + bonificacion;

              
              double val_base_iva_contratista =  accionContratista.getBase_iva_contratista();
              double val_iva_contratista  =  Util.redondear2(val_base_iva_contratista * por_iva_contratista/100, 2);
              double valorItem = subtotal + val_iva_contratista;

	     

          %>
              <tr class='<%= (i%2==0?"filagris1":"filaazul1") %>' id='fila<%=i%>' >
                <td width="10" align="center" nowrap >   <%-- &nbsp; --%>
             	   <input vspace="10" name="ckestado" type="checkbox" id="ckEstado<%= i %>" value='<%= id_accion    %>' onclick=" cambiarColorMouse(fila<%=i%>); valorPrefactura(<%= i %>)">
                </td>




                <td width="30"  align="center"> <%= id_solicitud %>   </td>
                 <td width="30"  align="center"> <%= parcial %>   </td>

                <td width="35"  align="center"> <%= id_accion %>  </td>
                <td width="80"  align="center"><%=num_os%> </td>

                <td width="60"  align="right"  id="valorPrev<%= i %>" > <%= Util.customFormat(material) %> </td>
                <td width="60"  align="right"  id="valorManoObra<%= i %>" > <%= Util.customFormat(mano_obra ) %></td>
                <td width="40"  align="right"  id="valorOtro<%= i %>" > <%= Util.customFormat(otros ) %></td>
                <td width="60"  align="right"  id="administracion<%= i %>" > <%= Util.customFormat(administracion ) %></td>
                <td width="60"  align="right"  id="imprevisto<%= i %>" > <%= Util.customFormat(imprevisto ) %></td>
                <td width="60"  align="right"  id="utilidad<%= i %>" > <%= Util.customFormat(utilidad ) %></td>
                <td width="60"  align="right"  id="valorPrev<%= i %>" > <%= Util.customFormat(total_contratista ) %>    </td>
                <td width="60"  align="right"  id="valorBonificacion<%= i %>" style="color:red"> <%= Util.customFormat(bonificacion) %>  </td>
                <td width="70"  align="right"  id="subtotal<%= i %>" > <%= Util.customFormat(subtotal) %>  </td>
                <td width="70"  align="right"  id="baseIva<%= i %>" > <%= Util.customFormat(val_base_iva_contratista) %>  </td>

                <td width="80"  align="right"  id="valorIva  <%= i %>" > <%= Util.FormatoMiles(val_iva_contratista)%>        </td>

                
                <td width="80"  align="right"  id="valorItem<%= i %>" > <%= Util.FormatoMiles(valorItem) %>      </td>

                <td width="200" align="left"> <%= nombre_cliente %></td>
                <td width="170" align="left"> <%= direccion %>     </td>
                <td width="50"  align="center"> <%= ciudad %>        </td>
                <td width="205" align="left">   <%= descripcion %>      </td>

              </tr>
           <%
                }
           %>

        </table>

        <p>&nbsp;</p>

        <table align="center">
          <tr>
            <td colspan="2" nowrap align="center">
                  <% if(clvsrv.ispermitted(perfil, "64")) { %>
           
                <img src="<%=BASEURL%>/images/botones/aceptar.gif"    height="21"  title='Consultar'  onClick="ValidarSeleccion()"      onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
                <%}%>

            <img src="<%=BASEURL%>/images/botones/salir.gif"      height="21"  title='Salir'      onClick="window.close();"     onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
          </tr>
        </table>
      </form >
    </div>



    <script >


      function ValidarSeleccion()
      {
          var item = 0;
          finItem = false;
          marcado = false;

          while (finItem == false){
             item++;
             if (document.getElementById("ckEstado"+item) != null){
                 if (document.getElementById("ckEstado"+item).checked == true){
                     marcado = true;
                     finItem = true;

                 }
             }
             else
                 finItem = true;
          }

          if (marcado) {
              formulario.submit();
          }
          else
              alert("Para prefacturar se requiere marcar alguno de los items");

      }


      function valorPrefactura(item)
      {
          var elemento  = document.getElementById("valorItem"+item);
          elemento      = elemento.firstChild.nodeValue;
          var valorItem = parseFloat(elemento.replace(/\,/g,""));

          elemento      = document.getElementById("totalPrefactura");
          elemento      = elemento.firstChild.nodeValue;
          var valorTotal= parseFloat(elemento.replace(/\,/g,""));

          var estado = document.getElementById("ckEstado"+item).checked;
          if (estado)
             valorTotal = valorTotal + valorItem;
          else
             valorTotal = valorTotal - valorItem;

          document.getElementById("totalPrefactura").innerHTML = Formatear(valorTotal,2,true);;
      }




      function seleccionTotal()
      {
          var estado = document.getElementById("ckTotal").checked;

          var item = 0;
          var valorTotal = 0;
          finItem = false;
          if (estado) {
             while (finItem == false){
                 item++;
                 if (document.getElementById("ckEstado"+item) != null){
                     document.getElementById("ckEstado"+item).checked = true;
                     var elemento  = document.getElementById("valorItem"+item);
                     elemento      = elemento.firstChild.nodeValue;
                     var valorItem = parseFloat(elemento.replace(/\,/g,""));
                     valorTotal = valorTotal + valorItem;
                 }
                 else
                     finItem = true;
             }
          }
          else {
             while (finItem == false){
                 item++;
                 if (document.getElementById("ckEstado"+item) != null){
                     document.getElementById("ckEstado"+item).checked = false;
                     document.getElementById("totalPrefactura").innerHTML = 0.00;
                 }
                 else
                     finItem = true;
             }
          }

          document.getElementById("totalPrefactura").innerHTML = Formatear(valorTotal,2,true);

      }


      function Formatear(numero,decimales,miles)
      {
        var numero = new oNumero(numero);
        return numero.formato(decimales, miles);
      }

	  function cambiarColorExPrefac(prefa,valo){
	  		//alert(prefa);
			for (i=0;i<formulario.length;i++){
				  if (formulario.elements[i].name=='expre' && formulario.elements[i].value==prefa ){
						//alert(formulario.elements[i].title);
	  				    document.getElementById("ckEstado"+formulario.elements[i].title+"").checked=valo;
				  }
			}
	  }

      //Objeto oNumero
      function oNumero(numero)
      {
      //Propiedades
      this.valor = numero || 0
      this.dec = -1;
      //M�todos
      this.formato = numFormat;
      this.ponValor = ponValor;
      //Definici�n de los m�todos
      function ponValor(cad)
      {
      if (cad =='-' || cad=='+') return
      if (cad.length ==0) return
      if (cad.indexOf('.') >=0)
          this.valor = parseFloat(cad);
      else
          this.valor = parseInt(cad);
      }
      function numFormat(dec, miles)
      {
      var num = this.valor, signo=3, expr;
      var cad = ""+this.valor;
      var ceros = "", pos, pdec, i;
      for (i=0; i < dec; i++)
      ceros += '0';
      pos = cad.indexOf('.')
      if (pos < 0)
          cad = cad+"."+ceros;
      else
          {
          pdec = cad.length - pos -1;
          if (pdec <= dec)
              {
              for (i=0; i< (dec-pdec); i++)
                  cad += '0';
              }
          else
              {
              num = num*Math.pow(10, dec);
              num = Math.round(num);
              num = num/Math.pow(10, dec);
              cad = new String(num);
              }
          }
      pos = cad.indexOf('.')
      if (pos < 0) pos = cad.lentgh
      if (cad.substr(0,1)=='-' || cad.substr(0,1) == '+')
             signo = 4;
      if (miles && pos > signo)
          do{
              expr = /([+-]?\d)(\d{3}[\.\,]\d*)/
              cad.match(expr)
              cad=cad.replace(expr, RegExp.$1+','+RegExp.$2)
              }
      while (cad.indexOf(',') > signo)
          if (dec<0) cad = cad.replace(/\./,'')
              return cad;
      }
      }//Fin del objeto oNumero:


    </script>




  </body>
</html>