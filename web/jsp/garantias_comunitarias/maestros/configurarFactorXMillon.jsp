<%-- 
    Document   : configurarValorXMillon
    Created on : 14/09/2016, 02:17:57 PM
    Author     : mcastillo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Configuración Factor X Millón</title>
        
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link href="./css/popup.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script> 
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script src="./js/configFactorXMillon.js" type="text/javascript"></script>
        
          <link type="text/css" rel="stylesheet" href="./css/contratos.css " />
     
        <!--jqgrid--> 
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
    </head>
    <body>
        <div id="capaSuperior"  style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Configuración Factor Por Millón"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">
            <br>
            <center>
                 <table id="tabla_config_factor"></table>
                 <div id="page_tabla_config_factor"></div>    
            </center>
            <div id="dialogLoading" style="display:none;">
                <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                <center>
                    <img src="./images/cargandoCM.gif"/>
                </center>
            </div>
            <div id="dialogMsj" title="Mensaje" style="display:none;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div>      
        </div>
        <div id="div_config_factor"  style="width: 530px;display:none;" > 
                <table aling="center" style=" width: 100%" >
                    <tr>                
                        <td>
                            <label> Convenio <span style="color:red;">*</span></label> 
                        </td>
                        <td>
                            <input id="codigo_convenio" type="text" maxlength="5"/>
                        </td>                       
                    </tr>
                    <tr>
                        <td>
                            <label> Unidad Negocio<span style="color:red;">*</span></label> 
                        </td>
                        <td>
                             <input type="text" id="id_config"  style="width: 50px" hidden >
                            <select id="und_negocio" class="combo_180px" style="width: 168px"></select> 
                        </td>                                             
                    </tr>  
                    <tr>
                        <td>
                            <label> Empresa<span style="color:red;">*</span></label> 
                        </td>
                        <td>                           
                            <select id="nit_empresa_fianza" class="combo_180px" style="width: 168px"></select> 
                        </td>                                             
                    </tr>  
                    <tr>                
                        <td>
                            <label> Plazo Inicial<span style="color:red;">*</span></label> 
                        </td>
                        <td>
                            <input id="plazo_inicial" class="solo-numero" />
                        </td>                       
                    </tr>
                    <tr>
                        <td
                            <label> Plazo Final<span style="color:red;">*</span></label> 
                        </td>
                        <td>
                            <input id="plazo_final" class="solo-numero" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label> % Comisión</label> 
                        </td>
                        <td>
                            <input type="text" id="porcentaje_comision" class="solo-numeric" onkeyup="changeStatusFactorValue(event,this.id,this.value)"/>
                        </td>
                    </tr>
                    <tr>                       
                       <td>
                            <label> Valor Comisión<span style="color:red;">*</span></label> 
                        </td>
                        <td>
                            <input type="text"  id="valor_comision" class="solo-numeric" onkeyup="changeStatusFactorValue(event,this.id,this.value)"/>
                        </td>
                    </tr>  
                    <tr>
                        <td>
                            <label> % IVA<span style="color:red;">*</span></label> 
                        </td>
                        <td>
                            <input type="text" id="porcentaje_iva" class="solo-numeric"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Es financiado</label> 
                        </td>
                        <td>
                            <input type="checkbox" name="financiado" id="financiado" onchange="isFinanciado();"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label> % Dto en desembolso</label> 
                        </td>
                        <td>
                            <input type="text" id="porcentaje_aval" class="solo-numeric"  onblur="porc_aval_cliente();"/>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label> % Financiacion aval</label> 
                        </td>
                        <td>
                            <input type="text" id="porcentaje_aval_finan" class="solo-numeric" readonly="true" />
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <label> Producto<span style="color:red;">*</span></label> 
                        </td>
                        <td>
                             <input type="text" id="id_config"  style="width: 50px" hidden >
                            <select id="producto" class="combo_180px" style="width: 168px"></select> 
                        </td>                                             
                    </tr> 
                     <tr>
                        <td>
                            <label> Cobertura<span style="color:red;">*</span></label> 
                        </td>
                        <td>
                             <input type="text" id="id_config"  style="width: 50px" hidden >
                            <select id="cobertura" class="combo_180px" style="width: 168px"></select> 
                        </td>                                             
                    </tr> 
                    <hr>
                </table> 
        </div> 
    </body>
</html>
