<%-- 
    Document   : empresasFianza
    Created on : 26/03/2018, 03:17:15 PM
    Author     : jbermudez
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Maestro Proveedores Fianza</title>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link href="./css/popup.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script> 
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script src="./js/proveedoresFianza.js" type="text/javascript"></script>

        <link type="text/css" rel="stylesheet" href="./css/contratos.css " />

        <!--jqgrid--> 
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
    </head>
    <body>
        <div id="capaSuperior"  style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Maestro Proveedores Fianza"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">
            <br>
            <center>
                <table id="tabla_prov_fianza"></table>
                <div id="page_tabla_prov_fianza"></div>    
            </center>
            <div id="dialogLoading" style="display:none;">
                <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                <center>
                    <img src="./images/cargandoCM.gif"/>
                </center>
            </div>
            <div id="dialogMsj" title="Mensaje" style="display:none;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div>      
        </div>
        <div id="div_prov_fianza"  style="width: 530px;display:none;" > 
            <table aling="center" style=" width: 100%" >
                <tr>
                    <td>
                        <label> Opci�n <span style="color:red;">*</span></label> 
                    </td>
                    <td>
                        <select id="campo" onchange="setLength(this.value)">
                            <option value="nit">Nit</option>
                            <option value="nombre">Nombre</option>
                        </select>
                    </td>                       
                </tr>
                <tr>
                    <td>
                        <label> NIT o Nombre <span style="color:red;">*</span></label> 
                    </td>
                    <td>
                        <input id="texto" type="number" maxlength="15"/>
                    </td>                       
                </tr>
                <tr>
                    <td>
                        <label> Proveedor</label> 
                    </td>
                    <td>
                        <select id="prov" style="width: 100%"></select>
                    </td>
                </tr>
            </table> 
        </div> 
    </body>
</html>
