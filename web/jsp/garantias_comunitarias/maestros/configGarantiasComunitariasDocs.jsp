<%-- 
    Document   : configGarantiasComunitariasDocs
    Created on : 12/09/2016, 10:25:00 AM
    Author     : mcastillo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">    


        <title>Oportunidad De Negocio</title>

        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui-2.css" />
        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script> 
        <script src="./js/jquery/editor/ckeditor/ckeditor.js" type="text/javascript"></script>

        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <script src="./js/configGarantiasComunitariasDocs.js" type="text/javascript"></script>
        <link type="text/css" rel="stylesheet" href="./css/contratos.css " />
    </head>
    <body>
        <center>
            <div id="tabs_documentos" style="width: 1750px;height:630px">
                <ul id="lst_documents">
                    <span style="float:right;">
                        <img id="correspondencias" title="Ver correspondencias" src="/fintra/images/botones/iconos/tab_search.png" 
                             style="margin-left: 5px; height: 100%; vertical-align: middle;"> 
                    </span>
                </ul>  
            </div>   
        </center>
        <div id="equiv_variables" style="display:none;">  
            <table id="drag_popup_equivalencias" width="294" style="margin-left:8px" >
                <tr>
                    <td  class="titulo_ventana" height="25">
                        <div style="float:left; font-size: 15px">CORRESPONDENCIAS</div> 
                        <div style="float:right"><a id="close" class="ui-widget-header ui-corner-all"><span>X</span></a></div>
                    </td>
                </tr>

            </table>
            <center><table id="tbl_equiv_variables" class="tablas" style=" width: 95%"></table></center>
        </div>
        <div id="dialogLoading" style="display:none;">
            <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
            <center>
                <img src="./images/cargandoCM.gif"/>
            </center>
        </div>
        <!-- Dialogo de los jsp> -->
        <div id="dialogMsj" style="display:none;">
            <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
        </div>
    </body>
</html>
