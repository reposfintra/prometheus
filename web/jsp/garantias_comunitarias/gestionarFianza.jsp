<%-- 
    Document   : gestionarFianza
    Created on : 29/09/2016, 03:29:10 PM
    Author     : mcastillo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>CxP Consolidada</title>
        
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link href="./css/popup.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script> 
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script src="./js/fianza.js" type="text/javascript"></script>
        
          <link type="text/css" rel="stylesheet" href="./css/fianza.css " />
     
        <!--jqgrid--> 
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
    </head>
    <body>
        <div id="capaSuperior"  style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=CxP Consolidada"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">
            <br>
            <center>
                 <div id="div_filtro_negocios" class="frm_search" style="width:700px">  
                    <table>
                         <tr>
                            <td colspan="2">
                                <label for="unidad_negocio">Unidad Negocio<span style="color:red;">*</span></label>
                                <select name="unidad_negocio" onchange="cargarComboGenerico2('agencia_unidad_negocio', 35);" id="unidad_negocio" class="combo_180px">
                                </select> 
                            </td>   
                            <td colspan="2">
                                <label for="agencia_unidad_negocio">Agencia</label>
                                <select name="agencia_unidad_negocio" id="agencia_unidad_negocio" class="combo_180px">
                                </select> 
                            </td>   
                            <td colspan="2">
                                <label for="empresa_fianza">Empresa<span style="color:red;">*</span></label>
                                <select name="empresa_fianza" id="empresa_fianza" class="combo_180px" style="width:180px">
                                </select> 
                            </td>   
                            <td colspan="2">
                                <label for="periodo_corte">Per�odo<span style="color:red;">*</span></label>
                                <input type="text" name="periodo_corte" id="periodo_corte" placeholder="AAAAMM" style="width:70px" class="solo-numero" maxlength="6">                       
                            </td>
                            <td>
                                <button id="listarNegocios" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                        role="button" aria-disabled="false">
                                    <span class="ui-button-text">Buscar</span>
                                </button>
                            </td>
                        </tr>  
                    </table>                   
                </div>
                <br><br>
                 <table id="tabla_deducciones_fianza"></table>
                 <div id="page_tabla_deducciones_fianza"></div>    
            </center>
            <div id="dialogLoading" style="display:none;">
                <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                <center>
                    <img src="./images/cargandoCM.gif"/>
                </center>
            </div>
            <div id="dialogMsj" title="Mensaje" style="display:none;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div>      
        </div>
        <div id="divSalidaEx" title="Exportacion" style=" display: block" >
                <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
                <center>
                    <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
                </center>
                <div id="respEx" style=" display: none"></div>
            </div> 
    </body>
</html>
