<%-- 
    Document   : resumen_indemnizados
    Created on : 5/10/2016, 10:41:42 AM
    Author     : mcastillo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <title>Resumen Indemnizados</title>


        <link href="./css/simple-sidebar.css" rel="stylesheet" type="text/css"/>
        <link href="./css/OportunidadNegocio.css" rel="stylesheet" type="text/css"/>


        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>     
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>      

        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <link type="text/css" rel="stylesheet" href="./css/fianza.css " />
             
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>
        <script type="text/javascript" src="./js/reporte_garantias.js"></script> 
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
             <jsp:include page="/toptsp.jsp?encabezado=Resumen Negocios Indemnizados"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">
            <center>  
                <div id="div_filtro_negocios" class="frm_search" style="width:230px">  
                    <table>
                         <tr>                           
                            <td colspan="2">
                                <label for="periodo_corte">Per�odo</label>
                                <input type="text" name="periodo_corte" id="periodo_corte" style="width:70px">                       
                            </td>
                            <td>
                                <button id="listarNegocios" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                        role="button" aria-disabled="false">
                                    <span class="ui-button-text">Buscar</span>
                                </button>
                            </td>
                        </tr>  
                    </table>                   
                </div>
                <br><br>
                <table id="tabla_negocios_indemnizados"></table>
                <div id="page_tabla_negocios_indemnizados"></div>
            </center>

            <div id="divSalidaEx" title="Exportacion" style=" display: block" >
                <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
                <center>
                    <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
                </center>
                <div id="respEx" style=" display: none"></div>
            </div>       
        </div>
        <div id="dialogLoading" style="display:none;">
                <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                <center>
                    <img src="./images/cargandoCM.gif"/>
                </center>
            </div>
            <div id="dialogMsj" title="Mensaje" style="display:none;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div> 
        <script type="text/javascript">  
               initResumenIndemnizacion();    
        </script>
    </body>
</html>
