<%-- 
    Document   : reporte_reclamaciones
    Created on : 21/09/2016, 09:07:49 AM
    Author     : mcastillo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
       
        <title>Reporte Reclamaciones </title>

        <link href="./css/simple-sidebar.css" rel="stylesheet" type="text/css"/>
        <link href="./css/OportunidadNegocio.css" rel="stylesheet" type="text/css"/>


        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>     
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>      

        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <link type="text/css" rel="stylesheet" href="./css/fianza.css " />
              
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>
        <script type="text/javascript" src="./js/reporte_garantias.js"></script> 
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
             <jsp:include page="/toptsp.jsp?encabezado=Reporte Reclamaciones con Garant�a"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">
            <center>  
                <div id="div_filtro_negocios" class="frm_search">  
                    <table>
                         <tr>
                            <td colspan="2">
                                <label for="unidad_negocio">Unidad Negocio<span style="color:red;">*</span></label>
                                <select name="unidad_negocio" id="unidad_negocio" class="combo_180px">
                                </select> 
                            </td>   
                            <td colspan="2">
                                <label>Empresa<span style="color:red;">*</span></label>
                                <select id="empresa_fianza">
                                </select> 
                            </td>
                            <td colspan="2">
                                <label for="periodo_corte">Per�odo<span style="color:red;">*</span></label>
                                <input type="text" name="periodo_corte" id="periodo_corte" placeholder="AAAAMM" style="width:70px" class="solo-numero" maxlength="6">                       
                            </td>                   
                            <td>
                                <button id="listarNegocios" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                        role="button" aria-disabled="false">
                                    <span class="ui-button-text">Buscar</span>
                                </button>
                            </td>
                        </tr>  
                    </table>                   
                </div>
                <br><br>
                <table id="tabla_reporte_reclamaciones"></table>
                <div id="page_tabla_reporte_reclamaciones"></div>
            </center>

            <div id="divSalidaEx" title="Exportacion" style=" display: block" >
                <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
                <center>
                    <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
                </center>
                <div id="respEx" style=" display: none"></div>
            </div>       
        </div>
        <div id="dialogLoading" style="display:none;">
                <p  style="font-size: 12px;text-align:justify;" id="msj2">Texto </p> <br/>
                <center>
                    <img src="./images/cargandoCM.gif"/>
                </center>
            </div>
            <div id="dialogMsj" title="Mensaje" style="display:none;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div> 
        <script type="text/javascript">  
               initReclamaciones();    
        </script>
    </body>
</html>
