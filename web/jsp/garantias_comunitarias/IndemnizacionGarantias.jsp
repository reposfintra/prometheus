<%-- 
    Document   : IndemnizacionGarantias
    Created on : 4/10/2016, 07:52:35 AM
    Author     : mcastillo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Indemnizacion a Garantias</title>

        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link href="./css/indemnizacion_fianza.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/loaderApi.css">
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>

        <script type="text/javascript" src="./js/indemnizacion_garantias.js"></script>
        <STYLE>
        .cvteste {
            background-color: rgba(33, 150, 243, 0.24);
        }
        </STYLE>
    </head>
    
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=INDEMNIZACION GARANTIAS"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:80%; z-index:0; left: 0px; top: 100px; ">
            <br>
            <div id="busqueda">             
                <div id="divInterno">
                    <center>
                        <div>
                            <div style="display: inline-block">
                                <label>Unidad Negocio</label><span style="color: red"> *</span> &nbsp;
                                <select id="unidad_negocio">                           
                                </select>  
                                <label style="display:none;">Cartera en</label><span style="color: red;display:none;"> *</span> &nbsp;
                                <select id="cartera_en" style="display:none;">  </select> 
                                <label>Empresa<span style="color:red;">*</span></label>
                                <select id="empresa_fianza">
                                </select> 
                                <label>Per�odo</label><span style="color: red"> *</span> &nbsp;
                                <input type="text" id="foto" value="" placeholder="AAAAMM" style="width: 55px;" class="solo-numero" maxlength="6">
                                <!--                   <label>Fecha corte</label><span style="color: red"> *</span> &nbsp;
                                                        <input type="text" id="fecha_inicio" class="inpt" readonly  style="width: 90px;">-->
                            </div>


<!--                            <div id="busqueda_avanzada">                  
                                <label>mora</label>
                                <select id="mora">                           
                                </select>
                                <label>C�dula: </label>
                                <input type="text" id="cedula_cliente" value="" maxlength="15" style="width: 90px;" class="solo-numero">
                                <label>Negocio: </label>
                                <input type="text" id="negocio" value="" style="width: 70px;" class="mayuscula">                   
                                <label>Monto: $</label>
                                <input type="text" id="monto_inicial" value="" style="width: 70px;" class="solo-numeric">
                                <span>a </span>
                                <input type="text" id="monto_final" value="" style="width: 70px;" class="solo-numeric">
                                <input type="checkbox" id="chk_acelerar_pagare" name="chk_acelerar_pagare">Aceleraci�n de pagar�
                                <input type="checkbox" id="chk_gac" name="chk_gac">GAC
                            </div>-->
                            <img  src='/fintra/images/More.png' id='mas' name ="mas" style="width: 42px;height: 40px;margin-bottom: -16px;" title="Busqueda avanzada" onclick="busqueda_avanzada(this.id)"/>
<!--                            <button id="buscarAvanzada" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only boton" style="margin-left: 10px;margin-top: -3px;"
                                    role="button" aria-disabled="false">
                                <span id="mas" class="ui-button-text">Mas...</span>
                            </button>-->
                        </div>
                        <div id="filtro_avanzada" style="margin-top: 18px;margin-bottom: 20px;">
                            <center>
                                <hr style="width: 100%;background: #d8d8d8;border: medium none gray; height: 3px;">
                                <label>mora</label>
                                <select id="mora"> </select>
                                <label>C�dula: </label>
                                <input type="text" id="cedula_cliente" value="" maxlength="15" style="width: 90px;" class="solo-numero">
                                <label>Negocio: </label>
                                <input type="text" id="negocio" value="" style="width: 70px;" class="mayuscula">                   
                                <label>Monto: $</label>
                                <input type="text" id="monto_inicial" value="" style="width: 70px;" class="solo-numeric">
                                <span>a </span>
                                <input type="text" id="monto_final" value="" style="width: 70px;" class="solo-numeric">
                                <input type="checkbox" id="chk_acelerar_pagare" name="chk_acelerar_pagare">Aceleraci�n de pagar�
                                <input type="checkbox" id="chk_gac" name="chk_gac">GAC
                                <input type="checkbox" id="chk_ixm" name="chk_ixm">IXM
                                <hr style="width: 100%;background: #d8d8d8;border: medium none gray; height: 3px;">
                            </center>
                        </div>
                        <div style="display: block;margin-top: 10px;" >
                            
                                <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 10px;margin-top: -3px;"
                                        role="button" aria-disabled="false">
                                    <span class="ui-button-text">Buscar</span>
                                </button>

                                <button id="ver_facturas" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 10px;margin-top: -3px;"
                                        role="button" aria-disabled="false">
                                    <span class="ui-button-text">Ver indemnizadas</span>
                                </button>
                            
                        </div>
                    </center>
                </div>

            </div>
            <br>
            <div id = "tabla" style="margin-top: 100px">
                <center>
                    <table id="tbl_facturas"></table>
                    <div id="page_facturas"></div>
                </center>
            </div>
            <div id="dialogo" title="Mensaje">
                <p id="msj"></p>

            </div>
            <div id="dialogLoading"  class="ventana" >
                <p  id="msj2">texto </p> <br/>
                <center>
                    <img src="./images/cargandoCM.gif"/>
                </center>
            </div>

            <div id="divSalidaEx" title="Exportacion" style=" display: block" >
                <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
                <center>
                    <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
                </center>
                <div id="respEx" style=" display: none"></div>
            </div> 
            
            <div id="div_info_negocio"  style="display: none; width: 750px" >
                <center>
                    <br>
                    <table id="tabla_info_negocio"></table>
                    <div id="page_tabla_info_negocio"></div>    
                </center>
            </div>
            
            <div id="loader-wrapper" style="display: none">
                <img src="http://www.fintra.co/wp-content/uploads/2016/04/fintrafooter.png" alt="" class='loaderimg' />
                <div id="loader">          </div>
                <div class="loader-section section-left"></div>
                <div class="loader-section section-right"></div>
            </div>

        </div>
    </body>
</html>
