<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*" %>
<%@page import="com.tsp.util.UtilFinanzas" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>

<%
    String Ano = model.ReportesSvc.getAno();
    String Mes = model.ReportesSvc.getMes();
    
    int anchoCol = 29; 
    int restoCol = 680;
    
    String estiloPtoMyEjt = "style='color:red;        font-weight:bold;'";
    String estiloPtoMnEjt = "style='color:darkgreen;  font-weight:bold;'";
    
    int today = Integer.parseInt(UtilFinanzas.getFechaActual_String(8));
%>

<html>
    <head><title>Reporte [Viajes Presupuestados Vs Viajes Ejecuados] Por Estandar</title>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
	<script language="javaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
    </head>
    <body onResize="redimensionar()" onLoad="redimensionar()">
		<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 			<jsp:include page="/toptsp.jsp?encabezado=Reporte Por Estandar"/>
		</div>
	<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<% 
    List listado = model.ReportesSvc.getListado();
    if (listado.size()>0){
%>
            <%
                for (int i_ag = 0 ; i_ag < listado.size(); i_ag++){
                    ViajesAgencia ag = (ViajesAgencia) listado.get(i_ag);
            %>
			<table border='2' >            
			<tr> 
				<td>
					<table width="100%">
						<td width="25%" class="subtitulo1">AGENCIA [<%= ag.getAgencia() %>] <%= ag.getAgenciaNombre() %></td>
						<td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
					</table>
				</td> 
			</tr>  
            <tr><td>
                <!-- ******************************* CLIENTES   ************************************ -->
                <%
                    for (int i_cl = 0 ; i_cl < ag.getListadoClientes().size(); i_cl++)      {
                        ViajesCliente cl = (ViajesCliente) ag.getListadoClientes().get(i_cl); 
                %>
                <table border="0">                
				<tr> 
					<td>
						<table width="100%">
							<td width="25%" class="subtitulo1"><b>CLIENTE [<%= cl.getCliente() %>] <%= cl.getClienteNombre() %></b></td>
							<td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
						</table>
					</td> 
				</tr>
                <tr><td>
                <!-- **************************** ESTANDAR ********************************** -->
                <table border='0' cellpadding='1' cellspacing='1' width='<%= (anchoCol*62)+restoCol %>'>
                        <tr class='tblTitulo'>
                        <th rowspan='2'>Estandar   </th>
                        <th rowspan='2'>Descripcion</th>
                        <th rowspan='2'>Presup<br>Mensual   </th>
                        <th rowspan='2'>Presup<br>a la Fecha</th>
                        <th rowspan='2'>Ejecut<br>a la Fecha</th>
                        <th rowspan='2'>Difer</th>
                        <th rowspan='2'>Porcen<br>Cumpl</th>
                        <% for (int ix_st = 1 ; ix_st <= 31 ; ix_st++ ) out.print("<th colspan='2'>"+ UtilFinanzas.DiaSemana(Ano + Mes + UtilFinanzas.DiaFormat(ix_st)) + " " + ix_st +"</th>"); %>
                        </tr>
                        
                        <tr class='tblTitulo' >
                        <% for (int ix_st = 1 ; ix_st <= 31 ; ix_st++ ) out.print("<th>Pt</th><th>Ej</th>"); %>
                        </tr>
                        
                        <%
                            for (int i_st = 0 ; i_st < cl.getListaStandar().size(); i_st++)      {
                                ViajesStandar st = (ViajesStandar) cl.getListaStandar().get(i_st);
                        %>
                        <tr class=<%=(i_st % 2 == 0 )?"filagris":"filaazul"%>>
                            <td class='bordereporte' width='50' align='center'><%= st.getStdJobNo()   %></td>
                            <td class='bordereporte' width='350'><%= st.getStdJobDesc()               %></td>
                            <td class='bordereporte' width='70' align='center'><%= st.getTotalPt()    %></td>
                            <td class='bordereporte' width='70' align='center'><%= st.getTotalPtF()   %></td>
                            <td class='bordereporte' width='70' align='center'><%= st.getTotalEj()    %></td>
                            <td class='bordereporte' width='70' align='center'><%= st.getDiferencia() %></td>
                            <td class='bordereporte' width='70' align='center'><%= UtilFinanzas.customFormat2(st.getPorcentajeIncumplimiento()) %></td>
                            <% for (int ix_st = 1; ix_st <= 31; ix_st++) {
                                String estilo = "";
                                int fecha = Integer.parseInt(st.getAno() + st.getMes() + UtilFinanzas.DiaFormat(ix_st));
                                if (fecha <= today ){
                                    estilo = ((st.getViajePtdo(ix_st) > st.getViajeEjdo(ix_st))?estiloPtoMyEjt:
                                              (st.getViajePtdo(ix_st) < st.getViajeEjdo(ix_st))?estiloPtoMnEjt:"");
                                }
                            %>
                            <td class='bordereporte' align='center' width='<%= anchoCol %>' <%= estilo %>><%= (st.getViajePtdo(ix_st)==0?"-":String.valueOf(st.getViajePtdo(ix_st))) %></td>
                            <td class='bordereporte' align='center' width='<%= anchoCol %>' <%= estilo %>><%= (st.getViajeEjdo(ix_st)==0?"-":String.valueOf(st.getViajeEjdo(ix_st))) %></td>
                            <%  } %>
                        </tr>
                        <%  } %>
                        <tr class='filagris'>
                            <td class='bordereporte' align='center' colspan='2'>Totales Acumulados por cliente</td>
                            <td class='bordereporte' align='center'><%= cl.getTotalPt()    %></td>
                            <td class='bordereporte' align='center'><%= cl.getTotalPtF()   %></td>
                            <td class='bordereporte' align='center'><%= cl.getTotalEj()    %></td>
                            <td class='bordereporte' align='center'><%= cl.getDiferencia() %></td>
                            <td class='bordereporte' align='center'><%= UtilFinanzas.customFormat2(cl.getPorcentajeIncumplimiento()) %></td>
                            <% for (int ix_cl = 1; ix_cl <= 31; ix_cl++) {
                                String estilo = "";
                                int fecha = Integer.parseInt(cl.getAno() + cl.getMes() + UtilFinanzas.DiaFormat(ix_cl));
                                if (fecha <= today ){
                                    estilo = ((cl.getViajePtdo(ix_cl) > cl.getViajeEjdo(ix_cl))?estiloPtoMyEjt:
                                              (cl.getViajePtdo(ix_cl) < cl.getViajeEjdo(ix_cl))?estiloPtoMnEjt:"");
                                }
                            %>
                            
                            <td class='bordereporte' align='center' <%= estilo%> ><%= (cl.getViajePtdo(ix_cl)==0?"-":String.valueOf(cl.getViajePtdo(ix_cl))) %></td>
                            <td class='bordereporte' align='center' <%= estilo%> ><%= (cl.getViajeEjdo(ix_cl)==0?"-":String.valueOf(cl.getViajeEjdo(ix_cl))) %></td>
                            <%  } %>

                        </tr>
                        
                    </table>
                <!-- *****************************  FIN ESTANDAR ***************************** -->
                </td></tr>
                </table>
                <%  } %>
                
                <!-- *****************************  FIN CLIENTES  ************************************ -->
                </td>
            </tr>
            <tr>
            <td>
            <!-- TOTALES AGENCIAS -->
                <br><br>
                <table border='1'>
                <tr><td>
                <table border='0' cellpadding='1' cellspacing='1' width='<%= (anchoCol*62)+restoCol %>'>
                        <tr class='tblTitulo'>
                        <th rowspan='2' colspan='2' >Totales Acumulados por Agencia</th>
                        <th rowspan='2'>Presup<br>Mensual   </th>
                        <th rowspan='2'>Presup<br>a la Fecha</th>
                        <th rowspan='2'>Ejecut<br>a la Fecha</th>
                        <th rowspan='2'>Difer</th>
                        <th rowspan='2'>Porcen<br>Cumpl</th>
                        <% for (int ix_ag = 1 ; ix_ag <= 31 ; ix_ag++ ) out.print("<th colspan='2' >"+ UtilFinanzas.DiaSemana(Ano + Mes + UtilFinanzas.DiaFormat(ix_ag)) + " " + ix_ag +"</th>"); %>
                        </tr>
                        
                        <tr class='tblTitulo' >
                        <% for (int ix_ag = 1 ; ix_ag <= 31 ; ix_ag++ ) out.print("<th >Pt</th><th >Ej</th>"); %>
                        </tr>
                

                    <tr class='filagris'>
                        <td class='bordereporte' width='400'align='center' colspan='2'> <%= ag.getAgenciaNombre() %> </td>
                        <td class='bordereporte' width='70' align='center'><%= ag.getTotalPt()    %></td>
                        <td class='bordereporte' width='70' align='center'><%= ag.getTotalPtF()   %></td>
                        <td class='bordereporte' width='70' align='center'><%= ag.getTotalEj()    %></td>
                        <td class='bordereporte' width='70' align='center'><%= ag.getDiferencia() %></td>
                        <td class='bordereporte' width='70' align='center'><%= UtilFinanzas.customFormat2(ag.getPorcentajeIncumplimiento()) %></td>
                        <% for (int ix_ag = 1; ix_ag <= 31; ix_ag++) {
                                String estilo = "";
                                int fecha = Integer.parseInt(ag.getAno() + ag.getMes() + UtilFinanzas.DiaFormat(ix_ag));
                                if (fecha <= today ){
                                    estilo = ((ag.getViajePtdo(ix_ag) > ag.getViajeEjdo(ix_ag))?estiloPtoMyEjt:
                                              (ag.getViajePtdo(ix_ag) < ag.getViajeEjdo(ix_ag))?estiloPtoMnEjt:"");
                                }
                        %>
                        <td class='bordereporte' align='center' width='<%= anchoCol %>' <%= estilo%> ><%= (ag.getViajePtdo(ix_ag)==0?"-":String.valueOf(ag.getViajePtdo(ix_ag))) %></td>
                        <td class='bordereporte' align='center' width='<%= anchoCol %>' <%= estilo%> ><%= (ag.getViajeEjdo(ix_ag)==0?"-":String.valueOf(ag.getViajeEjdo(ix_ag))) %></td>
                        <%  } %>
                    </tr>
                </table>                
                </td></tr></table>
            <!-- FIN TOTALES AGENCIAS -->
            </td>
            </tr>
        </table>
        <br><br>
        <%  } // end for %>        
        
<%   }     // end if %>
<table><img src="<%=BASEURL%>/images/botones/salir.gif" width="90" height="21" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></table>
    </div>
    </body>
</html>
