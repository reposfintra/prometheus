<%!
    public String convertir(String cadena){
        return cadena.replaceAll("'", "");
    }
%>

<%= model.DPtoSvc.getVarJSSeparador() %>
<%
    String SEPARADOR = "~";
    List ListaAgenciaClienteStandar = model.DPtoSvc.getListadoAgenciasClientesStandar();
    out.print(" var datos = [ ");
    if (ListaAgenciaClienteStandar!=null){
        for (int i=0; i<ListaAgenciaClienteStandar.size(); i++){
            DatosEstandar dt = (DatosEstandar) ListaAgenciaClienteStandar.get(i);
            String item = "\n['"+ dt.getCodigoAgencia()  +SEPARADOR+ convertir(dt.getDescripcionAgencia())  + SEPARADOR +
            dt.getCodigoCliente()   + SEPARADOR + convertir(dt.getDescripcionCliente())   + SEPARADOR +
            dt.getCodigoEstandar()  + SEPARADOR + convertir(dt.getDescripcionEstandar())  + SEPARADOR +
            dt.getCodigoOrigen()    + SEPARADOR + convertir(dt.getDescripcionOrigen())    + SEPARADOR +
            dt.getCodigoDestino()   + SEPARADOR + convertir(dt.getDescripcionDestino())   +  "']";
            out.print (item);
            if ((i+1)!=ListaAgenciaClienteStandar.size()) out.print(",");
        }
    }
    out.print("];");
%>
