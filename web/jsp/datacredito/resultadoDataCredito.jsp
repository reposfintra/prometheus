<%-- 
    Document   : resultadoDataCredito
    Created on : 19/07/2010, 02:12:26 PM
    Author     : maltamiranda
--%>

<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ page import    ="java.util.*" %>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/default.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/mac_os_x.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilotabla.css" rel="stylesheet" type="text/css">
<script src="<%=BASEURL%>/js/boton.js" type="text/javascript"></script>
<script src="<%=BASEURL%>/js/prototype.js" type="text/javascript"></script>
<script src="<%=BASEURL%>/js/datacredito.js" type="text/javascript"></script>
<script src="<%=BASEURL%>/js/effects.js" type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window.js" type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window_effects.js" type="text/javascript"></script>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
    </head>
    <body>
        <form name="formulario" id="formulario" action="" >
        <center>
        <%
            ArrayList arl;
            String opcion=request.getParameter("opcion");
            if(opcion.equals("subtabla")){
                arl= (ArrayList) session.getAttribute("acumulados");
            }
            else{
                arl= (ArrayList) session.getAttribute("datacredito");
            }
            int lineas_x_pagina=500;
            int paginas_visibles=10; 
            int pagina = Integer.parseInt((String) request.getParameter("pagina"));
            int paginas=((arl.size()-1) % lineas_x_pagina==0)?(arl.size()-1) / lineas_x_pagina:(arl.size()-1) / lineas_x_pagina+1;
            if(paginas>0)
            {if(pagina>=paginas)
            {   pagina--;
            }%>
        <div class="fixedHeaderTable">
            <table border="1">
                <%  int inicio = pagina*lineas_x_pagina+1;
                    int sw=0;
                    for (int i = 0; i < ((inicio+lineas_x_pagina<(arl.size()-1))?inicio+lineas_x_pagina:(arl.size())); i++) {
                        ArrayList arl2 = ((ArrayList) arl.get(i));%>
                        <%if(i==0){%>
                            <thead>
                        <%}%>
                        <%if(i==inicio){%>
                            <tbody style="height: <%=(!opcion.equals("subtabla"))?350:500%>px">
                        <%}%>
                        <tr id="fila<%=i%>"<%= (i == 0) ? " class='fila' align='center' " : " class='fila' align='center' style='cursor: pointer'  onmouseover=\"color_fila('fila" + i + "','orange');\" onmouseout=\"color_fila('fila" + i + "','#EAFFEA');\""%>>
                        <%if (i != 0) {%>
                        <td>&nbsp;<input type="checkbox" value="<%=(String) arl2.get(3)%>" name="obl"></td>
                        <%} else {%>
                            <th>&nbsp;<input type="checkbox" value="<%=(String) arl2.get(3)%>" id="All" onClick="Sell_all_col('obl');"></th>
                            <%i = inicio-1;sw=1;
                        }%>
                        <%for (int j = 0; j < arl2.size(); j++) {
                            if(i!=0&&sw!=1){%>
                            <td nowrap>&nbsp;<%=(String) arl2.get(j)%></td>
                            <%}else{%>
                            <th id="head<%=j%>" style="cursor: pointer" onmouseover="color_fila(this.id,'#00CC00')" onmouseout="color_fila(this.id,'green')" onclick="enviar('<%=CONTROLLER%>?estado=Reporte&accion=DataCredito&pagina=0&opcion=<%=opcion%>','ORDENAMIENTO','formulario','<%=j+1%>');" >&nbsp;<%=(String) arl2.get(j)%></th>
                            <%}
                        }%>
                        </tr>
                        <%sw=0;
                        if(i==((inicio+lineas_x_pagina<(arl.size()-1))?inicio+lineas_x_pagina:(arl.size()))-1){%>
                            </tbody>
                        <%}%>
                        <%if(i==inicio-1){%>
                            </thead>
                        <%}%>
                <%}%>
            </table><!--</form>-->
        </div>

            <%if ((arl.size()-1) > lineas_x_pagina) {%>
            <br>
            <%  if (pagina - paginas_visibles/2 >= 0) {
                    if (pagina + paginas_visibles/2 >paginas) {
                        if (paginas - paginas_visibles > 0) {
                            inicio = paginas - paginas_visibles;
                        } else {
                            inicio = 0;
                        }
                    } else {
                        inicio = pagina - paginas_visibles/2;
                    }

                } else {
                    inicio = 0;
                }
                %>
                <a onclick="enviar('<%=CONTROLLER%>?estado=Reporte&accion=DataCredito&pagina=<%=0%>&opcion=<%=opcion%>','PAGINACION','formulario','');" style='font-weight: bold;cursor: pointer'><%="<<"%>&nbsp;</a>
                <%
                for(int k=inicio;k<((inicio+paginas_visibles < paginas) ? inicio + paginas_visibles : paginas); k++) {%>
                    <a onclick="enviar('<%=CONTROLLER%>?estado=Reporte&accion=DataCredito&pagina=<%=k%>&opcion=<%=opcion%>','PAGINACION','formulario','');" <%=(pagina == k) ? "style='font-weight: bold;cursor: pointer'" : "style='cursor: pointer'"%>><%=k + 1%>&nbsp;</a>
                <%}%>
                <a onclick="enviar('<%=CONTROLLER%>?estado=Reporte&accion=DataCredito&pagina=<%=paginas-1%>&opcion=<%=opcion%>','PAGINACION','formulario','');" style='font-weight: bold;cursor: pointer'><%=">>"%>&nbsp;</a>
           <%}%>
           <br><br>
           <%   if(!opcion.equals("subtabla")){%>
                    <img src="<%=BASEURL%>/images/botones/agregar.gif" alt="" onMouseOver="botonOver(this);" onClick="enviar('<%=CONTROLLER%>?estado=Reporte&accion=DataCredito&pagina=<%=pagina%>&opcion=<%=opcion%>','AGREGAR_ACUMULADO','formulario','');" onMouseOut="botonOut(this);" style="cursor:hand">
                <%}
                else{%>
                    <img src="<%=BASEURL%>/images/botones/generar.gif" alt="" onMouseOver="botonOver(this);" onClick="enviar('<%=CONTROLLER%>?estado=Reporte&accion=DataCredito&pagina=<%=pagina%>&opcion=<%=opcion%>','GENERAR_REPORTE','formulario','');" onMouseOut="botonOut(this);" style="cursor:hand">
                    <img src="<%=BASEURL%>/images/botones/exportarExcel.gif" alt="" onMouseOver="botonOver(this);" onClick="enviar('<%=CONTROLLER%>?estado=Reporte&accion=DataCredito&pagina=<%=pagina%>&opcion=<%=opcion%>','EXPORTAR_EXCEL','formulario','');" onMouseOut="botonOut(this);" style="cursor:hand">
                    <img src="<%=BASEURL%>/images/botones/eliminar.gif" alt="" onMouseOver="botonOver(this);" onClick="enviar('<%=CONTROLLER%>?estado=Reporte&accion=DataCredito&pagina=<%=pagina%>&opcion=<%=opcion%>','ELIMINAR_ACUMULADOS','formulario','');" onMouseOut="botonOut(this);" style="cursor:hand">
                    <img src="<%=BASEURL%>/images/botones/restablecer.gif" alt="" onMouseOver="botonOver(this);" onClick="enviar('<%=CONTROLLER%>?estado=Reporte&accion=DataCredito&pagina=<%=pagina%>&opcion=<%=opcion%>','LIMPIAR_ACUMULADOS','formulario','');" onMouseOut="botonOut(this);" style="cursor:hand">
                    <img onmouseout="this.src='/fintravalores/images/botones/salir.gif'" onmouseover="this.src='/fintravalores/images/botones/salirOver.gif'" onclick="parent.wclose();" style="" src="/fintravalores/images/botones/salir.gif">
                <%}
            }%>
        <br><%
            if(request.getParameter("opcion")!=null){
                arl= (ArrayList) session.getAttribute("acumulados");
            }%>
    </center>
    </form>
    </body>
</html>
