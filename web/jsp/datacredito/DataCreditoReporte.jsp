<%--
    Document   : DataCreditoReporte
    Created on : 15/05/2013, 10:20:31 AM
    Author     : aariza
--%>


<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>

<%@page import="com.tsp.operation.model.beans.listaPoliticaBeans"%>
<%@page import="com.tsp.operation.model.ListaPoliticasReporteService"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>

<html>

    <%
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        ListaPoliticasReporteService lpservice = new ListaPoliticasReporteService(usuario.getBd());
        ArrayList<listaPoliticaBeans> lista = lpservice.getPoliticas();

    %>

    <head>
        <title>GENERADOR DE REPORTE A DATACREDITO</title>
<link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/jscal2.css" />
<link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/border-radius.css" />
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/default.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/mac_os_x.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilotabla.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
<script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>
<script src="<%=BASEURL%>/js/boton.js" type="text/javascript"></script>
<script src="<%=BASEURL%>/js/prototype.js" type="text/javascript"></script>
<script src="<%=BASEURL%>/js/datacredito.js" type="text/javascript"></script>
<script src="<%=BASEURL%>/js/effects.js" type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window.js" type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window_effects.js" type="text/javascript"></script>

    
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/> 
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>            
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <style>

            #div_espera {
                background-color:whitesmoke;
                position: absolute;
                left: 49%;
                top: 49%;
                width: 340px;
                height: 140px;
                margin-top: -100px;
                margin-left: -150px;
                overflow: auto;
                border:1px solid #D4D4D4;
                text-align:center;
                z-index:100;
            }

            .ui-th-column, .ui-jqgrid .ui-jqgrid-htable th.ui-th-column {
                border-bottom: 0 none;
                border-top: 0 none;
                font-size: 13px;
                overflow: hidden;
                text-align: center;
                white-space: nowrap;
            }
    
            .ui-widget-content {
                background: url("images/ui-bg_inset-soft_10_ffffff_1x100.png") repeat-x scroll 50% bottom #FFFFFF;
                border: 1px solid #008000;
                color: #0F0101;
                font-size: 12px;

            }

        </style>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=GENERADOR DE REPORTE A DATACREDITO"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <br>
            <center>
                    <input type="hidden" id="controller" value ="<%=CONTROLLER%>"/>
                    <input type="hidden" id="base" value ="<%=BASEURL%>"/>

                <table border="2">
                        <tr>
                            <td>
                                <!-- encabezado -->
                                
                                   <!-- negativos -->
                                <table class="tablaInferior" border="0" width="100%">
                                    <tr >
                                        <td width="50%" align="left" class="subtitulo1">&nbsp;Reporte</td>
                                        <td width="50%" align="left" class="barratitulo"><img src="<%= BASEURL%>/images/titulo.gif" alt="" width="32" height="20"  align="left"></td>
                                    </tr>
                                </table>
                                <!-- datos del formulario -->
                                <table class="tablaInferior" border="0">
                                    
                                     <tr class="fila">
                                        <td>&nbsp;&nbsp;Nombre Politica:&nbsp;&nbsp;</td>
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    <select id="selepol">
                                                   <option value="">Seleccione</option>
                                                        <%
                                                            Iterator<listaPoliticaBeans> iterator = lista.iterator();

                                                            while (iterator.hasNext()) {
                                                                listaPoliticaBeans beansLitas = iterator.next();
                        %>      
                                     <option value="<%=  beansLitas.getIdPolitica()%>"><%=  beansLitas.getPolitica()%></option>                 
                                                            
                             <% }%>                               
                                                    </select>
                                        </td>

                                    </tr>
                                        </table>
                                    </td>
                                </tr>
                                    
                                <tr class="fila">
                                    <td>&nbsp;&nbsp;Reportar A:&nbsp;&nbsp;</td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        datacredito <input type="radio" name="entidad" value="DATACREDITO" checked="true" />&nbsp;&nbsp;
                                        cifin <input type="radio" name="entidad" value="CIFIN" />

                                    </td>

                                </tr>

                                 </table>
                          
                         
                            </td>
                        </tr>
                    </table>
                    <br>
                <img src="<%=BASEURL%>/images/botones/aceptar.gif" id="preReporte_2" onMouseOver="botonOver(this);" onClick="preReporte2()" onMouseOut="botonOut(this);" style="cursor:pointer">
                <img src="<%=BASEURL%>/images/botones/generar.gif" alt=""  onMouseOver="botonOver(this);" onClick="generarReporte();" onMouseOut="botonOut(this);" style="cursor:pointer">
                    <tsp:boton value="salir" onclick="parent.close();"/>
                    <br><br>
                <div id="divTabla" style="display: none;">
                    <table id="grps" width="auto"  ></table>
                    <div id="pgrps" ></div>
                    <br>
                    <div>
                        <img src="<%=BASEURL%>/images/botones/exportarExcel.gif" onMouseOver="botonOver(this);" onclick="exportData('12','xls');" onMouseOut="botonOut(this);" style="cursor:pointer">
                        <img src="<%=BASEURL%>/images/botones/btnOcultar.gif" onMouseOver="botonOver(this);" onclick="$('#divTabla').css('display', 'none');" onMouseOut="botonOut(this);" style="cursor:pointer">
                        
                    </div>
                    
                </div>

                <div id="div_espera" class="center" style="display: none;">     

                    <h4 id="salida"></h4>
                    <img id="imgespera" style="margin-left: 45%" src="<%=BASEURL%>/images/bigrotation.gif" />

                    <div id="divSalida" class="center" style="display: none;"> 

                        <img src="<%=BASEURL%>/images/botones/VerListado.gif"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick="window.open('<%=CONTROLLER%>?estado=Ver&accion=Directorio&usuario=<%=usuario.getLogin()%>&evento=LOAD'); ocultarDIV();" style="cursor:pointer">
                        <img src="<%=BASEURL%>/images/botones/salir.gif"  onMouseOver="botonOver(this);" onClick="ocultarDIV()" onMouseOut="botonOut(this);" style="cursor:pointer">

                    </div>


                </div>


                <form id="form_expr" action="" method="post" name="form_expr">
                    <input type="hidden" name="pdfBuffer" id="pdfBuffer" value="" />
                    <input type="hidden" name="fileName" id="fileName" value="reporte_datacredito" />
                    <input type="hidden" name="fileType" id="fileType" value="" />
                </form>

            </center>
        </div>
    </body>
</html>