<%-- 
    Document   : ListaPoliticas
    Created on : 15/05/2013, 03:55:09 PM
    Author     : aariza
--%>

<%@page import="com.tsp.operation.model.beans.listaPoliticaBeans"%>
<%@page import="com.tsp.operation.model.ListaPoliticasReporteService"%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/jscal2.css" />
<link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/border-radius.css" />
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/default.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/mac_os_x.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilotabla.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
<script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>
<script src="<%=BASEURL%>/js/boton.js" type="text/javascript"></script>
<script src="<%=BASEURL%>/js/prototype.js" type="text/javascript"></script>
<script src="<%=BASEURL%>/js/datacredito.js" type="text/javascript"></script>
<script src="<%=BASEURL%>/js/effects.js" type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window.js" type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window_effects.js" type="text/javascript"></script>


<script language='javascript'>
  
  function accion(url,parametro){
         if(parametro=='AGREGAR'){
             window.open(url, '');
             return false;
             
         }else if(parametro=='MODIFICAR'){
             window.open(url,'')
             return false;
         }
       
            
  }
            
    </script>

<%
 Usuario usuario = (Usuario) session.getAttribute("Usuario");
 ListaPoliticasReporteService lpservice= new ListaPoliticasReporteService(usuario.getBd());
 ArrayList<listaPoliticaBeans> lista =  lpservice.getPoliticas();

%>

<html>
    <head>
        <title>POLITICAS DE GENERACION DE REPORTE</title>
    </head>
    <body onload="">
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=POLITICAS DE GENERACION DE REPORTE"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <br>
            <center>
                    <table border="2">
                        <tr>
                            <td>
                                <!-- encabezado -->
                            <input type="hidden" id="controller" value ="<%=CONTROLLER%>"/>
                                <table class="tablaInferior" border="0" width="100%">
                                    <tr >
                                        <td width="50%" align="left" class="subtitulo1">&nbsp;Lista de politicas para reporte</td>
                                        <td width="50%" align="left" class="barratitulo"><img src="<%= BASEURL%>/images/titulo.gif" alt="" width="32" height="20"  align="left"></td>
                                    </tr>
                                </table>
                                <!-- datos del formulario -->
                                <table class="tablaInferior" border="0">
                                    
                                    <tr class="fila"> 
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;Nombre Politica&nbsp;&nbsp;&nbsp;&nbsp;</td> 
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;Central de Riesgo&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;Convenio&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    </tr>
                                    
                                        <%
                                            Iterator<listaPoliticaBeans> iterator=lista.iterator();

                        while(iterator.hasNext()){%>
                    <tr>
                        <%     
                        listaPoliticaBeans beansLitas=iterator.next();
                        %>

                        <td class="bordereporte" align='center' nowrap style="font-size:11"><%=  beansLitas.getPolitica()%></td>
                        <td class="bordereporte" align='center' nowrap style="font-size:11"><%=  beansLitas.getCentralRiesgo() %></td>
                        <td class="bordereporte" align='center' nowrap style="font-size:11"><%=  beansLitas.getConvenio() %></td>
                        <td class="bordereporte" align='center' nowrap style="font-size:11"><img src="<%=BASEURL%>/images/botones/iconos/modificar.gif" onClick="accion('<%=BASEURL%>/jsp/datacredito/EditarCrearPoliticas.jsp?tipo=EDITAR&id=<%= beansLitas.getIdPolitica() %>&central=<%= beansLitas.getCentralRiesgo() %>&politica=<%=beansLitas.getPolitica()%>&idconv=<%= beansLitas.getIdConvenio() %>&bmonto=<%= beansLitas.isMonto() %>&bedad=<%= beansLitas.isEdad() %>&eini=<%= beansLitas.getEdadIni() %>&efin=<%= beansLitas.getEdadFin() %>&mini=<%= beansLitas.getMontoIni() %>&mfin=<%= beansLitas.getMontoFin() %>','MODIFICAR')" style="cursor:pointer"></td>
                        <td class="bordereporte" align='center' nowrap style="font-size:11"><img src="<%=BASEURL%>/images/botones/iconos/eliminar.gif"  onClick="borrarPolitica(<%= beansLitas.getIdPolitica() %>)" style="cursor:pointer"></td>

                        </tr>
                        <% }

                        %>

                                    
                                                                      
                                    
                                 </table>
                          
                         
                            </td>
                        </tr>
                    </table>
                    <br>
                    
                    <!-- href="<%=BASEURL%>/jsp/datacredito/EditarCrearPoliticas.jsp" -->
                    <img src="<%=BASEURL%>/images/botones/agregar.gif" onMouseOver="botonOver(this);" onClick="accion('<%=BASEURL%>/jsp/datacredito/EditarCrearPoliticas.jsp?tipo=AGREGAR','AGREGAR')" onMouseOut="botonOut(this);" style="cursor:pointer">
                    <tsp:boton value="salir" onclick="parent.close();"/>
                    <br><br>
                   <center class='comentario'>
                    <div id="comentario" style="visibility: hidden" >
                        <table border="2" align="center">
                            <tr>
                                <td>
                                    <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                        <tr>
                                            <td width="229" align="center" class="mensajes"><span class="normal"><div id="mensaje"></div></span></td>
                                            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                            <td width="58">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </center>     
            </center>
        </div>
    </body>
</html>