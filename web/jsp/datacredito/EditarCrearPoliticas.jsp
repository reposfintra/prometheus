<%-- 
    Document   : EditarPoliticas
    Created on : 15/05/2013, 03:34:04 PM
    Author     : aariza
--%>

<%@page import="com.tsp.operation.model.ListaPoliticasReporteService"%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="javax.servlet.*" %>
<%@ page import    ="javax.servlet.http.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/jscal2.css" />
<link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/border-radius.css" />
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/default.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/mac_os_x.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilotabla.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
<script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>
<script src="<%=BASEURL%>/js/boton.js" type="text/javascript"></script>
<script src="<%=BASEURL%>/js/prototype.js" type="text/javascript"></script>
<script src="<%=BASEURL%>/js/datacredito.js" type="text/javascript"></script>
<script src="<%=BASEURL%>/js/effects.js" type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window.js" type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window_effects.js" type="text/javascript"></script>

 <%
 String opcion = (request.getParameter("tipo") != null) ? request.getParameter("tipo") : "";
 String central = (request.getParameter("central") != null) ? request.getParameter("central") : ""; 
 String id= (request.getParameter("id") != null) ? request.getParameter("id") : "";
 String politica =  (request.getParameter("politica") != null) ? request.getParameter("politica") : "";
 String uneg =  (request.getParameter("uneg") != null) ? request.getParameter("uneg") : "";
 String idconv =  (request.getParameter("idconv") != null) ? request.getParameter("idconv") : "";
 String bmonto = (request.getParameter("bmonto") != null) ? request.getParameter("bmonto") : ""; 
 String bedad = (request.getParameter("bedad") != null) ? request.getParameter("bedad") : ""; 
 String eini = (request.getParameter("eini") != null) ? request.getParameter("eini") : ""; 
 String efin = (request.getParameter("efin") != null) ? request.getParameter("efin") : ""; 
 String mini = (request.getParameter("mini") != null) ? request.getParameter("mini") : ""; 
 String mfin = (request.getParameter("mfin") != null) ? request.getParameter("mfin") : ""; 

 

 ListaPoliticasReporteService lpservice= new ListaPoliticasReporteService();
 ArrayList listaentidad = lpservice.getEntidades();

%>

<html>
    <head>
        <title>EDITAR/AGREGAR POLITICAS DE GENERACION DE REPORTE</title>
    </head>
    <body onload="">
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=EDITAR/AGREGAR POLITICAS DE GENERACION DE REPORTE"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <br>
            <center>
                    <table border="2">
                        <tr>
                            <td>
                                <!-- encabezado -->
                                
                                <table class="tablaInferior" border="0" width="100%">
                                    <tr>
                                        <td width="50%" align="left" class="subtitulo1">&nbsp;Parametros Reporte</td>
                                        <td width="50%" align="left" class="barratitulo"><img src="<%= BASEURL%>/images/titulo.gif" alt="" width="32" height="20"  align="left"></td>
                                    </tr>
                                </table>
                                <!-- datos del formulario -->
                                <table class="tablaInferior" border="0">
                                    <input type="hidden" id="controller" value ="<%=CONTROLLER%>"/>
                                    <input type="hidden" id="idpolitica" value ="<%=id%>"/>
                                    <input type="hidden" id="idconvenio" value ="<%=idconv%>"/>
                                     <tr class="fila">
                                        <td>&nbsp;&nbsp;Central de Riesgo:&nbsp;&nbsp;</td>
                                        <td><br><table><tr><td>
                                                       <select id="centralries">
                                                        <option value="">Seleccione</option> 
                                                        <option value="DATACREDITO" <%if(central.equals("DATACREDITO")){ %> selected="selected" <% }%> >DATACREDITO</option>
                                                        <option value="SIFIN" <%if(central.equals("SIFIN")){ %> selected="selected" <% }%> >SIFIN</option>
                                                       </select>
                                                                                                          </td></tr></table>
                                        </td>
                                     </tr>
                                    
                                     <tr class="fila">
                                        <td>&nbsp;&nbsp;Nombre Politica:&nbsp;&nbsp;</td>
                                        <td><br><table><tr><td><input name="nompolit" id="nompolit" type="text" class="textbox" value="<%=politica%>" style="width:200px;"></td></tr></table>
                                        </td>
                                    </tr>
                                    
                                    <tr class="fila">
                                       <td>&nbsp;&nbsp;Entidad Que Reporta:&nbsp;&nbsp;</td>
                                        <td>
                                            <br><table><tr><td><select id="seleuneg" onchange="cargarConvenios()">
                                                        <option value="">Seleccione</option>
                                                                              <% 

                                                String[] dato2 = null;
                                                for (int i = 0; i < listaentidad.size(); i++) {
                                                dato2 = ((String) listaentidad.get(i)).split("-"); %>
                                                <option value="<%=dato2[0] %>"><%= dato2[1]%></option>
                                                <%    }

                                                %>
                                                              </select></td></tr></table>
                                        </td>
                                    </tr>
                                   <tr class="fila">
                                       <td>&nbsp;&nbsp;Convenio:&nbsp;&nbsp;</td>
                                        <td>
                                            <div id="selconvenios"> <br><table><tr><td><select id="seleconv">
                                                            <option value="">Seleccione</option>
                                                        </select></td></tr></table>
                                             </div>
                                        </td>
                                    </tr>
                                    
                                                                      
                                    <tr class="fila">
                                        <td><input type="checkbox" name="checkedad" id ="checkedad" value ="checkedad" <% if(bedad.equals("true")){ %> checked="checked"<% }%> >&nbsp;&nbsp;Edad Mora:&nbsp;&nbsp;</td>
                                        <td><br><table><tr><td><input name="edadini" id="edadini" type="text" class="textbox" style="width:80px;" size="15" onkeypress="return isNumberKey(event);" value="<%=eini%>"> - <input name="edadfin" id="edadfin" type="text" class="textbox" style="width:80px;" size="15" onkeypress="return isNumberKey(event);" value="<%=efin%>"> </td></tr></table>
                                        </td>
                                    </tr>
                                    <tr class="fila">
                                 
                                        <td><input type="checkbox" name="checkmonto" id="checkmonto" value ="checkmonto" <% if(bmonto.equals("true")){ %> checked="checked"<% }%> >&nbsp;&nbsp;Monto:&nbsp;&nbsp;</td>
                                        <td>
                                           <br><table><tr><td>$&nbsp;<input name="montoini" id="montoini" type="text" class="textbox" style="width:80px;" size="15" onkeypress="return isNumberKey(event);" value="<%=mini%>"> - $&nbsp;<input name="montofin" id="montofin" type="text" class="textbox" style="width:80px;" size="15" onkeypress="return isNumberKey(event);" value="<%=mfin%>"></td></tr></table>
                                        </td>
                                    </tr>
                               </table>
                          </td>
                        </tr>
                    </table>
                    <br>
                    <% if(opcion.equals("EDITAR")){ %>
                    <img src="<%=BASEURL%>/images/botones/guardar.png" alt=""  onMouseOver="botonOver(this);" onClick="actualizarPolitica()" onMouseOut="botonOut(this);" style="cursor:pointer">
                    <%
                      }else if(opcion.equals("AGREGAR")){ %>
                    <img src="<%=BASEURL%>/images/botones/agregar.png" alt=""  onMouseOver="botonOver(this);" onClick="agregarPolitica()" onMouseOut="botonOut(this);" style="cursor:pointer">        
                     
                    <%      
                      }

                    %>
                    
                    <tsp:boton value="salir" onclick="parent.close();"/>
                    <br><br>
                    
                <center class='comentario'>
                    <div id="comentario" style="visibility: hidden" >
                        <table border="2" align="center">
                            <tr>
                                <td>
                                    <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                        <tr>
                                            <td width="229" align="center" class="mensajes"><span class="normal"><div id="mensaje"></div></span></td>
                                            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                            <td width="58">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </center>  
                    
            </center>
                    
             
        </div>
    </body>
</html>