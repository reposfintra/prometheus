<%--
    Document   : GestionInteresMora
    Created on : 21/06/2010, 03:24:31 PM
    Author     : maltamiranda
--%>

<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/jscal2.css" />
<link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/border-radius.css" />
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/default.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/mac_os_x.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilotabla.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
<script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>
<script src="<%=BASEURL%>/js/boton.js" type="text/javascript"></script>
<script src="<%=BASEURL%>/js/prototype.js" type="text/javascript"></script>
<script src="<%=BASEURL%>/js/datacredito.js" type="text/javascript"></script>
<script src="<%=BASEURL%>/js/effects.js" type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window.js" type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window_effects.js" type="text/javascript"></script>

<html>
    <head>
        <title>GESTION INTERESES MORA</title>
    </head>
    <body onload="sendAction('<%=CONTROLLER%>?estado=Reporte&accion=DataCredito','','idclie','clselect','');sendAction('<%=CONTROLLER%>?estado=Reporte&accion=DataCredito','','idafl','aflselect','');init();">
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=GENERADOR DE REPORTE A DATACREDITO"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <br>
            <center>
                    <table border="2">
                        <tr>
                            <td>
                                <!-- encabezado -->
                                <table class="tablaInferior" border="0" width="100%">
                                    <tr >
                                        <td width="50%" align="left" class="subtitulo1">&nbsp;Parametros Reporte</td>
                                        <td width="50%" align="left" class="barratitulo"><img src="<%= BASEURL%>/images/titulo.gif" alt="" width="32" height="20"  align="left"></td>
                                    </tr>
                                </table>
                                <!-- datos del formulario -->
                                <table class="tablaInferior" border="0">
                                    <tr class="fila">
                                        <td>&nbsp;&nbsp;Nombre Afiliado:&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;<input name="nomafl" id="nomafl" type="text" class="textbox" style="width:200px;"  onChange="sendAction('<%=CONTROLLER%>?estado=Reporte&accion=DataCredito',document.getElementById('nomafl').value,'idafl','aflselect','');" size="15">
                                            <br><table><tr><td><div id="aflselect"></div></td><td><img src="<%=BASEURL%>/images/cargando.gif" alt="" id="imgworkingII" style="visibility: hidden"></td></tr></table>
                                        </td>
                                    </tr>
                                    <tr class="fila">
                                        <td>&nbsp;&nbsp;Nombre Cliente:&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;<input name="nomclie" id="nomclie" type="text" class="textbox" style="width:200px;"  onChange="sendAction('<%=CONTROLLER%>?estado=Reporte&accion=DataCredito',document.getElementById('nomclie').value,'idclie','clselect','');" size="15">
                                            <br><table><tr><td><div id="cliselect"></div></td><td><img src="<%=BASEURL%>/images/cargando.gif" alt="" id="imgworking" style="visibility: hidden"></td></tr></table>
                                        </td>
                                    </tr>
                                    <tr class="fila">
                                        <td>&nbsp;&nbsp;Codigo Cliente:&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;<input name="codcli" id="codcli" type="text" class="textbox" style="width:60px;" size="15">
                                        </td>
                                    </tr>
                                    <tr class="fila">
                                        <td>&nbsp;&nbsp;Fecha Ultimo Reporte:&nbsp;&nbsp;</td>
                                        <td>
                                            &nbsp;&nbsp;<input id="fechaReporte" name="fechaReporte" type="text" style="text-align: center" readonly>
                                            <img name="popcal" style="cursor:pointer" id="calendar-triggeri" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt="">
                                        </td>
                                    </tr>

                                    <tr class="fila">
                                        <td>&nbsp;&nbsp;Rango de vencimiento en dias:&nbsp;&nbsp;</td>
                                        <td>
                                            &nbsp;&nbsp;<input id="rangoi" name="rangoi" type="text" style="width: 60px" onkeyup="soloNumeros(this.id)"> &nbsp;&nbsp;a&nbsp;&nbsp;&nbsp;<input id="rangof" name="rangof" type="text" style="width: 60px" onkeyup="soloNumeros(this.id)">
                                        </td>
                                    </tr>
                                    <tr class="fila">
                                        <td>&nbsp;&nbsp;Negocio:&nbsp;&nbsp;</td>
                                        <td>&nbsp;&nbsp;<input type="text" value="" id="negocio" name="negocio"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    <br>
                    <img src="<%=BASEURL%>/images/botones/buscar.gif" alt=""  onMouseOver="botonOver(this);" onClick="enviar('<%=CONTROLLER%>?estado=Reporte&accion=DataCredito&opcion=tablaprincipal','BUSCAR_NEGOCIOS','formulario','');" onMouseOut="botonOut(this);" style="cursor:pointer">
                    <img src="<%=BASEURL%>/images/botones/VerListado.gif" alt=""  onMouseOver="botonOver(this);" onClick="enviar('<%=CONTROLLER%>?estado=Reporte&accion=DataCredito&opcion=subtabla','VER_ACUMULADOS','formulario','');" onMouseOut="botonOut(this);" style="cursor:pointer">
                    <tsp:boton value="salir" onclick="parent.close();"/>
                    <br><br>
                    <div id="tabla_generica"></div>
                    <div id="tabla_genericaII" style="width: 100%;height: 100%;background-color: #EEEEEE; color: #000; font-size: 14px;"></div>
            </center>
        </div>
    </body>
</html>
