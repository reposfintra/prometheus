<%-- 
    Document   : MaestroEntidadReporta
    Created on : 13/06/2013, 09:12:47 AM
    Author     : aariza
--%>


<%@page import="com.tsp.operation.model.ListaPoliticasReporteService"%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="javax.servlet.*" %>
<%@ page import    ="javax.servlet.http.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/jscal2.css" />
<link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/border-radius.css" />
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/default.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/alert.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/alphacube.css" rel="stylesheet" type="text/css"/>
<link href="<%=BASEURL%>/css/mac_os_x.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilotabla.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
<script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>
<script src="<%=BASEURL%>/js/boton.js" type="text/javascript"></script>
<script src="<%=BASEURL%>/js/prototype.js" type="text/javascript"></script>
<script src="<%=BASEURL%>/js/datacredito.js" type="text/javascript"></script>
<script src="<%=BASEURL%>/js/effects.js" type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window.js" type="text/javascript"></script>
<script src="<%=BASEURL%>/js/window_effects.js" type="text/javascript"></script>

 <%
 ListaPoliticasReporteService lpservice= new ListaPoliticasReporteService();
 ArrayList listaentidad =  lpservice.getEntidades();
 ArrayList listaconvenios =  lpservice.getConvenios();
 ArrayList listaentconv = lpservice.getEntidadConvenio();
%>

<html>
    <head>
        <title>MAESTRO ENTIDADES QUE REPORTAN</title>
    </head>
    <body onload="">
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=MAESTRO ENTIDADES QUE REPORTAN"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <br>
            <center>
                    <table border="2">
                        <tr>
                            <td>
                                <!-- encabezado -->
                           <input type="hidden" id="controller" value ="<%=CONTROLLER%>"/>     
                                <table class="tablaInferior" border="0" width="100%">
                                    <tr >
                                        <td width="50%" align="left" class="subtitulo1">&nbsp;Maestro Entidades</td>
                                        <td width="50%" align="left" class="barratitulo"><img src="<%= BASEURL%>/images/titulo.gif" alt="" width="32" height="20"  align="left"></td>
                                    </tr>
                                </table>
                                <!-- datos del formulario -->
                                <table class="tablaInferior" border="0">
                                    <input type="hidden" id="controller" value ="<%=CONTROLLER%>"/>
                         
                                    <tr class="fila">
                                 
                                        <td>&nbsp;&nbsp;Entidad que reporta:&nbsp;&nbsp;</td>
                                        <td>
                                        <br><table><tr><td>
                                                    <select id="entidad">
                                                        
                                                        
                                                        <option value="">Seleccione</option>
                                                <% 

                                                String[] dato1 = null;
                                                for (int i = 0; i < listaentidad.size(); i++) {
                                                dato1 = ((String) listaentidad.get(i)).split("-"); %>
                                                <option value="<%=dato1[0] %>"><%= dato1[1]%></option>
                                                <%    }

                                                %>
                                                    
                                                    
                                                    </select>
                                                
                                                </td></tr></table>   
                                        </td>
                                        <td>&nbsp;&nbsp;&nbsp;&nbsp;<img src="<%=BASEURL%>/images/botones/agregar.png" alt=""  onMouseOver="botonOver(this);" onClick="agregar()" onMouseOut="botonOut(this);" style="cursor:pointer">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                    </tr>
                           <tr class="fila">
                                 
                                        <td>&nbsp;&nbsp;Convenio:&nbsp;&nbsp;</td>
                                        <td>
                                        <br><table><tr><td>
                                                    <select id="conve">
                                                        
                                                        
                                                        <option value="">Seleccione</option>
                                                <% 

                                                String[] conven = null;
                                                for (int i = 0; i < listaconvenios.size(); i++) {
                                                conven = ((String) listaconvenios.get(i)).split("-"); %>
                                                <option value="<%=conven[0] %>"><%= conven[1]%></option>
                                                <%    }

                                                %>
                                                    
                                                    
                                                    </select>
                                                
                                                </td>
                                                <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                            </tr></table>   
                                        </td>
                                    </tr>
                                  
                               </table>
                           </td>
                        </tr>
                    </table>
                    <br>
                   <img src="<%=BASEURL%>/images/botones/aceptar.png" alt=""  onMouseOver="botonOver(this);" onClick="agregarEntidadConvenio()" onMouseOut="botonOut(this);" style="cursor:pointer">        
                    
                    <tsp:boton value="salir" onclick="parent.close();"/>
                    <br><br>
               
                    <div id="agrent" style="visibility: hidden" >
                        <table border="2" align="center">
                            <tr class="fila">
                               
                                   <td>&nbsp;&nbsp;Nombre Entidad:&nbsp;&nbsp;</td>
                                        <td><br><table><tr><td><input name="noment" id="noment" type="text" class="textbox" style="width:200px;"></td></tr></table>
                                        </td>
                                        
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;<img src="<%=BASEURL%>/images/botones/agregar.png" alt=""  onMouseOver="botonOver(this);" onClick="agregarEntidad()" onMouseOut="botonOut(this);" style="cursor:pointer">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                            </tr>
                        </table>
                    </div>
                    
                    
                <center class='comentario'>
                    <div id="comentario" style="visibility: hidden" >
                        <table border="2" align="center">
                            <tr>
                                <td>
                                    <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                        <tr>
                                            <td width="229" align="center" class="mensajes"><span class="normal"><div id="mensaje"></div></span></td>
                                            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                            <td width="58">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </center> 
                                            
                                            
                                            <table border="2">
                        <tr>
                            <td>
                                <!-- encabezado -->
                                
                                <table class="tablaInferior" border="0" width="100%">
                                    <tr>
                                        <td width="50%" align="left" class="subtitulo1">&nbsp;Parametros Reporte</td>
                                        <td width="50%" align="left" class="barratitulo"><img src="<%= BASEURL%>/images/titulo.gif" alt="" width="32" height="20"  align="left"></td>
                                    </tr>
                                </table>
                                <!-- datos del formulario -->
                                <table class="tablaInferior" border="0">
                                   <tr class="fila">
                                       <td>&nbsp;&nbsp;Entidad&nbsp;&nbsp;</td>
                                       <td>&nbsp;&nbsp;Convenio&nbsp;&nbsp;</td>
                                         <td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                     </tr>
                                     
                                     
                                           
                                                     
                               <% 

                                String[] dato4 = null;
                                for (int i = 0; i < listaentconv.size(); i++) {
                                    
                                    %>
                                <tr>
                                     <%
                                dato4 = ((String) listaentconv.get(i)).split("-"); %>
                                    <td class="bordereporte" align='center' nowrap style="font-size:11"><%=dato4[0] %></td>
                                    <td class="bordereporte" align='center' nowrap style="font-size:11"><%=dato4[1] %></td>
                                    <td class="bordereporte" align='center' nowrap style="font-size:11"><img src="<%=BASEURL%>/images/botones/iconos/eliminar.gif"  onClick="borrarEntidadConvenio(<%=dato4[2] %>)" style="cursor:pointer"></td>
                                 </tr>
                                <%    }

                                %>

                                                                   
                             
                               </table>
                          </td>
                        </tr>
                    </table>
                    
            </center>
                    
             
        </div>
    </body>
</html>