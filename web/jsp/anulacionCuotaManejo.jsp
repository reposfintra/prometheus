<%-- 
    Document   : anulacionCuotaManejo
    Created on : 10/08/2016, 06:13:06 PM
    Author     : egonzalez
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css" />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>  
        <script type="text/javascript" src="./js/anulacionCuotaManejo.js"></script> 
        <title>ANULACION CUOTA ADMINISTRACION</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=ANULACION CUOTA ADMINISTRACION"/>
        </div>
        <style>
            input{    
                width: 09px;
                color: black;
            }
        </style>
    <center>
        <div id="tablita" style="top: 170px;width: 412px;height: 95px;" >
            <div class="ui-dialog-titlebar ui-widget-header ui-helper-clearfix">
                <span class="ui-dialog-title text-center" id="ui-dialog-title-listaGlobal">
                    &nbsp;
                    <label class="titulotablita" ><b>FILTRO DE BUSQUEDA</b></label>
                </span>
            </div>
            <div class="row" style="padding: 5px">
                <div class="col-md-12 ">
                    <table id="tablainterna" style="height: 53px; width: 214px"  >
                        <tr>
                            <td>
                                <label>Negocio</label>
                            </td>
                            <td>
                                <input type="text" id="negocio" name="negocio" style="height: 15px;width: 110px;color: #070708;" >
                            </td>
                            <td>
                                <hr style="width: 2px;height: 39px;top: -25px;margin-left: 13px;color: rgba(128, 128, 128, 0.39);" /> 
                            </td>
                            <td>
                                <div style="padding-top: 10px">
                                    <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="margin-left: 15px;top: -7px;" > 
                                        <span class="ui-button-text">Buscar</span>
                                    </button>  
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div style="position: relative;top: 231px;">
            <table id="tabla_negocios" ></table>
            <div id="pager"></div>
        </div>
        <div id="info"  class="ventana" >
            <p id="notific">EXITO AL GUARDAR</p>
        </div>
    </center>
</body>
</html>
