<%-- 
    Document   : asociar_tipo_req_usuario
    Created on : 27/01/2016, 09:49:55 AM
    Author     : mcastillo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
     <head>
         <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Relacionar usuarios tipos prioridad</title>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>  
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>         
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>            
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <link type="text/css" rel="stylesheet" href="./css/popup.css"/>
        <link href="./css/style_azul.css" rel="stylesheet" type="text/css">
        <link href="./css/requisiciones.css" rel="stylesheet" type="text/css"/>
        <script type="text/javascript" src="./js/usuarios_requisicion.js"></script>   
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
             <jsp:include page="/toptsp.jsp?encabezado=Relacionar usuarios tipos prioridad requisición"/>
        </div>
       <div id="capaCentral" style="position:absolute; width:100%; height:83%; z-index:0; left: 0px; top: 100px; overflow:auto;">    
           <br>
    <center>
        <div id="div_asignar_tipo_requisicion"> 
           <div id="encabezadotablita" style="width: 795px">
                <label class="titulotablita"><b>RELACIONAR USUARIOS Y TIPOS DE REQUISICION</b></label>
           </div> 
           <div id="div_usuarios" style="position: absolute; top:40px; width: 300px ">    
                     <table border="0"  align="center">
                         <tr>
                             <td>
                                 <table id="listUsuarios" ></table>
                             </td>
                         </tr>
                     </table>   
             </div>
             <div id="div_tipos_requisicion" style="position:absolute; left: 430px; top:40px; width: 300px ">    
                 <table border="0"  align="center">
                     <tr>
                         <td>
                             <table id="listTiposReqUsuario" ></table>
                             <div id="page_tabla_tipo_req_usuario"></div>
                         </td>
                     </tr>
                 </table>   
             </div>
        </div>    
       </div>
       <div id="dialogMsg" title="Mensaje" style="display:none;">
            <p style="font-size: 12.5px;text-align:justify;" id="msj" > Texto </p>
       </div>           
    </body>
</html>
