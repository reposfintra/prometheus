<%--
    Document   : Modulo de Requisiciones Fintra
    Created on : 10/07/2013, 11:00:00 AM
    Author     : hcuello
--%>

<%@page session="true" %>
<%@ page import="com.tsp.operation.model.beans.Usuario"%>
<%@ page import="com.tsp.operation.model.beans.CmbGenericoBeans"%>
<%@ page import="com.tsp.operation.model.services.RequisicionesService"%>
<%@ page import="com.tsp.operation.model.beans.RequisicionesListadoBeans"%>


<!-- page contentType="text/html" pageEncoding="ISO-8859-1" -->
<!-- page contentType="text/html" pageEncoding="UTF-8" -->

<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="java.text.SimpleDateFormat" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="javax.servlet.*" %>
<%@ page import    ="javax.servlet.http.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>



<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String NombreUsuario = usuario.getNombre();
    String NmLogin = usuario.getLogin();
    
    String ano = request.getParameter("ano") != "" ? request.getParameter("ano") : "";
    String mes = request.getParameter("mes") != "" ? request.getParameter("mes") : "";
    //int proceso = Integer.parseInt(request.getParameter("proceso") != "" ? request.getParameter("proceso") : "0");
    String proceso = request.getParameter("proceso");
    
    RequisicionesService rqservice= new RequisicionesService(usuario.getBd());
    ArrayList CdroEstadisticas =  rqservice.RequisicionesIndicadores("SQL_INDICADORES_RQ",ano,mes,proceso);
    
    if ( CdroEstadisticas.size() > 0 ) { %>

        <table width="900" border="0" align="center" cellpadding="0" cellspacing="0">

            <tr><td><input name="broadcast" type="hidden" id="broadcast"><input type="hidden" name="meshd" id="meshd"></td></tr>

            <tr><td><span class="labels">REGISTRO DE INDICADORES</span></td></tr>

            <tr><td bgcolor="#165FB6"><img src="/sap_v2/clear.gif" width="1" height="1" /></td></tr>
            <tr><td>&nbsp;</td></tr>

            <tr>
                <td>
                    <table width="800" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#B4D2F3" class="labels">

                        <tr>
                          <td colspan="2"><div align="center">INDICADOR 1 </div></td>
                          <td width="161" bgcolor="#B4D2F3"><div align="center" class="style1">DATOS</div></td>
                          <td width="30" bgcolor="#B4D2F3"><div align="center" class="style1">ENE</div></td>
                          <td width="29" bgcolor="#B4D2F3"><div align="center" class="style1">FEB</div></td>
                          <td width="31" bgcolor="#B4D2F3"><div align="center" class="style1">MAR</div></td>
                          <td width="30" bgcolor="#B4D2F3"><div align="center" class="style1">ABR</div></td>
                          <td width="31" bgcolor="#B4D2F3"><div align="center" class="style1">MAY</div></td>
                          <td width="29" bgcolor="#B4D2F3"><p align="center" class="style1">JUN</p></td>
                          <td width="29" bgcolor="#B4D2F3"><div align="center" class="style1">JUL</div></td>
                          <td width="31" bgcolor="#B4D2F3"><div align="center" class="style1">AGO</div></td>
                          <td width="30" bgcolor="#B4D2F3"><div align="center" class="style1">SEP</div></td>
                          <td width="29" bgcolor="#B4D2F3"><div align="center" class="style1">OCT</div></td>
                          <td width="30" bgcolor="#B4D2F3"><div align="center" class="style1">NOV</div></td>
                          <td width="28" bgcolor="#B4D2F3"><div align="center" class="style1">DIC</div></td>
                          <td width="70" bgcolor="#B4D2F3"><div align="center" class="style1">Acumulado</div></td>
                        </tr>

                        <tr>
                            <td colspan="2" rowspan="2" align="center" bgcolor="#B4D2F3" class="style1">EFICACIA DE RESPUESTA A LAS SOLICITUDES ENVIADAS</td>
                            <td bgcolor="#B4D2F3">No. Total de requisiciones presentadas</td> <%
                            RequisicionesListadoBeans ListadoContenido = (RequisicionesListadoBeans) CdroEstadisticas.get(0);
                            %>
                            <td align="right"><div align="center"><%=ListadoContenido.getDescYearEne()%></div></td>
                            <td align="right"><div align="center"><%=ListadoContenido.getDescYearFeb()%></div></td>
                            <td align="right"><div align="center"><%=ListadoContenido.getDescYearMar()%></div></td>
                            <td align="right"><div align="center"><%=ListadoContenido.getDescYearAbr()%></div></td>
                            <td align="right"><div align="center"><%=ListadoContenido.getDescYearMay()%></div></td>
                            <td align="right"><div align="center"><%=ListadoContenido.getDescYearJun()%></div></td>
                            <td align="right"><div align="center"><%=ListadoContenido.getDescYearJul()%></div></td>
                            <td align="right"><div align="center"><%=ListadoContenido.getDescYearAgo()%></div></td>
                            <td align="right"><div align="center"><%=ListadoContenido.getDescYearSep()%></div></td>
                            <td align="right"><div align="center"><%=ListadoContenido.getDescYearOct()%></div></td>
                            <td align="right"><div align="center"><%=ListadoContenido.getDescYearNov()%></div></td>
                            <td align="right"><div align="center"><%=ListadoContenido.getDescYearDic()%></div></td>
                            <td align="right"><div align="center"><strong><span class="style23"><%=ListadoContenido.getDescYearEne()%></span></strong></div></td> <!-- Acumulado -->  
                        </tr>

                        <tr>
                            <td bgcolor="#B4D2F3">No. Total de requisiciones cerradas a satisfaccion</td> <%
                            RequisicionesListadoBeans ListadoContenido2 = (RequisicionesListadoBeans) CdroEstadisticas.get(1);
                            %>
                            <td align="right"><div align="center"><%=ListadoContenido2.getDescYearEne()%></div></td>
                            <td align="right"><div align="center"><%=ListadoContenido2.getDescYearFeb()%></div></td>
                            <td align="right"><div align="center"><%=ListadoContenido2.getDescYearMar()%></div></td>
                            <td align="right"><div align="center"><%=ListadoContenido2.getDescYearAbr()%></div></td>
                            <td align="right"><div align="center"><%=ListadoContenido2.getDescYearMay()%></div></td>
                            <td align="right"><div align="center"><%=ListadoContenido2.getDescYearJun()%></div></td>
                            <td align="right"><div align="center"><%=ListadoContenido2.getDescYearJul()%></div></td>
                            <td align="right"><div align="center"><%=ListadoContenido2.getDescYearAgo()%></div></td>
                            <td align="right"><div align="center"><%=ListadoContenido2.getDescYearSep()%></div></td>
                            <td align="right"><div align="center"><%=ListadoContenido2.getDescYearOct()%></div></td>
                            <td align="right"><div align="center"><%=ListadoContenido2.getDescYearNov()%></div></td>
                            <td align="right"><div align="center"><%=ListadoContenido2.getDescYearDic() %></div></td>
                            <td align="right"><div align="center"><strong><span class="style23"><%=ListadoContenido2.getDescAcumulado() %></span></strong></div></td>    <!-- Acumulado -->   
                        </tr>

                        <tr>
                            <td width="84" bgcolor="#B4D2F3"><div align="center" class="style1">META </div></td>
                            <td width="94" bgcolor="#B4D2F3"><div align="center" class="style1">90% Creciente</div></td>
                            <td bgcolor="#B4D2F3"><div align="center" class="style1">RESULTADO</div></td><%
                            RequisicionesListadoBeans ListadoContenido3 = (RequisicionesListadoBeans) CdroEstadisticas.get(2);
                            %>
                            <td height="30"><div align="center"><strong><span class="style23"><%=ListadoContenido3.getDescYearEne()%>%</span></strong></div></td> <!-- Ciclo FOR -->
                            <td><div align="center"><strong><span class="style23"><%=ListadoContenido3.getDescYearFeb()%>%</span></strong></div></td>
                            <td><div align="center"><strong><span class="style23"><%=ListadoContenido3.getDescYearMar()%>%</span></strong></div></td>
                            <td><div align="center"><strong><span class="style23"><%=ListadoContenido3.getDescYearAbr()%>%</span></strong></div></td>
                            <td><div align="center"><strong><span class="style23"><%=ListadoContenido3.getDescYearMay()%>%</span></strong></div></td>
                            <td><div align="center"><strong><span class="style23"><%=ListadoContenido3.getDescYearJun()%>%</span></strong></div></td>
                            <td><div align="center"><strong><span class="style23"><%=ListadoContenido3.getDescYearJul()%>%</span></strong></div></td>
                            <td><div align="center"><strong><span class="style23"><%=ListadoContenido3.getDescYearAgo()%>%</span></strong></div></td>
                            <td><div align="center"><strong><span class="style23"><%=ListadoContenido3.getDescYearSep()%>%</span></strong></div></td>
                            <td><div align="center"><strong><span class="style23"><%=ListadoContenido3.getDescYearOct()%>%</span></strong></div></td>
                            <td><div align="center"><strong><span class="style23"><%=ListadoContenido3.getDescYearNov()%>%</span></strong></div></td>
                            <td><div align="center"><strong><span class="style23"><%=ListadoContenido3.getDescYearDic()%>%</span></strong></div></td>
                            <td align="right"><div align="center">-</div></td>
                        </tr>

                    </table>
                </td>
            </tr>

            <tr><td>&nbsp;</td></tr>

            <tr>
                <td>
                    <table width="800" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#B4D2F3" class="labels">
                        <tr>
                          <td colspan="2"><div align="center">INDICADOR 2 </div></td>
                          <td width="64" bgcolor="#B4D2F3"><div align="center" class="style1">META</div></td>
                          <td width="178" bgcolor="#B4D2F3"><div align="center"><span class="style1">DATOS</span></div></td>
                          <td width="30" bgcolor="#B4D2F3"><div align="center" class="style1">ENE</div></td>
                          <td width="30" bgcolor="#B4D2F3"><div align="center" class="style1">FEB</div></td>
                          <td width="30" bgcolor="#B4D2F3"><div align="center" class="style1">MAR</div></td>
                          <td width="30" bgcolor="#B4D2F3"><div align="center" class="style1">ABR</div></td>
                          <td width="30" bgcolor="#B4D2F3"><div align="center" class="style1">MAY</div></td>
                          <td width="30" bgcolor="#B4D2F3"><p align="center" class="style1">JUN</p></td>
                          <td width="30" bgcolor="#B4D2F3"><div align="center" class="style1">JUL</div></td>
                          <td width="30" bgcolor="#B4D2F3"><div align="center" class="style1">AGO</div></td>
                          <td width="30" bgcolor="#B4D2F3"><div align="center" class="style1">SEP</div></td>
                          <td width="30" bgcolor="#B4D2F3"><div align="center" class="style1">OCT</div></td>
                          <td width="30" bgcolor="#B4D2F3"><div align="center" class="style1">NOV</div></td>
                          <td width="30" bgcolor="#B4D2F3"><div align="center" class="style1">DIC</div></td>
                          <td width="70" bgcolor="#B4D2F3">&nbsp;</td>
                        </tr>
                        <tr>
                          <td colspan="2" align="center" bgcolor="#B4D2F3" class="style1">EFICIENCIA DE RESPUESTA A LAS REQUISICIONES ENVIADAS</td>
                          <td bgcolor="#B4D2F3"><div align="center">3 Decreciente </div></td>
                          <td bgcolor="#B4D2F3"><div align="center">Tiempo de respuesta a las solicitudes de soporte implementado en el mes </div></td><%
                            RequisicionesListadoBeans ListadoContenido4 = (RequisicionesListadoBeans) CdroEstadisticas.get(3);
                            %>
                          <td align="center"><%=ListadoContenido4.getDescYearEne()%></td>
                          <td align="center"><%=ListadoContenido4.getDescYearFeb()%></td>
                          <td align="center"><%=ListadoContenido4.getDescYearMar()%></td>
                          <td align="center"><%=ListadoContenido4.getDescYearAbr()%></td>
                          <td align="center"><%=ListadoContenido4.getDescYearMay()%></td>
                          <td align="center"><%=ListadoContenido4.getDescYearJun()%></td>
                          <td align="center"><%=ListadoContenido4.getDescYearJul()%></td>
                          <td align="center"><%=ListadoContenido4.getDescYearAgo()%></td>
                          <td align="center"><%=ListadoContenido4.getDescYearSep()%></td>
                          <td align="center"><%=ListadoContenido4.getDescYearOct()%></td>
                          <td align="center"><%=ListadoContenido4.getDescYearNov()%></td>
                          <td align="center"><%=ListadoContenido4.getDescYearDic()%></td>
                          <td align="right">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>

            <tr><td>&nbsp;</td></tr>
            <tr><td></td></tr>

            <tr>
              <td><table width="800" border="0" align="center" cellpadding="1" cellspacing="1">
                  <tr>
                    <td width="50%" align="center"></td>
                    <td align="center"></td>
                  </tr>
                </table></td>
            </tr>

            <tr><td>&nbsp;</td></tr>

            <tr><td><span class="labels">OTROS INDICADORES </span></td></tr>
            <tr><td bgcolor="#165FB6"><img src="/sap_v2/clear.gif" width="1" height="1" /></td></tr>
            <tr><td>&nbsp;</td></tr><%
            RequisicionesListadoBeans ListadoContenido5 = (RequisicionesListadoBeans) CdroEstadisticas.get(4);%>

            <tr>
                <td>
                    <table width="750" border="1" align="center" cellpadding="0" cellspacing="0" bordercolor="#B4D2F3">
                        <tr>
                            <td width="280" bgcolor="#B4D2F3"><span class="labels">RQS ATENDIDAS EN MES ACTUAL </span></td>
                            <td width="60" bgcolor="#fcfcfc"><div align="center"><strong><span class="labels"><%=ListadoContenido5.getDescYearEne()%></span></strong></div></td>
                            <td width="300" bgcolor="#B4D2F3"><span class="labels">CERRADAS EN SU MES</span></td>
                            <td width="60" bgcolor="#fcfcfc" class="labels"><div align="center"><strong><span class="style23"><%=ListadoContenido5.getDescYearFeb()%></span></strong></div></td>
                            <td width="250" bgcolor="#B4D2F3" class="labels">REQUISICIONES CRONOGRAMADAS PENDIENTESE</td>
                            <td width="60" bgcolor="#fcfcfc" class="labels" align="center"><%=ListadoContenido5.getDescYearMar()%></td>
                        </tr>
                        <tr>
                            <td width="280" bgcolor="#B4D2F3" class="labels">PENDIENTES MES ACTUAL </td>
                            <td width="60" bgcolor="#fcfcfc" class="labels"><div align="center"><%=ListadoContenido5.getDescYearAbr()%></div></td>
                            <td width="300" bgcolor="#B4D2F3" class="labels">CERRADAS FUERA DE SU MES</td>
                            <td width="60" bgcolor="#fcfcfc" class="labels"><div align="center"><strong><span class="style23"><%=ListadoContenido5.getDescYearMay()%></span></strong></div></td>
                            <td width="250" bgcolor="#B4D2F3" class="labels">REQUISICIONES CRONOGRAMADAS CERRADAS</td>
                            <td width="60" align="center" bgcolor="#fcfcfc" class="labels"><%=ListadoContenido5.getDescYearJun()%></td>
                        </tr>
                        <tr>
                            <td width="280" bgcolor="#B4D2F3" class="labels">&nbsp;</td> <!-- TOTAL MARCADAS COMO  TECNICA A&Ntilde;O -->
                            <td width="60" bgcolor="#fcfcfc" class="labels"><div align="center"><strong></strong></div></td>
                            <td width="300" bgcolor="#B4D2F3" class="labels">CERRADAS MES ACT. NO DEL MES ACT.</td>
                            <td width="60" bgcolor="#fcfcfc" class="labels"><div align="center"><strong><span class="style23"><%=ListadoContenido5.getDescYearJul()%></span></strong></div></td>
                            <td width="250" bgcolor="#B4D2F3" class="labels">&nbsp;</td> <!-- REQUISICIONES PENDIENTES SOPORTE -->
                            <td width="60" align="center" bgcolor="#fcfcfc" class="labels"><a href="requisiciones_pendientes.php?proceso_sgc=11"></a></td>
                        </tr>
                        <!-- 
                        <tr>
                            <td width="280" bgcolor="#B4D2F3" class="labels">REQUISICIONES ATENDIDAS EN EL A&Ntilde;O</td>
                            <td width="60" bgcolor="#fcfcfc" class="labels"><div align="center"><strong></strong></div></td>
                            <td width="300" bgcolor="#B4D2F3" class="labels">TOTAL A&Ntilde;O</td>
                            <td width="60" bgcolor="#fcfcfc" class="labels"><div align="center"><strong></strong></div></td>
                            <td width="250" bgcolor="#B4D2F3" class="labels">&nbsp;</td>
                            <td width="60" bgcolor="#fcfcfc" class="labels">&nbsp;</td>
                        </tr> -->
                    </table>    
                </td>        
            </tr>

        </table>
        <%
        
    }else{ %>
        
        <table width="800" border="0" align="center" cellpadding="1" cellspacing="1">
                <tr><td width="50%" align="center"></td></tr>
        </table><%
    }
        %>
