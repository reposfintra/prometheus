<%--
    Document   : Modulo de Requisiciones Fintra
    Created on : 10/07/2013, 11:00:00 AM
    Author     : hcuello
--%>

<%@ page import="com.tsp.operation.model.beans.Usuario"%>
<%@ page import="com.tsp.operation.model.beans.CmbGenericoBeans"%>
<%@ page import="com.tsp.operation.model.services.RequisicionesService"%>
<%@ page import="com.tsp.operation.model.beans.RequisicionesListadoBeans"%>

<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="java.text.SimpleDateFormat" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>


<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String NombreUsuario = usuario.getNombre();
    String NmLogin = usuario.getLogin();
    
    String QryProceso = "";
    int idReq = 0;
    int IdProceso = 0;
    String QryTipReq = "";
    int IdTipReq = 0;
    String Asunto = "";
    String Descripcion = "";
    
    String evento = "";
    evento = request.getParameter("ACCIONE");
    
    String estado = request.getParameter("estado") != "" ? request.getParameter("estado") : "";
    String mes = request.getParameter("mes") != "" ? request.getParameter("mes") : "";
    String ano = request.getParameter("ano") != "" ? request.getParameter("ano") : "";
    String proceso = request.getParameter("proceso") != "" ? request.getParameter("proceso") : "";
    String item = request.getParameter("ITEM") != "" ? request.getParameter("ITEM") : "";
    String filtro = "NewEditar";
   
    RequisicionesService rqservice= new RequisicionesService();
    
    QryProceso = "SQL_OBTENER_COMBO_PROCESOS_SGC";
    QryTipReq = "SQL_OBTENER_TIPO_REQUISICION";

    if (evento.equals("EDITAR") ) {
        
        ArrayList listaRequisiciones =  rqservice.RequisicionesListadoBeans("SQL_OBTENER_LISTADO_REQUISICIONES",estado,mes,ano,proceso,NmLogin,item,filtro);
        RequisicionesListadoBeans ListadoContenido = (RequisicionesListadoBeans) listaRequisiciones.get(0);

        idReq = ListadoContenido.getId();
        IdProceso = ListadoContenido.getIdProcesoSgc();
        IdTipReq = ListadoContenido.getIdTipoRequisicion();
        Asunto = ListadoContenido.getAsunto();
        Descripcion = ListadoContenido.getDescripcion();
        
    }else if ( evento.equals("AGREGAR") ) {
        
                
    }

    ArrayList listaCombo =  rqservice.GetComboGenerico(QryProceso,"id","descripcion","");
    ArrayList listaComboTypeReq =  rqservice.GetComboGenerico(QryTipReq,"id","descripcion","");

%>


<form name="form_nueva_rq" id="form_nueva_rq" method="post" enctype="multipart/form-data" action="almacenar_nueva_requisicion.php" target="iframeUpload">

<input  type="hidden" name="contador_articulos" id="contador_articulos" value="0" />

<table align="center" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF">
    <!--
    <tr>
        <td class="titulo_ventana" id="drag_nueva_requisicion">
            <div style="float:left">REGISTRAR NUEVA REQUISICION	<input type="hidden" name="id_usuario" id="id_usuario" value="coco"/></div> 
            <div style="float:right" onClick="$('#div_nueva_requisicion').fadeOut('slow')"><a class="ui-widget-header ui-corner-all"><span>X</span></a></div>
        </td>
    </tr>
    -->
    
    <tr>
        <td id="drag_nueva_requisicion" style="cursor:hand">
            <div class="k-header">
                <span class="titulo">Nueva Requisicion</span>
                <span class="ui-widget-header ui-corner-all titulo_right" onClick="$('#div_nueva_requisicion').fadeOut('slow')">x</span>
            </div>
        </td>
    </tr>    
    
    <tr><td>&nbsp;</td></tr>    
    
    <tr>
        <td>
            <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                
                <tr>
                    <td width="168">GENERADA POR <%=evento %></td>
                    <td width="263"><%=NombreUsuario %></td>
                </tr>
                
                <tr>
                    <td >AL PROCESO</td>
                    <td align="left">

                        <select name="proceso"  id="proceso" class="combo_300px" onChange="mostrar_detalle(this.value)"><%
                            if ( listaCombo.size() > 0 ) { %>

                                <OPTION value='0' selected>-- SELECCIONAR --</OPTION><%

                                for (int i = 0; i < listaCombo.size(); i++) {

                                    CmbGenericoBeans CmbContenido = (CmbGenericoBeans) listaCombo.get(i);

                                    if ( CmbContenido.getIdCmb() == IdProceso ) {%>
                                        <option value="<%=CmbContenido.getIdCmb() %>" selected><%=CmbContenido.getDescripcionCmb() %></option> <%
                                    }else{ %>
                                        <option value="<%=CmbContenido.getIdCmb() %>"><%=CmbContenido.getDescripcionCmb() %></option> <%
                                    }
                                }

                            }else{%>

                                <OPTION value=''>No se han ingresado items</OPTION><%
                            }%>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td >TIPO REQUISICION</td>
                    <td align="left">
                        <select name="tipo_req"  id="tipo_req" class="combo_300px" onChange="mostrar_detalle(this.value)"><%
                            if ( listaCombo.size() > 0 ) { %>

                                <OPTION value='0' selected>-- SELECCIONAR --</OPTION><%

                                for (int i = 0; i < listaComboTypeReq.size(); i++) {

                                    CmbGenericoBeans CmbTipoReq = (CmbGenericoBeans) listaComboTypeReq.get(i);

                                    if ( CmbTipoReq.getIdCmb() == IdTipReq ) {%>
                                        <option value="<%=CmbTipoReq.getIdCmb() %>" selected><%=CmbTipoReq.getDescripcionCmb() %></option> <%
                                    }else{ %>
                                        <option value="<%=CmbTipoReq.getIdCmb() %>"><%=CmbTipoReq.getDescripcionCmb() %></option> <%
                                    }
                                }

                            }else{%>        
                                <OPTION value=''>No se han ingresado items</OPTION><%
                            }%>
                        </select>
                </td>
            </tr>

            <tr>
                <td>ASUNTO</td>
                <td><input name="asunto" type="text" class="textbox_300" id="asunto" value="<%=Asunto %>"  ></td>
            </tr>
            
            <tr>
                <td valign="top">DESCRIPCION</td>
                <td><textarea name="descripcion" class="textbox_300x100px" id="descripcion"><%=Descripcion %></textarea></td>
            </tr>
            <tr>
                <td colspan="2" align="left" valign="middle"><fieldset>
                    <legend class="labels">ARCHIVOS RELACIONADOS</legend>
                    <table width="350" border="0" align="left" cellpadding="1" cellspacing="1" class="labels">
                        <tr>
                            <td>
                                <table width="260" border="0" cellpadding="1" cellspacing="1">
                                        <tr>
                                                <td width="157"><input name="file_2" type="file" class="combo_250px" id="my_file_element_nuevo" /></td>
                                                <td width="27">&nbsp;</td>
                                        </tr>
                                </table>
                            </td>
                        </tr>

                        <tr>
                            <td><div id="files_list_nuevo" class="labels"></div>
                                    <script>
                                            <!-- Create an instance of the multiSelector class, pass it the output target and the max number of files -->
                                            var multi_selector = new MultiSelector( document.getElementById( 'files_list_nuevo' ), 3 );
                                            <!-- Pass in the file element -->
                                            multi_selector.addElement( document.getElementById( 'my_file_element_nuevo' ) );
                                    </script>
                            </td>
                        </tr>
                    </table>
                    </fieldset>
                </td>
            </tr>

            <tr>
                <td colspan="2"></td>
            </tr>

            <tr>
                <td colspan="2" id="zona_detalle" style="display:none; vertical-align:top;">
                    <table width="100%" border="0" cellspacing="1" cellpadding="1">
                        <tr>
                            <th scope="row">
                                <table width="100%" border="0" align="left" cellpadding="0" cellspacing="0" class="tablas">
                                    <tr>
                                            <th width="216">ARTICULO / SERVICIO</th>
                                            <th width="57">UND</th>
                                            <th width="73">CANT</th>
                                            <th width="30">&nbsp;</th>
                                    </tr>
                                    <tr>
                                        <td align="center"><input name="descripcion_detalle" type="text" class="textbox_300" id="descripcion_detalle" onkeypress="if(event.keyCode){ if(event.keyCode == 13){ $('#unidad_medida').focus() }}" ></td>
                                        <td align="center">
                                                <select name="unidad_medida" id="unidad_medida"	onkeypress="if(event.keyCode){ if(event.keyCode == 13){ $('#cantidad').focus() }}">
                                                <option value="" >...</option>
                                                </select>										
                                        </td>
                                        <td align="center"><input name="cantidad" type="text" class="textbox_60px" id="cantidad" style="text-align:right;" onfocus="$(this).select();" onblur="if(this.value=='')this.value='0'" value="0" onkeypress="if(event.keyCode){ if(event.keyCode == 13){ if (this.value == ''){ this.value='0'} agregar_articulo(1) }}"/></td>
                                        <td width="30" align="center"  onclick="agregar_articulo(1)" style="color:blue; text-decoration:">AGREGAR</td>
                                    </tr>
                                </table>
                        </th>
                    </tr>
                    <tr>
                        <td>

                            <fieldset><legend class="labels">ITEMS EN LA REQUISICION</legend>
                                <div id="div_detalle_rq" style=" height:150px; position:relative; overflow:auto;">
                                    <table border="0" align="left" cellpadding="0" cellspacing="0" class="tablas" id="tabla_detalle_rq" >
                                        <thead>
                                            <tr>
                                                <th width="20">#</th>
                                                <th width="300">ARTICULO / SERVICIO</th>
                                                <th width="57">UND</th>
                                                <th width="73">CANT</th>
                                                <th width="30">&nbsp;</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </fieldset>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr><td colspan="2">&nbsp;</td></tr>
        <tr style="display:none">
                <td colspan="2" align="center" class="labels"><iframe name="iframeUpload" id="iframeUpload" style="border:none; width:380px; height:100px;"></iframe></td>
        </tr>
        
        
        <tr>
            <td colspan="2" align="center"><%
                if ( evento.equals("AGREGAR") ) { %>
                    <!-- <input type="button" class="labels" value="Guardar" onClick="Agregar_requisicion();" />  -->
                    <button id="actulizar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="Agregar_requisicion();"><span class="ui-button-text">Guardar</span></button><%
                }else{ %>
                    <!-- <input type="button" class="labels" value="Editar" onClick="UpdateRequisicion(<%=idReq%>);" /> -->
                    <button id="actulizar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="UpdateRequisicion(<%=idReq%>);"><span class="ui-button-text">Actualizar</span></button><%
                }%>

                <!-- <input name="cancelar" type="button" class="labels" id="cancelar" value="Cancelar" onClick="$('#div_nueva_requisicion').fadeOut('slow');return false;" /> 
                <button id="actulizar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="$('#div_nueva_requisicion').fadeOut('slow');return false;"><span class="ui-button-text">Salir</span></button>
                -->
            </td>
        </tr>
    </table>
</td>
</tr>
</table>
                
</form>
    