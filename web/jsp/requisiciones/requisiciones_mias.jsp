<%--
    Document   : Modulo de Requisiciones Fintra
    Created on : 10/07/2013, 11:00:00 AM
    Author     : hcuello
--%>

<%@page session="true" %>
<%@ page import="com.tsp.operation.model.beans.Usuario"%>
<%@ page import="com.tsp.operation.model.beans.CmbGenericoBeans"%>
<%@ page import="com.tsp.operation.model.services.RequisicionesService"%>


<!-- page contentType="text/html" pageEncoding="ISO-8859-1" -->
<!-- page contentType="text/html" pageEncoding="UTF-8" -->

<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="java.text.SimpleDateFormat" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="javax.servlet.*" %>
<%@ page import    ="javax.servlet.http.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>



<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    RequisicionesService rqservice= new RequisicionesService(usuario.getBd());
    ArrayList listaCombo =  rqservice.GetComboGenerico("SQL_OBTENER_COMBO_PROCESOS_SGC","id","descripcion","");
    ArrayList listaComboTipoTareas =  rqservice.GetComboGenerico("SQL_TIPO_TAREASRQ","id","descripcion","");
    ArrayList listaCmbPrioridad =  rqservice.GetComboGenerico("SQL_OBTENER_PRIORIDAD","id","descripcion","");


    Date dNow = new Date( );

    SimpleDateFormat nd = new SimpleDateFormat ("yyyy");
    String aniocte = nd.format(dNow);
    
    SimpleDateFormat md = new SimpleDateFormat ("M");
    String mescte = md.format(dNow);
    
    int acorriente = Integer.parseInt(aniocte);
    int mescorriente = Integer.parseInt(mescte);
    
%>


<html>
    <head>
        
        <title>MIS REQUISICIONES</title>
        
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/estilostsp.css" >
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/style_azul.css" >
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/jquery/jquery-ui/jquery-ui.css"/>
        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/jscal2.css" />
        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/border-radius.css" />        
        
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery-1.4.2.min.js"></script>
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/jquery-latest.js"></script>
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>  
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script> 

        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>        
        
        <script language="JavaScript1.2">
            
        var $j = jQuery.noConflict();
        
        $(document).ready(
                
            function(){
            
                cargar_requisiciones();
                requisiciones_asignadas();
                $j("#div_nueva_requisicion").draggable({ handle: "#drag_nueva_requisicion"});
                $j("#div_detalle_requisicion").draggable({ handle: "#drag_detalle_requisicion"});
                $j("#div_atender_requisicion").draggable({ handle: "#drag_atender_requisicion"});
                
                
                $(document).on(
                    "click",".eliminar",function(){
                        //var parent = $(this).parents().get(0);
                        //$(parent).remove();
                        var tr = $(this).closest('tr');
                        tr.css("background-color","#FF3700");
                        tr.fadeOut(400, function(){tr.remove();});                                            
                    }
                    
                );

                $(document).on(
                    
                        "click",".inputCalendar",function(){
                        
                            //.input_Calendar
                            //var parent0 = $(this).parents().get(0);
                            //var parent1 = $(this).parents().get(1);
                            //alert("parent0: "+parent0.outerHTML);//.innerHTML
                            //alert("parent1: "+parent1.outerHTML);//.innerHTML

                            var Calendars = $(this).parents().get(0);
                            var InputCalendar = Calendars.children[0].id;
                            var TriggerCalendar = Calendars.children[0].id;

                            //alert("outerHTML: "+Calendars.outerHTML);
                            //alert(Calendars.children[0].id);

                            //---------------------------------------------------
                            var ObjetoSeleccionado = $(this).parents().get(0);
                            var ObjetoAtributo = ObjetoSeleccionado.children[0].id;
                            var ObjetoValor = ObjetoSeleccionado.children[0].value;
                            var HoraParam = 8.5;
                            var MiNow = new Date();
                            //---------------------------------------------------
                        
                            Calendar.setup({

                                inputField: InputCalendar,
                                trigger: TriggerCalendar,
                                align: "top",

                                onSelect: function() {

                                    this.hide();

                                    //alert($('#'+ObjetoAtributo).val());
                                    Cadid = ObjetoAtributo.substring(16);
                                    //alert(Cadid);

                                    Id_tarea = "tarea"+Cadid;
                                    Id_CalendarInputFie = "CalendarInputFie"+Cadid;
                                    Id_CalendarInputFin = "CalendarInputFin"+Cadid;
                                    Id_horas = "horas"+Cadid;
                                    Id_CalendarInputFci = "CalendarInputFci"+Cadid;
                                    Id_reproceso = "reproceso"+Cadid;                                

                                    if ( ObjetoAtributo.match(/CalendarInputFin.*/) ){

                                        if ( document.getElementById(Id_CalendarInputFie).value != "" ){

                                            var diferencia = Math.floor(( Date.parse($('#'+Id_CalendarInputFin).val()) - Date.parse($('#'+Id_CalendarInputFie).val()) ) / 86400000);

                                            if(diferencia < 0){

                                                document.getElementById(Id_CalendarInputFin).value = "";
                                                document.getElementById(Id_CalendarInputFie).value = "";
                                                document.getElementById(Id_horas).value = "";
                                                alert("La Fecha Estimada Inicial NO puede ser MAYOR que la Fecha Estimada Final ");

                                            }else if(diferencia == 0){

                                                document.getElementById(Id_horas).value = 1*HoraParam;

                                            }else{

                                                document.getElementById(Id_horas).value = diferencia*HoraParam;

                                            }

                                        }else{

                                            document.getElementById(Id_CalendarInputFin).value = "";
                                            alert("Debe escoger la Fecha Estimada Inicial");

                                        }    
                                    }

                                    if ( ObjetoAtributo.match(/CalendarInputFie.*/) ){

                                        if ( document.getElementById(Id_CalendarInputFin).value != "" ){

                                            var diferencia = Math.floor(( Date.parse($('#'+Id_CalendarInputFin).val()) - Date.parse($('#'+Id_CalendarInputFie).val()) ) / 86400000);

                                            if(diferencia < 0){

                                                document.getElementById(Id_CalendarInputFin).value = "";
                                                document.getElementById(Id_CalendarInputFie).value = "";
                                                document.getElementById(Id_horas).value = "";
                                                alert("La Fecha Estimada Inicial NO puede ser MAYOR que la Fecha Estimada Final ");

                                            }else if(diferencia == 0){

                                                document.getElementById(Id_horas).value = 1*HoraParam;

                                            }else{

                                                document.getElementById(Id_horas).value = diferencia*HoraParam;

                                            }                                        

                                        }

                                    }

                                }
                            });
        
                    }                    
                );

                $(document).on(
                    
                        "click",".inputCalendarCargados",function(){
                            
                            //.input_Calendar
                            //var parent0 = $(this).parents().get(0);
                            //var parent1 = $(this).parents().get(1);
                            //alert("parent0: "+parent0.outerHTML);//.innerHTML
                            //alert("parent1: "+parent1.outerHTML);//.innerHTML

                            var Calendars = $(this).parents().get(0);
                            var InputCalendar = Calendars.children[0].id;
                            var TriggerCalendar = Calendars.children[0].id;

                            //alert("outerHTML: "+Calendars.outerHTML);
                            //alert(Calendars.children[0].id);

                            //---------------------------------------------------
                            var ObjetoSeleccionado = $(this).parents().get(0);
                            var ObjetoAtributo = ObjetoSeleccionado.children[0].id;
                            var ObjetoValor = ObjetoSeleccionado.children[0].value;
                            var HoraParam = 8.5;
                            var MiNow = new Date();
                            //---------------------------------------------------
                            
                            ReprocesoValidar = "reproceso"+ObjetoAtributo.substring(16);
                            //CalendarFin = "CalendarInputFci"+ObjetoAtributo.substring(16);
                            //alert("Es: "+document.getElementById(ReprocesoValidar).value);
                            
                            if ( document.getElementById(ReprocesoValidar).value == "0.00" ) {
                                
                                if ( document.getElementById("AutorizacionGerencia").value == 0 || ObjetoAtributo.match(/CalendarInputFci.*/)  ) {
                                    
                                    Calendar.setup({

                                        inputField: InputCalendar,
                                        trigger: TriggerCalendar,
                                        align: "top",

                                        onSelect: function() {

                                            this.hide();

                                            //alert($('#'+ObjetoAtributo).val());
                                            Cadid = ObjetoAtributo.substring(16);
                                            //alert(Cadid);

                                            Id_tarea = "tarea"+Cadid;
                                            Id_CalendarInputFie = "CalendarInputFie"+Cadid;
                                            Id_CalendarInputFin = "CalendarInputFin"+Cadid;
                                            Id_horas = "horas"+Cadid;
                                            Id_CalendarInputFci = "CalendarInputFci"+Cadid;
                                            Id_reproceso = "reproceso"+Cadid;                                

                                            if ( ObjetoAtributo.match(/CalendarInputFin.*/) ){

                                                if ( document.getElementById(Id_CalendarInputFie).value != "" ){

                                                    var diferencia = Math.floor(( Date.parse($('#'+Id_CalendarInputFin).val()) - Date.parse($('#'+Id_CalendarInputFie).val()) ) / 86400000);

                                                    if(diferencia < 0){

                                                        document.getElementById(Id_CalendarInputFin).value = "";
                                                        document.getElementById(Id_CalendarInputFie).value = "";
                                                        document.getElementById(Id_horas).value = "";
                                                        alert("La Fecha Estimada Inicial NO puede ser MAYOR que la Fecha Estimada Final ");

                                                    }else if(diferencia == 0){

                                                        document.getElementById(Id_horas).value = 1*HoraParam;

                                                    }else{

                                                        document.getElementById(Id_horas).value = diferencia*HoraParam;

                                                    }

                                                }else{

                                                    document.getElementById(Id_CalendarInputFin).value = "";
                                                    alert("Debe escoger la Fecha Estimada Inicial");

                                                }    
                                            }

                                            if ( ObjetoAtributo.match(/CalendarInputFie.*/) ){

                                                if ( document.getElementById(Id_CalendarInputFin).value != "" ){

                                                    var diferencia = Math.floor(( Date.parse($('#'+Id_CalendarInputFin).val()) - Date.parse($('#'+Id_CalendarInputFie).val()) ) / 86400000);

                                                    if(diferencia < 0){

                                                        document.getElementById(Id_CalendarInputFin).value = "";
                                                        document.getElementById(Id_CalendarInputFie).value = "";
                                                        document.getElementById(Id_horas).value = "";
                                                        alert("La Fecha Estimada Inicial NO puede ser MAYOR que la Fecha Estimada Final ");

                                                    }else if(diferencia == 0){

                                                        document.getElementById(Id_horas).value = 1*HoraParam;

                                                    }else{

                                                        document.getElementById(Id_horas).value = diferencia*HoraParam;

                                                    }                                        

                                                }

                                            }

                                        }
                                    });
                                
                                }else{
                                    alert('ATENCION: La GERENCIA ya aprobó estos tiempos. Por favor comuniquese con la gerencia')
                                }
                                
                            }else{
                                alert('ATENCION: Actividad Cerrada. NOTA: El moderador puede reactivarla')
                            }    
        
                    }                    
                );


                $(document).on(
                    
                    "click",".inputCalendarNews",function(){
                        alert("Por Control: Para establecer la culminación de la tarea, primero debe almacenarse la actividad");
                    }
                );
                /*
                   $('#prueba').click(function() {  
                         verCronograma();
                    cargarPagina("jsp/requisiciones/cronograma_calendar.jsp");
                  //  alert("####################");
                });   */
        
                    
                /*
                
                $(document).on(
                    "blur",":text",function(){
                    }                    
                );


                $(document).on(
                    "blur",".input_450px",function(){
                        //alert("cambia!");
                        //EliminarTarea();
                        
                    }
                );                $(document.body ).each(
                    function() {
                        var parentTag = $( this ).parent().get(0).tagName;
                        alert(parentTag);
                        //$( this ).prepend( document.createTextNode( parentTag + " > " ) );
                    }
                );
                */    
            }
        );

        //var i = 2;
        var ns4 = (document.layers);
        var ie4 = (document.all && !document.getElementById);
        var ie5 = (document.all && document.getElementById);
        var ns6 = (!document.all && document.getElementById);
        var msg = new Array();

        function Posicionar_div(id_objeto,e){
          //alert(e);
          //alert(window.event);
          obj = document.getElementById(id_objeto);
          var posx = 0;
          var posy = 0;
          if (!e) var e = window.event;
          if (e.pageX || e.pageY) {
           //alert('page');
           posx = e.pageX;
           posy = e.pageY;
          }
          else if (e.clientX || e.clientY) {
                //alert('client'); 
                posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
                posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop; 
              }
              else
               alert('ninguna de las anteriores');
         //alert('posx='+posx + ' posy=' + posy);
         obj.style.left = posx;
         obj.style.top = posy;
         //alert('objleft='+posx + ' objtop=' + posy);
         //document.getElementById('posicion').innerHTML = 'scrollLeft='+document.body.scrollLeft+' scrollTop='+document.body.scrollTop+' cientX'+e.clientX +' clientY'+e.clientY;
        } 
    
        function PosicionarDivLitleLeft(id_objeto,e,resta){
            
            obj = document.getElementById(id_objeto);
            
            var posx = 0;
            var posy = 0;
            
            if (!e) var e = window.event;
            
            if (e.pageX || e.pageY) {
                //alert('page');
                posx = e.pageX;
                posy = e.pageY;
            }else if (e.clientX || e.clientY) {
                  //alert('client'); 
                  posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
                  posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop; 
            }else
                alert('ninguna de las anteriores');
             
           //alert('posx='+posx + ' posy=' + posy);
           
           obj.style.left = posx-resta;
           obj.style.top = posy;
           //alert('objleft='+posx + ' objtop=' + posy);
           //document.getElementById('posicion').innerHTML = 'scrollLeft='+document.body.scrollLeft+' scrollTop='+document.body.scrollTop+' cientX'+e.clientX +' clientY'+e.clientY;
        } 
    
        function PosicionarDivLitleRight(id_objeto,e,suma){
            
            obj = document.getElementById(id_objeto);
            var posx = 0;
            var posy = 0;
            
            if (!e) var e = window.event;
            
            if (e.pageX || e.pageY) {
                //alert('page');
                posx = e.pageX;
                posy = e.pageY;
            }else if (e.clientX || e.clientY) {
                  //alert('client'); 
                  posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
                  posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop; 
            }else
                alert('ninguna de las anteriores');
            
           //alert('posx='+posx + ' posy=' + posy);
           obj.style.left = posx+suma;
           obj.style.top = posy;
           //alert('objleft='+posx + ' objtop=' + posy);
           //document.getElementById('posicion').innerHTML = 'scrollLeft='+document.body.scrollLeft+' scrollTop='+document.body.scrollTop+' cientX'+e.clientX +' clientY'+e.clientY;
        } 

        function AgregarTarea(){
            
            var i = document.getElementById('SecuenciaDinamica').value;
            //alert("Al iniciar: "+i);
            
            // Clona la fila oculta que tiene los campos base, y la agrega al final de la tablita
            //$("#TablaConProgramacion tbody tr:eq(0)").clone().removeClass('fila-base').appendTo("#TablaConProgramacion tbody");//ORIGINAL
            //$("#TablaConProgramacion tbody tr:eq(0)").clone().removeClass('fila-base').appendTo("#TablaConProgramacion tbody").find(':text').val('rita');
            //$("#TablaConProgramacion tbody tr:eq(0)").clone().removeClass('fila-base').appendTo("#TablaConProgramacion tbody").find(':text').val('').attr('id', 'CalendarInput'+i);
            //$("#TablaConProgramacion tbody tr:eq(0)").clone().removeClass('fila-base').appendTo("#TablaConProgramacion tbody").find('.input_Calendar').val('').attr('id', 'CalendarInput'+i);
            
            $("#TablaConProgramacion tbody tr:eq(0)")
                .clone()
                .removeClass('fila-base')
                .appendTo("#TablaConProgramacion tbody")
                //.find(':text').val(i)
                //.attr('id', i)
                ;
                
            $("#TablaConProgramacion tbody tr").each(
                function(){
                    
                    if ( $(this)[0].className != "fila-base" && $(this)[0].className != "fila-repeat" ) {
                        
                        //alert($(this).find("td")[2].children[0].id);
                        
                        if ( $(this).find("td")[0].children[0].id == "" || $(this).find("td")[1].children[0].id == "" || $(this).find("td")[2].children[0].id == "" || $(this).find("td")[3].children[0].id == "" || $(this).find("td")[4].children[0].id == "" || $(this).find("td")[5].children[0].id == "" ) {
                            
                            $(this).find("td")[0].children[0].id = "IdTareaRq"+i;
                            $(this).find("td")[1].children[0].id = "tarea"+i;
                            $(this).find("td")[2].children[0].id = "CalendarInputFie"+i;
                            $(this).find("td")[3].children[0].id = "CalendarInputFin"+i;
                            $(this).find("td")[4].children[0].id = "horas"+i;
                            $(this).find("td")[5].children[0].id = "CalendarInputFci"+i;
                            $(this).find("td")[6].children[0].id = "reproceso"+i;
                            
                            //alert($(this).find("td")[2].children[0].id);

                        }
                        
                        if ( $(this).find("td")[0].children[0].name== "" || $(this).find("td")[1].children[0].name== "" || $(this).find("td")[2].children[0].name== "" || $(this).find("td")[3].children[0].name== "" || $(this).find("td")[4].children[0].name== "" || $(this).find("td")[5].children[0].name== "" ) {
                            
                            $(this).find("td")[0].children[0].name= "IdTareaRq"+i;
                            $(this).find("td")[1].children[0].name= "tarea"+i;
                            $(this).find("td")[2].children[0].name= "CalendarInputFie"+i;
                            $(this).find("td")[3].children[0].name= "CalendarInputFin"+i;
                            $(this).find("td")[4].children[0].name= "horas"+i;
                            $(this).find("td")[5].children[0].name= "CalendarInputFci"+i;
                            $(this).find("td")[6].children[0].name= "reproceso"+i;
                            //alert($(this).find("td")[2].children[0].id);

                        }                        
                    }
                    
                }
            )
            
            i++;
            //alert("i es: "+i)
            //$('#SecuenciaDinamica').val() = i;
            document.getElementById('SecuenciaDinamica').value = i;
            //alert("Al finalizar: "+document.getElementById('SecuenciaDinamica').value);
            

	}
        
	function CalcularLinea(){
            
        }        
        
	function EliminarTarea(IdTarea){
            alert("IdTarea: "+IdTarea);
            
        }
        
	function GuardarTarea2(){
            
            $('#TablaConProgramacion tbody tr').each(function () {

                //alert($(this)[0].className);
                if ( $(this)[0].className != "fila-base" ) {
                    
                        var tarea = $(this).find("td")[0].children[0].value;
                        var fiestimado = $(this).find("td")[1].children[0].value;
                        var ffinalizacion = $(this).find("td")[2].children[0].value;
                        var testimado = $(this).find("td")[3].children[0].value;
                        var fcierre = $(this).find("td")[4].children[0].value;
                        var reproceso = $(this).find("td")[5].children[0].value;

                        alert(tarea);
                        alert(fiestimado);
                        alert(ffinalizacion);
                        alert(testimado);
                        alert(fcierre);
                        alert(reproceso);
                        
                        var cont = document.getElementById('controller').value;
                        $.ajax({
                            type: "POST",
                            url : cont+"?estado=Requisicion&accion=Eventos",
                            async:true,
                            dataType: "html",
                            data:{
                                evento:"GUARDARACCIONES",
                                proceso:$('#proceso').val(),
                                tiporeq:$('#tipo_req').val(),
                                asunto:$('#asunto').val(),
                                descripcion:$('#descripcion').val()

                            },
                            success:function (data){
                                //alert(data);
                                if (data="OK"){
                                    cargar_requisiciones();
                                    $('#div_nueva_requisicion').fadeOut('slow');
                                }else{
                                    alert(".::ERROR AL CREAR LA REQUISICION::.");
                                }    
                            }
                        });                        

                }

            });
        
        }

	function GuardarTarea(){
            
            //alert(formData);
            //alert($('#tipo_tarea').val());
            //alert($('#idReqid').val());
            
            var formData = "";
            var cont = document.getElementById('controller').value;
            var EstadoReq = "";
            var DescripAccion = "";
            var IdActionRq = 0;
            
            if ( document.getElementById('tipo_tarea').value == 1 ) {
                
                eventus = "GUARDARACCIONES";
                EstadoReq = "";
                IdActionRq = 0;
                DescripAccion = "";
                formData = JSON.stringify(jQuery('#form_trouble').serializeArray());
                
            }else{
                
                eventus = "GUARDARONE";
                EstadoReq = $('#IdEstadoRq').val();
                IdActionRq = $('#IdAccionRq').val();
                DescripAccion = $('#DscAccion').val();
                formData = "";
            }
            
            //alert("eventus es: "+eventus);
            $.ajax({
                type: "POST",
                url : cont+"?estado=Requisicion&accion=Eventos",
                async:true,
                dataType: "json",
                data:{
                    evento:eventus,
                    tipo_tarea:$('#tipo_tarea').val(),
                    idReqTask:$('#idReqid').val(),
                    IdEstadoRq:EstadoReq,
                    IdActionRq:IdActionRq,
                    DscAction:DescripAccion,
                    formulario:formData
                },
                success:function (json){
                    
                    if (json.error) {
                        alert(".::ERROR AL CREAR LA TAREA o ACTIVIDAD::.");
                        return;
                    }
                    if (json.mensaje === "listo") {
                        //alert(".::TODO FINE para EVENTO -> " + eventus + "::.");
                        $('#div_atender_requisicion').fadeOut('slow');
                        requisiciones_asignadas();

                    } 

                }
            });
            
            
        }


        function cargar_requisiciones(){
            
            //$('#div_espera').fadeIn('slow');
            //alert(document.getElementById('coco').value);
            //alert("La ruta es: "+$('#coco').val());

            $('#div_espera').fadeIn('slow');
            $.ajax({
                type: "POST",
                url : "/fintra/jsp/requisiciones/listado_requisiciones.jsp",
                async:true,
                dataType: "html",
                data:{
                    estado:$('#estado').val(),
                    mes:$('#mes').val(),
                    ano:$('#ano').val(),
                    proceso:$('#proceso_filtro').val(),
                    asignadas:"",
                    selec_prioridades:$('#selec_prioridades').val(),
                    tipo_tarearq:$('#tipo_tarearq').val()                    
                    
                },
                success:function (data){
                    if (data!=""){
                        $("#misreq").html(data);
                        $('#div_espera').fadeOut('slow');
                    }
                }
            });
        }
        
        function IG_Partners(QRYdeConfirmacion, ObtRta){
            RptaIndividualGenerica(QRYdeConfirmacion, "SQL_RQ_PARTNERS", "CmbPartners", "cmb_partners", ObtRta, "S", document.getElementById('NmLogin').value, document.getElementById('proceso').value);
            RptaIndividualGenerica(QRYdeConfirmacion, "SQL_USUARIOS_REQ", "CmbPartners", "cmb_generado_por", "generado_por", "N", document.getElementById('NmLogin').value, document.getElementById('proceso').value);
        }
    
        function RptaIndividualGenerica(QryConfirmacion, QRYdeCombo, Option, NombreObjeto, ObjetoRespuesta, Especifico, Var1="", Var2="", Var3="", Var4=""){
            
            $.ajax({
                type: "POST",
                url : "/fintra/jsp/requisiciones/validar_respuesta.jsp",
                async:true,
                dataType: "html",
                data:{
                    _Qry:QryConfirmacion,
                    _QryCmb:QRYdeCombo,
                    _Opcion:Option,
                    _NmObjt:NombreObjeto,
                    _Especifico:Especifico,
                    _var1:Var1,
                    _var2:Var2,
                    _var3:Var3,
                    _var4:Var4
                },

                success:function (data){
                    if (data!=""){
                        //alert(data);
                        //$("#responsabilizar_a").css("display", "block");
                        //var result_style = document.getElementById('desaparecedor').style;
                        //result_style.display = 'block';
                        
                        $("#"+ObjetoRespuesta).html(data);
                        //$('#div_nueva_requisicion').fadeIn('slow');
                        //$('#div_espera').fadeOut('slow');
                    }
                }
            });

            //$('#div_espera').fadeOut('slow');
            
        }        
        
        function requisiciones_asignadas(){
        
            //$('#div_espera').fadeIn('slow');
            //alert(document.getElementById('coco').value);
            //alert("La ruta es: "+$('#coco').val());

            $('#div_espera').fadeIn('slow');
            $.ajax({
                type: "POST",
                url : "/fintra/jsp/requisiciones/requisiciones_asignadas.jsp",
                async:true,
                dataType: "html",
                data:{
                    estado:$('#estado_misrq').val(),
                    mes:$('#mes').val(),
                    ano:$('#ano').val(),
                    proceso:$('#proceso_filtro').val(),
                    asignadas:"",
                    selec_prioridades:$('#selec_prioridades').val(),
                    tipo_tarearq:$('#tipo_tarearq').val()                    
                    
                },
                success:function (data){
                    if (data!=""){
                        $("#mispendientes").html(data);
                        $('#div_espera').fadeOut('slow');
                    }
                }
            });
        }        
        
        function Cargar_detalle_requisicion(accion,item,e){
            //alert("item: "+item);
            
            PosicionarDivLitleLeft('div_detalle_requisicion',e,600);
            $('#div_espera').fadeIn('slow');
            
            $.ajax({
                type: "POST",
                url : "/fintra/jsp/requisiciones/requisicion_detalle.jsp",
                async:true,
                dataType: "html",
                
                data:{
                    estado:$('#estado').val(),
                    mes:$('#mes').val(),
                    ano:$('#ano').val(),
                    proceso:$('#proceso_filtro').val(),
                    selec_prioridades:$('#selec_prioridades').val(),
                    tipo_tarearq:$('#tipo_tarearq').val(),
                    ACCIONE:accion,
                    ITEM:item,
                    StatusTask:"",
                    UserTask:"",
                    ItemTask:""
                },

                success:function (data){
                    if (data!=""){
                        $('#div_detalle_requisicion').html(data);
                        $('#div_detalle_requisicion').fadeIn('slow');
                        $('#div_espera').fadeOut('slow');
                    }
                }
            });

            $('#div_espera').fadeOut('slow');
            
        }

        function CrearEditarRequisicion(accion,item,e){
            //alert("Accion es:"+accion);
            Posicionar_div('div_nueva_requisicion',e);
            $('#div_espera').fadeIn('slow');
            
            $.ajax({
                type: "POST",
                url : "/fintra/jsp/requisiciones/nueva_requisicion.jsp",
                async:true,
                dataType: "html",
                data:{
                    estado:$('#estado').val(),
                    mes:$('#mes').val(),
                    ano:$('#ano').val(),
                    proceso:$('#proceso_filtro').val(),
                    ACCIONE:accion,
                    ITEM:item
                },

                success:function (data){
                    if (data!=""){
                        $("#div_nueva_requisicion").html(data);
                        $('#div_nueva_requisicion').fadeIn('slow');
                        $('#div_espera').fadeOut('slow');
                    }
                }
            });

            $('#div_espera').fadeOut('slow');
            
        }   

        function Agregar_requisicion(){
        
            var cont = document.getElementById('controller').value;
              $.ajax({
                type: "POST",
                url : cont+"?estado=Requisicion&accion=Eventos",
                async:true,
                dataType: "html",
                data:{
                    evento:"INSERT",                    
                    proceso:$('#proceso').val(),
                    tiporeq:$('#tipo_req').val(),
                    asunto:$('#asunto').val(),
                    descripcion:$('#descripcion').val(),
                    prioridad:$('#prioridad').val()

                },
                success:function (data){
                    //alert(data);
                    if (data="OK"){
                        cargar_requisiciones();
                        $('#div_nueva_requisicion').fadeOut('slow');
                    }else{
                        alert(".::ERROR AL CREAR LA REQUISICION::.");
                    }    
                }
            });
        }   

        function UpdateRequisicion(id){

            var cont = document.getElementById('controller').value;
            $.ajax({
                type: "POST",
                url : cont+"?estado=Requisicion&accion=Eventos",
                async:true,
                dataType: "html",
                data:{
                    evento:"UPDATE",
                    proceso:$('#proceso').val(),
                    tiporeq:$('#tipo_req').val(),
                    asunto:$('#asunto').val(),
                    descripcion:$('#descripcion').val(),
                    prioridad:$('#prioridad').val(),
                    IdReq:id

                },
                success:function (data){
                    //alert(data);
                    if (data="OK"){
                        cargar_requisiciones();
                        $('#div_nueva_requisicion').fadeOut('slow');
                    }else{
                        alert(".::ERROR AL ACTUALIZAR LA REQUISICION::.");
                    }    
                }
            });

        }

        function AtenderRequisicion(item,e){
            
            var accion = "";
            
            //Posicionar_div('div_atender_requisicion',e);
            PosicionarDivLitleLeft('div_atender_requisicion',e,1000);
            $('#div_espera').fadeIn('slow');
            
            $.ajax({
                type: "POST",
                url : "/fintra/jsp/requisiciones/requisicion_atender.jsp",
                async:true,
                dataType: "html",
                data:{
                    estado:$('#estado').val(),
                    mes:$('#mes').val(),
                    ano:$('#ano').val(),
                    proceso:$('#proceso_filtro').val(),
                    ACCIONE:accion,
                    ITEM:item
                },

                success:function (data){
                    if (data!=""){
                        $("#div_atender_requisicion").html(data);
                        $('#div_atender_requisicion').fadeIn('slow');
                        $('#div_espera').fadeOut('slow');
                        PanelTipoTarea($('#TipReqi').val());
                    }
                }
            });

            $('#div_espera').fadeOut('slow');
            
        }

        function Cargar_click_derecho_requisicion(id,e){
        
          Posicionar_div('div_menu_click_derecho_requisicion',e);
          
          $.ajax(
          {
          type: "POST",
          url : "/fintra/jsp/requisiciones/menu_click_derecho_requisicion.jsp",
          async:true,
          dataType: "html",
          data:{
                id:id,
                TypeRqMod:"MisRq"
               },
          success:function (data){
                    if (data!=""){
                      $("#div_menu_click_derecho_requisicion").html(data);
                      $("#div_menu_click_derecho_requisicion").fadeIn('slow');
                      $("#div_menu_click_derecho_requisicion").show().delay(1000).fadeOut();
                    }
                  }
           }
          );

        }
        
        function PanelTipoTarea(enQsit){
            
            //alert("enQsit: "+enQsit)
            //alert("Estado: "+$('#tipo_tarea').val());
            
            if ( enQsit == 'Nuevo' ) {
                
                if ( $('#tipo_tarea').val() == 1 ) {

                    $('#ConProgramacion').show(500);
                    $('#SinProgramacion').hide(500);
                    
                    $("#TdEstadoRq").css("display", "none");

                }else if( $('#tipo_tarea').val() == 2 ) {

                    $('#ConProgramacion').hide(500);
                    $('#SinProgramacion').show(500);
                    
                    $("#TdEstadoRq").css("display", "block");
                    //$("#TdEstadoRq").html("<select name=''IdEstadoRq' class='combo_100px' id='IdEstadoRq'><option value='' selected>Disabled</option></select>");
                    
                    
                }
                
                
            
            }else{
                
                if ( enQsit == 1 ) {
                    
                    $('#ConProgramacion').show(500);
                    $('#SinProgramacion').hide(500);
                    $("#TdEstadoRq").css("display", "none");
                        
                }else if( enQsit == 0 || enQsit == 2 ) {

                    $('#ConProgramacion').hide(500);
                    $('#SinProgramacion').show(500);
                }            
            
            }
        }        
        
        $( "td:parent" ).fadeTo( 1500, 0.3 );
        
        /*Funciones Manuel*/
        var $j = jQuery.noConflict();
        
        
        function cargarPagina(pagina) {
            $j.ajax({
                type: 'POST',
                url: "" + pagina,
                dataType: 'html',
                success: function(html) {       
                  $j("#div_calendar").html(html);       
                },
                error: function(xhr, ajaxOptions, thrownError) {
                    alert("Error: " + xhr.status + "\n" +
                            "Message: " + xhr.statusText + "\n" +
                            "Response: " + xhr.responseText + "\n" + thrownError);
                }
            });
        }

        function verCronograma() {

            $j("#dialogCronograma").dialog({
                width:1000,
                height:730,
                show: "scale",
                hide: "scale",
                resizable: false,
                position: "center",
                modal: true,
                closeOnEscape: true, 
                buttons: {          
                    "Salir": function () {    
                      $j(this).dialog("close");  
                    }
                }
            });
        } 
        
        function validarNombresArchivos (){  
            var archivo = document.getElementById ('my_file_element_nuevo');            
            for (var i = 0; i < archivo.files.length; i++) {                     
                    var file = archivo.files[i].name;
                    var fname = file.substring(0, file.lastIndexOf('.')); 
                    if (!fname.match(/^[0-9a-zA-Z\_\-]*$/)) {                                
                        alert('Nombre de archivo no válido. por favor, Verifique. Archivo: '+ file);                                
                        return false;
                    }  
            }
            return true;     
        }
        
        function validarArchivos(){
            if (validarNombresArchivos){
                 var cont = document.getElementById('controller').value;     
              $("form#form_nueva_rq").submit(function(event){
 
                        //disable the default form submission
                        event.preventDefault();

                        //grab all form data  
                        var formData = new FormData($(this)[0]);
                        console.log(formData);
                        var formObj = $(this);                       
                        $.ajax({
                            url:  cont+'?estado=Requisicion&accion=Eventos&evento=INSERT',
                        type: 'POST',
                        data:  formData,
                        mimeType:"multipart/form-data",
                        contentType: false,
                            cache: false,
                            processData:false,
                        success: function(data, textStatus, jqXHR)
                        {
                            if (data="OK"){
                                cargar_requisiciones();
                                $('#div_nueva_requisicion').fadeOut('slow');
                            }else{
                                 alert(".::ERROR AL GUARDAR LA REQUISICION::.");
                            }    
                        },
                         error: function(jqXHR, textStatus, errorThrown) 
                         {
                         }          
                        });
                  
                    }); 
                 
              // Agregar_requisicion();
            }
        }
        
        function Guardar_Requisicion(formId,evento,id){

            var cont = document.getElementById('controller').value;    
            var paramId = (typeof id !== 'undefined') ? '&IdReq='+id : '';
            var verificador = true;

            if ( document.getElementById('proceso').value == '0' ) {
                alert("Por favor seleccione el Proceso");
                verificador = false;
            }else if ( document.getElementById('tipo_req').value == '0' ) {
                alert("Por favor seleccione el tipo de requerimiento");
                verificador = false;
            }else if ( document.getElementById('asunto').value == '' ) {
                alert("Por favor digite el asunto");
                verificador = false;
            }else if ( document.getElementById('descripcion').value == '' ) {
                alert("Por favor digite la descripcion");
                verificador = false;
            }else if ( document.getElementById('prioridad').value == '0' ) {
                alert("Por favor seleccione la prioridad del requerimiento");
                verificador = false;
            }
            
            if ( verificador == true && document.getElementById('cmb_partners') !== null && document.getElementById('cmb_generado_por') !== null) {
                
                if ( document.getElementById('cmb_partners').value == '0' ) {
                    alert("Por favor seleccione a quien le asignará la actividad");
                    verificador = false;
                }
                if ( document.getElementById('cmb_generado_por').value == '0' ) {
                    alert("Por favor seleccione a quien le asignará la actividad");
                    verificador = false;
                }                
                
            }
            
            if ( verificador == true ){
                
                $('form#'+formId).submit(function(event){

                          //disable the default form submission
                          event.preventDefault();

                          //grab all form data  
                          var formData = new FormData($(this)[0]);                                          
                          $.ajax({
                            url:  cont+'?estado=Requisicion&accion=Eventos&evento='+evento+paramId,
                            type: 'POST',
                            data:  formData,
                            mimeType:"multipart/form-data",
                            contentType: false,
                            cache: false,
                            processData:false,
                            success: function(data, textStatus, jqXHR)
                            {
                                 if (data="OK"){
                                      cargar_requisiciones();
                                      $('#div_nueva_requisicion').fadeOut('slow');
                                  }else{
                                       alert(".::ERROR AL GUARDAR LA REQUISICION::.");
                                  }  
                            }        
                          });

                });
            }    
        }
        
        function consultarNomarchivo(idReq,nomarchivo,containerId){
            var cont = document.getElementById('controller').value;
            $.ajax({
                type: "POST",
                url : cont+"?estado=Requisicion&accion=Eventos&evento=CONSULTAR_ARCHIVO",
                async:true,
                dataType: "html",
                data:{                   
                    IdReq: idReq,
                    nomarchivo:nomarchivo
                },
                success:function (archivo){
                    //alert(data);
                    if (archivo!=""){
                       // alert(archivo); 
                       if (containerId == 'div_nueva_requisicion') {
                            var dataFile = {                             
                                estado:$('#estado').val(),
                                mes:$('#mes').val(),
                                ano:$('#ano').val(),
                                proceso:$('#proceso').val(),
                                ACCIONE:'EDITAR',
                                ITEM:idReq,
                                archivolisto: nomarchivo
                            }; 
                            cargarpagina('/fintra/jsp/requisiciones/nueva_requisicion.jsp',dataFile,containerId);
                       }else{
                            var dataFile = {   
                                estado:$('#estado').val(),
                                mes:$('#mes').val(),
                                ano:$('#ano').val(),
                                proceso:$('#proceso_filtro').val(),
                                selec_prioridades:$('#selec_prioridades').val(),
                                tipo_tarearq:$('#tipo_tarearq').val(),
                                ACCIONE:'VISUALIZAR',
                                ITEM:idReq,
                                StatusTask:"",
                                UserTask:"",
                                ItemTask:"",
                                archivolisto: nomarchivo
                            };
                            cargarpagina('/fintra/jsp/requisiciones/requisicion_detalle.jsp',dataFile,containerId);
                       }      
                    }else{
                        alert(".::ERROR AL RECUPERAR ARCHIVO.");
                    }    
                }
            });
        }   
        
        function EliminarArchivo(idReq,nomarchivo){
            var cont = document.getElementById('controller').value;
            $.ajax({
                type: "POST",
                url : cont+"?estado=Requisicion&accion=Eventos&evento=BORRAR_ARCHIVO",
                async:true,
                dataType: "html",
                data:{                   
                    IdReq: idReq,
                    nomarchivo:nomarchivo
                },
                success:function (data){
                    if (data="OK"){
                        var dataFile = {                             
                             estado:$('#estado').val(),
                             mes:$('#mes').val(),
                             ano:$('#ano').val(),
                             proceso:$('#proceso').val(),
                             ACCIONE:'EDITAR',
                             ITEM:idReq,
                             archivolisto: nomarchivo
                         }; 
                         cargarpagina('/fintra/jsp/requisiciones/nueva_requisicion.jsp',dataFile,'div_nueva_requisicion');
                    }else{
                         alert(".::ERROR AL ELIMINAR ARCHIVO::.");
                    }  
                }
            });
        }   
        
        function cargarpagina(url, data, divId){
            $.ajax({
                type: "POST",
                url : url,
                async:true,
                dataType: "html",
                data: data,
                success:function (data){
                    if (data!=""){
                        $("#"+divId).html(data);
                        $('#'+divId).fadeIn('slow');                       
                    }
                }
            });
        }
    
    </script>
    
   
    
    
    <!--
    
        <script>
        $( "*", document.body ).each(function() {
        var parentTag = $( this ).parent().get( 0 ).tagName;
        $( this ).prepend( document.createTextNode( parentTag + " > " ) );
        });
        </script>       
    
    <style type="text/css">
            #TablaConProgramacion{	border: solid 1px #333;	width: 300px; }
            #TablaConProgramacion tbody tr{ background: #999; }
            .fila-base{ display: none; } /* fila base oculta */
            .eliminar{ cursor: pointer; color: #000; }
            input[type="text"]{ width: 80px; } /* ancho a los elementos input="text" */
    </style>    
    -->
    
    </head>
    
    <body>
        
    <div id="div_espera" style="display:none;z-index:1000; position:absolute"></div>
    <div id="div_nueva_requisicion" style="display:none;z-index:101; position:absolute; border: 1px solid #165FB6; background:#FFFFFF; padding: 3px 3px 3px 3px;">XXXXXXXXXXXXXXXX</div>
    <div id="div_detalle_requisicion" style="display:none;z-index:102; position:absolute; border: 1px solid #165FB6; background:#FFFFFF; padding: 3px 3px 3px 3px;">WWWWWWW</div>
    <div id="div_atender_requisicion" style="display:none;z-index:103; position:absolute; border: 1px solid #165FB6; background:#FFFFFF; padding: 3px 3px 3px 3px;">WAAAAAOOOOO</div>
    <div id="div_menu_click_derecho_requisicion" style="display:none;z-index:510;position:absolute;border:1px solid #165FB6; background-color:#FFFFFF; padding: 2px 2px 2px 2px;">YYY</div>

    <!-- DIV MANUEL-->
    <div id="dialogCronograma" title="Cronograma de Actividades" style="display:none;"> 
          <div id="div_calendar">

          </div>        
    </div>    
    
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=MIS REQUISICIONES"/>
        </div>
        
        <!-- ******************************************************************************************************* -->
        
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> <br>
            
            <center>
                
                <input type="hidden" id="baseurl" name="baseurl" value="<%=BASEURL%>">
                <input type="hidden" id="controller" name="controller" value ="<%=CONTROLLER%>"/>

                <table width="100%" border="0" cellpadding="0" cellspacing="1" class="labels">

                    <tr>
                        <td >
                            <fieldset>

                                <legend class="labels">FILTRO DE MIS REQUISICIONES GENERADAS</legend>

                                <table width="435" border="0" align="left" cellpadding="1" cellspacing="1" class="labels">
                                  <tr>

                                    <td width="62"><fieldset>
                                      <legend>A&Ntilde;O</legend>
                                      <select name="ano" class="combo_60px" id="ano" onChange="cargar_requisiciones();"><%
                                                    String cad = "";

                                                    for(int j=2009; j<=2020; j++){
                                                        int anio = j;
                                                        if ( anio == acorriente ) {
                                                            cad = "selected";
                                                        }else{ 
                                                            cad = "";
                                                        }%>
                                                        <option value="<%=anio%>" <%=cad%> > <%=anio%> </option><%
                                                    }%>
                                      </select>
                                    </fieldset></td>                                      
                                    <td width="62"><fieldset>
                                      <legend>MES</legend>
                                      <select name="mes" class="combo_60px" id="mes" onChange="cargar_requisiciones();">

                                                    <option value="0">< todos ></option><% 

                                                    String meses = "";
                                                    String cadi = "";

                                                    for(int i=1; i<=12; i++){
                                                        int value = i;
                                                        switch (value) {
                                                                    case 1:
                                                                        meses="ENE";
                                                                        break;
                                                                    case 2:
                                                                        meses="FEB";
                                                                        break;
                                                                    case 3:
                                                                        meses="MAR";
                                                                        break;
                                                                    case 4:
                                                                        meses="ABR";
                                                                        break;
                                                                    case 5:
                                                                        meses="MAY";
                                                                        break;
                                                                    case 6:
                                                                        meses="JUN";
                                                                        break;
                                                                    case 7:
                                                                        meses="JUL";
                                                                        break;
                                                                    case 8:
                                                                        meses="AGO";
                                                                        break;
                                                                    case 9:
                                                                        meses="SEP";
                                                                        break;
                                                                    case 10:
                                                                        meses="OCT";
                                                                        break;
                                                                    case 11:
                                                                        meses="NOV";
                                                                        break;
                                                                    case 12:
                                                                        meses="DIC";
                                                                        break;
                                                        } 

                                                        if ( value == mescorriente ) {
                                                            cadi = "selected";
                                                        }else{ 
                                                            cadi = "";
                                                        }%>

                                                        <option value="<%=value%>" <%=cadi%> > <%=meses%> </option><%

                                                    } %>
                                      </select>
                                    </fieldset></td>

                                    <td width="190">
                                        <fieldset>
                                            <legend>PROCESO</legend>
                                            <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                                <tr>
                                                    <td>
                                                        <select name="proceso_filtro" class="combo_180px" id="proceso_filtro" onChange="cargar_requisiciones();">
                                                            <option value="" selected>< todos ></option><%
                                                            for (int i = 0; i < listaCombo.size(); i++) {
                                                                CmbGenericoBeans CmbContenido = (CmbGenericoBeans) listaCombo.get(i);%>
                                                                 <option value="<%=CmbContenido.getIdCmb() %>"><%=CmbContenido.getDescripcionCmb() %></option><%
                                                            }%>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>

                                    <td width="108" ><fieldset>
                                      <legend>ESTADO</legend>
                                      <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                        <tr>
                                          <td><select name="estado" class="combo_100px" id="estado" onChange="cargar_requisiciones();">
                                              <option value="" selected>< todos ></option>
                                              <option value="1">PENDIENTE</option>
                                              <option value="2">ATENDIDA</option>
                                            </select>
                                          </td>
                                        </tr>
                                      </table>
                                    </fieldset></td>


                                    <td width="108" >
                                        <button id="actulizar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onClick="CrearEditarRequisicion('NUEVO','',event)">
                                            <span class="ui-button-text">Nueva Req</span>
                                        </button>                                         
                                    </td>

                                  </tr>
                                </table>
                            </fieldset>
                        </td>

                        <td>
                            <fieldset>

                                <legend class="labels">FILTRO DE MIS REQUISICIONES ASIGNADAS</legend>

                                <table width="435" border="0" align="left" cellpadding="1" cellspacing="1" class="labels">
                                  <tr>

                                    <td width="62"><fieldset>
                                        <legend>TIPO DE TAREA</legend>
                                        <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                            <tr>
                                                <td>
                                                    <select name="tipo_tarearq" class="combo_180px" id="tipo_tarearq" onChange="requisiciones_asignadas();">
                                                        <option value="" selected>< todos ></option><%
                                                        for (int i = 0; i < listaComboTipoTareas.size(); i++) {
                                                            CmbGenericoBeans CmbTipoTarea = (CmbGenericoBeans) listaComboTipoTareas.get(i);%>
                                                             <option value="<%=CmbTipoTarea.getIdCmb() %>"><%=CmbTipoTarea.getDescripcionCmb() %></option><%
                                                        }%>
                                                    </select>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset></td>

                                    <td width="108" ><fieldset>
                                      <legend>ESTADO</legend>
                                      <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                        <tr>
                                          <td><select name="estado_misrq" class="combo_100px" id="estado_misrq" onChange="requisiciones_asignadas();">
                                              <option value="" selected>< todos ></option>
                                              <option value="1">PENDIENTE</option>
                                              <option value="2">ATENDIDA</option>
                                            </select>
                                          </td>
                                        </tr>
                                      </table>
                                    </fieldset></td> 

                                    <td width="62"><fieldset>
                                        <legend>PRIORIDADES</legend>
                                        <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                            <tr>
                                                <td>
                                                    <select name="selec_prioridades" class="combo_180px" id="selec_prioridades" onChange="requisiciones_asignadas();">
                                                        <option value="" selected>< todos ></option><%
                                                        for (int i = 0; i < listaCmbPrioridad.size(); i++) {
                                                            CmbGenericoBeans CmbPrioritate = (CmbGenericoBeans) listaCmbPrioridad.get(i);%>
                                                             <option value="<%=CmbPrioritate.getIdCmb() %>"><%=CmbPrioritate.getDescripcionCmb() %></option><%
                                                        }%>
                                                    </select>
                                                </td>
                                            </tr>
                                        </table>
                                    </fieldset></td>                                    

                                  </tr>
                                </table>
                            </fieldset>
                        </td>                        


                    </tr>  

                    <tr><td colspan="2" id="mensajes_sistema">&nbsp;</td></tr>

                    <tr>
                        <td width="800" align="left">
                            <fieldset>
                            <legend>SALIDA - Generadas Por Mi</legend> 
                              <div id="fondo4" style="position:relative; width:1250px; height:600px; visibility: visible; overflow:auto;" align="center">
                                   <div id="misreq" align="left" style="position:absolute; z-index:58; left:0px; top:0px; visibility:visible;"></div>
                            </div>
                            </fieldset>
                        </td>


                        <td width="600">
                            <fieldset>
                                <legend>ENTRADA - Asignadas a Mi</legend> 
                                    <div id="fondo5" style="position:relative; width:600px; height:600px; visibility: visible; overflow:auto" align="center">
                                        <div id="mispendientes" align="center" style="position:absolute; z-index:58; left:0px; top:0px; visibility:visible;">
                                        </div>
                                    </div>
                              </fieldset>
                        </td>

                    </tr>

                    <div id="box" style="top:281px; left:417px; position:absolute; z-index:1000; width: 43px; height: 29px;"></div>

                </table>                

                <div id="contenido">
                    <button id="actulizar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="parent.close()">
                        <span class="ui-button-text">Salir</span>
                    </button>
                </div>


                <center class='comentario'>
                    <div id="comentario" style="visibility: hidden" >
                        <table border="2" align="center">
                            <tr>
                                <td>
                                    <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                        <tr>
                                            <td width="229" align="center" class="mensajes"><span class="normal"><div id="mensaje"></div></span></td>
                                            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                            <td width="58">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </center> 
                   
            </center>
             
        </div>
                                            
    </body>
</html>
