<%-- 
    Document   : cronograma_actividades
    Created on : 9/07/2015, 10:54:45 AM
    Author     : mcastillo
--%>

<%@page session="true" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page import    ="java.util.*" %>

<html>
    <head>       
        <title>	Cronograma </title>
        <meta http-equiv="Content-Type" content="text/html;charset=UTF-8">

        <link href="./css/style_azul.css" rel="stylesheet" type="text/css">
        <link href="./css/jquery/jquery-ui/jquery-ui.css" rel="stylesheet" type="text/css">
        <link href="./js/jquery/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
        <link href="./js/jquery/fullcalendar/fullcalendar.print.css" rel="stylesheet" media='print'  type="text/css" /> 
        <link href="./css/cronogramaActividadesReq.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>  
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script> 
        <script type="text/javascript" src="./js/jquery/fullcalendar/lib/moment.min.js"></script>  
        <script type="text/javascript" src="./js/jquery/fullcalendar/lib/jquery.min.js"></script>  
        <script  type="text/javascript" src="./js/jquery/fullcalendar/fullcalendar.min.js"> </script>
        <script  type="text/javascript" src="./js/jquery/fullcalendar/lang-all.js"> </script>
        <script type="text/javascript" src="./js/Cronograma_Actividades.js"> </script>
        
    </head>
    <body>
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Cronograma de tareas"/>

        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: auto;" align="center">
            
                <center>
                <div id="div_cronograma_actividades">
                    
                    <fieldset style="text-align:left; max-width: 52%;">
                        <legend>FILTROS</legend>
                        <fieldset style="display:inline-block">
                            <legend>Año</legend>
                            <select id='anho'> </select>
                        </fieldset>
                        <fieldset style="display:inline-block">
                            <legend>Mes</legend>
                            <select id='mes'></select>
                        </fieldset>
                        <fieldset style="display:inline-block">
                            <legend>Procesos</legend>
                            <select id='procesos'> </select>
                        </fieldset>
                        <input id="buscar" type="button" value="Ver cronograma"/>                   
                    </fieldset>
                 </div>
                <br>
                <div id="eventContent" title="Detalles de la actividad" style="display:none;">                  
                    <p id="eventInfo"></p>                  
                </div>
                <fieldset id="calcontainer" style="display:none;background: white none repeat scroll 0 0;max-width: 52%;">
                     <div id="calendar" > </div> 
                </fieldset>
                      
            </center>
       </div>
                        
    </body>
</html>
