<%--
    Document   : Modulo de Requisiciones Fintra
    Created on : 10/07/2013, 11:00:00 AM
    Author     : hcuello
--%>

<%@page session="true" %>
<%@ page import="com.tsp.operation.model.beans.Usuario"%>
<%@ page import="com.tsp.operation.model.beans.CmbGenericoBeans"%>
<%@ page import="com.tsp.operation.model.services.RequisicionesService"%>
<%@ page import="com.tsp.operation.model.beans.RequisicionesListadoBeans"%>

<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="java.text.SimpleDateFormat" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>


<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String NombreUsuario = usuario.getNombre();
    String NmLogin = usuario.getLogin();
    
    int idReq = 0;
    
    String QryProceso = "";
    int IdProceso = 0;
    
    String QryTipReq = "";
    int IdTipReq = 0;
    
    String QryPrioridad = "";
    int IdPrioridad = 0;
    
    String Asunto = "";
    String Descripcion = "";
    String Radicado = "";
    String TituloDiv = "";
    String evento = "";
    evento = request.getParameter("ACCIONE");
    
    String estado = request.getParameter("estado") != "" ? request.getParameter("estado") : "";
    String mes = request.getParameter("mes") != "" ? request.getParameter("mes") : "";
    String ano = request.getParameter("ano") != "" ? request.getParameter("ano") : "";
    String proceso = request.getParameter("proceso") != "" ? request.getParameter("proceso") : "";
    String asignadas = request.getParameter("asignadas") != "" ? request.getParameter("asignadas") : "";
    String Prioridades = request.getParameter("selec_prioridades") != "" ? request.getParameter("selec_prioridades") : "";
    String TipoRequisicion = request.getParameter("tipo_tarearq") != "" ? request.getParameter("tipo_tarearq") : "";
    
    String item = request.getParameter("ITEM") != "" ? request.getParameter("ITEM") : "";
    String archivolisto = (request.getParameter("archivolisto")!= null) ? request.getParameter("archivolisto") : "";
    String filtro = "";
   
    RequisicionesService rqservice= new RequisicionesService(usuario.getBd());
    
    QryProceso = "SQL_OBTENER_COMBO_PROCESOS_SGC";
    QryTipReq = "SQL_OBTENER_TIPO_REQUISICION";
    QryPrioridad = "SQL_OBTENER_PRIORIDAD";

    if (evento.equals("EDITAR") ) {
        
        filtro = "Editar";
        ArrayList listaRequisiciones =  rqservice.RequisicionesListadoBeans("SQL_OBTENER_LISTADO_REQUISICIONES",estado,mes,ano,proceso,asignadas,TipoRequisicion,Prioridades,NmLogin,item,filtro);
        RequisicionesListadoBeans ListadoContenido = (RequisicionesListadoBeans) listaRequisiciones.get(0);

        idReq = ListadoContenido.getId();
        Radicado = ListadoContenido.getRadicado();
        IdProceso = ListadoContenido.getIdProcesoSgc();
        IdTipReq = ListadoContenido.getIdTipoRequisicion();
        IdPrioridad = ListadoContenido.getIdPrioridad();
        Asunto = ListadoContenido.getAsunto();
        Descripcion = ListadoContenido.getDescripcion();
        TituloDiv = "Editar Requisicion No: "+Radicado;
        
    }else if ( evento.equals("NUEVO") ) {
        
        TituloDiv = "Nueva Requisicion";
        /*
        filtro = "Nuevo";
        ArrayList listaRequisiciones =  rqservice.RequisicionesListadoBeans("SQL_OBTENER_LISTADO_REQUISICIONES",estado,mes,ano,proceso,NmLogin,item,filtro);
        RequisicionesListadoBeans ListadoContenido = (RequisicionesListadoBeans) listaRequisiciones.get(0);

        idReq = ListadoContenido.getId();
        IdProceso = ListadoContenido.getIdProcesoSgc();
        IdTipReq = ListadoContenido.getIdTipoRequisicion();
        Asunto = ListadoContenido.getAsunto();
        Descripcion = ListadoContenido.getDescripcion();        
        */        
    }

    ArrayList listaCombo =  rqservice.GetComboGenerico(QryProceso,"id","descripcion","");
    ArrayList listaComboTypeReq =  rqservice.GetComboGenerico(QryTipReq,"id","descripcion","");
    ArrayList listaComboPrioridad =  rqservice.GetComboGenerico(QryPrioridad,"id","descripcion","");

%>

<script language="JavaScript1.2">

$(document).ready(
    function(){
        //alert("todo OK");
        //var result_style = document.getElementById('desaparecedor').style;
        //result_style.display = 'none';
    }
);
</script>

<form name="form_nueva_rq" id="form_nueva_rq" method="post" enctype="multipart/form-data" target="iframeUpload">

<input type="hidden" name="contador_articulos" id="contador_articulos" value="0" />
<input type="hidden" id="NmLogin" name="NmLogin" value="<%=NmLogin%>">

<table align="center" cellpadding="0" cellspacing="0" border="0" bgcolor="#FFFFFF">
    
    <tr>
        <td id="drag_nueva_requisicion" style="cursor:pointer">
            <div class="k-header">
                <span class="titulo"><%=TituloDiv%></span>
                <span class="ui-widget-header ui-corner-all titulo_right" onClick="$('#div_nueva_requisicion').fadeOut('slow')">x</span>
            </div>
        </td>
    </tr>    
    
    <tr><td>&nbsp;</td></tr>    
    
    <tr>
        <td>
            <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                
                <tr>
                    <td width="168" align="right">GENERADA POR: &nbsp;</td>
                    <td width="263"><%=NombreUsuario %></td>
                </tr>
                
                <tr>
                    <td align="right">AL PROCESO: &nbsp;</td>
                    <td align="left">

                        <select name="proceso"  id="proceso" class="combo_300px" onchange="IG_Partners('SQL_WHO_IAM', 'responsabilizar_a')"><%
                            if ( listaCombo.size() > 0 ) { %>

                                <OPTION value='0' selected>-- SELECCIONAR --</OPTION><%

                                for (int i = 0; i < listaCombo.size(); i++) {

                                    CmbGenericoBeans CmbContenido = (CmbGenericoBeans) listaCombo.get(i);

                                    if ( CmbContenido.getIdCmb() == IdProceso ) {%>
                                        <option value="<%=CmbContenido.getIdCmb() %>" selected><%=CmbContenido.getDescripcionCmb() %></option> <%
                                    }else{ %>
                                        <option value="<%=CmbContenido.getIdCmb() %>"><%=CmbContenido.getDescripcionCmb() %></option> <%
                                    }
                                }

                            }else{%>

                                <OPTION value=''>No se han ingresado items</OPTION><%
                            }%>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td align="right">TIPO REQUISICION: &nbsp;</td>
                    <td align="left">
                        <select name="tipo_req"  id="tipo_req" class="combo_300px"><%
                            if ( listaComboTypeReq.size() > 0 ) { %>

                                <OPTION value='0' selected>-- SELECCIONAR --</OPTION><%

                                for (int i = 0; i < listaComboTypeReq.size(); i++) {

                                    CmbGenericoBeans CmbTipoReq = (CmbGenericoBeans) listaComboTypeReq.get(i);

                                    if ( CmbTipoReq.getIdCmb() == IdTipReq ) {%>
                                        <option value="<%=CmbTipoReq.getIdCmb() %>" selected><%=CmbTipoReq.getDescripcionCmb() %></option> <%
                                    }else{ %>
                                        <option value="<%=CmbTipoReq.getIdCmb() %>"><%=CmbTipoReq.getDescripcionCmb() %></option> <%
                                    }
                                }

                            }else{%>        
                                <OPTION value=''>No se han ingresado items</OPTION><%
                            }%>
                        </select>
                    </td>
                </tr>

                <tr>
                    <td align="right">ASUNTO: &nbsp;</td>
                    <td><input name="asunto" type="text" class="textbox_300" id="asunto" value="<%=Asunto %>"  ></td>
                </tr>

                <tr>
                    <td align="right">DESCRIPCION: &nbsp;</td>
                    <td><textarea name="descripcion" class="textbox_300x100px" id="descripcion"><%=Descripcion %></textarea></td>
                </tr>
                
                <tr>
                    <td align="right">PRIORIDAD: &nbsp;</td>
                    <td align="left">
                        <select name="prioridad"  id="prioridad" class="combo_300px"><%
                            if ( listaComboPrioridad.size() > 0 ) { %>

                                <OPTION value='0' selected>-- SELECCIONAR --</OPTION><%

                                for (int i = 0; i < listaComboPrioridad.size(); i++) {

                                    CmbGenericoBeans CmbPrioridad = (CmbGenericoBeans) listaComboPrioridad.get(i);

                                    if ( CmbPrioridad.getIdCmb() == IdPrioridad ) {%>
                                        <option value="<%=CmbPrioridad.getIdCmb() %>" selected><%=CmbPrioridad.getDescripcionCmb() %></option> <%
                                    }else{ %>
                                        <option value="<%=CmbPrioridad.getIdCmb() %>"><%=CmbPrioridad.getDescripcionCmb() %></option> <%
                                    }
                                }

                            }else{%>        
                                <option value=''>No se han ingresado items</option><%
                            }%>
                        </select>
                    </td>
                </tr> <%
                if ( idReq == 0 ) { %>
                    <tr>
                        <td align="right">SOLICITADO POR: &nbsp;</td> <!-- style="display:block;" -->
                        <td id="generado_por">
                            <select class="combo_300px_disabled" disabled="disabled">
                                <option value='0' selected>-Disabled-</option>
                            </select>    
                        </td>
                    </tr>

                    <tr>
                        <td align="right">ASIGNAR A: &nbsp;</td> <!-- style="display:block;" -->
                        <td id="responsabilizar_a">
                            <select class="combo_300px_disabled" disabled="disabled">
                                <option value='0' selected>-Disabled-</option>
                            </select>    
                        </td>
                    </tr><%
                } %>
                
                <tr>
                    <td colspan="2" align="left" valign="middle"><fieldset>
                        <legend class="labels">ARCHIVOS RELACIONADOS: &nbsp;</legend>
                        <table class="labels" width="350" border="0" align="left" cellpadding="1" cellspacing="1">
                            <tr>
                                <td>
                                    <table width="260" border="0" cellpadding="1" cellspacing="1">
                                            <tr>
                                                    <td width="157"><input name="file_2" type="file" class="combo_250px" id="my_file_element_nuevo" multiple/></td>
                                                    <td width="27">&nbsp;</td>
                                            </tr>
                                    </table>
                                </td>
                            </tr>

                            <tr>
                                <td><div id="files_list_nuevo" class="labels"></div></td>
                            </tr>
                        </table>
                        </fieldset>
                    </td>
                </tr>
                
                <tr>
                    <td colspan="2"></td>
                </tr>
                
                    <%
                      if (evento.equals("EDITAR")) {  %>
                        <tr>
                            <td align="right" colspan="2">
                                <fieldset><legend>ARCHIVOS CARGADOS: &nbsp;</legend>

                                    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablas">
                                        <%java.util.List nombresArchivos = rqservice.getNombresArchivos(String.valueOf(idReq));
                                            for (int w = 0; w < nombresArchivos.size(); w++) {%>
                                        <tr class="fila">	

                                            <td ><a target="_blank"  onClick="consultarNomarchivo('<%=String.valueOf(idReq)%>','<%=nombresArchivos.get(w)%>','div_nueva_requisicion');" style="cursor:hand" ><strong><%=nombresArchivos.get(w)%></strong>				
                                                </a>
                                                <%
                                                    if (archivolisto != null && nombresArchivos.get(w).equals(archivolisto)) {
                                                %>		

                                                &nbsp;&nbsp;&nbsp;<a  target="_blank" href="<%=BASEURL%>/images/multiservicios/<%=NmLogin%>/<%=archivolisto%>">ver</a>

                                                <%	}%>
                                                &nbsp;&nbsp;&nbsp;<a href="#" onCLick="EliminarArchivo('<%=String.valueOf(idReq)%>','<%=nombresArchivos.get(w)%>');">Eliminar</a>
                                            </td>

                                        </tr>
                                        <%}%>     
                                    </table>

                                </fieldset>
                            </td>
                        </tr>
                <%}%>
                
                <!--VA EN EL DETALLE-->
                <tr>
                    <td colspan="2" id="zona_detalle" style="display:none; vertical-align:top;">
                        <table width="100%" border="0" cellspacing="1" cellpadding="1">
                            <tr>
                                <th scope="row">
                                    <table width="100%" border="0" align="left" cellpadding="0" cellspacing="0" class="tablas">
                                        <tr>
                                                <th width="216">ARTICULO / SERVICIO</th>
                                                <th width="57">UND</th>
                                                <th width="73">CANT</th>
                                                <th width="30">&nbsp;</th>
                                        </tr>
                                        <tr>
                                            <td align="center"><input name="descripcion_detalle" type="text" class="textbox_300" id="descripcion_detalle" onkeypress="if(event.keyCode){ if(event.keyCode == 13){ $('#unidad_medida').focus() }}" ></td>
                                            <td align="center">
                                                    <select name="unidad_medida" id="unidad_medida"	onkeypress="if(event.keyCode){ if(event.keyCode == 13){ $('#cantidad').focus() }}">
                                                    <option value="" >...</option>
                                                    </select>										
                                            </td>
                                            <td align="center"><input name="cantidad" type="text" class="textbox_60px" id="cantidad" style="text-align:right;" onfocus="$(this).select();" onblur="if(this.value=='')this.value='0'" value="0" onkeypress="if(event.keyCode){ if(event.keyCode == 13){ if (this.value == ''){ this.value='0'} agregar_articulo(1) }}"/></td>
                                            <td width="30" align="center"  onclick="agregar_articulo(1)" style="color:blue; text-decoration:">AGREGAR</td>
                                        </tr>
                                    </table>
                            </th>
                        </tr>
                        <tr>
                            <td>

                                <fieldset><legend class="labels">ITEMS EN LA REQUISICION</legend>
                                    <div id="div_detalle_rq" style=" height:150px; position:relative; overflow:auto;">
                                        <table border="0" align="left" cellpadding="0" cellspacing="0" class="tablas" id="tabla_detalle_rq" >
                                            <thead>
                                                <tr>
                                                    <th width="20">#</th>
                                                    <th width="300">ARTICULO / SERVICIO</th>
                                                    <th width="57">UND</th>
                                                    <th width="73">CANT</th>
                                                    <th width="30">&nbsp;</th>
                                                </tr>
                                            </thead>
                                            <tbody></tbody>
                                        </table>
                                    </div>
                                </fieldset>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr><td colspan="2">&nbsp;</td></tr>
            <tr style="display:none">
                    <td colspan="2" align="center" class="labels"><iframe name="iframeUpload" id="iframeUpload" style="border:none; width:380px; height:100px;"></iframe></td>
            </tr>
            <!--VA EN EL DETALLE-->

            <tr>
                <td colspan="2" align="center"><%
                    if ( evento.equals("NUEVO") ) { %>
                        <!-- <input type="button" class="labels" value="Guardar" onClick="Agregar_requisicion();" />  -->
                        <button id="actulizar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="if((form_nueva_rq.file_2.value!='' && validarNombresArchivos()) || form_nueva_rq.file_2.value==''){Guardar_Requisicion('form_nueva_rq','INSERT');}"><span class="ui-button-text">Guardar</span></button><%
                    }else{ %>
                        <!-- <input type="button" class="labels" value="Editar" onClick="UpdateRequisicion(<%=idReq%>);" /> -->
                        <button id="actulizar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="if((form_nueva_rq.file_2.value!='' && validarNombresArchivos()) || form_nueva_rq.file_2.value==''){Guardar_Requisicion('form_nueva_rq','UPDATE',<%=idReq%>);}"><span class="ui-button-text">Actualizar</span></button><%
                    }%>

                    <!-- <input name="cancelar" type="button" class="labels" id="cancelar" value="Cancelar" onClick="$('#div_nueva_requisicion').fadeOut('slow');return false;" /> 
                    <button id="actulizar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="$('#div_nueva_requisicion').fadeOut('slow');return false;"><span class="ui-button-text">Salir</span></button>
                    -->
                </td>
            </tr>
    </table>
</td>
</tr>
</table>
                
</form>
    