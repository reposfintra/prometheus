<%--
    Document   : Modulo de Requisiciones Fintra
    Created on : 10/07/2013, 11:00:00 AM
    Author     : hcuello
--%>

<%@page session="true" %>
<%@ page import="com.tsp.operation.model.beans.CmbGenericoBeans"%>
<%@ page import="com.tsp.operation.model.services.RequisicionesService"%>

<%@ page import="com.tsp.operation.model.beans.Usuario"%>
<%@ page import="com.tsp.operation.model.beans.CmbGenericoBeans"%>
<%@ page import="com.tsp.operation.model.services.RequisicionesService"%>
<%@ page import="com.tsp.operation.model.beans.RequisicionesListadoBeans"%>

<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="java.text.SimpleDateFormat" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>


<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String NmLogin = usuario.getLogin();
    
    String QryProceso = "";
    int idReq = 0;

    String item = request.getParameter("ITEM") != "" ? request.getParameter("ITEM") : "";
   
    RequisicionesService rqservice= new RequisicionesService(usuario.getBd());
    
    QryProceso = "SQL_USUARIOS_ASIGNACIONRQ";
    ArrayList listaOptions =  rqservice.GetComboGenerico(QryProceso,"codigo_usuario","nombre",NmLogin);
%>

<table align="center" cellpadding="0" cellspacing="0" border="0" class="labels">

    <tr>
        <td id="drag_asignar_responsable" style="cursor:pointer">
            <div class="k-header">
                <span class="titulo">Responsabilizar Usuario</span>
                <span class="ui-widget-header ui-corner-all titulo_right" onClick="$('#div_asignar_responsable').fadeOut('slow')">x</span>
            </div>
        </td>
    </tr>    

    <tr><td>&nbsp;</td></tr>    

    <tr>
        <td>
            <table width="100%"  border="0" align="center" cellpadding="1" cellspacing="1" class="tablas" id="tbl_asignacion_resp"><%

                if ( listaOptions.size() > 0 ) {%>
                
                    <tr>
                        <th width="5%" align="center">#</th>
                        <th width="95%" align="center">NOMBRE DE USUARIO</th>
                    </tr>
                    
                    <tr>
                        <th width="20" align="center"><input type="radio" name="RadioSelectResponsable" id="RadioSelectResponsable" value="0"></th>
                        <td>DESASIGNAR</td>
                    </tr><%
                    
                    for (int i = 0; i < listaOptions.size(); i++) {

                        CmbGenericoBeans OptContenido = (CmbGenericoBeans) listaOptions.get(i);%>

                        <tr>
                            <th width="20" bgcolor="#B4D2F3" align="center"><input type="radio" name="RadioSelectResponsable" id="RadioSelectResponsable" value="<%=OptContenido.getIdCmb()%>"></th>
                            <td><%=OptContenido.getDescripcionCmb()%></td>
                        </tr><%
                        
                    }%>    

                    <tr>
                        <td colspan="2" align="center">
                                <button id="actulizar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="GuardarResponsable(<%=item%>);"><span class="ui-button-text">Asignar</span></button>
                        </td>
                    </tr><%
                    
                }else{%>
                
                    <tr><td width="100%" align="center" bgcolor="FFFF66" class="labels"><span class="style_redword">LO SIENTO: NO TIENES EL PERFIL</span></td></tr><%                    
                }%>    


            </table>

        </td>
    </tr>

</table>
    
<script>
     $("#tbl_asignacion_resp tr:even").addClass("even");
     $("#tbl_asignacion_resp tr:odd").addClass("odd");

             $(
               function()
               {
                      $("#tbl_req_asignadas tr").hover(
                       function()
                       {
                            $(this).addClass("highlight");
                       },
                       function()
                       {
                            $(this).removeClass("highlight");
                       }
                      )
               }
              )
 </script>                    