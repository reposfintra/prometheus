<%-- 
    Document   : asociarProInternoUsuario
    Created on : 22/10/2014, 06:01:35 PM
    Author     : lcanchila
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
 String otro = request.getParameter("otro");
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Asociar Proceso Interno Usuario</title>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>        
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>            
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/buttons/botonVerde.css"/>
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/popup.css"/>
        <script type="text/javascript" src="<%=BASEURL%>/js/procesosInternos.js"></script>    
    </head>
    <body>
        <input type="hidden" id="otro" value="SI"/>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
             <jsp:include page="/toptsp.jsp?encabezado=Asociacion Procesos Internos Usuarios"/>
        </div>
        
        <div id="capaCentral" style="position:absolute; width:100%; height:83%; z-index:0; left: 0px; top: 100px; overflow:auto;">
             </br></br>
             <table border="0"  align="center">
                         <tr>
                             <td>
                                 <table id="listUsuarios" ></table>
                             </td>
                         </tr>
             </table> 
             </br></br>
             <div align="center" id="bt_cerrar" > 
                 <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="javascript:window.close()">
                   <span class="ui-button-text">Salir</span>
                 </button>
             </div>
        </div>
        <div id="div_asoc_user_prointerno"  style="display: none; " title="Asociar Proceso Interno a Usuarios" >
             <div align="center" id="filtros" style="left: 5%; top: 2%;position:absolute;  width: 800px" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all">
                 </br>
                 <table aling="center" style=" width: 100%" >
                     <tr>
                         <td style="width: 15%"><span>Cod usuario:</span></td>
                         <td style="width: 10%"><input type="text" id="codUsuario" readOnly name="codUsuario" style=" width: 80px"></td>
                         <td style="width: 10%"><span>Nombre:</span></td>
                         <td style="width: 35%"><input type="text" id="nombre" readOnly style=" width: 300px" name="nombre"></td>
                         <td style="width: 10%"><span>Login:</span></td>
                         <td style="width: 20%"><input type="text" id="login" name="login" readOnly style=" width: 150px" ></td>
                     </tr>
                 </table>
                  </br>
             </div> 
             </br>
             <div id="bt_listar_ProInt" style="position:absolute; left: 30%; top: 10%;  "> 
                 <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="listarProInterno();">
                   <span class="ui-button-text">Listar ProInterno</span>
                 </button>
             </div>
             <div id="bt_asociar_ProInterno" style="position:absolute; left: 50%; top: 10%; ">
                 <button id="buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="asociarProinterno();">
                   <span class="ui-button-text">Asociar ProInterno</span>
                 </button>
             </div>
             <div id="div_prointerno"  style="position: absolute; left: 10%; top: 15%;">    
                     <table border="0"  align="center">
                         <tr>
                             <td>
                                 <table id="datosUserProInterno" ></table>
                             </td>
                         </tr>
                     </table>   
             </div>
             <div id="div_show_proInterno" style="position:absolute; left: 7%; top: 45%; ">    
                 <table border="0"  align="center">
                     <tr>
                         <td>
                             <table id="cargarProInterno" ></table>
                         </td>
                     </tr>
                 </table>   
             </div>

         </div> 
    </body>
</html>
