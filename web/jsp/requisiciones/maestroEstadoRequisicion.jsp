<%-- 
    Document   : maestroEstadoRequisicion
    Created on : 13/07/2015, 05:12:08 PM
    Author     : mariana
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/maestrosRequisicion.js"></script> 
        <script type="text/javascript" href="http://www.ddginc-usa.com/spanish/editcheck.js"></script>
        <title>MAESTRO ESTADO REQUISICION</title>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=MAESTRO ESTADO REQUISICION"/>
        </div>
    <center>

        <div style="position: relative;top: 150px;">
            <table id="tabla_estado_requisicion" ></table>
            <div id="pager"></div>
        </div>

        <div id="dialogMsjestadoRequisicion"  class="ventana" >
            <div id="tablita" style="top: 0px;width: 435px;" >
                <div id="encabezadotablita">
                    <label class="titulotablita"><b>ESTADO REQUISICION</b></label>
                </div> 
                <table id="tablainterna"  >
                    <tr>
                        <td>
                            <input type="text" id="idestadoreq" hidden  >
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label>Nombre de Estado</label>
                        </td>
                        <td>
                            <input type="text" id="descestadoreq" onchange="conMayusculas(this)" style="width: 200px"  >
                        </td>
                    </tr>
                </table>

                <center>
                    <p id="dialogMsjSMS"  ></p>  
                    <p id="sms2"></p> 
                </center>
            </div>
        </div>
        <div id="info"  class="ventana" >
            <p id="notific" >EXITO AL GUARDAR</p>
        </div>
    </center>

</body>
<script>
    iniciar2();
</script>
</html>
