<%--
    Document   : Modulo de Requisiciones Fintra
    Created on : 10/07/2013, 11:00:00 AM
    Author     : hcuello
--%>

<%@ page import="com.tsp.operation.model.beans.CmbGenericoBeans"%>
<%@ page import="com.tsp.operation.model.services.RequisicionesService"%>


<!-- page contentType="text/html" pageEncoding="ISO-8859-1" -->
<!-- page contentType="text/html" pageEncoding="UTF-8" -->

<%@ page contentType="text/html; charset=ISO-8859-1" language="java" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="java.text.SimpleDateFormat" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="javax.servlet.*" %>
<%@ page import    ="javax.servlet.http.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>



<%
    RequisicionesService rqservice= new RequisicionesService();
    ArrayList listaCombo =  rqservice.GetComboGenerico("SQL_OBTENER_COMBO_PROCESOS_SGC","id","descripcion","");


    Date dNow = new Date( );

       SimpleDateFormat nd = new SimpleDateFormat ("yyyy");
    String aniocte = nd.format(dNow);
    
    SimpleDateFormat md = new SimpleDateFormat ("M");
    String mescte = md.format(dNow);
    
    int acorriente = Integer.parseInt(aniocte);
    int mescorriente = Integer.parseInt(mescte);
    
%>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>CONTROL DE REQUISICIONES FINTRA</title>
        
        <link href="/fintra/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="<%=BASEURL%>/css/style_azul.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/editarClientesOriginal.css"/>
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/jquery/jquery-ui/jquery-ui.css"/>
        
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery-1.4.2.min.js"></script>
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/jquery-ui-1.8.5.custom.min.js"></script>
        
        
        <script language="JavaScript1.2">
        
        $(document).ready(function(){
            
            cargar_requisiciones();
            //cargar_mis_pendientes();
            //$("#cantidad").numeric();
            //$("#valor").numeric();
            $("#div_nueva_requisicion").draggable({ handle: "#drag_nueva_requisicion"});
            //$("#div_detalle_requisicion").draggable({ handle: "#drag_detalle_requisicion"});
            //$("#div_editar_requisicion").draggable({ handle: "#drag_editar_requisicion"});

        });            

        var ns4 = (document.layers);
        var ie4 = (document.all && !document.getElementById);
        var ie5 = (document.all && document.getElementById);
        var ns6 = (!document.all && document.getElementById);
        var msg = new Array();

        function Posicionar_div(id_objeto,e){
          //alert(e);
          //alert(window.event);
          obj = document.getElementById(id_objeto);
          var posx = 0;
          var posy = 0;
          if (!e) var e = window.event;
          if (e.pageX || e.pageY) {
           //alert('page');
           posx = e.pageX;
           posy = e.pageY;
          }
          else if (e.clientX || e.clientY) {
                //alert('client'); 
                posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
                posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop; 
              }
              else
               alert('ninguna de las anteriores');
         //alert('posx='+posx + ' posy=' + posy);
         obj.style.left = posx;
         obj.style.top = posy;
         //alert('objleft='+posx + ' objtop=' + posy);
         //document.getElementById('posicion').innerHTML = 'scrollLeft='+document.body.scrollLeft+' scrollTop='+document.body.scrollTop+' cientX'+e.clientX +' clientY'+e.clientY;
        }            

        function cargar_requisiciones(){
            //$('#div_espera').fadeIn('slow');
            //alert(document.getElementById('coco').value);
            //alert("La ruta es: "+$('#coco').val());

            $('#div_espera').fadeIn('slow');
            $.ajax({
                type: "POST",
                url : "/fintra/jsp/requisiciones/listado_requisiciones.jsp",
                async:true,
                dataType: "html",
                data:{
                    estado:$('#estado').val(),
                    mes:$('#mes').val(),
                    ano:$('#ano').val(),
                    proceso:$('#proceso_filtro').val()
                },
                success:function (data){
                    if (data!=""){
                        $("#misreq").html(data);
                        $('#div_espera').fadeOut('slow');
                    }
                }
            });
        }

        function CrearEditarRequisicion(accion,item,e){
            
            var cont_articulos = 0;
            var cont_item = 0;
            var edit_text = 0;

            $('#div_espera').fadeIn('slow');
            Posicionar_div('div_nueva_requisicion',e);
            $.ajax({
                type: "POST",
                url : "/fintra/jsp/requisiciones/nueva_requisicion.jsp",
                async:true,
                dataType: "html",
                data:{
                    estado:$('#estado').val(),
                    mes:$('#mes').val(),
                    ano:$('#ano').val(),
                    proceso:$('#proceso_filtro').val(),
                    ACCIONE:accion,
                    ITEM:item
                },

                success:function (data){
                    if (data!=""){
                        $("#div_nueva_requisicion").html(data);
                        $('#div_nueva_requisicion').fadeIn('slow');
                        $('#div_espera').fadeOut('slow');
                    }
                }
            });

            $('#div_espera').fadeOut('slow');
        }

        function Agregar_requisicion(){
        
            var cont = document.getElementById('controller').value;
            $.ajax({
                type: "POST",
                url : cont+"?estado=Requisicion&accion=Eventos",
                async:true,
                dataType: "html",
                data:{
                    evento:"INSERT",
                    proceso:$('#proceso').val(),
                    tiporeq:$('#tipo_req').val(),
                    asunto:$('#asunto').val(),
                    descripcion:$('#descripcion').val()

                },
                success:function (data){
                    //alert(data);
                    if (data="OK"){
                        cargar_requisiciones();
                        $('#div_nueva_requisicion').fadeOut('slow');
                    }else{
                        alert(".::ERROR AL CREAR LA REQUISICION::.");
                    }    
                }
            });
        }   

        function UpdateRequisicion(id){

            var cont = document.getElementById('controller').value;
            $.ajax({
                type: "POST",
                url : cont+"?estado=Requisicion&accion=Eventos",
                async:true,
                dataType: "html",
                data:{
                    evento:"UPDATE",
                    proceso:$('#proceso').val(),
                    tiporeq:$('#tipo_req').val(),
                    asunto:$('#asunto').val(),
                    descripcion:$('#descripcion').val(),
                    IdReq:id

                },
                success:function (data){
                    //alert(data);
                    if (data="OK"){
                        cargar_requisiciones();
                        $('#div_nueva_requisicion').fadeOut('slow');
                    }else{
                        alert(".::ERROR AL ACTUALIZAR LA REQUISICION::.");
                    }    
                }
            });

        }


        function Cargar_click_derecho_requisicion(id,e){

          Posicionar_div('div_menu_click_derecho_requisicion',e);
          $.ajax(
          {
          type: "POST",
          url : "/fintra/jsp/requisiciones/menu_click_derecho_requisicion.jsp",
          async:true,
          dataType: "html",
          data:{
                id:id
               },
          success:function (data){
                    if (data!=""){
                      $("#div_menu_click_derecho_requisicion").html(data);
                      $("#div_menu_click_derecho_requisicion").fadeIn('slow');
                      $("#div_menu_click_derecho_requisicion").show().delay(1000).fadeOut();
                    }
                  }
           }
          );

        }

    </script>
    
    <div id="div_espera" style="display:none;z-index:1000; position:absolute"></div>
    <div id="div_nueva_requisicion" style="display:none;z-index:101; position:absolute; border: 1px solid #165FB6; background:#FFFFFF; padding: 3px 3px 3px 3px;">XXXXX</div>
    <div id="div_editar_requisicion" style="display:none;z-index:100; position:absolute;border: 1px solid #165FB6; background:#FFFFFF; padding: 3px 3px 3px 3px;"></div>
    <div id="div_detalle_requisicion" style="display:none;z-index:102; position:absolute;border: 1px solid #165FB6; background:#FFFFFF; padding: 3px 3px 3px 3px;"></div>
    <div id="div_menu_click_derecho_requisicion" style="display:none;z-index:510;position:absolute;border:1px solid #165FB6; background-color:#FFFFFF; padding: 2px 2px 2px 2px;"></div>

    </head>

    <body align="center">
        
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=CONTROL DE REQUISICIONES"/>
        </div>
        <!-- ******************************************************************************************************* -->
        
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            
            <br>
            
            <center>
                
                <input type="hidden" id="baseurl" name="baseurl" value="<%=BASEURL%>">
                <input type="hidden" id="controller" name="controller" value ="<%=CONTROLLER%>"/>
                
                <table width="100%" border="0" cellpadding="0" cellspacing="1" class="labels">
                    
                    <tr>
                        <td colspan="2">
                            <fieldset>
                                
                                <legend class="labels">FILTROS</legend>
                                
                                <table width="435" border="0" align="left" cellpadding="1" cellspacing="1" class="labels">
                                  <tr>
                                    <td width="62"><fieldset>
                                      <legend>MES</legend>
                                      <select name="mes" class="combo_60px" id="mes" onChange="cargar_requisiciones();">

                                                    <option value="0">< todos ></option><% 

                                                    String meses = "";
                                                    String cadi = "";

                                                    for(int i=1; i<=12; i++){
                                                        int value = i;
                                                        switch (value) {
                                                                    case 1:
                                                                        meses="ENE";
                                                                        break;
                                                                    case 2:
                                                                        meses="FEB";
                                                                        break;
                                                                    case 3:
                                                                        meses="MAR";
                                                                        break;
                                                                    case 4:
                                                                        meses="ABR";
                                                                        break;
                                                                    case 5:
                                                                        meses="MAY";
                                                                        break;
                                                                    case 6:
                                                                        meses="JUN";
                                                                        break;
                                                                    case 7:
                                                                        meses="JUL";
                                                                        break;
                                                                    case 8:
                                                                        meses="AGO";
                                                                        break;
                                                                    case 9:
                                                                        meses="SEP";
                                                                        break;
                                                                    case 10:
                                                                        meses="OCT";
                                                                        break;
                                                                    case 11:
                                                                        meses="NOV";
                                                                        break;
                                                                    case 12:
                                                                        meses="DIC";
                                                                        break;
                                                        } 

                                                        if ( value == mescorriente ) {
                                                            cadi = "selected";
                                                        }else{ 
                                                            cadi = "";
                                                        }%>

                                                        <option value="<%=value%>" <%=cadi%> > <%=meses%> </option><%

                                                    } %>
                                      </select>
                                    </fieldset></td>
                                    <td width="62"><fieldset>
                                      <legend>A&Ntilde;O</legend>
                                      <select name="ano" class="combo_60px" id="ano" onChange="cargar_requisiciones();"><%
                                                    String cad = "";

                                                    for(int j=2009; j<=2020; j++){
                                                        int anio = j;
                                                        if ( anio == acorriente ) {
                                                            cad = "selected";
                                                        }else{ 
                                                            cad = "";
                                                        }%>
                                                        <option value="<%=anio%>" <%=cad%> > <%=anio%> </option><%
                                                    }%>
                                      </select>
                                    </fieldset></td>
                                    <td width="190">
                                        <fieldset>
                                            <legend>PROCESO</legend>
                                            <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                                <tr>
                                                    <td>
                                                        <select name="proceso_filtro" class="combo_180px" id="proceso_filtro" onChange="cargar_requisiciones();">
                                                            <option value="" selected>< todos ></option><%
                                                            for (int i = 0; i < listaCombo.size(); i++) {
                                                                CmbGenericoBeans CmbContenido = (CmbGenericoBeans) listaCombo.get(i);%>
                                                                 <option value="<%=CmbContenido.getIdCmb() %>"><%=CmbContenido.getDescripcionCmb() %></option><%
                                                            }%>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>
                                    
                                    <td width="108" ><fieldset>
                                      <legend>ESTADO</legend>
                                      <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                        <tr>
                                          <td><select name="estado" class="combo_100px" id="estado" onChange="cargar_requisiciones();">
                                              <option value="" selected>< todos ></option>
                                              <option value="1">PENDIENTE</option>
                                              <option value="2">ATENDIDA</option>
                                            </select>
                                          </td>
                                        </tr>
                                      </table>
                                    </fieldset></td>
                                    

                                    <td width="108" >
                                        <button id="actulizar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onClick="CrearEditarRequisicion('AGREGAR','',event)">
                                            <span class="ui-button-text">Nueva Req</span>
                                        </button>                                         
                                    </td>
                                    
                                  </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>  
  
                    <tr><td colspan="2" id="mensajes_sistema">&nbsp;</td></tr>
                    
                    <tr>
                        <td width="800" align="left">
                            <fieldset>
                            <legend>SALIDA - Generadas Por Mi</legend> 
                              <div id="fondo4" style="position:relative; width:1250px; height:600px; visibility: visible; overflow:auto;" align="center">
                                   <div id="misreq" align="left" style="position:absolute; z-index:58; left:0px; top:0px; visibility:visible;"></div>
                            </div>
                            </fieldset>
                        </td>

                        
                        <td width="600">
                            <fieldset>
                                <legend>ENTRADA - Asignaciones</legend> 
                                    <div id="fondo5" style="position:relative; width:320px; height:600px; visibility: visible; overflow:auto" align="center">
                                        <div id="mispendientes" align="center" style="width:120px; position:absolute; z-index:58; left:0px; top:0px; visibility:visible;">
                                            <!-- <table><tr><td id="mensajes_sistema" class="mensajes">sdfgsdfgsdfgsdfg</td></tr></table> -->
                                        </div>
                                    </div>
                              </fieldset>
                        </td>
                        
                    </tr>
                    
                    
                    <!-- 
                    <tr><td colspan="2" id="mensajes_sistema">&nbsp;</td></tr>
                    
                    <tr>
                        <td width="1800" align="center" >
                            <fieldset>
                            <legend>RECAUDO GENERAL</legend> 
                              <div id="fondo4" style="position:relative; width:1800px; height:600px; visibility: visible; overflow:auto;" align="center">
                                   <div id="misreq" align="center" style="position:absolute; z-index:58; left:0px; top:0px; visibility:visible;"></div>
                            </div>
                            </fieldset>
                        </td>
                        
                    </tr>                    
                    -->
                    
                    
                    <div id="box" style="top:281px; left:417px; position:absolute; z-index:1000; width: 43px; height: 29px;"></div>

                </table>                
                
                <div id="contenido">
                    <button id="actulizar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="parent.close()">
                        <span class="ui-button-text">Salir</span>
                    </button>
                </div>

                                                        
                <center class='comentario'>
                    <div id="comentario" style="visibility: hidden" >
                        <table border="2" align="center">
                            <tr>
                                <td>
                                    <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                        <tr>
                                            <td width="229" align="center" class="mensajes"><span class="normal"><div id="mensaje"></div></span></td>
                                            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                            <td width="58">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </center> 
                                            
            </center>
                    
             
        </div>
                                            
                                            
                                      
                                            
    </body>
</html>
