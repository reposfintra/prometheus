<%--
    Document   : Modulo de Requisiciones Fintra
    Created on : 10/07/2013, 11:00:00 AM
    Author     : hcuello
--%>


<%@page session="true" %>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@page import="com.tsp.operation.model.beans.CmbGenericoBeans"%>
<%@page import="com.tsp.operation.model.beans.CmbGeneralScBeans"%>
<%@page import="com.tsp.operation.model.services.RequisicionesService"%>
<%@page import="com.tsp.operation.model.beans.BeansGenericoRta"%>

<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="java.text.SimpleDateFormat" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>


<%
    
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    //String NombreUsuario = usuario.getNombre();
    String NmLogin = usuario.getLogin();
    
    String Qry = request.getParameter("_Qry");
    String QryCmb = request.getParameter("_QryCmb");
    String _Opcion = request.getParameter("_Opcion");
    String _NmObjt = request.getParameter("_NmObjt");
    String _Especifico = request.getParameter("_Especifico");
    String _var1 = request.getParameter("_var1");
    String _var2 = request.getParameter("_var2");
    String _var3 = request.getParameter("_var3");
    String _var4 = request.getParameter("_var4");
    
    String Pertenezco_a = "";
    
    RequisicionesService rqservice= new RequisicionesService(usuario.getBd());
    
    if ( _Opcion.equals("CmbPartners") ) {

        ArrayList laRepuesta = rqservice.RespuestaUnica(Qry, _var1, _var2, _var3, _var4);

        if ( laRepuesta.size() > 0 ) {

            BeansGenericoRta ListadoContenido = (BeansGenericoRta) laRepuesta.get(0);

            if ( _Especifico.equals("S") ) {
                int IdProceso = ListadoContenido.getId();
                Pertenezco_a = Integer.toString(IdProceso);
            }else{
                Pertenezco_a = "";
            }

            ArrayList listaComboCompaneros =  rqservice.GetComboGenericoStr(QryCmb,"login","login",Pertenezco_a); %>

            <select name="<%=_NmObjt%>" id="<%=_NmObjt%>" class="combo_300px"><%

                if ( listaComboCompaneros.size() > 0 ) { %>

                <option value='0' selected>-- SELECCIONAR --</option><%

                    for (int i = 0; i < listaComboCompaneros.size(); i++) {

                        CmbGeneralScBeans CmbColaboradores = (CmbGeneralScBeans) listaComboCompaneros.get(i);
                        
                        if ( _Especifico.equals("N") ) {
                            
                            if ( CmbColaboradores.getIdCmbStr().equals(NmLogin) ) {%>
                                <option value="<%=CmbColaboradores.getIdCmbStr() %>" selected><%=CmbColaboradores.getDescripcionCmb() %></option> <%
                            }else{ %>
                                <option value="<%=CmbColaboradores.getIdCmbStr() %>"><%=CmbColaboradores.getDescripcionCmb() %></option> <%
                            }
                        }else{ %>
                            <option value="<%=CmbColaboradores.getIdCmbStr() %>"><%=CmbColaboradores.getDescripcionCmb() %></option>  <%
                        }    
                    }

                }else{%>
                    <option value='0' selected>-Disabled-</option><%
                }%>            

            </select><%

        }else{ %>

            <select class="combo_300px_disabled" disabled="disabled"><option value='0' selected>-Disabled-</option></select><%

        }
        
    }else if ( _Opcion.equals("CmbTipico") ) { 
        
        ArrayList listaCombo = rqservice.GetComboGenerico(Qry,"login","login",""); %>

        <select name="generado_por" id="generado_por" class="combo_300px"><%

            if ( listaCombo.size() > 0 ) { 
                
                for (int i = 0; i < listaCombo.size(); i++) {

                    CmbGenericoBeans CmbColaboradores = (CmbGenericoBeans) listaCombo.get(i); %>
                    <option value="<%=CmbColaboradores.getIdCmb()%>"><%=CmbColaboradores.getDescripcionCmb() %></option> <%

                }

            }else{%>
                <option value='0' selected>-Disabled-</option><%
            }%>            

        </select><%

    }

%>
