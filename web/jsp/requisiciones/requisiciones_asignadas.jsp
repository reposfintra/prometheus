<%--
    Document   : Modulo de Requisiciones Fintra
    Created on : 10/07/2013, 11:00:00 AM
    Author     : hcuello
--%>

<%@page session="true" %>
<%@ page import="com.tsp.operation.model.beans.Usuario"%>
<%@ page import="com.tsp.operation.model.beans.RequisicionesListadoBeans"%>
<%@ page import="com.tsp.operation.model.services.RequisicionesService"%>

<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="java.text.SimpleDateFormat" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="javax.servlet.*" %>
<%@ page import    ="javax.servlet.http.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>


<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String NombreUsuario = usuario.getNombre();
    String NmLogin = usuario.getLogin();

    String estado = request.getParameter("estado") != "" ? request.getParameter("estado") : "";
    String mes = request.getParameter("mes") != "" ? request.getParameter("mes") : "";
    String ano = request.getParameter("ano") != "" ? request.getParameter("ano") : "";
    String proceso = request.getParameter("proceso") != "" ? request.getParameter("proceso") : "";
    String asignadas = request.getParameter("asignadas") != "" ? request.getParameter("asignadas") : "";
    String Prioridades = request.getParameter("selec_prioridades") != "" ? request.getParameter("selec_prioridades") : "";
    String TipoRequisicion = request.getParameter("tipo_tarearq") != "" ? request.getParameter("tipo_tarearq") : "";
    
    String item = "";
    String filtro = "FilterOwn1";

    RequisicionesService rqservice= new RequisicionesService(usuario.getBd());
    ArrayList listaRequisiciones =  rqservice.RequisicionesListadoBeans("SQL_OBTENER_LISTADO_REQUISICIONES",estado,mes,ano,proceso,asignadas,TipoRequisicion,Prioridades,NmLogin,item,filtro);
    
%>


<table width="600" height="18" border='0' align='left' cellpadding="0" cellspacing="0" class="tablas" id="tbl_req_asignadas">
    
    <tr>
        <th width='15' align='center'>EST</th>
        <th width='90' align='center'>FECHA</th>
        <th width='100' align='center'>DEL PROCESO</th>
        <th width='200' align='center'>ASUNTO</th>
        <th width='50' align='center'>PRIO-GEREN</th>
        <th width='20' align='center'>...</th>
    </tr><%

    String src = "";
    String src_end = "";
    String alt = "";
    String cierre = "";
    String prioridad = "";
    int idprioridad = 0;
    int PrioridadGerencia = 0;
        
    if ( listaRequisiciones.size() > 0 ) {
        
        for (int i = 0; i < listaRequisiciones.size(); i++) { 

            RequisicionesListadoBeans ListadoContenido = (RequisicionesListadoBeans) listaRequisiciones.get(i);
            cierre = (ListadoContenido.getFchCierre() != null ) ? ListadoContenido.getFchCierre() : "";
            prioridad = (ListadoContenido.getDscPrioridad() != null ) ? ListadoContenido.getDscPrioridad() : "";
            idprioridad = ListadoContenido.getIdPrioridad();
            PrioridadGerencia = ListadoContenido.getIdPriorizacion();
            
            if ( ListadoContenido.getIdEstado() == 1 ) {
                src = "/fintra/images/flag_red.gif";
                src_end = "/fintra/images/requisiciones.jpg";
                alt = "Pendiente";
            }else if ( ListadoContenido.getIdEstado() == 2 ) {
                src = "/fintra/images/flag_green.gif";
                src_end = "/fintra/images/requisiciones-Grey.jpg";
                alt = "Finalizado";
            }else{
                src = "/fintra/images/flag_orange.gif";
                src_end = "/fintra/images/requisiciones.jpg";
                alt = "Otra Accion";
            } %>            

            <tr ondblclick="Cargar_detalle_requisicion('VISUALIZAR',<%=ListadoContenido.getId() %>,event);"> 
                
                <td align="center" ><div style="float:center; width:18px"><img src="<%=src%>" alt="<%=alt%>" title="<%=alt%>" name="imageField" width="16" height="16" border="0" align="middle" id="imageField" /> </div></td>                  
                <td align="justify"><%=ListadoContenido.getFchRadicacion()%></td>
                <td align="left"><%=ListadoContenido.getDscProcesoSgc()%></td>
                <td align="justify"><%=ListadoContenido.getAsunto()%></td><%
                
                if ( idprioridad == 5 ) { %>
                    <td align="center"><%=prioridad%> - <%=PrioridadGerencia%></td><%
                }else{%>
                    <td align="center"><%=prioridad%></td><%
                } %>
                
                <td align="center">
                    <img style="cursor:pointer;" src="<%=src_end%>" title="Resolver Requisiciones" width="20" height="20" border="0" <%
                        if ( ListadoContenido.getIdEstado() != 2 ) {   %>
                            onclick="AtenderRequisicion(<%=ListadoContenido.getId()%>,event);return false;" <%
                        }else{ %>
                            onclick="alert('Requisicion Cerrada!');" <%
                        }%>
                    >
                </td>
            </tr><%

        }
  
    }else{%>
    
        <tr>
            <td colspan="10">NO SE HAN GENERADO REQUISICIONES</td>
        </tr><%
    
    }%>
</table>

    <script>
         $("#tbl_req_asignadas tr:even").addClass("even");
         $("#tbl_req_asignadas tr:odd").addClass("odd");
		 
		 $(
		   function()
		   {
			  $("#tbl_req_asignadas tr").hover(
			   function()
			   {
				$(this).addClass("highlight");
			   },
			   function()
			   {
				$(this).removeClass("highlight");
			   }
			  )
		   }
		  )
     </script>