<%--
    Document   : Modulo de Requisiciones Fintra
    Created on : 10/07/2013, 11:00:00 AM
    Author     : hcuello
--%>

<%@page session="true" %>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@ page import="com.tsp.operation.model.beans.CmbGenericoBeans"%>
<%@ page import="com.tsp.operation.model.services.RequisicionesService"%>


<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="java.text.SimpleDateFormat" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="javax.servlet.*" %>
<%@ page import    ="javax.servlet.http.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>



<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    RequisicionesService rqservice= new RequisicionesService(usuario.getBd());
    ArrayList listaCombo =  rqservice.GetComboGenerico("SQL_OBTENER_COMBO_PROCESOS_SGC","id","descripcion","");


    Date dNow = new Date( );

    SimpleDateFormat nd = new SimpleDateFormat ("yyyy");
    String aniocte = nd.format(dNow);
    
    SimpleDateFormat md = new SimpleDateFormat ("M");
    String mescte = md.format(dNow);
    
    int acorriente = Integer.parseInt(aniocte);
    int mescorriente = Integer.parseInt(mescte);
    
%>


<html>
    <head>
        
        <title>BANDEJA ENTRADA REQUISICIONES</title>
        
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/estilostsp.css" >
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/style_azul.css" >
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/jquery/jquery-ui/jquery-ui.css"/>
        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/jscal2.css" />
        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/border-radius.css" />        
        
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery-1.4.2.min.js"></script>
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/jquery-latest.js"></script>
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>  
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script> 
        

        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>        
        
        <script language="JavaScript1.2">
            
        var $j = jQuery.noConflict();    
        
        $(document).ready(
        
            function(){
            
                cargar_requisiciones();
                
                $j("#div_detalle_requisicion").draggable({ handle: "#drag_detalle_requisicion"});
                $j("#div_asignar_responsable").draggable({ handle: "#drag_asignar_responsable"});
                
            }
        );
       
        var ns4 = (document.layers);
        var ie4 = (document.all && !document.getElementById);
        var ie5 = (document.all && document.getElementById);
        var ns6 = (!document.all && document.getElementById);
        var msg = new Array();

        function Posicionar_div(id_objeto,e){
          //alert(e);
          //alert(window.event);
          obj = document.getElementById(id_objeto);
          var posx = 0;
          var posy = 0;
          if (!e) var e = window.event;
          if (e.pageX || e.pageY) {
           //alert('page');
           posx = e.pageX;
           posy = e.pageY;
          }
          else if (e.clientX || e.clientY) {
                //alert('client'); 
                posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
                posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop; 
              }
              else
               alert('ninguna de las anteriores');
         //alert('posx='+posx + ' posy=' + posy);
         obj.style.left = posx;
         obj.style.top = posy;
         //alert('objleft='+posx + ' objtop=' + posy);
         //document.getElementById('posicion').innerHTML = 'scrollLeft='+document.body.scrollLeft+' scrollTop='+document.body.scrollTop+' cientX'+e.clientX +' clientY'+e.clientY;
        } 
    
        function PosicionarDivLitleLeft(id_objeto,e,resta){
            
            obj = document.getElementById(id_objeto);
            
            var posx = 0;
            var posy = 0;
            
            if (!e) var e = window.event;
            
            if (e.pageX || e.pageY) {
                //alert('page');
                posx = e.pageX;
                posy = e.pageY;
            }else if (e.clientX || e.clientY) {
                  //alert('client'); 
                  posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
                  posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop; 
            }else
                alert('ninguna de las anteriores');
             
           //alert('posx='+posx + ' posy=' + posy);
           
           obj.style.left = posx-resta;
           obj.style.top = posy;
           //alert('objleft='+posx + ' objtop=' + posy);
           //document.getElementById('posicion').innerHTML = 'scrollLeft='+document.body.scrollLeft+' scrollTop='+document.body.scrollTop+' cientX'+e.clientX +' clientY'+e.clientY;
        } 
    
        function PosicionarDivLitleRight(id_objeto,e,suma){
            
            obj = document.getElementById(id_objeto);
            var posx = 0;
            var posy = 0;
            
            if (!e) var e = window.event;
            
            if (e.pageX || e.pageY) {
                //alert('page');
                posx = e.pageX;
                posy = e.pageY;
            }else if (e.clientX || e.clientY) {
                  //alert('client'); 
                  posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
                  posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop; 
            }else
                alert('ninguna de las anteriores');
            
           //alert('posx='+posx + ' posy=' + posy);
           obj.style.left = posx+suma;
           obj.style.top = posy;
           //alert('objleft='+posx + ' objtop=' + posy);
           //document.getElementById('posicion').innerHTML = 'scrollLeft='+document.body.scrollLeft+' scrollTop='+document.body.scrollTop+' cientX'+e.clientX +' clientY'+e.clientY;
        } 
        
        
        function cargar_requisiciones(){
            
            //$('#div_espera').fadeIn('slow');
            //alert(document.getElementById('coco').value);
            //alert("La ruta es: "+$('#coco').val());

            $('#div_espera').fadeIn('slow');
            $.ajax({
                type: "POST",
                url : "/fintra/jsp/requisiciones/requisiciones_entrantes.jsp",
                async:true,
                dataType: "html",
                data:{
                    ano:$('#ano').val(),
                    mes:$('#mes').val(),
                    proceso:$('#proceso_filtro').val(),
                    estado:$('#estado').val(),
                    asignadas:$('#asignadas').val()
                },
                success:function (data){
                    if (data!=""){
                        $("#misreq").html(data);
                        $('#div_espera').fadeOut('slow');
                    }
                }
            });
        }
        
        function Cargar_detalle_requisicion(accion,item,e){
            
            PosicionarDivLitleLeft('div_detalle_requisicion',e,600);
            $('#div_espera').fadeIn('slow');
            
            $.ajax({
                type: "POST",
                url : "/fintra/jsp/requisiciones/requisicion_detalle.jsp",
                async:true,
                dataType: "html",
                
                data:{
                    estado:$('#estado').val(),
                    mes:$('#mes').val(),
                    ano:$('#ano').val(),
                    proceso:$('#proceso_filtro').val(),
                    ACCIONE:accion,
                    ITEM:item,
                    StatusTask:"",
                    UserTask:"",
                    ItemTask:""
                },

                success:function (data){
                    if (data!=""){
                        $('#div_detalle_requisicion').html(data);
                        $('#div_detalle_requisicion').fadeIn('slow');
                        $('#div_espera').fadeOut('slow');
                    }
                }
            });

            $('#div_espera').fadeOut('slow');
            
        }
        
        function consultarNomarchivo(idReq,nomarchivo,containerId){
            var cont = document.getElementById('controller').value;
            $.ajax({
                type: "POST",
                url : cont+"?estado=Requisicion&accion=Eventos&evento=CONSULTAR_ARCHIVO",
                async:true,
                dataType: "html",
                data:{                   
                    IdReq: idReq,
                    nomarchivo:nomarchivo
                },
                success:function (archivo){
                    //alert(data);
                    if (archivo!=""){
                       // alert(archivo); 
                       
                            var dataFile = {   
                                estado:$('#estado').val(),
                                mes:$('#mes').val(),
                                ano:$('#ano').val(),
                                proceso:$('#proceso_filtro').val(),
                                selec_prioridades:$('#selec_prioridades').val(),
                                tipo_tarearq:$('#tipo_tarearq').val(),
                                ACCIONE:'VISUALIZAR',
                                ITEM:idReq,
                                StatusTask:"",
                                UserTask:"",
                                ItemTask:"",
                                archivolisto: nomarchivo
                            };                          
                       cargarpagina('/fintra/jsp/requisiciones/requisicion_detalle.jsp',dataFile,containerId);
                }else{
                        alert(".::ERROR AL RECUPERAR ARCHIVO.");
                    }    
                }
            });
        }   
       
        function cargarpagina(url, data, divId){
                   $.ajax({
                       type: "POST",
                       url : url,
                       async:true,
                       dataType: "html",
                       data: data,
                       success:function (data){
                           if (data!=""){
                               $("#"+divId).html(data);
                               $('#'+divId).fadeIn('slow');                       
                           }
                       }
                   });
        }
        
        function AsignarResponsable(item, estado, e){
            //alert("item es: "+item+", estado es: "+estado);
            if ( estado == 1 ) {
                
                PosicionarDivLitleLeft('div_asignar_responsable',e,300);
                $('#div_espera').fadeIn('slow');

                $.ajax({
                    type: "POST",
                    url : "/fintra/jsp/requisiciones/asignar_responsable.jsp",
                    async:true,
                    dataType: "html",
                    data:{
                        ITEM:item
                    },

                    success:function (data){
                        if (data!=""){
                            $("#div_asignar_responsable").html(data);
                            $('#div_asignar_responsable').fadeIn('slow');
                            $('#div_espera').fadeOut('slow');
                        }
                    }
                });

                $('#div_espera').fadeOut('slow');
                
            }else{
            
                alert("Esta Requisición ya está cerrada");
                
            }
        }  
        
        function GuardarResponsable(Rqid){
            
            var cont = document.getElementById('controller').value;
            $.ajax({
                type: "POST",
                url : cont+"?estado=Requisicion&accion=Eventos",
                async:true,
                dataType: "html",
                data:{
                    evento:"UPDATE_RESPONSABLE",
                    RqResponsable:Rqid,
                    UsuarioResponsable:$('input[name=RadioSelectResponsable]').filter(':checked').val()
                    
                },
                success:function (data){
                    //alert(data);
                    if (data="OK"){
                        cargar_requisiciones();
                        $('#div_asignar_responsable').fadeOut('slow');
                    }else{
                        alert(".::ERROR AL CREAR LA REQUISICION::.");
                    }    
                }
            });
        }           
        
        function Cargar_click_derecho_requisicion(id,e){
          
            Posicionar_div('div_menu_click_derecho_requisicion',e);

            $.ajax(
            {
            type: "POST",
            url : "/fintra/jsp/requisiciones/menu_click_derecho_requisicion.jsp",
            async:true,
            dataType: "html",
            data:{
                  id:id,
                  TypeRqMod:"RqIncoming"
                 },
            success:function (data){
                      if (data!=""){
                        $("#div_menu_click_derecho_requisicion").html(data);
                        $("#div_menu_click_derecho_requisicion").fadeIn('slow');
                        $("#div_menu_click_derecho_requisicion").show().delay(1000).fadeOut();
                      }
                    }
             }
            );

        }
        
        $( "td:parent" ).fadeTo( 1500, 0.3 );
        
    </script>
    
   
    
    
    <!--
    
        <script>
        $( "*", document.body ).each(function() {
        var parentTag = $( this ).parent().get( 0 ).tagName;
        $( this ).prepend( document.createTextNode( parentTag + " > " ) );
        });
        </script>       
    
    <style type="text/css">
            #TablaConProgramacion{	border: solid 1px #333;	width: 300px; }
            #TablaConProgramacion tbody tr{ background: #999; }
            .fila-base{ display: none; } /* fila base oculta */
            .eliminar{ cursor: pointer; color: #000; }
            input[type="text"]{ width: 80px; } /* ancho a los elementos input="text" */
    </style>    
    -->
    
    <div id="div_espera" style="display:none;z-index:1000; position:absolute"></div>
    <div id="div_detalle_requisicion" style="display:none;z-index:102; position:absolute; border: 1px solid #165FB6; background:#FFFFFF; padding: 3px 3px 3px 3px;">WWWWWWW</div>
    <div id="div_atender_requisicion" style="display:none;z-index:103; position:absolute; border: 1px solid #165FB6; background:#FFFFFF; padding: 3px 3px 3px 3px;">WAAAAAOOOOO</div>
    <div id="div_menu_click_derecho_requisicion" style="display:none;z-index:510;position:absolute;border:1px solid #165FB6; background-color:#FFFFFF; padding: 2px 2px 2px 2px;">YYY</div>
    <div id="div_asignar_responsable" style="display:none;z-index:103; position:absolute; border: 1px solid #165FB6; background:#FFFFFF; padding: 3px 3px 3px 3px;">torototo</div>
    
    </head>

    <body>
        
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=BANDEJA DE ENTRADA"/>
        </div>
        
        <!-- ******************************************************************************************************* -->
        
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            
            <br>
            
            <center>
                
                <input type="hidden" id="baseurl" name="baseurl" value="<%=BASEURL%>">
                <input type="hidden" id="controller" name="controller" value ="<%=CONTROLLER%>"/>
                
                <table width="100%" border="0" cellpadding="1" cellspacing="1" class="labels">
                    
                    <tr>
                        <td >
                            <fieldset>
                                
                                <legend class="labels">FILTROS</legend>
                                
                                <table width="635" border="0" align="left" cellpadding="1" cellspacing="1" class="labels">
                                    <tr>
                                      
                                      
                                    <td width="62"><fieldset>
                                      <legend>A&Ntilde;O</legend>
                                      <select name="ano" class="combo_60px" id="ano" onChange="cargar_requisiciones();"><%
                                                    String cad = "";

                                                    for(int j=2009; j<=2020; j++){
                                                        int anio = j;
                                                        if ( anio == acorriente ) {
                                                            cad = "selected";
                                                        }else{ 
                                                            cad = "";
                                                        }%>
                                                        <option value="<%=anio%>" <%=cad%> > <%=anio%> </option><%
                                                    }%>
                                      </select>
                                    </fieldset>
                                    </td>
                                      
                                    <td width="62"><fieldset>
                                      <legend>MES</legend>
                                      <select name="mes" class="combo_60px" id="mes" onChange="cargar_requisiciones();">

                                                    <option value="0">< todos ></option><% 

                                                    String meses = "";
                                                    String cadi = "";

                                                    for(int i=1; i<=12; i++){
                                                        int value = i;
                                                        switch (value) {
                                                                    case 1:
                                                                        meses="ENE";
                                                                        break;
                                                                    case 2:
                                                                        meses="FEB";
                                                                        break;
                                                                    case 3:
                                                                        meses="MAR";
                                                                        break;
                                                                    case 4:
                                                                        meses="ABR";
                                                                        break;
                                                                    case 5:
                                                                        meses="MAY";
                                                                        break;
                                                                    case 6:
                                                                        meses="JUN";
                                                                        break;
                                                                    case 7:
                                                                        meses="JUL";
                                                                        break;
                                                                    case 8:
                                                                        meses="AGO";
                                                                        break;
                                                                    case 9:
                                                                        meses="SEP";
                                                                        break;
                                                                    case 10:
                                                                        meses="OCT";
                                                                        break;
                                                                    case 11:
                                                                        meses="NOV";
                                                                        break;
                                                                    case 12:
                                                                        meses="DIC";
                                                                        break;
                                                        } 

                                                        if ( value == mescorriente ) {
                                                            cadi = "selected";
                                                        }else{ 
                                                            cadi = "";
                                                        }%>

                                                        <option value="<%=value%>" <%=cadi%> > <%=meses%> </option><%

                                                    } %>
                                      </select>
                                    </fieldset>
                                    </td>
                                    
                                    <td width="190">
                                        <fieldset>
                                            <legend>PROCESO</legend>
                                            <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                                <tr>
                                                    <td>
                                                        <select name="proceso_filtro" class="combo_180px" id="proceso_filtro" onChange="cargar_requisiciones();">
                                                            <option value="" selected>< todos ></option><%
                                                            for (int i = 0; i < listaCombo.size(); i++) {
                                                                CmbGenericoBeans CmbContenido = (CmbGenericoBeans) listaCombo.get(i);%>
                                                                 <option value="<%=CmbContenido.getIdCmb() %>"><%=CmbContenido.getDescripcionCmb() %></option><%
                                                            }%>
                                                        </select>
                                                    </td>
                                                </tr>
                                            </table>
                                        </fieldset>
                                    </td>
                                    
                                    <td width="108" ><fieldset>
                                      <legend>ESTADO</legend>
                                      <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                        <tr>
                                          <td><select name="estado" class="combo_100px" id="estado" onChange="cargar_requisiciones();">
                                              <option value="" selected>< todos ></option>
                                              <option value="1">PENDIENTE</option>
                                              <option value="2">ATENDIDA</option>
                                            </select>
                                          </td>
                                        </tr>
                                      </table>
                                    </fieldset>
                                    </td>
                                    
                                    <td width="108" ><fieldset>
                                      <legend>ASIGNADAS</legend>
                                      <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                        <tr>
                                          <td><select name="asignadas" class="combo_100px" id="asignadas" onChange="cargar_requisiciones();">
                                              <option value="" selected>< todos ></option>
                                              <option value="1">Si</option>
                                              <option value="0">No</option>
                                            </select>
                                          </td>
                                        </tr>
                                      </table>
                                    </fieldset></td>                                    
                                    
                                  </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>  
  
                    <!-- <tr><td id="mensajes_sistema" align="center" >&nbsp;</td></tr> -->
                    
                    <tr>
                        <td width="1900" align="center">
                            <fieldset>
                            <legend>BANDEJA DE ENTRADA - Moderar Requisiciones</legend> 
                            
                              <div id="fondo4" style="position:relative; width:1880px; height:600px; visibility: visible; overflow:auto; left:0px; top:0px;" align="center">
                                   <div id="misreq" align="center" style="position:absolute; z-index:58; left:0px; top:0px; visibility:visible;"></div>
                            </div>
                            
                            </fieldset>
                        </td>
                        
                    </tr>

                    <div id="box" style="top:281px; left:417px; position:absolute; z-index:1000; width: 43px; height: 29px;"></div>

                </table>                
                
                <div id="contenido">
                    <button id="actulizar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="parent.close()">
                        <span class="ui-button-text">Salir</span>
                    </button>
                </div>

                                                        
                <center class='comentario'>
                    <div id="comentario" style="visibility: hidden" >
                        <table border="2" align="center">
                            <tr>
                                <td>
                                    <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                        <tr>
                                            <td width="229" align="center" class="mensajes"><span class="normal"><div id="mensaje"></div></span></td>
                                            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                            <td width="58">&nbsp;</td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </center> 
                                            
            </center>
             
        </div>
                                            
    </body>
</html>
