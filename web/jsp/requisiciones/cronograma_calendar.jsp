<%-- 
    Document   : cronograma_calendar
    Created on : 9/07/2015, 03:46:07 PM
    Author     : mcastillo
--%>

<link href="./js/jquery/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
<link href="./js/jquery/fullcalendar/fullcalendar.print.css" rel="stylesheet" media='print'  type="text/css" /> 
<script type="text/javascript" src="./js/jquery/fullcalendar/lib/moment.min.js"></script>  
<script type="text/javascript" src="./js/jquery/fullcalendar/lib/jquery.min.js"></script>  
<script  type="text/javascript" src="./js/jquery/fullcalendar/fullcalendar.min.js"> </script>
<script  type="text/javascript" src="./js/jquery/fullcalendar/lang-all.js"> </script>

<script>
    
    $(document).ready(function() {
                var DATA_FEED_URL = "/fintra/controller?estado=Requisicion&accion=Eventos"; 
		$('#calendar').fullCalendar({
			header: {
				left: 'prev,next today',
				center: 'title',
				right: 'month,agendaWeek,agendaDay'
			},
                        axisFormat:'h(:mm) a',
			defaultDate: new Date(),
                        lang: 'es',
			editable: false,
			eventLimit: true, // allow "more" link when too many events
			events: {
                               url: DATA_FEED_URL + "&evento=VERCRONOGRAMA"
                        }
		});	
    });
   
</script>
<div id='calendar'></div>