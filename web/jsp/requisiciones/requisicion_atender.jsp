<%--
    Document   : Modulo de Requisiciones Fintra
    Created on : 10/07/2013, 11:00:00 AM
    Author     : hcuello
--%>

<%@page session="true" %>
<%@ page import="com.tsp.operation.model.beans.Usuario"%>
<%@ page import="com.tsp.operation.model.beans.CmbGenericoBeans"%>
<%@ page import="com.tsp.operation.model.services.RequisicionesService"%>
<%@ page import="com.tsp.operation.model.beans.RequisicionesListadoBeans"%>

<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="java.text.SimpleDateFormat" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>


<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String NombreUsuario = usuario.getNombre();
    String NmLogin = usuario.getLogin();
    
    int idReq = 0;
    int EstadoReq = 0;
    int IdProceso = 0;
    int IdTipReq = 0;
    int Autorizado = 0;
    String DscTipoRequisicion = "";
    String Asunto = "";
    String Descripcion = "";
    String Radicado = "";
    String DscProcesoSgc = "";
    String FchRadicacion = "";
    String Estado = "";
    String Color = "";
    String FchCierre = "";
    String Atiende = "";
    
    String evento = "";
    evento = request.getParameter("ACCIONE");
    
    String estado = request.getParameter("estado") != "" ? request.getParameter("estado") : "";
    String mes = request.getParameter("mes") != "" ? request.getParameter("mes") : "";
    String ano = request.getParameter("ano") != "" ? request.getParameter("ano") : "";
    String proceso = request.getParameter("proceso") != "" ? request.getParameter("proceso") : "";
    String asignadas = request.getParameter("asignadas") != "" ? request.getParameter("asignadas") : "";
    String Prioridades = request.getParameter("selec_prioridades") != "" ? request.getParameter("selec_prioridades") : "";
    String TipoRequisicion = request.getParameter("tipo_tarearq") != "" ? request.getParameter("tipo_tarearq") : "";
       
    String item = request.getParameter("ITEM") != "" ? request.getParameter("ITEM") : "";
    String filtro = "Editar";
   
    RequisicionesService rqservice= new RequisicionesService(usuario.getBd());
    
    ArrayList listaRequisiciones =  rqservice.RequisicionesListadoBeans("SQL_OBTENER_LISTADO_REQUISICIONES",estado,mes,ano,proceso,asignadas,TipoRequisicion,Prioridades,NmLogin,item,filtro);
    RequisicionesListadoBeans ListadoContenido = (RequisicionesListadoBeans) listaRequisiciones.get(0);
    
    String StatusTask = "";
    String UserTask = "";
    String ItemTask = "";
    String FchCierreTask = "";
    int TipoReqi = 0;
    
    ArrayList listaTareas =  rqservice.RequisicionesListareas("SQL_LISTADO_TAREAS",item, StatusTask, UserTask, ItemTask);
    ArrayList listaComboTipoTareas =  rqservice.GetComboGenerico("SQL_TIPO_TAREASRQ","id","descripcion","");
    //ArrayList listaComboEstadoRq =  rqservice.GetComboGenerico("SQL_ESTADO_REQ","id","descripcion","");
    ArrayList listaComboEstadoRq =  rqservice.GetComboGenerico("SQL_ESTADO_REQ_TAREAS","id","descripcion","");
    
    
    if ( listaTareas.size() > 0 ) {
        
        RequisicionesListadoBeans ListadoTasksRqOne = (RequisicionesListadoBeans) listaTareas.get(0);
        EstadoReq = ListadoTasksRqOne.getIdEstadoTask(); //ListadoContenido.getIdEstado();
        
    }

    idReq = ListadoContenido.getId();
    Radicado = ListadoContenido.getRadicado();
    FchRadicacion = ListadoContenido.getFchRadicacion();
    IdProceso = ListadoContenido.getIdProcesoSgc();
    DscProcesoSgc = ListadoContenido.getDscProcesoSgc();
    IdTipReq = ListadoContenido.getIdTipoRequisicion();
    DscTipoRequisicion = ListadoContenido.getsetDscTipoRequisicion();
    Asunto = ListadoContenido.getAsunto();
    Descripcion = ListadoContenido.getDescripcion();
    TipoReqi = ListadoContenido.getCodTaskRq() == 0 ? 2 : ListadoContenido.getCodTaskRq();
    Autorizado = ListadoContenido.getAutorizado();
    
    if ( !FchCierre.equals("") ){  FchCierre = ListadoContenido.getFchCierre(); }
    if ( ListadoContenido.getAtiende().equals("") ){  Estado = "PENDIENTE"; }else{ Estado = ListadoContenido.getEstado(); }
    
    if ( ListadoContenido.getIdEstado() == 1 ) {
        Color = "#FFFF66";
        Estado = "PENDIENTE";
    }else if ( ListadoContenido.getIdEstado() == 2 ) {
        Color = "#C1DAD7";
        Estado = "FINALIZADO";
    }else{
        //src = "/fintra/images/flag_orange.gif";
        Estado = "Otra Accion";
    }
    
%>

    <input type="hidden" id="idReqid" name="idReqid" value="<%=item%>">
    <input type="hidden" id="TipReqi" name="TipReqi" value="<%=TipoReqi%>">
    <input type="hidden" id="AutorizacionGerencia" name="AutorizacionGerencia" value="<%=Autorizado%>">
    
    <table width="500"  border="0" align="center" cellpadding="0" cellspacing="0" class="labels">

        <tr>
            <td id="drag_nueva_requisicion" style="cursor:pointer">
                <div class="k-header">
                    <span class="titulo">Atender Requisicion No: <%=Radicado%></span>
                    <span class="ui-widget-header ui-corner-all titulo_right" onClick="$('#div_atender_requisicion').fadeOut('slow')">x</span>
                </div>
            </td>
        </tr>    

        <tr><td>&nbsp;</td></tr> 

        <tr bgcolor='#fcfcfc'>

            <td height="7" align="left" class="labels">

                <table width="500" border="0" align="center" cellpadding="0" cellspacing="2" class="labels">

                    <tr bgcolor="#fcfcfc">
                        <td colspan="3">
                            <table width="100%" border="0" align="left" cellpadding="1" cellspacing="1">
                                <tr>
                                    <td width="98%" align="left" bgcolor="<%=Color%>" class="labels"><span class="style_redword">ESTADO: <%=Estado%></span></td>
                                    <td width="2%" align="right" ><img src="/fintra/images/printer.gif" title="Imprimir" width="22" height="18" onclick="" style="cursor:pointer"/></td>
                                </tr>
                            </table>                    
                        </td>                      
                    </tr>

                    <tr><td colspan="3" height="1" bgcolor="#000033"></td></tr>

                    <tr>
                        <td valign="top" align="left">FCH. SOLICITUD</td>
                        <td><input name="asunto" type="text" class="textbox_300" id="asunto" value="<%=FchRadicacion%>" readonly="readonly" ></td>
                        <td align="right" ><img src="/fintra/images/cronograma.png" title="Cronograma" width="32" height="32" id="Cronograma" onclick="verCronograma();cargarPagina('jsp/requisiciones/cronograma_calendar.jsp');" style="cursor:pointer"/></td>
                    </tr>

                    <tr>
                        <td valign="top" align="left">TIPO REQUISICION</td>
                        <td colspan="2"><input name="asunto" type="text" class="textbox_300" id="asunto" value="<%=DscTipoRequisicion%>" readonly="readonly"  ></td>
                    </tr>                


                    <tr>
                        <td width="95" valign="top" align="left">ASUNTO</td>
                        <td colspan="2"><input name="asunto" type="text" class="textbox_300" id="asunto" value="<%=Asunto%>" readonly="readonly"  ></td>
                    </tr>

                    <tr>
                        <td valign="top" align="left">DESCRIPCION</td>
                        <td colspan="2"><textarea name="descripcion" class="textbox_600x100" id="descripcion" readonly="readonly" ><%=Descripcion %></textarea></td>
                    </tr>

                    <tr  bgcolor="#fcfcfc">
                        <td colspan="3" align="left" valign="top">
                            <fieldset><legend>ACCIONES TOMADAS</legend>

                                <table width="100%" border="0" align="left" cellpadding="0" cellspacing="0" class="labels">

                                    <tr>
                                        <td width="100%">
                                            <fieldset><legend>TIPO DE ACTIVIDAD y ACCION</legend>
                                                <table width="100%" align="center" border="0" cellpadding="1" cellspacing="1" class="labels">
                                                    <tr>
                                                        
                                                        <td width="30%" align="left" >
                                                            <select name="tipo_tarea" class="combo_300px" id="tipo_tarea" onChange="PanelTipoTarea('Nuevo');"><%
                                                                if ( TipoReqi == 1 ) {

                                                                    for (int i = 0; i < listaComboTipoTareas.size(); i++) {

                                                                        CmbGenericoBeans CmbTipoTarea = (CmbGenericoBeans) listaComboTipoTareas.get(i);

                                                                        if ( CmbTipoTarea.getIdCmb() == TipoReqi ) {%>
                                                                            <option value="<%=CmbTipoTarea.getIdCmb() %>" selected><%=CmbTipoTarea.getDescripcionCmb() %></option> <%
                                                                        }                                                             
                                                                    }

                                                                }else{

                                                                    for (int i = 0; i < listaComboTipoTareas.size(); i++) {
                                                                        CmbGenericoBeans CmbTipoTarea = (CmbGenericoBeans) listaComboTipoTareas.get(i);

                                                                        if ( CmbTipoTarea.getIdCmb() == TipoReqi ) {%>
                                                                            <option value="<%=CmbTipoTarea.getIdCmb() %>" selected><%=CmbTipoTarea.getDescripcionCmb() %></option> <%
                                                                        }else{ %>
                                                                            <option value="<%=CmbTipoTarea.getIdCmb() %>"><%=CmbTipoTarea.getDescripcionCmb() %></option> <%
                                                                        }                                                             
                                                                    }
                                                                }%>
                                                          </select>
                                                        </td>
                                                    
                                                        <td width="70%" align="left" id="TdEstadoRq" style="display: block">
                                                            <select name="IdEstadoRq" class="combo_100px" id="IdEstadoRq" onChange="PanelTipoTarea('Nuevo');"><%
                                                                for (int i = 0; i < listaComboEstadoRq.size(); i++) {
                                                                    CmbGenericoBeans CmbEstadosRq = (CmbGenericoBeans) listaComboEstadoRq.get(i);

                                                                    if ( CmbEstadosRq.getIdCmb() == EstadoReq ) {%>
                                                                        <option value="<%=CmbEstadosRq.getIdCmb() %>" selected><%=CmbEstadosRq.getDescripcionCmb() %></option> <%
                                                                    }else{ %>
                                                                        <option value="<%=CmbEstadosRq.getIdCmb() %>"><%=CmbEstadosRq.getDescripcionCmb() %></option> <%
                                                                    }
                                                                }%>
                                                            </select>
                                                        </td>
                                                        
                                                    </tr>
                                                </table>
                                            </fieldset> 

                                            <div align="center" style="position:relative; width:960px; height:400px; visibility: visible; overflow:auto">

                                                <div id="ConProgramacion" style="display: none;position:absolute;left:0px; top:0px">
                                                    <form name="form_trouble" id="form_trouble" action="">
                                                    <fieldset><legend>ACTIVIDADES</legend>
                                                        
                                                        <table id="TablaConProgramacion" class="labels" >
                                                                <!-- Cabecera de la tabla -->
                                                                <thead>
                                                                        <tr>
                                                                            <th>TAREA / ACTIVIDADES</th>
                                                                            <th>INICIO ESTIMADO</th>
                                                                            <th>FIN ESTIMADO</th>
                                                                            <th>TOTAL HORAS</th>
                                                                            <th>FINALIZACION ACT</th>
                                                                            <th>REPOROCESO</th>
                                                                            <th><input type="button" id="agregar" value="+" onclick="AgregarTarea()" ></th>
                                                                        </tr>
                                                                </thead>

                                                                <!-- Cuerpo de la tabla con los campos -->

                                                                <tbody>
                                                                    
                                                                        <!-- fila base para clonar y agregar al final -->
                                                                        <tr class="fila-base">
                                                                                <td style="display:none;"><input type="hidden" name="" id="" value=""></td>
                                                                                <td><input type="text" name="" id="" class="input_450px" value="" placeholder="Describe la actividad a Realizar..."></td>
                                                                                <td><input type="text" name="" id="" class="inputCalendar" value="" readonly="readonly" placeholder="Dbl Click..."></td>
                                                                                <td><input type="text" name="" id="" class="inputCalendar" value="" readonly="readonly" placeholder="Dbl Click..."></td>
                                                                                <td><input type="text" name="" id="" class="inputHoras_60px" value="" readonly="readonly"></td>
                                                                                <td><input type="text" name="" id="" class="inputCalendarNews" value="" readonly="readonly" placeholder="Disabled!"></td>
                                                                                <td><input type="text" name="" id="" class="inputHoras_60px" value="" readonly="readonly" placeholder="Disabled!"></td>
                                                                                <td><input type="button" id="agregar" class="eliminar" value="-"></td>
                                                                        </tr><%

                                                                        int secuencia = 1;
                                                                        if ( listaTareas.size() > 0 ) { 
                                                                            
                                                                            for (int i = 0; i < listaTareas.size(); i++) { 

                                                                                RequisicionesListadoBeans ListadoTasksRq = (RequisicionesListadoBeans) listaTareas.get(i);
                                                                                FchCierreTask = (!ListadoTasksRq.getFchTerminacion().equals("0099-01-01") ) ? ListadoTasksRq.getFchTerminacion() : ""; %>
                                                                                
                                                                                <tr class="fila-repeat">
                                                                                    <td style="display:none;"><input type="hidden" name="IdTareaRq<%=secuencia%>" id="IdTareaRq<%=secuencia%>" value="<%=ListadoTasksRq.getIdTask()%>"></td>
                                                                                    <td><input type="text" name="tarea<%=secuencia%>" id="tarea<%=secuencia%>" class="input_450px" value="<%=ListadoTasksRq.getDescripcionTask()%>"></td>
                                                                                    <td><input type="text" name="CalendarInputFie<%=secuencia%>" id="CalendarInputFie<%=secuencia%>" class="inputCalendarCargados" value="<%=ListadoTasksRq.getFchInicioTask()%>" readonly="readonly"></td>
                                                                                    <td><input type="text" name="CalendarInputFin<%=secuencia%>" id="CalendarInputFin<%=secuencia%>" class="inputCalendarCargados" value="<%=ListadoTasksRq.getFchFinTask()%>" readonly="readonly"></td>
                                                                                    <td><input type="text" name="horas<%=secuencia%>" id="horas<%=secuencia%>" class="inputHoras_60px" value="<%=ListadoTasksRq.getHourTask()%>" readonly="readonly"></td>
                                                                                    <td><input type="text" name="CalendarInputFci<%=secuencia%>" id="CalendarInputFci<%=secuencia%>" class="inputCalendarCargados" value="<%=FchCierreTask%>" readonly="readonly"></td>
                                                                                    <td><input type="text" name="reproceso<%=secuencia%>" id="reproceso<%=secuencia%>" class="inputHoras_60px" value="<%=ListadoTasksRq.getHoraReprocesoTask()%>" readonly="readonly" placeholder="Disabled!"></td><%
                                                                                    
                                                                                    if ( FchCierreTask == "" ) { %>
                                                                                        <td><input type="button" id="agregar" class="del_onesave" value="-" onclick="EliminarTarea(<%=ListadoTasksRq.getIdTask()%>)"></td> <%
                                                                                    }else{ %>
                                                                                        <td><input type="button" id="agregar" class="del_disabled" value=" " onclick="alert('ATENCION: Actividad Cerrada. NOTA: El moderador puede reactivarla')" ></td> <%
                                                                                    } %>
                                                                                        
                                                                                </tr><%
                                                                                
                                                                                secuencia++;
                                                                                
                                                                            }%>
                                                                            
                                                                            <input type="hidden" id="SecuenciaDinamica" value="<%=secuencia%>"><%
                                                                            
                                                                        }else{%>
                                                                        
                                                                            <tr class="fila-real" > <!-- style="display:none;" || Edt -->
                                                                                    <td style="display:none;"><input type="hidden" name="IdTareaRq<%=secuencia%>" id="IdTareaRq<%=secuencia%>" value=""></td>
                                                                                    <td><input type="text" name="tarea<%=secuencia%>" id="tarea<%=secuencia%>" class="input_450px" value="" placeholder="Describe la actividad a Realizar..."></td>
                                                                                    <td><input type="text" name="CalendarInputFie<%=secuencia%>" id="CalendarInputFie<%=secuencia%>" class="inputCalendar" value="" readonly="readonly" placeholder="Dbl Click..."></td>
                                                                                    <td><input type="text" name="CalendarInputFin<%=secuencia%>" id="CalendarInputFin<%=secuencia%>" class="inputCalendar" value="" readonly="readonly" placeholder="Dbl Click..."></td>
                                                                                    <td><input type="text" name="horas<%=secuencia%>" id="horas<%=secuencia%>" class="inputHoras_60px" value="" readonly="readonly"></td>
                                                                                    <td><input type="text" name="CalendarInputFci<%=secuencia%>" id="CalendarInputFci<%=secuencia%>" class="inputCalendarNews" value="" readonly="readonly" placeholder="Disabled!"></td>
                                                                                    <td><input type="text" name="reproceso<%=secuencia%>" id="reproceso<%=secuencia%>" class="inputHoras_60px" value="" readonly="readonly" placeholder="Disabled!"></td>
                                                                                    <td><input type="button" id="agregar" class="eliminar" value="-"></td> <!-- inputCalendarNews -->
                                                                            </tr>
                                                                            
                                                                            <input type="hidden" id="SecuenciaDinamica" value="<%=secuencia+1%>"><%
                                                                            
                                                                        }%>
                                                                        
                                                                </tbody>
                                                        </table>
                                                        
                                                    </fieldset> 
                                                    </form>
                                                </div>


                                                <div id="SinProgramacion" style="display: block;position:absolute;left:0px; top:0px">
                                                    <fieldset><legend>ACCION TOMADA</legend>
                                                        <table id="TablaSinProgramacion" class="labels"><%
                                                            
                                                            if ( listaTareas.size() > 0 ) { 

                                                                RequisicionesListadoBeans ListadoTasksRq = (RequisicionesListadoBeans) listaTareas.get(0);%>
                                                                
                                                                <input type="hidden" name="IdAccionRq" id="IdAccionRq" value="<%=ListadoTasksRq.getIdTask()%>">
                                                                <tr>
                                                                    <td><textarea name="DscAccion" id="DscAccion" class="textbox_930x100"><%=ListadoTasksRq.getDescripcionTask()%></textarea></td>
                                                                </tr><%

                                                            }else{%>
                                                            
                                                                <input type="hidden" name="IdAccionRq" id="IdAccionRq" value="0">
                                                                <tr>
                                                                    <td><textarea name="DscAccion" id="DscAccion" class="textbox_930x100"></textarea></td>
                                                                </tr><%
                                                            }%>
                                                            
                                                        </table>
                                                    </fieldset> 
                                                </div>

                                            </div>

                                        </td>
                                    </tr>
                                    
                                </table>

                            </fieldset>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2" align="center">
                                <button id="actulizar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="GuardarTarea();"><span class="ui-button-text">Guardar</span></button>
                        </td>
                    </tr>                


                </table>

            </td>

        </tr>

    </table>
</form>