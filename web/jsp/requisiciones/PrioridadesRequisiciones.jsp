<%-- 
    Document   : PrioridadesRequisiciones
    Created on : 9/07/2015, 09:30:24 AM
    Author     : egonzalez
--%>

<%@page import="java.util.Date"%>
<%@ page import    ="java.text.SimpleDateFormat" %>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>

<%
    Date dNow = new Date( );

    SimpleDateFormat nd = new SimpleDateFormat ("yyyy");
    String aniocte = nd.format(dNow);
    
    SimpleDateFormat md = new SimpleDateFormat ("M");
    String mescte = md.format(dNow);
    
    int acorriente = Integer.parseInt(aniocte);
    int mescorriente = Integer.parseInt(mescte);
    
%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Priorizacion de requisiciones</title>

        <link href="/fintra/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="/fintra/css/jquery/jquery-ui/jquery-ui.css"/>
        <link type="text/css" rel="stylesheet" href="/fintra/css/jquery/jquery.jqGrid/ui.jqgrid.css"/>
        <link href="/fintra/css/style_azul.css" rel="stylesheet" type="text/css">

        <!--
        <script type="text/javascript" src="/fintra/js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="/fintra/js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="/fintra/js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="/fintra/js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        -->

        <script type="text/javascript" src="/fintra/js/jquery/jquery.jqGrid/js/jquery.min.js"></script> 
        <script type="text/javascript" src="/fintra/js/jquery-ui-1.8.5.custom.min.js"></script>
        
        <script type="text/javascript" src="/fintra/js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="/fintra/js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="/fintra/js/jquery/jquery.jqGrid-4.7.0/plugins/jquery.tablednd.js"></script>
        
        <script type="text/javascript" src="/fintra/js/PrioridadesRequisiciones.js"></script>

        <style type="text/css">
            .ui-jqgrid .ui-jqgrid-htable th div {
                height: auto;
                overflow: hidden;
                padding-right: 4px;
                padding-top: 2px;
                position: relative;
                vertical-align: text-top;
                white-space: normal !important;
            }
            .prioridad {
                color:red;
                font-weight: bold !important; 
            }
        </style>

    </head>
    <body onload="init();">
        <div id="div_espera" style="display:none;z-index:1000; position:absolute"></div> 
        <div id="div_detalle_requisicion" style="display:none;z-index:102; position:absolute; border: 1px solid #165FB6; background:#FFFFFF; padding: 3px 3px 3px 3px;">WWWWWWW</div>
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Priorizacion de requisiciones"/>

        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: auto;" align="center">
            <center>
                <div>
                    <fieldset style="text-align:left; max-width: 95%;">
                        <legend>FILTROS</legend>
                        <fieldset style="display:inline-block">
                            <legend>A�o</legend>
                            <select id='anho' class="combo_60px"></select>
                        </fieldset>
                        <fieldset style="display:inline-block">
                            <legend>Mes</legend>
                            
                            <select id='mes' class="combo_60px"><% 

                                String meses = "";
                                String cadi = "";
                                String ValorString = "";
                                String ValueCmbo = "";

                                for(int i=1; i<=12; i++){
                                    
                                    int value = i;
                                    
                                    ValorString = Integer.toString(value);;
                                    if ( ValorString.length() == 1 ) { 
                                        ValueCmbo = "0"+ValorString;
                                    }else{ 
                                        ValueCmbo = ValorString;
                                    }                                    
                                    
                                    switch (value) {
                                        case 1:
                                            meses="ENE";
                                            break;
                                        case 2:
                                            meses="FEB";
                                            break;
                                        case 3:
                                            meses="MAR";
                                            break;
                                        case 4:
                                            meses="ABR";
                                            break;
                                        case 5:
                                            meses="MAY";
                                            break;
                                        case 6:
                                            meses="JUN";
                                            break;
                                        case 7:
                                            meses="JUL";
                                            break;
                                        case 8:
                                            meses="AGO";
                                            break;
                                        case 9:
                                            meses="SEP";
                                            break;
                                        case 10:
                                            meses="OCT";
                                            break;
                                        case 11:
                                            meses="NOV";
                                            break;
                                        case 12:
                                            meses="DIC";
                                            break;
                                    } 
                                    
                                    if ( value == mescorriente ) {
                                        cadi = "selected";
                                    }else{ 
                                        cadi = "";
                                    }%>

                                    <option value="<%=ValueCmbo%>" <%=cadi%> > <%=meses%> </option><%

                                } %>                                

                            </select>
                        </fieldset>
                        <fieldset style="display:inline-block">
                            <legend>Procesos</legend>
                            <select id='procesos'>
                                <option>...</option>
                                <option value="13">proceso</option>
                            </select>
                        </fieldset>
                        <input id="buscar" type="button" value="Buscar" onclick='buscarRequisiciones();'/>
                        <input id="guardar" type="button" value="Priorizar" onclick='guardarRequisiciones();'/>
                    </fieldset>
                </div>
                <br>
                <table id="tabla_requisiciones"></table>
                <div id="page_requisiciones"></div>
            </center>           
        </div>       
    </body>
</html>
