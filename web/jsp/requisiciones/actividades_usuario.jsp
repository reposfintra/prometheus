<%-- 
    Document   : actividades_usuario
    Created on : 2/12/2015, 04:06:46 PM
    Author     : mcastillo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1">
        <title>Actividades por Usuario</title>
        
        <link href="./css/style_azul.css" rel="stylesheet" type="text/css">
        <link href="./css/jquery/jquery-ui/jquery-ui.css" rel="stylesheet" type="text/css">
        <link href="./js/jquery/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css" />
        <link href="./js/jquery/fullcalendar/fullcalendar.print.css" rel="stylesheet" media='print'  type="text/css" /> 
        <link href="./css/cronogramaActividadesReq.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>  
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script> 
        <script type="text/javascript" src="./js/jquery/fullcalendar/lib/moment.min.js"></script>  
        <script type="text/javascript" src="./js/jquery/fullcalendar/lib/jquery.min.js"></script>  
        <script  type="text/javascript" src="./js/jquery/fullcalendar/fullcalendar.min.js"></script>
        <script  type="text/javascript" src="./js/jquery/fullcalendar/lang-all.js"></script>
        <script type="text/javascript" src="./js/Cronograma_Actividades.js"></script>
    </head>
    <body>
        
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Actividades por Usuario"/>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: auto;" align="center">
            
                <center>
                <div id="div_actividades_usuario">
                    
                    <fieldset style="text-align:left; max-width: 52%;">
                        <legend>FILTROS</legend>
                        <fieldset style="display:inline-block">
                            <legend>Año</legend>
                            <select id='anho'> </select>
                        </fieldset>
                        <fieldset style="display:inline-block">
                            <legend>Mes</legend>
                            <select id='mes'></select>
                        </fieldset>
                        <fieldset style="display:inline-block">
                            <legend>Procesos</legend>
                            <select id='procesos'> </select>
                        </fieldset>
                        <fieldset style="display:inline-block">
                            <legend>Usuarios</legend>
                            <select id='usuarios'> </select>
                        </fieldset>
                        <input id="buscar_acts" type="button" value="Ver actividades"/>                   
                    </fieldset>
                 </div>
                <br>
                <div id="eventContent" title="Detalles de la actividad" style="display:none;">                  
                    <p id="eventInfo"></p>                  
                </div>
                <fieldset id="calcontainer" style="display:none;background: white none repeat scroll 0 0;max-width: 52%;">
                     <div id="calendar" > </div> 
                </fieldset>
                      
            </center>
       </div>
        
    </body>
</html>
