<%--
    Document   : Modulo de Requisiciones Fintra
    Created on : 10/07/2013, 11:00:00 AM
    Author     : hcuello
--%>

<%@page session="true" %>
<%@page import="com.tsp.operation.model.beans.Usuario"%>
<%@ page import="com.tsp.operation.model.beans.CmbGenericoBeans"%>
<%@ page import="com.tsp.operation.model.services.RequisicionesService"%>


<!-- page contentType="text/html" pageEncoding="ISO-8859-1" -->
<!-- page contentType="text/html" pageEncoding="UTF-8" -->

<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="java.text.SimpleDateFormat" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="javax.servlet.*" %>
<%@ page import    ="javax.servlet.http.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>



<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    RequisicionesService rqservice= new RequisicionesService(usuario.getBd());
    ArrayList listaCombo =  rqservice.GetComboGenerico("SQL_OBTENER_COMBO_PROCESOS_SGC","id","descripcion","");

    Date dNow = new Date( );

    SimpleDateFormat nd = new SimpleDateFormat ("yyyy");
    String aniocte = nd.format(dNow);
    
    SimpleDateFormat md = new SimpleDateFormat ("M");
    String mescte = md.format(dNow);
    
    int acorriente = Integer.parseInt(aniocte);
    int mescorriente = Integer.parseInt(mescte);
    
%>


<html>
    
    <head>
        
        <title>INDICADORES SISTEMAS</title>
        
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/estilostsp.css" >
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/style_azul.css" >
        <link type="text/css" rel="stylesheet" href="<%=BASEURL%>/css/jquery/jquery-ui/jquery-ui.css"/>
        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/jscal2.css" />
        <link rel="stylesheet" type="text/css" href="<%=BASEURL%>/css/jCal/border-radius.css" />        
        
        <script type="text/javascript" src="<%=BASEURL%>/js/jquery-1.4.2.min.js"></script>
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/jquery-latest.js"></script>
        <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/jquery-ui-1.8.5.custom.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>  
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script> 

        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/jscal2.js"></script>
        <script type="text/javascript" src="<%=BASEURL%>/js/jCal/lang/es.js"></script>
        
        <script language="JavaScript1.2">
            
        var $j = jQuery.noConflict();
        var miFecha = new Date();
        var Monthy = miFecha.getMonth()+1;
        
        $(document).ready(
                
            function(){
                cargar_indicadores(document.getElementById('cmb_ano').value, Monthy, document.getElementById('proceso_filtro').value);
                
            }
        );
       
        var ns4 = (document.layers);
        var ie4 = (document.all && !document.getElementById);
        var ie5 = (document.all && document.getElementById);
        var ns6 = (!document.all && document.getElementById);
        var msg = new Array();

        function Posicionar_div(id_objeto,e){
          //alert(e);
          //alert(window.event);
          obj = document.getElementById(id_objeto);
          var posx = 0;
          var posy = 0;
          if (!e) var e = window.event;
          if (e.pageX || e.pageY) {
           //alert('page');
           posx = e.pageX;
           posy = e.pageY;
          }
          else if (e.clientX || e.clientY) {
                //alert('client'); 
                posx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
                posy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop; 
              }
              else
               alert('ninguna de las anteriores');
         //alert('posx='+posx + ' posy=' + posy);
         obj.style.left = posx;
         obj.style.top = posy;
         //alert('objleft='+posx + ' objtop=' + posy);
         //document.getElementById('posicion').innerHTML = 'scrollLeft='+document.body.scrollLeft+' scrollTop='+document.body.scrollTop+' cientX'+e.clientX +' clientY'+e.clientY;
        } 
    
        function cargar_indicadores(ano, mes, proceso){
            
            //$('#div_espera').fadeIn('slow');
            //alert("ano: "+ano+"mes: "+mes+"proceso: "+proceso);
            
            $('#div_espera').fadeIn('slow');
            $.ajax({
                type: "POST",
                url : "/fintra/jsp/requisiciones/req_indicador_resultados.jsp",
                async:true,
                dataType: "html",
                data:{
                    ano:ano,
                    mes:mes,
                    proceso:proceso
                },
                success:function (data){
                    if (data!=""){
                        $("#misreq").html(data);
                        $('#div_espera').fadeOut('slow');
                    }
                }
            });
            
        }
        
        
        $( "td:parent" ).fadeTo( 1500, 0.3 );
        
    </script>
    
    </head>    
    
    <body>    
        
        <div id="div_espera" style="display:none;z-index:1000; position:absolute"></div>
        <div id="div_nueva_requisicion" style="display:none;z-index:101; position:absolute; border: 1px solid #165FB6; background:#FFFFFF; padding: 3px 3px 3px 3px;">XXXXXXXXXXXXXXXX</div>

        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/toptsp.jsp?encabezado=INDICADORES"/>
        </div>        
        
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            
            <center>
                
                <input type="hidden" id="baseurl" name="baseurl" value="<%=BASEURL%>">
                <input type="hidden" id="controller" name="controller" value ="<%=CONTROLLER%>"/>
                
                <table width="70%" border="0" cellpadding="1" cellspacing="1" class="labels">
                    
                    <tr>
                        <td >
                            <fieldset>
                                
                                <legend class="labels">FILTROS</legend>
                                
                                <table width="635" border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                    <tr>
                                        <td width="62"><fieldset>
                                            <legend>A&Ntilde;O</legend>
                                            <select name="cmb_ano" class="combo_60px" id="cmb_ano" onChange="cargar_indicadores(document.getElementById('cmb_ano').value, '01', document.getElementById('proceso_filtro').value);"><%
                                                        String cad = "";

                                                        for(int j=2009; j<=2020; j++){
                                                            int anio = j;
                                                            if ( anio == acorriente ) {
                                                                cad = "selected";
                                                            }else{ 
                                                                cad = "";
                                                            }%>
                                                            <option value="<%=anio%>" <%=cad%> > <%=anio%> </option><%
                                                        }%>
                                            </select> 
                                            </fieldset>
                                        </td>
                                      
                                        <td width="190">
                                            <fieldset>
                                                <legend>PROCESO</legend>
                                                <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                                    <tr>
                                                        <td>
                                                            <select name="proceso_filtro" class="combo_180px" id="proceso_filtro" onChange="cargar_indicadores(document.getElementById('cmb_ano').value, '01', document.getElementById('proceso_filtro').value);">
                                                                <option value="" selected>< todos ></option><%
                                                                for (int i = 0; i < listaCombo.size(); i++) {
                                                                    CmbGenericoBeans CmbContenido = (CmbGenericoBeans) listaCombo.get(i);%>
                                                                     <option value="<%=CmbContenido.getIdCmb() %>"><%=CmbContenido.getDescripcionCmb() %></option><%
                                                                }%>
                                                            </select>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </td>
                                    
                                        <td>
                                            <table width="500" border="0" align="center" cellpadding="0" cellspacing="0" class="tablas" id="tbl_indicador">
                                                <tr>
                                                    <th class="labels" width="50" id="mes1" onclick="cargar_indicadores(document.getElementById('cmb_ano').value, '01', document.getElementById('proceso_filtro').value);"><div align="center">ENE</div></th> <!-- onMouseOut="cambiar_color_out(this)" -->
                                                    <td><div align="center">|</div></td>
                                                    <th class="labels" width="50" id="mes2" onclick="cargar_indicadores(document.getElementById('cmb_ano').value, '02', document.getElementById('proceso_filtro').value);"><div align="center">FEB</div></th>
                                                    <td><div align="center">|</div></td>
                                                    <th  class="labels" width="50" id="mes3" onclick="cargar_indicadores(document.getElementById('cmb_ano').value, '03', document.getElementById('proceso_filtro').value);"><div align="center">MAR</div></th>
                                                    <td><div align="center">|</div></td>
                                                    <th  class="labels" width="50" id="mes4" onclick="cargar_indicadores(document.getElementById('cmb_ano').value, '04', document.getElementById('proceso_filtro').value);"><div align="center">ABR</div></th>
                                                    <td><div align="center">|</div></td>
                                                    <th  class="labels" width="50" id="mes5" onclick="cargar_indicadores(document.getElementById('cmb_ano').value, '05', document.getElementById('proceso_filtro').value);"><div align="center">MAY</div></th>
                                                    <td><div align="center">|</div></td>
                                                    <th  class="labels" width="50" id="mes6" onclick="cargar_indicadores(document.getElementById('cmb_ano').value, '06', document.getElementById('proceso_filtro').value);"><div align="center">JUN</div></th>
                                                    <td><div align="center">|</div></td>
                                                    <th  class="labels" width="50" id="mes7" onclick="cargar_indicadores(document.getElementById('cmb_ano').value, '07', document.getElementById('proceso_filtro').value);"><div align="center">JUL</div></th>
                                                    <td><div align="center">|</div></td>
                                                    <th  class="labels" width="50" id="mes8" onclick="cargar_indicadores(document.getElementById('cmb_ano').value, '08', document.getElementById('proceso_filtro').value);"><div align="center">AGO</div></th>
                                                    <td><div align="center">|</div></td>
                                                    <th  class="labels" width="50" id="mes9" onclick="cargar_indicadores(document.getElementById('cmb_ano').value, '09', document.getElementById('proceso_filtro').value);"><div align="center">SEP</div></th>
                                                    <td><div align="center">|</div></td>
                                                    <th  class="labels" width="50" id="mes10" onclick="cargar_indicadores(document.getElementById('cmb_ano').value, '10', document.getElementById('proceso_filtro').value);"><div align="center">OCT</div></th>
                                                    <td><div align="center">|</div></td>
                                                    <th  class="labels" width="50" id="mes11" onclick="cargar_indicadores(document.getElementById('cmb_ano').value, '11', document.getElementById('proceso_filtro').value);"><div align="center">NOV</div></th>
                                                    <td><div align="center">|</div></td>
                                                    <th  class="labels" width="50" id="mes12" onclick="cargar_indicadores(document.getElementById('cmb_ano').value, '12', document.getElementById('proceso_filtro').value);"><div align="center">DIC</div></th>
                                                </tr>
                                            </table>    
                                        </td>                                     
                                    
                                    
                                    
                                  </tr>
                                </table>
                            </fieldset>
                        </td>
                    </tr>  
  
                    <!-- <tr><td id="mensajes_sistema" align="center" >&nbsp;</td></tr> -->
                    
                    <tr>
                        <td width="900" align="center">
                            <fieldset>
                            <legend>ESTADISTICAS REQUISICIONES</legend> 
                            
                                <div id="fondo4" style="position:relative; width:980px; height:600px; visibility: visible; overflow:auto; left:0px; top:0px;" align="center">
                                    <div id="misreq" align="center" style="position:absolute; z-index:58; left:0px; top:0px; visibility:visible;"></div>
                                </div>
                            
                            </fieldset>
                        </td>
                        
                    </tr>

                    <div id="box" style="top:281px; left:417px; position:absolute; z-index:1000; width: 43px; height: 29px;"></div>

                </table>    
                                                            
                <script>
                    $("#tbl_indicador th:even").addClass("even");
                    $("#tbl_indicador th:odd").addClass("odd");

                    $(
                        function() {
                            
                            $("#tbl_indicador th").hover(
                               function(){
                                    $(this).addClass("highlight");
                               },
                               function(){
                                   $(this).removeClass("highlight");
                               }
                            )
                    
                            $("#tbl_indicador th").click(
                                function(){    
                                    //$("#tbl_indicador th:even").removeClass("even");
                                    //$("#tbl_indicador th:odd").removeClass("odd");
                                    //alert("Three");
                                }            
                            )        
                        }
                    )
                 </script>                                                            
                
        </div>                                    
    </body>
    
    
</html>
