<%--
    Document   : Modulo de Requisiciones Fintra
    Created on : 10/07/2013, 11:00:00 AM
    Author     : hcuello
--%>

<%@page session="true" %>
<%@ page import="com.tsp.operation.model.beans.Usuario"%>
<%@ page import="com.tsp.operation.model.beans.CmbGenericoBeans"%>
<%@ page import="com.tsp.operation.model.services.RequisicionesService"%>
<%@ page import="com.tsp.operation.model.beans.RequisicionesListadoBeans"%>

<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="java.text.SimpleDateFormat" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>


<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String NombreUsuario = usuario.getNombre();
    String NmLogin = usuario.getLogin();
    
    String QryProceso = "";
    int idReq = 0;
    int IdProceso = 0;
    String QryTipReq = "";
    int IdTipReq = 0;
    String DscTipoRequisicion = "";
    String Asunto = "";
    String Descripcion = "";
    String Radicado = "";
    String DscProcesoSgc = "";
    String FchRadicacion = "";
    String Estado = "";
    String Color = "";
    String FchCierre = "";
    String Atiende = "";
    int CodTypeRq = 0;
    double SumaHoras = 0;
    
    String FchInicioTask = "";
    String FchFinTask = "";
    String FchCierreTask = "";
    
    String evento = "";
    evento = request.getParameter("ACCIONE");
    
    String estado = request.getParameter("estado") != "" ? request.getParameter("estado") : "";
    String mes = request.getParameter("mes") != "" ? request.getParameter("mes") : "";
    String ano = request.getParameter("ano") != "" ? request.getParameter("ano") : "";
    String proceso = request.getParameter("proceso") != "" ? request.getParameter("proceso") : "";
    String asignadas = request.getParameter("asignadas") != "" ? request.getParameter("asignadas") : "";
    String Prioridades = request.getParameter("selec_prioridades") != "" ? request.getParameter("selec_prioridades") : "";
    String TipoRequisicion = request.getParameter("tipo_tarearq") != "" ? request.getParameter("tipo_tarearq") : "";
    
    String item = request.getParameter("ITEM") != "" ? request.getParameter("ITEM") : "";

    String StatusTask = request.getParameter("StatusTask") != "" ? request.getParameter("StatusTask") : "";
    String UserTask = request.getParameter("UserTask") != "" ? request.getParameter("UserTask") : "";
    String ItemTask = request.getParameter("ItemTask") != "" ? request.getParameter("ItemTask") : "";
    String archivolisto = (request.getParameter("archivolisto")!= null) ? request.getParameter("archivolisto") : "";
    
    String filtro = "Editar";
   
    RequisicionesService rqservice= new RequisicionesService(usuario.getBd());
    
    QryProceso = "SQL_OBTENER_COMBO_PROCESOS_SGC";
    QryTipReq = "SQL_OBTENER_TIPO_REQUISICION";

    ArrayList listaRequisiciones =  rqservice.RequisicionesListadoBeans("SQL_OBTENER_LISTADO_REQUISICIONES",estado,mes,ano,proceso,asignadas,TipoRequisicion,Prioridades,NmLogin,item,filtro);
    RequisicionesListadoBeans ListadoContenido = (RequisicionesListadoBeans) listaRequisiciones.get(0);
    
    ArrayList listaTareas =  rqservice.RequisicionesListareas("SQL_LISTADO_TAREAS",item, StatusTask, UserTask, ItemTask);
    
    idReq = ListadoContenido.getId();
    Radicado = ListadoContenido.getRadicado();
    FchRadicacion = ListadoContenido.getFchRadicacion();
    IdProceso = ListadoContenido.getIdProcesoSgc();
    DscProcesoSgc = ListadoContenido.getDscProcesoSgc();
    IdTipReq = ListadoContenido.getIdTipoRequisicion();
    DscTipoRequisicion = ListadoContenido.getsetDscTipoRequisicion();
    Asunto = ListadoContenido.getAsunto();
    Descripcion = ListadoContenido.getDescripcion();
    CodTypeRq = ListadoContenido.getCodTaskRq();
    

    FchCierre = (ListadoContenido.getFchCierre() != null ) ? ListadoContenido.getFchCierre() : "";
    
    Atiende = ListadoContenido.getAtiende();
    
    if ( ListadoContenido.getIdEstado() == 1 ) {
        Color = "#FFFF66";
        Estado = "PENDIENTE";
    }else if ( ListadoContenido.getIdEstado() == 2 ) {
        Color = "#C1DAD7";
        Estado = "FINALIZADO";
    }else{
        //src = "/fintra/images/flag_orange.gif";
        Estado = "Otra Accion";
    }    
%>

<table width="650"  border="0" align="center" cellpadding="0" cellspacing="0" class="labels">
    
    <tr>
        <td id="drag_nueva_requisicion" style="cursor:pointer">
            <div class="k-header">
                <span class="titulo">Detalle Requisicion No: <%=Radicado%></span>
                <span class="ui-widget-header ui-corner-all titulo_right" onClick="$('#div_detalle_requisicion').fadeOut('slow')">x</span>
            </div>
        </td>
    </tr>    
    
    <tr><td>&nbsp;</td></tr>

    <tr bgcolor='#fcfcfc'>
        
        <td height="7" align="left" class="labels">
            
            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="2" class="labels">
                
                <tr bgcolor="#fcfcfc">
                    <td colspan="2">
                        <table width="100%" border="0" align="left" cellpadding="1" cellspacing="1">
                            <tr>
                                <td width="98%" align="left" bgcolor="<%=Color%>" class="labels"><span class="style_redword">ESTADO: <%=Estado%></span></td>
                                <td width="2%" align="right" ><img src="/fintra/images/printer.gif" alt="Imprimir" width="22" height="18" onclick="Imprimir_rq(<%=idReq %>)" style="cursor:pointer"/></td>
                            </tr>
                        </table>                    
                    </td>                      
                </tr>
                
                <tr><td align="center" colspan="2" height="1" bgcolor="#000033"></td></tr>
              
                <tr>
                    <td align="right">PROCESO: </td>
                    <td><input name="asunto" type="text" class="textbox_450" id="asunto" value="<%=DscProcesoSgc%>" readonly="readonly" disabled="disabled" ></td>
                </tr>
                
                <tr>
                    <td align="right">FCH. SOLICITUD: </td>
                    <td><input name="asunto" type="text" class="textbox_450" id="asunto" value="<%=FchRadicacion%>" readonly="readonly" disabled="disabled" ></td>
                </tr>
                
                <tr>
                    <td align="right">TIPO REQUISICION: </td>
                    <td><input name="asunto" type="text" class="textbox_450" id="asunto" value="<%=DscTipoRequisicion%>" readonly="readonly" disabled="disabled" ></td>
                </tr>                
                
                
                <tr>
                    <td align="right">ASUNTO: </td>
                    <td><input name="asunto" type="text" class="textbox_450" id="asunto" value="<%=Asunto%>" readonly="readonly" disabled="disabled" ></td>
                </tr>
                
                <tr>
                    <td align="right">DESCRIPCION: </td>
                    <!-- <td><input name="asunto" type="text" class="textbox_300" id="asunto" value="<%=Descripcion%>" readonly="readonly" disabled="disabled" ></td> -->
                    <td><textarea name="descripcion" class="textbox_450x100px" id="descripcion" readonly="readonly"><%=Descripcion %></textarea></td>
                </tr>
                
                <tr>
                    <td align="right">ATIENDE: </td>
                    <td><input name="asunto" type="text" class="textbox_450" id="asunto" value="<%=Atiende%>" readonly="readonly" disabled="disabled" ></td>
                </tr>

                <tr>
                    <td align="right">FCH. CIERRE: </td>
                    <td><input name="asunto" type="text" class="textbox_450" id="asunto" value="<%=FchCierre%>" readonly="readonly" disabled="disabled" ></td>
                </tr>
              
                <tr>
                    <td align="right" colspan="2">
                        
                        <fieldset><legend>ACCIONES TOMADAS</legend>

                            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablas" id="tbl_details_tareas"><%
                                
                                if ( ListadoContenido.getCodTaskRq() == 1 ) { %>

                                    <tr>
                                        <th width="40%" >TAREAS</th>
                                        <th>INICIA</th>
                                        <th>FINALIZA</th>
                                        <th>HORAS</th>
                                        <th>ESTADO</th>
                                        <th>FCH CIERRE</th>
                                    </tr><%

                                    if ( listaTareas.size() > 0 ) {

                                        for (int i = 0; i < listaTareas.size(); i++) { 

                                            RequisicionesListadoBeans ListadoTasksRq = (RequisicionesListadoBeans) listaTareas.get(i); 
                                            FchInicioTask = (ListadoTasksRq.getFchInicioTask() != null ) ? ListadoTasksRq.getFchInicioTask() : "";
                                            FchFinTask = (ListadoTasksRq.getFchFinTask() != null ) ? ListadoTasksRq.getFchFinTask() : "";
                                            FchCierreTask = (ListadoTasksRq.getFchTerminacion() != null ) ? ListadoTasksRq.getFchTerminacion() : ""; 
                                            //FchCierreTask = (ListadoTasksRq.getDescEstadoTask() != null ) ? ListadoTasksRq.getDescEstadoTask() : ""; 
                                            SumaHoras = SumaHoras + Double.parseDouble(ListadoTasksRq.getHourTask()); %>

                                            <tr>
                                                <td align="left"><%=ListadoTasksRq.getDescripcionTask()%></td>
                                                <td align="center"><%=FchInicioTask%></td>
                                                <td align="center"><%=FchFinTask%></td>
                                                <td align="center"><%=ListadoTasksRq.getHourTask()%></td>
                                                <td align="justify"><%=ListadoTasksRq.getDescEstadoTask()%></td>
                                                <td align="justify"><%=FchCierreTask%></td>
                                            </tr><%
                                        }%>

                                        <tr>
                                            <th colspan="3">TOTAL HORAS:</th>
                                            <th><%= SumaHoras %></th>
                                            <th colspan="2"></th>
                                        </tr><%

                                    }else{%>

                                        <tr><td colspan="10" bgcolor="#FFFF66">NO HAY TAREAS REGISTRADAS</td></tr><%

                                    }
                                        
                                }else{ %>
                                
                                    <tr>
                                        <th width="70%" >ACTIVIDAD</th>
                                        <th>ESTADO</th>
                                        <th>FCH ACCION</th>
                                    </tr><%
                                    
                                    if ( listaTareas.size() > 0 ) {

                                        for (int i = 0; i < listaTareas.size(); i++) { 

                                            RequisicionesListadoBeans ListadoTasksRq = (RequisicionesListadoBeans) listaTareas.get(i); 
                                            FchCierreTask = (ListadoTasksRq.getFchTerminacion() != null ) ? ListadoTasksRq.getFchTerminacion() : ""; %>

                                            <tr>
                                                <td align="left"><%=ListadoTasksRq.getDescripcionTask()%></td>
                                                <td align="center"><%=ListadoTasksRq.getDescEstadoTask()%></td>
                                                <td align="center"><%=FchCierreTask%></td>
                                            </tr><%
                                        }

                                    }else{%>

                                        <tr><td colspan="10" bgcolor="#FFFF66">NO HAY TAREAS REGISTRADAS</td></tr><%

                                    }                                    
                                    
                                } %>
                                
                            </table>

                        </fieldset>
                                
                    </td>
                </tr>
                <tr>
                    <td align="right" colspan="2">
                        <fieldset><legend>ARCHIVOS CARGADOS</legend>

                            <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="tablas" id="tbl_details_archivos">
                                <%
                                    java.util.List nombresArchivos = rqservice.getNombresArchivos(String.valueOf(idReq));
                                    for (int w = 0; w < nombresArchivos.size(); w++) {
                                %>
                                <tr class="fila">

                                    <td ><a target="_blank"  onClick="consultarNomarchivo('<%=String.valueOf(idReq)%>','<%=nombresArchivos.get(w)%>','div_detalle_requisicion');" style="cursor:hand" ><strong><%=nombresArchivos.get(w)%></strong>				
                                        </a>
                                        <%
                                            if (archivolisto != null && nombresArchivos.get(w).equals(archivolisto)) {
                                        %>		

                                        &nbsp;&nbsp;&nbsp;<a  target="_blank" href="<%=BASEURL%>/images/multiservicios/<%=NmLogin%>/<%=archivolisto%>">ver</a>                                   

                                        <%}%>
                                    </td>

                                </tr>
                                <%}%>
                            </table>

                        </fieldset>
                    </td>
                </tr>
                <!--
                <tr  bgcolor="#fcfcfc">
                    <td colspan="2" align="left" valign="top">
                        <fieldset><legend>ARCHIVOS RELACIONADOS</legend>

                            <table width="293" border="0" align="left" cellpadding="0" cellspacing="0" class="labels">

                                <tr><td height="1" bgcolor="#000033"></td></tr>

                                <tr>
                                    <td>
                                        <table width="256" border="0" align="left" cellpadding="1" cellspacing="1">
                                            <tr>
                                                <td width="28" align="center"><a href="descargar_archivo.php?id=<? echo $RcSetArchivo->fields['id']; ?>"><img src="<? echo $src; ?>" width="16" height="16" border="0" /></a></td>
                                                <td width="170" class="labels"><a href="descargar_archivo.php?id=<? echo $RcSetArchivo->fields['id']; ?>">aaaaa</a> </td>
                                                <td width="48" class="labels">5 MB</td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>

                            </table>

                        </fieldset>
                    </td>
                </tr>
                -->
            </table>
                        
        </td>
        
    </tr>
    
</table>
                                
<div id="contenido" align="center" >
    <button id="actulizar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" role="button" aria-disabled="false" onclick="$('#div_detalle_requisicion').fadeOut('slow')">
        <span class="ui-button-text">Salir</span>
    </button>
</div>
                                
                                
<script>
     $("#tbl_details_tareas tr:even").addClass("even");
     $("#tbl_details_tareas tr:odd").addClass("odd");

             $(
               function()
               {
                      $("#tbl_req_asignadas tr").hover(
                       function()
                       {
                            $(this).addClass("highlight");
                       },
                       function()
                       {
                            $(this).removeClass("highlight");
                       }
                      )
               }
              )
 </script>
