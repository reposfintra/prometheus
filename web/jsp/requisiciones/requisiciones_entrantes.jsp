<%--
    Document   : Modulo de Requisiciones Fintra
    Created on : 10/07/2013, 11:00:00 AM
    Author     : hcuello
--%>

<%@page session="true" %>
<%@ page import="com.tsp.operation.model.beans.Usuario"%>
<%@ page import="com.tsp.operation.model.beans.RequisicionesListadoBeans"%>
<%@ page import="com.tsp.operation.model.services.RequisicionesService"%>

<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ page session   ="true"%>
<%@ page errorPage ="/error/ErrorPage.jsp"%>
<%@ page import    ="java.util.*" %>
<%@ page import    ="java.text.SimpleDateFormat" %>
<%@ page import    ="com.tsp.util.*" %>
<%@ page import    ="javax.servlet.*" %>
<%@ page import    ="javax.servlet.http.*" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>


<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    String NombreUsuario = usuario.getNombre();
    String NmLogin = usuario.getLogin();

    String estado = request.getParameter("estado") != "" ? request.getParameter("estado") : "";
    String mes = request.getParameter("mes") != "" ? request.getParameter("mes") : "";
    String ano = request.getParameter("ano") != "" ? request.getParameter("ano") : "";
    String proceso = request.getParameter("proceso") != "" ? request.getParameter("proceso") : "";
    String asignadas = request.getParameter("asignadas") != "" ? request.getParameter("asignadas") : "";
    String Prioridades = request.getParameter("selec_prioridades") != "" ? request.getParameter("selec_prioridades") : "";
    String TipoRequisicion = request.getParameter("tipo_tarearq") != "" ? request.getParameter("tipo_tarearq") : "";
    
    String item = "";
    String filtro = "FilterBandejaEntrada";

    RequisicionesService rqservice= new RequisicionesService(usuario.getBd());
    ArrayList listaRequisiciones =  rqservice.RequisicionesListadoBeans("SQL_OBTENER_LISTADO_REQUISICIONES",estado,mes,ano,proceso,asignadas,TipoRequisicion,Prioridades,NmLogin,item,filtro);
    
%>


<table width="1880" height="18" border='1' align='center' cellpadding="0" cellspacing="0" class="tablas" id="tbl_proceso">
    
    <tr>
        <th width='20' align='center'>EST</th>
        <th width='51' align='center'>RAD</th>
        <th width='110' align='center'>FECHA</th>
        <th width='120' align='center'>TIPO RQ</th>
        <th width='170' align='center'>DEL PROCESO</th>
        <th width='90' align='center'>GENERADO POR</th>
        <th width='70' align='center'>PRIORIDAD</th>
        <th width='300' align='center'>ASUNTO</th>
        <th width='90'  align='center'>RESPONSABLE</th>
        <th width='128'  align='center'>TIPO TAREA</th>
        
        <th width='100'  align='center'>FECHA INICIO</th>
        <th width='100'  align='center'>FECHA FIN</th>
        <th width='100'  align='center'>HORAS TRABAJO</th>
        
        <th width='110' align='center'>CIERRE</th>
        
        <th width='50' align='center'>...</th>

    </tr><%

    String src = "";
    String alt = "";
    String cierre = "";
    String prioridad = "";
    String src_developer = "";
    
    
    if ( listaRequisiciones.size() > 0 ) {
        
        for (int i = 0; i < listaRequisiciones.size(); i++) { 

            RequisicionesListadoBeans ListadoContenido = (RequisicionesListadoBeans) listaRequisiciones.get(i);
            cierre = (ListadoContenido.getFchCierre() != null ) ? ListadoContenido.getFchCierre() : "";
            prioridad = (ListadoContenido.getDscPrioridad() != null ) ? ListadoContenido.getDscPrioridad() : "";
            String atendiendo = (ListadoContenido.getAtiende() != null ) ? ListadoContenido.getAtiende() : "";
            String FchRqInicio = (ListadoContenido.getRqFchInicio() != null ) ? ListadoContenido.getRqFchInicio() : "";
            String FchRqFin = (ListadoContenido.getRqFchFin() != null ) ? ListadoContenido.getRqFchFin() : "";
            
            if ( ListadoContenido.getIdEstado() == 1 ) {
                src = "/fintra/images/flag_red.gif";
                src_developer = "/fintra/images/devoloper.png";
                alt = "Pendiente";
            }else if ( ListadoContenido.getIdEstado() == 2 ) {
                src = "/fintra/images/flag_green.gif";
                src_developer = "/fintra/images/devoloper-gris.png";
                alt = "Finalizado";
            }else{
                src = "/fintra/images/flag_orange.gif";
                src_developer = "/fintra/images/devoloper.png";
                alt = "Otra Accion";
            } %>    
    
            <tr ondblclick="Cargar_detalle_requisicion('VISUALIZAR',<%=ListadoContenido.getId() %>,event);">  <!-- oncontextmenu="Cargar_click_derecho_requisicion(<%=ListadoContenido.getId() %>,event);return false;" -->
                
                <td align="center" ><div style="float:center; width:18px">
                       
                        <img src="<%=src%>" alt="<%=alt%>" title="<%=alt%>" name="imageField" width="16" height="16" border="0" align="middle" id="imageField" /> </div>
                    
                        <div style="float:right; width:18px"><%
                            if ( ListadoContenido.getAutorizado() == 99 ){ %>
                                <img src="/fintra/images/good.gif" alt="VoBo director <%=ListadoContenido.getAutoriza() %>"  title="VoBo director <%=ListadoContenido.getAutoriza() %>" name="imageField" width="26" height="23" border="0" align="middle" id="imageField3" /> <%
                            }%>
                        </div>
                </td>

                <td align="left"><%=ListadoContenido.getRadicado()%></td>
                <td align="justify"><%=ListadoContenido.getFchRadicacion()%></td>
                <td align="left"><%=ListadoContenido.getsetDscTipoRequisicion()%></td>
                <td align="left"><%=ListadoContenido.getDscProcesoSgc()%></td>
                <td align="center"><%=ListadoContenido.getUsuario()%></td>
                <td align="center"><%=prioridad%></td>
                <td align="justify"><%=ListadoContenido.getAsunto()%></td>
                <td align="center"><%=atendiendo%></td>
                <td align="justify"><%=ListadoContenido.getDscTipoTarea()%></td>
                
                <td align="center"><%=FchRqInicio%></td>
                <td align="center"><%=FchRqFin%></td>
                <td align="center"><%=ListadoContenido.getHorasTrabajo()%></td>
                
                <td align="left"><%=cierre%></td>
                
                <td align="center" ><div style="float:center; width:18px">
                    <div style="float:center; width:18px">
                        <img src="<%=src_developer%>"  title="Asignar a Desarrollador" name="imageDeveloper" width="24" height="21" border="0" align="middle" id="imageDeveloper" onclick="AsignarResponsable(<%=ListadoContenido.getId() %>, <%=ListadoContenido.getIdEstado() %>, event);" />
                    </div>
                </td>                
                
            </tr><%

        }
  
    }else{%>
    
        <tr>
            <td colspan="10">NO SE HAN GENERADO REQUISICIONES</td>
        </tr><%
    
    }%>
</table>

<script>
    $("#tbl_proceso tr:even").addClass("even");
    $("#tbl_proceso tr:odd").addClass("odd");

    $(
        function() {
            $("#tbl_proceso tr").hover(
               function(){
                    $(this).addClass("highlight");
               },
               function(){
                    $(this).removeClass("highlight");
               }
            )
        }
    )
 </script>