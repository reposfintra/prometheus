<%@ page session="true"%>
<%@ page errorPage="/error/ErrorPage.jsp"%>
<%@ page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
    <title>Buscar Costos Adicionales Planilla</title>
    <script src="<%=BASEURL%>/js/validacodextraflete.js"></script>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
	<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
	<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<% 
    TreeMap agencias = (TreeMap) request.getAttribute("agencias_tm");
	agencias.put( " Seleccione", "");
	Codextraflete codextra = (Codextraflete) request.getAttribute("codextra");
%> 

<body onresize="redimensionar()" onload = "redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Costos Adicionales Planilla"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
	<form name="form1" id="form1" method="post" action="<%=CONTROLLER%>?estado=Codextraflete&accion=Insert&opcion=2" >
    <table width="530" border="2" align="center">
		<tr>
			<td>

				<table width="100%" align="center">
					<tr>
						<td width="173" class="subtitulo1">Informacion Costos Adicionales</td>
						<td width="205" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
					</tr>					
					<tr class="fila" >
						<td width="166" nowrap >Codigo:</td>
						<td width="288" nowrap ><input name="codigo" type="text" class="textbox" id="codigo" value="<%= codextra!=null ? codextra.getCodextraflete() : "" %>" size="6" maxlength="6" <%= request.getParameter("retorno")!= null ? "readonly" : "" %>></td>
				  </tr>
					<tr class="fila">
						<td width="166" nowrap>Descripcion: </td>
						<td nowrap><input name="descripcion" type="text" class="textbox" id="descripcion2" value="<%= codextra!=null ? codextra.getDescripcion() : "" %>" size="35" maxlength="30"></td>
				  </tr>        
				   	<tr class="fila">
				  		<td width="166" nowrap>Cuenta:</td>
				  		<td nowrap><input name="cuenta" type="text" class="textbox" id="cuenta2" value="<%= codextra!=null ? codextra.getCuenta() : "" %>" size="30" maxlength="24"></td>
				  </tr>
					<tr class="fila">
					  <td width="166" nowrap>Agencia: </td>
					  <td nowrap><input:select name="agencia" attributesText="class=textbox" options="<%=agencias %>"/></td>
					  <%= codextra!=null ? "<script>form1.agencia.value = '" + codextra.getAgencia() + "';</script>"  : "" %>
				  </tr>
					<tr class="fila">
					  <td width="166" nowrap>Elemento:</td>
					  <td nowrap><input name="elemento" type="text" class="textbox" id="elemento2" value="<%= codextra!=null ? codextra.getElemento() : "" %>" size="6" maxlength="4"></td>
				  </tr>
					<tr class="fila">
					  <td width="166" nowrap >Ajuste: </td>
					  <td nowrap>
						<select name="ajuste" class="textbox">
						  <option value="S">S
						  <option value="P">P
						</select> </td>
					   <%= codextra!=null ? "<script>form1.ajuste.value = '" + codextra.getAjuste() + "';</script>"  : "" %>
				  </tr>
					<tr class="fila">
					  <td width="166" nowrap>Unidades:</td>
					   <td nowrap>
					  <select name="unidades" class="textbox">
						  <option value="S">S
						  <option value="N">N
						</select></td>
					  	<%= codextra!=null ? "<script>form1.unidades.value = '" + codextra.getUnidades() + "';</script>"  : "" %>
				  </tr>
				  <tr class="fila">
					  <td width="166" nowrap>Tipo :</td>
					   <td nowrap>
					  <select name="tipo" class="textbox">
						  <option value="">Seleccione un item</option>
						  <option value="R" >Reembolsable</option>
						  <option value="E" >Extraflete</option>								  			
						</select></td>
					  	<%= codextra!=null ? "<script>form1.tipo.value = '" + codextra.getTipo() + "';</script>"  : "" %>
				  </tr>
					
   			  </table>
			</td>
		</tr>
	</table><br>
<table align="center" width="530">
		<tr>
			<td align="center"><%if (request.getParameter("retorno")!= null){ %>								
									<img title='Modificar' src="<%= BASEURL %>/images/botones/modificar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="VerificarModificacion('<%=CONTROLLER%>?estado=Codextraflete&accion=Insert&opcion=3');" style="cursor:hand">
									<img title='Eliminar' src="<%= BASEURL %>/images/botones/anular.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="window.location='<%=CONTROLLER%>?estado=Codextraflete&accion=Insert&opcion=4&codigo=<%= codextra.getCodextraflete()%>'" style="cursor:hand"></img>								
						    <%}%>					
			</td>
	  </tr>
    </table><br>
    <table align="center" width="530">
		<tr>
			<td align="center">
				<%if (request.getParameter("retorno")!= null){ %>
				<img title='Regresar' src="<%= BASEURL %>/images/botones/regresar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" style="cursor:hand" onClick="window.location='<%= CONTROLLER %>?estado=Menu&accion=Cargar&carpeta=/jsp/masivo/extraflete/&pagina=buscarCodextrafletes.jsp&marco=no&opcion=33&item=16';" ></img>									
				<%} else {%>
				<img title='Buscar' src="<%= BASEURL %>/images/botones/buscar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="buscar();" style="cursor:hand"></img>
				<% } %> 
				<img title='Listar' src="<%= BASEURL %>/images/botones/detalles.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="window.location.href = '<%=CONTROLLER%>?estado=Codextraflete&accion=Insert&opcion=5'" style="cursor:hand"></img>
				<img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="window.close()" style="cursor:hand"></img>
			</td>
	  </tr>
    </table>  
    </form>	
	
    <div align="center">
<%        
        if(request.getParameter("mensaje")!=null) {
            out.print("<table width=450 border=2 align=center><tr><td><table width=100%  border=1 align=center  bordercolor=#F7F5F4 bgcolor=#FFFFFF><tr><td width=350 align=center class=mensajes>"+request.getParameter("mensaje")+"</td><td width=100><img src="+BASEURL+"/images/cuadronaranja.JPG></td></tr></table></td></tr></table>");            
        }
    %>
        
  </div>
</div>
</body>
</html>
