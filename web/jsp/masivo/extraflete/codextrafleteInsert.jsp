<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<html>
<head>
<title>Ingresar codigos de extra fletes</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src="<%=BASEURL%>/js/validacodextraflete.js"></script>
</head>
<body onresize="redimensionar()" onload = "redimensionar()">
<%
	TreeMap agencias = (TreeMap) request.getAttribute("agencias_tm");
	agencias.put( " Seleccione", "");
	Codextraflete codextra = (Codextraflete) request.getAttribute("codextra");
%>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Costos Adicionales Planilla"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=Codextraflete&accion=Insert&opcion=1">
  <table width="530" border="2" align="center" >
	  <tr>
			<td>
				<table width="100%" align="center">
					<tr>
						<td width="173" class="subtitulo1">&nbsp;Informacion Costos Adicionales</td>
						<td width="205" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
					</tr>					
					<tr class="fila">
					  <td width="166" nowrap>Codigo:</td>
					  <td width="288" nowrap><input name="codigo" type="text" class="textbox" id="codigo" value="<%= codextra!=null ? codextra.getCodextraflete() : "" %>" size="6" maxlength="10">
					  <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img></td>
				  </tr>
					<tr class="fila" >
					  <td width="166" nowrap >Descripcion: </td>
					  <td nowrap ><input name="descripcion" type="text" class="textbox" id="descripcion" value="<%= codextra!=null ? codextra.getDescripcion() : "" %>" size="35" maxlength="30">
					  <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img></td>
				  </tr>
					<tr class="fila" >
					  <td width="166" nowrap>Cuenta:</td>
					  <td nowrap><input name="cuenta" type="text" class="textbox" id="cuenta" value="<%= codextra!=null ? codextra.getCuenta() : "" %>" size="30" maxlength="24">					  </img></td>
				  </tr>
					<tr class="fila">
					  <td width="166" nowrap>Agencia:</td>
					  <td nowrap><input:select name="agencia" attributesText="class=textbox" options="<%=agencias %>"/>&nbsp;<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img></td>
					  <%= codextra!=null ? "<script>forma.agencia.value = '" + codextra.getAgencia() + "';</script>"  : "" %>
				  </tr>
					<tr class="fila">
					  <td width="166" nowrap>Elemento: </td>
					  <td nowrap><input name="elemento" type="text" class="textbox" id="elemento" value="<%= codextra!=null ? codextra.getElemento() : "" %>" size="6" maxlength="4">					  </img></td>
				  </tr>
					<tr class="fila">
					  <td width="166" nowrap>Ajuste: </td>
					  <td nowrap>
						<select name="ajuste" class="textbox">
						  <option value="S">S
						  <option value="P">P
						</select><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img></td>
					   <%= codextra!=null ? "<script>forma.ajuste.value = '" + codextra.getAjuste() + "';</script>"  : "" %>
				  </tr>
					<tr class="fila">
					  <td width="166" nowrap>Unidades:</td>
					   <td nowrap>
					  <select name="unidades" class="textbox">
						  <option value="S">S
						  <option value="N">N
						</select><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img></td>
					  	<%= codextra!=null ? "<script>forma.unidades.value = '" + codextra.getUnidades() + "';</script>"  : "" %>
				  </tr>
				  <tr class="fila">
					  <td width="166" nowrap>Tipo :</td>
					   <td nowrap>
					  <select name="tipo" class="textbox">
						  <option value="R">Reembolsable</option>
						  <option value="E">Extraflete</option>
						  
						</select><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img></td>
					  	<%= codextra!=null ? "<script>forma.tipo.value = '" + codextra.getTipo() + "';</script>"  : "" %>
				  </tr>
					<tr class="fila">
					  <td colspan='2' align=center></td>					   
				  	</tr>
			  </table>
			</td>
	  </tr>		
   </table>
   <br>
  <table align=center width='488'>
    <tr>
        <td align="center" colspan="2">
            <img title='Agregar c�digo extraflete' src="<%= BASEURL %>/images/botones/aceptar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="if( verificarInsert() ) form1.submit();" style="cursor:hand"></img>            
            <img title='Cancelar' src="<%= BASEURL %>/images/botones/cancelar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick='forma.reset();' style="cursor:hand"></img>
            <img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="window.close()" style="cursor:hand"></img>
        </td>
    </tr>
  </table>  
<br>
<%if(request.getParameter("mensaje")!=null) {%>
<table width="393" border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><b> 
				      <%=request.getParameter("mensaje")%></b></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%>
</form>
</div>
</body>
</html>
