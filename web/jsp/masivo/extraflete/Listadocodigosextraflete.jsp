<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@ page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
    <title>Listado De Costos Adicionales</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <script src="js/validar.js"></script>
	<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
	<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
	<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script> 
	<%
		String style = "simple";
		String position =  "bottom";
		String index =  "center";
		int maxPageItems = 10;
		int maxIndexPages = 10;
	%>
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listado De Costos Adicionales"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="form1" method="post" action="">
  <table width="100%"  border="2">
    <tr>
        <td>
            <table width="100%" align="center" class="tablaInferior"> 
                <tr>
                    <td colspan='7'>                
                        <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="barratitulo">
                            <tr>
                                <td width="50%" class="subtitulo1" colspan='4'>Datos Del Extraflete</td>
                                <td width="50%" class="barratitulo" colspan='3'><img src="<%=BASEURL%>/images/titulo.gif"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan='7'>
                        <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
                            <tr class="tblTitulo">    
							  <td width="10%" nowrap>Codigo</td>
							  <td width="10%" nowrap>Tipo</td>
							  <td width="32%" nowrap>Descripcion</td>
							  <td width="24%" nowrap>Cuenta</td>
							  <td width="10%" nowrap>Agencia</td>
							  <td width="9%" nowrap>Elemento</td>
							  <td width="6%" nowrap>Ajuste</td>
							  <td width="9%" nowrap>Unidades</td>
							</tr>
							<pg:pager
                                                            items="<%=model.codextrafleteService.getListarCodextraflete().size()%>"
                                                            index="<%= index%>"
                                                            maxPageItems="<%= maxPageItems %>"
                                                            maxIndexPages="<%= maxIndexPages %>"
                                                            isOffset="<%= true %>"
                                                            export="offset,currentPageNumber=pageNumber"
                                                            scope="request">
                                                          <%-- keep track of preference --%> 
								  <%
                                      for (int i = offset.intValue(),l = Math.min(i + maxPageItems, model.codextrafleteService.getListarCodextraflete().size());i < l; i++){
                                        %>
                                            <pg:item> 
                                         <% 
                                          List lista = model.codextrafleteService.getListarCodextraflete();                                                                  
                                          Codextraflete  registros = (Codextraflete) (lista.get(i));
                                          String codigo   = registros.getCodextraflete();
                                          String descrip  = registros.getDescripcion();
                                          String cuenta   = registros.getCuenta();
                                          String agencia  = registros.getAgencia();
                                          String elemento = registros.getElemento();
                                          String ajuste   = registros.getAjuste();
                                          String unidades = registros.getUnidades(); 
										  String tipo 	  = registros.getTipo(); 
										  tipo = (tipo.equals("R"))?"Reembolsable":"Extraflete";
                                        %>							   
							<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand" 
								onClick="window.location='<%=CONTROLLER%>?estado=Codextraflete&accion=Insert&opcion=2&codigo=<%=codigo%>'">
							  <td  nowrap class="bordereporte"><%= codigo  %></td>
							  <td  nowrap class="bordereporte"><%= tipo  %></td>
							  <td  nowrap class="bordereporte"><%= descrip %></td>
							  <td  nowrap class="bordereporte"><%= cuenta  %></td>
							  <td  nowrap class="bordereporte"><%= agencia %></td>
							  <td  nowrap class="bordereporte"><%= elemento%></td>
							  <td  nowrap class="bordereporte"><%= ajuste  %></td>
							  <td  nowrap class="bordereporte"><%= unidades%></td>
							</tr>
                                            </pg:item>
    					<% } %> 
                            <tr class="filagris">
                                <td height="30" colspan="5" nowrap><pg:index><jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/></pg:index></td>
                            </tr>
                          </pg:pager>
					  </table>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
  <table>
  	<tr>
      <td colspan="7" class="normal">
        <img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close()" style="cursor:hand"></img>
      </td>
    </tr>
  </table>  
</form>
</div>
</body>
</html>
