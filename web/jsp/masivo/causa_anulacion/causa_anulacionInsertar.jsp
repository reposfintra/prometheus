<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Insertar Codigo Discrepancia</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body>
<FORM name='forma' id='forma' method='POST' onSubmit="" action="<%=CONTROLLER%>?estado=Causa_Anulacion&accion=Insert">
<table width="416"  border="2" align="center">
  <tr>
    <td>
  	<table width="100%" class="tablaInferior">
      <tr>
        <td colspan="3">
		<table width="100%"  border="0" cellpadding="0" cellspacing="0" >
  		<tr>
	    <td width="46%" class="subtitulo1">Ingresar Causas Anulacion</td>
    	<td width="54%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="33" height="20"></td>
  		</tr>
		</table>
		</td>
        </tr>        
        <tr class="fila">
            <td width="119">Codigo</td>
            <td width="227"><input name="c_codigo" type="text" class="textbox" id="c_codigo" onKeyPress="soloAlfa(event)" maxlength="6">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
        </tr>
        <tr class="fila">
            <td>Descripcion</td>
            <td><textarea name="c_descripcion" class="textbox" id="c_descripcion" style="width:100% "></textarea></td>
        </tr>              
  </table>
 </td>
</tr>
</table>
</FORM>
<center>
	<img src="<%=BASEURL%>/images/botones/aceptar.gif" name="c_aceptar" onClick="return TCamposLlenos();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> <img src="<%=BASEURL%>/images/botones/cancelar.gif" name="c_cancelar" id="c_cancelar" onClick="forma.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" id="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">    	 
</center>         
<%String mensaje = (String) request.getAttribute("mensaje");%>
<%if(mensaje!=null){%>
<br>
<table border="2" align="center">
  <tr>
    <td>
	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><strong><%=mensaje%></strong></td>
        <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%>	
</body>
</html>
