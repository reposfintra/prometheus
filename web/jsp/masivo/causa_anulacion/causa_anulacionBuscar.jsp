<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head><title>Insertar Codigo Discrepancia</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body>
<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");    
%>

<form name="form2" method="post" action="<%=CONTROLLER%>?estado=Causa_Anulacion&accion=Serch&listar=True">
 <table border="2" align="center" width="416">
  <tr>
    <td><table width="100%" class="tablaInferior">
      <tr class="titulo" align="center">
        <td colspan="3"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="51%" class="subtitulo1">Buscar Causa de Anulacion </td>
            <td width="49%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="33" height="20"></td>
          </tr>
        </table>          </td>
      </tr>
      <tr class="fila">
        <td width="108">Codigo</td>
        <td width="159"><input name="c_codigo" type="text" class="textbox" id="c_codigo" onKeyPress="soloAlfa(event)" maxlength="6"></td>
      </tr>
      <tr class="fila">
        <td>Descripcion</td>
        <td><input name="c_descripcion" type="text" class="textbox" id="c_descripcion" maxlength="45"></td>
      </tr>         
    </table></td>
  </tr>
</table>
<br>
 <center>
    <img src="<%=BASEURL%>/images/botones/buscar.gif" name="c_buscar" onClick="form2.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> <img src="<%=BASEURL%>/images/botones/detalles.gif" name="c_buscar" onClick="location.href='<%=CONTROLLER%>?estado=Causa_Anulacion&accion=Serch&listar=True&c_codigo=&c_descripcion'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_buscar" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
 </center>
</form>
</body>
</html>
