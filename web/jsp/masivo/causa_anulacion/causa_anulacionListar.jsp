<%@page contentType="text/html"%>
<%@page session="true"%>
<%//@page errorPage="/error/error.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head><title>Listar Codigo Discrepancia</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body>
<% 	Vector vec = model.causas_anulacionService.getCausas_anulacion();
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
%>
<br>

<table width="600"  border="2" align="center">
  <tr>
    <td>
	<table width="100%" class="tablaInferior">
      <tr>
        <td colspan="2"><table width="100%"  border="0" cellpadding="0" cellspacing="0" class="barratitulo">
          <tr>
            <td width="33%" class="subtitulo1">Causas Anulacion </td>
            <td width="67%"><img src="<%=BASEURL%>/images/titulo.gif" width="33" height="20"></td>
          </tr>
        </table>          </td>
      </tr>
      <tr class="subtitulos" align="center">
	  <td colspan="2">
	  	<table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
	  		<tr class="tblTitulo" align="center">
	         <td width="14%" nowrap >Codigo</td>
    	     <td width="86%" nowrap >Descripcion</td>
       		</tr>
	      <pg:pager
    	    items="<%=vec.size()%>"
        	index="<%= index %>"
	        maxPageItems="<%= maxPageItems %>"
    	    maxIndexPages="<%= maxIndexPages %>"
	        isOffset="<%= true %>"
    	    export="offset,currentPageNumber=pageNumber"
        	scope="request">
      		<%for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vec.size()); i < l; i++){
		        Causas_Anulacion u = (Causas_Anulacion) vec.elementAt(i);%>
      		<pg:item>
	       <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" title="Modificar Causa_Anulacion..." onClick="window.open('<%=CONTROLLER%>?estado=Causa_Anulacion&accion=Serch&c_codigo=<%=u.getCodigo()%>&listar=False','myWindow','status=no,scrollbars=no,width=450,height=300,resizable=yes');">
        	<td class="bordereporte"><%=u.getCodigo()%></td>
        	<td class="bordereporte"><%=u.getDescripcion()%></td>
       	   </tr>
      		</pg:item>
      		<%  }%>
      	   <tr class="pie">
        	<td td height="20" colspan="9" nowrap align="center"> <pg:index>
          	<jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>    
    		</pg:index> 
			</td>
      	  	</tr>
      	  </pg:pager>
        </table>
	</td>
  </tr>
  </table>
  </td>
  </tr>
  </table>
<br>
<table width="600" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr align="left">
    <td><img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="location.href='<%=BASEURL%>/jsp/masivo/causa_anulacion/causa_anulacionBuscar.jsp'">
  </tr>
</table>
</body>
</html>
