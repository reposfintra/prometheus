<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Modificar Codigo Discrepancia</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body <%if(request.getParameter("reload")!=null){%>onLoad="window.opener.location.reload();"<%}%>>
<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    Causas_Anulacion u = model.causas_anulacionService.getCausa_anulacion();    
    String mensaje = (String) request.getAttribute("mensaje");
%>
<br>
<FORM name='forma' id='forma' method='POST' onSubmit="return validarTCamposLlenos();" action=''>
  <table width="416"  border="2" align="center">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr class="titulo" align="center">
            <td colspan="3"><table width="100%"  border="0" cellpadding="0" cellspacing="0" class="barratitulo">
              <tr>
                <td width="65%" class="subtitulo1">Modificar\Anular Causas de Anulacion</td>
                <td width="35%"><img src="<%=BASEURL%>/images/titulo.gif" width="33" height="20"></td>
              </tr>
            </table>            </td>
        </tr>
        <tr class="fila">
            <td width="119">Codigo</td>
            <td width="227"><%=u.getCodigo()%><input name="c_codigo" type="hidden" id="c_codigo" value="<%=u.getCodigo()%>"></td>
        </tr>
        <tr class="fila">
            <td>Descripcion</td>
            <td><textarea name="c_descripcion" class="textbox" id="c_descripcion" style="width:100% "><%=u.getDescripcion()%></textarea></td>
        </tr>        
    </table></td>
    </tr>
  </table>
  <br>
 <center> 
<% if (mensaje!="MsgAnulado"){%>
	<img src="<%=BASEURL%>/images/botones/modificar.gif" name="c_modificar" onClick="forma.action='<%=CONTROLLER%>?estado=Causas_Anulacion&accion=Update&modif=1'; forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	<img src="<%=BASEURL%>/images/botones/anular.gif" name="c_anular" onClick="forma.action='<%=CONTROLLER%>?estado=Causas_Anulacion&accion=Update'; forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">	
<%}
  if (mensaje!="MsgAgregado"){%>
	<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_buscar" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">               
<%}else{%>
	<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_buscar" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">               
<%}%>
</center>
<%  if(mensaje!=null){%>
<br>
<table border="2" align="center">
  <tr>
    <td>
	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
			<td width="229" align="center" class="mensajes"><strong>Modificación exitosa!</strong></td>
        <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
      </tr>
    </table></td>
  </tr>
</table>
<%  }%>
</FORM>
</body>
</html>