<!--
- Autor : Ing. Armando Oviedo C
- Date  : 15 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
- Modificado por : Ing. Ricardo Rosero
-->
<%--       
-@(#)
--Descripcion : Pagina JSP, que ingresa un elemento de tabla general
--%> 

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.operation.model.DAOS.*"%>

<html>
<head>
<title>Ingresar Elemento Tabla General</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
</head>
<body ONLOAD="redimensionar();" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 3px; top: 9px;">
  <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ingresar Elemento Tabla General"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="forma" action="<%=CONTROLLER%>?estado=TblGeneral&accion=Insert" method="post">
  <table width="45%"  border="2" align="center">
  	<tr>
    	<td>
			<table width="100%" align="center">
				<tr>
					<td width="40%" class="subtitulo1" nowrap>Datos</td>
					<td width="60%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
				</tr>
				<tr class="fila">
				  <td>C&oacute;digo Tabla </td>
				  <td><input type="text" name="codtabla"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img></td>
				</tr>
				
			  <tr class="fila">
				  <td>C�digo</td>
				  <td><input type="text" name="codigo"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img></td>
		      </tr>
				<tr class="fila">
				  <td>Descripci&oacute;n</td>
				  <td><input type="text" name="descripcion"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img></td>
			  </tr>
		  </table>
  		</td>
  	</tr>
  </table><br>
  <table align=center width='450'>
    <tr>
        <td align="center" colspan="2">
            <img src="<%= BASEURL %>/images/botones/aceptar.gif"  name="imgaceptar" onClick="forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">&nbsp; 
			<img src="<%= BASEURL %>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);"  style="cursor:hand ">&nbsp; 
			<img src="<%= BASEURL %>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);"  style="cursor:hand ">
			
        </td>
    </tr>
  </table>  
  </form>
  <br>
</div>
</body>
</html>
