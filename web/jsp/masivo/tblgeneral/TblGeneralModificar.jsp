<!--
- Autor : Ing. Armando Oviedo C
- Date  : 12 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--        
-@(#)
--Descripcion : Pagina JSP, que modifica o anula un item de tabla general--%>
 
<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Modificar Item Tabla General</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
</head>
<body onload="<%if(request.getParameter("reload")!=null){%> window.opener.location.reload();<%}%> redimensionar();" onresize="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modificar Item Tabla General"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%  
	TblGeneralService tgs = new TblGeneralService();
	TblGeneral tmp = new TblGeneral();
	String mensajemod = request.getParameter("mensajemod");
	String codtabla = request.getParameter("codtabla");
	String codigo = request.getParameter("codigo");
	String desc = request.getParameter("desc");
%>
<form name="forma" action="<%=CONTROLLER%>?estado=TblGeneral&accion=Modificar&mensaje=modificar" method="post">
<input type="hidden" name="codigo" value="<%=codigo%>"></input>
<input type="hidden" name="codtabla" value="<%=codtabla%>"></input>
  <table width="45%"  border="2" align="center">
  	<tr>
    	<td>
			<table width="100%" align="center">
				<tr>
					<td width="40%" class="subtitulo1" nowrap>Informaci�n Del Descuento</td>
					<td width="60%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
				</tr>
				<tr class="fila">
				  <td>C�digo</td>
				  <td><%=codigo%></td>
			  </tr>
			  <tr class="fila">
				  <td>C�digo Tabla</td>
				  <td><%=codtabla%></td>
			  </tr>
				<tr class="fila">
				  <td>Descripci�n</td>
				  <td><input name="desc" value="<%=desc%>"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img></td>
			  </tr>			  
				
		  </table>
  		</td>
  	</tr>
  </table><br>
  <table align=center width='450'>
    <tr>
        <td align="center" colspan="2">
            <img title='Modificar' src="<%= BASEURL %>/images/botones/modificar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="TCamposLlenos()"></img>                        
			<img title='Eliminar' src="<%= BASEURL %>/images/botones/eliminar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="document.forma.action='<%=CONTROLLER%>?estado=TblGeneral&accion=Modificar&mensaje=eliminar&codigo=<%=tmp.getCodigo()%>&codtabla=<%=tmp.getCodigoTabla()%>';document.forma.submit();"></img>
            <img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="window.close()"></img>
        </td>
    </tr>
  </table>
  
  </form>
  <br>
  
  <%}%>  
</div>
</body>
</html>
