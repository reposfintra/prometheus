<!--
- Autor : ricardo Rosero
- Date  : 23 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que presenta el formulario de busqueda para presentar los datos completos de un item dado
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.services.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>ITEM</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script> 
</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listado Items Tabla General"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
    TblGeneralService tgs = new TblGeneralService();
	String codtabla = request.getParameter("codtabla");
	String codigo = request.getParameter("codigo");
	tgs.listarPorCodigo(codtabla,codigo);
    Vector tblVec = tgs.obtTblGeneral();
	String action = CONTROLLER + "?estado=TblGeneral&accion=Modificar&mensaje=modificar";
	String action2 = CONTROLLER + "?estado=TblGeneral&action=Modificar&mensaje=anular";            	   
    TblGeneral tg; 
	if ( tblVec.size() >0 ){ 
  %>
 
    <form name="forma" method="post" action="<%= action %>" >
      <table width="465" border="2" align="center">
        <tr>
          <td><table width="100%"  border="0">
            <tr>
              <td width="48%" class="subtitulo1">DATOS ITEM</td>
              <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
            </tr>
          </table>
          <table width="100%" class="tablaInferior">
           <pg:pager
    items="<%=tblVec.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
           <%-- keep track of preference --%>
           <%
        for (int i = 0; i < tblVec.size(); i++) {
            tg = (TblGeneral) tblVec.elementAt(i);%>
           <pg:item>
         <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
            <td class="bordereporte">Codigo Tabla</td>
		<input type="hidden" value="<%= codtabla %>" name="codtabla">
		<input type="hidden" value="<%= codigo %>" name="codigo">
		<td class="bordereporte">
		<%=tg.getCodigoTabla()%></td></tr>
		<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
            <td class="bordereporte">Codigo</td>
			<td class="bordereporte"><%=tg.getCodigo()%>
			</td></tr>
			 <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
            <td class="bordereporte">Descripcion</td>
			<td class="bordereporte">
			  <input name="desc" type="text" class="textbox" value="<%=tg.getDescripcion()%>" size="100"></td></tr>
            </pg:item>
           <%}%>
           </pg:pager>
         </table></td>
       </tr>
     </table>    
            
          </table></td>
        </tr>
      </table>
     <br>
      <div align="center">
			   <img src="<%=BASEURL%>/images/botones/modificar.gif"  name="imgaceptar" onMouseOver="botonOver(this);" onClick="forma.submit();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;	
			   <img src="<%=BASEURL%>/images/botones/anular.gif"  name="imgaceptar" onMouseOver="botonOver(this);" onClick="document.location = '<%=action2%>'" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;	
			   <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="history.back();" onMouseOut="botonOut(this);" style="cursor:hand">
           </div>
    </form>
 <%}%>
  </div>
</body>

</html>
