<!--
- Autor : Ing. Armando Oviedo C
- Date  : 15 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--           
-@(#)
--Descripcion : Pagina JSP, que busca un elemento de tabla general por c�digo y c�digo de tabla
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>Listado De Reportes - Movimiento Tr�fico - TSP</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Descuento De Equipos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form action="<%=CONTROLLER%>?estado=TblGeneral&accion=Buscar&mensaje=listar" name="forma" method="post">
  <table width="50%"  border="2" align="center">
  <tr>
  <td colspan="2">
  	<table width="100%">
    <tr>
		<td width="40%" class="subtitulo1" nowrap>Buscar Descuentos</td>
		<td width="60%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
	</tr>
    <tr class="fila">
      <td>C�digo Tabla</td>
      <td><input name="codtabla" type="text" class="textbox"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img></td>
    </tr>
	<tr class="fila">
      <td>C�digo</td>
      <td><input name="codigo" type="text" class="textbox"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></img></td>
    </tr>
	
  </table>
  </td>
  </tr>
  </table>
  <br>
  <table align=center width='450'>
    <tr>
        <td align="center" colspan="2">
            <img title='Buscar' src="<%= BASEURL %>/images/botones/buscar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onclick="forma.submit()"></img>                        
			<img title='Detalles' src="<%= BASEURL %>/images/botones/detalles.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="document.location = '<%=CONTROLLER%>?estado=TblGeneral&accion=Buscar&mensaje=listartodos'"></img>
            <img title='Salir' src="<%= BASEURL %>/images/botones/salir.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="window.close()"></img>
        </td>
    </tr>
  </table>
</form>
</div>
</body>
</html>
