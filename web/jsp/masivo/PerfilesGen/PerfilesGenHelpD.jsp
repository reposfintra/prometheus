<!--  
	 - Author(s)       :      Ing Luis Eduardo Frieri	
	 - Description     :      AYUDA DESCRIPTIVA - Perfiles Vs Tablas Generales
	 - Date            :      26/01/2007
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
    <title>Descripcion de los campos para Relacionar tablas generales con perfiles de usuario</title>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <link href="/../../../css/estilostsp.css" rel="stylesheet" type="text/css">  
    
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
<body> 
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ayuda Descriptiva - Asignacion de perfiles a tablas generales"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:90%; z-index:0; left: 0px; top: 100px;"> 
   <p>&nbsp;</p>
   <table width="65%" border="2" align="center" id="tabla1" >
        <td>
         <table width="100%" height="50%" align="center">
              <tr>
                <td class="subtitulo1"><strong>Reporte</strong></td>
                <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
         </table>
         <table width="100%" borderColor="#999999" bgcolor="#F7F5F4">
           <tr>
             <td colspan="2" class="tblTitulo"><strong>Asignar tablas generales a perfiles de usuario</strong></td>
           </tr>
           <tr>
             <td colspan="2" class="subtitulo"><strong>INFORMACION</strong></td>
           </tr>
           <tr>
             <td width="41%" class="fila">Perfiles </td>
             <td width="59%"><span class="ayudaHtmlTexto">Lista que contiene todos los perfiles existentes </span></td>
           </tr>
           <tr>
             <td class="fila">Perfiles a relacionar </td>
             <td>Campo en el cual se cargan los perfiles seleccionados </td>
           </tr>
           <tr>
             <td class="fila">Tablas </td>
             <td>Lista que contiene todas los tablas generales existentes </td>
           </tr>
          
          
           <tr>
             <td class="fila">Tablas a relacionar </td>
             <td><span class="ayudaHtmlTexto">Campo en el cual se cargan las Tablas seleccionadas </span></td>
           </tr>
           <tr>
             <td class="fila">Boton Aceptar </td>
             <td>Boton que inicia el proceso de asignacion de las tablas seleccionadas a los perfiles seleccionados </td>
           </tr>
         </table></td>
  </table>
		<p></p>
	<center>
	<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	</center>
</div>  
</body>
</html>