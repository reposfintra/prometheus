<!--
- Autor : Ing. Tito Andr�s Maturana
- Date  : 13 enenro del 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la generaci�n de la relaci�n Work Group - Std job.
--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %><html>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page errorPage="/error/ErrorPage.jsp"%>
<head>
<title>Relaci&oacute;n Grupo de Trabajo - Standard Job</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<%
        String distrito = (String) session.getAttribute("Distrito");
        String link = CONTROLLER + "?estado=WorkGrp&accion=StdJob&cmd=show";	
        String msj = request.getParameter("mensaje");
        if ( msj==null ) msj = "";
%>
<script>       
        
        <%= model.wgroup_stdjobSvc.getVarCamposJS() %>
        <%= model.wgroup_stdjobSvc.getVarCamposJStd() %>
        var SeparadorJS = '<%= model.wgroup_stdjobSvc.getVarSeparadorJS() %>';
        var url = '<%= link%>';
        
        function wgroups_Std(){
            form.action = url;            
            form.clienteSelec.value = form.cliente.options(form.cliente.selectedIndex).value;
            form.origenSelec.value = form.origen.options(form.origen.selectedIndex).value;
            form.destinoSelec.value = form.destino.options(form.destino.selectedIndex).value;
            
            var cmb = form.c_stdSelec;
            for( i=0; i<cmb.length; i++ ){
                cmb[i].value = cmb[i].value + SeparadorJS + cmb[i].text;
            }
            selectAll(form.c_stdSelec);
            
            var cmb2 = form.c_wgSelec;
            for( i=0; i<cmb2.length; i++ ){
                cmb2[i].value = cmb2[i].value + SeparadorJS + cmb2[i].text;
            }
            selectAll(form.c_wgSelec);
            
            form.submit();
        }

        function loadCombo(datos, cmb){
            cmb.length = 0;
            for (i=0;i<datos.length;i++){
                var dat = datos[i].split(SeparadorJS);
                addOption(cmb, dat[0], dat[1]);
            }
            //cmb.style.width = 'auto';
            order(cmb); 
            deleteRepeat(cmb);                       
        }
        
        function unloadCombo(datos, cmb){
            for(j=0; j<datos.length; j++){
                for (i=0;i<cmb.length;i++){
                        var dat = datos[j].split(SeparadorJS);
                        if( dat[0] == cmb[i].value ){
                                cmb.remove(i);
                        }
                        //addOption(cmb, dat[0], dat[1]);
                }
            }
            order(cmb);
        }

        function addOption(Comb,valor,texto){
           if(valor!='' && texto!=''){
                var Ele = document.createElement("OPTION");
                Ele.value=valor;
                Ele.text=texto;
                Comb.add(Ele);
          }
        }

        function deleteRepeat(cmb){
          var ant='';
          i=0;
          while(i<cmb.length){
             if(ant== cmb[i].value){
                ant = cmb[i].value;
                cmb.remove(i);
                i--;
             } else {              
                ant = cmb[i].value;
             }            
             i++;
          }
        }

        function order(cmb){
           for (i=0; i<cmb.length;i++)
             for (j=i+1; j<cmb.length;j++)
                if (cmb[i].text > cmb[j].text){
                    var temp = document.createElement("OPTION");
                    temp.value = cmb[i].value;
                    temp.text  = cmb[i].text;
                    cmb[i].value = cmb[j].value;
                    cmb[i].text  = cmb[j].text;
                    cmb[j].value = temp.value;
                    cmb[j].text  = temp.text;
                }
        }

        function move(cmbO, cmbD){
           for (i=cmbO.length-1; i>=0 ;i--)
            if (cmbO[i].selected){
               addOption(cmbD, cmbO[i].value, cmbO[i].text)
               cmbO.remove(i);
            }
           //order(cmbD);
           deleteRepeat(cmbD);
        }
        
        function move2(cmbO, cmbD){
           for (i=cmbO.length-1; i>=0 ;i--)
            if (cmbO[i].selected){
               addOption(cmbD, cmbO[i].value, cmbO[i].text)
               cmbO.remove(i);
            }
           order(cmbD);
           deleteRepeat(cmbD);
        }
        
        function moveStd(cmbO, cmbD){
           for (i=cmbO.length-1; i>=0 ;i--)
            if (cmbO[i].selected){
               //wgroups_Std(cmbO[i].value);
               addOption(cmbD, cmbO[i].value, cmbO[i].text)               
            }
            
           wgroups_Std();
           //order(cmbD);
           deleteRepeat(cmbD);
        }

        function moveAll(cmbO, cmbD){
           for (i=cmbO.length-1; i>=0 ;i--){
             if(cmbO[i].value!='' && cmbO[i].text!=''){
               addOption(cmbD, cmbO[i].value, cmbO[i].text)
               cmbO.remove(i);
             }
           }
           order(cmbD);
           deleteRepeat(cmbD);
        }
        function moveAllStd(cmbO, cmbD){
           for (i=cmbO.length-1; i>=0 ;i--){
             if(cmbO[i].value!='' && cmbO[i].text!=''){
               addOption(cmbD, cmbO[i].value, cmbO[i].text)
               cmbO.remove(i);
             }
           }
           wgroups_Std();           
           order(cmbD);
           deleteRepeat(cmbD);
        }
        
        function selectAll(cmb){
            for (i=0;i<cmb.length;i++)
              cmb[i].selected = true;
        }
        
        function validar(){
              if( form.c_stdSelec.length==0)
                alert('Debe seleccionar al menos un Standard Job!')
              else
                form.submit();
        }
		
	//Actualizacion de los selects
	function actualizarOrigen() {
            form.c_stds.length = 0;
            form.action = "<%= CONTROLLER%>?estado=WorkGrpStdJob&accion=Update&cmd=show";
            form.clienteSelec.value = form.cliente.options(form.cliente.selectedIndex).value;            
            form.origen.options(0).selected = true;
            form.destino.options(0).selected = true;                            
            
            selectWgroupsStd();
            
            form.submit();
            
    	}
    	
    	function actualizarDestino() {
    	    form.c_stds.length = 0;
            form.action = "<%= CONTROLLER%>?estado=WorkGrpStdJob&accion=Update&cmd=show";
            form.clienteSelec.value = form.cliente.options(form.cliente.selectedIndex).value;
            form.origenSelec.value = form.origen.options(form.origen.selectedIndex).value;
            
            form.destino.options(0).selected = true; 
          
            selectWgroupsStd();
            
            form.submit();
            
    	}
    	
    	function actualizarStdJobs() {
            form.c_stds.length = 0;
            form.action = "<%= CONTROLLER%>?estado=WorkGrpStdJob&accion=Update&cmd=show";
            form.clienteSelec.value = form.cliente.options(form.cliente.selectedIndex).value;
            form.origenSelec.value = form.origen.options(form.origen.selectedIndex).value;
            form.destinoSelec.value = form.destino.options(form.destino.selectedIndex).value;
            
            selectWgroupsStd();
            
            form.submit();
    	}
    	
    	function selectWgroupsStd(){
            var cmb = form.c_stdSelec;
            for( i=0; i<cmb.length; i++ ){
                cmb[i].value = cmb[i].value + SeparadorJS + cmb[i].text;
            }
            selectAll(form.c_stdSelec);
            
            var cmb2 = form.c_wgSelec;
            for( i=0; i<cmb2.length; i++ ){
                cmb2[i].value = cmb2[i].value + SeparadorJS + cmb2[i].text;
            }
            selectAll(form.c_wgSelec);
        }

        
</script>
</head>

<body onresize="redimensionar()" onload = "redimensionar(); document.form.c_wgs.remove(0);
        <% /*if ( msj.equalsIgnoreCase("MsgUpdate") ){*/ %> loadCombo(CamposStd, document.form.c_stdSelec);unloadCombo(CamposStd, document.form.c_stds);
loadCombo(CamposJS, document.form.c_wgSelec); unloadCombo(CamposJS, document.form.c_wgs); <%/*}*/%>">
<%
	TreeMap clientes = model.clienteService.getTreemap();
        TreeMap origins = model.stdjobService.getOrigenesStdJobsCliente();
        TreeMap destinos = model.stdjobService.getDestinosStdJobsCliente();
        TreeMap stds = model.stdjobService.getStdJobsCliente();
        TreeMap workgroups = model.tblgensvc.getWork_group();
	
	clientes.put(" Seleccione", ""); 
        origins.put(" Seleccione", "");
        destinos.put(" Seleccione", "");
        
	
	
	
	String clientesel = ( request.getAttribute("cliente")!=null ) ? (String) request.getAttribute("cliente") : "";
	String origensel = ( request.getAttribute("origen")!=null ) ? (String) request.getAttribute("origen") : "";
        String destinosel = ( request.getAttribute("destino")!=null ) ? (String) request.getAttribute("destino") : "";
        
        /*System.out.println("................... clientesel " + clientesel + ", " + clientesel.length());
        System.out.println("................... origensel " + origensel + ", " + origensel.length());
        System.out.println("................... destinosel " + destinosel  + ", " + destinosel.length());*/
	
%>	
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Relaci�n Grupo de Trabajo - Est�ndar"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="form" method="post" action="<%= CONTROLLER%>?estado=WorkGrpStdJob&accion=Insert&cmd=show" id="form">
  <table width="958" border="2" align="center">
    <tr>
      <td width="987"><table width="100%" border="0" align="center">
        <tr valign="top">
          <td width="27%" align="center" valign="middle" class="subtitulo1">Est&aacute;ndares</td>
          <td width="5%" class="fila" ><input name="doc_selec" type="hidden" id="doc_selec">
            <input name="clienteSelec" type="hidden" id="clienteSelec">
            <input name="origenSelec" type="hidden" id="origenSelec">
            <input name="destinoSelec" type="hidden" id="destinoSelec">
            <input name="stdSel" type="hidden" id="stdSel"></td>
          <td width="36%" align="center" class="subtitulo1">Est&aacute;ndares<br>
            seleccionados</td>
          <td width="1%" class="fila" >&nbsp;</td>
          <td width="14%" align="center" class="subtitulo1">Grupos de trabajo             <br>
            seleccionados</td>
          <td width="5%" class="fila" >&nbsp;</td>
          <td width="12%" align="center" valign="middle" class="subtitulo1">Grupos de trabajo </td>
        </tr>
        <tr class="fila">
          <td><table width="100%"  border="0">
            <tr class="fila">
              <td >Cliente</td>
              <td ><input:select name="cliente" default="<%= clientesel %>" attributesText="id='cliente' class=textbox onChange=actualizarOrigen();" options="<%=clientes %>"/></td>
            </tr>
            <tr class="fila">
              <td>Origen</td>
              <td><input:select name="origen" default="<% = origensel %>" attributesText="id='origen' class=textbox onChange='actualizarDestino();'" options="<%=origins %>"/></td>
            </tr>
            <tr class="fila">
              <td>Destino</td>
              <td><input:select name="destino" default="<%= destinosel %>" attributesText="class=textbox onChange=actualizarStdJobs();" options="<%=destinos %>"/></td>
            </tr>
          </table></td>
          <td rowspan="2" align="center">              <img src="<%=BASEURL%>/images/botones/envDer.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick=" moveStd(c_stds,  c_stdSelec ); " style="cursor:hand "><br>
            <img src="<%=BASEURL%>/images/botones/enviarDerecha.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="moveAllStd(c_stds,  c_stdSelec ); " style="cursor:hand ">            <br>
              <img src="<%=BASEURL%>/images/botones/envIzq.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="move2(c_stdSelec, c_stds ); " style="cursor:hand "><br>
              <img src="<%=BASEURL%>/images/botones/enviarIzq.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="moveAll(c_stdSelec, c_stds ) ; " style="cursor:hand ">              <br>            
              <br>            </td><td rowspan="2"><select name="c_stdSelec" size="15" multiple class="textbox" id="select" style="height:330px; width:320px;">
                      </select></td>
          <td>&nbsp;</td>
          <td rowspan="2"><select name="c_wgSelec" size="15" multiple class="textbox" id="select2" style='height:330px; width:100%'>
          </select></td>
          <td rowspan="2" align="center">            <div align="center"> <img src="<%=BASEURL%>/images/botones/envIzq.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="move2(c_wgs, c_wgSelec );" style="cursor:hand "><br>
                <img src="<%=BASEURL%>/images/botones/enviarIzq.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="moveAll(c_wgs, c_wgSelec ) ; " style="cursor:hand "> <br>
                <img src="<%=BASEURL%>/images/botones/envDer.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="move2 (c_wgSelec,  c_wgs ); " style="cursor:hand "><br>
                <img src="<%=BASEURL%>/images/botones/enviarDerecha.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="moveAll(c_wgSelec,  c_wgs ); " style="cursor:hand "><br>
</div>
            </td><td rowspan="2"><input:select name="c_wgs" attributesText="class=textbox multiple size=15 style='width:100%; height:330px' class=textreadonly" options="<%= workgroups %>"/></td>
        </tr>
        <tr>
          <td class="fila"><input:select name="c_stds" attributesText="class=textbox multiple size=15 style='width:315px; height:265px' class=textreadonly" options="<%= stds %>"/></td>
          <td class="fila">&nbsp;</td>
          </tr>
      </table>      </td>
    </tr>
  </table>
  <div align="center"><br>
    <img src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="selectAll(form.c_stdSelec);selectAll(form.c_wgSelec);validar();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">&nbsp; <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);"  style="cursor:hand "></div>
</form>
<%if(request.getParameter("msg")!=null){%>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes"><%=request.getParameter("msg").toString() %></td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>	
<%}%>
</div>
</body>
</html>
