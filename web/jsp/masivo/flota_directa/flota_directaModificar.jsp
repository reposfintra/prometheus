<!--
- Autor : Ing. Jose de la rosa
- Date  : 2 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la actualización de las flotas directas
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Modificar Flota Directa</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<body onResize="redimensionar();" <%if(request.getParameter("reload")!=null){%>onLoad="parent.opener.location.reload();redimensionar();"<%}%>>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listar Flota Directa"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    Flota_directa f = model.flota_directaService.getFlota_directa();    
    String mensaje = (String) request.getAttribute("mensaje");
    Vector vec = new Vector();
%>
<FORM name='forma' id='forma' method='POST' action='<%=CONTROLLER%>?estado=Flota_directa&accion=Update'>
	<table width="420" border="2" align="center">
      <tr>
        <td>
          <table width="100%" class="tablaInferior">
            <tr class="fila">
              <td align="left" class="subtitulo1">&nbsp;Flota Directa </td>
              <td align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
            </tr>
          <tr class="fila">
            <td width="164" align="left" >Placa</td>
            <td valign="middle"><input name="c_nplaca" type="text" class="textbox" value="<%=f.getPlaca()%>" id="c_placa" size="7" maxlength="7">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
          </tr>
          <tr class="fila">
            <td align="left" >Nit Propietario</td>
            <td valign="middle"><input name="c_nnit" type="text" class="textbox" value="<%=f.getNit()%>" id="c_nit" size="15" maxlength="15">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
          </tr>	
        </table></td>
      </tr>
    </table>
	<input name="c_placa" type="hidden" value="<%=f.getPlaca()%>">
	<input name="c_nit" type="hidden" value="<%=f.getNit()%>">	
	<p>
	<div align="center"><img src="<%=BASEURL%>/images/botones/modificar.gif" style="cursor:hand" title="Modificar un flota directa" name="modificar"  onclick="return TCamposLlenos();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
	<img src="<%=BASEURL%>/images/botones/anular.gif" style="cursor:hand" title="Anular un responsable" name="anular"  onClick="window.location='<%=CONTROLLER%>?estado=Flota_directa&accion=Anular&c_nit=<%=f.getNit()%>&c_placa=<%=f.getPlaca()%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
	<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
	</p>
	<%  if(mensaje!=null){%>
		<table border="2" align="center">
		  <tr>
			<td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
			  <tr>
				<td width="229" align="center" class="mensajes"><%=mensaje%></td>
				<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
				<td width="58">&nbsp;</td>
			  </tr>
			</table></td>
		  </tr>
		</table>
	<%  }%>

</FORM>
</div>
</body>
</html>
