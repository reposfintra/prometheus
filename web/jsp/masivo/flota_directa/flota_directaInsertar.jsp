<!--
- Autor : Ing. Jose de la rosa
- Date  : 2 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el ingreso de las flotas directas
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Insertar Flota Directa</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/funciones.js"></script>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ingresar Flota Directa"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<FORM name='frmplaca' id='frmplaca' method='POST' action='<%=CONTROLLER%>?estado=Flota_directa&accion=Insert' onSubmit="return validarCamposLlenos(frmplaca);">
  <table width="450" border="2" align="center" >
    <tr>
      <td>
        <table width="100%" class="tablaInferior">
          <tr class="fila">
            <td align="left" class="subtitulo1">&nbsp;Flota Directa </td>
            <td width="60%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
          </tr>
          <tr class="fila">
            <td align="left" >Placa</td>
            <td valign="middle"><input name="c_placa" type="text" class="textbox" id="c_placa" size="7" maxlength="7">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
          </tr>
          <tr class="fila">
            <td width="40%" align="left" >Nit Propietario</td>
            <td valign="middle"><input name="propietario" attributesText="maxlength='15' size='16' class='textbox'">
				
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">&nbsp;<a class="Simulacion_Hiper" style="cursor:hand "  onClick="window.open('<%=CONTROLLER%>?estado=Menu&accion=Enviar&numero=37&propietario=ok','Prop','height=350,width=650,scrollbars=NO,resizable=yes')" >Buscar Propietario</a></td>
          </tr>	  
      </table></td>
    </tr>
  </table>
  <p>
<div align="center"><input type="image" src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand" title="Agregar una flota directa" name="ingresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/cancelar.gif" style="cursor:hand" name="regresar" title="Limpiar todos los registros" onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);" >&nbsp;
<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></div>
</p>  
  <%String mensaje = (String) request.getAttribute("mensaje"); 
 %>
    <%  if(mensaje!=null){%>
            <br>
            <table border="2" align="center">
              <tr>
                <td><table width="100%">
                    <tr>
                      <td width="229" align="center" class="mensajes"><%=mensaje%></td>
                      <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                      <td width="58">&nbsp;</td>
                    </tr>
                </table></td>
              </tr>
            </table>
    <%  }%>	
</FORM>
</div>
</body>
</html>
