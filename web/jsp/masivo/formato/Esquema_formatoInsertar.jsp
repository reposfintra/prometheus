<!--
- Autor : Ing. Jose de la rosa
- Date  : 24 de Noviembre de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja el ingreso especial
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<html>
<head>
<title>Formato Esquema</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>

<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Formato Esquema"/>
</div>
<%
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
	String campo = request.getParameter( "c_codigo" )!=null?request.getParameter( "c_codigo" ):"";
	LinkedList tbl = (LinkedList)request.getAttribute ( "set_formato");
	if(tbl!=null && request.getParameter( "c_codigo" )==null){
		TablaGen res1 = (TablaGen) tbl.get(0);
		campo = res1.getTable_code();
	}
%>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<FORM name='forma' id='forma' method='POST' action='<%=CONTROLLER%>?estado=Esquema_formato&accion=Insert'>
  <table width="650" border="2" align="center" >
    <tr>
      <td>
        <table width="100%" class="tablaInferior">
          <tr class="fila">
            <td colspan="2" align="left" class="subtitulo1">&nbsp;Formato Esquema </td>
            <td colspan="2" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
          </tr>
          <tr class="fila">
            <td width="16%" align="left" >Formato</td>
            <td colspan="3" align="left" ><select name="c_codigo" id="c_codigo" class="textbox" style='width:90%;' onChange="cambiarDatos();">
                            <% if(tbl!=null){
                                       for(int i = 0; i<tbl.size(); i++){
									   		TablaGen res = (TablaGen) tbl.get(i);%>
										  	<option value="<%=res.getTable_code()%>" <%=campo.equals( res.getTable_code() )?"selected":""%>><%=res.getDescripcion()%></option>
                                      <%}
                                }%>
              				</select><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"> </td>
            </tr>
		  <tr class="fila">
		  <td>Campo</td>
		  <td width="31%"><select name="c_nombre" id="c_nombre" class="textbox" style='width:90%;'>
            				<% LinkedList tblres = model.tablaGenService.obtenerTablas();
							if( tblres!=null && tbl!=null ){
								for(int i = 0; i<tblres.size(); i++){
									TablaGen respa = (TablaGen) tblres.get(i); 
									if( campo.equals( respa.getReferencia() ) ){%>
										<option value="<%=respa.getTable_code()%>" ><%=respa.getDescripcion()%></option>
									<%}
								}
							}%>
          					</select><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"> </td>
		  <td width="16%">Tipo</td>
		  <td width="37%"><select name="c_tipo" id="select" class="textbox" style='width:80%;'>
							<% LinkedList tbltip = (LinkedList)request.getAttribute ( "set_tipo");
                            if(tbltip!=null){
							   for(int i = 0; i<tbltip.size(); i++){
									TablaGen restip = (TablaGen) tbltip.get(i); %>
									<option value="<%=restip.getTable_code()%>"><%=restip.getDescripcion()%></option>
            					<%}
                            }%>
						  </select><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"> </td>
		  </tr>
          <tr class="fila">
            <td align="left" >Titulo </td>
            <td colspan="3" align="left" ><textarea name="c_titulo" type="text" class="textbox" id="c_titulo" cols="100" rows="1" onKeyPress="soloAlfa(event)"></textarea></td>
            </tr>	
          <tr class="fila">
            <td align="left" >Posicion Inicial </td>
            <td align="left" ><input name="c_pos_inicial" type="text" class="textbox" id="c_pos_inicial" size="8" maxlength="8"  onKeyPress="soloDigitos(event,'decNO')"> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
            <td valign="middle">Posicion Final</td>
            <td valign="middle"><input name="c_pos_final" type="text" class="textbox" id="c_pos_final" size="8" maxlength="8"  onKeyPress="soloDigitos(event,'decNO')"> 
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
          </tr>	
		  <tr class="fila">
		  <td>Orden</td>
		  <td><input name="c_orden" type="text" class="textbox" id="c_orden" size="8" maxlength="8"  onKeyPress="soloDigitos(event,'decNO')"> 
		  <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
		  <td colspan="2"></td>
		  </tr>  
      </table></td>
    </tr>
  </table>
  <p>
<div align="center"><img src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand" title="Agregar un Registro" name="ingresar"  onclick="return enviaDatos();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/cancelar.gif" style="cursor:hand" name="regresar" title="Limpiar todos los registros" onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);" >&nbsp;
<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></div>
</p>  
<%  if(request.getAttribute("mensaje")!=null){%>
		<table border="2" align="center">
		  <tr>
			<td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
			  <tr>
				<td width="229" align="center" class="mensajes"><%=(String)request.getAttribute("mensaje")%></td>
				<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
				<td width="58">&nbsp;</td>
			  </tr>
			</table></td>
		  </tr>
		</table>
	<%  }%>
</FORM>
</div>
<%=datos[1]%>
</body>
</html>
<script>
function enviaDatos( valor , entero ){
	if(forma.c_codigo.value == '' ){
		alert("El campo del formato no puede ser vacio!");
		forma.c_codigo.focus();
		return false;
	}
	else if(forma.c_tipo.value == '' ){
		alert("El campo del tipo del programa no puede ser vacio!");
		forma.c_tipo.focus();
		return false;
	}
	else if(forma.c_nombre.value == '' ){
		alert("El campo del nombre del programa no puede ser vacio!");
		forma.c_nombre.focus();
		return false;
	}
	else if(forma.c_orden.value == '' ){
		alert("El campo orden no puede ser vacio!");
		forma.c_orden.focus();
		return false;
	}
	else if(forma.c_pos_inicial.value == '' ){
		alert("El campo posicion inicial no puede ser vacio!");
		forma.c_pos_inicial.focus();
		return false;
	}
	else if(forma.c_pos_final.value == '' ){
		alert("El campo posicion final no puede ser vacio!");
		forma.c_pos_final.focus();
		return false;
	}
	else if( parseFloat(forma.c_pos_inicial.value) >= parseFloat(forma.c_pos_final.value) ){
		alert("El campo posicion inicial debe ser menor a la posicion final!");
		forma.c_pos_inicial.focus();
		return false;
	}
	else if ( parseInt(forma.c_titulo.value.length) > 200 ){
		alert("El campo titulo debe ser menor a 200 caracteres");
		forma.c_titulo.focus();
		return false;
	}
	else{
		forma.submit();
	}
}	

function cambiarDatos(){
	//alert("<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/masivo/formato&pagina=Esquema_formatoInsertar.jsp&opcion=35&marco=no");
	forma.action = "<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/masivo/formato&pagina=Esquema_formatoInsertar.jsp&opcion=35&marco=no";
	forma.submit();
}
</script>
