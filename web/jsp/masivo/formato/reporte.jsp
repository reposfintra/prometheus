<!--
- Autor : Ing. Osvaldo P�rez Ferrer
- Date : 28 de Octubre de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve para mostrar un reporte cuadro azul
--%>

<%@ page session="true"%>
<%//@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.operation.model.*, com.tsp.operation.model.beans.*, com.tsp.util.*,java.util.*, java.text.*"%>

<html>
<head>
    <title>Cuadro Azul</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

    <script src="<%=BASEURL%>/js/utilidades.js"></script>
    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<%
    
    
    String mensaje = (String)request.getAttribute("mens");
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
    
    Vector v;
   
%>
</head>

<body>
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=Cuadro Azul"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:82%; z-index:0; left: 0px; top:100px; overflow: scroll;">        
    <form id="form1"  name="form1" method="post" action="">

            <table width="100%"  border="0" align="center" cellpadding="2" cellspacing="3" onclick="document.getElementById('noexiste').style.visibility='hidden';">
                
                        
                <%
                   
                        v = model.FormatoSvc.getVector();
                        if( v !=null && v.size() > 0 ){
                %>
                
                <div align="center">                
                    <img src="<%=BASEURL%>/images/botones/exportarExcel.gif"  height="21" onMouseOver="botonOver(this);" onClick="reporte('<%=CONTROLLER%>');" onMouseOut="botonOut(this);" style="cursor:hand">
                    <img src="<%=BASEURL%>/images/botones/regresar.gif"  height="21" onMouseOver="botonOver(this);" onClick="location.replace('<%=BASEURL%>/jsp/masivo/formato/filtro.jsp');" onMouseOut="botonOut(this);" style="cursor:hand">
                    <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">            
                </div>
                <br>
                                    <table width="100%"  border="2" align="center">


                            <tr>
                            <td width="100%">
                                <table width="100%" class="tablaInferior">
                                    <tr>
                                        <td height="22" colspan=2 class="subtitulo1">Reporte Cuadro Azul</td>
                                        <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                                    </tr>
                                    </table> 

                                
                                <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">    
                                <tr class="tblTitulo" align="left">
                                    <td colspan="24"> &nbsp;<%=(v.size())%>&nbsp;Registros Encontrados</td>
                                </tr>                                
                                
                                
                                <tr class="tblTitulo" align="center">
                                        <td nowrap>Despacho#</td>
                                        <td nowrap>Remision</td>
                                        <td nowrap>Fecha Despacho</td>
                                        <td nowrap>Empresa/Dpto</td>
                                        <td nowrap>Origen</td>
                                        <td nowrap>Destino</td>
                                        <td nowrap>PO</td>
                                        <td nowrap>DO</td>
                                        <td nowrap>Contenido</td>                                    
                                        <td nowrap>Tipo Vehiculo</td>
                                        <td nowrap>Color</td>
                                        <td nowrap>Placa</td>
                                        <td nowrap>Conductor</td>
                                        <td nowrap>Cedula Conductor</td>
                                        <td nowrap>Celular Conductor</td>
                                        <td nowrap>Solicitante DLTD</td>
                                        <td nowrap>Tel-Ext-Cel</td>
                                        <td nowrap>Fecha Entrega</td>
                                        <td nowrap>N�mero Factura</td>
                                        <td nowrap>Fecha Factura</td>
                                        <td nowrap>Valor Flete</td>
                                        <td nowrap>Moneda</td>
                                        <td nowrap>Cost Center</td>
                                        <td nowrap>PO</td>

                                    </tr>
                                                                                                                                        
                                
                                    <%
                                     String[] mon = new String[2];
                                     for(int i = 0; i<v.size(); i++){
                                         ReporteCuadroAzul r = (ReporteCuadroAzul)v.get(i);
                                    %>
                                        <tr class="<%=i%2==0?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' 
                                        align="center">    
                                        
                                        <td class="bordeReporte" nowrap><%=r.getDespacho()%>&nbsp;</td>                                        
                                        <td class="bordeReporte" nowrap><%=r.getRemision()%>&nbsp;</td>
                                        <td class="bordeReporte" nowrap><%=r.getFecha_despacho()%>&nbsp;</td>
                                        <td class="bordeReporte" nowrap><%=r.getEmpresa_dpto()%>&nbsp;</td>
                                        <td class="bordeReporte" nowrap><%=r.getOrigen()%>&nbsp;</td>
                                        <td class="bordeReporte" nowrap><%=r.getDestino()%>&nbsp;</td>
                                        <td class="bordeReporte" nowrap><%=r.getP_o()%>&nbsp;</td>
                                        <td class="bordeReporte" nowrap><%=r.getD_o()%>&nbsp;</td>
                                        <td class="bordeReporte" nowrap><%=r.getContenido()%>&nbsp;</td>
                                        <td class="bordeReporte" nowrap><%=r.getTipo_vehiculo()%>&nbsp;</td>
                                        <td class="bordeReporte" nowrap><%=r.getColor()%>&nbsp;</td>
                                        <td class="bordeReporte" nowrap><%=r.getPlaca()%>&nbsp;</td>
                                        <td class="bordeReporte" nowrap><%=r.getConductor()%>&nbsp;</td>
                                        <td class="bordeReporte" nowrap><%=r.getCedula()%>&nbsp;</td>
                                        <td class="bordeReporte" nowrap><%=r.getCelular()%>&nbsp;</td>
                                        <td class="bordeReporte" nowrap><%=r.getSolicitante()%>&nbsp;</td>
                                        <td class="bordeReporte" nowrap><%=r.getTel_ext_cel()%>&nbsp;</td>
                                        <td class="bordeReporte" nowrap><%=r.getFecha_entrega()%>&nbsp;</td>
                                        <td class="bordeReporte" nowrap><%=r.getNumero_factura()%>&nbsp;</td>
                                        <td class="bordeReporte" nowrap><%=r.getFecha_factura()%>&nbsp;</td>
                                        <%
                                        mon = r.getValor_flete().split("_-_");
                                        if( mon.length > 1 ){%>
                                            <td class="bordeReporte" nowrap><%=mon[0]%>&nbsp;</td>
                                            <td class="bordeReporte" nowrap><%=mon[1]%>&nbsp;</td>                                        
                                        <%}
                                        else if( mon.length == 1 ){%>
                                            <td class="bordeReporte" nowrap><%=mon[0]%>&nbsp;</td>
                                            <td class="bordeReporte" nowrap>&nbsp;</td>
                                        <%}
                                         %>                                        
                                        <td class="bordeReporte" nowrap><%=r.getCost_center()%>&nbsp;</td>
                                        <td class="bordeReporte" nowrap><%=r.getP_o()%>&nbsp;</td>

                                        <%}%>
                                </tr> 
                            

                            </td>
                        </tr>            

                                      </table>                        
                            </table>   
                            <br>
                            <div align="center">                
                                <img src="<%=BASEURL%>/images/botones/exportarExcel.gif"  height="21" onMouseOver="botonOver(this);" onClick="reporte('<%=CONTROLLER%>');" onMouseOut="botonOut(this);" style="cursor:hand">
                                <img src="<%=BASEURL%>/images/botones/regresar.gif"  height="21" onMouseOver="botonOver(this);" onClick="location.replace('<%=BASEURL%>/jsp/masivo/formato/filtro.jsp');" onMouseOut="botonOut(this);" style="cursor:hand">
                                <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">            
                            </div>
                <%          }//fin si
                            else{
                                mensaje = "No se encontraron datos para el reporte en este periodo";%>
                                <br>
                                <div align="center">                                                
                                    <img src="<%=BASEURL%>/images/botones/regresar.gif"  height="21" onMouseOver="botonOver(this);" onClick="location.replace('<%=BASEURL%>/jsp/masivo/formato/filtro.jsp');" onMouseOut="botonOut(this);" style="cursor:hand">
                                    <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">            
                                </div>
                            <%}
                %>
                                                  
            </table>
    <br>
    
    </form>
    <br/>
	
        
    <div id="noexiste">
       <%if(mensaje != null && !mensaje.equals("") ){ %>
        
        <table border="2" align="center">
            <tr>                
                <td height="45"><table width="410" height="41" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                        <td width="282" height="35" align="center" class="mensajes"><%=mensaje%></td>
                        <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="78">&nbsp;</td>
                    </tr>
                </table></td>
            </tr>
        </table>        
               
       <%}%>     
    </div>
    	      
    </div>
<%=datos[1]%>
</body>
<script>


function reporte( con ){    
    alert("Su reporte se est� generando, consulte sus archivos en unos instantes");
    location.replace(con+"?estado=Formato&accion=Reporte&opcion=export");    
}
</script>
</html>