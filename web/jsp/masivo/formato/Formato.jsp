<!--  
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      22/10/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description:  Vista que permite capturar tipo de filtros para generar la corrida
 --%>

 
<%@page    session   ="true"%> 
<%@page    errorPage ="/error/ErrorPage.jsp"%>
<%@page    import    ="com.tsp.operation.model.beans.*"%>
<%@include file      ="/WEB-INF/InitModel.jsp"%>
<%@page    import    ="com.tsp.util.*"%>
<%@page    import    ="java.util.*" %>


<html>
<head>
      <title>Captura Datos Formato</title>
      <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
      <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
      <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
   
</head>
<body>


<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=FORMATO MERCANCIA"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>



<%  List    formatos   =  model.FormatoSvc.getTFormatos();
    String  msj        =  request.getParameter("msj"); 
   
    
    if(formatos!=null && formatos.size()>0){%>
    
     <form action='<%=CONTROLLER%>?estado=FormatoNacional&accion=Datos&evento=CAMPOS' method='post' name='formulario'>
         <table  width="550" border="2" align="center">
             <tr>
                  <td>  
                       <table width='100%' align='center' class='tablaInferior'>

                              <tr class="barratitulo">
                                    <td colspan='2' >
                                           <table cellpadding='0' cellspacing='0' width='100%'>
                                                 <tr>
                                                      <td align="left" width='55%' class="subtitulo1">&nbsp;TIPO DE FORMATO </td>
                                                      <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32"  height="20" align="left"></td>
                                                </tr>
                                           </table>
                                    </td>
                             </tr>
                             
                                                        
                                   <tr  class="fila">
                                   
                                        <td width='40%' > TIPO DE FORMATO </td>                                        
                                        <td width='*' > 
                                            <select name='formato' style="width:100%">
                                             <% for(int i=0;i<formatos.size();i++){
                                                 Hashtable  forma  = (Hashtable)formatos.get(i);    %> 
                                                    <option value='<%= (String)forma.get("codigo") %>'> <%= (String)forma.get("descripcion") %>
                                                <%}%>
                                            </select>
                                        </td>
                                        
                                   </tr>
                             
                     </table>
                     
                  </td>
               </tr>       
         </table>
      </form>
     
     
     <p>
           <img src="<%=BASEURL%>/images/botones/aceptar.gif"    name="acept"   height="21"  title='Aplicar Formato'      onClick="formulario.submit();"            onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
           <img src="<%=BASEURL%>/images/botones/salir.gif"      name="exit"    height="21"  title='Salir'                onClick="parent.close();"                 onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
     </p>
                             
 <%}%>

     
     
  <!-- mensajes -->
  <% if(msj!=null && !msj.equals("")){%>        
         <table border="2" align="center">
          <tr>
            <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
              <tr>
                <td width="470" align="center" class="mensajes"><%=msj%></td>
                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                <td width="58">&nbsp;</td>
              </tr>
            </table></td>
          </tr>
        </table>
        
       <%if(formatos==null || formatos.size()==0){%>
          <p>
              <img src="<%=BASEURL%>/images/botones/salir.gif"      name="exit"    height="21"  title='Salir'                onClick="parent.close();"                 onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
          </p>
      <%}%>
      
  <%} %>
  
  

</div>


</body>
</html>
