<!--  
     - Author(s)       :      FERNEL VILLACOB DIAZ
     - Date            :      22/10/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)  
     - Description:  Vista que permite capturar tipo de filtros para generar la corrida
 --%>

 
<%@page    session   ="true"%> 
<%@page    errorPage ="/error/ErrorPage.jsp"%>
<%@page    import    ="com.tsp.operation.model.beans.*"%>
<%@include file      ="/WEB-INF/InitModel.jsp"%>
<%@page    import    ="com.tsp.util.*"%>
<%@page    import    ="java.util.*" %>
<%@page contentType="text/html"%>

<%@taglib uri="/WEB-INF/tlds/tagsAgencia.tld" prefix="ag"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<% Util u = new Util(); %>
<%@page import="com.tsp.operation.model.*"%>

<html>
<head>
      <title>Captura Datos Formato</title>
      <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
      <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
      <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/script.js"></script> 
	 <script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
      
      
      <script>
         function sendFormato(theForm){
               var sw = 0;
               for(var i=0;i<theForm.length;i++){
                  var ele = theForm.elements[i];
				 
               }               
               if( sw==0)
                   theForm.submit();         
         }
         
         
         function search(formato, tipo, doc){
             sw=0;
             if(doc==''){
                alert('Deber� digitar el n�mero de documento');
                sw=1;
             }
             if(sw==0)
                location.href = '<%=CONTROLLER%>?estado=FormatoNacional&accion=Datos&evento=BUSCAR&formato='+ formato +'&tipoDoc='+ tipo +'&documento='+ doc;         
         }
		 
		 
		 function chequeo(form){
		 	var Centro = document.getElementById('CentroC');
			var caja = document.getElementById('CentroCT');
			if(form.Centro == 'FC_08'){
				form.caja.type = "Text";
			}
		 }
      </script>
      
      
      
   
</head>
<body>



<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=CAPTURA DATOS FORMATO"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>


<% String  formato   =  model.FormatoSvc.getFormato();
   List    campos    =  model.FormatoSvc.getCampos();
   List    tDoc      =  model.FormatoSvc.getTipoDocumento();
   String  msj       =  request.getParameter("msj");
   String  BOTON     =  (request.getParameter("BOTON")==null)?"INSERT":request.getParameter("BOTON");
   String  docNo     =  (request.getParameter("doc")    == null)?"":request.getParameter("doc");
   String  tipoDoc   =  (request.getParameter("tipoDoc")== null)?"":request.getParameter("tipoDoc");
   String  rrem   =  (request.getParameter("Remision")== null)?"":request.getParameter("Remision");
   String  Fcost   =  (request.getParameter("CentroC")== null)?"":request.getParameter("CentroC");
   String  Compe   =  (request.getParameter("Compensac")== null)?"":request.getParameter("Compensac");
   String  estatus   =  (BOTON.equals("MODIFICAR") )?"readonly":"";
   TreeMap remisionM =    model.FormatoSvc.getbuscarFormatosRemision();
           remisionM.put( " Seleccione", "");
   TreeMap Fcostos =    model.FormatoSvc.buscarCentrocostos();   
           Fcostos.put( " Seleccione", "");		   
	String mostrar = "";
	TreeMap Fcompensac =	model.FormatoSvc.buscarCompensac();
			Fcompensac.put( " Seleccione", "");	   
   %>

 <% if(msj!=null && !msj.equals("")){%>        
         <table border="2" align="center">
          <tr>
            <td>
                <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                  <tr>
                    <td width="470" align="center" class="mensajes"><%=msj%></td>
                    <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                    <td width="58">&nbsp;</td>
                  </tr>
               </table>
            </td>
          </tr>
        </table>
		<p>
        <img src="<%=BASEURL%>/images/botones/regresar.gif"    name="regre"   height="21"  title='Regresar'  onClick="location.href='<%=CONTROLLER%>?estado=FormatoNacional&accion=Datos&evento=RESET'"   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
        <img src="<%=BASEURL%>/images/botones/salir.gif"       name="exit"    height="21"  title='Salir'      onClick="parent.close();"                                                                        onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
          </p>
     
        
  <%}else{ %>

<%  if(campos!=null && campos.size()>0){%>

    <form action='<%=CONTROLLER%>?estado=FormatoNacional&accion=Datos&evento=<%=BOTON%>' method='post' name='formulario'>
         <table  width="600" border="2" align="center">
             <tr>
                  <td>  
                       <table width='100%' align='center' class='tablaInferior'>

                              <tr class="barratitulo">
                                    <td colspan='2' >
                                           <table cellpadding='0' cellspacing='0' width='100%'>
                                                 <tr>
                                                      <td align="left" width='55%' class="subtitulo1">&nbsp;FORMATO : <%= formato %> </td>
                                                      <td align="left" width='*'  ><img src="<%=BASEURL%>/images/titulo.gif" width="32"  height="20" align="left"></td>
                                                </tr>
                                           </table>
                                    </td>
                             </tr>
                             
                             <tr  class="fila">
                                    <td width='35%'>TIPO DE DOCUMENTO </td>
                                    <td width='*' > 
                                           <select    name='tipoDoc' style="width:100%" title='Tipo de Actividad'>
                                             <% for(int i=0;i<tDoc.size();i++){
                                                 Hashtable  doc  = (Hashtable)tDoc.get(i);
                                                 String     code = (String)doc.get("codigo");
												 String     sel  = ( tipoDoc.equals(code ) )?"selected":"";   
                                                 if( BOTON.equals("MODIFICAR") ){
                                                     if(  code.equals( tipoDoc  ) ){%>
                                                         <option value='<%= code %>' <%=sel%> > <%= (String)doc.get("descripcion") %>  
                                                        <% break;
                                                     }
                                                 }else{%>
                                                     <option value='<%= code %>' <%=sel%> > <%= (String)doc.get("descripcion") %>                                                  
                                                 <%}%>                                                 
                                             <%}%>
                                           </select>
                                    
                                    </td>
                             </tr>
                             
                             <tr  class="fila">
                                    <td > DOCUMENTO </td>
                                    <td > 
                                        <input type='text' name='documento'   style="width:50%"  <%=estatus%>  maxlength='10' value='<%= docNo %>'  title='N�mero del documento'  >
                                        <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif' width='17'   style=" cursor:hand"    name='i_load'       onclick="search('<%=formato%>', tipoDoc.value, documento.value);"  title='Buscar'   >
                                    </td>
                             </tr>
							  <tr  class="fila">
                                    <td > DESCRIPCION DE REMISION </td>
                                    <td>
										<%  
											
											List   campos2   =  model.FormatoSvc.getCampos();
                      						EsquemaFormato  campo2      = (EsquemaFormato)campos2.get(1);
											rrem = campo2.getRemision();
											
											%>
										<input:select name="Remision" options="<%=remisionM %>" default="<%= rrem %>" attributesText="style='width:50%;' class='listmenu'" />
									</td>          
							  </tr>
                             
                             
                             <% for(int i=0;i<campos.size();i++){
                                   EsquemaFormato  campo = (EsquemaFormato)campos.get(i);
                                   String          tipo  = campo.getTipo(); 
                                   String  condiciones = ""; 
								   condiciones = campo.getContenido();
								   String contenidos= "";
 								   contenidos = campo.getContenido();
								   String cen = campo.getCentro_C();
								  
								 %>
                                    <tr  class="fila"> 
                                   
                                        <td > <%= campo.getDescripcion() %> </td> 
                                        <td >   
										    <% if(campo.getDescripcion().equals("CENTRO DE COSTO")){ %>
												<% if(( BOTON.equals("INSERT")) && (condiciones != null && !condiciones.equals("FC_08"))){System.out.println("entro1");%>
														  <table style="display:none " id="tabla_texto">
										   		<%}else{%>
													<%if  ((condiciones.equals("")) && (cen.equals(""))){%>
														<table style="display:none " id="tabla_texto">
													<%}else if  (condiciones.equals(cen)){%>
														<table style="display:none " id="tabla_texto">
													<%}else{%>		
														<table style="display:block " id="tabla_texto">
													<%}%>
												<%}%>
															<tr>
																<td>
																	<%String costt= "";
																	  String muestra= "" ;
												  						costt = campo.getContenido();
																		if( costt != null && !costt.equals("FC_08")){ %>
																			
																			<% if((contenidos.equals("FC_01")) || (contenidos.equals("FC_02")) || (contenidos.equals("FC_03")) || (contenidos.equals("FC_04")) || (contenidos.equals("FC_05")) || (contenidos.equals("FC_06")) ||(contenidos.equals("FC_07"))){
																					muestra = "";
																 		    }else{
																				 muestra = campo.getContenido();
																				 
																			    }%>
																			<input  name='var' id="var" class="Text" type="hidden" size="72" maxlength="30" value="<%= campo.getContenido() %>" >
																			<input  name='CCOSTO' id="CCOSTO" class="Text" size="72" maxlength="30" value="<%= muestra %>" >
																		<% }else{ %>
																			<input  name='CCOSTO' id="CCOSTO" class="Text" size="72" maxlength="30"  >
																		<% }%>
																	
																</td>		
															</tr>   
														</table> 
												<%
												if((contenidos.equals("FC_01")) || (contenidos.equals("FC_02")) || (contenidos.equals("FC_03")) || (contenidos.equals("FC_04")) || (contenidos.equals("FC_05")) || (contenidos.equals("FC_06")) ||(contenidos.equals("FC_07"))){%>
													<input:select name="CentroC" options="<%=Fcostos %>"  default="<%= campo.getContenido() %>"  attributesText="class=textbox onChange='chequeo(this);'" />
												<%}else if(contenidos.equals("")|| contenidos== null){%>
													<input:select name="CentroC" options="<%=Fcostos %>"  default="<%= ""%>"  attributesText="class=textbox onChange='chequeo(this);'" />
												<%}else{%>	
													<input:select name="CentroC" options="<%=Fcostos %>"  default="<%= "FC_08"%>"  attributesText="class=textbox onChange='chequeo(this);'" />       
												<%}%>	
										<%}else if(campo.getDescripcion().equals("COMPENSAC")){ %>
											<% if (campo.getCompresac() != null){ %>
												<input:select name="Compensac" options="<%=Fcompensac %>" default="<%= campo.getCompresac()%>"  />
											<%}else{%>
											     <input:select name="Compensac" options="<%=Fcompensac %>" default="<%=""%>"  />
											<%}%>
										<%}else{
											if(tipo.equals("DATE") ){%>   
 
                                                  <input type='text' class="textbox"  name='<%=campo.getCampo()%>' title='<%=campo.getTitulo()%>'     readonly       value="<%= campo.getContenido() %>">                                                        
                                                  <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.<%=campo.getCampo()%>);return false;" HIDEFOCUS>
                                                        <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha">
                                                  </a>   
                                           
                                            <%}else if( tipo.equals("DATEHOUR")  ){%>    
                                             
                                                  <input type='text' class="textbox"  name='<%=campo.getCampo()%>' title='<%=campo.getTitulo()%>'     readonly       value="<%= campo.getContenido() %>">                                                        
                                                  <a href="javascript:void(0)" onclick="if(self.gfPop24)gfPop24.fPopCalendar(document.formulario.<%=campo.getCampo()%>);return false;" HIDEFOCUS>
                                                        <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la Fecha y Hora">
                                                  </a> 
                                                  
                                            <%}else if( tipo.equals("TEXTBOX") ){%>    
                                            		<textarea name='<%=campo.getCampo()%>' class="textbox" rows='5'  title='<%=campo.getTitulo()%>'    style="width:100%"><%= campo.getContenido() %></textarea>                                          
                                                 
                                            <%}else{%>
                                            
                                                 <input type='text' name='<%=campo.getCampo()%>' class="textbox" title='<%=campo.getTitulo()%>'    value='<%= campo.getContenido() %>'   style="width:100%"  maxlength='<%=campo.getLongitud()%>' >  
                                                 
                                            <%}%>
										<%}%>	
                                        </td>
                                        
                                   </tr>
                             
                             <%}%>
                             
                     </table>
                     
                  </td>
               </tr>       
         </table>
     </form>
     
     
     <p>
          <% if( BOTON.equals("INSERT") ){%>
              <img src="<%=BASEURL%>/images/botones/aceptar.gif"       name="acept"   height="21"  title='Guardar'      onClick="sendFormato(formulario);"                                                               onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
              <img src="<%=BASEURL%>/images/botones/regresar.gif"          name="regre"   height="21"  title='Regresar'     onClick="location.href='<%=CONTROLLER%>?estado=FormatoNacional&accion=Datos&evento=INIT'"        onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">        
          <%} else{%>
              <img src="<%=BASEURL%>/images/botones/modificar.gif"     name="modify"  height="21"  title='Modificar'    onClick="sendFormato(formulario);"                                                                                                                              onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
              <img src="<%=BASEURL%>/images/botones/eliminar.gif"      name="delete"  height="21"  title='Eliminar'     onClick="if( confirm('Desea Eliminar el Registro?')){ location.href='<%=CONTROLLER%>?estado=FormatoNacional&accion=Datos&evento=DELETE&formato=<%=formato%>&tipoDoc=<%=tipoDoc%>&documento=<%=docNo%>'}"      onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
              <img src="<%=BASEURL%>/images/botones/restablecer.gif"   name="reset"   height="21"  title='Restablecer'  onClick="location.href='<%=CONTROLLER%>?estado=FormatoNacional&accion=Datos&evento=RESET'"                                                                      onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
          <%}%>          
          <img src="<%=BASEURL%>/images/botones/salir.gif"             name="exit"    height="21"  title='Salir'        onClick="parent.close();"                                                                        onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
     </p>
                             
    
     
     
 	<%}%>
 	<br>
 
 
  <!-- mensajes -->
 <%}%>
 
 


</div>


<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js"     style='cursor: move;' ondragstart='' onMouseDown="_initMove(this);"    id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop24:plugins_24.js"  style='cursor: move;' ondragstart='' onMouseDown="_initMove(this);"    id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
<script>
function chequeo(form){
	var obj = document.getElementById('tabla_texto');
	var CCOSTO = document.getElementById('CCOSTO');
	var vari = document.getElementById('var');
	var costo ;
	if(form.value == "FC_08"){
		obj.style.display = 'block';
		CCOSTO.value = "";
	}else{
		obj.style.display = 'none';
		CCOSTO.value = form.value;
	}		
			
}
</script>


</body>
</html>
