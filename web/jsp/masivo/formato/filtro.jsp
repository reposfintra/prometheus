<!--
- Autor : Ing. Osvaldo P�rez Ferrer
- Date : 27 de Octubre de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve para consultar formato
--%>

<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.operation.model.*, com.tsp.operation.model.beans.*, com.tsp.util.*,java.util.*, java.text.*"%>

<html>
<head>
    <title>Generar Reporte Cuadro Azul</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">    
    <script src="<%=BASEURL%>/js/utilidades.js"></script>
    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<%
    String mensaje = request.getParameter("mensaje")==null? "Ha iniciado la generaci�n de su reporte, puede tardar algunos minutos..." : (String)request.getParameter("mensaje") ;
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    
    
    int anio      =  Util.AnoActual();
%>
</head>

<body>
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=Reporte Cuadro Azul"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top:100px; overflow: scroll;">        
    
    <form id="form1" method="post" action="">
    <table width="350"  border="2" align="center">
        <tr>
            <td><table width="100%"  border="1" align="center" class="tablaInferior">
            <tr>
            <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr>
            <td height="22" colspan=2 class="subtitulo1"><div align="center" class="subtitulo1">
                <strong> Periodo</strong>                     
            </div></td>
            <td width="212" class="barratitulo">
            <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left">
                    <%=datos[0]%>
        </tr>
            </table>
  
                
            <table width="100%"  border="0" align="center" cellpadding="2" cellspacing="3" onclick="document.getElementById('noexiste').style.visibility='hidden';">
                 
                       
                <tr  class="fila">
                    <td >A�o </td>
                    <td width='*' > 
                        <select name="anio" id="anio"  >
                            <option value="<%=String.valueOf(anio-1)%>"> <%=String.valueOf(anio-1)%>
                            <option value="<%=anio%>" selected> <%=anio%>
                            <option value="<%=anio+1%>"> <%=anio+1%>
                            
                        </select>                           
                    </td>
               </tr>
               
                <tr  class="fila" id="filadoc">
                    <td width='*'> Mes </td>
                    <td>
                        <select name="mes" id="mes" >
                        
                            <option value="Enero-01">Enero 
                            <option value="Febrero-02">Febrero
                            <option value="Marzo-03">Marzo
                            <option value="Abril-04">Abril
                            <option value="Mayo-05">Mayo 
                            <option value="Junio-06">Junio 
                            <option value="Julio-07">Julio 
                            <option value="Agosto-08">Agosto 
                            <option value="Septiembre-09">Septiembre
                            <option value="Octubre-10">Octubre
                            <option value="Noviembre-11">Noviembre
                            <option value="Diciembre-12">Diciembre
                            
                        </select>
                    </td>
                </tr>
               
                <tr  class="fila" id="filadoc">
                    <td width='*'> Formato Salida </td>
                    <td>
                        <select name="salida" id="salida" >
                        
                            <option value="web">WEB
                            <option value="excel">Excel
                            
                        </select>
                    </td>
                </tr>
               
            </table>
           
            </td>
            </tr>
        </table></td>
        </tr>
    </table>
    <br>
    <div align="center">
        
        <img src="<%=BASEURL%>/images/botones/aceptar.gif" id="btnAceptar" height="21" onMouseOver="botonOver(this);" onClick="sub('<%=CONTROLLER%>','<%=BASEURL%>');" onMouseOut="botonOut(this);" style="cursor:hand">        
        <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">            
    </div>
    </form>
    <br/>
	
        
    <div id="noexiste" <%=mensaje.equals("Ha iniciado la generaci�n de su reporte, puede tardar algunos minutos...")? "style='visibility:hidden'":""%>>
       <%if(mensaje != null && !mensaje.equals("") ){ %>
        
        <table border="2" align="center">
            <tr>                
                <td height="45"><table width="410" height="41" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                        <td width="282" height="35" align="center" class="mensajes"><%=mensaje%></td>
                        <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="78">&nbsp;</td>
                    </tr>
                </table></td>
            </tr>
        </table>        
               
       <%}%>     
    </div>
    	      
    </div>

<%=datos[1]%>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>        
</body>

</html>
<script>

function sub( con, url ){

    document.getElementById("btnAceptar").src=url+"/images/botones/aceptarDisable.gif";
    document.getElementById("btnAceptar").onclick = "";
    document.getElementById("btnAceptar").onmouseover = "";
    document.getElementById("btnAceptar").onmouseout = "";
    document.getElementById("btnAceptar").style.cursor=""
    
    document.getElementById('noexiste').style.visibility='visible';
    
    location.replace(con+"?estado=Formato&accion=Reporte&opcion="+form1.salida.value+"&anio="+form1.anio.value+"&mes="+form1.mes.value);    
}



</script>
