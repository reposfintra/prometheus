<!--  
	 - Author(s)       :      Jose de la Rosa
	 - Description     :      AYUDA FUNCIONAL - Ingreso especial
	 - Date            :      23/05/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>AYUDA FUNCIONAL - Modificar Esquema Formato</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/equipos/ingreso_especial/"; %>
<% String BASEIMG2 = BASEURL +"/images/ayuda/esquema_formato/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE ESQUEMA FORMATO</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa para modificar Esquema Formato.</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p>&nbsp;</p>
              </div></td>
          </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">En la siguiente pantalla se modifican los datos del esquema formato.</p>
            </td>
          </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG2%>ImagenModificar.JPG" border=0 ></div></td>
          </tr>
          
            <tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">El sistema verifica si alguno de los datos del formulario que esta seleccionado como campo obligatorio no esta lleno le saldr&aacute; en la pantalla el siguiente mensaje. </p>
            </td>
            </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG  src="<%=BASEIMG%>MensajeErrorCamposLLenos.JPG" border=0 ></div></td>
            </tr>
			
			<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            <p class="ayudaHtmlTexto">Si la fecha inicial es mayor a la fecha final le saldr&aacute; en la pantalla el siguiente mensaje. </p>
            </td>
            </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG  src="<%=BASEIMG2%>MensajeMenor.JPG" border=0 ></div></td>
            </tr>
			
            <tr>
              <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
                <p>Si la posicion de inicio es mayor a la fecha fina le mostrar&aacute; el siguiente mensaje.</p></td>
            </tr>
            <tr>
              <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>MensajeModificacion.JPG" border=0 ></div></td>
            </tr> 
            <tr>
              <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
                <p>Si el campo orden ya esta ingresado en el mismo formato en nuestra base de datos, entonces el sistema le mostrar&aacute; el siguiente mensaje. </p></td>
            </tr>
            <tr>
              <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG2%>MensajeOrden.JPG" border=0 ></div></td>
            </tr>
            <tr>
              <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
                <p>Si los la posici�n inicial o final estan dentro de un rango de formato ya esta ingresado en nuestra base de datos, entonces el sistema le mostrar&aacute; el siguiente mensaje. </p></td>
            </tr>
            <tr>
              <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG2%>MensajePosicion.JPG" border=0 ></div></td>
            </tr>		
            <tr>
                  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
                    <p>Si el Ingreso especial se anula, aparecer&aacute; el siguiente mensaje. </p></td>
                </tr>
                <tr>
              <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>MensajeAnular.JPG" border=0 ></div></td>
            </tr>
      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
