<!--
- Autor : Ing. Jose de la rosa
- Date  : 2 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja los ingresos especiales
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
	String campo = request.getParameter( "c_codigo" )!=null?request.getParameter( "c_codigo" ):"";
%>
<html>
<head><title>Buscar Esquema Formato</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Esquema Formato"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<form name="form2" method="post" action="<%=CONTROLLER%>?estado=Esquema_formato&accion=Search&opcion=listar">
    <table width="380" border="2" align="center">
      <tr>
        <td><table width="100%" align="center"  class="tablaInferior">
			<tr>
				<td width="173" class="subtitulo1">&nbsp;Esquema Formato </td>
				<td width="205" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
			</tr>
			<tr class="fila">
				<td width="173" align="left" >Formato </td>
			  <td valign="middle"><select name="c_codigo" id="c_codigo" class="textbox" style='width:90%;' onChange="cambiarDatos();">
									<option value=""></option>
									<% LinkedList tbl = (LinkedList)request.getAttribute ( "set_formato");
									   if(tbl!=null){
										   for(int i = 0; i<tbl.size(); i++){
											   TablaGen res = (TablaGen) tbl.get(i); %>
											  <option value="<%=res.getTable_code()%>" <%=campo.equals( res.getTable_code() )?"selected":""%>><%=res.getDescripcion()%></option>
										  <%}
									  }%>
								</select></td>
			</tr>			  
			<tr class="fila">
				<td align="left" >Nombre</td>
			    <td valign="middle"><select name="c_nombre" id="c_nombre" class="textbox" style='width:90%;'>
										<option value=""></option>
										<% LinkedList tblres = model.tablaGenService.obtenerTablas();
										if( tblres!=null && tbl!=null ){
											  for(int i = 0; i<tblres.size(); i++){
												   TablaGen respa = (TablaGen) tblres.get(i); 
												   if( campo.equals( respa.getReferencia() ) ){%>
													  <option value="<%=respa.getTable_code()%>"><%=respa.getDescripcion()%></option>
												  <%}
											  }
										 }%>
									</select></td>
			</tr>	
			<tr class="fila">
				<td width="173" align="left" >Titulo</td>
			  <td valign="middle"><input name="c_titulo" type="text" class="textbox" id="c_titulo"  maxlength="200" ></td>
			</tr>						  
        </table>
		</td>
      </tr>
    </table>
<p>
<div align="center"><img src="<%=BASEURL%>/images/botones/buscar.gif" style="cursor:hand" title="Buscar" name="buscar"  onClick="form2.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/detalles.gif" style="cursor:hand" title="Buscar todos los registros" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Esquema_formato&accion=Search&opcion=listar&c_codigo=&c_nombre=&c_titulo='" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/salir.gif"  name="regresar" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
</p>		
</form>
</div>
<%=datos[1]%>
</body>
</html>
<script>
function cambiarDatos(){
	form2.action = "<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/masivo/formato&pagina=Esquema_formatoBuscar.jsp&opcion=35&marco=no";
	form2.submit();
}
</script>