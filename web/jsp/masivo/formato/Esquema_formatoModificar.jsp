<!--
- Autor : Ing. Jose de la rosa
- Date  : 2 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la actualización de las flotas directas
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Modificar Formato Esquema</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<body onResize="redimensionar();" <%if(request.getParameter("reload")!=null){%>onLoad="parent.opener.location.reload();redimensionar();"<%}%>>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modificar Formato Esquema"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<%
    Esquema_formato esquema = model.esquema_formatoService.getEsquema ();
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<FORM name='forma' id='forma' method='POST' action='<%=CONTROLLER%>?estado=Esquema_formato&accion=Update'>
		<table width="650" border="2" align="center" >
		<tr>
		  <td>
			<table width="100%" class="tablaInferior">
			  <tr class="fila">
				<td colspan="2" align="left" class="subtitulo1">&nbsp;Formato Esquema </td>
				<td colspan="2" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
			  </tr>
			  <tr class="fila">
				<td align="left" >Formato</td>
				<td colspan="3" align="left" ><%=esquema.getNombre_programa ()%><input name="c_codigo" type="hidden" id="c_codigo" value="<%=esquema.getCodigo_programa ()%>"></td>
			  </tr>
			  <tr class="fila">
			  <td>Campo</td>
			  <td><%=esquema.getNom_campo ()%><input name="c_nombre" id="c_nombre2" type="hidden" value="<%=esquema.getNombre_campo ()%>"></td>
			  <td width="15%">Tipo</td>
			  <td width="35%"><select name="c_tipo" id="c_tipo" class="textbox" style='width:90%;'>
								<% LinkedList tbltip = (LinkedList)request.getAttribute ( "set_tipo");
									if(tbltip!=null){
									   for(int i = 0; i<tbltip.size(); i++){
										   TablaGen restip = (TablaGen) tbltip.get(i); %>
											<option value="<%=restip.getTable_code()%>" <%=esquema.getTipo ().equals(restip.getTable_code())?"selected":""%> ><%=restip.getDescripcion()%></option>
										<%}
									}%>
							  </select>
			    <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
			  </tr>
			  <tr class="fila">
				<td width="16%" align="left" >Titulo </td>
				<td colspan="3" align="left" ><textarea name="c_titulo" type="text" class="textbox" id="c_titulo" cols="100" rows="1" onKeyPress="soloAlfa(event)"><%=esquema.getTitulo()%></textarea></td>
			  </tr>	 		  
			  <tr class="fila">
				<td width="16%" align="left" >Posicion Inicial </td>
				<td width="34%" align="left" ><input name="c_pos_inicial" type="text" class="textbox" id="c_pos_inicial" size="8" maxlength="8"  onKeyPress="soloDigitos(event,'decNO')" value="<%=esquema.getPosicion_inicial()%>"> <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif">
				</td>
				<td valign="middle">Posicion Final</td>
				<td valign="middle"><input name="c_pos_final" type="text" class="textbox" id="c_pos_final" size="8" maxlength="8"  onKeyPress="soloDigitos(event,'decNO')" value="<%=esquema.getPosicion_final()%>"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
			  </tr>	
			  <tr class="fila">
				  <td>Orden</td>
				  <td><input name="c_orden" type="text" class="textbox" id="c_orden" size="8" maxlength="8"  onKeyPress="soloDigitos(event,'decNO')" value="<%=esquema.getOrden()%>"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
				  <td colspan="2"></td>
			  </tr>   
		  </table></td>
		</tr>
    </table>	
	  <input type="hidden" name="opcion" value="">
	<p>
	<div align="center">
	<img src="<%=BASEURL%>/images/botones/modificar.gif" style="cursor:hand" title="Modificar un registro" name="modificar"  onclick="return enviaDatos();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
	<img src="<%=BASEURL%>/images/botones/anular.gif" style="cursor:hand" title="Anular el registro" name="anular"  onClick="window.location='<%=CONTROLLER%>?estado=Esquema_formato&accion=Update&c_codigo=<%=esquema.getCodigo_programa ()%>&opcion=anular&c_nombre=<%=esquema.getNombre_campo ()%>';" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
	<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
	</p>
	<%  if(request.getAttribute("mensaje")!=null){%>
		<table border="2" align="center">
		  <tr>
			<td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
			  <tr>
				<td width="229" align="center" class="mensajes"><%=(String)request.getAttribute("mensaje")%></td>
				<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
				<td width="58">&nbsp;</td>
			  </tr>
			</table></td>
		  </tr>
		</table>
	<%  }%>

</FORM>
</div>
<%=datos[1]%>
</body>
</html>
<script>
function enviaDatos( valor , entero ){
	if(forma.c_codigo.value == '' ){
		alert("El campo del formato no puede ser vacio!");
		forma.c_codigo.focus();
		return false;
	}
	else if(forma.c_tipo.value == '' ){
		alert("El campo del tipo del programa no puede ser vacio!");
		forma.c_tipo.focus();
		return false;
	}
	else if(forma.c_nombre.value == '' ){
		alert("El campo del nombre del programa no puede ser vacio!");
		forma.c_nombre.focus();
		return false;
	}
	else if(forma.c_orden.value == '' ){
		alert("El campo orden no puede ser vacio!");
		forma.c_orden.focus();
		return false;
	}
	else if(forma.c_pos_inicial.value == '' ){
		alert("El campo posicion inicial no puede ser vacio!");
		forma.c_pos_inicial.focus();
		return false;
	}
	else if(forma.c_pos_final.value == '' ){
		alert("El campo posicion final no puede ser vacio!");
		forma.c_pos_final.focus();
		return false;
	}
	else if( parseFloat(forma.c_pos_inicial.value) >= parseFloat(forma.c_pos_final.value) ){
		alert("El campo posicion inicial debe ser menor a la posicion final!");
		forma.c_pos_inicial.focus();
		return false;
	}
	else if ( parseInt(forma.c_titulo.value.length) > 200 ){
		alert("El campo titulo debe ser menor a 200 caracteres");
		forma.c_titulo.focus();
		return false;
	}
	else{
		forma.opcion.value = 'modificar';
		forma.submit();
	}
}	
</script>