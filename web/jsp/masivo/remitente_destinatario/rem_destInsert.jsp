<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
    String codcli = (request.getParameter("codcli")!=null)?request.getParameter("codcli"):"";
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    
%>
<html>
<head>
<title>Ingresar Remitente Destinatario</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="<%=BASEURL%>/js/validar.js"></script>
<script src="<%=BASEURL%>/js/boton.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=Ingresar Remitente Destinatario"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top:100px; overflow: scroll;"> 
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=RemiDest&accion=Insert">
<table border="2" align="center" width="470">
  <tr>
    <td>
<table width="99%" align="center">
  <tr>
    <td  class="subtitulo1" width="50%"><p align="left">Agregar Remitente Destinatario </p></td>
    <td width="50%"  class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
  </tr>
</table>
  <table width="99%" align="center" >
  <tr  class="fila">
      <td width="132" nowrap ><strong>Secuencia</strong></td>
      <td width="299" nowrap ><input name="secuencia" type="text" class="textbox" id="secuencia"  value="<%=request.getParameter("secuencia")!=null?request.getParameter("secuencia"):""%>" size="6" maxlength="2" ></td>
  </tr>
    <tr  class="fila">
      <td width="132" nowrap ><strong>Tipo</strong></td>
      <td width="299" nowrap ><select name="tipo" class="listmenu" id="tipo" onFocus="sec()" >
        <option value="0"></option>
        <option value="R" >Remitente</option>
        <option value="D" >Destinatario</option>
      </select></td>
    </tr>
    <tr  class="fila">
      <td nowrap ><strong>Codigo del cliente</strong></td>
      <td nowrap class="Simulacion_Hiper"><input name="codcli" type="text"  class="textbox" id="codcli" size="15" maxlength="6"  value="<%=request.getParameter("codcli")!=null?request.getParameter("codcli"):""%>">
           <span class="Simulacion_Hiper"><a href="javascript:openVentana('<%=BASEURL%>','cliente');">Buscar Cliente</a></span></td>
    </tr>
    <tr  class="fila">
      <td nowrap ><strong>Nombre</strong></td>
      <td nowrap ><input name="nombre" type="text" class="textbox" id="nombre" size="50" maxlength="40" value="<%=request.getParameter("nombre")!=null?request.getParameter("nombre"):""%>"></td>
    </tr>
    <tr  class="fila">
      <td nowrap ><strong>Direccion</strong></td>
      <td nowrap ><input name="direccion" type="text" class="textbox" id="direccion" size="50" maxlength="40" value="<%=request.getParameter("direccion")!=null?request.getParameter("direccion"):""%>"></td>
    </tr>
    <tr  class="fila">
      <td nowrap ><strong>Referencia</strong></td>
      <td nowrap ><input name="referencia" type="text" class="textbox" id="referencia" size="14" maxlength="12" value="<%=request.getParameter("referencia")!=null?request.getParameter("referencia"):""%>"></td>
    </tr>
    <tr  class="fila">
      <td nowrap ><strong>Ciudad direcci&oacute;n </strong></td>
      <td nowrap ><input name="ciudad" type="text" class="textbox" id="ciudad" size="6" maxlength="2" value="<%=request.getParameter("ciudad")!=null?request.getParameter("ciudad"):""%>"> 
        <span class="Simulacion_Hiper"><a href="javascript:openVentana('<%=BASEURL%>','ciudad');">Buscar Ciudad </a></span></td>
    </tr>
    <tr  class="fila">
      <td nowrap ><strong>Telefono</strong></td>
      <td nowrap ><input name="telefono" type="text" class="textbox" id="telefono" onKeyPress="soloDigitos(event, 'decNO')" size="20" maxlength="20" value="<%=request.getParameter("telefono")!=null?request.getParameter("telefono"):""%>"></td>
    </tr>
    <tr  class="fila">
      <td nowrap ><strong>Nombre del Contacto</strong></td>
      <td nowrap ><input name="nombre_contacto" type="text" class="textbox" id="nombre_contacto" size="50" maxlength="40" value="<%=request.getParameter("nombre_contacto")!=null?request.getParameter("nombre_contacto"):""%>"></td>
    </tr>
  </table>  
  </td>
  </tr>
  </table>
  <br>
  <div align="center">    <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod"  height="21" onClick="validarFormularioRemDest(form1);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp; 
	<img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="form1.reset();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; 
	<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></div>



<%String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";%>
<%if( !msg.equals("") ){%>
	<p>
   <table border="2" align="center">
     <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"> <%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>  
   <%}%>
</form>    
</div>	
  
<%=datos[1]%>

</body>
</html>

<script>
document.getElementById("tipo").value = '<%=request.getParameter("tipo")!=null?request.getParameter("tipo"):""%>';
function openVentana(BASEURL,opc) {
    if (opc=="cliente")
        window.open(BASEURL+"/consultas/consultasCliente.jsp",'','top=20,left=200,width=700,heigth=450,scrollbars=yes,status=yes,resizable=yes');
    else if (opc=="ciudad") 
	    window.open(BASEURL+"/consultas/consultaCiudad.jsp",'','top=20,left=200,width=700,heigth=450,scrollbars=yes,status=yes,resizable=yes');       
}
function sec() {
    var va = document.getElementById("secuencia");
    
	var valor=""+va.value;
//alert(va.value+ " "+valor.length);
    if (valor.length==1) {
        va.value = "0"+valor;
    }
}
</script>