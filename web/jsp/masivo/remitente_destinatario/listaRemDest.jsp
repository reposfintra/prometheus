<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script src="<%=BASEURL%>/js/reporte.js"></script>
<script src="<%=BASEURL%>/js/boton.js"></script>
<title>Buscar Clientes</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

</head>
<body>
<%
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 20;
    int maxIndexPages = 5;
    Vector vecRemDest = model.remidestService.getVectorRemiDest();    
%>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=Listado Remitente Destinatario"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top:100px; overflow: scroll;">
<table border="2" align="center" width="732">
  <tr>
    <td>
	<table width="99%"  align="center"   >
  <tr>
    <td width="285" height="22"  class="subtitulo1"><p align="left">Lista de Remitentes y/o Destinatarios</p></td>
    <td width="401"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4" class="tblTitulo">
  <tr>
    <td width="74" nowrap>CODIGO</td>
    <td width="197" ><div align="center">
        <div align="left" nowrap>CLIENTE</div>
    </div></td>
    <td width="207" nowrap>NOMBRE</td>
    <td width="190" nowrap>DIRECCI&Oacute;N</td>
   <td width="190" nowrap>CIUDAD</td>
    <td nowrap> REFERENCIA </td>
    <td nowrap> TELEFONO </td>
    <td nowrap> NOMBRE CONTACTO </td>
  </tr>
  <pg:pager
    items="<%=vecRemDest.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
  <%-- keep track of preference --%>
   <%for (int i = offset.intValue(),
	         l = Math.min(i + maxPageItems, vecRemDest.size());
	     i < l; i++)
	{
        RemiDest remDest = (RemiDest) vecRemDest.elementAt(i);%>
  <pg:item>
  <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>"  title="Seleccione el remitente o destinatario" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" onClick="window.open('<%=BASEURL%>/jsp/masivo/remitente_destinatario/rem_destUpdate.jsp?pos=<%=i%>','M','status=yes,scrollbars=no,width=780,height=650,resizable=yes');">
    <td class="bordereporte" nowrap><%=remDest.getCodigo()%></td>
    <td class="bordereporte" nowrap><%=remDest.getNomcli()%></td>
    <td class="bordereporte" nowrap><%=remDest.getNombre()%></td>
    <td class="bordereporte" nowrap><%=remDest.getDireccion()%></td>
    <td class="bordereporte" nowrap><%=remDest.getCiudad()%></td>
    <td class="bordereporte" nowrap><%=remDest.getReferencia()%></td>
    <td class="bordereporte" nowrap><%=remDest.getTelefono()%></td>
    <td class="bordereporte" nowrap><%=remDest.getNombre_contacto()%></td>
  </tr>
  </pg:item>
  <%}
  if (vecRemDest.size()>=6){%>
  <tr class="filagris">
    <td height="92" colspan="8" nowrap align="center">
	<pg:index>
      <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
    </pg:index></td>
  </tr>
  <%}%>
  </pg:pager>
</table>
</td>
</tr>
</table>

<br>
<center>
  <table width="717" border="0">
    <tr>
      <td align="left"><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="location.href='<%=BASEURL%>/jsp/masivo/remitente_destinatario/rem_destSearch.jsp'" onMouseOut="botonOut(this);" style="cursor:hand"></td>
    </tr>
  </table>
      
</center>
</div>
</body>
</html>
<script>
function setearCampo(codcli) {
    var campo = parent.opener.document.form1.codcli;       
    campo.value=codcli;
    parent.close();
}
</script>
