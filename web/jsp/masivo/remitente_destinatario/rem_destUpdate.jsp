<%@ page session="true"%>
<%@ page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
  String pos = (request.getParameter("pos")!=null) ? request.getParameter("pos") : "0";
  Vector vecRemDest = model.remidestService.getVectorRemiDest();
  RemiDest rd = null;
	try{
           rd  = (RemiDest) vecRemDest.elementAt(Integer.parseInt(pos));  
        }
        catch(Exception ex){}
%>
<html>
<head>
    <title>Modificar Remitente Destinatario</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <script src="<%=BASEURL%>/js/validar.js"></script>
    <script src="<%=BASEURL%>/js/boton.js"></script>
    <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">

</head>

<body>
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=Modificar Remitente Destinatario"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top:100px; overflow: scroll;">
        <form name="form1" method="post" action="<%=CONTROLLER%>?estado=RemiDest&accion=Update">
        
             <% if(rd != null){%>
        <table border="2" align="center" width="470">
            <tr>
            <td>
                           
                <table width="99%" align="center">
                    <tr>
                        <td  class="subtitulo1" width="56%"><p align="left">Modificar Remitente Destinatario </p></td>
                        <td width="44%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                    </tr>
                </table>
                
                <table width="99%" align="center" >
                    <tr  class="fila">
                    <td width="139" nowrap ><strong>Tipo</strong></td>
                    <td width="302" nowrap ><%if(rd.getTipo().equals("RE")) 
                                    out.print("REMITENTE"); 
                                else
                                    out.print("DESTINATARIO");
                              %>
                    <input type="hidden" name="codigo" value="<%=rd.getCodigo()%>">
                    <input type="hidden" name="pos" value="<%=pos%>"></td>
                    </tr>
                    <tr  class="fila">
                    <td nowrap ><strong>Codigo del cliente</strong></td>
                    <td nowrap ><%=rd.getCodcli()%></td>
                    </tr>
	
                    <tr  class="fila">
                    <td nowrap ><strong>Ciudad direcci&oacute;n </strong></td>
                    <td nowrap ><%=rd.getCiudad()%></td>
                    </tr>
                    <tr  class="fila">
                    <td width="139" nowrap ><strong>Estado</strong></td>
                    <td width="302" nowrap ><select name="est" class="listmenu" id="ee">
                    <option value="A" selected <%if (rd.getEstado().equals("A")) {%>selected<%}%>>ACTIVO</option>
                    <option value=""  <%if (rd.getEstado().equals("")) {%>selected<%}%>>ANULADO</option>        
                    </select></td>
                    </tr>
                    <tr  class="fila">
                    <td nowrap ><strong>Direccion</strong></td>
                    <td nowrap ><input name="direccion" type="text" class="textbox" id="direccion" value="<%=rd.getDireccion()%>" size="50" maxlength="40"></td>
                    </tr>

                    <tr  class="fila">
                    <td nowrap ><strong>Nombre</strong></td>
                    <td nowrap ><input name="nombre" type="text" class="textbox" id="nombre" value="<%=rd.getNombre()%>" size="50" maxlength="40"></td>
                    </tr>
    
                    <tr  class="fila">
                    <td nowrap ><strong>Referencia</strong></td>
                    <td nowrap ><input name="referencia" type="text" class="textbox" id="referencia" value="<%=rd.getReferencia()%>" size="14" maxlength="12"></td>
                    </tr>
                    
                    <tr  class="fila">
                    <td nowrap ><strong>Telefono</strong></td>
                    <td nowrap ><input name="telefono" type="text" value="<%=rd.getTelefono()%>" class="textbox" id="telefono" onKeyPress="soloDigitos(event, 'decNO')" size="20" maxlength="20"></td>
                    </tr>
                    
                    <tr  class="fila">
                    <td nowrap ><strong>Nombre del Contacto</strong></td>
                    <td nowrap ><input name="nombre_contacto" type="text" value="<%=rd.getNombre_contacto()%>" class="textbox" id="nombre_contacto" size="50" maxlength="40"></td>
                    </tr>

                </table>  
                
            </td>
            </tr>
        </table>
        <br>
        <div align="center">    <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod"  height="21" onClick="form1.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp; 
        <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="form1.reset();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; 
        <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></div>
        <%}%>
        
        </form>

<%String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";%>
<%if( !msg.equals("")){%>
<script>parent.opener.location.reload();</script>
        <p>
        <table border="2" align="center">
            <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                        <td width="229" align="center" class="mensajes"> <%=msg%></td>
                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                    </tr>
                </table></td>
            </tr>
        </table>
        </p>
        <%if( msg.equals("Registro anulado Exitosamente")){%>
        <div align="center">
            <img src="<%=BASEURL%>/images/botones/regresar.gif"    height="21" onMouseOver="botonOver(this);" onClick="location.replace('<%=BASEURL%>/jsp/masivo/remitente_destinatario/rem_destSearch.jsp');" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; 
            <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
        </div>     
        <%}%>
   <%}%>
    </div>
</body>
</html>