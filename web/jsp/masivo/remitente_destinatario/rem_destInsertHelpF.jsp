<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE>Funcionamiento de la ventana de Ingreso de Equipo Propio</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
</HEAD>
<BODY>  

  <table width="95%"  border="2" align="center">
    <tr>
      <td>
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">INGRESAR REMITENTE / DESTINATARIO </div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento de la p&aacute;gina para ingreso de remitentes y/o destinatarios.</td>
          </tr>
          <tr>
            <td  height="18"  class="ayudaHtmlTexto"><p>&nbsp;</p>
            <p>Ingrese un n&uacute;mero para secuencia. </p>
            <p>En tipo, seleccione remitente o destinatario. </p></td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/masivo/rem_des/img1.JPG" width="694" height="423" ></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p>Puede ingresar los c&oacute;digos de cliente y ciudad si los conoce, sino puede obtenerlos realizando una b&uacute;squeda a partir de su nombre. </p>
              <p>Ingrese nombre y direcci&oacute;n del remitente o destinatario, y referencia si existe.</p>
              <p>Finalmente haga click en el bot&oacute;n Aceptar para ingresar el remitente o destinatario al sistema. Se mostrar&aacute; el siguiente mensaje. </p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEURL%>/images/ayuda/masivo/rem_des/img2.JPG" width="365" height="58" ></div></td>
          </tr>   
          
      </table></td>
    </tr>
  </table>
  <table width="416" align="center">
	<tr>
	<td align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	</td>
	</tr>
</table>
</BODY>
</HTML>
