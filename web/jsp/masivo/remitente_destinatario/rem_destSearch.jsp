<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input"%>

<html>
<head>
<title>Buscar Remitente Destinatario</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="<%=BASEURL%>/js/validar.js"></script>
<script src="<%=BASEURL%>/js/boton.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">

</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=Buscar Remitente Destinatario"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top:100px; overflow: scroll;">
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=RemiDest&accion=Search">
<table border="2" align="center" width="435">
  <tr>
    <td>
<table width="99%" align="center">
  <tr>
    <td  class="subtitulo1" width="56%"><p align="left">Busqueda de remitentes </p></td>
    <td width="44%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
  <table width="99%" align="center" >
    <tr  class="fila">
      <td width="134" nowrap ><strong>Tipo</strong></td>
      <td width="273" nowrap ><select name="tipo" class="listmenu" id="tipo">
        <option value="0"></option>
        <option value="R">Remitente</option>
        <option value="D">Destinatario</option>
      </select></td>
    </tr>
    <tr  class="fila">
      <td nowrap ><strong>Codigo del cliente</strong></td>
      <td nowrap class="Simulacion_Hiper"><input name="codcli" type="text" class="textbox" id="codcli" size="15" maxlength="6">
          <a href="javascript:openVentana('<%=BASEURL%>','cliente');">Buscar Cliente</a></td>
    </tr>
    <tr  class="fila">
      <td nowrap ><strong>Ciudad direcci&oacute;n </strong></td>
      <td nowrap ><input name="ciudad" type="text" class="textbox" id="ciudad" size="6" maxlength="2"> 
        <span class="Simulacion_Hiper"><a href="javascript:openVentana('<%=BASEURL%>','ciudad');">Buscar Ciudad </a></span></td>
    </tr>
  </table>  
  </td>
  </tr>
  </table>
  <br>
  <div align="center">    <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod"  height="21" onClick="validarRemDestSearch(form1);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp; 
	<img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="form1.reset();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; 
	<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></div>
</form>

<%String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";%>
<%if(!msg.equals("OK") && !msg.equals("")){%>
	<p>
   <table border="2" align="center">
     <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"> <%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
   <%}%>
</div>   
</body>
</html>
<script>
function openVentana(BASEURL,opc) {
    if (opc=="cliente")
        window.open(BASEURL+"/consultas/consultasCliente.jsp",'','top=0,left=200,width=700,heigth=950,scrollbars=yes,status=yes,resizable=yes');
    else if (opc=="ciudad") 
        window.open(BASEURL+"/consultas/consultaCiudad.jsp",'','top=0,left=200,width=700,heigth=950,scrollbars=yes,status=yes,resizable=yes');       
}
</script>