<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Descripci&oacute;n de campos Ingreso de Equipos Propios</title>

<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<% String BASEIMG = BASEURL +"/images/botones/"; %> 
<br>
<table width="594"  border="2" align="center">
  <tr>
    <td width="635" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">INGRESAR REMITENTE O DESTINATARIO </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">&nbsp;</td>
        </tr>
        <tr>
          <td width="111" class="fila">Secuencia:</td>
          <td width="461"  class="ayudaHtmlTexto"> Dato requerido para poder ubicar en Mims.</td>
        </tr>
        <tr>
          <td  class="fila"> Tipo:</td>
          <td  class="ayudaHtmlTexto">Seleccione remitente o destinatario.  </td>
        </tr>
		<tr>
          <td width="111"  class="fila"> C&oacute;digo de Cliente: </td>
          <td width="461"  class="ayudaHtmlTexto">Puede digitarlo si lo conoce, o buscarlo a partir del nombre del cliente . </td>
        </tr>
		<tr>
          <td width="111"  class="fila"> Nombre: </td>
          <td width="461"  class="ayudaHtmlTexto">Nombre del remitente o destinatario . </td>
        </tr>
		<tr>
		  <td  class="fila">Direcci&oacute;n:</td>
	      <td  class="ayudaHtmlTexto">Direcci&oacute;n del remitente o destinatario. </td>
		</tr>
		<tr>
		  <td  class="fila">Referencia:</td>
		  <td  class="ayudaHtmlTexto">Referencia del cliente . </td>
	    </tr>
		<tr>
		  <td  class="fila">Ciudad:</td>
		  <td  class="ayudaHtmlTexto">C&oacute;digo de la ciudad en la cual se encuentra la direcci&oacute;n, puede digitarlo si lo conoce, o buscarlo a partir del nombre de la ciudad. </td>
	    </tr>
		<tr>
		  <td  class="fila">Bot&oacute;n Aceptar: </td>
		  <td  class="ayudaHtmlTexto">Ingresa el remitente o destinatario . </td>
	    </tr>
		<tr>
		  <td  class="fila">Bot&oacute;n Cancelar:</td>
		  <td  class="ayudaHtmlTexto">Reinicia el formulario. </td>
	    </tr>
		<tr>
		  <td  class="fila">Bot&oacute;n Salir:</td>
		  <td  class="ayudaHtmlTexto">Cierra la ventana. </td>
	    </tr>
      </table>
    </td>
  </tr>
</table>
<br>
<table width="416" align="center">
	<tr>
	<td align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	</td>
	</tr>
</table>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
