<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script src="<%=BASEURL%>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<title>Buscar Clientes</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

</head>
<body>
<%
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 20;
    int maxIndexPages = 5;
    Vector vecCiudades = model.ciudadService.obtCiudades();
%>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=Ciudades"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top:100px; overflow: scroll;">
<table border="2" align="center" width="546">
  <tr>
    <td>
	<table width="99%"  align="center"   >
  <tr>
    <td width="227" height="22"  class="subtitulo1"><p align="left">Lista de Ciudades</p></td>
    <td width="229"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4" class="tblTitulo">
  <tr>
    <td width="158" nowrap>CODIGO</td>
    <td width="323" ><div align="center">
        <div align="left">CIUDAD</div>
    </div></td>
  </tr>
  <pg:pager
    items="<%=vecCiudades.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
  <%-- keep track of preference --%>
  <%
      for (int i = offset.intValue(),
	         l = Math.min(i + maxPageItems, vecCiudades.size());
	     i < l; i++)
	{
        Ciudad ciudad = (Ciudad) vecCiudades.elementAt(i);%>
  <pg:item>
  <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" onClick="setearCampo('<%=ciudad.getCodCiu()%>')">
    <td class="bordereporte"><%=ciudad.getCodCiu()%></td>
    <td class="bordereporte"><%=ciudad.getNomCiu()%></td>
  </tr>
  </pg:item>
  <%}
  if (vecCiudades.size()>=6){%>
  <tr class="filagris">
    <td height="92" colspan="2" nowrap align="center">
	<pg:index>
      <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
    </pg:index></td>
  </tr>
  <%}%>
  </pg:pager>
</table>
</td>
</tr>
</table>

<br>
<table width="543" border="0" align="center">
  <tr>
    <td align="left"><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="history.back();" onMouseOut="botonOut(this);" style="cursor:hand"></td>
  </tr>
</table>    
</div>
</body>
</html>
<script>
function setearCampo(ciu) {
    var campo = parent.opener.document.form1.ciudad;       
    campo.value=ciu;
    parent.close();
}
</script>