<%@ page session="true"%>
<%@ page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
%>
<html>
<head>
<title>Buscar Proveedor Escolta</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="<%=BASEURL%>/js/funciones.js"></script>
<script src="<%=BASEURL%>/js/boton.js"></script>
<script src="<%=BASEURL%>/js/reporte.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">

</head>

<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Proveedor Escolta"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=EscoltaProveedor&accion=Search">
<table border="2" align="center" width="352">
  <tr>
    <td>
<table width="99%" align="center">
  <tr>
    <td  class="subtitulo1" width="63%"><p align="left">Busqueda de Proveedores Escoltas </p></td>
    <td width="37%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
  <table width="99%" align="center" >
    <tr  class="fila">
      <td width="117" nowrap ><strong>Nit</strong></td>
      <td width="208" nowrap ><span class="Simulacion_Hiper">
        <input name="nit" type="text" class="textbox" id="nit" size="20" maxlength="15">
</span></td>
    </tr>
<tr  class="fila">
      <td nowrap >Ciudad Origen</td>
      <td nowrap ><input name="origen" type="text" class="textbox" id="ciudad" size="6" maxlength="2"> 
        <span class="Simulacion_Hiper"><a href="javascript:openVentana('<%=BASEURL%>');">Buscar Ciudad </a></span></td>
    </tr>
  </table>  
  </td>
  </tr>
  </table>
  <br>
  <div align="center">    <img src="<%=BASEURL%>/images/botones/buscar.gif" name="mod"  height="21" onClick="validarBuscar();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp; 
	<img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="form1.reset();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; 
	<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></div>
</form>
<%String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";%>
<%if(!msg.equals("OK") && !msg.equals("")){%>
	<p>
   <table border="2" align="center">
     <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"> <%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
   <%}%>
<% 
  Vector datos=null;
  if(model.proveedorEscoltaService.getVectorProveedores()!=null) {
      datos = model.proveedorEscoltaService.getVectorProveedores();
  }  
  if (datos!=null) {%>
<table border="2" align="center" width="605">
  <tr>
    <td>
	<table width="99%"  align="center"   >
  <tr>
    <td width="227" height="22"  class="subtitulo1"><p align="left">Tarifa de Proveedores de Escolta </p></td>
    <td width="229"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
<table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4" class="tblTitulo">
  <tr>
    <td width="203" height="20" nowrap>PROVEEDOR</td>
    <td width="121" ><div align="center">
        <div align="left">ORIGEN</div>
    </div></td>
    <td width="135" >DESTINO</td>
    <td width="100" >TARIFA</td>
  </tr>
  <pg:pager
    items="<%=datos.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
  <%-- keep track of preference --%>
  <%
      for (int i = offset.intValue(),
	         l = Math.min(i + maxPageItems, datos.size());
	     i < l; i++)
	{
        Proveedor_Escolta prov = (Proveedor_Escolta) datos.elementAt(i);%>
  <pg:item>
  <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" onClick="window.open('<%=BASEURL%>/jsp/masivo/proveedor_escolta/proveedorUpdate.jsp?pos=<%=i%>','','top=10,left=180,width=660,heigth=280,scrollbars=no,resizable=yes,status=yes')">
    <td class="bordereporte"><%=prov.getNomProveedor()%></td>
    <td class="bordereporte"><%=prov.getNomOrigen()%></td>
    <td class="bordereporte"><%=prov.getNomDestino()%></td>
    <td class="bordereporte"><%=prov.getTarifa()%></td>
  </tr>
  </pg:item>
  <%}
  if (datos.size()>=6){%>
  <tr class="filagris">
    <td colspan="4" nowrap align="center">
	<pg:index>
      <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
    </pg:index></td>
  </tr>
  <%}%>
  </pg:pager>
</table>
</td>
</tr>
</table>
<%}%>
</div>
</body>
</html>
<script>
function openVentana(BASEURL) {   
    window.open(BASEURL+"/consultas/consultaCiudad.jsp",'','top=20,left=200,width=600,heigth=350,scrollbars=yes,status=yes');
}
</script>