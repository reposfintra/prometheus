<%@ page session="true"%>
<%@ page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%  
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>

<html>
<head>
<title>Ingresar proveedor anticipo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="<%=BASEURL%>/js/funciones.js"></script>
<script src="<%=BASEURL%>/js/boton.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">

</head>

<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ingresar Tarifa Proveedor"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=EscoltaProveedor&accion=Insert">
<table border="2" align="center" width="317">
  <tr>
    <td>
<table width="99%" align="center">
  <tr>
    <td width="50%" height="22" class="subtitulo1"><p align="left">Proveedor Escolta </p></td>
    <td  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left">
<%=datos[0]%></td>
  </tr>
</table>
  <table width="99%" align="center" class="Letras">
    <tr  class="fila">
      <td width="131" nowrap ><strong>Nit</strong></td>
      <td width="159" nowrap><input name="nit" type="text" class="textbox" id="nit" size="18" maxlength="15">          </td>
      </tr>
    <tr  class="fila">
      <td width="131" nowrap ><strong>Ciudad Origen</strong></td>
      <td nowrap ><%TreeMap ori = model.ciudadService.getCiudades(); %>
            <input:select name="origen" options="<%=ori%>" attributesText="style='width:99% ' class='textbox'"/></td>
    </tr>
    <tr  class="fila">
      <td nowrap >Ciudad Destino</td>
      <td nowrap >
            <input:select name="destino" options="<%=ori%>" attributesText="style='width:99% ' class='textbox'"/></td>
      </tr>
    <tr  class="fila">
      <td nowrap >Tarifa</td>
      <td nowrap ><input name="tarifa" type="text" class="textbox" id="tar" size="15" onKeyPress="soloDigitos(event,'noDec')"></td>
      </tr>
    <tr  class="fila">
      <td nowrap ><strong>Codigo Contable </strong></td>
      <td nowrap ><input name="codigo" type="text" class="textbox" id="codigo" size="18" maxlength="10" ></td>
      </tr>
  </table>  
  </td>
  </tr>
  </table>
  <br>
  <div align="center">    
<img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod"  height="21" onClick="return validarProveedorEscolta(form1)" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp; 
	<img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="form1.reset();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; 
	<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></div>
</form>
<%String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";%>
<%if(!msg.equals("OK") && !msg.equals("")){%>
	<p>
   <table border="2" align="center">
     <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"> <%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
   <%}%>
</div>
<%=datos[1]%>
</body>
</html>
