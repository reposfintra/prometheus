<%@ page session="true"%>
<%@ page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Modificar Proveedor Escolta</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="<%=BASEURL%>/js/funciones.js"></script>
<script src="<%=BASEURL%>/js/boton.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">

</head>

<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modificar Tarifa Proveedor"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=EscoltaProveedor&accion=Update">
<%
  int pos = Integer.parseInt(request.getParameter("pos"));
  Vector datos = model.proveedorEscoltaService.getVectorProveedores();
  Proveedor_Escolta prov = (Proveedor_Escolta) datos.elementAt(pos);
%>
<table border="2" align="center" width="317">
  <tr>
    <td>
<table width="99%" align="center">
  <tr>
    <td width="50%" height="22"  class="subtitulo1"><p align="left">Proveedor Escolta </p></td>
    <td  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
  </tr>
</table>
  <table width="99%" align="center" class="Letras">
    <tr  class="fila">
      <td width="131" nowrap ><strong>Nit</strong></td>
      <td width="159" nowrap><input name="nit" type="text" class="textbox" id="nit" value="<%=prov.getNit()%>" size="18" maxlength="15" readonly>
        <input name="pos" type="hidden" id="pos" value="<%=pos%>"></td>
      </tr>
    <tr  class="fila">
      <td width="131" nowrap ><strong>Ciudad Origen</strong></td>
      <td nowrap ><%=prov.getNomOrigen()%>
        <input name="origen" type="hidden" id="origen" value="<%=prov.getOrigen()%>"></td>
    </tr>
    <tr  class="fila">
      <td nowrap >Ciudad Destino</td>
      <td nowrap ><%=prov.getNomDestino()%>
        <input name="destino" type="hidden" id="destino" value="<%=prov.getDestino()%>"></td>
      </tr>
    <tr  class="fila">
      <td nowrap >Estado</td>
      <td nowrap ><select name="est" class="listmenu" id="est">
        <option value="" <%if(prov.getReg_status().equals("")) {%>selected<%}%>>Activo</option>
        <option value="A" <%if(prov.getReg_status().equals("A")) {%> selected <%}%>>Inactivo</option>
      </select></td>
    </tr>
 
    <tr  class="fila">
      <td nowrap >Tarifa</td>
      <td nowrap ><input name="tarifa" type="text" class="textbox" id="tar" value="<%=prov.getTarifa()%>" size="15" onKeyPress="soloDigitos(event,'noDec')"></td>
      </tr>
    <tr  class="fila">
      <td nowrap ><strong>Codigo Contable </strong></td>
      <td nowrap ><input name="codigo" type="text" class="textbox" id="codigo" value="<%=prov.getCod_contable()%>" size="18" maxlength="10"></td>
      </tr>
  </table>  
  </td>
  </tr>
  </table>
  <br>
  <div align="center">    
<img src="<%=BASEURL%>/images/botones/modificar.gif" name="mod"  height="21" onClick="return validarProveedorEscolta(form1)" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp; 
	<img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="form1.reset();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; 
	<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand"></div>
</form>
<%String msg = (request.getParameter("msg")!=null)?request.getParameter("msg"):"";%>
<%if(!msg.equals("OK") && !msg.equals("")){%>
	<p>
   <table border="2" align="center">
     <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"> <%=msg%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
  </p>
   <%}%>
</div>
</body>
</html>
