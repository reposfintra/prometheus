<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
   <title>Manejo de Imagen</title> 
   <script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
   <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
   <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
   <link href="../../css/estilostsp.css" rel="stylesheet" type="text/css">
<%
        String codigo = request.getParameter("cod");
		String distrito = request.getParameter("dis");
		TBLActividades actividad = model.tLBActividadesServices.ObtenerTBLActividad(codigo, distrito);
		String url = CONTROLLER + "?estado=TBLActividad&accion=Insert&flag=A";
%>  
   <script>
   function anular(){
        forma.action = '<%= url %>';
        forma.submit();
   }
   </script>
   <link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body <%if(request.getParameter("reload")!=null){%>onLoad="parent.opener.location.reload();"<%}%>>
  <!-- Parametros de configuracion -->
  <FORM method='post' id="forma" NAME='formulario' onSubmit='return validarTCamposLlenos();' >
    <table width="350" border="2" align="center">
      <tr>
        <td><table width="100%" class="tablaInferior">
          <tr>
            <td colspan='3'>
              <table width='100%' border="0" cellpadding='0' cellspacing='0' class="barratitulo">
                <tr class="fila">
                  <td width="132" align="left"  class="subtitulo1">&nbsp;Detalle Actividad </td>
                  <td align="left" width="196" class="barratitulo"><img src="<%=BASEURL%>//images/cuadrosverde.JPG" width="32" height="20">
              </tr>
            </table></td>
          </tr>
          <TR class="fila">
            
           
            <TD valign="top" width='145'>C&oacute;digo Actividad : </TD>
            <TD valign="top" width='145'> <%= actividad.getCodigo() %><input name="c_codigo" type="hidden" id="c_codigo" value="<%= actividad.getCodigo() %>"></TD>
          </TR>
          <TR class="fila" width="145">
            <TD valign="top">Descripcion :</TD>
            <TD valign="top"><input name="c_descripcion" type="text" class="textbox" id="c_descripcion" value="<%= actividad.getDescripcion() %>" maxlength="6"  >
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></TD>
          </TR>
          <TR class="fila">
            <TD valign="top" width="145">Sigla : </TD>
            <TD valign="top"><input name="c_sigla" type="text" class="textbox" id="c_sigla" value="<%= actividad.getSigla() %>" maxlength="6" >
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></TD>
          </TR>
        </TABLE>        </td>
      </tr>
    </table>
<br>
<center>
	<img src="<%=BASEURL%>/images/botones/modificar.gif" name="c_modificar" onClick="forma.action='<%= CONTROLLER %>?estado=TBLActividad&accion=Insert&flag=M';forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
    <img src="<%=BASEURL%>/images/botones/anular.gif" name="c_anular" onClick="forma.action='<%=CONTROLLER %>?estado=TBLActividad&accion=Insert&flag=A'; forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_buscar" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">               
</center>
</FORM>
 <% if (request.getParameter("msg")!= null){ %>
  <br>
  <table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<% } %>
</body>
</html>
