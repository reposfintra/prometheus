<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="java.util.*" %>
<%@page import="com.tsp.util.Util"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
   <title>Manejo de Imagen</title> 
   <script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
   <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
   <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
   <link href="../../css/estilostsp.css" rel="stylesheet" type="text/css">
   <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body>
  <center>
  <FORM ACTION='<%= CONTROLLER %>?estado=TBLActividad&accion=Insert&flag=N' method='post' id="forma" NAME='formulario' onSubmit='return validarTCamposLlenos();' >
    <table width="350" border="2" align="center">
      <tr>
        <td width="3350"><table class="tablaInferior">
          <tr>
            <td colspan='3'>
              <table width='100%' cellpadding='0' cellspacing='0' class="barratitulo">
                <tr class="fila">
                  <td width="137" align="left"  class="subtitulo1">Ingresar Actividad</td>
                  <td align="left" width="193" class="barratitulo"><img src="<%=BASEURL%>//images/titulo.gif">
              </tr>
            </table></td>
          </tr>
          <TR class="fila">
            
           
            <TD valign="top" width='145'>C&oacute;digo Actividad : </TD>
            <TD valign="top" width='145'><input name="c_codigo" type="text" class="textbox" id="c_codigo" value="" maxlength="6">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></TD>
          </TR>
          <TR class="fila" width="145">
            <TD valign="top">Descripcion :</TD>
            <TD valign="top"><input name="c_descripcion" type="text" class="textbox" id="c_descripcion" value="" maxlength="15"  >
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></TD>
          </TR>
          <TR class="fila">
            <TD valign="top" width="145">Sigla : </TD>
            <TD valign="top"><input name="c_sigla" type="text" class="textbox" id="c_sigla" value="" maxlength="10" >
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></TD>
          </TR>
        </TABLE>        
		</td>
      </tr>
    </table>
	<br>
  	<center>              
	<img src="<%=BASEURL%>/images/botones/aceptar.gif" name="c_aceptar" onClick="return TCamposLlenos();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
	<img src="<%=BASEURL%>/images/botones/cancelar.gif" name="c_buscar" onClick="forma.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
	<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_buscar" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
     </center>
  </FORM>
  <% if (request.getParameter("msg")!= null){ %>
  <br>
  <table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<% } %>
</body>
</html>
