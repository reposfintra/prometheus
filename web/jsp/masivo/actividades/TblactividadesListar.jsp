<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
 
<html>
<head>
<title>Listado de Proveedor</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body <%if(request.getParameter("reload")!=null){%>onLoad=="parent.opener.location.reload();"<%}%>>
<p>&nbsp;</p>
<p>
<%  
        Usuario usuario = (Usuario)session.getAttribute("Usuario");
        String user = usuario.getLogin();
        String distrito = (String)session.getAttribute("Distrito");
		String style = "simple";
        String position =  "bottom";
        String index =  "center";
        int maxPageItems = 10;
        int maxIndexPages = 10;
        List actividades = model.tLBActividadesServices.ConsultarTBLActividades(distrito);
        TBLActividades actividad;
		
        if ( actividades.size() > 0 ){  
%>
</p>
<table width="650" border="2" align="center">
    <tr>
      <td>
	  <table width="100%" class="barratitulo">
              <tr>
                <td width="233" class="subtitulo1">Detalles del C&oacute;digo de Actividades </td>
                <td width="393" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="33" height="20"></td>
              </tr>
        </table>
		  <table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4">
          <tr class="tblTitulo">
          	<td width="196" align="center">Codigo</td>
          	<td width="402"  align="center">Descripci&oacute;n</td>
          	<td width="92"  align="center">Sigla</td>
          </tr>
        <pg:pager
         items="<%= actividades.size()%>"
         index="<%= index %>"
         maxPageItems="<%= maxPageItems %>"
        maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
        <%-- keep track of preference --%>
<%
      for (int i = offset.intValue(), l = Math.min(i + maxPageItems, actividades.size()); i < l; i++)
	  {
          actividad = (TBLActividades) actividades.get(i);
		  String link = CONTROLLER + "?estado=TBLActividad&accion=Insert&flag=mostrar&cod="+actividad.getCodigo();	
          %>
        <pg:item>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand"
        onClick="window.open('<%= link %>','','status=yes,scrollbars=yes,width=650,height=450,resizable=yes');" >
          <td width="196" class="bordereporte" ><%= actividad.getCodigo() %></td>
          <td width="402" class="bordereporte" ><%= actividad.getDescripcion() %></td>
          <td width="92" class="bordereporte" ><%= actividad.getSigla() %></td>
        </tr>
        </pg:item>
        <%}%>
        <tr  bordercolor="#E6E6E6" class="bordereporte">
          <td td height="20" colspan="6" nowrap align="center">
		   <pg:index>
            <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
           </pg:index> 
	      </td>
        </tr>
        </pg:pager>
      </table></td>
    </tr>
</table>
<p>
      <%}
 else { %>
</p>
  <table width="650" border="2" align="center">
    <tr>
      <td><table width="100%" height="35" border="0" align="center" cellpadding="0" cellspacing="0"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="253" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="49" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="336">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
</table>
  <p>&nbsp; </p>
<%}%>
<br>
<table width="650" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr align="left">
    <td><img src="<%=BASEURL%>/images/botones/salir.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="parent.close();">
   </tr>
</table>
</body>
</html>
