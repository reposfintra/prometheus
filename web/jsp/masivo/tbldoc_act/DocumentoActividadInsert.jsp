<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %><html>
<head>
<title>Documento sin t&iacute;tulo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilotsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilotsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<%
        String distrito = (String) session.getAttribute("Distrito");
        String link = CONTROLLER + "?estado=DocumentoActividad&accion=Actualizar&cmd=show";	
        String msj = request.getParameter("mensaje");
        if ( msj==null ) msj = "";
%>
<script>
        <%= model.documentoSvc.obtenerJSCampos(distrito)%>
        <%= model.tLBActividadesServices.obtenerJSCampos(distrito)%>
        <%= model.actividad_documentoSvc.getVarCamposJS() %>
        
        var SeparadorJS = '<%= model.documentoSvc.getVarSeparadorJS()%>';
        var url = '<%= link%>';
        
        function actividadesDoc(doc){
                form.action = url;
                form.doc_selec.value = doc;
                form.submit();
        }

        function loadCombo(datos, cmb){
            cmb.length = 0;
            for (i=0;i<datos.length;i++){
                var dat = datos[i].split(SeparadorJS);
                addOption(cmb, dat[0], dat[1]);
            }
            order(cmb);
        }
        
        function unloadCombo(datos, cmb){
            for(j=0; j<cmb.length; j++){
                for (i=0;i<datos.length;i++){
                        var dat = datos[i].split(SeparadorJS);
                        if( dat[0] == cmb[j].value ){
                                cmb.remove(j);
                        }
                        //addOption(cmb, dat[0], dat[1]);
                }
            }
            order(cmb);
        }

        function addOption(Comb,valor,texto){
           if(valor!='' && texto!=''){
                var Ele = document.createElement("OPTION");
                Ele.value=valor;
                Ele.text=texto;
                Comb.add(Ele);
          }
        }

        function deleteRepeat(cmb){
          var ant='';
          for (i=0;i<cmb.length;i++){
             if(ant== cmb[i].value)
                cmb.remove(i);
             ant = cmb[i].value;
          }
        }

        function order(cmb){
           for (i=0; i<cmb.length;i++)
             for (j=i+1; j<cmb.length;j++)
                if (cmb[i].text > cmb[j].text){
                    var temp = document.createElement("OPTION");
                    temp.value = cmb[i].value;
                    temp.text  = cmb[i].text;
                    cmb[i].value = cmb[j].value;
                    cmb[i].text  = cmb[j].text;
                    cmb[j].value = temp.value;
                    cmb[j].text  = temp.text;
                }
        }

        function move(cmbO, cmbD){
           for (i=cmbO.length-1; i>=0 ;i--)
            if (cmbO[i].selected){
               addOption(cmbD, cmbO[i].value, cmbO[i].text)
               cmbO.remove(i);
            }
           //order(cmbD);
           deleteRepeat(cmbD);
        }
        
        function move2(cmbO, cmbD){
           for (i=cmbO.length-1; i>=0 ;i--)
            if (cmbO[i].selected){
               addOption(cmbD, cmbO[i].value, cmbO[i].text)
               cmbO.remove(i);
            }
           order(cmbD);
           deleteRepeat(cmbD);
        }
        
        function moveDoc(cmbO, cmbD){
           for (i=cmbO.length-1; i>=0 ;i--)
            if (cmbO[i].selected){
               actividadesDoc(cmbO[i].value);
               //addOption(cmbD, cmbO[i].value, cmbO[i].text)               
            }
            
           //order(cmbD);
           deleteRepeat(cmbD);
        }

        function moveAll(cmbO, cmbD){
           for (i=cmbO.length-1; i>=0 ;i--){
             if(cmbO[i].value!='' && cmbO[i].text!=''){
               addOption(cmbD, cmbO[i].value, cmbO[i].text)
               cmbO.remove(i);
             }
           }
           order(cmbD);
           deleteRepeat(cmbD);
        }
        
        function selectAll(cmb){
            for (i=0;i<cmb.length;i++)
              cmb[i].selected = true;
        }
        
        function validar(){
              if( form.c_docSelec.length==0)
                alert('Debe seleccionar un documento!')
              else
                form.submit();
        }
        
</script>
</head>

<body onload="loadCombo(CamposJS, form.c_docs);loadCombo(CamposJS2, form.c_acts);
        <% if ( msj.equalsIgnoreCase("MsgUpdate") ){ %> 
        loadCombo(CamposJSACT, form.c_actSelec); loadCombo(CamposJSDoc, form.c_docSelec); 
        unloadCombo(CamposJSDoc, form.c_docs); unloadCombo(CamposJSACT, form.c_acts);
<%}%>">
<form name="form" method="post" action="<%= CONTROLLER%>?estado=DocumentoActividad&accion=Insert&cmd=show" id="form">
  <table width="841" border="2" align="center">
    <tr>
      <td width="829"><table width="100%" border="0" align="center">
        <tr valign="top">
          <td width="187" valign="middle" class="subtitulo1" align="center">Documentos</td>
          <td width="34"><input name="doc_selec" type="hidden" id="doc_selec"></td>
          <td width="159" class="subtitulo1" align="center">Documentos<br>seleccionados</td>
          <td width="18">&nbsp;</td>
          <td width="162"class="subtitulo1" align="center">Actividades<br>seleccionadas</td>
          <td width="36">&nbsp;</td>
          <td width="182" valign="middle" class="subtitulo1" align="center">Actividades</td>
        </tr>
        <tr>
          <td><select name="c_docs"  size="15" style='width:100%' class="textreadonly" id="c_docs">
                    </select></td>
          <td align="center">              <img src="<%=BASEURL%>/images/botones/envDer.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="if ( c_docSelec.length==0 ) moveDoc   (c_docs,  c_docSelec ); " style="cursor:hand "><br>
              <img src="<%=BASEURL%>/images/botones/envIzq.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="move2   (c_docSelec, c_docs ); loadCombo(CamposJS2, form.c_acts); c_actSelec.length = 0;  " style="cursor:hand "><br>            
              <br>
            </td><td><select name="c_docSelec" size="15" multiple class="textobligatorio" id="c_docSelec" style='width:100%'>
          </select></td>
          <td>&nbsp;</td>
          <td><select name="c_actSelec" size="15" multiple class="textobligatorio" id="c_actSelec" style='width:100%'>
          </select></td>
          <td align="center">            <div align="center"> <img src="<%=BASEURL%>/images/botones/envIzq.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="move   (c_acts, c_actSelec );" style="cursor:hand "><br>
                <img src="<%=BASEURL%>/images/botones/enviarIzq.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="moveAll(c_acts, c_actSelec ) ; " style="cursor:hand "> <br>
                <img src="<%=BASEURL%>/images/botones/envDer.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="move2   (c_actSelec,  c_acts ); " style="cursor:hand "><br>
                <img src="<%=BASEURL%>/images/botones/enviarDerecha.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="moveAll(c_actSelec,  c_acts ); " style="cursor:hand "><br>
</div>
            </td><td><select name="c_acts" size="15" multiple class="textreadonly" id="c_acts" style='width:100%'>
          </select></td>
        </tr>
      </table>      </td>
    </tr>
  </table>
  <div align="center"><br>
    <img src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="selectAll(form.c_docSelec);selectAll(form.c_actSelec);validar();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">&nbsp; <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);"  style="cursor:hand "></div>
</form>
<%if(request.getParameter("msg")!=null){%>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes"><%=request.getParameter("msg").toString() %></td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>	
<%}%>

</body>
</html>
