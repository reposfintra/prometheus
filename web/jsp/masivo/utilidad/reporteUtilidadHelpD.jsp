<!--
     - Author(s)       :      MARIO FONTALVO
     - Date            :      25/01/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--
     - @(#)
     - Description: Repoortes
--%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
    <title>GENERACION REPORTE UTILIDAD</title>
    <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script src ="<%= BASEURL %>/js/boton.js"></script>
</head>
<body>
<BR>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">GENERACION REPORTE UTILIDAD</div></td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2"> PARAMETROS GENERACION REPORTE UTILIDAD</td>
        </tr>
        <tr class='fila'>
          <td colspan='2' > A trav�s de este formulario se define el periodo al cual se le generar� el reporte de utilidad.</td>
        </tr>
        <tr>
          <td  class="fila" align='center'> Periodo </td>
          <td  class="ayudaHtmlTexto">A�o y mes que se le generar� el reporte.</td>
        </tr>
        <tr>
          <td  class="fila" align='center'> <img src='<%= BASEURL %>/images/botones/aceptar.gif'>  </td>
          <td  class="ayudaHtmlTexto">Boton de Inicio del proceso</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<br>
<center> <img  name="imgSalir" src="<%=BASEURL%>/images/botones/salir.gif"       onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" window.close(); ">
</center>
</body>
</html>
