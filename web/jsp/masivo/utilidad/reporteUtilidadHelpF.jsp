<!--
     - Author(s)       :      MARIO FONTALVO
     - Date            :      25/01/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--
     - @(#)  
     - Description: Reportes Viajes Presupuestado vs. Ejecutado
--%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
    <title>GENERACION REPORTE UTILIDAD</title>
    <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script src ="<%= BASEURL %>/js/boton.js"></script>
</head>
<body>
<BR>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td width="674" height="24"><div align="center">GENERACION REPORTE UTILIDAD</div></td>
        </tr>
		<tr class="subtitulo1">
          <td> PARAMETROS DE GENERACION REPORTE UTILIDAD</td>
        </tr>

		<tr class="ayudaHtmlTexto">
		<td>
                     Formulario de incio de proceso de generacion de Reporte de Utilidad para un periodo dado.   <br><br>
		    <div align="center"><img src="<%= BASEURL %>/images/ayuda/reporteUtilidad/dibujo01.JPG" align="absmiddle"></div>
                    <br>
                    En este reporte se describe la distribución  de utilidad de las planillas para una remesa determinada.

		  </td>
		</tr>
      </table>
    </td>
  </tr>
</table>
<br>
<center> <img  name="imgSalir" src="<%=BASEURL%>/images/botones/salir.gif"       onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" window.close(); ">
</center>
</body>
</html>
