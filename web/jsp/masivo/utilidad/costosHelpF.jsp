<!--
     - Author(s)       :      MARIO FONTALVO
     - Date            :      25/01/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--
     - @(#)  
     - Description: Reportes Viajes Presupuestado vs. Ejecutado
--%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
    <title>MANTENIMIENTO DE CONCEPTOS</title>
    <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script src ="<%= BASEURL %>/js/boton.js"></script>	
</head>
<body>
<BR>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td width="674" height="24"><div align="center">LISTADO MANTENIMIENTO DE CONCEPTOS.</div></td>
        </tr>
		<tr class="subtitulo1">
          <td> PARAMETROS DE MANTENIMIENTO DE CONCEPTOS</td>
        </tr>
		<tr class="ayudaHtmlTexto">
		<td>Consulta de costos del estandar<br>
		    <div align="center"><img src="<%= BASEURL %>/images/ayuda/reporteUtilidad/dibujo02.JPG" align="absmiddle"></div>
                    <br>

                    PRIMER LISTADO: Lista de costos del estandar.
		    <div align="center"><img src="<%= BASEURL %>/images/ayuda/reporteUtilidad/dibujo03.JPG" align="absmiddle"></div>
        	   <br>
                    SEGUNDO LISTADO: Lista de indices iniciales.
                    <br>
		    <div align="center"><img src="<%= BASEURL %>/images/ayuda/reporteUtilidad/dibujo04.JPG" align="absmiddle"></div>
        	    <br>
                    TERCER LISTADO : Lista de posibles rutas generadas apartir del origen selccionado hasta el destino del estandar.
                    <br>
		    <div align="center"><img src="<%= BASEURL %>/images/ayuda/reporteUtilidad/dibujo05.JPG" align="absmiddle"></div>
		  </td>
		</tr>
      </table>
    </td>
  </tr>
</table>
<br>
<center> <img  name="imgSalir" src="<%=BASEURL%>/images/botones/salir.gif"       onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" window.close(); ">
</center>
</body>
</html>
