<!--
     - Author(s)       :      MARIO FONTALVO
     - Date            :      25/01/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   
     - @(#)  
     - Description: Importacion Directa de un archivo
--%> 

<%@page session  = "true"%>
<%@page errorPage= "/error/ErrorPage.jsp"%>
<%@include file  = "/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.util.Util" %>
<%@page import="java.util.*" %>
<%@page import="com.tsp.operation.model.beans.*" %>
<%
    String msg      = (String) request.getAttribute("msg");
    String distrito = (String) session.getAttribute("Distrito");  
    String planilla = (request.getParameter("Planilla")!=null?request.getParameter("Planilla"):"");
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>    
<html>
<head>
    <title>UTILIDAD PLANILLA</title>
    <link   href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet">
    <script src ="<%= BASEURL %>/js/boton.js"></script>	
    <script src ="<%= BASEURL %>/js/reporte.js"></script>	
     <script>
       function goFormulario (){
          var key =  window.event.keyCode
          if (key == 13){
             if (forma.Planilla.value!='') { forma.submit(); } else { alert ('Indique el numero de la planilla, para poder continuar'); } 
          }
       }     
       
       function openWindow (url){
           var x = open (url, 'Costos', 'resizable, status=yes, menubars=yes');
           x.focus();
       }        
    </script>
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=INGRESOS Y COSTOS PRORRATEADOS"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">


        <center>
	<br>
	<form action="<%= CONTROLLER %>?estado=Planilla&accion=Costos" method="post" name="forma">
	<table width="300" border="2" align="center">
	<tr>
		<td align="center">
		
		<!-- cabecera de la seccion -->
		<table width="100%" >
			<tr>
			<td class="subtitulo1"  width="50%">Consulta Planilla</td>
			<td class="barratitulo" width="50%">
			  <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"  align='left'><%=datos[0]%>
			</td>
			</tr>
		</table>
		<!-- fin cabecera -->

		<table  width="100%" >	
			<tr class="fila">
			    <td width='30%' >Distrito</td>
			    <td width='70%' ><input type='text' name='Distrito' style='width:30%' readonly value=<%= distrito %>></td>
			</tr>
			<tr class="fila">
			    <td >Planilla</td>
			    <td ><input type='text' name='Planilla' style='width:100%' onkeypress='goFormulario();' value='<%= planilla %>' ></td>
			</tr>			
		 </table>
        </td>         
      </tr>
      </table>
      <br>
      <input type="hidden" value="Consultar" name="Opcion" >
      <img name="imgProcesar" src="<%=BASEURL%>/images/botones/buscar.gif"   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" if (forma.Planilla.value!='') { forma.submit(); } else { alert ('Indique el numero de la planilla, para poder continuar'); } ">
      &nbsp;
      <img name="imgcancelar" src="<%=BASEURL%>/images/botones/cancelar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" forma.Opcion.value='Resetear'; forma.submit(); ">
      &nbsp;
      <img name="imgSalir" src="<%=BASEURL%>/images/botones/salir.gif"       onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" window.close(); ">
      </form>
      <script>document.forma.Planilla.focus();</script>
        


<% if (msg!=null && !msg.equals("")) { %>
        <table border="2" align="center">
           <tr>
           <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                <tr>
                 <td width="320" align="center" class="mensajes"><%=msg%></td>
                 <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                 <td width="58">&nbsp;</td>
                </tr>
              </table>
           </td>
           </tr>
        </table>  
<% } %>      

    
    <% Vector lista = model.PlanillaCostosSvc.getListaRemesas();
       if (lista!=null && !lista.isEmpty()) { %>
        <table width="1000" border="2" align="center">
        <tr>
            <td align="center"  >

            <!-- cabecera de la seccion -->
            <table width="100%" >
                    <tr>
                    <td class="subtitulo1"  width="50%">INGRESOS Y COSTOS PRORRATEADOS</td>
                    <td class="barratitulo" width="50%">         
                        <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="18" >
                    </td>
                    </tr>
            </table>       
            
                       
            <%  for (int i=0; lista!=null && i<lista.size(); i++) { 
                Remesa r = (Remesa) lista.get(i); 
                   if (r.getPlanillas()!=null && !r.getPlanillas().isEmpty()) {
            %>
            <table width='100%' border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
            <tr>
               <td class='bordereporte'> 
               
                   <div style='overflow:scroll; width:100%'>
                   <table width='130%' >
                   <tr class='subtitulo1'>
                     <td colspan='10'>REMESA &nbsp;&nbsp; [<%= r.getNumrem()%>]&nbsp; <%= r.getDescripcion() %></td>
                   </tr>
                   <tr class='tblTitulo'>
                        <td class='bordereporte' align='center' width='11%'>Vlr Remesa</td>
                        <td class='bordereporte' align='center' width='11%'>Acum. PLAN</td>
                        <td class='bordereporte' align='center' width='10%'>Cliente</td>
                        <td class='bordereporte' align='center' width='10%'>Estandar</td>
                        <td class='bordereporte' align='center' width='10%'>Ruta</td>
                        <td class='bordereporte' align='center' width='10%'>Fecha</td>
                        <td class='bordereporte' align='center' width='10%'>Tipo</td>
                        <td class='bordereporte' align='center' width='10%'>Vlr Remesa ME</td>
                        <td class='bordereporte' align='center' width='7%'>Moneda</td>
                        <td class='bordereporte' align='center' width='9%'>Tasa</td>                        
                    </tr>
                    <tr class='filagris'> 
                        <td class='bordereporte' align='right' ><%= Util.customFormat(r.getValorRemesa() * r.getTasa())%>&nbsp;</td>
                        <td class='bordereporte' align='right' ><%= Util.customFormat(r.getValorTotalPlanillas()) %>&nbsp;</td>
                        <td class='bordereporte' align='center'><%= r.getCliente()                                %>&nbsp;</td>
                        <td class='bordereporte' align='center'><%= r.getStd_job_no()                             %>&nbsp;</td>
                        <td class='bordereporte' align='center'><%= r.getOrirem() + " - " + r.getDesrem()         %>&nbsp;</td>
                        <td class='bordereporte' align='center'><%= com.tsp.util.Util.dateFormat( r.getFecRem() ) %>&nbsp;</td>
                        <td class='bordereporte' align='center'><%= r.getTipoViaje()                              %>&nbsp;</td>
                        <td class='bordereporte' align='right' ><%= Util.customFormat(r.getValorRemesa())         %>&nbsp;</td>
                        <td class='bordereporte' align='center'><%= r.getCurrency()                               %>&nbsp;</td>
                        <td class='bordereporte' align='right' ><%= r.getTasa()                                   %>&nbsp;</td>                        
                   </tr>
                   </table>
                   
                   
                   <table width='130%' >
                   <tr class='tblTitulo'>
                        <td class='bordereporte' align='center' width='5%'>Planilla</td>
                        <td class='bordereporte' align='center' width='7%'>Fecha</td>
                        <td class='bordereporte' align='center' width='5%'>Ruta</td>                          
                        <td class='bordereporte' align='center' width='8%'>Valor Planilla</td>
                        <td class='bordereporte' align='center' width='8%' title='Porcentaje Prorrateo Planilla'>%PP</td>
                        <td class='bordereporte' align='center' width='8%' title='Valor Prorrateo Planilla'>VPP</td>
                        <td class='bordereporte' align='center' width='8%' title='Porcentaje Prorrateo Remesa'>%PR</td>
                        <td class='bordereporte' align='center' width='8%' title='Valor Prorrateo Remesa'>VPR</td>
                        <td class='bordereporte' align='center' width='8%'>Utilidad</td>
                        <td class='bordereporte' align='center' width='7%'>Tv</td>                          
                        <td class='bordereporte' align='center' width='7%'>Placa</td>                        
                        <td class='bordereporte' align='center' width='8%'>Vlr Planilla ME</td>
                        <td class='bordereporte' align='center' width='6%'>Mon</td>
                        <td class='bordereporte' align='center' width='8%'>Tasa</td>
                        
                   </tr>    
                   
                   
                   <%  Vector listap = r.getPlanillas();
                       for (int j = 0; listap!=null && j < listap.size();  j++){
                           Planilla p = (Planilla) listap.get(j); 
                           String url = "", imagen = "";
                           if (p.getNumpla().equals("ESTIMADA")  ){
                               url = "onclick=\"openWindow('" + CONTROLLER + "?estado=SJ&accion=Costos&Opcion=Consultar&Distrito="+ distrito + "&Estandar=" + r.getStd_job_no() +"&Origen=" + p.getOripla()+"');\" style='cursor=hand' ";
                               url+= "onmousemove=\"this.className='filaseleccion'\" onmouseout=\"this.className='"+ (j%2==0?"filagris":"filaazul") +"' \" title='haga click aqui para para consultar datos del estandar' ";
                               imagen = "<img src='"+ BASEURL +"/images/roja.JPG' width='8'>&nbsp;";
                           }
                   %>
                   <tr class='<%= (j%2==0?"filagris":"filaazul") %>' <%= url %> >
                        <td class='bordereporte' align='center'><%= imagen + p.getNumpla()                              %></td>
                        <td class='bordereporte' align='center'><%= com.tsp.util.Util.dateFormat( p.getFechadespacho()) %></td>
                        <td class='bordereporte' align='center'><%= p.getOripla() + " - "  + p.getDespla()        %></td>
                        <td class='bordereporte' align='right' ><%= Util.customFormat(p.getVlrPlanilla() * p.getTasa()) %>&nbsp;</td>
                        <td class='bordereporte' align='right' ><%= Util.customFormat(p.getPorcentaje())          %>&nbsp;</td>
                        <td class='bordereporte' align='right' ><%= Util.customFormat(p.getVlrParcial())          %>&nbsp;</td>
                        <td class='bordereporte' align='right' ><%= Util.customFormat(p.getPorcentajeProrrateo()) %>&nbsp;</td>
                        <td class='bordereporte' align='right' ><%= Util.customFormat(p.getVlrProrrateado())      %>&nbsp;</td>
                        <td class='bordereporte' align='right' ><%= ( !p.getTipoviaje().equals("VAC")?Util.customFormat( p.getVlrProrrateado() - p.getVlrParcial() ):"")      %>&nbsp;</td>
                        <td class='bordereporte' align='center'><%= p.getTipoviaje()                              %>&nbsp;</td>
                        <td class='bordereporte' align='center'><%= p.getPlaveh()                                 %>&nbsp;</td>
                        <td class='bordereporte' align='right' ><%= Util.customFormat(p.getVlrPlanilla())         %>&nbsp;</td>
                        <td class='bordereporte' align='center'><%= p.getCurrency()                               %>&nbsp;</td>
                        <td class='bordereporte' align='right' ><%= p.getTasa()                                   %>&nbsp;</td>
                        
                   </tr>
                   <% } %>
                   </table>
                   <div>
            </td>
            </tr>
            </table>                   
            <% } } %>
            
        </td>
        </tr>
        </table>              
    <% } %>                  
</center>
</div>
<%=datos[1]%>
</body>
</html>
<script>
   forma.Planilla.focus();
</script>
