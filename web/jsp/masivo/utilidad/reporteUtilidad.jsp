<!--
     - Author(s)       :      MARIO FONTALVO
     - Date            :      25/01/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   
     - @(#)  
     - Description: Importacion Directa de un archivo
--%> 

<%@page session  = "true"%>
<%@page errorPage= "/error/ErrorPage.jsp"%>
<%@include file  = "/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.util.Util" %>
<%@page import="java.util.*" %>
<%@page import="com.tsp.operation.model.beans.*" %>
<%  
    String msg      = (String) request.getAttribute("msg");
    String distrito = (String) session.getAttribute("Distrito");  
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
%>
<html>
<head>
    <title>REPORTE UTILIDAD</title>
    <link   href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet">
    <script src ="<%= BASEURL %>/js/boton.js"></script>	
    <script src ="<%= BASEURL %>/js/reporte.js"></script>	
    <script>
    
    
        function addOption(Comb,valor,texto){
                var Ele = document.createElement("OPTION");
                Ele.value=valor;
                Ele.text=texto;
                Comb.add(Ele);
        }    
 
        function Llenar(CmbAnno, CmbMes){
            var Meses    = new Array('Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
            var FechaAct = new Date();
            CmbAnno.length = 0;
            CmbMes.length  = 0;
            for (i=FechaAct.getYear()-10;i<=FechaAct.getYear()+10;i++) addOption(CmbAnno,i,i)
            CmbAnno.value = FechaAct.getYear();
            for (i=0;i<Meses.length;i++)
                if ((i+1)<10) addOption(CmbMes,'0'+(i+1),Meses[i]);
                else          addOption(CmbMes,(i+1),Meses[i]);                
            CmbMes.value  = ((FechaAct.getMonth()+1)<10)?('0'+(FechaAct.getMonth()+1)):(FechaAct.getMonth()+1);                 
        }    
    </script>
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=REPORTE UTILIDAD"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">


        <center>
	<br>
	<form action="<%= CONTROLLER %>?estado=Reporte&accion=Utilidad" method="post" name="forma">
	<table width="400" border="2" align="center">
	<tr>
		<td align="center">
		
		<!-- cabecera de la seccion -->
		<table width="100%" >
			<tr>
			<td class="subtitulo1"  width="50%">REPORTE UTILIDAD</td>
			<td class="barratitulo" width="50%">
			  <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"  align='left'><%=datos[0]%>
			</td>
			</tr>
		</table>
		<!-- fin cabecera -->

		<table  width="100%" >	
			<tr class="fila">
			    <td width='30%' >Periodo</td>
			    <td width='70%'>
                                <select class="select" style="width:49%" name="ano"></select>
                                <select class="select" style="width:49%" name="mes"></select>			    
			    </td>
			</tr>			
		 </table>
        </td>         
      </tr>
      </table>
      <br>
      <input type="hidden" value="Generacion" name="opcion" >
      <img name="imgProcesar" src="<%=BASEURL%>/images/botones/aceptar.gif"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" forma.submit(); ">
      &nbsp;
      <img name="imgSalir" src="<%=BASEURL%>/images/botones/salir.gif"       onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" window.close(); ">
      </form>
        


<% if (msg!=null && !msg.equals("")) { %>
        <table border="2" align="center">
           <tr>
           <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                <tr>
                 <td width="320" align="center" class="mensajes"><%=msg%></td>
                 <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                 <td width="58">&nbsp;</td>
                </tr>
              </table>
           </td>
           </tr>
        </table>  
<% } %>      
      
    </center>
</div>
<%=datos[1]%>
</body>
</html>

<script>
Llenar(forma.ano,forma.mes);
</script>
