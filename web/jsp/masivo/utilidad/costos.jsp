<!--
     - Author(s)       :      MARIO FONTALVO
     - Date            :      25/01/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   
     - @(#)  
     - Description: Importacion Directa de un archivo
--%> 

<%@page session  = "true"%>
<%@page errorPage= "/error/ErrorPage.jsp"%>
<%@include file  = "/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.util.Util" %>
<%@page import="java.util.*" %>
<%@page import="com.tsp.operation.model.beans.*" %>
<%  String Estandar = (String) (request.getParameter("Estandar")!=null?request.getParameter("Estandar"):"");
    String msg      = (String) request.getAttribute("msg");
    String distrito = (String) session.getAttribute("Distrito");  
    StdJob sj       = model.SJCostoSvc.getEstandar();
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>    
<html>
<head>
    <title>COSTO ESTIMADO DE RUTA</title>
    <link   href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet">
    <script src ="<%= BASEURL %>/js/boton.js"></script>	
    <script src ="<%= BASEURL %>/js/reporte.js"></script>	
    <script>
       function goFormulario (){
          var key =  window.event.keyCode
          if (key == 13){
             if (fimp.Estandar.value!='') { fimp.submit(); } else { alert ('Indique el estandar, para poder continuar'); } 
          }
       }
       function goLink (distrito, estandar, origen){
           var url = '<%= CONTROLLER %>?estado=SJ&accion=Costos&Opcion=Consultar&Distrito='+ distrito +'&Estandar='+ estandar +'&Origen=' + origen ;
           window.location.href = url;
       }
       function nuevoOrigen (origen){
           var url = '<%= CONTROLLER %>?estado=SJ&accion=Costos&Opcion=NuevoOrigen&Origen=' + origen ;
           window.location.href = url;
       }       
       
    </script>
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Costo Estimado de Ruta"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">


        <center>
	<br>
	<form action="<%= CONTROLLER %>?estado=SJ&accion=Costos" method="post" name="fimp">
	<table width="300" border="2" align="center">
	<tr>
		<td align="center">
		
		<!-- cabecera de la seccion -->
		<table width="100%" >
			<tr>
			<td class="subtitulo1"  width="50%">Consulta Estandar</td>
			<td class="barratitulo" width="50%">
			  <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"  align='left'><%=datos[0]%>
			</td>
			</tr>
		</table>
		<!-- fin cabecera -->

		<table  width="100%" >	
			<tr class="fila">
			    <td width='30%' >Distrito</td>
			    <td width='70%' ><input type='text' name='Distrito' style='width:30%' readonly value=<%= distrito %>></td>
			</tr>
			<tr class="fila">
			    <td >Estandar</td>
			    <td ><input type='text' name='Estandar' style='width:100%' onkeypress='goFormulario();' value='<%= Estandar %>' ></td>
			</tr>			
		 </table>
        </td>         
      </tr>
      </table>
      <br>
      <input type="hidden" value="Consultar" name="Opcion" >
      <img name="imgProcesar" src="<%=BASEURL%>/images/botones/buscar.gif"   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" if (fimp.Estandar.value!='') { fimp.submit(); } else { alert ('Indique el estandar, para poder continuar'); } ">
      &nbsp;
      
      <% if (sj!=null) { %>
      <img name="imgcancelar" src="<%=BASEURL%>/images/botones/cancelar.gif" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" fimp.Opcion.value='Resetear'; fimp.submit(); ">
      &nbsp;
      <% } %>
      <img name="imgSalir" src="<%=BASEURL%>/images/botones/salir.gif"       onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" window.close(); ">
      </form>
      <script>document.fimp.Estandar.focus();</script>
        


<% if (msg!=null && !msg.equals("")) { %>
        <table border="2" align="center">
           <tr>
           <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                <tr>
                 <td width="320" align="center" class="mensajes"><%=msg%></td>
                 <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                 <td width="58">&nbsp;</td>
                </tr>
              </table>
           </td>
           </tr>
        </table>  
<% } %>      

    
    <% Vector lista = model.SJCostoSvc.getListado();
       if (lista!=null && !lista.isEmpty()) { %>
        <table width="980" border="2" align="center">
        <tr>
            <td align="center">

            <!-- cabecera de la seccion -->
            <table width="100%" >
                    <tr>
                    <td class="subtitulo1"  width="50%">Listado para el Estandar : <%= sj.getstd_job_no() %></td>
                    <td class="barratitulo" width="50%">
                      <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" >
                    </td>
                    </tr>
            </table>       
            <table width='100%'>
                   <tr class='tblTitulo'>
                        <td >Indice</td>
                        <td >Origen</td>
                        <td >Destino</td>
                        <td width='40' align='center' >IS</td>
                        <td >&nbsp;Tipo&nbsp;</td>
                        <td >Valor Costo</td>
                        <td >Moneda</td>
                        <td >Tasa</td>
                        <td >Cambio</td>
                        <td >AgRelDes</td>
                        <td >Codigo +CF</td>
                        <td >Codigo +FT</td>
                        <td >Fecha Creacion</td>
                   </tr>    
            <% for (int i=0; lista!=null && i<lista.size(); i++){
                   SJCosto sjc = (SJCosto) lista.get(i); 
                   %>
                   <tr class='<%= (i%2==0?"filagris":"filaazul") %>'>
                        <td class='bordereporte' align='center'><%= i %></td>
                        <td class='bordereporte' align='center'><%= sjc.getCodOrigen()  %></td>
                        <td class='bordereporte' align='center'><%= sjc.getCodDestino() %></td>
                        <td class='bordereporte' align='center'><% if (sjc.getIndiceSig()!=-1) out.print(sjc.getIndiceSig()); else out.print("&nbsp;");  %></td>
                        <td class='bordereporte' align='center'><%= sjc.getTipoCosto()  %></td>
                        <td class='bordereporte' align='right' ><%= Util.customFormat(sjc.getValorCosto()) %></td>
                        <td class='bordereporte' align='center'><%= sjc.getMoneda()     %></td>
                        <td class='bordereporte' align='right' ><%= sjc.getTasa() %></td>
                        <td class='bordereporte' align='right' ><%= Util.customFormat(sjc.getTasa() * sjc.getValorCosto()) %></td>
                        <td class='bordereporte' align='center'><%= sjc.getCodAgenciaRelDes() %></td>
                        <td class='bordereporte' align='center'><%= sjc.getCodMasFT()   %></td>
                        <td class='bordereporte' align='center'><%= sjc.getCodMasCF()   %></td>
                        <td class='bordereporte' align='center'><%= Util.dateTimeFormat(sjc.getFechaCreacion())   %></td>
                   </tr>

            <% } %>
            </table>
            </td>
            </tr>
            </table>
            
            
            
            
            <!-- INDICES INICIALES -->
            <% TreeMap tii = model.SJCostoSvc.getIndicesIniciales(); 
               if (tii!=null && !tii.isEmpty()){ %>
               <br>
            <table width="300" border="2" align="center">
            <tr>
                <td align="center">

                <!-- cabecera de la seccion -->
                <table width="100%" >
                        <tr>
                        <td class="subtitulo1"  width="50%">INDICES INICIALES</td>
                        <td class="barratitulo" width="50%">
                          <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" >
                        </td>
                        </tr>
                </table>                
                <table width='100%'>
                    <tr class='tblTitulo'>
                        <td width='50%' align='center' >ORIGEN</td>
                        <td width='50%' align='center' >INDICE</td>
                   </tr>                 
            <% Iterator it = tii.keySet().iterator();
               int i = 0;
               while ( it.hasNext() ){
                  String key   = (String) it.next();
                  String value = (String) tii.get(key);%>
                    <tr class='<%= (i++%2==0?"filagris":"filaazul") %>' onMouseOver='cambiarColorMouse(this)' onclick=" nuevoOrigen('<%= key %>');" style='cursor:hand;' title='Haga click para seleccionar el origen'>
                        <td class='bordereporte' align='center'><%= key   %></td>
                        <td class='bordereporte' align='center'><%= value %></td>
                    </tr>    
            <% } %>
                </table>
                </td>
            </tr>
            </table>
            <% } %>
            
            
            
            
            
            
            <!-- RUTAS -->
            <% Vector lrutas = model.SJCostoSvc.getListarutas();
               if (lrutas!=null && !lrutas.isEmpty()){ %>
               
               <br>
            <table width="700" border="2" align="center">
            <tr>
                <td align="center">

                <table width="100%" >
                        <tr>
                        <td class="subtitulo1"  width="50%">DATOS DEL ESTANDAR</td>
                        <td class="barratitulo" width="50%">
                          <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" >
                        </td>
                        </tr>
                </table>                
                <table width='100%'>
                    <tr class='fila'>
                        <td ><%= "["+ sj.getstd_job_no() + "] " + " " +sj.getorigin_code() + " - " + sj.getdestination_code() %></td>
                   </tr>                 
                </table>   
                
                
                <!-- cabecera de la seccion -->
                <table width="100%" >
                        <tr>
                        <td class="subtitulo1"  width="50%">RUTAS GENERADAS</td>
                        <td class="barratitulo" width="50%">
                          <img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" >
                        </td>
                        </tr>
                </table>                
                
                <table width='100%'>
                    <tr class='tblTitulo'>
                        <td width='5%'  align='center' >IND</td>
                        <td width='70%' align='center' >RUTAS</td>
                        <td width='15%' align='center' >VALOR</td>
                        <td width='10%' align='center' >ESTADO</td>
                   </tr>                 
            <% for(int i = 0; i < lrutas.size(); i++) {
                  SJRuta ruta = (SJRuta) lrutas.get(i);  %>
                    <tr class='<%= (i%2==0?"filagris":"filaazul") %>'>
                        <td class='bordereporte' align='center' >&nbsp;<%= (i+1) %></td>
                        <td class='bordereporte' >&nbsp;<%= ruta.getRuta() %></td>
                        <td class='bordereporte' align='right'  >&nbsp;<%= Util.customFormat(ruta.getCostoTotal()) %></td>
                        <td class='bordereporte' align='center' ><%= (ruta.isExitoso()?"<font color='red'>exitoso</font>":"&nbsp;") %></td>
                    </tr>    
            <% } %>
                </table>
                </td>
            </tr>
            </table>
            <% } %>
            
    <% } %>            
      
</center>
</div>
<%=datos[1]%>
</body>
</html>
