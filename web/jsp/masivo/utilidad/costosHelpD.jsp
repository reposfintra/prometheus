<!--
     - Author(s)       :      MARIO FONTALVO
     - Date            :      25/01/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--
     - @(#)
     - Description: Repoortes
--%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>COSTO ESTIMADO DE RUTA</title>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script src ="<%= BASEURL %>/js/boton.js"></script>	
</head>
<body>
<BR>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">COSTO ESTIMADO DE RUTA</div></td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2"> PARAMETROS DE COSTO ESTIMADO DE RUTA</td>
        </tr>
        <tr>
          <td  class="fila" align='center'> Distrito </td>
          <td  class="ayudaHtmlTexto">Distrito del estandar que desea consultar, por default aparece el distrito del usuario en session.</td>
        </tr>
        <tr>
          <td  class="fila" align='center'> Estandar  </td>
          <td  class="ayudaHtmlTexto">Estandar que desea consultar</td>
        </tr>
        <tr>
          <td class="fila" align='center'> <img src='<%= BASEURL %>/images/botones/buscar.gif'>   </td>
          <td class="ayudaHtmlTexto">Boton de que inicia el proceso de consulta de los costos del estandar</td>
        </tr>
        <tr>
          <td class="fila" align='center'> <img src='<%= BASEURL %>/images/botones/cancelar.gif'>   </td>
          <td class="ayudaHtmlTexto">Boton de REINICIO de proceso.</td>
        </tr>


      </table>
    </td>
  </tr>
</table>
</body>
<br>
<center> <img  name="imgSalir" src="<%=BASEURL%>/images/botones/salir.gif"       onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" window.close(); ">
</center>
</html>
