<!--
     - Author(s)       :      MARIO FONTALVO
     - Date            :      25/01/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--
     - @(#)
     - Description: Repoortes
--%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>INGRESOS Y COSTOS PRORRATEADOS</title>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script src ="<%= BASEURL %>/js/boton.js"></script>
</head>
<body>
<BR>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">INGRESOS Y COSTOS PRORRATEADOS</div></td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2"> PARAMETROS DE INGRESOS Y COSTOS PRORRATEADOS</td>
        </tr>
        <tr class='fila'>
          <td colspan='2' > Utilidades de planillas vs. remesas</td>
        </tr>
        <tr>
          <td  class="fila" align='center'> Distrito  </td>
          <td  class="ayudaHtmlTexto">Distrito de la planilla</td>
        </tr>
        <tr>
          <td  class="fila" align='center'> Planilla  </td>
          <td  class="ayudaHtmlTexto">Numero de planilla a verificar</td>
        </tr>
        <tr>
          <td  class="fila" align='center'> <img src='<%= BASEURL %>/images/botones/buscar.gif'>  </td>
          <td  class="ayudaHtmlTexto">Boton de inicio de proceso de prorrateo</td>
        </tr>
        <tr>
          <td class="fila" align='center'> <img src='<%= BASEURL %>/images/botones/cancelar.gif'>   </td>
          <td class="ayudaHtmlTexto">Boton de Reinicio del formulario</td>
        </tr>


      </table>
    </td>
  </tr>
</table>
<br>
<center> <img  name="imgSalir" src="<%=BASEURL%>/images/botones/salir.gif"       onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" onClick=" window.close(); ">
</center>

</body>
</html>
