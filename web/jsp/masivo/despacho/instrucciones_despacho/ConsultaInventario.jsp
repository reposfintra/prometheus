<%-- Declaracion de librerias--%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
    <head>
        <title>Reporte Traking</title>        

<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script>
	
	function enviarForm(){
		formulario.submit();
	}
	
		</script>
        
    </head>
   <body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Informe Traking LG"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

        <form name='formulario' method='POST' id="formulario" action="<%=CONTROLLER%>?estado=Cargue&accion=LGManager&cmd=Inventario" onSubmit="return validar(this);">
            <table width="56%"  border="2" align="center">
              <tr>
                <td><table width="100%"  border="0" class="tablaInferior">
                  
				  <tr>
                   <td  colspan="4" ><table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="barratitulo">
                       <tr>
                         <td class="subtitulo1" colspan='3'>Reporte de Inventarios</td>
                         <td width="62%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif"></td>
                       </tr>
                     </table></td>
       			  </tr>
				  <tr class="fila">
				    <td width="23%" align="left">Orden de Compra </td>
			        <td width="31%" align="left"><input name="ocompra" type="text" id="ocompra" style="width:100% "></td>
				    <td width="16%" align="left">Factura</td>
				    <td width="30%" align="left"><input name="factura" type="text" id="factura2" style="width:100% "></td>
				  </tr>
				  <tr class="informacion">
				    <td colspan="4" align="left" class="informacion">*Si desea ver todo el inventario, no digite numero de orden de compra ni factura. </td>
			      </tr>
  
                </table></td>
              </tr>
          </table>
			<br>
            <table align="center">
              <tr>
                <td colspan="2" nowrap align="center">
<img name="Guardar" src="<%=BASEURL%>/images/botones/aceptar.gif"  style = "cursor:hand" align="middle" type="image" id="Guardar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="enviarForm();">                  
&nbsp;
                  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" align="absmiddle" style="cursor:hand" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
              </tr>
          </table>
            <br>
            <br>
            <p>
              <%if(request.getParameter("mensaje")!=null){%>
            </p>
            <table border="2" align="center">
              <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                      <td width="229" align="center" class="mensajes"><span class="normal"><%=request.getParameter("mensaje")%></span></td>
                      <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                      <td width="58">&nbsp;</td>
                    </tr>
                </table></td>
              </tr>
            </table>
            <%}%>
        </form > 
   </div>  
	<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    

</body>
</html>