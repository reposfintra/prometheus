<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Ingresar Informacion de Cargue</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 

<script>
	function copiarFecha(){
		var sw=true;
		
		for(i=0;i<=form2.length-1;i++){
			if(form2.elements[i].value==''){
				sw = false;
			}
			
		}
		if(sw==false){
			if(confirm('Se encontraron varias fechas sin digitar,\nel programa colocara automaticamente las fechas.\nDesea continuar?')){
//	alert("Copiar fecha");
	//se recorre el formulario para saber cuales son los campo de destinatario
				for(i=0;i<=form2.length-1;i++){
			
			//se identifica si el campo es de destinatario
					if(form2.elements[i].name.indexOf("dest_")>=0){
		//		alert("Encontre el destinatario");
			//se divide el nombre del campo para saber cual es el numero de la remesa
						var vec = form2.elements[i].name.split("_");
				//se obtiene el codigo del destinatario
						var dest = form2.elements[i].value;
//					alert("Destinatario "+dest);
		    	    	if(vec.length>1){
					//nombre de la remesa
							var remesa = vec[1];
					//fecha a copiar
							var fecha = document.getElementById("fecha"+dest+remesa).value;
					//se recorre nuevamente el formulario para saber donde debo copiar las fechas
							for(j=0;j<=form2.length-1;j++){
								if(form2.elements[j].name.indexOf("fecha"+dest)>=0){
									if(form2.elements[j].name!= "fecha"+dest+remesa){
										form2.elements[j].value = fecha;
									}
								}
							}
						}
					}		        
				}
		
		//LLENAMOS LAS VACIAS CON LA FECHA DE CARGUE.
				for(i=0;i<=form2.length-1;i++){
					if(form2.elements[i].name.indexOf("dest_")>=0){
						var vec = form2.elements[i].name.split("_");
				//se obtiene el codigo del destinatario
						var dest = form2.elements[i].value;
						if(vec.length>1){
					//nombre de la remesa
							var remesa = vec[1];
						//fecha a copiar
							var fecha = document.getElementById("fecha"+dest+remesa).value;
							if(fecha==''){
								document.getElementById("fecha"+dest+remesa).value=document.getElementById("fcargue"+dest+remesa).value;
					
							}
						}
					}
				}
				alert("Ahora a enviar el form");
				form2.submit();
			}
		}
		else{
			form2.submit();
		}
	}
	
	
</script> 


<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body onLoad="form1.planilla.focus();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=CARGUE"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
  

  <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
  <br>
  <form name="form1" method="post" action="<%=CONTROLLER%>?estado=CargueLG&accion=Manager&cmd=BuscarFCargue" >
   <table width="40%"  border="2" align="center">
    <tr>
      <td>

    <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="barratitulo">
      <tr>
        <td width="50%" class="subtitulo1" colspan='3'>Buscar Planilla </td>
        <td width="50%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif"></td>
      </tr>
    </table>
    <table width="100%" align="center">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr class="fila">
              <td width="179" >Numero </td>
              <td width="306"><input name="planilla" type="text" id="planilla">
                <input name="modificar" type="hidden" id="modificar">                </td>
            </tr>
        </table></td>
      </tr>
    </table>	</td>
      </tr>
    </table>
	<BR>
	<div align="center">
	  <img  style="cursor:hand " src="<%=BASEURL%>/images/botones/buscar.gif" align="middle" width="87" height="21" border="0" onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);" onClick="if(form1.planilla.value ==''){ alert('Escriba la planilla');}else{ form1.submit();this.disabled='true';}">
	</div>
  </form>
<form name="form2" method="post" action="<%=CONTROLLER%>?estado=CargueLG&accion=Manager&cmd=IngresarFCargue">
	  <%if(model.instrucciones_despachoService.getRemesas()!=null){
 %>

    
  <table width="95%"  border="2" align="center">
    <tr>
      <td>
	<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="barratitulo">
      <tr>
        <td class="subtitulo1"><table width="100%" border="0" bgcolor="#4D71B0" align="center">
          <tr class="subtitulo1">
            <td> Ingresar fecha real de cita de entrega</td>
          </tr>
        </table></td>
        </tr>
    </table>
    <table width="100%" border="1" align="center" bordercolor="#999999">
      <tr class="tblTitulo">
        <td width="78" nowrap><strong>PLANILLA</strong></td>
        <td width="99"><div align="center"><strong>REMESA</strong></div></td>
        <td width="232"><div align="center">DESCRIPCION</div></td>
        <td width="154" ><div align="center"> <strong>DESTINATARIO</strong></div></td>
        <td width="57" ><div align="center">M3</div></td>
        <td width="194" ><div align="center">ORDEN DE COMPRA </div></td>
        <td width="153" ><div align="center">FECHA CITA ENTREGA </div></td>
        <td width="172" ><div align="center">FECHA REAL DE CITA ENTREGA </div></td>
      </tr>
      <% Vector remesas = model.instrucciones_despachoService.getRemesas();
	  
	  	for(int i = 0; i<remesas.size(); i++){
	  		Remesa remesa= (Remesa) remesas.elementAt(i);
		%>
      <tr class="<%=i%2==0?"filagris":"filaazul"%>" title="" >
        <td class="bordereporte"> <%=remesa.getNumpla()%></td>
        <td class="bordereporte"> <%=remesa.getNumrem()%> </td>
        <td class="bordereporte"> <%=remesa.getDescripcion()%> </td>
        <td class="bordereporte"><%=remesa.getNom_destinatario()%>
          <input type="hidden" name="dest_<%=remesa.getNumrem()%>" value="<%=remesa.getDestinatario()%>"></td>
        <td class="bordereporte"><div align="center"><%=remesa.getPesoReal()%></div></td>
        <td class="bordereporte"><div align="center"><%=remesa.getDocumento()%>
          </div></td>
        <td class="bordereporte"><div align="center"><%=remesa.getFecha_cargue()%>
          <input type="hidden" name="fcargue<%=remesa.getDestinatario()%><%=remesa.getNumrem()%>" value="<%=remesa.getFecha_cargue()%>">
        </div></td>
        <td class="bordereporte"><div align="center">
            <input name="fecha<%=remesa.getDestinatario()%><%=remesa.getNumrem()%>" type="text" class="textbox" id="fecha<%=remesa.getDestinatario()%><%=remesa.getNumrem()%>"   readonly size="20" maxlength="16" value="<%=remesa.getFecha_realcargue().equals("0099-01-01 00:00:00")?"":remesa.getFecha_realcargue()%>">
            <%if(remesa.getFecha_realcargue().equals("0099-01-01 00:00:00")){%><img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.form2.fecha<%=remesa.getDestinatario()%><%=remesa.getNumrem()%>);return false;" HIDEFOCUS><%}%> </div></td>
      </tr>
      <%}
  %>
    </table></td>
    </tr>
  </table>
    <p align="center">
	<img  src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="copiarFecha();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"  style="cursor:hand "> 
	<img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand ">	    </p>
     <p>
        <%}%>
       <br>
       <%if(request.getParameter("mensaje")!=null){%>
  </p>
     <table border="2" align="center">
      <tr>
        <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
              <td width="229" align="center" class="mensajes"><span class="normal"><%=request.getParameter("mensaje")%></span></td>
              <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
              <td width="58">&nbsp;</td>
            </tr>
        </table></td>
      </tr>
    </table>
    <%}%>

   </form>    
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins_24.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
</body>
</html>
