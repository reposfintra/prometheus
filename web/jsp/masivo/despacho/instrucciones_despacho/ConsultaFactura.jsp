<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@taglib uri="/WEB-INF/tlds/tagsAgencia.tld" prefix="ag"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
    <head>

        <title>Consultar Facturas LG</title>        
		<script src='<%=BASEURL%>/js/date-picker.js'></script>
		<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
		<script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script> 
        <link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">

		<script>
			var valorRadio = "";
		
			function setRadio( value ){
			
				valorRadio = value;	
				
			}
			
			function validar( form ){
			
				if( valorRadio == "0" ){
					if(form.fecini.value == ''){
						alert( 'Seleccione la fecha de inicio...' );
						return false;
					}
					if(form.fecfin.value == ""){
						alert( 'Seleccione la fecha de finalizacion...' );
						return false;
					}
				}
				
				if ( valorRadio == "1" ) {
				
					if( form.fac_tsp.value == "" ){
						alert( 'Defina el numero de factura TSP para poder continuar...' );
						form.fac_tsp.focus();
						return false;
					}
					
					
				}
				
				if ( valorRadio == "2" ) {
				
					if ( form.ocompra.value == "" ) {
						alert( 'Defina el numero de la orden de compra para poder continuar...' );
						form.ocompra.focus();
						return false;
					}
					
				}
				
				if ( valorRadio == "3" ) {
				
					if ( form.fac_lg.value == "" ) {
						alert( 'Defina el numero de la factura de LG para poder continuar...' );
						form.fac_lg.focus();
						return false;
					}
					
				}
				
				return true;
				
			}				
			
			
	
		</script>
    </head>
   <body onResize="redimensionar()" onLoad="redimensionar();formulario.fac_tsp.focus();setRadio(0);" >
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Reporte Facturas Cliente LG"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

        <form name='formulario' method='POST' id="formulario" action="<%=CONTROLLER%>?estado=CargueLG&accion=Manager&cmd=ReporteFactura" onSubmit="return validar(this);">
            <table width="53%"  border="2" align="center">
              <tr>
                <td><table width="100%"  border="0" class="tablaInferior">
                  
				  <tr>
                   <td colspan="6">				   <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="barratitulo">
                     <tr>
                       <td width="50%" class="subtitulo1" colspan='3'>Consulta de Facturas</td>
                       <td width="50%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif"></td>
                     </tr>
                   </table>
				   </td>
          			
                  </tr>
                  <tr class="fila">
				    <td align="left"><div align="left">
				      <input name="opcion" type="radio" value="0" checked onClick="setRadio(this.value);">
				    </div></td>
				    <td align="left">Fecha Inicio</td>
				    <td align="left"><input name="fecini" type="text" class="textbox" id="fecini" size="10" readonly>
                      <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fecini);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/Calendario/calbtn.gif" width="34" height="22" border="0" alt=""></a></td>
			        <td colspan="2" align="left"><div align="left"></div>Fecha Fin </td>
			        <td align="left"><input name="fecfin" type="text" class="textbox" id="fecfin2" size="10" readonly>
                      <span class="Letras"> <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fecfin);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/Calendario/calbtn.gif" width="34" height="22" border="0" alt=""></a></span></td>
				  </tr>  
                  <tr class="fila">
                    <td width="4%" align="left"><input name="opcion" type="radio" id="opcion" onClick="setRadio(this.value);" value="1"></td>
                    <td width="25%" align="left">&nbsp;Numero de Factura TSP </td>
                    <td width="25%" align="left"><input name="fac_tsp" id="fac_tsp" type="text" class="textbox" size="15" maxlength="10"></td>
                    <td width="4%" align="left"><input name="opcion" id="radio2" type="radio" value="3" onClick="setRadio(this.value);"></td>
                    <td width="22%" align="left">Numero Factura LG</td>
                    <td width="20%" align="left"><input name="fac_lg" id="fac_lg" type="text" class="textbox" size="15" maxlength="10"></td>
                  </tr>
				  
				  <tr class="fila">
                    <td align="left"><input name="opcion" id="radio" type="radio" value="2" onClick="setRadio(this.value);"></td>
                    <td align="left">&nbsp;Orden de compra</td>
                    <td colspan="4" align="left">
					
                      <input name="ocompra" id="ocompra" type="text" class="textbox" size="15" maxlength="10">  					
					  
					</td>
                  </tr>
				  
				  
                </table></td>
              </tr>
          </table>
			<br>
            <table align="center">
              <tr>
                <td colspan="2" nowrap align="center">
<input name="Guardar" src="<%=BASEURL%>/images/botones/aceptar.gif"  style = "cursor:hand" align="middle" type="image" id="Guardar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">                  
&nbsp;
                  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" align="absmiddle" style="cursor:hand" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
              </tr>
          </table>
            <br>
            <br>
            <p>
              <%if(request.getParameter("mensaje")!=null){%>
            </p>
            <table border="2" align="center">
              <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                      <td width="229" align="center" class="mensajes"><span class="normal"><%=request.getParameter("mensaje")%></span></td>
                      <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                      <td width="58">&nbsp;</td>
                    </tr>
                </table></td>
              </tr>
            </table>
            <%}%>
        </form > 
  <form name="form2" method="post" action="<%=CONTROLLER%>?estado=CargueLG&accion=Manager&cmd=Relacionar">
	  <%if(model.instrucciones_despachoService.getRemesas()!=null){
 %>

    
  <table width="79%"  border="2" align="center">
    <tr>
      <td>
	<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="barratitulo">
      <tr>
        <td class="subtitulo1"><table width="100%" border="0" bgcolor="#4D71B0" align="center">
          <tr>
            <td class="subtitulo1">Lista de Remesas encontradas</td>
          </tr>
        </table></td>
        </tr>
    </table>
    <table width="100%" border="1" align="center" bordercolor="#999999">
      <tr class="tblTitulo">
        <td width="158" nowrap><div align="center"><strong>PLANILLA</strong></div></td>
        <td width="151"><div align="center"><strong>REMESA</strong></div></td>
        <td width="248"><div align="center">FACTURA TSP </div></td>
        <td width="200" ><div align="center">ORDEN DE COMPRA </div></td>
        <td width="184" ><div align="center">FACTURA</div></td>
        </tr>
      <% Vector remesas = model.instrucciones_despachoService.getRemesas();
	  
	  	for(int i = 0; i<remesas.size(); i++){
	  		Remesa remesa= (Remesa) remesas.elementAt(i);
		%>
      <tr class="<%=i%2==0?"filagris":"filaazul"%>"  style="cursor:hand" title="Ver Datos Factura..." onClick="window.location='<%=CONTROLLER%>?estado=CargueLG&accion=Manager&cmd=ReporteFactura&opcion=1&fac_tsp=<%=remesa.getFactura()%>'" >
        <td class="bordereporte"> <div align="center"><%=remesa.getNumpla()%></div></td>
        <td class="bordereporte"> <div align="center"><%=remesa.getNumrem()%> </div></td>
        <td class="bordereporte"> <div align="center"><%=remesa.getFactura()%> </div></td>
        <td class="bordereporte"><div align="center"><%=remesa.getDocumento()%>
              <input type="hidden" name="Vieja<%=remesa.getNumrem()%>" value="<%=request.getParameter("ocompraV"+remesa.getNumrem())!=null?request.getParameter("ocompraV"+remesa.getNumrem()):remesa.getDocumento()%>">
        </div></td>
        <td class="bordereporte"><div align="center"><%=remesa.getDocumento_rel()%></div></td>
        </tr>
      <%}
  %>
    </table></td>
    </tr>
  </table>
    <p align="center"> 
	<img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand ">	    </p>
    <p>
       <%}%>
      <br>
  </p>
  </form>    

	</div>  
	<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    

	</body>
</html>