<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Consultar Informacion de Cargue</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 

<script>
	function validarOCompra(){
		var sw=true;

		for(i=0;i<=form2.length-1;i++){

        	if(form2.elements[i].name.indexOf("ocompra")>=0){

				if(form2.elements[i].value==""){

					sw = false;
				}
			}        
		}
		if(sw==false){
			alert("Uno o mas campos de Ordenes de compra esta vacio, ingrese todos los datos.");
			
		}
		else{
			form2.submit();
//			form2.imgaceptar.disabled=true;
		}
	}
	
	
</script> 


<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=CARGUE"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
  

  <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
<form name="form2" method="post" action="<%=CONTROLLER%>?estado=CargueLG&accion=Manager&cmd=Relacionar">
<%if(model.instrucciones_despachoService.getLista()!=null){
 %>

    
  <table width="2762"  border="2" align="center">
    <tr>
      <td>
	<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="barratitulo">
      <tr>
        <td class="subtitulo1"><table width="100%" border="0" bgcolor="#4D71B0" align="center">
          <tr>
            <td class="subtitulo1">Reporte Traking </td>
          </tr>
        </table></td>
        </tr>
    </table>
    <table width="100%" border="1" align="center" bordercolor="#999999">
      <tr class="tblTitulo">
        <td width="38" nowrap>Des</td>
        <td width="83"><div align="center">Fecha Vencimiento </div></td>
        <td width="72"><div align="center">No. Orden</div></td>
        <td width="136" ><div align="center"> <strong>Factura</strong></div></td>
        <td width="161" ><div align="center">Cliente</div></td>
        <td width="40" ><div align="center">Item</div></td>
        <td width="42" ><div align="center">Qty</div></td>
        <td width="28" ><div align="center">Cub Unit </div></td>
        <td width="51" ><div align="center">Cub Total </div></td>
        <td width="54" >Fecha Factura </td>
        <td width="83" >Direccion</td>
        <td width="53" >Ciudad Destino </td>
        <td width="33" >#OC</td>
        <td width="93" >Almacenadora</td>
        <td width="102" >Transportadora</td>
        <td width="95" >Observaciones</td>
        <td width="70" >Inst. Especiales </td>
        <td width="50" >Item(1)</td>
        <td width="33" >Loc</td>
        <td width="89" >Appoint_date</td>
        <td width="87" >Appoint_time</td>
        <td width="66" >Fecha despacho </td>
        <td width="83" >Qty Despachada </td>
        <td width="34" >Placa</td>
        <td width="67" >Conductor</td>
        <td width="51" >Remesa</td>
        <td width="41" >Fecha Cita </td>

        <td width="71" >Qty Entregada a Cliente</td>
        <td width="55" >Fecha Entrega al cliente </td>
        <td width="106" >Observaciones Transportadora </td>
        <td width="60" >No Novedad </td>
        <td width="60" >Motivo Novedad </td>
        <td width="99" >Fecha entrega de devolucion a la almacenadorda</td>
        <td width="95" >Qty Entregada en la almacenadora </td>
        <td width="84" >Observacion de la entrega en la almm. </td>
        <td width="65" >Fecha reporte novedad </td>
		       
      </tr>
      <% Vector remesas = model.instrucciones_despachoService.getLista();
	  
	  	for(int i = 0; i<remesas.size(); i++){
	  		Instrucciones_Despacho inst = (Instrucciones_Despacho) remesas.elementAt(i);
		%>
      <tr class="<%=i%2==0?"filagris":"filaazul"%>" >
        <td class="bordereporte">&nbsp; <%=inst.getDesp()%>  </td>
        <td class="bordereporte">&nbsp;  <%= inst.getExpire_date()%>   </td>
        <td class="bordereporte">&nbsp;<%=inst.getOrder_no() %>  </td>
        <td class="bordereporte">&nbsp;<%= inst.getFactura() %>  <%if(!inst.getFactura().equals("") && model.ImagenSvc.existeImagen("014","FAC",inst.getFactura())){%><img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Ver Imagen" width="15" height="15" onClick="window.open('<%=CONTROLLER%>?estado=Imagen&accion=Control&documento=<%= inst.getFactura() %>&actividad=014&tipoDocumento=FAC&cmd=noAdmin','','width=800, height=600 scrollbars=yes, resizable = yes,top=100,left=200')" style="cursor:hand "><%}%></td>
        <td class="bordereporte">&nbsp;<%=inst.getShip_to_name()%> </td>
        <td class="bordereporte">&nbsp;<%=inst.getModelo()%>          </td>
        <td class="bordereporte">&nbsp;<%=inst.getOrder_qty()%></td>
        <td class="bordereporte">&nbsp;<%=inst.getCub_unit()%>          </td>
        <td class="bordereporte">&nbsp;<%=inst.getTotal_volumen()%></td>
        <td class="bordereporte">&nbsp;<%=inst.getFecha_factura()%></td>
        <td class="bordereporte">&nbsp;<%=inst.getAddr1()%></td>
        <td class="bordereporte">&nbsp;<%=inst.getCiudad()%></td>
        <td class="bordereporte">&nbsp;<%=inst.getNumpla()%></td>
        <td class="bordereporte">&nbsp;<%=inst.getBodega() %></td>
        <td class="bordereporte">&nbsp;<%= "TRANSPORTES SANCHEZ POLO"%></td>
        <td class="bordereporte">&nbsp;<%=inst.getObservacion()!=null?inst.getObservacion():""%></td>
        <td class="bordereporte">&nbsp;<%=inst.getInst_especiales()%></td>
        <td class="bordereporte">&nbsp;<%= inst.getItem() %></td>
        <td class="bordereporte">&nbsp;<%= inst.getLoc()%></td>
        <td class="bordereporte">&nbsp;<%= inst.getAppoint_date() %></td>
        <td class="bordereporte">&nbsp;<%=inst.getAppoint_time()%></td>
        <td class="bordereporte">&nbsp;<%=inst.getFechaDesp()%></td>
        <td class="bordereporte">&nbsp;<%=inst.getCant_despachada()%></td>
        <td class="bordereporte">&nbsp;<%= inst.getPlaca() %></td>
        <td class="bordereporte">&nbsp;<%= inst.getConductor()%></td>
        <td class="bordereporte">&nbsp;<%= inst.getNumrem()%></td>
        <td class="bordereporte">&nbsp;<%= inst.getFecha_cargue()%></td>
        <td class="bordereporte">&nbsp;<%=inst.getCantida_entregada()%></td>
        <td class="bordereporte">&nbsp;<%=inst.getFechaEntrega()!=null? inst.getFechaEntrega():""%></td>
        <td class="bordereporte">&nbsp;<%=inst.getObservacion_novedad() %></td>
        <td class="bordereporte">&nbsp;<%=inst.getCantida_novedad()%></td>
        <td class="bordereporte">&nbsp;<%=inst.getMotivo_novedad()%></td>
        <td class="bordereporte">&nbsp;<%=inst.getFecha_devolucion() %></td>
        <td class="bordereporte">&nbsp;<%=inst.getFecha_devolucion().equals("")?0:inst.getCantida_novedad()%></td>
        <td class="bordereporte">&nbsp;<%=inst.getFecha_devolucion().equals("")?"":inst.getObservacion_devolucion() %></td>
        <td class="bordereporte"><%=inst.getFecha_novedad()%></td>

      </tr>
      <%}
  %>
    </table></td>
    </tr>
  </table>
    <p align="center"><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgsalir" onMouseOver="botonOver(this);" onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/masivo/despacho/instrucciones_despacho&pagina=ReporteTraking.jsp&marco=no&opcion=InfoTrakin';" onMouseOut="botonOut(this);" style="cursor:hand"> 
	<img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand ">	    </p>
     <p>
        <%}%>
       <br>
       <%if(request.getParameter("mensaje")!=null){%>
  </p>
     <table border="2" align="center">
      <tr>
        <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
              <td width="229" align="center" class="mensajes"><span class="normal"><%=request.getParameter("mensaje")%></span></td>
              <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
              <td width="58">&nbsp;</td>
            </tr>
        </table></td>
      </tr>
    </table>
    <%}%>

  </form>    
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
</body>
</html>
