<%-- Declaracion de librerias--%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
    <head>
        <title>Reporte Traking</title>        

<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script>
	function buscar(lista,campo){
		var texto = campo.value.toLowerCase();
		for( var i=0; i<lista.length; i++ ){
			var v = lista[i].innerText.toLowerCase();
			if ( v.indexOf(texto) >= 0 ){
				lista.selectedIndex = i;
				break;
			}
		}
	}
	function enviarForm(){
		var sw=true;
		if(formulario.ocompra.value=='' && formulario.factura.value=='' ){
			if(formulario.fecini.value==''){
				alert("Seleccione  la fecha de inicio");
				sw=false;
				return false;
			
			}
			if(formulario.fecfin.value==''){
				alert("Seleccione  la fecha de finalizacion");
				sw=false;			
						return false;
			}
		}
		if(sw){
			
			formulario.submit();
		}

	}
	
		</script>
        
    </head>
   <body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Informe Traking LG"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

        <form name='formulario' method='POST' id="formulario" action="<%=CONTROLLER%>?estado=Cargue&accion=LGManager&cmd=Reporte" onSubmit="return validar(this);">
            <table width="56%"  border="2" align="center">
              <tr>
                <td><table width="100%"  border="0" class="tablaInferior">
                  
				  <tr>
                   <td  colspan="4" ><table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="barratitulo">
                       <tr>
                         <td class="subtitulo1" colspan='3'>Reporte de Facturas de Clientes </td>
                         <td width="62%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif"></td>
                       </tr>
                     </table></td>
       			  </tr>
                  
                  <tr class="fila">
                    <td width="23%" align="left">Fecha Inicial</td>
                    <td width="31%" align="left"><input name="fecini" type="text" class="textbox" id="fecini" size="10" readonly>
                      <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fecini);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/Calendario/calbtn.gif" width="34" height="22" border="0" alt=""></a></td>
                    <td width="16%" align="left">Fecha Final</td>
                    <td width="30%" align="left"><input name="fecfin" type="text" class="textbox" id="fecfin2" size="10" readonly>
                      <span class="Letras"> <a href="javascript:void(0)" onClick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fecfin);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/Calendario/calbtn.gif" width="34" height="22" border="0" alt=""></a></span>					</td>
                  </tr>
				  
				  <tr class="fila">
                    <td align="left">Orden Compra </td>
                    <td align="left"><input name="ocompra" type="text" id="ocompra" style="width:100% "></td>
                    <td align="left">Factura</td>
                    <td align="left"><input name="factura" type="text" id="factura" style="width:100% ">
					
					</td>
                  </tr>
				  <tr class="fila">
				    <td align="left">Destinatario</td>
				    <td colspan="3" align="left"><input name="destinatario" type="text" id="destinatario">
				      <span class="Simulacion_Hiper" onClick="window.open('<%=BASEURL%>/jsp/masivo/despacho/instrucciones_despacho/destinatarios.jsp','','status=yes,scrollbars=yes,resizable=yes')" style="cursor:hand ">Consultar Destinatarios</span> </td>
			      </tr>
				  <tr class="fila">
				    <td rowspan="2" align="left">Origen</td>
				    <td align="left">Campo de busqueda:
                    <input type="text" class="textbox" name='campoOrigen' id="campoOrigen" style="width:250" onKeyUp="buscar(document.formulario.origen,this)" ></td>
				    <td rowspan="2" align="left">Destino</td>
				    <td align="left">Campo de busqueda:
                    <input type="text" class="textbox" name='campoDestino' id="campoDestino" style="width:250" onKeyUp="buscar(document.formulario.destino,this)" ></td>
			      </tr>
				  <tr class="fila">
				    <td align="left"> <input:select default="" name="origen" options="<%=model.ciudadService.getTreMapCiudades()%>" attributesText="id='origen' size='3' class='listmenu' style='width:100%' onclick='document.formulario.campoOrigen.value = this[this.selectedIndex].innerText;'"/></td>
			        <td align="left"> <input:select default="" name="destino" options="<%=model.ciudadService.getTreMapCiudades()%>" attributesText="id='destino' size='3' class='listmenu' style='width:100%' onclick='document.formulario.campoDestino.value = this[this.selectedIndex].innerText;'"/></td>
				  </tr>  
				  
                </table></td>
              </tr>
          </table>
			<br>
            <table align="center">
              <tr>
                <td colspan="2" nowrap align="center">
<img name="Guardar" src="<%=BASEURL%>/images/botones/aceptar.gif"  style = "cursor:hand" align="middle" type="image" id="Guardar"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="enviarForm();">                  
&nbsp;
                  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" align="absmiddle" style="cursor:hand" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
              </tr>
          </table>
            <br>
            <br>
            <p>
              <%if(request.getParameter("mensaje")!=null){%>
            </p>
            <table border="2" align="center">
              <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                      <td width="229" align="center" class="mensajes"><span class="normal"><%=request.getParameter("mensaje")%></span></td>
                      <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                      <td width="58">&nbsp;</td>
                    </tr>
                </table></td>
              </tr>
            </table>
            <%}%>
        </form > 
	</div>  
	<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    

	</body>
</html>