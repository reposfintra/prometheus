<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Ingresar Solucion Novedad</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
<script>
	function validar(){
		
			form1.submit();
		
	}
</script> 



<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=CARGUE"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
  

  <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
  <br>
  <form name="form1" method="post" action="<%=CONTROLLER%>?estado=CargueLG&accion=Manager&cmd=ListarNovedad" >
   <table width="40%"  border="2" align="center">
    <tr>
      <td>

    <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="barratitulo">
      <tr>
        <td width="50%" class="subtitulo1" colspan='3'>Listar Novedades </td>
        <td width="50%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif"></td>
      </tr>
    </table>
    <table width="100%" align="center">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr class="fila">
              <td colspan="2" ><table width="100%" border="0" class="letraresaltada">
                <tr>
                  <td width="21%">Periodo:</td>
                  <td width="12%">A&ntilde;o </td>
                  <td width="21%">
				  <select name="ano">
				 <% for(int c = 0; c<12; c++){			
					int ano = 2007;
			%>
			<option value="<%=ano + c%>" >
				<%=ano + c%>
			</option>
	<%		}%>
                  </select></td>
                  <td width="23%">Mes</td>
                  <td width="23%"> <select name="mes">
                    <option value="01">Enero</option>
                    <option value="02">Febrero</option>
                    <option value="03">Marzo</option>
                    <option value="04">Abril</option>
                    <option value="05">Mayo</option>
                    <option value="06">Junio</option>
                    <option value="07">Julio</option>
                    <option value="08">Agosto</option>
                    <option value="09">Septiembre</option>
                    <option value="10">Octubre</option>
                    <option value="11">Noviembre</option>
                    <option value="12">Diciembre</option>
				
                  </select></td>
                </tr>
              </table></td>
              </tr>
            <tr class="fila">
              <td width="27" ><input name="solucion" type="checkbox" id="solucion" value="checkbox"></td>
              <td width="459">Mostrar Novedades con solucion</td>
            </tr>
            <tr class="fila">
              <td ><input name="nosolucion" type="checkbox" id="nosolucion" value="checkbox"></td>
              <td>Mostrar Novedades sin solucion </td>
            </tr>
        </table></td>
      </tr>
    </table>	</td>
      </tr>
    </table>
	<BR>
	<div align="center">
	  <img  style="cursor:hand " src="<%=BASEURL%>/images/botones/buscar.gif" align="middle" width="87" height="21" border="0" onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);" onClick="validar();">
	</div>
  </form>
<form name="form2" method="post" action="<%=CONTROLLER%>?estado=CargueLG&accion=Manager&cmd=GuardarNovedad">
	  <%if(model.instrucciones_despachoService.getLista()!=null && model.instrucciones_despachoService.getLista().size()>0){
 %>

    
  <table width="110%"  border="2" align="center">
    <tr>
      <td>
	<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="barratitulo">
      <tr>
        <td class="subtitulo1"><table width="100%" border="0" bgcolor="#4D71B0" align="center">
          <tr class="subtitulo1">
            <td> Lista de solucion a novedades mes de:
			<%if(request.getParameter("mes").equals("01")){%><%="ENERO"%><%}%>
			<%if(request.getParameter("mes").equals("02")){%><%="FEBRERO"%><%}%>
			<%if(request.getParameter("mes").equals("03")){%><%="MARZO"%><%}%>
			<%if(request.getParameter("mes").equals("04")){%><%="ABRIL"%><%}%>
			<%if(request.getParameter("mes").equals("05")){%><%="MAYO"%><%}%>
			<%if(request.getParameter("mes").equals("06")){%><%="JUNIO"%><%}%>
			<%if(request.getParameter("mes").equals("07")){%><%="JULIO"%><%}%>
			<%if(request.getParameter("mes").equals("08")){%><%="AGOSTO"%><%}%>
			<%if(request.getParameter("mes").equals("09")){%><%="SEPTIEMBRE"%><%}%>
			<%if(request.getParameter("mes").equals("10")){%><%="OCTUBRE"%><%}%>
			<%if(request.getParameter("mes").equals("11")){%><%="NOVIEMBRE"%><%}%>
			<%if(request.getParameter("mes").equals("12")){%><%="DICIEMBRE"%><%}%>
            <input type="hidden" name="ano" value="<%=request.getParameter("ano")%>">
            <input type="hidden" name="mes" value="<%=request.getParameter("mes")%>">			
			 </td>
          </tr>
        </table></td>
        </tr>
    </table>
    <table width="100%" border="1" align="center" bordercolor="#999999">
      <tr class="tblTitulo">
        <td width="101" nowrap>Fecha Novedad </td>
        <td width="80"><div align="center"><strong>Ciudad Ubicacion Mercancia</strong></div></td>
        <td width="59"><div align="center">Remesa</div></td>
        <td width="79" ><div align="center"> <strong>Orden de compra </strong></div></td>
        <td width="77" ><div align="center">Factura</div></td>
        <td width="77" ><div align="center">Fecha Factura</div></td>
        <td width="143" ><div align="center">Destinatario</div></td>
        <td width="86" ><div align="center">Producto</div></td>
        <td width="75" ><div align="center">Numero Novedad </div></td>
        <td width="131" ><div align="center">Cantidad Novedad </div></td>
        <td width="164" ><div align="center">Motivo Novedad</div></td>
        <td width="214" ><div align="center">Solucion Novedad </div></td>
      </tr>
      <% Vector remesas = model.instrucciones_despachoService.getLista();
	  
	  	for(int i = 0; i<remesas.size(); i++){
	  		Instrucciones_Despacho remesa= (Instrucciones_Despacho) remesas.elementAt(i);
		%>
      <tr class="<%=i%2==0?"filagris":"filaazul"%>" title="" >
        <td class="bordereporte"> <div align="center"><%=remesa.getFecha_novedad()%></div></td>
        <td class="bordereporte"> <div align="center"><%=remesa.getUbicacion_mercancia()%> </div></td>
        <td class="bordereporte"> <div align="center"><%=remesa.getNumrem()%> </div></td>
        <td class="bordereporte"><div align="center"><%=remesa.getOrder_no()%>          </div></td>
        <td class="bordereporte"><div align="center"><%=remesa.getFactura()%></div></td>
        <td class="bordereporte"><div align="center"><%=remesa.getFecha_factura()%>
          </div></td>
        <td class="bordereporte"><div align="center"><%=remesa.getShip_to_name()%>
        </div></td>
        <td class="bordereporte"><div align="center"><%=remesa.getModelo()%></div></td>
        <td class="bordereporte"><div align="center"><%=remesa.getCodigo_novedad()%>
             </div></td>
        <td class="bordereporte">
          <div align="center"><%=remesa.getCantida_novedad()%></div></td>
        <td class="bordereporte">
          <div align="center"><%=remesa.getMotivo_novedad()%></div></td>
        <td class="bordereporte">
			<%Vector causas = model.instrucciones_despachoService.getCausas();

			%>
			<select name="causa<%=remesa.getOrder_no()%><%=remesa.getModelo()%>" <%=remesa.getSolucion_novedad().equals("")?"":"disabled"%> style="width:100% ">
	<%		for(int c = 0; c<causas.size(); c++){			
				TablaGen t = (TablaGen) causas.elementAt(c);
	%>
			<option value="<%=t.getTable_code()%>" <%=remesa.getSolucion_novedad().equals(t.getTable_code())?"selected":""%>  >
				<%=t.getDescripcion()%>
			</option>
	<%		}%>
            </select></td>
      </tr>
      <%}
  %>
    </table></td>
    </tr>
  </table>
    <p align="center">
	<img  src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="form2.submit();this.disable=true" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"  style="cursor:hand "> 
	<img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand ">	
	<img src="<%= BASEURL%>/images/botones/exportarExcel.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="form2.action = '<%=CONTROLLER%>?estado=CargueLG&accion=Manager&cmd=ReporteNovedad';form2.submit(); this.disabled=true;" onMouseOut="botonOut(this);" style="cursor:hand "> </p>
     <p>
        <%}%>
       <br>
       <%if(request.getParameter("mensaje")!=null){%>
  </p>
     <table border="2" align="center">
      <tr>
        <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
              <td width="229" align="center" class="mensajes"><span class="normal"><%=request.getParameter("mensaje")%></span></td>
              <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
              <td width="58">&nbsp;</td>
            </tr>
        </table></td>
      </tr>
    </table>
    <%}%>

  </form>    
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins_24.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
</body>
</html>
