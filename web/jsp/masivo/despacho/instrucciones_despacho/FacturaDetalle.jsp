<%@page session="true"%>
<%@page import="java.util.*" %>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Factura Detalle</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
 <script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
 <script type='text/javascript' src="<%=BASEURL%>/js/consultadespacho.js"></script> 
 <script type='text/javascript' src="<%=BASEURL%>/js/general.js"></script> 
 <script type='text/javascript' src="<%=BASEURL%>/js/validar.js"></script> 
 <style type="text/css">
<!--
.Estilo1 {color: #D0E8E8}
-->
 </style>

</head> 
<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Factura Detalle"/>
</div> 

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
	<%String msg = request.getParameter("msg");
		if ( msg!=null && !msg.equals("") ){%>							
		  <table border="2" align="center">
			<tr>
				<td>
					<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						<tr>
							<td width="229" align="center" class="mensajes"><%=msg%></td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		  </table>		  
		        <br>
		        
 	            <img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgsalir" onMouseOver="botonOver(this);" onClick="window.location='<%=BASEURL%>/jsp/masivo/despacho/instrucciones_despacho/ConsultaFactura.jsp';" onMouseOut="botonOut(this);" style="cursor:hand">
 	            <input name="salir" src="<%=BASEURL%>/images/botones/salir.gif" type="image" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" class="boton" onclick="window.close();">
	  <%}
		else {
          Vector otros_datos = model.instrucciones_despachoService.getLista();
					
			if ( otros_datos != null && otros_datos.size() > 0 ) {
				BeanGeneral info_especifica = ( BeanGeneral ) otros_datos.elementAt( 0 );
		%>		
<form name="form1" method="post" >
  <table width="90%" border="2" align="center">
    <tr>
      <td width="100%" height="60">
	  
        <table width="100%">
		
          <tr>
            <td width="50%" height="22" align="left" class="subtitulo1">&nbsp;Informaci&oacute;n de la Factura TSP N&deg; <%=info_especifica.getValor_26()%>.</td>
            <td width="50%" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"align="left"></td>
          </tr>
		  
        </table>
		
        <table width="100%">
		
          <tr>
            <td width="13%" class="fila">Cliente:</td>
            <td width="23%" class="letra"><%=info_especifica.getValor_02()%> ( <%=info_especifica.getValor_01()%> ). </td>
            <td width="14%" class="fila">Agencia Facturacion:</td>
            <td width="19%" class="letra"><%=info_especifica.getValor_03()%></td>
            <td width="12%" class="fila">Moneda:</td>
			<td width="19%" class="letra"><%=info_especifica.getValor_04()%></td>			 
          </tr>
		  
          <tr>
            <td class="fila">Nit:</td>
            <td class="letra"><%=info_especifica.getValor_05()%></td>
            <td class="fila">Fecha Factura:</td>
			<td class="letra"><%=info_especifica.getValor_06()%></td>
            <td class="fila">Tasa:</td>           
			<td class="letra"><%=Util.customFormat( Double.parseDouble( info_especifica.getValor_07() ) )%></td>
          </tr>
		 
          <tr>
            <td class="fila">Forma de Pago:</td>
            <td class="letra"><%=info_especifica.getValor_08()%></td>
            <td class="fila">Plazo:</td>
            <td class="letra"><%=info_especifica.getValor_09()%></td>
            <td class="fila">Valor Total:</td>
            <td class="letra"><%=Util.customFormat( Double.parseDouble( info_especifica.getValor_10() ) )%></td>
          </tr>

      </table>
      </td>
    </tr>
  </table>
  
  <table width="90%" border="2" align="center">
    <tr>
      <td height="26"><table width="100%">
          <tr class="fila" id="cantidad">
            <td width="20%">Descripcion:</td>
            <td width="80%"  align="left" >
              <textarea name="desc" cols="100%" rows="2" onKeyPress="soloAlfa(event)" readonly><%=info_especifica.getValor_11()%></textarea>
            </td>
          </tr>
          <tr class="fila" id="cantidad">
            <td height="3"  >Observacion:</td>
            <td  align="left">
              <textarea name="obse" cols="100%" rows="2" onKeyPress="soloAlfa(event)" readonly><%=info_especifica.getValor_12()%></textarea>
            </td>
          </tr>
      </table></td>
    </tr>
  </table>
  

  
  <table width="90%" border="2" align="center">
      <tr>
        <td  font-size:"10px">
          <table width="100%" align="center">
            <tr>
              <td width="50%" height="22" align="center" class="subtitulo1">&nbsp;Detalle del Documento</td>
              <td width="50%" align="center" class="barratitulo"><div align="left"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></div></td>
            </tr>
          </table>
          <div align="center">
            <table id="detalle" width="100%" align="center" >
              <tr  id="fila1" class="tblTituloFactura">
                <td width="4%" align="center">
				<table width="79%" height="37" border="1" align="center"  bordercolor="#999999" bgcolor="#F7F5F4">
				<tr class="tblTitulo">
				 <td width="50" nowrap style="font-size:13px "><div align="center">Item</div></td>
				</tr>
				</table>
				</td>
                <td width="96%" align="center"> 
				  <table width="100%" border="1"  bordercolor="#999999" bgcolor="#F7F5F4" align="center">
				  <tr class="tblTitulo">
               
                  <td width="78" nowrap style="font-size:11px "><div align="center">Remesa</div></td>
                  <td width="268" align="center" style="font-size:11px ">Descripcion</td>
                  <td width="50" align="center" style="font-size:11px "><div align="center">Cantidad</div></td>
                  <td width="115" align="center" style="font-size:11px "><div align="center">Valor Unitario </div></td>
                  <td width="97" align="center" style="font-size:11px "><div align="center">Valor</div></td>
                  <td width="143" align="center" style="font-size:11px ">Orden de compra</td>
                  <td width="139" align="center" style="font-size:11px ">Factura LG </td>
				  </tr>
				  </table>
				</td>
              </tr>
			  
            <%
			for ( int i = 0; i < otros_datos.size(); i++ ) {
				BeanGeneral info = ( BeanGeneral ) otros_datos.elementAt( i );
	  		%>  	
	  		<tr class="<%=(i % 2 != 0 )?"filaazul":"filagris"%>" nowrap  bordercolor="#D1DCEB">
                <td class="bordereporte" align="center">&nbsp;<%=info.getValor_13()%></td>  
				<td>
					<table width='100%' class='tablaInferior'>
					  <tr class='<%=(i%2!=0)?"filaazul":"filagris"%>'  >
						<td width='9%' align="center">&nbsp;<%=info.getValor_14()%></td>
						<td width='30%' align="left">&nbsp;<%=info.getValor_16()%></td>
						<td width="6%" align="center">&nbsp;<%=Util.customFormat( Double.parseDouble( info.getValor_17() ) )%></td>
						<td width='12%' align="right">&nbsp;<%=Util.customFormat( Double.parseDouble( info.getValor_18() ) )%></td>
						<td width='11%' align="right">&nbsp;<%=Util.customFormat( Double.parseDouble( info.getValor_20() ) )%></td>
					    <td width='17%' align="right"><div align="center">&nbsp;<%=info.getValor_27()%></div></td>
					    <td width='9%' align="right"><div align="center">&nbsp;<%=info.getValor_28()%></div></td>
					    <td width='6%' align="right"><div align="center">&nbsp;<%if(!info.getValor_28().equals("") && model.ImagenSvc.existeImagen("014","FAC",info.getValor_28())){%><img src="<%=BASEURL%>/images/botones/iconos/documento.gif" alt="Ver Imagen" width="15" height="15" onClick="window.open('<%=CONTROLLER%>?estado=Imagen&accion=Control&documento=<%=info.getValor_28()%>&actividad=014&tipoDocumento=FAC&cmd=noAdmin','','width=800, height=600 scrollbars=yes, resizable = yes,top=100,left=200')" style="cursor:hand "><%}%></div></td>
					  </tr>
					</table>
				</td>
			
              </tr>
				<%}%>
            </table>
        </div></td>
      </tr>
    </table>
	
	<p align="center"><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgsalir" onMouseOver="botonOver(this);" onClick="window.location='<%=BASEURL%>/jsp/masivo/despacho/instrucciones_despacho/ConsultaFactura.jsp';" onMouseOut="botonOut(this);" style="cursor:hand">
	<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  height="21" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
    <img src="<%= BASEURL%>/images/botones/exportarExcel.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="form1.action = '<%=CONTROLLER%>?estado=CargueLG&accion=Manager&cmd=FacturaExcel';form1.submit(); this.disabled=true;" onMouseOut="botonOut(this);" style="cursor:hand "> <br>
    <br>
	</p>

    <p>
      <%if(request.getParameter("mensaje")!=null){%>
    </p>
    <table border="2" align="center">
      <tr>
        <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
              <td width="229" align="center" class="mensajes"><span class="normal"><%=request.getParameter("mensaje")%></span></td>
              <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
              <td width="58">&nbsp;</td>
            </tr>
        </table></td>
      </tr>
    </table>
    <%}%>
    <p align="center">&nbsp;	  </p>
</form >
<%}
	}%>
</div>
</body>
</html>