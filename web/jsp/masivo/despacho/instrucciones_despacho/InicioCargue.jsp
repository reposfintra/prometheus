<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Ingresar Informacion de Cargue</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 

<script>
	function validarOCompra(){
		var sw=true;

		for(i=0;i<=form2.length-1;i++){

        	if(form2.elements[i].name.indexOf("ocompra")>=0){

				if(form2.elements[i].value==""){

					sw = false;
				}
			}        
		}
		if(sw==false){
			alert("Uno o mas campos de Ordenes de compra esta vacio, ingrese todos los datos.");
			
		}
		else{
			form2.submit();
//			form2.imgaceptar.disabled=true;
		}
	}
	
	
</script> 


<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body onLoad="form1.planilla.focus();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=CARGUE"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
  

  <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
  <br>
  <form name="form1" method="post" action="<%=CONTROLLER%>?estado=CargueLG&accion=Manager&cmd=Buscar" >
   <table width="40%"  border="2" align="center">
    <tr>
      <td>

    <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="barratitulo">
      <tr>
        <td width="50%" class="subtitulo1" colspan='3'>Buscar Datos</td>
        <td width="50%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif"></td>
      </tr>
    </table>
    <table width="100%" align="center">
      <tr>
        <td><table width="100%" cellpadding="0" cellspacing="0">
            <tr class="fila">
              <td width="176" rowspan="3" >Numero </td>
              <td width="152" rowspan="3"><input name="planilla" type="text" id="planilla">
                <input name="modificar" type="hidden" id="modificar">                </td>
              <td width="33"><input name="opcion" type="radio" value="1" checked></td>
              <td width="119">Planilla</td>
            </tr>
            <tr class="fila">
              <td><input name="opcion" type="radio" value="2"></td>
              <td>Remesa</td>
            </tr>
            <tr class="fila">
              <td><input name="opcion" type="radio" value="3"></td>
              <td>Orden de compra </td>
            </tr>
        </table></td>
      </tr>
    </table>	</td>
      </tr>
    </table>
	<BR>
	<div align="center">
	  <img  style="cursor:hand " src="<%=BASEURL%>/images/botones/buscar.gif" align="middle" width="87" height="21" border="0" onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);" onClick="if(form1.planilla.value ==''){ alert('Escriba la planilla');}else{ form1.submit();this.disabled='true';}">
	</div>
  </form>
<form name="form2" method="post" action="<%=CONTROLLER%>?estado=CargueLG&accion=Manager&cmd=Relacionar">
	  <%if(model.instrucciones_despachoService.getRemesas()!=null){
 %>

    
  <table width="95%"  border="2" align="center">
    <tr>
      <td>
	<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="barratitulo">
      <tr>
        <td class="subtitulo1"><table width="100%" border="0" bgcolor="#4D71B0" align="center">
          <tr>
            <td> &nbsp;<img src='<%=BASEURL%>/images/cuadrito.gif' width='9' height='9'>&nbsp; <a href="javascript:window.location='<%=CONTROLLER%>?estado=CargueLG&accion=Manager&cmd=Buscar&planilla=<%=request.getParameter("planilla")%>&opcion='+form2.opcion.value" class="encabezado">Relacionar con orden de compra </a> &nbsp;
			<%if(request.getParameter("mostrar")!=null){%><img src='<%=BASEURL%>/images/cuadrito.gif' width='9' height='9'>&nbsp;
			 <a href="javascript:window.location='<%=CONTROLLER%>?estado=CargueLG&accion=Manager&cmd=Cantidades&planilla=<%=request.getParameter("planilla")%>';" class="encabezado">Ingresar Cantidades de despacho 
			 <input name="mostrar" type="hidden" id="mostrar">
			 </a>
			 <%}%>&nbsp;
              <input name="planilla" type="hidden" id="planilla" value="<%=request.getParameter("planilla")%>">
              <input name="opcion" type="HIDDEN" id="opcion" value="<%=request.getParameter("opcion")%>"></td>
          </tr>
        </table></td>
        </tr>
    </table>
    <table width="100%" border="1" align="center" bordercolor="#999999">
      <tr class="tblTitulo">
        <td width="110" nowrap><strong>PLANILLA</strong></td>
        <td width="105"><div align="center"><strong>REMESA</strong></div></td>
        <td width="203"><div align="center">DESCRIPCION</div></td>
        <td width="140" ><div align="center"> <strong>DESTINATARIO</strong></div></td>
        <td width="57" ><div align="center">M3</div></td>
        <td width="199" ><div align="center">ORDEN DE COMPRA </div></td>
        <td width="131" ><div align="center">FACTURA</div></td>
        <td width="178" ><div align="center">FECHA FACTURA </div></td>
      </tr>
      <% Vector remesas = model.instrucciones_despachoService.getRemesas();
	  
	  	for(int i = 0; i<remesas.size(); i++){
	  		Remesa remesa= (Remesa) remesas.elementAt(i);
		%>
      <tr class="<%=i%2==0?"filagris":"filaazul"%>"  style="cursor:hand" title="Relacionar Orden de Compra..." >
        <td class="bordereporte"> <%=remesa.getNumpla()%></td>
        <td class="bordereporte"> <%=remesa.getNumrem()%> </td>
        <td class="bordereporte"> <%=remesa.getDescripcion()%> </td>
        <td class="bordereporte"><%=remesa.getNom_destinatario()%></td>
        <td class="bordereporte"><%=remesa.getPesoReal()%></td>
        <td class="bordereporte"><input name="ocompra<%=remesa.getNumrem()%>" type="text" id="ocompra<%=remesa.getNumrem()%>" size="10" value="<%=remesa.getDocumento()%>">
          <img src="<%=BASEURL%>/images/botones/iconos/lupa.gif" alt="Listar ordenes de compra.." name="imglupa" width="20" height="20" style="cursor:hand" onClick="window.open('<%=CONTROLLER%>?estado=CargueLG&accion=Manager&cmd=Listar&numrem=<%=remesa.getNumrem()%>','','status=yes,scrollbars=no,width=700,height=470,resizable=yes');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
          <input type="hidden" name="Vieja<%=remesa.getNumrem()%>" value="<%=request.getParameter("ocompraV"+remesa.getNumrem())!=null?request.getParameter("ocompraV"+remesa.getNumrem()):remesa.getDocumento()%>"></td>
        <td class="bordereporte"><input name="factura<%=remesa.getNumrem()%>" type="text" value="<%=remesa.getDocumento_rel()%>" size="10" maxlength="15"></td>
        <td class="bordereporte"><input name="fecha<%=remesa.getNumrem()%>" type="text" class="textbox" id="fecha<%=remesa.getNumrem()%>" value="<%=remesa.getFecha_factura().equals("0099-01-01 00:00:00")?"":remesa.getFecha_factura()%>"  readonly size="13" maxlength="16">
          <img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.form2.fecha<%=remesa.getNumrem()%>);return false;" HIDEFOCUS> </td>
      </tr>
      <%}
  %>
    </table></td>
    </tr>
  </table>
    <p align="center"><img  src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="validarOCompra();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"  style="cursor:hand "> 
	<img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand ">	    </p>
     <p>
        <%}%>
       <br>
       <%if(request.getParameter("mensaje")!=null){%>
  </p>
     <table border="2" align="center">
      <tr>
        <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
              <td width="229" align="center" class="mensajes"><span class="normal"><%=request.getParameter("mensaje")%></span></td>
              <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
              <td width="58">&nbsp;</td>
            </tr>
        </table></td>
      </tr>
    </table>
    <%}%>

   </form>    
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
</body>
</html>
