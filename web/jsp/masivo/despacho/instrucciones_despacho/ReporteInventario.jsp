<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Consultar Informacion de Cargue</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 


<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=CARGUE"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
  

  <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
<form name="form2" method="post" action="">
<%if(model.instrucciones_despachoService.getLista()!=null){
 %>

    
  <table width="90%"  border="2" align="center">
    <tr>
      <td height="76">
	<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="barratitulo">
      <tr>
        <td class="subtitulo1"><table width="100%" border="0" bgcolor="#4D71B0" align="center">
          <tr>
            <td class="subtitulo1">Inventario</td>
          </tr>
        </table></td>
        </tr>
    </table>
    <table width="100%" border="1" align="center" bordercolor="#999999">
      <tr class="tblTitulo">
        <td width="129" nowrap>Ubicacion</td>
        <td width="104"><div align="center">Modelo</div>          </td>
        <td width="197">Descripcion</td>
        <td width="132">Cantidad Entradas </td>
        <td width="131">Cantidad Salidas </td>
        <td width="110">Cantidad Saldo </td>
        <td width="76">Unidad</td>
      </tr>
      <% Vector remesas = model.instrucciones_despachoService.getLista();
	  
	  	for(int i = 0; i<remesas.size(); i++){
	  		Instrucciones_Despacho inst = (Instrucciones_Despacho) remesas.elementAt(i);
		%>
      <tr class="<%=i%2==0?"filagris":"filaazul"%>" style="cursor:hand " title="Ver detalles" onClick="window.open('<%=CONTROLLER%>?estado=CargueLG&accion=Manager&cmd=InventarioDetalle&ocompra=<%=request.getParameter("ocompra")%>&factura=<%=request.getParameter("factura")%>&ubicacion=<%=inst.getCod_ubicacion()%>&material=<%=inst.getModelo()%>','','scrollbars=no, resizable = yes,toobar=no')" >
        <td class="bordereporte">&nbsp;<%=inst.getBodega()%>   </td>
        <td class="bordereporte">&nbsp;<%=inst.getModelo()%>  </td>
        <td class="bordereporte">&nbsp;<%=inst.getDescripcionMaterial()%></td>
        <td class="bordereporte">&nbsp;<%=inst.getCantida_entrada()%></td>
        <td class="bordereporte">&nbsp;<%=inst.getCantida_salida()%></td>
        <td class="bordereporte">&nbsp;<%=inst.getCantida_saldo()%></td>
        <td class="bordereporte">&nbsp;<%=inst.getUnidad()%></td>
      </tr>
      <%}
  %>
    </table></td>
    </tr>
  </table>
    <p align="center"><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgsalir" onMouseOver="botonOver(this);" onClick="window.location='<%=BASEURL%>/jsp/masivo/despacho/instrucciones_despacho/ConsultaInventario.jsp';" onMouseOut="botonOut(this);" style="cursor:hand"> 
	<img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand ">	    </p>
     <p>
        <%}%>
       <br>
       <%if(request.getParameter("mensaje")!=null){%>
  </p>
     <table border="2" align="center">
      <tr>
        <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
              <td width="229" align="center" class="mensajes"><span class="normal"><%=request.getParameter("mensaje")%></span></td>
              <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
              <td width="58">&nbsp;</td>
            </tr>
        </table></td>
      </tr>
    </table>
    <%}%>

  </form>    
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
</body>
</html>
