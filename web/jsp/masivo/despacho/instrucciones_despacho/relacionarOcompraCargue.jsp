<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Ingresar Informacion de Cargue</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
<script>
	function verificarCantidad(numero){
		var cantidad1 = document.getElementById("cantVieja_"+numero).value;
		var cantidad2 = document.getElementById("cantRef"+numero).value;
		if(Number(cantidad1)>Number(cantidad2)){
			alert("La cantidad de cargue es menor, seleccione una causa");
			document.getElementById("causa"+numero).disabled=false;
		}
		else{
			document.getElementById("causa"+numero).disabled=true;
		}
		
	}
	function verificarCausas(){
		var sw=true;

		for(i=0;i<=form2.length-1;i++){

        	if(form2.elements[i].name.indexOf("causa")>=0){
				if(form2.elements[i].disabled==false){
					if(form2.elements[i].value==""){

						sw = false;
					}
				}
			}        
		}
		if(sw==false){
			alert("Debe seleccionar la causa para poder continuar.");
			
		}
		else{
			form2.submit();
//			form2.imgaceptar.disabled=true;
		}
		
	}
	
	function copiarCantidad(numrem){
		//alert("Copiar Cantidad "+numrem);
		var check = document.getElementById("check"+numrem);
//		alert(check.checked);
		if(check.checked==true){
		
			for(i=0;i<=form2.length-1;i++){
				var nombre = "cantVieja_"+numrem;
				//alert(form2.elements[i].name+" : "+nombre);
				if(form2.elements[i].name.indexOf(nombre)>=0){
					
					var cantidad1 = form2.elements[i].value;
					var numero = form2.elements[i].name.split("_");
					if(numero.length>1){
						var cantRef = document.getElementById("cantRef"+numero[1]);
						cantRef.value= cantidad1;	
						document.getElementById("causa"+numero[1]).disabled=true;
					}
				}        
			}
		}
		
	}
	
</script> 


<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=CARGUE"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
  

  <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
  <br>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=CargueLG&accion=Manager&cmd=Buscar" >
   <table width="40%"  border="2" align="center">
     <tr>
       <td>
         <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="barratitulo">
           <tr>
             <td width="50%" class="subtitulo1" colspan='3'>Buscar Datos</td>
             <td width="50%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif"></td>
           </tr>
         </table>
         <table width="100%" align="center">
           <tr>
             <td><table width="100%" cellpadding="0" cellspacing="0">
                 <tr class="fila">
                   <td width="176" rowspan="3" >Numero </td>
                   <td width="152" rowspan="3"><input name="planilla" type="text" id="planilla4">
                       <input name="modificar" type="hidden" id="modificar">
                   </td>
                   <td width="33"><input name="opcion" type="radio" value="1" checked></td>
                   <td width="119">Planilla</td>
                 </tr>
                 <tr class="fila">
                   <td><input name="opcion" type="radio" value="2"></td>
                   <td>Remesa</td>
                 </tr>
                 <tr class="fila">
                   <td><input name="opcion" type="radio" value="3"></td>
                   <td>Orden de compra </td>
                 </tr>
             </table></td>
           </tr>
       </table></td>
     </tr>
   </table>
   <BR>
	<div align="center">
	  <img  style="cursor:hand " src="<%=BASEURL%>/images/botones/buscar.gif" align="middle" width="87" height="21" border="0" onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);" onClick="if(form1.planilla.value ==''){ alert('Escriba la planilla');}else{ form1.submit();this.disabled='true';}">
	</div>
  </form>
	  <form name="form2" method="post" action="<%=CONTROLLER%>?estado=CargueLG&accion=Manager&cmd=CantidadCargueOcompra">
	  <%if(model.instrucciones_despachoService.getRemesas()!=null){
 %>

    
  <table width="51%"  border="2" align="center">
    <tr>
      <td>
	<table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="barratitulo">
      <tr>
        <td class="subtitulo1"><table width="100%" border="0" bgcolor="#4D71B0" align="center">
          <tr>
            <td class="subtitulo1">Datos orden de compra </td>
          </tr>
        </table></td>
        </tr>
    </table>
    <table width="100%" border="1" align="center" bordercolor="#999999">
      <tr class="tblTitulo">
        <td width="175" ><div align="center">ORDEN DE COMPRA </div></td>
        <td width="434" ><div align="center">REFERENCIAS</div>          </td>
        </tr>
      <% Vector remesas = model.instrucciones_despachoService.getRemesas();
	  
	  	for(int i = 0; i<remesas.size(); i++){
	  		Remesa remesa= (Remesa) remesas.elementAt(i);
		%>
      <tr class="<%=i%2==0?"filagris":"filaazul"%>"  style="cursor:hand" title="Relacionar Orden de Compra..." >
        <td class="bordereporte"><%=remesa.getDocumento()%>
          </td>
        <td class="bordereporte"><table width="100%" border="1" bordercolor="#999999">
          <tr class="tblTitulo">
            <td width="25%" height="37"><div align="center">Referencia</div></td>
            <td width="20%"><div align="center">Cantidad</div></td>
            <td width="18%"><div align="center">Cantidad Despacho</div></td>
            <td width="37%">Causa</td>
          </tr>
		  <% Vector ref = remesa.getReferencias()!=null?remesa.getReferencias():new Vector();
		  	for(int j = 0; j<ref.size(); j++){
				Instrucciones_Despacho inst = (Instrucciones_Despacho) ref.elementAt(j);
		  %>
          <tr class="<%=j%2==0?"filagris":"filaazul"%>">
            <td><%=inst.getModelo()%></td>
            <td><div align="center"><%=inst.getOrder_qty()%>
                  <input name="cantVieja_<%=remesa.getNumrem()%><%=inst.getModelo()%>" type="hidden" id="cantVieja_<%=remesa.getNumrem()%><%=inst.getModelo()%>" value="<%=inst.getOrder_qty()%>">
            </div></td>
            <td><div align="center">
			 <input name="cantRef<%=remesa.getNumrem()%><%=inst.getModelo()%>" readonly type="text" size="10" onChange="verificarCantidad('<%=remesa.getNumrem()%><%=inst.getModelo()%>');" value="0">
			  </div></td>
            <td><p id="causa">
			<%Vector causas = model.instrucciones_despachoService.getCausas();

			%>
			<select name="causa<%=remesa.getNumrem()%><%=inst.getModelo()%>" style="width:100% ">
	<%		for(int c = 0; c<causas.size(); c++){			
				TablaGen t = (TablaGen) causas.elementAt(c);
	%>
			<option value="<%=t.getTable_code()%>" <%=inst.getCausa().equals(t.getTable_code())?"selected":""%>  >
				<%=t.getDescripcion()%>
			</option>
	<%		}%>
            </select>
			</p></td>
          </tr>
		  <%}%>
        </table></td>
        </tr>
      <%}
  %>
    </table>    </td>
    </tr>
  </table>
    <p align="center"><img  src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="verificarCausas();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"  style="cursor:hand "> 
	<img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand ">	    </p>
     <p>
        <%}%>
       <br>
       <%if(request.getParameter("mensaje")!=null){%>
  </p>
     <table border="2" align="center">
      <tr>
        <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
              <td width="229" align="center" class="mensajes"><span class="normal"><%=request.getParameter("mensaje")%></span></td>
              <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
              <td width="58">&nbsp;</td>
            </tr>
        </table></td>
      </tr>
    </table>
    <%}%>

   </form>    
	  

</div>
</body>
</html>
