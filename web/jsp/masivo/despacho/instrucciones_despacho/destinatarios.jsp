<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>Consultar Destinatarios</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/validar.js">
</script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</script>
<script>
	function buscar(lista,campo){
		var texto = campo.value.toLowerCase();
		for( var i=0; i<lista.length; i++ ){
			var v = lista[i].innerText.toLowerCase();
			if ( v.indexOf(texto) >= 0 ){
				lista.selectedIndex = i;
				break;
			}
		}
	}
	function setearCampo(ciu) {
    var campo = parent.opener.document.formulario.destinatario;       
    campo.value=ciu;
    parent.close();
}
	</script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body>
<form action="<%=CONTROLLER%>?estado=Cargue&accion=LGManager&cmd=destinatarios" method="post" name="form2" id="form2">
  <table width="52%"  border="2" align="center">
    <tr>
      <td>
        <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="barratitulo">
          <tr>
            <td class="subtitulo1" colspan='3'>Buscar Destinatarios </td>
            <td width="59%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"> </td>
          </tr>
        </table>
        <TABLE width='100%' border='0' class='tablaInferior'>
          <TR class="letra">
            <TD width="16%" height="35" rowspan="2" class="letraresaltada">Destino</TD>
            <TD>Campo de busqueda:
                    <input type="text" class="textbox" name='campoOrigen' id="campoOrigen" style="width:250" onKeyUp="buscar(document.form2.ciudad,this)" >
	        	                </TD>
          </TR>
          <TR class="letra">
            <TD><%
	 
	  TreeMap ciudades =model.ciudadService.getTreMapCiudades(); 
	  String corigen="";
	  if(request.getParameter("ciudad")!=null){
	  	corigen = request.getParameter("ciudad");
	  }
	  
	  %>
              <input name="controller" type="hidden" id="controller2" value="<%=CONTROLLER%>">
 <input:select default="" name="ciudad" options="<%=model.ciudadService.getTreMapCiudades()%>" attributesText="id='ciudad' size='3' class='listmenu' style='width:100%' onclick='document.form2.campoOrigen.value = this[this.selectedIndex].innerText;'"/>
             
          </TR>
          <TR class="letra">
            <TD height="35" class="letraresaltada">Nombre</TD>
            <TD><input name="nombre" type="text" id="nombre">
            <img name="imageField" type="image" style="cursor:hand " src="<%=BASEURL%>/images/botones/iconos/lupa.gif" align="middle" width="16" height="16" border="0" onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);" onClick="form2.submit();"></TD>
          </TR>
      </TABLE></td>
    </tr>
  </table>
  <div align="center">  <br>
  </div>
  <table width="69%"  border="2" align="center">
	<tr>
	  <td>
  <table width="100%" class="tablaInferior">
    <tr>
      <td height="22" colspan=2 class="subtitulo1">Lista de Destinatarios<span class="Letras">
      </span></td>
      <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
    </tr>
  </table>
  <table width="100%" border="1" bordercolor="#999999">
	<tr class="tblTitulo">
	<td colspan="2"  bordercolor="#999999">Destinatario</td>
	</tr>
	
	
 <%
if(model.remidestService.getVectorRemiDest()!=null){%>
	<%String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 20;
    int maxIndexPages = 10;
	String numrem =null;
	String tipo ="002";
	String estado="";
	
	%>

	<pg:pager
    items="<%= model.remidestService.getVectorRemiDest().size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
	<%	for (int i = offset.intValue(),
	         l = Math.min(i + maxPageItems, model.remidestService.getVectorRemiDest().size());
	     i < l; i++){
		 //for(int i=0; i<model.remidestService.getVectorRemiDest().size();i++){
  		
            RemiDest rd = (RemiDest) model.remidestService.getVectorRemiDest().elementAt(i);
            String coddest = rd.getCodigo();
            String nomdest = rd.getNombre();
			String direccion  = rd.getDireccion();
    	%> 
	<pg:item>
	<tr class="<%=i%2==0?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" onClick="setearCampo('<%=coddest%>')">
	    <td width="47%" class="bordereporte"><strong><%=nomdest%></strong><br>
     <%=coddest%></td>
      <td width="53%" class="bordereporte"><%=direccion%></td>
      </tr>
	</pg:item>
	 <%
	 }%>


	<tr class="filaresaltada">
	  <td colspan="2"><div align="center">
	   <pg:index>
	      <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
    	</pg:index>
	  </div>
	  
	  </td>
    </tr>
	</pg:pager>	 
<%}%>

	
  </table>
  </td>
  </tr></table>
  
  <div align="center"><br>
    <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">
  </div>
</form>
</body>
</html>
