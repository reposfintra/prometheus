<!--
- Autor : ricardo Rosero
- Date  : 13/01/2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, encargada de guardar una configuracion en un archivo properties
--%>
<%@page import="java.util.*"%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>

<head>
<title>Ejecutar Estadísticas</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC="<%=BASEURL%>/js/boton.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script src="<%=BASEURL%>/js/validar.js"></script>
</head>
<% String Mensaje       = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):""; %>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Configuracion Estadisticas Carga Placa"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<form name="forma" id="forma" method="post" action="<%= CONTROLLER %>?estado=EstCarga&accion=Placa">
  <table border="2" align="center">
   <tr>
     <td>
       <table width="388" align="center" border="0">
         <tr class="fila">
	       <td align="center" class="subtitulo1">Estadísticas Carga - Placa</td>
		   <td width="54%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="33" height="20"></td>
  		 </tr>
       </table>
       <table width="100%" align="center" border="0">
         <tr class="fila">
	       <td align="center">Clasificación Carga</td>
	       <td align="center" colspan="2">Período Estadística</td>
    	 </tr>
		 <tr class="fila"> 
		   <td align="center">Clasificacion 1</td>
	       <td align="center"><input type="radio" name="periodo1" value="6"> 6 Meses</td>
		   <td align="center"><input type="radio" name="periodo1" value="12"> 12 Meses</td>
		 </tr>
		 <tr class="fila">
		   <td align="center">Clasificacion 2</td>
	       <td align="center"><input type="radio" name="periodo2" value="6"> 6 Meses</td>
		   <td align="center"><input type="radio" name="periodo2" value="12"> 12 Meses</td>
		 </tr>
		 <tr class="fila">
		   <td align="center">Clasificacion 3</td>
	       <td align="center"><input type="radio" name="periodo3" value="6"> 6 Meses</td>
		   <td align="center"><input type="radio" name="periodo3" value="12"> 12 Meses</td>
		 </tr>
		 <tr class="fila">
		   <td align="center">Clasificacion 4</td>
	       <td align="center"><input type="radio" name="periodo4" value="6"> 6 Meses</td>
		   <td align="center"><input type="radio" name="periodo4" value="12"> 12 Meses</td>
		 </tr>
		 <tr class="fila">
		   <td align="center">Clasificacion 5</td>
		   <td align="center"><input type="radio" name="periodo5" value="6"> 6 Meses</td>
		   <td align="center"><input type="radio" name="periodo5" value="12"> 12 Meses</td>
		 </tr>
		
	   </table>
	  </td> 
    </tr> 
  </table>
 <br>
 <div align="center"><br>
    <img src="<%= BASEURL %>/images/botones/aceptar.gif"  name="imgaceptar" onClick="forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">&nbsp; 
	<img src="<%= BASEURL %>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);"  style="cursor:hand ">&nbsp; 
	<img src="<%= BASEURL %>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);"  style="cursor:hand "></div>

 </form>
<p>
  <%if(!Mensaje.equals("")){%>
</p>
<p>
<table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
          <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="58">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<p></p>
<%}%>
</div>
</body>
</html>
