<!--
- Autor : ricardo Rosero
- Date  : 13/01/2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, encargada de generar un reprote de estadisticas
--%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<% String Mensaje       = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):""; %>
<%
        String accion = CONTROLLER+"?estado=EstCargaPlaca&accion=Manager";
%>
<html>
<head>
<title>EstCargaPlaca</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="<%=BASEURL%>/js/validar.js"></script>
<script src="<%=BASEURL%>/js/date-picker.js"></script>
<script src="<%=BASEURL%>/js/boton.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Generar Reporte Estadisticas Carga Placa"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
    <form action="<%=accion%>" method="post" name="forma" id="forma">
      <table width="332" border="2" align="center">
        <tr>
          <td width="272" class="subtitulo1"><div align="center">Generar Reporte de la Estadisticas de Carga Placa</div></td>
          <td width="42" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="38" height="31"></td>
        </tr>
      </table>
        <div align="center"><br>
        <img src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="forma.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
		<img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >
		</div>
    </form>
    <p>
      <%if(!Mensaje.equals("")){%>
    </p>
    <p>
    <table border="2" align="center">
      <tr>
        <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
              <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
              <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
              <td width="58">&nbsp;</td>
            </tr>
        </table></td>
      </tr>
    </table>
    <p></p>
    <%}%>
</div>
</body>
</html>
