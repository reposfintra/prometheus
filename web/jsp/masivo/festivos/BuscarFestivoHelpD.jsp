<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Descripci&oacute;n de Buscar Festivos</title>

<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body>
<% String BASEIMG = BASEURL +"/images/botones/"; %> 
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">DESCRIPCI&Oacute;N DE BUSCAR FESTIVO</div></td>
        <tr>
          <td  width="123" class="fila"> Fecha Inicial </td>
          <td  width="551" class="ayudaHtmlTexto"> Fecha Inicial a partir de la cual de se buscaran los festivos </td>
        </tr>
        <tr>
          <td  width="123" class="fila"> Fecha Final </td>
          <td  width="551" class="ayudaHtmlTexto"> Fecha Final hasta la cual se buscaran los festivos </td>
        </tr>
        <tr>
          <td width="123"  class="fila"> Pais </td>
          <td width="551"  class="ayudaHtmlTexto"> Debera seleccionar el pais al que corresponde el festivo </td>
        </tr>
	<tr>
          <td width="123"  class="fila"> Bot&oacute;n Aceptar </td>
          <td width="551"  class="ayudaHtmlTexto"> Inicia el proceso de busqueda del festivo </td>
        </tr>
        <tr>
          <td width="123"  class="fila"> Bot&oacute;n Salir </td>
          <td width="551"  class="ayudaHtmlTexto"> Cierra la ventana </td>
        </tr>		
      </table>
    </td>
  </tr>
</table>
<br>
<table width="416" align="center">
	<tr>
	<td align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	</td>
	</tr>
</table>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
