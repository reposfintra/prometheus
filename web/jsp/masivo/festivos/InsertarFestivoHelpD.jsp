<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Descripci&oacute;n de Insertar festivos</title>

<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body>
<% String BASEIMG = BASEURL +"/images/botones/"; %> 
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">DESCRIPCI&Oacute;N DE INGRESAR FESTIVOS</div></td>
        <tr>
          <td  width="123" class="fila"> Fecha </td>
          <td  width="551" class="ayudaHtmlTexto"> Debera digitar la fecha del nuevo festivo </td>
        </tr>
        <tr>
          <td width="123"  class="fila"> Descripcion </td>
          <td width="551"  class="ayudaHtmlTexto"> Debera digitar la descripción del nuevo festivo </td>
        </tr>
        <tr>
          <td width="123"  class="fila"> Pais </td>
          <td width="551"  class="ayudaHtmlTexto"> Debera seleccionar el pais al cual se le relacionara el festivo </td>
        </tr>
	<tr>
          <td width="123"  class="fila"> Bot&oacute;n Aceptar </td>
          <td width="551"  class="ayudaHtmlTexto"> Inicia el proceso de creaci&oacute;n del festivo</td>
        </tr>
	<tr>
          <td width="123"  class="fila"> Bot&oacute;n Cancelar </td>
          <td width="551"  class="ayudaHtmlTexto"> Limpia el formulario</td>
        </tr>
        <tr>
          <td width="123"  class="fila"> Bot&oacute;n Salir </td>
          <td width="551"  class="ayudaHtmlTexto"> Cierra la ventana </td>
        </tr>		
      </table>
    </td>
  </tr>
</table>
<br>
<table width="416" align="center">
	<tr>
	<td align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	</td>
	</tr>
</table>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
