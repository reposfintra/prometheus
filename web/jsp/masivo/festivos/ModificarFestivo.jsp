<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
  String p =(request.getParameter ("pais")!=null)?request.getParameter ("pais"):"";
%>
<html>
<head>
<title>Modificar Festivos</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<body>
<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");    
    String mensaje = (String) request.getParameter("mensaje")==null?"":(String) request.getParameter("mensaje");
%>
<FORM name='forma' id='forma' method='POST' onSubmit="return validarTCamposLlenos();" action=''>
<% if(mensaje.equals("")){
%>  
<input type='hidden' name='fecha_vieja' value=<%=request.getParameter("fecha")%>>
  <table width="416"  border="2" align="center">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr class="titulo" align="center">
            <td colspan="2"><table width="100%"  border="0" cellpadding="0" cellspacing="0" class="barratitulo">
              <tr>
                <td width="56%" class="subtitulo1">Modificar\Anular Festivos</td>
                <td width="10%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="33" height="20"><%=datos[0]%></td>
              </tr>
            </table>            </td>
        </tr>
        <tr class="fila">
            <td width="119">Fecha</td>
            <td> 
                <input type="text" name="fecha" id="fecha" class='textbox' size="10" readonly style='width:100' value="<%=request.getParameter("fecha")%>">
            </td>
        </tr>
        <tr class="fila">
            <td>Descripcion</td>
            <td><input name="descripcion" type="text" class="textbox" id="descripcion" value="<%=request.getParameter("descripcion")%>"></input></td>
        </tr>
        <tr class="fila">
            <td>Pais</td>
             <td><select name="pais" id="pais" value="<%=p%>">
              <option value="CO" <%if(p.equals("CO")){%> selected <%}%>>COLOMBIA</option>
              <option value="VE" <%if(p.equals("VE")){%> selected <%}%>>VENEZUELA</option>
            </select></td>
        </tr>
    </table></td>
    </tr>
  </table>
  <br>
 <center> 
	<img src="<%=BASEURL%>/images/botones/modificar.gif" name="modificar" onClick="forma.action='<%=CONTROLLER%>?estado=Mantenimiento&accion=Festivos&opcion=modificar'; forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	<img src="<%=BASEURL%>/images/botones/anular.gif" name="anular" onClick="forma.action='<%=CONTROLLER%>?estado=Mantenimiento&accion=Festivos&opcion=eliminar'; forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">	
	<img src="<%=BASEURL%>/images/botones/salir.gif" name="salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">               
</center>
<br>
<%}%>
<% if(mensaje.equals("eliminado")){
%>    
<table border="2" align="center">
  <tr>
    <td>
    <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
	<td width="229" align="center" class="mensajes"><strong>Eliminacion exitosa!</strong></td>
        <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
<center>
</br>
<img src="<%=BASEURL%>/images/botones/salir.gif" name="salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">               
</center>
<%}%>
<% if(mensaje.equals("modificado")){
%>    
<table border="2" align="center">
  <tr>
    <td>
    <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
	<td width="229" align="center" class="mensajes"><strong>Modificaci�n exitosa!</strong></td>
        <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
      </tr>
    </table>
    </td>
  </tr>
</table>
<center>
</br>
<img src="<%=BASEURL%>/images/botones/salir.gif" name="salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">               
</center>
<%}%>
</FORM>
<%=datos[1]%>
</body>
</html>