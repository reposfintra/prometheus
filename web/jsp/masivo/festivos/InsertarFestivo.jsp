<!--
     - Author(s)       :      Ing. Luis Eduardo Frieri
     - Date            :      11/01/2007  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->

<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head><title>Insertar Festivos</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
</head>
<body>
<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");    
%>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ingresar Festivo"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<form name="forma" method="post" action="<%=CONTROLLER%>?estado=Mantenimiento&accion=Festivos&opcion=insertar">
 <table border="2" align="center" width="416">
  <tr>
    <td><table width="100%" class="tablaInferior">
      <tr class="titulo" align="center">
        <td colspan="3"><table width="100%"  border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td width="51%" class="subtitulo1">Insertar Festivo </td>
            <td width="49%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="33" height="20" align="left"><%=datos[0]%></td>
          </tr>
        </table>          </td>
      </tr>
      <tr class="fila">
        <td width="108">Fecha</td>
        <td width="159">
            <input type="text" name="fecha" id="fecha" class='textbox' size="10" readonly style='width:100'>
            <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma.fecha);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="Click aqui para escoger la fecha"></a>
        	<img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
		</td>
      </tr>
      <tr class="fila">
        <td>Descripcion</td>
        <td><input name="descripcion" type="text" class="textbox" id="descripcion"></td>
      </tr>
	  <tr class="fila">
        <td>Pais</td>
        <td><select name="pais" id="pais">
              <option value="">SELECCIONE</option>
              <option value="CO">COLOMBIA</option>
              <option value="VE">VENEZUELA</option>
            </select>
        <img  src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
		</td>
      </tr>         
    </table></td>
  </tr>
</table>
<br>
 <center>
   <img src="<%=BASEURL%>/images/botones/aceptar.gif"   name="imgAceptar"   height="21" onMouseOver="botonOver(this);" onClick="validar();" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;  
   <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgCancelar"  height="21" onMouseOver="botonOver(this);" onClick="forma.reset();"  onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; 
   <img src="<%=BASEURL%>/images/botones/salir.gif"     name="imgSalir"     height="21" onMouseOver="botonOver(this);" onClick="window.close();"      onMouseOut="botonOut(this);" style="cursor:hand">
 </center>
</form>
<%String mensaje = (String) request.getParameter("mensaje")==null?"":(String) request.getParameter("mensaje");%>
<%if(mensaje.equals("ok")){%>
<br>
<table border="2" align="center">
  <tr>
    <td>
	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><strong>Insercción Exitosa</strong></td>
        <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%>
<%if(mensaje.equals("wrong")){%>
<br>
<table border="2" align="center">
  <tr>
    <td>
	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><strong>Ya Existe un festivo en la fecha seleccionada¡</strong></td>
        <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%>
</div>
<%=datos[1]%>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>
<script>
function validar(){
		if (( forma.fecha.value == '' ) || ( forma.pais.value == '' )){
			alert( 'Debe Ingresar todos los campos obligatorios para continuar...' );
		}else{
		      forma.submit();
		}	  		
}		
</script>