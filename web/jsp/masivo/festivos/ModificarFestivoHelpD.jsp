<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Descripci&oacute;n de Modificar festivo</title>

<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body>
<% String BASEIMG = BASEURL +"/images/botones/"; %> 
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">DESCRIPCI&Oacute;N DE MODIFICAR UN FESTIVO</div></td>
        <tr>
          <td  width="123" class="fila"> Fecha </td>
          <td  width="551" class="ayudaHtmlTexto"> Indica la fecha del festivo seleccionado </td>
        </tr>
        <tr>
          <td width="123"  class="fila"> Descripcion </td>
          <td width="551"  class="ayudaHtmlTexto"> Muestra la descripccion del festivo seleccionado </td>
        </tr>
        <tr>
          <td width="123"  class="fila"> Pais </td>
          <td width="551"  class="ayudaHtmlTexto"> Indica el pais del festivo seleccionado </td>
        </tr>
	<tr>
          <td width="123"  class="fila"> Bot&oacute;n Modificar </td>
          <td width="551"  class="ayudaHtmlTexto"> Inicia el proceso de actualización del Festivo, si se desea cambiar la fecha del festivo se debe anular el festivo y crear uno nuevo</td>
        </tr>
        <tr>
        <tr>
          <td width="123"  class="fila"> Bot&oacute;n Anular </td>
          <td width="551"  class="ayudaHtmlTexto"> Elimina el festivo seleccionado</td>
        </tr>
        <tr>
          <td width="123"  class="fila"> Bot&oacute;n Salir </td>
          <td width="551"  class="ayudaHtmlTexto"> Cierra la ventana </td>
        </tr>		
      </table>
    </td>
  </tr>
</table>
<br>
<table width="416" align="center">
	<tr>
	<td align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	</td>
	</tr>
</table>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
