<%@page contentType="text/html"%>
<%@page session="true"%>
<%//@page errorPage="/error/error.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head><title>Listar Festivos</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body>
<%  
    Vector vec = model.mantenimientoFestivosService.getVector();      
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int sw = 0;
%>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listado Festivos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<form name="forma" method="post" action="<%=CONTROLLER%>?estado=Mantenimiento&accion=Festivos&opcion=excel">
<% if (vec.size()>0){ %>
<table width="300"  border="2" align="center">
  <tr>
    <td>
	<table width="100%" class="tablaInferior">
      <tr>
        <td colspan="2"><table width="100%"  border="0" cellpadding="0" cellspacing="0" class="barratitulo">
          <tr>
            <td width="33%" class="subtitulo1">Festivos</td>
            <td width="67%"><img src="<%=BASEURL%>/images/titulo.gif" width="33" height="20"></td>
          </tr>
        </table>          </td>
      </tr>
      <tr class="subtitulos" align="center">
	  <td colspan="2">
	  	<table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
	  		<tr class="tblTitulo" align="center">
	         <td width="50%" nowrap >Fecha</td>
	         <td width="50%" nowrap >Pais</td>
    	     <td width="50%" nowrap >Descripcion</td>
       		</tr>
      		<%for (int i = 0; i < vec.size(); i++){
                        sw = 1;
		        Festivos dato = (Festivos) model.mantenimientoFestivosService.getVector().elementAt(i);%>
	        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" title="Modificar Equivalencia..." onClick="window.open('<%=CONTROLLER%>?estado=Mantenimiento&accion=Festivos&opcion=detalle&fecha=<%=dato.getFecha()%>&descripcion=<%=dato.getDescripcion()%>&pais=<%=dato.getPais()%>','myWindow','status=no,scrollbars=no,width=450,height=300,resizable=yes');">
        	<td class="bordereporte" align="center"><%=dato.getFecha()%></td>
        	<td class="bordereporte" align="center"><%=(dato.getPais().equals("VE"))?"VENEZUELA":"COLOMBIA"%></td>
        	<td class="bordereporte" align="center"><%=dato.getDescripcion()%></td>
       	        </tr>
      		<%  }%>
      	        <tr class="pie">
        	<td td height="20" colspan="9" nowrap align="center">  
                </td>
                </tr>
        </table>
	</td>
  </tr>
  </table>
  </td>
  </tr>
  </table>
<br>
<table width="600" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr align="left">
    <td><img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="location.href='<%=BASEURL%>/jsp/masivo/festivos/BuscarFestivo.jsp'">
    <td align="left" width="50%">
    <img src="<%=BASEURL%>/images/botones/iconos/excel.jpg"  name="imgaceptar" onClick="forma.submit();"  style="cursor:hand" height="30"></td>
  </tr>
</table>
<%String mensaje = (String) request.getAttribute("mensaje");%>
<%if(mensaje!=null){%>
<br>
<table border="2" align="center">
  <tr>
    <td>
	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><strong><%=mensaje%></strong></td>
        <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%>
<%}else{%>
<br>
<table border="2" align="center">
  <tr>
    <td>
	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><strong>Su Busqueda no Arrojo resultados</strong></td>
        <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%>
</form>
</div>
</body>
</html>
