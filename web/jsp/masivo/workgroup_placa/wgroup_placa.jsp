<!--
- Date  : 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, permite realizar las funciones basicas sobre la tabla de wgroup-placa
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input"%>
<%
List                ListaGnral  			= 	model.wgroup_placaSvc.getList();
Wgroup_Placa      	Datos       			= 	model.wgroup_placaSvc.getDato();
String              wgroup      			= 	(Datos!=null)?Datos.getWgroup() :"";
String              placa 					= 	(Datos!=null)?Datos.getPlaca():"";
String 				Mensaje       			= 	(request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
String 				NombreBoton   			= 	(Datos==null)?"Guardar":"Modificar";
String 				TNombreBoton   			= 	(Datos==null)?"aceptar":"modificar";
String 				NombreBoton1  			= 	(ListaGnral!=null && ListaGnral.size()>0)?"Ocultar Lista":"Listado";
String 				TNombreBoton1  			= 	(ListaGnral!=null && ListaGnral.size()>0)?"btnOcultar":"detalles";
String 				BloquearText  			= 	(Datos==null)?"":"ReadOnly";
String 				BloquearSelect			= 	"";
String 				BloquearBoton 			=	(Datos!=null)?"":"Disabled";
%>

<html>
<head>
  <title>work_group - placa</title>
  <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>  
  <link href="../css/estilostsp.css" rel='stylesheet'> 
  <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
  <script>
      function validar(form){
            if (form.Opcion.value=='Guardar' || form.Opcion.value=='Modificar'){
                if (form.wgroup.value ==''){
                    alert('Defina el work_group para poder continuar...')
                    return false;
                }
                if (form.placa.value ==''){
                    alert('Defina la placa para poder continuar...')
                    return false;
                }
            }
            return true;
      }
    function SelAll2(){
            for(i=0;i<FormularioListado.length;i++)
                    FormularioListado.elements[i].checked=FormularioListado.All.checked;
    }
    function ActAll2(){
            FormularioListado.All.checked = true;
            for(i=1;i<FormularioListado.length;i++)	
                    if (!FormularioListado.elements[i].checked){
                            FormularioListado.All.checked = false;
                            break;
                    }
    }      
    
    function validarlistado(form){
            for(i=1;i<FormularioListado.length;i++)	
                    if (FormularioListado.elements[i].checked)
                            return true;
            alert('Por favor seleccione un registro para poder continuar');
            return false;
    }      
  </script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado= work_group - placa"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<FORM action="<%=CONTROLLER%>?estado=Wgroup_Placa&accion=Manager" method='POST'  name='formulario'>

    <table width='415' align='center'  border='2'>
	  <tr>
	    <td width="218" align="left" class="subtitulo1">&nbsp;Informacion </td>	 
	    <td width="179" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
      </tr>
	  <tr>	  
       <td colspan="2">
          <table  border='0' width='100%' class="tablaInferior">                    
               <tr class="fila">
                    <td width='29%'>&nbsp work_group :</td>
                    <td><% TreeMap work = model.tblgensvc.getWork_group(); %>
        			<input:select name="wgroup" options="<%=work%>" attributesText="style='width:84%;' class='listmenu'"/>					
                 <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"> </td>                        
               </tr>
               <tr class="fila">
                  <td width='29%'>&nbsp placa :
                    <input type='hidden' name='original' value="<%=wgroup+","+placa%>"/></td>
                 <td ><input type='text' name='placa' maxlength='12' class="textbox" size="12" style="text-transform:uppercase">
                 <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"> </td>
              </tr>               
          </table>
      </td>
      </tr>
        <tr>           
        </tr>
  </table>
  <br>
    <table align="center">
  		<td colspan="2" align="center"> 
		<img src="<%=BASEURL%>/images/botones/restablecer.gif"  name="imgrestablecer" onClick="Opcion.value='Nuevo';formulario.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
		<img src="<%=BASEURL%>/images/botones/<%=TNombreBoton%>.gif"  name="imgsalir" onClick="Opcion.value='<%=NombreBoton%>';if(validar(formulario)){formulario.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
		<img src="<%=BASEURL%>/images/botones/<%=TNombreBoton1%>.gif"  name="imgdetalles" onClick="Opcion.value='<%=NombreBoton1%>';if(validar(formulario)){formulario.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
		<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
		</td>
    </table>
    <br>
<input type='hidden' name='Opcion'/> 
</FORM>
<script> 
formulario.wgroup.value = '<%= wgroup %>';
formulario.placa.value = '<%= placa %>';
</script>



<center class='comentario'>


 <%if(!Mensaje.equals("")){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%}%>


<%  if(ListaGnral!=null && ListaGnral.size()>0) { %>
    <form action="<%=CONTROLLER%>?estado=Wgroup_Placa&accion=Manager" method='post' name='FormularioListado'>
        <br>
        <tr><th height='28' colspan='7'>
		<table width="460" border="2">
          <tr>
            <td width="448"><table width="100%">
              <tr>
                <td width="74%" class="subtitulo1">LISTADO GENERAL DE REGISTROS</td>
                <td width="26%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
            </table>
			<table bgcolor="#F7F5F4" bordercolor="#999999" border="1" width="100%">
		<tr class="tblTitulo" align="center">
            <th width='19' align="center">N�</th>
            <th width='21' align="center"><input type='checkbox' name='All' onclick='jscript: SelAll2();'></th>
            <th width='58' align="center">WORK_GROUP</th>
            <th width='195' align="center">PLACA</th>
 
        </tr>
        <%
            int Cont = 1;
            Iterator it2 = ListaGnral.iterator();
            while(it2.hasNext()){
                Wgroup_Placa dat = (Wgroup_Placa) it2.next();
				String e = (Cont % 2 == 0 )?"filaazul":"filagris";
                String Estilo = (dat.getReg_status().equals("A"))?"filaresaltada":e;
                %>
        <tr class='<%= Estilo %>'>
            <td  align='center' class="bordereporte"><span class='comentario'><%=Cont++%></span></td>
            <td width="21" align="center" class="bordereporte"><input type='checkbox' name='LOV' value='<%= dat.getWgroup()+","+dat.getPlaca() %>' onclick='jscript: ActAll2();'></td>
            <td width="58" align='center' class="bordereporte"><a href='<%=CONTROLLER%>?estado=Wgroup_Placa&accion=Manager&Opcion=Seleccionar&original=<%= dat.getWgroup()+","+dat.getPlaca() %>'><%= dat.getWgroup() %></a></td>
            <td width="195" align="center" class="bordereporte"><%= dat.getPlaca()%></td>
 
        </tr>
        <%  }   %>        
        </table></td>
          </tr>
        </table></th>
        </tr>

        <p><br>
            <input type='hidden' name='Opcion'/>             
</p>
        <table align="center">
  		<td colspan="2" align="center"> 
  			<img src="<%=BASEURL%>/images/botones/anular.gif"  name="imganular" onClick="Opcion.value='Anular';if(validarlistado(FormularioListado)){FormularioListado.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
  			<img src="<%=BASEURL%>/images/botones/activar.gif"  name="imgsalir" onClick="Opcion.value='Activar';if(validarlistado(FormularioListado)){FormularioListado.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
			<img src="<%=BASEURL%>/images/botones/eliminar.gif"  name="imgeliminar" onClick="Opcion.value='Eliminar';if(validarlistado(FormularioListado)){FormularioListado.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
			</td>
        </table>
        <p>&nbsp;        </p>
    </form>
<%  } %>
</div>
</center>
</body>
</html>
