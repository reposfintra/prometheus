<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Definici&oacute;n otros conceptos</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<script src="<%=BASEURL%>/js/validar.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
</head>
<body>
<%
        Concepto concepto = (Concepto) session.getAttribute("conceptoDefinir");
        if ( concepto == null) concepto = new Concepto();
        String msg = request.getParameter("msg");
		if ( msg==null ) msg = "";
%>
<%
		if(!msg.matches("MsgAgregado")){
%>
<form name="forma" id='forma' onSubmit='return validarTCamposLlenos();' method="post" action="<%=CONTROLLER + "?estado=OtrConceptos&accion=Definir&cmd=show"%>" >
  <table width="444" height="87" border="2" align="center">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td width="50%" class="subtitulo1">Informaci&oacute;n</td>
          <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>      <table width="100%"  border="0" align="center" class="tablaInferior">
        <tr class="fila">
          <td colspan="2">Ha seleccionado otro/a <%= request.getParameter("campo") %>, especifique cual: 
            <input name="tabla" type="hidden" id="tabla" value="<%= (request.getParameter("tabla")==null)? concepto.getC_tabla() : request.getParameter("tabla") %>">
            <input name="campo" type="hidden" id="campo" value="<%= request.getParameter("campo") %>">
            <%
          String link = CONTROLLER + "?estado=OtrConceptos&accion=Consultar&cmd=show";
		  %></td>
          </tr>
        <tr class="fila">
          <td width="27%">Descripci&oacute;n</td>
          <td width="73%"><input name="c_descripcion" type="text" class="textbox" id="c_descripcion" size="50" maxlength="40" value="<%= concepto.getC_descripcion()%>"></td>
        </tr>
      </table></td>
    </tr>
  </table>
  <div align="center"><br>
    <img src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="if ( validarTCamposLlenos() ) forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">&nbsp;  <img src="<%= BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);"  style="cursor:hand ">&nbsp; <img src="<%= BASEURL%>/images/botones/detalles.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.open('<%= link %>','','status=yes,scrollbars=yes,width=780,height=450,resizable=yes');" onMouseOut="botonOut(this);"  style="cursor:hand ">&nbsp; <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);"  style="cursor:hand "> </div>
  </form>
<%				
                if(request.getParameter("msg")!=null){
%>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes"><%=request.getParameter("msg").toString() %></td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>	
<%				}
		}
		else{
%>
<table border="2" align="center">
  <tr>
    <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="282" align="center" class="mensajes">Se ha agregado el nuevo concepto exitosamente.&nbsp;&nbsp;<a href="javaScript:parent.close()" class="letraresaltada">CERRAR</a></td>
          <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="78">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>

<%
		}
%>
</body>
</html>
