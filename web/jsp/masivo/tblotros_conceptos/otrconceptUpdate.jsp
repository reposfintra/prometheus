<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Definici&oacute;n otros conceptos</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<%
        String url = CONTROLLER + "?estado=OtrConceptos&accion=Anular&cmd=show";
%>
<script>        
        function anular(){
                forma.action = '<%=url%>';
                forma.submit();
        }
</script>
<script src="<%=BASEURL%>/js/validar.js"></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body <%if(request.getParameter("mensaje").equalsIgnoreCase("MsgModificado") ){ %>onLoad="parent.opener.location.reload();"<%}%>>
<%
        Concepto concepto = (Concepto) session.getAttribute("conceptoUpdate");
        if ( concepto == null) concepto = new Concepto();
		
		String estado = request.getParameter("estado");
        if ( estado == null ) estado = "";
		
        if( !estado.matches("Anulado") ){  		
%>
<form name="forma" id='forma' onSubmit='return validarTCamposLlenos();' method="post" action="<%=CONTROLLER + "?estado=OtrConceptos&accion=Update&cmd=show"%>" >
  <table width="444" height="87" border="2" align="center">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td width="50%" class="subtitulo1">Informaci&oacute;n</td>
          <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20">
            <input name="tabla" type="hidden" id="tabla" value="<%= concepto.getC_tabla() %>">
            <input name="descripcion" type="hidden" id="descripcion" value="<%= concepto.getC_descripcion() %>"></td>
        </tr>
      </table>      <table width="100%"  border="0" align="center" class="tablaInferior">
        <tr class="fila">
          <td>Tabla</td>
          <td><input name="c_tabla" value="<%= concepto.getC_tabla() %>" type="text" class="textbox" id="c_tabla" size="40" maxlength="30">
            <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
        </tr>
        <tr class="fila">
          <td width="27%">Descripci&oacute;n</td>
          <td width="73%"><input name="c_descripcion" value="<%= concepto.getC_descripcion() %>" type="text" class="textbox" id="c_descripcion" size="50" maxlength="40">
            <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
        </tr>
      </table></td>
    </tr>
  </table>
  <div align="center"><br>
    <img src="<%=BASEURL%>/images/botones/modificar.gif" name="c_modificar" onClick="if( validarTCamposLlenos() ) forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">&nbsp; <img src="<%=BASEURL%>/images/botones/anular.gif" name="c_anular" onClick="forma.action='<%= url%>'; forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">&nbsp; <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_buscar" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand "> </div>
  </form>
<%                
                if(request.getParameter("msg")!=null){
%>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes"><%=request.getParameter("msg")%></td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>	
<%
                }
        }
        else{
%>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Concepto anulado exitosamente.</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <br>
  <table width="415" border="0" align="center" cellpadding="0" cellspacing="0">
    <tr align="left">
      <td><img src="<%=BASEURL%>/images/botones/salir.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="parent.close();">   
    </tr>
  </table>
  <%
        }
%>
</body>
</html>
