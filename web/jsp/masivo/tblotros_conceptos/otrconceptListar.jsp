<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
 
<html>
<head>
<title>Listado de Proveedor</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
</head>

<body>
<p>&nbsp;</p>
<p>
<%  
        String style = "simple";
        String position =  "bottom";
        String index =  "center";
        int maxPageItems = 15;
        int maxIndexPages = 10;
        Vector vec = model.otros_conceptosSvc.listarConceptos();        
        Concepto con;

	if ( vec.size() > 0 ){  
%>
</p>
<table width="90%" border="2" align="center">
    <tr>
      <td>
	  <table width="100%">
              <tr>
                <td width="373" class="subtitulo1">&nbsp;Informaci&oacute;n</td>
                <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
              </tr>
        </table>
		  <table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4">
          <tr class="tblTitulo">
            <td width="14%" align="center">Tabla</td>
          <td width="24%" align="center">Descripci&oacute;n</td>
          </tr>
        <pg:pager
         items="<%= vec.size()%>"
         index="<%= index %>"
         maxPageItems="<%= maxPageItems %>"
         maxIndexPages="<%= maxIndexPages %>"
         isOffset="<%= true %>"
         export="offset,currentPageNumber=pageNumber"
         scope="request">
        <%-- keep track of preference --%>
<%
      for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vec.size()); i < l; i++)
	  {
          con = (Concepto) vec.elementAt(i);
          String link = CONTROLLER + "?estado=OtrConceptos&accion=Obtener&cmd=show&tabla=" + con.getC_tabla() +
                            "&desc=" + con.getC_descripcion() + "&mensaje=";	
%>
        <pg:item>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand"
        onClick="window.open('<%= link %>','','status=yes,scrollbars=yes,width=780,height=450,resizable=yes');" >
          <td width="14%"  class="bordereporte" ><%= con.getC_tabla()%></td>
          <td width="24%"  class="bordereporte" ><%= con.getC_descripcion()%></td>
        </tr>
        </pg:item>
        <%}%>
        <tr   class="bordereporte" >
          <td td height="20" colspan="2" nowrap align="center">
		   <pg:index>
            <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
           </pg:index> 
	      </td>
        </tr>
        </pg:pager>
      </table></td>
    </tr>
</table>
<p>
      <%}
 else { %>
</p>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p>&nbsp; </p>
<%}%>
<br>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr align="left">
    <td><img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="parent.close();" style="cursor:hand "> 
  </tr>
</table>
</body>
</html>
