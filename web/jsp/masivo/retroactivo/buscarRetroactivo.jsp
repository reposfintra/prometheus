<!-- 
- Autor : Ing. Diogenes Bastidas Morales
- Date  : 4 de febrero de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, captura la informacion para la busqueda 
--%>

<%-- Declaracion de librerias--%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Buscar Retroactivo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="<%=BASEURL%>/js/validar.js"></script>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Buscar Retroactivo"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<%
   String mensaje = (request.getParameter("mensaje")!=null)?request.getParameter("mensaje"):"";
   int sw = 0;
   if (mensaje.equals("MsgAgregado")){
	 mensaje = "Retroactivo ingresado exitosamente!";
   }
   else if (mensaje.equals("MsgErrorIng")){
	 mensaje = "Error...Retroactivo ya existe!";
	 sw = 1;
   }
   TreeMap rutas = model.stdjobdetselService.getTreeMapStdSel();
%>
    <form action="<%=CONTROLLER%>?estado=Retroactivo&accion=Busqueda" method="post" name="forma" id="forma" onSubmit="return validarTCamposLlenos();">
      <table width="400" border="2" align="center">
        <tr>
          <td><table width="100%" class="tablaInferior">
            <tr>
              <td width="50%" class="subtitulo1">Parametos de busqueda </td>
              <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20">
              </td>
            </tr>
          </table>
            <table width="100%" border="0" align="center" class="tablaInferior">
              <tr class="fila" >
                <td width="34%">Standar</td>
                <td width="66%"><input:select name="stdjob" options="<%= rutas %>" attributesText="style='width:90%;' class='listmenu'" default="<%= "TR" %>" /></td>
              </tr>
              <tr class="fila" >
                <td>Ruta </td>
                <td><input name="ruta" type="text" id="ruta" size="15" maxlength="10"></td>
              </tr>
            </table></td>
        </tr>
      </table>
     </br>
      <div align="center">
	    <img src="<%=BASEURL%>/images/botones/buscar.gif"  name="imgbuscar" onClick="forma.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;       <img src="<%=BASEURL%>/images/botones/detalles.gif"  name="imgdetalles"  onMouseOver="botonOver(this);" onClick="window.location='<%=CONTROLLER%>?estado=Retroactivo&accion=Busqueda&stdjob=&ruta='" onMouseOut="botonOut(this);" style="cursor:hand"> &nbsp;
        <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" style="cursor:hand">
    </div>
    </form>
	
	
</div>  
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"></iframe>  
</body>
</html>
