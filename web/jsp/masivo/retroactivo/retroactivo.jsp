<!-- 
- Autor : Ing. Diogenes Bastidas Morales
- Date  : 4 de febrero de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, captura la informacion de retroactivo
--%>

<%-- Declaracion de librerias--%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Registrar Retroactivo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="<%=BASEURL%>/js/validar.js"></script>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<script SRC='<%=BASEURL%>/js/ingreso.js'></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Registrar Retroactivo"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<%
   String mensaje = (request.getParameter("mensaje")!=null)?request.getParameter("mensaje"):"";
   int sw = 0;
   if (mensaje.equals("MsgAgregado")){
	 mensaje = "Retroactivo ingresado exitosamente!";
   }
   else if (mensaje.equals("MsgErrorIng")){
	 mensaje = "Error...Retroactivo ya existe!";
	 sw = 1;
   }

   TreeMap rutas = model.stdjobdetselService.getTreeMapStdSel();
   rutas.remove("Todas las rutas.");

%>
    <form action="<%=CONTROLLER%>?estado=Retroactivo&accion=Insertar" method="post" name="forma" id="forma" >
      <table width="500" border="2" align="center">
        <tr>
          <td><table width="100%" class="tablaInferior">
            <tr>
              <td width="50%" class="subtitulo1">Información </td>
              <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20">
              </td>
            </tr>
          </table>
            <table width="100%" border="0" align="center" class="tablaInferior">
              <tr class="fila">
                <td width="29%">Fecha inicial</td>
                <td width="71%">
                  <input name='fechai' type='text' class="textbox" id="fechai" style='width:120' value='' readonly>
                  <a href="javascript:void(0);" class="link" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.forma.fechai);return false;"  HIDEFOCUS > <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16"
               border="0" alt="De click aqu&iacute; para ver el calendario."></a> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
              </tr>
              <tr class="fila" >
                <td>Fecha final</td>
                <td>
                  <input name='fechaf' type='text' class="textbox" id="fechaf" style='width:120' value='' readonly>
                  <a href="javascript:void(0);" class="link" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.forma.fechaf);return false;"  HIDEFOCUS > <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16"
               border="0" alt="De click aqu&iacute; para ver el calendario."></a> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
              </tr>
              <tr class="fila" >
                <td>Standar</td>
				   
                <td><input:select name="stdjob" options="<%= rutas %>" attributesText="style='width:95%;' class='listmenu'" /><img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
              </tr>
              <tr class="fila" >
                <td>Ruta </td>
                <td><input name="ruta" type="text" id="ruta" size="15" maxlength="10">
                <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
              </tr>
              <tr class="fila" >
                <td>Valor Standar </td>
                <td><input name="vlr_stbjob" type="text" id="vlr_stbjob" onKeyPress="soloDigitos(event,'decOK')" size="15" maxlength="8" onChange="formatoDolar(this,2)">
                <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
              </tr>
              <tr class="fila" >
                <td>Valor Retroactivo </td>
                <td><input name="vlr_ret" type="text" id="vlr_ret" onKeyPress="soloDigitos(event,'decOK')" size="15" maxlength="8" onChange="formatoDolar(this,2)">
                <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
              </tr>
            </table></td>
        </tr>
      </table>
      <div align="center"><br>
        <img src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="validarRetroactivo();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">&nbsp; <img src="<%= BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);"  style="cursor:hand ">&nbsp; <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);"  style="cursor:hand "></div>
    </form>
	
	<%if( !mensaje.equalsIgnoreCase("")){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="262" align="center" class="mensajes"><%=mensaje%></td>
        <td width="32" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="44">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%>
</div>  
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins_24.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"></iframe>  
</body>
</html>
