<!--
- Autor : Ing. Diogenes Bastidas Morales
- Date  : 4 de febrero de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que lista los retroactivos
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>

<html>
<head>
<title>Listado Retroactivos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Retroactivos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
  <%String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;

    Vector Vect = model.retroactivoService.getRetroactivos();
    Retroactivo rect; 
	if ( Vect.size() >0 ){ 
  %>
  <table width="90%" border="2" align="center">
    <tr>
      <td><table width="100%"  border="0">
        <tr>
          <td width="48%" class="subtitulo1"><span class="subtitulos">Informaci&oacute;n</span></td>
          <td width="52%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
        </tr>
      </table>
        <table width="99%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
        <tr class="tblTitulo" align="center">
          <td width="78" >Ruta</td>
          <td width="139" >Standar</td>
          <td width="85"  >Inicio</td>
          <td width="67"  >Final</td>
          <td width="86"  >Valor Standar </td>
          <td width="99"  >Valor Retroactivo </td>
        </tr>
        <pg:pager
    items="<%=Vect.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
        <%-- keep track of preference --%>
        <%
        for (int i = offset.intValue(), l = Math.min(i + maxPageItems, Vect.size()); i < l; i++) {
            rect = (Retroactivo) Vect.elementAt(i);%>
        <pg:item>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand"  
        onClick="window.open('<%=CONTROLLER%>?estado=Retroactivo&accion=Buscar&pagina=retroactivo.jsp&carpeta=jsp/masivo/retroactivo&stdjob=<%=rect.getStd_job_no()%>&ruta=<%=rect.getRuta()%>&fecha1=<%=rect.getFecha1()%>&fecha2=<%=rect.getFecha2()%>' ,'Mod','status=yes,scrollbars=no,width=650,height=650,resizable=yes');">
          <td height="20" class="bordereporte" ><%=rect.getRuta()%></td>
          <td height="20" class="bordereporte"><%=rect.getStd_job_no()%> <%=rect.getDesStdjob()%></td>
          <td height="20" class="bordereporte"><%=rect.getFecha1()%></td>
          <td height="20" class="bordereporte"><%=rect.getFecha2()%></td>
          <td height="20" class="bordereporte" align="right"><%=UtilFinanzas.customFormat2(rect.getValor_stdjob())%></td>
          <td height="20" class="bordereporte" align="right"><%=UtilFinanzas.customFormat2(rect.getValor_retro())%></td>
        </tr>
        </pg:item>
        <%}%>
        <tr class="bordereporte">
          <td colspan="6" align="center" valign="middle" >
            <div align="center"><pg:index>
              <jsp:include page="../../../WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
    </pg:index></div></td>
        </tr>
        </pg:pager>
      </table></td>
    </tr>
  </table>
  <%}
 else { %>
 <table border="2" align="center">
    <tr>
      <td><table width="410" height="40" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <%}%>
  <br>
  <table width="90%" border="0" align="center">
   <tr>
     <td><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgaceptar" onclick="window.location='<%=CONTROLLER%>?estado=Idioma&accion=Cargar&pagina=buscarRetroactivo.jsp&ruta=/jsp/masivo/retroactivo/'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
   </tr>
 </table>

</div>
</body>
</html>
