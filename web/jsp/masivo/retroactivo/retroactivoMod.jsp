<!-- 
- Autor : Ing. Diogenes Bastidas Morales
- Date  : 4 de febrero de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, captura la informacion de retroactivo
--%>

<%-- Declaracion de librerias--%>
<%@ page session="true"%>
<%@page import ="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Modificar Retroactivo</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="<%=BASEURL%>/js/validar.js"></script>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<script SRC='<%=BASEURL%>/js/ingreso.js'></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body onResize="redimensionar()" <%= ( request.getParameter("reload")!=null ) ?"onLoad='redimensionar();window.opener.location.reload();'" : "onLoad='redimensionar();'"%>>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Modificar Retroactivo"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<%
   String mensaje = (request.getParameter("mensaje")!=null)?request.getParameter("mensaje"):"";
   int sw = 0;
   if (mensaje.equals("MsgModificado")){
	 mensaje = "Retroactivo modificado exitosamente!";
   }
   Retroactivo rect= model.retroactivoService.getRetroactivo();
   TreeMap rutas = model.stdjobdetselService.getTreeMapStdSel();
   rutas.remove("TR");

%>
    <form action="<%=CONTROLLER%>?estado=Retroactivo&accion=Modificar" method="post" name="forma" id="forma">
      <table width="500" border="2" align="center">
        <tr>
          <td><table width="100%" class="tablaInferior">
            <tr>
              <td width="50%" class="subtitulo1">Información </td>
              <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20">
              </td>
            </tr>
          </table>
            <table width="100%" border="0" align="center" class="tablaInferior">
              <tr>
                <td width="40%" class="fila">Fecha inicial</td>
                <td width="60%" class="letra"><%=rect.getFecha1()%>
                <input name="fechai" type="hidden" id="fechai" value="<%=rect.getFecha1()%>"></td>
              </tr>
              <tr>
                <td class="fila">Fecha final</td>
                <td class="letra"><%=rect.getFecha2()%> <input name="fechaf" type="hidden" id="fechaf" value="<%=rect.getFecha2()%>"></td>
              </tr>
              <tr>
                <td class="fila" >Standar</td>
				   
                <td class="letra"><%=rect.getStd_job_no()%> <%=rect.getDesStdjob()%>
                <input name="stdjob" type="hidden" id="stdjob" value="<%=rect.getStd_job_no()%>"></td>
              </tr>
              <tr>
                <td class="fila">Ruta </td>
                <td class="letra"><%=rect.getRuta()%>
                <input name="ruta" type="hidden" id="ruta" value="<%=rect.getRuta()%>"></td>
              </tr>
              <tr class="fila" >
                <td>Valor Standar </td>
                <td><input name="vlr_stbjob" type="text" id="vlr_stbjob" onKeyPress="soloDigitos(event,'decOK')" value="<%=UtilFinanzas.customFormat2(rect.getValor_stdjob())%>" size="15" maxlength="8" onChange="formatoDolar(this,2)">
                <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
              </tr>
              <tr class="fila" >
                <td>Valor Retroactivo </td>
                <td><input name="vlr_ret" type="text" id="vlr_ret" onKeyPress="soloDigitos(event,'decOK')" value="<%=UtilFinanzas.customFormat2(rect.getValor_retro())%>" size="15" maxlength="8" onChange="formatoDolar(this,2)">
                <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
              </tr>
            </table></td>
        </tr>
      </table>
      <div align="center"><br>
        <img src="<%= BASEURL%>/images/botones/modificar.gif"  name="imgaceptar" onClick="validarRetroactivo();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">&nbsp; 
		<img src="<%=BASEURL%>/images/botones/anular.gif"  name="imganular" onClick="window.location='<%=CONTROLLER%>?estado=Retroactivo&accion=Anular&fechai=<%=rect.getFecha1()%>&fechaf=<%=rect.getFecha2()%>&stdjob=<%=rect.getStd_job_no()%>&ruta=<%=rect.getRuta()%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; 
		<img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);"  style="cursor:hand "></div>
    </form>
	
	<%if( !mensaje.equalsIgnoreCase("")){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="262" align="center" class="mensajes"><%=mensaje%></td>
        <td width="32" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="44">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<%}%>
</div>  
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"></iframe>  
</body>
</html>
