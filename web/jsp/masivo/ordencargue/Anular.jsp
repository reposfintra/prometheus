<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Anular Orden de carga</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/validarOCargue.js">
</script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</script>

<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">

</head>

<body >
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Anular Orden de Carga"/>
</div>

 <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
 <%
 	//Datos de la orden de carga
	String standard="";
	String fecha="";
	String remitente="";
	String placa="";
	String trailer="";
	String c1="";
	String c2="";
	String pc1="";
	String pc2="";
	String p1="";
	String p2="";
	String p3="";
	String p4="";
	String p5="";
	String contenido="";
	String entregar="";
	String observacion="";
	String orden="";
	String conductor="";	
	if(model.imprimirOrdenService.getHojaOrden()!=null){
		HojaOrdenDeCarga oc = model.imprimirOrdenService.getHojaOrden();
		standard=oc.getStd_job_no();
		orden = oc.getOrden();
		fecha=oc.getCreation_date();
		remitente=oc.getRemitente();
		placa=oc.getPlaca();
		conductor=oc.getConductor();
		trailer=oc.getTrailer();
		if(oc.getContenedores()!=null){
			String vec[] =oc.getContenedores().split(",");
			c1=vec.length>0?vec[0]:"";
			c2=vec.length>1?vec[1]:"";
		}
		pc1=oc.getPrecintoc1();
		pc2=oc.getPrecintoc2();
		p1=oc.getPrecinto1();
		p2=oc.getPrecinto2();
		p3=oc.getPrecinto3();
		p4=oc.getPrecinto4();
		p5=oc.getPrecinto5();
		contenido=oc.getContenido();
		entregar=oc.getEntregar();
		observacion=oc.getObservacion();
	}
	
			  String origen="";
			  model.stdjobdetselService.searchStdJob(standard);
			  Stdjobdetsel sj=new Stdjobdetsel();
			   if(model.stdjobdetselService.getStandardDetSel()!=null){
				sj = model.stdjobdetselService.getStandardDetSel();
			    origen = sj.getOrigin_code();
			//out.println(origen);
            }
	
 %>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=OCarga&accion=Anular" >
  <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
  <%java.util.Date date = new java.util.Date();
    SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd hh:mma");
	String fecpla = s.format(date);   
               %>
  <table width="80%"  border="2" align="center">
    <tr>
      <td>
	    <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="22" colspan=2 class="subtitulo1">Informacion Orden de Carga <%=orden%>
              <input name="orden" type="hidden" id="orden" value="<%=orden%>"></td>
            <td width="304" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
          </tr>
        </table>
	    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td height="22" class="subtitulo1">Cliente</td>
          </tr>
      </table>                  
	  <table width="100%" class="tablaInferior">
        <tr class="letra">
          <td width="20%" class="filaresaltada">Estandar Job
              </td>
			 
          <td colspan="4" class="letra">
            
            <%=sj.getSj()+"-"+sj.getSj_desc()%>
</td>
          </tr>
        <tr class="letra">
          <td scope="row" class="filaresaltada">Remitente</td>
          <td scope="row"><%=remitente%></td>
          <td width="16%" scope="row" class="filaresaltada">FECHA DE CARGUE</td>
          <td width="21%" colspan="2" scope="row">
             
			 <%=fecha.substring(0,16)%>
</td>
          </tr>
		</table>
		      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tablaInferior">
          <tr>
            <td height="22" class="subtitulo1">Datos Planilla </td>
            </tr>
        </table>		
		<table width="100%" class="tablaInferior">
        <tr class="letra">
          <td height="26" colspan="2" scope="row" class="filaresaltada">Placa</th>
          <td colspan="2"><%=placa%></td>
          <td class="filaresaltada">Conductor</td>
          <td><%=conductor%></td>
        </tr>
        <tr class="letra">
          <td width="61" rowspan="2" class="filaresaltada" scope="row">Trailer</td>
          <td width="66" rowspan="2"  scope="row">
            <%=trailer%>
			</td>
          <td width="86" rowspan="2" class="filaresaltada">Contenedores</td>
          <td width="88">&nbsp;<%=c1%></td>
          <td width="96" rowspan="2" class="filaresaltada">Precintos de Contedores</td>
          <td width="158"><%=pc1%></td>
        </tr>
        <tr class="letra">
          <td width="88" height="31">&nbsp;<%=c2%></td>
          <td width="158"><%=pc2%></td>
        </tr>
		</table>		
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tablaInferior">
          <tr>
            <td height="22" class="subtitulo1">Otros Precintos </td>
            </tr>
        </table>		
		<table width="100%" height="44" class="tablaInferior">
          <tr class="letra">
            <td width="20%"> <div align="center">
                  <%=p1%>
                </div>
		    </td><td ><div align="center">
                <%=p2%>
              </div></td>
           <td width="20%">
		   <div align="center">
              <%=p3%>
            </div></td>
            <td width="20%"><div align="center">
              <%=p4%>
            </div></td>
            <td width="20%"><div align="center">
              <%=p5%>
            </div></td>
          </tr>
        </table>
        <table width="100%" class="tablaInferior">
          <tr>
            <td height="22" class="subtitulo1">Observaciones</td>
            </tr>
        </table>       
        <table width="100%" class="tablaInferior">
          <tr class="letra">
            <td width="55%"  align="center" valign="top" scope="row" class="filaresaltada">CONTENIDO</td>
            <td  class="filaresaltada" scope="row" align="center">ENTREGAR A
		      </td>
            </tr>
          <tr class="letra">
            <td  align="center" valign="top" scope="row"><%=contenido%></td>
            <td scope="row" align="center"><%=entregar%></td>
          </tr>
          <tr class="filaresaltada">
            <td colspan="2" align="center" valign="top" scope="row">OBSERVACIONES</td>
            </tr>
          <tr class="letra">
            <td colspan="2" align="center" valign="top" scope="row"><%=observacion%></td>
            </tr>
        </table>	    </td>
	  </tr>
    </table>
<div align="center"><br>
            <img src="<%=BASEURL%>/images/botones/regresar.gif" width="101" height="21"  onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);" style="cursor:hand " onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/masivo/ordencargue/&pagina=InicioAnular.jsp&marco=no'">
          <input name="imageField" type="image" src="<%=BASEURL%>/images/botones/anular.gif" width="90" height="21" border="0" onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);" onClick="form1.submit();this.disabled=true">
        <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">        </div>
        <p align="center">&nbsp;           </p>
</form>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins_24.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
<%=datos[1]%>
</body>
</html>
