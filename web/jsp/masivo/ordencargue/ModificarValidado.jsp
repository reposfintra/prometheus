<%@ page session="true"%>
<%@ page errorPage="error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*,com.tsp.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%  
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Orden de carga</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/validarOCargue.js">
</script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</script>

<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">

</head>
<%String accion = CONTROLLER+"?estado=Colpapel&accion=Validar&cmd=show";
	accion = request.getParameter("precinto")!=null?accion+"&precinto=ok":accion;
	accion = request.getParameter("precintoc")!=null?accion+"&precintoc=ok":accion;
	Usuario usuario = (Usuario) session.getAttribute("Usuario");
  	String dis= request.getParameter("precintoc")!=null?"disabled":"";
	String destinatario = request.getParameter("destinatarios");
	String orden = request.getParameter("orden");
%>

<body <%if(request.getAttribute("mensaje")!=null){%>onLoad="alert('<%=(String)request.getAttribute("mensaje")%>');"<%}%> onLoad="form1.placa.focus()" >
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modificar Orden de Cargue"/>
</div>

 <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<form name="form1" method="post" action="<%=CONTROLLER%>?estado=OCarga&accion=Validar&modificar=show" onSubmit="return  ValidarColpapel(this);">
 <p align="center">&nbsp;    </p>
 
 <table width="80%"  border="2" align="center">
   <tr>
     <td>
       <table width="100%" border="0" cellpadding="0" cellspacing="0">
         <tr>
           <td height="22" colspan=2 class="subtitulo1">Informacion Orden de Carga <%=request.getParameter("orden")%>
             <input name="orden" type="hidden" id="orden" value="<%=request.getParameter("orden")%>"></td>
           <td width="507" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
         </tr>
       </table>
       <table width="100%" border="0" cellpadding="0" cellspacing="0">
         <tr>
           <td height="22" class="subtitulo1">Cliente</td>
         </tr>
       </table>
       <table width="100%">
         <tr class="letra">
           <td width="20%" class="filaresaltada" scope="row">Estandar Job </td>
           <% 
	  Stdjobdetsel sj=new Stdjobdetsel();
	  String standard = request.getParameter("standard");
	  String origen ="";
	  
      if(model.stdjobdetselService.getStandardDetSel()!=null){
			sj = model.stdjobdetselService.getStandardDetSel();
			origen = sj.getOrigin_code();
			//out.println(origen);
            }%>
           <td colspan="4" class="letra"> <span class="Estilo17"> <%=request.getParameter("sj_nombre")%> </span>
               <input name="standard" type="hidden" id="standard" value="<%=request.getParameter("standard")%>">
               <input name="sj_nombre" type="hidden" id="sj_nombre" value="<%=request.getParameter("sj_nombre")%>">
               <span class="Estilo6">
               <input name="docudest" type="hidden" id="docudest">
               </span></td>
         </tr>
         <tr class="fila">
           <th colspan="2" scope="row"><div align="left"><span class="Estilo6">
               <input name="remitentes" type="hidden" id="remitentes" value="<%=request.getParameter("remitentes")%>">
               <a class="Simulacion_Hiper" style="cursor:hand " onClick="abrirPagina('<%=BASEURL%>/colpapel/remitentes.jsp?sj='+form1.standard.value+'&origen='+form1.origstd.value,'');">
               <input name="imagenre" type="hidden" id="imagenre3" value="<%=request.getParameter("imagenre")%>">
               <input name="origstd" type="hidden" id="origstd" value="<%=request.getParameter("origstd")%>">
               Modificar Remitentes</a>
               
               <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> <img src=<%=request.getParameter("imagenre")%> width="17" height="17" id="imre">  
			                   <a class="Simulacion_Hiper" style="cursor:hand " onClick="abrirPagina('<%=CONTROLLER%>?estado=Buscar&accion=Destinatarios&sj='+form1.standard.value+'&ocargue=<%=orden%>&destinatario=<%=destinatario%>','');">Modificar Destinatarios</a>
							    <img src="<%=request.getParameter("imagendest")%>" width="17" height="17" id="imdest">
                  <input name="imagendest" type="hidden" id="imagendest" value="<%=request.getParameter("imagendest")%>">
                  <input name="destinatarios" type="hidden" id="destinatarios3" value="<%=request.getParameter("destinatarios")%>" >
</span></div></th>
           <th width="15%" scope="row"><span class="filaresaltada">FECHA DE CARGUE</span></th>
           <th width="29%" colspan="2" scope="row"><div align="left"><span class="letra">
               <input name="fechadesp" type="text" id="fechadesp" size="18" value="<%=request.getParameter("fechadesp")%>" readonly>
<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fechadesp);return false;" HIDEFOCUS>
                     <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></img></a>           </span></div></th>
         </tr>
       </table>
       <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tablaInferior">
         <tr>
           <td height="22" class="subtitulo1">Datos Planilla </td>
         </tr>
       </table>
       <table width="100%" class="tablaInferior">
         <tr class="filaresaltada">
           <th height="26" colspan="2" scope="row"><div align="left"><span class="Estilo7">Placa</span></div></th>
           <td colspan="2" class="<%=(String)request.getAttribute("placa")%>"><input name="placa" type="text" id="placa" onChange="cambiarFormulario('<%=CONTROLLER%>?estado=OCarga&accion=Validar&modif=show');" value="<%=request.getParameter("placa")%>" size="12" maxlength="7">
           <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
           <td>Conductor</td>
           <td colspan="3" class="<%=(String)request.getAttribute("cedula")%>"><input name="conductor" type="text" id="conductor2" size="12" value="<%=request.getParameter("conductor")%>" onChange="cambiarFormulario('<%=CONTROLLER%>?estado=OCarga&accion=Validar&modif=show');">
            - <%=request.getAttribute("nombre")%> <a style="cursor:hand " onClick="window.open('<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/trafico/conductor&pagina=Conductor.jsp&titulo=Conductor&marco=no','','status=yes,scrollbars=yes,resizable=yes')" class="Simulacion_Hiper">Consultar/Agregar Conductor</a> </td>
         </tr>
         <tr class="filaresaltada">
           <th width="61" rowspan="2" class="Estilo7" scope="row"><div align="left">Trailer</div></th>
           <th width="66" rowspan="2"  scope="row" class="<%=(String)request.getAttribute("trailer")%>"><div align="left">
               <input name="trailer" type="text" id="trailer3" value="<%=request.getParameter("trailer")%>" size="12" maxlength="7">
           </div></th>
           <td><strong>&iquest;Trailer de TSP? </strong></td>
           <td width="86" rowspan="2"><strong>Contenedores</strong></td>
           <td width="88"><%String c1= "";
	  if(request.getParameter("c1")!=null)c1=request.getParameter("c1");%>
               <input name="c1" type="text" id="c12" value="<%=c1%>" size="15"></td>
           <td width="88" rowspan="2"><strong><span class="Estilo8"><strong>
             <input name="tipo_cont" type="radio" value="FINV" <%if(request.getParameter("tipo_cont").equals("FINV")){%>checked<%}%> >
            TSP</strong></span><br>
            <input name="tipo_cont" type="radio" value="NAV"  <%if(request.getParameter("tipo_cont").equals("NAV")){%>checked<%}%>>
            <strong>Naviera</strong> </strong></td>
           <td width="96" rowspan="2"><span class="Estilo8"><strong>Precintos de Contedores </strong></span></td>
           <td width="158" class="<%=(String)request.getAttribute("cp1")%>"><input name="c1precinto" type="text" id="c1precinto2" onChange="cambiarFormulario('<%=CONTROLLER%>?estado=OCarga&accion=Validar&modif=show');" value="<%=request.getParameter("c1precinto")!=null?request.getParameter("c1precinto"):""%>" size="20" maxlength="15" ></td>
         </tr>
         <tr class="filaresaltada">
           <td height="31"><strong><strong>
             <input name="tipo_tra" type="radio" value="FINV" <%if(request.getParameter("tipo_tra").equals("FINV")){%>checked<%}%>>
            Si </strong><br>
            <input name="tipo_tra" type="radio" value="NAV" <%if(request.getParameter("tipo_tra").equals("NAV")){%>checked<%}%>>
            <strong>No</strong> </strong></td>
           <td width="88"><%String c2="";
	  if(request.getParameter("c2")!=null)c2=request.getParameter("c2");%>
               <input name="c2" type="text" id="c22" value="<%=c2%>" size="15"></td>
           <td width="158" class="<%=(String)request.getAttribute("cp2")%>"><input name="c2precinto" type="text" id="c2precinto" onChange="cambiarFormulario('<%=CONTROLLER%>?estado=OCarga&accion=Validar&modif=show');" value="<%=request.getParameter("c2precinto")!=null?request.getParameter("c2precinto"):""%>" size="20" maxlength="15"></td>
         </tr>
       </table>
       <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tablaInferior">
         <tr>
           <td height="22" class="subtitulo1">Otros Precintos </td>
         </tr>
       </table>
       <table width="100%" height="44" class="tablaInferior">
         <tr class="filaresaltada">
           <td width="20%"  class="<%=(String)request.getAttribute("p1")%>">

               <%dis= request.getParameter("precinto")!=null?"disabled":"";%>
               <div align="center">
                 <input name="precintos" type="text" id="precintos6" onChange="cambiarFormulario('<%=CONTROLLER%>?estado=OCarga&accion=Validar&modif=show');" value="<%=request.getParameter("precintos")!=null?request.getParameter("precintos"):""%>" size="20" maxlength="15" >
               </div>
</td>
           <td class="<%=(String)request.getAttribute("p2")%>"><div align="center">
               <input name="precintos2" type="text" id="precintos22" onChange="cambiarFormulario('<%=CONTROLLER%>?estado=OCarga&accion=Validar&modif=show');" value="<%=request.getParameter("precintos2")!=null?request.getParameter("precintos2"):""%>" size="20" maxlength="15" >
           </div></td>
           <td width="20%" class="<%=(String)request.getAttribute("p3")%>">
             <div align="center">
               <input name="precintos3" type="text" id="precintos32" onChange="cambiarFormulario('<%=CONTROLLER%>?estado=OCarga&accion=Validar&modif=show');" value="<%=request.getParameter("precintos3")!=null?request.getParameter("precintos3"):""%>" size="20" maxlength="15" >
           </div></td>
           <td width="20%" class="<%=(String)request.getAttribute("p4")%>"><div align="center">
               <input name="precintos4" type="text" id="precintos42" onChange="cambiarFormulario('<%=CONTROLLER%>?estado=OCarga&accion=Validar&modif=show');" value="<%=request.getParameter("precintos4")!=null?request.getParameter("precintos4"):""%>" size="20" maxlength="15">
           </div></td>
           <td width="20%" class="<%=(String)request.getAttribute("p5")%>"><div align="center">
               <input name="precintos5" type="text" id="precintos52" onChange="cambiarFormulario('<%=CONTROLLER%>?estado=OCarga&accion=Validar&modif=show');" value="<%=request.getParameter("precintos5")!=null?request.getParameter("precintos5"):""%>" size="20" maxlength="15">
           </div></td>
         </tr>
       </table>
       <table width="100%" class="tablaInferior">
         <tr>
           <td height="22" class="subtitulo1">Observaciones</td>
         </tr>
       </table>
       <table width="100%" class="tablaInferior">
         <tr class="filaresaltada">
           <th width="55%"  align="center" valign="top" scope="row">CONTENIDO</th>
           <th  scope="row">ENTREGAR A </th>
         </tr>
         <tr class="filaresaltada">
           <th  align="center" valign="top" scope="row"><textarea name="contenido" style="width:100%" class="textbox" id="contenido"><%=request.getParameter("contenido")%></textarea></th>
           <th scope="row"><textarea name="entregar" style="width:100%" class="textbox" id="entregar"><%=request.getParameter("entregar")%></textarea></th>
         </tr>
         <tr class="filaresaltada">
           <th colspan="2" align="center" valign="top" scope="row">OBSERVACIONES</th>
         </tr>
         <tr class="filaresaltada">
           <th colspan="2" align="center" valign="top" scope="row"><textarea name="observacion" style="width:100%" class="textbox" id="observacion"><%=request.getParameter("observacion")%></textarea></th>
         </tr>
     </table>
	  <table width="100%" class="tablaInferior">
          <tr>
            <td height="22" class="subtitulo1">Contacto</td>
          </tr>
        </table>
        <table width="100%" height="44" class="tablaInferior">
          <tr class="filaresaltada">
            <td width="13%">
              <div align="center">            </div>              
              <div align="left">Nombre            </div>              <div align="center">            </div>              <div align="center">            </div>              <div align="center">            </div></td>
            <td width="32%"><input name="nombre" type="text" id="nombre" style="width:80% " value="<%=request.getParameter("nombre")%>"></td>
            <td width="8%">Telefono(s)</td>
            <td width="47%"><input name="telefono" type="text" id="telefono" style="width:80% " value="<%=request.getParameter("telefono")%>"></td>
          </tr>
        </table></td>
   </tr>
 </table>
 <p align="center">
      <input name="imageField" type="image" src="<%=BASEURL%>/images/botones/aceptar.gif" width="90" height="21" border="0" onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);" onClick="from1.submit(); this.disbled=true">
      <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand"> </p>
</form>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins_24.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
<%=datos[1]%>
</body>
</html>
