<%@ include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>.: Descripción del programa de orden de carga :.</title>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Orden de Carga </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">  Orden de Carga </td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2"> Cliente </td>
        </tr>
        <tr>
          <td width="149" class="fila"> Fecha de Cargue</td>
          <td width="525"  class="ayudaHtmlTexto">Campo que muestra la fecha de creacion del despacho, generado automaticamente. </td>
        </tr>
        <tr>
          <td  class="fila">Agregar Remitentes</td>
          <td  class="ayudaHtmlTexto">Link que direcciona a una pantalla donde se pueden seleccionar Remitentes. </td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2"> Datos Planilla </td>
        </tr>
        <tr>
          <td width="149" class="fila"> Placa</td>
          <td width="525"  class="ayudaHtmlTexto">Campo para digitar la placa conformada por tres letras y tres numeros. </td>
        </tr>
        <tr>
          <td width="149" class="fila"> &iquest;Trailer de TSP?</td>
          <td width="525"  class="ayudaHtmlTexto">Campo de chequeo para escoger entre la opci&oacute;n Si y la Opci&oacute;n No. </td>
        </tr>
        <tr>
          <td width="149" class="fila"> Trailer</td>
          <td width="525"  class="ayudaHtmlTexto">Campo para digitar el Codigo de un trailer. El codigo esta conformado por numeros y letras.. </td>
        </tr>
        <tr>
          <td width="149" class="fila"> Contenedor</td>
          <td width="525"  class="ayudaHtmlTexto">Campos para digitar el codigo de contenedores. El codigo puede estar conformado por numeros y letras. </td>
        </tr>
        <tr>
          <td width="149" class="fila"> TSP y Naviera</td>
          <td width="525"  class="ayudaHtmlTexto">Es un campo de chequeo para describir si el contenedor pertenece a TSP o a alguna Naviera.</td>
        </tr>
        <tr>
          <td width="149" class="fila"> Precintos de Contenedores</td>
          <td width="525"  class="ayudaHtmlTexto">Campos para digitar los numeros de los precintos del contenedor. </td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2"> Otros Precintos </td>
        </tr>
        <tr>
          <td width="149" class="fila"> Otros Precintos</td>
          <td width="525"  class="ayudaHtmlTexto">Se compone de cinco campos para registrar mas numeros de precintos en caso que hagan falta. </td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2"> Observaciones </td>
        </tr>
        <tr>
          <td width="149" class="fila"> Observaciones</td>
          <td width="525"  class="ayudaHtmlTexto">Campo de texto donde se ingresan todas las observaciones de la orden de carga </td>
        </tr>
        <tr>
          <td width="149" class="fila"> Contenido </td>
          <td width="525"  class="ayudaHtmlTexto">Campo de texto donde se ingresa el contenido de la orden de carga. </td>
        </tr>
        <tr>
          <td width="149" class="fila"> Entregar a </td>
          <td width="525"  class="ayudaHtmlTexto">Campo de texto donde se ingresa a quien entregar en  la orden de carga.</td>
        </tr>
      </table></td>
  </tr>
</table>
<p>&nbsp;</p>
<p>&nbsp;</p>

<p align="center" class="fuenteAyuda"> Fintravalores S. A. </p>
<p>&nbsp;</p>
<p>&nbsp;</p>

</body>
</html>