<!--  
     - Author(s)       :      Karen Reales
     - Date            :      06/05/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   
     - @(#)  
     - Description: Ayuda
--%> 
<%@include file="/WEB-INF/InitModel.jsp"%>


<html>
    <head>
        <title>.: Descripción del programa de orden de carga :.</title>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <BR>
        <table width="696"  border="2" align="center">
            <tr>
            <td width="811" valign="top" >
                <table width="100%" border="0" align="center">
                    <tr  class="subtitulo">
                        <td height="24" align="center">
                        ORDEN DE CARGUE </td>
                    </tr>
                    <tr class="subtitulo1">
                        <td> CREACION DE UNA ORDEN DE CARGA </td>
                    </tr>
                    <tr class="ayudaHtmlTexto">
                        <td> Debe seleccionar el standar a utilizar.<br>
                            <div align="center">                             <img src="<%=BASEURL%>/images/ayuda/nuevaocarga/imagen1.PNG" width="584" height="251"> </div>
                            <br>
                            Si no sabe el codigo del cliente haga click sobre el link &quot;Consultar clientes&quot; y una ventana como esta aparecera. <br>	
		    
                            <div align="center">
                                <p>
                                  <img src="<%=BASEURL%>/images/ayuda/nuevaocarga/imagen2.PNG" width="447" height="192"> </p>
                          </div>
                            <p>
                                <br>
                            Aparece una lista con nombres del clientes segun su busqueda, seleccione el que desea utilizar. </p>
                            <p align="center">
                              <img src="<%=BASEURL%>/images/ayuda/nuevaocarga/imagen3.PNG" width="458" height="163"> </p>
                            <p align="left">Una vez que seleccione el cliente haga click en buscar para listar los origenes y destinos disponibles para el cliente.</p>      
                            <div align="center">
                              <p><img src="<%=BASEURL%>/images/ayuda/nuevaocarga/imagen4.PNG" width="459" height="139"></p>
                              <p align="justify"><br>
                              Una vez que seleccione el cliente haga click en buscar para listar los origenes y destinos disponibles para el cliente. A continuacion seleccione origen, destino y por ultimo standar asi como se muestra a continuacion.<br>
                              </p><br>
                              <img src="<%=BASEURL%>/images/ayuda/nuevaocarga/imagen5.PNG" width="480" height="143">                              <br>
                              <img src="<%=BASEURL%>/images/ayuda/nuevaocarga/imagen6.PNG" width="483" height="195">                              <br>
                              <p align="justify">Una vez seleccionado el standar  usted debe llenar los campos como se indica a continuacion, obligatoriamente siempre debe digitar una placa y seleccionar un remitente</p><br>
                              <br>
                              <img src="<%=BASEURL%>/images/ayuda/nuevaocarga/imagen8.PNG" width="484" height="317"><br>
                              <img src="<%=BASEURL%>/images/ayuda/nuevaocarga/imagen7.PNG" width="399" height="315">                              <br>                      
                          </div></td>
                    </tr>
                </table>
            </td>
            </tr>
        </table>
        <p align="center" class="fuenteAyuda"> Fintravalores S. A. </p>
    </body>

</html>