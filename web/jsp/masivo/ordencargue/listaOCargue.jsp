<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<title>Buscar Orden de Carga</title>
<script language="javascript" src="<%=BASEURL%>/js/validarOCargue.js"></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=OCarga&accion=Listar" onSubmit="return verificarStandard();">
<table width="80%"  border="2" align="center">
  <tr>
    <td>
      <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="barratitulo">
        <tr>
          <td width="50%" class="subtitulo1" colspan='3'>Buscar Orden de carga </td>
          <td width="50%" class="barratitulo" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif" align="left"> </td>
        </tr>
      </table>
      <TABLE width='100%' border='0' class='tablaInferior'>
        <TR class="letra">
          <TD width="18%" class="letraresaltada">Cliente</TD>
          <TD><table cellpadding='0' cellspacing='0' width='100%' class='letra'>
              <tr id='op1'>
                <td colspan='2' class="letraresaltada"><span class="Simulacion_Hiper" style="cursor:hand " onClick="window.open('<%=BASEURL%>/consultas/consultasClientes.jsp','','HEIGHT=200,WIDTH=600,SCROLLBARS=YES,RESIZABLE=YES')">Consultar clientes...</span></td>
              </tr>
              <tr id='op2' class="letraresaltada">
                <td colspan="2"><input name="cliente" type="text" id="cliente" onKeyPress="return enviarForm();"></td>
              </tr>
          </table></TD>
          <TD width="15%"><span class="letraresaltada">Agencia</span></TD>
          <TD width="29%"><%TreeMap agencia = model.agenciaService.listar(); 
				  String agen = request.getParameter("agency")!=null?request.getParameter("agency"):"NADA";
							 %>
              <input:select name="agency" options="<%=agencia%>"   default='<%=agen%>' /></TD>
        </TR>
        <TR class="letra">
          <TD height='14' class="letraresaltada"> Usuario</TD>
          <TD height='14' colspan="3" class="letraresaltada"><input name="usuario" type="text" id="usuario3" onKeyPress="return enviarForm();"></TD>
        </TR>
      </TABLE></td>
  </tr>
</table>
<div align="center">
  <img name="imageField" type="image" style="cursor:hand " src="<%=BASEURL%>/images/botones/buscar.gif" align="middle" width="87" height="21" border="0" onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);" onClick="this.disabled=true; form1.submit();"></div>
</form>
<%
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 7;
    int maxIndexPages = 10;
    Vector nits = model.imprimirOrdenService.getVec_lista();
	if(nits!=null){
%>

<table width="80%"  border="2" align="center">
  <tr>
    <td>
	<table width="100%"  border="0" >
      <tr>
        <td width="42%" class="subtitulo1">LISTA ORDENES DE CARGUE </td>
        <td width="58%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
      </tr>
    </table>
    <table width="100%" class="tablaInferior">
  <tr class="subtitulos">
  <td  nowrap colspan="6">
	  <table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4">
  		<tr class="tblTitulo">
  			<td width="216" ><div align="center">Orden de Carga No. </div></td>
	    	<td width="467"><div align="center">Standard</div></td>
  		    <td width="40"><div align="center">Placa</div></td>
  		    <td width="40">Usuario</td>
  		</tr>
  <pg:pager
    items="<%=nits.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
  <%-- keep track of preference --%>
  <%
      for (int i = offset.intValue(),
	         l = Math.min(i + maxPageItems, nits.size());
	     i < l; i++)
	{
        HojaOrdenDeCarga cond = (HojaOrdenDeCarga) nits.elementAt(i);%>
  <pg:item>
	  <tr height="30" class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' nowrap style="cursor:hand" title="Seleccionar Orden De Cargue..." onClick="copiarOCargue('<%=cond.getOrden()%>');">
	    <td class="bordereporte" ><div align="center"><%=cond.getOrden()%></div></td>
    	<td class="bordereporte"><%=cond.getStd_job_no()%></td>
	    <td class="bordereporte"><div align="center"><%=cond.getPlaca()%></div></td>
	    <td class="bordereporte"><div align="center"><%=cond.getCreation_user()%></div></td>
	  </tr>
  </pg:item>
  <%}
  %>
	  <tr bgcolor="#FFFFFF" class="fila">
    	<td height="30" colspan="4" nowrap><pg:index>
	      <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
    	</pg:index></td>
	  </tr>
  </pg:pager>
 	</table>
</td>
</tr>
</table>
</td>
    
</table>

<br>  
  <%}else{%>
<table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="229" align="center" class="mensajes"><span class="normal">No se encontraron datos.</span></td>
          <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="58">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<%}%>
</body>
</html>



