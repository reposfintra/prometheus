<!--  
     - Author(s)       :      Karen Reales
     - Date            :      06/05/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   
     - @(#)  
     - Description: Ayuda
--%> 
<%@include file="/WEB-INF/InitModel.jsp"%>


<html>
    <head>
        <title>.: Descripción del programa de orden de carga :.</title>
        <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <BR>
        <table width="696"  border="2" align="center">
            <tr>
            <td width="811" valign="top" >
                <table width="100%" border="0" align="center">
                    <tr  class="subtitulo">
                        <td height="24" align="center">
                        ORDEN DE CARGUE </td>
                    </tr>
                    <tr class="subtitulo1">
                        <td> ANULACION DE UNA ORDEN DE CARGA</td>
                    </tr>
                    <tr class="ayudaHtmlTexto">
                      <td> <div align="left">Escriba el numero de la orden de carga a buscar. </div>                        
                        <div align="center"><br>
                          <img src="<%=BASEURL%>/images/ayuda/modifocarga/imagen1.PNG" width="489" height="152"> </div><br>
                          <br> 
                          Llene los datos a anular. <br>
                          <br>         
						  <div align="center">               
                          <img src="<%=BASEURL%>/images/ayuda/modifocarga/imagen3.PNG" width="558" height="539"></div>
						  <br>
						  <div align="center">
					  </div>                       
					  <br>                     </td>
                    </tr>
                </table>
            </td>
            </tr>
        </table>
        <p align="center" class="fuenteAyuda"> Fintravalores S. A. </p>
    </body>

</html>