<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@page import="com.tsp.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Orden de carga</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/validarOCargue.js">
</script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</script>

<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">

</head>

<body onLoad="form1.placa.focus()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ordenes de Carga"/>
</div>

 <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
  
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=OCarga&accion=Validar&cmd=show" onSubmit="return  ValidarColpapel(form1)">
  <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
  <%java.util.Date date = new java.util.Date();
    SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd HH:mm");
	String fecpla1 = s.format(date);   
	String fecpla=com.tsp.util.Util.fecha_Zonificada(fecpla1, usuario.getLogin());
	String standard = request.getParameter("standard");
	String remitentes = "a";
	String imagen =BASEURL+"/images/equis2.gif";
	String destinatario = "a";
	String imagenD =BASEURL+"/images/equis2.gif";

	HojaOrdenDeCarga oc  = model.imprimirOrdenService.getHojaOrden();
	if(oc!=null){
		standard = oc.getStd_job_no();
		remitentes = oc.getRemitente();
		destinatario= oc.getDestinatarios();
		imagen =BASEURL+"/images/ok2.gif";
		imagenD=BASEURL+"/images/ok2.gif";
	}
	          
 %>
  <table width="80%"  border="2" align="center">
    <tr>
      <td>
	    <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="22" colspan=2 class="subtitulo1">Informacion Orden de Carga</td>
            <td width="304" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
          </tr>
        </table>
	    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td height="22" class="subtitulo1">Cliente</td>
          </tr>
      </table>                  
	  <table width="100%">
        <tr class="letra">
          <td width="20%" class="filaresaltada" scope="row">Estandar Job
              </td>
			  <% 
	  Stdjobdetsel sj=new Stdjobdetsel();
	 String pa ="";
	  String uw="";
	  String vlr ="";
	  String origen ="";
	  float unidad_default = 0;
	  
      if(model.stdjobdetselService.getStandardDetSel()!=null){
			sj = model.stdjobdetselService.getStandardDetSel();
            uw=sj.getUnit_of_work();
            pa=sj.getPorcentaje_ant();
			vlr = ""+sj.getVlr_freight();             
			origen = sj.getOrigin_code();
			unidad_default=sj.getUnidades();
			//out.println(origen);
            }%>
          <td colspan="4" class="letra">
            
            <%=sj.getSj()+"-"+sj.getSj_desc()%>
            <input name="standard" type="HIDDEN" id="standard" value="<%=standard%>">
            <input name="sj_nombre" type="hidden" id="sj_nombre" value="<%=sj.getSj()+"-"+sj.getSj_desc()%> ">
            <span class="Estilo6">
            <input name="docudest" type="hidden" id="docudest">
            </span> </td>
          </tr>
        <tr class="fila">
          <th colspan="2" scope="row"><div align="left"><span class="Estilo6">
            <input name="remitentes" type="hidden" id="remitentes" value="<%=remitentes%>">
            <a class="Simulacion_Hiper" style="cursor:hand "  onClick="abrirPagina('<%=BASEURL%>/colpapel/remitentes.jsp?sj='+form1.standard.value+'&origen='+form1.origstd.value,'');">
            <input name="origstd" type="hidden" id="origstd2" value="<%=origen%>">
            <input name="imagenre" type="hidden" id="imagenre2" value="<%=imagen%>">
            Agregar Remitentes
            </a>            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">            <img src="<%=imagen%>" width="17" height="17" id="imre">
            
            <input name="imagendest" type="hidden" id="imagendest" value="<%=imagenD%>">
            <input name="destinatarios" type="hidden" id="destinatarios" value="<%=destinatario%>" >
			<%if(request.getParameter("cmd")!=null){
				if(!request.getParameter("cmd").equals("remesa")){%>
		 <a class="Simulacion_Hiper" style="cursor:hand " onClick="abrirPagina('<%=CONTROLLER%>?estado=Buscar&accion=Destinatarios&sj='+form1.standard.value,'');">Agregar Destinatarios</a>
			 	<%}
				else{%>
				<a class="Simulacion_Hiper" style="cursor:hand " onClick="abrirPagina('<%=CONTROLLER%>?estado=Buscar&accion=Destinatarios&sj='+form1.standard.value+'&numrem=<%=request.getParameter("numrem").toUpperCase()%>&ocargue=<%=request.getParameter("numrem").toUpperCase()%>','');">Agregar Destinatarios</a>
			<% }
		}else{%><a class="Simulacion_Hiper" style="cursor:hand " onClick="abrirPagina('<%=CONTROLLER%>?estado=Buscar&accion=Destinatarios&sj='+form1.standard.value+'&numrem=<%=request.getParameter("numrem").toUpperCase()%>&ocargue=<%=request.getParameter("numrem").toUpperCase()%>','');">Agregar Destinatarios</a>
		<% }%>
		  <img src="<%=imagenD%>" width="17" height="17" id="imdest">
          </span></div>            </th>
          <th width="17%" scope="row"><span class="filaresaltada">FECHA DE CARGUE</span></th>
          <th width="23%" colspan="2" scope="row"><div align="left"><span class="letra">
              <div align="left">
                <input name="fechadesp" type="text" class="textbox" id="fechadesp" value="<%=fecpla.toLowerCase()%>" readonly size="18">
                <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fechadesp);return false;" HIDEFOCUS>
                       <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></img></a></div>          </tr>
		</table>
		      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tablaInferior">
          <tr>
            <td height="22" class="subtitulo1">Datos Planilla </td>
            </tr>
        </table>		
		<table width="100%" class="tablaInferior">
        <tr class="filaresaltada">
          <th height="26" colspan="2" scope="row"><div align="left"><span class="Estilo7">Placa</span></div></th>
          <td colspan="7"><input name="placa" type="text" class="textbox" id="placa2" size="12" maxlength="7">
            <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
        </tr>
        <tr class="filaresaltada">
          <th width="61" rowspan="2" class="Estilo7" scope="row"><div align="left">Trailer</div></th>
          <th width="66" rowspan="2" class="Estilo7" scope="row"><div align="left">
            <input name="trailer" type="text" class="textbox" id="trailer2" size="12" maxlength="7">
          </div></th>
          <td colspan="2"><strong>&iquest;Trailer de TSP? </strong></td>
          <td width="86" rowspan="2"><strong>Contenedores</strong></td>
          <td width="88"><input name="c1" type="text" class="textbox" id="c14" size="15"></td>
          <td width="88" rowspan="2"><strong>
            <input name="tipo_cont" type="radio" value="FINV">
TSP</strong><br>
<input name="tipo_cont" type="radio" value="NAV" checked>
<strong>Naviera</strong></td>
          <td width="96" rowspan="2"><span class="Estilo8"><strong>Precintos de Contedores </strong></span></td>
          <td width="158"><input name="c1precinto" type="text" class="textbox" id="c1precinto3"  size="20" maxlength="15"></td>
        </tr>
        <tr class="filaresaltada">
          <td height="31" colspan="2"><strong>
            <input name="tipo_tra" type="radio" value="FINV" checked>
Si </strong><br>
<input name="tipo_tra" type="radio" value="NAV">
<strong>No</strong></td>
          <td width="88"><input name="c2" type="text" class="textbox" id="c23" size="15"></td>
          <td width="158"><input name="c2precinto" type="text" class="textbox" id="c2precinto3"  size="20" maxlength="15"></td>
        </tr>
		</table>		
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tablaInferior">
          <tr>
            <td height="22" class="subtitulo1">Otros Precintos </td>
            </tr>
        </table>		
		<table width="100%" height="44" class="tablaInferior">
          <tr class="filaresaltada">
            <td width="20%">              <div align="center">
                  <input name="precintos" type="text" class="textbox" id="precintos"  size="20" maxlength="15">
                </div>
		    </td><td ><div align="center">
                <input name="precintos2" type="text" class="textbox" id="precintos2"  size="20" maxlength="15">
              </div></td>
           <td width="20%">
		   <div align="center">
              <input name="precintos3" type="text" class="textbox" id="precintos3"  size="20" maxlength="15">
            </div></td>
            <td width="20%"><div align="center">
              <input name="precintos4" type="text" class="textbox" id="precintos4"  size="20" maxlength="15">
            </div></td>
            <td width="20%"><div align="center">
              <input name="precintos5" type="text" class="textbox" id="precintos5"  size="20" maxlength="15">
            </div></td>
          </tr>
        </table>
        <table width="100%" class="tablaInferior">
          <tr>
            <td height="22" class="subtitulo1">Observaciones</td>
            </tr>
        </table>       
        <table width="100%" class="tablaInferior">
          <tr class="filaresaltada">
            <th width="55%"  align="center" valign="top" scope="row">CONTENIDO</th>
            <th  scope="row">ENTREGAR A
		      </th>
            </tr>
          <tr class="filaresaltada">
            <th  align="center" valign="top" scope="row"><textarea name="contenido" style="width:100%" class="textbox" id="contenido"></textarea></th>
            <th scope="row"><textarea name="entregar" style="width:100%" class="textbox" id="textarea2"></textarea></th>
          </tr>
          <tr class="filaresaltada">
            <th colspan="2" align="center" valign="top" scope="row">OBSERVACIONES</th>
            </tr>
          <tr class="filaresaltada">
            <th colspan="2" align="center" valign="top" scope="row"><textarea name="observacion" style="width:100%" class="textbox" id="observacion"></textarea></th>
            </tr>
        </table>	    
        <table width="100%" class="tablaInferior">
          <tr>
            <td height="22" class="subtitulo1">Contacto</td>
          </tr>
        </table>
        <table width="100%" height="44" class="tablaInferior">
          <tr class="filaresaltada">
            <td width="13%">
              <div align="center">            </div>              
              <div align="left">Nombre            </div>              <div align="center">            </div>              <div align="center">            </div>              <div align="center">            </div></td>
            <td width="32%"><input name="nombre" type="text" id="nombre" style="width:80% "></td>
            <td width="8%">Telefono(s)</td>
            <td width="47%"><input name="telefono" type="text" id="telefono" style="width:80% "></td>
          </tr>
        </table></td>
	  </tr>
    </table>
<div align="center"><br>
            <img src="<%=BASEURL%>/images/botones/regresar.gif" width="101" height="21"  onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);" style="cursor:hand " onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/masivo/ordencargue/&pagina=preliminar.jsp&marco=no'">
          <input name="imageField" type="image" src="<%=BASEURL%>/images/botones/aceptar.gif" width="90" height="21" border="0" onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);">
        <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">        </div>
        <p align="center">&nbsp;           </p>
</form>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins_24.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
<%=datos[1]%>
</body>
</html>
