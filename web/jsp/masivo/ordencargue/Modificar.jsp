<%@page session="true"%>

<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Orden de carga</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/validarOCargue.js">
</script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</script>

<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">

</head>

<body onLoad="form1.placa.focus()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modificar Orden de Carga"/>
</div>

 <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
 <%
 	//Datos de la orden de carga
	String standard="";
	String fecha="";
	String remitente="";
	String placa="";
	String trailer="";
	String c1="";
	String c2="";
	String pc1="";
	String pc2="";
	String p1="";
	String p2="";
	String p3="";
	String p4="";
	String p5="";
	String contenido="";
	String entregar="";
	String observacion="";
	String orden="";	
	String tipotra="";
	String tipocon="";
	String conductor="";
	String fecha_cargue="";
	String destinatario = "a";
	String imagenD =BASEURL+"/images/equis2.gif";
	String nombre="";
	String telefono="";
	
	if(model.imprimirOrdenService.getHojaOrden()!=null){
		HojaOrdenDeCarga oc = model.imprimirOrdenService.getHojaOrden();
		standard=oc.getStd_job_no();
		orden = oc.getOrden();
		fecha=oc.getCreation_date();
		remitente=oc.getRemitente();
		placa=oc.getPlaca();
		conductor=oc.getConductor();
		trailer=oc.getTrailer();
		if(oc.getContenedores()!=null){
			String vec[] =oc.getContenedores().split(",");
			c1=vec.length>0?vec[0]:"";
			c2=vec.length>1?vec[1]:"";
		}
		pc1=oc.getPrecintoc1();
		pc2=oc.getPrecintoc2();
		p1=oc.getPrecinto1();
		p2=oc.getPrecinto2();
		p3=oc.getPrecinto3();
		p4=oc.getPrecinto4();
		p5=oc.getPrecinto5();
		contenido=oc.getContenido();
		entregar=oc.getEntregar();
		observacion=oc.getObservacion();
		tipotra=oc.getTipotrailer();
		tipocon=oc.getTipocont();
		fecha_cargue = oc.getFecha_cargue();
		destinatario = oc.getDestinatarios();
		
		nombre=oc.getNombre();
		telefono=oc.getTelefono();
		if(!destinatario.equals("")){
			imagenD =BASEURL+"/images/ok2.gif";
		}
	}
	
	
 %>
<form name="form1" method="post" action="<%=CONTROLLER%>?estado=OCarga&accion=Validar&modif=show" onSubmit="return  ValidarColpapel(form1)">
  <%Usuario usuario = (Usuario) session.getAttribute("Usuario");%>
  <%java.util.Date date = new java.util.Date();
    SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd hh:mma");
	String fecpla = s.format(date);   
               %>
  <table width="80%"  border="2" align="center">
    <tr>
      <td>
	    <table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td height="22" colspan=2 class="subtitulo1">Informacion Orden de Carga <%=orden%>
              <input name="orden" type="hidden" id="orden" value="<%=orden%>"></td>
            <td width="304" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
          </tr>
        </table>
	    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
          <td height="22" class="subtitulo1">Cliente</td>
          </tr>
      </table>                  
	  <table width="100%">
        <tr class="letra">
          <td width="20%" class="filaresaltada" scope="row">Estandar Job
              </td>
			  <% 
			  String origen="";
			  model.stdjobdetselService.searchStdJob(standard);
			  Stdjobdetsel sj=new Stdjobdetsel();
			   if(model.stdjobdetselService.getStandardDetSel()!=null){
				sj = model.stdjobdetselService.getStandardDetSel();
			    origen = sj.getOrigin_code();
			//out.println(origen);
            }%>
          <td colspan="4" class="letra">
            
            <%=sj.getSj()+"-"+sj.getSj_desc()%>
            <input name="standard" type="hidden" id="standard" value="<%=standard%>">
            <input name="sj_nombre" type="hidden" id="sj_nombre" value="<%=sj.getSj()+"-"+sj.getSj_desc()%> ">
            <span class="Estilo6">
            <input name="docudest" type="hidden" id="docudest">
            </span> </td>
          </tr>
        <tr class="fila">
          <th colspan="2" scope="row"><div align="left"><span class="Estilo6">
            <input name="remitentes" type="hidden" id="remitentes" value="<%=remitente%>">
            <a class="Simulacion_Hiper" style="cursor:hand "  onClick="abrirPagina('<%=BASEURL%>/colpapel/remitentes.jsp?sj='+form1.standard.value+'&origen='+form1.origstd.value,'');">
            <input name="imagenre" type="hidden" id="imagenre2" value="<%=BASEURL%>/images/ok2.gif">
            <input name="origstd" type="hidden" id="origstd2" value="<%=origen%>">
            Modificar Remitentes<img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">            </a>
            <img src="<%=BASEURL%>/images/ok2.gif" width="24" height="21" id="imre">            
                  <a class="Simulacion_Hiper" style="cursor:hand " onClick="abrirPagina('<%=CONTROLLER%>?estado=Buscar&accion=Destinatarios&sj='+form1.standard.value+'&ocargue=<%=orden%>&destinatario=<%=destinatario%>&modif=ok','');">Modificar Destinatarios</a> <img src="<%=imagenD%>" width="17" height="17" id="imdest">
                  <input name="imagendest" type="hidden" id="imagendest" value="<%=imagenD%>">
                  <input name="destinatarios" type="hidden" id="destinatarios" value="<%=destinatario%>" >
              </span></div>            </th>
          <th width="16%" scope="row"><span class="filaresaltada">FECHA DE CARGUE</span></th>
          <th width="21%" colspan="2" scope="row">
              <input name="fechadesp" type="text" class="textbox" id="fechadesp" value="<%=fecha_cargue%>" size="18" readonly>
<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fechadesp);return false;" HIDEFOCUS>
                     <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></img></a></th>
          </tr>
		</table>
		      <table width="100%" border="0" cellpadding="0" cellspacing="0" class="tablaInferior">
          <tr>
            <td height="22" class="subtitulo1">Datos Planilla </td>
            </tr>
        </table>		
		<table width="100%" class="tablaInferior">
        <tr class="filaresaltada">
          <th height="26" colspan="2" scope="row"><div align="left"><span class="Estilo7">Placa</span></div></th>
          <td colspan="2"><input name="placa" type="text" class="textbox" id="placa" value="<%=placa%>" size="12" maxlength="7">
          <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          <td>Conductor</td>
          <td colspan="3">
            <input name="conductor" type="text" id="conductor2" size="12" value="<%=conductor%>" >
            <a style="cursor:hand " onClick="window.open('<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/trafico/conductor&pagina=Conductor.jsp&titulo=Conductor&marco=no','','status=yes,scrollbars=yes,resizable=yes')" class="Simulacion_Hiper">Consultar/Agregar Conductor</a> </td>
          </tr>
        <tr class="filaresaltada">
          <th width="61" rowspan="2" class="Estilo7" scope="row"><div align="left">Trailer</div></th>
          <th width="66" rowspan="2" class="Estilo7" scope="row"><div align="left">
            <input name="trailer" type="text" class="textbox" id="trailer" value="<%=trailer%>" size="12" maxlength="7">
          </div></th>
          <td><strong>&iquest;Trailer de TSP? </strong></td>
          <td width="86" rowspan="2"><strong>Contenedores</strong></td>
          <td width="88"><input name="c1" type="text" class="textbox" id="c1" value="<%=c1%>" size="15"></td>
          <td width="88" rowspan="2"><strong>
            <span class="Estilo8"><strong>
            <input name="tipo_cont" type="radio" value="FINV" <%if(tipocon.equals("FINV")){%>checked<%}%> >
TSP</strong></span><br>
<input name="tipo_cont" type="radio" value="NAV"  <%if(tipocon.equals("NAV")){%>checked<%}%>>
<strong>Naviera</strong></strong></td>
          <td width="96" rowspan="2"><span class="Estilo8"><strong>Precintos de Contedores </strong></span></td>
          <td width="158"><input name="c1precinto" type="text" class="textbox" id="c1precinto" value="<%=pc1%>" size="20" maxlength="15"></td>
        </tr>
        <tr class="filaresaltada">
          <td height="31"><strong>
            <strong>
            <input name="tipo_tra" type="radio" value="FINV" <%if(tipotra.equals("FINV")){%>checked<%}%>>
Si </strong><br>
<input name="tipo_tra" type="radio" value="NAV" <%if(tipotra.equals("NAV")){%>checked<%}%>>
<strong>No</strong> </strong></td>
          <td width="88"><input name="c2" type="text" class="textbox" id="c2" value="<%=c2%>" size="15"></td>
          <td width="158"><input name="c2precinto" type="text" class="textbox" id="c2precinto" value="<%=pc2%>"  size="20" maxlength="15"></td>
        </tr>
		</table>		
		<table width="100%" border="0" cellpadding="0" cellspacing="0" class="tablaInferior">
          <tr>
            <td height="22" class="subtitulo1">Otros Precintos </td>
            </tr>
        </table>		
		<table width="100%" height="44" class="tablaInferior">
          <tr class="filaresaltada">
            <td width="20%">              <div align="center">
                  <input name="precintos" type="text" class="textbox" id="precintos" value="<%=p1%>"  size="20" maxlength="15">
                </div>
		    </td><td ><div align="center">
                <input name="precintos2" type="text" class="textbox" id="precintos2" value="<%=p2%>"  size="20" maxlength="15">
              </div></td>
           <td width="20%">
		   <div align="center">
              <input name="precintos3" type="text" class="textbox" id="precintos3" value="<%=p3%>"  size="20" maxlength="15">
            </div></td>
            <td width="20%"><div align="center">
              <input name="precintos4" type="text" class="textbox" id="precintos4" value="<%=p4%>"  size="20" maxlength="15">
            </div></td>
            <td width="20%"><div align="center">
              <input name="precintos5" type="text" class="textbox" id="precintos5" value="<%=p5%>" size="20" maxlength="15" o>
            </div></td>
          </tr>
        </table>
        <table width="100%" class="tablaInferior">
          <tr>
            <td height="22" class="subtitulo1">Observaciones</td>
            </tr>
        </table>       
        <table width="100%" class="tablaInferior">
          <tr class="filaresaltada">
            <th width="55%"  align="center" valign="top" scope="row">CONTENIDO</th>
            <th  scope="row">ENTREGAR A
		      </th>
            </tr>
          <tr class="filaresaltada">
            <th  align="center" valign="top" scope="row"><textarea name="contenido" style="width:100%" class="textbox" id="contenido"><%=contenido%></textarea></th>
            <th scope="row"><textarea name="entregar" style="width:100%" class="textbox" id="entregar"><%=entregar%></textarea></th>
          </tr>
          <tr class="filaresaltada">
            <th colspan="2" align="center" valign="top" scope="row">OBSERVACIONES</th>
            </tr>
          <tr class="filaresaltada">
            <th colspan="2" align="center" valign="top" scope="row"><textarea name="observacion" style="width:100%" class="textbox" id="observacion"><%=observacion%></textarea></th>
            </tr>
        </table>	  
		 <table width="100%" class="tablaInferior">
          <tr>
            <td height="22" class="subtitulo1">Contacto</td>
          </tr>
        </table>
        <table width="100%" height="44" class="tablaInferior">
          <tr class="filaresaltada">
            <td width="13%">
              <div align="center">            </div>              
              <div align="left">Nombre            </div>              <div align="center">            </div>              <div align="center">            </div>              <div align="center">            </div></td>
            <td width="32%"><input name="nombre" type="text" id="nombre" style="width:80% " value="<%=nombre%>"></td>
            <td width="8%">Telefono(s)</td>
            <td width="47%"><input name="telefono" type="text" id="telefono" style="width:80% " value="<%=telefono%>"></td>
          </tr>
        </table>  </td>
	  </tr>
    </table>
<div align="center"><br>
            <img src="<%=BASEURL%>/images/botones/regresar.gif" width="101" height="21"  onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);" style="cursor:hand " onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/masivo/ordencargue/&pagina=InicioModificar.jsp&marco=no'">
          <input name="imageField" type="image" src="<%=BASEURL%>/images/botones/aceptar.gif" width="90" height="21" border="0" onMouseOver="botonOver(this);"  onMouseOut="botonOut(this);">
        <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">        </div>
        <p align="center">&nbsp;           </p>
</form>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins_24.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
<%=datos[1]%>
</body>
</html>
