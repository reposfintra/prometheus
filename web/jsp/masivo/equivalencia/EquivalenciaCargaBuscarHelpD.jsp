<!--  
	 - Author(s)       :      Jose de la rosa
	 - Description     :      AYUDA DESCRIPTIVA - Buscar Equivalencia de Carga
	 - Date            :      08/06/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>AYUDA DESCRIPTIVA - Buscar Equivalencia de Carga</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Equivalencia de Carga</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Buscar Equivalencia de Carga</td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2">INFORMACION DE LA EQUIVALENCIA DE CARGA </td>
        </tr>
        <tr>
          <td  class="fila">Codigo Carga </td>
          <td  class="ayudaHtmlTexto">Campo para digitar el codigo de la carga a ingresar. Este campo es de m&aacute;ximo 12 caracteres. </td>
        </tr> 
        <tr>
          <td  class="fila">Codigo Carga Legal</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el codigo de la carga legal a ingresar. Este campo es de m&aacute;ximo 12 caracteres.</td>
        </tr>		       
        <tr>
          <td  class="fila">Tipo de Carga</td>
          <td  class="ayudaHtmlTexto">Campo para seleccionar el tipo de la carga a ingresar. </td>
        </tr>
        <tr>
          <td class="fila">Bot&oacute;n Buscar </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para confirmar y realizar la busqueda de la equivalencia de carga.</td>
        </tr>
		<tr>
          <td class="fila">Bot&oacute;n Detalle </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que permite realizar una busqueda de todas las equivalencias de carga.</td>
        </tr>
		<tr>
          <td width="149" class="fila">Bot&oacute;n Salir</td>
          <td width="525"  class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista 'Buscar Equivalencia de Carga' y volver a la vista del men&uacute;.</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</body>
</html>
