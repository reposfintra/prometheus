<!--  
	 - Author(s)       :      Jose de la rosa
	 - Description     :      AYUDA DESCRIPTIVA - agregar equivalencia de carga
	 - Date            :      16/01/2007 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>AYUDA DESCRIPTIVA - Equivalencia Carga</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Equivalencia Carga </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Ingresar Equivalencia de Carga </td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2">INFORMACION DE LA EQUIVALENCIA DE CARGA </td>
        </tr>
        
        <tr>
          <td  class="fila">Codigo Carga </td>
          <td  class="ayudaHtmlTexto">Campo para digitar el codigo de la carga a ingresar. Este campo es de m&aacute;ximo 12 caracteres. </td>
        </tr> 
        <tr>
          <td  class="fila">Codigo Carga Legal</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el codigo de la carga legal a ingresar. Este campo es de m&aacute;ximo 12 caracteres.</td>
        </tr>		       
        <tr>
          <td  class="fila">Tipo de Carga</td>
          <td  class="ayudaHtmlTexto">Campo para seleccionar el tipo de la carga a ingresar. </td>
        </tr>
        <tr>
          <td  class="fila">Descripci&oacute;n Corta </td>
          <td  class="ayudaHtmlTexto">Campo para digitar una descripcion corta a ingresar. Este campo es de m&aacute;ximo 20 caracteres. </td>
        </tr>		
        <tr>
          <td  class="fila"> Valor Mercancia </td>
          <td  class="ayudaHtmlTexto">Campo para digitar el valor de la mercancia a ingresar. Este campo es de m&aacute;ximo 12 caracteres. </td>
        </tr>
        <tr>
          <td  class="fila"> Unidad Mercancia </td>
          <td  class="ayudaHtmlTexto">Campo para seleccionar la unidad de la mercancia a ingresar. </td>
        </tr>
        <tr>
          <td  class="fila"> Descripcion Carga </td>
          <td  class="ayudaHtmlTexto">Campo para digitar la una descripcion de la carga. </td>
        </tr>				
        <tr>
          <td class="fila">Bot&oacute;n Ingresar </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para ingresar datos de equivalencia de carga. </td>
        </tr>
        <tr>
          <td class="fila">Bot&oacute;n Cancelar </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para reiniciar los campos a su estado inicial.</td>
        </tr>        
        <tr>
          <td width="149" class="fila">Bot&oacute;n Salir</td>
          <td width="525"  class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista 'Equivalencia de Carga' y volver a la vista del menu.</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</body>
</html>
