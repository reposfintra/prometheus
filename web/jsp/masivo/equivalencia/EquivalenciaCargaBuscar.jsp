<!--
- Autor : Ing. Jose de la rosa
- Date  : 12 de Enero de 2007
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la equivalencia de la carga
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head><title>Buscar Equivalencia de Carga</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Buscar Equivalencia de Carga"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<form name="form2" method="post" action="<%=CONTROLLER%>?estado=Equivalencia&accion=Carga&opcion=listar">
    <table width="380" border="2" align="center">
      <tr>
        <td><table width="100%" align="center"  class="tablaInferior">
			<tr>
				<td width="173" class="subtitulo1">&nbsp;Esquema Formato </td>
				<td width="205" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
			</tr>
			<tr class="fila">
				<td width="173" align="left" >Codigo Carga</td>
			    <td valign="middle"><input name="c_codigo_carga" type="text" class="textbox" id="c_codigo_carga" size="12" maxlength="12"></td>
			</tr>			  
			<tr class="fila">
				<td align="left" >Codigo Carga Legal </td>
			    <td valign="middle"><input name="c_carga_legal" type="text" class="textbox" id="c_carga_legal" size="12" maxlength="12"></td>
			</tr>	
			<tr class="fila">
				<td width="173" align="left" >Tipo Carga </td>
			    <td valign="middle"><select name="c_tipo_carga" id="c_tipo_carga" class="textbox" style='width:90%;'>
            			<option></option>
						<% LinkedList tbltip = (LinkedList)request.getAttribute ( "set_tipo");
						if(tbltip!=null){
						   for(int i = 0; i<tbltip.size(); i++){
								TablaGen restip = (TablaGen) tbltip.get(i); %>
								<option value="<%=restip.getTable_code()%>"><%=restip.getDescripcion()%></option>
							<%}
						}%>
          				</select></td>
			</tr>						  
        </table>
		</td>
      </tr>
    </table>
<p>
<div align="center"><img src="<%=BASEURL%>/images/botones/buscar.gif" style="cursor:hand" title="Buscar" name="buscar"  onClick="form2.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/detalles.gif" style="cursor:hand" title="Buscar todos los registros" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Equivalencia&accion=Carga&opcion=listar&c_codigo_carga=&c_carga_legal=&c_tipo_carga='" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/salir.gif"  name="regresar" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
</p>		
</form>
</div>
<%=datos[1]%>
</body>
</html>