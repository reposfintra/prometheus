<!--
- Autor : Ing. Jose de la rosa
- Date  : 12 de Enero de 2007
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la equivalencia de la carga
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<html>
<head>
<title>Ingresar Equivalencia de Carga</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>

<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Insertar Equivalencia de Carga"/>
</div>
<%
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
	LinkedList tbltip = (LinkedList)request.getAttribute ( "set_tipo");
	LinkedList tbluni = (LinkedList)request.getAttribute ( "set_unidad");
%>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<FORM name='forma' id='forma' method='POST' action='<%=CONTROLLER%>?estado=Equivalencia&accion=Carga&opcion=insert'>
  <table width="650" border="2" align="center" >
    <tr>
      <td>
        <table width="100%" class="tablaInferior">
          <tr class="fila">
            <td colspan="2" align="left" class="subtitulo1">&nbsp;Equivalencia Carga </td>
            <td colspan="2" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
          </tr>
          <tr class="fila">
			<td width="20%" align="left" >Codigo Carga</td>
			<td align="left" ><input name="c_codigo_carga" type="text" class="textbox" id="c_codigo_carga" size="12" maxlength="12"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"> </td>
			<td align="left" >Codigo Carga Legal</td>
			<td align="left" ><input name="c_carga_legal" type="text" class="textbox" id="c_carga_legal" size="12" maxlength="12"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"> </td>
          </tr>
		  <tr class="fila">
			  <td>Tipo de Carga</td>
			  <td width="26%"><select name="c_tipo_carga" id="c_tipo_carga" class="textbox" style='width:90%;'>
							<%if(tbltip!=null){
							   for(int i = 0; i<tbltip.size(); i++){
									TablaGen restip = (TablaGen) tbltip.get(i); %>
									<option value="<%=restip.getTable_code()%>"><%=restip.getDescripcion()%></option>
								<%}
							}%>
							</select><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
			  <td width="21%">Descripción Corta</td>
			  <td width="33%"><input name="c_desc_corta" type="text" class="textbox" id="c_desc_corta2" onKeyPress="soloAlfa(event)" size="20" maxlength="20">		      </td>
		  </tr>	
          <tr class="fila">
            <td align="left" >Valor Mercancia</td>
            <td align="left">              <input name="c_vlr_mercancia" type="text" class="textbox" id="c_vlr_mercancia2" size="12" maxlength="12" onKeyPress="soloDigitos(event,'decNO')">
              <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"> </td>
            <td align="left">Unidad Mercancia </td>
            <td align="left"><select name="c_und_mercancia" id="c_und_mercancia" class="textbox" style='width:50%;'>
              <%if(tbluni!=null){
							   for(int i = 0; i<tbluni.size(); i++){
									TablaGen resuni = (TablaGen) tbluni.get(i); %>
              <option value="<%=resuni.getTable_code()%>"><%=resuni.getDescripcion()%></option>
              <%}
							}%>
            </select>
              <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
          </tr>	
		  <tr class="fila">
		    <td>Descripcion Carga </td>
		    <td colspan="3"><textarea name="c_desc_carga" cols="90" rows="3" class="textbox" id="c_desc_carga" onKeyPress="soloAlfa(event)" type="text"></textarea><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
		  </tr>  
      </table></td>
    </tr>
  </table>
  <p>
<div align="center"><img src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand" title="Agregar un Registro" name="ingresar"  onclick="return enviaDatos();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
<img src="<%=BASEURL%>/images/botones/cancelar.gif" style="cursor:hand" name="regresar" title="Limpiar todos los registros" onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);" >&nbsp;
<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></div>
</p>  
<%  if(request.getAttribute("mensaje")!=null){%>
		<table border="2" align="center">
		  <tr>
			<td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
			  <tr>
				<td width="229" align="center" class="mensajes"><%=(String)request.getAttribute("mensaje")%></td>
				<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
				<td width="58">&nbsp;</td>
			  </tr>
			</table></td>
		  </tr>
		</table>
	<%  }%>
</FORM>
</div>
<%=datos[1]%>
</body>
</html>
<script>

function enviaDatos( ){
	if(forma.c_codigo_carga.value == '' ){
		alert("No se puede procesar la información. Verifique que todos los datos esten llenos.");
		forma.c_codigo_carga.focus();
		return false;
	}
	else if(forma.c_carga_legal.value == '' ){
		alert("No se puede procesar la información. Verifique que todos los datos esten llenos.");
		forma.c_carga_legal.focus();
		return false;
	}
	else if(forma.c_vlr_mercancia.value == '' ){
		alert("No se puede procesar la información. Verifique que todos los datos esten llenos.");
		forma.c_vlr_mercancia.focus();
		return false;
	}
	else if(forma.c_desc_carga.value == '' ){
		alert("No se puede procesar la información. Verifique que todos los datos esten llenos.");
		forma.c_desc_carga.focus();
		return false;
	}
	else if(forma.c_tipo_carga.value == '' ){
		alert("No se puede procesar la información. Verifique que todos los datos esten llenos.");
		forma.c_tipo_carga.focus();
		return false;
	}
	else{
		forma.submit();
	}
}	
</script>
