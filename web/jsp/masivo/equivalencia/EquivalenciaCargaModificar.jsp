<!--
- Autor : Ing. Jose de la rosa
- Date  : 12 de Enero de 2007
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la equivalencia de la carga
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Modificar Equivalencia de Carga</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<body onResize="redimensionar();" <%if(request.getParameter("reload")!=null){%>onLoad="parent.opener.location.reload();redimensionar();"<%}%>>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modificar Equivalencia de Carga"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

<%
    EquivalenciaCarga eq = model.equivalenciaCargaService.getEquivalencia();
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
	LinkedList tbltip = (LinkedList)request.getAttribute ( "set_tipo");
	LinkedList tbluni = (LinkedList)request.getAttribute ( "set_unidad");
%>
<FORM name='forma' id='forma' method='POST' action='<%=CONTROLLER%>?estado=Equivalencia&accion=Carga&opcion=modificar'>
		<table width="650" border="2" align="center" >
			<tr>
			  <td>
				<table width="100%" class="tablaInferior">
				  <tr class="fila">
					<td colspan="2" align="left" class="subtitulo1">&nbsp;Equivalencia Carga </td>
					<td colspan="2" align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
				  </tr>
				  <tr class="fila">
					<td width="20%" align="left" >Codigo Carga</td>
					<td align="left" ><%=eq.getCodigo_carga()%><input name="c_codigo_carga" type="hidden" class="textbox" id="c_codigo_carga"value="<%=eq.getCodigo_carga()%>"></td>
					<td align="left" >Codigo Carga Legal</td>
					<td align="left" ><input name="c_carga_legal" type="text" class="textbox" id="c_carga_legal" value="<%=eq.getCodigo_legal()%>" size="12" maxlength="12"><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"> </td>
				  </tr>
				  <tr class="fila">
					  <td>Tipo de Carga</td>
					  <td width="26%"><select name="c_tipo_carga" id="c_tipo_carga" class="textbox" style='width:90%;'>
									<%if(tbltip!=null){
									   for(int i = 0; i<tbltip.size(); i++){
											TablaGen restip = (TablaGen) tbltip.get(i); %>
											<option <%=eq.getTipo_carga().equals( restip.getTable_code() )?"selected":""%> value="<%=restip.getTable_code()%>"><%=restip.getDescripcion()%></option>
										<%}
									}%>
									</select><img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
					  <td width="21%">Descripción Corta</td>
					  <td width="33%"><input name="c_desc_corta" type="text" class="textbox" id="c_desc_corta2" onKeyPress="soloAlfa(event)" value="<%=eq.getDescripcion_corta()%>" size="20" maxlength="20"></td>
				  </tr>	
				  <tr class="fila">
					<td align="left" > Valor Mercancia </td>
					<td align="left" >
                      <input name="c_vlr_mercancia" type="text" class="textbox" id="c_vlr_mercancia2" size="12" maxlength="12" value="<%=eq.getValor_mercancia()%>" onKeyPress="soloDigitos(event,'decNO')">
                      <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"> </td>
				    <td align="left" >Unidad Mercancia </td>
				    <td align="left" ><select name="c_und_mercancia" id="select" class="textbox" style='width:50%;'>
                      <%if(tbluni!=null){
									   for(int i = 0; i<tbluni.size(); i++){
											TablaGen resuni = (TablaGen) tbluni.get(i); %>
                      <option <%=eq.getUnidad_mercancia().equals( resuni.getTable_code() )?"selected":""%> value="<%=resuni.getTable_code()%>"><%=resuni.getDescripcion()%></option>
                      <%}
									}%>
                    </select>
			        <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
				  </tr>	
				  <tr class="fila">
					<td>Descripcion Carga</td>
					<td colspan="3"><textarea name="c_desc_carga" cols="90" rows="3" class="textbox" id="textarea" onKeyPress="soloAlfa(event)"><%=eq.getDescripcion_carga()%></textarea>
				    <img src="<%=BASEURL%>/images/botones/iconos/obligatorio.gif"></td>
				  </tr>  
			  </table></td>
			</tr>
		  </table>	
	  <input type="hidden" name="opcion" value="">
	<p>
	<div align="center">
	<img src="<%=BASEURL%>/images/botones/modificar.gif" style="cursor:hand" title="Modificar un registro" name="modificar"  onclick="return enviaDatos();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
	<img src="<%=BASEURL%>/images/botones/anular.gif" style="cursor:hand" title="Anular el registro" name="anular"  onClick="window.location='<%=CONTROLLER%>?estado=Equivalencia&accion=Carga&opcion=anular&c_codigo_carga=<%=eq.getCodigo_carga()%>';" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
	<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" ></div>
	</p>
	<%  if(request.getAttribute("mensaje")!=null){%>
		<table border="2" align="center">
		  <tr>
			<td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
			  <tr>
				<td width="229" align="center" class="mensajes"><%=(String)request.getAttribute("mensaje")%></td>
				<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
				<td width="58">&nbsp;</td>
			  </tr>
			</table></td>
		  </tr>
		</table>
	<%  }%>

</FORM>
</div>
<%=datos[1]%>
</body>
</html>
<script>
function enviaDatos( ){
	if(forma.c_codigo_carga.value == '' ){
		alert("No se puede procesar la información. Verifique que todos los datos esten llenos.");
		forma.c_codigo_carga.focus();
		return false;
	}
	else if(forma.c_carga_legal.value == '' ){
		alert("No se puede procesar la información. Verifique que todos los datos esten llenos.");
		forma.c_carga_legal.focus();
		return false;
	}
	else if(forma.c_vlr_mercancia.value == '' ){
		alert("No se puede procesar la información. Verifique que todos los datos esten llenos.");
		forma.c_vlr_mercancia.focus();
		return false;
	}
	else if(forma.c_desc_carga.value == '' ){
		alert("No se puede procesar la información. Verifique que todos los datos esten llenos.");
		forma.c_desc_carga.focus();
		return false;
	}
	else if(forma.c_tipo_carga.value == '' ){
		alert("No se puede procesar la información. Verifique que todos los datos esten llenos.");
		forma.c_tipo_carga.focus();
		return false;
	}
	else{
		forma.submit();
	}
}	
</script>