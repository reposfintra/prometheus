<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title></title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<script src="<%=BASEURL%>/js/validar.js"></script>
<script src="<%=BASEURL%>/js/validar.js"></script>
<%
        String url = CONTROLLER + "?estado=Aplicacion&accion=Anular&cmd=show";
%>
<script>        
        function anular(){
                forma.action = '<%=url%>';
                forma.submit();
        }
</script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body <%if(request.getParameter("mensaje").equalsIgnoreCase("MsgModificado") ){ %>onLoad="parent.opener.location.reload();"<%}%>>
<div align="center">
  <%
        //Consulta del perfil-vista 
        Usuario usuario = (Usuario) session.getAttribute("Usuario");
        String login = usuario.getLogin();
        Vector pvpag = model.perfil_vistaService.consultPerfil_vistas(login, "AppUpdate.jsp");
        System.out.println("---> login: " + login);
        System.out.println("---> pvpag.size(): " + pvpag.size());
        System.out.println("---> codigo: " + model.perfil_vistaService.propiedad("c_codigo", pvpag));
        System.out.println("---> descripcion: " + model.perfil_vistaService.propiedad("c_descripcion", pvpag));
        //Fin consulta del perfil-vista

        Aplicacion app = model.appSvc.getAplicacion();
        model.appSvc.setAplicacion(null);
        if( app==null ) app = new Aplicacion();
        
        String estado = request.getParameter("estado");
        if ( estado == null ) estado = "";
        
        if( !estado.matches("Anulado") ){        
%>
    <input:form name="forma" bean="app" attributesText="id='forma' onSubmit='return validarTCamposLlenos();'" method="post" action="<%=CONTROLLER + "?estado=Aplicacion&accion=Update&cmd=show"%>" >
    <table width="479" border="2" align="center">
      <tr>
        <td><table width="100%" class="tablaInferior">
          <tr>
            <td width="50%" class="subtitulo1">Informaci&oacute;n</td>
            <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"> </td>
          </tr>
        </table>
          <table width="100%" align="center">
            <tr class="fila">
              <td>C&oacute;digo<input:hidden default="<%= app.getDistrito()%>" name="c_cia" attributesText="readonly class='textbox' id='c_cia'"/></td>
              <td><input:text default="<%= app.getC_codigo()%>" name="c_codigo" attributesText="readonly class='textbox' id='c_codigo' size='25' maxlength='10'"/></td>
            </tr>
            <tr class="fila">
              <td width="22%">Descripci&oacute;n</td>
              <td width="78%"><input value="<%= app.getC_descripcion()%>" name="c_descripcion" class='textbox' id='c_descripcion2' size='50' maxlength='40'"/>
                  <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
            </tr>
          </table>      </td>
      </tr>
    </table>
    <br>
    <img src="<%=BASEURL%>/images/botones/modificar.gif" name="c_modificar" onClick="if( validarTCamposLlenos() ) forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand "> <img src="<%=BASEURL%>/images/botones/anular.gif" name="c_anular" onClick="anular();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">
    <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_buscar" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">  
    </input:form>
        <%              if(request.getParameter("msg")!=null){%>
</div>
<table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes"><%=request.getParameter("msg").toString() %></td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
</table>	
<%              }
        }
        else{
%>
<table border="2" align="center">
  <tr>
    <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="282" align="center" class="mensajes"><%=request.getParameter("msg").toString() %></td>
          <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="78">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<p>&nbsp;</p>
<table width="415" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr align="left">
    <td><img src="<%=BASEURL%>/images/botones/salir.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="parent.close();"> 
  </tr>
</table>
<p>&nbsp;</p>
<p>
  <%
        }
%>
</p>
</body>
</html>
