<!--  
     - Author(s)       :      MARIO FONTALVO
     - Date            :      13/01/2006  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   
     - @(#)  
     - Description: Formulario de asignacion de fechas de envio, 
       agencia de envio, fecha_recibido de soportes Remesas
--%> 
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*" %>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.Cumplido" %>
<%@page import="com.tsp.util.Util" %>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
    String Mensaje   = (request.getParameter("Mensaje")!=null?request.getParameter("Mensaje"):"");
    model.agenciaService.listar();
    TreeMap agencias = model.agenciaService.getListaAgencias();
    List    lista    = model.cumplidoService.getListaDocumentos();
    String fltRemesa = (String) session.getAttribute("fltRemesa");
    String fltEstado = (String) session.getAttribute("fltEstado");
    String fltTipo   = (String) session.getAttribute("fltTipo");
    String fltFecIni = (String) session.getAttribute("fltFecIni");
    String fltFecFin = (String) session.getAttribute("fltFecFin");
    
    
    fltRemesa = (fltRemesa==null?"":fltRemesa);
    fltEstado = (fltEstado==null || fltEstado.equals("")?"TODOS" :fltEstado);
    fltTipo   = (fltTipo  ==null || fltTipo.equals  ("")?"TODAS" :fltTipo  );
    fltFecIni = (fltFecIni==null?"":fltFecIni);
    fltFecFin = (fltFecFin==null?"":fltFecFin);
    int cols  = 4;  //(fltTipo.equals("TODAS")?4:2);
%>
<html>
<head>
    <title>Registro Fechas Documentos Remesa</title>
    <script src ="<%= BASEURL %>/js/boton.js"></script>
    <script src ="<%= BASEURL %>/js/validarRegistroCheque.js"></script>
    <script src ="<%= BASEURL %>/js/validarSoporteRemesa.js"></script>
    <link   href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>    

</head>
<body>


<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=DOCUMENTOS SOPORTE REMESA"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<center>


<table align="center"  border="2" bgcolor="#F7F5F4" width='<%= (420+(cols*145)) %>'>
<tr>
  <td align='center' >
  
  
        <table class='tablaInferior' width='100%'>
        <tr>
            <td colspan='<%= (4+cols) %>'>
             <table border='0' width='100%' cellspacing='0'>
                  <tr>
                     <td class='subtitulo1'  width="50%">&nbsp; Busqueda Documentos Soporte Remesa</td>
                     <td class='barratitulo' width="50%"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
                  </tr>
             </table>             
            </td> 
        </tr>  
        <form action="<%= CONTROLLER %>?estado=Asignacion&accion=FecDocSopRem&Opcion=Buscar" method="post" name="Formulario">
        <tr class='fila'>
            <td colspan='<%= (3+cols) %>' width='97%'>           
                &nbsp;
                <span style='width:140;'>Remesa</span>
                <span style='width:200;'><input type='text' name='Remesa' style='width:100; text-align:center; text-transform:uppercase;' onfocus='this.select();' ></span>
                &nbsp;
                <span style='width:120;'>Estado Documento</span>
                <select name='Estado' style='width:120;'>
                      <option value='ENVIADOS' >Enviados</option>
                      <option value='RECIBIDOS'>Recibidos</option>
                      <option value='TODOS'    >Todos</option>
                </select>
                &nbsp;
                <span style='width:100;'>Tipo de Fechas</span>
                &nbsp;
                <select name='Tipo' style='width:120;'>
                      <option value='LOGICA' >Logica</option>
                      <option value='FISICA' >Fisica</option>
                      <option value='TODAS'  >Todos</option>
                </select>                
            </td>
            <td rowspan='2' align='center' valign='center'>
                <img src='<%=BASEURL%>/images/botones/iconos/lupa.gif' width='28'  style='cursor:hand' onclick=" if(validarFormuarioFiltro(Formulario)){ Formulario.submit(); } ">
            </td>            
        </tr>
        
        <tr class='fila'>
            <td colspan='<%= (3+cols) %>'>
                &nbsp;
                <span style='width:140;'>Agencia Facturadora</span> 
                <input:select name="Agencia" attributesText="<%= "id='Agencia'  style='width:200;' class='textbox'  " %>"  default="NADA" options="<%= agencias %>" />
                &nbsp;
                <span style='width:120;'>Fecha Cumplido</span>            
                <input name="fecIni" type="text" id="fecIni" value="" readonly  style='text-align:center' onfocus='this.select();' onkeyup='jscript: _keys(this);'>
                <a href='javascript:void(0)' onclick='if(self.gfPop)gfPop.fPopCalendar(document.Formulario.fecIni);return false;' HIDEFOCUS><img src='<%= BASEURL %>/js/Calendario/cal.gif' width='16' height='16' border='0' alt='De click aqui para escoger la fecha'></a>
                <input name="fecFin" type="text" id="fecFin"   value="" readonly  style='text-align:center' onfocus='this.select();' onkeyup='jscript: _keys(this);'>
                <a href='javascript:void(0)' onclick='if(self.gfPop)gfPop.fPopCalendar(document.Formulario.fecFin);return false;' HIDEFOCUS><img src='<%= BASEURL %>/js/Calendario/cal.gif' width='16' height='16' border='0' alt='De click aqui para escoger la fecha'></a>
            </td>
        </tr>    
        
        <script>
             Formulario.Remesa.value = '<%= fltRemesa %>';
             Formulario.Estado.value = '<%= fltEstado %>';
             Formulario.Tipo.value   = '<%= fltTipo   %>';
             Formulario.fecIni.value = '<%= fltFecIni %>';
             Formulario.fecFin.value = '<%= fltFecFin %>';
        </script>
        </form>
        
        
        <!-- listado -->
        <% if (lista!=null && lista.size()>0) { 
            String style      = "simple";
            String position   = "bottom";
            String index      = "center";
            int maxPageItems  = 15;
            int maxIndexPages = 10;
        %>
        
        <tr class='tblTitulo'>
            <th class='bordeReporte' width='55'>Remesa</th>
            <th class='bordeReporte' width='175'>Soporte</th>
            <th class='bordeReporte' width='150'>Agencia Envio</th>
            <% if ( "TODAS|LOGICA".indexOf(fltTipo)!=-1  ) { %>
            <th class='bordeReporte' width='145'>Envio Logico</th>
            <th class='bordeReporte' width='145'>Recibido Logico</th>            
            <% } %>
            
            <% if ( "TODAS|FISICA".indexOf(fltTipo)!=-1  ) { %>
            <th class='bordeReporte' width='145'>Envio Fisico</th>
            <th class='bordeReporte' width='145'>Recibido Fisico</th>
            <% } %>
            
            <th class='bordeReporte' width='40' align='center' >Grabar</th>
        </tr>
        
        <pg:pager
             items         ="<%= lista.size()%>"
             index         ="<%= index %>"
             maxPageItems  ="<%= maxPageItems %>"
             maxIndexPages ="<%= maxIndexPages %>"
             isOffset      ="<%= true %>"
             export        ="offset,currentPageNumber=pageNumber"
             scope         ="request">
            
         <%  for (int i = offset.intValue(), l = Math.min(i + maxPageItems, lista.size()); i < l; i++){
                Cumplido c = (Cumplido) lista.get(i); %>
            <pg:item>
            <form action='<%= CONTROLLER %>?estado=Asignacion&accion=FecDocSopRem' method='post' name='form<%= i %>' onsubmit=' return _onsubmit(this); ' >
                <tr class='<%= (i%2==0?"filagris":"filaazul") %>'>
                    <td class='bordeReporte' nowrap><%= c.getRemesa() %></td>
                    <td class='bordeReporte' nowrap><%= c.getComent() %></td>
                    <td class='bordeReporte' nowrap ><input:select name="AgenciaEnvio" attributesText="<%= "id='AgenciaEnvio'  style='width:100%;' class='textbox'  " %>"  default="NADA" options="<%= agencias %>" /></td>
                    
                    
                    
                    <!-- LOGICO -->
                    <% if ( "TODAS|LOGICA".indexOf(fltTipo)!=-1 ) { %>
                    <td class='bordeReporte' align='center' nowrap>
                       <input type='text' name='FechaEnvioLogico'    style='width:85%; text-align:center;' readonly >
                       <span class="comentario" id='FEL<%= i %>'><a href="javascript:void(0)" onClick=" if(self.gfPop)gfPop.fPopCalendar(form<%= i %>.FechaEnvioLogico);return false; " hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt=""></a> </span>
                    </td>
                    <td class='bordeReporte' align='center' nowrap>
                       <input type='text' name='FechaRecibidoLogico' style='width:85%; text-align:center;' readonly  >
                       <span class="comentario" id='FRL<%= i %>'><a href="javascript:void(0)" onClick=" if(self.gfPop)gfPop.fPopCalendar(form<%= i %>.FechaRecibidoLogico);return false; " hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt=""></a> </span>
                    </td> 
                    <% } %>
                    <!-- FIN LOGICO-->
                    
                    
                    <!-- FISICO -->
                    <% if ( "TODAS|FISICA".indexOf(fltTipo)!=-1 ) { %>
                    <td class='bordeReporte' align='center' nowrap>
                       <input type='text' name='FechaEnvioFisico'    style='width:85%; text-align:center;' readonly  >
                       <span class="comentario" id='FEF<%= i %>'><a href="javascript:void(0)" onClick=" if(self.gfPop)gfPop.fPopCalendar(form<%= i %>.FechaEnvioFisico);return false;" hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt=""></a> </span>
                    </td>
                    <td class='bordeReporte' align='center' nowrap>
                       <input type='text' name='FechaRecibidoFisico' style='width:85%; text-align:center;' readonly  >
                       <span class="comentario" id='FRF<%= i %>'><a href="javascript:void(0)" onClick=" if(self.gfPop)gfPop.fPopCalendar(form<%= i %>.FechaRecibidoFisico);return false;  " hidefocus><img name="popcal" align="absmiddle" src="<%=BASEURL%>/js/calendartsp/calbtn.gif" width="16" height="16" border="0" alt=""></a> </span>
                    </td>
                    <% } %>
                    <!-- FIN FISICO -->
                    
                    
                    <td nowrap class="bordereporte" align='center' >
                        <input id='boton<%= i %>'  type='image' src='<%=BASEURL%>/images/botones/iconos/modificar.gif'  title='Modificar'>
                    </td>
                </tr>
                <input type='hidden' name='Remesa'  value='<%= c.getRemesa () %>'>
                <input type='hidden' name='Soporte' value='<%= c.getSoporte() %>'>
                <input type='hidden' name='Tipo'    value='<%= fltTipo %>'>
                <input type='hidden' name='editAGE' value='true'>
                <input type='hidden' name='editFEF' value='true'>
                <input type='hidden' name='editFRF' value='true'>                
                <input type='hidden' name='editFEL' value='true'>
                <input type='hidden' name='editFRL' value='true'>
                <input type='hidden' name='Opcion'  value='Edit'>
                <script>
                
                
                   form<%= i %>.AgenciaEnvio.value = '<%= (c.getAgencia_envio().equals("")?"NADA":c.getAgencia_envio() ) %>';
                   form<%= i %>.editAGE.value      = '<%= (c.getAgencia_envio().equals("")?"true":"false" ) %>';
                   
                   initFecha (<%= i %>, '<%= c.getFecha_envio()           %>', 'FechaEnvioFisico'   , 'FEF');
                   initFecha (<%= i %>, '<%= c.getFecha_recibido()        %>', 'FechaRecibidoFisico', 'FRF');
                   initFecha (<%= i %>, '<%= c.getFecha_envio_logico()    %>', 'FechaEnvioLogico'   , 'FEL');
                   initFecha (<%= i %>, '<%= c.getFecha_recibido_logico() %>', 'FechaRecibidoLogico', 'FRL');
                   
                   enabledBoton (<%= i %>);
                   
                   
                </script>
            </form>   
            </pg:item>
           <% } //end for %> 
           

            <tr>
                <td colspan="6" align="center" >
                        <pg:index>
                        <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
                        </pg:index> 
                </td>
            </tr>
        </pg:pager> 
        <% } // end if %>
        <!-- fin listado -->
        
        </table>
  
  
  </td>
</tr>  
</table>

    <br>
    <img src='<%=BASEURL%>/images/botones/salir.gif'   style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='window.close();'>        



<% if (!Mensaje.equals("")) { %>
    <br><br>
        <table border="2" align="center">
           <tr>
           <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                <tr>
                 <td width="320" align="center" class="mensajes"><%=Mensaje%></td>
                 <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                 <td width="58">&nbsp;</td>
                </tr>
              </table>
           </td>
           </tr>
        </table>  
<% } %> 

</center>
</div>
<iframe width=188 height=166 name="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins_24.js" id="gToday:normal:<%=BASEURL%>/js/calendartsp/agenda.js:gfPop:<%=BASEURL%>/js/calendartsp/plugins_24.js" src="<%=BASEURL%>/js/calendartsp/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
</body>
</html>

<script>
  
</script>
