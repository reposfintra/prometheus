<!--
     - Author(s)       :      MARIO FONTALVO
     - Date            :      10/03/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--
     - @(#)
     - Description: Formulario de edicion de nuevos conceptos
--%>
<%@ page session="true"%>

<%@ page import="com.tsp.operation.model.beans.TblConceptos, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
   TblConceptos concepto = (TblConceptos) request.getAttribute("concepto");
   String       accion   = (String) request.getAttribute("Accion");
   String       accion2  = (String) request.getAttribute("Accion2");
   String       msg      = (String) request.getAttribute("Mensaje");
   String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
   String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
   <link rel="stylesheet" href="../../../css/estilostsp.css">
   <title>Conceptos</title>
   <script src="<%=BASEURL%>/js/boton.js"></script>
   <script src="<%=BASEURL%>/js/validar.js"></script>
   <script language="javascript" src="<%=BASEURL%>/js/ingreso.js"></script>
   <script>
      function validar(form){
         var sw = true;
         with (form){
            for (var i=0; i<elements.length; i++){
               if (elements[i].value==''){
                  alert ( 'Debe definir un valor para la propiedad : ' +elements[i].alias );
                  if (sw) elements[i].focus();
                  sw = false;
                  break;
               }
            }
         }
         return sw;
      }
      function addOption(Comb,valor,texto){
            var Ele = document.createElement("OPTION");
            Ele.value=valor;
            Ele.text=texto;
            Comb.add(Ele);
      } 
      function nuevaClase (opcion){
         if (opcion.value=='*NEW*'){
           alert ('Recuerde que para actualizar el listado de clases que tiene cargado \ndeberá ingresar nuevamente desde la opcion lo trajo hasta aqui.');
           var url = '<%= CONTROLLER %>?estado=TablaGen&accion=Manager&cmd=mng&type=TCONCCLASS';
           newWindow(url, 'Classes'); 
           opcion.selectedIndex = 0;
           
         }
      }
      
        function newWindow (url, name){
          var win = open (url,name,' top=5,left=5, width='+ (screen.width-25) +', height='+ (screen.height-100) +', scrollbar=no, status=yes, resizable  ');
          win.focus();
        }      
   </script>
</head>
<body>

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Edicion de Conceptos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">


<center>

    <form name='formulario' action='<%= CONTROLLER%>?estado=Opciones&accion=Conceptos' method='post'>
    <table border='2' width='450'>
    <tr>
    <td>
    <!-- *************************************************************** -->
    <table class='tablaInferior' width='100%' cellspacing='0' cellpadding='0'>
    <tr>
    <td>
        <table width='100%'>
        <tr>
           <td  class="subtitulo1" width="50%">Conceptos </td>
           <td  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align='left'><%=datos[0]%></td>
        </tr>   
        </table>
    </td>
    </tr>

    <tr>
    <td>
        <table width='100%'>
        <tr class='fila'>
           <td>&nbsp;Distrito</td>
           <td> 
                <% if (accion!=null && accion.equals("Insert")) { %>
                <input:select name="dstrct" options="<%=  model.ciaService.getCbxCia() %>" attributesText="id='dstrct' style='width:125;' class='listmenu'" default="<%= "FINV" %>"/>
                <% } else { %>
                <input type='text' name='dstrct' readonly alias='Distrito'>
                <% } %>
                <img src='<%= BASEURL %>/images/botones/iconos/obligatorio.gif'>
           </td>
        </tr>        
        <tr class='fila'>
           <td width='170'>&nbsp;Codigo</td>
           <td>
                <input type='text' name='concept_code' maxlength='6' alias='Codigo' onkeypress=' soloAlfa(event); '>
                <img src='<%= BASEURL %>/images/botones/iconos/obligatorio.gif'>
           </td>
        </tr>        
        <tr class='fila'>
           <td>&nbsp;Descripcion</td>
           <td>
               <input type='text' name='concept_desc' maxlength='30' style='width:90%' alias='Descripcion' onkeypress=' soloAlfa(event); '>
               <img src='<%= BASEURL %>/images/botones/iconos/obligatorio.gif'>
           </td>
        </tr>   
        <tr class='fila'>
           <td>&nbsp;Clase</td>
           <td> 
                <input:select name="concept_class" options="<%=  model.ConceptosSvc.getCmbClases() %>" attributesText="id='concept_class' style='width:90%;' class='listmenu' alias='Clase' onchange=' nuevaClase(this); ' "/>
                <img src='<%= BASEURL %>/images/botones/iconos/obligatorio.gif' >
           </td>
        </tr>      
        <tr class='fila'>
           <td>&nbsp;Cuenta Contable</td>
           <td>
                <input type='text' name='account' maxlength='25' alias='Cuenta Contable' onkeypress=' soloAlfa(event); '>
                <img src='<%= BASEURL %>/images/botones/iconos/obligatorio.gif'>
           </td>
        </tr>  
        <tr class='fila'>
           <td>&nbsp;Codigo de Migración</td>
           <td>
                <input type='text' name='codigo_migracion' maxlength='5' alias='Codigo de Migracion' onkeypress=' soloAlfa(event); '>
                <img src='<%= BASEURL %>/images/botones/iconos/obligatorio.gif'>
           </td>
        </tr>          
        <tr class='fila'>
           <td>&nbsp;Visible</td>
           <td>
                <select name='visible' style='width:125;' alias='Visible'>
                  <option value='Y'>Si</option>
                  <option value='N' selected>No</option>
                </select>
                <img src='<%= BASEURL %>/images/botones/iconos/obligatorio.gif'>
           </td>
        </tr>         
        <tr class='fila'>
           <td>&nbsp;Modificable</td>
           <td>
                <select name='modif' style='width:125;' alias='Modificable'>
                  <option value='Y' selected>Si</option>
                  <option value='N'>No</option>
                </select>
                <img src='<%= BASEURL %>/images/botones/iconos/obligatorio.gif'>
           </td>
        </tr> 
        <tr class='fila'>
           <td>&nbsp;Indicador de Aplicación</td>
           <td>
                <select name='ind_application' style='width:125;' alias='Indicador de Aplicacion'>
                  <option value='V' selected >Valor</option>
                  <option value='S' >Saldo</option>
                </select>
                <img src='<%= BASEURL %>/images/botones/iconos/obligatorio.gif'>
           </td>
        </tr>         
        <tr class='fila'>
           <td>&nbsp;Indicador de Signo</td>
           <td>
                <select name='ind_signo' style='width:125;' alias='Indicador de signo'>
                  <option value='1' selected>Positivo</option>
                  <option value='-1'>Negativo</option>
                </select>
                <img src='<%= BASEURL %>/images/botones/iconos/obligatorio.gif'>
           </td>
        </tr>   
        <tr class='fila'>
           <td>&nbsp;Tipo Cuenta</td>
           <td>
                <select name='tipo_cuenta' style='width:125;' alias='Tipo de Cuenta'>
                  <option value='E' selected>E</option>
                  <option value='C'>C</option>
                  <option value='Q'>Q</option>				  
                </select>
                <img src='<%= BASEURL %>/images/botones/iconos/obligatorio.gif'>
           </td>
        </tr> 		    
		
        <tr class='fila'>
           <td>&nbsp;Asocia Cheque</td>
           <td>
                <select name='asocia_cheque' style='width:125;' alias='Asocia Cheque'>
                  <option value='S'>Si</option>
                  <option value='N' selected>No</option>				
                </select>
                <img src='<%= BASEURL %>/images/botones/iconos/obligatorio.gif'>
           </td>
        </tr> 		    
		
		<tr class='fila'>
           <td>&nbsp;Tipo</td>
           <td>
                <select name='tipo' style='width:125;' alias='Tipo'>
                  <option value='V'>Valor</option>
                  <option value='P' selected>Porcentaje</option>
                </select>
                <img src='<%= BASEURL %>/images/botones/iconos/obligatorio.gif'>
           </td>
        </tr> 		    

        <tr class='fila'>
           <td>&nbsp;Moneda</td>
           <td> 
                <input:select name="currency" options="<%=  model.monedaService.obtenerMonedas() %>" attributesText="id='currency' style='width:90%;' class='listmenu' alias='Moneda' onchange='cambiar(currency, vlr);' " default="PES"/>
                <img src='<%= BASEURL %>/images/botones/iconos/obligatorio.gif' >
           </td>
        </tr>		
						
				
		<tr class='fila'>
           <td>&nbsp;Valor por defecto</td>
           <td>
		   		<input type="text" name="vlr" value="0"  maxlength='10' alias='Valor por defecto' onkeypress=" soloDigitos(event, 'decOK');  "  onblur="cambiar(currency, vlr);" onFocus="this.select();" style="text-align:right ">
                <img src='<%= BASEURL %>/images/botones/iconos/obligatorio.gif'>
           </td>
        </tr> 	

        <tr class='fila'>
           <td>&nbsp;Descuento Fijo</td>
           <td>
                <select name='descuento_fijo' style='width:125;' alias='Descuento Fijo'>
                  <option value='S'>Si</option>
                  <option value='N' selected>No</option>				
                </select>
                <img src='<%= BASEURL %>/images/botones/iconos/obligatorio.gif'>
           </td>
        </tr> 		
		
        <tr class='fila'>
           <td>&nbsp;Tipo concepto</td>
           <td>
                <select name='ingreso_costo' style='width:125;' alias='Ingreso Costo'>
                  <option value='I'>Ingreso</option>
                  <option value='C' selected>Costo</option>				
                </select>
                <img src='<%= BASEURL %>/images/botones/iconos/obligatorio.gif'>
           </td>
        </tr> 						
						
        </table>
    </td>
    </tr>

    </table>
    
    
    <!-- *************************************************************** -->
    </td>
    </tr>
    </table>
    
    <input type='hidden' name='Opcion' value='' alias='Opcion'>
    </form>
    
   <img src="<%=BASEURL%>/images/botones/aceptar.gif"   name="imgAceptar"   height="21" onMouseOver="botonOver(this);" onClick="if (validar(formulario)) {formulario.submit();}" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;  
   <img src="<%=BASEURL%>/images/botones/cancelar.gif"  name="imgCancelar"  height="21" onMouseOver="botonOver(this);" onClick="formulario.reset();"  onMouseOut="botonOut(this);" style="cursor:hand">&nbsp; 
   <img src="<%=BASEURL%>/images/botones/salir.gif"     name="imgSalir"     height="21" onMouseOver="botonOver(this);" onClick="window.close();"      onMouseOut="botonOut(this);" style="cursor:hand">
   
   
   
<%if( msg!=null) {%>
   <p>
   <table border="2" align="center">
       <tr>
           <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
               <tr>
                   <td width="229" align="center" class="mensajes"> <%= msg %></td>
                   <td width="29" background="<%= BASEURL %>/images/cuadronaranja.JPG">&nbsp;</td>
                   <td width="58">&nbsp;</td>
               </tr>
           </table></td>
       </tr>
   </table>
  </p>
<%}%>   
</center>
</div>
<%=datos[1]%>
</body>
</html>

<script>
<% if (concepto!=null) {%>
    formulario.dstrct.value           = '<%= concepto.getDstrct          () %>';
    formulario.concept_code.value     = '<%= concepto.getConcept_code    () %>';
    formulario.concept_desc.value     = '<%= concepto.getConcept_desc    () %>';
    formulario.concept_class.value    = '<%= concepto.getConcept_class   () %>';
    formulario.account.value          = '<%= concepto.getAccount         () %>';
    formulario.codigo_migracion.value = '<%= concepto.getCodigo_migracion() %>';
    formulario.visible.value          = '<%= concepto.getVisible         () %>';
    formulario.modif.value            = '<%= concepto.getModif           () %>';
    formulario.ind_application.value  = '<%= concepto.getInd_application () %>';
    formulario.ind_signo.value        = '<%= concepto.getInd_signo       () %>';
	
	formulario.tipo_cuenta.value     = '<%= concepto.getTipo_cuenta     () %>';
	formulario.asocia_cheque.value   = '<%= concepto.getAsocia_cheque   () %>';
	formulario.tipo.value            = '<%= concepto.getTipo            () %>';
	formulario.vlr.value             = '<%= concepto.getVlr             () %>';
	formulario.currency.value        = '<%= concepto.getCurrency        () %>';
	formulario.descuento_fijo.value  = '<%= concepto.getDescuento_fijo  () %>';
	formulario.ingreso_costo.value   = '<%= concepto.getIngreso_costo   () %>';
	
<% }
   if (accion!=null) {%>
    formulario.Opcion.value = '<%= accion %>';
    if (formulario.Opcion.value=='Update'){
      formulario.concept_code.readOnly = true;
    }
<% }
   if (accion2!=null && accion2.equals("reload")) {%>
   parent.opener.location.href = '<%= CONTROLLER %>?estado=Opciones&accion=Conceptos&Opcion=List';
<% } %>
addOption(formulario.concept_class,'*NEW*','------- AGREGAR UNA NUEVA CLASE -------');




function cambiar(moneda, valor){
  var vlNet = valor.value.replace( new RegExp(",","g"), "");
  if(moneda.value == 'DOL'){    
     valor.onkeypress = new Function (" soloDigitos(event, 'decOK'); ");  
	 valor.value     = Fdolar(vlNet,2);
	 valor.onchange = new Function (" formatoDolar(this,2); ");  
	 
  }else{
     valor.onkeypress = new Function (" soloDigitos(event, 'decNO'); ");  
	 valor.value     = formato(Math.round(vlNet));
	 valor.onchange = new Function (" formatear(this); ");  
  }
}

/*
function cambiar(moneda, valor){
	if (moneda.value=='DOL')
		formatoDolar(valor,2);
	else
		formatear(valor);
}*/
</script>
