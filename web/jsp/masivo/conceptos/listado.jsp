<!--
     - Author(s)       :      MARIO FONTALVO
     - Date            :      10/03/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--
     - @(#)
     - Description: Listado general de conceptos
--%>
<%@ page session="true"%>
<%@ page import="com.tsp.operation.model.beans.TblConceptos, java.util.*, com.tsp.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%
    Vector vector = model.ConceptosSvc.getVector();
    String msg    = (String) request.getAttribute("Mensaje");
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
   <link rel="stylesheet" href="../../../css/estilostsp.css">
    <title>Listado de Conceptos</title>
    <script src="<%= BASEURL %>/js/boton.js"></script>
    <script>
        function validar( form ){
           with (form){
              for (var i=0;i<elements.length;i++){
                if (elements[i].type=='checkbox' && elements[i].checked)
                  return true;
              }
           }
           alert ('Debe seleccionar por lo menos un item para poder continuar');
           return false;
        }
    
        function accion(op){
           if ( validar (formulario) ){
              formulario.Opcion.value=op;
              formulario.submit();
           }
        }
        
        function editar(index){
           var url = formulario.action + '&Opcion=Editar&index=' + index;
           newWindow(url, 'Edicion');
        }
        
        function newWindow (url, name){
          var win = open (url,name,' top=5,left=5, width='+ (screen.width-25) +', height='+ (screen.height-100) +', scrollbar=no, status=yes, resizable  ');
          win.focus();
        }        
    </script>
</head>
<body>


<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Listado de Conceptos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">


<center>
    <% if (vector!=null && !vector.isEmpty()) {%>
    <form name='formulario' action='<%= CONTROLLER%>?estado=Opciones&accion=Conceptos' method='post'>
    

    <table border='2' width='1505'>
    <tr>
    <td>
    <!-- *************************************************************** -->
    <table class='tablaInferior' width='100%' cellspacing='0' cellpadding='0'>
    <tr>
    <td>
        <table width='100%'>
        <tr>
           <td  class="subtitulo1" width="50%">&nbsp;Listado de Conceptos </td>
           <td  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align='left'><%=datos[0]%></td>
        </tr>   
        </table>
    </td>
    </tr>

    <tr>
    <td>
        <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
        <!--<table width='100%'>-->
        <tr class='tblTitulo'>
           <th width='35' align='center' >&nbsp;</th>
           <th width='50' >DISTRITO</th>
           <th width='50' >CODIGO</th>
           <th width='200'>DESCRIPCION</th>
           <th width='200'>CLASE</th>
           <th width='100'>CUENTA</th>
           <th width='100' >MIGRACION</th>
           <th width='50' >VIS</th>
           <th width='50' >MOD</th>
           <th width='50' nowrap>APL</th>
           <th width='50' nowrap>SIG</th>
		   
           <th width='70' nowrap>Tipo Cuenta</th>		   
           <th width='70' nowrap>Asocia <br> Cheque</th>		   
           <th width='70' nowrap>Tipo</th>		   
           <th width='120' nowrap>Valor</th>		   
           <th width='70' nowrap>Moneda</th>		   
           <th width='70' nowrap>Descuento<br> 
             Fijo</th>		   
           <th width='70' nowrap>Tipo<br> 
             Concepto</th>		   		   		   		   		   		   		   
        </tr>  
      
        <%
           String style      = "simple";
           String position   = "bottom";
           String index      = "center";
           int maxPageItems  = 12;
           int maxIndexPages = 10;
        %>
            
        <pg:pager items="<%= vector.size()%>" index="<%= index %>" maxPageItems ="<%= maxPageItems %>"
                  maxIndexPages="<%= maxIndexPages %>" isOffset="<%= true %>" export ="offset,currentPageNumber=pageNumber" scope="request">        
        <%  for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vector.size()); i < l; i++){
                TblConceptos con = (TblConceptos) vector.get(i);
                String estilo = (i%2==0?"filagris":"filaazul");
                if (con.getReg_status().equals("A")) estilo = "filaverde";
				String  valor = (con.getCurrency().equals("DOL")? UtilFinanzas.customFormat("#,##0.00",con.getVlr() ,2) : UtilFinanzas.customFormat("#,###",con.getVlr() ,0)  );
		%>
        <pg:item>
        <tr class='<%= estilo %>' style='cursor:hand;' onMouseOut='cambiarColorMouse(this)' onMouseOver='cambiarColorMouse(this)' >
           <td nowrap class='bordereporte' align='center'><input type='checkbox' name='LOV' value='<%= con.getDstrct() + "~" + con.getConcept_code() %>' ></td>
           <td nowrap class='bordereporte' onclick='editar(<%= i %>);' align='center'><%= con.getDstrct()                                   %></td>
           <td nowrap class='bordereporte' onclick='editar(<%= i %>);' align='center'><%= con.getConcept_code()                             %></td>
           <td nowrap class='bordereporte' onclick='editar(<%= i %>);'               >&nbsp;<%= con.getConcept_desc()                       %></td>
           <td nowrap class='bordereporte' onclick='editar(<%= i %>);'               ><%= (!con.getConcept_class().equals("")?con.getClass_desc():"") %></td>
           <td nowrap class='bordereporte' onclick='editar(<%= i %>);' align='center'><%= con.getAccount()                                  %></td>
           <td nowrap class='bordereporte' onclick='editar(<%= i %>);' align='center'><%= (con.getCodigo_migracion()!=null?con.getCodigo_migracion():"") %></td>
           <td nowrap class='bordereporte' onclick='editar(<%= i %>);' align='center'><%= (con.getVisible().equals("Y")?"SI":"NO")          %></td>
           <td nowrap class='bordereporte' onclick='editar(<%= i %>);' align='center'><%= (con.getModif().equals("Y")?"SI":"NO")            %></td>
           <td nowrap class='bordereporte' onclick='editar(<%= i %>);' align='center'><%= (con.getInd_application().equals("V")?"VALOR":"SALDO")  %></td>
           <td nowrap class='bordereporte' onclick='editar(<%= i %>);' align='center'><%= (con.getInd_signo()>0?"POS":"NEG")                %></td>
		   
		   <td nowrap class='bordereporte' onclick='editar(<%= i %>);' align='center'><%= con.getTipo_cuenta()                              %></td>
		   <td nowrap class='bordereporte' onclick='editar(<%= i %>);' align='center'><%= (con.getAsocia_cheque().equals("S")?"SI":"NO")      %></td>
		   <td nowrap class='bordereporte' onclick='editar(<%= i %>);' align='center'><%= (con.getTipo().equals("V")?"VALOR":"PORC")%></td>
		   <td nowrap class='bordereporte' onclick='editar(<%= i %>);' align='right' ><%= valor            %>&nbsp;</td>
		   <td nowrap class='bordereporte' onclick='editar(<%= i %>);' align='center'><%= con.getCurrency()                                 %></td>
		   <td nowrap class='bordereporte' onclick='editar(<%= i %>);' align='center'><%= (con.getDescuento_fijo().equals("S")?"SI":"NO")   %></td>
		   <td nowrap class='bordereporte' onclick='editar(<%= i %>);' align='center'><%= (con.getIngreso_costo().equals("I")?"INGRESO":"COSTO")  %></td>
        </tr>   
        </pg:item>
        <% } %>
        <tr>
           <td colspan="18" align="center" class='filagris' >
              <pg:index>
              <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
              </pg:index> 
           </td>
        </tr>
        </pg:pager>          
        </table>
    </td>
    </tr>

    </table>
    
    
    <!-- *************************************************************** -->
    </td>
    </tr>
    </table>    
    <input type='hidden' name='Opcion' value=''>
    </form>
    
    

        <br>
        <input type='image' src='<%=BASEURL%>/images/botones/anular.gif'   onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick="accion('Anular')">
        &nbsp;        
        <input type='image' src='<%=BASEURL%>/images/botones/activar.gif'  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick="accion('Activar')">
        &nbsp;        
        <input type='image' src='<%=BASEURL%>/images/botones/eliminar.gif' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick="accion('Delete')">
        &nbsp;
        <input type='image' src='<%=BASEURL%>/images/botones/salir.gif'    onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='window.close();'>
        
    
    <%} else { msg = "No hay conceptos por mostrar"; }%>
    
 
<%if( msg!=null) {%>
   <p>
   <table border="2" align="center">
       <tr>
           <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
               <tr>
                   <td width="229" align="center" class="mensajes"> <%= msg %></td>
                   <td width="29" background="<%= BASEURL %>/images/cuadronaranja.JPG">&nbsp;</td>
                   <td width="58">&nbsp;</td>
               </tr>
           </table></td>
       </tr>
   </table>
  </p>
<%}%>     
</center>    



</div>
<%=datos[1]%>
</body>
</html>
