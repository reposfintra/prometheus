<!--
     - Author(s)       :      MARIO FONTALVO
     - Date            :      25/01/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--
     - @(#)
     - Description: Repoortes
--%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>MANTENIMIENTO DE CONCEPTOS</title>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body>
<BR>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">MANTENIMIENTO DE CONCEPTOS</div></td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2"> PARAMETROS DE REGISTRO DE CONCEPTOS</td>
        </tr>
        <tr class='fila'>
          <td colspan='2' > Listado de Conceptos Generales de la base de datos a traves de esta vista se pueden realizar la siguientes operaciones</td>
        </tr>
        <tr>
          <td  class="fila" align='center'> <input type='checkbox'>  </td>
          <td  class="ayudaHtmlTexto">Casillas de verificacion para seleccionar los conceptos con que se desean trabajar</td>
        </tr>
        <tr>
          <td  class="fila" align='center'> <img src='<%= BASEURL %>/images/botones/anular.gif'>  </td>
          <td  class="ayudaHtmlTexto">Boton de ANULACION del registro de la base de datos</td>
        </tr>
        <tr>
          <td class="fila" align='center'> <img src='<%= BASEURL %>/images/botones/activar.gif'>   </td>
          <td class="ayudaHtmlTexto">Boton de ACTIVACION del registro de la base de datos</td>
        </tr>
        <tr>
          <td class="fila" align='center'> <img src='<%= BASEURL %>/images/botones/eliminar.gif'>   </td>
          <td class="ayudaHtmlTexto">Boton de ELIMINACION del registro de la base de datos</td>
        </tr>


      </table>
    </td>
  </tr>
</table>
</body>
</html>
