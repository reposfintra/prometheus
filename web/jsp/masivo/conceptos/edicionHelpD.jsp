<!--
     - Author(s)       :      MARIO FONTALVO
     - Date            :      25/01/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--
     - @(#)
     - Description: Repoortes
--%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>MANTENIMIENTO DE CONCEPTOS</title>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body>
<BR>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">MANTENIMIENTO DE CONCEPTOS</div></td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2"> PARAMETROS DE REGISTRO DE CONCEPTOS</td>
        </tr>
        <tr>
          <td width="172" class="fila"> DISTRITO </td>
          <td width="502"  class="ayudaHtmlTexto">Distrito del concepto</td>
        </tr>
        <tr>
          <td width="172" class="fila"> CODIGO </td>
          <td width="502"  class="ayudaHtmlTexto">Codigo del concepto.</td>
        </tr>
        
        <tr>
          <td  class="fila">DESCRIPCION</td>
          <td  class="ayudaHtmlTexto">Descripcion del concepto</td>
        </tr>
        <tr>
          <td class="fila">CLASE</td>
          <td  class="ayudaHtmlTexto">Clase relacionada al concepto , si le asigna el mismo este se considera como una clase, en caso contrario es un concepto especifico de un concepto general</td>
        </tr>
        <tr>
          <td width="172" class="fila"> CUENTA CONTABLE </td>
          <td width="502"  class="ayudaHtmlTexto">Codigo de cuenta contable de concepto.</td>
        </tr>
        <tr>
          <td width="172" class="fila"> CODIGO DE MIGRACION </td>
          <td width="502"  class="ayudaHtmlTexto">Codigo de Migracion del concepto.</td>
        </tr>

        <tr>
          <td width="172" class="fila"> VISIBLE </td>
          <td width="502"  class="ayudaHtmlTexto">Indica si este concepto es visible desde otras vistas</td>
        </tr>
        
        <tr>
          <td width="172" class="fila"> MODIFICABLE </td>
          <td width="502"  class="ayudaHtmlTexto">Indica si este concepto permite ser modificado desde otra vista.</td>
        </tr>
        <tr>
          <td width="172" class="fila"> INDICADOR DE APLICACION </td>
          <td width="502"  class="ayudaHtmlTexto">Indica si este valor aplica a SALDO o a VALOR.</td>
        </tr>
        <tr>
          <td width="172" class="fila"> INDICADOR DE SIGNO </td>
          <td width="502"  class="ayudaHtmlTexto">Indica si este valor debe considerarse positivo o negativo</td>
        </tr>


      </table>
    </td>
  </tr>
</table>
</body>
</html>
