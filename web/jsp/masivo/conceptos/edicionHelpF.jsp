<!--
     - Author(s)       :      MARIO FONTALVO
     - Date            :      25/01/2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%--   
     - @(#)  
     - Description: Reportes Viajes Presupuestado vs. Ejecutado
--%> 
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
    <title>MANTENIMIENTO DE CONCEPTOS</title>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body>
<BR>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td width="674" height="24"><div align="center">MANTENIMIENTO DE CONCEPTOS.</div></td>
        </tr>
		<tr class="subtitulo1">
          <td> PARAMETROS DE MANTENIMIENTO DE CONCEPTOS</td>
        </tr>
		<tr class="ayudaHtmlTexto">
		<td>Formulario de Mantenimiento de Conceptos<br>
		    <div align="center"><img src="<%= BASEURL %>/images/ayuda/conceptos/dibujo01.JPG" align="absmiddle"></div>
                    <br>	
        	    Parametros generales el registro y edicion de conceptos<br>
 
		  </td>
		</tr>
      </table>
    </td>
  </tr>
</table>
</body>
</html>
