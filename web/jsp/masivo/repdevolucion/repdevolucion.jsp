<!--
- Autor : ricardo Rosero
- Date  : 20/01/2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, encargada de generar un reprote de devoluciones
--%>
<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<% String Mensaje       = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):""; %>
<html>
<head>
<title>Reporte Devolucion</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="<%=BASEURL%>/js/validar.js"></script>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Generar Reporte"/>
</div>
<%
        String accion = CONTROLLER+"?estado=Reporte&accion=Devolucion";
%>
 <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
    <form action="<%=accion%>" method="post" name="forma" id="forma" onSubmit="return validarTCamposLlenos();">
      <table width="577" border="2" align="center">
        <tr>
          <td><table width="100%" class="tablaInferior">
            <tr>
              <td width="50%" class="subtitulo1">Generar Informe </td>
              <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20">
              </td>
            </tr>
          </table>
            <table width="100%" border="0" align="center" class="tablaInferior">
              <tr class="fila">
                <td><strong>Fecha inicial </strong></td>
                <td>
                  <input name='fechai' type='text' class="textbox" id="fechai" style='width:120' value='' readonly>
                  <a href="javascript:void(0);" class="link" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.forma.fechai);return false;"  HIDEFOCUS > <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16"
               border="0" alt="De click aqu&iacute; para ver el calendario."></a> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
              </tr>
              <tr class="fila" >
                <td><strong>Fecha final </strong></td>
                <td>
                  <input name='fechaf' type='text' class="textbox" id="fechaf" style='width:120' value='' readonly>
                  <a href="javascript:void(0);" class="link" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.forma.fechaf);return false;"  HIDEFOCUS > <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16"
               border="0" alt="De click aqu&iacute; para ver el calendario."></a> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
              </tr>
            </table></td>
        </tr>
      </table>
      <div align="center"><br>
        <img src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="forma.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
		<img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >
		</div>
		</form>
   <iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
</iframe>
   <p>
     <%if(!Mensaje.equals("")){%>
   </p>
   <p>
   <table border="2" align="center">
     <tr>
       <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
           <tr>
             <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
             <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
             <td width="58">&nbsp;</td>
           </tr>
       </table></td>
     </tr>
   </table>
   <p></p>
   <%}%>

</body>
</html>
