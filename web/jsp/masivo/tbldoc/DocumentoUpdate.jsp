<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Asignaci&oacute;n de Demora</title>
<link href="<%= BASEURL %>/css/estilotsp.css" rel='stylesheet'>
<script src="<%=BASEURL%>/js/validar.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<%
        String url = CONTROLLER + "?estado=Documento&accion=Anular&cmd=show";
%>
<script>        
        function anular(){
                forma.action = '<%=url%>';
                forma.submit();
        }
</script>
<script src="<%=BASEURL%>/js/validate.js">
</script>
<script language="JScript">
	function validar(){
		if(validate_field(forma.c_document_type,"",true,false,null,null,true) && 
                        validate_field(forma.c_document_name,"",true,false,null,null,false) && 
                        validate_field(forma.c_sigla,"",true,false,null,null,true)){
			return true;
		}
		else{
			return false;
		}
	}
</script>
</head>
<body <%if(request.getParameter("mensaje").equalsIgnoreCase("MsgModificado") ){ %>onLoad="parent.opener.location.reload();"<%}%>>
<%
        Documento doc = model.documentoSvc.getDocumento();
        model.documentoSvc.setDocumento(null);
        if( doc==null ) doc = new Documento();

        TreeMap si_no = new TreeMap();
        si_no.put("Si","S");
        si_no.put("No","N");        
        
        String estado = request.getParameter("estado");
        if ( estado == null ) estado = "";
        
        if( !estado.matches("Anulado") ){
%>
<form name="forma" id="forma" method="post" action="<%=CONTROLLER%>?estado=Documento&accion=Update&cmd=show"  onSubmit="return validar();">
  <table width="695" border="2" align="center">
    <tr>
      <td><table width="100%" align="center" cellpadding="3" cellspacing="2" class="tablaInferior">
        <tr class="fila">
          <td colspan="2" class="subtitulo1">Informaci&oacute;n del Documento</td>
          <td colspan="2" class="barratitulo"><img src="<%=BASEURL%>//images/titulo.gif">
            <input name="c_document_type" type="hidden" id="c_document_type" value="<%=doc.getC_document_type() %>">
            <input name="c_cia" type="hidden" id="c_cia" value="<%=doc.getDistrito() %>"></td>
          </tr>
        <tr class="fila">
          <td width="17%">Tipo de documento</td>
          <td width="24%"><input readonly name="c_ndocument_type" type="text" class="textbox" id="c_ndocument_type" maxlength="15" value="<%= doc.getC_document_type()%>">            </td>
          <td width="22%">Nombre del documento </td>
          <td width="37%"><input name="c_document_name" type="text" class="textbox" id="c_document_name" maxlength="15" value="<%= doc.getC_document_name()%>">
            <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
        </tr>
        <tr class="fila">
          <td>Banco</td>
          <td><input:select name="c_banco" options="<%= si_no %>" default="<%= doc.getC_banco() %>" attributesText="class='texto'"/><img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          <td>Referencia</td>
          <td><input name="c_ref_1" type="text" class="textbox" id="c_ref_1" size="40" maxlength="30" value="<%= doc.getC_ref_1()%>"></td>
        </tr>
        <tr class="fila">
          <td>Sigla</td>
          <td><input name="c_sigla" type="text" class="textbox" id="c_sigla" maxlength="6" value="<%= doc.getC_sigla()%>">
            <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
    </tr>
  </table>
  <div align="center"><br>
    <img src="<%=BASEURL%>/images/botones/modificar.gif" name="c_modificar" onClick="if( validar() ) forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"  style="cursor:hand ">&nbsp;  <img src="<%=BASEURL%>/images/botones/anular.gif" name="c_anular" onClick="anular();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"  style="cursor:hand ">&nbsp;  <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_buscar" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"  style="cursor:hand "></div>
</form>
<%              if(request.getParameter("msg")!=null){%>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes"><%=request.getParameter("msg").toString() %></td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>	
<%
                }
        }
        else{
%>
<table border="2" align="center">
  <tr>
    <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="282" align="center" class="mensajes"><%=request.getParameter("msg").toString() %></td>
          <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="78">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<br>
<table width="415" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr align="left">
    <td><img src="<%=BASEURL%>/images/botones/salir.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="parent.close();"> 
  </tr>
</table>
<%
        }
%>
</body>
</html>
