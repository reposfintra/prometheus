<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Asignaci&oacute;n de Demora</title>
<script src="<%=BASEURL%>/js/validar.js"></script>
<script src="<%=BASEURL%>/js/validate.js">
</script>
<script language="JScript">
	function validar(){
		if(validate_field(forma.c_document_type,"",true,false,null,null,true) && 
                        validate_field(forma.c_document_name,"",true,false,null,null,false) && 
                        validate_field(forma.c_sigla,"",true,false,null,null,true)){
			return true;
		}
		else{
			return false;
		}
	}
</script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body>
<%
        Documento doc = model.documentoSvc.getDocumento();
        model.documentoSvc.setDocumento(null);
        if( doc==null ) doc = new Documento();;

        TreeMap si_no = new TreeMap();
        si_no.put("Si","S");
        si_no.put("No","N");        
%>
<form name="forma" id="forma" method="post" action="<%=CONTROLLER%>?estado=Documento&accion=Insert&cmd=show"  onSubmit="return validar();">
  <table width="695" border="2" align="center">
    <tr>
      <td><table width="100%" align="center" cellpadding="3" cellspacing="2" class="tablaInferior">
        <tr class="fila">
          <td colspan="2" class="subtitulo1">Informaci&oacute;n del Documento</td>
          <td colspan="2" class="barratitulo"><img src="<%=BASEURL%>//images/titulo.gif"></td>
          </tr>
        <tr class="fila">
          <td width="20%">Tipo de documento</td>
          <td width="23%"><input name="c_document_type" type="text" class="textbox" id="c_document_type" maxlength="15" value="<%= doc.getC_document_type()%>">
            <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          <td width="23%">Nombre del documento </td>
          <td width="34%"><input name="c_document_name" type="text" class="textbox" id="c_document_name" maxlength="15" value="<%= doc.getC_document_name()%>">
            <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
        </tr>
        <tr class="fila">
          <td>Banco</td>
          <td><input:select name="c_banco" options="<%= si_no %>" default="<%= doc.getC_banco() %>" attributesText="class='textbox'"/><img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          <td>Referencia</td>
          <td><input name="c_ref_1" type="text" class="textbox" id="c_ref_1" size="40" maxlength="30" value="<%= doc.getC_ref_1()%>">            </td>
        </tr>
        <tr class="fila">
          <td>Sigla</td>
          <td><input name="c_sigla" type="text" class="textbox" id="c_sigla" maxlength="6" value="<%= doc.getC_sigla()%>">
            <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
    </tr>
  </table>
  <div align="center"><br>
      <img src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="if ( validar() ) forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"  style="cursor:hand ">&nbsp; <img src="<%= BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);"   style="cursor:hand ">&nbsp; <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);"   style="cursor:hand "></div>
</form>
<%if(request.getParameter("msg")!=null){%>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes"><%=request.getParameter("msg").toString() %></td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>	
<%}%>
</body>
</html>
