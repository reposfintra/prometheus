<!--
- Autor : Ing. Jose de la rosa
- Date  : 9 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que modifica un producto
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Modificar Registro Tarjeta</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script>
	function Validar(){		
		if( form1.ncliente.value == '' ){
			alert('Debe escribir la Codigo del cliente');
			form1.cliente.focus;
			return false;
		}
		if( form1.ndescripcion.value == '' ){
			alert('Debe escribir la descripcion');
			form1.descripcion.focus;
			return false;
		}
		if( form1.nfechaini.value == '' ){
			alert('Debe escribir la fecha inicial');
			form1.fechaini.focus;
			return false;
		}
		if( form1.nfechafin.value == '' ){
			alert('Debe escribir la fecha final');
			form1.fechafin.focus;
			return false;
		}
		if( form1.nfechaini.value > form1.nfechafin.value ){
                        alert('La fecha inicial no puede ser mayor que la fecha final');
                        return false;
                }
			return true;
	}
	
        function CompletarCodigo(){		
            if(form1.ncliente.value.length > 0 && form1.ncliente.value.length < 6){
		var tamano = 6-form1.ncliente.value.length;
		var ceros='';
		i =1;
		while(i<=tamano){
			ceros = ceros + '0';
			i++;
		}
		form1.ncliente.value = ceros +form1.ncliente.value;		
            }	
        }		
	</script>
<body <%if(request.getParameter("reload")!=null){%>onLoad="parent.opener.location.reload();redimensionar();"<%}%> onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modificar Caja Lectora"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%    
    Caja_lec dat 			= (Caja_lec) model.cajaLectoraSvc.getDato();    
    dat 				= (dat!=null)?dat : new Caja_lec(); 
    String Mensaje       	= (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);        %>
<FORM name='form1' id='form1' method='POST' action='<%=CONTROLLER%>?estado=CajaLectora&accion=Manager'>
	<table width="500" border="2" align="center">
	<input name="Opcion" type="hidden" id="Opcion">
      <tr>
        <td>
    	<table width="100%" class="tablaInferior">
		<tr class="fila">
		  <td align="left" class="subtitulo1">&nbsp;Modificar Caja Lectora</td>
		  <td align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
		</tr>	
        <tr class="fila">
            <td width="158">Numero Caja </td>
            <td width="238"><%=dat.getNum_caja()%>
            <input name="numcaja" type="hidden"  id="numcaja" value="<%=dat.getNum_caja()%>"></td>
        </tr>
        <tr class="fila">
            <td width="158">Estado</td>
            <%String estado = (dat.getReg_status().equals("A"))?"ANULADO":"ACTIVO";%>
            <td width="238"><%=estado%>
            <input name="numcaja" type="hidden"  id="numcaja" value="<%=dat.getNum_caja()%>"></td>
        </tr>
        <tr class="fila">
                <td colspan="1">Codigo Cliente </td>
                <td colspan="3"><%if(estado.equals("ACTIVO")){%>
                                    <input name="ncliente" type="text" class="textbox" id="ncliente" value="<%=dat.getCodigo_cliente()%>"  maxlength="50" onKeyPress="soloAlfa(event)" onBlur='CompletarCodigo();'>
                                    <input name="cliente" type="hidden"  id="cliente" value="<%=dat.getCodigo_cliente()%>">
				<%}else{%>
                                    <%=dat.getCodigo_cliente()%>
                                <%}%>
		</td>
        </tr>				
        
        <tr class="fila">
            <td>Descripcion</td>
            <td><%if(estado.equals("ACTIVO")){%>
                    <input name="ndescripcion" type="text"  id="ndescripcion" value="<%=dat.getDescripcion()%>"  maxlength="100" onKeyPress="soloAlfa(event)">
                    <input name="descripcion" type="hidden"  id="descripcion" value="<%=dat.getDescripcion()%>">
		<%}else{%>
                    <%=dat.getDescripcion()%>
                <%}%>
           </td>
        </tr>
        
        <tr class="fila">
            <td>Fecha Inicial</td><%String fechai =  (dat.getDato().length() > 10 )?dat.getDato().substring(0,10):"";%>
            <td><%if(estado.equals("ACTIVO")){%>
                    <input name="nfechaini" type="text" class="textbox" id="nfechaini" value="<%=fechai%>"  maxlength="20" readonly>&nbsp;<img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.nfechaini);return false;" HIDEFOCUS>
                    <input name="fechaini" type="hidden"  id="fechaini" value="<%=fechai%>">
                <%}else{%>
                    <%=fechai%>
                <%}%>
            </td>
        </tr>
        
        <tr class="fila">
            <td>Fecha Final</td><%String fechaf =  (dat.getDato().length() > 10 )?dat.getDato().substring(10,dat.getDato().length()):"";%>
            <td><%if(estado.equals("ACTIVO")){%>
                <input name="nfechafin" type="text" class="textbox" id="nfechafin" value="<%=fechaf%>"  maxlength="20" readonly>&nbsp;<img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.nfechafin);return false;" HIDEFOCUS>
                <input name="fechafin" type="hidden"  id="fechafin" value="<%=fechaf%>">
                 <%}else{%>
                    <%=fechaf%>
                <%}%>
            </td>
        </tr>
   		         
    </table>
		</td>
      </tr>
    </table>
	<p>
	<div align="center">
	<%if(estado.equals("ACTIVO")){%>
            <img src="<%=BASEURL%>/images/botones/modificar.gif" style="cursor:hand" title="Modificar un registro" name="modificar"  onClick="form1.Opcion.value='Modificar';if(Validar()){form1.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
            <img src="<%=BASEURL%>/images/botones/anular.gif"    style="cursor:hand" title="Anular un registro" name="anular"        onClick="form1.Opcion.value='Anular';form1.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;	
        <%}else{%>
            <img src="<%=BASEURL%>/images/botones/activar.gif"    style="cursor:hand" title="Activa un registro" name="activar"        onClick="form1.Opcion.value='Activar';form1.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;	
        <%}%>
	<img src="<%=BASEURL%>/images/botones/salir.gif"     style="cursor:hand" name="salir" title="Salir al Menu Principal"    onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" ></div>
	</p>
	<%if(!Mensaje.equals("")){%>
		<table border="2" align="center">
		  <tr>
			<td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
			  <tr>
				<td width="229" align="center" class="mensajes"><%=Mensaje%></td>
				<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
				<td width="58">&nbsp;</td>
			  </tr>
			</table></td>
		  </tr>
		</table>
	<%  }%>

</FORM>
</div>
<%=datos[1]%>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
</body>
</html>
