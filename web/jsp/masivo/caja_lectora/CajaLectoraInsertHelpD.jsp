<!--
	 - Author(s)       :      Jose de la rosa
	 - Description     :      AYUDA DESCRIPTIVA - Extracto
	 - Date            :      01/06/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>AYUDA DESCRIPTIVA </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<% String BASEIMG = BASEURL +"/images/botones/";%> 
<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Caja Lectora </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Insercion de Registros </td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2">INFORMACION</td>
        </tr>
        <tr>
          <td  class="fila">Numero Caja </td>
          <td  class="ayudaHtmlTexto">Campo para digitar el numero de la caja lectora. Este campo es de m&aacute;ximo 30 caracteres.</td>
        </tr>		
        <tr>
          <td  class="fila">Codigo del Cliente</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el codigo del cliente. Este campo es de m&aacute;ximo 50 caracteres.</td>
        </tr>		
		<tr>
          <td  class="fila">Descripcion</td>
          <td  class="ayudaHtmlTexto">Campo para digitar la descripcion. Este campo es de m&aacute;ximo 100 caracteres.</td>
        </tr>		
		<tr>
          <td  class="fila">Fecha inicial</td>
          <td  class="ayudaHtmlTexto">Campo para digitar la fecha inicial. Este campo es de m&aacute;ximo 20 caracteres.</td>
        </tr>		
		<tr>
          <td  class="fila">Fecha final </td>
          <td  class="ayudaHtmlTexto">Campo para digitar la fecha final. Este campo es de m&aacute;ximo 20 caracteres.</td>
        </tr>				
        <tr>
          <td class="fila"><img src="<%=BASEIMG%>aceptar.gif"></td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para confirmar y realizar la inserci&oacute;n del registro.</td>
        </tr>
		<tr>
          <td width="149" class="fila"><img src="<%=BASEIMG%>restablecer.gif"></td>
          <td width="525"  class="ayudaHtmlTexto">Bot&oacute;n para restablecer el formulario en su estado inicial.</td>
        </tr>
		<tr>
          <td width="149" class="fila"><img src="<%=BASEIMG%>salir.gif"></td>
          <td width="525"  class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista actual y volver a la vista del men&uacute;.</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</body>
</html>
