<!--
- Autor : Ing. Juan M. Escandon
- Date  : 04 Agosto 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Vista para mostrar la informacion de las ayudas
--%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE></TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script src="<%= BASEURL %>/js/boton.js"></script>
</HEAD>
<BODY>
<% String BASEIMG = BASEURL +"/images/ayuda/CajaLectora/";%> 
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="1" >      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="20"><div align="center">MANUAL PARA LA INSERCION DE REGISTROS DEL PROGRAMA DE CAJA LECTORA</div></td>
        </tr>
        <tr class="subtitulo1">
          <td>Descripci&oacute;n del funcionamiento del programa para la inserci&oacute;n de registros en la tabla de Tabla Gen "Caja Lec" </td>
        </tr>
        <tr>
          <td  height="18"  class="ayudaHtmlTexto">La interfaz de grabaci&oacute;n de registros es la sgte : </td>
        </tr>
        <tr>
          <td height="18"  class="ayudaHtmlTexto">&nbsp;</td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEIMG%>Insertar.jpg"> </div></td>
        </tr>
        <tr>
          <td  class="ayudaHtmlTexto"><table cellspacing="0" cellpadding="0">
              <tr>
                <td width="960" class="ayudaHtmlTexto" align="center">&nbsp;</td>
              </tr>
          </table>
            <div align="center"></div></td>
        </tr>
      </table></td>
    </tr>	
  </table>
  <br>
  <table width="100%">
    <tr align="center">
      	<td align="center"><img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
      </tr>
   </table>
</BODY>
</HTML>
