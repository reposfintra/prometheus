	<!--
     - Author(s)       :      MARIO FONTALVO
     - Date            :      10/12/2005  
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   
     - @(#)  
     - Description: Formulario asignar clientes a los diferentes soportes
 --%> 
<%@page session   = "true"%> 
<%@page errorPage = "/error/ErrorPage.jsp"%>
<%@page import    = "java.util.*" %>
<%@page import    = "com.tsp.operation.model.beans.*" %>
<%@include file   = "/WEB-INF/InitModel.jsp"%>
<%@include file="/jsp/masivo/asignaciones/generacionJS.jsp"%>
<html>
<head>
    <title>Soporte Clientes</title>
    <script src ="<%= BASEURL %>/js/boton.js"></script>
    <link   href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
    <link   href="<%= BASEURL %>/css/EstilosReportes.css" rel='stylesheet'>
    <script>
        <% showDatosJs (model.SoporteClienteSvc.getListaSoportes() , out, "datosS");   %>
        <% showDatosJs (model.SoporteClienteSvc.getListaClientes() , out, "datosC");   %>
        <% showDatosJs (model.SoporteClienteSvc.getListaRelacion() , out, "datosR"); 
		   String res = (request.getParameter("msg")!= null)? request.getParameter("msg") : "";%>		
        var separador = '~';
        
        function addOption(Comb,valor,texto){
            var Ele = document.createElement("OPTION");
            Ele.value=valor;
            Ele.text=texto;
            Comb.add(Ele);
        }
 
        function loadCombo(datos, cmb){
            cmb.length = 0;
            for (i=0;i<datos.length;i++){
                var dat = (new String(datos[i])).split(separador);
                addOption(cmb, dat[0], dat[1]);
            }
        }   
     
        function move(cmbO, cmbD){
           for (i=cmbO.length-1; i>=0 ;i--)
            if (cmbO[i].selected){
               addOption(cmbD, cmbO[i].value, cmbO[i].text);
            }
           //order(cmbD);
           deleteRepeat(cmbD);
        }   
		
		        
        function remove(cmb){
           for (i=cmb.length-1; i>=0 ;i--)
            if (cmb[i].selected){
               cmb.remove(i);
            }
        }
        
        function order(cmb){
           for (i=0; i<cmb.length;i++)
             for (j=i+1; j<cmb.length;j++)
                if (cmb[i].text > cmb[j].text){
                    var temp = document.createElement("OPTION");
                    temp.value = cmb[i].value;
                    temp.text  = cmb[i].text;
                    cmb[i].value = cmb[j].value;
                    cmb[i].text  = cmb[j].text;
                    cmb[j].value = temp.value;
                    cmb[j].text  = temp.text;
                }
        }        
        
        function deleteRepeat(cmb){
          for (i=0;i<cmb.length;i++){
             for (j=i+1;j<cmb.length;j++){
               if(cmb[i].value == cmb[j].value)
                  cmb.remove(j);
             }
          }
        }        
        
        function moveAll(cmbO, cmbD){
           for (i=cmbO.length-1; i>=0 ;i--){
             if(cmbO[i].value!='' && cmbO[i].text!=''){
               addOption(cmbD, cmbO[i].value, cmbO[i].text)
//               cmbO.remove(i);
             }
           }
           //order(cmbD);
           deleteRepeat(cmbD);
        } 
        
        function removeAll(cmb){
          cmb.length = 0;
        }        
       
        function loadCombo2(datos, cmb, ocurrencia){
		
            cmb.length = 0;
            sw = 0;
            var i, j;
  		 for (j = 0; j< ocurrencia.length; j++){
				if ( ocurrencia[j].selected){

					for (i=0;i<datos.length;i++){
						var dat = (new String(datos[i])).split(separador);
					
						if ( dat[0]==ocurrencia[j].value){
							cmb.value = dat[1];
							if (cmb.selectedIndex == -1)						
							   addOption(cmb, dat[1], getDescripcion(dat[1], datosS));
						}
					} 
				}
			}	
			
			order(cmb);
			
        }       
 		
		function loadCombo2A(datos, cmb, ocurrencia){
		
            cmb.length = 0;
            sw = 0;
            var i, j;
  					for (i=0;i<datos.length;i++){
						var dat = (new String(datos[i])).split(separador);
					
						if ( dat[0]==ocurrencia){
							cmb.value = dat[1];
							if (cmb.selectedIndex == -1)						
							   addOption(cmb, dat[1], getDescripcion(dat[1], datosS));
						}
					} 
				//}
			//}	
			
			order(cmb);
			
        } 
		
        function getDescripcion (id, datos){
            var i;
            for (i=0;i<datos.length;i++){
                var dat = (new String(datos[i])).split(separador);
                if (dat[0]==id){
                    return dat[1];
                }
            }        
            return "";
        }
        
        function validar(){
            if (form1.Clientes.length==0){
               alert ('Debe indicar un cliente para poder continuar.....');
               return false;
            }
            var i;
            for (i=0;i<form1.Soportes.length;i++)
              form1.Soportes[i].selected = true;
            for (i=0;i<form1.Clientes.length;i++)
              form1.Clientes[i].selected = true;
              
			  
			  
           return true;   
        }        
    </script>
    <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onLoad="seleccionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Asignacion Soportes Clientes"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">


    <center>


    <form action="<%= CONTROLLER %>?estado=Soporte&accion=Cliente&Opcion=Grabar" method="post" name="form1" id="form1" >
	 <%if(!res.equals("")){%>
	 <br>
	  <table border="2" align="center">
	  <tr>
		<td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
		  <tr>
			<td width="300" align="center" class="mensajes"><%=res%></td>
			<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
			<td width="58">&nbsp;</td>
		  </tr>
		</table></td>
	  </tr>
	</table>
	<script>
	  parent.opener.location.reload();
	</script>
	
	 <%}%>
    <table width="1000" height="378" border='2' align='center' >
      <tr>
        <td height="292" colspan='2' align="center" class='fila' >
       	  <table width="100%" height="327" align="center" class="tablaInferior">
            <tr>
				<td height="28" colspan="12">
		  			<table width="100%"  border="0">
  						<tr>
    						<td width="37%" class="subtitulo1">ASIGNACION SOPORTES CLIENTES</td>
    						<td width="63%" class="barratitulo"><img src="<%= BASEURL %>/images/titulo.gif" width="32" height="20"></td>
  						</tr>
			  </table>           	  </td>
          	</tr>
			<tr class="tblTitulo">
            <th width="22%" height="31">Clientes</th>
            <th width="5%" class="bordereporte" bgcolor='#D1DCEB'></th>
            <th width="22%">Clientes a Procesar</th>
			<th width="0%"  class='barratitulo'></th>
            <th width="22%" >Soportes a Procesar</th>
            <th width="5%" class="bordereporte" bgcolor='#D1DCEB'></th>
            <th width="22%">Soportes</th>
            

          </tr>
          <tr class="tbltitulo">
			<th height="200"><select name="ClientesG" size="18" multiple class="select"  style="width:100%" onchange='loadCombo2(datosR, form1.Soportes, this);'>
			</select></th>            
            <th class="bordereporte" bgcolor='#D1DCEB'>
                    <image src='<%= BASEURL %>/images/botones/envDer.gif'        style="cursor:hand" onclick="move     (ClientesG, Clientes );" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
                    <image src='<%= BASEURL %>/images/botones/enviarDerecha.gif'     style="cursor:hand" onclick="moveAll  (ClientesG, Clientes );" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>								
                    <image src='<%= BASEURL %>/images/botones/envIzq.gif'        style="cursor:hand" onclick="remove   (Clientes );" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
                    <image src='<%= BASEURL %>/images/botones/enviarIzq.gif' style="cursor:hand" onclick="removeAll(Clientes );" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>		
            </th>
            <th><select name="Clientes"  size="18" multiple class="select" style="width:100%" >
            </select></th>
			            
            <td class="bordereporte" bgcolor='#D1DCEB'>&nbsp;</td>
			
			<th><select name="Soportes" size="18" multiple class="select" style="width:100%">
			</select></th>
            <th class="bordereporte" bgcolor='#D1DCEB'>
                    <image src='<%= BASEURL %>/images/botones/envIzq.gif'        style="cursor:hand" onclick="move     (SoportesG, Soportes );" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
                    <image src='<%= BASEURL %>/images/botones/enviarIzq.gif'     style="cursor:hand" onclick="moveAll  (SoportesG, Soportes );" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
                    <image src='<%= BASEURL %>/images/botones/envDer.gif'        style="cursor:hand" onclick="remove   (Soportes );" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
                    <image src='<%= BASEURL %>/images/botones/enviarDerecha.gif' style="cursor:hand" onclick="removeAll(Soportes );" onMouseOver='botonOver(this);' onMouseOut='botonOut(this);'><br>
            </th>
			<th><select name="SoportesG" size="18" multiple class="select" style="width:100%" >
			</select></th>
            
			
          </tr>	        
        </table>
        <br>      </td>
      </tr>
    </table>
    <br>
    <img src='<%=BASEURL%>/images/botones/aceptar.gif' style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='if (validar()) { form1.submit(); } '>
    &nbsp;
    <img src='<%=BASEURL%>/images/botones/salir.gif'  style='cursor:hand' onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onclick='window.close();'>
    
    </form>
    <center>
</div>    
</body>
</html>
<script>
	
    loadCombo (datosS,form1.SoportesG);
    loadCombo (datosC,form1.ClientesG);
    <%  String codcli = (String) request.getParameter("codcli");
	    if (codcli!=null && !codcli.equals("")) { %>
		  
			
	<% } %>	
	
	function seleccionar(){
			   <% if (codcli!=null && !codcli.equals("")) { %>
		    //alert();
			
			form1.ClientesG.value = '<%= codcli %>';
			loadCombo2A(datosR, form1.Soportes, '<%= codcli %>');			
			move(form1.ClientesG, form1.Clientes );
			
			<%}%>
		
	}
	
</script>
