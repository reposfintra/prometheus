<!--
- Autor : Ing. Juan Manuel Escando Perez
- Date  : 29 Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite la creacion del archivo PDF
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="java.util.Vector"%>
<%@page import="java.text.*"%>
<%@page import="com.tsp.pdf.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ page import="java.io.File"%>
<html>
<head>
<title>Hoja de Control de Viaje</title>
</head>
<h1>
JBuilder Generated JSP
</h1>
<%
  HojaControlViajePDF hvc =  new HojaControlViajePDF();
  
  String ruta = application.getRealPath("/");
  
  /*
  File xslt = new File(ruta + "Templates/hvc2.xsl");
  File pdf = new File(ruta + "pdf/hvc.pdf");*/
  HojaControlViaje hoja = model.hojaControlViajeSvc.getDato();
  File xslt = (File) session.getAttribute("ArchivoXSLT");
  File pdf = (File) session.getAttribute("ArchivoPDF");
  String planilla = (String) session.getAttribute("Planilla");
  
  hvc.generarPDF(xslt, pdf, hoja);
  response.sendRedirect("pdf/hvc.pdf");
  
%>
</body>
</html>
