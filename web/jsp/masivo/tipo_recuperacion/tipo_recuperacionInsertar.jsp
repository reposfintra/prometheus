<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head><title>Insertar Codigo Discrepancia</title>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../css/estilostsp.css" rel='stylesheet' type="text/css">
</script>
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Tipo de Recuperacion de Anticipo"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<FORM name='forma' id='forma' method='POST' onSubmit="" action='<%=CONTROLLER%>?estado=Tipo_RecuperacionA&accion=Insert'>
  <table width="460" border="2" align="center">
    <tr>
      <td width="3350">
	    <table width="100%" border="0"align="center" >
          <tr>
            <td colspan='3'>
              <table cellpadding='0' cellspacing='0' width='100%'>
                <tr class="fila">
                  <td width="293" align="left"  class="subtitulo1" nowrap>Ingresar Tipo Recuperacion Anticipo</td>
                  <td align="left" width="147" class="barratitulo"><img src="<%=BASEURL%>//images/titulo.gif" width="32" height="20">
                </tr> 
			  </table>
			</td>
		  </tr>
          <tr class="fila">
            <td width="119">Codigo</td>
            <td width="227"><input name="c_codigo" type="text" class="textbox" id="c_codigo" onKeyPress="soloAlfa(event)" maxlength="6"></td>
          </tr>
          <tr class="fila">
            <td>Descripcion</td>
            <td><textarea name="c_descripcion" class="textbox" id="c_descripcion" style="width:100% "></textarea></td>
          </tr>        
        </table>
      </td>
    </tr>
  </table>
</FORM>
<center>
	<img src="<%=BASEURL%>/images/botones/aceptar.gif" name="i_aceptar" onClick="validarTCamposLlenos();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">  <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> <img src="<%=BASEURL%>/images/botones/cancelar.gif" name="c_buscar" onClick="forma.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">    	 
</center>

<%String mensaje = (String) request.getAttribute("mensaje"); 
 %>
    <%  if(mensaje!=null){%>
<table border="2" align="center">
  <tr>
    <td>
	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%= mensaje %></td>
        <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
      </tr>
    </table></td>
  </tr>
</table>
    <%  }%>	
</div>
</body>
</html>
