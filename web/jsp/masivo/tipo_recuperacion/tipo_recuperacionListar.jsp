<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head><title>Listar Codigo Discrepancia</title>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
 <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
 <script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Tipos de Recuperacion de Anticipos"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<% 	Vector vec = model.trecuperacionaService.getTipo_Recuperacion_Anticipo();
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 5;
    int maxIndexPages = 5;
	if (vec.size() > 0) {

%>
<table width="70%" border="2" align="center">
  <tr>
    <td>
      <table width="100%"  align="center">
        <tr>
          <td class="barratitulo">
	        <table width="100%">
              <tr>
                <td width="50%" align="left"  class="subtitulo1">Tipos de Recuperacion de Anticipo</td>
                <td align="left" width="50%" class="barratitulo"><img src="<%=BASEURL%>//images/titulo.gif" width="32" height="20">
              </tr> 
	        </table>
	      </td>
  		</tr>
  	  <table width="100%" border="1" cellpadding="4" cellspacing="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
    	<tr class="tblTitulo" align="center">
          <td width="162" nowrap >Codigo</td>
          <td width="300" nowrap >Descripcion</td>
		</tr>
    <pg:pager
        items="<%=vec.size()%>"
        index="<%= index %>"
        maxPageItems="<%= maxPageItems %>"
        maxIndexPages="<%= maxIndexPages %>"
        isOffset="<%= true %>"
        export="offset,currentPageNumber=pageNumber"
        scope="request">
<%
    for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vec.size()); i < l; i++){
        Tipo_Recuperacion_Anticipo u = (Tipo_Recuperacion_Anticipo) vec.elementAt(i);%>
        <pg:item>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" title="Modificar Tipo Recuperacion de Anticipo..." onClick="window.open('<%=CONTROLLER%>?estado=Tipo_RecuperacionA&accion=Serch&c_codigo=<%=u.getCodigo()%>&listar=False','myWindow','status=no,scrollbars=no,width=450,height=230,resizable=yes');">
          <td><span class="style7"><%=u.getCodigo()%></span></td>
          <td><span class="style7"><%=u.getDescripcion()%></span></td>
	    </tr>
      </pg:item>
<%  }%>
    	<tr class="pie">
          <td td height="20" colspan="9" nowrap align="center">          <pg:index>
                <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
        </pg:index>
          </td>
    	</tr>
    </pg:pager>        
	  </table>
	</td>
  </tr>
</table>
<%}else {%>
<table border="2" align="center">
      <tr>
        <td>
	      <table width="70%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
              <td width="229" align="center" class="mensajes">No se encontraron Tipos de Recuperacionde Anticipos</td>
              <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
            </tr>
          </table>
	    </td>
      </tr>
    </table>
<%}%>
<p>
<table width="70%" align="center">
  <tr>
  	<td>
      <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="location.href='<%=BASEURL%>/jsp/masivo/tipo_recuperacion/tipo_recuperacionBuscar.jsp'">
    </td>
  </tr>
</table>		
</div>
</body>
</html>
