<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head><title>Insertar Codigo Discrepancia</title>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../css/estilostsp.css" rel='stylesheet' type="text/css">
</script>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<body onresize="redimensionar()" <%if(request.getParameter("reload")!=null){%>onLoad="window.opener.location.reload();"<%}%>>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Tipo de Recuperacion de Anticipo"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<
<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");
    Tipo_Recuperacion_Anticipo u = model.trecuperacionaService.getTipo_Recuperacion();    
    String mensaje = (String) request.getAttribute("mensaje");
%>
<FORM name='forma' id='forma' method='POST' onSubmit="" action='<%=CONTROLLER%>?estado=Tipo_RecuperacionA&accion=Update&modif=1'>
 <table width="460" border="2" align="center">
    <tr>
      <td width="3350">
	    <table width="100%" border="0"align="center" >
          <tr>
            <td colspan='3'>
              <table cellpadding='0' cellspacing='0' width='100%'>
                <tr class="fila">
                  <td width="215" align="left"  class="subtitulo1">Modificar Tipo Recuperacion Anticipo</td>
                  <td align="left" width="202" class="barratitulo"><img src="<%=BASEURL%>//images/titulo.gif" width="32" height="20">
                </tr> 
			  </table>
			</td>
		  </tr>
          <tr class="fila">
            <td width="119">Codigo</td>
            <td width="227"><input name="c_codigo" type="text" class="textbox" id="c_codigo" onKeyPress="soloAlfa(event)" value="<%=u.getCodigo()%>" maxlength="6" readonly=""></td>
          </tr>
          <tr class="fila">
            <td>
                <span class="letra">Descripcion</span>                
            </td>
            <td><textarea name="c_descripcion" class="textbox" id="c_descripcion" style="width:100% "><%=u.getDescripcion()%></textarea></td>
          </tr>        
          <tr class="pie">
            <td align='center' colspan="3" >
<%              if (mensaje!="MsgAnulado"){%>
					<input type="submit" onclick="forma.action='<%=CONTROLLER%>?estado=Tipo_RecuperacionA&accion=Update&modif=1'" name="c_Submit1" value="Modificar" class="boton">&nbsp;
					<input type="submit" onclick="forma.action='<%=CONTROLLER%>?estado=Tipo_RecuperacionA&accion=Update'" name="c_Submit2" value="Anular" class="boton">&nbsp;              
<%              }
				if (mensaje!="MsgAgregado"){%>
					<button onclick="window.close()" class="boton">Cancelar</button>                
<%            	}else{%>
					<button onclick="window.location='<%=BASEURL%>/inicio.htm'" class="boton">Cancelar</button>
<%            	}%>
            </td>
          </tr>        
        </table>
	  </td>
    </tr>
  </table>	  
</FORM>
<center>
	<img src="<%=BASEURL%>/images/botones/modificar.gif" name="i_modificar" onClick="forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> <img src="<%=BASEURL%>/images/botones/anular.gif" name="i_anular" onClick="window.location='<%=CONTROLLER%>?estado=Tipo_RecuperacionA&accion=Update&c_codigo=<%=u.getCodigo()%>&c_descripcion=<%=u.getDescripcion()%>'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">    	 
</center>
<%  if(mensaje!=null){%>	
<table border="2" align="center">
  <tr>
    <td>
	<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes">Tipo de Recuperacion  modificado con �xito!</td>
        <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
      </tr>
    </table></td>
  </tr>
</table>	    
<%}%>
</div>
</body>
</html>
