<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head><title>Insertar Codigo Discrepancia</title>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../css/estilostsp.css" rel='stylesheet' type="text/css">
</script>
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Tipo de Recuperacion de Anticipo"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<%
    Usuario usuario = (Usuario) session.getAttribute("Usuario");    
%>
<form name="form2" method="post" action="<%=CONTROLLER%>?estado=Tipo_RecuperacionA&accion=Serch&listar=True">
  <table width="448" border="2" align="center">
    <tr>
      <td>
	    <table width="100%" border="0"align="center" class="tablaInferior">
          <tr>
            <td colspan='3'>
              <table cellpadding='0' cellspacing='0' width='100%'>
                <tr class="fila">
                  <td width="186" align="left"  class="subtitulo1">Buscar Tipo de Recuperacion de Anticipos</td>
			      <td align="left" width="202" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                </tr> 
			  </table>
			</td>
		  </tr>       
          <tr class="fila">
            <td width="108">Codigo</td>
            <td width="159"><input name="c_codigo" type="text" class="textbox" id="c_codigo" onKeyPress="soloAlfa(event)" maxlength="6"></td>
          </tr>        
          <tr class="fila">
            <td width="108">Descripcion</td>
            <td width="159"><input name="c_descripcion" type="text" class="textbox" id="c_descripcion" maxlength="45"></td>
          </tr> 
          <tr class="fila">
            <td colspan="2" align="right"><a href="<%=CONTROLLER%>?estado=Tipo_RecuperacionA&accion=Serch&listar=True&c_codigo=&c_descripcion">Ver Todos <img src="<%= BASEURL%>/images/botones/iconos/lupa.gif"  width="20" height="20"></a></div></td>            
          </tr>        
        </table>
      </td>
	</tr>
  </table>
</form>
<center>
	<img src="<%=BASEURL%>/images/botones/buscar.gif" name="i_buscar" onClick="form2.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">  <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> <img src="<%=BASEURL%>/images/botones/cancelar.gif" name="c_buscar" onClick="form2.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">    	 
</center>
</div>
</body>
</html>
