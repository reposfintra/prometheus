<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="java.util.*" %>
<%@page import="com.tsp.util.Util"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
   <title>Manejo de Imagen</title> 
   <script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
   <link href="<%= BASEURL %>/css/estilotsp.css" rel='stylesheet'>
   <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
	<script SRC='<%=BASEURL%>/js/boton.js'></script>
	<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body <%if(request.getParameter("mensaje").equalsIgnoreCase("MsgModificado") ){ %>onLoad="parent.opener.location.reload();parent.opener.parent.opener.location.reload();"<%}%>>
  <center>

  <!-- Parametros de configuracion -->
<%    
        String  codestado  = ( request.getParameter("codestado")!=null)? request.getParameter("codestado") : "";
        String  tipo = ( request.getParameter("tipo") !=null)? request.getParameter("tipo") : "";
        String fileLength = "1258292";
        String path = ( request.getParameter("path") !=null)? request.getParameter("path") : "";
        
        String msj = ( request.getParameter("comentario")==null )?"":request.getParameter("comentario");

        if(!msj.matches("1")){
%>
  
  <FORM ACTION='<%=BASEURL%>/updateEstado.do?maxFileSize=<%=fileLength%>' method='post' id="forma" enctype="MULTIPART/FORM-DATA" NAME='formulario' onSubmit='return validarTCamposLlenos();' >
    <table width="716" border="2" align="center">
      <tr>
        <td><table width="708" border="0"align="center" bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td colspan='2'>
              <table cellpadding='0' cellspacing='0' width='100%'>
                <tr class="fila">
                  <td align="left" width='50%' class="subtitulo1">Cambiar im&aacute;gen del C&oacute;digo de Estado </td>
                  <td align="left" width='50%' class="barratitulo"><img src="<%=BASEURL%>//images/titulo.gif"></td>
                </tr>
            </table></td>
          </tr>
          <TR class="fila">
            <TD width="10%">Imagen </TD>
            <TD width="90%"><input  accept="image/jpeg, image/gif, image/x-ms-bmp" name='filename' type='file' class="textbox" id="filename" value="<%= path %>" size="45">
            <input name="c_tipo" type="hidden" id="c_tipo" value="<%=tipo %>">
            <input name="c_codestado" type="hidden" id="c_codestado"  value="<%=codestado %>"></TD>
          </TR>
        </TABLE>        </td>
      </tr>
    </table>
  
    <div align="center"><br>
      <img src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="if ( validarTCamposLlenos() ) forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp; <img src="<%= BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);" >&nbsp; <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >    </div>
  </FORM>
    <!-- Mensaje -->
  <%if(msj!=null && !msj.equals("")){%> 

      <table border="2" align="center">
      <tr>
        <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="320" align="center" class="mensajes"><%=msj%></td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="58">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table>
 <%}%>
<%
        }
        else{
%>  
<table border="2" align="center">
  <tr>
    <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="282" align="center" class="mensajes">Se ha actualizado la imagen exitosamente.</td>
          <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="78">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>  
<br>
<table width="415" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr align="left">
    <td><img src="<%=BASEURL%>/images/botones/salir.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="parent.close();"> 
  </tr>
</table>
<%
        }
%>
  
  


 
  
</body>
</html>
