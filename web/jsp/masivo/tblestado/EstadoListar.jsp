<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
 
<html>
<head>
<title>Listado de Proveedor</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body>
<p>&nbsp;</p>
<p>
<%  
        String style = "simple";
        String position =  "bottom";
        String index =  "center";
        int maxPageItems = 10;
        int maxIndexPages = 10;
        Vector estados = model.tbl_estadoSvc.obtenerEstados();
        TblEstado estado;

        if ( estados.size() > 0 ){  
%>
</p>
<table width="90%" border="2" align="center">
    <tr>
      <td>
	  <table width="100%">
              <tr>
                <td width="385" class="subtitulo1">Detalles del C&oacute;digo de Estado </td>
                <td width="484" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif"></td>
              </tr>
        </table>
		  <table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4">
          <tr class="tblTitulo">
          <td width="5%" align="center">Tipo</td>
          <td width="17%"  align="center">C&oacute;digo</td>
          <td width="19%"  align="center">Imagen</td>
          <td width="18%"  align="center">Color</td>
          <td width="18%"  align="center">Fondo </td>
          <td width="23%"  align="center">Color de la Letra </td>
          </tr>
        <pg:pager
         items="<%= estados.size()%>"
         index="<%= index %>"
         maxPageItems="<%= maxPageItems %>"
        maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
        <%-- keep track of preference --%>
<%
      for (int i = offset.intValue(), l = Math.min(i + maxPageItems, estados.size()); i < l; i++)
	  {
          estado = (TblEstado) estados.elementAt(i);
          String link = CONTROLLER + "?estado=TblEstado&accion=Obtener&cmd=show&tipo=" + estado.getTipo() +
                            "&codestado=" + estado.getCodestado() + "&mensaje=";	
%>
        <pg:item>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand"
        onClick="window.open('<%= link %>','','status=yes,scrollbars=yes,width=650,height=450,resizable=yes');" >
          <td width="5%"  class="bordereporte" ><%= (estado.getTipo().matches("1"))? "Tabla" : "Documento" %></td>
          <td width="17%"  class="bordereporte" ><%= estado.getCodestado() %></td>
          <td width="19%"  class="bordereporte" ><%= estado.getFilename() %></td>
          <td width="18%"  class="bordereporte" ><%= estado.getColor()  %></td>
          <td width="18%"  class="bordereporte" ><%= estado.getFondo() %></td>
          <td width="23%"  class="bordereporte" ><%= estado.getColor_letra() %></td>
        </tr>
        </pg:item>
        <%}%>
        <tr  class="bordereporte">
          <td td height="20" colspan="6" nowrap align="center">
		   <pg:index>
            <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>      
           </pg:index> 
	      </td>
        </tr>
        </pg:pager>
      </table></td>
    </tr>
</table>
<p>
      <%}
 else { %>
</p>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p>&nbsp; </p>
<%}%>
<br>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr align="left">
    <td><img src="<%=BASEURL%>/images/botones/salir.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="parent.close();"> 
  </tr>
</table>
</body>
</html>
