<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="java.util.*" %>
<%@page import="com.tsp.util.Util"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Manejo de Imagen</title>
<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/Validaciones.js"></script>  
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body>
  <center>

  <!-- Parametros de configuracion -->
<% 
        TblEstado cod_estado = (TblEstado) session.getAttribute("cod_estado");
        session.removeAttribute("cod_estado");
   
        String  codestado  = ( cod_estado!=null)? cod_estado.getCodestado() : "";
        String  tipo = ( cod_estado !=null)? cod_estado.getTipo() : "";
        String  color = ( cod_estado !=null)? cod_estado.getColor() : "";
        String  fondo = ( cod_estado !=null)? cod_estado.getFondo() : "";
        String  color_letra = ( cod_estado !=null)? cod_estado.getColor_letra() : "";
        String  descripcion = ( cod_estado !=null)? cod_estado.getDescripcion() : "";
        String fileLength = "1258292";
        String path = ( cod_estado !=null)? cod_estado.getPath() : "";
        System.out.println("----------------> path en la vista: " + path);
        
        
        String msj = ( request.getParameter("comentario")==null )?"":request.getParameter("comentario");
     
        TreeMap c_tipo = new TreeMap();
        c_tipo.put("Tabla","1");
        c_tipo.put("Documento", "2");     
%>
  
  <FORM ACTION='<%=BASEURL%>/uploadEstado.do?maxFileSize=<%=fileLength%>' method='post' id="forma" enctype="MULTIPART/FORM-DATA" NAME='formulario' onSubmit='return validarTCamposLlenos();' >
    <table width="746" border="2" align="center">
      <tr>
        <td><table width="100%" border="0"align="center" class="tablaInferior">
          <tr>
            <td colspan='4'>
              <table cellpadding='0' cellspacing='0' width='100%'>
                <tr class="fila">
                  <td align="left" width='50%' class="subtitulo1">&nbsp;Detalles del C&oacute;digo de Estado </td>
                  <td align="left" width='50%' class="barratitulo"><img src="<%=BASEURL%>//images/titulo.gif"></td>
                </tr>
            </table></td>
          </tr>
          <TR class="fila">
            <TD width="15%" height="24" >Tipo </TD>
            <TD width="23%">
              <input:select name='c_tipo' attributesText="class=listmenu  id='c_tipo' style='width:90%'" default="<%= tipo %>" options="<%= c_tipo %>"/> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
            </TD>
            <TD width="16%">Imagen </TD>
            <TD width="46%"><input  accept="image/jpeg, image/gif, image/x-ms-bmp" name='filename' type='file' class="textbox" id="filename" value="<%= path %>" size="45">
            <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></TD>
          </TR>
          <TR class="fila">
            <TD>C&oacute;digo Estado </TD>
            <TD><input name="c_codestado" type="text" class="textbox" id="c_codestado" value="<%= codestado %>" maxlength="2">
            <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></TD>
            <TD>Color</TD>
            <TD><input name="c_color" type="text" class="textbox" id="c_color"  onKeyPress="soloHexa(event)" value="<%= color %>" maxlength="6">
            <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></TD>
          </TR>
          <TR class="fila">
            <TD>Fondo</TD>
            <TD><input name="c_fondo" type="text" class="textbox" id="c_fondo"  onKeyPress="soloHexa(event)" value="<%= fondo %>" maxlength="6">
            <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></TD>
            <TD>Color de la Letra </TD>
            <TD><input name="c_color_letra" type="text" class="textbox" id="c_color_letra"  onKeyPress="soloHexa(event)"  value="<%= color_letra %>" maxlength="6">
            <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"> </TD>
          </TR>
          <TR class="fila">
            <TD valign="top">Descripci&oacute;n</TD>
            <TD colspan="3"><div align="center">
              <textarea name="c_descripcion" cols="100" rows="5" class="textbox" id="c_descripcion"><%= descripcion %></textarea>
              <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" align="absmiddle"></div></TD>
          </TR>
        </TABLE>        </td>
      </tr>
    </table>
  
    <div align="center"><br>
      <img src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="if ( validarTCamposLlenos() ) forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp; <img src="<%= BASEURL%>/images/botones/cancelar.gif"  name="imgcancelar"  onMouseOver="botonOver(this);" onClick="forma.reset();" onMouseOut="botonOut(this);" >&nbsp; <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >  </div>
  </FORM>
  
  
  <!-- habilitamos el focus -->
  <script>formulario.filename.focus();</script>
  
  
  <!-- Mensaje -->
  <%if(msj!=null && !msj.equals("")){%> 

      <table border="2" align="center">
      <tr>
        <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="320" align="center" class="mensajes"><%=msj%></td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="58">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table>
 <%}%>

 
  
</body>
</html>
