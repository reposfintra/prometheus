<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="java.util.*" %>
<%@page import="com.tsp.util.Util"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<% 
        TblEstado estado = (TblEstado) session.getAttribute("estado");

        String  tipo = ( request.getParameter("tipo")!=null)? request.getParameter("tipo") : "" ;
        String  codestado  = ( request.getParameter("codestado")!=null)? request.getParameter("c_codestado"):"";
        
        String msj        = ( request.getParameter("comentario")==null )?"":request.getParameter("comentario");
        
        TreeMap c_tipo = new TreeMap();
        c_tipo.put("Tabla","1");
        c_tipo.put("Documento", "2");
        
        String anulado = request.getParameter("estado");
        if( anulado == null ) anulado = "";
%>
<html>
<head>
   <title>Manejo de Imagen</title> 
   <script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
   <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
	<script SRC='<%=BASEURL%>/js/boton.js'></script>
	<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<%
        String url = CONTROLLER + "?estado=TblEstado&accion=Anular&cmd=show";
%>   
   <script>
   function anular(){
        forma.action = '<%= url %>';
        forma.submit();
   }
   </script>
   <script>
   function validar(){
        if( forma.c_color.length < 4 ){ 
                alert('La longitud de caracteres debe ser 4.');
                forma.c_color.focus();
                return false;
        }
        else return true;
   }
   </script>
</head>
<body   <%if(request.getParameter("mensaje").equalsIgnoreCase("MsgModificado") ){ %>onLoad="parent.opener.location.reload();"<%}%>>
  <center>

  <!-- Parametros de configuracion -->
<%
        if( !anulado.matches("Anulado")){
%>
  
  <FORM ACTION='<%= CONTROLLER %>?estado=TblEstado&accion=Update&cmd=show' method='post' id="forma" NAME='formulario' onSubmit='return validarTCamposLlenos();' >
    <table width="642" border="2" align="center">
      <tr>
        <td><table cellpadding='0' cellspacing='0' width='100%'>
            <tr class="fila">
              <td align="left" width='50%' class="subtitulo1">&nbsp;Detalles del C&oacute;digo de Estado </td>
              <td align="left" width='50%' class="barratitulo"><img src="<%=BASEURL%>//images/titulo.gif">
                  <input name="c_tipo" type="hidden" id="c_tipo" value="<%= estado.getTipo() %>">
                  <input name="c_codestado" type="hidden" id="c_codestado" value="<%= estado.getCodestado() %>" >
                  <input type="hidden" name="c_filename" id="c_filename"  value="<%= estado.getFilename() %>" ></td>
            </tr>
          </table>
          <table width="100%" border="0"align="center" class="tablaInferior">
            <TR class="fila">
              <TD width="28%">Tipo </TD>
              <TD width="30%"> <input:select name='c_ntipo' attributesText="class=listmenu  id='c_tipo' style='width:90%'" default="<%= estado.getTipo() %>" options="<%= c_tipo %>"/> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></TD>
              <TD width="42%" rowspan="4" valign="top"><div align="center"><img src="<%= BASEURL %>/vista_previa/<%= estado.getFilename() %>" width="126" height="123"></div></TD>
            </TR>
            <TR class="fila">
              <TD valign="top">C&oacute;digo Estado </TD>
              <TD valign="top"><input name="c_ncodestado" type="text" id="c_ncodestado2" maxlength="2" value="<%= estado.getCodestado() %>">
              <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></TD>
            </TR>
            <TR class="fila">
              <TD valign="top">Color</TD>
              <TD valign="top"><input name="c_color" type="text" id="c_color2" maxlength="6" value="<%= estado.getColor() %>" onKeyPress="soloHexa(event)" >
              <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></TD>
            </TR>
            <TR class="fila">
              <TD valign="top">Fondo</TD>
              <TD valign="top"><input name="c_fondo" type="text" id="c_fondo2" maxlength="6" value="<%= estado.getFondo() %>"  onKeyPress="soloHexa(event)" >
              <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></TD>
            </TR>
            <TR class="fila">
              <TD valign="top">Color de la Letra </TD>
              <TD valign="top"><input name="c_color_letra" type="text" id="c_color_letra2" maxlength="6" value="<%= estado.getColor_letra() %>"  onKeyPress="soloHexa(event)" >
              <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></TD>
              <%            
                      String link = CONTROLLER + "?estado=TblEstado&accion=UpdateImagen&cmd=show&tipo=" + estado.getTipo() +
                            "&codestado=" + estado.getCodestado(); 
%>
              <TD><div align="center" class="letraresaltada" style="cursor:hand" 
                        onClick="window.open('<%= link %>','','status=yes,scrollbars=yes,width=800,height=250,resizable=yes');">Cambiar Imagen </div></TD>
            </TR>
            <TR class="fila">
              <TD valign="top">Descripci&oacute;n</TD>
              <TD colspan="2"><div align="center">
                  <textarea name="c_descripcion" cols="50" rows="5" id="textarea"><%= estado.getDescripcion()%></textarea>
                  <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10" align="absmiddle"> </div></TD>
            </TR>
          </TABLE>        </td>
      </tr>
    </table>
  
    <div align="center"><br>
      <img src="<%=BASEURL%>/images/botones/modificar.gif" name="c_modificar" onClick="if( validar() ) forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> <img src="<%=BASEURL%>/images/botones/anular.gif" name="c_anular" onClick="anular();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_buscar" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">  </div>
  </FORM>
 
<%
        }
        else{
%>
<table border="2" align="center">
  <tr>
    <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="282" align="center" class="mensajes"><%= msj %></td>
          <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="78">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<br>
<table width="415" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr align="left">
    <td><img src="<%=BASEURL%>/images/botones/salir.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="parent.close();"> 
  </tr>
</table>
<%
        }
%>
  
  
</body>
</html>
