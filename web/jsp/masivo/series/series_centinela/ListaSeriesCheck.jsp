<!--
- Autor : Ing. Armando Oviedo C
- Date  : 10 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra el listados de series
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script> 
<title>Listado de series</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<%
    
    
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 30;
    int maxIndexPages = 50; 
%>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/toptsp.jsp?encabezado=Verificación De Series"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<table width="100%" align="center" BORDER='2'>
    <tr>
        <td>
            <table width='100%' class="tablaInferior">
                <tr>
                    <td colspan='11'>
                        <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="barratitulo">
                            <tr>
                                <td width="50%" class="subtitulo1" colspan='5' align=center>Lista De Series</td>
                                <td width="50%" colspan='5'><img src="<%=BASEURL%>/images/titulo.gif"></td>
                            </tr> 
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan='10'>
                        <table  width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
                            <tr class="tblTitulo">
                                <td align=center>Distrito</td>
                                <td align=center>Agencia</td>
                                <td align=center>Tipo Documento</td>
                                <td align=center>Banco</td>
                                <td align=center>Sucursal</td>
                                <td align=center>Serie Inicial</td>
                                <td align=center>Serie Final</td>
                                <td align=center>Ultimo Usado</td>
                                <td align=center>Restantes</td>                                
                                <td align=center>Ver Todas Las Series</td>
                            </tr>                                             
                                <pg:pager
                                items="<%=model.seriesService.getListaGrupos().size()%>"
                                index="<%= index %>"
                                maxPageItems="<%= maxPageItems %>"
                                maxIndexPages="<%= maxIndexPages %>"
                                isOffset="<%= true %>"
                                export="offset,currentPageNumber=pageNumber"
                                scope="request">
                                <%-- keep track of preference --%>
                                  <%
                                    Vector listaGrupos = model.seriesService.getListaGrupos();    
                                    if(listaGrupos!=null){
                                        for(int i = offset.intValue(),l = Math.min(i + maxPageItems, model.seriesService.getListaGrupos().size());i < l; i++){            
                                            Series tmp = new Series();%>
                                <pg:item>
                                <%
                                            tmp = (Series)(listaGrupos.elementAt(i));
                                            SerieGroupValidation seriegrupo = (SerieGroupValidation)(tmp.getListaSeries().elementAt(0));                                            
                                            String estado = model.seriesService.getActualState(tmp);
                                            if(estado.equalsIgnoreCase("amarillo")){%>
                                                <tr style="cursor:hand" class='filaamarilla' onclick="window.open('<%=CONTROLLER%>?estado=SeriesV&accion=Detalle&posicion=<%=i%>' ,'','status=yes,scrollbars=no,width=700,height=400,resizable=yes')">
                                            <%}
                                            else if(estado.equalsIgnoreCase("rojo")){%>
                                                <tr style="cursor:hand" class='filaroja' onclick="window.open('<%=CONTROLLER%>?estado=SeriesV&accion=Detalle&posicion=<%=i%>' ,'','status=yes,scrollbars=no,width=700,height=400,resizable=yes')">
                                            <%}
                                            else{%>
                                                <tr style="cursor:hand" class='filaverde' onclick="window.open('<%=CONTROLLER%>?estado=SeriesV&accion=Detalle&posicion=<%=i%>' ,'','status=yes,scrollbars=no,width=700,height=400,resizable=yes')">
                                            <%}
                                %>                                          
                                <td class='bordereporte' align=center nowrap><%=tmp.getDistrito()%></td>
                                <td class='bordereporte' align=center nowrap><%=tmp.getAgency_id()%></td>
                                <td class='bordereporte' align=center nowrap><%=tmp.getNombre_Documento()%></td>
                                <td class='bordereporte' align=center nowrap><%=tmp.getBranch_code()%></td>
                                <td class='bordereporte' align=center nowrap><%=tmp.getBank_account_no()%></td>
                                <td class='bordereporte' align=center nowrap><%=seriegrupo.getInitialSerialNo()%></td>
                                <td class='bordereporte' align=center nowrap><%=seriegrupo.getFinalSerialNo()%></td>
                                <td class='bordereporte' align=center nowrap><%=seriegrupo.getLastNumber()%></td>
                                <td class='bordereporte' align=center nowrap><%=seriegrupo.getRestantes()%></td>                                
                                <td class='bordereporte' align=center nowrap><img title='Ver todas las series de este grupo...' src="<%= BASEURL %>/images/botones/iconos/detalles.gif"></img></td>
                            </tr>
                            </pg:item>
                                        <%}
                                        }%>                          
   <tr class="pie">
    <td height="30" colspan="10" nowrap><pg:index><jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/></pg:index></td>
  </tr>
  </pg:pager>
                        </table>
                    </td>
                </tr>
        </table>
        </td>
        </tr>
</table>
</div>
</body>
</html>
