<!--
- Autor : Ing. Armando Oviedo C
- Date  : 10 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra todos las series del grupo
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script> 
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/toptsp.jsp?encabezado=Detalle Grupo De Series"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<table width="100%" border="2" align="center">
	<tr>
		<td>
			<table width='100%' class="tablaInferior">
				<tr>
					<td colspan="5">
						<table width="100%"  border="0" cellpadding="0" cellspacing="0" class="barratitulo">
                                                    <tr>
                                                        <td width="50%" class="subtitulo1" colspan='3' align=center>Detalle De Grupo</td>
                                                        <td width="50%" colspan='2'><img src="<%=BASEURL%>/images/titulo.gif"></td>
                                                    </tr> 
                                                </table>
					</td>
				</tr>								
	  			<tr>
					<td colspan="5">
						<table  width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">
							<tr class="tblTitulo">
								<td align=center nowrap>Distrito</td>
								<td align=center nowrap>Agencia</td>
								<td align=center nowrap>Tipo Documento</td>
								<td align=center nowrap>Banco</td>
								<td align=center nowrap>Nombre Cuenta</td>								
							</tr>
							<%
                                                                Vector listaGrupos = model.seriesService.getListaGrupos();
                                                                int pos = Integer.parseInt(request.getParameter("posicion"));
								Series tmp = (Series)(listaGrupos.elementAt(pos));
                                                                String estado = model.seriesService.getActualState(tmp);
                                            if(estado.equalsIgnoreCase("amarillo")){%>
                                                <tr style="cursor:hand" class='filaamarilla'>
                                            <%}
                                            else if(estado.equalsIgnoreCase("rojo")){%>
                                                <tr style="cursor:hand" class='filaroja'>
                                            <%}
                                            else{%>
                                                <tr style="cursor:hand" class='filaverde' >
                                            <%}
                                                        %>								                                                        
                                                           <td class='bordereporte'  align=center nowrap><%= tmp.getDistrito() %></td>
                                                           <td class='bordereporte'  align=center nowrap><%= tmp.getAgency_id() %></td>
                                                           <td class='bordereporte'  align=center nowrap><%= tmp.getNombre_Documento() %></td>
                                                           <td class='bordereporte'  align=center nowrap><%= tmp.getBranch_code() %></td>
                                                           <td class='bordereporte'  align=center nowrap><%= tmp.getBank_account_no() %></td>                                                             
                                                        </tr>
                                                </table>
                                                <br>
                                                <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">                        
                                                    <tr class="tblTitulo">
                                                        <td  align=center nowrap>Inicio De Serie</td>
                                                        <td  align=center nowrap>Fin De Serie</td>
                                                        <td  align=center nowrap>N�mero Actual</td>
                                                        <td  align=center nowrap>Restantes</td>
                                                    </tr>
								<%
                                                                    for(int i=0;i<tmp.getListaSeries().size();i++){
                                                                        SerieGroupValidation seriegrupo = (SerieGroupValidation)(tmp.getListaSeries().elementAt(i));%>                                                                    
							<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand">
								<td class='bordereporte'  align=center nowrap><%=seriegrupo.getInitialSerialNo()%></td>
								<td class='bordereporte'  align=center nowrap><%=seriegrupo.getFinalSerialNo()%></td>
								<td class='bordereporte' align=center nowrap><%=seriegrupo.getLastNumber()%></td>
								<td class='bordereporte' align=center nowrap><%=seriegrupo.getRestantes()%></td>
							</tr>
							<%}
                                                                %>
						</table>
					</td>
  				</tr>  				  
                        </table>
		</td>
		</tr>
</table>
<br>
<table align=center>
    <tr align=center>
        <td align="right"><img title='Regresar' src="<%= BASEURL %>/images/botones/regresar.gif" onmouseover="botonOver(this);" onmouseout="botonOut(this);" onClick="parent.close()"></img></td>
    </tr>
</div>
</body>
</html>
