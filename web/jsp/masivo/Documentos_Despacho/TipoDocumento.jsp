<!--
- Autor : Ing. Juan Manuel Escandon Perez
- Date  : Julio de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--%>

<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<% String Mensaje       = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
%>
<html>
<head>
    <title>Tipo de Documentos</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script src='<%= BASEURL %>/js/validar.js'></script>
	<script src="<%= BASEURL %>/js/boton.js"></script>
	<script>
		function Validar(){				                                                                   
                            //form1.Opcion.value='Guardar';  
                            if( form1.tipo.value == "" ){
                                    alert( 'Defina un tipo de documento para continuar...' );
                                    return false;
                            }
                            if( form1.numrem.value == "" ){
                                    alert( 'Defina un numero de documento para continuar...' );
                                    return false;
                            }	                        
                            return true;			
		}	
	</script>
</head>
<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Documentos de Despacho"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

    <form name="form1" method="post" action="<%= CONTROLLER %>?estado=ValidarDocumentos&accion=Despacho" onsubmit='return Validar();'>
	<br>
        <table width="550" border="2" align="center" >
			<tr>
				<td>
					<table width="100%" height="34" border="0" align="center" class="tablaInferior">
					<tr><td>
            			<table width="100%">
							<tr>
								<td height="24" width="50%" class="subtitulo1"><p align="left">&nbsp;Tipo de Documentos </p></td>
								<td  class="barratitulo" width="50%"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"></td>
								<input name="Opcion" type="hidden" id="Opcion">
								<input name="destinatarios" type="hidden" id="destinatarios">
							</tr>  
						</table> 
						<table width="100%">
						<tr class="fila">
						    <td align="left" width="40%" class="letra_resaltada">&nbsp;&nbsp;Documentos Internos </td>
						    <td align="center" width="10%">&nbsp;<input name="tipo" id="tipo" type="radio" value="0" checked></td>
							<td align="left" width="40%" class="letra_resaltada">&nbsp;&nbsp;Documentos Relacionados </td>
						    <td align="center" width="10%">&nbsp;<input name="tipo" id="tipo" type="radio" value="1"></td>
						</tr> 
						<tr class="fila">
							<td align="center" class="letra_resaltada" colspan="4">&nbsp;
								<input name="c_tdoc" type="radio" value="001" checked>
								Planilla &nbsp;
								  <input type="radio" name="c_tdoc" value="002">
							Remesa&nbsp;&nbsp;<input name="numrem" type="text" class="textbox" id="numrem" size="12" maxlength="12">							        </td>
						</tr>						  
						</table> 
						</td>
						</tr>
					</table> 
			  </td>        
		  </tr>        	
		</table>
		<br>
        <table align="center">
          <tr align="center">
            <td><input type="image" src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">  <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
          </tr>
        </table>
        <p>
          <%if(!Mensaje.equals("")){%>
</p>
        <table border="2" align="center">
          <tr>
            <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                <tr>
                  <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
                  <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                  <td width="58">&nbsp;</td>
                </tr>
            </table></td>
          </tr>
        </table>
        <%}%>
    </form>
	</div>
</body>
</html>
