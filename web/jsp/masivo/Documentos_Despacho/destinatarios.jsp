<%@ page session="true"%>

<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>Agregar Destinatarios</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script language="javascript" src="<%=BASEURL%>/js/validar.js">
</script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script> 
</script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">


<% String estadopag = (request.getParameter("estado")!=null)?request.getParameter("estado"):""; 
   String pag       = (request.getParameter("pag")!=null)?request.getParameter("pag"):"";
%>

</head>
<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Destinatarios"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<form action="<%=CONTROLLER%>?estado=Buscar&accion=Destinatarios" method="post" name="form2" id="form2">
  <div align="center">  <br>
  </div>
  
<% if( model.remidestService.getVectorRemiDest().size() > 0 ) {//Inicio condicional  %>
  <table width="99%"  border="2" align="center">
	<tr>
	  <td>
  <table width="100%" class="tablaInferior">
    <tr>
      <td height="22" colspan=2 class="subtitulo1">Lista de Destinatarios<span class="Letras">
        <input name="numrem" type="hidden" id="numrem" value="<%=request.getParameter("numrem")%>">
        <input name="modif" type="hidden" id="modif" value="<%=request.getParameter("modif")!=null?"1":""%>">
      </span></td>
      <td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
    </tr>
  </table>
  <table width="100%" border="1" bordercolor="#999999">
	<tr class="tblTitulo">
	<td colspan="2"  bordercolor="#999999">Destinatario</td>
	<td  bordercolor="#999999">Documentos</td>
	</tr>
	
	
 <%
if(model.remidestService.getVectorRemiDest()!=null){%>
	<%String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 5;
    int maxIndexPages = 10;
	String numrem = request.getParameter("numrem");
	String tipo ="002";
	String estado="";	
		
	%>

	<pg:pager
    items="<%= model.remidestService.getVectorRemiDest().size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
	<%	for (int i = offset.intValue(),
	         l = Math.min(i + maxPageItems, model.remidestService.getVectorRemiDest().size());
	     i < l; i++){
		 //for(int i=0; i<model.remidestService.getVectorRemiDest().size();i++){
  		
            RemiDest rd = (RemiDest) model.remidestService.getVectorRemiDest().elementAt(i);
            String coddest = rd.getCodigo();
            String nomdest = rd.getNombre();
			String direccion  = rd.getDireccion();
    	%> 
	<pg:item>
	<tr class="<%=i%2==0?"filagris":"filaazul"%>">
      <td class="bordereporte"><strong><%=nomdest%></strong><br>
     <%=coddest%></td>
      <td width="26%" class="bordereporte"><%=direccion%></td>
      <td width="39%" class="bordereporte"><input name="docui<%=coddest%>" type="hidden" id="docuint<%=coddest%>" size="50">	  	
      	<a class="Simulacion_Hiper" onClick=<%if(!estadopag.equals("anular")){%>"window.open('<%=CONTROLLER%>?estado=remesa_docto&accion=Manager&Opcion=agregar&destinatario=<%=coddest%>&nombre=<%=nomdest%>&numrem=<%=numrem%>','')"<%}else{%>"window.open('<%=CONTROLLER%>?estado=remesa_docto&accion=Manager&Opcion=anular&destinatario=<%=coddest%>&nombre=<%=nomdest%>&numrem=<%=numrem%>','')"<%}%>style="cursor:hand ">MODIFICAR  DOCUMENTOS RELACIONADOS</a> 			
	  </td>
	</tr>
	</pg:item>
	 <%
	 }%>


	<tr class="filaresaltada">
	  <td colspan="4"><div align="center">
	   <pg:index>
	      <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
    	</pg:index>
	  </div>
	  
	  </td>
    </tr>
	</pg:pager>	 
<%}
}//Fin de if de Destinatarios
else{
 String mensaje = "No hay destinatarios asociados a esta remesa";%>
 <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=mensaje%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
<%}//Fin Else%>

	
  </table>
  </td>
  </tr></table>
  <br>
  <div align="center">
  	<img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">
	<img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgregresar" onclick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/masivo/Documentos_Despacho&pagina=<%=pag%>&titulo=Tipo de Documentos&marco=no'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
  </div>
</form>
</div>
</body>
</html>
