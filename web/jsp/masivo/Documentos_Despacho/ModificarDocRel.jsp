<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<% 
String destinatario = request.getParameter("destinatario");
String nomdest = request.getParameter("nombre");
String numrem = request.getParameter("numrem");
model.RemDocSvc.LISTTLBDOC();
List LTipoDoc = model.RemDocSvc.getList();
String lista = model.RemDocSvc.LISTCOMBO();

String Mensaje       = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
%>
<html>
<head>
    <title>Modificar documentos</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script src='<%=BASEURL%>/js/validarDOM.js'></script>
    <script src='<%=BASEURL%>/js/validarDocumentos.js'></script>
    </script>
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<%
	Vector vec = model.RemDocSvc.getVec();
	int i = 0;
%>

<body <%if(request.getParameter("reload")!=null){%>onLoad="window.opener.opener.location.reload();redimensionar();"  <%}%>onResize="redimensionar();">
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modificar documentos"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">  

        <form method="post" name="formulario" id="formulario" action="<%=CONTROLLER%>?estado=DocumentosDespacho&accion=Manager">
        <table width="100%"  border="2" cellspacing="0" cellpadding="0" align="center">
            <tr>
            <td><table width="100%" border="0" cellpadding="3" cellspacing="2"  class="tablaInferior" align="center" id="tablefather">
            <tr>
                <td width="190" height="24"  class="subtitulo1" colspan="2">DOCUMENTOS DE: <%=nomdest%></td>
                <td width="404"  class="barratitulo" colspan="2"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                <input type="hidden" name="documentosRem" id="documentosRem">
                <input type="hidden" name="numrem" value="<%=numrem%>">
                <input name="destinatario" type="hidden" id="destinatario" value="<%=request.getParameter("destinatario")%>">
                <input name="nombre" type="hidden" id="nombre" value="<%=request.getParameter("nombre")%>">
            </tr>   

            <tr align="center" class="fila">
                <td  width="25%">TIPO DE DOCUMENTO</td>
                <td  width="25%">DOCUMENTO</td>
                <td  width="25%">TIPO DE DOCUMENTO RELACIONADO </td>
                <td  width="25%">DOCUMENTO RELACIONADO</td>
            </tr>
        </table>
           
<%String documento="";
  String tipo_doc ="";
%>
<div id="panel" style="width:auto ">
                <table width="100%"  border="0" align="center" cellpadding="0" cellspacing="0" class="tablaInferior" id="tabla1">
<%for(i =0; i<vec.size();i++){	 	
		Vector hijos = new Vector();
		remesa_docto rem = (remesa_docto) vec.elementAt(i);
		if(rem.getDestinatario().equals(destinatario)){
			documento = rem.getDocumento();
			tipo_doc = rem.getTipo_doc();
			hijos =  rem.getHijos();
		}   %>                 
<tr class="fila" align="center">
                        <td width="25%"><select name="tp<%=i%>" id="tp<%=i%>" class="textbox" onKeyPress="soloAlfa(event);">
<% Iterator it = LTipoDoc.iterator();
   while (it.hasNext()){ 
   remesa_docto rd = (remesa_docto) it.next();
   %>
                            <option value="<%=rd.getDocument_type()%>" <%=rd.getDocument_type().equals(tipo_doc)?"selected":""%>><%=rd.getDocument_name()%></option>
	<% } %>
                            </select>
	
                        </td>
                        <td width="25%"><input type="text" name="dp<%=i%>" id="dp<%=i%>" class="textbox" value="<%=documento%>" onKeyPress="soloAlfa(event)">
                        &nbsp;
                        <input type="button" name="enviar3" value="+" class="boton" onClick="agregarFila('<%=lista%>','<%=i%>')"></td>
                        <td><table width="100%" border="0" cellpadding="0" cellspacing="0" id="tablah<%=i%>">
  <%for(int j = 0; j<hijos.size(); j++){
  		remesa_docto remH = (remesa_docto) hijos.elementAt(j);
  %>
                            <tr>
                                <td width="50%" height="21"><div align="center">
                                    <select name="tp<%=i%>_TipodocRel<%=i%><%=j%>" class="textbox" id="select">
          <% it = LTipoDoc.iterator();
   while (it.hasNext()){ 
   remesa_docto rd = (remesa_docto) it.next();
   %>
                                    <option value="<%=rd.getDocument_type()%>" <%=rd.getDocument_type().equals(remH.getTipo_doc())?"selected":""%>> <%=rd.getDocument_name()%></option>
          <% } %>
                                    </select>
                                </div></td>
                                <td width="50%"><div align="center">
                                    <input type="text" name="tp<%=i%>_DocumentoRel<%=i%><%=j%>" id="tp<%=i%>_DocumentoRel<%=i%><%=j%>" class="textbox" value="<%=remH.getDocumento()%>" onKeyPress="soloAlfa(event)">
			
                                </div>
                                </td>
                            </tr>
  <%}%>
  
  
                        </table>    </td>

                    </tr>
                    <%}%>
                </table>
</div>
        </td>
        </tr>
        </table>
        <br>
        <table width="100%" border="0" id="ultima">
            <tr align="center">
                <td colspan="4"> <img src="<%=BASEURL%>/images/botones/agregar.gif"  name="imgagregar" onClick="AgregarFilaDOCREL('<%=lista%>','<%=i%>');" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="formulario.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> </td>
            </tr>
        </table>
        <p>&nbsp;</p>
		
<%if(!Mensaje.equals("")){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%}%>		
    </form>
    </div>
</body>
</html>
