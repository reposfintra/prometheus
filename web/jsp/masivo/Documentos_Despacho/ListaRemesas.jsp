<!--
- Autor : Ing. Rodrigo Salazar
- Date  : 10 de Noviembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja las listas de las remesas de cumplidos .
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%
        String tipo    		=   (request.getParameter("tipo")!=null)?request.getParameter("tipo"):"";
        String tipodoc 		= 	request.getParameter ("c_tdoc");
        String Opcion  		= 	(request.getParameter("Opcion")!=null)?request.getParameter("Opcion"):"";
		String numrem       =   (request.getParameter("numrem")!=null)?request.getParameter("numrem"):"";
%>
<html>
    <head>
        <title>Listado Remesa</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
    </head>
    <%-- Inicio Body --%>
    <body> 
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listado Remesas"/>
        </div>

        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">    
            <%
			Vector vec = model.planillaService.ReporteVector();
			Planilla p;
			if ( vec.size() >0 ){ %>
                <%-- Inicio Tabla Principal --%>
				<table width="600" border="2" align="center">
                    <tr>
                        <td>
                        <%-- Inicio Tabla Cabecera --%>
                        <table width="100%" align="center">
                            <tr>
                                <td height="50%" colspan=2 class="subtitulo1"><p align="left">Remesas asociadas a la planilla <%=numrem%></p></td>
                                <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                            </tr>
                        </table>
                        <%-- Inicio Tabla Secundaria --%>
                        <table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
							<tr class="tblTitulo" align="center">
								<td width="16%" align="center">Remesa</td>
								<td width="27%" align="center">Cliente</td>
								<td width="19%" align="center">Fecha</td>
								<td width="19%" align="center">Origen Remesa </td>
								<td width="19%" align="center">Destino Remesa </td>
							</tr>
							<% for(int i = 0; i<vec.size(); i++){	
								p = (Planilla) vec.elementAt(i);%>
								<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand" onClick="window.location='<%=CONTROLLER%>?estado=remesa_docto&accion=Manager&numrem=<%=p.getNumrem()%>&tipo=<%=tipo%>&Opcion=<%=Opcion%>&destinatarios='">
									<td width="16%" class="bordereporte" align="center"><%=p.getNumrem()%></td>
									<td width="27%" class="bordereporte" align="center"><%=p.getNitpro()%></td>
									<td width="19%" class="bordereporte" align="center"><%=p.getFecdsp()%></td>
									<td width="19%" class="bordereporte" align="center"><%=p.getNomori()%></td>
									<td width="19%" class="bordereporte" align="center"><%=p.getNomdest()%></td>
								</tr>
							<%}%>
							<tr  class="pie">
								<td td height="20" colspan="12" nowrap align="center">
								</td>
							</tr>
						</table>
                        </td>
                    </tr>
                </table>
                <br>
            <%}else{%>
                <table border="2" align="center">
                    <tr>
                        <td>
                            <table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                                <tr>
                                    <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
                                    <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                    <td width="78">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table> 
                <br>
            <%}%>
            <table width="600" border="0" align="center">
                <tr>
                    <td>
                        <img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Volver" name="buscar"  onClick="window.location='<%=BASEURL%>/jsp/masivo/Documentos_Despacho/TipoDocumento.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
                    </td>
                </tr>
            </table>   
        </div>
    </body>
</html>
