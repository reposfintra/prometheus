<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@ page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<% 
String numrem       =   (request.getParameter("numrem")!=null)?request.getParameter("numrem"):"";
List LTipoDoc = model.RemDocSvc.getList();
Vector documentos = model.RemDocSvc.getDocumentos();
%>
<html>
<head>
<title>Modificar documentos</title>

<script src='<%=BASEURL%>/js/validarDOM.js'></script>
<script src='<%= BASEURL %>/js/validar.js'></script>
<script src="<%= BASEURL %>/js/boton.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
</script>
</head>
<body onResize="redimensionar()" onLoad="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Documentos de Despacho"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<form method="post" name="formulario" id="formulario" action="<%=CONTROLLER%>?estado=remesa_docto&accion=Manager">
<br>
<table width="700"  border="2" align="center">
  <tr>
    <td><table width="100%"align="center">
      <tr class="titulo">
		<td height="24" colspan="2"  class="subtitulo1"><p align="left">&nbsp;Remesa <%=numrem%>
            <input type="hidden" name="numrem" value="<%=numrem%>">
            <input type="hidden" name="items" value="<%=documentos.size()%>">
            <input type="hidden" name="Opcion" value="ModificarDocumento">
</p></td>
        <td width="66%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              <input type="hidden" name="documentosRem" id="documentosRem">
			  <input type="hidden" name="numrem1" id="numrem1">
              <input name="Reload" type="hidden" id="Reload">
      </tr>
      <tr align="center" class="fila">
        <td colspan="2" class="letra_resaltada">TIPO</td>
        <td width="66%" class="letra_resaltada">DOCUMENTO</td>
      </tr>
	  <%for(int i =0; i < documentos.size();i++){
	        remesa_docto doc = (remesa_docto) documentos.get(i);
	     %>
	  
      <tr class="fila" >
        <td width="4%" valign="top"><input type="checkbox" name="pos<%=i%>" value="ok"></td>
        <td width="30%" valign="top"><select name="tipodoc<%=i%>" id="select" class="textbox" style="width:95%" onChange="campoformulario(<%=i%>);" >
          <% Iterator it = LTipoDoc.iterator();
			   while (it.hasNext()){ 
			   remesa_docto rd = (remesa_docto) it.next();
			   %>
          <option value="<%=rd.getImportacion()%>-<%=rd.getExportacion()%>-<%=rd.getDocument_type()%>" <%=(doc.getTipo_doc().equals( rd.getDocument_type() ) )?"selected":""%> > <%=rd.getDocument_name()%></option>
          <% } %>
        </select>
          <input type="hidden" name="tdoc<%=i%>" value="<%=doc.getTipo_doc()%>">
          <input type="hidden" name="doc<%=i%>" value="<%=doc.getDocumento()%>"></td>
        <td>
		<table width="100%" border="0">
          <tr class="fila" >
            <td>C&oacute;digo              </td>
            <td colspan="2"><input name="documento<%=i%>" type="text" class="textbox" id="documento<%=i%>" size="50" onKeyPress="soloAlfa(event);" value="<%=doc.getDocumento()%>" ></td>
            </tr>
 		<tr valign="top" class="fila" id="ffecha<%=i%>" style="display:<%=doc.getImportacion().equals("S")?"block":"none"%>" >
   <td>
       Fecha SIA</td>
   <td width="33%">
     <input name="fecha_sia<%=i%>" type="text" class="textbox" id="fecha_sia<%=i%>" size="17" readonly value="<%=(doc.getFecha_sia().equals("0099-01-01 00:00:00"))?"":doc.getFecha_sia().substring(0,16)%>">
     <img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onClick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fecha_sia<%=i%>);return false;" hidefocus></td>
   <td width="49%"><div align="center">
     Fecha ETA
       <input name="fecha_eta<%=i%>" type="text" class="textbox" id="fecha_eta<%=i%>" size="17" readonly value="<%=(doc.getFecha_eta().equals("0099-01-01 00:00:00"))?"":doc.getFecha_eta().substring(0,16)%>">
       <a href="javascript:void(0)" onClick="jscript: show_calendar('fecha_eta<%=i%>');" hidefocus> </a> <img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onClick="if(self.gfPop)gfPop.fPopCalendar(document.formulario.fecha_eta<%=i%>);return false;" hidefocus> </div></td>
   </tr>
 <tr valign="top" class="fila" id="fcon<%=i%>" style="display:<%=doc.getImportacion().equals("N") || doc.getImportacion().equals("S")?"block":"none"%>">
   <td width="18%" valign="middle">Descripci&oacute;n <br>     </td>
   <td colspan="2"><textarea name="descripcion<%=i%>" rows="2" class="textbox" id="descripcion<%=i%>" style="width:87%" onKeyPress="return soloNumText(event)"><%=doc.getDescripcion_cga()%></textarea></td>
   </tr>
        </table>
		</td>
      </tr>
      <%}%>
      
    </table></td>
  </tr>
</table>

<br>
<table width="80%" align="center">
	<tr align="center">
			<td colspan="2" >
				<img src="<%=BASEURL%>/images/botones/modificar.gif"  name="imgaceptar" onClick="validarDocumentos();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
				<img src="<%=BASEURL%>/images/botones/eliminar.gif"  name="imgaceptar" onClick="Borrar();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
				<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
				<img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Volver" name="buscar"  onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/masivo/Documentos_Despacho&pagina=BuscarDocumento.jsp&marco=no'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"></td>
	</tr>
</table>
<%
  String Mensaje       = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
  if(!Mensaje.equals("")){
%>
<table border="2" align="center">
  <tr>
    <td><table width="410" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="282" align="center" class="mensajes"><%=Mensaje%></td>
          <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
          <td width="78">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<br>
<%}%>
</form>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins_24.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
</body>
</html>