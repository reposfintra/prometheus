<!--
- Autor : Ing. Juan Manuel Escandon Perez
- Date  : Julio de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra los datos de entrada para 
--              la inicializacion del Reporte de Remesas sin documentos
--%>

<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<% String Mensaje       = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
   String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
   String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>

    <title>Registro de Tarjeta</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script src='<%= BASEURL %>/js/validar.js'></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Registro Tarjeta"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

    <form name="form1" method="post" action="<%= CONTROLLER %>?estado=RegistroTarjeta&accion=Manager">
        <table width="40%" border="2" align="center">
			<tr>
				<td>
					<table width="99%" height="34" border="0" align="center" class="tablaInferior">
            			<tr>
							<td width="50%" height="24"  class="subtitulo1"><p align="left">&nbsp;Buscar Registro</p></td>
            				<td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" align="left"><%=datos[0]%></td>
							<input name="Opcion" type="hidden" id="Opcion">
        				</tr>     												
						<tr class="fila">
							<td align="left"class="letra_resaltada">&nbsp;&nbsp;Numero Tarjeta </td>
							<td align="left">&nbsp;<input type="text" id="numtarjeta" name="numtarjeta" class="textbox" maxlength="30"></td>
						</tr>
						<tr class="fila">
                			<td class="letra_resaltada" align="left">&nbsp;&nbsp;Cedula Conductor </td>
               			    <td align="left">&nbsp;<input type="text" id="cedula" name="cedula" class="textbox" maxlength="15"></td>	  
            			</tr>
						<tr class="fila">
							<td class="letra_resaltada" align="left">&nbsp;&nbsp;Placa</td>
							<td align="left">&nbsp;<input type="text" id="placa" name="placa" class="textbox" maxlength="15"></td>            
					  	</tr>
						<tr class="fila">
							<td class="letra_resaltada" align="left">&nbsp;&nbsp;Fecha Creacion</td>
							<td align="left">&nbsp;<input type="text" id="fechacreacion" name="fechacreacion" class="textbox" maxlength="15">&nbsp;<img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fechacreacion);return false;" HIDEFOCUS></td>            
					  	</tr>   
					</table> 
			  </td>        
		  </tr>        	
		</table>
		<br>
        <table align="center">
          <tr align="center">
            <td> <img src="<%=BASEURL%>/images/botones/buscar.gif"  name="imgbuscar" onClick="form1.Opcion.value='ListadoReg';form1.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> <img src="<%=BASEURL%>/images/botones/detalles.gif"  name="imgdetalles" onClick="form1.Opcion.value='Listado';form1.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
          </tr>
        </table>
        
        <p>
          <%if(!Mensaje.equals("")){%>
</p>
        <table border="2" align="center">
          <tr>
            <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                <tr>
                  <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
                  <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                  <td width="58">&nbsp;</td>
                </tr>
            </table></td>
          </tr>
        </table>
        <%}%>
    </form>
	</div>
	<%=datos[1]%>
	<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins_24.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
</body>
</html>
