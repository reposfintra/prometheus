<!--
- Autor : Ing. Jose de la rosa
- Date  : 9 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que modifica un producto
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<html>
<head>
<title>Modificar Registro Tarjeta</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
</head>
<body <%if(request.getParameter("reload")!=null){%>onLoad="parent.opener.location.reload();redimensionar();"<%}%> onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Modificar Registro Tarjeta"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%    
    RegistroTarjeta dat 	= (RegistroTarjeta) model.registroTarjetaSvc.getDato();    
	dat 					= (dat!=null)?dat : new RegistroTarjeta(); 
    String Mensaje       	= (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<script>
	function Validar(){
		if( form1.nnumtarjeta.value == '' ){
			alert('Debe escribir un Numero de tarjeta');
			form1.numtarjeta.focus;
			return false;
		}
		if( form1.ncedula.value == '' ){
			alert('Debe escribir la Cedula del conductor');
			form1.cedula.focus;
			return false;
		}
		if( form1.nplaca.value == '' ){
			alert('Debe escribir la Placa del vehiculo');
			form1.placa.focus;
			return false;
		}
		if( form1.nfechacreacion.value == '' ){
			alert('Debe escribir la Fecha de creacion');
			form1.nfechacreacion.focus;
			return false;
		}
			return true;
	}	
	
	function ValidarNumTarjeta(){	
		if( form1.nnumtarjeta.value == '' ){
			alert('Debe escribir un Numero de tarjeta');
			form1.numtarjeta.focus;
			return false;
		}
			return true;
	}
</script>	
<FORM name='form1' id='form1' method='POST' action='<%=CONTROLLER%>?estado=RegistroTarjeta&accion=Manager'>
	<table width="500" border="2" align="center">
	<input name="Opcion" type="hidden" id="Opcion">
      <tr>
        <td>
    	<table width="100%" class="tablaInferior">
		<tr class="fila">
		  <td align="left" class="subtitulo1">&nbsp;Modificar Registro Tarjeta </td>
		  <td align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
		</tr>	
        <tr class="fila">
            <td width="158">Numero Tarjeta </td>
            <td width="238"><input name="nnumtarjeta" type="text" class="textbox" id="nnumtarjeta" value="<%=dat.getNum_tarjeta()%>"  maxlength="30" onKeyPress="soloAlfa(event)">
              				<input name="numtarjeta" type="hidden"  id="numtarjeta" value="<%=dat.getNum_tarjeta()%>"></td>
        	</tr>
                <tr class="fila">
                <td colspan="1">Placa</td>
                <td colspan="3"><input name="nplaca" type="text" class="textbox" id="nplaca" value="<%=dat.getPlaca()%>"  maxlength="15" onKeyPress="soloAlfa(event)">
								<input name="placa" type="hidden"  id="placa" value="<%=dat.getPlaca()%>">																																				 
				</td>
        </tr>				
        <tr class="fila">
            <td>Cedula Conductor</td>
            <td><input name="ncedula" type="text" class="ncedula" id="conductor" value="<%=dat.getCedula_conductor()%>"  maxlength="15" onKeyPress="soloAlfa(event)">
				<input name="cedula" type="hidden"  id="cedula" value="<%=dat.getCedula_conductor()%>">
                <a class="Simulacion_Hiper" style="cursor:hand "  onClick="window.open('<%=CONTROLLER%>?estado=Menu&accion=Enviar&numero=37&registroT=ok','Prop','height=450,width=750,left=300,status=yes,scrollbars=no,resizable=yes')" >Buscar Conductor </a> </td>
        </tr>
        <tr class="fila">
            <td>Fecha Creacion </td>
            <td><input name="nfechacreacion" type="text" class="textbox" id="nfechacreacion" value="<%=dat.getFecha_creacion()%>"  maxlength="20" onKeyPress="soloAlfa(event)">&nbsp;<img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.nfechacreacion);return false;" HIDEFOCUS>
				<input name="fechacreacion" type="hidden"  id="fechacreacion" value="<%=dat.getFecha_creacion()%>">
			</td>
        </tr>      		         
    </table>
		</td>
      </tr>
    </table>
	<p>
	<div align="center">
	<img src="<%=BASEURL%>/images/botones/modificar.gif" style="cursor:hand" title="Modificar un registro" name="modificar"  onClick="form1.Opcion.value='Modificar';if(Validar()){form1.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
	<img src="<%=BASEURL%>/images/botones/anular.gif"    style="cursor:hand" title="Anular un registro" name="anular"        onClick="form1.Opcion.value='Anular';if(ValidarNumTarjeta()){form1.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
	<img src="<%=BASEURL%>/images/botones/eliminar.gif"    style="cursor:hand" title="Eliminar un registro" name="anular"    onClick="form1.Opcion.value='Eliminar';if(ValidarNumTarjeta()){form1.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
	<img src="<%=BASEURL%>/images/botones/salir.gif"     style="cursor:hand" name="salir" title="Salir al Menu Principal"    onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" ></div>
	</p>
	<%if(!Mensaje.equals("")){%>
		<table border="2" align="center">
		  <tr>
			<td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
			  <tr>
				<td width="229" align="center" class="mensajes"><%=Mensaje%></td>
				<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
				<td width="58">&nbsp;</td>
			  </tr>
			</table></td>
		  </tr>
		</table>
	<%  }%>

</FORM>
</div>
<%=datos[1]%>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins_24.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
</body>
</html>
