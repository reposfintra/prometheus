<!--
- Autor : Ing. Jose de la rosa
- Date  : 9 de Octubre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que lista los productos
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head><title>Listar Registro Tarjetas</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Listado de Registros de Tarjetas"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%  
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);

    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
		
    List Listado = model.registroTarjetaSvc.getList();
    if(Listado!=null && Listado.size()>0) { %>   
<table width="750" border="2" align="center">
    <tr>
      <td>
	  	  <table width="100%" align="center">
              <tr>
                <td width="373" class="subtitulo1">&nbsp;Registro Tarjeta</td>
                <td width="427" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
              </tr>
        </table>	
	<table width="100%" border="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
		<tr class="tblTitulo" align="center">
			<td width="10%" nowrap >Numero Tarjeta </td>
			<td width="30%" nowrap >Placa</td>
			<td width="30%" nowrap >Cedula Conductor</td>
			<td width="30%" nowrap >Fecha Creacion</td>
		</tr>
		<pg:pager
			items="<%=Listado.size()%>"
			index="<%= index %>"
			maxPageItems="<%= maxPageItems %>"
			maxIndexPages="<%= maxIndexPages %>"
			isOffset="<%= true %>"
			export="offset,currentPageNumber=pageNumber"
			scope="request">
	<%
		for (int i = offset.intValue(), l = Math.min(i + maxPageItems, Listado.size()); i < l; i++){
			RegistroTarjeta dat  = (RegistroTarjeta) Listado.get(i);%>
			<pg:item>
				<tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" onClick="window.open('<%=CONTROLLER%>?estado=RegistroTarjeta&accion=Manager&numtarjeta=<%=dat.getNum_tarjeta()%>&placa=<%=dat.getPlaca()%>&cedula=<%=dat.getCedula_conductor()%>&fechacreacion=<%=dat.getFecha_creacion()%>&marco=no&Opcion=Buscar','myWindow','status=no,scrollbars=no,width=650,height=350,left=180,resizable=yes');"  >
					<td align="center" class="bordereporte"><%=dat.getNum_tarjeta()%></td>
					<td align="center" class="bordereporte"><%=dat.getPlaca()%></td>
					<td align="center" class="bordereporte"><%=dat.getCedula_conductor()%></td>
					<td align="center" class="bordereporte"><%=dat.getFecha_creacion()%></td>
				</tr>
		  </pg:item>
	<%}%>
		<tr class="pie">
			<td td height="20" colspan="10" nowrap align="center">          <pg:index>
					<jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>
			</pg:index>
			</td>
		</tr>
    	</pg:pager>        
	 </table>
	</td>
  </tr>
</table>
<br>
<%}else { %>
</p>
  <table border="2" align="center">
    <tr>
      <td><table width="410" height="50" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="282" align="center" class="mensajes">Su b&uacute;squeda no arroj&oacute; resultados!</td>
            <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="78">&nbsp;</td>
          </tr>
      </table></td>
    </tr>
  </table>
  <p>&nbsp; </p>
  <%}%>
 <table width="750" border="0" align="center">
    <tr>
      <td>
	  <img src="<%=BASEURL%>/images/botones/regresar.gif" style="cursor:hand" title="Volver" name="buscar"  onClick="window.location='<%=BASEURL%>/jsp/masivo/registro_tarjeta/RegistroTarjetaSearch.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	  </td>
	</tr>
</table>
</div>
<%=datos[1]%>
</body>
</html>
