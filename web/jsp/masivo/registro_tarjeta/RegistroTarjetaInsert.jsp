<!--
- Autor : Ing. Juan Manuel Escandon Perez
- Date  : Julio de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra los datos de entrada para 
--              la inicializacion del Reporte de Remesas sin documentos
--%>

<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<% String Mensaje       = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>

    <title>Registro de Tarjeta</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script src='<%= BASEURL %>/js/validar.js'></script>
	<script src="<%= BASEURL %>/js/boton.js"></script>
	<script>
	function Validar(){
		if( form1.numtarjeta.value == '' ){
			alert('Debe escribir un Numero de tarjeta');
			form1.numtarjeta.focus;
			return false;
		}
		if( form1.cedula.value == '' ){
			alert('Debe escribir la Cedula del conductor');
			form1.cedula.focus;
			return false;
		}
		if( form1.placa.value == '' ){
			alert('Debe escribir la Placa del vehiculo');
			form1.placa.focus;
			return false;
		}
			return true;
	}	
	</script>
</head>
<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Registro Tarjeta"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

    <form name="form1" method="post" action="<%= CONTROLLER %>?estado=RegistroTarjeta&accion=Manager">
        <table width="500" border="2" align="center">
			<tr>
				<td>
					<table width="100%" height="34" border="0" align="center" class="tablaInferior">
            			<tr>
							<td height="24"  class="subtitulo1"><p align="left">&nbsp;Insertar Registro</p></td>
            				<td  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
							<input name="Opcion" type="hidden" id="Opcion">
        				</tr>     												
						<tr class="fila">
							<td align="left" width="158" class="letra_resaltada">&nbsp;&nbsp;Numero Tarjeta </td>
							<td align="left" width="238">&nbsp;<input type="text" id="numtarjeta" name="numtarjeta" class="textbox" maxlength="30" onKeyPress="soloAlfa(event)"></td>
						</tr>
						<tr class="fila">
                			<td class="letra_resaltada" align="left">&nbsp;&nbsp;Cedula Conductor </td>
               			    <td align="left">&nbsp;<input type="text" id="cedula" name="cedula" class="textbox" maxlength="15" onKeyPress="soloAlfa(event)"> <a class="Simulacion_Hiper" style="cursor:hand "  onClick="window.open('<%=CONTROLLER%>?estado=Menu&accion=Enviar&numero=37&registroTI=ok','Prop','height=450,width=750,left=300,status=yes,scrollbars=no,resizable=yes')" >Buscar Conductor </a></td>	  
            			</tr>
						<tr class="fila">
							<td class="letra_resaltada" align="left">&nbsp;&nbsp;Placa</td>
							<td align="left">&nbsp;<input type="text" id="placa" name="placa" class="textbox" maxlength="12" onKeyPress="soloAlfa(event)"></td>            
					  	</tr>   
					</table> 
			  </td>        
		  </tr>        	
		</table>
		<br>
        <table align="center">
          <tr align="center">
            <td> <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="form1.Opcion.value='Guardar';if(Validar()){form1.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> <img src="<%=BASEURL%>/images/botones/restablecer.gif"  name="imgrestablecer" onClick="form1.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
          </tr>
        </table>
        
        <p>
          <%if(!Mensaje.equals("")){%>
</p>
        <table border="2" align="center">
          <tr>
            <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                <tr>
                  <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
                  <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                  <td width="58">&nbsp;</td>
                </tr>
            </table></td>
          </tr>
        </table>
        <%}%>
    </form>
	</div>
	<%=datos[1]%>
</body>
</html>
