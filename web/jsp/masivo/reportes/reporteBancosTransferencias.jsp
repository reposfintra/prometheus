<!--
- Autor : Ing. Ivan Dario Gomez Vanegas
- Date  : Octubre 25 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra el reporte de viaje
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>Produccion Pendiente</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'> 
<link href="<%= BASEURL %>/css/estilomenuemergente.css" rel='stylesheet'> 
<link href="../../trafico/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body onResize="redimensionar()" onLoad="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Produccion Pendiente"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<center>
<%
	List Listado = model.ReporteBTSvc.getListaReporte();
	
%>
<%  if(Listado!=null && Listado.size()>0) { %>   
<table width="95%" border="2" align="center">
  <tr>
    <td>  
      <table width="100%" border="0" align="center">
            <tr>
   		<td width="313"  height="24"  class="subtitulo1"><p align="left">Planillas Cumplidas Por Liquidar</p></td>
            	<td width="865"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>						
            </tr>
       </table>			
       <table width="100%" border="0">
        <tr>
          <td>
             <table width="100%" border="1" align="left" bordercolor="#999999" bgcolor="#F7F5F4">
                <tr class="tblTitulo" align="center">
                    <td  nowrap>CLIENTE   </td>
                    <td  nowrap>NOMBRE    </td>
                    <td  nowrap>PLANILLAS </td>
                </tr>
         
		<% int Cont = 1;
                    Iterator it = Listado.iterator();
                    while(it.hasNext()){
                       Hashtable fila  = (Hashtable) it.next();
                       String estilo =  (Cont++ % 2 == 0 ) ? "filaazul" : "filagris";
                       %>
                        <tr class="<%=estilo%>">
                          <td  class="bordereporte"><%=(String)fila.get("nit")%></td>
                          <td  class="bordereporte"><%=(String)fila.get("nombre")%></td>
                          <td  align="center" class="bordereporte">
                             <a href="#" onclick="javascript: window.open('<%=CONTROLLER%>?estado=Liquidar&accion=OC&Opcion=VistaPrevia&produccion=yes&parametro=<%=(String)fila.get("nit")%><%=(String)fila.get("ocs")%>&tipo=1','LiquidacionIvan','');">
                                 <%=(String)fila.get("totaloc")%>
                             </a>
                          </td>
                        </tr>
                <%}%>
        
              </table>	  
          </td>
       </tr>
    </table>
    	  
    </td>
   </tr>
</table>

<BR>
<img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgaceptar" onclick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">




<%}else{
     String mensaje = "No hay datos para estos parametros";%>
     <p><table border="2" align="center">
      <tr>
        <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
          <tr>
            <td width="229" align="center" class="mensajes"><%=mensaje%></td>
            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
            <td width="58">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table>
    </p>
    <table width="1200" border="0" align="center">
     <tr>
          <td align="left" width="50%"><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgaceptar" onclick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/masivo/reportes&pagina=InicialReporte.jsp&titulo=Reporte de Viajes&marco=no'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>	  
        </tr>
    </table> 
 <%}%>


</center>
</div>
</body>
</html>