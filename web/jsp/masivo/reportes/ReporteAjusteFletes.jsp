<!--
- Autor : Ing.Luis Eduardo Frieri
- Date  : 7 marzo 2007
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, genera reporte ajustes de flete
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>

<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>

<script>
function validarretro(){
  	var fecha1 = document.forma.fechai.value.replace('-','').replace('-','');
   	var fecha2 = document.forma.fechaf.value.replace('-','').replace('-','');
	var mes1 = fecha1.substring(4,6);
	var mes2 = fecha2.substring(4,6);
	var a�o1 = fecha1.substring(0,4);
	var a�o2 = fecha2.substring(0,4);
	if((document.forma.fechai.value == '') || (document.forma.fechaf.value == '')){
            alert('No deje espacios en blanco.');
        }else{
           
            if(fecha1>fecha2) {     
                    alert('La fecha final debe ser mayor que la fecha inicial');
                    return (false);
            }else {
                    forma.submit();
            }
        
        }
        
}
</script>

<title>Consulta Reporte Gerencial Ajustes a Fletes</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
</head>
<% String Msg = request.getParameter("Msg");%>
<%-- Inicio Body --%>
<body onLoad="redimensionar();" onResize="redimensionar();">
	<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte Gerencial de Ajustes a Fletes"/>
	</div>
	
	<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
	<form name="forma" method="post" action="<%=CONTROLLER%>?estado=ReporteGerencialAjustes&accion=Fletes">
		<table width="500" border="2" align="center">
			<tr>
				<td>
					<table width="100%" align="center"  class="tablaInferior">
						<tr class="fila">
							<td width="50%" align="left" class="subtitulo1">Consulta Reporte Gerencial Ajustes a Fletes</td>
							<td width="50%"  align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
						</tr>
					</table>
					 <table width="100%" border="0" align="center" class="tablaInferior">
                                              <tr class="fila">
                                                <td><strong>Fecha inicial </strong></td>
                                                <td>
                                                  <input name='fechai' type='text' class="textbox" id="fechai" style='width:120' value='' readonly>
                                                  <a href="javascript:void(0);" class="link" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.forma.fechai);return false;"  HIDEFOCUS > <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16"
                                               border="0" alt="De click aqu&iacute; para ver el calendario."></a> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
                                              </tr>
                                              <tr class="fila" >
                                                <td><strong>Fecha final </strong></td>
                                                <td>
                                                  <input name='fechaf' type='text' class="textbox" id="fechaf" style='width:120' value='' readonly>
                                                  <a href="javascript:void(0);" class="link" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.forma.fechaf);return false;"  HIDEFOCUS > <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16"
                                               border="0" alt="De click aqu&iacute; para ver el calendario."></a> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
                                              </tr>

                                            </table>
				</td>
			</tr>
		</table>
		<p align="center">
			<img src="<%=BASEURL%>/images/botones/exportarExcel.gif"  name="Excel" style="cursor:hand" title="Exportar a Excel" onMouseOver="botonOver(this);" onClick="validarretro();" onMouseOut="botonOut(this);" >&nbsp;
			<img src="<%=BASEURL%>/images/botones/salir.gif"  name="regresar" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >
		</p>
	</form>
	 <%if(Msg != null){
              %>
               <table border="2" align="center">
                  <tr>
                    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                       <tr>
                        <td width="229" align="center" class="mensajes"><%=Msg%> 
                       </td>
                       <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                      </tr>
                    </table>
                    </td>
                  </tr>
                </table>
            <%  
             }%>
	</div>
		 <iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
     </iframe>
</body>
</html>
