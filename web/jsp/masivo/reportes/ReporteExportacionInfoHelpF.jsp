<%@ include file="/WEB-INF/InitModel.jsp"%>

<HTML>
<HEAD>
<TITLE>Reporte de Exportaci&oacute;n</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</HEAD>
<BODY>  

  <table width="95%"  border="2" align="center">
    <tr>
      <td>
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">Reporte de Exportaci&oacute;n </div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa de Reporte de Exportaci&oacute;n. </td>
          </tr>
          <tr>
            <td  height="18"  class="ayudaHtmlTexto">Formulario de Reporte de Exportaci&oacute;n. </td>
          </tr>
          <tr>
            <td height="18"  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL%>/images/ayuda/masivo/reportes/REPEXP/Dibujo3.PNG"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">Luego de seleccionar los reportes a generar, se presentar&aacute; el siguiente mensaje: </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%= BASEURL%>/images/ayuda/trafico/RepPC/Dibujo4.PNG"></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">&nbsp;</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto">Los reportes generados se pueden ver en los archivos del usuario. </td>
          </tr>
          
          
          
      </table></td>
    </tr>
  </table>
</BODY>
</HTML>
