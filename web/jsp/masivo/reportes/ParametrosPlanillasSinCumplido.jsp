<!--
- Autor : Ing. David Velasquez Gonzalez
- Date  : Noviembre de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra los datos de entrada para 
--              la inicializacion del Reporte de planillas Sin Cumplido
--%>

<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>

<% String mensaje = request.getParameter("Mensaje"); 
   String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
   String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>

<% List	ListaAgencias = model.ciudadService.ListarAgencias();%>
<html>
<head>
    <title>Reporte de Planillas Sin Cumplido</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script src='<%= BASEURL %>/js/validar.js'></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
	<script>
		function formato2digitos( num ){
    		return (num<10? '0':'') + num;
  		}

		function formato (fecha){
			return fecha.getYear() + '-' + formato2digitos(fecha.getMonth()+1) + '-' + formato2digitos(fecha.getDate());
		}
		
		function cargar(){
  			var fechaActual  =  new Date();
  			var fechaMenos30 =  new Date();
  			fechaMenos30.setDate(fechaActual.getDate()-30);
			form1.finicial.value = formato(fechaMenos30);
			form1.ffinal.value = formato(fechaActual);
		}
	</script>
</head>
<%-- Inicio Body --%>
<body onLoad="cargar();"> 

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte Planillas Sin Cumplido"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 102px; overflow: scroll;"> 

    <form name="form1" method="post" action="<%= CONTROLLER %>?estado=Planillasin&accion=Cumplido">
        <table width="44%" border="2" align="center">
			<tr>
				<td>
					<table width="100%" height="40" border="0" align="center" class="tablaInferior">
            			<tr>
							<td width="50%" height="24"  class="subtitulo1"><p align="left">Parametros Iniciales</p></td>
            				<td width="50%"  class="barratitulo"><%=datos[0]%><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
							<input name="Opcion" type="hidden" id="Opcion">
        				</tr>     						
						<tr class="fila">
							<td width="50%" align="left"class="letra_resaltada">&nbsp;&nbsp;FECHA INICIAL</td>
							<td width="50%" align="left">&nbsp;&nbsp;<input type='text' id="finicial" name="finicial" readonly="true" class="textbox"></input> 														
								<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.finicial);return false;" HIDEFOCUS>
									<img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha">
								</a><img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
							</td>
						</tr>
						<tr class="fila">
							<td width="50%" align="left"class="letra_resaltada">&nbsp;&nbsp;FECHA FINAL</td>
							<td width="50%" align="left">&nbsp;&nbsp;<input type='text' id="ffinal" name="ffinal" readonly="true" class="textbox"></input> 														
								<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.ffinal);return false;" HIDEFOCUS>
									<img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha">
							    </a><img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10">
								
							</td>
						</tr>
						<tr class="fila">
						  <td width="50%" class="letra_resaltada" align="left">&nbsp;&nbsp;CLIENTE</td>
						  <td width="50%"  align="left">&nbsp;&nbsp;<input type="text" id="codcli" name="codcli" class="textbox" maxlength="7">
						  <a onClick="window.open('<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=consultasCliente.jsp&marco=no&carpeta=/consultas','','status=yes,scrollbars=no,width=650,height=600,resizable=yes')" style="cursor:hand"><img height="20" width="25" src="<%=BASEURL%>/images/botones/iconos/user.jpg"></a>						  
						  </td>
					  </tr>   
					</table> 
			  </td>        
		  </tr>        	
		</table>
        <table align="center">
          <tr align="center">
            <td colspan="2"> <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="form1.Opcion.value='Generar';form1.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> </td>
          </tr>
        </table>
    </form>
	 <%if(mensaje != null){ %>
               <table border="2" align="center">
                  <tr>
                    <td>
					  <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                       <tr>
                        <td width="229" align="center" class="mensajes"><%=mensaje%> 
                       </td>
                       <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                      </tr>
                     </table>
                   </td>
                 </tr>
               </table>
     <% }%>	
</div>
	 <%=datos[1]%>
	<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>
