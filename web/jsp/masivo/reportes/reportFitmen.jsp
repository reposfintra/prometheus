<%@ page session="true"%>
<%@ page errorPage="../error/error.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Generar Reporte Fitmen</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script src="<%=BASEURL%>/js/validar.js"></script>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script SRC='<%=BASEURL%>/js/boton.js'></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Generar Reporte"/>
</div>
<%
        String accion = CONTROLLER+"?estado=Fitmen&accion=Manager";
%>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
    <form action="<%=accion%>" method="post" name="forma" id="forma">
      <table width="500" border="2" align="center">
        <tr>
          <td><table width="100%" class="tablaInferior">
              <tr class="barratitulo">
                <td align="left">
                  <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="45%" class="subtitulo1"> Generar Reporte Fitmen </td>
                      <td width="55%"><img src="<%=BASEURL%>/images/titulo.gif"></td>
                    </tr>
                </table></td>
              </tr>
              <tr class="fila">
                <td align="left" ><div align="center">Haga clic en Aceptar para iniciar el proceso </div></td>
              </tr>
          </table></td>
        </tr>
      </table>
      <div align="center"><br>
        <img src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="forma.submit()" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
		<img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >
	  </div>
    </form>
</div>
</body>
</html>
