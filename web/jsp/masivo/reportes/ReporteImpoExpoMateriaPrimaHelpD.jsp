<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Descripci&oacute;n de campos para la generaci&oacute;n de reporte de materia prima</title>

<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<% String BASEIMG = BASEURL +"/images/botones/"; %> 
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">GENERACI&Oacute;N REPORTE MATERIA PRIMA </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">GENARAR PROCESO</td>
        </tr>
        <tr>
          <td width="123" class="fila">Cliente</td>
          <td width="551"  class="ayudaHtmlTexto"> El codigo del cliente </td>
        </tr>
        <tr>
          <td  class="fila"> Consultar Clientes </td>
          <td  class="ayudaHtmlTexto"> Use esta opci&oacute;n si desea buscar el cliente por nombre y asi obtener el codigo </td>
        </tr>
        <tr>
          <td  class="fila">Tipo documento </td>
          <td  class="ayudaHtmlTexto"> Se refiere al tipo de documento con el cual se desea generar el reporte </td>
        </tr>
		<tr>
          <td width="123"  class="fila"> Bot&oacute;n Aceptar </td>
          <td width="551"  class="ayudaHtmlTexto">Inicia el proceso de generaci&oacute;n del reporte. </td>
        </tr>
		<tr>
          <td width="123"  class="fila"> Bot&oacute;n Salir </td>
          <td width="551"  class="ayudaHtmlTexto">Cierra la ventana. </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<br>
<table width="416" align="center">
	<tr>
	<td align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	</td>
	</tr>
</table>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
