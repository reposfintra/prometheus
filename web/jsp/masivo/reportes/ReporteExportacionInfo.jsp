<!--
- Autor : Ing. Andr�s Maturana De La Cruz
- Date  : 26 agosto 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve para Listar cuentas.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.finanzas.contab.model.*"%>
<%@page import="com.tsp.finanzas.contab.model.beans.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head><title>Reporte de Exportaci&oacute;n</title> 
 <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script>
    function SelecAll(){
            for(i=0;i<forma.length;i++)
                    forma.elements[i].checked=forma.All.checked;
    }
    function ActAll(){
            forma.All.checked = true;
            for(i=1;i<forma.length;i++)	
                    if (!forma.elements[i].checked){
                            forma.All.checked = false;
                            break;
                    }
    } 
	function validarForma(){
		//alert('Hola...' + forma.elements.length);
		for( i=0; i< forma.elements.length; i++){
			//alert('*Hola...' + forma.elements[i].name + "-" + forma.elements[i].checked );
			if( forma.elements[i].checked!=false ){
				//alert('**Hola...' + forma.elements[i].name );
				return true;
			}
		}
		alert('Debe seleccionar al menos un registro.');
		return false;
	}
</script>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Reporte de Exportaci�n"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
<% 
    Vector info = (Vector) request.getAttribute("info");
	
	if (info.size() > 0) {
%>
<br>
<form name="forma" action='<%=CONTROLLER %>?estado=Reporte&accion=ExportacionXLS' id="forma" method="post">
<table width="100%" border="2" align="center">
  <tr>
    <td>
	  <table width="100%">
        <tr>
          <td width="50%" class="subtitulo1">Listado</td>
          <td width="50%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
        </tr>
      </table>
	  <table width="100%" border="1" cellpadding="4" cellspacing="1" bordercolor="#999999" bgcolor="#F7F5F4" align="center">
        <tr class="tblTitulo" align="center">
          <td nowrap ><div align="center">
            <input type='checkbox' name='All' onclick='jscript: SelecAll();'>
          </div></td>
          <td nowrap ><div align="center">Tipo</div></td>
		  <td nowrap ><div align="center">Documento</div></td>
		  <td nowrap ><div align="center">Descripci&oacute;n</div></td>
		  <td nowrap >Cliente</td>
		  <td nowrap ><div align="center">Entrega a SIA </div></td>
		  <td nowrap ><div align="center">Entrega a ETA </div></td>
		  </tr>
<%
    for (int i = 0; i < info.size(); i++){
        Hashtable ht = (Hashtable) info.get(i);
		
%>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
          <td><div align="center">
            <input type="checkbox" name='LOV' value="<%= ht.get("dstrct") + "|" + ht.get("tipo_doc") + "|" + ht.get("documento") %>">
          </div></td>
          <td><%= ht.get("tipo_doc_desc") %></td>
          <td><%= ht.get("documento") %></td>
          <td><%= ht.get("descripcion") %></td>
          <td><%= ht.get("nombrecliente") %></td>
          <td><%= ht.get("fecha_sia") %></td>
          <td><%= ht.get("fecha_eta") %></td>
          </tr>
<%  }%>
      </table>	</td>
  </tr>  
</table>
  <br>
  
  <br>
  <center>
    <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="c_aceptar" onClick="if (validarForma()) forma.submit(); " onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
    <img src="<%=BASEURL%>/images/botones/cancelar.gif" name="c_cancelar" onClick="forma.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> 
      <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="window.history.back();"  style="cursor:hand">
    <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
  </center>
</form>
<%} else {%>
    <table border="2" align="center">
      <tr>
        <td>
	      <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
            <tr>
              <td width="229" align="center" class="mensajes">No se encontraron resultados.</td>
              <td width="29"><img src="<%=BASEURL%>/images/cuadronaranja.JPG"></td>
            </tr>
          </table>
	    </td>
      </tr>
    </table>
<p>
<center>
      <img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="window.history.back();"  style="cursor:hand">
	  <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
</center>
<%}%>
</div>
<%=datos[1]%>
</body>
</html>
