<!--
- Autor : Ing. Juan Manuel Escandon Perez
- Date  : Noviembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra los datos de entrada para 
--              la inicializacion del Reporte de planillas anuladas
--%>

<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<% List	ListaAgencias = model.ciudadService.ListarAgencias();%>
<%
 String Mensaje = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
    
 String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
 String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>

<html>
<head>
    <title>Reporte de Planillas anuladas</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script src='<%= BASEURL %>/js/validar.js'></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <link href="<%= BASEURL %>/css/estilomenuemergente.css" rel='stylesheet'> 
    <link href="../../trafico/css/estilostsp.css" rel="stylesheet" type="text/css">
    <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validarDocumentos.js"></script>

    <script>
        function formato2digitos( num ){
        return (num<10? '0':'') + num;
        }

        function formato (fecha){
        return fecha.getYear() + '-' + formato2digitos(fecha.getMonth()+1) + '-' + formato2digitos(fecha.getDate());
        }
		
        function cargar(){
        var fechaActual  =  new Date();
        var fechaMenos30 =  new Date();
        fechaMenos30.setDate(fechaActual.getDate()-30);
        form1.finicial.value = formato(fechaMenos30);
        form1.ffinal.value = formato(fechaActual);
        }
    </script>
</head>
<body onResize="redimensionar()" onLoad="redimensionar();cargar();">
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Parametros de Busqueda"/>
    </div>

    <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

        <form name="form1" method="post" action="<%= CONTROLLER %>?estado=Anulacion&accion=Planilla">
        <table width="410" border="2" align="center">
            <tr>
            <td><table width="100%"  border="0">
                <tr>
                    <td width="59%" class="subtitulo1">Planillas Anuladas</td>
                    <td width="38%" class="barratitulo"><%=datos[0]%><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"align="left"></td>
                    
                </tr>
            </table>
                <table width="100%" class="tablaInferior" >
                 						
                    <tr class="fila">
                    <td align="left"class="letra_resaltada">&nbsp;&nbsp;FECHA INICIAL</td>
                    <td align="left">&nbsp;&nbsp;<input type='text' id="finicial" name="finicial" readonly="true" class="textbox"></input> 														
                        <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.finicial);return false;" HIDEFOCUS>
                        <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha">
                        </a>
                    </td>
                    </tr>
                    <tr class="fila">
                    <td align="left"class="letra_resaltada">&nbsp;&nbsp;FECHA FINAL</td>
                    <td align="left">&nbsp;&nbsp;<input type='text' id="ffinal" name="ffinal" readonly="true" class="textbox"></input> 														
                        <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.ffinal);return false;" HIDEFOCUS>
                        <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha">
                        </a>
                    </td>
                    </tr>
                    <tr class="fila">
                    <td class="letra_resaltada" align="left">&nbsp;&nbsp;AGENCIA</td>
                    <td align="left">&nbsp;&nbsp;<select name='agencia' id="agencia" class="textbox">
                        <option value="%">TODAS</option> 
			<%    if(ListaAgencias.size() > 0) {
                                  Iterator It3 = ListaAgencias.iterator();
                              	  while( It3.hasNext() ) {
                               	      Ciudad  datos2 =  (Ciudad) It3.next();                             
                               	      out.print("<option value='"+datos2.getCodCiu()+"'>["+datos2.getCodCiu()+"] "+datos2.getNomCiu()+"</option> \n");
                              	  }
                      	      }
                       	%>
                    </select></td>	  
                    </tr>
                    <tr class="fila">
                    <td class="letra_resaltada" align="left">&nbsp;&nbsp;USUARIO</td>
                    <td align="left">&nbsp;&nbsp;<input type="text" id="usuario" name="usuario" class="textbox" maxlength="20"></td>            
                    </tr>   
                </table> 
            </td>        
            </tr>  
            <input name="Opcion" type="hidden" id="Opcion">
        </table>
        <table align="center">
            <tr align="center">
                <td colspan="2"> <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="form1.Opcion.value='Generar';form1.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> </td>
            </tr>
        </table>
      </form>
    </div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
<%=datos[1]%>
</body>
</html>
