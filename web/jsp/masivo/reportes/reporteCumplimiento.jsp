  <!--
     - Author(s)       :      Osvaldo P�rez Ferrer
     - Date            :      15 de Junio de 2006
     - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
  -->
 <%--   - @(#)
     - Description:  Formulario para seleccionar rango de fecha
     -               para Indicador de Cumplimiento  
 --%>


<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.operation.model.*, com.tsp.operation.model.beans.*, com.tsp.util.*,java.util.*, java.text.*"%>

<html>
<head>
    <title>Indicador de Cumplimiento Colocaci&oacute;n</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">

    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
    
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <script type='text/javascript' src="<%=BASEURL%>/js/date-picker.js"></script>
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">

    <script>
        function validFechas(){
        var fecha1 = document.form1.inicio.value.replace('-','').replace('-','');
        var fecha2 = document.form1.fin.value.replace('-','').replace('-','');
        var fech1 = parseFloat(fecha1);
        var fech2 = parseFloat(fecha2);
        if(fech1>=fech2) {     
        alert('La fecha final debe ser mayor que la fecha inicial');
        return (false);
        }  
            
        return true;
        }        
    </script>
<%
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
    
    String hoy     = Utility.getHoy("-");     
    String mensaje = (String) request.getAttribute("mensaje");         
    
%>
</head>

<body onLoad="redimensionar();" onResize="redimensionar();">
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=Indicador Cumplimiento Colocaci�n"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">        
        <form id="forml" name="form1" method="post" action="<%=CONTROLLER%>?estado=Reporte&accion=Cumplimiento&opcion=sub">                
        <table width="49%"  border="2" align="center" id="tablafechas">
            <tr>
                <td><table width="100%"  border="1" align="center" class="tablaInferior">
                <tr>
                <td>
                <table width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                <td height="22" colspan=2 class="subtitulo1">
                    Indicador Cumplimiento Colocaci&oacute;n</td>
                    <td width="50%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif"><%=datos[0]%>
                </td>
                
                    
            </tr>
                </table>
                <table width="100%"  border="0" align="center" cellpadding="2" cellspacing="3">
                    <tr class="fila">
                        <td width="31%">Periodo Inicial: </td>
                        <td width="69%">
                            <input name="inicio" type="text" class="textbox" id="inicio" value='<%=hoy%>' size="13" readonly>
                            <img src="<%=BASEURL%>/images/cal.gif" name="popcal" width="16" height="16" border="0"
                            align="top"  style="cursor:hand"
                            onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.inicio);return false;" 
                            alt="Click para ingresar fecha" HIDEFOCUS></span>
                        </td>
                    </tr>
                    <tr class="fila">
                    <td><strong>Periodo Final: </strong></td>
                    <td>
                        <input name="fin" type="text" class="textbox" id="fin" value='<%=hoy%>' size="13" readonly>
                        <img src="<%=BASEURL%>/images/cal.gif" name="popcal" width="16" height="16" border="0"
                        align="top"  style="cursor:hand"
                        onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fin);return false;"
                        alt="Click para ingresar fecha" HIDEFOCUS></span>
                    </td>
                    </tr>                  
                </table></td>
                </tr>
            </table></td>
            </tr>
        </table>
        <br>
        <table align="center">
            <tr><td>
                <img id="baceptar" name="baceptar"  src="<%=BASEURL%>/images/botones/aceptar.gif" style="cursor:hand"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="if(validFechas()){document.form1.submit();}">            
            </td><td>
                    <img  src="<%=BASEURL%>/images/botones/cancelar.gif" style="cursor:hand"  onClick="form1.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">         
                </td><td>  
                    <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">            
                </td>
            </tr>    
        </table>                           

        </form>
    	   
            
            <%if(mensaje!=null){%>
            
        <table border="2" align="center">
            <tr>
                <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                    <tr>
                        <td width="229" align="center" class="mensajes"><%=mensaje%></td>
                        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                    </tr>
                </table></td>
            </tr>
        </table>
         <%}%>
   	        
	
    </div>	
    <iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
<%=datos[1]%>    
</body>
</html>
