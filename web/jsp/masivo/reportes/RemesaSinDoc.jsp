<!--
- Autor : Ing. Juan Manuel Escandon Perez
- Date  : Noviembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra el reporte de viaje
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>Remesas sin documentos</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'> 
<link href="<%= BASEURL %>/css/estilomenuemergente.css" rel='stylesheet'> 
<link href="../../trafico/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/menu_emergente.js"></script>
</head>
<body onLoad="redimensionar();" onResize="redimensionar();">
<%
	List Listado = model.RemesaSinDocSvc.getListRSD();
	String fechai = request.getParameter("fechai");
	String fechaf = request.getParameter("fechaf");
%>

<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
		<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Remesas sin documentos"/>
	</div>
	<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">  
<table width="1400" border="2" align="center">
  <tr>
    <td>  
      <table width="100%" border="0" align="center">
			<tr>
   				<td width="313"  height="24"  class="subtitulo1"><p align="left">Listado General</p></td>
            	<td width="865"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>						
            </tr>
	  </table>			
	  <table width="100%" border="0" height="10">
        <tr>
          <td><table width="98.795%" border="1" align="left" bordercolor="#999999" bgcolor="#F7F5F4">
            <tr class="tblTitulo" align="center">
              <td width="8%" nowrap>Remesa</td>
              <td width="11%" nowrap>Fecha remesa</td>
              <td width="8%" nowrap>Cliente</td>
              <td width="12%" nowrap>Origen</td>
              <td width="12%" nowrap>Destino</td>
              <td width="5%" nowrap>Peso </td>
              <td width="5%" nowrap>Unidad</td>
              <td width="10%" nowrap>Agencia</td>
              <td width="20%" nowrap>Fecha creacion</td>
              <td width="15%" nowrap>Usuario</td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td> <div  style=" overflow:auto ; WIDTH: 100%; HEIGHT:340px; "><table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4" id="principal">
        
		<% int Cont = 1;
    	Iterator it = Listado.iterator();
        while(it.hasNext()){
			RemesaSinDoc dat  = (RemesaSinDoc) it.next();
			String estilo =  (Cont++ % 2 == 0 ) ? "filaazul" : "filagris";
			
       %>
        <tr class="<%=estilo%>" onMouseOver="cambiarColorMouse(this,'<%=dat.getNumrem()%>')" style="cursor:hand ">
          <td width="8%" align="center" class="bordereporte"><%=dat.getNumrem()%></td>
          <td width="11%"  align="center" class="bordereporte"><%=(dat.getFecharemesa() != null) ? dat.getFecharemesa() : ""%></td>
          <td width="8%"  align="center" class="bordereporte"><%=(dat.getCliente() != null) ? dat.getCliente() : ""%></td>
          <td width="12%"  align="center" class="bordereporte"><%=(dat.getOrigen()!=null) ? dat.getOrigen() : ""%></td>
          <td width="12%"  align="center" class="bordereporte"><%=(dat.getDestino()!=null) ? dat.getDestino() : ""%></td>
          <td width="5%"  align="center" class="bordereporte"><%=dat.getCantidad()%></td>
          <td width="5%"  align="center" class="bordereporte"><%=(dat.getUnidad()!= null) ? dat.getUnidad() : ""%></td>
          <td width="10%"  align="center" nowrap class="bordereporte"><%=(dat.getAgencia()!= null) ? dat.getAgencia() : ""%></td>
          <td width="20%"  align="center" nowrap class="bordereporte"><%=(dat.getFechacreacion()!= null) ? dat.getFechacreacion() : ""%></td>
          <td width="15%"  align="center" class="bordereporte"><%=(dat.getUsuariocreacion() != null) ? dat.getUsuariocreacion() : ""%></td>
        </tr>
        <%}%>
      </table></div></td>
        </tr>
      </table>	  
    </td>
  </tr>
</table>

<table width="1200" border="0" align="center">
    <tr>
      <td align="left" width="50%"><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgaceptar" onclick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/masivo/reportes&pagina=ParametrosRemesaSD.jsp&titulo=Parametros de busqueda'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
	  <td align="left" width="50%"><img src="<%=BASEURL%>/images/botones/iconos/excel.jpg"  name="imgaceptar" onclick="window.location='<%= CONTROLLER %>?estado=RemesaSin&accion=Doc&finicial=<%=fechai%>&ffinal=<%=fechaf%>&Opcion=Excel'"  style="cursor:hand" height="30"></td>
    </tr>
</table> 
	</div>
<div class="skin" id="menu" onMouseOver="highlight();" onMouseOut="lowlight()" onClick="jumpto(numrem,'<%=BASEURL%>','<%=CONTROLLER%>');">
    <hr>
	<div opcion='documentos' class="menuitem" url="">&nbsp;&nbsp Agregar doc. internos</div>
	<div opcion='destinatarios'    class="menuitem" url="">&nbsp;&nbsp Agregar doc. a destinatario</div>
	<hr>
</div>

</body>
</html>
<script>
   principal.oncontextmenu = showMenu;
   document.body.onclick = hideMenuPrincipal;
   
   var anteriorMouse;
   var colorAnteriorMouse;
   var numrem;
function cambiarColorMouse(fila,nr){
	if ( fila == anteriorMouse ){
		return;
	}
	if ( anteriorMouse != null ){
		anteriorMouse.className = colorAnteriorMouse;
		numrem = nr;
	}
	var aux = fila.className;
	fila.className = "filaseleccion";
	anteriorMouse = fila;
	colorAnteriorMouse = aux;
}


</script>