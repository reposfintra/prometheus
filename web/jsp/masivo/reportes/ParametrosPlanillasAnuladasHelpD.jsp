<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Descripcion de campos Reporte de Planillas Anuladas</title>


<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<% String BASEIMG = BASEURL +"/images/botones/"; %> 
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Reporte de Planillas Anuladas</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">PROCESOS</td>
        </tr>
        <tr>
          <td  class="fila">Fecha Inicial </td>
          <td  class="ayudaHtmlTexto">Fecha del Rango inicial </td>
        </tr>
        <tr>
          <td  class="fila">Fecha Final </td>
          <td  class="ayudaHtmlTexto">Fecha del Rango final </td>
        </tr>
         <tr>
          <td  class="fila">Agencia</td>
          <td  class="ayudaHtmlTexto">Desplega las agencias asociadas para escoger</td>
        </tr>
         <tr>
          <td  class="fila">Usuario</td>
          <td  class="ayudaHtmlTexto">Recibe el usuario del que se desea mostrar el reporte</td>
        </tr>
        <tr>
          <td class="fila"> Boton Aceptar <img src="<%=BASEURL%>/images/botones/aceptar.gif"></td>
          <td  class="ayudaHtmlTexto">Boton para Mostrar el Reporte</td>
        </tr>
        <tr>
          <td width="556" class="fila">Boton Salir <img src="<%=BASEIMG%>salir.gif"></td>
          <td width="556"  class="ayudaHtmlTexto">Boton para salir de la vista , regresando a control trafico.</td>
        </tr>
      </table>
      <br>
      <center>
	<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
      </center>
      <br>
        <p align="center" class="fuenteAyuda"> Fintravalores S. A. </p>
      <br>
    </td>
  </tr>
</table>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp;</p>
</body>
</html>
