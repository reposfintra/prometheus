<!--
- Autor : Ing. Osvaldo P�rez Ferrer
- Date : 19 de Octubre de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que sirve para consultar importaciones
--%>

<%@ page session="true"%>
<%//@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%@page import="com.tsp.operation.model.*, com.tsp.operation.model.beans.*, com.tsp.util.*,java.util.*, java.text.*"%>

<html>
<head>
    <title>Consultar Importaciones</title>
    <script src="<%=BASEURL%>/js/utilidades.js"></script>
    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
    <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
    <link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<%
    
    String opc = request.getParameter("opc")!=null? request.getParameter("opc") : "";  
    String mensaje = "";
    String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
    String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);    
    Vector v;
    ImpoExpo imp = null;

    
%>
</head>

<body>
    <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
        <jsp:include page="/toptsp.jsp?encabezado=Consultar Importaciones Materia Prima"/>
    </div>
    <div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top:100px; overflow: scroll;">        
    <form id="form1"  name="form1" method="post" action="">
<%
   
		v = model.impoExpoService.getVector();
		if( v !=null && v.size() > 0 ){
%>
            <table width="100%"  border="0" align="center" cellpadding="2" cellspacing="3">
            <tr>
            <td width="100%">
                <table width="100%" class="tablaInferior">
                    <tr>
                        <td height="22" colspan=2 class="subtitulo1">Importaciones</td>
                        <td width="50%" class="barratitulo"><img align="left" src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"><%=datos[0]%></td>
                    </tr>
                </table>                                   
                <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4">    
                <tr class="tblTitulo" align="center">
                    <td >C�digo</td>
                    <td width="130">Fecha ETA</td>
                    <td width="130">Fecha SIA</td>                    
                    <td >Cliente</td>
					<td width="80">Llegada CDR</td>                    
					<td width="80">Salida CDR</td>
                    <td >Usuario Reporte</td>
                    <td >Fecha Reporte</td>
                    <td >Reporte</td>
                </tr>
				<%
				String action="";
				String llegada = "";
				String salida  = "";
				String fecha_reporte ="";
				for(int i=0; i<v.size(); i++){
					Hashtable cab = (Hashtable)v.get( i );
					fecha_reporte  = ( (String)cab.get( "fecha_genero" )).equals("0099-01-01")? "" : (String)cab.get( "fecha_genero" );
					llegada = ((String)cab.get( "fecha_llegada_cdr" )).equals("0099-01-01")? "" : (String)cab.get( "fecha_llegada_cdr" );
					salida  = ((String)cab.get( "fecha_salida_cdr" )).equals("0099-01-01")? "" : (String)cab.get( "fecha_salida_cdr" );
					//action = "location.replace('"+CONTROLLER+"?estado=Reporte&accion=MateriaPrima&impo="+(String)cab.get( "documento" )+"&cliente="+(String)cab.get( "codcli" )+"&tipodoc="+(String)cab.get( "tipo_doc" )+"&tipo=','myWindow','width=650,height=550,resizable=yes,status=yes')";
					%>
					<tr class="<%=i%2==0?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)' style="cursor:hand" title="Click para Modificar" align="center">    
						<td class="bordereporte"><%=(String)cab.get( "documento" )%>&nbsp;</td>    
						<td class="bordereporte"><%=(String)cab.get( "fecha_eta" )%>&nbsp;</td>
						<td class="bordereporte"><%=(String)cab.get( "fecha_sia" )%>&nbsp;</td>
						<td class="bordereporte"><%=(String)cab.get( "nombrecliente" )%>&nbsp;</td>
						<td class="bordereporte"><%=llegada%>&nbsp;</td>
						<td class="bordereporte"><%=salida%>&nbsp;</td>
						<td class="bordereporte"><%=(String)cab.get( "usuario_genero" )%>&nbsp;</td>
						<td class="bordereporte"><%=fecha_reporte%>&nbsp;</td>
						<td class="bordereporte"><input type="checkbox" name="impo<%=i%>" id="impo<%=i%>" value="<%=(String)cab.get( "documento" )%>"></td>
					</tr>            
				<%}%>
			  </table>            
			</td>
		</tr>    
	  </table>                      
	<br>
	<div align="center">                
        <img src="<%=BASEURL%>/images/botones/exportarExcel.gif"  height="21" onMouseOver="botonOver(this);" onClick="reporte('<%=CONTROLLER%>');" onMouseOut="botonOut(this);" style="cursor:hand">
        <img src="<%=BASEURL%>/images/botones/regresar.gif"  height="21" onMouseOver="botonOver(this);" onClick="location.replace('<%=BASEURL%>/jsp/masivo/reportes/ReporteImpoExpoMateriaPrima.jsp');" onMouseOut="botonOut(this);" style="cursor:hand">
        <img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">            
    </div>
	 </form>       
	<% }else{ %>
	<br>    
	<table border="2" align="center">
		<tr>                
			<td height="45"><table width="410" height="41" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
				<tr>
					<td width="282" height="35" align="center" class="mensajes">Su b�squeda no produjo resultados</td>
					<td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
					<td width="78">&nbsp;</td>
				</tr>
			</table></td>
		</tr>
	</table>  
	<br>
	<div align="center">                        
		<img src="<%=BASEURL%>/images/botones/regresar.gif"  height="21" onMouseOver="botonOver(this);" onClick="location.replace('<%=BASEURL%>/jsp/masivo/reportes/ReporteImpoExpoMateriaPrima.jsp');" onMouseOut="botonOut(this);" style="cursor:hand">
		<img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close();" onMouseOut="botonOut(this);" style="cursor:hand">            
	</div>      
   <%}%>     
</div>
<%=datos[1]%>
</body>
</html>
<script>
function validar(){
    if(form1.impo.value == ""){
        alert("Debe ingresar el c�digo de la Importacion");
        return false;
    }    
    
    return true;
}

function reporte( con ){
    var num ="";
    for( var i=0; i < form1.length; i++ ){
        var ele  = form1.elements[i];      
        if(ele.type=='checkbox'  && ele.checked){
            num+= ele.value + ",";
        }
    }
    if( num == "" ){
        alert("Debe seleccionar por lo menos una Importacion para el reporte");
    }else{
        alert("Ha iniciado la generacion de los reportes, consulte sus Archivos en unos instantes");
        location.replace( con+"?estado=Reporte&accion=MateriaPrima&tipo=&impo="+num );
    }    
}
</script>