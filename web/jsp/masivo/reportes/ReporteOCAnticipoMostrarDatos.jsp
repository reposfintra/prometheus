<!--
- Autor : Ing. Iv�n Dario Devia Acosta
- Date  : 24 de Noviembre de 2006, 3:02 AM 
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Vista que muestra las 
--%>

<%-- Declaracion de librerias--%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>


<html>
<head>
<title>Reporte OC con Anticipo sin reporte en tr�fico </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type='text/javascript' src="<%=BASEURL%>/js/reporte.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/script.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
    <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte de OC con Anticipos sin Tr�fico"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px;"> 
 <% String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 10;
    int maxIndexPages = 10;
    UtilFinanzas u = new UtilFinanzas();
    List Listado = model.reporteOcAnt.getList();
    String fechaini = ( request.getParameter("fechaini")!=null )?request.getParameter("fechaini"):"";
    String fechafin   = ( request.getParameter("fechafin")!=null)?request.getParameter("fechafin"):"";
                
    if(Listado!=null && Listado.size()>0) { %> 
  
<form name="form2" method="post" action="<%=CONTROLLER%>?estado=ReporteOC&accion=Anticipo" >  

<table width="4000" border="2" align="center">
  <tr>
    <td>  
      <table width="100%" border="0" align="center">
	     <tr>
   		<td width="3000"  height="24"  class="subtitulo1">Reporte OC con Anticipo sin reporte en tr�fico</td>
            	<td width="900"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
            </tr>
      </table>			
      <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4" id="principal">
        <tr class="tblTitulo" align="center">
              <td width="100" align="center" title="Agencia generadora OC">Ag. Gen. OC</td>
              <td width="100" align="center" title="Usuario generador OC">Usr. Gen. OC</td>
              <td width="100" align="center" title="OC: Numero de planilla">OC.</td>    				
              <td width="100" align="center" title="Contador de planillas ">Cont. OC.</td>    				
				<td width="100" align="center" title="Valor de la planilla">Valor OC</td>    				
				<td width="100" align="center" title="Identificaci�n del vehiculo">Placa</td>    				
				<td width="100" align="center" title="Fecha de Generaci�n de la OC">Fecha. Gen. OC</td>    				
				<td width="100" align="center" title="D�as de generada la OC">D�as. Gen. OC</td>    				
				<td width="100" align="center" title="Fecha de impresi�n de OC">Fecha. Imp. OC</td>    				
				<td width="100" align="center" title="Fecha generac�on anticipo">Fecha. Gen. Ant.</td>
				<td width="100" align="center" title="Valor anticipo">Anticipo</td>
				<td width="100" align="center" title="Numero del Cheque del anticipo">No. Cheq. Ant.</td>
				<td width="100" align="center" title="Fecha de impresion Cheque de anticipo">Fecha. Imp. Cheq. Ant.</td>
				<td width="100" align="center" title="Ruta">Ruta</td>
				<td width="100" align="center" title="Agencia origen del despacho">Agen. Orig.</td>
				<td width="100" align="center" title="Agencia destino del despacho">Agen. Dest.</td>
				<td width="100" align="center" title="Numero puestos de control">No. psto. Ctrl.</td>
				<td width="100" align="center" title="OT: Numero de remesa">OT.</td>
				<td width="100" align="center" title="Valor de la remes">Valor OT.</td>
				<td width="100" align="center" title="Descripci�n OT">Desc. OT.</td>
				<td width="100" align="center" title="Cliente">Cliente</td>
				<td width="100" align="center" title="Numero Factura Cliente">Fact Cliente</td>
				<td width="100" align="center" title="Agencia que genera el anticipo">Ag. Gen. Ant.</td>
				<td width="100" align="center" title="Cedula Generador Anticipo">Ced. Gen. Ant.</td>
				<td width="100" align="center" title="Generador Anticipo">Gen. Ant.</td>
				<td width="100" align="center" title="Valor Flete">Valor. Ant.</td>
				<td width="100" align="center" title="Fecha cumplido OC">Fecha. Cumpl. OC</td>
				<td width="100" align="center" title="Cedula Usuario cumplido ">Ced. Usr. Cumpl.</td>
				<td width="100" align="center" title="Cumplida por">Cumplida por</td>
				<td width="100" align="center" title="Tipo Viaje">Tipo Viaje</td>
				<td width="100" align="center" title="Discrepancia">Dcrpcia</td>
				<td width="100" align="center" title="Otras OC asociadas">Otras OC Asoc.</td>
				<td width="100" align="center" title="Clasificac�on">Clfcion</td>
				<td width="100" align="center" title="Rango Demora">Rango Demora</td>
				<td width="100" align="center" title="Calificacion">Calificaci�n</td>
		
              
              
              
              
			
        </tr>
        <pg:pager
            items="<%=Listado.size()%>"
            index="<%= index %>"
            maxPageItems="<%= maxPageItems %>"
            maxIndexPages="<%= maxIndexPages %>"
            isOffset="<%= true %>"
            export="offset,currentPageNumber=pageNumber"
            scope="request">
            <%
            for (int i = offset.intValue(), l = Math.min(i + maxPageItems, Listado.size()); i < l; i++) {
                ReporteOCAnticipo rp = (ReporteOCAnticipo) Listado.get(i);%>
        <pg:item>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
        
                                                                                                        
              <td  class="bordereporte" width='52'  align="center" title="Agencia generadora OC"> <%=(rp.getCiudad()!=null)? rp.getCiudad():""%></td>    				
              <td  class="bordereporte" width='84' align="center" title="Usuario generador OC"><% if( !rp.getDespachador().equals("")){%> <%=rp.getDespachador()%>  <%}else{%>&nbsp;<%}%></td>    				
              <td  class="bordereporte" width='80' align="center" title="OC: Numero planilla"><% if( !rp.getNumpla().equals("")){%> <%=rp.getNumpla()%>  <%}else{%>&nbsp;<%}%></td>    				
              <td  class="bordereporte" width='114' align="center" title="Contador de planillas "><%=(rp.getContaroc()!=null)? rp.getContaroc():""%></td>    				
              <td  class="bordereporte" width='92' align="center" title="Valor de la planilla"><%=(rp.getVlrpla2()!=null)? u.customFormat(rp.getVlrpla2()):""%></td>							
              <td  class="bordereporte" width='81' align="center" title="Placa vehiculo"><% if( !rp.getPlatlr().equals("")){%> <%=rp.getPlatlr()%>  <%}else{%>&nbsp;<%}%></td>    				
              <td  class="bordereporte" width='124' align="center" title="Fecha Generaci�n de OC"><% if( !rp.getCreation_date().equals("")){%> <%=rp.getCreation_date()%>  <%}else{%>&nbsp;<%}%></td>    				
              <td  class="bordereporte" width='66' align="center" title="D�as de generada la OC"><% if( !rp.getDif().equals("")){%> <%=rp.getDif()%>  <%}else{%>&nbsp;<%}%></td>    				
              <td  class="bordereporte" width='124' align="center" title="Fecha impresi�n OC"><% if( !rp.getPrinter_date().equals("")){%> <%=rp.getPrinter_date()%>  <%}else{%>&nbsp;<%}%></td>    				
              <td  class="bordereporte" width='122' align="center" title="Fecha generac�on anticipo"><% if( !rp.getFechaanticipo().equals("")){%> <%=rp.getFechaanticipo()%>  <%}else{%>&nbsp;<%}%></td>
              <td  class="bordereporte" width='101' align="center" title="Valor anticipo"><% if( !rp.getVlr().equals("")){%> <%=u.customFormat(rp.getVlr())%>  <%}else{%>&nbsp;<%}%></td>
              <td  class="bordereporte" width='79' align="center" title="Numero Cheque anticipo"><% if( !rp.getDocument().equals("")){%> <%=rp.getDocument()%>  <%}else{%>&nbsp;<%}%></td>
              <td  class="bordereporte" width='124' align="center" title="Fecha impresion Cheque anticipo"><% if( !rp.getFecha_cheque().equals("")){%> <%=rp.getFecha_cheque()%>  <%}else{%>&nbsp;<%}%></td>
              <td  class="bordereporte" width='91' align="center" title="Ruta"><% if( !rp.getRuta_pla().equals("")){%> <%=rp.getRuta_pla()%>  <%}else{%>&nbsp;<%}%></td>
              <td  class="bordereporte" width='101' align="center" title="Agencia origen del despacho"><% if( !rp.getOrigenoc().equals("")){%> <%=rp.getOrigenoc()%>  <%}else{%>&nbsp;<%}%></td>
              <td  class="bordereporte" width='100' align="center" title="Agencia destino del despacho"><% if( !rp.getDestinooc().equals("")){%> <%=rp.getDestinooc()%>  <%}else{%>&nbsp;<%}%></td>
              <td  class="bordereporte" width='80' align="center" title="Numero puestos de control"><% if( !rp.getPuestocontrol().equals("")){%> <%=rp.getPuestocontrol()%>  <%}else{%>&nbsp;<%}%></td>
              <td  class="bordereporte" width='91' align="center" title="OT: Numero remesa"><% if( !rp.getNumrem().equals("")){%> <%=rp.getNumrem()%>  <%}else{%>&nbsp;<%}%></td>
              <td  class="bordereporte" width='100' align="center" title="Valor de remesa"><% if( !rp.getVlrrem2().equals("")){%> <%=u.customFormat(rp.getVlrrem2())%>  <%}else{%>&nbsp;<%}%></td>
              <td  class="bordereporte" width='217' align="center" title="Descripci�n OT"><% if( !rp.getCliente().equals("")){%> <%=rp.getCliente()%>  <%}else{%>&nbsp;<%}%> - <% if( !rp.getDescripcion_via().equals("")){%> <%=rp.getDescripcion_via()%>  <%}else{%>&nbsp;<%}%> - <% if( !rp.getProducto().equals("")){%> <%=rp.getProducto()%>  <%}else{%>&nbsp;<%}%></td>
              <td  class="bordereporte" width='161' align="center" title="Cliente"><% if( !rp.getCliente().equals("")){%> <%=rp.getCliente()%>  <%}else{%>&nbsp;<%}%></td>
              <td  class="bordereporte" width='77' align="center" title="Numero Factura Cliente"><% if( !rp.getFacturacliente().equals("")){%> <%=rp.getFacturacliente()%>  <%}else{%>&nbsp;<%}%></td>
              <td  class="bordereporte" width='97' align="center" title="Agencia que genera anticipo"><% if( !rp.getAgenciaanticipo().equals("")){%> <%=rp.getAgenciaanticipo()%>  <%}else{%>&nbsp;<%}%></td>
              <td  class="bordereporte" width='95' align="center" title="Cedula Generador Anticipo"><% if( !rp.getCedulaanticipo().equals("")){%> <%=rp.getCedulaanticipo()%>  <%}else{%>&nbsp;<%}%></td>
              <td  class="bordereporte" width='103' align="center" title="Generador Anticipo"><% if( !rp.getNombreanticipo().equals("")){%> <%=rp.getNombreanticipo()%>  <%}else{%>&nbsp;<%}%></td>
              <td  class="bordereporte" width='91' align="center" title="Valor Flete"><% if( !rp.getValorflete().equals("")){%> <%=u.customFormat(rp.getValorflete())%>  <%}else{%>&nbsp;<%}%></td>
              <td  class="bordereporte" width='101' align="center" title="Fecha cumplido OC"><% if( !rp.getFechacumplido().equals("")){%> <%=rp.getFechacumplido()%>  <%}else{%>&nbsp;<%}%></td>
              <td  class="bordereporte" width='87' align="center" title="Cedula Usuario cumplido "><% if( !rp.getCedulacumplido().equals("")){%> <%=rp.getCedulacumplido()%>  <%}else{%>&nbsp;<%}%></td>
              <td  class="bordereporte" width='122' align="center" title="Cumplida por"><% if( !rp.getNombrecumplido().equals("")){%> <%=rp.getNombrecumplido()%>  <%}else{%>&nbsp;<%}%></td>
              <td  class="bordereporte" width='46' align="center" title="Tipo Viaje"><% if( !rp.getTipo_viaje().equals("")){%> <%=rp.getTipo_viaje()%>  <%}else{%>&nbsp;<%}%></td>
              <td  class="bordereporte" width='48' align="center" title="Discrepancia"><% if( !rp.getDiscrepancia().equals("")){%> <%=rp.getDiscrepancia()%>  <%}else{%>&nbsp;<%}%></td>
              <td  class="bordereporte" width='533' align="center" title="Otras OC asociadas"><% if( !rp.getOcasociadas().equals("")){%> <%=rp.getOcasociadas()%>  <%}else{%>&nbsp;<%}%></td>
              <td  class="bordereporte" width='57' align="center" title="Clasificac�on"><% if( !rp.getClasificacion().equals("")){%> <%=rp.getClasificacion()%>  <%}else{%>&nbsp;<%}%></td>
              <td  class="bordereporte" width='139' align="center" title="Rango Demora"><%=(rp.getRango()!=null)? rp.getRango():""%></td>
              <td  class="bordereporte" width='139' align="center" title="Calificacion"><%=(rp.getCalificacion()!=null)? rp.getCalificacion():""%></td>
	</tr>
        </pg:item>
          <%}%>
        <tr class="bordereporte">
          <td height="65" colspan="20" align="left" class="bordereporte">
            <div style=" width:30% ">
                <pg:index>
                <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>        
                </pg:index>
            </div>
          </td>
        </tr>
        </pg:pager>
      </table>
    </td>

				
   </tr>
</table>


<input type=hidden id='Opcion' name='Opcion' value=''> 
 <input type=hidden id='fechaini' name='fechaini' value='<%=fechaini%>'> 
 <input type=hidden id='fechafin' name='fechafin' value='<%=fechafin%>'> 
<table width="1800" border="0" align="center">
    <tr>
      <td align="left" width="9%"><img src="<%=BASEURL%>/images/botones/regresar.gif"      name="imgaceptar" onclick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/masivo/reportes/ReporteOCAnticipoSinTrafico&pagina=ReporteOCAnticipo.jsp&marco=no'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
      <td align="left" width="91%"><img src="<%=BASEURL%>/images/botones/exportarExcel.gif"  name="imgaceptar" onclick="Opcion.value = 'Excel'; form2.submit();"  style="cursor:hand"></td>
    </tr>
</table>

  <%}
    else{
        String mensaje = "No se generan datos en este rango de fechas";%>
        <p>
            <table border="2" align="center">
                <tr>
                    <td>
                        <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                            <tr>
                                <td width="229" align="center" class="mensajes"><%=mensaje%></td>
                                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="58">&nbsp;</td>
                            </tr>
                        </table>
                        <table width="50%" border="0" align="center">
                            <tr>
                                <td align="left" width="9%"><img src="<%=BASEURL%>/images/botones/regresar.gif"      name="imgaceptar" onclick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/masivo/reportes/ReporteOCAnticipoSinTrafico&pagina=ReporteOCAnticipo.jsp&marco=no'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </p>

  <%}%>	
</form>	
</div> 
</body>
</html>


