<!--
- Autor : Ing. Juan Manuel Escandon Perez
- Date  : Noviembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra el reporte de viaje
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>Reporte de Viaje</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'> 
<link href="<%= BASEURL %>/css/estilomenuemergente.css" rel='stylesheet'> 
<link href="../../trafico/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body onResize="redimensionar()" onLoad="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte Viaje"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%
	List Listado = model.ReporteViajeSvc.getListRV();
	String fechai = request.getParameter("fechai");
	String fechaf = request.getParameter("fechaf");
%>
<%  if(Listado!=null && Listado.size()>0) { %>   
<table width="1400" border="2" align="center">
  <tr>
    <td>  
      <table width="100%" border="0" align="center">
			<tr>
   				<td width="313"  height="24"  class="subtitulo1"><p align="left">Listado General</p></td>
            	<td width="865"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>						
            </tr>
	  </table>			
	  <table width="100%" border="0">
        <tr>
          <td><table width="98.6%" border="1" align="left" bordercolor="#999999" bgcolor="#F7F5F4">
            <tr class="tblTitulo" align="center">
              <td width="8%" nowrap>Placa</td>
              <td width="15%" nowrap>Descarque</td>
              <td width="8%" nowrap>#Planilla </td>
              <td width="8%" nowrap>#Remesa </td>
              <td width="8%" nowrap>Remision</td>
              <td width="5%" nowrap>Peso </td>
              <td width="5%" nowrap>Unidad</td>
              <td width="10%" nowrap>Origen</td>
              <td width="10%" nowrap>Destino</td>
              <td width="30%" nowrap>Standartjob </td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td> <div  style=" overflow:auto ; WIDTH: 100%; HEIGHT:400px; "><table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4" id="principal">
        
		<% int Cont = 1;
    	Iterator it = Listado.iterator();
        while(it.hasNext()){
			ReporteViaje dat  = (ReporteViaje) it.next();
			String estilo =  (Cont++ % 2 == 0 ) ? "filaazul" : "filagris";
			
     %>
        <tr class="<%=estilo%>">
          <td width="8%" align="center" class="bordereporte"><%=dat.getPlaca()%></td>
          <td width="15%"  align="center" class="bordereporte"><%=(dat.getFecdescargue() != null) ? dat.getFecdescargue() : ""%></td>
          <td width="8%"  align="center" class="bordereporte"><%=(dat.getNumpla() != null) ? dat.getNumpla() : ""%></td>
          <td width="8%"  align="center" class="bordereporte"><%=(dat.getNumrem()!=null) ? dat.getNumrem() : ""%></td>
          <td width="8%"  align="center" class="bordereporte"><%=(dat.getRemision()!=null) ? dat.getRemision() : ""%></td>
          <td width="5%"  align="center" class="bordereporte"><%=dat.getPesoreal()%></td>
          <td width="5%"  align="center" class="bordereporte"><%=(dat.getUnidad()!= null) ? dat.getUnidad() : ""%></td>
          <td width="10%"  align="center" nowrap class="bordereporte"><%=(dat.getOrigen()!= null) ? dat.getOrigen() : ""%></td>
          <td width="10%"  align="center" nowrap class="bordereporte"><%=(dat.getDestino()!= null) ? dat.getDestino() : ""%></td>
          <td width="30%"  align="center" class="bordereporte"><%=(dat.getDescripcion() != null) ? dat.getDescripcion() : ""%></td>
        </tr>
        <%}%>
      </table></div></td>
        </tr>
      </table>	  
    </td>
  </tr>
</table>

<table width="1200" border="0" align="center">
    <tr>
      <td align="left" width="50%"><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgaceptar" onclick="history.back();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
	  <td align="left" width="50%"><img src="<%=BASEURL%>/images/botones/iconos/excel.jpg"  name="imgaceptar" onclick="window.location='<%= CONTROLLER %>?estado=Reporte&accion=Viaje&finicial=<%=fechai%>&ffinal=<%=fechaf%>&Opcion=Excel'"  style="cursor:hand" height="30"></td>
    </tr>
</table> 
<%}else{
 String mensaje = "No hay datos para estos parametros";%>
 <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=mensaje%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
<table width="1200" border="0" align="center">
 <tr>
      <td align="left" width="50%"><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgaceptar" onclick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/masivo/reportes&pagina=InicialReporte.jsp&titulo=Reporte de Viajes&marco=no'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>	  
    </tr>
</table> 
 <%}%>



</div>
</body>
</html>