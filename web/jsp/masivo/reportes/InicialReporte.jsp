<!--
- Autor : Ing. Juan Manuel Escandon Perez
- Date  : Noviembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra los datos de entrada para 
--              la inicializacion del Reporte de Viaje
--%>

<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<% List	ListaAgencias = model.ciudadService.ListarAgencias();%>
<html>
<head>
<tsp:InitCalendar/>
    <title>Reporte de Viajes por Placa</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script src='<%= BASEURL %>/js/validar.js'></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
	<script>
		function formato2digitos( num ){
    		return (num<10? '0':'') + num;
  		}

		function formato (fecha){
			return fecha.getYear() + '-' + formato2digitos(fecha.getMonth()+1) + '-' + formato2digitos(fecha.getDate());
		}
		
		/*funcion anterior
		function cargar(){
  			var fechaActual  =  new Date();
  			var fechaMenos30 =  new Date();
  			fechaMenos30.setDate(fechaActual.getDate()-30);
			form1.finicial.value = formato(fechaMenos30);
			form1.ffinal.value = formato(fechaActual);
		}*/
	    function cargar(){
  			var fechaActual  =  new Date();
  			var fechaInicial =  new Date();
			form1.finicial.value = formato(fechaInicial).substr(0,8)+'01';
			form1.ffinal.value = formato(fechaActual);
		}

		
	</script>
</head>
<body onResize="redimensionar()" onLoad="redimensionar();cargar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte de Viajes por Placa"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
    <form name="form1" method="post" action="<%= CONTROLLER %>?estado=Reporte&accion=Viaje">
        <table width="60%" border="2" align="center">
			<tr>
				<td>
					<table width="99%" height="34" border="0" align="center" class="tablaInferior">
            			<tr>
							<td width="190" height="24"  class="subtitulo1"><p align="left">Parametros de Busqueda</p></td>
            				<td width="404"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
							<input name="Opcion" type="hidden" id="Opcion">
        				</tr>     						
						<tr class="fila">
							<td align="left"class="letra_resaltada">&nbsp;&nbsp;FECHA INICIAL</td>
							<td align="left">&nbsp;&nbsp;<input type='text' id="finicial" name="finicial" readonly="true" class="textbox"></input> 														
														<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.finicial);return false;" HIDEFOCUS>
															<img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha">
														</a>
							</td>
						</tr>
						<tr class="fila">
							<td align="left"class="letra_resaltada">&nbsp;&nbsp;FECHA FINAL</td>
							<td align="left">&nbsp;&nbsp;<input type='text' id="ffinal" name="ffinal" readonly="true" class="textbox"></input> 														
														<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.ffinal);return false;" HIDEFOCUS>
															<img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha">
														</a>
							</td>
						</tr>
						<tr class="fila">
							<td class="letra_resaltada" align="left">&nbsp;&nbsp;PLACA</td>
							<td align="left">&nbsp;&nbsp;<input type="text" id="placa" <input name="placa" class="textbox" maxlength="6" onKeyPress="soloAlfa(event)"></td>            
					  	</tr>    
					</td>        
				</tr>
        	</table>
		</table>
        <table align="center">
          <tr align="center">
            <td colspan="2"> <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="form1.Opcion.value='Generar';if(Validar(form1)){form1.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> </td>
          </tr>
        </table>
    </form>
	</div>
	<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
<script>
function Validar(form){
	if( form.finicial.value > form.ffinal.value ){
		alert('La fecha incial debe ser menor a la fecha final de busqueda');
		return false;
	}
	if(form.placa.value == '' ){
		alert('Debe ingresar una placa');
		return false;
	}
	return true;
}
</script>
</html>
