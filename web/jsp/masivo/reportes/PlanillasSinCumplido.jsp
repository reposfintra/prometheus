<!--
- Autor : Ing. David Velasquez Gonzalez
- Modified: Ing. Enrique De Lavalle R.
- Creation Date  : Noviembre de 2006
- Modified Date: 16 Marzo de 2007
- Copyrigth Notice : Fintravalores S.A. S.A
-->

<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>Reporte de Planillas Sin Cumplido</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'> 
<link href="<%= BASEURL %>/css/estilomenuemergente.css" rel='stylesheet'> 
<link href="../../trafico/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>

</head>
<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte Planillas Sin Cumplido"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%  String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 15;
    int maxIndexPages = 10;
	
	UtilFinanzas u = new UtilFinanzas();
	
	List Listado = model.planillaSinCumplidoService.getListPA();
	String fechai = request.getParameter("fechai");
	String fechaf = request.getParameter("fechaf");
	String cliente = (request.getParameter("cliente")!=null)?request.getParameter("cliente"):"%";
    String ciudad = request.getParameter("ciudad");
%>
<%  if(Listado!=null && Listado.size()>0) { %>   
<table width="1800" border="0" align="center">
    <tr>
      <td align="left" width="8%"><img src="<%=BASEURL%>/images/botones/regresar.gif"      name="imgaceptar" onclick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/masivo/reportes&pagina=ParametrosPlanillasSinCumplido.jsp&marco=no'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
      <td align="left" width="8%"><img src="<%=BASEURL%>/images/botones/exportarExcel.gif"  name="imgaceptar" onclick="window.location='<%= CONTROLLER %>?estado=Planillasin&accion=Cumplido&finicial=<%=fechai%>&ffinal=<%=fechaf%>&cliente=<%=cliente%>&Opcion=Excel'" style="cursor:hand"></td>
	  <td align="left" width="84%"><img src="<%=BASEURL%>/images/botones/salir.gif"   name="mod"  height="21"  onclick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
	  
    </tr>
</table>
<table width="3000" border="2" align="center">
  <tr>
    <td>  
      <table width="100%" border="0" align="center">
        <tr>
   			<td width="313"  height="24"  class="subtitulo1"><p align="left">Reporte Planillas Sin Cumplido</p></td>
            <td width="865"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>						
        </tr>
	  </table>			
		  <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4" id="principal">
        <tr class="tblTitulo" align="center">
             <%--1--%><td nowrap width="3%">Planilla</td>
             <%--2--%><td nowrap width="3%" >Generador Oc</td>
             <%--3--%><td nowrap width="4%" >OT</td>
             <%--4--%><td nowrap width="4%" >Valor OT</td>
             <%--5--%><td nowrap width="4%" >Fecha Generaci�n</td>
			 <%--6--%><td nowrap width="4%" >Hora Generaci�n</td> 		            
             <%--7--%><td nowrap width="4%" >Fecha Entrega</td>
			 <%--8--%><td nowrap width="4%" >Hora Entrega</td>
			 <%--9--%><td nowrap width="3%" >D�as Gen</td>
			 <%--10--%><td nowrap width="3%" >Placa</td>
             <%--11--%><td nowrap width="5%" >Sede Pago</td>
             <%--12--%><td nowrap width="3%" >Origen</td>
             <%--13--%><td nowrap width="3%" >Destino</td>
             <%--14--%><td nowrap width="5%" >Agencia De Cumplido</td>
             <%--15--%><td nowrap width="3%" >Tipo de Viaje</td>
             <%--16--%><td nowrap width="5%" >Cliente</td>
             <%--17--%><td nowrap width="3%" >Codigo Cliente</td>
             <%--18--%><td nowrap width="4%" >No Factura</td>
             <%--19--%><td nowrap width="5%" >Valor Factura</td>
             <%--20--%><td nowrap width="4%" >Anticipo</td>
             <%--21--%><td nowrap width="3%" >Fecha Anticipo</td>
			 <%--21--%><td nowrap width="3%" >Hora Anticipo</td>
             <%--22--%><td nowrap width="5%" >Valor OC</td>
             <%--23--%><td nowrap width="2%" >Estado</td>
             <%--24--%><td nowrap width="2%" >Dias</td>
			 <%--25--%><td nowrap width="2%" >Cont. OC</td>			 
             <%--26--%><td nowrap width="3%" >Clasificacion</td>
             <%--27--%><td nowrap width="3%" >Calificacion de OC</td>
			
            </tr>
	<pg:pager
        items="<%=Listado.size()%>"
        index="<%= index %>"
        maxPageItems="<%= maxPageItems %>"
        maxIndexPages="<%= maxIndexPages %>"
        isOffset="<%= true %>"
        export="offset,currentPageNumber=pageNumber"
        scope="request">
          <%-- keep track of preference --%>
          <%
        for (int i = offset.intValue(), l = Math.min(i + maxPageItems, Listado.size()); i < l; i++) {
            PlanillaSinCumplido dat  = (PlanillaSinCumplido) Listado.get(i);%>
     <pg:item>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
		  <%--1--%><td nowrap width="3%" align="center" class="bordereporte"><%=(dat.getNumpla()!=null)?dat.getNumpla() : ""%></td>
		  <%--2--%><td nowrap width="3%"  align="center" class="bordereporte"><%=(dat.getGenerador_oc()!=null)?dat.getGenerador_oc() : ""%></td>
		  <%--3--%><td nowrap width="4%"  align="center" class="bordereporte"><%=(dat.getOt()!=null)?dat.getOt() : ""%></td>
		  <%--4--%><td nowrap width="4%"  align="center" class="bordereporte"><%=u.customFormat(dat.getValor_ot())%></td>
		  
		  <% String fechaGen = (dat.getFecha_gen()!=null)?dat.getFecha_gen():"" ;
         	 String fechaEnt = (dat.getFecha_ent()!=null)?dat.getFecha_ent():""; 	 
			 if (dat.getFecha_gen()!=null){%>
		  		<%--5--%><td nowrap width="4%"  align="center" class="bordereporte"><%=fechaGen.substring(0,10)%></td>
				<%--6--%><td nowrap width="4%"  align="center" class="bordereporte"><%=fechaGen.substring(11,19)%></td>					
		  <%}else{%>
			 	<%--5--%><td nowrap width="4%"  align="center" class="bordereporte">&nbsp;</td>
			 	<%--6--%><td nowrap width="4%"  align="center" class="bordereporte">&nbsp;</td> 
		  <%}%>
		  <% if(dat.getFecha_ent()!=null){  %>
		  		<%--7--%><td nowrap width="4%"  align="center" class="bordereporte"><%=fechaEnt.substring(0,10)%></td>
		  		<%--8--%><td nowrap width="4%"  align="center" class="bordereporte"><%=fechaEnt.substring(11,19)%></td>
		  <%}else{%>
			    <%--7--%><td nowrap width="4%"  align="center" class="bordereporte">&nbsp;</td>
			    <%--8--%><td nowrap width="4%"  align="center" class="bordereporte">&nbsp;</td>
		  <%}%>
		  <%--9--%><td nowrap width="3%"  align="center" class="bordereporte"><%=dat.getDiasDeGen()%></td>		   
		  <%--10--%><td nowrap width="3%"  align="center" class="bordereporte"><%=(dat.getPlaca()!=null)?dat.getPlaca() : ""%></td>
		  <%--11--%><td nowrap width="5%"  align="center" class="bordereporte"><%=(dat.getSede()!=null)?dat.getSede() : ""%></td>
		  <%--12--%><td nowrap width="3%"  align="center" class="bordereporte"><%=(dat.getOrigen()!=null)?dat.getOrigen() : ""%></td>
		  <%--13--%><td nowrap width="3%"  align="center" class="bordereporte"><%=(dat.getDestino()!=null)?dat.getDestino() : ""%></td>
		  <%--14--%><td nowrap width="5%"  align="center" class="bordereporte"><%=(dat.getAgencia_dest()!=null)?dat.getAgencia_dest() : "" %></td>
		  <%--15--%><td nowrap width="3%"  align="center" class="bordereporte"><%=(dat.getTipoviaje()!=null)?dat.getTipoviaje() : ""%></td>
		  <%--16--%><td nowrap width="5%"  align="center" class="bordereporte"><%=(dat.getCliente()!=null)?dat.getCliente() : ""%></td>
		  <%--17--%><td nowrap width="3%"  align="center" class="bordereporte"><%=(dat.getCod_cliente()!=null)?dat.getCod_cliente() : ""%></td>
		  <%--18--%><td nowrap width="4%"  align="center" class="bordereporte"><%=(dat.getNo_factura()!=null)?dat.getNo_factura() : ""%></td>
		  <%--19--%><td nowrap width="5%"  align="center" class="bordereporte"><%=(dat.getNo_factura()!=null)?u.customFormat(dat.getValor_fact()): ""%></td>
		  <%--20--%><td nowrap width="4%"  align="center" class="bordereporte"><%=u.customFormat(dat.getAnticipo())%></td>
		  <%String fechaAnt = (dat.getFecha_anticipo()!=null)?dat.getFecha_anticipo():"";
		     if(!dat.getFecha_anticipo().equals("")){%>
			 	<%--21--%><td nowrap width="3%"  align="center" class="bordereporte"><%=fechaAnt.substring(0,10)%></td>
				<%--21--%><td nowrap width="3%"  align="center" class="bordereporte"><%=fechaAnt.substring(11,19)%></td>
			<%}else{%>
			   <%--21--%><td nowrap width="4%"  align="center" class="bordereporte">&nbsp;</td>		
			   <%--21--%><td nowrap width="4%"  align="center" class="bordereporte">&nbsp;</td>	
		  <%}%>	   		  
		  <%--22--%><td nowrap align="center" class="bordereporte"><%=u.customFormat(dat.getValor_oc())%></td>
		  <%--23--%><td nowrap width="2%" align="center" class="bordereporte"><%=(dat.getVacio()!=null)?dat.getVacio() : ""%></td>
		  <%--24--%><td nowrap align="center" rap class="bordereporte"><%=dat.getDias()%></td>
		  <%if(dat.getContaroc()>0){%>
		  		<%--25--%><td nowrap align="center" class="bordereporte"><%=dat.getContaroc()%></td>
		  <%}else{%>
		  		<%--25--%><td nowrap align="center" class="bordereporte">&nbsp;</td>
		  <%}%>		
		  <%--26--%><td nowrap align="center" class="bordereporte"><%=(dat.getClasificacion()!=null)?dat.getClasificacion() : ""%></td>
		  <%--27--%><td nowrap align="center" class="bordereporte"><%=dat.getCalificacion()%></td>
          
		</pg:item>
		<%}%>
        <tr class="bordereporte">
          <td height="65" colspan="28" align="left" class="bordereporte">
		  <div style=" width:30% "><pg:index>
                <jsp:include page="../../../WEB-INF/jsp/googlesinimg.jsp" flush="true"/>        
            </pg:index></div></td>
          </tr>
        </pg:pager>
      </table>	  
    </td>
  </tr>
</table>

<table width="1800" border="0" align="center">
    <tr>
      <td align="left" width="8%"><img src="<%=BASEURL%>/images/botones/regresar.gif"      name="imgaceptar" onclick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/masivo/reportes&pagina=ParametrosPlanillasSinCumplido.jsp&marco=no'"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
      <td align="left" width="8%"><img src="<%=BASEURL%>/images/botones/exportarExcel.gif"  name="imgaceptar" onclick="window.location='<%= CONTROLLER %>?estado=Planillasin&accion=Cumplido&finicial=<%=fechai%>&ffinal=<%=fechaf%>&cliente=<%=cliente%>&Opcion=Excel'"  style="cursor:hand"></td>
	  <td align="left" width="84%"><img src="<%=BASEURL%>/images/botones/salir.gif"   name="mod"  height="21"  onclick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
	  
    </tr>
</table>
<%}else{
 String mensaje = "No hay datos para estos parametros";%>
 <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=mensaje%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
<table width="1800" border="0" align="center">
 <tr>
      <td align="left" width="50%"><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgaceptar" onclick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/masivo/reportes&pagina=ParametrosPlanillasSinCumplido.jsp&marco=no'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>	  
    </tr>
</table> 
 <%}%>

</div>
</body>
</html>
