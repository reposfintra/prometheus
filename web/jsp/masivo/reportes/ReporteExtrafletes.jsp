<!--
- Autor : Ing. Tito Andr�s Maturana
- Date  : 26 diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la generaci�n del reporte de xtrafletes y costos reembolsables.
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.operation.controller.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
</script>
<title>Reporte de Extrafletes y Costos Reembolsables</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<link href="../../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<script src='<%= BASEURL %>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src='<%=BASEURL%>/js/tools.js' language='javascript'></script>
<script>
function validarForma(){
    for (i = 0; i < forma.elements.length; i++){
            if (forma.elements[i].value == "" && forma.elements[i].name!="clientes" 
					&& forma.elements[i].name!="text" && forma.elements[i].name!="tipo"  && forma.elements[i].name!="document" ){
                    forma.elements[i].focus();
                    alert('No se puede procesar la informaci�n. Campo Vac�o.');
                    return false;	
            }
    }
    return true;
}
</script>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:110px; z-index:0; left: 0px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Reporte de Extrafletes y Costos Reembolsables"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 109px; overflow: scroll;">
<%
	TreeMap clientes = model.clienteService.getTreemap();
	TreeMap docs = model.RemDocSvc.getDocuments();
	clientes.put(" Todos", "");
	docs.put(" Todos", "");
%>
<form name="forma" action='<%=CONTROLLER%>?estado=Reporte&accion=Extrafletes' id="forma" method="post">
  <table width="500"  border="2" align="center" cellpadding="0" cellspacing="0">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr>
          <td colspan="2" ><table width="100%"  border="0" cellpadding="0" cellspacing="1" class="barratitulo">
              <tr>
                <td width="56%" class="subtitulo1">&nbsp;Detalles  del Reporte </td>
                <td width="44%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
          </table></td>
        </tr>
        <tr class="fila">
          <td width="27%" valign="middle">Cliente</td>
          <td width="73%" nowrap>
            <table width="100%"  border="0" class="tablaInferior">
              <tr>
                <td><input name="text" type="text" class="textbox" id="campo" style="width:200;" onKeyUp="buscar(document.forma.clientes,this)" size="15" ></td>
              </tr>
              <tr>
                <td><input:select name="clientes" attributesText="class=textbox" options="<%=clientes %>"/>&nbsp;</td>
              </tr>
            </table></td>
        </tr>
        <tr class="fila">
          <td>Fecha Inicial </td>
          <td nowrap>
            <input name="FechaI" type='text' class="textbox" id="FechaI" style='width:120' value='' readonly>            
            <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma.FechaI);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16"
               border="0" alt="De click aqu&iacute; para escoger la fecha inicial"></a> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
        </tr>
        <tr class="fila">
          <td>Fecha Final </td>
          <td nowrap>
            <input name="FechaF" type='text' class="textbox" id="FechaF" style='width:120' value='' readonly>
            <a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.forma.FechaF);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16"
               border="0" alt="De click aqu&iacute; para escoger la fecha inicial"></a> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
        </tr>
        <tr class="fila">
          <td>Tipo </td>
          <td nowrap><select name="tipo" class="textbox" id="tipo">
            <option value="" selected>Todos</option>
            <option value="R">Costos Reembolsables</option>
            <option value="E">Extraflete</option>
          </select></td>
        </tr>
        <tr class="fila">
          <td>Tipo de Documento</td>
          <td nowrap><input:select name="document" attributesText="class=textbox" options="<%=docs %>" />&nbsp;</td>
        </tr>
        <tr class="fila">
          <td>Incluir Valor Costo </td>
          <td nowrap><input name="costos" type="checkbox" id="costos" value="1" checked></td>
        </tr>
      </table></td>
    </tr>
  </table>
  <br>
  
  <br>
  <center>
    <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="c_aceptar" onClick="if (validarForma()) forma.submit(); " onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
    <img src="<%=BASEURL%>/images/botones/cancelar.gif" name="c_cancelar" onClick="forma.reset();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"> 
    <img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
  </center>
</form>
<% if( request.getParameter("msg") != null ){%>
  <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=request.getParameter("msg").toString()%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
 <%} %>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>
