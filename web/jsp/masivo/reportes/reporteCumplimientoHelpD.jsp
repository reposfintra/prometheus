<%@ include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Descripci&oacute;n de campos Proceso de Mayorizaci&oacute;n</title>

<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
</head>

<body>
<% String BASEIMG = BASEURL +"/images/botones/"; %> 
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">INDICADOR CUMPLIMIENTO COLOCACION </div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">INFORMACION</td>
        </tr>
        <tr>
          <td width="123" class="fila">Fecha Inicial </td>
          <td width="551"  class="ayudaHtmlTexto"> Selecciona del calendario la fecha inicio para el rango. </td>
        </tr>
        <tr>
          <td  class="fila"> Fecha Final </td>
          <td  class="ayudaHtmlTexto"> Selecciona del calendario la fecha final para el rango. </td>
        </tr>
		<tr>
          <td width="123"  class="fila"> Bot&oacute;n Aceptar </td>
          <td width="551"  class="ayudaHtmlTexto">Inicia la generaci&oacute;n del reporte de indicador.</td>
        </tr>
		<tr>
          <td width="123"  class="fila"> Bot&oacute;n Salir </td>
          <td width="551"  class="ayudaHtmlTexto">Cierra la ventana. </td>
        </tr>
		<tr>
          <td width="123"  class="fila"> Bot&oacute;n Cancelar </td>
          <td width="551"  class="ayudaHtmlTexto">Reinicia los valores de las fechas. </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<br>
<table width="416" align="center">
	<tr>
	<td align="center">
		<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:pointer" name="c_salir" onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" align="absmiddle">
	</td>
	</tr>
</table>
<p>&nbsp;</p>

<p>&nbsp;</p>
<p>&nbsp; </p>
</body>
</html>
