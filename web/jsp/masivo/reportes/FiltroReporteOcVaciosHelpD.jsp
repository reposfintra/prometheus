<!--
- Autor : Ing. Enrique De Lavalle.
- Date  : 27 de Noviembre 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Jsp que muestra la ayuda para reporte de Oc de Viajes Vacios con anticipo sin reporte 
--%>
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
    <title>Descripcion de los campos a ingresar en el Reporte Oc de vacios con anticipos sin reporte</title>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <link href="/../../../css/estilostsp.css" rel="stylesheet" type="text/css">  
    <style type="text/css">
<!--
.Estilo3 {font-size: 12px}
-->
    </style>
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
<body> 
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ayuda Descriptiva - Reporte Oc Vacios con anticipo sin reporte"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:90%; z-index:0; left: 0px; top: 100px;"> 
   <p>&nbsp;</p>
   <table width="73%" border="2" align="center" id="tabla1" >
        <td>
         <table width="100%" height="50%" align="center">
              <tr>
                <td class="subtitulo1"><strong>Despacho</strong></td>
                <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
         </table>
         <table width="100%" borderColor="#999999" bgcolor="#F7F5F4">
           <tr>
             <td colspan="3" class="tblTitulo"><strong>Reporte OC de vacios con anticipos sin reporte</strong></td>
		   </tr>
           <tr>
             <td colspan="4" class="subtitulo"><strong>INFORMACION: </strong><strong>Ingreso de datos.</strong></td>
		   </tr>
           <tr>
             <td width="26%" class="fila">Fecha Inicial </td>
			 <td width="21%" class="fila"><img src="../../../../images/ayuda/reporte/Reporte%20OC%20Vacios/fecha.JPG" width="159" height="30"></td>
             <td width="53%"><span class="ayudaHtmlTexto">Campo para escoger la fecha inicial de la busqueda de los reportes  que se desean obtener.Tiene como valor predeterminado la fecha actual menos 30 dias. </span></td>
           </tr>
           <tr>
             <td class="fila">Fecha Final</td>
			 <td width="21%" class="fila"><img src="../../../../images/ayuda/reporte/Reporte%20OC%20Vacios/fechafin.JPG" width="159" height="30"></td>
             <td><span class="ayudaHtmlTexto">Campo para escoger la fecha final de la busqueda de los reportes  que se desean obtener.Tiene como valor predeterminado la fecha actual. </span></td>
           </tr>
           <tr>
             <td class="fila">Bot&oacute;n Aceptar</td>
              <td class="fila"><div align="center"><img src="../../../../images/ayuda/reporte/Reporte%20OC%20Vacios/btnAceptar.JPG" width="94" height="25"></div></td>
             <td width="53%"><span class="ayudaHtmlTexto">Bot&oacute;n para realizar el procedimiento de b&uacute;squeda del Reporte OC de vacios con anticipos sin reporte. </span></td>
           </tr>
           <tr>
             <td class="fila">Bot&oacute;n Salir </td>
			 <td width="21%" class="fila"><div align="center"><img src="../../../../images/ayuda/reporte/Reporte%20OC%20Vacios/btnSalir.JPG" width="88" height="25"></div></td>
             <td><span class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista 'Reporte OC de vacios con anticipos sin reporte.' y volver a la vista del men&uacute;.</span></td>
           </tr>
		    <tr>
             <td colspan="3" class="subtitulo"><strong>INFORMACION: Reporte.</strong></td>
		   </tr>
		     <tr>
             <td class="fila">Bot&oacute;n Regresar </td>
			 <td width="21%" class="fila"><div align="center"><img src="../../../../images/ayuda/reporte/Reporte%20OC%20Vacios/btnRegresar.JPG" width="100" height="23"></div></td>
             <td><span class="ayudaHtmlTexto">Bot&oacute;n para regresar la ventana para ingresar el rango de fechas que genera el reporte. </span></td>
           </tr>
		     <tr>
             <td class="fila">Bot&oacute;n Exportar </td>
			 <td width="21%" class="fila"><div align="center"><img src="../../../../images/ayuda/reporte/Reporte%20OC%20Vacios/btnExportar.JPG" width="103" height="25"></div></td>
             <td><span class="ayudaHtmlTexto">Bot&oacute;n para exportar a un archivo excel los datos arrojados como resultado de la busqueda </span></td>
           </tr>
		     <tr>
             <td class="fila">Bot&oacute;n Salir </td>
			 <td width="21%" class="fila"><div align="center"><img src="../../../../images/ayuda/reporte/Reporte%20OC%20Vacios/btnSalir.JPG" width="88" height="25"></div></td>
             <td><span class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista 'Reporte OC de vacios con anticipos sin reporte.' y volver a la vista del men&uacute;.</span></td>
           </tr>
           <tr>
             <td colspan="2" align="center" class="letra">&nbsp;</td>
           </tr>
         </table></td>
  </table>
		<p></p>
	<center>
	<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	</center>
</div>  
</body>
</html>