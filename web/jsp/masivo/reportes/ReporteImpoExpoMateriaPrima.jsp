<%@page contentType="text/html"%>
<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.finanzas.contab.model.*,com.tsp.finanzas.contab.model.beans.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
        //Se cargan las opciones de transaccion y tipo Cuenta
        TreeMap opc_docs = new TreeMap();
        opc_docs.put("Importación","MPI");
        //opc_docs.put("Exportación","EXP");
        
        String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
        String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
        String mensaje = (String) request.getAttribute("mensaje");    
%>
<html>
    <head>
        <title>Generación de reportes materia prima</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
        <script src='<%=BASEURL%>/js/boton.js'></script>
        <script language="javascript" src="<%=BASEURL%>/js/general.js"></script>        
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte Materia Prima"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
            <input type="hidden" name="baseurl" id="baseurl" value="<%=BASEURL%>" >
            <div id="working" class="letrasFrame" align="right" style="visibility:hidden"><img src="<%=BASEURL%>/images/cargando.gif">&nbsp;Cargando...&nbsp;&nbsp;</div>
            <br>
            <form name="form1" id="form1" method="post" action="<%=CONTROLLER%>?estado=Reporte&accion=MateriaPrima&tipo=ok">
            <table width="500" border="2" align="center">
                <tr>
                    <td>
                    <table width="100%" align="center" cellpadding="3" cellspacing="2" class="tablaInferior">
                    <tr class="fila">
                        <td colspan="2" class="subtitulo1">Reporte Materia Prima</td>
                        <td width="47%" colspan="2" class="barratitulo"><img src="<%=BASEURL%>//images/titulo.gif"align="left"><%=datos[0]%></td>
                    </tr>                    
                    <tr class="fila">
                        <td width="26%" rowspan="2" class="fila"><div align="left">Cliente :</div></td>
                        <td colspan="3">
                            <p><span class="Simulacion_Hiper" style="cursor:hand " onClick="window.open('<%=BASEURL%>/consultas/consultasClientes.jsp','','HEIGHT=200,WIDTH=600,STATUS=YES,SCROLLBARS=YES,RESIZABLE=YES')">Consultar clientes...</span></p>
                        </td>
                    </tr>
                    <tr class="fila">
                    <td colspan="3">                                                                                        
                    <input name="cliente" type="text" class="textbox" id="cliente" value="" onKeyPress="soloAlfa(event);" maxlength="25" >                                                                                                                  
                </td>                    
                </tr>      
				<tr class="fila">
                    <td >Importación : </td>
                    <td colspan="3"><input name="impo" id="impo" type="text" class="textbox" size="12">            
                    </td>
                </tr>          
                <tr class="fila">
                    <td >Tipo documento :</td>
                    <td colspan="3">
                        <input:select name="tipodoc" options="<%= opc_docs %>" attributesText="class='textbox' id='tipodoc' style='width:200'"/>
                    </td>
                </tr> 
				<tr class="fila">
                    <td height="20" colspan="2"><a class="Simulacion_Hiper" style="cursor:hand" onclick="listar('<%=CONTROLLER%>', '1', form1.tipodoc.value);">&nbsp;Ver Importaciones pendientes por generar</a>&nbsp;</td>
					<td colspan="2">
					<input name="inicio" type="text" class="textbox" id="inicio" value='' size="12" readonly>
					<img src="<%=BASEURL%>/images/cal.gif" name="popcal" width="16" height="16" border="0"
					align="top"  style="cursor:hand"
					onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.inicio);return false;" 
					alt="Click para ingresar fecha" HIDEFOCUS>&nbsp;
					<input name="fin" type="text" class="textbox" id="fin" value='' size="12" readonly>
                        <img src="<%=BASEURL%>/images/cal.gif" name="popcal" width="16" height="16" border="0"
                        align="top"  style="cursor:hand"
                        onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.fin);return false;"
                        alt="Click para ingresar fecha" HIDEFOCUS></td>
                    
                </tr>
                <tr class="fila">
                    <td height="20" colspan="4" class="Simulacion_Hiper" style="cursor:hand" onclick="listar('<%=CONTROLLER%>', '2', form1.tipodoc.value);">&nbsp;Ver todas las Importaciones</td>                    
                </tr>                           
            </table>
            </td>
            </tr>
            </table>
            <div align="center"><br>
                <img src="<%= BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="form1.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);"  style="cursor:hand ">&nbsp;                 
                <img src="<%= BASEURL%>/images/botones/salir.gif"  name="imgsalir"  onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);"   style="cursor:hand ">
            </div>            
            </form>
            <br>
            <%if(mensaje!=null){%>
                <table border="2" align="center">
                    <tr>
                        <td height="45"><table width="410" height="41" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                            <tr>
                                <td width="282" height="35" align="center" class="mensajes"><%=mensaje%></td>
                                <td width="28" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="78">&nbsp;</td>
                            </tr>
                        </table></td>
                    </tr>
                </table>
                <br>
            <%}%>
        </div> 
		<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>           
        <%=datos[1]%>
    </body>
</html>
<script>
function listar( controller, opc, tipodoc ){
    if( opc == "1" ){   
		if( form1.fin.value=="" || form1.inicio.value=="" ){
			alert("debe llenar las fechas obligatoriamente");
			return false;
		}
		else if( form1.fin.value < form1.inicio.value ){
			alert("la fecha inicial debe ser menor a la fecha final");
			return false;
		}
		else
        	location.replace( controller+"?estado=Reporte&accion=MateriaPrima&pendiente=ok&tipo=ok&tipodoc="+tipodoc+"&fin="+form1.fin.value+"&inicio="+form1.inicio.value);
    }
    else if( opc == "2" ){   
        location.replace( controller+"?estado=Reporte&accion=MateriaPrima&tipo=ok&tipodoc="+tipodoc);
    }
}
</script>