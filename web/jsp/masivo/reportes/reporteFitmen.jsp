<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
 
<html>
<head>
<title>Reporte Fitmen</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body onresize="redimensionar()" onload = 'redimensionar()'>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Generar Reporte"/>
</div>
<%
	String mensaje = ( request.getParameter("mensaje")==null ) ? "" : request.getParameter("mensaje");
%>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
  <table width="533" border="2" align="center">
  <tr>
    <td><table width="100%" height="73" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
        <tr>
          <td width="322" align="center" class="mensajes">Reporte exportado exitosamente a: <br>
            <%= mensaje %> </td>
          <td width="54" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        </tr>
    </table></td>
  </tr>
</table>
<p>&nbsp;</p>
<table width="536" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr align="left">
    <td><div align="center">
	<img src="<%=BASEURL%>/images/botones/regresar.gif" name="c_regresar" id="c_regresar" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="parent.history.back();">&nbsp;
	<img src="<%=BASEURL%>/images/botones/salir.gif" name="c_salir" id="c_salir" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" onClick="parent.close();">
    </div>
    </tr>
</table>
</div>
</body>
</html>
