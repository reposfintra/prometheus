<%@ page session="true"%>
<%@ page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
<title>Agregar Destinatarios</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script language="javascript" src="<%= BASEURL %>/js/validar.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>

<link href="/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body>
<br>
<form action="" method="post" name="form2" id="form2">
  <table width="100%"  border="2" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td><table width="100%" class="tablaInferior">
      <tr>
        <td colspan="3">
          <table width="100%"  border="0" cellpadding="0" cellspacing="0" class="barratitulo">
            <tr>
              <td width="25%" class="subtitulo1">Destinatarios
                <input name="numrem" type="hidden" id="numrem" value="<%=request.getParameter("numrem")%>">
                  <input name="modif" type="hidden" id="modif" value="<%=request.getParameter("modif")!=null?"1":""%>"></td>
              <td width="75%"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
            </tr>
          </table>
          </td>
      </tr>
      <tr class="fila">
        <td colspan="2"><strong class="Letras">
          <input name="sj" type="hidden" id="sj" value="<%=request.getParameter("sj")%>">
      CIUDAD DE DESTINO </strong> </td>
        <td>
          <%
	  String sj1 = request.getParameter("sj");
	  String cliente1=sj1.substring(0,3);
	  model.remidestService.searchCiudades(cliente1);
	  TreeMap ciudades = model.remidestService.getCiudades(); 
	  String corigen="";
	  if(request.getParameter("origen")!=null){
	  	corigen = request.getParameter("origen");
	  }
	  String onChange = "onChange=\"buscarDestinatarios2('"+BASEURL+"',this.value);\" class='listmenu'";
	  %>
          <input:select name="ciudad" options="<%=ciudades%>" attributesText="<%=onChange%>" default='<%=corigen%>'/> </td>
      </tr>
      <%
	if(request.getParameter("origen")!=null){
		String sj = request.getParameter("sj");
		String cliente=sj.substring(0,3);
     	List lista=model.remidestService.getDestinatarios(cliente,corigen);
  		Iterator it=lista.iterator();
	    int i=0;
  		while (it.hasNext()){
            
            RemiDest rd = (RemiDest) it.next();
            String coddest = rd.getCodigo();
            String nomdest = rd.getNombre();
    	%>
      <tr class="fila">
        <td width="7%">
          <input name="check<%=coddest%>" type="checkbox" id="check<%=coddest%>" value="<%=coddest%>" onClick="onCheckD(this.name,'<%=BASEURL%>');"></td>
        <td width="39%"><span class="Letras"><strong> <%=nomdest%><br>
                <%=coddest%></strong></span></td>
        <td width="54%" align="center"><input name="docui<%=coddest%>" type="hidden" id="docuint<%=coddest%>" size="50">
            <%if(request.getParameter("modif")==null){%>
            <a class="Simulacion_Hiper" onClick="window.open('<%=CONTROLLER%>?estado=RemesaDocto&accion=Aplicar&destinatario=<%=coddest%>&nombre=<%=nomdest%>&generar=ok','')" style="cursor:hand ">AGREGAR DOCUMENTOS RELACIONADOS</a> <br>
            <%}else{%>
            <a class="Simulacion_Hiper" onClick="window.open('<%=BASEURL%>/docremesas/ModificarDocRel.jsp?destinatario=<%=coddest%>&nombre=<%=nomdest%>&numrem=<%=request.getParameter("numrem")%>','','scrollbars=no,status=yes')" style="cursor:hand ">MODIFICAR DOCUMENTOS RELACIONADOS</a>
            <%}%>
        </td>
      </tr>
      <%
	 i++;
	 }
	 }%>
    </table></td>
  </tr>
</table><br>
<center><img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></center>
</form>

</body>
</html>
