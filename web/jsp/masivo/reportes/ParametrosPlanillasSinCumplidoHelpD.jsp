<!--
- Autor : Ing. Enrique De Lavalle.
- Date  : 27 de Noviembre 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Jsp que muestra la ayuda para reporte de Oc de Viajes Vacios con anticipo sin reporte 
--%>
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
    <title>Descripcion de los campos a ingresar en el Reporte de planillas sin cumplido</title>
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css"> 
    <link href="/../../../css/estilostsp.css" rel="stylesheet" type="text/css">  
    
</head>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
<body> 
<% String BASEIMG = BASEURL +"/images/ayuda/reporte/ReportePlanillasSinCumplido/"; %> 
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ayuda Descriptiva - Reporte Reporte de planillas sin cumplido"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:90%; z-index:0; left: 0px; top: 100px;"> 
   <p>&nbsp;</p>
   <table width="74%" border="2" align="center" id="tabla1" >
        <td>
         <table width="100%" height="50%" align="center">
              <tr>
                <td class="subtitulo1"><strong>Reportes</strong></td>
                <td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
              </tr>
         </table>
         <table width="100%" bordercolor="#999999" bgcolor="#F7F5F4">
           <tr>
             <td colspan="3" class="tblTitulo"><strong>Reporte Planillas sin cumplido </strong></td>
           </tr>
           <tr>
             <td colspan="4" class="subtitulo"><strong>INFORMACION: </strong><strong>Ingreso de datos.</strong></td>
           </tr>
           <tr>
             <td width="21%" class="fila">Fecha Inicial </td>
             <td width="31%" class="fila"><div align="center"><img src="<%=BASEIMG%>fecha.JPG" width="159" height="30"></div></td>
             <td width="48%"><span class="ayudaHtmlTexto">Campo para escoger la fecha inicial de la busqueda de los reportes que se desean obtener. Tiene como valor predeterminado la fecha actual menos 30 dias. </span></td>
           </tr>
           <tr>
             <td class="fila">Fecha Final</td>
             <td width="31%" class="fila"><div align="center"><img src="<%=BASEIMG%>fechafin.JPG" width="159" height="30"></div></td>
             <td><span class="ayudaHtmlTexto">Campo para escoger la fecha final de la busqueda de los reportes que se desean obtener. Tiene como valor predeterminado la fecha actual.</span></td>
           </tr>
           <tr>
             <td class="fila">Cliente</td>
             <td class="fila"><div align="center"></div></td>
             <td>Campo en el cual se ingresa el codigo del cliente para complementar la busqueda. </td>
           </tr>
           <tr>
             <td class="fila">Bot&oacute;n Aceptar</td>
             <td class="fila"><div align="center"><img src="<%=BASEIMG%>btnAceptar.JPG" width="94" height="25"></div></td>
             <td width="48%"><span class="ayudaHtmlTexto">Bot&oacute;n para realizar el procedimiento de b&uacute;squeda del Reporte de planillas sin cumplido. </span></td>
           </tr>
           <tr>
             <td class="fila">Bot&oacute;n Salir </td>
             <td width="31%" class="fila"><div align="center"><img src="<%=BASEIMG%>btnSalir.JPG" width="88" height="25"></div></td>
             <td><span class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista 'Reporte de planillas sin cumplido' y volver a la vista del men&uacute;.</span></td>
           </tr>
           <tr>
             <td colspan="3" class="subtitulo"><strong>INFORMACION: Reporte.</strong></td>
           </tr>
           <tr>
             <td class="fila">Bot&oacute;n Regresar </td>
             <td width="31%" class="fila"><div align="center"><img src="<%=BASEIMG%>btnRegresar.JPG" width="104" height="23"></div></td>
             <td><span class="ayudaHtmlTexto">Bot&oacute;n para regresar la ventana para ingresar el rango de fechas que genera el reporte. </span></td>
           </tr>
           <tr>
             <td class="fila">Bot&oacute;n Exportar </td>
             <td width="31%" class="fila"><div align="center"><img src="<%=BASEIMG%>btnExportar.JPG" width="103" height="25"></div></td>
             <td><span class="ayudaHtmlTexto">Bot&oacute;n para exportar a un archivo excel los datos arrojados como resultado del reporte.</span></td>
           </tr>
           <tr>
             <td class="fila">Bot&oacute;n Salir </td>
             <td width="31%" class="fila"><div align="center"><img src="<%=BASEIMG%>btnSalir.JPG" width="88" height="25"></div></td>
             <td><span class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista 'Reporte de planillas sin cumplido.' y volver a la vista del men&uacute;.</span></td>
           </tr>
           <tr>
             <td colspan="2" align="center" class="letra">&nbsp;</td>
           </tr>
         </table></td>
        </table>
		<p></p>
	<center>
	<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
	</center>
</div>  
</body>
</html>