<!--
- Autor : Ing. Enrique De Lavalle.
- Date  : 27 de Noviembre 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Jsp que muestra la ayuda para reporte de Oc de Viajes Vacios con anticipo sin reporte 
--%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>Funcionalidad del Reporte OC de vacios con anticipos sin reporte</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="/../../../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/reporte/ReportePlanillasSinCumplido/"; %> 
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE REPORTE WEB</div></td>
          </tr>
          <tr class="subtitulo1">
            <td><p>Descripci&oacute;n del funcionamiento del programa Reporte OC de vacios con anticipos sin reporte</p>
            <p>&nbsp;</p></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEIMG%>parametros.JPG" width="804" height="196"></div></td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>    </td>
</tr>
<tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">Si el rango escogido no arroja alg&uacute;n dato se ver&aacute; este mensaje </p>            </td>
          </tr>
<tr>
            <td  class="ayudaHtmlTexto"><div align="center"><img src="<%=BASEIMG%>mensajeNodatos.JPG" width="357" height="57"></div></td>
          </tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Finalmente si el rango de fechas son ingresados correctamente y tiene datos que mostar, el proceso se realizar&aacute; satisfactoriamente, y le saldr&aacute; la siguinete pantalla. </p>
    <p>&nbsp;</p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><img src="<%=BASEIMG%>Reporte.JPG" width="959" height="711"></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><p>&nbsp;</p>
    <p>Cuando se da click sobre el boton exportar se guarda un documento excel con el resultado del reporte web obtenido </p></td>
</tr>
<tr>
  <td  class="ayudaHtmlTexto"><div align="center">
    <p><img src="<%=BASEIMG%>docExcel.JPG" width="899" height="645"></p>
    <p>&nbsp;</p>
  </div></td>
</tr>
      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>

