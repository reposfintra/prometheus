<!--
- Autor : Ing. Ivan Dario Gomez Vanegas
- Date  : 1 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que genera el reporte de las Ot del Cliente Drummond
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>

<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Consulta Reporte Planilla</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
</head>
<% List ListadoClientes =  model.ReporteDrummondSvc.getListadoCliente();
   String Msg = request.getParameter("msg");%>
<%-- Inicio Body --%>
<body onLoad="redimensionar();" onResize="redimensionar();">
	<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte Drummond"/>
	</div>
	
	<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">
	<form name="forma" method="post" action="<%=CONTROLLER%>?estado=Reporte&accion=Drummond">
		<table width="500" border="2" align="center">
			<tr>
				<td>
					<table width="100%" align="center"  class="tablaInferior">
						<tr class="fila">
							<td width="50%" align="left" class="subtitulo1">&nbsp;Consulta De Reportes - Clientes </td>
							<td width="50%"  align="left" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
						</tr>
					</table>
					 <table width="100%" border="0" align="center" class="tablaInferior">
                                              <tr class="fila">
                                                <td><strong>Fecha inicial </strong></td>
                                                <td>
                                                  <input name='fechai' type='text' class="textbox" id="fechai" style='width:120' value='' readonly>
                                                  <a href="javascript:void(0);" class="link" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.forma.fechai);return false;"  HIDEFOCUS > <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16"
                                               border="0" alt="De click aqu&iacute; para ver el calendario."></a> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
                                              </tr>
                                              <tr class="fila" >
                                                <td><strong>Fecha final </strong></td>
                                                <td>
                                                  <input name='fechaf' type='text' class="textbox" id="fechaf" style='width:120' value='' readonly>
                                                  <a href="javascript:void(0);" class="link" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.forma.fechaf);return false;"  HIDEFOCUS > <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16"
                                               border="0" alt="De click aqu&iacute; para ver el calendario."></a> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
                                              </tr>
                                              <tr class='fila'>
                                                <td><strong>Cliente</strong></td>
                                                <td>
                                                   <select name="cliente" class="textbox">
                                                   <% Iterator It = ListadoClientes.iterator();
                                                      while(It.hasNext()){
                                                      Cliente  cliente   = (Cliente)It.next();   
                                                      String selectd = (cliente.getCodcli().equals("000361"))?"selected='selected'":"";%>
                                                       <option  <%=selectd%> value="<%=cliente.getCodcli()%>"><%=cliente.getNomcli()%> </option>
                                                     <%}%>
                                                   </select><img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
                                              </tr>
                                              
                                            </table>
				</td>
			</tr>
		</table>
		<p align="center">
			<img src="<%=BASEURL%>/images/botones/exportarExcel.gif"  name="Excel" style="cursor:hand" title="Exportar a Excel" onMouseOver="botonOver(this);" onClick="forma.submit();" onMouseOut="botonOut(this);" >&nbsp;
			<img src="<%=BASEURL%>/images/botones/salir.gif"  name="regresar" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >
		</p>
	</form>
	 <%if(Msg != null){
              %>
               <table border="2" align="center">
                  <tr>
                    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                       <tr>
                        <td width="229" align="center" class="mensajes"><%=Msg%> 
                       </td>
                       <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                      </tr>
                    </table>
                    </td>
                  </tr>
                </table>
            <%  
             }%>
	 <iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;">
     </iframe>
	</div>
</body>
</html>
