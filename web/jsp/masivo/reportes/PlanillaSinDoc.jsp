<!--
- Autor : Ing. Diogenes Bastidas Morales
- Date  : Noviembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra el reporte de viaje
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
<head>
<title>Reporte de Planillas Sin Documentos</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'> 
<link href="<%= BASEURL %>/css/estilomenuemergente.css" rel='stylesheet'> 
<link href="../../trafico/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>


</head>
<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte de Planillas Sin Documentos"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%  String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 15;
    int maxIndexPages = 10;
	
	Vector vec = model.planillaService.getPlasindoc();

%>
<%  if(vec!=null && vec.size()>0) { %>   
<table width="900" border="2" align="center">
  <tr>
    <td width="3007">  
      <table width="100%" border="0" align="center">
			<tr>
   				<td width="50%"  height="24"  class="subtitulo1"><p align="left">Listado General</p></td>
            	<td width="50%"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>						
            </tr>
	  </table>			
		  <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4" id="principal">
        <tr class="tblTitulo" align="center">
              <td width="12%">Planilla</td>
              <td width="20%" >Fecha</td>
              <td width="40%" >Ruta</td>
              <td width="28%" >Cliente</td>
              
			 
            </tr>
	<pg:pager
    items="<%=vec.size()%>"
    index="<%= index %>"
    maxPageItems="<%= maxPageItems %>"
    maxIndexPages="<%= maxIndexPages %>"
    isOffset="<%= true %>"
    export="offset,currentPageNumber=pageNumber"
    scope="request">
          <%-- keep track of preference --%>
          <%
        for (int i = offset.intValue(), l = Math.min(i + maxPageItems, vec.size()); i < l; i++) {
            Planilla pla  = (Planilla) vec.elementAt(i);%>
     <pg:item>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>" onMouseOver='cambiarColorMouse(this)'   style="cursor:hand"
		 onClick="window.open('<%=BASEURL%>/docremesas/documentos.jsp?numrem=<%=pla.getNumrem()%>' ,'D','status=yes,scrollbars=no,width=500,height=300,resizable=yes');">
          <td width="12%" align="center" class="bordereporte"><%=(pla.getNumpla()!=null)?pla.getNumpla() : ""%></td>
          <td width="20%"  align="center" class="bordereporte"><%=(pla.getFechadespacho()!=null)? String.toValueOf(pla.getFechadespacho()) : ""%></td>
          <td width="40%"  align="center" class="bordereporte"><%=(pla.getStd_job_rec()!=null)?pla.getStd_job_rec() : ""%></td>
          <td width="28%"  align="center" class="bordereporte"><%=(pla.getClientes()!=null)?pla.getClientes() : ""%></td>
          
        </tr>
		</pg:item>
		<%}%>
        <tr class="bordereporte">
          <td height="65" colspan="20" align="left" class="bordereporte">
		  <div align="center"><pg:index>
                <jsp:include page="../../../WEB-INF/jsp/googlesinimg.jsp" flush="true"/>        
            </pg:index></div></td>
          </tr>
        </pg:pager>
      </table>	  
    </td>
  </tr>
</table>

<%}else{
 String mensaje = "No Tiene planillas pendientes por documentos";%>
 <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=mensaje%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</p>
<table width="900" border="0" align="center">
 <tr>
      <td align="left" width="50%"><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgaceptar" onclick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/masivo/reportes&pagina=ReportePlanillasinDoc.jsp&marco=no'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
    </tr>
</table> 
 <%}%>



</div>
</body>
</html>
