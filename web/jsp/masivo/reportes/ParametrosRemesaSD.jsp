<!--
- Autor : Ing. Juan Manuel Escandon Perez
- Date  : Noviembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra los datos de entrada para 
--              la inicializacion del Reporte de Remesas sin documentos
--%>

<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://www.sanchezpolo.com/sot" prefix="tsp"%>
<% List	ListaAgencias = model.ciudadService.ListarAgencias();%>
<html>
<head>

    <title>Reporte de Viajes</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
    <link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
    <script src='<%= BASEURL %>/js/validar.js'></script>
	<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>
<body onResize="redimensionar()" onLoad="redimensionar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Parametros de Busqueda"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 

    <form name="form1" method="post" action="<%= CONTROLLER %>?estado=RemesaSin&accion=Doc">
        <table width="60%" border="2" align="center">
			<tr>
				<td>
					<table width="99%" height="34" border="0" align="center" class="tablaInferior">
            			<tr>
							<td width="190" height="24"  class="subtitulo1"><p align="left">Parametros Iniciales</p></td>
            				<td width="404"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
							<input name="Opcion" type="hidden" id="Opcion">
        				</tr>     						
						<tr class="fila">
							<td align="left"class="letra_resaltada">&nbsp;&nbsp;FECHA INICIAL</td>
							<td align="">&nbsp;&nbsp;<tsp:Date id="finicial" formulario="form1" name="finicial" fechaInicial="hoy - 30 dias"/></td>
						</tr>
						<tr class="fila">
							<td align="left"class="letra_resaltada">&nbsp;&nbsp;FECHA FINAL</td>
							<td align="left">&nbsp;&nbsp;<tsp:Date id="ffinal" formulario="form1"  name="ffinal" fechaInicial="hoy"/></td>
						</tr>
						<tr class="fila">
                			<td class="letra_resaltada" align="left">&nbsp;&nbsp;AGENCIA</td>
                			<td align="left">&nbsp;&nbsp;<select name='agencia' id="agencia" class="textbox">
                				<option value="%">TODAS</option> 
								<%    if(ListaAgencias.size() > 0) {
                          				 Iterator It3 = ListaAgencias.iterator();
                           				while( It3.hasNext() ) {
                               				Ciudad  datos2 =  (Ciudad) It3.next();                             
                               				out.print("<option value='"+datos2.getCodCiu()+"'>["+datos2.getCodCiu()+"] "+datos2.getNomCiu()+"</option> \n");
                           				}
                      				 }
                       			%>
                    			</select></td>	  
            			</tr>
						<tr class="fila">
							<td class="letra_resaltada" align="left">&nbsp;&nbsp;USUARIO</td>
							<td align="left">&nbsp;&nbsp;<input type="text" id="usuario" name="usuario" class="textbox" maxlength="15"></td>            
					  	</tr>   
					</table> 
					</td>        
				</tr>        	
		</table>
        <table align="center">
          <tr align="center">
            <td colspan="2"> <img src="<%=BASEURL%>/images/botones/aceptar.gif"  name="imgaceptar" onClick="form1.Opcion.value='Generar';form1.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> <img src="<%=BASEURL%>/images/botones/salir.gif"  name="imgsalir" onClick="parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"> </td>
          </tr>
        </table>
    </form>
	</div>
</body>
<tsp:InitCalendar/>
</html>
