<!--
- Autor : Ing. Enrique De Lavalle.
- Date  : 23 de Noviembre 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Jsp que muestra el reporte Oc Vacios con anticipo sin reporte 
--%>

<%-- Declaracion de librerias--%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>



<html>
<head>
<title>.: OC de Viajes Vacios con Anticipo sin reporte</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type='text/javascript' src="<%=BASEURL%>/js/reporte.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/script.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=OC Vac�os con Anticipo sin Reporte"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; overflow:scroll"> 
<br>
<%
  List Listado = model.reporteOcAnt.getListVacios();
  UtilFinanzas u = new UtilFinanzas();
  
  
  String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
  String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
  
    String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 6;
    int maxIndexPages = 10;
	
	String fechainicial = ( request.getParameter("fechainicial")!=null )?request.getParameter("fechainicial"):"";
	String fechafinal   = ( request.getParameter("fechafinal")!=null)?request.getParameter("fechafinal"):"";
%>

<%  if(Listado!=null && Listado.size()>0) { %>  
<form name="form1" method="post" id="forma"  action="<%=CONTROLLER%>?estado=ReporteOC&accion=AnticipoVacio">
<table width="1800" border="0" align="center">
    <tr>
      <td align="left" width="8%"><img src="<%=BASEURL%>/images/botones/regresar.gif"      name="imgaceptar" onclick="Opcion.value = 'Regresar'; form1.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
      <td align="left" width="8%"><img src="<%=BASEURL%>/images/botones/exportarExcel.gif"  name="imgaceptar" onclick="Opcion.value = 'Excel'; form1.submit();"  style="cursor:hand"></td>
	  <td align="left" width="84%"><img src="<%=BASEURL%>/images/botones/salir.gif"   name="mod"  height="21"  onclick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
	  
    </tr>
</table>
<table width="3000" border="2" align="center">
  <tr>
    <td>  
      <table width="100%" border="0" >
			<tr>
   				<td width="313"  height="24"  class="subtitulo1"><p align="left">OC Vac�os con Anticipo sin Reporte</p></td>
            	<td width="865"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>						
            </tr>
	  </table>			
	
	<table  width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4" id="principal">
        <tr class="tblTitulo" align="center">
				<td width="100" align="center" title="Agencia generadora OC">Ag. Gen. OC</td>    				
			 	<td width="120" align="center" title="Usuario generador OC">Usr. Gen. OC</td>    				
			  	<td width="120" align="center" title="OC: Numero de planilla">OC.</td>    				
				<td width="120" align="center" title="Contador de planillas ">Cont. OC.</td>    				
				<td width="120" align="center" title="Valor de la planilla">Valor OC</td>    				
				<td width="120" align="center" title="Identificaci�n del vehiculo">Placa</td>    				
				<td width="120" align="center" title="Fecha de Generaci�n de la OC">Fecha. Gen. OC</td>    				
				<td width="120" align="center" title="D�as de generada la OC">D�as. Gen. OC</td>    				
				<td width="120" align="center" title="Fecha de impresi�n de OC">Fecha. Imp. OC</td>    				
				<td width="120" align="center" title="Fecha generac�on anticipo">Fecha. Gen. Ant.</td>
				<td width="120" align="center" title="Valor anticipo">Anticipo</td>
				<td width="120" align="center" title="Numero del Cheque del anticipo">No. Cheq. Ant.</td>
				<td width="120" align="center" title="Fecha de impresion del Cheque del anticipo">Fecha. Imp. Cheq. Ant.</td>
				<td width="250" align="center" title="Ruta">Ruta</td>
				<td width="120" align="center" title="Agencia origen del despacho">Agen. Orig.</td>
				<td width="120" align="center" title="Agencia destino del despacho">Agen. Dest.</td>
				<td width="120" align="center" title="Numero puestos de control">No. psto. Ctrl.</td>
				<td width="120" align="center" title="Descuento puesto de control">Desc. psto. Crtl.</td>
				<td width="120" align="center" title="OT: Numero de remesa">OT.</td>
				<td width="120" align="center" title="Valor de la remes">Valor OT.</td>
				<td width="500" align="center" title="Descripci�n OT">Desc. OT.</td>
				<td width="120" align="center" title="Cliente">Cliente</td>
				<td width="140" align="center" title="Factura del Cliente">No Factura</td>
				<td width="120" align="center" title="Agencia que genera el anticipo">Ag. Anticipo</td>
				<td width="140" align="center" title="Cedula Generador Anticipo">Ced. Gen. Ant.</td>
				<td width="200" align="center" title="Generador Anticipo">Gen. Ant.</td>
				<td width="120" align="center" title="Valor Flete">Valor. Flete</td>
				<td width="120" align="center" title="Fecha cumplido OC">Fecha. Cumpl. OC</td>
				<td width="120" align="center" title="Cedula Usuario cumplido ">Ced. Usr. Cumpl.</td>
				<td width="120" align="center" title="Cumplida por">Cumplida por</td>
				<td width="120" align="center" title="C�digo Cliente">Cod. Cliente</td>
				<td width="500" align="center" title="Otras OC asociadas">Otras OC Asociadas</td>
				<td width="180" align="center" title="D�as">D�as</td>
		</tr>
	<pg:pager
		items="<%=Listado.size()%>"
		index="<%= index %>"
		maxPageItems="<%= maxPageItems %>"
		maxIndexPages="<%= maxIndexPages %>"
		isOffset="<%= true %>"
		export="offset,currentPageNumber=pageNumber"
  		 scope="request">
          <%-- keep track of preference --%>
          <%
        for (int i = offset.intValue(), l = Math.min(i + maxPageItems, Listado.size()); i < l; i++) {
             ReporteOCAnticipo rpv = (ReporteOCAnticipo) Listado.get(i);%>  
    	<pg:item>
        <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">			
			
        	<td  align="center" title="Agencia generadora OC"><% if( !rpv.getCiudad().equals("")){%> <%=rpv.getCiudad()%>  <%}else{%>&nbsp;<%}%></td>  
			<td  align="center" title="Usuario generador OC"><% if( !rpv.getDespachador().equals("")){%> <%=rpv.getDespachador()%>  <%}else{%>&nbsp;<%}%></td>    				
			<td  align="center" title="OC: Numero de planilla"><% if( !rpv.getNumpla().equals("")){%> <%=rpv.getNumpla()%>  <%}else{%>&nbsp;<%}%> </td>    				
			<td  align="center" title="Contador de planillas "><%=(rpv.getContaroc()!=null)? rpv.getContaroc():""%></td>    				    				
			<td  align="center" title="Valor de la planilla"><% if( !rpv.getVlrpla2().equals("")){%> <%=u.customFormat(rpv.getVlrpla2())%>  <%}else{%>&nbsp;<%}%></td>					
			<td  align="center" title="Identificaci�n del vehiculo"><% if( !rpv.getPlatlr().equals("")){%> <%=rpv.getPlatlr()%>  <%}else{%>&nbsp;<%}%></td> 				
			<td  align="center" title="Fecha de Generaci�n de la OC"><% if( !rpv.getCreation_date().equals("")){%> <%=rpv.getCreation_date()%>  <%}else{%>&nbsp;<%}%> </td>    									
			<% if(rpv.getDif().equals("0") && rpv.getPrinter_date().equals("No Impresa")){%>
			<td  align="center" title="D�as de generada la OC">&nbsp;</td>
			<%}else{%>
			<td  align="center" title="D�as de generada la OC"><%=rpv.getDif()%></td>
			<%}%>
			<td  align="center" title="Fecha de impresi�n de OC"><% if( !rpv.getPrinter_date().equals("")){%> <%=rpv.getPrinter_date()%>  <%}else{%>&nbsp;<%}%></td>    									
			<td  align="center" title="Fecha generac�on anticipo"><% if( !rpv.getFechaanticipo().equals("")){%> <%=rpv.getFechaanticipo()%>  <%}else{%>&nbsp;<%}%></td>					
			<td  align="center" title="Valor anticipo"><% if( !rpv.getVlr().equals("")){%> <%=u.customFormat(rpv.getVlr())%>  <%}else{%>&nbsp;<%}%></td>					
			<td  align="center" title="Numero del Cheque del anticipo"><% if( !rpv.getDocument().equals("")){%> <%=rpv.getDocument()%>  <%}else{%>&nbsp;<%}%></td>					
			<td  align="center" title="Fecha de impresion del Cheque del anticipo"><% if( !rpv.getFecha_cheque().equals("")){%> <%=rpv.getFecha_cheque()%>  <%}else{%>&nbsp;<%}%></td>					
			<td  align="center" title="Ruta"><% if( !rpv.getDesc_ruta().equals("")){%> <%=rpv.getDesc_ruta()%>--<%=rpv.getOrigenoc()%>- <%=rpv.getDestinooc()%> <%}else{%>&nbsp;<%}%></td>					
			<td  align="center" title="Agencia origen del despacho"><% if( !rpv.getOrigenoc().equals("")){%> <%=rpv.getOrigenoc()%>  <%}else{%>&nbsp;<%}%> </td>					
			<td  align="center" title="Agencia destino del despacho"> <% if( !rpv.getDestinooc().equals("")){%> <%=rpv.getDestinooc()%>  <%}else{%>&nbsp;<%}%></td>					
			<td  align="center" title="Numero puestos de control"><% if( !rpv.getPuestocontrol().equals("")){%> <%=rpv.getPuestocontrol()%>  <%}else{%>&nbsp;<%}%></td>
			<td  align="center" title="Descuento puesto de control"><% if( !rpv.getPuestocontrol().equals("")){%> <%=u.customFormat((Integer.parseInt(rpv.getPuestocontrol())*30000))%>  <%}else{%>&nbsp;<%}%></td>					
			<td  align="center" title="OT: Numero de remesa"><% if( !rpv.getNumrem().equals("")){%> <%=rpv.getNumrem()%>  <%}else{%>&nbsp;<%}%> </td>					
			<td  align="center" title="Valor de la remesa"><% if( !rpv.getVlrrem2().equals("")){%> <%=u.customFormat(rpv.getVlrrem2())%>  <%}else{%>&nbsp;<%}%> </td>					
			<td  align="center" title="Descripci�n OT"><% if( !rpv.getProducto().equals("")){%> <%=rpv.getProducto()%>  <%}else{%>&nbsp;<%}%></td>					
			<td  align="center" title="Cliente"><% if( !rpv.getNomCliente().equals("")){%> <%=rpv.getNomCliente()%>  <%}else{%>&nbsp;<%}%></td>					
			<td  align="center" title="Factura Cliente"><% if( !rpv.getFacturacliente().equals("")){%> <%=rpv.getFacturacliente()%>  <%}else{%>&nbsp;<%}%></td>					
			<td  align="center" title="Agencia que genera el anticipo"><% if( !rpv.getAgenciaanticipo().equals("")){%> <%=rpv.getAgenciaanticipo()%>  <%}else{%>&nbsp;<%}%> </td>					
			<td  align="center" title="Cedula Generador Anticipo"><% if( !rpv.getCedulaanticipo().equals("")){%> <%=rpv.getCedulaanticipo()%>  <%}else{%>&nbsp;<%}%></td>					
			<td  align="center" title="Generador Anticipo"><% if( !rpv.getNombreanticipo().equals("")){%> <%=rpv.getNombreanticipo()%>  <%}else{%>&nbsp;<%}%> </td>					
			<td  align="center" title="Valor Flete"><% if( !rpv.getValorflete().equals("")){%> <%=u.customFormat(rpv.getValorflete())%>  <%}else{%>&nbsp;<%}%></td>					
			<td  align="center" title="Fecha cumplido OC"> <% if( !rpv.getFechacumplido().equals("")){%> <%=rpv.getFechacumplido()%>  <%}else{%>&nbsp;<%}%></td>					
			<td  align="center" title="Cedula Usuario cumplido "><% if( !rpv.getCedulacumplido().equals("")){%> <%=rpv.getCedulacumplido()%>  <%}else{%>&nbsp;<%}%> </td>					
			<td  align="center" title="Cumplida por"> <% if( !rpv.getNombrecumplido().equals("")){%> <%=rpv.getNombrecumplido()%>  <%}else{%>&nbsp;<%}%> </td>					
			<td  align="center" title="C�digo Cliente"><% if( !rpv.getCodCliente().equals("")){%> <%=rpv.getCodCliente()%>  <%}else{%>&nbsp;<%}%></td>
			<td  width="500" align="center" title="Otras OC asociadas"><% if( !rpv.getOcasociadas().equals("")){%> <%=rpv.getOcasociadas()%>  <%}else{%>&nbsp;<%}%></td>
			<% int d = 0;
			  d = Integer.parseInt(rpv.getDias());
			  
			 if(rpv.getPrinter_date().equals("No Impresa")){%>
			 <td  align="center" title="D�as">No Impresa</td>
			<%}else{  
				if(rpv.getDias().equals("1")){
			%>
			<td  align="center" title="D�as">1 d�a</td>
			<%}else if(d==2||d==3){%>
			<td  align="center" title="D�as">2 y 3 D�as</td>
			<%}else if (d>=4){%>
			<td  align="center" title="D�as">4 y m�s d�as</td></td>
			<%}}%>
			
        </tr>
		</pg:item>
		<%}%>
		<tr class="bordereporte">
          <td height="65" colspan="20" align="left" class="bordereporte">
		  <div style=" width:30% ">
		   <pg:index>
              <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>   				   
            </pg:index></div></td>
          </tr>
         </pg:pager>
      </table>	  
    </td>
  </tr>
</table>
 <input type=hidden id='Opcion' name='Opcion' value=''> 
 <input type=hidden id='fechaini' name='fechaini' value='<%=fechainicial%>'> 
 <input type=hidden id='fechafin' name='fechafin' value='<%=fechafinal%>'> 
<table width="1800" border="0" align="center">
    <tr>
      <td align="left" width="8%"><img src="<%=BASEURL%>/images/botones/regresar.gif"      name="imgaceptar" onclick="Opcion.value = 'Regresar'; form1.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
      <td align="left" width="8%"><img src="<%=BASEURL%>/images/botones/exportarExcel.gif"  name="imgaceptar" onclick="Opcion.value = 'Excel'; form1.submit();"  style="cursor:hand"></td>
	  <td align="left" width="84%"><img src="<%=BASEURL%>/images/botones/salir.gif"   name="mod"  height="21"  onclick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
	  
    </tr>
</table>

<%}else{
 String mensaje = "No hay datos para estos parametros";%>
 <p><table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=mensaje%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
<table width="1800" border="0" align="center">
    <tr>
      <td align="left" width="22%">&nbsp;</td>
	  <td align="left" width="7%"><img src="<%=BASEURL%>/images/botones/regresar.gif"      name="imgaceptar" onClick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/masivo/reportes/ReporteOCAnticipoSinTrafico&pagina=FiltroReporteOcVacios.jsp'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
      <td align="left" width="71%"><img src="<%=BASEURL%>/images/botones/salir.gif"   name="mod"  height="21"  onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
    </tr>
</table>
</p> <%}%>
 

</form>
	


</div>
</body>
</html>


