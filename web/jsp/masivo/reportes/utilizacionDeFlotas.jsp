<!--
- Autor : Ing. Henry A. Osorio González
- Date  : 20 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que genera el reporte d eUtilización de FLotas
--%>

<%-- Declaracion de librerias--%>
<%@ page session="true"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<html>
<head>
    <title>Utilizacion de Flotas</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<script src='<%=BASEURL%>/js/date-picker.js'></script>
    <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
	<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
	<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/boton.js"></script>
	<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validarDocumentos.js"></script>

</head>

<body>
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 1px; top: 0px;">
 <jsp:include page="/toptsp.jsp?encabezado=Reporte de Utilizacion de Flota"/>
</div>
<div id="capaCentral" style="position:absolute; width:100%; height:87%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<form name="form2" method="post" action="<%=CONTROLLER%>?estado=FlotaUtilizada&accion=Reporte" >
<table width="410" border="2" align="center">
  <tr>
    <td><table width="100%"  border="0">
      <tr>
        <td width="59%" class="subtitulo1">Reporte de Utilizaci&oacute;n de flota </td>
        <td width="41%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
      </tr>
    </table>
    <table width="100%" class="tablaInferior" >
      <tr class="fila">
        <td width="167" nowrap>Fecha  Inicial</td>
        <td width="219" nowrap><input name="fechai" type="text" class="textbox" id="fechai"  value="" size="15" readonly> 
        <img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.form2.fechai);return false;" HIDEFOCUS>          </span></td>
      </tr>
      <tr class="fila">
        <td nowrap>Fecha Final</td>
        <td nowrap><input name='fechaf' type='text' class="textbox" id="fechaf"  value="" size="15" readonly>
          <a href="javascript:void(0)" onclick="jscript: show_calendar('fechaf');" HIDEFOCUS>		  </a><img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.form2.fechaf);return false;" HIDEFOCUS></td>
      </tr>
      
    </table></td>
  </tr>
</table> 
<br>
   <table align="center">
   <tr><td>
        <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod"  height="21"  onclick="form2.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">        
   	  <img src="<%=BASEURL%>/images/botones/salir.gif" name="mod"  height="21" onclick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
  </td></tr>
 </table>
<br>
<%String msg = (request.getParameter("msg")!=null) ? request.getParameter("msg") : "";%>
<%if(!msg.equals("")){%>
<table border="2" align="center">
  <tr>
    <td><table width="100%" border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="262" align="center" class="mensajes"><%=request.getParameter("msg")%> !</td>
        <td width="32" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="44">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table><%}%>

</form>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>    
</body>
</html>
