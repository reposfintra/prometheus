<!--
- Autor : Ing. Juan Escandon
- Date  : 10 de Enero del 2006
- copyrigth : Fintravalores S.A. S.A.
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que genera el reporte de vehiculos perdidos
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>Reporte Vehiculos Perdidos</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado= Reporte Vehiculos Perdidos"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<% String Mensaje = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";%>
<form name="forma" method="post" action="">
  <table width="500" border="2" align="center">
    <tr>
      <td><table width="100%" class="tablaInferior">
        <tr class="barratitulo">
          <td align="left">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="45%" class="subtitulo1">Reporte Vehiculos Perdidos</td>
                <td width="55%"><img src="<%=BASEURL%>/images/titulo.gif"></td>
              </tr>
            </table>            </td>
          </tr>
        <tr class="fila">
          <td align="left" ><div align="center">Haga clic en Aceptar para iniciar el proceso </div></td>
          </tr>
      </table></td>
    </tr>
  </table>
 <br>
 <center>
<img src="<%=BASEURL%>/images/botones/aceptar.gif" title="Aceptar..." name="c_imgaceptar"  onClick="forma.action='<%=CONTROLLER%>?estado=Reporte&accion=VehPerdidos'; forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">
<img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" title="Salir..." onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand ">
</center>
</form>
  <% if(!Mensaje.equals("")){%>
  <table border="2" align="center">
  <tr>
    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
      <tr>
        <td width="229" align="center" class="mensajes"><%=Mensaje%></td>
        <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
        <td width="58">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
 <%}%>
 </div>
</body>
</html>
