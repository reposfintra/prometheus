<!--
- Autor : Ing. Juan Manuel Escandon Perez
- Date  : Noviembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra el reporte de viaje
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<html>
    <head>
        <title>Reporte de Planillas Anuladas</title>
        <link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'> 
        <link href="<%= BASEURL %>/css/estilomenuemergente.css" rel='stylesheet'> 
        <link href="../../trafico/css/estilostsp.css" rel="stylesheet" type="text/css">
        <link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
        <script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
        <script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>

    </head>
    <body onResize="redimensionar()" onLoad="redimensionar()">
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte Planillas Anuladas"/>
        </div>

        <div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<%  String style = "simple";
    String position =  "bottom";
    String index =  "center";
    int maxPageItems = 15;
    int maxIndexPages = 10;
	
	List Listado = model.AnulacionPlanillaSvc.getListPA();
	String fechai = request.getParameter("fechai");
	String fechaf = request.getParameter("fechaf");
%>
<%  if(Listado!=null && Listado.size()>0) { %>   
            <table width="6000" border="2" align="center">
                <tr>
                <td>  
                    <table width="400%" border="0" align="center">
                        <tr>
                            <td width="4000"  height="24"  class="subtitulo1">Listado General</td>
                            <td width="900"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
                        </tr>
                    </table>			
					 
			
	  		
                    <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4" id="principal">
                        <tr class="tblTitulo" align="center">
                            <td width="86" align="center" title="Planilla">Planilla</td>
                            <td width="71" align="center" title="Placa">Placa</td>
                            <td width="86" align="center" title="Remesa">Remesa</td>
                            <td width="140" align="center" title="Cliente">Cliente</td>
                            <td width="243" align="center" title="Standartjob">Standartjob</td>
                            <td width="162" align="center" title="Fec. Creacion">Fec. Creacion</td>
                            <td width="131" align="center" title="Fec. Anulacion">Fec. Anulacion</td>                        
                            <td width="99" align="center" title="Diferencia">Diferencia</td>
                            <td width="125" align="center" title="Usuario">Usuario</td>
                            <td width="95" align="center" title="Anticipo">Vlr. Anticipo En Pesos</td>
                            <td width="95" align="center" title="Banco">Banco</td>
                            <td width="89" align="center" title="Sucursal">Sucursal</td>
                            <td width="87" align="center" title="Agencia">Agencia</td>
                            <td width="127" align="center" title="Estado anticipo">Estado anticipo</td>
                            <td width="138" align="center" title="Estado Impresion">Estado Impresion</td>
                            <td width="132" align="center" title="Causa">Causa</td>
                            <td width="156" align="center" title="Forma recuperacion">Forma recuperacion</td>
                            <td width="121" align="center" title="Cumplido">Cumplido</td>
                            <td width="154" align="center" title="Fec. Cumplido">Fec. Cumplido</td>
                            <td width="143" align="center" title="Fecha Proy llegada Cheque">Fecha Proy llegada Cheque</td>
                            <td width="121" align="center" title="Agen Desp OC">Agen Desp OC</td>
                            <td width="150" align="center" title="Fecha Print OC">Fecha Print OC</td>
                            <td width="87" align="center" title="Valor OC">Valor OC</td>
                            <td width="138" align="center" title="Motivo de la Anulacion">Observaciones de la Anulacion</td>
                            <td width="132" align="center" title="Cheque Anticipo">Cheque Anticipo</td>
                            <td width="123" align="center" title="Estado Cheque">Estado Cheque</td>
                            <td width="140" align="center" title="Nombre Gen Anti">Nombre Gen Anti</td>
                            <td width="146" align="center" title="Agen Gen Anti">Agen Gen Anti</td>
                            <td width="170" align="center" title="Supp to Pay">Supp to Pay</td>
                            <td width="166" align="center" title="Supplier Name">Supplier Name</td>
                            <td width="147" align="center" title="Ced Propietario">Ced Propietario</td>
                            <td width="143" align="center" title="Banco Propietario">Banco Propietario</td>
                            <td width="155" align="center" title="Sucursal Propietario">Sucursal Propietario</td>
                            <td width="50" align="center" title="Ingreso">Ingreso</td>
                            <td width="50" align="center" title="Valor Ingreso">Valor Ingreso</td>
                            <td width="165" align="center" title="Valor Anticipo">Valor Anticipo</td>
                            <td width="89" align="center" title="Moneda">Moneda</td>
                            <td width="122" align="center" title="Fecha Anti">Fecha Anti</td>
                            <td width="130" align="center" title="Valor Pendiente Recuperar">Valor Pendiente Recuperar</td>
                            <td width="118" align="center" title="Tiene Anticipo">Tiene Anticipo</td>
                        </tr>
                        <pg:pager
                            items="<%=Listado.size()%>"
                            index="<%= index %>"
                            maxPageItems="<%= maxPageItems %>"
                            maxIndexPages="<%= maxIndexPages %>"
                            isOffset="<%= true %>"
                            export="offset,currentPageNumber=pageNumber"
                            scope="request">
                            <%-- keep track of preference --%>
          <%
        for (int i = offset.intValue(), l = Math.min(i + maxPageItems, Listado.size()); i < l; i++) {
            AnulacionPlanilla dat  = (AnulacionPlanilla) Listado.get(i);%>
                            <pg:item>
                                <tr class="<%=(i % 2 == 0 )?"filagris":"filaazul"%>">
                                    <td  class="bordereporte" width="86" align="center"><%=(dat.getNumpla()!=null)?dat.getNumpla() : ""%></td>
                                    <td  class="bordereporte" width="71"  align="center" ><%=(dat.getPlaca()!=null)?dat.getPlaca() : ""%></td>
                                    <td  class="bordereporte" width="86"  align="center" ><%=(dat.getRemesa()!=null)?dat.getRemesa() : ""%></td>
                                    <td  class="bordereporte" width="140"  align="center" ><%=(dat.getCliente()!=null)?dat.getCliente() : ""%></td>
                                    <td  class="bordereporte" width="243"  align="center" ><%=(dat.getDescripcion()!=null)?dat.getDescripcion() : ""%></td>
                                    <td  class="bordereporte" width="162"  align="center" ><%=(dat.getFechacreacion()!=null)?dat.getFechacreacion() : ""%></td>
                                    <td  class="bordereporte" width="131" align="center" ><%=(dat.getFechaanulacion()!=null)?dat.getFechaanulacion() : ""%></td>                                
                                    <td  class="bordereporte" width="99"  align="center" ><%=(dat.getDiferencia()!=null)?dat.getDiferencia() : ""%></td>
                                    <td  class="bordereporte" width="125"  align="center" ><%=(dat.getUsuario()!=null)?dat.getUsuario() : ""%></td>
                                    <td  class="bordereporte" width="95" align="center" ><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(dat.getValoranticipo())%></div></td>
                                    <td  class="bordereporte" width="95"  align="center" ><%=(dat.getBanco()!=null)?dat.getBanco() : ""%></td>
                                    <td  class="bordereporte" width="89"  align="center" ><%=(dat.getSucursal()!=null)?dat.getSucursal() : ""%></td>
                                    <td  class="bordereporte" width="87"  align="center" ><%=(dat.getAgencia()!=null)?dat.getAgencia() : ""%></td>
                                    <td  class="bordereporte" width="127"  align="center" ><%=(dat.getEstadoanulacion()!=null)?dat.getEstadoanulacion() : ""%></td>
                                    <td  class="bordereporte" width="138"  align="center" ><%=(dat.getEstadoimpresion()!=null)?dat.getEstadoimpresion() : ""%></td>
                                    <td  class="bordereporte" width="132"  align="center" ><%=(dat.getCausaanulacion()!=null)?dat.getCausaanulacion() : ""%></td>
                                    <td  class="bordereporte" width="156"  align="center" ><%=(dat.getFormarecuperacion()!=null)?dat.getFormarecuperacion() : ""%></td>
                                    <td  class="bordereporte" width="121"  align="center" ><%=(dat.getCumplimiento()!=null)?dat.getCumplimiento() : ""%></td>
                                    <td  class="bordereporte" width="154" align="center" ><%=(dat.getFechacumplimiento()!=null)?dat.getFechacumplimiento() : ""%></td>
                                    <td  class="bordereporte" width="143"align="center"  ><%-- =(dat.getFechaproyectada()!=null)?dat.getFechaproyectada() : ""--%></td>
                                    <td  class="bordereporte" width="121"align="center"  ><%=(dat.getCiudad()!=null)?dat.getCiudad() : ""%></td>
                                    <td  class="bordereporte" width="150"align="center"  ><%=(dat.getFechaimpresionoc()!=null)?dat.getFechaimpresionoc() : ""%></td>
                                    <td  class="bordereporte" width="87"align="center"  ><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(dat.getValoroc())%></div></td>
                                    <td  class="bordereporte" width="138"align="center" ><%=(dat.getMotivoanulacion()!=null)?dat.getMotivoanulacion() : ""%></td>
                                    <td  class="bordereporte" width="132"align="center" ><%=(dat.getChequeanticipo()!=null)?dat.getChequeanticipo() : ""%></td>
                                    <td  class="bordereporte" width="123"align="center" ><%-- =(dat.getEstadocheque()!=null)?dat.getEstadocheque() : "" --%></td>
                                    <td  class="bordereporte" width="140"align="center" ><%=(dat.getNombreanticipo()!=null)?dat.getNombreanticipo() : "" %></td>
                                    <td  class="bordereporte" width="146"align="center" ><%=(dat.getAgencia()!=null)?dat.getAgencia() : ""%></td>
                                    <td  class="bordereporte" width="170"align="center" ><%=(dat.getSupptopay()!=null)?dat.getSupptopay() : ""%></td>
                                    <td  class="bordereporte" width="166"align="center" ><%=(dat.getSuppliername()!=null)?dat.getSuppliername() : ""%></td>
                                    <td  class="bordereporte" width="147"align="center" ><%=(dat.getCedulapropietario()!=null)?dat.getCedulapropietario() : ""%></td>
                                    <td  class="bordereporte" width="143"align="center" ><%=(dat.getBancopropietario()!=null)?dat.getBancopropietario() : ""%></td>
                                    <td  class="bordereporte" width="155"align="center" ><%=(dat.getSucursalpropietario()!=null)?dat.getSucursalpropietario() : ""%></td>
                                    <td  class="bordereporte" width="50"align="center" >---</td>
                                    <td  class="bordereporte" width="50"align="center" >---</td>
                                    <td  class="bordereporte" width="165"align="center" ><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(dat.getValorforaneoanticipo()) %></div></td>
                                    <td  class="bordereporte" width="89"align="center" ><%=(dat.getMoneda()!=null)?dat.getMoneda() : ""%></td>
                                    <td  class="bordereporte" width="122"align="center" ><%=(dat.getFechaanticipo()!=null)?dat.getFechaanticipo() : ""%></td>
                                    <td  class="bordereporte" width="130"align="center" ><div align="right"><%= com.tsp.util.UtilFinanzas.customFormat2(dat.getValorrecuperar()) %></div></td>
                                    <td  class="bordereporte" width="118"align="center" ><%=(dat.getTieneanticipo()!=null)?dat.getTieneanticipo() : ""%></td>
                                    
		
  
                                </tr>
                            </pg:item>
		<%}%>
                            <tr class="bordereporte">
                            <td height="65" colspan="40" align="left" class="bordereporte">
                            <div style=" width:30% "><pg:index>
                                <jsp:include page="../../../WEB-INF/jsp/googlesinimg.jsp" flush="true"/>        
                            </pg:index></div></td>
                            </tr>
                        </pg:pager>
                  </table>	  
                </td>
                </tr>
            </table>

            <table width="1800" border="0" align="center">
    		<tr>
     
                <td align="left" width="9%"><img src="<%=BASEURL%>/images/botones/regresar.gif"  name="imgaceptar" onclick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/masivo/reportes&pagina=ParametrosPlanillasAnuladas.jsp&marco=no'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
                <td align="left" width="91%"><img src="<%=BASEURL%>/images/botones/exportarExcel.gif"  name="imgaceptar" onclick="window.location='<%= CONTROLLER %>?estado=Anulacion&accion=Planilla&finicial=<%=fechai%>&ffinal=<%=fechaf%>&Opcion=Excel'; message();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" ></td>
                		
				</tr>
            </table> 
<% }else{
 String mensaje = "No hay datos para estos parametros";%>
            <p><table border="2" align="center">
                <tr>
                    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                        <tr>
                            <td width="229" align="center" class="mensajes"><%=mensaje%></td>
                            <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                            <td width="58">&nbsp;</td>
                        </tr>
                    </table></td>
                </tr>
            </table>
			
			<table width="1800" border="0" align="center">
				<tr>
				  <td align="left" width="22%">&nbsp;</td>
				  <td align="left" width="7%"><img src="<%=BASEURL%>/images/botones/regresar.gif"      name="imgaceptar" onclick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/masivo/reportes&pagina=ParametrosPlanillasAnuladas.jsp&marco=no'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
				  <td align="left" width="71%"><img src="<%=BASEURL%>/images/botones/salir.gif"   name="mod"  height="21"  onClick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
				</tr>
			</table>
            </p>
             
 <%}%>

        </div>
<script>
function message(){
    
    alert ("El proceso se ha iniciado");

}
</script>
    </body>
</html>
