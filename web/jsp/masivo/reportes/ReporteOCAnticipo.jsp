<!--
- Autor : Ing. Iv�n Dar�o Devia Acosta
- Date  : 24 de Noviembre de 2006, 2:40 AM
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que muestra el reporte OC generadas con anticipo que no tengan
--              reporte en tr�fico
--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<%
 String Mensaje = (request.getParameter("Mensaje")!=null)?request.getParameter("Mensaje"):"";
    
 String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
 String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>
<html>
<head>
<title>Reporte OC con Anticipo sin reporte en tr�fico  </title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel='stylesheet'> 
<link href="<%= BASEURL %>/css/estilomenuemergente.css" rel='stylesheet'> 
<link href="../../trafico/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validarDocumentos.js"></script>

</head>
<body onResize="redimensionar()" onLoad="redimensionar()" onLoad="cargar()">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte OC con Anticipo sin reporte en tr�fico"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">

<form name="form2" method="post" action="<%=CONTROLLER%>?estado=ReporteOC&accion=Anticipo" >
<table width="410" border="2" align="center">
  <tr>
    <td><table width="100%"  border="0">
      <tr>
        <td width="59%" class="subtitulo1">Reporte de OC con Anticipo sin reporte en tr�fico</td>
        <td width="38%" class="barratitulo"><%=datos[0]%><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"align="left"></td>
      </tr>
    </table>
    <table width="100%" class="tablaInferior" >
      <tr class="fila">
        <td width="167" nowrap>Fecha  Inicial</td>
        <td width="219" nowrap><input name="fechaini" type="text" class="textbox" id="fechaini"  value="" size="15" readonly> 
        <a href="javascript:void(0)" onclick="jscript: show_calendar('fechaini');" HIDEFOCUS>  </a>
            <img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.form2.fechaini);return false;" HIDEFOCUS> 
            <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
      </tr>
      <tr class="fila">
        <td nowrap>Fecha Final</td>
        <td nowrap><input name='fechafin' type='text' class="textbox" id="fechafin"  value="" size="15" readonly>
          <a href="javascript:void(0)" onclick="jscript: show_calendar('fechafin');" HIDEFOCUS>  </a>
            <img src="<%=BASEURL%>/images/cal.gif" width="16" height="16" align="absmiddle" style="cursor:hand " onclick="if(self.gfPop)gfPop.fPopCalendar(document.form2.fechafin);return false;" HIDEFOCUS>
            <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
      </tr>
      <input type=hidden id='Opcion' name='Opcion' value=''> 
    </table></td>
  </tr>
</table> 
<br>
 <table align="center">
   <tr><td>
        <img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod"  height="21"  onclick="if(validarVacio()){ Opcion.value = 'Listado';form2.submit();}" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">        
        <img src="<%=BASEURL%>/images/botones/salir.gif"   name="mod"  height="21"  onclick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">
  </td></tr>
 </table>
<br>
</div>
<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
<%=datos[1]%>

<script language="javascript">
function validarVacio(){
	if(document.form2.fechaini.value==''){
		alert("Debe ingresar la fecha inicial\n   para el rango del reporte")
		return (false);
	}
	if(document.form2.fechafin.value==''){
		alert("Debe ingresar la fecha final \n  para el rango del reporte")
		return (false);
	}
	return (true);

}

</script>

</body>
</html>
