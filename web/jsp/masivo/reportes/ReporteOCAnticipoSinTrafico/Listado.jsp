<!--
- Autor : Ing. Iv�n Dario Devia Acosta
- Date  : 24 de Noviembre de 2006, 3:02 AM 
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Vista que muestra las 
--%>

<%-- Declaracion de librerias--%>
<%@ page contentType="text/html; charset=iso-8859-1" language="java" errorPage="" %>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp" %>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>


<html>
<head>
<title>Reporte OC con Anticipo sin reporte en tr�fico </title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<script type='text/javascript' src="<%=BASEURL%>/js/reporte.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/boton.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/script.js"></script>
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
</head>

<body onLoad="redimensionar();" onResize="redimensionar();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
    <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte de OC con Anticipos sin Tr�fico"/>
</div>

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow:scroll"> 
 <% String style = "simple";
    String position =  "bottom";
    String index =  "center";
	String msg = (String) request.getAttribute("msg");
    int maxPageItems = 15;
    int maxIndexPages = 10;
    UtilFinanzas u = new UtilFinanzas();
    Vector Listado = model.reporteOcAnt.getVector();
    String fechaini = ( request.getParameter("fechaini")!=null )?request.getParameter("fechaini"):"";
    String fechafin   = ( request.getParameter("fechafin")!=null)?request.getParameter("fechafin"):"";
                
    if(Listado!=null && Listado.size()>0) { %> 
  
<form name="form2" method="post" action="<%=CONTROLLER%>?estado=ReporteOcs&accion=ConAnticiposSinReporte" >  

<table width="6000" border="2" align="center">
  <tr>
    <td>  
      <table width="100%" border="0" align="center">
	     <tr>
   		<td width="3000"  height="24"  class="subtitulo1">Reporte OC con Anticipo sin reporte en tr�fico</td>
            	<td width="900"  class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
            </tr>
      </table>			
      <table width="100%" border="1" align="center" bordercolor="#999999" bgcolor="#F7F5F4" id="principal">
        <tr class="tblTitulo" align="center">
			<!-- datos de la planilla -->
            <td width="200" align="center" title="Agencia generadora OC">Agencia Generadora OC</td>
            <td width="200" align="center" title="Usuario generador OC">Despachador</td>
            <td width="100" align="center" title="OC: Numero de planilla">OC</td>    				
			<td width="100" align="center" title="Valor de la planilla">Valor OC</td> 
            <td width="90" align="center" title="Contador de planillas ">Contador de OC</td>    							   				
			<td width="90" align="center" title="Identificaci�n del vehiculo">Placa</td>
			<td width="90" align="center" title="Cedula Propietario del Vehiculo">Cedula<br>Propietario</td>
			<td width="200" align="center" title="Nombre Propietario del Vehiculo">Nombre<br>Propietario</td>   
			<td width="90" align="center" title="Indica si el viaje es lleno o un vacio">Tipo Viaje</td> 
			<td width="90" align="center" title="Numero puestos de control">Nro. Psto. Ctrl.</td>
			<td width="80" align="center" title="Ruta de la OC">Ruta</td>
			<td width="200" align="center" title="Agencia origen del despacho">Agencia Origen</td>
			<td width="200" align="center" title="Agencia destino del despacho">Agencia Destino</td>
			<td width="110" align="center" title="Fecha de Generaci�n de la OC">Fecha Generacion OC</td>    				
			<td width="110" align="center" title="Fecha de impresi�n de OC">Fecha <br>Impresion OC</td>    				
			<td width="90" align="center" title="D�as de generada la OC">D�as Gen. OC</td>    				
			<td width="250" align="center" title="Otras OC asociadas">Otras OC Asociadas</td>
			
			<!-- datos del cumplido -->
			<td width="100" align="center" bgcolor="#CCFF66" title="Fecha cumplido OC" >Fecha<br> Cumplido</td>
			<td width="100" align="center" bgcolor="#CCFF66" title="Cedula Usuario cumplido ">Cedula <br>Usr. Cumpl.</td>
			<td width="200" align="center" bgcolor="#CCFF66" title="Cumplida por">Cumplida por</td>
			
			<!-- datos del anticipo -->
			<td width="200" align="center" bgcolor="#CC9966" title="Agencia que genera el anticipo">Agencia Generadora Anticipo </td>
			<td width="110" align="center" bgcolor="#CC9966" title="Fecha generac�on anticipo">Fecha Gen. Ant.</td>
			<td width="100" align="center" bgcolor="#CC9966" title="Cedula Generador Anticipo">Cedula Gen. Ant.</td>
			<td width="200" align="center" bgcolor="#CC9966" title="Generador Anticipo">Generador Anticipo</td>			
			<td width="100" align="center" bgcolor="#CC9966" title="Valor anticipo">Anticipo</td>
			
			<!-- datos del cheque -->
			<td width="100" align="center" bgcolor="#CC9966" title="Numero del Cheque del anticipo">Cheque</td>
			<td width="100" align="center" bgcolor="#CC9966" title="Fecha de impresion Cheque de anticipo">Fecha Impresion Cheque</td>
			<td width="200" align="center" bgcolor="#CC9966" title="Fecha de impresion Cheque de anticipo">Banco</td>
			<td width="200" align="center" bgcolor="#CC9966" title="Fecha de impresion Cheque de anticipo">Sucursal</td>
			
			<!-- datos de la remesa -->
			<td width="100" align="center" bgcolor="#FFFFCC" title="Estado de la relacion entre la planilla y la remesa">Estado<br>Relacion</td>
			<td width="100" align="center" bgcolor="#FFFFCC" title="Estado de la remesa">Estado<br>Remesa</td>			
			<td width="100" align="center" bgcolor="#FFFFCC" title="OT: Numero de remesa">OT</td>
			<td width="100" align="center" bgcolor="#FFFFCC" title="Valor de la remes">Valor OT</td>
			<td width="400" align="center" bgcolor="#FFFFCC" title="Descripci�n OT">Descripcion OT</td>
			<td width="90" align="center" bgcolor="#FFFFCC" title="Codigo Cliente">Cod. Cliente</td>
			<td width="250" align="center" bgcolor="#FFFFCC" title="Cliente">Cliente</td>
			<td width="100" align="center" bgcolor="#FFFFCC" title="Nit Cliente">Nit. Cliente</td>
			<td width="100" align="center" bgcolor="#FFFFCC" title="Numero Factura Cliente">Factura<br>Cliente</td>
			<td width="100" align="center" bgcolor="#FFFFCC" title="Ultima fecha de pago de la factura">Fecha <br>Ultimo Pago</td>
			
			<!-- otros datos -->
			<td width="100" align="center" bgcolor="#CC9933" title="Clasificac�on">Clasificaci�n</td>
			<td width="130" align="center" bgcolor="#CC9933" title="Rango Demora">Rango Demora</td>
			<td width="90" align="center" bgcolor="#CC9933" title="Calificacion">Calificaci�n</td>
		
        </tr>
        <pg:pager
            items="<%=Listado.size()%>"
            index="<%= index %>"
            maxPageItems="<%= maxPageItems %>"
            maxIndexPages="<%= maxIndexPages %>"
            isOffset="<%= true %>"
            export="offset,currentPageNumber=pageNumber"
            scope="request">
            <%
            for (int i = offset.intValue(), l = Math.min(i + maxPageItems, Listado.size()); i < l; i++) {
                ReporteOCAnticipo rp = (ReporteOCAnticipo) Listado.get(i);
				String estilo = (i % 2 == 0 )?"filagris":"filaazul";
		    %>
			    
		<pg:param name="fechaini" value="<%= fechaini %>"/>				
		<pg:param name="fechafin" value="<%= fechafin %>"/>				
				
        <pg:item>
		
        <tr class="<%= estilo %>" style="cursor:hand; " onDblClick="this.className=(this.className=='filaverde'?'<%= estilo %>':'filaverde');">

			<!-- datos de la planilla -->
            <td class="bordereporte" align="left" title="Agencia generadora OC">&nbsp;<%= rp.getAgcpla() %></td>
            <td class="bordereporte" align="left" title="Usuario generador OC">&nbsp;<%= rp.getDespachador() %></td>
            <td class="bordereporte" align="center" title="OC: Numero de planilla"><%= rp.getNumpla() %></td>    				
			<td class="bordereporte" align="right" title="Valor de la planilla"><%= Util.customFormat(Double.parseDouble(rp.getVlrpla2())) %>&nbsp;</td> 
            <td class="bordereporte" align="center" title="Contador de planillas "><%= rp.getContaroc() %></td>    							   				
			<td class="bordereporte" align="center" title="Identificaci�n del vehiculo"><%= rp.getPlaca() %></td>   
			<td class="bordereporte" align="center" title="Cedula Propietario del Vehiculo"><%= rp.getNitpro() %></td>
			<td class="bordereporte" align="left"  title="Nombre Propietario del Vehiculo">&nbsp;<%= rp.getNompro() %></td>   			
			<td class="bordereporte" align="center" title="Indica si el viaje es lleno o un vacio"><%= rp.getTipo_viaje() %></td> 
			<td class="bordereporte" align="center" title="Numero puestos de control"><%= rp.getPuestocontrol() %></td>
			<td class="bordereporte" align="center" title="Ruta de la OC"><%= rp.getRuta_pla() %></td>
			<td class="bordereporte" align="left" title="Agencia origen del despacho">&nbsp;<%= rp.getAgc_origen() %></td>
			<td class="bordereporte" align="left" title="Agencia destino del despacho">&nbsp;<%= rp.getAgc_destino() %></td>
			<td class="bordereporte" align="center" title="Fecha de Generaci�n de la OC"><%= rp.getCreation_date() %></td>    				
			<td class="bordereporte" align="center" title="Fecha de impresi�n de OC"><%= (rp.getPrinter_date().equals("0099-01-01")?"":rp.getPrinter_date() ) %></td>    				
			<td class="bordereporte" align="center" title="D�as de generada la OC"><%= rp.getDif() %></td>    				
			<td class="bordereporte" align="center" title="Otras OC asociadas"><%= rp.getOcasociadas() %></td>
			
			<!-- datos del cumplido -->
			<td class="bordereporte" align="center" title="Fecha cumplido OC"><%= (rp.getFechacumplido().equals("0099-01-01")?"":rp.getFechacumplido() ) %></td>
			<td class="bordereporte" align="center" title="Cedula Usuario cumplido "><%= rp.getCedulacumplido() %></td>
			<td class="bordereporte" align="left" title="Cumplida por">&nbsp;<%= rp.getNombrecumplido() %></td>
			
			<!-- datos del anticipo -->
			<td class="bordereporte" align="left" title="Agencia que genera el anticipo">&nbsp;<%= rp.getAgenciaanticipo() %> </td>
			<td class="bordereporte" align="center" title="Fecha generac�on anticipo"><%= (rp.getFechaanticipo().equals("0099-01-01")?"":rp.getFechaanticipo() ) %></td>
			<td class="bordereporte" align="center" title="Cedula Generador Anticipo"><%= rp.getCedulaanticipo() %></td>
			<td class="bordereporte" align="left" title="Generador Anticipo">&nbsp;<%= rp.getNombreanticipo() %></td>			
			<td class="bordereporte" align="right" title="Valor anticipo"><%= Util.customFormat(Double.parseDouble(rp.getVlr())) %>&nbsp;</td>
			
			<!-- datos del cheque -->
			<td class="bordereporte" align="center" title="Numero del Cheque del anticipo"><%= rp.getDocument() %></td>
			<td class="bordereporte" align="center" title="Fecha de impresion Cheque de anticipo"><%= (rp.getFecha_cheque().equals("0099-01-01")?"":rp.getFecha_cheque() ) %></td>
			<td class="bordereporte" align="left" title="Banco Anticipo">&nbsp;<%= rp.getBanco() %></td>			
			<td class="bordereporte" align="left" title="Sucursal Anticipo">&nbsp;<%= rp.getSucursal() %></td>			
			
			
			<!-- datos de la remesa -->
			<td class="bordereporte" align="center" title="Estado de la relacion entre la planilla y la remesa"><%= rp.getEstado_relacion() %></td>
			<td class="bordereporte" align="center" title="Estado de la remesa"><%= rp.getEstado_remesa() %></td>						
			<td class="bordereporte" align="center" title="OT: Numero de remesa"><%= rp.getNumrem() %></td>
			<td class="bordereporte" align="right" title="Valor de la remesa"><%= Util.customFormat(Double.parseDouble(rp.getVlrrem2())) %>&nbsp;</td>
			<td class="bordereporte" align="left" title="Descripci�n OT">&nbsp;<%= rp.getDescripcion() %></td>
			
			<td class="bordereporte" align="center" title="Codigo Cliente"><%= rp.getCodCliente() %></td>
			<td class="bordereporte" align="left" title="Cliente">&nbsp;<%= rp.getNomCliente() %></td>
			<td class="bordereporte" align="center" title="Nit Cliente"><%= rp.getCliente() %></td>			
			<td class="bordereporte" align="center" title="Numero Factura Cliente"><%= rp.getFacturacliente() %></td>
			<td class="bordereporte" align="center" title="Ultima fecha de pago de la factura"><%= (rp.getFecha_pago_remesa().equals("0099-01-01")?"":rp.getFecha_pago_remesa() ) %></td>			
			
			<!-- otros datos -->
			<td class="bordereporte" align="center" title="Clasificac�on"><%= rp.getClasificacion() %></td>
			<td class="bordereporte" align="center" title="Rango Demora"><%= rp.getRango() %></td>
			<td class="bordereporte" align="center" title="Calificacion"><%= rp.getCalificacion() %></td>                                                                                                    

	   </tr>
        </pg:item>
          <%}%>
        <tr class="bordereporte">
          <td height="65" colspan="32" align="left" class="bordereporte">
            <div style=" width:30% ">
                <pg:index>
                <jsp:include page="/WEB-INF/jsp/googlesinimg.jsp" flush="true"/>        
                </pg:index>
            </div>
          </td>
        </tr>
        </pg:pager>
      </table>
    </td>

				
   </tr>
</table>


 <input type=hidden id='opcion' name='opcion' value=''> 
 <input type=hidden id='fechaini' name='fechaini' value='<%=fechaini%>'> 
 <input type=hidden id='fechafin' name='fechafin' value='<%=fechafin%>'> 
  <%} %>
  

<table  border="0" align="left">
    <tr>     
      <td align="left" ><img src="<%=BASEURL%>/images/botones/regresar.gif"      name="imgaceptar" onclick="window.location='<%=CONTROLLER%>?estado=Menu&accion=Cargar&carpeta=/jsp/masivo/reportes/ReporteOCAnticipoSinTrafico&pagina=Consulta.jsp&marco=no'" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>
      <% if (Listado!=null && !Listado.isEmpty()) { %><td align="left" ><img src="<%=BASEURL%>/images/botones/exportarExcel.gif"  name="imgaceptar" onclick="opcion.value = 'Excel'; form2.submit();"  style="cursor:hand"></td><% } %>
	  <td align="left" ><img src="<%=BASEURL%>/images/botones/salir.gif"      name="imgsalir" onclick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand"></td>	  
    </tr>
</table>  
  <br>
<br>

  
   <% if(msg!=null && !msg.trim().equals("")) { %>
        <p>
            <table border="2" align="center">
                <tr>
                    <td>
                        <table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                            <tr>
                                <td width="229" align="center" class="mensajes"><%=msg%></td>
                                <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                                <td width="58">&nbsp;</td>
                            </tr>
                        </table>                     
                    </td>
                </tr>
            </table>
        </p>

  <%}%>	
</form>	
</div> 
</body>
</html>


