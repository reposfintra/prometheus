<!--
- Autor : Ing. Enrique De Lavalle
- Date  : 1 de Diciembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que pide la fecha de inicio y final para generar el reporte de las  OC de viajes Vacios con anticipos sin reportes
--%>

<%-- Declaracion de librerias--%>

<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@page import="com.tsp.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@taglib uri="http://jsptags.com/tags/navigation/pager" prefix="pg" %>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*, java.text.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>

<%
 String mensaje= request.getParameter("Mensaje");
 
 String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
 String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
%>

<html>
<head>

<title>Consulta Reporte OC de vacios con anticipo sin reporte</title>

<link href="<%= BASEURL %>/css/estilomenuemergente.css" rel='stylesheet'> 
<link href="../../trafico/css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="../../../css/estilostsp.css" rel="stylesheet" type="text/css">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">

<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script src='<%=BASEURL%>/js/date-picker.js'></script>
<script type='text/javascript' src="<%= BASEURL %>/js/reporte.js"></script>
<script language="JavaScript" type="text/javascript" src="<%=BASEURL%>/js/validarDocumentos.js"></script>

</head>

<%-- Inicio Body --%>
<body onLoad="cargar();">

	<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte OC de vacios con anticipo"/>
	</div>
	
	<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow:scroll; ">
	<form name="forma" method="post" action="<%=CONTROLLER%>?estado=ReporteOC&accion=AnticipoVacio">
		<table width="500" border="2" align="center">
			<tr>
				<td>
					<table width="100%" align="center"  class="tablaInferior">
						<tr class="fila">
							<td width="57%" align="left" class="subtitulo1">&nbsp;Consulta De Reportes - Datos Iniciales</td>
							<td width="38%" class="barratitulo"><%=datos[0]%><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"align="left"></td>
						</tr>
					</table>
					<table width="100%" border="0" align="center" class="tablaInferior">
                           <tr class="fila">
                               <td><strong>Fecha inicial </strong></td>
                               <td> <input name='fechaini' type='text' class="textbox" id="fechaini" style='width:120' value='' readonly>
                                    <a href="javascript:void(0);" class="link" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.forma.fechaini);return false;"  HIDEFOCUS > <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16"
                                     border="0" alt="De click aqu&iacute; para ver el calendario."></a> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
                            </tr>
                            <tr class="fila" >
                               <td><strong>Fecha final </strong></td>
                                <td> <input name='fechafin' type='text' class="textbox" id="fechafin" style='width:120' value='' readonly>
                                     <a href="javascript:void(0);" class="link" onFocus="if(self.gfPop)gfPop.fPopCalendar(document.forma.fechafin);return false;"  HIDEFOCUS > <img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16"
                                      border="0" alt="De click aqu&iacute; para ver el calendario."></a> <img src="<%= BASEURL%>/images/botones/iconos/obligatorio.gif" width="10" height="10"></td>
                            </tr>
                                              
                                    <input type=hidden id='Opcion' name='Opcion' value=''> 
                   </table>
				</td>
			</tr>
		</table>
		<p align="center">
			<img src="<%=BASEURL%>/images/botones/aceptar.gif" name="mod"  height="21"  onclick="Opcion.value = 'Listado';forma.submit();"  onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand">        
   			<img src="<%=BASEURL%>/images/botones/salir.gif"   name="mod"  height="21"  onclick="window.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);" style="cursor:hand" >
		</p>
	</form>
	 <%if(mensaje != null){ %>
               <table border="2" align="center">
                  <tr>
                    <td><table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
                       <tr>
                        <td width="229" align="center" class="mensajes"><%=mensaje%> 
                       </td>
                       <td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
                        <td width="58">&nbsp;</td>
                      </tr>
                    </table>
                    </td>
                  </tr>
                </table>
            <% }%>	 
	</div>
	<iframe width="188" height="166" name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins_24.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
	 <%=datos[1]%>
</body>


<script language="javascript">


function formato2digitos( num ){
	return (num<10? '0':'') + num;
}

function formato (fecha){
	return fecha.getYear() + '-' + formato2digitos(fecha.getMonth()+1) + '-' + formato2digitos(fecha.getDate());
}
		
function cargar(){
		var fechaActual  =  new Date();
		var fechaMenos30 =  new Date();
		fechaMenos30.setDate(fechaActual.getDate()-30);
		forma.fechaini.value = formato(fechaMenos30);
		forma.fechafin.value = formato(fechaActual);
}

</script>

</html>

