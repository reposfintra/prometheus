<!--
- Autor : Ing. Jose de la rosa
- Date  : 05 de Abril de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la migracion de cumplidos dada una fecha
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Reporte Stantares Inconsistentes</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<% String msg = (String) request.getParameter("msg");%>
</head>
<%-- Inicio Body --%>
<body onLoad="redimensionar();" onResize="redimensionar();">
	<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte Standares Inconsistentes"/>
	</div>
	
	<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">  
		<form name="forma" method="post" action="<%=CONTROLLER%>?estado=Reporte&accion=StandarMalos">
			<table width="500" border="2" align="center">
              <tr>
                <td><table width="100%" class="tablaInferior">
                    <tr class="barratitulo">
                      <td align="left">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="55%" class="subtitulo1"> Reporte Standares Inconsistentes</td>
                            <td width="45%"><img src="<%=BASEURL%>/images/titulo.gif"></td>
                          </tr>
                      </table></td>
                    </tr>
                    <tr class="fila">
                      <td align="left" ><div align="center">Haga clic en Exportar para iniciar el proceso </div></td>
                    </tr>
                </table></td>
              </tr>
            </table>
			<p align="center">
				<img src="<%=BASEURL%>/images/botones/exportarExcel.gif" title="Generar Reporte" style="cursor:hand" name="exportar"  onclick="forma.submit();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
				<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >
			</p>
		</form>
		<% if(!msg.equals("")){%>
		<br>
		<table border="2" align="center">
			<tr>
				<td>
					<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						<tr>
							<td width="229" align="center" class="mensajes"><%=msg%></td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<%}%>
	</div>
	<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>
