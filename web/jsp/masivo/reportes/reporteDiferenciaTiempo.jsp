<!--
- Autor : Ing. Jose de la rosa
- Date  : 22 de Noviembre de 2005
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que maneja la migracion de cumplidos dada una fecha
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%>
<%@page errorPage="../error/ErrorPage.jsp"%>
<%@page import="java.util.*"%>
<%@page import="com.tsp.operation.model.*"%>
<%@page import="com.tsp.util.*"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>

<html>
<head>
<title>Reporte Diferencia Tiempo Carge Y Descarge</title>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%= BASEURL %>/js/validar.js"></script>
<% String msg = (String) request.getParameter("msg");%>
</head>
<%-- Inicio Body --%>
<body onLoad="redimensionar();" onResize="redimensionar();">
	<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
	<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Reporte Diferencia Tiempo Carge Y Descarge"/>
	</div>
	
	<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;">  
		<form name="form1" method="post" action="<%=CONTROLLER%>?estado=ReporteDiferencia&accion=Tiempo">
			<table width="500" border="2" align="center">
				<tr>
					<td>
						<table width="100%" class="tablaInferior">
							<tr>
								<td width="55%" class="subtitulo1" colspan="2">&nbsp;Reporte Diferencia Carge Y Descarge</td>
								<td class="barratitulo" colspan="2"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
							</tr>
							<tr class="fila">
								<td> &nbsp;Codigo Cliente
								</td>
								<td colspan="3">
									<input name="c_cliente" type="text" class="textbox" id="codcli" size="10" maxlength="10" onKeyPress="soloAlfa(event)">
									<a onClick="window.open('<%=CONTROLLER%>?estado=Menu&accion=Cargar&pagina=consultasCliente.jsp&marco=no&carpeta=/consultas','','status=yes,scrollbars=yes,width=550,height=250,resizable=yes')" style="cursor:hand"><img height="20" width="25" src="<%=BASEURL%>/images/botones/iconos/lupa.gif"></a>
								</td>
							</tr>							
							<tr class="fila">
								<td align="left" > &nbsp;Fecha Inicial </td>
								<td valign="middle" >
									<input name="c_fecha_ini" type="text" readonly class="textbox" id="c_fecha_ini" size="12">
									<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.c_fecha_ini);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a></td>
							
								<td align="left" > &nbsp;Fecha Inicial </td>
								<td valign="middle" >
									<input name="c_fecha_fin" type="text" readonly class="textbox" id="c_fecha_fin" size="12">
									<a href="javascript:void(0)" onclick="if(self.gfPop)gfPop.fPopCalendar(document.form1.c_fecha_fin);return false;" HIDEFOCUS><img src="<%=BASEURL%>\js\Calendario\cal.gif" width="16" height="16" border="0" alt="De click aqui para escoger la fecha"></a></td>
							</tr>	
													
						</table>
					</td>
				</tr>
		  </table>
			<p align="center">
				<img src="<%=BASEURL%>/images/botones/exportarExcel.gif" title="Generar Reporte" style="cursor:hand" name="exportar"  onclick="return valiTCamposLlenos(form1);" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">&nbsp;
				<img src="<%=BASEURL%>/images/botones/salir.gif" style="cursor:hand" name="salir" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >
			</p>
		</form>
		<% if(msg!=null){%>
		<br>
		<table border="2" align="center">
			<tr>
				<td>
					<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
						<tr>
							<td width="229" align="center" class="mensajes"><%=msg%></td>
							<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
							<td width="58">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<%}%>
	</div>
	<iframe width=188 height=166 name="gToday:normal:agenda.js:gfPop:plugins.js" id="gToday:normal:agenda.js:gfPop:plugins.js" src="<%=BASEURL%>/js/Calendario/ipopeng.htm" scrolling="no" frameborder="0" style="visibility:visible; z-index:999; position:absolute; left:-500px; top:0px;"> </iframe>
</body>
</html>
