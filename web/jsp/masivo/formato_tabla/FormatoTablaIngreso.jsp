<!--
- Autor : Ing. Jose de la rosa
- Date  : 25 Noviembre de 2006
- Copyrigth Notice : Fintravalores S.A. S.A
-->
<%--
-@(#)
--Descripcion : Pagina JSP, que permite buscar las facturas de los clientes
--%>

<%-- Declaracion de librerias--%>
<%@page contentType="text/html"%>
<%@page session="true"%> 
<%@page import="java.util.*" %>
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.*"%>
<%@include file="/WEB-INF/InitModel.jsp"%>
<%@page import="com.tsp.operation.model.beans.*"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<%
	String path = application.getRealPath(model.menuService.getRealUrl(request.getRequestURI()));
	String datos[] = model.menuService.getContenidoMenu(BASEURL,request.getRequestURI(),path);
    Vector vec = model.formato_tablaService.getVecftabla ();
	String tabla = request.getParameter("c_tabla")!=null?request.getParameter("c_tabla"):"";%>
<html>
<head><title>Formato Tabla</title></head>
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
<script type='text/javascript' src="<%=BASEURL%>/js/general.js"></script>
<body onLoad="redimensionar();" onResize="redimensionar();" onKeyDown="onKeyDown();">
<div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
<jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Ingresar Formato Tabla"/>
</div> 

<div id="capaCentral" style="position:absolute; width:100%; height:85%; z-index:0; left: 0px; top: 100px; overflow: scroll;"> 
<form action="<%=CONTROLLER%>?estado=Formato&accion=Tabla&opcion=1" method="post" name="form1" >
	<table width="450" border="2" align="center">
		<tr>
			<td>
				<table width="100%" class="tablaInferior">
					<tr class="fila">
					<td colspan="2" >
						<table width="100%"  border="0" cellspacing="1" cellpadding="0">
							<tr>
								<td width="50%" class="subtitulo1">&nbsp;Formato de Tablas</td>
								<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20" align="left"><%=datos[0]%></td>
							</tr>
						</table>
					</td>
					</tr>
					<tr class="fila">
						<td width="24%">Nombre Tabla:</td>
						<td width="76%"><input name="c_tabla" type="text" class="textbox" value="<%=tabla%>" maxlength="60" style='width:90%;'> 
										<img src="<%=BASEURL%>/images/botones/iconos/lupa.gif" width="20" height="20" style="cursor:hand" title="Buscar Nombre Tabla" name="buscar"  onClick="return validarNombreTabla();" ></td>
					</tr>			
				</table>
			</td>
		</tr>
	</table>
	<%if( ( vec == null || vec.size() == 0 ) && request.getAttribute ("mensaje") == null){%>
		<br>
		<div align="center">
			<img src="<%=BASEURL%>/images/botones/salir.gif"  name="salir" style="cursor:hand" title="Salir al Menu Principal" onMouseOver="botonOver(this);" onClick="parent.close();" onMouseOut="botonOut(this);" >
		</div>
	<%}%>
	<br>
  </form>
	<%if( vec != null && vec.size() > 0 ){%>
	<form action="<%=CONTROLLER%>?estado=Formato&accion=Tabla&opcion=2" method="post" name="form2" >
	
		<table width="1000" border="2" align="center">
			<tr>
				<td>
					<TABLE width="100%" align="center"  class="tablaInferior">
				        <tr class="fila">
							<td width="50%" class="subtitulo1">&nbsp;Listado Formato de Tablas</td>
							<td width="50%" class="barratitulo"><img src="<%=BASEURL%>/images/titulo.gif" width="32" height="20"></td>
						</tr>
						<tr>
							<td colspan="2">
								<table width="100%" class="tablaInferior">
									<tr class="fila">
										<td width="30%">&nbsp;</td>
										<td width="10%">Formato</td>
										<td width="30%"><select name="c_formato" id="c_formato" class="textbox" style='width:90%;'>
														<% Vector vecfor = model.formato_tablaService.getVecformato ();
														if(vecfor!=null){
														   for(int k = 0; k<vecfor.size(); k++){
																Formato_tabla f = (Formato_tabla) vecfor.get(k); %>
																<option value="<%=f.getFormato()%>"><%=f.getNombre_formato()%></option>
															<%}
														}%>
													  </select></td>
										<td width="30%">&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
						  <td colspan="2" align="center">
						  <table width="100%" border="1" bordercolor="#999999">
                            <tr class="tblTitulo">
                              <td width="3%" height="24" align="center">Key</td>
                              <td width="2%" nowrap align="center"><input type="checkbox" name="checkboxall" id ="checkboxall" value="checkbox" onClick="seleccionartodo('<%=vec.size()%>',this.checked);"></td>
                              <td width="14%" align="center">Nombre Campo Tabla </td>
                              <td width="5%"  align="center">Orden</td>
							  <td width="14%" align="center">Valor Default</td>
                              <td width="14%" align="center">Nombre Campo JSP</td>
                              <td width="15%" align="center">Nombre Etiqueta JSP </td>
                              <td width="5%"  align="center">Validar</td>
                              <td width="14%" align="center">Tabla a Validar</td>
                              <td width="14%" align="center">Campo a Validar</td>
                            </tr>
                            <tr>
							  <%
							  	boolean disa = false, esSerial = false, sw = false;
								int cont = 0;
								if( session.getAttribute ("vecTemp")!=null ){
									Vector v = (Vector)session.getAttribute ("vecTemp");
									for(int i=0; i< v.size(); i++){
										Formato_tabla formatoTemp = (Formato_tabla)v.get(i);
										if(formatoTemp.getPrimaria().equals("S") ){
											disa = true;
											cont++;
										}
										if( formatoTemp.getTipo_campo().equals("int4") )
											esSerial = true;
									}
									if( cont == 1 && esSerial == true ){
										disa = false;
									}
								}
								else{
									for(int i=0; i<vec.size();i++){
										Formato_tabla formato = (Formato_tabla)vec.get(i);
										if(formato.getPrimaria().equals("S") ){
											disa = true;
											cont++;
										}
										if( formato.getTipo_campo().equals("int4") )
											esSerial = true;
									}
									if( cont == 1 && esSerial == true ){
										disa = false;
									}
								}
								
								for(int i=0; i<vec.size();i++){
									Formato_tabla formato = (Formato_tabla)vec.get(i);
									sw = formato.getPrimaria().equals("S")?true:false;
									if( request.getAttribute ("VecCamp")!=null ){
										Vector v = (Vector)request.getAttribute ("VecCamp");
										for(int j=0; j< v.size(); j++){
											String ca = (String)v.get(j);
											if( ca.equals( formato.getCampo_tabla() ) )
												sw = true;
										}
									}
									String m = "", dek="";
									int longitud = 0;
									m = m.valueOf ( formato.getLongitud() );
									String []def = m.split("\\.");
									if (def.length==2){
										if( Integer.parseInt(def[1]) > 0 ){
											dek ="soloDigitos(event,'decOK')";
											longitud = (int)Integer.parseInt(def[0])-Integer.parseInt(def[1]);
										}
										else{
											dek = "soloDigitos(event,'decNO')";
											longitud = (int)formato.getLongitud();
										}
									}
									else{
										dek = "soloDigitos(event,'decNO')";
										longitud = (int)formato.getLongitud();
									}
									
									if( formato.getTipo_campo().equals("moneda") )
										dek ="soloDigitos(event,'decOK')";
									String validado = formato.getTipo_campo().equals("int")||formato.getTipo_campo().equals("decimal")||formato.getTipo_campo().equals("numeric")
													  ||formato.getTipo_campo().equals("int2")||formato.getTipo_campo().equals("int4")||formato.getTipo_campo().equals("int8")||formato.getTipo_campo().equals("float4")
													  ||formato.getTipo_campo().equals("float8")||formato.getTipo_campo().equals("serial4")||formato.getTipo_campo().equals("serial8")||formato.getTipo_campo().equals("moneda")?dek:"";
									%>
									<tr class="filaazul">
									  <td width="3%"   class="bordereporte" align="center"><input type="checkbox" name="c_primaria" id="c_primaria<%=i%>" value="<%=i%>" <%=formato.getPrimaria().equals("S")?"checked":""%> <%=disa==true? "disabled":" onclick='if (this.checked) { form2.checkbox"+i+".checked=true; }' "%> ></td>
									  <td width="2%"   class="bordereporte" align="center"><input type="checkbox" name="checkbox" id="checkbox<%=i%>" value="<%=i%>" <%=sw==true?"checked":""%>
											onclick="
												if( this.checked != true ){ 
													<%if( !formato.getPrimaria().equals("S") ){%>
														form2.c_primaria<%=i%>.checked=false;
													<%}else{%>
														<%if( cont == 1 && esSerial == true ){%>
															form2.c_primaria<%=i%>.checked=false;
														<%}else{%>
															alert('No puede deshabilitar el campo de la llave'); this.checked = true; 
														<%}%>
													<%}%> 
												}" ></td>
									  <td width="14%"  class="bordereporte" align="center"><input name="c_campo<%=i%>" type="text" class="textbox" id="c_campo<%=i%>" style="width:95%" value="<%=formato.getCampo_tabla()%>" readonly></td>
									  <td width="5%"   class="bordereporte" align="center"><input name="c_orden<%=i%>" type="text" class="textbox" id="c_orden<%=i%>" value="<%=formato.getOrden()==0?"":""+formato.getOrden()%>" onblur="existeOrden('<%=vec.size()%>','<%=i%>')" style="width:95%" maxlength="8"  onKeyPress="soloDigitos(event,'decNO')"></td>
									  <td width="14%"  class="bordereporte" align="center"><input name="c_default<%=i%>" type="text" class="textbox" id="c_default<%=i%>" value="<%=formato.getValor_campo()%>" style="width:95%" <%=!formato.getTipo_campo().equals("text")?"maxlength='"+longitud+"'":""%> onKeyPress="<%=validado%>" ></td>
									  <td width="14%"  class="bordereporte" align="center"><input name="c_campoJSP<%=i%>" type="text" class="textbox" id="c_campoJSP<%=i%>" value="<%=formato.getCampo_jsp()%>" style="width:95%" maxlength="50"></td>
									  <td width="15%"  class="bordereporte" align="center"><textarea name="c_titulo<%=i%>" class="textbox" id="c_titulo<%=i%>" style="width:95%" rows="1"><%=formato.getTitulo()%></textarea></td>
									  <td width="5%"   class="bordereporte" align="center"><select name="c_validar<%=i%>" id="c_validar<%=i%>" class="textbox" style='width:95%;' onChange="validarActivado('<%=i%>');">
																							<option value="S" <%=formato.getValidar().equals("S")?"selected":""%>>SI</option>
																							<option value="N" <%=formato.getValidar().equals("N")?"selected":""%>>NO</option>
																						  </select></td>
									  <td width="14%"  class="bordereporte" align="center"><input name="c_tabla_val<%=i%>" type="text" class="textbox" id="c_tabla_val<%=i%>" value="<%=formato.getTabla_val()%>" <%=formato.getValidar().equals("N")?"disabled":""%> style="width:95%"></td>
									  <td width="14%"  class="bordereporte" align="center"><input name="c_campo_val<%=i%>" type="text" class="textbox" id="c_campo_val<%=i%>" value="<%=formato.getCampo_val()%>" <%=formato.getValidar().equals("N")?"disabled":""%> style="width:95%"></td>
									</tr>
									<input name="c_tipo<%=i%>" type="hidden" value='<%=formato.getTipo_campo()%>'>
									<input name="c_longitud<%=i%>" type="hidden" value='<%=formato.getLongitud()%>'>
									<input name="primaria<%=i%>" type="hidden" value='<%=formato.getPrimaria()%>'>
								<%}%>
                          </table>
						  </td>
					  </tr>
			  </TABLE>			  
			  </td>
			</tr>
  		</table>
		<input name="c_tabla" type="hidden" value="<%=tabla%>">
		<p align="center">
		<img src="<%=BASEURL%>/images/botones/aceptar.gif"  height="21" name="imgaceptar"  onMouseOver="botonOver(this);" onClick="validacionReporte('<%=vec.size()%>');" onMouseOut="botonOut(this);" style="cursor:hand">&nbsp;
		<img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close(); this.disabled=true;" onMouseOut="botonOut(this);" style="cursor:hand">
		</p>
  </form>
	<%--Mensaje de informacion--%>
	<%}else{
		if( request.getAttribute ("mensaje") != null ){%>
			<br>
			<table border="2" align="center">
				<tr>
					<td>
						<table width="100%"  border="1" align="center"  bordercolor="#F7F5F4" bgcolor="#FFFFFF">
							<tr>
								<td width="229" align="center" class="mensajes"><%=(String)request.getAttribute ("mensaje")%></td>
								<td width="29" background="<%=BASEURL%>/images/cuadronaranja.JPG">&nbsp;</td>
								<td width="58">&nbsp;</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<p align="center">
				<img src="<%=BASEURL%>/images/botones/salir.gif"  height="21" name="imgsalir"  onMouseOver="botonOver(this);" onClick="window.close(); this.disabled=true;" onMouseOut="botonOut(this);" style="cursor:hand">
			</p>		
		<%}
	}
	if( request.getAttribute ("mensajeScript") != null ){%>
		<script>
			alert('<%=(String)request.getAttribute ("mensajeScript")%>');
		</script>
	<%}%>
</div>
<%=datos[1]%>
</body>
</html>
<script>
function validarNombreTabla(){
	if(form1.c_tabla.value == ''){
		alert('Debe llenar el nombre de la tabla');
		form1.c_tabla.focus();
		return false;
	}
	else{
		form1.submit();
	}
}

function validarActivado(valor){
	var validar = document.getElementById("c_validar"+valor);
	var tipo    = document.getElementById("c_tipo"+valor);
	if( validar.value == 'N' ){
		document.getElementById("c_tabla_val"+valor).disabled = true;
		document.getElementById("c_campo_val"+valor).disabled = true;
	}
	else{
		document.getElementById("c_tabla_val"+valor).disabled = false;
		document.getElementById("c_campo_val"+valor).disabled = false;	
	}
	if( tipo.value == "TEXTBOX" ){
		document.getElementById("c_longitud"+valor).disabled = true;
	}
	else{
		document.getElementById("c_longitud"+valor).disabled = false;	
	}
}

function seleccionartodo(size,check){
	if(check == true){
		for (i=0;i<size;i++){
			var checkbox="checkbox"+i;
			document.getElementById(checkbox).checked=true;
		}    
	}else{
		for (i=0;i<size;i++){
			var checkbox="checkbox"+i;
			document.getElementById(checkbox).checked=false;
		}  
	}
}

function validacionReporte( tam ){
    var num ="", key = "";
	var estaDisabled = false;
    for( var i=0; i < tam ; i++ ){
        var checkbox="checkbox"+i;
        if(document.getElementById(checkbox).checked){
            num+= document.getElementById(checkbox).value;
			if( document.getElementById("c_tipo"+i).value != "TEXTBOX" ){
				if( document.getElementById("c_longitud"+i).value == "" ){
					alert("El campo longitud no debe ser vacio!");
					document.getElementById("c_longitud"+i).focus();
					return false;
				}
			}
			if( document.getElementById("c_orden"+i).value == "" ){
				alert("El campo orden no debe ser vacio!");
				document.getElementById("c_orden"+i).focus();
				return false;
			}
			else if( document.getElementById("c_campoJSP"+i).value == "" ){
				alert("El campo nombre JSP no debe ser vacio!");
				document.getElementById("c_campoJSP"+i).focus();
				return false;
			}
			else if( document.getElementById("c_validar"+i).value == "S" ){
				if( document.getElementById("c_tabla_val"+i).value == "" ){
					alert("El campo nombre tabla a validar no debe ser vacio!");
					document.getElementById("c_tabla_val"+i).focus();
					return false;
				}
				else if( document.getElementById("c_campo_val"+i).value == "" ){
					alert("El campo nombre campo a validar no debe ser vacio!");
					document.getElementById("c_campo_val"+i).focus();
					return false;
				}
			}
        }
		var primaria="c_primaria"+i;
        if( document.getElementById(primaria).disabled == true && estaDisabled == false ){
			estaDisabled = true;
		} 
        if(document.getElementById(primaria).checked && document.getElementById(primaria).disabled == false){
            key+= document.getElementById(primaria).value;
        }
    }
    if( num == "" ){
        alert("Debe seleccionar por lo menos un campo");
    }else{
	    if( key == "" && estaDisabled == false ){
        	alert("Debe seleccionar por lo menos una llave");
		}
		else{
			if(form2.c_formato.value == ''){
				alert("No existe formato disponible");
				return false;
			}
			else{
				form2.imgaceptar.disabled=true;
				form2.imgaceptar.src = "<%=BASEURL%>/images/botones/aceptarDisable.gif";
				form2.submit();
			}
		}
    }    
}

function existeOrden( tam, i ){
	var cont = 0;
	var existe_orden = false;
	for( var k=0; k < tam ; k++ ){
		if( i != k ){
			if( document.getElementById("c_orden"+k).value == document.getElementById("c_orden"+i).value && document.getElementById("c_orden"+k).value != '' && document.getElementById("c_orden"+i).value != ''){
				cont++;
			}
		}
	}
	if( cont > 0 ){
		existe_orden = true;
	}
	if( existe_orden == true ){
		document.getElementById("c_orden"+i).value = '';
		alert("El orden ya existe");
		document.getElementById("c_orden"+i).focus();
		return false;
	}
}
</script>