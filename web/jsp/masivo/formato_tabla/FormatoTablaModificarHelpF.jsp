<!--  
	 - Author(s)       :      Jose de la Rosa
	 - Description     :      AYUDA FUNCIONAL - Modificar Formato Tabla
	 - Date            :      29/11/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<%@ include file="/WEB-INF/InitModel.jsp"%>
<HTML>
<HEAD>
<TITLE>AYUDA FUNCIONAL - Modificar Formato Tabla</TITLE>
<META http-equiv=Content-Type content="text/html; charset=windows-1252">
<link href="../css/estilostsp.css" rel="stylesheet" type="text/css"> 
<link href="<%=BASEURL%>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script> 
</HEAD>
<BODY> 
<% String BASEIMG = BASEURL +"/images/ayuda/masivo/formato_tabla/"; %>
  <table width="95%"  border="2" align="center">
    <tr>
      <td height="117" >
        <table width="100%" border="0" align="center">
          <tr  class="subtitulo">
            <td height="20"><div align="center">MANUAL DE FORMATO TABLA</div></td>
          </tr>
          <tr class="subtitulo1">
            <td>Descripci&oacute;n del funcionamiento del programa para Modificar Acuerdos Especiales.</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center">
              <p>&nbsp;</p>
              </div></td>
          </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">En la siguiente pantalla se busca el nombre de la tabla a ingresar.</p>
            </td>
          </tr>
            <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG src="<%=BASEIMG%>VistaModificar1.JPG" border=0 ></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            	<p class="ayudaHtmlTexto">El sistema verifica si la tabla existe en postgres, si no existe le saldr&aacute; en la pantalla el siguiente mensaje. </p>
            </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG  src="<%=BASEIMG%>MensajeNoExistenDatos.JPG" border=0 ></div></td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            	<p class="ayudaHtmlTexto">Si la tabla existe en postgres, le saldr&aacute; la siguiente pantalla donde llenar� los datos correspondientes.</p>
            </td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG  src="<%=BASEIMG%>VistaModificar2.JPG" border=0 ></div></td>
          </tr>		  
          <tr>
            <td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
            	<p class="ayudaHtmlTexto">Los campos que se validan dependen solo de los campos que se chequen en la vista. El sistema verifica del campo seleccionado si esta lleno el campo orden, si no esta lleno le saldr&aacute; en la pantalla el siguiente mensaje. </p>
          	</td>
          </tr>
          <tr>
            <td  class="ayudaHtmlTexto"><div align="center"><IMG  src="<%=BASEIMG%>MensajeOrdenVacio.JPG" border=0 ></div></td>
            </tr>
            <tr>
				<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
					<p class="ayudaHtmlTexto">El sistema verifica si el orden ya existe, si no lo estan le saldr&aacute; en la pantalla el siguiente mensaje. </p>
				</td>
            </tr>
				<tr>
				<td  class="ayudaHtmlTexto"><div align="center"><IMG  src="<%=BASEIMG%>MensajeOrdenExistente.JPG" border=0 ></div></td>
            </tr>					
            <tr>
				<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
					<p class="ayudaHtmlTexto">El sistema verifica si el nombre del campo JSP esta lleno, si no lo estan le saldr&aacute; en la pantalla el siguiente mensaje. </p>
				</td>
            </tr>
				<tr>
				<td  class="ayudaHtmlTexto"><div align="center"><IMG  src="<%=BASEIMG%>MensajeCampoJSPVacio.JPG" border=0 ></div></td>
            </tr>
            <tr>
				<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
					<p class="ayudaHtmlTexto">El sistema verifica si el nombre de la etiqueta JSP esta lleno, si no lo estan le saldr&aacute; en la pantalla el siguiente mensaje. </p>
				</td>
            </tr>
				<tr>
				<td  class="ayudaHtmlTexto"><div align="center"><IMG  src="<%=BASEIMG%>MensajeEtiquetaVacio.JPG" border=0 ></div></td>
            </tr>
            <tr>
				<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
					<p class="ayudaHtmlTexto">El sistema verifica si el nombre de la tabla a validar esta lleno, si no lo estan le saldr&aacute; en la pantalla el siguiente mensaje. </p>
				</td>
            </tr>
				<tr>
				<td  class="ayudaHtmlTexto"><div align="center"><IMG  src="<%=BASEIMG%>MensajeTablaValidarVacio.JPG" border=0 ></div></td>
            </tr>
            <tr>
				<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
					<p class="ayudaHtmlTexto">El sistema verifica si el nombre del campo a validar esta lleno, si no lo estan le saldr&aacute; en la pantalla el siguiente mensaje. </p>
				</td>
            </tr>
				<tr>
				<td  class="ayudaHtmlTexto"><div align="center"><IMG  src="<%=BASEIMG%>MensajeCampoValidarVacio.JPG" border=0 ></div></td>
            </tr>
            <tr>
				<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
					<p class="ayudaHtmlTexto">El sistema verifica si se ha chequeado algun campo, si no lo esta le saldr&aacute; en la pantalla el siguiente mensaje. </p>
				</td>
            </tr>
				<tr>
				<td  class="ayudaHtmlTexto"><div align="center"><IMG  src="<%=BASEIMG%>MensajeSeleccionCampo.JPG" border=0 ></div></td>
            </tr>		
            <tr>
				<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
					<p class="ayudaHtmlTexto">El sistema verifica si se ha chequeado algun campo con llave primaria, si no lo esta le saldr&aacute; en la pantalla el siguiente mensaje. </p>
				</td>
            </tr>
				<tr>
				<td  class="ayudaHtmlTexto"><div align="center"><IMG  src="<%=BASEIMG%>MensajeCampoLLave.JPG" border=0 ></div></td>
            </tr>				
			<tr>
				<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
				<p class="ayudaHtmlTexto">El sistema verifica si los datos estan digitados correctamente le saldr&aacute; en la pantalla el siguiente mensaje. </p>
            </td>
            </tr>
            <tr>
				<td  class="ayudaHtmlTexto"><div align="center"><IMG  src="<%=BASEIMG%>MensajeModificacion.JPG" border=0 ></div></td>
            </tr>
			<tr>
				<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
					<p class="ayudaHtmlTexto">Si se va a eliminar el formato le saldr&aacute; en la pantalla el siguiente mensaje para confirmar si esta seguro que se va a eliminar. </p>
				</td>
            </tr>
				<tr>
				<td  class="ayudaHtmlTexto"><div align="center"><IMG  src="<%=BASEIMG%>MensajeConfirmacionEliminacion.JPG" border=0 ></div></td>
            </tr>	
			<tr>
				<td  class="ayudaHtmlTexto"><p class="ayudaHtmlTexto">&nbsp;</p>
				<p class="ayudaHtmlTexto">El sistema verifica si la confirmacion de la eliminacion fu� exitosa le saldr&aacute; en la pantalla el siguiente mensaje. </p>
            </td>
            </tr>
            <tr>
				<td  class="ayudaHtmlTexto"><div align="center"><IMG  src="<%=BASEIMG%>MensajeEliminar.JPG" border=0 ></div></td>
            </tr>			
      </table></td>
    </tr>
  </table>
  <p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</BODY>
</HTML>
