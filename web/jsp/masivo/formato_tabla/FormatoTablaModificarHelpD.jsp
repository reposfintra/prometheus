<!--  
	 - Author(s)       :      Jose de la rosa
	 - Description     :      AYUDA DESCRIPTIVA - Modificar Formato Tabla
	 - Date            :      29/11/2006 
	 - Copyright Notice:      TRANSPORTES SANCHEZ POLO S.A.
-->
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<%@page session="true"%> 
<%@page errorPage="/error/ErrorPage.jsp"%>
<%@page import="com.tsp.operation.model.beans.*, java.util.*"%>
<%@ include file="/WEB-INF/InitModel.jsp"%>
<%@ taglib uri="http://jakarta.apache.org/taglibs/input-0.90" prefix="input" %>
<html>
<head>
<title>AYUDA DESCRIPTIVA - Modificar Formato Tabla</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="<%= BASEURL %>/css/estilostsp.css" rel="stylesheet" type="text/css">
<script type='text/javascript' src="<%= BASEURL %>/js/boton.js"></script>
</head>

<body>
<br>
<table width="696"  border="2" align="center">
  <tr>
    <td width="811" >
      <table width="100%" border="0" align="center">
        <tr  class="subtitulo">
          <td height="24" colspan="2"><div align="center">Formato Tabla</div></td>
        </tr>
        <tr class="subtitulo1">
          <td colspan="2">Modificar Formato Tabla</td>
        </tr>
		<tr class="subtitulo1">
          <td colspan="2">INFORMACION DEL FORMATO TABLA</td>
        </tr>
		<tr>
           <td  class="fila">Nombre Tabla</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el nombre de la tabla a buscar. Este campo es de m&aacute;ximo 50 caracteres.</td>
        </tr>
        <tr>
           <td  class="fila">Formato Tabla</td>
          <td  class="ayudaHtmlTexto">Campo para escoger el formato de la tabla.</td>
        </tr>		
        <tr>
          <td  class="fila">Key</td>
          <td  class="ayudaHtmlTexto">Campo para chequear el campo que va a pertenecer a la llave primaria.</td>
        </tr>
		<tr>
          <td  class="fila"><input type="checkbox"></td>
          <td  class="ayudaHtmlTexto">Campo para chequear los campos que va a guardar.</td>
        </tr>
        <tr>
          <td  class="fila">Nombre Campo Tabla</td>
          <td  class="ayudaHtmlTexto">Campo que permite mostrar el nombre del campo de la tabla buscada.</td>
        </tr>
        <tr>
          <td  class="fila">Orden</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el orden de los campos. Este campo es de m&aacute;ximo 8 caracteres.</td>
        </tr>		
        <tr>
          <td  class="fila">Valor Default</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el valor por defecto del campo. Este campo es de m&aacute;ximo 50 caracteres.</td>
        </tr>		
        <tr>
          <td  class="fila">Nombre Campo JSP</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el nombre del campo de la JSP. Este campo es de m&aacute;ximo 50 caracteres.</td>
        </tr>		
        <tr>
          <td  class="fila">Nombre Etiqueta JSP</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el nombre del campo que se va a mostrar en la vista.</td>
        </tr>
        <tr>
          <td  class="fila">Validar</td>
          <td  class="ayudaHtmlTexto">Campo para para escoger si el campo de la tabla se debe validar con otra tabla.</td>
        </tr>		
        <tr>
          <td  class="fila">Tabla a Validar</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el nombre de la tabla que se va validar. Este campo es de m&aacute;ximo 50 caracteres.</td>
        </tr>
        <tr>
          <td  class="fila">Campo a Validar</td>
          <td  class="ayudaHtmlTexto">Campo para digitar el nombre del campo que se va validar. Este campo es de m&aacute;ximo 50 caracteres.</td>
        </tr>		
        <tr>
          <td class="fila">Bot&oacute;n Modificar </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n para confirmar y realizar la modificaci&oacute;n del Formato Tabla.</td>
        </tr>
		<tr>
          <td class="fila">Bot&oacute;n Eliminar </td>
          <td  class="ayudaHtmlTexto">Bot&oacute;n que elimina todos los registros de la tabla con ese formato.</td>
        </tr>
		<tr>
          <td width="149" class="fila">Bot&oacute;n Salir</td>
          <td width="525"  class="ayudaHtmlTexto">Bot&oacute;n para salir de la vista 'Modificar Formato Tabla' y volver a la vista del menu.</td>
        </tr>
      </table>
    </td>
  </tr>
</table>
<p></p>
<center>
<img src = "<%=BASEURL%>/images/botones/salir.gif" style = "cursor:hand" name = "imgsalir" onClick = "parent.close();" onMouseOver="botonOver(this);" onMouseOut="botonOut(this);">
</center>
</body>
</html>
