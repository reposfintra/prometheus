<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="ES">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Plantilla</title>


         <!----------------------------------------JQUERY--------------------------------------------->
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css"/>     
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>
        <!----------------------------------------JQUERY--------------------------------------------->

        <!-------------------------------------JQGRID----------------------------------------->
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery.jqGrid/ui.jqgrid.css"/>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script> 
        <!-------------------------------------/JQGRID----------------------------------------->

        <!-------------------------------------Notificaciones----------------------------------------->
        <link type="text/css" rel="stylesheet" href="./css/toastr.min.css"/>
        <script type="text/javascript" src="./js/toastr.min.js"></script>
        <!-------------------------------------/Notificaciones----------------------------------------->

        <!----------------------------------------Pre-Load--------------------------------------------->
        <!--se necesita un div para que funcione el preload se llama loader-wrapper-->

        <link type="text/css" rel="stylesheet" href="./css/main2.css">

        <!----------------------------------------Pre-Load--------------------------------------------->

        <!----------------------------------------bootstrap--------------------------------------------->
        <link href="./css/bootstrap/css/bootsrtap-3.3.7.min.css" rel="stylesheet" type="text/css"/>
        <!----------------------------------------bootstrap--------------------------------------------->
        
        
        <!----------------------------------------js-plantilla--------------------------------------------->
        <script src="./js/plantilla.js" type="text/javascript"></script>
        <!----------------------------------------js-plantilla--------------------------------------------->
        
        <!----------------------------------------JQUERY- Confirmation------------------------------------->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.2.3/jquery-confirm.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.2.3/jquery-confirm.min.js"></script>
        <!----------------------------------------JQUERY- Confirmation------------------------------------->

    </head>

    <body>

        <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=Plantilla"/>

        <div class="container-fluid">

        </div>

        
        

        <!-- **********************************************************************************************************************************************************
                                                                             loader-wrapper
        *********************************************************************************************************************************************************** -->
        <div id="loader-wrapper">
            <div id="loader"></div>
            <img id="logowast" src="/fintra/images/Selectrik.png" style="z-index: 10002 !important">
            <div class="loader-section section-left">	
            </div>
            <div class="loader-section section-right"></div>
        </div>


        <!-- **********************************************************************************************************************************************************
                                                                        FIN  loader-wrapper
        *********************************************************************************************************************************************************** -->


    </body>
</html>
