<%-- 
    Document   : variables_score
    Created on : Jun 5, 2019, 10:51:03 AM
    Author     : jdbc
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>VARIABLES DE PUNTAJE</title>
        <link href="./css/popup.css" rel="stylesheet" type="text/css">
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/TransportadorasApi.css " />
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery-ui/jquery.ui.min.js"></script>
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>   
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.jqGrid.min.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/js/jquery.tabletojson.js"></script>    
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid-4.7.0/plugins/jquery.contextmenu.js"></script>
        <style>
            #content-section {
                margin-top: 100px;                
            }
            
            #buscador-wrapper {
                width: 750px;
                margin: 0 auto 20px auto;
                background-color: white;
                border-color: #2A88C8;
                border-width: 1px;
                border-style: solid;
                border-radius: 10px;
                padding: 10px;
            }
            
            #titulo-wrapper {
                background-color: #2A88C8;
                padding: 5px 0;
                margin-bottom: 10px;
                color: white;
                text-align: center;
            }
            
            h1 {
                margin: 0;
                font-weight: bolder;
            }
            
            #identificador {
                width: 125px;
            }
             
            #gbox_tVariables-score {
                margin: 20px auto;
            }
            
            #gbox_tFiltros-duro {
                margin: 20px auto;
                width: 930px;
            }
            
        </style>
    </head>
    <body>
        <div id="capaSuperior" style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=VARIABLES DE PUNTAJE"/>
        </div>
        <section id="content-section">
            <section id="buscador-wrapper">
                <div id="filtro-wrapper">
                    <div id="titulo-wrapper">
                        <h1 id="titulo-filtro">FILTRO</h1>
                    </div>
                    <div id="campos-wrapper">
                        <label for="modelo">Modelo</label>
                        <select id="modelo">
                            <option value="SCORECARD">Score Card</option>
                            <option value="FDURO">Filtro Duro</option>
                        </select>
                        <label for="unidadNegocio">Unidad de negocio</label>
                        <select id="unidadNegocio">
                            <option value="MICRO">Microcr�dito</option>
                            <option value="EDUCA">Educativo</option>
                        </select>
                        <label for="opcion">Buscar por</label>
                        <select id="opcion">
                            <option value="10">C�dula cliente</option>
                            <option value="11">N�mero de solicitud</option>
                        </select>
                        <input id="identificador" type="number"/>
                        <button id="button-buscar" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only">
                            <span class="ui-button-text">Buscar</span>
                        </button>
                    </div>
                </div>
            </section>
            <section id="table-wrapper">
                <table id="tVariables-score"></table>
            </section>
            <section id="table-wrapper2">
                <table id="tFiltros-duro"></table>
                <div id="page_Fduro"></div>
            </section>            
        </section>
        <div id="info"  class="ventana" >
            <p id="notific"></p>
        </div>
        <div id="div_desc_filtros_duros"  style="width: 530px;display:none;" > 
        <!--<table border="1" aling="center" style="width: 100%">-->
        <table width="100%"  border="1" cellpadding="1" cellspacing="1" bordercolor="#999999" bgcolor="F7F5F4" class="letras">
            <colgroup>
                    <col style="width: 10%"/>
                    <col style="width: 40%"/>                    
                    <col style="width: 10%"/>
            </colgroup>
            <thead>
                    <tr>
                        <th rowspan="2"><h3>Filtros Duros</h3></th>
                        <th rowspan="2"><h3>Descripcion</h3></th>
                        <th rowspan="2"><h3>Decision</h3></th>
                    </tr>
            </thead>
            <tbody>
                    <tr>
                            <th>Filtro1</th>
                            <td>M�ximo dos (2) moras hist�ricas de 60+ d�as en todos los sectores</td>
                            <td>0 : Pasa el filtro <br>1 : No pasa el filtro</td>
                    </tr>
                    <tr>
                            <th>Filtro2</th>
                            <td>M�ximo dos (2) moras de 30+ a la fecha en financiero, real o cooperativas</td>
                            <td>0 : Pasa el filtro <br>1 : No pasa el filtro</td>
                    </tr>
                    <tr>
                            <th>Filtro3</th>
                            <td>Si posee una (1) mora de 30+ a la fecha en TELCOS, enviar a zona gris</td>
                            <td>0 : Pasa el filtro <br>1 : No pasa el filtro <br>2 : Pasa a zona gris</td>
                    </tr>
                    <tr>
                            <th>Filtro4</th>
                            <td>M�ximo una (1) cuenta reestructurada</td>
                            <td>0 : Pasa el filtro <br>1 : No pasa el filtro</td>
                    </tr>
                    <tr>
                            <th>Filtro5</th>
                            <td>M�ximo seis (6) huellas en los �ltimos 6 meses</td>
                            <td>0 : Pasa el filtro <br>1 : No pasa el filtro</td>
                    </tr>
                    <tr>
                            <th>Filtro6</th>
                            <td>No tener cuentas embargadas o en mal manejo</td>
                            <td>0 : Pasa el filtro <br>1 : No pasa el filtro</td>
                    </tr>
                    <tr>
                            <th>Filtro7</th>
                            <td>Lista Restrictiva</td>
                            <td>0 : Pasa el filtro <br>1 : No pasa el filtro</td>
                    </tr>
                    <!--<tr>
                            <th>Decision</th>
                            <td>Decisi�n a partir de la respuesta de cada uno de los siete (7) filtros</td>
                            <td>0 : Pasa el filtro <br>1 : No pasa el filtro <br>2 : Pasa a zona gris</td>
                    </tr>-->
        </table>
        </div> 
        <script src="./js/variables_score.js"></script>
    </body>
</html>
