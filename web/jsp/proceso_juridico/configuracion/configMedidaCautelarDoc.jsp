<%-- 
    Document   : configMedidaCautelar
    Created on : 11/09/2015, 03:32:45 PM
    Author     : mcastillo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Configuración Medidas Cautelares</title>
        
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <link href="./css/popup.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script> 
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script src="./js/jquery/editor/nicEdit.js" type="text/javascript"></script>
        <script src="./js/demandaDocs.js" type="text/javascript"></script>
        
        <!--css logica de negocio-->
        <link href="./css/proceso_ejecutivo.css" rel="stylesheet" type="text/css">
        
    </head>
    <body>
        <div id="capaSuperior"  style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=CONFIGURAR MEDIDA CAUTELAR"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">
            <center>
                <div id="div_medida"  style="width: 900px" >  
                    
                    <h4>Encabezado</h4> 
                    <textarea id ="header" name="header" class="editor"  rows="5" ></textarea>
                    </br> 

                    <h4>Intro</h4> 
                    <textarea id ="intro" name="intro" class="editor" rows="10"></textarea>
                    </br> 
                    
                    <div id="divMedidas">
                        <h4> <span> MEDIDAS </span> </h4>               
                    </div>
                    
                    <h4>Conclusion</h4> 
                    <textarea id ="conclusion" name="conclusion" class="editor"  rows="10"></textarea>
                    </br> 
                    
                    <h4>Pie del documento</h4> 
                    <textarea id ="footer" name="footer" class="editor"  rows="5" ></textarea>
                    </br> 
                    
                    <h4>Pie de pagina</h4> 
                    <textarea id ="footerpage" name="footerpage" class="editor"  rows="3" ></textarea>
                    </br>                   
                    <div id="btn_save_config" title="Guarda la configuración" style="position:absolute; left: 47%; width: 10%; "> 
                        <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" > Guardar </span>
                    </div>
                    <div id="btn_close" title="Cierra la ventana" style="position:absolute; left: 52%; width: 10%; "> 
                        <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple"> Salir </span>
                    </div>
                </div>
            </center>
    
            <div id="dialogMsj" title="Mensaje" style="display:none;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div>  

        </div>
 
        <script type="text/javascript">
            bkLib.onDomLoaded(function() {          
                    new nicEditor({iconsPath : './js/jquery/editor/nicEditorIcons.gif'}).panelInstance('header');
                    new nicEditor({iconsPath : './js/jquery/editor/nicEditorIcons.gif'}).panelInstance('intro');
                    new nicEditor({iconsPath : './js/jquery/editor/nicEditorIcons.gif'}).panelInstance('conclusion');
                    new nicEditor({iconsPath : './js/jquery/editor/nicEditorIcons.gif'}).panelInstance('footer');
                    new nicEditor({iconsPath : './js/jquery/editor/nicEditorIcons.gif'}).panelInstance('footerpage');         
            });
            initConfigMedidaDoc();
        </script>
 
    </body>
</html>
