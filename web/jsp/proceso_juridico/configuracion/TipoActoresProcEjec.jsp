<%-- 
    Document   : TipoActoresProcEjec
    Created on : 23/09/2015, 04:39:44 PM
    Author     : mcastillo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Tipo Actores Proceso Juridico</title>
        
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link href="./css/popup.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script> 
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script src="./js/actoresProcEjec.js" type="text/javascript"></script>

        <!--css logica de negocio-->
        <link href="./css/proceso_ejecutivo.css" rel="stylesheet" type="text/css">

        <!--jqgrid--> 
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
    </head>
    <body>
        <div id="capaSuperior"  style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=TIPO ACTORES PROCESO JURIDICO"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">
         
            <center>
                <br>
                <table id="tabla_tipo_actores"></table>
                <div id="page_tabla_tipo_actores"></div>    
            </center>
            
        <div id="div_tipoActores"  style="display: none; width: 800px" >                       
            <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 60px;padding: 0px 10px 5px 10px">
                </br>
                <table aling="center" style=" width: 100%" >
                    <tr>
                    <input type="hidden" id="idTipo" name="idTipo">  
                    <td style="width: 10%"><span>Nombre</span></td>                          
                    <td style="width: 90%"><input type="text" id="tipo" name="tipo" style=" width: 320px" ></td>                            
                    </tr>                        
                </table>
            </div>  
            </br> 
        </div>     
            
        <div id="dialogMsj" title="Mensaje" style="display:none;">
            <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
        </div>  
            
        </div>
        <script type="text/javascript">  
               initTipoActores();    
        </script>
        
    </body>
</html>
