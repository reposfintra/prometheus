<%-- 
    Document   : configurarEstadosCartera
    Created on : 1/09/2015, 02:36:26 PM
    Author     : mcastillo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Configuración estados de la cartera</title>
        
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <link href="./css/popup.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script> 
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script src="./js/MenuAplicacionPagos.js" type="text/javascript"></script>
        <script src="./js/carteraStatus.js" type="text/javascript"></script>

        <!--css logica de negocio-->
        <link href="./css/proceso_ejecutivo.css" rel="stylesheet" type="text/css">

        <!--jqgrid--> 
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
    </head>
    <body>
       <div id="capaSuperior"  style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=CONFIGURACIÓN ESTADOS CARTERA"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">
            <div id="div_config_estados_cartera">  
                <center>
                    <br>
                    <table id="tabla_unidades_negocio"></table>
                    <div id="page_tabla_unidades_negocio"></div> 
                </center>
            </div>     
            <!-- Dialogo ventana adición unidades de negocio -->
            <div id="dialogAsignarUndProceso" title="Agregar nueva unidad al proceso" style="display:none;">       
                <br>                         
                    <table border="0" align="left" cellpadding="1" cellspacing="1" class="labels">
                        <tr>
                            <td style="width: 25%"><span>Unidad de negocio<b style="color:red">*</b></span></td>        
                            <td style="width: 75%">
                                <select name="unidad_negocio" class="combo_180px" id="unidad_negocio" style="width: 250px"></select>
                            </td>
                        </tr>                        
                    </table>              
            </div>
            
            <div id="dialogMsj" title="Mensaje" style="display:none;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div>  
            
        </div>
    </body>
</html>
