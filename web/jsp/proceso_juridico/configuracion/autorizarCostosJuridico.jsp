<%-- 
    Document   : autorizarCostosJuridico
    Created on : 28/09/2015, 04:32:01 PM
    Author     : mcastillo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Autorizar Costos Proceso Ejecutivo</title>
        
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link href="./css/popup.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script> 
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script src="./js/etapasProcEjec.js" type="text/javascript"></script>

        <!--css logica de negocio-->
        <link href="./css/proceso_ejecutivo.css" rel="stylesheet" type="text/css">

        <!--jqgrid--> 
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
    </head>
    <body>
        <div id="capaSuperior"  style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=AUTORIZACION COSTOS"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">
         
            <center>
                <br>
                <table id="tabla_autoriz_costos"></table>
                <div id="page_tabla_autoriz_costos"></div>    
            </center>
            <!-- Dialogo ventana autorizar costos> -->
            <div id="dialogAprobacionCostos" title="Aprobación costos proceso juridico" style="display:none;">       
                <br>
                <fieldset>                   
                    <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                        <tr>
                            <td style="width: 25%"><span>Seleccione el estado<b style="color:red">*</b></span></td>        
                            <td style="width: 75%">
                                <select name="estados_aprob" class="combo_180px" id="estados_aprob" style="width: 250px"></select>
                            </td>
                        </tr>
                    </table>
                </fieldset>
            </div>
            <div id="dialogMsj" title="Mensaje" style="display:none;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div> 
         </div>
        <script type="text/javascript">
                initAutorizacion();      
        </script>              
    </body>
</html>
