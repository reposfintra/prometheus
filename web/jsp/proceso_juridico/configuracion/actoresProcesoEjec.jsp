<%-- 
    Document   : actoresProcesoEjec
    Created on : 23/09/2015, 04:39:11 PM
    Author     : mcastillo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Actores Proceso Juridico</title>
        
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link href="./css/popup.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script> 
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script src="./js/actoresProcEjec.js" type="text/javascript"></script>

        <!--css logica de negocio-->
        <link href="./css/proceso_ejecutivo.css" rel="stylesheet" type="text/css">

        <!--jqgrid--> 
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
    </head>
    <body>
        <div id="capaSuperior"  style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=ACTORES PROCESO JURIDICO"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">
         
            <center>
                <br>
                <table id="tabla_actores"></table>
                <div id="page_tabla_actores"></div>    
            </center>
            
        <div id="div_actores"  style="display: none; width: 800px" >                       
                </br>

                     <table aling="center" style=" width: 100%" >
                         <input type="hidden" id="idActor" name="idActor">  
                         <tr>                            
                             <td style="width: 10%"><span>Tipo:<b style="color:red">*</b></span></td>
                             <td style="width: 40%"> <select name="tipo" style="font-size: 14px;width:225px" id="tipo">
                             </select></td>                           
                             <td style="width: 20%"><span>Estado:</span></td>   
                             <td style="width: 30%;padding:10px">
                                 <input type="radio" name="estado" value="A" checked> Activo
                                 <input type="radio" name="estado" value="I" > Inactivo
                             </td>  
                         </tr> 
                         <tr>
                                <td style="width: 10%"><span>Tipo Doc.:<b style="color:red">*</b></span></td>
                                <td style="width: 40%"> <select name="tipo_doc" style="font-size: 14px;width:225px" id="tipo_doc">
                                </select></td>      
                                <td style="width: 10%"><span>Lugar Exp.:</span></td>                          
                                <td style="width: 30%"><input type="text" id="doc_lugar_exp" name="doc_lugar_exp" style=" width: 248px;" ></td>                              
                         </tr>
                         <tr>
                             <td style="width: 10%"><span>Documento:<b style="color:red">*</b></span></td>                          
                             <td style="width: 30%"><input type="text" id="documento" name="documento" class="solo-numero" maxlength="15" style=" width: 218px;text-transform: uppercase;" ></td>   
                             <td style="width: 10%"><span>Direcci�n:</span></td>                          
                             <td style="width: 40%"><input type="text" id="direccion" name="direccion" style=" width: 248px;" ></td>  
                         </tr>
                         <tr>
                              <td style="width: 10%"><span>Nombre:<b style="color:red">*</b></span></td>   
                              <td style="width: 100%" colspan="4"><textarea id ="nombre" name="nombre" rows="1" maxlength="150" style="text-transform: uppercase;resize:none;width: 99%"></textarea></td>                      
                         </tr>
                         <tr>    
                             <td style="width: 10%"><span>Pa�s:<b style="color:red">*</b></span></td>
                             <td style="width: 50%"> <select name="pais" style="font-size: 14px;width:225px" id="pais">
                                 </select></td>
                             <td style="width: 10%"><span>Email:</span></td>                          
                             <td style="width: 40%"><input type="text" id="email" name="email" style=" width: 248px;" ></td> 
                         </tr>
                         <tr>  
                             <td style="width: 10%"><span>Departamento:<b style="color:red">*</b></span></td>
                             <td style="width: 40%"> <select name="departamento" style="font-size: 14px;width:225px" id="departamento">
                                 </select></td>
                                   <td style="width: 10%"><span>Celular:</span></td>                          
                             <td style="width: 30%"><input type="text" id="celular" name="celular" class="solo-numero" maxlength="10" style=" width: 248px;text-transform: uppercase;" ></td>  
                         </tr>
                         <tr>
                             <td style="width: 10%"><span>Ciudad:<b style="color:red">*</b></span></td>
                             <td style="width: 50%"> <select name="ciudad" style="font-size: 14px;width:225px" id="ciudad"> 
                                 </select></td>
                             <td style="width: 10%"><span>Tarjeta Prof.:</span></td>                          
                             <td style="width: 30%"><input type="text" id="tarjeta_prof" name="tarjeta_prof" maxlength="10" style=" width: 248px;text-transform: uppercase;" ></td>  
                         </tr>
                         <tr>  
                             <td style="width: 10%"><span>Tel�fono:</span></td>                          
                             <td style="width: 40%"><input type="text" id="telefono" name="telefono" style=" width: 218px;text-transform: uppercase;" ></td>  
                             <td style="width: 10%"><span>Ext:</span></td>                          
                             <td style="width: 40%"><input type="text" id="ext" name="ext" class="solo-numero" maxlength="4" style=" width: 80px;text-transform: uppercase;" ></td>
                         </tr>
                     </table> 
                </br> 
         </div>

            
        <div id="dialogMsj" title="Mensaje" style="display:none;">
            <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
        </div>  
            
        </div>
        <script type="text/javascript">  
               initActores();    
        </script>
    </body>
</html>
