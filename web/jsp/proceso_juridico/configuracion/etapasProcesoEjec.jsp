<%-- 
    Document   : etapasProcesoEjec
    Created on : 2/09/2015, 05:55:05 PM
    Author     : mcastillo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Etapas proceso ejecutivo</title>
        
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link href="./css/popup.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script> 
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script src="./js/etapasProcEjec.js" type="text/javascript"></script>

        <!--css logica de negocio-->
        <link href="./css/proceso_ejecutivo.css" rel="stylesheet" type="text/css">

        <!--jqgrid--> 
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        
    </head>
    <body>
       <div id="capaSuperior"  style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=ETAPAS PROCESO EJECUTIVO"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">
         
            <center>
                <br>
                <table id="tabla_etapas"></table>
                <div id="page_tabla_etapas"></div>    
            </center>
            
             <div id="div_etapas"  style="display: none; width: 800px" >                       
                 <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 110px;padding: 0px 10px 5px 10px">
                     </br>
                     <table aling="center" style=" width: 100%" >
                         <tr>
                            <input type="hidden" id="idEtapa" name="idEtapa">  
                            <td style="width: 10%"><span>Nombre</span></td>                          
                            <td style="width: 40%"><input type="text" id="nometapa" name="nometapa" style=" width: 350px" ></td>   
                            <td style="width: 15%"><span>D�as estimados</span></td>                          
                            <td style="width: 25%"><input type="text" id="estimado_dias" name="estimado_dias" class="solo-numero" style=" width: 120px" ></td>
                         </tr>                       
                         <tr>
                                <td style="width: 10%"><span>Descripcion:</span></td>   
                                <td style="width: 90%" colspan="4"><textarea id ="descetapa" name="descetapa" rows="3" maxlength="300" style="resize:none;width:96%"></textarea></td>                        
                         </tr>
                     </table>
                 </div>  
                </br> 
         </div>
         <div id="div_editar_etapas"  style="display: none; width: 850px" >
              <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 130px;padding: 0px 10px 5px 10px">
                     </br>
                     <table aling="center" style=" width: 100%" >
                        <tr>
                            <input type="hidden" id="idEtapaEdit" name="idEtapaEdit"> 
                            <td style="width: 10%"><span>Nombre</span></td>                          
                            <td style="width: 50%"><input type="text" id="nometapaEdit" name="nometapaEdit" style=" width: 350px" ></td>
                            <td style="width: 15%"><span>D�as estimados</span></td>                          
                            <td style="width: 25%"><input type="text" id="estimado_diasEdit" name="estimado_diasEdit"  class="solo-numero" style=" width: 168px" ></td>
                        </tr>                      
                        <tr>
                            <td style="width: 10%"><span>Descripcion:</span></td>   
                            <td style="width: 90%" colspan="4"><textarea id ="descetapaEdit" name="descetapaEdit" rows="3" maxlength="300" style="resize:none;width:98%"></textarea></td>                        
                        </tr>    
                        <tr>
                            <td align="left" colspan="3">
                            <td style="float:right" align="right">                                
                                <button id="btn_actualizarEtapa" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" 
                                        role="button" aria-disabled="false">
                                    <span class="ui-button-text">Actualizar</span>
                                </button>
                            </td>
                        </tr>                    
                     </table>
                </div>  
                </br>  
                <div id="div_costos_etapa" style="position:absolute; left: 3%; top: 33%; width: 40% ">    
                    <table border="0"  align="center">
                        <tr>
                            <td>
                                <table id="costosEtapa" ></table>
                                <div id="page_tabla_costos_etapa"></div>
                            </td>
                        </tr>
                    </table> 
                </div>       
                <div id="div_respuestas_etapa" style="position: absolute; left: 52%; top: 33%; width: 40% ">    
                    <table border="0"  align="center">
                        <tr>
                            <td>
                                <table id="respuestasEtapa" ></table>
                                <div id="page_tabla_respuestas_etapa"></div>
                            </td>
                        </tr>
                    </table>   
                </div>                      
        </div> 
            
        <div id="div_respuestasEtapa" style="display: none; width: 800px" >                
                 
                 <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 135px;padding: 0px 10px 5px 10px">
                     </br>
                     <table aling="center" style=" width: 100%" >
                         <tr>     
                            <input type="hidden" id="idRespuesta" name="idRespuesta"> 
                            <td style="width: 10%"><span>Nombre</span></td>
                            <td style="width: 40%"><input type="text" id="respuesta" name="respuesta" style=" width: 222px" </td>
                            <td style="width: 25%"><span>D�as estimados</span></td>                          
                            <td style="width: 25%"><input type="text" id="estimado_dias_resp" name="estimado_dias_resp" class="solo-numero" style=" width: 130px" ></td>
                         </tr>
                         <tr>
                             <td style="width: 10%"><span>Descripcion:</span></td>   
                             <td style="width: 90%" colspan="4"><textarea id ="descRespuesta" name="descRespuesta" rows="2" maxlength="300" style="resize:none;width:95%"></textarea></td>                        
                         </tr>
                         <tr>
                             <td  style="width: 10%">
                                 <label for="orden" > Orden :</label>                                                
                             </td>               
                             <td  style="width: 40%">
                                 <input type="text" id="secuencia" name="secuencia" class="solo-numero" style=" width: 150px" maxlength="2" >                             
                             </td>
                             <td style="width: 25%"><span>Finaliza proceso</span></td>   
                             <td style="width: 25%;padding:10px">
                                 <input type="radio" name="finproc" value="S">Si
                                 <input type="radio" name="finproc" value="N" checked> No
                             </td>                     
                         </tr>
                     </table>
                 </div>   
                 </br> 
        </div>   

        <div id="div_costosEtapa" style="display: none; width: 600px" >                
                 
                 <div id="filtros" class="ui-jqgrid ui-widget ui-widget-content ui-corner-all" style="height: 120px;width: 430px;padding: 0px 10px 5px 10px">
                     </br>
                     <table aling="center" style=" width: 100%" >
                         <tr>         
                            <input type="hidden" id="idCosto" name="idCosto"> 
                            <td style="width: 10%"><span>Concepto</span></td>
                            <td style="width: 90%" colspan="3"><input type="text" id="concepto" name="concepto" style=" width: 350px"></td>
                         </tr>
                         <tr>
                            <td  style="width: 10%">
                                 <label for="tipo" > Tipo :</label>                                                
                            </td>               
                            <td  style="width: 40%">
                                 <select name="tipo_costo" id="tipo_costo" style="width: 155px">
                                 </select>     
                            </td>
                            <td style="width: 25%"><span>Solo automotor</span></td>   
                            <td style="width: 25%;padding:10px">
                                <input type="radio" name="sauto" value="S">Si
                                <input type="radio" name="sauto" value="N" checked> No
                            </td>                     
                         </tr>
                         <tr>
                            <td style="width: 10%"><span id="lbl_valor">Valor</span><span id="money" style="float:right;display:none;">$</span></td>
                            <td style="width: 40%"><input type="text" id="valor" name="valor" class="solo-numeric"  style="width: 150px"><span id="percent" style="float:right;display:none;">%</span> </td>
                         </tr>    
                     </table>
                 </div>   
                 </br> 
        </div> 
             <!-- Dialogo ventana ver trazabilidad -->
            <div id="dialogTrazEtapa" title="Ver Trazabilidad Etapa" style="display:none;">       
                <center><table id="tabla_etapa_traz"></table></center>
            </div>
            <div id="dialogMsj" title="Mensaje" style="display:none;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div>  
            
        </div>
        <script type="text/javascript">
             initEtapas();      
        </script>       
    </body>
</html>
