<%-- 
    Document   : configDemandaDoc
    Created on : 11/09/2015, 03:32:10 PM
    Author     : mcastillo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Configurar Demanda</title>
        
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <link href="./css/popup.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script> 
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
<!--        <script src="./js/jquery/editor/nicEdit.js" type="text/javascript"></script>-->
        <script src="./js/jquery/editor/nicEdit-latest.js" type="text/javascript"></script>
        <script src="./js/demandaDocs.js" type="text/javascript"></script>
        
        <!--css logica de negocio-->
        <link href="./css/proceso_ejecutivo.css" rel="stylesheet" type="text/css">
        
    </head>
    <body>
        <div id="capaSuperior"  style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=CONFIGURAR DEMANDA"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">
            <center>
              <div id="div_demanda_docs"  style="display: none; width: 950px" >
                 <div id="tabs" style="width: 930px">
                    <ul>
                        <li><a href="#tab_demanda">Demanda</a></li>
                        <li><a href="#tab_medidas">Medidas Cautelares</a></li>
                        <li><a href="#tab_poder">Poder</a></li>
                        <span style="float:right;">
                            <img id="correspondencias" title="Ver correspondencias" src="/fintra/images/botones/iconos/tab_search.png" 
                                 style="margin-left: 5px; height: 100%; vertical-align: middle;"> 
                        </span>
                    </ul>
                    
                <div id="tab_demanda">  
                    
                    <h4>Encabezado</h4> 
                    <textarea id ="header_dem" name="header_dem"  class="editor" rows="5"></textarea>
                    </br> 

                    <h4>Intro</h4> 
                    <textarea id ="intro_dem" name="intro_dem" class="editor"  rows="10"></textarea>
                    </br> 
                    
                    <div id="divHechos" style="text-align: left">
                        <h4> <span> HECHOS <img id="mas_hechos" src="/fintra/images/botones/iconos/add.png" title="Agregar hecho"
                                                    style="margin-left: 5px; height: 100%; vertical-align: middle;"> 
                            </span> </h4>               
                    </div>

                    <div id="divPretensiones" style="text-align: left">
                        <h4> <span> PRETENSIONES <img id="mas_pretensiones" src="/fintra/images/botones/iconos/add.png" title="Agregar pretensión"
                                                          style="margin-left: 5px; height: 100%; vertical-align: middle;"> 
                            </span></h4>
                    </div>
                    
                     <h4>FUNDAMENTOS DE DERECHO</h4> 
                    <textarea id ="fundamentos" name="fundamentos" class="editor" rows="3"></textarea>
                    </br> 
                    
                     <h4>COMPETENCIA Y CUANTIA</h4> 
                    <textarea id ="competencia" name="competencia" class="editor" rows="3"></textarea>
                    </br> 
                    
                     <h4>PRUEBAS</h4> 
                    <textarea id ="prueba" name="prueba" class="editor" rows="5"></textarea>
                    </br> 
                    
                     <h4>ANEXOS</h4> 
                    <textarea id ="anexo" name="anexo" class="editor" rows="3"></textarea>
                    </br> 
                    
                     <h4>NOTIFICACIONES</h4> 
                    <textarea id ="notificacion" name="notificacion" class="editor" rows="10" ></textarea>
                    </br> 
                    
<!--                <h4>Conclusion</h4> 
                    <textarea id ="concl_dem" name="concl_dem" class="editor"  rows="10" ></textarea>
                    </br> -->
                    
                    <h4>Pie del documento</h4> 
                    <textarea id ="footer_dem" name="footer_dem" class="editor"  rows="5" ></textarea>
                    </br> 
                    
<!--                <h4>Pie de pagina</h4> 
                    <textarea id ="footerpage_dem" name="footerpage_dem"  class="editor" rows="3"></textarea>
                    </br> -->
                                        
                </div>
                <div id="tab_medidas">                  
                    <h4>Encabezado</h4> 
                    <textarea id ="header_med" name="header_med" class="editor"  rows="5" ></textarea>
                    </br> 

                    <h4>Intro</h4> 
                    <textarea id ="intro_med" name="intro_med" class="editor" rows="10"></textarea>
                    </br> 

                    <div id="divMedidas" style="text-align: left">     
                        <h4> <span> MEDIDAS <img id="mas_medidas" src="/fintra/images/botones/iconos/add.png" title="Agregar"
                                                          style="margin-left: 5px; height: 100%; vertical-align: middle;"> 
                            </span> </h4>                                 
                    </div>

<!--                    <h4>MEDIDAS</h4> 
                    <textarea id ="medidas" name="medidas" class="editor" rows="10"></textarea>
                    </br> -->

<!--                <h4>Conclusion</h4> 
                    <textarea id ="concl_med" name="concl_med" class="editor"  rows="10"></textarea>
                    </br> -->

                    <h4>Pie del documento</h4> 
                    <textarea id ="footer_med" name="footer_med" class="editor"  rows="5" ></textarea>
                    </br> 

<!--                <h4>Pie de pagina</h4> 
                    <textarea id ="footerpage_med" name="footerpage_med" class="editor"  rows="3" ></textarea>
                    </br>  -->
                </div>
                <div id="tab_poder">                   
                    <h4>Encabezado</h4> 
                    <textarea id ="header_pod" name="header_pod" class="editor"  rows="5"></textarea>
                    </br> 

                    <h4>Intro</h4> 
                    <textarea id ="intro_pod" name="intro_pod" class="editor" rows="10" ></textarea>
                    </br> 

<!--               <h4>Conclusion</h4> 
                    <textarea id ="concl_pod" name="concl_pod" class="editor" rows="10"></textarea>
                    </br> -->

                    <h4>Pie del documento</h4> 
                    <textarea id ="footer_pod" name="footer_pod" class="editor" rows="5"></textarea>
                    </br> 

<!--                <h4>Pie de pagina</h4> 
                    <textarea id ="footerpage_pod" name="footerpage_pod" class="editor"  rows="3"></textarea>
                    </br> -->
                </div>
              
            </div>
            <br>
            <div id="btn_save_config" title="Guarda la configuración" style="position:absolute; left: 890px; "> 
                <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple" > Guardar </span>
            </div>
            <div id="btn_close" title="Cierra la ventana" style="position:absolute; left: 990px; "> 
                <span aling="center"  class="form-submit-button form-submit-button-simple_green_apple"> Salir </span>
            </div>
         </div>                 
        </center>
            <!-- Dialogo ventana equivalencias variable -->
            <div id="equiv_variables" style="display:none;">  
                <table width="264" id="drag_popup_equivalencias" >
                    <tr>
                        <td  class="titulo_ventana" height="25">
                            <div style="float:left; font-size: 15px">CORRESPONDENCIAS</div> 
                            <div style="float:right"><a id="close" class="ui-widget-header ui-corner-all"><span>X</span></a></div>
                        </td>
                    </tr>

                </table>
                <center><table id="tbl_equiv_variables" class="tablas" style=" width: 95%"></table></center>
            </div>
            <div id="dialogMsj" title="Mensaje" style="display:none;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div>  
            
            <div id="div_asignar_cond_especial"  style="display: none; width: 400px">
                
            </div>

        </div>
 
        <script type="text/javascript">
          bkLib.onDomLoaded(function() {               
                new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('header_dem');
                new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('intro_dem');
                new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('fundamentos');
                new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('competencia');
                new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('prueba');
                new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('anexo');
                new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('notificacion');
//              new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('concl_dem');
                new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('footer_dem');
//              new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('footerpage_dem');
                new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('header_med');
                new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('intro_med');              
//              new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('concl_med');
                new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('footer_med');
//              new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('footerpage_med');
                new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('header_pod');
                new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('intro_pod');
//              new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('concl_pod');
                new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('footer_pod');
//              new nicEditor({maxHeight : 280, buttonList : ['bold','italic','underline','left','center','right','justify','ol','ul','fontSize','fontFamily','indent','outdent' ]}).panelInstance('footerpage_pod');
            });

            initConfigDemandaDoc();
        </script>
    </body>
</html>
