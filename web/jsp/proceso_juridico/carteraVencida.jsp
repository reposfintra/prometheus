<%-- 
    Document   : carteraVencida
    Created on : 8/09/2015, 10:59:11 AM
    Author     : mcastillo
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
        <title>Recepción de Procesos</title>
        
        <link type="text/css" rel="stylesheet" href="./css/jquery/jquery-ui/jquery-ui.css" />
        <link type="text/css" rel="stylesheet" href="./css/buttons/botonVerde.css"/>
        <link href="./css/popup.css" rel="stylesheet" type="text/css">

        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.min.js"></script> 
        <script type="text/javascript" src="./js/jquery-ui-1.8.5.custom.min.js"></script>
        <script src="./js/carteraVencida.js" type="text/javascript"></script>

        <!--css logica de negocio-->
        <link href="./css/proceso_ejecutivo.css" rel="stylesheet" type="text/css">

        <!--jqgrid--> 
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/grid.locale-es.js"></script>
        <script type="text/javascript" src="./js/jquery/jquery.jqGrid/js/jquery.jqGrid.min.js"></script>
        
    </head>
    <body>
        <div id="capaSuperior"  style="position:absolute; width:100%; height:100px; z-index:0; left: 0px; top: 0px;">
            <jsp:include page="/WEB-INF/toptsp.jsp?encabezado=RECEPCION PROCESOS"/>
        </div>
        <div id="capaCentral" style="position:absolute; width:100%; height:100%; z-index:0; left: 0px; top: 100px; ">
           <center>
               <fieldset style="width:900px">
                <legend class="labels">FILTROS</legend>

                <table width="850" border="0" align="left" cellpadding="1" cellspacing="1" class="labels">
                    <tr>
                        <td width="190">
                            <fieldset>
                                <legend>UNIDADES DE NEGOCIO</legend>
                                <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                    <tr>
                                        <td>
                                            <select name="unidad_negocio" class="combo_180px" id="unidad_negocio">

                                            </select>
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td>  
                        <td width="100">
                            <fieldset>
                                <legend>NEGOCIO</legend>
                                <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                    <tr>
                                        <td>
                                            <input type="text" name="negocio"  id="negocio">
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td> 
                        <td width="100">
                            <fieldset>
                                <legend>CEDULA</legend>
                                <table border="0" align="center" cellpadding="1" cellspacing="1" class="labels">
                                    <tr>
                                        <td>
                                            <input type="text" name="cedula" id="cedula" class="solo-numero">
                                        </td>
                                    </tr>
                                </table>
                            </fieldset>
                        </td> 
                        <td>
                            <span  class="form-submit-button form-submit-button-simple_green_apple" id="btn_search_negocios" />Buscar </span>
                        </td>
                    </tr>
                </table>
            </fieldset>           
                <br>
                <table id="tabla_cartera"></table>
                <div id="page_tabla_cartera"></div>    
            </center>
            <div id="dialogMsj" title="Mensaje" style="display:none;">
                <p style="font-size: 12px;text-align:justify;" id="msj" > Texto </p>
            </div>  

            <div id="divSalidaEx" title="Exportacion" style=" display: block" >
                <p  id="msjEx" style=" display:  none"> Espere un momento por favor...</p>
                <center>
                    <img id="imgloadEx" style="position: relative;  top: 7px; display: none " src="./images/cargandoCM.gif"/>
                </center>
                <div id="respEx" style=" display: none"></div>
            </div> 
        </div>
    </body>
</html>
